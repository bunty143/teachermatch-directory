if (window['dojo']) dojo.provide('dwr.interface.CGServiceAjax');
if (typeof this['CGServiceAjax'] == 'undefined') CGServiceAjax = {};
CGServiceAjax._path = '/dwr';
CGServiceAjax.getJobDescription = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobDescription', arguments);
};
CGServiceAjax.getEmailAddress = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getEmailAddress', arguments);
};
CGServiceAjax.getFieldOfSchoolList = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFieldOfSchoolList', arguments);
};
CGServiceAjax.getFieldOfSchoolList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFieldOfSchoolList', arguments);
};
CGServiceAjax.getFieldOfSchoolList = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFieldOfSchoolList', arguments);
};
CGServiceAjax.saveGraduationDate = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveGraduationDate', arguments);
};
CGServiceAjax.canleGraduationDate = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'canleGraduationDate', arguments);
};
CGServiceAjax.getFieldOfDistrictList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFieldOfDistrictList', arguments);
};
CGServiceAjax.saveMessage = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveMessage', arguments);
};
CGServiceAjax.getPhoneDetail = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getPhoneDetail', arguments);
};
CGServiceAjax.getCertificationGrid = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getCertificationGrid', arguments);
};
CGServiceAjax.getTranscriptGrid = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getTranscriptGrid', arguments);
};
CGServiceAjax.downloadTranscript = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadTranscript', arguments);
};
CGServiceAjax.downloadResume = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadResume', arguments);
};
CGServiceAjax.downloadPortfolioReport = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadPortfolioReport', arguments);
};
CGServiceAjax.getCoverLetter = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getCoverLetter', arguments);
};
CGServiceAjax.generatePDReport = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'generatePDReport', arguments);
};
CGServiceAjax.checkBaseCompeleted = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'checkBaseCompeleted', arguments);
};
CGServiceAjax.downloadGeneralKnowledge = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadGeneralKnowledge', arguments);
};
CGServiceAjax.downloadSubjectAreaExam = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadSubjectAreaExam', arguments);
};
CGServiceAjax.getIncompleteInfoHtml = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getIncompleteInfoHtml', arguments);
};
CGServiceAjax.checkSpCompleted = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'checkSpCompleted', arguments);
};
CGServiceAjax.checkAssessmentCompleted = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'checkAssessmentCompleted', arguments);
};
CGServiceAjax.setDistrictQPortfoliouestions = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'setDistrictQPortfoliouestions', arguments);
};
CGServiceAjax.downloadReferenceForCandidate = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadReferenceForCandidate', arguments);
};
CGServiceAjax.changeStatusElectronicReferences = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'changeStatusElectronicReferences', arguments);
};
CGServiceAjax.editElectronicReferences = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'editElectronicReferences', arguments);
};
CGServiceAjax.deleteVIdeoLink = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'deleteVIdeoLink', arguments);
};
CGServiceAjax.editVideoLink = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'editVideoLink', arguments);
};
CGServiceAjax.downloadUploadedDocumentForCg = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadUploadedDocumentForCg', arguments);
};
CGServiceAjax.saveUpdateElectronicReferences = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveUpdateElectronicReferences', arguments);
};
CGServiceAjax.findDuplicateReferences = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'findDuplicateReferences', arguments);
};
CGServiceAjax.saveOrUpdateVideoLink = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveOrUpdateVideoLink', arguments);
};
CGServiceAjax.removeReferencesForNoble = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'removeReferencesForNoble', arguments);
};
CGServiceAjax.getVideoPlayDivForProfile = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getVideoPlayDivForProfile', arguments);
};
CGServiceAjax.getDegreeMasterList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getDegreeMasterList', arguments);
};
CGServiceAjax.getRegionMasterList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getRegionMasterList', arguments);
};
CGServiceAjax.getUniversityMasterList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getUniversityMasterList', arguments);
};
CGServiceAjax.getFilterCertificateTypeList = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFilterCertificateTypeList', arguments);
};
CGServiceAjax.getSubjectByDistrict = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getSubjectByDistrict', arguments);
};
CGServiceAjax.getJobCategoryByDistrict = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobCategoryByDistrict', arguments);
};
CGServiceAjax.getJobCategoryByHQAndBranch = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobCategoryByHQAndBranch', arguments);
};
CGServiceAjax.getJobSubCategoryByCategory = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobSubCategoryByCategory', arguments);
};
CGServiceAjax.getJobListByTeacher = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobListByTeacher', arguments);
};
CGServiceAjax.getJobListByTeacherByProfile = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobListByTeacherByProfile', arguments);
};
CGServiceAjax.saveTags = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveTags', arguments);
};
CGServiceAjax.updateTFAByUser = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'updateTFAByUser', arguments);
};
CGServiceAjax.displayselectedTFA = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayselectedTFA', arguments);
};
CGServiceAjax.getOnlineActivityData = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getOnlineActivityData', arguments);
};
CGServiceAjax.changeExpiryActivityStatus = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'changeExpiryActivityStatus', arguments);
};
CGServiceAjax.getZoneListAll = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getZoneListAll', arguments);
};
CGServiceAjax.displayGeoZoneSchoolGrid = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayGeoZoneSchoolGrid', arguments);
};
CGServiceAjax.getSelectedSchoolsList = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getSelectedSchoolsList', arguments);
};
CGServiceAjax.showLetterOfIntentForCG = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'showLetterOfIntentForCG', arguments);
};
CGServiceAjax.getOnlineActivityQuestionsForCG = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getOnlineActivityQuestionsForCG', arguments);
};
CGServiceAjax.saveOnlineActivity = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveOnlineActivity', arguments);
};
CGServiceAjax.displayOnlineActivityForCG = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayOnlineActivityForCG', arguments);
};
CGServiceAjax.displayMultipleOnlineActivityForCG = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayMultipleOnlineActivityForCG', arguments);
};
CGServiceAjax.displayOnlineActivityNotesForCG = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayOnlineActivityNotesForCG', arguments);
};
CGServiceAjax.makeEPIIncomplete = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'makeEPIIncomplete', arguments);
};
CGServiceAjax.makeSpIncomplete = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'makeSpIncomplete', arguments);
};
CGServiceAjax.makeAssessmentIncomplete = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'makeAssessmentIncomplete', arguments);
};
CGServiceAjax.showAssessmentDetails = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'showAssessmentDetails', arguments);
};
CGServiceAjax.getTeacherIds = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getTeacherIds', arguments);
};
CGServiceAjax.getSlider = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getSlider', arguments);
};
CGServiceAjax.getiFrameForTree = function(callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getiFrameForTree', arguments);
};
CGServiceAjax.getBreakUpData = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getBreakUpData', arguments);
};
CGServiceAjax.getDistrictQPortfoliouestions = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getDistrictQPortfoliouestions', arguments);
};
CGServiceAjax.getDistrictAssessmentDetails = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getDistrictAssessmentDetails', arguments);
};
CGServiceAjax.saveAssessmentInvite = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveAssessmentInvite', arguments);
};
CGServiceAjax.getAssessmentInviteQuestionAnswers = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getAssessmentInviteQuestionAnswers', arguments);
};
CGServiceAjax.getAssessmentDetails = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getAssessmentDetails', arguments);
};
CGServiceAjax.sendAssessmentInvite = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'sendAssessmentInvite', arguments);
};
CGServiceAjax.displayExamCalendar = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayExamCalendar', arguments);
};
CGServiceAjax.getColorAndEvent = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getColorAndEvent', arguments);
};
CGServiceAjax.getDisabledSlotIds = function(callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getDisabledSlotIds', arguments);
};
CGServiceAjax.getStartEndDateByAssessment = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getStartEndDateByAssessment', arguments);
};
CGServiceAjax.getMultipleInviteAssessment = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getMultipleInviteAssessment', arguments);
};
CGServiceAjax.sendMultipleAssessmentInvite = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'sendMultipleAssessmentInvite', arguments);
};
CGServiceAjax.getScoreForQuestions = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getScoreForQuestions', arguments);
};
CGServiceAjax.getAvailability = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getAvailability', arguments);
};
