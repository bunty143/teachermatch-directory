function openWindow(url,windowName,width,height)
{
	//alert(url);
	windowName=windowName||"_blank";width=width||900;height=height||600;
	var left=(window.screen.availWidth-width)/2;
	var top=(window.screen.availHeight-height)/2;
	var mywindow=window.open(url,windowName,'width='+width+',height='+height+',left='+left+',top='+top+',scrollbars=1');
	//alert(mywindow);
	//alert(dwr.util.toDescriptiveString(mywindow,5));
	var len;

	try{
		len=mywindow.length;
	}catch(err){}


	/*if (!mywindow || mywindow.closed || typeof mywindow == 'undefined' || typeof mywindow.closed == 'undefined') {
       alert("dd");
		return true;
    }*/

	//alert(len);


	if ((!mywindow) || len==undefined || mywindow.length!=0 ){
		{
			//alert("A popup blocker has been detected, please disable your popup blocker or click the link to open the new window.");
			//alert("Pop-up window is blocked on this browser. Please allow pop-ups for this site to view this report.");
			$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
			$('#myModal').modal('show');
		}
	}else
		mywindow.focus();

	//for chrome only
	if (navigator && (navigator.userAgent.toLowerCase()).indexOf("chrome") > -1) {
		if (!mywindow)
		{
			//alert("Pop-up window is blocked on this browser. Please allow pop-ups for this site to view this report.");
			$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
			$('#myModal').modal('show');
		}
		else {
			mywindow.onload = function() {
				setTimeout(function() {
					if (mywindow.screenX === 0) {
						//alert("Pop-up window is blocked on this browser. Please allow pop-ups for this site to view this report.");
						$('#blockMessage').html(""+resourceJSON.PopUpWindowBlock+" <a target = '_blank' href='"+url+"'>"+resourceJSON.BtnClick+"</a> "+resourceJSON.ToOpenNewWindow+".");
						$('#myModal').modal('show');
						mywindow.close();
					}
				}, 0);
			};
		}
	}
}
//onclick="openWindow(appPath+'Pages/Service/Support.aspx','Support',650,490);return false;"
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
/*function pausecomp(millis) 
{
	var date = new Date();
	var curDate = null;

	do { curDate = new Date(); } 
	while(curDate-date < millis);
}*/
function pausecomp(ms) {
	ms += new Date().getTime();
	while (new Date() < ms){}
	} 
function generatePDReport(teacherId,hrefId)
{
	//alert("generatePDReport()");
	$('#loadingDiv').show();

	CandidateReportAjax.generatePDReport(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){

		$('#loadingDiv').hide();

		//document.getElementById(hrefId).href=data;
		if (data.indexOf(".doc") !=-1) {
		    $('#modalDownloadPDR').modal('hide');
		    document.getElementById('ifrmPDR').src = ""+data+"";
		}
		else
		{
			$('#modalDownloadPDR').modal('hide');
			document.getElementById('ifrmPDR').src = ""+data+"";
			try{
				$('#modalDownloadPDR').modal('show');
			}catch(err)
			  {}
		}
		//window.prompt('ss',data);
		/*var mywindow = window.open(data);
		if (!mywindow){
		    alert("A popup blocker has been detected, please click the link for to open the new window.");
		}
		 */		
		//openWindow(data,'Support',650,490);

		//alert(data);
		//window.open(data, '_blank', 'width=800,height=475,resizable=yes,toolbar=no,status=no,scrollable=no,menubar=no,directories=no,location=no,dependant=no');

		//<a href="" onClick="javascript:window.open('newwindow.htm', '_blank', 'width=800,height=475,resizable=yes,toolbar=no,status=no,scrollable=no,menubar=no,directories=no,location=no,dependant=no'); return false;" target="_blank" title="video(New&#xA;Window)">
		//<a href='' onClick=\"javascript:window.open('newwindow.htm', '_blank', 'width=800,height=475,resizable=yes,toolbar=no,status=no,scrollable=no,menubar=no,directories=no,location=no,dependant=no'); return false;\" target='_blank' title='video(New&#xA;Window)'>
		//$('linkurl').html("");

		//$('linkurl').html("vishwanath");
		//$('linkurl').html("<a href='' onClick=\"javascript:window.open('newwindow.htm', '_blank', 'width=800,height=475,resizable=yes,toolbar=no,status=no,scrollable=no,menubar=no,directories=no,location=no,dependant=no'); return false;\" target='_blank' title='video(New&#xA;Window)'>a</a>");
		//document.getElementById("linkurl").innerHTML="<a href='' id='sss' onClick=\"javascript:window.open('"+data+"', '_blank', 'width=800,height=475,resizable=yes,toolbar=no,status=no,scrollable=no,menubar=no,directories=no,location=no,dependant=no'); return false;\" target='_blank' title='video(New&#xA;Window)'>a</a>";
		// window.prompt('ss',document.getElementById("linkurl").innerHTML);
		//document.getElementById("linkurl").click();
		//$('#sss').click();
		/*var a = document.createElement('a');
		a.href="";
		a.target = '_blank';
		document.body.appendChild(a);
		a.click();*/
	}
	});	
}
//called from teacher dashboard
function checkBaseCompeleted(teacherId)
{
	//alert(teacherId);
	CandidateReportAjax.checkBaseCompeleted(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		//alert(data);
		if(data==1)
		{
			//generatePDReport(teacherId);
			$('#message2show').html(""+resourceJSON.ThankForYourInterest+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a> "+resourceJSON.MsgPurchaseYourReport+".");
			$('#myModal2').modal('show');
			notifyTMAdmins(teacherId);

		}else if(data==3)
		{
			//$('#message2show').html("Your Base Inventory is Timed Out. You cannot buy Professional Development Report. Please contact Client Services at  <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a>.");
			$('#message2show').html(""+resourceJSON.BaseInventoryIsTimeOut+" <a href=\"mailto:clientservices@teachermatch.net\">clientservices@teachermatch.net</a> "+resourceJSON.AnyFurtherQues+".");
			$('#myModal2').modal('show');
		}
		else
		{
			$('#message2show').html(""+resourceJSON.CompleteYourBaseInventory+".");
			$('#myModal2').modal('show');
		}
	}
	});	

}
//called form admin
function checkBaseCompeletedForPDR(teacherId,hrefId)
{
	//alert(teacherId);
	CandidateReportAjax.checkBaseCompeleted(teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		//alert(data);
		if(data==1)
		{
			generatePDReport(teacherId,hrefId);
			return false;

		}else if(data==3)
		{
			$('#message2show').html(""+resourceJSON.CandidateTimeOutBaseInvevtory+".");
			$('#myModal2').modal('show');
			return false;
		}
		else
		{
			$('#message2show').html(""+resourceJSON.CandidateHasNotComplete+".");
			$('#myModal2').modal('show');
			return false;
		}
	}
	});	

}
function notifyTMAdmins(teacherId)
{
	CandidateReportAjax.notifyTMAdmins(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		//alert(data);

	}
	});	
}

function generatePilarReport(teacherId,jobId,hrefId)
{
	jobOrder={jobId:jobId};
	//alert(jobOrder.jobId);
	//$('#loadingDiv').show();
	CandidateReportAjax.generatePilarReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		$('#loadingDiv').hide();
		//alert(data);
		if(data==0)
		{
			$('#message2show').html(resourceJSON.NoJobSpecificInventory);
			$('#myModal2').modal('show');
			return false;
		}else if(data==1)
		{
			$('#message2show').html(resourceJSON.CandidateHasNotCompleteThisJobOrder);
			$('#myModal2').modal('show');
			return false;
		}else if(data==2)
		{
			$('#message2show').html(resourceJSON.CandidateHasTimeOut);
			$('#myModal2').modal('show');
			return false;
		}else if(data==3)
			generateJSAReport(teacherId,jobOrder,hrefId);
	}
	});	
}

function generateJSAReport(teacherId,jobOrder,hrefId)
{
	//alert(jobOrder.jobId);
	$('#loadingDiv').show();
	CandidateReportAjax.generateJSAReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		$('#loadingDiv').hide();

		document.getElementById(hrefId).href=data;
		//window.open(data);
		//openWindow(data,'Support',650,490);
	}
	});	
}