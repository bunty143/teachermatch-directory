//js jquery.js
/*! jQuery v1.7.2 jquery.com | jquery.org/license */
(function(a, b) {
	function cy(a) {
		return f.isWindow(a) ? a : a.nodeType === 9 ? a.defaultView || a.parentWindow : !1
	}

	function cu(a) {
		if (!cj[a]) {
			var b = c.body,
				d = f("<" + a + ">").appendTo(b),
				e = d.css("display");
			d.remove();
			if (e === "none" || e === "") {
				ck || (ck = c.createElement("iframe"), ck.frameBorder = ck.width = ck.height = 0), b.appendChild(ck);
				if (!cl || !ck.createElement) cl = (ck.contentWindow || ck.contentDocument).document, cl.write((f.support.boxModel ? "<!doctype html>" : "") + "<html><body>"), cl.close();
				d = cl.createElement(a), cl.body.appendChild(d), e = f.css(d, "display"), b.removeChild(ck)
			}
			cj[a] = e
		}
		return cj[a]
	}

	function ct(a, b) {
		var c = {};
		f.each(cp.concat.apply([], cp.slice(0, b)), function() {
			c[this] = a
		});
		return c
	}

	function cs() {
		cq = b
	}

	function cr() {
		setTimeout(cs, 0);
		return cq = f.now()
	}

	function ci() {
		try {
			return new a.ActiveXObject("Microsoft.XMLHTTP")
		} catch (b) {}
	}

	function ch() {
		try {
			return new a.XMLHttpRequest
		} catch (b) {}
	}

	function cb(a, c) {
		a.dataFilter && (c = a.dataFilter(c, a.dataType));
		var d = a.dataTypes,
			e = {},
			g, h, i = d.length,
			j, k = d[0],
			l, m, n, o, p;
		for (g = 1; g < i; g++) {
			if (g === 1)
				for (h in a.converters) typeof h == "string" && (e[h.toLowerCase()] = a.converters[h]);
			l = k, k = d[g];
			if (k === "*") k = l;
			else if (l !== "*" && l !== k) {
				m = l + " " + k, n = e[m] || e["* " + k];
				if (!n) {
					p = b;
					for (o in e) {
						j = o.split(" ");
						if (j[0] === l || j[0] === "*") {
							p = e[j[1] + " " + k];
							if (p) {
								o = e[o], o === !0 ? n = p : p === !0 && (n = o);
								break
							}
						}
					}
				}!n && !p && f.error("No conversion from " + m.replace(" ", " to ")), n !== !0 && (c = n ? n(c) : p(o(c)))
			}
		}
		return c
	}

	function ca(a, c, d) {
		var e = a.contents,
			f = a.dataTypes,
			g = a.responseFields,
			h, i, j, k;
		for (i in g) i in d && (c[g[i]] = d[i]);
		while (f[0] === "*") f.shift(), h === b && (h = a.mimeType || c.getResponseHeader("content-type"));
		if (h)
			for (i in e)
				if (e[i] && e[i].test(h)) {
					f.unshift(i);
					break
				}
		if (f[0] in d) j = f[0];
		else {
			for (i in d) {
				if (!f[0] || a.converters[i + " " + f[0]]) {
					j = i;
					break
				}
				k || (k = i)
			}
			j = j || k
		}
		if (j) {
			j !== f[0] && f.unshift(j);
			return d[j]
		}
	}

	function b_(a, b, c, d) {
		if (f.isArray(b)) f.each(b, function(b, e) {
			c || bD.test(a) ? d(a, e) : b_(a + "[" + (typeof e == "object" ? b : "") + "]", e, c, d)
		});
		else if (!c && f.type(b) === "object")
			for (var e in b) b_(a + "[" + e + "]", b[e], c, d);
		else d(a, b)
	}

	function b$(a, c) {
		var d, e, g = f.ajaxSettings.flatOptions || {};
		for (d in c) c[d] !== b && ((g[d] ? a : e || (e = {}))[d] = c[d]);
		e && f.extend(!0, a, e)
	}

	function bZ(a, c, d, e, f, g) {
		f = f || c.dataTypes[0], g = g || {}, g[f] = !0;
		var h = a[f],
			i = 0,
			j = h ? h.length : 0,
			k = a === bS,
			l;
		for (; i < j && (k || !l); i++) l = h[i](c, d, e), typeof l == "string" && (!k || g[l] ? l = b : (c.dataTypes.unshift(l), l = bZ(a, c, d, e, l, g)));
		(k || !l) && !g["*"] && (l = bZ(a, c, d, e, "*", g));
		return l
	}

	function bY(a) {
		return function(b, c) {
			typeof b != "string" && (c = b, b = "*");
			if (f.isFunction(c)) {
				var d = b.toLowerCase().split(bO),
					e = 0,
					g = d.length,
					h, i, j;
				for (; e < g; e++) h = d[e], j = /^\+/.test(h), j && (h = h.substr(1) || "*"), i = a[h] = a[h] || [], i[j ? "unshift" : "push"](c)
			}
		}
	}

	function bB(a, b, c) {
		var d = b === "width" ? a.offsetWidth : a.offsetHeight,
			e = b === "width" ? 1 : 0,
			g = 4;
		if (d > 0) {
			if (c !== "border")
				for (; e < g; e += 2) c || (d -= parseFloat(f.css(a, "padding" + bx[e])) || 0), c === "margin" ? d += parseFloat(f.css(a, c + bx[e])) || 0 : d -= parseFloat(f.css(a, "border" + bx[e] + "Width")) || 0;
			return d + "px"
		}
		d = by(a, b);
		if (d < 0 || d == null) d = a.style[b];
		if (bt.test(d)) return d;
		d = parseFloat(d) || 0;
		if (c)
			for (; e < g; e += 2) d += parseFloat(f.css(a, "padding" + bx[e])) || 0, c !== "padding" && (d += parseFloat(f.css(a, "border" + bx[e] + "Width")) || 0), c === "margin" && (d += parseFloat(f.css(a, c + bx[e])) || 0);
		return d + "px"
	}

	function bo(a) {
		var b = c.createElement("div");
		bh.appendChild(b), b.innerHTML = a.outerHTML;
		return b.firstChild
	}

	function bn(a) {
		var b = (a.nodeName || "").toLowerCase();
		b === "input" ? bm(a) : b !== "script" && typeof a.getElementsByTagName != "undefined" && f.grep(a.getElementsByTagName("input"), bm)
	}

	function bm(a) {
		if (a.type === "checkbox" || a.type === "radio") a.defaultChecked = a.checked
	}

	function bl(a) {
		return typeof a.getElementsByTagName != "undefined" ? a.getElementsByTagName("*") : typeof a.querySelectorAll != "undefined" ? a.querySelectorAll("*") : []
	}

	function bk(a, b) {
		var c;
		b.nodeType === 1 && (b.clearAttributes && b.clearAttributes(), b.mergeAttributes && b.mergeAttributes(a), c = b.nodeName.toLowerCase(), c === "object" ? b.outerHTML = a.outerHTML : c !== "input" || a.type !== "checkbox" && a.type !== "radio" ? c === "option" ? b.selected = a.defaultSelected : c === "input" || c === "textarea" ? b.defaultValue = a.defaultValue : c === "script" && b.text !== a.text && (b.text = a.text) : (a.checked && (b.defaultChecked = b.checked = a.checked), b.value !== a.value && (b.value = a.value)), b.removeAttribute(f.expando), b.removeAttribute("_submit_attached"), b.removeAttribute("_change_attached"))
	}

	function bj(a, b) {
		if (b.nodeType === 1 && !!f.hasData(a)) {
			var c, d, e, g = f._data(a),
				h = f._data(b, g),
				i = g.events;
			if (i) {
				delete h.handle, h.events = {};
				for (c in i)
					for (d = 0, e = i[c].length; d < e; d++) f.event.add(b, c, i[c][d])
			}
			h.data && (h.data = f.extend({}, h.data))
		}
	}

	function bi(a, b) {
		return f.nodeName(a, "table") ? a.getElementsByTagName("tbody")[0] || a.appendChild(a.ownerDocument.createElement("tbody")) : a
	}

	function U(a) {
		var b = V.split("|"),
			c = a.createDocumentFragment();
		if (c.createElement)
			while (b.length) c.createElement(b.pop());
		return c
	}

	function T(a, b, c) {
		b = b || 0;
		if (f.isFunction(b)) return f.grep(a, function(a, d) {
			var e = !!b.call(a, d, a);
			return e === c
		});
		if (b.nodeType) return f.grep(a, function(a, d) {
			return a === b === c
		});
		if (typeof b == "string") {
			var d = f.grep(a, function(a) {
				return a.nodeType === 1
			});
			if (O.test(b)) return f.filter(b, d, !c);
			b = f.filter(b, d)
		}
		return f.grep(a, function(a, d) {
			return f.inArray(a, b) >= 0 === c
		})
	}

	function S(a) {
		return !a || !a.parentNode || a.parentNode.nodeType === 11
	}

	function K() {
		return !0
	}

	function J() {
		return !1
	}

	function n(a, b, c) {
		var d = b + "defer",
			e = b + "queue",
			g = b + "mark",
			h = f._data(a, d);
		h && (c === "queue" || !f._data(a, e)) && (c === "mark" || !f._data(a, g)) && setTimeout(function() {
			!f._data(a, e) && !f._data(a, g) && (f.removeData(a, d, !0), h.fire())
		}, 0)
	}

	function m(a) {
		for (var b in a) {
			if (b === "data" && f.isEmptyObject(a[b])) continue;
			if (b !== "toJSON") return !1
		}
		return !0
	}

	function l(a, c, d) {
		if (d === b && a.nodeType === 1) {
			var e = "data-" + c.replace(k, "-$1").toLowerCase();
			d = a.getAttribute(e);
			if (typeof d == "string") {
				try {
					d = d === "true" ? !0 : d === "false" ? !1 : d === "null" ? null : f.isNumeric(d) ? +d : j.test(d) ? f.parseJSON(d) : d
				} catch (g) {}
				f.data(a, c, d)
			} else d = b
		}
		return d
	}

	function h(a) {
		var b = g[a] = {},
			c, d;
		a = a.split(/\s+/);
		for (c = 0, d = a.length; c < d; c++) b[a[c]] = !0;
		return b
	}
	var c = a.document,
		d = a.navigator,
		e = a.location,
		f = function() {
			function J() {
				if (!e.isReady) {
					try {
						c.documentElement.doScroll("left")
					} catch (a) {
						setTimeout(J, 1);
						return
					}
					e.ready()
				}
			}
			var e = function(a, b) {
					return new e.fn.init(a, b, h)
				},
				f = a.jQuery,
				g = a.$,
				h, i = /^(?:[^#<]*(<[\w\W]+>)[^>]*$|#([\w\-]*)$)/,
				j = /\S/,
				k = /^\s+/,
				l = /\s+$/,
				m = /^<(\w+)\s*\/?>(?:<\/\1>)?$/,
				n = /^[\],:{}\s]*$/,
				o = /\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,
				p = /"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,
				q = /(?:^|:|,)(?:\s*\[)+/g,
				r = /(webkit)[ \/]([\w.]+)/,
				s = /(opera)(?:.*version)?[ \/]([\w.]+)/,
				t = /(msie) ([\w.]+)/,
				u = /(mozilla)(?:.*? rv:([\w.]+))?/,
				v = /-([a-z]|[0-9])/ig,
				w = /^-ms-/,
				x = function(a, b) {
					return (b + "").toUpperCase()
				},
				y = d.userAgent,
				z, A, B, C = Object.prototype.toString,
				D = Object.prototype.hasOwnProperty,
				E = Array.prototype.push,
				F = Array.prototype.slice,
				G = String.prototype.trim,
				H = Array.prototype.indexOf,
				I = {};
			e.fn = e.prototype = {
				constructor: e,
				init: function(a, d, f) {
					var g, h, j, k;
					if (!a) return this;
					if (a.nodeType) {
						this.context = this[0] = a, this.length = 1;
						return this
					}
					if (a === "body" && !d && c.body) {
						this.context = c, this[0] = c.body, this.selector = a, this.length = 1;
						return this
					}
					if (typeof a == "string") {
						a.charAt(0) !== "<" || a.charAt(a.length - 1) !== ">" || a.length < 3 ? g = i.exec(a) : g = [null, a, null];
						if (g && (g[1] || !d)) {
							if (g[1]) {
								d = d instanceof e ? d[0] : d, k = d ? d.ownerDocument || d : c, j = m.exec(a), j ? e.isPlainObject(d) ? (a = [c.createElement(j[1])], e.fn.attr.call(a, d, !0)) : a = [k.createElement(j[1])] : (j = e.buildFragment([g[1]], [k]), a = (j.cacheable ? e.clone(j.fragment) : j.fragment).childNodes);
								return e.merge(this, a)
							}
							h = c.getElementById(g[2]);
							if (h && h.parentNode) {
								if (h.id !== g[2]) return f.find(a);
								this.length = 1, this[0] = h
							}
							this.context = c, this.selector = a;
							return this
						}
						return !d || d.jquery ? (d || f).find(a) : this.constructor(d).find(a)
					}
					if (e.isFunction(a)) return f.ready(a);
					a.selector !== b && (this.selector = a.selector, this.context = a.context);
					return e.makeArray(a, this)
				},
				selector: "",
				jquery: "1.7.2",
				length: 0,
				size: function() {
					return this.length
				},
				toArray: function() {
					return F.call(this, 0)
				},
				get: function(a) {
					return a == null ? this.toArray() : a < 0 ? this[this.length + a] : this[a]
				},
				pushStack: function(a, b, c) {
					var d = this.constructor();
					e.isArray(a) ? E.apply(d, a) : e.merge(d, a), d.prevObject = this, d.context = this.context, b === "find" ? d.selector = this.selector + (this.selector ? " " : "") + c : b && (d.selector = this.selector + "." + b + "(" + c + ")");
					return d
				},
				each: function(a, b) {
					return e.each(this, a, b)
				},
				ready: function(a) {
					e.bindReady(), A.add(a);
					return this
				},
				eq: function(a) {
					a = +a;
					return a === -1 ? this.slice(a) : this.slice(a, a + 1)
				},
				first: function() {
					return this.eq(0)
				},
				last: function() {
					return this.eq(-1)
				},
				slice: function() {
					return this.pushStack(F.apply(this, arguments), "slice", F.call(arguments).join(","))
				},
				map: function(a) {
					return this.pushStack(e.map(this, function(b, c) {
						return a.call(b, c, b)
					}))
				},
				end: function() {
					return this.prevObject || this.constructor(null)
				},
				push: E,
				sort: [].sort,
				splice: [].splice
			}, e.fn.init.prototype = e.fn, e.extend = e.fn.extend = function() {
				var a, c, d, f, g, h, i = arguments[0] || {},
					j = 1,
					k = arguments.length,
					l = !1;
				typeof i == "boolean" && (l = i, i = arguments[1] || {}, j = 2), typeof i != "object" && !e.isFunction(i) && (i = {}), k === j && (i = this, --j);
				for (; j < k; j++)
					if ((a = arguments[j]) != null)
						for (c in a) {
							d = i[c], f = a[c];
							if (i === f) continue;
							l && f && (e.isPlainObject(f) || (g = e.isArray(f))) ? (g ? (g = !1, h = d && e.isArray(d) ? d : []) : h = d && e.isPlainObject(d) ? d : {}, i[c] = e.extend(l, h, f)) : f !== b && (i[c] = f)
						}
					return i
			}, e.extend({
				noConflict: function(b) {
					a.$ === e && (a.$ = g), b && a.jQuery === e && (a.jQuery = f);
					return e
				},
				isReady: !1,
				readyWait: 1,
				holdReady: function(a) {
					a ? e.readyWait++ : e.ready(!0)
				},
				ready: function(a) {
					if (a === !0 && !--e.readyWait || a !== !0 && !e.isReady) {
						if (!c.body) return setTimeout(e.ready, 1);
						e.isReady = !0;
						if (a !== !0 && --e.readyWait > 0) return;
						A.fireWith(c, [e]), e.fn.trigger && e(c).trigger("ready").off("ready")
					}
				},
				bindReady: function() {
					if (!A) {
						A = e.Callbacks("once memory");
						if (c.readyState === "complete") return setTimeout(e.ready, 1);
						if (c.addEventListener) c.addEventListener("DOMContentLoaded", B, !1), a.addEventListener("load", e.ready, !1);
						else if (c.attachEvent) {
							c.attachEvent("onreadystatechange", B), a.attachEvent("onload", e.ready);
							var b = !1;
							try {
								b = a.frameElement == null
							} catch (d) {}
							c.documentElement.doScroll && b && J()
						}
					}
				},
				isFunction: function(a) {
					return e.type(a) === "function"
				},
				isArray: Array.isArray || function(a) {
					return e.type(a) === "array"
				},
				isWindow: function(a) {
					return a != null && a == a.window
				},
				isNumeric: function(a) {
					return !isNaN(parseFloat(a)) && isFinite(a)
				},
				type: function(a) {
					return a == null ? String(a) : I[C.call(a)] || "object"
				},
				isPlainObject: function(a) {
					if (!a || e.type(a) !== "object" || a.nodeType || e.isWindow(a)) return !1;
					try {
						if (a.constructor && !D.call(a, "constructor") && !D.call(a.constructor.prototype, "isPrototypeOf")) return !1
					} catch (c) {
						return !1
					}
					var d;
					for (d in a);
					return d === b || D.call(a, d)
				},
				isEmptyObject: function(a) {
					for (var b in a) return !1;
					return !0
				},
				error: function(a) {
					throw new Error(a)
				},
				parseJSON: function(b) {
					if (typeof b != "string" || !b) return null;
					b = e.trim(b);
					if (a.JSON && a.JSON.parse) return a.JSON.parse(b);
					if (n.test(b.replace(o, "@").replace(p, "]").replace(q, ""))) return (new Function("return " + b))();
					e.error("Invalid JSON: " + b)
				},
				parseXML: function(c) {
					if (typeof c != "string" || !c) return null;
					var d, f;
					try {
						a.DOMParser ? (f = new DOMParser, d = f.parseFromString(c, "text/xml")) : (d = new ActiveXObject("Microsoft.XMLDOM"), d.async = "false", d.loadXML(c))
					} catch (g) {
						d = b
					}(!d || !d.documentElement || d.getElementsByTagName("parsererror").length) && e.error("Invalid XML: " + c);
					return d
				},
				noop: function() {},
				globalEval: function(b) {
					b && j.test(b) && (a.execScript || function(b) {
						a.eval.call(a, b)
					})(b)
				},
				camelCase: function(a) {
					return a.replace(w, "ms-").replace(v, x)
				},
				nodeName: function(a, b) {
					return a.nodeName && a.nodeName.toUpperCase() === b.toUpperCase()
				},
				each: function(a, c, d) {
					var f, g = 0,
						h = a.length,
						i = h === b || e.isFunction(a);
					if (d) {
						if (i) {
							for (f in a)
								if (c.apply(a[f], d) === !1) break
						} else
							for (; g < h;)
								if (c.apply(a[g++], d) === !1) break
					} else if (i) {
						for (f in a)
							if (c.call(a[f], f, a[f]) === !1) break
					} else
						for (; g < h;)
							if (c.call(a[g], g, a[g++]) === !1) break; return a
				},
				trim: G ? function(a) {
					return a == null ? "" : G.call(a)
				} : function(a) {
					return a == null ? "" : (a + "").replace(k, "").replace(l, "")
				},
				makeArray: function(a, b) {
					var c = b || [];
					if (a != null) {
						var d = e.type(a);
						a.length == null || d === "string" || d === "function" || d === "regexp" || e.isWindow(a) ? E.call(c, a) : e.merge(c, a)
					}
					return c
				},
				inArray: function(a, b, c) {
					var d;
					if (b) {
						if (H) return H.call(b, a, c);
						d = b.length, c = c ? c < 0 ? Math.max(0, d + c) : c : 0;
						for (; c < d; c++)
							if (c in b && b[c] === a) return c
					}
					return -1
				},
				merge: function(a, c) {
					var d = a.length,
						e = 0;
					if (typeof c.length == "number")
						for (var f = c.length; e < f; e++) a[d++] = c[e];
					else
						while (c[e] !== b) a[d++] = c[e++];
					a.length = d;
					return a
				},
				grep: function(a, b, c) {
					var d = [],
						e;
					c = !!c;
					for (var f = 0, g = a.length; f < g; f++) e = !!b(a[f], f), c !== e && d.push(a[f]);
					return d
				},
				map: function(a, c, d) {
					var f, g, h = [],
						i = 0,
						j = a.length,
						k = a instanceof e || j !== b && typeof j == "number" && (j > 0 && a[0] && a[j - 1] || j === 0 || e.isArray(a));
					if (k)
						for (; i < j; i++) f = c(a[i], i, d), f != null && (h[h.length] = f);
					else
						for (g in a) f = c(a[g], g, d), f != null && (h[h.length] = f);
					return h.concat.apply([], h)
				},
				guid: 1,
				proxy: function(a, c) {
					if (typeof c == "string") {
						var d = a[c];
						c = a, a = d
					}
					if (!e.isFunction(a)) return b;
					var f = F.call(arguments, 2),
						g = function() {
							return a.apply(c, f.concat(F.call(arguments)))
						};
					g.guid = a.guid = a.guid || g.guid || e.guid++;
					return g
				},
				access: function(a, c, d, f, g, h, i) {
					var j, k = d == null,
						l = 0,
						m = a.length;
					if (d && typeof d == "object") {
						for (l in d) e.access(a, c, l, d[l], 1, h, f);
						g = 1
					} else if (f !== b) {
						j = i === b && e.isFunction(f), k && (j ? (j = c, c = function(a, b, c) {
							return j.call(e(a), c)
						}) : (c.call(a, f), c = null));
						if (c)
							for (; l < m; l++) c(a[l], d, j ? f.call(a[l], l, c(a[l], d)) : f, i);
						g = 1
					}
					return g ? a : k ? c.call(a) : m ? c(a[0], d) : h
				},
				now: function() {
					return (new Date).getTime()
				},
				uaMatch: function(a) {
					a = a.toLowerCase();
					var b = r.exec(a) || s.exec(a) || t.exec(a) || a.indexOf("compatible") < 0 && u.exec(a) || [];
					return {
						browser: b[1] || "",
						version: b[2] || "0"
					}
				},
				sub: function() {
					function a(b, c) {
						return new a.fn.init(b, c)
					}
					e.extend(!0, a, this), a.superclass = this, a.fn = a.prototype = this(), a.fn.constructor = a, a.sub = this.sub, a.fn.init = function(d, f) {
						f && f instanceof e && !(f instanceof a) && (f = a(f));
						return e.fn.init.call(this, d, f, b)
					}, a.fn.init.prototype = a.fn;
					var b = a(c);
					return a
				},
				browser: {}
			}), e.each("Boolean Number String Function Array Date RegExp Object".split(" "), function(a, b) {
				I["[object " + b + "]"] = b.toLowerCase()
			}), z = e.uaMatch(y), z.browser && (e.browser[z.browser] = !0, e.browser.version = z.version), e.browser.webkit && (e.browser.safari = !0), j.test(" ") && (k = /^[\s\xA0]+/, l = /[\s\xA0]+$/), h = e(c), c.addEventListener ? B = function() {
				c.removeEventListener("DOMContentLoaded", B, !1), e.ready()
			} : c.attachEvent && (B = function() {
				c.readyState === "complete" && (c.detachEvent("onreadystatechange", B), e.ready())
			});
			return e
		}(),
		g = {};
	f.Callbacks = function(a) {
		a = a ? g[a] || h(a) : {};
		var c = [],
			d = [],
			e, i, j, k, l, m, n = function(b) {
				var d, e, g, h, i;
				for (d = 0, e = b.length; d < e; d++) g = b[d], h = f.type(g), h === "array" ? n(g) : h === "function" && (!a.unique || !p.has(g)) && c.push(g)
			},
			o = function(b, f) {
				f = f || [], e = !a.memory || [b, f], i = !0, j = !0, m = k || 0, k = 0, l = c.length;
				for (; c && m < l; m++)
					if (c[m].apply(b, f) === !1 && a.stopOnFalse) {
						e = !0;
						break
					}
				j = !1, c && (a.once ? e === !0 ? p.disable() : c = [] : d && d.length && (e = d.shift(), p.fireWith(e[0], e[1])))
			},
			p = {
				add: function() {
					if (c) {
						var a = c.length;
						n(arguments), j ? l = c.length : e && e !== !0 && (k = a, o(e[0], e[1]))
					}
					return this
				},
				remove: function() {
					if (c) {
						var b = arguments,
							d = 0,
							e = b.length;
						for (; d < e; d++)
							for (var f = 0; f < c.length; f++)
								if (b[d] === c[f]) {
									j && f <= l && (l--, f <= m && m--), c.splice(f--, 1);
									if (a.unique) break
								}
					}
					return this
				},
				has: function(a) {
					if (c) {
						var b = 0,
							d = c.length;
						for (; b < d; b++)
							if (a === c[b]) return !0
					}
					return !1
				},
				empty: function() {
					c = [];
					return this
				},
				disable: function() {
					c = d = e = b;
					return this
				},
				disabled: function() {
					return !c
				},
				lock: function() {
					d = b, (!e || e === !0) && p.disable();
					return this
				},
				locked: function() {
					return !d
				},
				fireWith: function(b, c) {
					d && (j ? a.once || d.push([b, c]) : (!a.once || !e) && o(b, c));
					return this
				},
				fire: function() {
					p.fireWith(this, arguments);
					return this
				},
				fired: function() {
					return !!i
				}
			};
		return p
	};
	var i = [].slice;
	f.extend({
		Deferred: function(a) {
			var b = f.Callbacks("once memory"),
				c = f.Callbacks("once memory"),
				d = f.Callbacks("memory"),
				e = "pending",
				g = {
					resolve: b,
					reject: c,
					notify: d
				},
				h = {
					done: b.add,
					fail: c.add,
					progress: d.add,
					state: function() {
						return e
					},
					isResolved: b.fired,
					isRejected: c.fired,
					then: function(a, b, c) {
						i.done(a).fail(b).progress(c);
						return this
					},
					always: function() {
						i.done.apply(i, arguments).fail.apply(i, arguments);
						return this
					},
					pipe: function(a, b, c) {
						return f.Deferred(function(d) {
							f.each({
								done: [a, "resolve"],
								fail: [b, "reject"],
								progress: [c, "notify"]
							}, function(a, b) {
								var c = b[0],
									e = b[1],
									g;
								f.isFunction(c) ? i[a](function() {
									g = c.apply(this, arguments), g && f.isFunction(g.promise) ? g.promise().then(d.resolve, d.reject, d.notify) : d[e + "With"](this === i ? d : this, [g])
								}) : i[a](d[e])
							})
						}).promise()
					},
					promise: function(a) {
						if (a == null) a = h;
						else
							for (var b in h) a[b] = h[b];
						return a
					}
				},
				i = h.promise({}),
				j;
			for (j in g) i[j] = g[j].fire, i[j + "With"] = g[j].fireWith;
			i.done(function() {
				e = "resolved"
			}, c.disable, d.lock).fail(function() {
				e = "rejected"
			}, b.disable, d.lock), a && a.call(i, i);
			return i
		},
		when: function(a) {
			function m(a) {
				return function(b) {
					e[a] = arguments.length > 1 ? i.call(arguments, 0) : b, j.notifyWith(k, e)
				}
			}

			function l(a) {
				return function(c) {
					b[a] = arguments.length > 1 ? i.call(arguments, 0) : c, --g || j.resolveWith(j, b)
				}
			}
			var b = i.call(arguments, 0),
				c = 0,
				d = b.length,
				e = Array(d),
				g = d,
				h = d,
				j = d <= 1 && a && f.isFunction(a.promise) ? a : f.Deferred(),
				k = j.promise();
			if (d > 1) {
				for (; c < d; c++) b[c] && b[c].promise && f.isFunction(b[c].promise) ? b[c].promise().then(l(c), j.reject, m(c)) : --g;
				g || j.resolveWith(j, b)
			} else j !== a && j.resolveWith(j, d ? [a] : []);
			return k
		}
	}), f.support = function() {
		var b, d, e, g, h, i, j, k, l, m, n, o, p = c.createElement("div"),
			q = c.documentElement;
		p.setAttribute("className", "t"), p.innerHTML = "   <link/><table></table><a href='/a' style='top:1px;float:left;opacity:.55;'>a</a><input type='checkbox'/>", d = p.getElementsByTagName("*"), e = p.getElementsByTagName("a")[0];
		if (!d || !d.length || !e) return {};
		g = c.createElement("select"), h = g.appendChild(c.createElement("option")), i = p.getElementsByTagName("input")[0], b = {
			leadingWhitespace: p.firstChild.nodeType === 3,
			tbody: !p.getElementsByTagName("tbody").length,
			htmlSerialize: !!p.getElementsByTagName("link").length,
			style: /top/.test(e.getAttribute("style")),
			hrefNormalized: e.getAttribute("href") === "/a",
			opacity: /^0.55/.test(e.style.opacity),
			cssFloat: !!e.style.cssFloat,
			checkOn: i.value === "on",
			optSelected: h.selected,
			getSetAttribute: p.className !== "t",
			enctype: !!c.createElement("form").enctype,
			html5Clone: c.createElement("nav").cloneNode(!0).outerHTML !== "<:nav></:nav>",
			submitBubbles: !0,
			changeBubbles: !0,
			focusinBubbles: !1,
			deleteExpando: !0,
			noCloneEvent: !0,
			inlineBlockNeedsLayout: !1,
			shrinkWrapBlocks: !1,
			reliableMarginRight: !0,
			pixelMargin: !0
		}, f.boxModel = b.boxModel = c.compatMode === "CSS1Compat", i.checked = !0, b.noCloneChecked = i.cloneNode(!0).checked, g.disabled = !0, b.optDisabled = !h.disabled;
		try {
			delete p.test
		} catch (r) {
			b.deleteExpando = !1
		}!p.addEventListener && p.attachEvent && p.fireEvent && (p.attachEvent("onclick", function() {
			b.noCloneEvent = !1
		}), p.cloneNode(!0).fireEvent("onclick")), i = c.createElement("input"), i.value = "t", i.setAttribute("type", "radio"), b.radioValue = i.value === "t", i.setAttribute("checked", "checked"), i.setAttribute("name", "t"), p.appendChild(i), j = c.createDocumentFragment(), j.appendChild(p.lastChild), b.checkClone = j.cloneNode(!0).cloneNode(!0).lastChild.checked, b.appendChecked = i.checked, j.removeChild(i), j.appendChild(p);
		if (p.attachEvent)
			for (n in {
					submit: 1,
					change: 1,
					focusin: 1
				}) m = "on" + n, o = m in p, o || (p.setAttribute(m, "return;"), o = typeof p[m] == "function"), b[n + "Bubbles"] = o;
		j.removeChild(p), j = g = h = p = i = null, f(function() {
			var d, e, g, h, i, j, l, m, n, q, r, s, t, u = c.getElementsByTagName("body")[0];
			!u || (m = 1, t = "padding:0;margin:0;border:", r = "position:absolute;top:0;left:0;width:1px;height:1px;", s = t + "0;visibility:hidden;", n = "style='" + r + t + "5px solid #000;", q = "<div " + n + "display:block;'><div style='" + t + "0;display:block;overflow:hidden;'></div></div>" + "<table " + n + "' cellpadding='0' cellspacing='0'>" + "<tr><td></td></tr></table>", d = c.createElement("div"), d.style.cssText = s + "width:0;height:0;position:static;top:0;margin-top:" + m + "px", u.insertBefore(d, u.firstChild), p = c.createElement("div"), d.appendChild(p), p.innerHTML = "<table><tr><td style='" + t + "0;display:none'></td><td>t</td></tr></table>", k = p.getElementsByTagName("td"), o = k[0].offsetHeight === 0, k[0].style.display = "", k[1].style.display = "none", b.reliableHiddenOffsets = o && k[0].offsetHeight === 0, a.getComputedStyle && (p.innerHTML = "", l = c.createElement("div"), l.style.width = "0", l.style.marginRight = "0", p.style.width = "2px", p.appendChild(l), b.reliableMarginRight = (parseInt((a.getComputedStyle(l, null) || {
				marginRight: 0
			}).marginRight, 10) || 0) === 0), typeof p.style.zoom != "undefined" && (p.innerHTML = "", p.style.width = p.style.padding = "1px", p.style.border = 0, p.style.overflow = "hidden", p.style.display = "inline", p.style.zoom = 1, b.inlineBlockNeedsLayout = p.offsetWidth === 3, p.style.display = "block", p.style.overflow = "visible", p.innerHTML = "<div style='width:5px;'></div>", b.shrinkWrapBlocks = p.offsetWidth !== 3), p.style.cssText = r + s, p.innerHTML = q, e = p.firstChild, g = e.firstChild, i = e.nextSibling.firstChild.firstChild, j = {
				doesNotAddBorder: g.offsetTop !== 5,
				doesAddBorderForTableAndCells: i.offsetTop === 5
			}, g.style.position = "fixed", g.style.top = "20px", j.fixedPosition = g.offsetTop === 20 || g.offsetTop === 15, g.style.position = g.style.top = "", e.style.overflow = "hidden", e.style.position = "relative", j.subtractsBorderForOverflowNotVisible = g.offsetTop === -5, j.doesNotIncludeMarginInBodyOffset = u.offsetTop !== m, a.getComputedStyle && (p.style.marginTop = "1%", b.pixelMargin = (a.getComputedStyle(p, null) || {
				marginTop: 0
			}).marginTop !== "1%"), typeof d.style.zoom != "undefined" && (d.style.zoom = 1), u.removeChild(d), l = p = d = null, f.extend(b, j))
		});
		return b
	}();
	var j = /^(?:\{.*\}|\[.*\])$/,
		k = /([A-Z])/g;
	f.extend({
		cache: {},
		uuid: 0,
		expando: "jQuery" + (f.fn.jquery + Math.random()).replace(/\D/g, ""),
		noData: {
			embed: !0,
			object: "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000",
			applet: !0
		},
		hasData: function(a) {
			a = a.nodeType ? f.cache[a[f.expando]] : a[f.expando];
			return !!a && !m(a)
		},
		data: function(a, c, d, e) {
			if (!!f.acceptData(a)) {
				var g, h, i, j = f.expando,
					k = typeof c == "string",
					l = a.nodeType,
					m = l ? f.cache : a,
					n = l ? a[j] : a[j] && j,
					o = c === "events";
				if ((!n || !m[n] || !o && !e && !m[n].data) && k && d === b) return;
				n || (l ? a[j] = n = ++f.uuid : n = j), m[n] || (m[n] = {}, l || (m[n].toJSON = f.noop));
				if (typeof c == "object" || typeof c == "function") e ? m[n] = f.extend(m[n], c) : m[n].data = f.extend(m[n].data, c);
				g = h = m[n], e || (h.data || (h.data = {}), h = h.data), d !== b && (h[f.camelCase(c)] = d);
				if (o && !h[c]) return g.events;
				k ? (i = h[c], i == null && (i = h[f.camelCase(c)])) : i = h;
				return i
			}
		},
		removeData: function(a, b, c) {
			if (!!f.acceptData(a)) {
				var d, e, g, h = f.expando,
					i = a.nodeType,
					j = i ? f.cache : a,
					k = i ? a[h] : h;
				if (!j[k]) return;
				if (b) {
					d = c ? j[k] : j[k].data;
					if (d) {
						f.isArray(b) || (b in d ? b = [b] : (b = f.camelCase(b), b in d ? b = [b] : b = b.split(" ")));
						for (e = 0, g = b.length; e < g; e++) delete d[b[e]];
						if (!(c ? m : f.isEmptyObject)(d)) return
					}
				}
				if (!c) {
					delete j[k].data;
					if (!m(j[k])) return
				}
				f.support.deleteExpando || !j.setInterval ? delete j[k] : j[k] = null, i && (f.support.deleteExpando ? delete a[h] : a.removeAttribute ? a.removeAttribute(h) : a[h] = null)
			}
		},
		_data: function(a, b, c) {
			return f.data(a, b, c, !0)
		},
		acceptData: function(a) {
			if (a.nodeName) {
				var b = f.noData[a.nodeName.toLowerCase()];
				if (b) return b !== !0 && a.getAttribute("classid") === b
			}
			return !0
		}
	}), f.fn.extend({
		data: function(a, c) {
			var d, e, g, h, i, j = this[0],
				k = 0,
				m = null;
			if (a === b) {
				if (this.length) {
					m = f.data(j);
					if (j.nodeType === 1 && !f._data(j, "parsedAttrs")) {
						g = j.attributes;
						for (i = g.length; k < i; k++) h = g[k].name, h.indexOf("data-") === 0 && (h = f.camelCase(h.substring(5)), l(j, h, m[h]));
						f._data(j, "parsedAttrs", !0)
					}
				}
				return m
			}
			if (typeof a == "object") return this.each(function() {
				f.data(this, a)
			});
			d = a.split(".", 2), d[1] = d[1] ? "." + d[1] : "", e = d[1] + "!";
			return f.access(this, function(c) {
				if (c === b) {
					m = this.triggerHandler("getData" + e, [d[0]]), m === b && j && (m = f.data(j, a), m = l(j, a, m));
					return m === b && d[1] ? this.data(d[0]) : m
				}
				d[1] = c, this.each(function() {
					var b = f(this);
					b.triggerHandler("setData" + e, d), f.data(this, a, c), b.triggerHandler("changeData" + e, d)
				})
			}, null, c, arguments.length > 1, null, !1)
		},
		removeData: function(a) {
			return this.each(function() {
				f.removeData(this, a)
			})
		}
	}), f.extend({
		_mark: function(a, b) {
			a && (b = (b || "fx") + "mark", f._data(a, b, (f._data(a, b) || 0) + 1))
		},
		_unmark: function(a, b, c) {
			a !== !0 && (c = b, b = a, a = !1);
			if (b) {
				c = c || "fx";
				var d = c + "mark",
					e = a ? 0 : (f._data(b, d) || 1) - 1;
				e ? f._data(b, d, e) : (f.removeData(b, d, !0), n(b, c, "mark"))
			}
		},
		queue: function(a, b, c) {
			var d;
			if (a) {
				b = (b || "fx") + "queue", d = f._data(a, b), c && (!d || f.isArray(c) ? d = f._data(a, b, f.makeArray(c)) : d.push(c));
				return d || []
			}
		},
		dequeue: function(a, b) {
			b = b || "fx";
			var c = f.queue(a, b),
				d = c.shift(),
				e = {};
			d === "inprogress" && (d = c.shift()), d && (b === "fx" && c.unshift("inprogress"), f._data(a, b + ".run", e), d.call(a, function() {
				f.dequeue(a, b)
			}, e)), c.length || (f.removeData(a, b + "queue " + b + ".run", !0), n(a, b, "queue"))
		}
	}), f.fn.extend({
		queue: function(a, c) {
			var d = 2;
			typeof a != "string" && (c = a, a = "fx", d--);
			if (arguments.length < d) return f.queue(this[0], a);
			return c === b ? this : this.each(function() {
				var b = f.queue(this, a, c);
				a === "fx" && b[0] !== "inprogress" && f.dequeue(this, a)
			})
		},
		dequeue: function(a) {
			return this.each(function() {
				f.dequeue(this, a)
			})
		},
		delay: function(a, b) {
			a = f.fx ? f.fx.speeds[a] || a : a, b = b || "fx";
			return this.queue(b, function(b, c) {
				var d = setTimeout(b, a);
				c.stop = function() {
					clearTimeout(d)
				}
			})
		},
		clearQueue: function(a) {
			return this.queue(a || "fx", [])
		},
		promise: function(a, c) {
			function m() {
				--h || d.resolveWith(e, [e])
			}
			typeof a != "string" && (c = a, a = b), a = a || "fx";
			var d = f.Deferred(),
				e = this,
				g = e.length,
				h = 1,
				i = a + "defer",
				j = a + "queue",
				k = a + "mark",
				l;
			while (g--)
				if (l = f.data(e[g], i, b, !0) || (f.data(e[g], j, b, !0) || f.data(e[g], k, b, !0)) && f.data(e[g], i, f.Callbacks("once memory"), !0)) h++, l.add(m);
			m();
			return d.promise(c)
		}
	});
	var o = /[\n\t\r]/g,
		p = /\s+/,
		q = /\r/g,
		r = /^(?:button|input)$/i,
		s = /^(?:button|input|object|select|textarea)$/i,
		t = /^a(?:rea)?$/i,
		u = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i,
		v = f.support.getSetAttribute,
		w, x, y;
	f.fn.extend({
		attr: function(a, b) {
			return f.access(this, f.attr, a, b, arguments.length > 1)
		},
		removeAttr: function(a) {
			return this.each(function() {
				f.removeAttr(this, a)
			})
		},
		prop: function(a, b) {
			return f.access(this, f.prop, a, b, arguments.length > 1)
		},
		removeProp: function(a) {
			a = f.propFix[a] || a;
			return this.each(function() {
				try {
					this[a] = b, delete this[a]
				} catch (c) {}
			})
		},
		addClass: function(a) {
			var b, c, d, e, g, h, i;
			if (f.isFunction(a)) return this.each(function(b) {
				f(this).addClass(a.call(this, b, this.className))
			});
			if (a && typeof a == "string") {
				b = a.split(p);
				for (c = 0, d = this.length; c < d; c++) {
					e = this[c];
					if (e.nodeType === 1)
						if (!e.className && b.length === 1) e.className = a;
						else {
							g = " " + e.className + " ";
							for (h = 0, i = b.length; h < i; h++) ~g.indexOf(" " + b[h] + " ") || (g += b[h] + " ");
							e.className = f.trim(g)
						}
				}
			}
			return this
		},
		removeClass: function(a) {
			var c, d, e, g, h, i, j;
			if (f.isFunction(a)) return this.each(function(b) {
				f(this).removeClass(a.call(this, b, this.className))
			});
			if (a && typeof a == "string" || a === b) {
				c = (a || "").split(p);
				for (d = 0, e = this.length; d < e; d++) {
					g = this[d];
					if (g.nodeType === 1 && g.className)
						if (a) {
							h = (" " + g.className + " ").replace(o, " ");
							for (i = 0, j = c.length; i < j; i++) h = h.replace(" " + c[i] + " ", " ");
							g.className = f.trim(h)
						} else g.className = ""
				}
			}
			return this
		},
		toggleClass: function(a, b) {
			var c = typeof a,
				d = typeof b == "boolean";
			if (f.isFunction(a)) return this.each(function(c) {
				f(this).toggleClass(a.call(this, c, this.className, b), b)
			});
			return this.each(function() {
				if (c === "string") {
					var e, g = 0,
						h = f(this),
						i = b,
						j = a.split(p);
					while (e = j[g++]) i = d ? i : !h.hasClass(e), h[i ? "addClass" : "removeClass"](e)
				} else if (c === "undefined" || c === "boolean") this.className && f._data(this, "__className__", this.className), this.className = this.className || a === !1 ? "" : f._data(this, "__className__") || ""
			})
		},
		hasClass: function(a) {
			var b = " " + a + " ",
				c = 0,
				d = this.length;
			for (; c < d; c++)
				if (this[c].nodeType === 1 && (" " + this[c].className + " ").replace(o, " ").indexOf(b) > -1) return !0;
			return !1
		},
		val: function(a) {
			var c, d, e, g = this[0]; {
				if (!!arguments.length) {
					e = f.isFunction(a);
					return this.each(function(d) {
						var g = f(this),
							h;
						if (this.nodeType === 1) {
							e ? h = a.call(this, d, g.val()) : h = a, h == null ? h = "" : typeof h == "number" ? h += "" : f.isArray(h) && (h = f.map(h, function(a) {
								return a == null ? "" : a + ""
							})), c = f.valHooks[this.type] || f.valHooks[this.nodeName.toLowerCase()];
							if (!c || !("set" in c) || c.set(this, h, "value") === b) this.value = h
						}
					})
				}
				if (g) {
					c = f.valHooks[g.type] || f.valHooks[g.nodeName.toLowerCase()];
					if (c && "get" in c && (d = c.get(g, "value")) !== b) return d;
					d = g.value;
					return typeof d == "string" ? d.replace(q, "") : d == null ? "" : d
				}
			}
		}
	}), f.extend({
		valHooks: {
			option: {
				get: function(a) {
					var b = a.attributes.value;
					return !b || b.specified ? a.value : a.text
				}
			},
			select: {
				get: function(a) {
					var b, c, d, e, g = a.selectedIndex,
						h = [],
						i = a.options,
						j = a.type === "select-one";
					if (g < 0) return null;
					c = j ? g : 0, d = j ? g + 1 : i.length;
					for (; c < d; c++) {
						e = i[c];
						if (e.selected && (f.support.optDisabled ? !e.disabled : e.getAttribute("disabled") === null) && (!e.parentNode.disabled || !f.nodeName(e.parentNode, "optgroup"))) {
							b = f(e).val();
							if (j) return b;
							h.push(b)
						}
					}
					if (j && !h.length && i.length) return f(i[g]).val();
					return h
				},
				set: function(a, b) {
					var c = f.makeArray(b);
					f(a).find("option").each(function() {
						this.selected = f.inArray(f(this).val(), c) >= 0
					}), c.length || (a.selectedIndex = -1);
					return c
				}
			}
		},
		attrFn: {
			val: !0,
			css: !0,
			html: !0,
			text: !0,
			data: !0,
			width: !0,
			height: !0,
			offset: !0
		},
		attr: function(a, c, d, e) {
			var g, h, i, j = a.nodeType;
			if (!!a && j !== 3 && j !== 8 && j !== 2) {
				if (e && c in f.attrFn) return f(a)[c](d);
				if (typeof a.getAttribute == "undefined") return f.prop(a, c, d);
				i = j !== 1 || !f.isXMLDoc(a), i && (c = c.toLowerCase(), h = f.attrHooks[c] || (u.test(c) ? x : w));
				if (d !== b) {
					if (d === null) {
						f.removeAttr(a, c);
						return
					}
					if (h && "set" in h && i && (g = h.set(a, d, c)) !== b) return g;
					a.setAttribute(c, "" + d);
					return d
				}
				if (h && "get" in h && i && (g = h.get(a, c)) !== null) return g;
				g = a.getAttribute(c);
				return g === null ? b : g
			}
		},
		removeAttr: function(a, b) {
			var c, d, e, g, h, i = 0;
			if (b && a.nodeType === 1) {
				d = b.toLowerCase().split(p), g = d.length;
				for (; i < g; i++) e = d[i], e && (c = f.propFix[e] || e, h = u.test(e), h || f.attr(a, e, ""), a.removeAttribute(v ? e : c), h && c in a && (a[c] = !1))
			}
		},
		attrHooks: {
			type: {
				set: function(a, b) {
					if (r.test(a.nodeName) && a.parentNode) f.error("type property can't be changed");
					else if (!f.support.radioValue && b === "radio" && f.nodeName(a, "input")) {
						var c = a.value;
						a.setAttribute("type", b), c && (a.value = c);
						return b
					}
				}
			},
			value: {
				get: function(a, b) {
					if (w && f.nodeName(a, "button")) return w.get(a, b);
					return b in a ? a.value : null
				},
				set: function(a, b, c) {
					if (w && f.nodeName(a, "button")) return w.set(a, b, c);
					a.value = b
				}
			}
		},
		propFix: {
			tabindex: "tabIndex",
			readonly: "readOnly",
			"for": "htmlFor",
			"class": "className",
			maxlength: "maxLength",
			cellspacing: "cellSpacing",
			cellpadding: "cellPadding",
			rowspan: "rowSpan",
			colspan: "colSpan",
			usemap: "useMap",
			frameborder: "frameBorder",
			contenteditable: "contentEditable"
		},
		prop: function(a, c, d) {
			var e, g, h, i = a.nodeType;
			if (!!a && i !== 3 && i !== 8 && i !== 2) {
				h = i !== 1 || !f.isXMLDoc(a), h && (c = f.propFix[c] || c, g = f.propHooks[c]);
				return d !== b ? g && "set" in g && (e = g.set(a, d, c)) !== b ? e : a[c] = d : g && "get" in g && (e = g.get(a, c)) !== null ? e : a[c]
			}
		},
		propHooks: {
			tabIndex: {
				get: function(a) {
					var c = a.getAttributeNode("tabindex");
					return c && c.specified ? parseInt(c.value, 10) : s.test(a.nodeName) || t.test(a.nodeName) && a.href ? 0 : b
				}
			}
		}
	}), f.attrHooks.tabindex = f.propHooks.tabIndex, x = {
		get: function(a, c) {
			var d, e = f.prop(a, c);
			return e === !0 || typeof e != "boolean" && (d = a.getAttributeNode(c)) && d.nodeValue !== !1 ? c.toLowerCase() : b
		},
		set: function(a, b, c) {
			var d;
			b === !1 ? f.removeAttr(a, c) : (d = f.propFix[c] || c, d in a && (a[d] = !0), a.setAttribute(c, c.toLowerCase()));
			return c
		}
	}, v || (y = {
		name: !0,
		id: !0,
		coords: !0
	}, w = f.valHooks.button = {
		get: function(a, c) {
			var d;
			d = a.getAttributeNode(c);
			return d && (y[c] ? d.nodeValue !== "" : d.specified) ? d.nodeValue : b
		},
		set: function(a, b, d) {
			var e = a.getAttributeNode(d);
			e || (e = c.createAttribute(d), a.setAttributeNode(e));
			return e.nodeValue = b + ""
		}
	}, f.attrHooks.tabindex.set = w.set, f.each(["width", "height"], function(a, b) {
		f.attrHooks[b] = f.extend(f.attrHooks[b], {
			set: function(a, c) {
				if (c === "") {
					a.setAttribute(b, "auto");
					return c
				}
			}
		})
	}), f.attrHooks.contenteditable = {
		get: w.get,
		set: function(a, b, c) {
			b === "" && (b = "false"), w.set(a, b, c)
		}
	}), f.support.hrefNormalized || f.each(["href", "src", "width", "height"], function(a, c) {
		f.attrHooks[c] = f.extend(f.attrHooks[c], {
			get: function(a) {
				var d = a.getAttribute(c, 2);
				return d === null ? b : d
			}
		})
	}), f.support.style || (f.attrHooks.style = {
		get: function(a) {
			return a.style.cssText.toLowerCase() || b
		},
		set: function(a, b) {
			return a.style.cssText = "" + b
		}
	}), f.support.optSelected || (f.propHooks.selected = f.extend(f.propHooks.selected, {
		get: function(a) {
			var b = a.parentNode;
			b && (b.selectedIndex, b.parentNode && b.parentNode.selectedIndex);
			return null
		}
	})), f.support.enctype || (f.propFix.enctype = "encoding"), f.support.checkOn || f.each(["radio", "checkbox"], function() {
		f.valHooks[this] = {
			get: function(a) {
				return a.getAttribute("value") === null ? "on" : a.value
			}
		}
	}), f.each(["radio", "checkbox"], function() {
		f.valHooks[this] = f.extend(f.valHooks[this], {
			set: function(a, b) {
				if (f.isArray(b)) return a.checked = f.inArray(f(a).val(), b) >= 0
			}
		})
	});
	var z = /^(?:textarea|input|select)$/i,
		A = /^([^\.]*)?(?:\.(.+))?$/,
		B = /(?:^|\s)hover(\.\S+)?\b/,
		C = /^key/,
		D = /^(?:mouse|contextmenu)|click/,
		E = /^(?:focusinfocus|focusoutblur)$/,
		F = /^(\w*)(?:#([\w\-]+))?(?:\.([\w\-]+))?$/,
		G = function(
			a) {
			var b = F.exec(a);
			b && (b[1] = (b[1] || "").toLowerCase(), b[3] = b[3] && new RegExp("(?:^|\\s)" + b[3] + "(?:\\s|$)"));
			return b
		},
		H = function(a, b) {
			var c = a.attributes || {};
			return (!b[1] || a.nodeName.toLowerCase() === b[1]) && (!b[2] || (c.id || {}).value === b[2]) && (!b[3] || b[3].test((c["class"] || {}).value))
		},
		I = function(a) {
			return f.event.special.hover ? a : a.replace(B, "mouseenter$1 mouseleave$1")
		};
	f.event = {
			add: function(a, c, d, e, g) {
				var h, i, j, k, l, m, n, o, p, q, r, s;
				if (!(a.nodeType === 3 || a.nodeType === 8 || !c || !d || !(h = f._data(a)))) {
					d.handler && (p = d, d = p.handler, g = p.selector), d.guid || (d.guid = f.guid++), j = h.events, j || (h.events = j = {}), i = h.handle, i || (h.handle = i = function(a) {
						return typeof f != "undefined" && (!a || f.event.triggered !== a.type) ? f.event.dispatch.apply(i.elem, arguments) : b
					}, i.elem = a), c = f.trim(I(c)).split(" ");
					for (k = 0; k < c.length; k++) {
						l = A.exec(c[k]) || [], m = l[1], n = (l[2] || "").split(".").sort(), s = f.event.special[m] || {}, m = (g ? s.delegateType : s.bindType) || m, s = f.event.special[m] || {}, o = f.extend({
							type: m,
							origType: l[1],
							data: e,
							handler: d,
							guid: d.guid,
							selector: g,
							quick: g && G(g),
							namespace: n.join(".")
						}, p), r = j[m];
						if (!r) {
							r = j[m] = [], r.delegateCount = 0;
							if (!s.setup || s.setup.call(a, e, n, i) === !1) a.addEventListener ? a.addEventListener(m, i, !1) : a.attachEvent && a.attachEvent("on" + m, i)
						}
						s.add && (s.add.call(a, o), o.handler.guid || (o.handler.guid = d.guid)), g ? r.splice(r.delegateCount++, 0, o) : r.push(o), f.event.global[m] = !0
					}
					a = null
				}
			},
			global: {},
			remove: function(a, b, c, d, e) {
				var g = f.hasData(a) && f._data(a),
					h, i, j, k, l, m, n, o, p, q, r, s;
				if (!!g && !!(o = g.events)) {
					b = f.trim(I(b || "")).split(" ");
					for (h = 0; h < b.length; h++) {
						i = A.exec(b[h]) || [], j = k = i[1], l = i[2];
						if (!j) {
							for (j in o) f.event.remove(a, j + b[h], c, d, !0);
							continue
						}
						p = f.event.special[j] || {}, j = (d ? p.delegateType : p.bindType) || j, r = o[j] || [], m = r.length, l = l ? new RegExp("(^|\\.)" + l.split(".").sort().join("\\.(?:.*\\.)?") + "(\\.|$)") : null;
						for (n = 0; n < r.length; n++) s = r[n], (e || k === s.origType) && (!c || c.guid === s.guid) && (!l || l.test(s.namespace)) && (!d || d === s.selector || d === "**" && s.selector) && (r.splice(n--, 1), s.selector && r.delegateCount--, p.remove && p.remove.call(a, s));
						r.length === 0 && m !== r.length && ((!p.teardown || p.teardown.call(a, l) === !1) && f.removeEvent(a, j, g.handle), delete o[j])
					}
					f.isEmptyObject(o) && (q = g.handle, q && (q.elem = null), f.removeData(a, ["events", "handle"], !0))
				}
			},
			customEvent: {
				getData: !0,
				setData: !0,
				changeData: !0
			},
			trigger: function(c, d, e, g) {
				if (!e || e.nodeType !== 3 && e.nodeType !== 8) {
					var h = c.type || c,
						i = [],
						j, k, l, m, n, o, p, q, r, s;
					if (E.test(h + f.event.triggered)) return;
					h.indexOf("!") >= 0 && (h = h.slice(0, -1), k = !0), h.indexOf(".") >= 0 && (i = h.split("."), h = i.shift(), i.sort());
					if ((!e || f.event.customEvent[h]) && !f.event.global[h]) return;
					c = typeof c == "object" ? c[f.expando] ? c : new f.Event(h, c) : new f.Event(h), c.type = h, c.isTrigger = !0, c.exclusive = k, c.namespace = i.join("."), c.namespace_re = c.namespace ? new RegExp("(^|\\.)" + i.join("\\.(?:.*\\.)?") + "(\\.|$)") : null, o = h.indexOf(":") < 0 ? "on" + h : "";
					if (!e) {
						j = f.cache;
						for (l in j) j[l].events && j[l].events[h] && f.event.trigger(c, d, j[l].handle.elem, !0);
						return
					}
					c.result = b, c.target || (c.target = e), d = d != null ? f.makeArray(d) : [], d.unshift(c), p = f.event.special[h] || {};
					if (p.trigger && p.trigger.apply(e, d) === !1) return;
					r = [
						[e, p.bindType || h]
					];
					if (!g && !p.noBubble && !f.isWindow(e)) {
						s = p.delegateType || h, m = E.test(s + h) ? e : e.parentNode, n = null;
						for (; m; m = m.parentNode) r.push([m, s]), n = m;
						n && n === e.ownerDocument && r.push([n.defaultView || n.parentWindow || a, s])
					}
					for (l = 0; l < r.length && !c.isPropagationStopped(); l++) m = r[l][0], c.type = r[l][1], q = (f._data(m, "events") || {})[c.type] && f._data(m, "handle"), q && q.apply(m, d), q = o && m[o], q && f.acceptData(m) && q.apply(m, d) === !1 && c.preventDefault();
					c.type = h, !g && !c.isDefaultPrevented() && (!p._default || p._default.apply(e.ownerDocument, d) === !1) && (h !== "click" || !f.nodeName(e, "a")) && f.acceptData(e) && o && e[h] && (h !== "focus" && h !== "blur" || c.target.offsetWidth !== 0) && !f.isWindow(e) && (n = e[o], n && (e[o] = null), f.event.triggered = h, e[h](), f.event.triggered = b, n && (e[o] = n));
					return c.result
				}
			},
			dispatch: function(c) {
				c = f.event.fix(c || a.event);
				var d = (f._data(this, "events") || {})[c.type] || [],
					e = d.delegateCount,
					g = [].slice.call(arguments, 0),
					h = !c.exclusive && !c.namespace,
					i = f.event.special[c.type] || {},
					j = [],
					k, l, m, n, o, p, q, r, s, t, u;
				g[0] = c, c.delegateTarget = this;
				if (!i.preDispatch || i.preDispatch.call(this, c) !== !1) {
					if (e && (!c.button || c.type !== "click")) {
						n = f(this), n.context = this.ownerDocument || this;
						for (m = c.target; m != this; m = m.parentNode || this)
							if (m.disabled !== !0) {
								p = {}, r = [], n[0] = m;
								for (k = 0; k < e; k++) s = d[k], t = s.selector, p[t] === b && (p[t] = s.quick ? H(m, s.quick) : n.is(t)), p[t] && r.push(s);
								r.length && j.push({
									elem: m,
									matches: r
								})
							}
					}
					d.length > e && j.push({
						elem: this,
						matches: d.slice(e)
					});
					for (k = 0; k < j.length && !c.isPropagationStopped(); k++) {
						q = j[k], c.currentTarget = q.elem;
						for (l = 0; l < q.matches.length && !c.isImmediatePropagationStopped(); l++) {
							s = q.matches[l];
							if (h || !c.namespace && !s.namespace || c.namespace_re && c.namespace_re.test(s.namespace)) c.data = s.data, c.handleObj = s, o = ((f.event.special[s.origType] || {}).handle || s.handler).apply(q.elem, g), o !== b && (c.result = o, o === !1 && (c.preventDefault(), c.stopPropagation()))
						}
					}
					i.postDispatch && i.postDispatch.call(this, c);
					return c.result
				}
			},
			props: "attrChange attrName relatedNode srcElement altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
			fixHooks: {},
			keyHooks: {
				props: "char charCode key keyCode".split(" "),
				filter: function(a, b) {
					a.which == null && (a.which = b.charCode != null ? b.charCode : b.keyCode);
					return a
				}
			},
			mouseHooks: {
				props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
				filter: function(a, d) {
					var e, f, g, h = d.button,
						i = d.fromElement;
					a.pageX == null && d.clientX != null && (e = a.target.ownerDocument || c, f = e.documentElement, g = e.body, a.pageX = d.clientX + (f && f.scrollLeft || g && g.scrollLeft || 0) - (f && f.clientLeft || g && g.clientLeft || 0), a.pageY = d.clientY + (f && f.scrollTop || g && g.scrollTop || 0) - (f && f.clientTop || g && g.clientTop || 0)), !a.relatedTarget && i && (a.relatedTarget = i === a.target ? d.toElement : i), !a.which && h !== b && (a.which = h & 1 ? 1 : h & 2 ? 3 : h & 4 ? 2 : 0);
					return a
				}
			},
			fix: function(a) {
				if (a[f.expando]) return a;
				var d, e, g = a,
					h = f.event.fixHooks[a.type] || {},
					i = h.props ? this.props.concat(h.props) : this.props;
				a = f.Event(g);
				for (d = i.length; d;) e = i[--d], a[e] = g[e];
				a.target || (a.target = g.srcElement || c), a.target.nodeType === 3 && (a.target = a.target.parentNode), a.metaKey === b && (a.metaKey = a.ctrlKey);
				return h.filter ? h.filter(a, g) : a
			},
			special: {
				ready: {
					setup: f.bindReady
				},
				load: {
					noBubble: !0
				},
				focus: {
					delegateType: "focusin"
				},
				blur: {
					delegateType: "focusout"
				},
				beforeunload: {
					setup: function(a, b, c) {
						f.isWindow(this) && (this.onbeforeunload = c)
					},
					teardown: function(a, b) {
						this.onbeforeunload === b && (this.onbeforeunload = null)
					}
				}
			},
			simulate: function(a, b, c, d) {
				var e = f.extend(new f.Event, c, {
					type: a,
					isSimulated: !0,
					originalEvent: {}
				});
				d ? f.event.trigger(e, null, b) : f.event.dispatch.call(b, e), e.isDefaultPrevented() && c.preventDefault()
			}
		}, f.event.handle = f.event.dispatch, f.removeEvent = c.removeEventListener ? function(a, b, c) {
			a.removeEventListener && a.removeEventListener(b, c, !1)
		} : function(a, b, c) {
			a.detachEvent && a.detachEvent("on" + b, c)
		}, f.Event = function(a, b) {
			if (!(this instanceof f.Event)) return new f.Event(a, b);
			a && a.type ? (this.originalEvent = a, this.type = a.type, this.isDefaultPrevented = a.defaultPrevented || a.returnValue === !1 || a.getPreventDefault && a.getPreventDefault() ? K : J) : this.type = a, b && f.extend(this, b), this.timeStamp = a && a.timeStamp || f.now(), this[f.expando] = !0
		}, f.Event.prototype = {
			preventDefault: function() {
				this.isDefaultPrevented = K;
				var a = this.originalEvent;
				!a || (a.preventDefault ? a.preventDefault() : a.returnValue = !1)
			},
			stopPropagation: function() {
				this.isPropagationStopped = K;
				var a = this.originalEvent;
				!a || (a.stopPropagation && a.stopPropagation(), a.cancelBubble = !0)
			},
			stopImmediatePropagation: function() {
				this.isImmediatePropagationStopped = K, this.stopPropagation()
			},
			isDefaultPrevented: J,
			isPropagationStopped: J,
			isImmediatePropagationStopped: J
		}, f.each({
			mouseenter: "mouseover",
			mouseleave: "mouseout"
		}, function(a, b) {
			f.event.special[a] = {
				delegateType: b,
				bindType: b,
				handle: function(a) {
					var c = this,
						d = a.relatedTarget,
						e = a.handleObj,
						g = e.selector,
						h;
					if (!d || d !== c && !f.contains(c, d)) a.type = e.origType, h = e.handler.apply(this, arguments), a.type = b;
					return h
				}
			}
		}), f.support.submitBubbles || (f.event.special.submit = {
			setup: function() {
				if (f.nodeName(this, "form")) return !1;
				f.event.add(this, "click._submit keypress._submit", function(a) {
					var c = a.target,
						d = f.nodeName(c, "input") || f.nodeName(c, "button") ? c.form : b;
					d && !d._submit_attached && (f.event.add(d, "submit._submit", function(a) {
						a._submit_bubble = !0
					}), d._submit_attached = !0)
				})
			},
			postDispatch: function(a) {
				a._submit_bubble && (delete a._submit_bubble, this.parentNode && !a.isTrigger && f.event.simulate("submit", this.parentNode, a, !0))
			},
			teardown: function() {
				if (f.nodeName(this, "form")) return !1;
				f.event.remove(this, "._submit")
			}
		}), f.support.changeBubbles || (f.event.special.change = {
			setup: function() {
				if (z.test(this.nodeName)) {
					if (this.type === "checkbox" || this.type === "radio") f.event.add(this, "propertychange._change", function(a) {
						a.originalEvent.propertyName === "checked" && (this._just_changed = !0)
					}), f.event.add(this, "click._change", function(a) {
						this._just_changed && !a.isTrigger && (this._just_changed = !1, f.event.simulate("change", this, a, !0))
					});
					return !1
				}
				f.event.add(this, "beforeactivate._change", function(a) {
					var b = a.target;
					z.test(b.nodeName) && !b._change_attached && (f.event.add(b, "change._change", function(a) {
						this.parentNode && !a.isSimulated && !a.isTrigger && f.event.simulate("change", this.parentNode, a, !0)
					}), b._change_attached = !0)
				})
			},
			handle: function(a) {
				var b = a.target;
				if (this !== b || a.isSimulated || a.isTrigger || b.type !== "radio" && b.type !== "checkbox") return a.handleObj.handler.apply(this, arguments)
			},
			teardown: function() {
				f.event.remove(this, "._change");
				return z.test(this.nodeName)
			}
		}), f.support.focusinBubbles || f.each({
			focus: "focusin",
			blur: "focusout"
		}, function(a, b) {
			var d = 0,
				e = function(a) {
					f.event.simulate(b, a.target, f.event.fix(a), !0)
				};
			f.event.special[b] = {
				setup: function() {
					d++ === 0 && c.addEventListener(a, e, !0)
				},
				teardown: function() {
					--d === 0 && c.removeEventListener(a, e, !0)
				}
			}
		}), f.fn.extend({
			on: function(a, c, d, e, g) {
				var h, i;
				if (typeof a == "object") {
					typeof c != "string" && (d = d || c, c = b);
					for (i in a) this.on(i, c, d, a[i], g);
					return this
				}
				d == null && e == null ? (e = c, d = c = b) : e == null && (typeof c == "string" ? (e = d, d = b) : (e = d, d = c, c = b));
				if (e === !1) e = J;
				else if (!e) return this;
				g === 1 && (h = e, e = function(a) {
					f().off(a);
					return h.apply(this, arguments)
				}, e.guid = h.guid || (h.guid = f.guid++));
				return this.each(function() {
					f.event.add(this, a, e, d, c)
				})
			},
			one: function(a, b, c, d) {
				return this.on(a, b, c, d, 1)
			},
			off: function(a, c, d) {
				if (a && a.preventDefault && a.handleObj) {
					var e = a.handleObj;
					f(a.delegateTarget).off(e.namespace ? e.origType + "." + e.namespace : e.origType, e.selector, e.handler);
					return this
				}
				if (typeof a == "object") {
					for (var g in a) this.off(g, c, a[g]);
					return this
				}
				if (c === !1 || typeof c == "function") d = c, c = b;
				d === !1 && (d = J);
				return this.each(function() {
					f.event.remove(this, a, d, c)
				})
			},
			bind: function(a, b, c) {
				return this.on(a, null, b, c)
			},
			unbind: function(a, b) {
				return this.off(a, null, b)
			},
			live: function(a, b, c) {
				f(this.context).on(a, this.selector, b, c);
				return this
			},
			die: function(a, b) {
				f(this.context).off(a, this.selector || "**", b);
				return this
			},
			delegate: function(a, b, c, d) {
				return this.on(b, a, c, d)
			},
			undelegate: function(a, b, c) {
				return arguments.length == 1 ? this.off(a, "**") : this.off(b, a, c)
			},
			trigger: function(a, b) {
				return this.each(function() {
					f.event.trigger(a, b, this)
				})
			},
			triggerHandler: function(a, b) {
				if (this[0]) return f.event.trigger(a, b, this[0], !0)
			},
			toggle: function(a) {
				var b = arguments,
					c = a.guid || f.guid++,
					d = 0,
					e = function(c) {
						var e = (f._data(this, "lastToggle" + a.guid) || 0) % d;
						f._data(this, "lastToggle" + a.guid, e + 1), c.preventDefault();
						return b[e].apply(this, arguments) || !1
					};
				e.guid = c;
				while (d < b.length) b[d++].guid = c;
				return this.click(e)
			},
			hover: function(a, b) {
				return this.mouseenter(a).mouseleave(b || a)
			}
		}), f.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(a, b) {
			f.fn[b] = function(a, c) {
				c == null && (c = a, a = null);
				return arguments.length > 0 ? this.on(b, null, a, c) : this.trigger(b)
			}, f.attrFn && (f.attrFn[b] = !0), C.test(b) && (f.event.fixHooks[b] = f.event.keyHooks), D.test(b) && (f.event.fixHooks[b] = f.event.mouseHooks)
		}),
		function() {
			function x(a, b, c, e, f, g) {
				for (var h = 0, i = e.length; h < i; h++) {
					var j = e[h];
					if (j) {
						var k = !1;
						j = j[a];
						while (j) {
							if (j[d] === c) {
								k = e[j.sizset];
								break
							}
							if (j.nodeType === 1) {
								g || (j[d] = c, j.sizset = h);
								if (typeof b != "string") {
									if (j === b) {
										k = !0;
										break
									}
								} else if (m.filter(b, [j]).length > 0) {
									k = j;
									break
								}
							}
							j = j[a]
						}
						e[h] = k
					}
				}
			}

			function w(a, b, c, e, f, g) {
				for (var h = 0, i = e.length; h < i; h++) {
					var j = e[h];
					if (j) {
						var k = !1;
						j = j[a];
						while (j) {
							if (j[d] === c) {
								k = e[j.sizset];
								break
							}
							j.nodeType === 1 && !g && (j[d] = c, j.sizset = h);
							if (j.nodeName.toLowerCase() === b) {
								k = j;
								break
							}
							j = j[a]
						}
						e[h] = k
					}
				}
			}
			var a = /((?:\((?:\([^()]+\)|[^()]+)+\)|\[(?:\[[^\[\]]*\]|['"][^'"]*['"]|[^\[\]'"]+)+\]|\\.|[^ >+~,(\[\\]+)+|[>+~])(\s*,\s*)?((?:.|\r|\n)*)/g,
				d = "sizcache" + (Math.random() + "").replace(".", ""),
				e = 0,
				g = Object.prototype.toString,
				h = !1,
				i = !0,
				j = /\\/g,
				k = /\r\n/g,
				l = /\W/;
			[0, 0].sort(function() {
				i = !1;
				return 0
			});
			var m = function(b, d, e, f) {
				e = e || [], d = d || c;
				var h = d;
				if (d.nodeType !== 1 && d.nodeType !== 9) return [];
				if (!b || typeof b != "string") return e;
				var i, j, k, l, n, q, r, t, u = !0,
					v = m.isXML(d),
					w = [],
					x = b;
				do {
					a.exec(""), i = a.exec(x);
					if (i) {
						x = i[3], w.push(i[1]);
						if (i[2]) {
							l = i[3];
							break
						}
					}
				} while (i);
				if (w.length > 1 && p.exec(b))
					if (w.length === 2 && o.relative[w[0]]) j = y(w[0] + w[1], d, f);
					else {
						j = o.relative[w[0]] ? [d] : m(w.shift(), d);
						while (w.length) b = w.shift(), o.relative[b] && (b += w.shift()), j = y(b, j, f)
					} else {
					!f && w.length > 1 && d.nodeType === 9 && !v && o.match.ID.test(w[0]) && !o.match.ID.test(w[w.length - 1]) && (n = m.find(w.shift(), d, v), d = n.expr ? m.filter(n.expr, n.set)[0] : n.set[0]);
					if (d) {
						n = f ? {
							expr: w.pop(),
							set: s(f)
						} : m.find(w.pop(), w.length === 1 && (w[0] === "~" || w[0] === "+") && d.parentNode ? d.parentNode : d, v), j = n.expr ? m.filter(n.expr, n.set) : n.set, w.length > 0 ? k = s(j) : u = !1;
						while (w.length) q = w.pop(), r = q, o.relative[q] ? r = w.pop() : q = "", r == null && (r = d), o.relative[q](k, r, v)
					} else k = w = []
				}
				k || (k = j), k || m.error(q || b);
				if (g.call(k) === "[object Array]")
					if (!u) e.push.apply(e, k);
					else if (d && d.nodeType === 1)
					for (t = 0; k[t] != null; t++) k[t] && (k[t] === !0 || k[t].nodeType === 1 && m.contains(d, k[t])) && e.push(j[t]);
				else
					for (t = 0; k[t] != null; t++) k[t] && k[t].nodeType === 1 && e.push(j[t]);
				else s(k, e);
				l && (m(l, h, e, f), m.uniqueSort(e));
				return e
			};
			m.uniqueSort = function(a) {
				if (u) {
					h = i, a.sort(u);
					if (h)
						for (var b = 1; b < a.length; b++) a[b] === a[b - 1] && a.splice(b--, 1)
				}
				return a
			}, m.matches = function(a, b) {
				return m(a, null, null, b)
			}, m.matchesSelector = function(a, b) {
				return m(b, null, null, [a]).length > 0
			}, m.find = function(a, b, c) {
				var d, e, f, g, h, i;
				if (!a) return [];
				for (e = 0, f = o.order.length; e < f; e++) {
					h = o.order[e];
					if (g = o.leftMatch[h].exec(a)) {
						i = g[1], g.splice(1, 1);
						if (i.substr(i.length - 1) !== "\\") {
							g[1] = (g[1] || "").replace(j, ""), d = o.find[h](g, b, c);
							if (d != null) {
								a = a.replace(o.match[h], "");
								break
							}
						}
					}
				}
				d || (d = typeof b.getElementsByTagName != "undefined" ? b.getElementsByTagName("*") : []);
				return {
					set: d,
					expr: a
				}
			}, m.filter = function(a, c, d, e) {
				var f, g, h, i, j, k, l, n, p, q = a,
					r = [],
					s = c,
					t = c && c[0] && m.isXML(c[0]);
				while (a && c.length) {
					for (h in o.filter)
						if ((f = o.leftMatch[h].exec(a)) != null && f[2]) {
							k = o.filter[h], l = f[1], g = !1, f.splice(1, 1);
							if (l.substr(l.length - 1) === "\\") continue;
							s === r && (r = []);
							if (o.preFilter[h]) {
								f = o.preFilter[h](f, s, d, r, e, t);
								if (!f) g = i = !0;
								else if (f === !0) continue
							}
							if (f)
								for (n = 0;
									(j = s[n]) != null; n++) j && (i = k(j, f, n, s), p = e ^ i, d && i != null ? p ? g = !0 : s[n] = !1 : p && (r.push(j), g = !0));
							if (i !== b) {
								d || (s = r), a = a.replace(o.match[h], "");
								if (!g) return [];
								break
							}
						}
					if (a === q)
						if (g == null) m.error(a);
						else break;
					q = a
				}
				return s
			}, m.error = function(a) {
				throw new Error("Syntax error, unrecognized expression: " + a)
			};
			var n = m.getText = function(a) {
					var b, c, d = a.nodeType,
						e = "";
					if (d) {
						if (d === 1 || d === 9 || d === 11) {
							if (typeof a.textContent == "string") return a.textContent;
							if (typeof a.innerText == "string") return a.innerText.replace(k, "");
							for (a = a.firstChild; a; a = a.nextSibling) e += n(a)
						} else if (d === 3 || d === 4) return a.nodeValue
					} else
						for (b = 0; c = a[b]; b++) c.nodeType !== 8 && (e += n(c));
					return e
				},
				o = m.selectors = {
					order: ["ID", "NAME", "TAG"],
					match: {
						ID: /#((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
						CLASS: /\.((?:[\w\u00c0-\uFFFF\-]|\\.)+)/,
						NAME: /\[name=['"]*((?:[\w\u00c0-\uFFFF\-]|\\.)+)['"]*\]/,
						ATTR: /\[\s*((?:[\w\u00c0-\uFFFF\-]|\\.)+)\s*(?:(\S?=)\s*(?:(['"])(.*?)\3|(#?(?:[\w\u00c0-\uFFFF\-]|\\.)*)|)|)\s*\]/,
						TAG: /^((?:[\w\u00c0-\uFFFF\*\-]|\\.)+)/,
						CHILD: /:(only|nth|last|first)-child(?:\(\s*(even|odd|(?:[+\-]?\d+|(?:[+\-]?\d*)?n\s*(?:[+\-]\s*\d+)?))\s*\))?/,
						POS: /:(nth|eq|gt|lt|first|last|even|odd)(?:\((\d*)\))?(?=[^\-]|$)/,
						PSEUDO: /:((?:[\w\u00c0-\uFFFF\-]|\\.)+)(?:\((['"]?)((?:\([^\)]+\)|[^\(\)]*)+)\2\))?/
					},
					leftMatch: {},
					attrMap: {
						"class": "className",
						"for": "htmlFor"
					},
					attrHandle: {
						href: function(a) {
							return a.getAttribute("href")
						},
						type: function(a) {
							return a.getAttribute("type")
						}
					},
					relative: {
						"+": function(a, b) {
							var c = typeof b == "string",
								d = c && !l.test(b),
								e = c && !d;
							d && (b = b.toLowerCase());
							for (var f = 0, g = a.length, h; f < g; f++)
								if (h = a[f]) {
									while ((h = h.previousSibling) && h.nodeType !== 1);
									a[f] = e || h && h.nodeName.toLowerCase() === b ? h || !1 : h === b
								}
							e && m.filter(b, a, !0)
						},
						">": function(a, b) {
							var c, d = typeof b == "string",
								e = 0,
								f = a.length;
							if (d && !l.test(b)) {
								b = b.toLowerCase();
								for (; e < f; e++) {
									c = a[e];
									if (c) {
										var g = c.parentNode;
										a[e] = g.nodeName.toLowerCase() === b ? g : !1
									}
								}
							} else {
								for (; e < f; e++) c = a[e], c && (a[e] = d ? c.parentNode : c.parentNode === b);
								d && m.filter(b, a, !0)
							}
						},
						"": function(a, b, c) {
							var d, f = e++,
								g = x;
							typeof b == "string" && !l.test(b) && (b = b.toLowerCase(), d = b, g = w), g("parentNode", b, f, a, d, c)
						},
						"~": function(a, b, c) {
							var d, f = e++,
								g = x;
							typeof b == "string" && !l.test(b) && (b = b.toLowerCase(), d = b, g = w), g("previousSibling", b, f, a, d, c)
						}
					},
					find: {
						ID: function(a, b, c) {
							if (typeof b.getElementById != "undefined" && !c) {
								var d = b.getElementById(a[1]);
								return d && d.parentNode ? [d] : []
							}
						},
						NAME: function(a, b) {
							if (typeof b.getElementsByName != "undefined") {
								var c = [],
									d = b.getElementsByName(a[1]);
								for (var e = 0, f = d.length; e < f; e++) d[e].getAttribute("name") === a[1] && c.push(d[e]);
								return c.length === 0 ? null : c
							}
						},
						TAG: function(a, b) {
							if (typeof b.getElementsByTagName != "undefined") return b.getElementsByTagName(a[1])
						}
					},
					preFilter: {
						CLASS: function(a, b, c, d, e, f) {
							a = " " + a[1].replace(j, "") + " ";
							if (f) return a;
							for (var g = 0, h;
								(h = b[g]) != null; g++) h && (e ^ (h.className && (" " + h.className + " ").replace(/[\t\n\r]/g, " ").indexOf(a) >= 0) ? c || d.push(h) : c && (b[g] = !1));
							return !1
						},
						ID: function(a) {
							return a[1].replace(j, "")
						},
						TAG: function(a, b) {
							return a[1].replace(j, "").toLowerCase()
						},
						CHILD: function(a) {
							if (a[1] === "nth") {
								a[2] || m.error(a[0]), a[2] = a[2].replace(/^\+|\s*/g, "");
								var b = /(-?)(\d*)(?:n([+\-]?\d*))?/.exec(a[2] === "even" && "2n" || a[2] === "odd" && "2n+1" || !/\D/.test(a[2]) && "0n+" + a[2] || a[2]);
								a[2] = b[1] + (b[2] || 1) - 0, a[3] = b[3] - 0
							} else a[2] && m.error(a[0]);
							a[0] = e++;
							return a
						},
						ATTR: function(a, b, c, d, e, f) {
							var g = a[1] = a[1].replace(j, "");
							!f && o.attrMap[g] && (a[1] = o.attrMap[g]), a[4] = (a[4] || a[5] || "").replace(j, ""), a[2] === "~=" && (a[4] = " " + a[4] + " ");
							return a
						},
						PSEUDO: function(b, c, d, e, f) {
							if (b[1] === "not")
								if ((a.exec(b[3]) || "").length > 1 || /^\w/.test(b[3])) b[3] = m(b[3], null, null, c);
								else {
									var g = m.filter(b[3], c, d, !0 ^ f);
									d || e.push.apply(e, g);
									return !1
								} else if (o.match.POS.test(b[0]) || o.match.CHILD.test(b[0])) return !0;
							return b
						},
						POS: function(a) {
							a.unshift(!0);
							return a
						}
					},
					filters: {
						enabled: function(a) {
							return a.disabled === !1 && a.type !== "hidden"
						},
						disabled: function(a) {
							return a.disabled === !0
						},
						checked: function(a) {
							return a.checked === !0
						},
						selected: function(a) {
							a.parentNode && a.parentNode.selectedIndex;
							return a.selected === !0
						},
						parent: function(a) {
							return !!a.firstChild
						},
						empty: function(a) {
							return !a.firstChild
						},
						has: function(a, b, c) {
							return !!m(c[3], a).length
						},
						header: function(a) {
							return /h\d/i.test(a.nodeName)
						},
						text: function(a) {
							var b = a.getAttribute("type"),
								c = a.type;
							return a.nodeName.toLowerCase() === "input" && "text" === c && (b === c || b === null)
						},
						radio: function(a) {
							return a.nodeName.toLowerCase() === "input" && "radio" === a.type
						},
						checkbox: function(a) {
							return a.nodeName.toLowerCase() === "input" && "checkbox" === a.type
						},
						file: function(a) {
							return a.nodeName.toLowerCase() === "input" && "file" === a.type
						},
						password: function(a) {
							return a.nodeName.toLowerCase() === "input" && "password" === a.type
						},
						submit: function(a) {
							var b = a.nodeName.toLowerCase();
							return (b === "input" || b === "button") && "submit" === a.type
						},
						image: function(a) {
							return a.nodeName.toLowerCase() === "input" && "image" === a.type
						},
						reset: function(a) {
							var b = a.nodeName.toLowerCase();
							return (b === "input" || b === "button") && "reset" === a.type
						},
						button: function(a) {
							var b = a.nodeName.toLowerCase();
							return b === "input" && "button" === a.type || b === "button"
						},
						input: function(a) {
							return /input|select|textarea|button/i.test(a.nodeName)
						},
						focus: function(a) {
							return a === a.ownerDocument.activeElement
						}
					},
					setFilters: {
						first: function(a, b) {
							return b === 0
						},
						last: function(a, b, c, d) {
							return b === d.length - 1
						},
						even: function(a, b) {
							return b % 2 === 0
						},
						odd: function(a, b) {
							return b % 2 === 1
						},
						lt: function(a, b, c) {
							return b < c[3] - 0
						},
						gt: function(a, b, c) {
							return b > c[3] - 0
						},
						nth: function(a, b, c) {
							return c[3] - 0 === b
						},
						eq: function(a, b, c) {
							return c[3] - 0 === b
						}
					},
					filter: {
						PSEUDO: function(a, b, c, d) {
							var e = b[1],
								f = o.filters[e];
							if (f) return f(a, c, b, d);
							if (e === "contains") return (a.textContent || a.innerText || n([a]) || "").indexOf(b[3]) >= 0;
							if (e === "not") {
								var g = b[3];
								for (var h = 0, i = g.length; h < i; h++)
									if (g[h] === a) return !1;
								return !0
							}
							m.error(e)
						},
						CHILD: function(a, b) {
							var c, e, f, g, h, i, j, k = b[1],
								l = a;
							switch (k) {
								case "only":
								case "first":
									while (l = l.previousSibling)
										if (l.nodeType === 1) return !1;
									if (k === "first") return !0;
									l = a;
								case "last":
									while (l = l.nextSibling)
										if (l.nodeType === 1) return !1;
									return !0;
								case "nth":
									c = b[2], e = b[3];
									if (c === 1 && e === 0) return !0;
									f = b[0], g = a.parentNode;
									if (g && (g[d] !== f || !a.nodeIndex)) {
										i = 0;
										for (l = g.firstChild; l; l = l.nextSibling) l.nodeType === 1 && (l.nodeIndex = ++i);
										g[d] = f
									}
									j = a.nodeIndex - e;
									return c === 0 ? j === 0 : j % c === 0 && j / c >= 0
							}
						},
						ID: function(a, b) {
							return a.nodeType === 1 && a.getAttribute("id") === b
						},
						TAG: function(a, b) {
							return b === "*" && a.nodeType === 1 || !!a.nodeName && a.nodeName.toLowerCase() === b
						},
						CLASS: function(a, b) {
							return (" " + (a.className || a.getAttribute("class")) + " ").indexOf(b) > -1
						},
						ATTR: function(a, b) {
							var c = b[1],
								d = m.attr ? m.attr(a, c) : o.attrHandle[c] ? o.attrHandle[c](a) : a[c] != null ? a[c] : a.getAttribute(c),
								e = d + "",
								f = b[2],
								g = b[4];
							return d == null ? f === "!=" : !f && m.attr ? d != null : f === "=" ? e === g : f === "*=" ? e.indexOf(g) >= 0 : f === "~=" ? (" " + e + " ").indexOf(g) >= 0 : g ? f === "!=" ? e !== g : f === "^=" ? e.indexOf(g) === 0 : f === "$=" ? e.substr(e.length - g.length) === g : f === "|=" ? e === g || e.substr(0, g.length + 1) === g + "-" : !1 : e && d !== !1
						},
						POS: function(a, b, c, d) {
							var e = b[2],
								f = o.setFilters[e];
							if (f) return f(a, c, b, d)
						}
					}
				},
				p = o.match.POS,
				q = function(a, b) {
					return "\\" + (b - 0 + 1)
				};
			for (var r in o.match) o.match[r] = new RegExp(o.match[r].source + /(?![^\[]*\])(?![^\(]*\))/.source), o.leftMatch[r] = new RegExp(/(^(?:.|\r|\n)*?)/.source + o.match[r].source.replace(/\\(\d+)/g, q));
			o.match.globalPOS = p;
			var s = function(a, b) {
				a = Array.prototype.slice.call(a, 0);
				if (b) {
					b.push.apply(b, a);
					return b
				}
				return a
			};
			try {
				Array.prototype.slice.call(c.documentElement.childNodes, 0)[0].nodeType
			} catch (t) {
				s = function(a, b) {
					var c = 0,
						d = b || [];
					if (g.call(a) === "[object Array]") Array.prototype.push.apply(d, a);
					else if (typeof a.length == "number")
						for (var e = a.length; c < e; c++) d.push(a[c]);
					else
						for (; a[c]; c++) d.push(a[c]);
					return d
				}
			}
			var u, v;
			c.documentElement.compareDocumentPosition ? u = function(a, b) {
					if (a === b) {
						h = !0;
						return 0
					}
					if (!a.compareDocumentPosition || !b.compareDocumentPosition) return a.compareDocumentPosition ? -1 : 1;
					return a.compareDocumentPosition(b) & 4 ? -1 : 1
				} : (u = function(a, b) {
					if (a === b) {
						h = !0;
						return 0
					}
					if (a.sourceIndex && b.sourceIndex) return a.sourceIndex - b.sourceIndex;
					var c, d, e = [],
						f = [],
						g = a.parentNode,
						i = b.parentNode,
						j = g;
					if (g === i) return v(a, b);
					if (!g) return -1;
					if (!i) return 1;
					while (j) e.unshift(j), j = j.parentNode;
					j = i;
					while (j) f.unshift(j), j = j.parentNode;
					c = e.length, d = f.length;
					for (var k = 0; k < c && k < d; k++)
						if (e[k] !== f[k]) return v(e[k], f[k]);
					return k === c ? v(a, f[k], -1) : v(e[k], b, 1)
				}, v = function(a, b, c) {
					if (a === b) return c;
					var d = a.nextSibling;
					while (d) {
						if (d === b) return -1;
						d = d.nextSibling
					}
					return 1
				}),
				function() {
					var a = c.createElement("div"),
						d = "script" + (new Date).getTime(),
						e = c.documentElement;
					a.innerHTML = "<a name='" + d + "'/>", e.insertBefore(a, e.firstChild), c.getElementById(d) && (o.find.ID = function(a, c, d) {
						if (typeof c.getElementById != "undefined" && !d) {
							var e = c.getElementById(a[1]);
							return e ? e.id === a[1] || typeof e.getAttributeNode != "undefined" && e.getAttributeNode("id").nodeValue === a[1] ? [e] : b : []
						}
					}, o.filter.ID = function(a, b) {
						var c = typeof a.getAttributeNode != "undefined" && a.getAttributeNode("id");
						return a.nodeType === 1 && c && c.nodeValue === b
					}), e.removeChild(a), e = a = null
				}(),
				function() {
					var a = c.createElement("div");
					a.appendChild(c.createComment("")), a.getElementsByTagName("*").length > 0 && (o.find.TAG = function(a, b) {
						var c = b.getElementsByTagName(a[1]);
						if (a[1] === "*") {
							var d = [];
							for (var e = 0; c[e]; e++) c[e].nodeType === 1 && d.push(c[e]);
							c = d
						}
						return c
					}), a.innerHTML = "<a href='#'></a>", a.firstChild && typeof a.firstChild.getAttribute != "undefined" && a.firstChild.getAttribute("href") !== "#" && (o.attrHandle.href = function(a) {
						return a.getAttribute("href", 2)
					}), a = null
				}(), c.querySelectorAll && function() {
					var a = m,
						b = c.createElement("div"),
						d = "__sizzle__";
					b.innerHTML = "<p class='TEST'></p>";
					if (!b.querySelectorAll || b.querySelectorAll(".TEST").length !== 0) {
						m = function(b, e, f, g) {
							e = e || c;
							if (!g && !m.isXML(e)) {
								var h = /^(\w+$)|^\.([\w\-]+$)|^#([\w\-]+$)/.exec(b);
								if (h && (e.nodeType === 1 || e.nodeType === 9)) {
									if (h[1]) return s(e.getElementsByTagName(b), f);
									if (h[2] && o.find.CLASS && e.getElementsByClassName) return s(e.getElementsByClassName(h[2]), f)
								}
								if (e.nodeType === 9) {
									if (b === "body" && e.body) return s([e.body], f);
									if (h && h[3]) {
										var i = e.getElementById(h[3]);
										if (!i || !i.parentNode) return s([], f);
										if (i.id === h[3]) return s([i], f)
									}
									try {
										return s(e.querySelectorAll(b), f)
									} catch (j) {}
								} else if (e.nodeType === 1 && e.nodeName.toLowerCase() !== "object") {
									var k = e,
										l = e.getAttribute("id"),
										n = l || d,
										p = e.parentNode,
										q = /^\s*[+~]/.test(b);
									l ? n = n.replace(/'/g, "\\$&") : e.setAttribute("id", n), q && p && (e = e.parentNode);
									try {
										if (!q || p) return s(e.querySelectorAll("[id='" + n + "'] " + b), f)
									} catch (r) {} finally {
										l || k.removeAttribute("id")
									}
								}
							}
							return a(b, e, f, g)
						};
						for (var e in a) m[e] = a[e];
						b = null
					}
				}(),
				function() {
					var a = c.documentElement,
						b = a.matchesSelector || a.mozMatchesSelector || a.webkitMatchesSelector || a.msMatchesSelector;
					if (b) {
						var d = !b.call(c.createElement("div"), "div"),
							e = !1;
						try {
							b.call(c.documentElement, "[test!='']:sizzle")
						} catch (f) {
							e = !0
						}
						m.matchesSelector = function(a, c) {
							c = c.replace(/\=\s*([^'"\]]*)\s*\]/g, "='$1']");
							if (!m.isXML(a)) try {
								if (e || !o.match.PSEUDO.test(c) && !/!=/.test(c)) {
									var f = b.call(a, c);
									if (f || !d || a.document && a.document.nodeType !== 11) return f
								}
							} catch (g) {}
							return m(c, null, null, [a]).length > 0
						}
					}
				}(),
				function() {
					var a = c.createElement("div");
					a.innerHTML = "<div class='test e'></div><div class='test'></div>";
					if (!!a.getElementsByClassName && a.getElementsByClassName("e").length !== 0) {
						a.lastChild.className = "e";
						if (a.getElementsByClassName("e").length === 1) return;
						o.order.splice(1, 0, "CLASS"), o.find.CLASS = function(a, b, c) {
							if (typeof b.getElementsByClassName != "undefined" && !c) return b.getElementsByClassName(a[1])
						}, a = null
					}
				}(), c.documentElement.contains ? m.contains = function(a, b) {
					return a !== b && (a.contains ? a.contains(b) : !0)
				} : c.documentElement.compareDocumentPosition ? m.contains = function(a, b) {
					return !!(a.compareDocumentPosition(b) & 16)
				} : m.contains = function() {
					return !1
				}, m.isXML = function(a) {
					var b = (a ? a.ownerDocument || a : 0).documentElement;
					return b ? b.nodeName !== "HTML" : !1
				};
			var y = function(a, b, c) {
				var d, e = [],
					f = "",
					g = b.nodeType ? [b] : b;
				while (d = o.match.PSEUDO.exec(a)) f += d[0], a = a.replace(o.match.PSEUDO, "");
				a = o.relative[a] ? a + "*" : a;
				for (var h = 0, i = g.length; h < i; h++) m(a, g[h], e, c);
				return m.filter(f, e)
			};
			m.attr = f.attr, m.selectors.attrMap = {}, f.find = m, f.expr = m.selectors, f.expr[":"] = f.expr.filters, f.unique = m.uniqueSort, f.text = m.getText, f.isXMLDoc = m.isXML, f.contains = m.contains
		}();
	var L = /Until$/,
		M = /^(?:parents|prevUntil|prevAll)/,
		N = /,/,
		O = /^.[^:#\[\.,]*$/,
		P = Array.prototype.slice,
		Q = f.expr.match.globalPOS,
		R = {
			children: !0,
			contents: !0,
			next: !0,
			prev: !0
		};
	f.fn.extend({
		find: function(a) {
			var b = this,
				c, d;
			if (typeof a != "string") return f(a).filter(function() {
				for (c = 0, d = b.length; c < d; c++)
					if (f.contains(b[c], this)) return !0
			});
			var e = this.pushStack("", "find", a),
				g, h, i;
			for (c = 0, d = this.length; c < d; c++) {
				g = e.length, f.find(a, this[c], e);
				if (c > 0)
					for (h = g; h < e.length; h++)
						for (i = 0; i < g; i++)
							if (e[i] === e[h]) {
								e.splice(h--, 1);
								break
							}
			}
			return e
		},
		has: function(a) {
			var b = f(a);
			return this.filter(function() {
				for (var a = 0, c = b.length; a < c; a++)
					if (f.contains(this, b[a])) return !0
			})
		},
		not: function(a) {
			return this.pushStack(T(this, a, !1), "not", a)
		},
		filter: function(a) {
			return this.pushStack(T(this, a, !0), "filter", a)
		},
		is: function(a) {
			return !!a && (typeof a == "string" ? Q.test(a) ? f(a, this.context).index(this[0]) >= 0 : f.filter(a, this).length > 0 : this.filter(a).length > 0)
		},
		closest: function(a, b) {
			var c = [],
				d, e, g = this[0];
			if (f.isArray(a)) {
				var h = 1;
				while (g && g.ownerDocument && g !== b) {
					for (d = 0; d < a.length; d++) f(g).is(a[d]) && c.push({
						selector: a[d],
						elem: g,
						level: h
					});
					g = g.parentNode, h++
				}
				return c
			}
			var i = Q.test(a) || typeof a != "string" ? f(a, b || this.context) : 0;
			for (d = 0, e = this.length; d < e; d++) {
				g = this[d];
				while (g) {
					if (i ? i.index(g) > -1 : f.find.matchesSelector(g, a)) {
						c.push(g);
						break
					}
					g = g.parentNode;
					if (!g || !g.ownerDocument || g === b || g.nodeType === 11) break
				}
			}
			c = c.length > 1 ? f.unique(c) : c;
			return this.pushStack(c, "closest", a)
		},
		index: function(a) {
			if (!a) return this[0] && this[0].parentNode ? this.prevAll().length : -1;
			if (typeof a == "string") return f.inArray(this[0], f(a));
			return f.inArray(a.jquery ? a[0] : a, this)
		},
		add: function(a, b) {
			var c = typeof a == "string" ? f(a, b) : f.makeArray(a && a.nodeType ? [a] : a),
				d = f.merge(this.get(), c);
			return this.pushStack(S(c[0]) || S(d[0]) ? d : f.unique(d))
		},
		andSelf: function() {
			return this.add(this.prevObject)
		}
	}), f.each({
		parent: function(a) {
			var b = a.parentNode;
			return b && b.nodeType !== 11 ? b : null
		},
		parents: function(a) {
			return f.dir(a, "parentNode")
		},
		parentsUntil: function(a, b, c) {
			return f.dir(a, "parentNode", c)
		},
		next: function(a) {
			return f.nth(a, 2, "nextSibling")
		},
		prev: function(a) {
			return f.nth(a, 2, "previousSibling")
		},
		nextAll: function(a) {
			return f.dir(a, "nextSibling")
		},
		prevAll: function(a) {
			return f.dir(a, "previousSibling")
		},
		nextUntil: function(a, b, c) {
			return f.dir(a, "nextSibling", c)
		},
		prevUntil: function(a, b, c) {
			return f.dir(a, "previousSibling", c)
		},
		siblings: function(a) {
			return f.sibling((a.parentNode || {}).firstChild, a)
		},
		children: function(a) {
			return f.sibling(a.firstChild)
		},
		contents: function(a) {
			return f.nodeName(a, "iframe") ? a.contentDocument || a.contentWindow.document : f.makeArray(a.childNodes)
		}
	}, function(a, b) {
		f.fn[a] = function(c, d) {
			var e = f.map(this, b, c);
			L.test(a) || (d = c), d && typeof d == "string" && (e = f.filter(d, e)), e = this.length > 1 && !R[a] ? f.unique(e) : e, (this.length > 1 || N.test(d)) && M.test(a) && (e = e.reverse());
			return this.pushStack(e, a, P.call(arguments).join(","))
		}
	}), f.extend({
		filter: function(a, b, c) {
			c && (a = ":not(" + a + ")");
			return b.length === 1 ? f.find.matchesSelector(b[0], a) ? [b[0]] : [] : f.find.matches(a, b)
		},
		dir: function(a, c, d) {
			var e = [],
				g = a[c];
			while (g && g.nodeType !== 9 && (d === b || g.nodeType !== 1 || !f(g).is(d))) g.nodeType === 1 && e.push(g), g = g[c];
			return e
		},
		nth: function(a, b, c, d) {
			b = b || 1;
			var e = 0;
			for (; a; a = a[c])
				if (a.nodeType === 1 && ++e === b) break;
			return a
		},
		sibling: function(a, b) {
			var c = [];
			for (; a; a = a.nextSibling) a.nodeType === 1 && a !== b && c.push(a);
			return c
		}
	});
	var V = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
		W = / jQuery\d+="(?:\d+|null)"/g,
		X = /^\s+/,
		Y = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/ig,
		Z = /<([\w:]+)/,
		$ = /<tbody/i,
		_ = /<|&#?\w+;/,
		ba = /<(?:script|style)/i,
		bb = /<(?:script|object|embed|option|style)/i,
		bc = new RegExp("<(?:" + V + ")[\\s/>]", "i"),
		bd = /checked\s*(?:[^=]|=\s*.checked.)/i,
		be = /\/(java|ecma)script/i,
		bf = /^\s*<!(?:\[CDATA\[|\-\-)/,
		bg = {
			option: [1, "<select multiple='multiple'>", "</select>"],
			legend: [1, "<fieldset>", "</fieldset>"],
			thead: [1, "<table>", "</table>"],
			tr: [2, "<table><tbody>", "</tbody></table>"],
			td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
			col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
			area: [1, "<map>", "</map>"],
			_default: [0, "", ""]
		},
		bh = U(c);
	bg.optgroup = bg.option, bg.tbody = bg.tfoot = bg.colgroup = bg.caption = bg.thead, bg.th = bg.td, f.support.htmlSerialize || (bg._default = [1, "div<div>", "</div>"]), f.fn.extend({
		text: function(a) {
			return f.access(this, function(a) {
				return a === b ? f.text(this) : this.empty().append((this[0] && this[0].ownerDocument || c).createTextNode(a))
			}, null, a, arguments.length)
		},
		wrapAll: function(a) {
			if (f.isFunction(a)) return this.each(function(b) {
				f(this).wrapAll(a.call(this, b))
			});
			if (this[0]) {
				var b = f(a, this[0].ownerDocument).eq(0).clone(!0);
				this[0].parentNode && b.insertBefore(this[0]), b.map(function() {
					var a = this;
					while (a.firstChild && a.firstChild.nodeType === 1) a = a.firstChild;
					return a
				}).append(this)
			}
			return this
		},
		wrapInner: function(a) {
			if (f.isFunction(a)) return this.each(function(b) {
				f(this).wrapInner(a.call(this, b))
			});
			return this.each(function() {
				var b = f(this),
					c = b.contents();
				c.length ? c.wrapAll(a) : b.append(a)
			})
		},
		wrap: function(a) {
			var b = f.isFunction(a);
			return this.each(function(c) {
				f(this).wrapAll(b ? a.call(this, c) : a)
			})
		},
		unwrap: function() {
			return this.parent().each(function() {
				f.nodeName(this, "body") || f(this).replaceWith(this.childNodes)
			}).end()
		},
		append: function() {
			return this.domManip(arguments, !0, function(a) {
				this.nodeType === 1 && this.appendChild(a)
			})
		},
		prepend: function() {
			return this.domManip(arguments, !0, function(a) {
				this.nodeType === 1 && this.insertBefore(a, this.firstChild)
			})
		},
		before: function() {
			if (this[0] && this[0].parentNode) return this.domManip(arguments, !1, function(a) {
				this.parentNode.insertBefore(a, this)
			});
			if (arguments.length) {
				var a = f
					.clean(arguments);
				a.push.apply(a, this.toArray());
				return this.pushStack(a, "before", arguments)
			}
		},
		after: function() {
			if (this[0] && this[0].parentNode) return this.domManip(arguments, !1, function(a) {
				this.parentNode.insertBefore(a, this.nextSibling)
			});
			if (arguments.length) {
				var a = this.pushStack(this, "after", arguments);
				a.push.apply(a, f.clean(arguments));
				return a
			}
		},
		remove: function(a, b) {
			for (var c = 0, d;
				(d = this[c]) != null; c++)
				if (!a || f.filter(a, [d]).length) !b && d.nodeType === 1 && (f.cleanData(d.getElementsByTagName("*")), f.cleanData([d])), d.parentNode && d.parentNode.removeChild(d);
			return this
		},
		empty: function() {
			for (var a = 0, b;
				(b = this[a]) != null; a++) {
				b.nodeType === 1 && f.cleanData(b.getElementsByTagName("*"));
				while (b.firstChild) b.removeChild(b.firstChild)
			}
			return this
		},
		clone: function(a, b) {
			a = a == null ? !1 : a, b = b == null ? a : b;
			return this.map(function() {
				return f.clone(this, a, b)
			})
		},
		html: function(a) {
			return f.access(this, function(a) {
				var c = this[0] || {},
					d = 0,
					e = this.length;
				if (a === b) return c.nodeType === 1 ? c.innerHTML.replace(W, "") : null;
				if (typeof a == "string" && !ba.test(a) && (f.support.leadingWhitespace || !X.test(a)) && !bg[(Z.exec(a) || ["", ""])[1].toLowerCase()]) {
					a = a.replace(Y, "<$1></$2>");
					try {
						for (; d < e; d++) c = this[d] || {}, c.nodeType === 1 && (f.cleanData(c.getElementsByTagName("*")), c.innerHTML = a);
						c = 0
					} catch (g) {}
				}
				c && this.empty().append(a)
			}, null, a, arguments.length)
		},
		replaceWith: function(a) {
			if (this[0] && this[0].parentNode) {
				if (f.isFunction(a)) return this.each(function(b) {
					var c = f(this),
						d = c.html();
					c.replaceWith(a.call(this, b, d))
				});
				typeof a != "string" && (a = f(a).detach());
				return this.each(function() {
					var b = this.nextSibling,
						c = this.parentNode;
					f(this).remove(), b ? f(b).before(a) : f(c).append(a)
				})
			}
			return this.length ? this.pushStack(f(f.isFunction(a) ? a() : a), "replaceWith", a) : this
		},
		detach: function(a) {
			return this.remove(a, !0)
		},
		domManip: function(a, c, d) {
			var e, g, h, i, j = a[0],
				k = [];
			if (!f.support.checkClone && arguments.length === 3 && typeof j == "string" && bd.test(j)) return this.each(function() {
				f(this).domManip(a, c, d, !0)
			});
			if (f.isFunction(j)) return this.each(function(e) {
				var g = f(this);
				a[0] = j.call(this, e, c ? g.html() : b), g.domManip(a, c, d)
			});
			if (this[0]) {
				i = j && j.parentNode, f.support.parentNode && i && i.nodeType === 11 && i.childNodes.length === this.length ? e = {
					fragment: i
				} : e = f.buildFragment(a, this, k), h = e.fragment, h.childNodes.length === 1 ? g = h = h.firstChild : g = h.firstChild;
				if (g) {
					c = c && f.nodeName(g, "tr");
					for (var l = 0, m = this.length, n = m - 1; l < m; l++) d.call(c ? bi(this[l], g) : this[l], e.cacheable || m > 1 && l < n ? f.clone(h, !0, !0) : h)
				}
				k.length && f.each(k, function(a, b) {
					b.src ? f.ajax({
						type: "GET",
						global: !1,
						url: b.src,
						async: !1,
						dataType: "script"
					}) : f.globalEval((b.text || b.textContent || b.innerHTML || "").replace(bf, "/*$0*/")), b.parentNode && b.parentNode.removeChild(b)
				})
			}
			return this
		}
	}), f.buildFragment = function(a, b, d) {
		var e, g, h, i, j = a[0];
		b && b[0] && (i = b[0].ownerDocument || b[0]), i.createDocumentFragment || (i = c), a.length === 1 && typeof j == "string" && j.length < 512 && i === c && j.charAt(0) === "<" && !bb.test(j) && (f.support.checkClone || !bd.test(j)) && (f.support.html5Clone || !bc.test(j)) && (g = !0, h = f.fragments[j], h && h !== 1 && (e = h)), e || (e = i.createDocumentFragment(), f.clean(a, i, e, d)), g && (f.fragments[j] = h ? e : 1);
		return {
			fragment: e,
			cacheable: g
		}
	}, f.fragments = {}, f.each({
		appendTo: "append",
		prependTo: "prepend",
		insertBefore: "before",
		insertAfter: "after",
		replaceAll: "replaceWith"
	}, function(a, b) {
		f.fn[a] = function(c) {
			var d = [],
				e = f(c),
				g = this.length === 1 && this[0].parentNode;
			if (g && g.nodeType === 11 && g.childNodes.length === 1 && e.length === 1) {
				e[b](this[0]);
				return this
			}
			for (var h = 0, i = e.length; h < i; h++) {
				var j = (h > 0 ? this.clone(!0) : this).get();
				f(e[h])[b](j), d = d.concat(j)
			}
			return this.pushStack(d, a, e.selector)
		}
	}), f.extend({
		clone: function(a, b, c) {
			var d, e, g, h = f.support.html5Clone || f.isXMLDoc(a) || !bc.test("<" + a.nodeName + ">") ? a.cloneNode(!0) : bo(a);
			if ((!f.support.noCloneEvent || !f.support.noCloneChecked) && (a.nodeType === 1 || a.nodeType === 11) && !f.isXMLDoc(a)) {
				bk(a, h), d = bl(a), e = bl(h);
				for (g = 0; d[g]; ++g) e[g] && bk(d[g], e[g])
			}
			if (b) {
				bj(a, h);
				if (c) {
					d = bl(a), e = bl(h);
					for (g = 0; d[g]; ++g) bj(d[g], e[g])
				}
			}
			d = e = null;
			return h
		},
		clean: function(a, b, d, e) {
			var g, h, i, j = [];
			b = b || c, typeof b.createElement == "undefined" && (b = b.ownerDocument || b[0] && b[0].ownerDocument || c);
			for (var k = 0, l;
				(l = a[k]) != null; k++) {
				typeof l == "number" && (l += "");
				if (!l) continue;
				if (typeof l == "string")
					if (!_.test(l)) l = b.createTextNode(l);
					else {
						l = l.replace(Y, "<$1></$2>");
						var m = (Z.exec(l) || ["", ""])[1].toLowerCase(),
							n = bg[m] || bg._default,
							o = n[0],
							p = b.createElement("div"),
							q = bh.childNodes,
							r;
						b === c ? bh.appendChild(p) : U(b).appendChild(p), p.innerHTML = n[1] + l + n[2];
						while (o--) p = p.lastChild;
						if (!f.support.tbody) {
							var s = $.test(l),
								t = m === "table" && !s ? p.firstChild && p.firstChild.childNodes : n[1] === "<table>" && !s ? p.childNodes : [];
							for (i = t.length - 1; i >= 0; --i) f.nodeName(t[i], "tbody") && !t[i].childNodes.length && t[i].parentNode.removeChild(t[i])
						}!f.support.leadingWhitespace && X.test(l) && p.insertBefore(b.createTextNode(X.exec(l)[0]), p.firstChild), l = p.childNodes, p && (p.parentNode.removeChild(p), q.length > 0 && (r = q[q.length - 1], r && r.parentNode && r.parentNode.removeChild(r)))
					}
				var u;
				if (!f.support.appendChecked)
					if (l[0] && typeof(u = l.length) == "number")
						for (i = 0; i < u; i++) bn(l[i]);
					else bn(l);
				l.nodeType ? j.push(l) : j = f.merge(j, l)
			}
			if (d) {
				g = function(a) {
					return !a.type || be.test(a.type)
				};
				for (k = 0; j[k]; k++) {
					h = j[k];
					if (e && f.nodeName(h, "script") && (!h.type || be.test(h.type))) e.push(h.parentNode ? h.parentNode.removeChild(h) : h);
					else {
						if (h.nodeType === 1) {
							var v = f.grep(h.getElementsByTagName("script"), g);
							j.splice.apply(j, [k + 1, 0].concat(v))
						}
						d.appendChild(h)
					}
				}
			}
			return j
		},
		cleanData: function(a) {
			var b, c, d = f.cache,
				e = f.event.special,
				g = f.support.deleteExpando;
			for (var h = 0, i;
				(i = a[h]) != null; h++) {
				if (i.nodeName && f.noData[i.nodeName.toLowerCase()]) continue;
				c = i[f.expando];
				if (c) {
					b = d[c];
					if (b && b.events) {
						for (var j in b.events) e[j] ? f.event.remove(i, j) : f.removeEvent(i, j, b.handle);
						b.handle && (b.handle.elem = null)
					}
					g ? delete i[f.expando] : i.removeAttribute && i.removeAttribute(f.expando), delete d[c]
				}
			}
		}
	});
	var bp = /alpha\([^)]*\)/i,
		bq = /opacity=([^)]*)/,
		br = /([A-Z]|^ms)/g,
		bs = /^[\-+]?(?:\d*\.)?\d+$/i,
		bt = /^-?(?:\d*\.)?\d+(?!px)[^\d\s]+$/i,
		bu = /^([\-+])=([\-+.\de]+)/,
		bv = /^margin/,
		bw = {
			position: "absolute",
			visibility: "hidden",
			display: "block"
		},
		bx = ["Top", "Right", "Bottom", "Left"],
		by, bz, bA;
	f.fn.css = function(a, c) {
		return f.access(this, function(a, c, d) {
			return d !== b ? f.style(a, c, d) : f.css(a, c)
		}, a, c, arguments.length > 1)
	}, f.extend({
		cssHooks: {
			opacity: {
				get: function(a, b) {
					if (b) {
						var c = by(a, "opacity");
						return c === "" ? "1" : c
					}
					return a.style.opacity
				}
			}
		},
		cssNumber: {
			fillOpacity: !0,
			fontWeight: !0,
			lineHeight: !0,
			opacity: !0,
			orphans: !0,
			widows: !0,
			zIndex: !0,
			zoom: !0
		},
		cssProps: {
			"float": f.support.cssFloat ? "cssFloat" : "styleFloat"
		},
		style: function(a, c, d, e) {
			if (!!a && a.nodeType !== 3 && a.nodeType !== 8 && !!a.style) {
				var g, h, i = f.camelCase(c),
					j = a.style,
					k = f.cssHooks[i];
				c = f.cssProps[i] || i;
				if (d === b) {
					if (k && "get" in k && (g = k.get(a, !1, e)) !== b) return g;
					return j[c]
				}
				h = typeof d, h === "string" && (g = bu.exec(d)) && (d = +(g[1] + 1) * +g[2] + parseFloat(f.css(a, c)), h = "number");
				if (d == null || h === "number" && isNaN(d)) return;
				h === "number" && !f.cssNumber[i] && (d += "px");
				if (!k || !("set" in k) || (d = k.set(a, d)) !== b) try {
					j[c] = d
				} catch (l) {}
			}
		},
		css: function(a, c, d) {
			var e, g;
			c = f.camelCase(c), g = f.cssHooks[c], c = f.cssProps[c] || c, c === "cssFloat" && (c = "float");
			if (g && "get" in g && (e = g.get(a, !0, d)) !== b) return e;
			if (by) return by(a, c)
		},
		swap: function(a, b, c) {
			var d = {},
				e, f;
			for (f in b) d[f] = a.style[f], a.style[f] = b[f];
			e = c.call(a);
			for (f in b) a.style[f] = d[f];
			return e
		}
	}), f.curCSS = f.css, c.defaultView && c.defaultView.getComputedStyle && (bz = function(a, b) {
		var c, d, e, g, h = a.style;
		b = b.replace(br, "-$1").toLowerCase(), (d = a.ownerDocument.defaultView) && (e = d.getComputedStyle(a, null)) && (c = e.getPropertyValue(b), c === "" && !f.contains(a.ownerDocument.documentElement, a) && (c = f.style(a, b))), !f.support.pixelMargin && e && bv.test(b) && bt.test(c) && (g = h.width, h.width = c, c = e.width, h.width = g);
		return c
	}), c.documentElement.currentStyle && (bA = function(a, b) {
		var c, d, e, f = a.currentStyle && a.currentStyle[b],
			g = a.style;
		f == null && g && (e = g[b]) && (f = e), bt.test(f) && (c = g.left, d = a.runtimeStyle && a.runtimeStyle.left, d && (a.runtimeStyle.left = a.currentStyle.left), g.left = b === "fontSize" ? "1em" : f, f = g.pixelLeft + "px", g.left = c, d && (a.runtimeStyle.left = d));
		return f === "" ? "auto" : f
	}), by = bz || bA, f.each(["height", "width"], function(a, b) {
		f.cssHooks[b] = {
			get: function(a, c, d) {
				if (c) return a.offsetWidth !== 0 ? bB(a, b, d) : f.swap(a, bw, function() {
					return bB(a, b, d)
				})
			},
			set: function(a, b) {
				return bs.test(b) ? b + "px" : b
			}
		}
	}), f.support.opacity || (f.cssHooks.opacity = {
		get: function(a, b) {
			return bq.test((b && a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? parseFloat(RegExp.$1) / 100 + "" : b ? "1" : ""
		},
		set: function(a, b) {
			var c = a.style,
				d = a.currentStyle,
				e = f.isNumeric(b) ? "alpha(opacity=" + b * 100 + ")" : "",
				g = d && d.filter || c.filter || "";
			c.zoom = 1;
			if (b >= 1 && f.trim(g.replace(bp, "")) === "") {
				c.removeAttribute("filter");
				if (d && !d.filter) return
			}
			c.filter = bp.test(g) ? g.replace(bp, e) : g + " " + e
		}
	}), f(function() {
		f.support.reliableMarginRight || (f.cssHooks.marginRight = {
			get: function(a, b) {
				return f.swap(a, {
					display: "inline-block"
				}, function() {
					return b ? by(a, "margin-right") : a.style.marginRight
				})
			}
		})
	}), f.expr && f.expr.filters && (f.expr.filters.hidden = function(a) {
		var b = a.offsetWidth,
			c = a.offsetHeight;
		return b === 0 && c === 0 || !f.support.reliableHiddenOffsets && (a.style && a.style.display || f.css(a, "display")) === "none"
	}, f.expr.filters.visible = function(a) {
		return !f.expr.filters.hidden(a)
	}), f.each({
		margin: "",
		padding: "",
		border: "Width"
	}, function(a, b) {
		f.cssHooks[a + b] = {
			expand: function(c) {
				var d, e = typeof c == "string" ? c.split(" ") : [c],
					f = {};
				for (d = 0; d < 4; d++) f[a + bx[d] + b] = e[d] || e[d - 2] || e[0];
				return f
			}
		}
	});
	var bC = /%20/g,
		bD = /\[\]$/,
		bE = /\r?\n/g,
		bF = /#.*$/,
		bG = /^(.*?):[ \t]*([^\r\n]*)\r?$/mg,
		bH = /^(?:color|date|datetime|datetime-local|email|hidden|month|number|password|range|search|tel|text|time|url|week)$/i,
		bI = /^(?:about|app|app\-storage|.+\-extension|file|res|widget):$/,
		bJ = /^(?:GET|HEAD)$/,
		bK = /^\/\//,
		bL = /\?/,
		bM = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi,
		bN = /^(?:select|textarea)/i,
		bO = /\s+/,
		bP = /([?&])_=[^&]*/,
		bQ = /^([\w\+\.\-]+:)(?:\/\/([^\/?#:]*)(?::(\d+))?)?/,
		bR = f.fn.load,
		bS = {},
		bT = {},
		bU, bV, bW = ["*/"] + ["*"];
	try {
		bU = e.href
	} catch (bX) {
		bU = c.createElement("a"), bU.href = "", bU = bU.href
	}
	bV = bQ.exec(bU.toLowerCase()) || [], f.fn.extend({
		load: function(a, c, d) {
			if (typeof a != "string" && bR) return bR.apply(this, arguments);
			if (!this.length) return this;
			var e = a.indexOf(" ");
			if (e >= 0) {
				var g = a.slice(e, a.length);
				a = a.slice(0, e)
			}
			var h = "GET";
			c && (f.isFunction(c) ? (d = c, c = b) : typeof c == "object" && (c = f.param(c, f.ajaxSettings.traditional), h = "POST"));
			var i = this;
			f.ajax({
				url: a,
				type: h,
				dataType: "html",
				data: c,
				complete: function(a, b, c) {
					c = a.responseText, a.isResolved() && (a.done(function(a) {
						c = a
					}), i.html(g ? f("<div>").append(c.replace(bM, "")).find(g) : c)), d && i.each(d, [c, b, a])
				}
			});
			return this
		},
		serialize: function() {
			return f.param(this.serializeArray())
		},
		serializeArray: function() {
			return this.map(function() {
				return this.elements ? f.makeArray(this.elements) : this
			}).filter(function() {
				return this.name && !this.disabled && (this.checked || bN.test(this.nodeName) || bH.test(this.type))
			}).map(function(a, b) {
				var c = f(this).val();
				return c == null ? null : f.isArray(c) ? f.map(c, function(a, c) {
					return {
						name: b.name,
						value: a.replace(bE, "\r\n")
					}
				}) : {
					name: b.name,
					value: c.replace(bE, "\r\n")
				}
			}).get()
		}
	}), f.each("ajaxStart ajaxStop ajaxComplete ajaxError ajaxSuccess ajaxSend".split(" "), function(a, b) {
		f.fn[b] = function(a) {
			return this.on(b, a)
		}
	}), f.each(["get", "post"], function(a, c) {
		f[c] = function(a, d, e, g) {
			f.isFunction(d) && (g = g || e, e = d, d = b);
			return f.ajax({
				type: c,
				url: a,
				data: d,
				success: e,
				dataType: g
			})
		}
	}), f.extend({
		getScript: function(a, c) {
			return f.get(a, b, c, "script")
		},
		getJSON: function(a, b, c) {
			return f.get(a, b, c, "json")
		},
		ajaxSetup: function(a, b) {
			b ? b$(a, f.ajaxSettings) : (b = a, a = f.ajaxSettings), b$(a, b);
			return a
		},
		ajaxSettings: {
			url: bU,
			isLocal: bI.test(bV[1]),
			global: !0,
			type: "GET",
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			processData: !0,
			async: !0,
			accepts: {
				xml: "application/xml, text/xml",
				html: "text/html",
				text: "text/plain",
				json: "application/json, text/javascript",
				"*": bW
			},
			contents: {
				xml: /xml/,
				html: /html/,
				json: /json/
			},
			responseFields: {
				xml: "responseXML",
				text: "responseText"
			},
			converters: {
				"* text": a.String,
				"text html": !0,
				"text json": f.parseJSON,
				"text xml": f.parseXML
			},
			flatOptions: {
				context: !0,
				url: !0
			}
		},
		ajaxPrefilter: bY(bS),
		ajaxTransport: bY(bT),
		ajax: function(a, c) {
			function w(a, c, l, m) {
				if (s !== 2) {
					s = 2, q && clearTimeout(q), p = b, n = m || "", v.readyState = a > 0 ? 4 : 0;
					var o, r, u, w = c,
						x = l ? ca(d, v, l) : b,
						y, z;
					if (a >= 200 && a < 300 || a === 304) {
						if (d.ifModified) {
							if (y = v.getResponseHeader("Last-Modified")) f.lastModified[k] = y;
							if (z = v.getResponseHeader("Etag")) f.etag[k] = z
						}
						if (a === 304) w = "notmodified", o = !0;
						else try {
							r = cb(d, x), w = "success", o = !0
						} catch (A) {
							w = "parsererror", u = A
						}
					} else {
						u = w;
						if (!w || a) w = "error", a < 0 && (a = 0)
					}
					v.status = a, v.statusText = "" + (c || w), o ? h.resolveWith(e, [r, w, v]) : h.rejectWith(e, [v, w, u]), v.statusCode(j), j = b, t && g.trigger("ajax" + (o ? "Success" : "Error"), [v, d, o ? r : u]), i.fireWith(e, [v, w]), t && (g.trigger("ajaxComplete", [v, d]), --f.active || f.event.trigger("ajaxStop"))
				}
			}
			typeof a == "object" && (c = a, a = b), c = c || {};
			var d = f.ajaxSetup({}, c),
				e = d.context || d,
				g = e !== d && (e.nodeType || e instanceof f) ? f(e) : f.event,
				h = f.Deferred(),
				i = f.Callbacks("once memory"),
				j = d.statusCode || {},
				k, l = {},
				m = {},
				n, o, p, q, r, s = 0,
				t, u, v = {
					readyState: 0,
					setRequestHeader: function(a, b) {
						if (!s) {
							var c = a.toLowerCase();
							a = m[c] = m[c] || a, l[a] = b
						}
						return this
					},
					getAllResponseHeaders: function() {
						return s === 2 ? n : null
					},
					getResponseHeader: function(a) {
						var c;
						if (s === 2) {
							if (!o) {
								o = {};
								while (c = bG.exec(n)) o[c[1].toLowerCase()] = c[2]
							}
							c = o[a.toLowerCase()]
						}
						return c === b ? null : c
					},
					overrideMimeType: function(a) {
						s || (d.mimeType = a);
						return this
					},
					abort: function(a) {
						a = a || "abort", p && p.abort(a), w(0, a);
						return this
					}
				};
			h.promise(v), v.success = v.done, v.error = v.fail, v.complete = i.add, v.statusCode = function(a) {
				if (a) {
					var b;
					if (s < 2)
						for (b in a) j[b] = [j[b], a[b]];
					else b = a[v.status], v.then(b, b)
				}
				return this
			}, d.url = ((a || d.url) + "").replace(bF, "").replace(bK, bV[1] + "//"), d.dataTypes = f.trim(d.dataType || "*").toLowerCase().split(bO), d.crossDomain == null && (r = bQ.exec(d.url.toLowerCase()), d.crossDomain = !(!r || r[1] == bV[1] && r[2] == bV[2] && (r[3] || (r[1] === "http:" ? 80 : 443)) == (bV[3] || (bV[1] === "http:" ? 80 : 443)))), d.data && d.processData && typeof d.data != "string" && (d.data = f.param(d.data, d.traditional)), bZ(bS, d, c, v);
			if (s === 2) return !1;
			t = d.global, d.type = d.type.toUpperCase(), d.hasContent = !bJ.test(d.type), t && f.active++ === 0 && f.event.trigger("ajaxStart");
			if (!d.hasContent) {
				d.data && (d.url += (bL.test(d.url) ? "&" : "?") + d.data, delete d.data), k = d.url;
				if (d.cache === !1) {
					var x = f.now(),
						y = d.url.replace(bP, "$1_=" + x);
					d.url = y + (y === d.url ? (bL.test(d.url) ? "&" : "?") + "_=" + x : "")
				}
			}(d.data && d.hasContent && d.contentType !== !1 || c.contentType) && v.setRequestHeader("Content-Type", d.contentType), d.ifModified && (k = k || d.url, f.lastModified[k] && v.setRequestHeader("If-Modified-Since", f.lastModified[k]), f.etag[k] && v.setRequestHeader("If-None-Match", f.etag[k])), v.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + (d.dataTypes[0] !== "*" ? ", " + bW + "; q=0.01" : "") : d.accepts["*"]);
			for (u in d.headers) v.setRequestHeader(u, d.headers[u]);
			if (d.beforeSend && (d.beforeSend.call(e, v, d) === !1 || s === 2)) {
				v.abort();
				return !1
			}
			for (u in {
					success: 1,
					error: 1,
					complete: 1
				}) v[u](d[u]);
			p = bZ(bT, d, c, v);
			if (!p) w(-1, "No Transport");
			else {
				v.readyState = 1, t && g.trigger("ajaxSend", [v, d]), d.async && d.timeout > 0 && (q = setTimeout(function() {
					v.abort("timeout")
				}, d.timeout));
				try {
					s = 1, p.send(l, w)
				} catch (z) {
					if (s < 2) w(-1, z);
					else throw z
				}
			}
			return v
		},
		param: function(a, c) {
			var d = [],
				e = function(a, b) {
					b = f.isFunction(b) ? b() : b, d[d.length] = encodeURIComponent(a) + "=" + encodeURIComponent(b)
				};
			c === b && (c = f.ajaxSettings.traditional);
			if (f.isArray(a) || a.jquery && !f.isPlainObject(a)) f.each(a, function() {
				e(this.name, this.value)
			});
			else
				for (var g in a) b_(g, a[g], c, e);
			return d.join("&").replace(bC, "+")
		}
	}), f.extend({
		active: 0,
		lastModified: {},
		etag: {}
	});
	var cc = f.now(),
		cd = /(\=)\?(&|$)|\?\?/i;
	f.ajaxSetup({
		jsonp: "callback",
		jsonpCallback: function() {
			return f.expando + "_" + cc++
		}
	}), f.ajaxPrefilter("json jsonp", function(b, c, d) {
		var e = typeof b.data == "string" && /^application\/x\-www\-form\-urlencoded/.test(b.contentType);
		if (b.dataTypes[0] === "jsonp" || b.jsonp !== !1 && (cd.test(b.url) || e && cd.test(b.data))) {
			var g, h = b.jsonpCallback = f.isFunction(b.jsonpCallback) ? b.jsonpCallback() : b.jsonpCallback,
				i = a[h],
				j = b.url,
				k = b.data,
				l = "$1" + h + "$2";
			b.jsonp !== !1 && (j = j.replace(cd, l), b.url === j && (e && (k = k.replace(cd, l)), b.data === k && (j += (/\?/.test(j) ? "&" : "?") + b.jsonp + "=" + h))), b.url = j, b.data = k, a[h] = function(a) {
				g = [a]
			}, d.always(function() {
				a[h] = i, g && f.isFunction(i) && a[h](g[0])
			}), b.converters["script json"] = function() {
				g || f.error(h + " was not called");
				return g[0]
			}, b.dataTypes[0] = "json";
			return "script"
		}
	}), f.ajaxSetup({
		accepts: {
			script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
		},
		contents: {
			script: /javascript|ecmascript/
		},
		converters: {
			"text script": function(a) {
				f.globalEval(a);
				return a
			}
		}
	}), f.ajaxPrefilter("script", function(a) {
		a.cache === b && (a.cache = !1), a.crossDomain && (a.type = "GET", a.global = !1)
	}), f.ajaxTransport("script", function(a) {
		if (a.crossDomain) {
			var d, e = c.head || c.getElementsByTagName("head")[0] || c.documentElement;
			return {
				send: function(f, g) {
					d = c.createElement("script"), d.async = "async", a.scriptCharset && (d.charset = a.scriptCharset), d.src = a.url, d.onload = d.onreadystatechange = function(a, c) {
						if (c || !d.readyState || /loaded|complete/.test(d.readyState)) d.onload = d.onreadystatechange = null, e && d.parentNode && e.removeChild(d), d = b, c || g(200, "success")
					}, e.insertBefore(d, e.firstChild)
				},
				abort: function() {
					d && d.onload(0, 1)
				}
			}
		}
	});
	var ce = a.ActiveXObject ? function() {
			for (var a in cg) cg[a](0, 1)
		} : !1,
		cf = 0,
		cg;
	f.ajaxSettings.xhr = a.ActiveXObject ? function() {
			return !this.isLocal && ch() || ci()
		} : ch,
		function(a) {
			f.extend(f.support, {
				ajax: !!a,
				cors: !!a && "withCredentials" in a
			})
		}(f.ajaxSettings.xhr()), f.support.ajax && f.ajaxTransport(function(c) {
			if (!c.crossDomain || f.support.cors) {
				var d;
				return {
					send: function(e, g) {
						var h = c.xhr(),
							i, j;
						c.username ? h.open(c.type, c.url, c.async, c.username, c.password) : h.open(c.type, c.url, c.async);
						if (c.xhrFields)
							for (j in c.xhrFields) h[j] = c.xhrFields[j];
						c.mimeType && h.overrideMimeType && h.overrideMimeType(c.mimeType), !c.crossDomain && !e["X-Requested-With"] && (e["X-Requested-With"] = "XMLHttpRequest");
						try {
							for (j in e) h.setRequestHeader(j, e[j])
						} catch (k) {}
						h.send(c.hasContent && c.data || null), d = function(a, e) {
							var j, k, l, m, n;
							try {
								if (d && (e || h.readyState === 4)) {
									d = b, i && (h.onreadystatechange = f.noop, ce && delete cg[i]);
									if (e) h.readyState !== 4 && h.abort();
									else {
										j = h.status, l = h.getAllResponseHeaders(), m = {}, n = h.responseXML, n && n.documentElement && (m.xml = n);
										try {
											m.text = h.responseText
										} catch (a) {}
										try {
											k = h.statusText
										} catch (o) {
											k = ""
										}!j && c.isLocal && !c.crossDomain ? j = m.text ? 200 : 404 : j === 1223 && (j = 204)
									}
								}
							} catch (p) {
								e || g(-1, p)
							}
							m && g(j, k, m, l)
						}, !c.async || h.readyState === 4 ? d() : (i = ++cf, ce && (cg || (cg = {}, f(a).unload(ce)), cg[i] = d), h.onreadystatechange = d)
					},
					abort: function() {
						d && d(0, 1)
					}
				}
			}
		});
	var cj = {},
		ck, cl, cm = /^(?:toggle|show|hide)$/,
		cn = /^([+\-]=)?([\d+.\-]+)([a-z%]*)$/i,
		co, cp = [
			["/*height*/", "/*marginTop*/", "/*marginBottom*/", "/*paddingTop*/", "/*paddingBottom*/"],
			["width", "marginLeft", "marginRight", "paddingLeft", "paddingRight"],
			["opacity"]
		],
		cq;
	f.fn.extend({
		show: function(a, b, c) {
			var d, e;
			if (a || a === 0) return this.animate(ct("show", 3), a, b, c);
			for (var g = 0, h = this.length; g < h; g++) d = this[g], d.style && (e = d.style.display, !f._data(d, "olddisplay") && e === "none" && (e = d.style.display = ""), (e === "" && f.css(d, "display") === "none" || !f.contains(d.ownerDocument.documentElement, d)) && f._data(d, "olddisplay", cu(d.nodeName)));
			for (g = 0; g < h; g++) {
				d = this[g];
				if (d.style) {
					e = d.style.display;
					if (e === "" || e === "none") d.style.display = f._data(d, "olddisplay") || ""
				}
			}
			return this
		},
		hide: function(a, b, c) {
			if (a || a === 0) return this.animate(ct("hide", 3), a, b, c);
			var d, e, g = 0,
				h = this.length;
			for (; g < h; g++) d = this[g], d.style && (e = f.css(d, "display"), e !== "none" && !f._data(d, "olddisplay") && f._data(d, "olddisplay", e));
			for (g = 0; g < h; g++) this[g].style && (this[g].style.display = "none");
			return this
		},
		_toggle: f.fn.toggle,
		toggle: function(a, b, c) {
			var d = typeof a == "boolean";
			f.isFunction(a) && f.isFunction(b) ? this._toggle.apply(this, arguments) : a == null || d ? this.each(function() {
				var b = d ? a : f(this).is(":hidden");
				f(this)[b ? "show" : "hide"]()
			}) : this.animate(ct("toggle", 3), a, b, c);
			return this
		},
		fadeTo: function(a, b, c, d) {
			return this.filter(":hidden").css("opacity", 0).show().end().animate({
				opacity: b
			}, a, c, d)
		},
		animate: function(a, b, c, d) {
			function g() {
				e.queue === !1 && f._mark(this);
				var b = f.extend({}, e),
					c = this.nodeType === 1,
					d = c && f(this).is(":hidden"),
					g, h, i, j, k, l, m, n, o, p, q;
				b.animatedProperties = {};
				for (i in a) {
					g = f.camelCase(i), i !== g && (a[g] = a[i], delete a[i]);
					if ((k = f.cssHooks[g]) && "expand" in k) {
						l = k.expand(a[g]), delete a[g];
						for (i in l) i in a || (a[i] = l[i])
					}
				}
				for (g in a) {
					h = a[g], f.isArray(h) ? (b.animatedProperties[g] = h[1], h = a[g] = h[0]) : b.animatedProperties[g] = b.specialEasing && b.specialEasing[g] || b.easing || "swing";
					if (h === "hide" && d || h === "show" && !d) return b.complete.call(this);
					c && (g === "height" || g === "width") && (b.overflow = [this.style.overflow, this.style.overflowX, this.style.overflowY], f.css(this, "display") === "inline" && f.css(this, "float") === "none" && (!f.support.inlineBlockNeedsLayout || cu(this.nodeName) === "inline" ? this.style.display = "inline-block" : this.style.zoom = 1))
				}
				b.overflow != null && (this.style.overflow = "visible");
				for (i in a) j = new f.fx(this, b, i), h = a[i], cm.test(h) ? (q = f._data(this, "toggle" + i) || (h === "toggle" ? d ? "show" : "hide" : 0), q ? (f._data(this, "toggle" + i, q === "show" ? "hide" : "show"), j[q]()) : j[h]()) : (m = cn.exec(h), n = j.cur(), m ? (o = parseFloat(m[2]), p = m[3] || (f.cssNumber[i] ? "" : "px"), p !== "px" && (f.style(this, i, (o || 1) + p), n = (o || 1) / j.cur() * n, f.style(this, i, n + p)), m[1] && (o = (m[1] === "-=" ? -1 : 1) * o + n), j.custom(n, o, p)) : j.custom(n, h, ""));
				return !0
			}
			var e = f.speed(b, c, d);
			if (f.isEmptyObject(a)) return this.each(e.complete, [!1]);
			a = f.extend({}, a);
			return e.queue === !1 ? this.each(g) : this.queue(e.queue, g)
		},
		stop: function(a, c, d) {
			typeof a != "string" && (d = c, c = a, a = b), c && a !== !1 && this.queue(a || "fx", []);
			return this.each(function() {
				function h(a, b, c) {
					var e = b[c];
					f.removeData(a, c, !0), e.stop(d)
				}
				var b, c = !1,
					e = f.timers,
					g = f._data(this);
				d || f._unmark(!0, this);
				if (a == null)
					for (b in g) g[b] && g[b].stop && b.indexOf(".run") === b.length - 4 && h(this, g, b);
				else g[b = a + ".run"] && g[b].stop && h(this, g, b);
				for (b = e.length; b--;) e[b].elem === this && (a == null || e[b].queue === a) && (d ? e[b](!0) : e[b].saveState(), c = !0, e.splice(b, 1));
				(!d || !c) && f.dequeue(this, a)
			})
		}
	}), f.each({
		slideDown: ct("show", 1),
		slideUp: ct("hide", 1),
		slideToggle: ct("toggle", 1),
		fadeIn: {
			opacity: "show"
		},
		fadeOut: {
			opacity: "hide"
		},
		fadeToggle: {
			opacity: "toggle"
		}
	}, function(a, b) {
		f.fn[a] = function(a, c, d) {
			return this.animate(b, a, c, d)
		}
	}), f.extend({
		speed: function(a, b, c) {
			var d = a && typeof a == "object" ? f.extend({}, a) : {
				complete: c || !c && b || f.isFunction(a) && a,
				duration: a,
				easing: c && b || b && !f.isFunction(b) && b
			};
			d.duration = f.fx.off ? 0 : typeof d.duration == "number" ? d.duration : d.duration in f.fx.speeds ? f.fx.speeds[d.duration] : f.fx.speeds._default;
			if (d.queue == null || d.queue === !0) d.queue = "fx";
			d.old = d.complete, d.complete = function(a) {
				f.isFunction(d.old) && d.old.call(this), d.queue ? f.dequeue(this, d.queue) : a !== !1 && f._unmark(this)
			};
			return d
		},
		easing: {
			linear: function(a) {
				return a
			},
			swing: function(a) {
				return -Math.cos(a * Math.PI) / 2 + .5
			}
		},
		timers: [],
		fx: function(a, b, c) {
			this.options = b, this.elem = a, this.prop = c, b.orig = b.orig || {}
		}
	}), f.fx.prototype = {
		update: function() {
			this.options.step && this.options.step.call(this.elem, this.now, this), (f.fx.step[this.prop] || f.fx.step._default)(this)
		},
		cur: function() {
			if (this.elem[this.prop] != null && (!this.elem.style || this.elem.style[this.prop] == null)) return this.elem[this.prop];
			var a, b = f.css(this.elem, this.prop);
			return isNaN(a = parseFloat(b)) ? !b || b === "auto" ? 0 : b : a
		},
		custom: function(a, c, d) {
			function h(a) {
				return e.step(a)
			}
			var e = this,
				g = f.fx;
			this.startTime = cq || cr(), this.end = c, this.now = this.start = a, this.pos = this.state = 0, this.unit = d || this.unit || (f.cssNumber[this.prop] ? "" : "px"), h.queue = this.options.queue, h.elem = this.elem, h.saveState = function() {
				f._data(e.elem, "fxshow" + e.prop) === b && (e.options.hide ? f._data(e.elem, "fxshow" + e.prop, e.start) : e.options.show && f._data(e.elem, "fxshow" + e.prop, e.end))
			}, h() && f.timers.push(h) && !co && (co = setInterval(g.tick, g.interval))
		},
		show: function() {
			var a = f._data(this.elem, "fxshow" + this.prop);
			this.options.orig[this.prop] = a || f.style(this.elem, this.prop), this.options.show = !0, a !== b ? this.custom(this.cur(), a) : this.custom(this.prop === "width" || this.prop === "height" ? 1 : 0, this.cur()), f(this.elem).show()
		},
		hide: function() {
			this.options.orig[this.prop] = f._data(this.elem, "fxshow" + this.prop) || f.style(this.elem, this.prop), this.options.hide = !0, this.custom(this.cur(), 0)
		},
		step: function(a) {
			var b, c, d, e = cq || cr(),
				g = !0,
				h = this.elem,
				i = this.options;
			if (a || e >= i.duration + this.startTime) {
				this.now = this.end, this.pos = this.state = 1, this.update(), i.animatedProperties[this.prop] = !0;
				for (b in i.animatedProperties) i.animatedProperties[b] !== !0 && (g = !1);
				if (g) {
					i.overflow != null && !f.support.shrinkWrapBlocks && f.each(["", "X", "Y"], function(a, b) {
						h.style["overflow" + b] = i.overflow[a]
					}), i.hide && f(h).hide();
					if (i.hide || i.show)
						for (b in i.animatedProperties) f.style(h, b, i.orig[b]), f.removeData(h, "fxshow" + b, !0), f.removeData(h, "toggle" + b, !0);
					d = i.complete, d && (i.complete = !1, d.call(h))
				}
				return !1
			}
			i.duration == Infinity ? this.now = e : (c = e - this.startTime, this.state = c / i.duration, this.pos = f.easing[i.animatedProperties[this.prop]](this.state, c, 0, 1, i.duration), this.now = this.start + (this.end - this.start) * this.pos), this.update();
			return !0
		}
	}, f.extend(f.fx, {
		tick: function() {
			var a, b = f.timers,
				c = 0;
			for (; c < b.length; c++) a = b[c], !a() && b[c] === a && b.splice(c--, 1);
			b.length || f.fx.stop()
		},
		interval: 13,
		stop: function() {
			clearInterval(co), co = null
		},
		speeds: {
			slow: 600,
			fast: 200,
			_default: 400
		},
		step: {
			opacity: function(a) {
				f.style(a.elem, "opacity", a.now)
			},
			_default: function(a) {
				a.elem.style && a.elem.style[a.prop] != null ? a.elem.style[a.prop] = a.now + a.unit : a.elem[a.prop] = a.now
			}
		}
	}), f.each(cp.concat.apply([], cp), function(a, b) {
		b.indexOf("margin") && (f.fx.step[b] = function(a) {
			f.style(a.elem, b, Math.max(0, a.now) + a.unit)
		})
	}), f.expr && f.expr.filters && (f.expr.filters.animated = function(a) {
		return f.grep(f.timers, function(b) {
			return a === b.elem
		}).length
	});
	var cv, cw = /^t(?:able|d|h)$/i,
		cx = /^(?:body|html)$/i;
	"getBoundingClientRect" in c.documentElement ? cv = function(a, b, c, d) {
		try {
			d = a.getBoundingClientRect()
		} catch (e) {}
		if (!d || !f.contains(c, a)) return d ? {
			top: d.top,
			left: d.left
		} : {
			top: 0,
			left: 0
		};
		var g = b.body,
			h = cy(b),
			i = c.clientTop || g.clientTop || 0,
			j = c.clientLeft || g.clientLeft || 0,
			k = h.pageYOffset || f.support.boxModel && c.scrollTop || g.scrollTop,
			l = h.pageXOffset || f.support.boxModel && c.scrollLeft || g.scrollLeft,
			m = d.top + k - i,
			n = d.left + l - j;
		return {
			top: m,
			left: n
		}
	} : cv = function(a, b, c) {
		var d, e = a.offsetParent,
			g = a,
			h = b.body,
			i = b.defaultView,
			j = i ? i.getComputedStyle(a, null) : a.currentStyle,
			k = a.offsetTop,
			l = a.offsetLeft;
		while ((a = a.parentNode) && a !== h && a !== c) {
			if (f.support.fixedPosition && j.position === "fixed") break;
			d = i ? i.getComputedStyle(a, null) : a.currentStyle, k -= a.scrollTop, l -= a.scrollLeft, a === e && (k += a.offsetTop, l += a.offsetLeft, f.support.doesNotAddBorder && (!f.support.doesAddBorderForTableAndCells || !cw.test(a.nodeName)) && (k += parseFloat(d.borderTopWidth) || 0, l += parseFloat(d.borderLeftWidth) || 0), g = e, e = a.offsetParent), f.support.subtractsBorderForOverflowNotVisible && d.overflow !== "visible" && (k += parseFloat(d.borderTopWidth) || 0, l += parseFloat(d.borderLeftWidth) || 0), j = d
		}
		if (j.position === "relative" || j.position === "static") k += h.offsetTop, l += h.offsetLeft;
		f.support.fixedPosition && j.position === "fixed" && (k += Math.max(c.scrollTop, h.scrollTop), l += Math.max(c.scrollLeft, h.scrollLeft));
		return {
			top: k,
			left: l
		}
	}, f.fn.offset = function(a) {
		if (arguments.length) return a === b ? this : this.each(function(b) {
			f.offset.setOffset(this, a, b)
		});
		var c = this[0],
			d = c && c.ownerDocument;
		if (!d) return null;
		if (c === d.body) return f.offset.bodyOffset(c);
		return cv(c, d, d.documentElement)
	}, f.offset = {
		bodyOffset: function(a) {
			var b = a.offsetTop,
				c = a.offsetLeft;
			f.support.doesNotIncludeMarginInBodyOffset && (b += parseFloat(f.css(a, "marginTop")) || 0, c += parseFloat(f.css(a, "marginLeft")) || 0);
			return {
				top: b,
				left: c
			}
		},
		setOffset: function(a, b, c) {
			var d = f.css(a, "position");
			d === "static" && (a.style.position = "relative");
			var e = f(a),
				g = e.offset(),
				h = f.css(a, "top"),
				i = f.css(a, "left"),
				j = (d === "absolute" || d === "fixed") && f.inArray("auto", [h, i]) > -1,
				k = {},
				l = {},
				m, n;
			j ? (l = e.position(), m = l.top, n = l.left) : (m = parseFloat(h) || 0, n = parseFloat(i) || 0), f.isFunction(b) && (b = b.call(a, c, g)), b.top != null && (k.top = b.top - g.top + m), b.left != null && (k.left = b.left - g.left + n), "using" in b ? b.using.call(a, k) : e.css(k)
		}
	}, f.fn.extend({
		position: function() {
			if (!this[0]) return null;
			var a = this[0],
				b = this.offsetParent(),
				c = this.offset(),
				d = cx.test(b[0].nodeName) ? {
					top: 0,
					left: 0
				} : b.offset();
			c.top -= parseFloat(f.css(a, "marginTop")) || 0, c.left -= parseFloat(f.css(a, "marginLeft")) || 0, d.top += parseFloat(f.css(b[0], "borderTopWidth")) || 0, d.left += parseFloat(f.css(b[0], "borderLeftWidth")) || 0;
			return {
				top: c.top - d.top,
				left: c.left - d.left
			}
		},
		offsetParent: function() {
			return this.map(function() {
				var a = this.offsetParent || c.body;
				while (a && !cx.test(a.nodeName) && f.css(a, "position") === "static") a = a.offsetParent;
				return a
			})
		}
	}), f.each({
		scrollLeft: "pageXOffset",
		scrollTop: "pageYOffset"
	}, function(a, c) {
		var d = /Y/.test(c);
		f.fn[a] = function(e) {
			return f.access(this, function(a, e, g) {
				var h = cy(a);
				if (g === b) return h ? c in h ? h[c] : f.support.boxModel && h.document.documentElement[e] || h.document.body[e] : a[e];
				h ? h.scrollTo(d ? f(h).scrollLeft() : g, d ? g : f(h).scrollTop()) : a[e] = g
			}, a, e, arguments.length, null)
		}
	}), f.each({
		Height: "height",
		Width: "width"
	}, function(a, c) {
		var d = "client" + a,
			e = "scroll" + a,
			g = "offset" + a;
		f.fn["inner" + a] = function() {
			var a = this[0];
			return a ? a.style ? parseFloat(f.css(a, c, "padding")) : this[c]() : null
		}, f.fn["outer" + a] = function(a) {
			var b = this[0];
			return b ? b.style ? parseFloat(f.css(b, c, a ? "margin" : "border")) : this[c]() : null
		}, f.fn[c] = function(a) {
			return f.access(this, function(a, c, h) {
				var i, j, k, l;
				if (f.isWindow(a)) {
					i = a.document, j = i.documentElement[d];
					return f.support.boxModel && j || i.body && i.body[d] || j
				}
				if (a.nodeType === 9) {
					i = a.documentElement;
					if (i[d] >= i[e]) return i[d];
					return Math.max(a.body[e], i[e], a.body[g], i[g])
				}
				if (h === b) {
					k = f.css(a, c), l = parseFloat(k);
					return f.isNumeric(l) ? l : k
				}
				f(a).css(c, h)
			}, c, a, arguments.length, null)
		}
	}), a.jQuery = a.$ = f, typeof define == "function" && define.amd && define.amd.jQuery && define("jquery", [], function() {
		return f
	})
})(window);

//js js/report/jquery-ui.min.js
/*! jQuery UI - v1.10.1 - 2013-02-15
* http://jqueryui.com
* Includes: jquery.ui.core.js, jquery.ui.widget.js, jquery.ui.mouse.js, jquery.ui.draggable.js, jquery.ui.droppable.js, jquery.ui.resizable.js, jquery.ui.selectable.js, jquery.ui.sortable.js, jquery.ui.effect.js, jquery.ui.accordion.js, jquery.ui.autocomplete.js, jquery.ui.button.js, jquery.ui.datepicker.js, jquery.ui.dialog.js, jquery.ui.effect-blind.js, jquery.ui.effect-bounce.js, jquery.ui.effect-clip.js, jquery.ui.effect-drop.js, jquery.ui.effect-explode.js, jquery.ui.effect-fade.js, jquery.ui.effect-fold.js, jquery.ui.effect-highlight.js, jquery.ui.effect-pulsate.js, jquery.ui.effect-scale.js, jquery.ui.effect-shake.js, jquery.ui.effect-slide.js, jquery.ui.effect-transfer.js, jquery.ui.menu.js, jquery.ui.position.js, jquery.ui.progressbar.js, jquery.ui.slider.js, jquery.ui.spinner.js, jquery.ui.tabs.js, jquery.ui.tooltip.js
* Copyright 2013 jQuery Foundation and other contributors; Licensed MIT */
(function(e,t){function i(t,n){var r,i,o,u=t.nodeName.toLowerCase();return"area"===u?(r=t.parentNode,i=r.name,!t.href||!i||r.nodeName.toLowerCase()!=="map"?!1:(o=e("img[usemap=#"+i+"]")[0],!!o&&s(o))):(/input|select|textarea|button|object/.test(u)?!t.disabled:"a"===u?t.href||n:n)&&s(t)}function s(t){return e.expr.filters.visible(t)&&!e(t).parents().addBack().filter(function(){return e.css(this,"visibility")==="hidden"}).length}var n=0,r=/^ui-id-\d+$/;e.ui=e.ui||{};if(e.ui.version)return;e.extend(e.ui,{version:"1.10.1",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),e.fn.extend({_focus:e.fn.focus,focus:function(t,n){return typeof t=="number"?this.each(function(){var r=this;setTimeout(function(){e(r).focus(),n&&n.call(r)},t)}):this._focus.apply(this,arguments)},scrollParent:function(){var t;return e.ui.ie&&/(static|relative)/.test(this.css("position"))||/absolute/.test(this.css("position"))?t=this.parents().filter(function(){return/(relative|absolute|fixed)/.test(e.css(this,"position"))&&/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0):t=this.parents().filter(function(){return/(auto|scroll)/.test(e.css(this,"overflow")+e.css(this,"overflow-y")+e.css(this,"overflow-x"))}).eq(0),/fixed/.test(this.css("position"))||!t.length?e(document):t},zIndex:function(n){if(n!==t)return this.css("zIndex",n);if(this.length){var r=e(this[0]),i,s;while(r.length&&r[0]!==document){i=r.css("position");if(i==="absolute"||i==="relative"||i==="fixed"){s=parseInt(r.css("zIndex"),10);if(!isNaN(s)&&s!==0)return s}r=r.parent()}}return 0},uniqueId:function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++n)})},removeUniqueId:function(){return this.each(function(){r.test(this.id)&&e(this).removeAttr("id")})}}),e.extend(e.expr[":"],{data:e.expr.createPseudo?e.expr.createPseudo(function(t){return function(n){return!!e.data(n,t)}}):function(t,n,r){return!!e.data(t,r[3])},focusable:function(t){return i(t,!isNaN(e.attr(t,"tabindex")))},tabbable:function(t){var n=e.attr(t,"tabindex"),r=isNaN(n);return(r||n>=0)&&i(t,!r)}}),e("<a>").outerWidth(1).jquery||e.each(["Width","Height"],function(n,r){function u(t,n,r,s){return e.each(i,function(){n-=parseFloat(e.css(t,"padding"+this))||0,r&&(n-=parseFloat(e.css(t,"border"+this+"Width"))||0),s&&(n-=parseFloat(e.css(t,"margin"+this))||0)}),n}var i=r==="Width"?["Left","Right"]:["Top","Bottom"],s=r.toLowerCase(),o={innerWidth:e.fn.innerWidth,innerHeight:e.fn.innerHeight,outerWidth:e.fn.outerWidth,outerHeight:e.fn.outerHeight};e.fn["inner"+r]=function(n){return n===t?o["inner"+r].call(this):this.each(function(){e(this).css(s,u(this,n)+"px")})},e.fn["outer"+r]=function(t,n){return typeof t!="number"?o["outer"+r].call(this,t):this.each(function(){e(this).css(s,u(this,t,!0,n)+"px")})}}),e.fn.addBack||(e.fn.addBack=function(e){return this.add(e==null?this.prevObject:this.prevObject.filter(e))}),e("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(e.fn.removeData=function(t){return function(n){return arguments.length?t.call(this,e.camelCase(n)):t.call(this)}}(e.fn.removeData)),e.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),e.support.selectstart="onselectstart"in document.createElement("div"),e.fn.extend({disableSelection:function(){return this.bind((e.support.selectstart?"selectstart":"mousedown")+".ui-disableSelection",function(e){e.preventDefault()})},enableSelection:function(){return this.unbind(".ui-disableSelection")}}),e.extend(e.ui,{plugin:{add:function(t,n,r){var i,s=e.ui[t].prototype;for(i in r)s.plugins[i]=s.plugins[i]||[],s.plugins[i].push([n,r[i]])},call:function(e,t,n){var r,i=e.plugins[t];if(!i||!e.element[0].parentNode||e.element[0].parentNode.nodeType===11)return;for(r=0;r<i.length;r++)e.options[i[r][0]]&&i[r][1].apply(e.element,n)}},hasScroll:function(t,n){if(e(t).css("overflow")==="hidden")return!1;var r=n&&n==="left"?"scrollLeft":"scrollTop",i=!1;return t[r]>0?!0:(t[r]=1,i=t[r]>0,t[r]=0,i)}})})(jQuery),function(e,t){var n=0,r=Array.prototype.slice,i=e.cleanData;e.cleanData=function(t){for(var n=0,r;(r=t[n])!=null;n++)try{e(r).triggerHandler("remove")}catch(s){}i(t)},e.widget=function(t,n,r){var i,s,o,u,a={},f=t.split(".")[0];t=t.split(".")[1],i=f+"-"+t,r||(r=n,n=e.Widget),e.expr[":"][i.toLowerCase()]=function(t){return!!e.data(t,i)},e[f]=e[f]||{},s=e[f][t],o=e[f][t]=function(e,t){if(!this._createWidget)return new o(e,t);arguments.length&&this._createWidget(e,t)},e.extend(o,s,{version:r.version,_proto:e.extend({},r),_childConstructors:[]}),u=new n,u.options=e.widget.extend({},u.options),e.each(r,function(t,r){if(!e.isFunction(r)){a[t]=r;return}a[t]=function(){var e=function(){return n.prototype[t].apply(this,arguments)},i=function(e){return n.prototype[t].apply(this,e)};return function(){var t=this._super,n=this._superApply,s;return this._super=e,this._superApply=i,s=r.apply(this,arguments),this._super=t,this._superApply=n,s}}()}),o.prototype=e.widget.extend(u,{widgetEventPrefix:s?u.widgetEventPrefix:t},a,{constructor:o,namespace:f,widgetName:t,widgetFullName:i}),s?(e.each(s._childConstructors,function(t,n){var r=n.prototype;e.widget(r.namespace+"."+r.widgetName,o,n._proto)}),delete s._childConstructors):n._childConstructors.push(o),e.widget.bridge(t,o)},e.widget.extend=function(n){var i=r.call(arguments,1),s=0,o=i.length,u,a;for(;s<o;s++)for(u in i[s])a=i[s][u],i[s].hasOwnProperty(u)&&a!==t&&(e.isPlainObject(a)?n[u]=e.isPlainObject(n[u])?e.widget.extend({},n[u],a):e.widget.extend({},a):n[u]=a);return n},e.widget.bridge=function(n,i){var s=i.prototype.widgetFullName||n;e.fn[n]=function(o){var u=typeof o=="string",a=r.call(arguments,1),f=this;return o=!u&&a.length?e.widget.extend.apply(null,[o].concat(a)):o,u?this.each(function(){var r,i=e.data(this,s);if(!i)return e.error("cannot call methods on "+n+" prior to initialization; "+"attempted to call method '"+o+"'");if(!e.isFunction(i[o])||o.charAt(0)==="_")return e.error("no such method '"+o+"' for "+n+" widget instance");r=i[o].apply(i,a);if(r!==i&&r!==t)return f=r&&r.jquery?f.pushStack(r.get()):r,!1}):this.each(function(){var t=e.data(this,s);t?t.option(o||{})._init():e.data(this,s,new i(o,this))}),f}},e.Widget=function(){},e.Widget._childConstructors=[],e.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(t,r){r=e(r||this.defaultElement||this)[0],this.element=e(r),this.uuid=n++,this.eventNamespace="."+this.widgetName+this.uuid,this.options=e.widget.extend({},this.options,this._getCreateOptions(),t),this.bindings=e(),this.hoverable=e(),this.focusable=e(),r!==this&&(e.data(r,this.widgetFullName,this),this._on(!0,this.element,{remove:function(e){e.target===r&&this.destroy()}}),this.document=e(r.style?r.ownerDocument:r.document||r),this.window=e(this.document[0].defaultView||this.document[0].parentWindow)),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:e.noop,_getCreateEventData:e.noop,_create:e.noop,_init:e.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled "+"ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:e.noop,widget:function(){return this.element},option:function(n,r){var i=n,s,o,u;if(arguments.length===0)return e.widget.extend({},this.options);if(typeof n=="string"){i={},s=n.split("."),n=s.shift();if(s.length){o=i[n]=e.widget.extend({},this.options[n]);for(u=0;u<s.length-1;u++)o[s[u]]=o[s[u]]||{},o=o[s[u]];n=s.pop();if(r===t)return o[n]===t?null:o[n];o[n]=r}else{if(r===t)return this.options[n]===t?null:this.options[n];i[n]=r}}return this._setOptions(i),this},_setOptions:function(e){var t;for(t in e)this._setOption(t,e[t]);return this},_setOption:function(e,t){return this.options[e]=t,e==="disabled"&&(this.widget().toggleClass(this.widgetFullName+"-disabled ui-state-disabled",!!t).attr("aria-disabled",t),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")),this},enable:function(){return this._setOption("disabled",!1)},disable:function(){return this._setOption("disabled",!0)},_on:function(t,n,r){var i,s=this;typeof t!="boolean"&&(r=n,n=t,t=!1),r?(n=i=e(n),this.bindings=this.bindings.add(n)):(r=n,n=this.element,i=this.widget()),e.each(r,function(r,o){function u(){if(!t&&(s.options.disabled===!0||e(this).hasClass("ui-state-disabled")))return;return(typeof o=="string"?s[o]:o).apply(s,arguments)}typeof o!="string"&&(u.guid=o.guid=o.guid||u.guid||e.guid++);var a=r.match(/^(\w+)\s*(.*)$/),f=a[1]+s.eventNamespace,l=a[2];l?i.delegate(l,f,u):n.bind(f,u)})},_off:function(e,t){t=(t||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,e.unbind(t).undelegate(t)},_delay:function(e,t){function n(){return(typeof e=="string"?r[e]:e).apply(r,arguments)}var r=this;return setTimeout(n,t||0)},_hoverable:function(t){this.hoverable=this.hoverable.add(t),this._on(t,{mouseenter:function(t){e(t.currentTarget).addClass("ui-state-hover")},mouseleave:function(t){e(t.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(t){this.focusable=this.focusable.add(t),this._on(t,{focusin:function(t){e(t.currentTarget).addClass("ui-state-focus")},focusout:function(t){e(t.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(t,n,r){var i,s,o=this.options[t];r=r||{},n=e.Event(n),n.type=(t===this.widgetEventPrefix?t:this.widgetEventPrefix+t).toLowerCase(),n.target=this.element[0],s=n.originalEvent;if(s)for(i in s)i in n||(n[i]=s[i]);return this.element.trigger(n,r),!(e.isFunction(o)&&o.apply(this.element[0],[n].concat(r))===!1||n.isDefaultPrevented())}},e.each({show:"fadeIn",hide:"fadeOut"},function(t,n){e.Widget.prototype["_"+t]=function(r,i,s){typeof i=="string"&&(i={effect:i});var o,u=i?i===!0||typeof i=="number"?n:i.effect||n:t;i=i||{},typeof i=="number"&&(i={duration:i}),o=!e.isEmptyObject(i),i.complete=s,i.delay&&r.delay(i.delay),o&&e.effects&&e.effects.effect[u]?r[t](i):u!==t&&r[u]?r[u](i.duration,i.easing,s):r.queue(function(n){e(this)[t](),s&&s.call(r[0]),n()})}})}(jQuery),function(e,t){var n=!1;e(document).mouseup(function(){n=!1}),e.widget("ui.mouse",{version:"1.10.1",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var t=this;this.element.bind("mousedown."+this.widgetName,function(e){return t._mouseDown(e)}).bind("click."+this.widgetName,function(n){if(!0===e.data(n.target,t.widgetName+".preventClickEvent"))return e.removeData(n.target,t.widgetName+".preventClickEvent"),n.stopImmediatePropagation(),!1}),this.started=!1},_mouseDestroy:function(){this.element.unbind("."+this.widgetName),this._mouseMoveDelegate&&e(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(t){if(n)return;this._mouseStarted&&this._mouseUp(t),this._mouseDownEvent=t;var r=this,i=t.which===1,s=typeof this.options.cancel=="string"&&t.target.nodeName?e(t.target).closest(this.options.cancel).length:!1;if(!i||s||!this._mouseCapture(t))return!0;this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){r.mouseDelayMet=!0},this.options.delay));if(this._mouseDistanceMet(t)&&this._mouseDelayMet(t)){this._mouseStarted=this._mouseStart(t)!==!1;if(!this._mouseStarted)return t.preventDefault(),!0}return!0===e.data(t.target,this.widgetName+".preventClickEvent")&&e.removeData(t.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(e){return r._mouseMove(e)},this._mouseUpDelegate=function(e){return r._mouseUp(e)},e(document).bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate),t.preventDefault(),n=!0,!0},_mouseMove:function(t){return e.ui.ie&&(!document.documentMode||document.documentMode<9)&&!t.button?this._mouseUp(t):this._mouseStarted?(this._mouseDrag(t),t.preventDefault()):(this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,t)!==!1,this._mouseStarted?this._mouseDrag(t):this._mouseUp(t)),!this._mouseStarted)},_mouseUp:function(t){return e(document).unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,t.target===this._mouseDownEvent.target&&e.data(t.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(t)),!1},_mouseDistanceMet:function(e){return Math.max(Math.abs(this._mouseDownEvent.pageX-e.pageX),Math.abs(this._mouseDownEvent.pageY-e.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return!0}})}(jQuery),function(e,t){e.widget("ui.draggable",e.ui.mouse,{version:"1.10.1",widgetEventPrefix:"drag",options:{addClasses:!0,appendTo:"parent",axis:!1,connectToSortable:!1,containment:!1,cursor:"auto",cursorAt:!1,grid:!1,handle:!1,helper:"original",iframeFix:!1,opacity:!1,refreshPositions:!1,revert:!1,revertDuration:500,scope:"default",scroll:!0,scrollSensitivity:20,scrollSpeed:20,snap:!1,snapMode:"both",snapTolerance:20,stack:!1,zIndex:!1,drag:null,start:null,stop:null},_create:function(){this.options.helper==="original"&&!/^(?:r|a|f)/.test(this.element.css("position"))&&(this.element[0].style.position="relative"),this.options.addClasses&&this.element.addClass("ui-draggable"),this.options.disabled&&this.element.addClass("ui-draggable-disabled"),this._mouseInit()},_destroy:function(){this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"),this._mouseDestroy()},_mouseCapture:function(t){var n=this.options;return this.helper||n.disabled||e(t.target).closest(".ui-resizable-handle").length>0?!1:(this.handle=this._getHandle(t),this.handle?(e(n.iframeFix===!0?"iframe":n.iframeFix).each(function(){e("<div class='ui-draggable-iframeFix' style='background: #fff;'></div>").css({width:this.offsetWidth+"px",height:this.offsetHeight+"px",position:"absolute",opacity:"0.001",zIndex:1e3}).css(e(this).offset()).appendTo("body")}),!0):!1)},_mouseStart:function(t){var n=this.options;return this.helper=this._createHelper(t),this.helper.addClass("ui-draggable-dragging"),this._cacheHelperProportions(),e.ui.ddmanager&&(e.ui.ddmanager.current=this),this._cacheMargins(),this.cssPosition=this.helper.css("position"),this.scrollParent=this.helper.scrollParent(),this.offset=this.positionAbs=this.element.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},e.extend(this.offset,{click:{left:t.pageX-this.offset.left,top:t.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.originalPosition=this.position=this._generatePosition(t),this.originalPageX=t.pageX,this.originalPageY=t.pageY,n.cursorAt&&this._adjustOffsetFromHelper(n.cursorAt),n.containment&&this._setContainment(),this._trigger("start",t)===!1?(this._clear(),!1):(this._cacheHelperProportions(),e.ui.ddmanager&&!n.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t),this._mouseDrag(t,!0),e.ui.ddmanager&&e.ui.ddmanager.dragStart(this,t),!0)},_mouseDrag:function(t,n){this.position=this._generatePosition(t),this.positionAbs=this._convertPositionTo("absolute");if(!n){var r=this._uiHash();if(this._trigger("drag",t,r)===!1)return this._mouseUp({}),!1;this.position=r.position}if(!this.options.axis||this.options.axis!=="y")this.helper[0].style.left=this.position.left+"px";if(!this.options.axis||this.options.axis!=="x")this.helper[0].style.top=this.position.top+"px";return e.ui.ddmanager&&e.ui.ddmanager.drag(this,t),!1},_mouseStop:function(t){var n,r=this,i=!1,s=!1;e.ui.ddmanager&&!this.options.dropBehaviour&&(s=e.ui.ddmanager.drop(this,t)),this.dropped&&(s=this.dropped,this.dropped=!1),n=this.element[0];while(n&&(n=n.parentNode))n===document&&(i=!0);return!i&&this.options.helper==="original"?!1:(this.options.revert==="invalid"&&!s||this.options.revert==="valid"&&s||this.options.revert===!0||e.isFunction(this.options.revert)&&this.options.revert.call(this.element,s)?e(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){r._trigger("stop",t)!==!1&&r._clear()}):this._trigger("stop",t)!==!1&&this._clear(),!1)},_mouseUp:function(t){return e("div.ui-draggable-iframeFix").each(function(){this.parentNode.removeChild(this)}),e.ui.ddmanager&&e.ui.ddmanager.dragStop(this,t),e.ui.mouse.prototype._mouseUp.call(this,t)},cancel:function(){return this.helper.is(".ui-draggable-dragging")?this._mouseUp({}):this._clear(),this},_getHandle:function(t){var n=!this.options.handle||!e(this.options.handle,this.element).length?!0:!1;return e(this.options.handle,this.element).find("*").addBack().each(function(){this===t.target&&(n=!0)}),n},_createHelper:function(t){var n=this.options,r=e.isFunction(n.helper)?e(n.helper.apply(this.element[0],[t])):n.helper==="clone"?this.element.clone().removeAttr("id"):this.element;return r.parents("body").length||r.appendTo(n.appendTo==="parent"?this.element[0].parentNode:n.appendTo),r[0]!==this.element[0]&&!/(fixed|absolute)/.test(r.css("position"))&&r.css("position","absolute"),r},_adjustOffsetFromHelper:function(t){typeof t=="string"&&(t=t.split(" ")),e.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var t=this.offsetParent.offset();this.cssPosition==="absolute"&&this.scrollParent[0]!==document&&e.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop());if(this.offsetParent[0]===document.body||this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()==="html"&&e.ui.ie)t={top:0,left:0};return{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if(this.cssPosition==="relative"){var e=this.element.position();return{top:e.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:e.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.element.css("marginLeft"),10)||0,top:parseInt(this.element.css("marginTop"),10)||0,right:parseInt(this.element.css("marginRight"),10)||0,bottom:parseInt(this.element.css("marginBottom"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t,n,r,i=this.options;i.containment==="parent"&&(i.containment=this.helper[0].parentNode);if(i.containment==="document"||i.containment==="window")this.containment=[i.containment==="document"?0:e(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,i.containment==="document"?0:e(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,(i.containment==="document"?0:e(window).scrollLeft())+e(i.containment==="document"?document:window).width()-this.helperProportions.width-this.margins.left,(i.containment==="document"?0:e(window).scrollTop())+(e(i.containment==="document"?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];if(!/^(document|window|parent)$/.test(i.containment)&&i.containment.constructor!==Array){n=e(i.containment),r=n[0];if(!r)return;t=e(r).css("overflow")!=="hidden",this.containment=[(parseInt(e(r).css("borderLeftWidth"),10)||0)+(parseInt(e(r).css("paddingLeft"),10)||0),(parseInt(e(r).css("borderTopWidth"),10)||0)+(parseInt(e(r).css("paddingTop"),10)||0),(t?Math.max(r.scrollWidth,r.offsetWidth):r.offsetWidth)-(parseInt(e(r).css("borderLeftWidth"),10)||0)-(parseInt(e(r).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(t?Math.max(r.scrollHeight,r.offsetHeight):r.offsetHeight)-(parseInt(e(r).css("borderTopWidth"),10)||0)-(parseInt(e(r).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom],this.relative_container=n}else i.containment.constructor===Array&&(this.containment=i.containment)},_convertPositionTo:function(t,n){n||(n=this.position);var r=t==="absolute"?1:-1,i=this.cssPosition!=="absolute"||this.scrollParent[0]!==document&&!!e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,s=/(html|body)/i.test(i[0].tagName);return{top:n.top+this.offset.relative.top*r+this.offset.parent.top*r-(this.cssPosition==="fixed"?-this.scrollParent.scrollTop():s?0:i.scrollTop())*r,left:n.left+this.offset.relative.left*r+this.offset.parent.left*r-(this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():s?0:i.scrollLeft())*r}},_generatePosition:function(t){var n,r,i,s,o=this.options,u=this.cssPosition!=="absolute"||this.scrollParent[0]!==document&&!!e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,a=/(html|body)/i.test(u[0].tagName),f=t.pageX,l=t.pageY;return this.originalPosition&&(this.containment&&(this.relative_container?(r=this.relative_container.offset(),n=[this.containment[0]+r.left,this.containment[1]+r.top,this.containment[2]+r.left,this.containment[3]+r.top]):n=this.containment,t.pageX-this.offset.click.left<n[0]&&(f=n[0]+this.offset.click.left),t.pageY-this.offset.click.top<n[1]&&(l=n[1]+this.offset.click.top),t.pageX-this.offset.click.left>n[2]&&(f=n[2]+this.offset.click.left),t.pageY-this.offset.click.top>n[3]&&(l=n[3]+this.offset.click.top)),o.grid&&(i=o.grid[1]?this.originalPageY+Math.round((l-this.originalPageY)/o.grid[1])*o.grid[1]:this.originalPageY,l=n?i-this.offset.click.top>=n[1]||i-this.offset.click.top>n[3]?i:i-this.offset.click.top>=n[1]?i-o.grid[1]:i+o.grid[1]:i,s=o.grid[0]?this.originalPageX+Math.round((f-this.originalPageX)/o.grid[0])*o.grid[0]:this.originalPageX,f=n?s-this.offset.click.left>=n[0]||s-this.offset.click.left>n[2]?s:s-this.offset.click.left>=n[0]?s-o.grid[0]:s+o.grid[0]:s)),{top:l-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+(this.cssPosition==="fixed"?-this.scrollParent.scrollTop():a?0:u.scrollTop()),left:f-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+(this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():a?0:u.scrollLeft())}},_clear:function(){this.helper.removeClass("ui-draggable-dragging"),this.helper[0]!==this.element[0]&&!this.cancelHelperRemoval&&this.helper.remove(),this.helper=null,this.cancelHelperRemoval=!1},_trigger:function(t,n,r){return r=r||this._uiHash(),e.ui.plugin.call(this,t,[n,r]),t==="drag"&&(this.positionAbs=this._convertPositionTo("absolute")),e.Widget.prototype._trigger.call(this,t,n,r)},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}}}),e.ui.plugin.add("draggable","connectToSortable",{start:function(t,n){var r=e(this).data("ui-draggable"),i=r.options,s=e.extend({},n,{item:r.element});r.sortables=[],e(i.connectToSortable).each(function(){var n=e.data(this,"ui-sortable");n&&!n.options.disabled&&(r.sortables.push({instance:n,shouldRevert:n.options.revert}),n.refreshPositions(),n._trigger("activate",t,s))})},stop:function(t,n){var r=e(this).data("ui-draggable"),i=e.extend({},n,{item:r.element});e.each(r.sortables,function(){this.instance.isOver?(this.instance.isOver=0,r.cancelHelperRemoval=!0,this.instance.cancelHelperRemoval=!1,this.shouldRevert&&(this.instance.options.revert=!0),this.instance._mouseStop(t),this.instance.options.helper=this.instance.options._helper,r.options.helper==="original"&&this.instance.currentItem.css({top:"auto",left:"auto"})):(this.instance.cancelHelperRemoval=!1,this.instance._trigger("deactivate",t,i))})},drag:function(t,n){var r=e(this).data("ui-draggable"),i=this;e.each(r.sortables,function(){var s=!1,o=this;this.instance.positionAbs=r.positionAbs,this.instance.helperProportions=r.helperProportions,this.instance.offset.click=r.offset.click,this.instance._intersectsWith(this.instance.containerCache)&&(s=!0,e.each(r.sortables,function(){return this.instance.positionAbs=r.positionAbs,this.instance.helperProportions=r.helperProportions,this.instance.offset.click=r.offset.click,this!==o&&this.instance._intersectsWith(this.instance.containerCache)&&e.contains(o.instance.element[0],this.instance.element[0])&&(s=!1),s})),s?(this.instance.isOver||(this.instance.isOver=1,this.instance.currentItem=e(i).clone().removeAttr("id").appendTo(this.instance.element).data("ui-sortable-item",!0),this.instance.options._helper=this.instance.options.helper,this.instance.options.helper=function(){return n.helper[0]},t.target=this.instance.currentItem[0],this.instance._mouseCapture(t,!0),this.instance._mouseStart(t,!0,!0),this.instance.offset.click.top=r.offset.click.top,this.instance.offset.click.left=r.offset.click.left,this.instance.offset.parent.left-=r.offset.parent.left-this.instance.offset.parent.left,this.instance.offset.parent.top-=r.offset.parent.top-this.instance.offset.parent.top,r._trigger("toSortable",t),r.dropped=this.instance.element,r.currentItem=r.element,this.instance.fromOutside=r),this.instance.currentItem&&this.instance._mouseDrag(t)):this.instance.isOver&&(this.instance.isOver=0,this.instance.cancelHelperRemoval=!0,this.instance.options.revert=!1,this.instance._trigger("out",t,this.instance._uiHash(this.instance)),this.instance._mouseStop(t,!0),this.instance.options.helper=this.instance.options._helper,this.instance.currentItem.remove(),this.instance.placeholder&&this.instance.placeholder.remove(),r._trigger("fromSortable",t),r.dropped=!1)})}}),e.ui.plugin.add("draggable","cursor",{start:function(){var t=e("body"),n=e(this).data("ui-draggable").options;t.css("cursor")&&(n._cursor=t.css("cursor")),t.css("cursor",n.cursor)},stop:function(){var t=e(this).data("ui-draggable").options;t._cursor&&e("body").css("cursor",t._cursor)}}),e.ui.plugin.add("draggable","opacity",{start:function(t,n){var r=e(n.helper),i=e(this).data("ui-draggable").options;r.css("opacity")&&(i._opacity=r.css("opacity")),r.css("opacity",i.opacity)},stop:function(t,n){var r=e(this).data("ui-draggable").options;r._opacity&&e(n.helper).css("opacity",r._opacity)}}),e.ui.plugin.add("draggable","scroll",{start:function(){var t=e(this).data("ui-draggable");t.scrollParent[0]!==document&&t.scrollParent[0].tagName!=="HTML"&&(t.overflowOffset=t.scrollParent.offset())},drag:function(t){var n=e(this).data("ui-draggable"),r=n.options,i=!1;if(n.scrollParent[0]!==document&&n.scrollParent[0].tagName!=="HTML"){if(!r.axis||r.axis!=="x")n.overflowOffset.top+n.scrollParent[0].offsetHeight-t.pageY<r.scrollSensitivity?n.scrollParent[0].scrollTop=i=n.scrollParent[0].scrollTop+r.scrollSpeed:t.pageY-n.overflowOffset.top<r.scrollSensitivity&&(n.scrollParent[0].scrollTop=i=n.scrollParent[0].scrollTop-r.scrollSpeed);if(!r.axis||r.axis!=="y")n.overflowOffset.left+n.scrollParent[0].offsetWidth-t.pageX<r.scrollSensitivity?n.scrollParent[0].scrollLeft=i=n.scrollParent[0].scrollLeft+r.scrollSpeed:t.pageX-n.overflowOffset.left<r.scrollSensitivity&&(n.scrollParent[0].scrollLeft=i=n.scrollParent[0].scrollLeft-r.scrollSpeed)}else{if(!r.axis||r.axis!=="x")t.pageY-e(document).scrollTop()<r.scrollSensitivity?i=e(document).scrollTop(e(document).scrollTop()-r.scrollSpeed):e(window).height()-(t.pageY-e(document).scrollTop())<r.scrollSensitivity&&(i=e(document).scrollTop(e(document).scrollTop()+r.scrollSpeed));if(!r.axis||r.axis!=="y")t.pageX-e(document).scrollLeft()<r.scrollSensitivity?i=e(document).scrollLeft(e(document).scrollLeft()-r.scrollSpeed):e(window).width()-(t.pageX-e(document).scrollLeft())<r.scrollSensitivity&&(i=e(document).scrollLeft(e(document).scrollLeft()+r.scrollSpeed))}i!==!1&&e.ui.ddmanager&&!r.dropBehaviour&&e.ui.ddmanager.prepareOffsets(n,t)}}),e.ui.plugin.add("draggable","snap",{start:function(){var t=e(this).data("ui-draggable"),n=t.options;t.snapElements=[],e(n.snap.constructor!==String?n.snap.items||":data(ui-draggable)":n.snap).each(function(){var n=e(this),r=n.offset();this!==t.element[0]&&t.snapElements.push({item:this,width:n.outerWidth(),height:n.outerHeight(),top:r.top,left:r.left})})},drag:function(t,n){var r,i,s,o,u,a,f,l,c,h,p=e(this).data("ui-draggable"),d=p.options,v=d.snapTolerance,m=n.offset.left,g=m+p.helperProportions.width,y=n.offset.top,b=y+p.helperProportions.height;for(c=p.snapElements.length-1;c>=0;c--){u=p.snapElements[c].left,a=u+p.snapElements[c].width,f=p.snapElements[c].top,l=f+p.snapElements[c].height;if(!(u-v<m&&m<a+v&&f-v<y&&y<l+v||u-v<m&&m<a+v&&f-v<b&&b<l+v||u-v<g&&g<a+v&&f-v<y&&y<l+v||u-v<g&&g<a+v&&f-v<b&&b<l+v)){p.snapElements[c].snapping&&p.options.snap.release&&p.options.snap.release.call(p.element,t,e.extend(p._uiHash(),{snapItem:p.snapElements[c].item})),p.snapElements[c].snapping=!1;continue}d.snapMode!=="inner"&&(r=Math.abs(f-b)<=v,i=Math.abs(l-y)<=v,s=Math.abs(u-g)<=v,o=Math.abs(a-m)<=v,r&&(n.position.top=p._convertPositionTo("relative",{top:f-p.helperProportions.height,left:0}).top-p.margins.top),i&&(n.position.top=p._convertPositionTo("relative",{top:l,left:0}).top-p.margins.top),s&&(n.position.left=p._convertPositionTo("relative",{top:0,left:u-p.helperProportions.width}).left-p.margins.left),o&&(n.position.left=p._convertPositionTo("relative",{top:0,left:a}).left-p.margins.left)),h=r||i||s||o,d.snapMode!=="outer"&&(r=Math.abs(f-y)<=v,i=Math.abs(l-b)<=v,s=Math.abs(u-m)<=v,o=Math.abs(a-g)<=v,r&&(n.position.top=p._convertPositionTo("relative",{top:f,left:0}).top-p.margins.top),i&&(n.position.top=p._convertPositionTo("relative",{top:l-p.helperProportions.height,left:0}).top-p.margins.top),s&&(n.position.left=p._convertPositionTo("relative",{top:0,left:u}).left-p.margins.left),o&&(n.position.left=p._convertPositionTo("relative",{top:0,left:a-p.helperProportions.width}).left-p.margins.left)),!p.snapElements[c].snapping&&(r||i||s||o||h)&&p.options.snap.snap&&p.options.snap.snap.call(p.element,t,e.extend(p._uiHash(),{snapItem:p.snapElements[c].item})),p.snapElements[c].snapping=r||i||s||o||h}}}),e.ui.plugin.add("draggable","stack",{start:function(){var t,n=this.data("ui-draggable").options,r=e.makeArray(e(n.stack)).sort(function(t,n){return(parseInt(e(t).css("zIndex"),10)||0)-(parseInt(e(n).css("zIndex"),10)||0)});if(!r.length)return;t=parseInt(e(r[0]).css("zIndex"),10)||0,e(r).each(function(n){e(this).css("zIndex",t+n)}),this.css("zIndex",t+r.length)}}),e.ui.plugin.add("draggable","zIndex",{start:function(t,n){var r=e(n.helper),i=e(this).data("ui-draggable").options;r.css("zIndex")&&(i._zIndex=r.css("zIndex")),r.css("zIndex",i.zIndex)},stop:function(t,n){var r=e(this).data("ui-draggable").options;r._zIndex&&e(n.helper).css("zIndex",r._zIndex)}})}(jQuery),function(e,t){function n(e,t,n){return e>t&&e<t+n}e.widget("ui.droppable",{version:"1.10.1",widgetEventPrefix:"drop",options:{accept:"*",activeClass:!1,addClasses:!0,greedy:!1,hoverClass:!1,scope:"default",tolerance:"intersect",activate:null,deactivate:null,drop:null,out:null,over:null},_create:function(){var t=this.options,n=t.accept;this.isover=!1,this.isout=!0,this.accept=e.isFunction(n)?n:function(e){return e.is(n)},this.proportions={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight},e.ui.ddmanager.droppables[t.scope]=e.ui.ddmanager.droppables[t.scope]||[],e.ui.ddmanager.droppables[t.scope].push(this),t.addClasses&&this.element.addClass("ui-droppable")},_destroy:function(){var t=0,n=e.ui.ddmanager.droppables[this.options.scope];for(;t<n.length;t++)n[t]===this&&n.splice(t,1);this.element.removeClass("ui-droppable ui-droppable-disabled")},_setOption:function(t,n){t==="accept"&&(this.accept=e.isFunction(n)?n:function(e){return e.is(n)}),e.Widget.prototype._setOption.apply(this,arguments)},_activate:function(t){var n=e.ui.ddmanager.current;this.options.activeClass&&this.element.addClass(this.options.activeClass),n&&this._trigger("activate",t,this.ui(n))},_deactivate:function(t){var n=e.ui.ddmanager.current;this.options.activeClass&&this.element.removeClass(this.options.activeClass),n&&this._trigger("deactivate",t,this.ui(n))},_over:function(t){var n=e.ui.ddmanager.current;if(!n||(n.currentItem||n.element)[0]===this.element[0])return;this.accept.call(this.element[0],n.currentItem||n.element)&&(this.options.hoverClass&&this.element.addClass(this.options.hoverClass),this._trigger("over",t,this.ui(n)))},_out:function(t){var n=e.ui.ddmanager.current;if(!n||(n.currentItem||n.element)[0]===this.element[0])return;this.accept.call(this.element[0],n.currentItem||n.element)&&(this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("out",t,this.ui(n)))},_drop:function(t,n){var r=n||e.ui.ddmanager.current,i=!1;return!r||(r.currentItem||r.element)[0]===this.element[0]?!1:(this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function(){var t=e.data(this,"ui-droppable");if(t.options.greedy&&!t.options.disabled&&t.options.scope===r.options.scope&&t.accept.call(t.element[0],r.currentItem||r.element)&&e.ui.intersect(r,e.extend(t,{offset:t.element.offset()}),t.options.tolerance))return i=!0,!1}),i?!1:this.accept.call(this.element[0],r.currentItem||r.element)?(this.options.activeClass&&this.element.removeClass(this.options.activeClass),this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("drop",t,this.ui(r)),this.element):!1)},ui:function(e){return{draggable:e.currentItem||e.element,helper:e.helper,position:e.position,offset:e.positionAbs}}}),e.ui.intersect=function(e,t,r){if(!t.offset)return!1;var i,s,o=(e.positionAbs||e.position.absolute).left,u=o+e.helperProportions.width,a=(e.positionAbs||e.position.absolute).top,f=a+e.helperProportions.height,l=t.offset.left,c=l+t.proportions.width,h=t.offset.top,p=h+t.proportions.height;switch(r){case"fit":return l<=o&&u<=c&&h<=a&&f<=p;case"intersect":return l<o+e.helperProportions.width/2&&u-e.helperProportions.width/2<c&&h<a+e.helperProportions.height/2&&f-e.helperProportions.height/2<p;case"pointer":return i=(e.positionAbs||e.position.absolute).left+(e.clickOffset||e.offset.click).left,s=(e.positionAbs||e.position.absolute).top+(e.clickOffset||e.offset.click).top,n(s,h,t.proportions.height)&&n(i,l,t.proportions.width);case"touch":return(a>=h&&a<=p||f>=h&&f<=p||a<h&&f>p)&&(o>=l&&o<=c||u>=l&&u<=c||o<l&&u>c);default:return!1}},e.ui.ddmanager={current:null,droppables:{"default":[]},prepareOffsets:function(t,n){var r,i,s=e.ui.ddmanager.droppables[t.options.scope]||[],o=n?n.type:null,u=(t.currentItem||t.element).find(":data(ui-droppable)").addBack();e:for(r=0;r<s.length;r++){if(s[r].options.disabled||t&&!s[r].accept.call(s[r].element[0],t.currentItem||t.element))continue;for(i=0;i<u.length;i++)if(u[i]===s[r].element[0]){s[r].proportions.height=0;continue e}s[r].visible=s[r].element.css("display")!=="none";if(!s[r].visible)continue;o==="mousedown"&&s[r]._activate.call(s[r],n),s[r].offset=s[r].element.offset(),s[r].proportions={width:s[r].element[0].offsetWidth,height:s[r].element[0].offsetHeight}}},drop:function(t,n){var r=!1;return e.each(e.ui.ddmanager.droppables[t.options.scope]||[],function(){if(!this.options)return;!this.options.disabled&&this.visible&&e.ui.intersect(t,this,this.options.tolerance)&&(r=this._drop.call(this,n)||r),!this.options.disabled&&this.visible&&this.accept.call(this.element[0],t.currentItem||t.element)&&(this.isout=!0,this.isover=!1,this._deactivate.call(this,n))}),r},dragStart:function(t,n){t.element.parentsUntil("body").bind("scroll.droppable",function(){t.options.refreshPositions||e.ui.ddmanager.prepareOffsets(t,n)})},drag:function(t,n){t.options.refreshPositions&&e.ui.ddmanager.prepareOffsets(t,n),e.each(e.ui.ddmanager.droppables[t.options.scope]||[],function(){if(this.options.disabled||this.greedyChild||!this.visible)return;var r,i,s,o=e.ui.intersect(t,this,this.options.tolerance),u=!o&&this.isover?"isout":o&&!this.isover?"isover":null;if(!u)return;this.options.greedy&&(i=this.options.scope,s=this.element.parents(":data(ui-droppable)").filter(function(){return e.data(this,"ui-droppable").options.scope===i}),s.length&&(r=e.data(s[0],"ui-droppable"),r.greedyChild=u==="isover")),r&&u==="isover"&&(r.isover=!1,r.isout=!0,r._out.call(r,n)),this[u]=!0,this[u==="isout"?"isover":"isout"]=!1,this[u==="isover"?"_over":"_out"].call(this,n),r&&u==="isout"&&(r.isout=!1,r.isover=!0,r._over.call(r,n))})},dragStop:function(t,n){t.element.parentsUntil("body").unbind("scroll.droppable"),t.options.refreshPositions||e.ui.ddmanager.prepareOffsets(t,n)}}}(jQuery),function(e,t){function n(e){return parseInt(e,10)||0}function r(e){return!isNaN(parseInt(e,10))}e.widget("ui.resizable",e.ui.mouse,{version:"1.10.1",widgetEventPrefix:"resize",options:{alsoResize:!1,animate:!1,animateDuration:"slow",animateEasing:"swing",aspectRatio:!1,autoHide:!1,containment:!1,ghost:!1,grid:!1,handles:"e,s,se",helper:!1,maxHeight:null,maxWidth:null,minHeight:10,minWidth:10,zIndex:90,resize:null,start:null,stop:null},_create:function(){var t,n,r,i,s,o=this,u=this.options;this.element.addClass("ui-resizable"),e.extend(this,{_aspectRatio:!!u.aspectRatio,aspectRatio:u.aspectRatio,originalElement:this.element,_proportionallyResizeElements:[],_helper:u.helper||u.ghost||u.animate?u.helper||"ui-resizable-helper":null}),this.element[0].nodeName.match(/canvas|textarea|input|select|button|img/i)&&(this.element.wrap(e("<div class='ui-wrapper' style='overflow: hidden;'></div>").css({position:this.element.css("position"),width:this.element.outerWidth(),height:this.element.outerHeight(),top:this.element.css("top"),left:this.element.css("left")})),this.element=this.element.parent().data("ui-resizable",this.element.data("ui-resizable")),this.elementIsWrapper=!0,this.element.css({marginLeft:this.originalElement.css("marginLeft"),marginTop:this.originalElement.css("marginTop"),marginRight:this.originalElement.css("marginRight"),marginBottom:this.originalElement.css("marginBottom")}),this.originalElement.css({marginLeft:0,marginTop:0,marginRight:0,marginBottom:0}),this.originalResizeStyle=this.originalElement.css("resize"),this.originalElement.css("resize","none"),this._proportionallyResizeElements.push(this.originalElement.css({position:"static",zoom:1,display:"block"})),this.originalElement.css({margin:this.originalElement.css("margin")}),this._proportionallyResize()),this.handles=u.handles||(e(".ui-resizable-handle",this.element).length?{n:".ui-resizable-n",e:".ui-resizable-e",s:".ui-resizable-s",w:".ui-resizable-w",se:".ui-resizable-se",sw:".ui-resizable-sw",ne:".ui-resizable-ne",nw:".ui-resizable-nw"}:"e,s,se");if(this.handles.constructor===String){this.handles==="all"&&(this.handles="n,e,s,w,se,sw,ne,nw"),t=this.handles.split(","),this.handles={};for(n=0;n<t.length;n++)r=e.trim(t[n]),s="ui-resizable-"+r,i=e("<div class='ui-resizable-handle "+s+"'></div>"),i.css({zIndex:u.zIndex}),"se"===r&&i.addClass("ui-icon ui-icon-gripsmall-diagonal-se"),this.handles[r]=".ui-resizable-"+r,this.element.append(i)}this._renderAxis=function(t){var n,r,i,s;t=t||this.element;for(n in this.handles){this.handles[n].constructor===String&&(this.handles[n]=e(this.handles[n],this.element).show()),this.elementIsWrapper&&this.originalElement[0].nodeName.match(/textarea|input|select|button/i)&&(r=e(this.handles[n],this.element),s=/sw|ne|nw|se|n|s/.test(n)?r.outerHeight():r.outerWidth(),i=["padding",/ne|nw|n/.test(n)?"Top":/se|sw|s/.test(n)?"Bottom":/^e$/.test(n)?"Right":"Left"].join(""),t.css(i,s),this._proportionallyResize());if(!e(this.handles[n]).length)continue}},this._renderAxis(this.element),this._handles=e(".ui-resizable-handle",this.element).disableSelection(),this._handles.mouseover(function(){o.resizing||(this.className&&(i=this.className.match(/ui-resizable-(se|sw|ne|nw|n|e|s|w)/i)),o.axis=i&&i[1]?i[1]:"se")}),u.autoHide&&(this._handles.hide(),e(this.element).addClass("ui-resizable-autohide").mouseenter(function(){if(u.disabled)return;e(this).removeClass("ui-resizable-autohide"),o._handles.show()}).mouseleave(function(){if(u.disabled)return;o.resizing||(e(this).addClass("ui-resizable-autohide"),o._handles.hide())})),this._mouseInit()},_destroy:function(){this._mouseDestroy();var t,n=function(t){e(t).removeClass("ui-resizable ui-resizable-disabled ui-resizable-resizing").removeData("resizable").removeData("ui-resizable").unbind(".resizable").find(".ui-resizable-handle").remove()};return this.elementIsWrapper&&(n(this.element),t=this.element,this.originalElement.css({position:t.css("position"),width:t.outerWidth(),height:t.outerHeight(),top:t.css("top"),left:t.css("left")}).insertAfter(t),t.remove()),this.originalElement.css("resize",this.originalResizeStyle),n(this.originalElement),this},_mouseCapture:function(t){var n,r,i=!1;for(n in this.handles){r=e(this.handles[n])[0];if(r===t.target||e.contains(r,t.target))i=!0}return!this.options.disabled&&i},_mouseStart:function(t){var r,i,s,o=this.options,u=this.element.position(),a=this.element;return this.resizing=!0,/absolute/.test(a.css("position"))?a.css({position:"absolute",top:a.css("top"),left:a.css("left")}):a.is(".ui-draggable")&&a.css({position:"absolute",top:u.top,left:u.left}),this._renderProxy(),r=n(this.helper.css("left")),i=n(this.helper.css("top")),o.containment&&(r+=e(o.containment).scrollLeft()||0,i+=e(o.containment).scrollTop()||0),this.offset=this.helper.offset(),this.position={left:r,top:i},this.size=this._helper?{width:a.outerWidth(),height:a.outerHeight()}:{width:a.width(),height:a.height()},this.originalSize=this._helper?{width:a.outerWidth(),height:a.outerHeight()}:{width:a.width(),height:a.height()},this.originalPosition={left:r,top:i},this.sizeDiff={width:a.outerWidth()-a.width(),height:a.outerHeight()-a.height()},this.originalMousePosition={left:t.pageX,top:t.pageY},this.aspectRatio=typeof o.aspectRatio=="number"?o.aspectRatio:this.originalSize.width/this.originalSize.height||1,s=e(".ui-resizable-"+this.axis).css("cursor"),e("body").css("cursor",s==="auto"?this.axis+"-resize":s),a.addClass("ui-resizable-resizing"),this._propagate("start",t),!0},_mouseDrag:function(t){var n,r=this.helper,i={},s=this.originalMousePosition,o=this.axis,u=this.position.top,a=this.position.left,f=this.size.width,l=this.size.height,c=t.pageX-s.left||0,h=t.pageY-s.top||0,p=this._change[o];if(!p)return!1;n=p.apply(this,[t,c,h]),this._updateVirtualBoundaries(t.shiftKey);if(this._aspectRatio||t.shiftKey)n=this._updateRatio(n,t);return n=this._respectSize(n,t),this._updateCache(n),this._propagate("resize",t),this.position.top!==u&&(i.top=this.position.top+"px"),this.position.left!==a&&(i.left=this.position.left+"px"),this.size.width!==f&&(i.width=this.size.width+"px"),this.size.height!==l&&(i.height=this.size.height+"px"),r.css(i),!this._helper&&this._proportionallyResizeElements.length&&this._proportionallyResize(),e.isEmptyObject(i)||this._trigger("resize",t,this.ui()),!1},_mouseStop:function(t){this.resizing=!1;var n,r,i,s,o,u,a,f=this.options,l=this;return this._helper&&(n=this._proportionallyResizeElements,r=n.length&&/textarea/i.test(n[0].nodeName),i=r&&e.ui.hasScroll(n[0],"left")?0:l.sizeDiff.height,s=r?0:l.sizeDiff.width,o={width:l.helper.width()-s,height:l.helper.height()-i},u=parseInt(l.element.css("left"),10)+(l.position.left-l.originalPosition.left)||null,a=parseInt(l.element.css("top"),10)+(l.position.top-l.originalPosition.top)||null,f.animate||this.element.css(e.extend(o,{top:a,left:u})),l.helper.height(l.size.height),l.helper.width(l.size.width),this._helper&&!f.animate&&this._proportionallyResize()),e("body").css("cursor","auto"),this.element.removeClass("ui-resizable-resizing"),this._propagate("stop",t),this._helper&&this.helper.remove(),!1},_updateVirtualBoundaries:function(e){var t,n,i,s,o,u=this.options;o={minWidth:r(u.minWidth)?u.minWidth:0,maxWidth:r(u.maxWidth)?u.maxWidth:Infinity,minHeight:r(u.minHeight)?u.minHeight:0,maxHeight:r(u.maxHeight)?u.maxHeight:Infinity};if(this._aspectRatio||e)t=o.minHeight*this.aspectRatio,i=o.minWidth/this.aspectRatio,n=o.maxHeight*this.aspectRatio,s=o.maxWidth/this.aspectRatio,t>o.minWidth&&(o.minWidth=t),i>o.minHeight&&(o.minHeight=i),n<o.maxWidth&&(o.maxWidth=n),s<o.maxHeight&&(o.maxHeight=s);this._vBoundaries=o},_updateCache:function(e){this.offset=this.helper.offset(),r(e.left)&&(this.position.left=e.left),r(e.top)&&(this.position.top=e.top),r(e.height)&&(this.size.height=e.height),r(e.width)&&(this.size.width=e.width)},_updateRatio:function(e){var t=this.position,n=this.size,i=this.axis;return r(e.height)?e.width=e.height*this.aspectRatio:r(e.width)&&(e.height=e.width/this.aspectRatio),i==="sw"&&(e.left=t.left+(n.width-e.width),e.top=null),i==="nw"&&(e.top=t.top+(n.height-e.height),e.left=t.left+(n.width-e.width)),e},_respectSize:function(e){var t=this._vBoundaries,n=this.axis,i=r(e.width)&&t.maxWidth&&t.maxWidth<e.width,s=r(e.height)&&t.maxHeight&&t.maxHeight<e.height,o=r(e.width)&&t.minWidth&&t.minWidth>e.width,u=r(e.height)&&t.minHeight&&t.minHeight>e.height,a=this.originalPosition.left+this.originalSize.width,f=this.position.top+this.size.height,l=/sw|nw|w/.test(n),c=/nw|ne|n/.test(n);return o&&(e.width=t.minWidth),u&&(e.height=t.minHeight),i&&(e.width=t.maxWidth),s&&(e.height=t.maxHeight),o&&l&&(e.left=a-t.minWidth),i&&l&&(e.left=a-t.maxWidth),u&&c&&(e.top=f-t.minHeight),s&&c&&(e.top=f-t.maxHeight),!e.width&&!e.height&&!e.left&&e.top?e.top=null:!e.width&&!e.height&&!e.top&&e.left&&(e.left=null),e},_proportionallyResize:function(){if(!this._proportionallyResizeElements.length)return;var e,t,n,r,i,s=this.helper||this.element;for(e=0;e<this._proportionallyResizeElements.length;e++){i=this._proportionallyResizeElements[e];if(!this.borderDif){this.borderDif=[],n=[i.css("borderTopWidth"),i.css("borderRightWidth"),i.css("borderBottomWidth"),i.css("borderLeftWidth")],r=[i.css("paddingTop"),i.css("paddingRight"),i.css("paddingBottom"),i.css("paddingLeft")];for(t=0;t<n.length;t++)this.borderDif[t]=(parseInt(n[t],10)||0)+(parseInt(r[t],10)||0)}i.css({height:s.height()-this.borderDif[0]-this.borderDif[2]||0,width:s.width()-this.borderDif[1]-this.borderDif[3]||0})}},_renderProxy:function(){var t=this.element,n=this.options;this.elementOffset=t.offset(),this._helper?(this.helper=this.helper||e("<div style='overflow:hidden;'></div>"),this.helper.addClass(this._helper).css({width:this.element.outerWidth()-1,height:this.element.outerHeight()-1,position:"absolute",left:this.elementOffset.left+"px",top:this.elementOffset.top+"px",zIndex:++n.zIndex}),this.helper.appendTo("body").disableSelection()):this.helper=this.element},_change:{e:function(e,t){return{width:this.originalSize.width+t}},w:function(e,t){var n=this.originalSize,r=this.originalPosition;return{left:r.left+t,width:n.width-t}},n:function(e,t,n){var r=this.originalSize,i=this.originalPosition;return{top:i.top+n,height:r.height-n}},s:function(e,t,n){return{height:this.originalSize.height+n}},se:function(t,n,r){return e.extend(this._change.s.apply(this,arguments),this._change.e.apply(this,[t,n,r]))},sw:function(t,n,r){return e.extend(this._change.s.apply(this,arguments),this._change.w.apply(this,[t,n,r]))},ne:function(t,n,r){return e.extend(this._change.n.apply(this,arguments),this._change.e.apply(this,[t,n,r]))},nw:function(t,n,r){return e.extend(this._change.n.apply(this,arguments),this._change.w.apply(this,[t,n,r]))}},_propagate:function(t,n){e.ui.plugin.call(this,t,[n,this.ui()]),t!=="resize"&&this._trigger(t,n,this.ui())},plugins:{},ui:function(){return{originalElement:this.originalElement,element:this.element,helper:this.helper,position:this.position,size:this.size,originalSize:this.originalSize,originalPosition:this.originalPosition}}}),e.ui.plugin.add("resizable","animate",{stop:function(t){var n=e(this).data("ui-resizable"),r=n.options,i=n._proportionallyResizeElements,s=i.length&&/textarea/i.test(i[0].nodeName),o=s&&e.ui.hasScroll(i[0],"left")?0:n.sizeDiff.height,u=s?0:n.sizeDiff.width,a={width:n.size.width-u,height:n.size.height-o},f=parseInt(n.element.css("left"),10)+(n.position.left-n.originalPosition.left)||null,l=parseInt(n.element.css("top"),10)+(n.position.top-n.originalPosition.top)||null;n.element.animate(e.extend(a,l&&f?{top:l,left:f}:{}),{duration:r.animateDuration,easing:r.animateEasing,step:function(){var r={width:parseInt(n.element.css("width"),10),height:parseInt(n.element.css("height"),10),top:parseInt(n.element.css("top"),10),left:parseInt(n.element.css("left"),10)};i&&i.length&&e(i[0]).css({width:r.width,height:r.height}),n._updateCache(r),n._propagate("resize",t)}})}}),e.ui.plugin.add("resizable","containment",{start:function(){var t,r,i,s,o,u,a,f=e(this).data("ui-resizable"),l=f.options,c=f.element,h=l.containment,p=h instanceof e?h.get(0):/parent/.test(h)?c.parent().get(0):h;if(!p)return;f.containerElement=e(p),/document/.test(h)||h===document?(f.containerOffset={left:0,top:0},f.containerPosition={left:0,top:0},f.parentData={element:e(document),left:0,top:0,width:e(document).width(),height:e(document).height()||document.body.parentNode.scrollHeight}):(t=e(p),r=[],e(["Top","Right","Left","Bottom"]).each(function(e,i){r[e]=n(t.css("padding"+i))}),f.containerOffset=t.offset(),f.containerPosition=t.position(),f.containerSize={height:t.innerHeight()-r[3],width:t.innerWidth()-r[1]},i=f.containerOffset,s=f.containerSize.height,o=f.containerSize.width,u=e.ui.hasScroll(p,"left")?p.scrollWidth:o,a=e.ui.hasScroll(p)?p.scrollHeight:s,f.parentData={element:p,left:i.left,top:i.top,width:u,height:a})},resize:function(t){var n,r,i,s,o=e(this).data("ui-resizable"),u=o.options,a=o.containerOffset,f=o.position,l=o._aspectRatio||t.shiftKey,c={top:0,left:0},h=o.containerElement;h[0]!==document&&/static/.test(h.css("position"))&&(c=a),f.left<(o._helper?a.left:0)&&(o.size.width=o.size.width+(o._helper?o.position.left-a.left:o.position.left-c.left),l&&(o.size.height=o.size.width/o.aspectRatio),o.position.left=u.helper?a.left:0),f.top<(o._helper?a.top:0)&&(o.size.height=o.size.height+(o._helper?o.position.top-a.top:o.position.top),l&&(o.size.width=o.size.height*o.aspectRatio),o.position.top=o._helper?a.top:0),o.offset.left=o.parentData.left+o.position.left,o.offset.top=o.parentData.top+o.position.top,n=Math.abs((o._helper?o.offset.left-c.left:o.offset.left-c.left)+o.sizeDiff.width),r=Math.abs((o._helper?o.offset.top-c.top:o.offset.top-a.top)+o.sizeDiff.height),i=o.containerElement.get(0)===o.element.parent().get(0),s=/relative|absolute/.test(o.containerElement.css("position")),i&&s&&(n-=o.parentData.left),n+o.size.width>=o.parentData.width&&(o.size.width=o.parentData.width-n,l&&(o.size.height=o.size.width/o.aspectRatio)),r+o.size.height>=o.parentData.height&&(o.size.height=o.parentData.height-r,l&&(o.size.width=o.size.height*o.aspectRatio))},stop:function(){var t=e(this).data("ui-resizable"),n=t.options,r=t.containerOffset,i=t.containerPosition,s=t.containerElement,o=e(t.helper),u=o.offset(),a=o.outerWidth()-t.sizeDiff.width,f=o.outerHeight()-t.sizeDiff.height;t._helper&&!n.animate&&/relative/.test(s.css("position"))&&e(this).css({left:u.left-i.left-r.left,width:a,height:f}),t._helper&&!n.animate&&/static/.test(s.css("position"))&&e(this).css({left:u.left-i.left-r.left,width:a,height:f})}}),e.ui.plugin.add("resizable","alsoResize",{start:function(){var t=e(this).data("ui-resizable"),n=t.options,r=function(t){e(t).each(function(){var t=e(this);t.data("ui-resizable-alsoresize",{width:parseInt(t.width(),10),height:parseInt(t.height(),10),left:parseInt(t.css("left"),10),top:parseInt(t.css("top"),10)})})};typeof n.alsoResize=="object"&&!n.alsoResize.parentNode?n.alsoResize.length?(n.alsoResize=n.alsoResize[0],r(n.alsoResize)):e.each(n.alsoResize,function(e){r(e)}):r(n.alsoResize)},resize:function(t,n){var r=e(this).data("ui-resizable"),i=r.options,s=r.originalSize,o=r.originalPosition,u={height:r.size.height-s.height||0,width:r.size.width-s.width||0,top:r.position.top-o.top||0,left:r.position.left-o.left||0},a=function(t,r){e(t).each(function(){var t=e(this),i=e(this).data("ui-resizable-alsoresize"),s={},o=r&&r.length?r:t.parents(n.originalElement[0]).length?["width","height"]:["width","height","top","left"];e.each(o,function(e,t){var n=(i[t]||0)+(u[t]||0);n&&n>=0&&(s[t]=n||null)}),t.css(s)})};typeof i.alsoResize=="object"&&!i.alsoResize.nodeType?e.each(i.alsoResize,function(e,t){a(e,t)}):a(i.alsoResize)},stop:function(){e(this).removeData("resizable-alsoresize")}}),e.ui.plugin.add("resizable","ghost",{start:function(){var t=e(this).data("ui-resizable"),n=t.options,r=t.size;t.ghost=t.originalElement.clone(),t.ghost.css({opacity:.25,display:"block",position:"relative",height:r.height,width:r.width,margin:0,left:0,top:0}).addClass("ui-resizable-ghost").addClass(typeof n.ghost=="string"?n.ghost:""),t.ghost.appendTo(t.helper)},resize:function(){var t=e(this).data("ui-resizable");t.ghost&&t.ghost.css({position:"relative",height:t.size.height,width:t.size.width})},stop:function(){var t=e(this).data("ui-resizable");t.ghost&&t.helper&&t.helper.get(0).removeChild(t.ghost.get(0))}}),e.ui.plugin.add("resizable","grid",{resize:function(){var t=e(this).data("ui-resizable"),n=t.options,r=t.size,i=t.originalSize,s=t.originalPosition,o=t.axis,u=typeof n.grid=="number"?[n.grid,n.grid]:n.grid,a=u[0]||1,f=u[1]||1,l=Math.round((r.width-i.width)/a)*a,c=Math.round((r.height-i.height)/f)*f,h=i.width+l,p=i.height+c,d=n.maxWidth&&n.maxWidth<h,v=n.maxHeight&&n.maxHeight<p,m=n.minWidth&&n.minWidth>h,g=n.minHeight&&n.minHeight>p;n.grid=u,m&&(h+=a),g&&(p+=f),d&&(h-=a),v&&(p-=f),/^(se|s|e)$/.test(o)?(t.size.width=h,t.size.height=p):/^(ne)$/.test(o)?(t.size.width=h,t.size.height=p,t.position.top=s.top-c):/^(sw)$/.test(o)?(t.size.width=h,t.size.height=p,t.position.left=s.left-l):(t.size.width=h,t.size.height=p,t.position.top=s.top-c,t.position.left=s.left-l)}})}(jQuery),function(e,t){e.widget("ui.selectable",e.ui.mouse,{version:"1.10.1",options:{appendTo:"body",autoRefresh:!0,distance:0,filter:"*",tolerance:"touch",selected:null,selecting:null,start:null,stop:null,unselected:null,unselecting:null},_create:function(){var t,n=this;this.element.addClass("ui-selectable"),this.dragged=!1,this.refresh=function(){t=e(n.options.filter,n.element[0]),t.addClass("ui-selectee"),t.each(function(){var t=e(this),n=t.offset();e.data(this,"selectable-item",{element:this,$element:t,left:n.left,top:n.top,right:n.left+t.outerWidth(),bottom:n.top+t.outerHeight(),startselected:!1,selected:t.hasClass("ui-selected"),selecting:t.hasClass("ui-selecting"),unselecting:t.hasClass("ui-unselecting")})})},this.refresh(),this.selectees=t.addClass("ui-selectee"),this._mouseInit(),this.helper=e("<div class='ui-selectable-helper'></div>")},_destroy:function(){this.selectees.removeClass("ui-selectee").removeData("selectable-item"),this.element.removeClass("ui-selectable ui-selectable-disabled"),this._mouseDestroy()},_mouseStart:function(t){var n=this,r=this.options;this.opos=[t.pageX,t.pageY];if(this.options.disabled)return;this.selectees=e(r.filter,this.element[0]),this._trigger("start",t),e(r.appendTo).append(this.helper),this.helper.css({left:t.pageX,top:t.pageY,width:0,height:0}),r.autoRefresh&&this.refresh(),this.selectees.filter(".ui-selected").each(function(){var r=e.data(this,"selectable-item");r.startselected=!0,!t.metaKey&&!t.ctrlKey&&(r.$element.removeClass("ui-selected"),r.selected=!1,r.$element.addClass("ui-unselecting"),r.unselecting=!0,n._trigger("unselecting",t,{unselecting:r.element}))}),e(t.target).parents().addBack().each(function(){var r,i=e.data(this,"selectable-item");if(i)return r=!t.metaKey&&!t.ctrlKey||!i.$element.hasClass("ui-selected"),i.$element.removeClass(r?"ui-unselecting":"ui-selected").addClass(r?"ui-selecting":"ui-unselecting"),i.unselecting=!r,i.selecting=r,i.selected=r,r?n._trigger("selecting",t,{selecting:i.element}):n._trigger("unselecting",t,{unselecting:i.element}),!1})},_mouseDrag:function(t){this.dragged=!0;if(this.options.disabled)return;var n,r=this,i=this.options,s=this.opos[0],o=this.opos[1],u=t.pageX,a=t.pageY;return s>u&&(n=u,u=s,s=n),o>a&&(n=a,a=o,o=n),this.helper.css({left:s,top:o,width:u-s,height:a-o}),this.selectees.each(function(){var n=e.data(this,"selectable-item"),f=!1;if(!n||n.element===r.element[0])return;i.tolerance==="touch"?f=!(n.left>u||n.right<s||n.top>a||n.bottom<o):i.tolerance==="fit"&&(f=n.left>s&&n.right<u&&n.top>o&&n.bottom<a),f?(n.selected&&(n.$element.removeClass("ui-selected"),n.selected=!1),n.unselecting&&(n.$element.removeClass("ui-unselecting"),n.unselecting=!1),n.selecting||(n.$element.addClass("ui-selecting"),n.selecting=!0,r._trigger("selecting",t,{selecting:n.element}))):(n.selecting&&((t.metaKey||t.ctrlKey)&&n.startselected?(n.$element.removeClass("ui-selecting"),n.selecting=!1,n.$element.addClass("ui-selected"),n.selected=!0):(n.$element.removeClass("ui-selecting"),n.selecting=!1,n.startselected&&(n.$element.addClass("ui-unselecting"),n.unselecting=!0),r._trigger("unselecting",t,{unselecting:n.element}))),n.selected&&!t.metaKey&&!t.ctrlKey&&!n.startselected&&(n.$element.removeClass("ui-selected"),n.selected=!1,n.$element.addClass("ui-unselecting"),n.unselecting=!0,r._trigger("unselecting",t,{unselecting:n.element})))}),!1},_mouseStop:function(t){var n=this;return this.dragged=!1,e(".ui-unselecting",this.element[0]).each(function(){var r=e.data(this,"selectable-item");r.$element.removeClass("ui-unselecting"),r.unselecting=!1,r.startselected=!1,n._trigger("unselected",t,{unselected:r.element})}),e(".ui-selecting",this.element[0]).each(function(){var r=e.data(this,"selectable-item");r.$element.removeClass("ui-selecting").addClass("ui-selected"),r.selecting=!1,r.selected=!0,r.startselected=!0,n._trigger("selected",t,{selected:r.element})}),this._trigger("stop",t),this.helper.remove(),!1}})}(jQuery),function(e,t){function n(e,t,n){return e>t&&e<t+n}e.widget("ui.sortable",e.ui.mouse,{version:"1.10.1",widgetEventPrefix:"sort",ready:!1,options:{appendTo:"parent",axis:!1,connectWith:!1,containment:!1,cursor:"auto",cursorAt:!1,dropOnEmpty:!0,forcePlaceholderSize:!1,forceHelperSize:!1,grid:!1,handle:!1,helper:"original",items:"> *",opacity:!1,placeholder:!1,revert:!1,scroll:!0,scrollSensitivity:20,scrollSpeed:20,scope:"default",tolerance:"intersect",zIndex:1e3,activate:null,beforeStop:null,change:null,deactivate:null,out:null,over:null,receive:null,remove:null,sort:null,start:null,stop:null,update:null},_create:function(){var e=this.options;this.containerCache={},this.element.addClass("ui-sortable"),this.refresh(),this.floating=this.items.length?e.axis==="x"||/left|right/.test(this.items[0].item.css("float"))||/inline|table-cell/.test(this.items[0].item.css("display")):!1,this.offset=this.element.offset(),this._mouseInit(),this.ready=!0},_destroy:function(){this.element.removeClass("ui-sortable ui-sortable-disabled"),this._mouseDestroy();for(var e=this.items.length-1;e>=0;e--)this.items[e].item.removeData(this.widgetName+"-item");return this},_setOption:function(t,n){t==="disabled"?(this.options[t]=n,this.widget().toggleClass("ui-sortable-disabled",!!n)):e.Widget.prototype._setOption.apply(this,arguments)},_mouseCapture:function(t,n){var r=null,i=!1,s=this;if(this.reverting)return!1;if(this.options.disabled||this.options.type==="static")return!1;this._refreshItems(t),e(t.target).parents().each(function(){if(e.data(this,s.widgetName+"-item")===s)return r=e(this),!1}),e.data(t.target,s.widgetName+"-item")===s&&(r=e(t.target));if(!r)return!1;if(this.options.handle&&!n){e(this.options.handle,r).find("*").addBack().each(function(){this===t.target&&(i=!0)});if(!i)return!1}return this.currentItem=r,this._removeCurrentsFromItems(),!0},_mouseStart:function(t,n,r){var i,s=this.options;this.currentContainer=this,this.refreshPositions(),this.helper=this._createHelper(t),this._cacheHelperProportions(),this._cacheMargins(),this.scrollParent=this.helper.scrollParent(),this.offset=this.currentItem.offset(),this.offset={top:this.offset.top-this.margins.top,left:this.offset.left-this.margins.left},e.extend(this.offset,{click:{left:t.pageX-this.offset.left,top:t.pageY-this.offset.top},parent:this._getParentOffset(),relative:this._getRelativeOffset()}),this.helper.css("position","absolute"),this.cssPosition=this.helper.css("position"),this.originalPosition=this._generatePosition(t),this.originalPageX=t.pageX,this.originalPageY=t.pageY,s.cursorAt&&this._adjustOffsetFromHelper(s.cursorAt),this.domPosition={prev:this.currentItem.prev()[0],parent:this.currentItem.parent()[0]},this.helper[0]!==this.currentItem[0]&&this.currentItem.hide(),this._createPlaceholder(),s.containment&&this._setContainment(),s.cursor&&(e("body").css("cursor")&&(this._storedCursor=e("body").css("cursor")),e("body").css("cursor",s.cursor)),s.opacity&&(this.helper.css("opacity")&&(this._storedOpacity=this.helper.css("opacity")),this.helper.css("opacity",s.opacity)),s.zIndex&&(this.helper.css("zIndex")&&(this._storedZIndex=this.helper.css("zIndex")),this.helper.css("zIndex",s.zIndex)),this.scrollParent[0]!==document&&this.scrollParent[0].tagName!=="HTML"&&(this.overflowOffset=this.scrollParent.offset()),this._trigger("start",t,this._uiHash()),this._preserveHelperProportions||this._cacheHelperProportions();if(!r)for(i=this.containers.length-1;i>=0;i--)this.containers[i]._trigger("activate",t,this._uiHash(this));return e.ui.ddmanager&&(e.ui.ddmanager.current=this),e.ui.ddmanager&&!s.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t),this.dragging=!0,this.helper.addClass("ui-sortable-helper"),this._mouseDrag(t),!0},_mouseDrag:function(t){var n,r,i,s,o=this.options,u=!1;this.position=this._generatePosition(t),this.positionAbs=this._convertPositionTo("absolute"),this.lastPositionAbs||(this.lastPositionAbs=this.positionAbs),this.options.scroll&&(this.scrollParent[0]!==document&&this.scrollParent[0].tagName!=="HTML"?(this.overflowOffset.top+this.scrollParent[0].offsetHeight-t.pageY<o.scrollSensitivity?this.scrollParent[0].scrollTop=u=this.scrollParent[0].scrollTop+o.scrollSpeed:t.pageY-this.overflowOffset.top<o.scrollSensitivity&&(this.scrollParent[0].scrollTop=u=this.scrollParent[0].scrollTop-o.scrollSpeed),this.overflowOffset.left+this.scrollParent[0].offsetWidth-t.pageX<o.scrollSensitivity?this.scrollParent[0].scrollLeft=u=this.scrollParent[0].scrollLeft+o.scrollSpeed:t.pageX-this.overflowOffset.left<o.scrollSensitivity&&(this.scrollParent[0].scrollLeft=u=this.scrollParent[0].scrollLeft-o.scrollSpeed)):(t.pageY-e(document).scrollTop()<o.scrollSensitivity?u=e(document).scrollTop(e(document).scrollTop()-o.scrollSpeed):e(window).height()-(t.pageY-e(document).scrollTop())<o.scrollSensitivity&&(u=e(document).scrollTop(e(document).scrollTop()+o.scrollSpeed)),t.pageX-e(document).scrollLeft()<o.scrollSensitivity?u=e(document).scrollLeft(e(document).scrollLeft()-o.scrollSpeed):e(window).width()-(t.pageX-e(document).scrollLeft())<o.scrollSensitivity&&(u=e(document).scrollLeft(e(document).scrollLeft()+o.scrollSpeed))),u!==!1&&e.ui.ddmanager&&!o.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t)),this.positionAbs=this._convertPositionTo("absolute");if(!this.options.axis||this.options.axis!=="y")this.helper[0].style.left=this.position.left+"px";if(!this.options.axis||this.options.axis!=="x")this.helper[0].style.top=this.position.top+"px";for(n=this.items.length-1;n>=0;n--){r=this.items[n],i=r.item[0],s=this._intersectsWithPointer(r);if(!s)continue;if(r.instance!==this.currentContainer)continue;if(i!==this.currentItem[0]&&this.placeholder[s===1?"next":"prev"]()[0]!==i&&!e.contains(this.placeholder[0],i)&&(this.options.type==="semi-dynamic"?!e.contains(this.element[0],i):!0)){this.direction=s===1?"down":"up";if(this.options.tolerance!=="pointer"&&!this._intersectsWithSides(r))break;this._rearrange(t,r),this._trigger("change",t,this._uiHash());break}}return this._contactContainers(t),e.ui.ddmanager&&e.ui.ddmanager.drag(this,t),this._trigger("sort",t,this._uiHash()),this.lastPositionAbs=this.positionAbs,!1},_mouseStop:function(t,n){if(!t)return;e.ui.ddmanager&&!this.options.dropBehaviour&&e.ui.ddmanager.drop(this,t);if(this.options.revert){var r=this,i=this.placeholder.offset();this.reverting=!0,e(this.helper).animate({left:i.left-this.offset.parent.left-this.margins.left+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollLeft),top:i.top-this.offset.parent.top-this.margins.top+(this.offsetParent[0]===document.body?0:this.offsetParent[0].scrollTop)},parseInt(this.options.revert,10)||500,function(){r._clear(t)})}else this._clear(t,n);return!1},cancel:function(){if(this.dragging){this._mouseUp({target:null}),this.options.helper==="original"?this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper"):this.currentItem.show();for(var t=this.containers.length-1;t>=0;t--)this.containers[t]._trigger("deactivate",null,this._uiHash(this)),this.containers[t].containerCache.over&&(this.containers[t]._trigger("out",null,this._uiHash(this)),this.containers[t].containerCache.over=0)}return this.placeholder&&(this.placeholder[0].parentNode&&this.placeholder[0].parentNode.removeChild(this.placeholder[0]),this.options.helper!=="original"&&this.helper&&this.helper[0].parentNode&&this.helper.remove(),e.extend(this,{helper:null,dragging:!1,reverting:!1,_noFinalSort:null}),this.domPosition.prev?e(this.domPosition.prev).after(this.currentItem):e(this.domPosition.parent).prepend(this.currentItem)),this},serialize:function(t){var n=this._getItemsAsjQuery(t&&t.connected),r=[];return t=t||{},e(n).each(function(){var n=(e(t.item||this).attr(t.attribute||"id")||"").match(t.expression||/(.+)[\-=_](.+)/);n&&r.push((t.key||n[1]+"[]")+"="+(t.key&&t.expression?n[1]:n[2]))}),!r.length&&t.key&&r.push(t.key+"="),r.join("&")},toArray:function(t){var n=this._getItemsAsjQuery(t&&t.connected),r=[];return t=t||{},n.each(function(){r.push(e(t.item||this).attr(t.attribute||"id")||"")}),r},_intersectsWith:function(e){var t=this.positionAbs.left,n=t+this.helperProportions.width,r=this.positionAbs.top,i=r+this.helperProportions.height,s=e.left,o=s+e.width,u=e.top,a=u+e.height,f=this.offset.click.top,l=this.offset.click.left,c=r+f>u&&r+f<a&&t+l>s&&t+l<o;return this.options.tolerance==="pointer"||this.options.forcePointerForContainers||this.options.tolerance!=="pointer"&&this.helperProportions[this.floating?"width":"height"]>e[this.floating?"width":"height"]?c:s<t+this.helperProportions.width/2&&n-this.helperProportions.width/2<o&&u<r+this.helperProportions.height/2&&i-this.helperProportions.height/2<a},_intersectsWithPointer:function(e){var t=this.options.axis==="x"||n(this.positionAbs.top+this.offset.click.top,e.top,e.height),r=this.options.axis==="y"||n(this.positionAbs.left+this.offset.click.left,e.left,e.width),i=t&&r,s=this._getDragVerticalDirection(),o=this._getDragHorizontalDirection();return i?this.floating?o&&o==="right"||s==="down"?2:1:s&&(s==="down"?2:1):!1},_intersectsWithSides:function(e){var t=n(this.positionAbs.top+this.offset.click.top,e.top+e.height/2,e.height),r=n(this.positionAbs.left+this.offset.click.left,e.left+e.width/2,e.width),i=this._getDragVerticalDirection(),s=this._getDragHorizontalDirection();return this.floating&&s?s==="right"&&r||s==="left"&&!r:i&&(i==="down"&&t||i==="up"&&!t)},_getDragVerticalDirection:function(){var e=this.positionAbs.top-this.lastPositionAbs.top;return e!==0&&(e>0?"down":"up")},_getDragHorizontalDirection:function(){var e=this.positionAbs.left-this.lastPositionAbs.left;return e!==0&&(e>0?"right":"left")},refresh:function(e){return this._refreshItems(e),this.refreshPositions(),this},_connectWith:function(){var e=this.options;return e.connectWith.constructor===String?[e.connectWith]:e.connectWith},_getItemsAsjQuery:function(t){var n,r,i,s,o=[],u=[],a=this._connectWith();if(a&&t)for(n=a.length-1;n>=0;n--){i=e(a[n]);for(r=i.length-1;r>=0;r--)s=e.data(i[r],this.widgetFullName),s&&s!==this&&!s.options.disabled&&u.push([e.isFunction(s.options.items)?s.options.items.call(s.element):e(s.options.items,s.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),s])}u.push([e.isFunction(this.options.items)?this.options.items.call(this.element,null,{options:this.options,item:this.currentItem}):e(this.options.items,this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"),this]);for(n=u.length-1;n>=0;n--)u[n][0].each(function(){o.push(this)});return e(o)},_removeCurrentsFromItems:function(){var t=this.currentItem.find(":data("+this.widgetName+"-item)");this.items=e.grep(this.items,function(e){for(var n=0;n<t.length;n++)if(t[n]===e.item[0])return!1;return!0})},_refreshItems:function(t){this.items=[],this.containers=[this];var n,r,i,s,o,u,a,f,l=this.items,c=[[e.isFunction(this.options.items)?this.options.items.call(this.element[0],t,{item:this.currentItem}):e(this.options.items,this.element),this]],h=this._connectWith();if(h&&this.ready)for(n=h.length-1;n>=0;n--){i=e(h[n]);for(r=i.length-1;r>=0;r--)s=e.data(i[r],this.widgetFullName),s&&s!==this&&!s.options.disabled&&(c.push([e.isFunction(s.options.items)?s.options.items.call(s.element[0],t,{item:this.currentItem}):e(s.options.items,s.element),s]),this.containers.push(s))}for(n=c.length-1;n>=0;n--){o=c[n][1],u=c[n][0];for(r=0,f=u.length;r<f;r++)a=e(u[r]),a.data(this.widgetName+"-item",o),l.push({item:a,instance:o,width:0,height:0,left:0,top:0})}},refreshPositions:function(t){this.offsetParent&&this.helper&&(this.offset.parent=this._getParentOffset());var n,r,i,s;for(n=this.items.length-1;n>=0;n--){r=this.items[n];if(r.instance!==this.currentContainer&&this.currentContainer&&r.item[0]!==this.currentItem[0])continue;i=this.options.toleranceElement?e(this.options.toleranceElement,r.item):r.item,t||(r.width=i.outerWidth(),r.height=i.outerHeight()),s=i.offset(),r.left=s.left,r.top=s.top}if(this.options.custom&&this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this);else for(n=this.containers.length-1;n>=0;n--)s=this.containers[n].element.offset(),this.containers[n].containerCache.left=s.left,this.containers[n].containerCache.top=s.top,this.containers[n].containerCache.width=this.containers[n].element.outerWidth(),this.containers[n].containerCache.height=this.containers[n].element.outerHeight();return this},_createPlaceholder:function(t){t=t||this;var n,r=t.options;if(!r.placeholder||r.placeholder.constructor===String)n=r.placeholder,r.placeholder={element:function(){var r=e(document.createElement(t.currentItem[0].nodeName)).addClass(n||t.currentItem[0].className+" ui-sortable-placeholder").removeClass("ui-sortable-helper")[0];return n||(r.style.visibility="hidden"),r},update:function(e,i){if(n&&!r.forcePlaceholderSize)return;i.height()||i.height(t.currentItem.innerHeight()-parseInt(t.currentItem.css("paddingTop")||0,10)-parseInt(t.currentItem.css("paddingBottom")||0,10)),i.width()||i.width(t.currentItem.innerWidth()-parseInt(t.currentItem.css("paddingLeft")||0,10)-parseInt(t.currentItem.css("paddingRight")||0,10))}};t.placeholder=e(r.placeholder.element.call(t.element,t.currentItem)),t.currentItem.after(t.placeholder),r.placeholder.update(t,t.placeholder)},_contactContainers:function(t){var n,r,i,s,o,u,a,f,l,c=null,h=null;for(n=this.containers.length-1;n>=0;n--){if(e.contains(this.currentItem[0],this.containers[n].element[0]))continue;if(this._intersectsWith(this.containers[n].containerCache)){if(c&&e.contains(this.containers[n].element[0],c.element[0]))continue;c=this.containers[n],h=n}else this.containers[n].containerCache.over&&(this.containers[n]._trigger("out",t,this._uiHash(this)),this.containers[n].containerCache.over=0)}if(!c)return;if(this.containers.length===1)this.containers[h]._trigger("over",t,this._uiHash(this)),this.containers[h].containerCache.over=1;else{i=1e4,s=null,o=this.containers[h].floating?"left":"top",u=this.containers[h].floating?"width":"height",a=this.positionAbs[o]+this.offset.click[o];for(r=this.items.length-1;r>=0;r--){if(!e.contains(this.containers[h].element[0],this.items[r].item[0]))continue;if(this.items[r].item[0]===this.currentItem[0])continue;f=this.items[r].item.offset()[o],l=!1,Math.abs(f-a)>Math.abs(f+this.items[r][u]-a)&&(l=!0,f+=this.items[r][u]),Math.abs(f-a)<i&&(i=Math.abs(f-a),s=this.items[r],this.direction=l?"up":"down")}if(!s&&!this.options.dropOnEmpty)return;this.currentContainer=this.containers[h],s?this._rearrange(t,s,null,!0):this._rearrange(t,null,this.containers[h].element,!0),this._trigger("change",t,this._uiHash()),this.containers[h]._trigger("change",t,this._uiHash(this)),this.options.placeholder.update(this.currentContainer,this.placeholder),this.containers[h]._trigger("over",t,this._uiHash(this)),this.containers[h].containerCache.over=1}},_createHelper:function(t){var n=this.options,r=e.isFunction(n.helper)?e(n.helper.apply(this.element[0],[t,this.currentItem])):n.helper==="clone"?this.currentItem.clone():this.currentItem;return r.parents("body").length||e(n.appendTo!=="parent"?n.appendTo:this.currentItem[0].parentNode)[0].appendChild(r[0]),r[0]===this.currentItem[0]&&(this._storedCSS={width:this.currentItem[0].style.width,height:this.currentItem[0].style.height,position:this.currentItem.css("position"),top:this.currentItem.css("top"),left:this.currentItem.css("left")}),(!r[0].style.width||n.forceHelperSize)&&r.width(this.currentItem.width()),(!r[0].style.height||n.forceHelperSize)&&r.height(this.currentItem.height()),r},_adjustOffsetFromHelper:function(t){typeof t=="string"&&(t=t.split(" ")),e.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_getParentOffset:function(){this.offsetParent=this.helper.offsetParent();var t=this.offsetParent.offset();this.cssPosition==="absolute"&&this.scrollParent[0]!==document&&e.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop());if(this.offsetParent[0]===document.body||this.offsetParent[0].tagName&&this.offsetParent[0].tagName.toLowerCase()==="html"&&e.ui.ie)t={top:0,left:0};return{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if(this.cssPosition==="relative"){var e=this.currentItem.position();return{top:e.top-(parseInt(this.helper.css("top"),10)||0)+this.scrollParent.scrollTop(),left:e.left-(parseInt(this.helper.css("left"),10)||0)+this.scrollParent.scrollLeft()}}return{top:0,left:0}},_cacheMargins:function(){this.margins={left:parseInt(this.currentItem.css("marginLeft"),10)||0,top:parseInt(this.currentItem.css("marginTop"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t,n,r,i=this.options;i.containment==="parent"&&(i.containment=this.helper[0].parentNode);if(i.containment==="document"||i.containment==="window")this.containment=[0-this.offset.relative.left-this.offset.parent.left,0-this.offset.relative.top-this.offset.parent.top,e(i.containment==="document"?document:window).width()-this.helperProportions.width-this.margins.left,(e(i.containment==="document"?document:window).height()||document.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top];/^(document|window|parent)$/.test(i.containment)||(t=e(i.containment)[0],n=e(i.containment).offset(),r=e(t).css("overflow")!=="hidden",this.containment=[n.left+(parseInt(e(t).css("borderLeftWidth"),10)||0)+(parseInt(e(t).css("paddingLeft"),10)||0)-this.margins.left,n.top+(parseInt(e(t).css("borderTopWidth"),10)||0)+(parseInt(e(t).css("paddingTop"),10)||0)-this.margins.top,n.left+(r?Math.max(t.scrollWidth,t.offsetWidth):t.offsetWidth)-(parseInt(e(t).css("borderLeftWidth"),10)||0)-(parseInt(e(t).css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left,n.top+(r?Math.max(t.scrollHeight,t.offsetHeight):t.offsetHeight)-(parseInt(e(t).css("borderTopWidth"),10)||0)-(parseInt(e(t).css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top])},_convertPositionTo:function(t,n){n||(n=this.position);var r=t==="absolute"?1:-1,i=this.cssPosition!=="absolute"||this.scrollParent[0]!==document&&!!e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,s=/(html|body)/i.test(i[0].tagName);return{top:n.top+this.offset.relative.top*r+this.offset.parent.top*r-(this.cssPosition==="fixed"?-this.scrollParent.scrollTop():s?0:i.scrollTop())*r,left:n.left+this.offset.relative.left*r+this.offset.parent.left*r-(this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():s?0:i.scrollLeft())*r}},_generatePosition:function(t){var n,r,i=this.options,s=t.pageX,o=t.pageY,u=this.cssPosition!=="absolute"||this.scrollParent[0]!==document&&!!e.contains(this.scrollParent[0],this.offsetParent[0])?this.scrollParent:this.offsetParent,a=/(html|body)/i.test(u[0].tagName);return this.cssPosition==="relative"&&(this.scrollParent[0]===document||this.scrollParent[0]===this.offsetParent[0])&&(this.offset.relative=this._getRelativeOffset()),this.originalPosition&&(this.containment&&(t.pageX-this.offset.click.left<this.containment[0]&&(s=this.containment[0]+this.offset.click.left),t.pageY-this.offset.click.top<this.containment[1]&&(o=this.containment[1]+this.offset.click.top),t.pageX-this.offset.click.left>this.containment[2]&&(s=this.containment[2]+this.offset.click.left),t.pageY-this.offset.click.top>this.containment[3]&&(o=this.containment[3]+this.offset.click.top)),i.grid&&(n=this.originalPageY+Math.round((o-this.originalPageY)/i.grid[1])*i.grid[1],o=this.containment?n-this.offset.click.top>=this.containment[1]&&n-this.offset.click.top<=this.containment[3]?n:n-this.offset.click.top>=this.containment[1]?n-i.grid[1]:n+i.grid[1]:n,r=this.originalPageX+Math.round((s-this.originalPageX)/i.grid[0])*i.grid[0],s=this.containment?r-this.offset.click.left>=this.containment[0]&&r-this.offset.click.left<=this.containment[2]?r:r-this.offset.click.left>=this.containment[0]?r-i.grid[0]:r+i.grid[0]:r)),{top:o-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+(this.cssPosition==="fixed"?-this.scrollParent.scrollTop():a?0:u.scrollTop()),left:s-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+(this.cssPosition==="fixed"?-this.scrollParent.scrollLeft():a?0:u.scrollLeft())}},_rearrange:function(e,t,n,r){n?n[0].appendChild(this.placeholder[0]):t.item[0].parentNode.insertBefore(this.placeholder[0],this.direction==="down"?t.item[0]:t.item[0].nextSibling),this.counter=this.counter?++this.counter:1;var i=this.counter;this._delay(function(){i===this.counter&&this.refreshPositions(!r)})},_clear:function(t,n){this.reverting=!1;var r,i=[];!this._noFinalSort&&this.currentItem.parent().length&&this.placeholder.before(this.currentItem),this._noFinalSort=null;if(this.helper[0]===this.currentItem[0]){for(r in this._storedCSS)if(this._storedCSS[r]==="auto"||this._storedCSS[r]==="static")this._storedCSS[r]="";this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")}else this.currentItem.show();this.fromOutside&&!n&&i.push(function(e){this._trigger("receive",e,this._uiHash(this.fromOutside))}),(this.fromOutside||this.domPosition.prev!==this.currentItem.prev().not(".ui-sortable-helper")[0]||this.domPosition.parent!==this.currentItem.parent()[0])&&!n&&i.push(function(e){this._trigger("update",e,this._uiHash())}),this!==this.currentContainer&&(n||(i.push(function(e){this._trigger("remove",e,this._uiHash())}),i.push(function(e){return function(t){e._trigger("receive",t,this._uiHash(this))}}.call(this,this.currentContainer)),i.push(function(e){return function(t){e._trigger("update",t,this._uiHash(this))}}.call(this,this.currentContainer))));for(r=this.containers.length-1;r>=0;r--)n||i.push(function(e){return function(t){e._trigger("deactivate",t,this._uiHash(this))}}.call(this,this.containers[r])),this.containers[r].containerCache.over&&(i.push(function(e){return function(t){e._trigger("out",t,this._uiHash(this))}}.call(this,this.containers[r])),this.containers[r].containerCache.over=0);this._storedCursor&&e("body").css("cursor",this._storedCursor),this._storedOpacity&&this.helper.css("opacity",this._storedOpacity),this._storedZIndex&&this.helper.css("zIndex",this._storedZIndex==="auto"?"":this._storedZIndex),this.dragging=!1;if(this.cancelHelperRemoval){if(!n){this._trigger("beforeStop",t,this._uiHash());for(r=0;r<i.length;r++)i[r].call(this,t);this._trigger("stop",t,this._uiHash())}return this.fromOutside=!1,!1}n||this._trigger("beforeStop",t,this._uiHash()),this.placeholder[0].parentNode.removeChild(this.placeholder[0]),this.helper[0]!==this.currentItem[0]&&this.helper.remove(),this.helper=null;if(!n){for(r=0;r<i.length;r++)i[r].call(this,t);this._trigger("stop",t,this._uiHash())}return this.fromOutside=!1,!0},_trigger:function(){e.Widget.prototype._trigger.apply(this,arguments)===!1&&this.cancel()},_uiHash:function(t){var n=t||this;return{helper:n.helper,placeholder:n.placeholder||e([]),position:n.position,originalPosition:n.originalPosition,offset:n.positionAbs,item:n.currentItem,sender:t?t.element:null}}})}(jQuery),jQuery.effects||function(e,t){var n="ui-effects-";e.effects={effect:{}},function(e,t){function h(e,t,n){var r=u[t.type]||{};return e==null?n||!t.def?null:t.def:(e=r.floor?~~e:parseFloat(e),isNaN(e)?t.def:r.mod?(e+r.mod)%r.mod:0>e?0:r.max<e?r.max:e)}function p(t){var n=s(),r=n._rgba=[];return t=t.toLowerCase(),c(i,function(e,i){var s,u=i.re.exec(t),a=u&&i.parse(u),f=i.space||"rgba";if(a)return s=n[f](a),n[o[f].cache]=s[o[f].cache],r=n._rgba=s._rgba,!1}),r.length?(r.join()==="0,0,0,0"&&e.extend(r,l.transparent),n):l[t]}function d(e,t,n){return n=(n+1)%1,n*6<1?e+(t-e)*n*6:n*2<1?t:n*3<2?e+(t-e)*(2/3-n)*6:e}var n="backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor",r=/^([\-+])=\s*(\d+\.?\d*)/,i=[{re:/rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(e){return[e[1],e[2],e[3],e[4]]}},{re:/rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,parse:function(e){return[e[1]*2.55,e[2]*2.55,e[3]*2.55,e[4]]}},{re:/#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,parse:function(e){return[parseInt(e[1],16),parseInt(e[2],16),parseInt(e[3],16)]}},{re:/#([a-f0-9])([a-f0-9])([a-f0-9])/,parse:function(e){return[parseInt(e[1]+e[1],16),parseInt(e[2]+e[2],16),parseInt(e[3]+e[3],16)]}},{re:/hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d?(?:\.\d+)?)\s*)?\)/,space:"hsla",parse:function(e){return[e[1],e[2]/100,e[3]/100,e[4]]}}],s=e.Color=function(t,n,r,i){return new e.Color.fn.parse(t,n,r,i)},o={rgba:{props:{red:{idx:0,type:"byte"},green:{idx:1,type:"byte"},blue:{idx:2,type:"byte"}}},hsla:{props:{hue:{idx:0,type:"degrees"},saturation:{idx:1,type:"percent"},lightness:{idx:2,type:"percent"}}}},u={"byte":{floor:!0,max:255},percent:{max:1},degrees:{mod:360,floor:!0}},a=s.support={},f=e("<p>")[0],l,c=e.each;f.style.cssText="background-color:rgba(1,1,1,.5)",a.rgba=f.style.backgroundColor.indexOf("rgba")>-1,c(o,function(e,t){t.cache="_"+e,t.props.alpha={idx:3,type:"percent",def:1}}),s.fn=e.extend(s.prototype,{parse:function(n,r,i,u){if(n===t)return this._rgba=[null,null,null,null],this;if(n.jquery||n.nodeType)n=e(n).css(r),r=t;var a=this,f=e.type(n),d=this._rgba=[];r!==t&&(n=[n,r,i,u],f="array");if(f==="string")return this.parse(p(n)||l._default);if(f==="array")return c(o.rgba.props,function(e,t){d[t.idx]=h(n[t.idx],t)}),this;if(f==="object")return n instanceof s?c(o,function(e,t){n[t.cache]&&(a[t.cache]=n[t.cache].slice())}):c(o,function(t,r){var i=r.cache;c(r.props,function(e,t){if(!a[i]&&r.to){if(e==="alpha"||n[e]==null)return;a[i]=r.to(a._rgba)}a[i][t.idx]=h(n[e],t,!0)}),a[i]&&e.inArray(null,a[i].slice(0,3))<0&&(a[i][3]=1,r.from&&(a._rgba=r.from(a[i])))}),this},is:function(e){var t=s(e),n=!0,r=this;return c(o,function(e,i){var s,o=t[i.cache];return o&&(s=r[i.cache]||i.to&&i.to(r._rgba)||[],c(i.props,function(e,t){if(o[t.idx]!=null)return n=o[t.idx]===s[t.idx],n})),n}),n},_space:function(){var e=[],t=this;return c(o,function(n,r){t[r.cache]&&e.push(n)}),e.pop()},transition:function(e,t){var n=s(e),r=n._space(),i=o[r],a=this.alpha()===0?s("transparent"):this,f=a[i.cache]||i.to(a._rgba),l=f.slice();return n=n[i.cache],c(i.props,function(e,r){var i=r.idx,s=f[i],o=n[i],a=u[r.type]||{};if(o===null)return;s===null?l[i]=o:(a.mod&&(o-s>a.mod/2?s+=a.mod:s-o>a.mod/2&&(s-=a.mod)),l[i]=h((o-s)*t+s,r))}),this[r](l)},blend:function(t){if(this._rgba[3]===1)return this;var n=this._rgba.slice(),r=n.pop(),i=s(t)._rgba;return s(e.map(n,function(e,t){return(1-r)*i[t]+r*e}))},toRgbaString:function(){var t="rgba(",n=e.map(this._rgba,function(e,t){return e==null?t>2?1:0:e});return n[3]===1&&(n.pop(),t="rgb("),t+n.join()+")"},toHslaString:function(){var t="hsla(",n=e.map(this.hsla(),function(e,t){return e==null&&(e=t>2?1:0),t&&t<3&&(e=Math.round(e*100)+"%"),e});return n[3]===1&&(n.pop(),t="hsl("),t+n.join()+")"},toHexString:function(t){var n=this._rgba.slice(),r=n.pop();return t&&n.push(~~(r*255)),"#"+e.map(n,function(e){return e=(e||0).toString(16),e.length===1?"0"+e:e}).join("")},toString:function(){return this._rgba[3]===0?"transparent":this.toRgbaString()}}),s.fn.parse.prototype=s.fn,o.hsla.to=function(e){if(e[0]==null||e[1]==null||e[2]==null)return[null,null,null,e[3]];var t=e[0]/255,n=e[1]/255,r=e[2]/255,i=e[3],s=Math.max(t,n,r),o=Math.min(t,n,r),u=s-o,a=s+o,f=a*.5,l,c;return o===s?l=0:t===s?l=60*(n-r)/u+360:n===s?l=60*(r-t)/u+120:l=60*(t-n)/u+240,u===0?c=0:f<=.5?c=u/a:c=u/(2-a),[Math.round(l)%360,c,f,i==null?1:i]},o.hsla.from=function(e){if(e[0]==null||e[1]==null||e[2]==null)return[null,null,null,e[3]];var t=e[0]/360,n=e[1],r=e[2],i=e[3],s=r<=.5?r*(1+n):r+n-r*n,o=2*r-s;return[Math.round(d(o,s,t+1/3)*255),Math.round(d(o,s,t)*255),Math.round(d(o,s,t-1/3)*255),i]},c(o,function(n,i){var o=i.props,u=i.cache,a=i.to,f=i.from;s.fn[n]=function(n){a&&!this[u]&&(this[u]=a(this._rgba));if(n===t)return this[u].slice();var r,i=e.type(n),l=i==="array"||i==="object"?n:arguments,p=this[u].slice();return c(o,function(e,t){var n=l[i==="object"?e:t.idx];n==null&&(n=p[t.idx]),p[t.idx]=h(n,t)}),f?(r=s(f(p)),r[u]=p,r):s(p)},c(o,function(t,i){if(s.fn[t])return;s.fn[t]=function(s){var o=e.type(s),u=t==="alpha"?this._hsla?"hsla":"rgba":n,a=this[u](),f=a[i.idx],l;return o==="undefined"?f:(o==="function"&&(s=s.call(this,f),o=e.type(s)),s==null&&i.empty?this:(o==="string"&&(l=r.exec(s),l&&(s=f+parseFloat(l[2])*(l[1]==="+"?1:-1))),a[i.idx]=s,this[u](a)))}})}),s.hook=function(t){var n=t.split(" ");c(n,function(t,n){e.cssHooks[n]={set:function(t,r){var i,o,u="";if(r!=="transparent"&&(e.type(r)!=="string"||(i=p(r)))){r=s(i||r);if(!a.rgba&&r._rgba[3]!==1){o=n==="backgroundColor"?t.parentNode:t;while((u===""||u==="transparent")&&o&&o.style)try{u=e.css(o,"backgroundColor"),o=o.parentNode}catch(f){}r=r.blend(u&&u!=="transparent"?u:"_default")}r=r.toRgbaString()}try{t.style[n]=r}catch(f){}}},e.fx.step[n]=function(t){t.colorInit||(t.start=s(t.elem,n),t.end=s(t.end),t.colorInit=!0),e.cssHooks[n].set(t.elem,t.start.transition(t.end,t.pos))}})},s.hook(n),e.cssHooks.borderColor={expand:function(e){var t={};return c(["Top","Right","Bottom","Left"],function(n,r){t["border"+r+"Color"]=e}),t}},l=e.Color.names={aqua:"#00ffff",black:"#000000",blue:"#0000ff",fuchsia:"#ff00ff",gray:"#808080",green:"#008000",lime:"#00ff00",maroon:"#800000",navy:"#000080",olive:"#808000",purple:"#800080",red:"#ff0000",silver:"#c0c0c0",teal:"#008080",white:"#ffffff",yellow:"#ffff00",transparent:[null,null,null,0],_default:"#ffffff"}}(jQuery),function(){function i(t){var n,r,i=t.ownerDocument.defaultView?t.ownerDocument.defaultView.getComputedStyle(t,null):t.currentStyle,s={};if(i&&i.length&&i[0]&&i[i[0]]){r=i.length;while(r--)n=i[r],typeof i[n]=="string"&&(s[e.camelCase(n)]=i[n])}else for(n in i)typeof i[n]=="string"&&(s[n]=i[n]);return s}function s(t,n){var i={},s,o;for(s in n)o=n[s],t[s]!==o&&!r[s]&&(e.fx.step[s]||!isNaN(parseFloat(o)))&&(i[s]=o);return i}var n=["add","remove","toggle"],r={border:1,borderBottom:1,borderColor:1,borderLeft:1,borderRight:1,borderTop:1,borderWidth:1,margin:1,padding:1};e.each(["borderLeftStyle","borderRightStyle","borderBottomStyle","borderTopStyle"],function(t,n){e.fx.step[n]=function(e){if(e.end!=="none"&&!e.setAttr||e.pos===1&&!e.setAttr)jQuery.style(e.elem,n,e.end),e.setAttr=!0}}),e.fn.addBack||(e.fn.addBack=function(e){return this.add(e==null?this.prevObject:this.prevObject.filter(e))}),e.effects.animateClass=function(t,r,o,u){var a=e.speed(r,o,u);return this.queue(function(){var r=e(this),o=r.attr("class")||"",u,f=a.children?r.find("*").addBack():r;f=f.map(function(){var t=e(this);return{el:t,start:i(this)}}),u=function(){e.each(n,function(e,n){t[n]&&r[n+"Class"](t[n])})},u(),f=f.map(function(){return this.end=i(this.el[0]),this.diff=s(this.start,this.end),this}),r.attr("class",o),f=f.map(function(){var t=this,n=e.Deferred(),r=e.extend({},a,{queue:!1,complete:function(){n.resolve(t)}});return this.el.animate(this.diff,r),n.promise()}),e.when.apply(e,f.get()).done(function(){u(),e.each(arguments,function(){var t=this.el;e.each(this.diff,function(e){t.css(e,"")})}),a.complete.call(r[0])})})},e.fn.extend({_addClass:e.fn.addClass,addClass:function(t,n,r,i){return n?e.effects.animateClass.call(this,{add:t},n,r,i):this._addClass(t)},_removeClass:e.fn.removeClass,removeClass:function(t,n,r,i){return arguments.length>1?e.effects.animateClass.call(this,{remove:t},n,r,i):this._removeClass.apply(this,arguments)},_toggleClass:e.fn.toggleClass,toggleClass:function(n,r,i,s,o){return typeof r=="boolean"||r===t?i?e.effects.animateClass.call(this,r?{add:n}:{remove:n},i,s,o):this._toggleClass(n,r):e.effects.animateClass.call(this,{toggle:n},r,i,s)},switchClass:function(t,n,r,i,s){return e.effects.animateClass.call(this,{add:n,remove:t},r,i,s)}})}(),function(){function r(t,n,r,i){e.isPlainObject(t)&&(n=t,t=t.effect),t={effect:t},n==null&&(n={}),e.isFunction(n)&&(i=n,r=null,n={});if(typeof n=="number"||e.fx.speeds[n])i=r,r=n,n={};return e.isFunction(r)&&(i=r,r=null),n&&e.extend(t,n),r=r||n.duration,t.duration=e.fx.off?0:typeof r=="number"?r:r in e.fx.speeds?e.fx.speeds[r]:e.fx.speeds._default,t.complete=i||n.complete,t}function i(t){return!t||typeof t=="number"||e.fx.speeds[t]?!0:typeof t=="string"&&!e.effects.effect[t]}e.extend(e.effects,{version:"1.10.1",save:function(e,t){for(var r=0;r<t.length;r++)t[r]!==null&&e.data(n+t[r],e[0].style[t[r]])},restore:function(e,r){var i,s;for(s=0;s<r.length;s++)r[s]!==null&&(i=e.data(n+r[s]),i===t&&(i=""),e.css(r[s],i))},setMode:function(e,t){return t==="toggle"&&(t=e.is(":hidden")?"show":"hide"),t},getBaseline:function(e,t){var n,r;switch(e[0]){case"top":n=0;break;case"middle":n=.5;break;case"bottom":n=1;break;default:n=e[0]/t.height}switch(e[1]){case"left":r=0;break;case"center":r=.5;break;case"right":r=1;break;default:r=e[1]/t.width}return{x:r,y:n}},createWrapper:function(t){if(t.parent().is(".ui-effects-wrapper"))return t.parent();var n={width:t.outerWidth(!0),height:t.outerHeight(!0),"float":t.css("float")},r=e("<div></div>").addClass("ui-effects-wrapper").css({fontSize:"100%",background:"transparent",border:"none",margin:0,padding:0}),i={width:t.width(),height:t.height()},s=document.activeElement;try{s.id}catch(o){s=document.body}return t.wrap(r),(t[0]===s||e.contains(t[0],s))&&e(s).focus(),r=t.parent(),t.css("position")==="static"?(r.css({position:"relative"}),t.css({position:"relative"})):(e.extend(n,{position:t.css("position"),zIndex:t.css("z-index")}),e.each(["top","left","bottom","right"],function(e,r){n[r]=t.css(r),isNaN(parseInt(n[r],10))&&(n[r]="auto")}),t.css({position:"relative",top:0,left:0,right:"auto",bottom:"auto"})),t.css(i),r.css(n).show()},removeWrapper:function(t){var n=document.activeElement;return t.parent().is(".ui-effects-wrapper")&&(t.parent().replaceWith(t),(t[0]===n||e.contains(t[0],n))&&e(n).focus()),t},setTransition:function(t,n,r,i){return i=i||{},e.each(n,function(e,n){var s=t.cssUnit(n);s[0]>0&&(i[n]=s[0]*r+s[1])}),i}}),e.fn.extend({effect:function(){function o(n){function u(){e.isFunction(i)&&i.call(r[0]),e.isFunction(n)&&n()}var r=e(this),i=t.complete,o=t.mode;(r.is(":hidden")?o==="hide":o==="show")?u():s.call(r[0],t,u)}var t=r.apply(this,arguments),n=t.mode,i=t.queue,s=e.effects.effect[t.effect];return e.fx.off||!s?n?this[n](t.duration,t.complete):this.each(function(){t.complete&&t.complete.call(this)}):i===!1?this.each(o):this.queue(i||"fx",o)},_show:e.fn.show,show:function(e){if(i(e))return this._show.apply(this,arguments);var t=r.apply(this,arguments);return t.mode="show",this.effect.call(this,t)},_hide:e.fn.hide,hide:function(e){if(i(e))return this._hide.apply(this,arguments);var t=r.apply(this,arguments);return t.mode="hide",this.effect.call(this,t)},__toggle:e.fn.toggle,toggle:function(t){if(i(t)||typeof t=="boolean"||e.isFunction(t))return this.__toggle.apply(this,arguments);var n=r.apply(this,arguments);return n.mode="toggle",this.effect.call(this,n)},cssUnit:function(t){var n=this.css(t),r=[];return e.each(["em","px","%","pt"],function(e,t){n.indexOf(t)>0&&(r=[parseFloat(n),t])}),r}})}(),function(){var t={};e.each(["Quad","Cubic","Quart","Quint","Expo"],function(e,n){t[n]=function(t){return Math.pow(t,e+2)}}),e.extend(t,{Sine:function(e){return 1-Math.cos(e*Math.PI/2)},Circ:function(e){return 1-Math.sqrt(1-e*e)},Elastic:function(e){return e===0||e===1?e:-Math.pow(2,8*(e-1))*Math.sin(((e-1)*80-7.5)*Math.PI/15)},Back:function(e){return e*e*(3*e-2)},Bounce:function(e){var t,n=4;while(e<((t=Math.pow(2,--n))-1)/11);return 1/Math.pow(4,3-n)-7.5625*Math.pow((t*3-2)/22-e,2)}}),e.each(t,function(t,n){e.easing["easeIn"+t]=n,e.easing["easeOut"+t]=function(e){return 1-n(1-e)},e.easing["easeInOut"+t]=function(e){return e<.5?n(e*2)/2:1-n(e*-2+2)/2}})}()}(jQuery),function(e,t){var n=0,r={},i={};r.height=r.paddingTop=r.paddingBottom=r.borderTopWidth=r.borderBottomWidth="hide",i.height=i.paddingTop=i.paddingBottom=i.borderTopWidth=i.borderBottomWidth="show",e.widget("ui.accordion",{version:"1.10.1",options:{active:0,animate:{},collapsible:!1,event:"click",header:"> li > :first-child,> :not(li):even",heightStyle:"auto",icons:{activeHeader:"ui-icon-triangle-1-s",header:"ui-icon-triangle-1-e"},activate:null,beforeActivate:null},_create:function(){var t=this.options;this.prevShow=this.prevHide=e(),this.element.addClass("ui-accordion ui-widget ui-helper-reset").attr("role","tablist"),!t.collapsible&&(t.active===!1||t.active==null)&&(t.active=0),this._processPanels(),t.active<0&&(t.active+=this.headers.length),this._refresh()},_getCreateEventData:function(){return{header:this.active,panel:this.active.length?this.active.next():e(),content:this.active.length?this.active.next():e()}},_createIcons:function(){var t=this.options.icons;t&&(e("<span>").addClass("ui-accordion-header-icon ui-icon "+t.header).prependTo(this.headers),this.active.children(".ui-accordion-header-icon").removeClass(t.header).addClass(t.activeHeader),this.headers.addClass("ui-accordion-icons"))},_destroyIcons:function(){this.headers.removeClass("ui-accordion-icons").children(".ui-accordion-header-icon").remove()},_destroy:function(){var e;this.element.removeClass("ui-accordion ui-widget ui-helper-reset").removeAttr("role"),this.headers.removeClass("ui-accordion-header ui-accordion-header-active ui-helper-reset ui-state-default ui-corner-all ui-state-active ui-state-disabled ui-corner-top").removeAttr("role").removeAttr("aria-selected").removeAttr("aria-controls").removeAttr("tabIndex").each(function(){/^ui-accordion/.test(this.id)&&this.removeAttribute("id")}),this._destroyIcons(),e=this.headers.next().css("display","").removeAttr("role").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-labelledby").removeClass("ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content ui-accordion-content-active ui-state-disabled").each(function(){/^ui-accordion/.test(this.id)&&this.removeAttribute("id")}),this.options.heightStyle!=="content"&&e.css("height","")},_setOption:function(e,t){if(e==="active"){this._activate(t);return}e==="event"&&(this.options.event&&this._off(this.headers,this.options.event),this._setupEvents(t)),this._super(e,t),e==="collapsible"&&!t&&this.options.active===!1&&this._activate(0),e==="icons"&&(this._destroyIcons(),t&&this._createIcons()),e==="disabled"&&this.headers.add(this.headers.next()).toggleClass("ui-state-disabled",!!t)},_keydown:function(t){if(t.altKey||t.ctrlKey)return;var n=e.ui.keyCode,r=this.headers.length,i=this.headers.index(t.target),s=!1;switch(t.keyCode){case n.RIGHT:case n.DOWN:s=this.headers[(i+1)%r];break;case n.LEFT:case n.UP:s=this.headers[(i-1+r)%r];break;case n.SPACE:case n.ENTER:this._eventHandler(t);break;case n.HOME:s=this.headers[0];break;case n.END:s=this.headers[r-1]}s&&(e(t.target).attr("tabIndex",-1),e(s).attr("tabIndex",0),s.focus(),t.preventDefault())},_panelKeyDown:function(t){t.keyCode===e.ui.keyCode.UP&&t.ctrlKey&&e(t.currentTarget).prev().focus()},refresh:function(){var t=this.options;this._processPanels();if(t.active===!1&&t.collapsible===!0||!this.headers.length)t.active=!1,this.active=e();t.active===!1?this._activate(0):this.active.length&&!e.contains(this.element[0],this.active[0])?this.headers.length===this.headers.find(".ui-state-disabled").length?(t.active=!1,this.active=e()):this._activate(Math.max(0,t.active-1)):t.active=this.headers.index(this.active),this._destroyIcons(),this._refresh()},_processPanels:function(){this.headers=this.element.find(this.options.header).addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-all"),this.headers.next().addClass("ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom").filter(":not(.ui-accordion-content-active)").hide()},_refresh:function(){var t,r=this.options,i=r.heightStyle,s=this.element.parent(),o=this.accordionId="ui-accordion-"+(this.element.attr("id")||++n);this.active=this._findActive(r.active).addClass("ui-accordion-header-active ui-state-active ui-corner-top").removeClass("ui-corner-all"),this.active.next().addClass("ui-accordion-content-active").show(),this.headers.attr("role","tab").each(function(t){var n=e(this),r=n.attr("id"),i=n.next(),s=i.attr("id");r||(r=o+"-header-"+t,n.attr("id",r)),s||(s=o+"-panel-"+t,i.attr("id",s)),n.attr("aria-controls",s),i.attr("aria-labelledby",r)}).next().attr("role","tabpanel"),this.headers.not(this.active).attr({"aria-selected":"false",tabIndex:-1}).next().attr({"aria-expanded":"false","aria-hidden":"true"}).hide(),this.active.length?this.active.attr({"aria-selected":"true",tabIndex:0}).next().attr({"aria-expanded":"true","aria-hidden":"false"}):this.headers.eq(0).attr("tabIndex",0),this._createIcons(),this._setupEvents(r.event),i==="fill"?(t=s.height(),this.element.siblings(":visible").each(function(){var n=e(this),r=n.css("position");if(r==="absolute"||r==="fixed")return;t-=n.outerHeight(!0)}),this.headers.each(function(){t-=e(this).outerHeight(!0)}),this.headers.next().each(function(){e(this).height(Math.max(0,t-e(this).innerHeight()+e(this).height()))}).css("overflow","auto")):i==="auto"&&(t=0,this.headers.next().each(function(){t=Math.max(t,e(this).css("height","").height())}).height(t))},_activate:function(t){var n=this._findActive(t)[0];if(n===this.active[0])return;n=n||this.active[0],this._eventHandler({target:n,currentTarget:n,preventDefault:e.noop})},_findActive:function(t){return typeof t=="number"?this.headers.eq(t):e()},_setupEvents:function(t){var n={keydown:"_keydown"};t&&e.each(t.split(" "),function(e,t){n[t]="_eventHandler"}),this._off(this.headers.add(this.headers.next())),this._on(this.headers,n),this._on(this.headers.next(),{keydown:"_panelKeyDown"}),this._hoverable(this.headers),this._focusable(this.headers)},_eventHandler:function(t){var n=this.options,r=this.active,i=e(t.currentTarget),s=i[0]===r[0],o=s&&n.collapsible,u=o?e():i.next(),a=r.next(),f={oldHeader:r,oldPanel:a,newHeader:o?e():i,newPanel:u};t.preventDefault();if(s&&!n.collapsible||this._trigger("beforeActivate",t,f)===!1)return;n.active=o?!1:this.headers.index(i),this.active=s?e():i,this._toggle(f),r.removeClass("ui-accordion-header-active ui-state-active"),n.icons&&r.children(".ui-accordion-header-icon").removeClass(n.icons.activeHeader).addClass(n.icons.header),s||(i.removeClass("ui-corner-all").addClass("ui-accordion-header-active ui-state-active ui-corner-top"),n.icons&&i.children(".ui-accordion-header-icon").removeClass(n.icons.header).addClass(n.icons.activeHeader),i.next().addClass("ui-accordion-content-active"))},_toggle:function(t){var n=t.newPanel,r=this.prevShow.length?this.prevShow:t.oldPanel;this.prevShow.add(this.prevHide).stop(!0,!0),this.prevShow=n,this.prevHide=r,this.options.animate?this._animate(n,r,t):(r.hide(),n.show(),this._toggleComplete(t)),r.attr({"aria-expanded":"false","aria-hidden":"true"}),r.prev().attr("aria-selected","false"),n.length&&r.length?r.prev().attr("tabIndex",-1):n.length&&this.headers.filter(function(){return e(this).attr("tabIndex")===0}).attr("tabIndex",-1),n.attr({"aria-expanded":"true","aria-hidden":"false"}).prev().attr({"aria-selected":"true",tabIndex:0})},_animate:function(e,t,n){var s,o,u,a=this,f=0,l=e.length&&(!t.length||e.index()<t.index()),c=this.options.animate||{},h=l&&c.down||c,p=function(){a._toggleComplete(n)};typeof h=="number"&&(u=h),typeof h=="string"&&(o=h),o=o||h.easing||c.easing,u=u||h.duration||c.duration;if(!t.length)return e.animate(i,u,o,p);if(!e.length)return t.animate(r,u,o,p);s=e.show().outerHeight(),t.animate(r,{duration:u,easing:o,step:function(e,t){t.now=Math.round(e)}}),e.hide().animate(i,{duration:u,easing:o,complete:p,step:function(e,n){n.now=Math.round(e),n.prop!=="height"?f+=n.now:a.options.heightStyle!=="content"&&(n.now=Math.round(s-t.outerHeight()-f),f=0)}})},_toggleComplete:function(e){var t=e.oldPanel;t.removeClass("ui-accordion-content-active").prev().removeClass("ui-corner-top").addClass("ui-corner-all"),t.length&&(t.parent()[0].className=t.parent()[0].className),this._trigger("activate",null,e)}})}(jQuery),function(e,t){var n=0;e.widget("ui.autocomplete",{version:"1.10.1",defaultElement:"<input>",options:{appendTo:null,autoFocus:!1,delay:300,minLength:1,position:{my:"left top",at:"left bottom",collision:"none"},source:null,change:null,close:null,focus:null,open:null,response:null,search:null,select:null},pending:0,_create:function(){var t,n,r,i=this.element[0].nodeName.toLowerCase(),s=i==="textarea",o=i==="input";this.isMultiLine=s?!0:o?!1:this.element.prop("isContentEditable"),this.valueMethod=this.element[s||o?"val":"text"],this.isNewMenu=!0,this.element.addClass("ui-autocomplete-input").attr("autocomplete","off"),this._on(this.element,{keydown:function(i){if(this.element.prop("readOnly")){t=!0,r=!0,n=!0;return}t=!1,r=!1,n=!1;var s=e.ui.keyCode;switch(i.keyCode){case s.PAGE_UP:t=!0,this._move("previousPage",i);break;case s.PAGE_DOWN:t=!0,this._move("nextPage",i);break;case s.UP:t=!0,this._keyEvent("previous",i);break;case s.DOWN:t=!0,this._keyEvent("next",i);break;case s.ENTER:case s.NUMPAD_ENTER:this.menu.active&&(t=!0,i.preventDefault(),this.menu.select(i));break;case s.TAB:this.menu.active&&this.menu.select(i);break;case s.ESCAPE:this.menu.element.is(":visible")&&(this._value(this.term),this.close(i),i.preventDefault());break;default:n=!0,this._searchTimeout(i)}},keypress:function(r){if(t){t=!1,r.preventDefault();return}if(n)return;var i=e.ui.keyCode;switch(r.keyCode){case i.PAGE_UP:this._move("previousPage",r);break;case i.PAGE_DOWN:this._move("nextPage",r);break;case i.UP:this._keyEvent("previous",r);break;case i.DOWN:this._keyEvent("next",r)}},input:function(e){if(r){r=!1,e.preventDefault();return}this._searchTimeout(e)},focus:function(){this.selectedItem=null,this.previous=this._value()},blur:function(e){if(this.cancelBlur){delete this.cancelBlur;return}clearTimeout(this.searching),this.close(e),this._change(e)}}),this._initSource(),this.menu=e("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({input:e(),role:null}).hide().data("ui-menu"),this._on(this.menu.element,{mousedown:function(t){t.preventDefault(),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur});var n=this.menu.element[0];e(t.target).closest(".ui-menu-item").length||this._delay(function(){var t=this;this.document.one("mousedown",function(r){r.target!==t.element[0]&&r.target!==n&&!e.contains(n,r.target)&&t.close()})})},menufocus:function(t,n){if(this.isNewMenu){this.isNewMenu=!1;if(t.originalEvent&&/^mouse/.test(t.originalEvent.type)){this.menu.blur(),this.document.one("mousemove",function(){e(t.target).trigger(t.originalEvent)});return}}var r=n.item.data("ui-autocomplete-item");!1!==this._trigger("focus",t,{item:r})?t.originalEvent&&/^key/.test(t.originalEvent.type)&&this._value(r.value):this.liveRegion.text(r.value)},menuselect:function(e,t){var n=t.item.data("ui-autocomplete-item"),r=this.previous;this.element[0]!==this.document[0].activeElement&&(this.element.focus(),this.previous=r,this._delay(function(){this.previous=r,this.selectedItem=n})),!1!==this._trigger("select",e,{item:n})&&this._value(n.value),this.term=this._value(),this.close(e),this.selectedItem=n}}),this.liveRegion=e("<span>",{role:"status","aria-live":"polite"}).addClass("ui-helper-hidden-accessible").insertAfter(this.element),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_destroy:function(){clearTimeout(this.searching),this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete"),this.menu.element.remove(),this.liveRegion.remove()},_setOption:function(e,t){this._super(e,t),e==="source"&&this._initSource(),e==="appendTo"&&this.menu.element.appendTo(this._appendTo()),e==="disabled"&&t&&this.xhr&&this.xhr.abort()},_appendTo:function(){var t=this.options.appendTo;return t&&(t=t.jquery||t.nodeType?e(t):this.document.find(t).eq(0)),t||(t=this.element.closest(".ui-front")),t.length||(t=this.document[0].body),t},_initSource:function(){var t,n,r=this;e.isArray(this.options.source)?(t=this.options.source,this.source=function(n,r){r(e.ui.autocomplete.filter(t,n.term))}):typeof this.options.source=="string"?(n=this.options.source,this.source=function(t,i){r.xhr&&r.xhr.abort(),r.xhr=e.ajax({url:n,data:t,dataType:"json",success:function(e){i(e)},error:function(){i([])}})}):this.source=this.options.source},_searchTimeout:function(e){clearTimeout(this.searching),this.searching=this._delay(function(){this.term!==this._value()&&(this.selectedItem=null,this.search(null,e))},this.options.delay)},search:function(e,t){e=e!=null?e:this._value(),this.term=this._value();if(e.length<this.options.minLength)return this.close(t);if(this._trigger("search",t)===!1)return;return this._search(e)},_search:function(e){this.pending++,this.element.addClass("ui-autocomplete-loading"),this.cancelSearch=!1,this.source({term:e},this._response())},_response:function(){var e=this,t=++n;return function(r){t===n&&e.__response(r),e.pending--,e.pending||e.element.removeClass("ui-autocomplete-loading")}},__response:function(e){e&&(e=this._normalize(e)),this._trigger("response",null,{content:e}),!this.options.disabled&&e&&e.length&&!this.cancelSearch?(this._suggest(e),this._trigger("open")):this._close()},close:function(e){this.cancelSearch=!0,this._close(e)},_close:function(e){this.menu.element.is(":visible")&&(this.menu.element.hide(),this.menu.blur(),this.isNewMenu=!0,this._trigger("close",e))},_change:function(e){this.previous!==this._value()&&this._trigger("change",e,{item:this.selectedItem})},_normalize:function(t){return t.length&&t[0].label&&t[0].value?t:e.map(t,function(t){return typeof t=="string"?{label:t,value:t}:e.extend({label:t.label||t.value,value:t.value||t.label},t)})},_suggest:function(t){var n=this.menu.element.empty();this._renderMenu(n,t),this.menu.refresh(),n.show(),this._resizeMenu(),n.position(e.extend({of:this.element},this.options.position)),this.options.autoFocus&&this.menu.next()},_resizeMenu:function(){var e=this.menu.element;e.outerWidth(Math.max(e.width("").outerWidth()+1,this.element.outerWidth()))},_renderMenu:function(t,n){var r=this;e.each(n,function(e,n){r._renderItemData(t,n)})},_renderItemData:function(e,t){return this._renderItem(e,t).data("ui-autocomplete-item",t)},_renderItem:function(t,n){return e("<li>").append(e("<a>").text(n.label)).appendTo(t)},_move:function(e,t){if(!this.menu.element.is(":visible")){this.search(null,t);return}if(this.menu.isFirstItem()&&/^previous/.test(e)||this.menu.isLastItem()&&/^next/.test(e)){this._value(this.term),this.menu.blur();return}this.menu[e](t)},widget:function(){return this.menu.element},_value:function(){return this.valueMethod.apply(this.element,arguments)},_keyEvent:function(e,t){if(!this.isMultiLine||this.menu.element.is(":visible"))this._move(e,t),t.preventDefault()}}),e.extend(e.ui.autocomplete,{escapeRegex:function(e){return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")},filter:function(t,n){var r=new RegExp(e.ui.autocomplete.escapeRegex(n),"i");return e.grep(t,function(e){return r.test(e.label||e.value||e)})}}),e.widget("ui.autocomplete",e.ui.autocomplete,{options:{messages:{noResults:"No search results.",results:function(e){return e+(e>1?" results are":" result is")+" available, use up and down arrow keys to navigate."}}},__response:function(e){var t;this._superApply(arguments);if(this.options.disabled||this.cancelSearch)return;e&&e.length?t=this.options.messages.results(e.length):t=this.options.messages.noResults,this.liveRegion.text(t)}})}(jQuery),function(e,t){var n,r,i,s,o="ui-button ui-widget ui-state-default ui-corner-all",u="ui-state-hover ui-state-active ",a="ui-button-icons-only ui-button-icon-only ui-button-text-icons ui-button-text-icon-primary ui-button-text-icon-secondary ui-button-text-only",f=function(){var t=e(this).find(":ui-button");setTimeout(function(){t.button("refresh")},1)},l=function(t){var n=t.name,r=t.form,i=e([]);return n&&(n=n.replace(/'/g,"\\'"),r?i=e(r).find("[name='"+n+"']"):i=e("[name='"+n+"']",t.ownerDocument).filter(function(){return!this.form})),i};e.widget("ui.button",{version:"1.10.1",defaultElement:"<button>",options:{disabled:null,text:!0,label:null,icons:{primary:null,secondary:null}},_create:function(){this.element.closest("form").unbind("reset"+this.eventNamespace).bind("reset"+this.eventNamespace,f),typeof this.options.disabled!="boolean"?this.options.disabled=!!this.element.prop("disabled"):this.element.prop("disabled",this.options.disabled),this._determineButtonType(),this.hasTitle=!!this.buttonElement.attr("title");var t=this,u=this.options,a=this.type==="checkbox"||this.type==="radio",c=a?"":"ui-state-active",h="ui-state-focus";u.label===null&&(u.label=this.type==="input"?this.buttonElement.val():this.buttonElement.html()),this._hoverable(this.buttonElement),this.buttonElement.addClass(o).attr("role","button").bind("mouseenter"+this.eventNamespace,function(){if(u.disabled)return;this===n&&e(this).addClass("ui-state-active")}).bind("mouseleave"+this.eventNamespace,function(){if(u.disabled)return;e(this).removeClass(c)}).bind("click"+this.eventNamespace,function(e){u.disabled&&(e.preventDefault(),e.stopImmediatePropagation())}),this.element.bind("focus"+this.eventNamespace,function(){t.buttonElement.addClass(h)}).bind("blur"+this.eventNamespace,function(){t.buttonElement.removeClass(h)}),a&&(this.element.bind("change"+this.eventNamespace,function(){if(s)return;t.refresh()}),this.buttonElement.bind("mousedown"+this.eventNamespace,function(e){if(u.disabled)return;s=!1,r=e.pageX,i=e.pageY}).bind("mouseup"+this.eventNamespace,function(e){if(u.disabled)return;if(r!==e.pageX||i!==e.pageY)s=!0})),this.type==="checkbox"?this.buttonElement.bind("click"+this.eventNamespace,function(){if(u.disabled||s)return!1}):this.type==="radio"?this.buttonElement.bind("click"+this.eventNamespace,function(){if(u.disabled||s)return!1;e(this).addClass("ui-state-active"),t.buttonElement.attr("aria-pressed","true");var n=t.element[0];l(n).not(n).map(function(){return e(this).button("widget")[0]}).removeClass("ui-state-active").attr("aria-pressed","false")}):(this.buttonElement.bind("mousedown"+this.eventNamespace,function(){if(u.disabled)return!1;e(this).addClass("ui-state-active"),n=this,t.document.one("mouseup",function(){n=null})}).bind("mouseup"+this.eventNamespace,function(){if(u.disabled)return!1;e(this).removeClass("ui-state-active")}).bind("keydown"+this.eventNamespace,function(t){if(u.disabled)return!1;(t.keyCode===e.ui.keyCode.SPACE||t.keyCode===e.ui.keyCode.ENTER)&&e(this).addClass("ui-state-active")}).bind("keyup"+this.eventNamespace+" blur"+this.eventNamespace,function(){e(this).removeClass("ui-state-active")}),this.buttonElement.is("a")&&this.buttonElement.keyup(function(t){t.keyCode===e.ui.keyCode.SPACE&&e(this).click()})),this._setOption("disabled",u.disabled),this._resetButton()},_determineButtonType:function(){var e,t,n;this.element.is("[type=checkbox]")?this.type="checkbox":this.element.is("[type=radio]")?this.type="radio":this.element.is("input")?this.type="input":this.type="button",this.type==="checkbox"||this.type==="radio"?(e=this.element.parents().last(),t="label[for='"+this.element.attr("id")+"']",this.buttonElement=e.find(t),this.buttonElement.length||(e=e.length?e.siblings():this.element.siblings(),this.buttonElement=e.filter(t),this.buttonElement.length||(this.buttonElement=e.find(t))),this.element.addClass("ui-helper-hidden-accessible"),n=this.element.is(":checked"),n&&this.buttonElement.addClass("ui-state-active"),this.buttonElement.prop("aria-pressed",n)):this.buttonElement=this.element},widget:function(){return this.buttonElement},_destroy:function(){this.element.removeClass("ui-helper-hidden-accessible"),this.buttonElement.removeClass(o+" "+u+" "+a).removeAttr("role").removeAttr("aria-pressed").html(this.buttonElement.find(".ui-button-text").html()),this.hasTitle||this.buttonElement.removeAttr("title")},_setOption:function(e,t){this._super(e,t);if(e==="disabled"){t?this.element.prop("disabled",!0):this.element.prop("disabled",!1);return}this._resetButton()},refresh:function(){var t=this.element.is("input, button")?this.element.is(":disabled"):this.element.hasClass("ui-button-disabled");t!==this.options.disabled&&this._setOption("disabled",t),this.type==="radio"?l(this.element[0]).each(function(){e(this).is(":checked")?e(this).button("widget").addClass("ui-state-active").attr("aria-pressed","true"):e(this).button("widget").removeClass("ui-state-active").attr("aria-pressed","false")}):this.type==="checkbox"&&(this.element.is(":checked")?this.buttonElement.addClass("ui-state-active").attr("aria-pressed","true"):this.buttonElement.removeClass("ui-state-active").attr("aria-pressed","false"))},_resetButton:function(){if(this.type==="input"){this.options.label&&this.element.val(this.options.label);return}var t=this.buttonElement.removeClass(a),n=e("<span></span>",this.document[0]).addClass("ui-button-text").html(this.options.label).appendTo(t.empty()).text(),r=this.options.icons,i=r.primary&&r.secondary,s=[];r.primary||r.secondary?(this.options.text&&s.push("ui-button-text-icon"+(i?"s":r.primary?"-primary":"-secondary")),r.primary&&t.prepend("<span class='ui-button-icon-primary ui-icon "+r.primary+"'></span>"),r.secondary&&t.append("<span class='ui-button-icon-secondary ui-icon "+r.secondary+"'></span>"),this.options.text||(s.push(i?"ui-button-icons-only":"ui-button-icon-only"),this.hasTitle||t.attr("title",e.trim(n)))):s.push("ui-button-text-only"),t.addClass(s.join(" "))}}),e.widget("ui.buttonset",{version:"1.10.1",options:{items:"button, input[type=button], input[type=submit], input[type=reset], input[type=checkbox], input[type=radio], a, :data(ui-button)"},_create:function(){this.element.addClass("ui-buttonset")},_init:function(){this.refresh()},_setOption:function(e,t){e==="disabled"&&this.buttons.button("option",e,t),this._super(e,t)},refresh:function(){var t=this.element.css("direction")==="rtl";this.buttons=this.element.find(this.options.items).filter(":ui-button").button("refresh").end().not(":ui-button").button().end().map(function(){return e(this).button("widget")[0]}).removeClass("ui-corner-all ui-corner-left ui-corner-right").filter(":first").addClass(t?"ui-corner-right":"ui-corner-left").end().filter(":last").addClass(t?"ui-corner-left":"ui-corner-right").end().end()},_destroy:function(){this.element.removeClass("ui-buttonset"),this.buttons.map(function(){return e(this).button("widget")[0]}).removeClass("ui-corner-left ui-corner-right").end().button("destroy")}})}(jQuery),function(e,t){function s(){this._curInst=null,this._keyEvent=!1,this._disabledInputs=[],this._datepickerShowing=!1,this._inDialog=!1,this._mainDivId="ui-datepicker-div",this._inlineClass="ui-datepicker-inline",this._appendClass="ui-datepicker-append",this._triggerClass="ui-datepicker-trigger",this._dialogClass="ui-datepicker-dialog",this._disableClass="ui-datepicker-disabled",this._unselectableClass="ui-datepicker-unselectable",this._currentClass="ui-datepicker-current-day",this._dayOverClass="ui-datepicker-days-cell-over",this.regional=[],this.regional[""]={closeText:"Done",prevText:"Prev",nextText:"Next",currentText:"Today",monthNames:["January","February","March","April","May","June","July","August","September","October","November","December"],monthNamesShort:["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],dayNames:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],dayNamesShort:["Sun","Mon","Tue","Wed","Thu","Fri","Sat"],dayNamesMin:["Su","Mo","Tu","We","Th","Fr","Sa"],weekHeader:"Wk",dateFormat:"mm/dd/yy",firstDay:0,isRTL:!1,showMonthAfterYear:!1,yearSuffix:""},this._defaults={showOn:"focus",showAnim:"fadeIn",showOptions:{},defaultDate:null,appendText:"",buttonText:"...",buttonImage:"",buttonImageOnly:!1,hideIfNoPrevNext:!1,navigationAsDateFormat:!1,gotoCurrent:!1,changeMonth:!1,changeYear:!1,yearRange:"c-10:c+10",showOtherMonths:!1,selectOtherMonths:!1,showWeek:!1,calculateWeek:this.iso8601Week,shortYearCutoff:"+10",minDate:null,maxDate:null,duration:"fast",beforeShowDay:null,beforeShow:null,onSelect:null,onChangeMonthYear:null,onClose:null,numberOfMonths:1,showCurrentAtPos:0,stepMonths:1,stepBigMonths:12,altField:"",altFormat:"",constrainInput:!0,showButtonPanel:!1,autoSize:!1,disabled:!1},e.extend(this._defaults,this.regional[""]),this.dpDiv=o(e("<div id='"+this._mainDivId+"' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>"))}function o(t){var n="button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";return t.delegate(n,"mouseout",function(){e(this).removeClass("ui-state-hover"),this.className.indexOf("ui-datepicker-prev")!==-1&&e(this).removeClass("ui-datepicker-prev-hover"),this.className.indexOf("ui-datepicker-next")!==-1&&e(this).removeClass("ui-datepicker-next-hover")}).delegate(n,"mouseover",function(){e.datepicker._isDisabledDatepicker(i.inline?t.parent()[0]:i.input[0])||(e(this).parents(".ui-datepicker-calendar").find("a").removeClass("ui-state-hover"),e(this).addClass("ui-state-hover"),this.className.indexOf("ui-datepicker-prev")!==-1&&e(this).addClass("ui-datepicker-prev-hover"),this.className.indexOf("ui-datepicker-next")!==-1&&e(this).addClass("ui-datepicker-next-hover"))})}function u(t,n){e.extend(t,n);for(var r in n)n[r]==null&&(t[r]=n[r]);return t}e.extend(e.ui,{datepicker:{version:"1.10.1"}});var n="datepicker",r=(new Date).getTime(),i;e.extend(s.prototype,{markerClassName:"hasDatepicker",maxRows:4,_widgetDatepicker:function(){return this.dpDiv},setDefaults:function(e){return u(this._defaults,e||{}),this},_attachDatepicker:function(t,n){var r,i,s;r=t.nodeName.toLowerCase(),i=r==="div"||r==="span",t.id||(this.uuid+=1,t.id="dp"+this.uuid),s=this._newInst(e(t),i),s.settings=e.extend({},n||{}),r==="input"?this._connectDatepicker(t,s):i&&this._inlineDatepicker(t,s)},_newInst:function(t,n){var r=t[0].id.replace(/([^A-Za-z0-9_\-])/g,"\\\\$1");return{id:r,input:t,selectedDay:0,selectedMonth:0,selectedYear:0,drawMonth:0,drawYear:0,inline:n,dpDiv:n?o(e("<div class='"+this._inlineClass+" ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>")):this.dpDiv}},_connectDatepicker:function(t,r){var i=e(t);r.append=e([]),r.trigger=e([]);if(i.hasClass(this.markerClassName))return;this._attachments(i,r),i.addClass(this.markerClassName).keydown(this._doKeyDown).keypress(this._doKeyPress).keyup(this._doKeyUp),this._autoSize(r),e.data(t,n,r),r.settings.disabled&&this._disableDatepicker(t)},_attachments:function(t,n){var r,i,s,o=this._get(n,"appendText"),u=this._get(n,"isRTL");n.append&&n.append.remove(),o&&(n.append=e("<span class='"+this._appendClass+"'>"+o+"</span>"),t[u?"before":"after"](n.append)),t.unbind("focus",this._showDatepicker),n.trigger&&n.trigger.remove(),r=this._get(n,"showOn"),(r==="focus"||r==="both")&&t.focus(this._showDatepicker);if(r==="button"||r==="both")i=this._get(n,"buttonText"),s=this._get(n,"buttonImage"),n.trigger=e(this._get(n,"buttonImageOnly")?e("<img/>").addClass(this._triggerClass).attr({src:s,alt:i,title:i}):e("<button type='button'></button>").addClass(this._triggerClass).html(s?e("<img/>").attr({src:s,alt:i,title:i}):i)),t[u?"before":"after"](n.trigger),n.trigger.click(function(){return e.datepicker._datepickerShowing&&e.datepicker._lastInput===t[0]?e.datepicker._hideDatepicker():e.datepicker._datepickerShowing&&e.datepicker._lastInput!==t[0]?(e.datepicker._hideDatepicker(),e.datepicker._showDatepicker(t[0])):e.datepicker._showDatepicker(t[0]),!1})},_autoSize:function(e){if(this._get(e,"autoSize")&&!e.inline){var t,n,r,i,s=new Date(2009,11,20),o=this._get(e,"dateFormat");o.match(/[DM]/)&&(t=function(e){n=0,r=0;for(i=0;i<e.length;i++)e[i].length>n&&(n=e[i].length,r=i);return r},s.setMonth(t(this._get(e,o.match(/MM/)?"monthNames":"monthNamesShort"))),s.setDate(t(this._get(e,o.match(/DD/)?"dayNames":"dayNamesShort"))+20-s.getDay())),e.input.attr("size",this._formatDate(e,s).length)}},_inlineDatepicker:function(t,r){var i=e(t);if(i.hasClass(this.markerClassName))return;i.addClass(this.markerClassName).append(r.dpDiv),e.data(t,n,r),this._setDate(r,this._getDefaultDate(r),!0),this._updateDatepicker(r),this._updateAlternate(r),r.settings.disabled&&this._disableDatepicker(t),r.dpDiv.css("display","block")},_dialogDatepicker:function(t,r,i,s,o){var a,f,l,c,h,p=this._dialogInst;return p||(this.uuid+=1,a="dp"+this.uuid,this._dialogInput=e("<input type='text' id='"+a+"' style='position: absolute; top: -100px; width: 0px;'/>"),this._dialogInput.keydown(this._doKeyDown),e("body").append(this._dialogInput),p=this._dialogInst=this._newInst(this._dialogInput,!1),p.settings={},e.data(this._dialogInput[0],n,p)),u(p.settings,s||{}),r=r&&r.constructor===Date?this._formatDate(p,r):r,this._dialogInput.val(r),this._pos=o?o.length?o:[o.pageX,o.pageY]:null,this._pos||(f=document.documentElement.clientWidth,l=document.documentElement.clientHeight,c=document.documentElement.scrollLeft||document.body.scrollLeft,h=document.documentElement.scrollTop||document.body.scrollTop,this._pos=[f/2-100+c,l/2-150+h]),this._dialogInput.css("left",this._pos[0]+20+"px").css("top",this._pos[1]+"px"),p.settings.onSelect=i,this._inDialog=!0,this.dpDiv.addClass(this._dialogClass),this._showDatepicker(this._dialogInput[0]),e.blockUI&&e.blockUI(this.dpDiv),e.data(this._dialogInput[0],n,p),this},_destroyDatepicker:function(t){var r,i=e(t),s=e.data(t,n);if(!i.hasClass(this.markerClassName))return;r=t.nodeName.toLowerCase(),e.removeData(t,n),r==="input"?(s.append.remove(),s.trigger.remove(),i.removeClass(this.markerClassName).unbind("focus",this._showDatepicker).unbind("keydown",this._doKeyDown).unbind("keypress",this._doKeyPress).unbind("keyup",this._doKeyUp)):(r==="div"||r==="span")&&i.removeClass(this.markerClassName).empty()},_enableDatepicker:function(t){var r,i,s=e(t),o=e.data(t,n);if(!s.hasClass(this.markerClassName))return;r=t.nodeName.toLowerCase();if(r==="input")t.disabled=!1,o.trigger.filter("button").each(function(){this.disabled=!1}).end().filter("img").css({opacity:"1.0",cursor:""});else if(r==="div"||r==="span")i=s.children("."+this._inlineClass),i.children().removeClass("ui-state-disabled"),i.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!1);this._disabledInputs=e.map(this._disabledInputs,function(e){return e===t?null:e})},_disableDatepicker:function(t){var r,i,s=e(t),o=e.data(t,n);if(!s.hasClass(this.markerClassName))return;r=t.nodeName.toLowerCase();if(r==="input")t.disabled=!0,o.trigger.filter("button").each(function(){this.disabled=!0}).end().filter("img").css({opacity:"0.5",cursor:"default"});else if(r==="div"||r==="span")i=s.children("."+this._inlineClass),i.children().addClass("ui-state-disabled"),i.find("select.ui-datepicker-month, select.ui-datepicker-year").prop("disabled",!0);this._disabledInputs=e.map(this._disabledInputs,function(e){return e===t?null:e}),this._disabledInputs[this._disabledInputs.length]=t},_isDisabledDatepicker:function(e){if(!e)return!1;for(var t=0;t<this._disabledInputs.length;t++)if(this._disabledInputs[t]===e)return!0;return!1},_getInst:function(t){try{return e.data(t,n)}catch(r){throw"Missing instance data for this datepicker"}},_optionDatepicker:function(n,r,i){var s,o,a,f,l=this._getInst(n);if(arguments.length===2&&typeof r=="string")return r==="defaults"?e.extend({},e.datepicker._defaults):l?r==="all"?e.extend({},l.settings):this._get(l,r):null;s=r||{},typeof r=="string"&&(s={},s[r]=i),l&&(this._curInst===l&&this._hideDatepicker(),o=this._getDateDatepicker(n,!0),a=this._getMinMaxDate(l,"min"),f=this._getMinMaxDate(l,"max"),u(l.settings,s),a!==null&&s.dateFormat!==t&&s.minDate===t&&(l.settings.minDate=this._formatDate(l,a)),f!==null&&s.dateFormat!==t&&s.maxDate===t&&(l.settings.maxDate=this._formatDate(l,f)),"disabled"in s&&(s.disabled?this._disableDatepicker(n):this._enableDatepicker(n)),this._attachments(e(n),l),this._autoSize(l),this._setDate(l,o),this._updateAlternate(l),this._updateDatepicker(l))},_changeDatepicker:function(e,t,n){this._optionDatepicker(e,t,n)},_refreshDatepicker:function(e){var t=this._getInst(e);t&&this._updateDatepicker(t)},_setDateDatepicker:function(e,t){var n=this._getInst(e);n&&(this._setDate(n,t),this._updateDatepicker(n),this._updateAlternate(n))},_getDateDatepicker:function(e,t){var n=this._getInst(e);return n&&!n.inline&&this._setDateFromField(n,t),n?this._getDate(n):null},_doKeyDown:function(t){var n,r,i,s=e.datepicker._getInst(t.target),o=!0,u=s.dpDiv.is(".ui-datepicker-rtl");s._keyEvent=!0;if(e.datepicker._datepickerShowing)switch(t.keyCode){case 9:e.datepicker._hideDatepicker(),o=!1;break;case 13:return i=e("td."+e.datepicker._dayOverClass+":not(."+e.datepicker._currentClass+")",s.dpDiv),i[0]&&e.datepicker._selectDay(t.target,s.selectedMonth,s.selectedYear,i[0]),n=e.datepicker._get(s,"onSelect"),n?(r=e.datepicker._formatDate(s),n.apply(s.input?s.input[0]:null,[r,s])):e.datepicker._hideDatepicker(),!1;case 27:e.datepicker._hideDatepicker();break;case 33:e.datepicker._adjustDate(t.target,t.ctrlKey?-e.datepicker._get(s,"stepBigMonths"):-e.datepicker._get(s,"stepMonths"),"M");break;case 34:e.datepicker._adjustDate(t.target,t.ctrlKey?+e.datepicker._get(s,"stepBigMonths"):+e.datepicker._get(s,"stepMonths"),"M");break;case 35:(t.ctrlKey||t.metaKey)&&e.datepicker._clearDate(t.target),o=t.ctrlKey||t.metaKey;break;case 36:(t.ctrlKey||t.metaKey)&&e.datepicker._gotoToday(t.target),o=t.ctrlKey||t.metaKey;break;case 37:(t.ctrlKey||t.metaKey)&&e.datepicker._adjustDate(t.target,u?1:-1,"D"),o=t.ctrlKey||t.metaKey,t.originalEvent.altKey&&e.datepicker._adjustDate(t.target,t.ctrlKey?-e.datepicker._get(s,"stepBigMonths"):-e.datepicker._get(s,"stepMonths"),"M");break;case 38:(t.ctrlKey||t.metaKey)&&e.datepicker._adjustDate(t.target,-7,"D"),o=t.ctrlKey||t.metaKey;break;case 39:(t.ctrlKey||t.metaKey)&&e.datepicker._adjustDate(t.target,u?-1:1,"D"),o=t.ctrlKey||t.metaKey,t.originalEvent.altKey&&e.datepicker._adjustDate(t.target,t.ctrlKey?+e.datepicker._get(s,"stepBigMonths"):+e.datepicker._get(s,"stepMonths"),"M");break;case 40:(t.ctrlKey||t.metaKey)&&e.datepicker._adjustDate(t.target,7,"D"),o=t.ctrlKey||t.metaKey;break;default:o=!1}else t.keyCode===36&&t.ctrlKey?e.datepicker._showDatepicker(this):o=!1;o&&(t.preventDefault(),t.stopPropagation())},_doKeyPress:function(t){var n,r,i=e.datepicker._getInst(t.target);if(e.datepicker._get(i,"constrainInput"))return n=e.datepicker._possibleChars(e.datepicker._get(i,"dateFormat")),r=String.fromCharCode(t.charCode==null?t.keyCode:t.charCode),t.ctrlKey||t.metaKey||r<" "||!n||n.indexOf(r)>-1},_doKeyUp:function(t){var n,r=e.datepicker._getInst(t.target);if(r.input.val()!==r.lastVal)try{n=e.datepicker.parseDate(e.datepicker._get(r,"dateFormat"),r.input?r.input.val():null,e.datepicker._getFormatConfig(r)),n&&(e.datepicker._setDateFromField(r),e.datepicker._updateAlternate(r),e.datepicker._updateDatepicker(r))}catch(i){}return!0},_showDatepicker:function(t){t=t.target||t,t.nodeName.toLowerCase()!=="input"&&(t=e("input",t.parentNode)[0]);if(e.datepicker._isDisabledDatepicker(t)||e.datepicker._lastInput===t)return;var n,r,i,s,o,a,f;n=e.datepicker._getInst(t),e.datepicker._curInst&&e.datepicker._curInst!==n&&(e.datepicker._curInst.dpDiv.stop(!0,!0),n&&e.datepicker._datepickerShowing&&e.datepicker._hideDatepicker(e.datepicker._curInst.input[0])),r=e.datepicker._get(n,"beforeShow"),i=r?r.apply(t,[t,n]):{};if(i===!1)return;u(n.settings,i),n.lastVal=null,e.datepicker._lastInput=t,e.datepicker._setDateFromField(n),e.datepicker._inDialog&&(t.value=""),e.datepicker._pos||(e.datepicker._pos=e.datepicker._findPos(t),e.datepicker._pos[1]+=t.offsetHeight),s=!1,e(t).parents().each(function(){return s|=e(this).css("position")==="fixed",!s}),o={left:e.datepicker._pos[0],top:e.datepicker._pos[1]},e.datepicker._pos=null,n.dpDiv.empty(),n.dpDiv.css({position:"absolute",display:"block",top:"-1000px"}),e.datepicker._updateDatepicker(n),o=e.datepicker._checkOffset(n,o,s),n.dpDiv.css({position:e.datepicker._inDialog&&e.blockUI?"static":s?"fixed":"absolute",display:"none",left:o.left+"px",top:o.top+"px"}),n.inline||(a=e.datepicker._get(n,"showAnim"),f=e.datepicker._get(n,"duration"),n.dpDiv.zIndex(e(t).zIndex()+1),e.datepicker._datepickerShowing=!0,e.effects&&e.effects.effect[a]?n.dpDiv.show(a,e.datepicker._get(n,"showOptions"),f):n.dpDiv[a||"show"](a?f:null),n.input.is(":visible")&&!n.input.is(":disabled")&&n.input.focus(),e.datepicker._curInst=n)},_updateDatepicker:function(t){this.maxRows=4,i=t,t.dpDiv.empty().append(this._generateHTML(t)),this._attachHandlers(t),t.dpDiv.find("."+this._dayOverClass+" a").mouseover();var n,r=this._getNumberOfMonths(t),s=r[1],o=17;t.dpDiv.removeClass("ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4").width(""),s>1&&t.dpDiv.addClass("ui-datepicker-multi-"+s).css("width",o*s+"em"),t.dpDiv[(r[0]!==1||r[1]!==1?"add":"remove")+"Class"]("ui-datepicker-multi"),t.dpDiv[(this._get(t,"isRTL")?"add":"remove")+"Class"]("ui-datepicker-rtl"),t===e.datepicker._curInst&&e.datepicker._datepickerShowing&&t.input&&t.input.is(":visible")&&!t.input.is(":disabled")&&t.input[0]!==document.activeElement&&t.input.focus(),t.yearshtml&&(n=t.yearshtml,setTimeout(function(){n===t.yearshtml&&t.yearshtml&&t.dpDiv.find("select.ui-datepicker-year:first").replaceWith(t.yearshtml),n=t.yearshtml=null},0))},_getBorders:function(e){var t=function(e){return{thin:1,medium:2,thick:3}[e]||e};return[parseFloat(t(e.css("border-left-width"))),parseFloat(t(e.css("border-top-width")))]},_checkOffset:function(t,n,r){var i=t.dpDiv.outerWidth(),s=t.dpDiv.outerHeight(),o=t.input?t.input.outerWidth():0,u=t.input?t.input.outerHeight():0,a=document.documentElement.clientWidth+(r?0:e(document).scrollLeft()),f=document.documentElement.clientHeight+(r?0:e(document).scrollTop());return n.left-=this._get(t,"isRTL")?i-o:0,n.left-=r&&n.left===t.input.offset().left?e(document).scrollLeft():0,n.top-=r&&n.top===t.input.offset().top+u?e(document).scrollTop():0,n.left-=Math.min(n.left,n.left+i>a&&a>i?Math.abs(n.left+i-a):0),n.top-=Math.min(n.top,n.top+s>f&&f>s?Math.abs(s+u):0),n},_findPos:function(t){var n,r=this._getInst(t),i=this._get(r,"isRTL");while(t&&(t.type==="hidden"||t.nodeType!==1||e.expr.filters.hidden(t)))t=t[i?"previousSibling":"nextSibling"];return n=e(t).offset(),[n.left,n.top]},_hideDatepicker:function(t){var r,i,s,o,u=this._curInst;if(!u||t&&u!==e.data(t,n))return;this._datepickerShowing&&(r=this._get(u,"showAnim"),i=this._get(u,"duration"),s=function(){e.datepicker._tidyDialog(u)},e.effects&&(e.effects.effect[r]||e.effects[r])?u.dpDiv.hide(r,e.datepicker._get(u,"showOptions"),i,s):u.dpDiv[r==="slideDown"?"slideUp":r==="fadeIn"?"fadeOut":"hide"](r?i:null,s),r||s(),this._datepickerShowing=!1,o=this._get(u,"onClose"),o&&o.apply(u.input?u.input[0]:null,[u.input?u.input.val():"",u]),this._lastInput=null,this._inDialog&&(this._dialogInput.css({position:"absolute",left:"0",top:"-100px"}),e.blockUI&&(e.unblockUI(),e("body").append(this.dpDiv))),this._inDialog=!1)},_tidyDialog:function(e){e.dpDiv.removeClass(this._dialogClass).unbind(".ui-datepicker-calendar")},_checkExternalClick:function(t){if(!e.datepicker._curInst)return;var n=e(t.target),r=e.datepicker._getInst(n[0]);(n[0].id!==e.datepicker._mainDivId&&n.parents("#"+e.datepicker._mainDivId).length===0&&!n.hasClass(e.datepicker.markerClassName)&&!n.closest("."+e.datepicker._triggerClass).length&&e.datepicker._datepickerShowing&&(!e.datepicker._inDialog||!e.blockUI)||n.hasClass(e.datepicker.markerClassName)&&e.datepicker._curInst!==r)&&e.datepicker._hideDatepicker()},_adjustDate:function(t,n,r){var i=e(t),s=this._getInst(i[0]);if(this._isDisabledDatepicker(i[0]))return;this._adjustInstDate(s,n+(r==="M"?this._get(s,"showCurrentAtPos"):0),r),this._updateDatepicker(s)},_gotoToday:function(t){var n,r=e(t),i=this._getInst(r[0]);this._get(i,"gotoCurrent")&&i.currentDay?(i.selectedDay=i.currentDay,i.drawMonth=i.selectedMonth=i.currentMonth,i.drawYear=i.selectedYear=i.currentYear):(n=new Date,i.selectedDay=n.getDate(),i.drawMonth=i.selectedMonth=n.getMonth(),i.drawYear=i.selectedYear=n.getFullYear()),this._notifyChange(i),this._adjustDate(r)},_selectMonthYear:function(t,n,r){var i=e(t),s=this._getInst(i[0]);s["selected"+(r==="M"?"Month":"Year")]=s["draw"+(r==="M"?"Month":"Year")]=parseInt(n.options[n.selectedIndex].value,10),this._notifyChange(s),this._adjustDate(i)},_selectDay:function(t,n,r,i){var s,o=e(t);if(e(i).hasClass(this._unselectableClass)||this._isDisabledDatepicker(o[0]))return;s=this._getInst(o[0]),s.selectedDay=s.currentDay=e("a",i).html(),s.selectedMonth=s.currentMonth=n,s.selectedYear=s.currentYear=r,this._selectDate(t,this._formatDate(s,s.currentDay,s.currentMonth,s.currentYear))},_clearDate:function(t){var n=e(t);this._selectDate(n,"")},_selectDate:function(t,n){var r,i=e(t),s=this._getInst(i[0]);n=n!=null?n:this._formatDate(s),s.input&&s.input.val(n),this._updateAlternate(s),r=this._get(s,"onSelect"),r?r.apply(s.input?s.input[0]:null,[n,s]):s.input&&s.input.trigger("change"),s.inline?this._updateDatepicker(s):(this._hideDatepicker(),this._lastInput=s.input[0],typeof s.input[0]!="object"&&s.input.focus(),this._lastInput=null)},_updateAlternate:function(t){var n,r,i,s=this._get(t,"altField");s&&(n=this._get(t,"altFormat")||this._get(t,"dateFormat"),r=this._getDate(t),i=this.formatDate(n,r,this._getFormatConfig(t)),e(s).each(function(){e(this).val(i)}))},noWeekends:function(e){var t=e.getDay();return[t>0&&t<6,""]},iso8601Week:function(e){var t,n=new Date(e.getTime());return n.setDate(n.getDate()+4-(n.getDay()||7)),t=n.getTime(),n.setMonth(0),n.setDate(1),Math.floor(Math.round((t-n)/864e5)/7)+1},parseDate:function(t,n,r){if(t==null||n==null)throw"Invalid arguments";n=typeof n=="object"?n.toString():n+"";if(n==="")return null;var i,s,o,u=0,a=(r?r.shortYearCutoff:null)||this._defaults.shortYearCutoff,f=typeof a!="string"?a:(new Date).getFullYear()%100+parseInt(a,10),l=(r?r.dayNamesShort:null)||this._defaults.dayNamesShort,c=(r?r.dayNames:null)||this._defaults.dayNames,h=(r?r.monthNamesShort:null)||this._defaults.monthNamesShort,p=(r?r.monthNames:null)||this._defaults.monthNames,d=-1,v=-1,m=-1,g=-1,y=!1,b,w=function(e){var n=i+1<t.length&&t.charAt(i+1)===e;return n&&i++,n},E=function(e){var t=w(e),r=e==="@"?14:e==="!"?20:e==="y"&&t?4:e==="o"?3:2,i=new RegExp("^\\d{1,"+r+"}"),s=n.substring(u).match(i);if(!s)throw"Missing number at position "+u;return u+=s[0].length,parseInt(s[0],10)},S=function(t,r,i){var s=-1,o=e.map(w(t)?i:r,function(e,t){return[[t,e]]}).sort(function(e,t){return-(e[1].length-t[1].length)});e.each(o,function(e,t){var r=t[1];if(n.substr(u,r.length).toLowerCase()===r.toLowerCase())return s=t[0],u+=r.length,!1});if(s!==-1)return s+1;throw"Unknown name at position "+u},x=function(){if(n.charAt(u)!==t.charAt(i))throw"Unexpected literal at position "+u;u++};for(i=0;i<t.length;i++)if(y)t.charAt(i)==="'"&&!w("'")?y=!1:x();else switch(t.charAt(i)){case"d":m=E("d");break;case"D":S("D",l,c);break;case"o":g=E("o");break;case"m":v=E("m");break;case"M":v=S("M",h,p);break;case"y":d=E("y");break;case"@":b=new Date(E("@")),d=b.getFullYear(),v=b.getMonth()+1,m=b.getDate();break;case"!":b=new Date((E("!")-this._ticksTo1970)/1e4),d=b.getFullYear(),v=b.getMonth()+1,m=b.getDate();break;case"'":w("'")?x():y=!0;break;default:x()}if(u<n.length){o=n.substr(u);if(!/^\s+/.test(o))throw"Extra/unparsed characters found in date: "+o}d===-1?d=(new Date).getFullYear():d<100&&(d+=(new Date).getFullYear()-(new Date).getFullYear()%100+(d<=f?0:-100));if(g>-1){v=1,m=g;do{s=this._getDaysInMonth(d,v-1);if(m<=s)break;v++,m-=s}while(!0)}b=this._daylightSavingAdjust(new Date(d,v-1,m));if(b.getFullYear()!==d||b.getMonth()+1!==v||b.getDate()!==m)throw"Invalid date";return b},ATOM:"yy-mm-dd",COOKIE:"D, dd M yy",ISO_8601:"yy-mm-dd",RFC_822:"D, d M y",RFC_850:"DD, dd-M-y",RFC_1036:"D, d M y",RFC_1123:"D, d M yy",RFC_2822:"D, d M yy",RSS:"D, d M y",TICKS:"!",TIMESTAMP:"@",W3C:"yy-mm-dd",_ticksTo1970:(718685+Math.floor(492.5)-Math.floor(19.7)+Math.floor(4.925))*24*60*60*1e7,formatDate:function(e,t,n){if(!t)return"";var r,i=(n?n.dayNamesShort:null)||this._defaults.dayNamesShort,s=(n?n.dayNames:null)||this._defaults.dayNames,o=(n?n.monthNamesShort:null)||this._defaults.monthNamesShort,u=(n?n.monthNames:null)||this._defaults.monthNames,a=function(t){var n=r+1<e.length&&e.charAt(r+1)===t;return n&&r++,n},f=function(e,t,n){var r=""+t;if(a(e))while(r.length<n)r="0"+r;return r},l=function(e,t,n,r){return a(e)?r[t]:n[t]},c="",h=!1;if(t)for(r=0;r<e.length;r++)if(h)e.charAt(r)==="'"&&!a("'")?h=!1:c+=e.charAt(r);else switch(e.charAt(r)){case"d":c+=f("d",t.getDate(),2);break;case"D":c+=l("D",t.getDay(),i,s);break;case"o":c+=f("o",Math.round(((new Date(t.getFullYear(),t.getMonth(),t.getDate())).getTime()-(new Date(t.getFullYear(),0,0)).getTime())/864e5),3);break;case"m":c+=f("m",t.getMonth()+1,2);break;case"M":c+=l("M",t.getMonth(),o,u);break;case"y":c+=a("y")?t.getFullYear():(t.getYear()%100<10?"0":"")+t.getYear()%100;break;case"@":c+=t.getTime();break;case"!":c+=t.getTime()*1e4+this._ticksTo1970;break;case"'":a("'")?c+="'":h=!0;break;default:c+=e.charAt(r)}return c},_possibleChars:function(e){var t,n="",r=!1,i=function(n){var r=t+1<e.length&&e.charAt(t+1)===n;return r&&t++,r};for(t=0;t<e.length;t++)if(r)e.charAt(t)==="'"&&!i("'")?r=!1:n+=e.charAt(t);else switch(e.charAt(t)){case"d":case"m":case"y":case"@":n+="0123456789";break;case"D":case"M":return null;case"'":i("'")?n+="'":r=!0;break;default:n+=e.charAt(t)}return n},_get:function(e,n){return e.settings[n]!==t?e.settings[n]:this._defaults[n]},_setDateFromField:function(e,t){if(e.input.val()===e.lastVal)return;var n=this._get(e,"dateFormat"),r=e.lastVal=e.input?e.input.val():null,i=this._getDefaultDate(e),s=i,o=this._getFormatConfig(e);try{s=this.parseDate(n,r,o)||i}catch(u){r=t?"":r}e.selectedDay=s.getDate(),e.drawMonth=e.selectedMonth=s.getMonth(),e.drawYear=e.selectedYear=s.getFullYear(),e.currentDay=r?s.getDate():0,e.currentMonth=r?s.getMonth():0,e.currentYear=r?s.getFullYear():0,this._adjustInstDate(e)},_getDefaultDate:function(e){return this._restrictMinMax(e,this._determineDate(e,this._get(e,"defaultDate"),new Date))},_determineDate:function(t,n,r){var i=function(e){var t=new Date;return t.setDate(t.getDate()+e),t},s=function(n){try{return e.datepicker.parseDate(e.datepicker._get(t,"dateFormat"),n,e.datepicker._getFormatConfig(t))}catch(r){}var i=(n.toLowerCase().match(/^c/)?e.datepicker._getDate(t):null)||new Date,s=i.getFullYear(),o=i.getMonth(),u=i.getDate(),a=/([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,f=a.exec(n);while(f){switch(f[2]||"d"){case"d":case"D":u+=parseInt(f[1],10);break;case"w":case"W":u+=parseInt(f[1],10)*7;break;case"m":case"M":o+=parseInt(f[1],10),u=Math.min(u,e.datepicker._getDaysInMonth(s,o));break;case"y":case"Y":s+=parseInt(f[1],10),u=Math.min(u,e.datepicker._getDaysInMonth(s,o))}f=a.exec(n)}return new Date(s,o,u)},o=n==null||n===""?r:typeof n=="string"?s(n):typeof n=="number"?isNaN(n)?r:i(n):new Date(n.getTime());return o=o&&o.toString()==="Invalid Date"?r:o,o&&(o.setHours(0),o.setMinutes(0),o.setSeconds(0),o.setMilliseconds(0)),this._daylightSavingAdjust(o)},_daylightSavingAdjust:function(e){return e?(e.setHours(e.getHours()>12?e.getHours()+2:0),e):null},_setDate:function(e,t,n){var r=!t,i=e.selectedMonth,s=e.selectedYear,o=this._restrictMinMax(e,this._determineDate(e,t,new Date));e.selectedDay=e.currentDay=o.getDate(),e.drawMonth=e.selectedMonth=e.currentMonth=o.getMonth(),e.drawYear=e.selectedYear=e.currentYear=o.getFullYear(),(i!==e.selectedMonth||s!==e.selectedYear)&&!n&&this._notifyChange(e),this._adjustInstDate(e),e.input&&e.input.val(r?"":this._formatDate(e))},_getDate:function(e){var t=!e.currentYear||e.input&&e.input.val()===""?null:this._daylightSavingAdjust(new Date(e.currentYear,e.currentMonth,e.currentDay));return t},_attachHandlers:function(t){var n=this._get(t,"stepMonths"),i="#"+t.id.replace(/\\\\/g,"\\");t.dpDiv.find("[data-handler]").map(function(){var t={prev:function(){window["DP_jQuery_"+r].datepicker._adjustDate(i,-n,"M")},next:function(){window["DP_jQuery_"+r].datepicker._adjustDate(i,+n,"M")},hide:function(){window["DP_jQuery_"+r].datepicker._hideDatepicker()},today:function(){window["DP_jQuery_"+r].datepicker._gotoToday(i)},selectDay:function(){return window["DP_jQuery_"+r].datepicker._selectDay(i,+this.getAttribute("data-month"),+this.getAttribute("data-year"),this),!1},selectMonth:function(){return window["DP_jQuery_"+r].datepicker._selectMonthYear(i,this,"M"),!1},selectYear:function(){return window["DP_jQuery_"+r].datepicker._selectMonthYear(i,this,"Y"),!1}};e(this).bind(this.getAttribute("data-event"),t[this.getAttribute("data-handler")])})},_generateHTML:function(e){var t,n,r,i,s,o,u,a,f,l,c,h,p,d,v,m,g,y,b,w,E,S,x,T,N,C,k,L,A,O,M,_,D,P,H,B,j,F,I,q=new Date,R=this._daylightSavingAdjust(new Date(q.getFullYear(),q.getMonth(),q.getDate())),U=this._get(e,"isRTL"),z=this._get(e,"showButtonPanel"),W=this._get(e,"hideIfNoPrevNext"),X=this._get(e,"navigationAsDateFormat"),V=this._getNumberOfMonths(e),$=this._get(e,"showCurrentAtPos"),J=this._get(e,"stepMonths"),K=V[0]!==1||V[1]!==1,Q=this._daylightSavingAdjust(e.currentDay?new Date(e.currentYear,e.currentMonth,e.currentDay):new Date(9999,9,9)),G=this._getMinMaxDate(e,"min"),Y=this._getMinMaxDate(e,"max"),Z=e.drawMonth-$,et=e.drawYear;Z<0&&(Z+=12,et--);if(Y){t=this._daylightSavingAdjust(new Date(Y.getFullYear(),Y.getMonth()-V[0]*V[1]+1,Y.getDate())),t=G&&t<G?G:t;while(this._daylightSavingAdjust(new Date(et,Z,1))>t)Z--,Z<0&&(Z=11,et--)}e.drawMonth=Z,e.drawYear=et,n=this._get(e,"prevText"),n=X?this.formatDate(n,this._daylightSavingAdjust(new Date(et,Z-J,1)),this._getFormatConfig(e)):n,r=this._canAdjustMonth(e,-1,et,Z)?"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click' title='"+n+"'><span class='ui-icon ui-icon-circle-triangle-"+(U?"e":"w")+"'>"+n+"</span></a>":W?"":"<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='"+n+"'><span class='ui-icon ui-icon-circle-triangle-"+(U?"e":"w")+"'>"+n+"</span></a>",i=this._get(e,"nextText"),i=X?this.formatDate(i,this._daylightSavingAdjust(new Date(et,Z+J,1)),this._getFormatConfig(e)):i,s=this._canAdjustMonth(e,1,et,Z)?"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click' title='"+i+"'><span class='ui-icon ui-icon-circle-triangle-"+(U?"w":"e")+"'>"+i+"</span></a>":W?"":"<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='"+i+"'><span class='ui-icon ui-icon-circle-triangle-"+(U?"w":"e")+"'>"+i+"</span></a>",o=this._get(e,"currentText"),u=this._get(e,"gotoCurrent")&&e.currentDay?Q:R,o=X?this.formatDate(o,u,this._getFormatConfig(e)):o,a=e.inline?"":"<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>"+this._get(e,"closeText")+"</button>",f=z?"<div class='ui-datepicker-buttonpane ui-widget-content'>"+(U?a:"")+(this._isInRange(e,u)?"<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'>"+o+"</button>":"")+(U?"":a)+"</div>":"",l=parseInt(this._get(e,"firstDay"),10),l=isNaN(l)?0:l,c=this._get(e,"showWeek"),h=this._get(e,"dayNames"),p=this._get(e,"dayNamesMin"),d=this._get(e,"monthNames"),v=this._get(e,"monthNamesShort"),m=this._get(e,"beforeShowDay"),g=this._get(e,"showOtherMonths"),y=this._get(e,"selectOtherMonths"),b=this._getDefaultDate(e),w="",E;for(S=0;S<V[0];S++){x="",this.maxRows=4;for(T=0;T<V[1];T++){N=this._daylightSavingAdjust(new Date(et,Z,e.selectedDay)),C=" ui-corner-all",k="";if(K){k+="<div class='ui-datepicker-group";if(V[1]>1)switch(T){case 0:k+=" ui-datepicker-group-first",C=" ui-corner-"+(U?"right":"left");break;case V[1]-1:k+=" ui-datepicker-group-last",C=" ui-corner-"+(U?"left":"right");break;default:k+=" ui-datepicker-group-middle",C=""}k+="'>"}k+="<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix"+C+"'>"+(/all|left/.test(C)&&S===0?U?s:r:"")+(/all|right/.test(C)&&S===0?U?r:s:"")+this._generateMonthYearHeader(e,Z,et,G,Y,S>0||T>0,d,v)+"</div><table class='ui-datepicker-calendar'><thead>"+"<tr>",L=c?"<th class='ui-datepicker-week-col'>"+this._get(e,"weekHeader")+"</th>":"";for(E=0;E<7;E++)A=(E+l)%7,L+="<th"+((E+l+6)%7>=5?" class='ui-datepicker-week-end'":"")+">"+"<span title='"+h[A]+"'>"+p[A]+"</span></th>";k+=L+"</tr></thead><tbody>",O=this._getDaysInMonth(et,Z),et===e.selectedYear&&Z===e.selectedMonth&&(e.selectedDay=Math.min(e.selectedDay,O)),M=(this._getFirstDayOfMonth(et,Z)-l+7)%7,_=Math.ceil((M+O)/7),D=K?this.maxRows>_?this.maxRows:_:_,this.maxRows=D,P=this._daylightSavingAdjust(new Date(et,Z,1-M));for(H=0;H<D;H++){k+="<tr>",B=c?"<td class='ui-datepicker-week-col'>"+this._get(e,"calculateWeek")(P)+"</td>":"";for(E=0;E<7;E++)j=m?m.apply(e.input?e.input[0]:null,[P]):[!0,""],F=P.getMonth()!==Z,I=F&&!y||!j[0]||G&&P<G||Y&&P>Y,B+="<td class='"+((E+l+6)%7>=5?" ui-datepicker-week-end":"")+(F?" ui-datepicker-other-month":"")+(P.getTime()===N.getTime()&&Z===e.selectedMonth&&e._keyEvent||b.getTime()===P.getTime()&&b.getTime()===N.getTime()?" "+this._dayOverClass:"")+(I?" "+this._unselectableClass+" ui-state-disabled":"")+(F&&!g?"":" "+j[1]+(P.getTime()===Q.getTime()?" "+this._currentClass:"")+(P.getTime()===R.getTime()?" ui-datepicker-today":""))+"'"+((!F||g)&&j[2]?" title='"+j[2].replace(/'/g,"&#39;")+"'":"")+(I?"":" data-handler='selectDay' data-event='click' data-month='"+P.getMonth()+"' data-year='"+P.getFullYear()+"'")+">"+(F&&!g?"&#xa0;":I?"<span class='ui-state-default'>"+P.getDate()+"</span>":"<a class='ui-state-default"+(P.getTime()===R.getTime()?" ui-state-highlight":"")+(P.getTime()===Q.getTime()?" ui-state-active":"")+(F?" ui-priority-secondary":"")+"' href='#'>"+P.getDate()+"</a>")+"</td>",P.setDate(P.getDate()+1),P=this._daylightSavingAdjust(P);k+=B+"</tr>"}Z++,Z>11&&(Z=0,et++),k+="</tbody></table>"+(K?"</div>"+(V[0]>0&&T===V[1]-1?"<div class='ui-datepicker-row-break'></div>":""):""),x+=k}w+=x}return w+=f,e._keyEvent=!1,w},_generateMonthYearHeader:function(e,t,n,r,i,s,o,u){var a,f,l,c,h,p,d,v,m=this._get(e,"changeMonth"),g=this._get(e,"changeYear"),y=this._get(e,"showMonthAfterYear"),b="<div class='ui-datepicker-title'>",w="";if(s||!m)w+="<span class='ui-datepicker-month'>"+o[t]+"</span>";else{a=r&&r.getFullYear()===n,f=i&&i.getFullYear()===n,w+="<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>";for(l=0;l<12;l++)(!a||l>=r.getMonth())&&(!f||l<=i.getMonth())&&(w+="<option value='"+l+"'"+(l===t?" selected='selected'":"")+">"+u[l]+"</option>");w+="</select>"}y||(b+=w+(s||!m||!g?"&#xa0;":""));if(!e.yearshtml){e.yearshtml="";if(s||!g)b+="<span class='ui-datepicker-year'>"+n+"</span>";else{c=this._get(e,"yearRange").split(":"),h=(new Date).getFullYear(),p=function(e){var t=e.match(/c[+\-].*/)?n+parseInt(e.substring(1),10):e.match(/[+\-].*/)?h+parseInt(e,10):parseInt(e,10);return isNaN(t)?h:t},d=p(c[0]),v=Math.max(d,p(c[1]||"")),d=r?Math.max(d,r.getFullYear()):d,v=i?Math.min(v,i.getFullYear()):v,e.yearshtml+="<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";for(;d<=v;d++)e.yearshtml+="<option value='"+d+"'"+(d===n?" selected='selected'":"")+">"+d+"</option>";e.yearshtml+="</select>",b+=e.yearshtml,e.yearshtml=null}}return b+=this._get(e,"yearSuffix"),y&&(b+=(s||!m||!g?"&#xa0;":"")+w),b+="</div>",b},_adjustInstDate:function(e,t,n){var r=e.drawYear+(n==="Y"?t:0),i=e.drawMonth+(n==="M"?t:0),s=Math.min(e.selectedDay,this._getDaysInMonth(r,i))+(n==="D"?t:0),o=this._restrictMinMax(e,this._daylightSavingAdjust(new Date(r,i,s)));e.selectedDay=o.getDate(),e.drawMonth=e.selectedMonth=o.getMonth(),e.drawYear=e.selectedYear=o.getFullYear(),(n==="M"||n==="Y")&&this._notifyChange(e)},_restrictMinMax:function(e,t){var n=this._getMinMaxDate(e,"min"),r=this._getMinMaxDate(e,"max"),i=n&&t<n?n:t;return r&&i>r?r:i},_notifyChange:function(e){var t=this._get(e,"onChangeMonthYear");t&&t.apply(e.input?e.input[0]:null,[e.selectedYear,e.selectedMonth+1,e])},_getNumberOfMonths:function(e){var t=this._get(e,"numberOfMonths");return t==null?[1,1]:typeof t=="number"?[1,t]:t},_getMinMaxDate:function(e,t){return this._determineDate(e,this._get(e,t+"Date"),null)},_getDaysInMonth:function(e,t){return 32-this._daylightSavingAdjust(new Date(e,t,32)).getDate()},_getFirstDayOfMonth:function(e,t){return(new Date(e,t,1)).getDay()},_canAdjustMonth:function(e,t,n,r){var i=this._getNumberOfMonths(e),s=this._daylightSavingAdjust(new Date(n,r+(t<0?t:i[0]*i[1]),1));return t<0&&s.setDate(this._getDaysInMonth(s.getFullYear(),s.getMonth())),this._isInRange(e,s)},_isInRange:function(e,t){var n,r,i=this._getMinMaxDate(e,"min"),s=this._getMinMaxDate(e,"max"),o=null,u=null,a=this._get(e,"yearRange");return a&&(n=a.split(":"),r=(new Date).getFullYear(),o=parseInt(n[0],10),u=parseInt(n[1],10),n[0].match(/[+\-].*/)&&(o+=r),n[1].match(/[+\-].*/)&&(u+=r)),(!i||t.getTime()>=i.getTime())&&(!s||t.getTime()<=s.getTime())&&(!o||t.getFullYear()>=o)&&(!u||t.getFullYear()<=u)},_getFormatConfig:function(e){var t=this._get(e,"shortYearCutoff");return t=typeof t!="string"?t:(new Date).getFullYear()%100+parseInt(t,10),{shortYearCutoff:t,dayNamesShort:this._get(e,"dayNamesShort"),dayNames:this._get(e,"dayNames"),monthNamesShort:this._get(e,"monthNamesShort"),monthNames:this._get(e,"monthNames")}},_formatDate:function(e,t,n,r){t||(e.currentDay=e.selectedDay,e.currentMonth=e.selectedMonth,e.currentYear=e.selectedYear);var i=t?typeof t=="object"?t:this._daylightSavingAdjust(new Date(r,n,t)):this._daylightSavingAdjust(new Date(e.currentYear,e.currentMonth,e.currentDay));return this.formatDate(this._get(e,"dateFormat"),i,this._getFormatConfig(e))}}),e.fn.datepicker=function(t){if(!this.length)return this;e.datepicker.initialized||(e(document).mousedown(e.datepicker._checkExternalClick),e.datepicker.initialized=!0),e("#"+e.datepicker._mainDivId).length===0&&e("body").append(e.datepicker.dpDiv);var n=Array.prototype.slice.call(arguments,1);return typeof t!="string"||t!=="isDisabled"&&t!=="getDate"&&t!=="widget"?t==="option"&&arguments.length===2&&typeof arguments[1]=="string"?e.datepicker["_"+t+"Datepicker"].apply(e.datepicker,[this[0]].concat(n)):this.each(function(){typeof t=="string"?e.datepicker["_"+t+"Datepicker"].apply(e.datepicker,[this].concat(n)):e.datepicker._attachDatepicker(this,t)}):e.datepicker["_"+t+"Datepicker"].apply(e.datepicker,[this[0]].concat(n))},e.datepicker=new s,e.datepicker.initialized=!1,e.datepicker.uuid=(new Date).getTime(),e.datepicker.version="1.10.1",window["DP_jQuery_"+r]=e}(jQuery),function(e,t){var n={buttons:!0,height:!0,maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0,width:!0},r={maxHeight:!0,maxWidth:!0,minHeight:!0,minWidth:!0};e.widget("ui.dialog",{version:"1.10.1",options:{appendTo:"body",autoOpen:!0,buttons:[],closeOnEscape:!0,closeText:"close",dialogClass:"",draggable:!0,hide:null,height:"auto",maxHeight:null,maxWidth:null,minHeight:150,minWidth:150,modal:!1,position:{my:"center",at:"center",of:window,collision:"fit",using:function(t){var n=e(this).css(t).offset().top;n<0&&e(this).css("top",t.top-n)}},resizable:!0,show:null,title:null,width:300,beforeClose:null,close:null,drag:null,dragStart:null,dragStop:null,focus:null,open:null,resize:null,resizeStart:null,resizeStop:null},_create:function(){this.originalCss={display:this.element[0].style.display,width:this.element[0].style.width,minHeight:this.element[0].style.minHeight,maxHeight:this.element[0].style.maxHeight,height:this.element[0].style.height},this.originalPosition={parent:this.element.parent(),index:this.element.parent().children().index(this.element)},this.originalTitle=this.element.attr("title"),this.options.title=this.options.title||this.originalTitle,this._createWrapper(),this.element.show().removeAttr("title").addClass("ui-dialog-content ui-widget-content").appendTo(this.uiDialog),this._createTitlebar(),this._createButtonPane(),this.options.draggable&&e.fn.draggable&&this._makeDraggable(),this.options.resizable&&e.fn.resizable&&this._makeResizable(),this._isOpen=!1},_init:function(){this.options.autoOpen&&this.open()},_appendTo:function(){var t=this.options.appendTo;return t&&(t.jquery||t.nodeType)?e(t):this.document.find(t||"body").eq(0)},_destroy:function(){var e,t=this.originalPosition;this._destroyOverlay(),this.element.removeUniqueId().removeClass("ui-dialog-content ui-widget-content").css(this.originalCss).detach(),this.uiDialog.stop(!0,!0).remove(),this.originalTitle&&this.element.attr("title",this.originalTitle),e=t.parent.children().eq(t.index),e.length&&e[0]!==this.element[0]?e.before(this.element):t.parent.append(this.element)},widget:function(){return this.uiDialog},disable:e.noop,enable:e.noop,close:function(t){var n=this;if(!this._isOpen||this._trigger("beforeClose",t)===!1)return;this._isOpen=!1,this._destroyOverlay(),this.opener.filter(":focusable").focus().length||e(this.document[0].activeElement).blur(),this._hide(this.uiDialog,this.options.hide,function(){n._trigger("close",t)})},isOpen:function(){return this._isOpen},moveToTop:function(){this._moveToTop()},_moveToTop:function(e,t){var n=!!this.uiDialog.nextAll(":visible").insertBefore(this.uiDialog).length;return n&&!t&&this._trigger("focus",e),n},open:function(){var t=this;if(this._isOpen){this._moveToTop()&&this._focusTabbable();return}this._isOpen=!0,this.opener=e(this.document[0].activeElement),this._size(),this._position(),this._createOverlay(),this._moveToTop(null,!0),this._show(this.uiDialog,this.options.show,function(){t._focusTabbable(),t._trigger("focus")}),this._trigger("open")},_focusTabbable:function(){var e=this.element.find("[autofocus]");e.length||(e=this.element.find(":tabbable")),e.length||(e=this.uiDialogButtonPane.find(":tabbable")),e.length||(e=this.uiDialogTitlebarClose.filter(":tabbable")),e.length||(e=this.uiDialog),e.eq(0).focus()},_keepFocus:function(t){function n(){var t=this.document[0].activeElement,n=this.uiDialog[0]===t||e.contains(this.uiDialog[0],t);n||this._focusTabbable()}t.preventDefault(),n.call(this),this._delay(n)},_createWrapper:function(){this.uiDialog=e("<div>").addClass("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front "+this.options.dialogClass).hide().attr({tabIndex:-1,role:"dialog"}).appendTo(this._appendTo()),this._on(this.uiDialog,{keydown:function(t){if(this.options.closeOnEscape&&!t.isDefaultPrevented()&&t.keyCode&&t.keyCode===e.ui.keyCode.ESCAPE){t.preventDefault(),this.close(t);return}if(t.keyCode!==e.ui.keyCode.TAB)return;var n=this.uiDialog.find(":tabbable"),r=n.filter(":first"),i=n.filter(":last");t.target!==i[0]&&t.target!==this.uiDialog[0]||!!t.shiftKey?(t.target===r[0]||t.target===this.uiDialog[0])&&t.shiftKey&&(i.focus(1),t.preventDefault()):(r.focus(1),t.preventDefault())},mousedown:function(e){this._moveToTop(e)&&this._focusTabbable()}}),this.element.find("[aria-describedby]").length||this.uiDialog.attr({"aria-describedby":this.element.uniqueId().attr("id")})},_createTitlebar:function(){var t;this.uiDialogTitlebar=e("<div>").addClass("ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix").prependTo(this.uiDialog),this._on(this.uiDialogTitlebar,{mousedown:function(t){e(t.target).closest(".ui-dialog-titlebar-close")||this.uiDialog.focus()}}),this.uiDialogTitlebarClose=e("<button></button>").button({label:this.options.closeText,icons:{primary:"ui-icon-closethick"},text:!1}).addClass("ui-dialog-titlebar-close").appendTo(this.uiDialogTitlebar),this._on(this.uiDialogTitlebarClose,{click:function(e){e.preventDefault(),this.close(e)}}),t=e("<span>").uniqueId().addClass("ui-dialog-title").prependTo(this.uiDialogTitlebar),this._title(t),this.uiDialog.attr({"aria-labelledby":t.attr("id")})},_title:function(e){this.options.title||e.html("&#160;"),e.text(this.options.title)},_createButtonPane:function(){this.uiDialogButtonPane=e("<div>").addClass("ui-dialog-buttonpane ui-widget-content ui-helper-clearfix"),this.uiButtonSet=e("<div>").addClass("ui-dialog-buttonset").appendTo(this.uiDialogButtonPane),this._createButtons()},_createButtons:function(){var t=this,n=this.options.buttons;this.uiDialogButtonPane.remove(),this.uiButtonSet.empty();if(e.isEmptyObject(n)||e.isArray(n)&&!n.length){this.uiDialog.removeClass("ui-dialog-buttons");return}e.each(n,function(n,r){var i,s;r=e.isFunction(r)?{click:r,text:n}:r,r=e.extend({type:"button"},r),i=r.click,r.click=function(){i.apply(t.element[0],arguments)},s={icons:r.icons,text:r.showText},delete r.icons,delete r.showText,e("<button></button>",r).button(s).appendTo(t.uiButtonSet)}),this.uiDialog.addClass("ui-dialog-buttons"),this.uiDialogButtonPane.appendTo(this.uiDialog)},_makeDraggable:function(){function r(e){return{position:e.position,offset:e.offset}}var t=this,n=this.options;this.uiDialog.draggable({cancel:".ui-dialog-content, .ui-dialog-titlebar-close",handle:".ui-dialog-titlebar",containment:"document",start:function(n,i){e(this).addClass("ui-dialog-dragging"),t._blockFrames(),t._trigger("dragStart",n,r(i))},drag:function(e,n){t._trigger("drag",e,r(n))},stop:function(i,s){n.position=[s.position.left-t.document.scrollLeft(),s.position.top-t.document.scrollTop()],e(this).removeClass("ui-dialog-dragging"),t._unblockFrames(),t._trigger("dragStop",i,r(s))}})},_makeResizable:function(){function o(e){return{originalPosition:e.originalPosition,originalSize:e.originalSize,position:e.position,size:e.size}}var t=this,n=this.options,r=n.resizable,i=this.uiDialog.css("position"),s=typeof r=="string"?r:"n,e,s,w,se,sw,ne,nw";this.uiDialog.resizable({cancel:".ui-dialog-content",containment:"document",alsoResize:this.element,maxWidth:n.maxWidth,maxHeight:n.maxHeight,minWidth:n.minWidth,minHeight:this._minHeight(),handles:s,start:function(n,r){e(this).addClass("ui-dialog-resizing"),t._blockFrames(),t._trigger("resizeStart",n,o(r))},resize:function(e,n){t._trigger("resize",e,o(n))},stop:function(r,i){n.height=e(this).height(),n.width=e(this).width(),e(this).removeClass("ui-dialog-resizing"),t._unblockFrames(),t._trigger("resizeStop",r,o(i))}}).css("position",i)},_minHeight:function(){var e=this.options;return e.height==="auto"?e.minHeight:Math.min(e.minHeight,e.height)},_position:function(){var e=this.uiDialog.is(":visible");e||this.uiDialog.show(),this.uiDialog.position(this.options.position),e||this.uiDialog.hide()},_setOptions:function(t){var i=this,s=!1,o={};e.each(t,function(e,t){i._setOption(e,t),e in n&&(s=!0),e in r&&(o[e]=t)}),s&&(this._size(),this._position()),this.uiDialog.is(":data(ui-resizable)")&&this.uiDialog.resizable("option",o)},_setOption:function(e,t){var n,r,i=this.uiDialog;e==="dialogClass"&&i.removeClass(this.options.dialogClass).addClass(t);if(e==="disabled")return;this._super(e,t),e==="appendTo"&&this.uiDialog.appendTo(this._appendTo()),e==="buttons"&&this._createButtons(),e==="closeText"&&this.uiDialogTitlebarClose.button({label:""+t}),e==="draggable"&&(n=i.is(":data(ui-draggable)"),n&&!t&&i.draggable("destroy"),!n&&t&&this._makeDraggable()),e==="position"&&this._position(),e==="resizable"&&(r=i.is(":data(ui-resizable)"),r&&!t&&i.resizable("destroy"),r&&typeof t=="string"&&i.resizable("option","handles",t),!r&&t!==!1&&this._makeResizable()),e==="title"&&this._title(this.uiDialogTitlebar.find(".ui-dialog-title"))},_size:function(){var e,t,n,r=this.options;this.element.show().css({width:"auto",minHeight:0,maxHeight:"none",height:0}),r.minWidth>r.width&&(r.width=r.minWidth),e=this.uiDialog.css({height:"auto",width:r.width}).outerHeight(),t=Math.max(0,r.minHeight-e),n=typeof r.maxHeight=="number"?Math.max(0,r.maxHeight-e):"none",r.height==="auto"?this.element.css({minHeight:t,maxHeight:n,height:"auto"}):this.element.height(Math.max(0,r.height-e)),this.uiDialog.is(":data(ui-resizable)")&&this.uiDialog.resizable("option","minHeight",this._minHeight())},_blockFrames:function(){this.iframeBlocks=this.document.find("iframe").map(function(){var t=e(this);return e("<div>").css({position:"absolute",width:t.outerWidth(),height:t.outerHeight()}).appendTo(t.parent()).offset(t.offset())[0]})},_unblockFrames:function(){this.iframeBlocks&&(this.iframeBlocks.remove(),delete this.iframeBlocks)},_createOverlay:function(){if(!this.options.modal)return;e.ui.dialog.overlayInstances||this._delay(function(){e.ui.dialog.overlayInstances&&this.document.bind("focusin.dialog",function(t){!e(t.target).closest(".ui-dialog").length&&!e(t.target).closest(".ui-datepicker").length&&(t.preventDefault(),e(".ui-dialog:visible:last .ui-dialog-content").data("ui-dialog")._focusTabbable())})}),this.overlay=e("<div>").addClass("ui-widget-overlay ui-front").appendTo(this._appendTo()),this._on(this.overlay,{mousedown:"_keepFocus"}),e.ui.dialog.overlayInstances++},_destroyOverlay:function(){if(!this.options.modal)return;this.overlay&&(e.ui.dialog.overlayInstances--,e.ui.dialog.overlayInstances||this.document.unbind("focusin.dialog"),this.overlay.remove(),this.overlay=null)}}),e.ui.dialog.overlayInstances=0,e.uiBackCompat!==!1&&e.widget("ui.dialog",e.ui.dialog,{_position:function(){var t=this.options.position,n=[],r=[0,0],i;if(t){if(typeof t=="string"||typeof t=="object"&&"0"in t)n=t.split?t.split(" "):[t[0],t[1]],n.length===1&&(n[1]=n[0]),e.each(["left","top"],function(e,t){+n[e]===n[e]&&(r[e]=n[e],n[e]=t)}),t={my:n[0]+(r[0]<0?r[0]:"+"+r[0])+" "+n[1]+(r[1]<0?r[1]:"+"+r[1]),at:n.join(" ")};t=e.extend({},e.ui.dialog.prototype.options.position,t)}else t=e.ui.dialog.prototype.options.position;i=this.uiDialog.is(":visible"),i||this.uiDialog.show(),this.uiDialog.position(t),i||this.uiDialog.hide()}})}(jQuery),function(e,t){var n=/up|down|vertical/,r=/up|left|vertical|horizontal/;e.effects.effect.blind=function(t,i){var s=e(this),o=["position","top","bottom","left","right","height","width"],u=e.effects.setMode(s,t.mode||"hide"),a=t.direction||"up",f=n.test(a),l=f?"height":"width",c=f?"top":"left",h=r.test(a),p={},d=u==="show",v,m,g;s.parent().is(".ui-effects-wrapper")?e.effects.save(s.parent(),o):e.effects.save(s,o),s.show(),v=e.effects.createWrapper(s).css({overflow:"hidden"}),m=v[l](),g=parseFloat(v.css(c))||0,p[l]=d?m:0,h||(s.css(f?"bottom":"right",0).css(f?"top":"left","auto").css({position:"absolute"}),p[c]=d?g:m+g),d&&(v.css(l,0),h||v.css(c,g+m)),v.animate(p,{duration:t.duration,easing:t.easing,queue:!1,complete:function(){u==="hide"&&s.hide(),e.effects.restore(s,o),e.effects.removeWrapper(s),i()}})}}(jQuery),function(e,t){e.effects.effect.bounce=function(t,n){var r=e(this),i=["position","top","bottom","left","right","height","width"],s=e.effects.setMode(r,t.mode||"effect"),o=s==="hide",u=s==="show",a=t.direction||"up",f=t.distance,l=t.times||5,c=l*2+(u||o?1:0),h=t.duration/c,p=t.easing,d=a==="up"||a==="down"?"top":"left",v=a==="up"||a==="left",m,g,y,b=r.queue(),w=b.length;(u||o)&&i.push("opacity"),e.effects.save(r,i),r.show(),e.effects.createWrapper(r),f||(f=r[d==="top"?"outerHeight":"outerWidth"]()/3),u&&(y={opacity:1},y[d]=0,r.css("opacity",0).css(d,v?-f*2:f*2).animate(y,h,p)),o&&(f/=Math.pow(2,l-1)),y={},y[d]=0;for(m=0;m<l;m++)g={},g[d]=(v?"-=":"+=")+f,r.animate(g,h,p).animate(y,h,p),f=o?f*2:f/2;o&&(g={opacity:0},g[d]=(v?"-=":"+=")+f,r.animate(g,h,p)),r.queue(function(){o&&r.hide(),e.effects.restore(r,i),e.effects.removeWrapper(r),n()}),w>1&&b.splice.apply(b,[1,0].concat(b.splice(w,c+1))),r.dequeue()}}(jQuery),function(e,t){e.effects.effect.clip=function(t,n){var r=e(this),i=["position","top","bottom","left","right","height","width"],s=e.effects.setMode(r,t.mode||"hide"),o=s==="show",u=t.direction||"vertical",a=u==="vertical",f=a?"height":"width",l=a?"top":"left",c={},h,p,d;e.effects.save(r,i),r.show(),h=e.effects.createWrapper(r).css({overflow:"hidden"}),p=r[0].tagName==="IMG"?h:r,d=p[f](),o&&(p.css(f,0),p.css(l,d/2)),c[f]=o?d:0,c[l]=o?0:d/2,p.animate(c,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){o||r.hide(),e.effects.restore(r,i),e.effects.removeWrapper(r),n()}})}}(jQuery),function(e,t){e.effects.effect.drop=function(t,n){var r=e(this),i=["position","top","bottom","left","right","opacity","height","width"],s=e.effects.setMode(r,t.mode||"hide"),o=s==="show",u=t.direction||"left",a=u==="up"||u==="down"?"top":"left",f=u==="up"||u==="left"?"pos":"neg",l={opacity:o?1:0},c;e.effects.save(r,i),r.show(),e.effects.createWrapper(r),c=t.distance||r[a==="top"?"outerHeight":"outerWidth"](!0)/2,o&&r.css("opacity",0).css(a,f==="pos"?-c:c),l[a]=(o?f==="pos"?"+=":"-=":f==="pos"?"-=":"+=")+c,r.animate(l,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){s==="hide"&&r.hide(),e.effects.restore(r,i),e.effects.removeWrapper(r),n()}})}}(jQuery),function(e,t){e.effects.effect.explode=function(t,n){function y(){c.push(this),c.length===r*i&&b()}function b(){s.css({visibility:"visible"}),e(c).remove(),u||s.hide(),n()}var r=t.pieces?Math.round(Math.sqrt(t.pieces)):3,i=r,s=e(this),o=e.effects.setMode(s,t.mode||"hide"),u=o==="show",a=s.show().css("visibility","hidden").offset(),f=Math.ceil(s.outerWidth()/i),l=Math.ceil(s.outerHeight()/r),c=[],h,p,d,v,m,g;for(h=0;h<r;h++){v=a.top+h*l,g=h-(r-1)/2;for(p=0;p<i;p++)d=a.left+p*f,m=p-(i-1)/2,s.clone().appendTo("body").wrap("<div></div>").css({position:"absolute",visibility:"visible",left:-p*f,top:-h*l}).parent().addClass("ui-effects-explode").css({position:"absolute",overflow:"hidden",width:f,height:l,left:d+(u?m*f:0),top:v+(u?g*l:0),opacity:u?0:1}).animate({left:d+(u?0:m*f),top:v+(u?0:g*l),opacity:u?1:0},t.duration||500,t.easing,y)}}}(jQuery),function(e,t){e.effects.effect.fade=function(t,n){var r=e(this),i=e.effects.setMode(r,t.mode||"toggle");r.animate({opacity:i},{queue:!1,duration:t.duration,easing:t.easing,complete:n})}}(jQuery),function(e,t){e.effects.effect.fold=function(t,n){var r=e(this),i=["position","top","bottom","left","right","height","width"],s=e.effects.setMode(r,t.mode||"hide"),o=s==="show",u=s==="hide",a=t.size||15,f=/([0-9]+)%/.exec(a),l=!!t.horizFirst,c=o!==l,h=c?["width","height"]:["height","width"],p=t.duration/2,d,v,m={},g={};e.effects.save(r,i),r.show(),d=e.effects.createWrapper(r).css({overflow:"hidden"}),v=c?[d.width(),d.height()]:[d.height(),d.width()],f&&(a=parseInt(f[1],10)/100*v[u?0:1]),o&&d.css(l?{height:0,width:a}:{height:a,width:0}),m[h[0]]=o?v[0]:a,g[h[1]]=o?v[1]:0,d.animate(m,p,t.easing).animate(g,p,t.easing,function(){u&&r.hide(),e.effects.restore(r,i),e.effects.removeWrapper(r),n()})}}(jQuery),function(e,t){e.effects.effect.highlight=function(t,n){var r=e(this),i=["backgroundImage","backgroundColor","opacity"],s=e.effects.setMode(r,t.mode||"show"),o={backgroundColor:r.css("backgroundColor")};s==="hide"&&(o.opacity=0),e.effects.save(r,i),r.show().css({backgroundImage:"none",backgroundColor:t.color||"#ffff99"}).animate(o,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){s==="hide"&&r.hide(),e.effects.restore(r,i),n()}})}}(jQuery),function(e,t){e.effects.effect.pulsate=function(t,n){var r=e(this),i=e.effects.setMode(r,t.mode||"show"),s=i==="show",o=i==="hide",u=s||i==="hide",a=(t.times||5)*2+(u?1:0),f=t.duration/a,l=0,c=r.queue(),h=c.length,p;if(s||!r.is(":visible"))r.css("opacity",0).show(),l=1;for(p=1;p<a;p++)r.animate({opacity:l},f,t.easing),l=1-l;r.animate({opacity:l},f,t.easing),r.queue(function(){o&&r.hide(),n()}),h>1&&c.splice.apply(c,[1,0].concat(c.splice(h,a+1))),r.dequeue()}}(jQuery),function(e,t){e.effects.effect.puff=function(t,n){var r=e(this),i=e.effects.setMode(r,t.mode||"hide"),s=i==="hide",o=parseInt(t.percent,10)||150,u=o/100,a={height:r.height(),width:r.width(),outerHeight:r.outerHeight(),outerWidth:r.outerWidth()};e.extend(t,{effect:"scale",queue:!1,fade:!0,mode:i,complete:n,percent:s?o:100,from:s?a:{height:a.height*u,width:a.width*u,outerHeight:a.outerHeight*u,outerWidth:a.outerWidth*u}}),r.effect(t)},e.effects.effect.scale=function(t,n){var r=e(this),i=e.extend(!0,{},t),s=e.effects.setMode(r,t.mode||"effect"),o=parseInt(t.percent,10)||(parseInt(t.percent,10)===0?0:s==="hide"?0:100),u=t.direction||"both",a=t.origin,f={height:r.height(),width:r.width(),outerHeight:r.outerHeight(),outerWidth:r.outerWidth()},l={y:u!=="horizontal"?o/100:1,x:u!=="vertical"?o/100:1};i.effect="size",i.queue=!1,i.complete=n,s!=="effect"&&(i.origin=a||["middle","center"],i.restore=!0),i.from=t.from||(s==="show"?{height:0,width:0,outerHeight:0,outerWidth:0}:f),i.to={height:f.height*l.y,width:f.width*l.x,outerHeight:f.outerHeight*l.y,outerWidth:f.outerWidth*l.x},i.fade&&(s==="show"&&(i.from.opacity=0,i.to.opacity=1),s==="hide"&&(i.from.opacity=1,i.to.opacity=0)),r.effect(i)},e.effects.effect.size=function(t,n){var r,i,s,o=e(this),u=["position","top","bottom","left","right","width","height","overflow","opacity"],a=["position","top","bottom","left","right","overflow","opacity"],f=["width","height","overflow"],l=["fontSize"],c=["borderTopWidth","borderBottomWidth","paddingTop","paddingBottom"],h=["borderLeftWidth","borderRightWidth","paddingLeft","paddingRight"],p=e.effects.setMode(o,t.mode||"effect"),d=t.restore||p!=="effect",v=t.scale||"both",m=t.origin||["middle","center"],g=o.css("position"),y=d?u:a,b={height:0,width:0,outerHeight:0,outerWidth:0};p==="show"&&o.show(),r={height:o.height(),width:o.width(),outerHeight:o.outerHeight(),outerWidth:o.outerWidth()},t.mode==="toggle"&&p==="show"?(o.from=t.to||b,o.to=t.from||r):(o.from=t.from||(p==="show"?b:r),o.to=t.to||(p==="hide"?b:r)),s={from:{y:o.from.height/r.height,x:o.from.width/r.width},to:{y:o.to.height/r.height,x:o.to.width/r.width}};if(v==="box"||v==="both")s.from.y!==s.to.y&&(y=y.concat(c),o.from=e.effects.setTransition(o,c,s.from.y,o.from),o.to=e.effects.setTransition(o,c,s.to.y,o.to)),s.from.x!==s.to.x&&(y=y.concat(h),o.from=e.effects.setTransition(o,h,s.from.x,o.from),o.to=e.effects.setTransition(o,h,s.to.x,o.to));(v==="content"||v==="both")&&s.from.y!==s.to.y&&(y=y.concat(l).concat(f),o.from=e.effects.setTransition(o,l,s.from.y,o.from),o.to=e.effects.setTransition(o,l,s.to.y,o.to)),e.effects.save(o,y),o.show(),e.effects.createWrapper(o),o.css("overflow","hidden").css(o.from),m&&(i=e.effects.getBaseline(m,r),o.from.top=(r.outerHeight-o.outerHeight())*i.y,o.from.left=(r.outerWidth-o.outerWidth())*i.x,o.to.top=(r.outerHeight-o.to.outerHeight)*i.y,o.to.left=(r.outerWidth-o.to.outerWidth)*i.x),o.css(o.from);if(v==="content"||v==="both")c=c.concat(["marginTop","marginBottom"]).concat(l),h=h.concat(["marginLeft","marginRight"]),f=u.concat(c).concat(h),o.find("*[width]").each(function(){var n=e(this),r={height:n.height(),width:n.width(),outerHeight:n.outerHeight(),outerWidth:n.outerWidth()};d&&e.effects.save(n,f),n.from={height:r.height*s.from.y,width:r.width*s.from.x,outerHeight:r.outerHeight*s.from.y,outerWidth:r.outerWidth*s.from.x},n.to={height:r.height*s.to.y,width:r.width*s.to.x,outerHeight:r.height*s.to.y,outerWidth:r.width*s.to.x},s.from.y!==s.to.y&&(n.from=e.effects.setTransition(n,c,s.from.y,n.from),n.to=e.effects.setTransition(n,c,s.to.y,n.to)),s.from.x!==s.to.x&&(n.from=e.effects.setTransition(n,h,s.from.x,n.from),n.to=e.effects.setTransition(n,h,s.to.x,n.to)),n.css(n.from),n.animate(n.to,t.duration,t.easing,function(){d&&e.effects.restore(n,f)})});o.animate(o.to,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){o.to.opacity===0&&o.css("opacity",o.from.opacity),p==="hide"&&o.hide(),e.effects.restore(o,y),d||(g==="static"?o.css({position:"relative",top:o.to.top,left:o.to.left}):e.each(["top","left"],function(e,t){o.css(t,function(t,n){var r=parseInt(n,10),i=e?o.to.left:o.to.top;return n==="auto"?i+"px":r+i+"px"})})),e.effects.removeWrapper(o),n()}})}}(jQuery),function(e,t){e.effects.effect.shake=function(t,n){var r=e(this),i=["position","top","bottom","left","right","height","width"],s=e.effects.setMode(r,t.mode||"effect"),o=t.direction||"left",u=t.distance||20,a=t.times||3,f=a*2+1,l=Math.round(t.duration/f),c=o==="up"||o==="down"?"top":"left",h=o==="up"||o==="left",p={},d={},v={},m,g=r.queue(),y=g.length;e.effects.save(r,i),r.show(),e.effects.createWrapper(r),p[c]=(h?"-=":"+=")+u,d[c]=(h?"+=":"-=")+u*2,v[c]=(h?"-=":"+=")+u*2,r.animate(p,l,t.easing);for(m=1;m<a;m++)r.animate(d,l,t.easing).animate(v,l,t.easing);r.animate(d,l,t.easing).animate(p,l/2,t.easing).queue(function(){s==="hide"&&r.hide(),e.effects.restore(r,i),e.effects.removeWrapper(r),n()}),y>1&&g.splice.apply(g,[1,0].concat(g.splice(y,f+1))),r.dequeue()}}(jQuery),function(e,t){e.effects.effect.slide=function(t,n){var r=e(this),i=["position","top","bottom","left","right","width","height"],s=e.effects.setMode(r,t.mode||"show"),o=s==="show",u=t.direction||"left",a=u==="up"||u==="down"?"top":"left",f=u==="up"||u==="left",l,c={};e.effects.save(r,i),r.show(),l=t.distance||r[a==="top"?"outerHeight":"outerWidth"](!0),e.effects.createWrapper(r).css({overflow:"hidden"}),o&&r.css(a,f?isNaN(l)?"-"+l:-l:l),c[a]=(o?f?"+=":"-=":f?"-=":"+=")+l,r.animate(c,{queue:!1,duration:t.duration,easing:t.easing,complete:function(){s==="hide"&&r.hide(),e.effects.restore(r,i),e.effects.removeWrapper(r),n()}})}}(jQuery),function(e,t){e.effects.effect.transfer=function(t,n){var r=e(this),i=e(t.to),s=i.css("position")==="fixed",o=e("body"),u=s?o.scrollTop():0,a=s?o.scrollLeft():0,f=i.offset(),l={top:f.top-u,left:f.left-a,height:i.innerHeight(),width:i.innerWidth()},c=r.offset(),h=e("<div class='ui-effects-transfer'></div>").appendTo(document.body).addClass(t.className).css({top:c.top-u,left:c.left-a,height:r.innerHeight(),width:r.innerWidth(),position:s?"fixed":"absolute"}).animate(l,t.duration,t.easing,function(){h.remove(),n()})}}(jQuery),function(e,t){e.widget("ui.menu",{version:"1.10.1",defaultElement:"<ul>",delay:300,options:{icons:{submenu:"ui-icon-carat-1-e"},menus:"ul",position:{my:"left top",at:"right top"},role:"menu",blur:null,focus:null,select:null},_create:function(){this.activeMenu=this.element,this.mouseHandled=!1,this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content ui-corner-all").toggleClass("ui-menu-icons",!!this.element.find(".ui-icon").length).attr({role:this.options.role,tabIndex:0}).bind("click"+this.eventNamespace,e.proxy(function(e){this.options.disabled&&e.preventDefault()},this)),this.options.disabled&&this.element.addClass("ui-state-disabled").attr("aria-disabled","true"),this._on({"mousedown .ui-menu-item > a":function(e){e.preventDefault()},"click .ui-state-disabled > a":function(e){e.preventDefault()},"click .ui-menu-item:has(a)":function(t){var n=e(t.target).closest(".ui-menu-item");!this.mouseHandled&&n.not(".ui-state-disabled").length&&(this.mouseHandled=!0,this.select(t),n.has(".ui-menu").length?this.expand(t):this.element.is(":focus")||(this.element.trigger("focus",[!0]),this.active&&this.active.parents(".ui-menu").length===1&&clearTimeout(this.timer)))},"mouseenter .ui-menu-item":function(t){var n=e(t.currentTarget);n.siblings().children(".ui-state-active").removeClass("ui-state-active"),this.focus(t,n)},mouseleave:"collapseAll","mouseleave .ui-menu":"collapseAll",focus:function(e,t){var n=this.active||this.element.children(".ui-menu-item").eq(0);t||this.focus(e,n)},blur:function(t){this._delay(function(){e.contains(this.element[0],this.document[0].activeElement)||this.collapseAll(t)})},keydown:"_keydown"}),this.refresh(),this._on(this.document,{click:function(t){e(t.target).closest(".ui-menu").length||this.collapseAll(t),this.mouseHandled=!1}})},_destroy:function(){this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-corner-all ui-menu-icons").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show(),this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").children("a").removeUniqueId().removeClass("ui-corner-all ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function(){var t=e(this);t.data("ui-menu-submenu-carat")&&t.remove()}),this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")},_keydown:function(t){function a(e){return e.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g,"\\$&")}var n,r,i,s,o,u=!0;switch(t.keyCode){case e.ui.keyCode.PAGE_UP:this.previousPage(t);break;case e.ui.keyCode.PAGE_DOWN:this.nextPage(t);break;case e.ui.keyCode.HOME:this._move("first","first",t);break;case e.ui.keyCode.END:this._move("last","last",t);break;case e.ui.keyCode.UP:this.previous(t);break;case e.ui.keyCode.DOWN:this.next(t);break;case e.ui.keyCode.LEFT:this.collapse(t);break;case e.ui.keyCode.RIGHT:this.active&&!this.active.is(".ui-state-disabled")&&this.expand(t);break;case e.ui.keyCode.ENTER:case e.ui.keyCode.SPACE:this._activate(t);break;case e.ui.keyCode.ESCAPE:this.collapse(t);break;default:u=!1,r=this.previousFilter||"",i=String.fromCharCode(t.keyCode),s=!1,clearTimeout(this.filterTimer),i===r?s=!0:i=r+i,o=new RegExp("^"+a(i),"i"),n=this.activeMenu.children(".ui-menu-item").filter(function(){return o.test(e(this).children("a").text())}),n=s&&n.index(this.active.next())!==-1?this.active.nextAll(".ui-menu-item"):n,n.length||(i=String.fromCharCode(t.keyCode),o=new RegExp("^"+a(i),"i"),n=this.activeMenu.children(".ui-menu-item").filter(function(){return o.test(e(this).children("a").text())})),n.length?(this.focus(t,n),n.length>1?(this.previousFilter=i,this.filterTimer=this._delay(function(){delete this.previousFilter},1e3)):delete this.previousFilter):delete this.previousFilter}u&&t.preventDefault()},_activate:function(e){this.active.is(".ui-state-disabled")||(this.active.children("a[aria-haspopup='true']").length?this.expand(e):this.select(e))},refresh:function(){var t,n=this.options.icons.submenu,r=this.element.find(this.options.menus);r.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-corner-all").hide().attr({role:this.options.role,"aria-hidden":"true","aria-expanded":"false"}).each(function(){var t=e(this),r=t.prev("a"),i=e("<span>").addClass("ui-menu-icon ui-icon "+n).data("ui-menu-submenu-carat",!0);r.attr("aria-haspopup","true").prepend(i),t.attr("aria-labelledby",r.attr("id"))}),t=r.add(this.element),t.children(":not(.ui-menu-item):has(a)").addClass("ui-menu-item").attr("role","presentation").children("a").uniqueId().addClass("ui-corner-all").attr({tabIndex:-1,role:this._itemRole()}),t.children(":not(.ui-menu-item)").each(function(){var t=e(this);/[^\-\u2014\u2013\s]/.test(t.text())||t.addClass("ui-widget-content ui-menu-divider")}),t.children(".ui-state-disabled").attr("aria-disabled","true"),this.active&&!e.contains(this.element[0],this.active[0])&&this.blur()},_itemRole:function(){return{menu:"menuitem",listbox:"option"}[this.options.role]},_setOption:function(e,t){e==="icons"&&this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(t.submenu),this._super(e,t)},focus:function(e,t){var n,r;this.blur(e,e&&e.type==="focus"),this._scrollIntoView(t),this.active=t.first(),r=this.active.children("a").addClass("ui-state-focus"),this.options.role&&this.element.attr("aria-activedescendant",r.attr("id")),this.active.parent().closest(".ui-menu-item").children("a:first").addClass("ui-state-active"),e&&e.type==="keydown"?this._close():this.timer=this._delay(function(){this._close()},this.delay),n=t.children(".ui-menu"),n.length&&/^mouse/.test(e.type)&&this._startOpening(n),this.activeMenu=t.parent(),this._trigger("focus",e,{item:t})},_scrollIntoView:function(t){var n,r,i,s,o,u;this._hasScroll()&&(n=parseFloat(e.css(this.activeMenu[0],"borderTopWidth"))||0,r=parseFloat(e.css(this.activeMenu[0],"paddingTop"))||0,i=t.offset().top-this.activeMenu.offset().top-n-r,s=this.activeMenu.scrollTop(),o=this.activeMenu.height(),u=t.height(),i<0?this.activeMenu.scrollTop(s+i):i+u>o&&this.activeMenu.scrollTop(s+i-o+u))},blur:function(e,t){t||clearTimeout(this.timer);if(!this.active)return;this.active.children("a").removeClass("ui-state-focus"),this.active=null,this._trigger("blur",e,{item:this.active})},_startOpening:function(e){clearTimeout(this.timer);if(e.attr("aria-hidden")!=="true")return;this.timer=this._delay(function(){this._close(),this._open(e)},this.delay)},_open:function(t){var n=e.extend({of:this.active},this.options.position);clearTimeout(this.timer),this.element.find(".ui-menu").not(t.parents(".ui-menu")).hide().attr("aria-hidden","true"),t.show().removeAttr("aria-hidden").attr("aria-expanded","true").position(n)},collapseAll:function(t,n){clearTimeout(this.timer),this.timer=this._delay(function(){var r=n?this.element:e(t&&t.target).closest(this.element.find(".ui-menu"));r.length||(r=this.element),this._close(r),this.blur(t),this.activeMenu=r},this.delay)},_close:function(e){e||(e=this.active?this.active.parent():this.element),e.find(".ui-menu").hide().attr("aria-hidden","true").attr("aria-expanded","false").end().find("a.ui-state-active").removeClass("ui-state-active")},collapse:function(e){var t=this.active&&this.active.parent().closest(".ui-menu-item",this.element);t&&t.length&&(this._close(),this.focus(e,t))},expand:function(e){var t=this.active&&this.active.children(".ui-menu ").children(".ui-menu-item").first();t&&t.length&&(this._open(t.parent()),this._delay(function(){this.focus(e,t)}))},next:function(e){this._move("next","first",e)},previous:function(e){this._move("prev","last",e)},isFirstItem:function(){return this.active&&!this.active.prevAll(".ui-menu-item").length},isLastItem:function(){return this.active&&!this.active.nextAll(".ui-menu-item").length},_move:function(e,t,n){var r;this.active&&(e==="first"||e==="last"?r=this.active[e==="first"?"prevAll":"nextAll"](".ui-menu-item").eq(-1):r=this.active[e+"All"](".ui-menu-item").eq(0));if(!r||!r.length||!this.active)r=this.activeMenu.children(".ui-menu-item")[t]();this.focus(n,r)},nextPage:function(t){var n,r,i;if(!this.active){this.next(t);return}if(this.isLastItem())return;this._hasScroll()?(r=this.active.offset().top,i=this.element.height(),this.active.nextAll(".ui-menu-item").each(function(){return n=e(this),n.offset().top-r-i<0}),this.focus(t,n)):this.focus(t,this.activeMenu.children(".ui-menu-item")[this.active?"last":"first"]())},previousPage:function(t){var n,r,i;if(!this.active){this.next(t);return}if(this.isFirstItem())return;this._hasScroll()?(r=this.active.offset().top,i=this.element.height(),this.active.prevAll(".ui-menu-item").each(function(){return n=e(this),n.offset().top-r+i>0}),this.focus(t,n)):this.focus(t,this.activeMenu.children(".ui-menu-item").first())},_hasScroll:function(){return this.element.outerHeight()<this.element.prop("scrollHeight")},select:function(t){this.active=this.active||e(t.target).closest(".ui-menu-item");var n={item:this.active};this.active.has(".ui-menu").length||this.collapseAll(t,!0),this._trigger("select",t,n)}})}(jQuery),function(e,t){function h(e,t,n){return[parseFloat(e[0])*(l.test(e[0])?t/100:1),parseFloat(e[1])*(l.test(e[1])?n/100:1)]}function p(t,n){return parseInt(e.css(t,n),10)||0}function d(t){var n=t[0];return n.nodeType===9?{width:t.width(),height:t.height(),offset:{top:0,left:0}}:e.isWindow(n)?{width:t.width(),height:t.height(),offset:{top:t.scrollTop(),left:t.scrollLeft()}}:n.preventDefault?{width:0,height:0,offset:{top:n.pageY,left:n.pageX}}:{width:t.outerWidth(),height:t.outerHeight(),offset:t.offset()}}e.ui=e.ui||{};var n,r=Math.max,i=Math.abs,s=Math.round,o=/left|center|right/,u=/top|center|bottom/,a=/[\+\-]\d+(\.[\d]+)?%?/,f=/^\w+/,l=/%$/,c=e.fn.position;e.position={scrollbarWidth:function(){if(n!==t)return n;var r,i,s=e("<div style='display:block;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'></div></div>"),o=s.children()[0];return e("body").append(s),r=o.offsetWidth,s.css("overflow","scroll"),i=o.offsetWidth,r===i&&(i=s[0].clientWidth),s.remove(),n=r-i},getScrollInfo:function(t){var n=t.isWindow?"":t.element.css("overflow-x"),r=t.isWindow?"":t.element.css("overflow-y"),i=n==="scroll"||n==="auto"&&t.width<t.element[0].scrollWidth,s=r==="scroll"||r==="auto"&&t.height<t.element[0].scrollHeight;return{width:i?e.position.scrollbarWidth():0,height:s?e.position.scrollbarWidth():0}},getWithinInfo:function(t){var n=e(t||window),r=e.isWindow(n[0]);return{element:n,isWindow:r,offset:n.offset()||{left:0,top:0},scrollLeft:n.scrollLeft(),scrollTop:n.scrollTop(),width:r?n.width():n.outerWidth(),height:r?n.height():n.outerHeight()}}},e.fn.position=function(t){if(!t||!t.of)return c.apply(this,arguments);t=e.extend({},t);var n,l,v,m,g,y,b=e(t.of),w=e.position.getWithinInfo(t.within),E=e.position.getScrollInfo(w),S=(t.collision||"flip").split(" "),x={};return y=d(b),b[0].preventDefault&&(t.at="left top"),l=y.width,v=y.height,m=y.offset,g=e.extend({},m),e.each(["my","at"],function(){var e=(t[this]||"").split(" "),n,r;e.length===1&&(e=o.test(e[0])?e.concat(["center"]):u.test(e[0])?["center"].concat(e):["center","center"]),e[0]=o.test(e[0])?e[0]:"center",e[1]=u.test(e[1])?e[1]:"center",n=a.exec(e[0]),r=a.exec(e[1]),x[this]=[n?n[0]:0,r?r[0]:0],t[this]=[f.exec(e[0])[0],f.exec(e[1])[0]]}),S.length===1&&(S[1]=S[0]),t.at[0]==="right"?g.left+=l:t.at[0]==="center"&&(g.left+=l/2),t.at[1]==="bottom"?g.top+=v:t.at[1]==="center"&&(g.top+=v/2),n=h(x.at,l,v),g.left+=n[0],g.top+=n[1],this.each(function(){var o,u,a=e(this),f=a.outerWidth(),c=a.outerHeight(),d=p(this,"marginLeft"),y=p(this,"marginTop"),T=f+d+p(this,"marginRight")+E.width,N=c+y+p(this,"marginBottom")+E.height,C=e.extend({},g),k=h(x.my,a.outerWidth(),a.outerHeight());t.my[0]==="right"?C.left-=f:t.my[0]==="center"&&(C.left-=f/2),t.my[1]==="bottom"?C.top-=c:t.my[1]==="center"&&(C.top-=c/2),C.left+=k[0],C.top+=k[1],e.support.offsetFractions||(C.left=s(C.left),C.top=s(C.top)),o={marginLeft:d,marginTop:y},e.each(["left","top"],function(r,i){e.ui.position[S[r]]&&e.ui.position[S[r]][i](C,{targetWidth:l,targetHeight:v,elemWidth:f,elemHeight:c,collisionPosition:o,collisionWidth:T,collisionHeight:N,offset:[n[0]+k[0],n[1]+k[1]],my:t.my,at:t.at,within:w,elem:a})}),t.using&&(u=function(e){var n=m.left-C.left,s=n+l-f,o=m.top-C.top,u=o+v-c,h={target:{element:b,left:m.left,top:m.top,width:l,height:v},element:{element:a,left:C.left,top:C.top,width:f,height:c},horizontal:s<0?"left":n>0?"right":"center",vertical:u<0?"top":o>0?"bottom":"middle"};l<f&&i(n+s)<l&&(h.horizontal="center"),v<c&&i(o+u)<v&&(h.vertical="middle"),r(i(n),i(s))>r(i(o),i(u))?h.important="horizontal":h.important="vertical",t.using.call(this,e,h)}),a.offset(e.extend(C,{using:u}))})},e.ui.position={fit:{left:function(e,t){var n=t.within,i=n.isWindow?n.scrollLeft:n.offset.left,s=n.width,o=e.left-t.collisionPosition.marginLeft,u=i-o,a=o+t.collisionWidth-s-i,f;t.collisionWidth>s?u>0&&a<=0?(f=e.left+u+t.collisionWidth-s-i,e.left+=u-f):a>0&&u<=0?e.left=i:u>a?e.left=i+s-t.collisionWidth:e.left=i:u>0?e.left+=u:a>0?e.left-=a:e.left=r(e.left-o,e.left)},top:function(e,t){var n=t.within,i=n.isWindow?n.scrollTop:n.offset.top,s=t.within.height,o=e.top-t.collisionPosition.marginTop,u=i-o,a=o+t.collisionHeight-s-i,f;t.collisionHeight>s?u>0&&a<=0?(f=e.top+u+t.collisionHeight-s-i,e.top+=u-f):a>0&&u<=0?e.top=i:u>a?e.top=i+s-t.collisionHeight:e.top=i:u>0?e.top+=u:a>0?e.top-=a:e.top=r(e.top-o,e.top)}},flip:{left:function(e,t){var n=t.within,r=n.offset.left+n.scrollLeft,s=n.width,o=n.isWindow?n.scrollLeft:n.offset.left,u=e.left-t.collisionPosition.marginLeft,a=u-o,f=u+t.collisionWidth-s-o,l=t.my[0]==="left"?-t.elemWidth:t.my[0]==="right"?t.elemWidth:0,c=t.at[0]==="left"?t.targetWidth:t.at[0]==="right"?-t.targetWidth:0,h=-2*t.offset[0],p,d;if(a<0){p=e.left+l+c+h+t.collisionWidth-s-r;if(p<0||p<i(a))e.left+=l+c+h}else if(f>0){d=e.left-t.collisionPosition.marginLeft+l+c+h-o;if(d>0||i(d)<f)e.left+=l+c+h}},top:function(e,t){var n=t.within,r=n.offset.top+n.scrollTop,s=n.height,o=n.isWindow?n.scrollTop:n.offset.top,u=e.top-t.collisionPosition.marginTop,a=u-o,f=u+t.collisionHeight-s-o,l=t.my[1]==="top",c=l?-t.elemHeight:t.my[1]==="bottom"?t.elemHeight:0,h=t.at[1]==="top"?t.targetHeight:t.at[1]==="bottom"?-t.targetHeight:0,p=-2*t.offset[1],d,v;a<0?(v=e.top+c+h+p+t.collisionHeight-s-r,e.top+c+h+p>a&&(v<0||v<i(a))&&(e.top+=c+h+p)):f>0&&(d=e.top-t.collisionPosition.marginTop+c+h+p-o,e.top+c+h+p>f&&(d>0||i(d)<f)&&(e.top+=c+h+p))}},flipfit:{left:function(){e.ui.position.flip.left.apply(this,arguments),e.ui.position.fit.left.apply(this,arguments)},top:function(){e.ui.position.flip.top.apply(this,arguments),e.ui.position.fit.top.apply(this,arguments)}}},function(){var t,n,r,i,s,o=document.getElementsByTagName("body")[0],u=document.createElement("div");t=document.createElement(o?"div":"body"),r={visibility:"hidden",width:0,height:0,border:0,margin:0,background:"none"},o&&e.extend(r,{position:"absolute",left:"-1000px",top:"-1000px"});for(s in r)t.style[s]=r[s];t.appendChild(u),n=o||document.documentElement,n.insertBefore(t,n.firstChild),u.style.cssText="position: absolute; left: 10.7432222px;",i=e(u).offset().left,e.support.offsetFractions=i>10&&i<11,t.innerHTML="",n.removeChild(t)}()}(jQuery),function(e,t){e.widget("ui.progressbar",{version:"1.10.1",options:{max:100,value:0,change:null,complete:null},min:0,_create:function(){this.oldValue=this.options.value=this._constrainedValue(),this.element.addClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").attr({role:"progressbar","aria-valuemin":this.min}),this.valueDiv=e("<div class='ui-progressbar-value ui-widget-header ui-corner-left'></div>").appendTo(this.element),this._refreshValue()},_destroy:function(){this.element.removeClass("ui-progressbar ui-widget ui-widget-content ui-corner-all").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.valueDiv.remove()},value:function(e){if(e===t)return this.options.value;this.options.value=this._constrainedValue(e),this._refreshValue()},_constrainedValue:function(e){return e===t&&(e=this.options.value),this.indeterminate=e===!1,typeof e!="number"&&(e=0),this.indeterminate?!1:Math.min(this.options.max,Math.max(this.min,e))},_setOptions:function(e){var t=e.value;delete e.value,this._super(e),this.options.value=this._constrainedValue(t),this._refreshValue()},_setOption:function(e,t){e==="max"&&(t=Math.max(this.min,t)),this._super(e,t)},_percentage:function(){return this.indeterminate?100:100*(this.options.value-this.min)/(this.options.max-this.min)},_refreshValue:function(){var t=this.options.value,n=this._percentage();this.valueDiv.toggle(this.indeterminate||t>this.min).toggleClass("ui-corner-right",t===this.options.max).width(n.toFixed(0)+"%"),this.element.toggleClass("ui-progressbar-indeterminate",this.indeterminate),this.indeterminate?(this.element.removeAttr("aria-valuenow"),this.overlayDiv||(this.overlayDiv=e("<div class='ui-progressbar-overlay'></div>").appendTo(this.valueDiv))):(this.element.attr({"aria-valuemax":this.options.max,"aria-valuenow":t}),this.overlayDiv&&(this.overlayDiv.remove(),this.overlayDiv=null)),this.oldValue!==t&&(this.oldValue=t,this._trigger("change")),t===this.options.max&&this._trigger("complete")}})}(jQuery),function(e,t){var n=5;e.widget("ui.slider",e.ui.mouse,{version:"1.10.1",widgetEventPrefix:"slide",options:{animate:!1,distance:0,max:100,min:0,orientation:"horizontal",range:!1,step:1,value:0,values:null,change:null,slide:null,start:null,stop:null},_create:function(){this._keySliding=!1,this._mouseSliding=!1,this._animateOff=!0,this._handleIndex=null,this._detectOrientation(),this._mouseInit(),this.element.addClass("ui-slider ui-slider-"+this.orientation+" ui-widget"+" ui-widget-content"+" ui-corner-all"),this._refresh(),this._setOption("disabled",this.options.disabled),this._animateOff=!1},_refresh:function(){this._createRange(),this._createHandles(),this._setupEvents(),this._refreshValue()},_createHandles:function(){var t,n,r=this.options,i=this.element.find(".ui-slider-handle").addClass("ui-state-default ui-corner-all"),s="<a class='ui-slider-handle ui-state-default ui-corner-all' href='#'></a>",o=[];n=r.values&&r.values.length||1,i.length>n&&(i.slice(n).remove(),i=i.slice(0,n));for(t=i.length;t<n;t++)o.push(s);this.handles=i.add(e(o.join("")).appendTo(this.element)),this.handle=this.handles.eq(0),this.handles.each(function(t){e(this).data("ui-slider-handle-index",t)})},_createRange:function(){var t=this.options,n="";t.range?(t.range===!0&&(t.values?t.values.length&&t.values.length!==2?t.values=[t.values[0],t.values[0]]:e.isArray(t.values)&&(t.values=t.values.slice(0)):t.values=[this._valueMin(),this._valueMin()]),!this.range||!this.range.length?(this.range=e("<div></div>").appendTo(this.element),n="ui-slider-range ui-widget-header ui-corner-all"):this.range.removeClass("ui-slider-range-min ui-slider-range-max").css({left:"",bottom:""}),this.range.addClass(n+(t.range==="min"||t.range==="max"?" ui-slider-range-"+t.range:""))):this.range=e([])},_setupEvents:function(){var e=this.handles.add(this.range).filter("a");this._off(e),this._on(e,this._handleEvents),this._hoverable(e),this._focusable(e)},_destroy:function(){this.handles.remove(),this.range.remove(),this.element.removeClass("ui-slider ui-slider-horizontal ui-slider-vertical ui-widget ui-widget-content ui-corner-all"),this._mouseDestroy()},_mouseCapture:function(t){var n,r,i,s,o,u,a,f,l=this,c=this.options;return c.disabled?!1:(this.elementSize={width:this.element.outerWidth(),height:this.element.outerHeight()},this.elementOffset=this.element.offset(),n={x:t.pageX,y:t.pageY},r=this._normValueFromMouse(n),i=this._valueMax()-this._valueMin()+1,this.handles.each(function(t){var n=Math.abs(r-l.values(t));if(i>n||i===n&&(t===l._lastChangedValue||l.values(t)===c.min))i=n,s=e(this),o=t}),u=this._start(t,o),u===!1?!1:(this._mouseSliding=!0,this._handleIndex=o,s.addClass("ui-state-active").focus(),a=s.offset(),f=!e(t.target).parents().addBack().is(".ui-slider-handle"),this._clickOffset=f?{left:0,top:0}:{left:t.pageX-a.left-s.width()/2,top:t.pageY-a.top-s.height()/2-(parseInt(s.css("borderTopWidth"),10)||0)-(parseInt(s.css("borderBottomWidth"),10)||0)+(parseInt(s.css("marginTop"),10)||0)},this.handles.hasClass("ui-state-hover")||this._slide(t,o,r),this._animateOff=!0,!0))},_mouseStart:function(){return!0},_mouseDrag:function(e){var t={x:e.pageX,y:e.pageY},n=this._normValueFromMouse(t);return this._slide(e,this._handleIndex,n),!1},_mouseStop:function(e){return this.handles.removeClass("ui-state-active"),this._mouseSliding=!1,this._stop(e,this._handleIndex),this._change(e,this._handleIndex),this._handleIndex=null,this._clickOffset=null,this._animateOff=!1,!1},_detectOrientation:function(){this.orientation=this.options.orientation==="vertical"?"vertical":"horizontal"},_normValueFromMouse:function(e){var t,n,r,i,s;return this.orientation==="horizontal"?(t=this.elementSize.width,n=e.x-this.elementOffset.left-(this._clickOffset?this._clickOffset.left:0)):(t=this.elementSize.height,n=e.y-this.elementOffset.top-(this._clickOffset?this._clickOffset.top:0)),r=n/t,r>1&&(r=1),r<0&&(r=0),this.orientation==="vertical"&&(r=1-r),i=this._valueMax()-this._valueMin(),s=this._valueMin()+r*i,this._trimAlignValue(s)},_start:function(e,t){var n={handle:this.handles[t],value:this.value()};return this.options.values&&this.options.values.length&&(n.value=this.values(t),n.values=this.values()),this._trigger("start",e,n)},_slide:function(e,t,n){var r,i,s;this.options.values&&this.options.values.length?(r=this.values(t?0:1),this.options.values.length===2&&this.options.range===!0&&(t===0&&n>r||t===1&&n<r)&&(n=r),n!==this.values(t)&&(i=this.values(),i[t]=n,s=this._trigger("slide",e,{handle:this.handles[t],value:n,values:i}),r=this.values(t?0:1),s!==!1&&this.values(t,n,!0))):n!==this.value()&&(s=this._trigger("slide",e,{handle:this.handles[t],value:n}),s!==!1&&this.value(n))},_stop:function(e,t){var n={handle:this.handles[t],value:this.value()};this.options.values&&this.options.values.length&&(n.value=this.values(t),n.values=this.values()),this._trigger("stop",e,n)},_change:function(e,t){if(!this._keySliding&&!this._mouseSliding){var n={handle:this.handles[t],value:this.value()};this.options.values&&this.options.values.length&&(n.value=this.values(t),n.values=this.values()),this._lastChangedValue=t,this._trigger("change",e,n)}},value:function(e){if(arguments.length){this.options.value=this._trimAlignValue(e),this._refreshValue(),this._change(null,0);return}return this._value()},values:function(t,n){var r,i,s;if(arguments.length>1){this.options.values[t]=this._trimAlignValue(n),this._refreshValue(),this._change(null,t);return}if(!arguments.length)return this._values();if(!e.isArray(arguments[0]))return this.options.values&&this.options.values.length?this._values(t):this.value();r=this.options.values,i=arguments[0];for(s=0;s<r.length;s+=1)r[s]=this._trimAlignValue(i[s]),this._change(null,s);this._refreshValue()},_setOption:function(t,n){var r,i=0;t==="range"&&this.options.range===!0&&(n==="min"?(this.options.value=this._values(0),this.options.values=null):n==="max"&&(this.options.value=this._values(this.options.values.length-1),this.options.values=null)),e.isArray(this.options.values)&&(i=this.options.values.length),e.Widget.prototype._setOption.apply(this,arguments);switch(t){case"orientation":this._detectOrientation(),this.element.removeClass("ui-slider-horizontal ui-slider-vertical").addClass("ui-slider-"+this.orientation),this._refreshValue();break;case"value":this._animateOff=!0,this._refreshValue(),this._change(null,0),this._animateOff=!1;break;case"values":this._animateOff=!0,this._refreshValue();for(r=0;r<i;r+=1)this._change(null,r);this._animateOff=!1;break;case"min":case"max":this._animateOff=!0,this._refreshValue(),this._animateOff=!1;break;case"range":this._animateOff=!0,this._refresh(),this._animateOff=!1}},_value:function(){var e=this.options.value;return e=this._trimAlignValue(e),e},_values:function(e){var t,n,r;if(arguments.length)return t=this.options.values[e],t=this._trimAlignValue(t),t;if(this.options.values&&this.options.values.length){n=this.options.values.slice();for(r=0;r<n.length;r+=1)n[r]=this._trimAlignValue(n[r]);return n}return[]},_trimAlignValue:function(e){if(e<=this._valueMin())return this._valueMin();if(e>=this._valueMax())return this._valueMax();var t=this.options.step>0?this.options.step:1,n=(e-this._valueMin())%t,r=e-n;return Math.abs(n)*2>=t&&(r+=n>0?t:-t),parseFloat(r.toFixed(5))},_valueMin:function(){return this.options.min},_valueMax:function(){return this.options.max},_refreshValue:function(){var t,n,r,i,s,o=this.options.range,u=this.options,a=this,f=this._animateOff?!1:u.animate,l={};this.options.values&&this.options.values.length?this.handles.each(function(r){n=(a.values(r)-a._valueMin())/(a._valueMax()-a._valueMin())*100,l[a.orientation==="horizontal"?"left":"bottom"]=n+"%",e(this).stop(1,1)[f?"animate":"css"](l,u.animate),a.options.range===!0&&(a.orientation==="horizontal"?(r===0&&a.range.stop(1,1)[f?"animate":"css"]({left:n+"%"},u.animate),r===1&&a.range[f?"animate":"css"]({width:n-t+"%"},{queue:!1,duration:u.animate})):(r===0&&a.range.stop(1,1)[f?"animate":"css"]({bottom:n+"%"},u.animate),r===1&&a.range[f?"animate":"css"]({height:n-t+"%"},{queue:!1,duration:u.animate}))),t=n}):(r=this.value(),i=this._valueMin(),s=this._valueMax(),n=s!==i?(r-i)/(s-i)*100:0,l[this.orientation==="horizontal"?"left":"bottom"]=n+"%",this.handle.stop(1,1)[f?"animate":"css"](l,u.animate),o==="min"&&this.orientation==="horizontal"&&this.range.stop(1,1)[f?"animate":"css"]({width:n+"%"},u.animate),o==="max"&&this.orientation==="horizontal"&&this.range[f?"animate":"css"]({width:100-n+"%"},{queue:!1,duration:u.animate}),o==="min"&&this.orientation==="vertical"&&this.range.stop(1,1)[f?"animate":"css"]({height:n+"%"},u.animate),o==="max"&&this.orientation==="vertical"&&this.range[f?"animate":"css"]({height:100-n+"%"},{queue:!1,duration:u.animate}))},_handleEvents:{keydown:function(t){var r,i,s,o,u=e(t.target).data("ui-slider-handle-index");switch(t.keyCode){case e.ui.keyCode.HOME:case e.ui.keyCode.END:case e.ui.keyCode.PAGE_UP:case e.ui.keyCode.PAGE_DOWN:case e.ui.keyCode.UP:case e.ui.keyCode.RIGHT:case e.ui.keyCode.DOWN:case e.ui.keyCode.LEFT:t.preventDefault();if(!this._keySliding){this._keySliding=!0,e(t.target).addClass("ui-state-active"),r=this._start(t,u);if(r===!1)return}}o=this.options.step,this.options.values&&this.options.values.length?i=s=this.values(u):i=s=this.value();switch(t.keyCode){case e.ui.keyCode.HOME:s=this._valueMin();break;case e.ui.keyCode.END:s=this._valueMax();break;case e.ui.keyCode.PAGE_UP:s=this._trimAlignValue(i+(this._valueMax()-this._valueMin())/n);break;case e.ui.keyCode.PAGE_DOWN:s=this._trimAlignValue(i-(this._valueMax()-this._valueMin())/n);break;case e.ui.keyCode.UP:case e.ui.keyCode.RIGHT:if(i===this._valueMax())return;s=this._trimAlignValue(i+o);break;case e.ui.keyCode.DOWN:case e.ui.keyCode.LEFT:if(i===this._valueMin())return;s=this._trimAlignValue(i-o)}this._slide(t,u,s)},click:function(e){e.preventDefault()},keyup:function(t){var n=e(t.target).data("ui-slider-handle-index");this._keySliding&&(this._keySliding=!1,this._stop(t,n),this._change(t,n),e(t.target).removeClass("ui-state-active"))}}})}(jQuery),function(e){function t(e){return function(){var t=this.element.val();e.apply(this,arguments),this._refresh(),t!==this.element.val()&&this._trigger("change")}}e.widget("ui.spinner",{version:"1.10.1",defaultElement:"<input>",widgetEventPrefix:"spin",options:{culture:null,icons:{down:"ui-icon-triangle-1-s",up:"ui-icon-triangle-1-n"},incremental:!0,max:null,min:null,numberFormat:null,page:10,step:1,change:null,spin:null,start:null,stop:null},_create:function(){this._setOption("max",this.options.max),this._setOption("min",this.options.min),this._setOption("step",this.options.step),this._value(this.element.val(),!0),this._draw(),this._on(this._events),this._refresh(),this._on(this.window,{beforeunload:function(){this.element.removeAttr("autocomplete")}})},_getCreateOptions:function(){var t={},n=this.element;return e.each(["min","max","step"],function(e,r){var i=n.attr(r);i!==undefined&&i.length&&(t[r]=i)}),t},_events:{keydown:function(e){this._start(e)&&this._keydown(e)&&e.preventDefault()},keyup:"_stop",focus:function(){this.previous=this.element.val()},blur:function(e){if(this.cancelBlur){delete this.cancelBlur;return}this._refresh(),this.previous!==this.element.val()&&this._trigger("change",e)},mousewheel:function(e,t){if(!t)return;if(!this.spinning&&!this._start(e))return!1;this._spin((t>0?1:-1)*this.options.step,e),clearTimeout(this.mousewheelTimer),this.mousewheelTimer=this._delay(function(){this.spinning&&this._stop(e)},100),e.preventDefault()},"mousedown .ui-spinner-button":function(t){function r(){var e=this.element[0]===this.document[0].activeElement;e||(this.element.focus(),this.previous=n,this._delay(function(){this.previous=n}))}var n;n=this.element[0]===this.document[0].activeElement?this.previous:this.element.val(),t.preventDefault(),r.call(this),this.cancelBlur=!0,this._delay(function(){delete this.cancelBlur,r.call(this)});if(this._start(t)===!1)return;this._repeat(null,e(t.currentTarget).hasClass("ui-spinner-up")?1:-1,t)},"mouseup .ui-spinner-button":"_stop","mouseenter .ui-spinner-button":function(t){if(!e(t.currentTarget).hasClass("ui-state-active"))return;if(this._start(t)===!1)return!1;this._repeat(null,e(t.currentTarget).hasClass("ui-spinner-up")?1:-1,t)},"mouseleave .ui-spinner-button":"_stop"},_draw:function(){var e=this.uiSpinner=this.element.addClass("ui-spinner-input").attr("autocomplete","off").wrap(this._uiSpinnerHtml()).parent().append(this._buttonHtml());this.element.attr("role","spinbutton"),this.buttons=e.find(".ui-spinner-button").attr("tabIndex",-1).button().removeClass("ui-corner-all"),this.buttons.height()>Math.ceil(e.height()*.5)&&e.height()>0&&e.height(e.height()),this.options.disabled&&this.disable()},_keydown:function(t){var n=this.options,r=e.ui.keyCode;switch(t.keyCode){case r.UP:return this._repeat(null,1,t),!0;case r.DOWN:return this._repeat(null,-1,t),!0;case r.PAGE_UP:return this._repeat(null,n.page,t),!0;case r.PAGE_DOWN:return this._repeat(null,-n.page,t),!0}return!1},_uiSpinnerHtml:function(){return"<span class='ui-spinner ui-widget ui-widget-content ui-corner-all'></span>"},_buttonHtml:function(){return"<a class='ui-spinner-button ui-spinner-up ui-corner-tr'><span class='ui-icon "+this.options.icons.up+"'>&#9650;</span>"+"</a>"+"<a class='ui-spinner-button ui-spinner-down ui-corner-br'>"+"<span class='ui-icon "+this.options.icons.down+"'>&#9660;</span>"+"</a>"},_start:function(e){return!this.spinning&&this._trigger("start",e)===!1?!1:(this.counter||(this.counter=1),this.spinning=!0,!0)},_repeat:function(e,t,n){e=e||500,clearTimeout(this.timer),this.timer=this._delay(function(){this._repeat(40,t,n)},e),this._spin(t*this.options.step,n)},_spin:function(e,t){var n=this.value()||0;this.counter||(this.counter=1),n=this._adjustValue(n+e*this._increment(this.counter));if(!this.spinning||this._trigger("spin",t,{value:n})!==!1)this._value(n),this.counter++},_increment:function(t){var n=this.options.incremental;return n?e.isFunction(n)?n(t):Math.floor(t*t*t/5e4-t*t/500+17*t/200+1):1},_precision:function(){var e=this._precisionOf(this.options.step);return this.options.min!==null&&(e=Math.max(e,this._precisionOf(this.options.min))),e},_precisionOf:function(e){var t=e.toString(),n=t.indexOf(".");return n===-1?0:t.length-n-1},_adjustValue:function(e){var t,n,r=this.options;return t=r.min!==null?r.min:0,n=e-t,n=Math.round(n/r.step)*r.step,e=t+n,e=parseFloat(e.toFixed(this._precision())),r.max!==null&&e>r.max?r.max:r.min!==null&&e<r.min?r.min:e},_stop:function(e){if(!this.spinning)return;clearTimeout(this.timer),clearTimeout(this.mousewheelTimer),this.counter=0,this.spinning=!1,this._trigger("stop",e)},_setOption:function(e,t){if(e==="culture"||e==="numberFormat"){var n=this._parse(this.element.val());this.options[e]=t,this.element.val(this._format(n));return}(e==="max"||e==="min"||e==="step")&&typeof t=="string"&&(t=this._parse(t)),e==="icons"&&(this.buttons.first().find(".ui-icon").removeClass(this.options.icons.up).addClass(t.up),this.buttons.last().find(".ui-icon").removeClass(this.options.icons.down).addClass(t.down)),this._super(e,t),e==="disabled"&&(t?(this.element.prop("disabled",!0),this.buttons.button("disable")):(this.element.prop("disabled",!1),this.buttons.button("enable")))},_setOptions:t(function(e){this._super(e),this._value(this.element.val())}),_parse:function(e){return typeof e=="string"&&e!==""&&(e=window.Globalize&&this.options.numberFormat?Globalize.parseFloat(e,10,this.options.culture):+e),e===""||isNaN(e)?null:e},_format:function(e){return e===""?"":window.Globalize&&this.options.numberFormat?Globalize.format(e,this.options.numberFormat,this.options.culture):e},_refresh:function(){this.element.attr({"aria-valuemin":this.options.min,"aria-valuemax":this.options.max,"aria-valuenow":this._parse(this.element.val())})},_value:function(e,t){var n;e!==""&&(n=this._parse(e),n!==null&&(t||(n=this._adjustValue(n)),e=this._format(n))),this.element.val(e),this._refresh()},_destroy:function(){this.element.removeClass("ui-spinner-input").prop("disabled",!1).removeAttr("autocomplete").removeAttr("role").removeAttr("aria-valuemin").removeAttr("aria-valuemax").removeAttr("aria-valuenow"),this.uiSpinner.replaceWith(this.element)},stepUp:t(function(e){this._stepUp(e)}),_stepUp:function(e){this._start()&&(this._spin((e||1)*this.options.step),this._stop())},stepDown:t(function(e){this._stepDown(e)}),_stepDown:function(e){this._start()&&(this._spin((e||1)*-this.options.step),this._stop())},pageUp:t(function(e){this._stepUp((e||1)*this.options.page)}),pageDown:t(function(e){this._stepDown((e||1)*this.options.page)}),value:function(e){if(!arguments.length)return this._parse(this.element.val());t(this._value).call(this,e)},widget:function(){return this.uiSpinner}})}(jQuery),function(e,t){function i(){return++n}function s(e){return e.hash.length>1&&decodeURIComponent(e.href.replace(r,""))===decodeURIComponent(location.href.replace(r,""))}var n=0,r=/#.*$/;e.widget("ui.tabs",{version:"1.10.1",delay:300,options:{active:null,collapsible:!1,event:"click",heightStyle:"content",hide:null,show:null,activate:null,beforeActivate:null,beforeLoad:null,load:null},_create:function(){var t=this,n=this.options;this.running=!1,this.element.addClass("ui-tabs ui-widget ui-widget-content ui-corner-all").toggleClass("ui-tabs-collapsible",n.collapsible).delegate(".ui-tabs-nav > li","mousedown"+this.eventNamespace,function(t){e(this).is(".ui-state-disabled")&&t.preventDefault()}).delegate(".ui-tabs-anchor","focus"+this.eventNamespace,function(){e(this).closest("li").is(".ui-state-disabled")&&this.blur()}),this._processTabs(),n.active=this._initialActive(),e.isArray(n.disabled)&&(n.disabled=e.unique(n.disabled.concat(e.map(this.tabs.filter(".ui-state-disabled"),function(e){return t.tabs.index(e)}))).sort()),this.options.active!==!1&&this.anchors.length?this.active=this._findActive(n.active):this.active=e(),this._refresh(),this.active.length&&this.load(n.active)},_initialActive:function(){var t=this.options.active,n=this.options.collapsible,r=location.hash.substring(1);if(t===null){r&&this.tabs.each(function(n,i){if(e(i).attr("aria-controls")===r)return t=n,!1}),t===null&&(t=this.tabs.index(this.tabs.filter(".ui-tabs-active")));if(t===null||t===-1)t=this.tabs.length?0:!1}return t!==!1&&(t=this.tabs.index(this.tabs.eq(t)),t===-1&&(t=n?!1:0)),!n&&t===!1&&this.anchors.length&&(t=0),t},_getCreateEventData:function(){return{tab:this.active,panel:this.active.length?this._getPanelForTab(this.active):e()}},_tabKeydown:function(t){var n=e(this.document[0].activeElement).closest("li"),r=this.tabs.index(n),i=!0;if(this._handlePageNav(t))return;switch(t.keyCode){case e.ui.keyCode.RIGHT:case e.ui.keyCode.DOWN:r++;break;case e.ui.keyCode.UP:case e.ui.keyCode.LEFT:i=!1,r--;break;case e.ui.keyCode.END:r=this.anchors.length-1;break;case e.ui.keyCode.HOME:r=0;break;case e.ui.keyCode.SPACE:t.preventDefault(),clearTimeout(this.activating),this._activate(r);return;case e.ui.keyCode.ENTER:t.preventDefault(),clearTimeout(this.activating),this._activate(r===this.options.active?!1:r);return;default:return}t.preventDefault(),clearTimeout(this.activating),r=this._focusNextTab(r,i),t.ctrlKey||(n.attr("aria-selected","false"),this.tabs.eq(r).attr("aria-selected","true"),this.activating=this._delay(function(){this.option("active",r)},this.delay))},_panelKeydown:function(t){if(this._handlePageNav(t))return;t.ctrlKey&&t.keyCode===e.ui.keyCode.UP&&(t.preventDefault(),this.active.focus())},_handlePageNav:function(t){if(t.altKey&&t.keyCode===e.ui.keyCode.PAGE_UP)return this._activate(this._focusNextTab(this.options.active-1,!1)),!0;if(t.altKey&&t.keyCode===e.ui.keyCode.PAGE_DOWN)return this._activate(this._focusNextTab(this.options.active+1,!0)),!0},_findNextTab:function(t,n){function i(){return t>r&&(t=0),t<0&&(t=r),t}var r=this.tabs.length-1;while(e.inArray(i(),this.options.disabled)!==-1)t=n?t+1:t-1;return t},_focusNextTab:function(e,t){return e=this._findNextTab(e,t),this.tabs.eq(e).focus(),e},_setOption:function(e,t){if(e==="active"){this._activate(t);return}if(e==="disabled"){this._setupDisabled(t);return}this._super(e,t),e==="collapsible"&&(this.element.toggleClass("ui-tabs-collapsible",t),!t&&this.options.active===!1&&this._activate(0)),e==="event"&&this._setupEvents(t),e==="heightStyle"&&this._setupHeightStyle(t)},_tabId:function(e){return e.attr("aria-controls")||"ui-tabs-"+i()},_sanitizeSelector:function(e){return e?e.replace(/[!"$%&'()*+,.\/:;<=>?@\[\]\^`{|}~]/g,"\\$&"):""},refresh:function(){var t=this.options,n=this.tablist.children(":has(a[href])");t.disabled=e.map(n.filter(".ui-state-disabled"),function(e){return n.index(e)}),this._processTabs(),t.active===!1||!this.anchors.length?(t.active=!1,this.active=e()):this.active.length&&!e.contains(this.tablist[0],this.active[0])?this.tabs.length===t.disabled.length?(t.active=!1,this.active=e()):this._activate(this._findNextTab(Math.max(0,t.active-1),!1)):t.active=this.tabs.index(this.active),this._refresh()},_refresh:function(){this._setupDisabled(this.options.disabled),this._setupEvents(this.options.event),this._setupHeightStyle(this.options.heightStyle),this.tabs.not(this.active).attr({"aria-selected":"false",tabIndex:-1}),this.panels.not(this._getPanelForTab(this.active)).hide().attr({"aria-expanded":"false","aria-hidden":"true"}),this.active.length?(this.active.addClass("ui-tabs-active ui-state-active").attr({"aria-selected":"true",tabIndex:0}),this._getPanelForTab(this.active).show().attr({"aria-expanded":"true","aria-hidden":"false"})):this.tabs.eq(0).attr("tabIndex",0)},_processTabs:function(){var t=this;this.tablist=this._getList().addClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").attr("role","tablist"),this.tabs=this.tablist.find("> li:has(a[href])").addClass("ui-state-default ui-corner-top").attr({role:"tab",tabIndex:-1}),this.anchors=this.tabs.map(function(){return e("a",this)[0]}).addClass("ui-tabs-anchor").attr({role:"presentation",tabIndex:-1}),this.panels=e(),this.anchors.each(function(n,r){var i,o,u,a=e(r).uniqueId().attr("id"),f=e(r).closest("li"),l=f.attr("aria-controls");s(r)?(i=r.hash,o=t.element.find(t._sanitizeSelector(i))):(u=t._tabId(f),i="#"+u,o=t.element.find(i),o.length||(o=t._createPanel(u),o.insertAfter(t.panels[n-1]||t.tablist)),o.attr("aria-live","polite")),o.length&&(t.panels=t.panels.add(o)),l&&f.data("ui-tabs-aria-controls",l),f.attr({"aria-controls":i.substring(1),"aria-labelledby":a}),o.attr("aria-labelledby",a)}),this.panels.addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").attr("role","tabpanel")},_getList:function(){return this.element.find("ol,ul").eq(0)},_createPanel:function(t){return e("<div>").attr("id",t).addClass("ui-tabs-panel ui-widget-content ui-corner-bottom").data("ui-tabs-destroy",!0)},_setupDisabled:function(t){e.isArray(t)&&(t.length?t.length===this.anchors.length&&(t=!0):t=!1);for(var n=0,r;r=this.tabs[n];n++)t===!0||e.inArray(n,t)!==-1?e(r).addClass("ui-state-disabled").attr("aria-disabled","true"):e(r).removeClass("ui-state-disabled").removeAttr("aria-disabled");this.options.disabled=t},_setupEvents:function(t){var n={click:function(e){e.preventDefault()}};t&&e.each(t.split(" "),function(e,t){n[t]="_eventHandler"}),this._off(this.anchors.add(this.tabs).add(this.panels)),this._on(this.anchors,n),this._on(this.tabs,{keydown:"_tabKeydown"}),this._on(this.panels,{keydown:"_panelKeydown"}),this._focusable(this.tabs),this._hoverable(this.tabs)},_setupHeightStyle:function(t){var n,r=this.element.parent();t==="fill"?(n=r.height(),n-=this.element.outerHeight()-this.element.height(),this.element.siblings(":visible").each(function(){var t=e(this),r=t.css("position");if(r==="absolute"||r==="fixed")return;n-=t.outerHeight(!0)}),this.element.children().not(this.panels).each(function(){n-=e(this).outerHeight(!0)}),this.panels.each(function(){e(this).height(Math.max(0,n-e(this).innerHeight()+e(this).height()))}).css("overflow","auto")):t==="auto"&&(n=0,this.panels.each(function(){n=Math.max(n,e(this).height("").height())}).height(n))},_eventHandler:function(t){var n=this.options,r=this.active,i=e(t.currentTarget),s=i.closest("li"),o=s[0]===r[0],u=o&&n.collapsible,a=u?e():this._getPanelForTab(s),f=r.length?this._getPanelForTab(r):e(),l={oldTab:r,oldPanel:f,newTab:u?e():s,newPanel:a};t.preventDefault();if(s.hasClass("ui-state-disabled")||s.hasClass("ui-tabs-loading")||this.running||o&&!n.collapsible||this._trigger("beforeActivate",t,l)===!1)return;n.active=u?!1:this.tabs.index(s),this.active=o?e():s,this.xhr&&this.xhr.abort(),!f.length&&!a.length&&e.error("jQuery UI Tabs: Mismatching fragment identifier."),a.length&&this.load(this.tabs.index(s),t),this._toggle(t,l)},_toggle:function(t,n){function o(){r.running=!1,r._trigger("activate",t,n)}function u(){n.newTab.closest("li").addClass("ui-tabs-active ui-state-active"),i.length&&r.options.show?r._show(i,r.options.show,o):(i.show(),o())}var r=this,i=n.newPanel,s=n.oldPanel;this.running=!0,s.length&&this.options.hide?this._hide(s,this.options.hide,function(){n.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),u()}):(n.oldTab.closest("li").removeClass("ui-tabs-active ui-state-active"),s.hide(),u()),s.attr({"aria-expanded":"false","aria-hidden":"true"}),n.oldTab.attr("aria-selected","false"),i.length&&s.length?n.oldTab.attr("tabIndex",-1):i.length&&this.tabs.filter(function(){return e(this).attr("tabIndex")===0}).attr("tabIndex",-1),i.attr({"aria-expanded":"true","aria-hidden":"false"}),n.newTab.attr({"aria-selected":"true",tabIndex:0})},_activate:function(t){var n,r=this._findActive(t);if(r[0]===this.active[0])return;r.length||(r=this.active),n=r.find(".ui-tabs-anchor")[0],this._eventHandler({target:n,currentTarget:n,preventDefault:e.noop})},_findActive:function(t){return t===!1?e():this.tabs.eq(t)},_getIndex:function(e){return typeof e=="string"&&(e=this.anchors.index(this.anchors.filter("[href$='"+e+"']"))),e},_destroy:function(){this.xhr&&this.xhr.abort(),this.element.removeClass("ui-tabs ui-widget ui-widget-content ui-corner-all ui-tabs-collapsible"),this.tablist.removeClass("ui-tabs-nav ui-helper-reset ui-helper-clearfix ui-widget-header ui-corner-all").removeAttr("role"),this.anchors.removeClass("ui-tabs-anchor").removeAttr("role").removeAttr("tabIndex").removeUniqueId(),this.tabs.add(this.panels).each(function(){e.data(this,"ui-tabs-destroy")?e(this).remove():e(this).removeClass("ui-state-default ui-state-active ui-state-disabled ui-corner-top ui-corner-bottom ui-widget-content ui-tabs-active ui-tabs-panel").removeAttr("tabIndex").removeAttr("aria-live").removeAttr("aria-busy").removeAttr("aria-selected").removeAttr("aria-labelledby").removeAttr("aria-hidden").removeAttr("aria-expanded").removeAttr("role")}),this.tabs.each(function(){var t=e(this),n=t.data("ui-tabs-aria-controls");n?t.attr("aria-controls",n).removeData("ui-tabs-aria-controls"):t.removeAttr("aria-controls")}),this.panels.show(),this.options.heightStyle!=="content"&&this.panels.css("height","")},enable:function(n){var r=this.options.disabled;if(r===!1)return;n===t?r=!1:(n=this._getIndex(n),e.isArray(r)?r=e.map(r,function(e){return e!==n?e:null}):r=e.map(this.tabs,function(e,t){return t!==n?t:null})),this._setupDisabled(r)},disable:function(n){var r=this.options.disabled;if(r===!0)return;if(n===t)r=!0;else{n=this._getIndex(n);if(e.inArray(n,r)!==-1)return;e.isArray(r)?r=e.merge([n],r).sort():r=[n]}this._setupDisabled(r)},load:function(t,n){t=this._getIndex(t);var r=this,i=this.tabs.eq(t),o=i.find(".ui-tabs-anchor"),u=this._getPanelForTab(i),a={tab:i,panel:u};if(s(o[0]))return;this.xhr=e.ajax(this._ajaxSettings(o,n,a)),this.xhr&&this.xhr.statusText!=="canceled"&&(i.addClass("ui-tabs-loading"),u.attr("aria-busy","true"),this.xhr.success(function(e){setTimeout(function(){u.html(e),r._trigger("load",n,a)},1)}).complete(function(e,t){setTimeout(function(){t==="abort"&&r.panels.stop(!1,!0),i.removeClass("ui-tabs-loading"),u.removeAttr("aria-busy"),e===r.xhr&&delete r.xhr},1)}))},_ajaxSettings:function(t,n,r){var i=this;return{url:t.attr("href"),beforeSend:function(t,s){return i._trigger("beforeLoad",n,e.extend({jqXHR:t,ajaxSettings:s},r))}}},_getPanelForTab:function(t){var n=e(t).attr("aria-controls");return this.element.find(this._sanitizeSelector("#"+n))}})}(jQuery),function(e){function n(t,n){var r=(t.attr("aria-describedby")||"").split(/\s+/);r.push(n),t.data("ui-tooltip-id",n).attr("aria-describedby",e.trim(r.join(" ")))}function r(t){var n=t.data("ui-tooltip-id"),r=(t.attr("aria-describedby")||"").split(/\s+/),i=e.inArray(n,r);i!==-1&&r.splice(i,1),t.removeData("ui-tooltip-id"),r=e.trim(r.join(" ")),r?t.attr("aria-describedby",r):t.removeAttr("aria-describedby")}var t=0;e.widget("ui.tooltip",{version:"1.10.1",options:{content:function(){var t=e(this).attr("title")||"";return e("<a>").text(t).html()},hide:!0,items:"[title]:not([disabled])",position:{my:"left top+15",at:"left bottom",collision:"flipfit flip"},show:!0,tooltipClass:null,track:!1,close:null,open:null},_create:function(){this._on({mouseover:"open",focusin:"open"}),this.tooltips={},this.parents={},this.options.disabled&&this._disable()},_setOption:function(t,n){var r=this;if(t==="disabled"){this[n?"_disable":"_enable"](),this.options[t]=n;return}this._super(t,n),t==="content"&&e.each(this.tooltips,function(e,t){r._updateContent(t)})},_disable:function(){var t=this;e.each(this.tooltips,function(n,r){var i=e.Event("blur");i.target=i.currentTarget=r[0],t.close(i,!0)}),this.element.find(this.options.items).addBack().each(function(){var t=e(this);t.is("[title]")&&t.data("ui-tooltip-title",t.attr("title")).attr("title","")})},_enable:function(){this.element.find(this.options.items).addBack().each(function(){var t=e(this);t.data("ui-tooltip-title")&&t.attr("title",t.data("ui-tooltip-title"))})},open:function(t){var n=this,r=e(t?t.target:this.element).closest(this.options.items);if(!r.length||r.data("ui-tooltip-id"))return;r.attr("title")&&r.data("ui-tooltip-title",r.attr("title")),r.data("ui-tooltip-open",!0),t&&t.type==="mouseover"&&r.parents().each(function(){var t=e(this),r;t.data("ui-tooltip-open")&&(r=e.Event("blur"),r.target=r.currentTarget=this,n.close(r,!0)),t.attr("title")&&(t.uniqueId(),n.parents[this.id]={element:this,title:t.attr("title")},t.attr("title",""))}),this._updateContent(r,t)},_updateContent:function(e,t){var n,r=this.options.content,i=this,s=t?t.type:null;if(typeof r=="string")return this._open(t,e,r);n=r.call(e[0],function(n){if(!e.data("ui-tooltip-open"))return;i._delay(function(){t&&(t.type=s),this._open(t,e,n)})}),n&&this._open(t,e,n)},_open:function(t,r,i){function f(e){a.of=e;if(s.is(":hidden"))return;s.position(a)}var s,o,u,a=e.extend({},this.options.position);if(!i)return;s=this._find(r);if(s.length){s.find(".ui-tooltip-content").html(i);return}r.is("[title]")&&(t&&t.type==="mouseover"?r.attr("title",""):r.removeAttr("title")),s=this._tooltip(r),n(r,s.attr("id")),s.find(".ui-tooltip-content").html(i),this.options.track&&t&&/^mouse/.test(t.type)?(this._on(this.document,{mousemove:f}),f(t)):s.position(e.extend({of:r},this.options.position)),s.hide(),this._show(s,this.options.show),this.options.show&&this.options.show.delay&&(u=this.delayedShow=setInterval(function(){s.is(":visible")&&(f(a.of),clearInterval(u))},e.fx.interval)),this._trigger("open",t,{tooltip:s}),o={keyup:function(t){if(t.keyCode===e.ui.keyCode.ESCAPE){var n=e.Event(t);n.currentTarget=r[0],this.close(n,!0)}},remove:function(){this._removeTooltip(s)}};if(!t||t.type==="mouseover")o.mouseleave="close";if(!t||t.type==="focusin")o.focusout="close";this._on(!0,r,o)},close:function(t){var n=this,i=e(t?t.currentTarget:this.element),s=this._find(i);if(this.closing)return;clearInterval(this.delayedShow),i.data("ui-tooltip-title")&&i.attr("title",i.data("ui-tooltip-title")),r(i),s.stop(!0),this._hide(s,this.options.hide,function(){n._removeTooltip(e(this))}),i.removeData("ui-tooltip-open"),this._off(i,"mouseleave focusout keyup"),i[0]!==this.element[0]&&this._off(i,"remove"),this._off(this.document,"mousemove"),t&&t.type==="mouseleave"&&e.each(this.parents,function(t,r){e(r.element).attr("title",r.title),delete n.parents[t]}),this.closing=!0,this._trigger("close",t,{tooltip:s}),this.closing=!1},_tooltip:function(n){var r="ui-tooltip-"+t++,i=e("<div>").attr({id:r,role:"tooltip"}).addClass("ui-tooltip ui-widget ui-corner-all ui-widget-content "+(this.options.tooltipClass||""));return e("<div>").addClass("ui-tooltip-content").appendTo(i),i.appendTo(this.document[0].body),this.tooltips[r]=n,i},_find:function(t){var n=t.data("ui-tooltip-id");return n?e("#"+n):e()},_removeTooltip:function(e){e.remove(),delete this.tooltips[e.attr("id")]},_destroy:function(){var t=this;e.each(this.tooltips,function(n,r){var i=e.Event("blur");i.target=i.currentTarget=r[0],t.close(i,!0),e("#"+n).remove(),r.data("ui-tooltip-title")&&(r.attr("title",r.data("ui-tooltip-title")),r.removeData("ui-tooltip-title"))})}})}(jQuery);

//js twitter-bootstrap/js/bootstrap.min.js

/**
* bootstrap.js v3.0.0 by @fat and @mdo
* Copyright 2013 Twitter Inc.
* http://www.apache.org/licenses/LICENSE-2.0
*/
if(!jQuery)throw new Error("Bootstrap requires jQuery");+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]}}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one(a.support.transition.end,function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b()})}(window.jQuery),+function(a){"use strict";var b='[data-dismiss="alert"]',c=function(c){a(c).on("click",b,this.close)};c.prototype.close=function(b){function c(){f.trigger("closed.bs.alert").remove()}var d=a(this),e=d.attr("data-target");e||(e=d.attr("href"),e=e&&e.replace(/.*(?=#[^\s]*$)/,""));var f=a(e);b&&b.preventDefault(),f.length||(f=d.hasClass("alert")?d:d.parent()),f.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one(a.support.transition.end,c).emulateTransitionEnd(150):c())};var d=a.fn.alert;a.fn.alert=function(b){return this.each(function(){var d=a(this),e=d.data("bs.alert");e||d.data("bs.alert",e=new c(this)),"string"==typeof b&&e[b].call(d)})},a.fn.alert.Constructor=c,a.fn.alert.noConflict=function(){return a.fn.alert=d,this},a(document).on("click.bs.alert.data-api",b,c.prototype.close)}(window.jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d)};b.DEFAULTS={loadingText:"loading..."},b.prototype.setState=function(a){var b="disabled",c=this.$element,d=c.is("input")?"val":"html",e=c.data();a+="Text",e.resetText||c.data("resetText",c[d]()),c[d](e[a]||this.options[a]),setTimeout(function(){"loadingText"==a?c.addClass(b).attr(b,b):c.removeClass(b).removeAttr(b)},0)},b.prototype.toggle=function(){var a=this.$element.closest('[data-toggle="buttons"]');if(a.length){var b=this.$element.find("input").prop("checked",!this.$element.hasClass("active")).trigger("change");"radio"===b.prop("type")&&a.find(".active").removeClass("active")}this.$element.toggleClass("active")};var c=a.fn.button;a.fn.button=function(c){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof c&&c;e||d.data("bs.button",e=new b(this,f)),"toggle"==c?e.toggle():c&&e.setState(c)})},a.fn.button.Constructor=b,a.fn.button.noConflict=function(){return a.fn.button=c,this},a(document).on("click.bs.button.data-api","[data-toggle^=button]",function(b){var c=a(b.target);c.hasClass("btn")||(c=c.closest(".btn")),c.button("toggle"),b.preventDefault()})}(window.jQuery),+function(a){"use strict";var b=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=this.sliding=this.interval=this.$active=this.$items=null,"hover"==this.options.pause&&this.$element.on("mouseenter",a.proxy(this.pause,this)).on("mouseleave",a.proxy(this.cycle,this))};b.DEFAULTS={interval:5e3,pause:"hover",wrap:!0},b.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},b.prototype.getActiveIndex=function(){return this.$active=this.$element.find(".item.active"),this.$items=this.$active.parent().children(),this.$items.index(this.$active)},b.prototype.to=function(b){var c=this,d=this.getActiveIndex();return b>this.$items.length-1||0>b?void 0:this.sliding?this.$element.one("slid",function(){c.to(b)}):d==b?this.pause().cycle():this.slide(b>d?"next":"prev",a(this.$items[b]))},b.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition.end&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},b.prototype.next=function(){return this.sliding?void 0:this.slide("next")},b.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},b.prototype.slide=function(b,c){var d=this.$element.find(".item.active"),e=c||d[b](),f=this.interval,g="next"==b?"left":"right",h="next"==b?"first":"last",i=this;if(!e.length){if(!this.options.wrap)return;e=this.$element.find(".item")[h]()}this.sliding=!0,f&&this.pause();var j=a.Event("slide.bs.carousel",{relatedTarget:e[0],direction:g});if(!e.hasClass("active")){if(this.$indicators.length&&(this.$indicators.find(".active").removeClass("active"),this.$element.one("slid",function(){var b=a(i.$indicators.children()[i.getActiveIndex()]);b&&b.addClass("active")})),a.support.transition&&this.$element.hasClass("slide")){if(this.$element.trigger(j),j.isDefaultPrevented())return;e.addClass(b),e[0].offsetWidth,d.addClass(g),e.addClass(g),d.one(a.support.transition.end,function(){e.removeClass([b,g].join(" ")).addClass("active"),d.removeClass(["active",g].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger("slid")},0)}).emulateTransitionEnd(600)}else{if(this.$element.trigger(j),j.isDefaultPrevented())return;d.removeClass("active"),e.addClass("active"),this.sliding=!1,this.$element.trigger("slid")}return f&&this.cycle(),this}};var c=a.fn.carousel;a.fn.carousel=function(c){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c),g="string"==typeof c?c:f.slide;e||d.data("bs.carousel",e=new b(this,f)),"number"==typeof c?e.to(c):g?e[g]():f.interval&&e.pause().cycle()})},a.fn.carousel.Constructor=b,a.fn.carousel.noConflict=function(){return a.fn.carousel=c,this},a(document).on("click.bs.carousel.data-api","[data-slide], [data-slide-to]",function(b){var c,d=a(this),e=a(d.attr("data-target")||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"")),f=a.extend({},e.data(),d.data()),g=d.attr("data-slide-to");g&&(f.interval=!1),e.carousel(f),(g=d.attr("data-slide-to"))&&e.data("bs.carousel").to(g),b.preventDefault()}),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var b=a(this);b.carousel(b.data())})})}(window.jQuery),+function(a){"use strict";var b=function(c,d){this.$element=a(c),this.options=a.extend({},b.DEFAULTS,d),this.transitioning=null,this.options.parent&&(this.$parent=a(this.options.parent)),this.options.toggle&&this.toggle()};b.DEFAULTS={toggle:!0},b.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},b.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b=a.Event("show.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.$parent&&this.$parent.find("> .panel > .in");if(c&&c.length){var d=c.data("bs.collapse");if(d&&d.transitioning)return;c.collapse("hide"),d||c.data("bs.collapse",null)}var e=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[e](0),this.transitioning=1;var f=function(){this.$element.removeClass("collapsing").addClass("in")[e]("auto"),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return f.call(this);var g=a.camelCase(["scroll",e].join("-"));this.$element.one(a.support.transition.end,a.proxy(f,this)).emulateTransitionEnd(350)[e](this.$element[0][g])}}},b.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse").removeClass("in"),this.transitioning=1;var d=function(){this.transitioning=0,this.$element.trigger("hidden.bs.collapse").removeClass("collapsing").addClass("collapse")};return a.support.transition?(this.$element[c](0).one(a.support.transition.end,a.proxy(d,this)).emulateTransitionEnd(350),void 0):d.call(this)}}},b.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()};var c=a.fn.collapse;a.fn.collapse=function(c){return this.each(function(){var d=a(this),e=d.data("bs.collapse"),f=a.extend({},b.DEFAULTS,d.data(),"object"==typeof c&&c);e||d.data("bs.collapse",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.collapse.Constructor=b,a.fn.collapse.noConflict=function(){return a.fn.collapse=c,this},a(document).on("click.bs.collapse.data-api","[data-toggle=collapse]",function(b){var c,d=a(this),e=d.attr("data-target")||b.preventDefault()||(c=d.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,""),f=a(e),g=f.data("bs.collapse"),h=g?"toggle":d.data(),i=d.attr("data-parent"),j=i&&a(i);g&&g.transitioning||(j&&j.find('[data-toggle=collapse][data-parent="'+i+'"]').not(d).addClass("collapsed"),d[f.hasClass("in")?"addClass":"removeClass"]("collapsed")),f.collapse(h)})}(window.jQuery),+function(a){"use strict";function b(){a(d).remove(),a(e).each(function(b){var d=c(a(this));d.hasClass("open")&&(d.trigger(b=a.Event("hide.bs.dropdown")),b.isDefaultPrevented()||d.removeClass("open").trigger("hidden.bs.dropdown"))})}function c(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}var d=".dropdown-backdrop",e="[data-toggle=dropdown]",f=function(b){a(b).on("click.bs.dropdown",this.toggle)};f.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=c(e),g=f.hasClass("open");if(b(),!g){if("ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a('<div class="dropdown-backdrop"/>').insertAfter(a(this)).on("click",b),f.trigger(d=a.Event("show.bs.dropdown")),d.isDefaultPrevented())return;f.toggleClass("open").trigger("shown.bs.dropdown"),e.focus()}return!1}},f.prototype.keydown=function(b){if(/(38|40|27)/.test(b.keyCode)){var d=a(this);if(b.preventDefault(),b.stopPropagation(),!d.is(".disabled, :disabled")){var f=c(d),g=f.hasClass("open");if(!g||g&&27==b.keyCode)return 27==b.which&&f.find(e).focus(),d.click();var h=a("[role=menu] li:not(.divider):visible a",f);if(h.length){var i=h.index(h.filter(":focus"));38==b.keyCode&&i>0&&i--,40==b.keyCode&&i<h.length-1&&i++,~i||(i=0),h.eq(i).focus()}}}};var g=a.fn.dropdown;a.fn.dropdown=function(b){return this.each(function(){var c=a(this),d=c.data("dropdown");d||c.data("dropdown",d=new f(this)),"string"==typeof b&&d[b].call(c)})},a.fn.dropdown.Constructor=f,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=g,this},a(document).on("click.bs.dropdown.data-api",b).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",e,f.prototype.toggle).on("keydown.bs.dropdown.data-api",e+", [role=menu]",f.prototype.keydown)}(window.jQuery),+function(a){"use strict";var b=function(b,c){this.options=c,this.$element=a(b),this.$backdrop=this.isShown=null,this.options.remote&&this.$element.load(this.options.remote)};b.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},b.prototype.toggle=function(a){return this[this.isShown?"hide":"show"](a)},b.prototype.show=function(b){var c=this,d=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(d),this.isShown||d.isDefaultPrevented()||(this.isShown=!0,this.escape(),this.$element.on("click.dismiss.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.backdrop(function(){var d=a.support.transition&&c.$element.hasClass("fade");c.$element.parent().length||c.$element.appendTo(document.body),c.$element.show(),d&&c.$element[0].offsetWidth,c.$element.addClass("in").attr("aria-hidden",!1),c.enforceFocus();var e=a.Event("shown.bs.modal",{relatedTarget:b});d?c.$element.find(".modal-dialog").one(a.support.transition.end,function(){c.$element.focus().trigger(e)}).emulateTransitionEnd(300):c.$element.focus().trigger(e)}))},b.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").attr("aria-hidden",!0).off("click.dismiss.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one(a.support.transition.end,a.proxy(this.hideModal,this)).emulateTransitionEnd(300):this.hideModal())},b.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.focus()},this))},b.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keyup.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keyup.dismiss.bs.modal")},b.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.removeBackdrop(),a.$element.trigger("hidden.bs.modal")})},b.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},b.prototype.backdrop=function(b){var c=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var d=a.support.transition&&c;if(this.$backdrop=a('<div class="modal-backdrop '+c+'" />').appendTo(document.body),this.$element.on("click.dismiss.modal",a.proxy(function(a){a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus.call(this.$element[0]):this.hide.call(this))},this)),d&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;d?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("in"),a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()):b&&b()};var c=a.fn.modal;a.fn.modal=function(c,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},b.DEFAULTS,e.data(),"object"==typeof c&&c);f||e.data("bs.modal",f=new b(this,g)),"string"==typeof c?f[c](d):g.show&&f.show(d)})},a.fn.modal.Constructor=b,a.fn.modal.noConflict=function(){return a.fn.modal=c,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(b){var c=a(this),d=c.attr("href"),e=a(c.attr("data-target")||d&&d.replace(/.*(?=#[^\s]+$)/,"")),f=e.data("modal")?"toggle":a.extend({remote:!/#/.test(d)&&d},e.data(),c.data());b.preventDefault(),e.modal(f,this).one("hide",function(){c.is(":visible")&&c.focus()})}),a(document).on("show.bs.modal",".modal",function(){a(document.body).addClass("modal-open")}).on("hidden.bs.modal",".modal",function(){a(document.body).removeClass("modal-open")})}(window.jQuery),+function(a){"use strict";var b=function(a,b){this.type=this.options=this.enabled=this.timeout=this.hoverState=this.$element=null,this.init("tooltip",a,b)};b.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1},b.prototype.init=function(b,c,d){this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d);for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focus",i="hover"==g?"mouseleave":"blur";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},b.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},b.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show),void 0):c.show()},b.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type);return clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide),void 0):c.hide()},b.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){if(this.$element.trigger(b),b.isDefaultPrevented())return;var c=this.tip();this.setContent(),this.options.animation&&c.addClass("fade");var d="function"==typeof this.options.placement?this.options.placement.call(this,c[0],this.$element[0]):this.options.placement,e=/\s?auto?\s?/i,f=e.test(d);f&&(d=d.replace(e,"")||"top"),c.detach().css({top:0,left:0,display:"block"}).addClass(d),this.options.container?c.appendTo(this.options.container):c.insertAfter(this.$element);var g=this.getPosition(),h=c[0].offsetWidth,i=c[0].offsetHeight;if(f){var j=this.$element.parent(),k=d,l=document.documentElement.scrollTop||document.body.scrollTop,m="body"==this.options.container?window.innerWidth:j.outerWidth(),n="body"==this.options.container?window.innerHeight:j.outerHeight(),o="body"==this.options.container?0:j.offset().left;d="bottom"==d&&g.top+g.height+i-l>n?"top":"top"==d&&g.top-l-i<0?"bottom":"right"==d&&g.right+h>m?"left":"left"==d&&g.left-h<o?"right":d,c.removeClass(k).addClass(d)}var p=this.getCalculatedOffset(d,g,h,i);this.applyPlacement(p,d),this.$element.trigger("shown.bs."+this.type)}},b.prototype.applyPlacement=function(a,b){var c,d=this.tip(),e=d[0].offsetWidth,f=d[0].offsetHeight,g=parseInt(d.css("margin-top"),10),h=parseInt(d.css("margin-left"),10);isNaN(g)&&(g=0),isNaN(h)&&(h=0),a.top=a.top+g,a.left=a.left+h,d.offset(a).addClass("in");var i=d[0].offsetWidth,j=d[0].offsetHeight;if("top"==b&&j!=f&&(c=!0,a.top=a.top+f-j),/bottom|top/.test(b)){var k=0;a.left<0&&(k=-2*a.left,a.left=0,d.offset(a),i=d[0].offsetWidth,j=d[0].offsetHeight),this.replaceArrow(k-e+i,i,"left")}else this.replaceArrow(j-f,j,"top");c&&d.offset(a)},b.prototype.replaceArrow=function(a,b,c){this.arrow().css(c,a?50*(1-a/b)+"%":"")},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},b.prototype.hide=function(){function b(){"in"!=c.hoverState&&d.detach()}var c=this,d=this.tip(),e=a.Event("hide.bs."+this.type);return this.$element.trigger(e),e.isDefaultPrevented()?void 0:(d.removeClass("in"),a.support.transition&&this.$tip.hasClass("fade")?d.one(a.support.transition.end,b).emulateTransitionEnd(150):b(),this.$element.trigger("hidden.bs."+this.type),this)},b.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},b.prototype.hasContent=function(){return this.getTitle()},b.prototype.getPosition=function(){var b=this.$element[0];return a.extend({},"function"==typeof b.getBoundingClientRect?b.getBoundingClientRect():{width:b.offsetWidth,height:b.offsetHeight},this.$element.offset())},b.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},b.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},b.prototype.tip=function(){return this.$tip=this.$tip||a(this.options.template)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},b.prototype.validate=function(){this.$element[0].parentNode||(this.hide(),this.$element=null,this.options=null)},b.prototype.enable=function(){this.enabled=!0},b.prototype.disable=function(){this.enabled=!1},b.prototype.toggleEnabled=function(){this.enabled=!this.enabled},b.prototype.toggle=function(b){var c=b?a(b.currentTarget)[this.type](this.getDelegateOptions()).data("bs."+this.type):this;c.tip().hasClass("in")?c.leave(c):c.enter(c)},b.prototype.destroy=function(){this.hide().$element.off("."+this.type).removeData("bs."+this.type)};var c=a.fn.tooltip;a.fn.tooltip=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof c&&c;e||d.data("bs.tooltip",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.tooltip.Constructor=b,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=c,this}}(window.jQuery),+function(a){"use strict";var b=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");b.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),b.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),b.prototype.constructor=b,b.prototype.getDefaults=function(){return b.DEFAULTS},b.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content")[this.options.html?"html":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},b.prototype.hasContent=function(){return this.getTitle()||this.getContent()},b.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},b.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")},b.prototype.tip=function(){return this.$tip||(this.$tip=a(this.options.template)),this.$tip};var c=a.fn.popover;a.fn.popover=function(c){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof c&&c;e||d.data("bs.popover",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.popover.Constructor=b,a.fn.popover.noConflict=function(){return a.fn.popover=c,this}}(window.jQuery),+function(a){"use strict";function b(c,d){var e,f=a.proxy(this.process,this);this.$element=a(c).is("body")?a(window):a(c),this.$body=a("body"),this.$scrollElement=this.$element.on("scroll.bs.scroll-spy.data-api",f),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||(e=a(c).attr("href"))&&e.replace(/.*(?=#[^\s]+$)/,"")||"")+" .nav li > a",this.offsets=a([]),this.targets=a([]),this.activeTarget=null,this.refresh(),this.process()}b.DEFAULTS={offset:10},b.prototype.refresh=function(){var b=this.$element[0]==window?"offset":"position";this.offsets=a([]),this.targets=a([]);var c=this;this.$body.find(this.selector).map(function(){var d=a(this),e=d.data("target")||d.attr("href"),f=/^#\w/.test(e)&&a(e);return f&&f.length&&[[f[b]().top+(!a.isWindow(c.$scrollElement.get(0))&&c.$scrollElement.scrollTop()),e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){c.offsets.push(this[0]),c.targets.push(this[1])})},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.$scrollElement[0].scrollHeight||this.$body[0].scrollHeight,d=c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(b>=d)return g!=(a=f.last()[0])&&this.activate(a);for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(!e[a+1]||b<=e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,a(this.selector).parents(".active").removeClass("active");var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate")};var c=a.fn.scrollspy;a.fn.scrollspy=function(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=c,this},a(window).on("load",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);b.scrollspy(b.data())})})}(window.jQuery),+function(a){"use strict";var b=function(b){this.element=a(b)};b.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.attr("data-target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a")[0],f=a.Event("show.bs.tab",{relatedTarget:e});if(b.trigger(f),!f.isDefaultPrevented()){var g=a(d);this.activate(b.parent("li"),c),this.activate(g,g.parent(),function(){b.trigger({type:"shown.bs.tab",relatedTarget:e})})}}},b.prototype.activate=function(b,c,d){function e(){f.removeClass("active").find("> .dropdown-menu > .active").removeClass("active"),b.addClass("active"),g?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu")&&b.closest("li.dropdown").addClass("active"),d&&d()}var f=c.find("> .active"),g=d&&a.support.transition&&f.hasClass("fade");g?f.one(a.support.transition.end,e).emulateTransitionEnd(150):e(),f.removeClass("in")};var c=a.fn.tab;a.fn.tab=function(c){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new b(this)),"string"==typeof c&&e[c]()})},a.fn.tab.Constructor=b,a.fn.tab.noConflict=function(){return a.fn.tab=c,this},a(document).on("click.bs.tab.data-api",'[data-toggle="tab"], [data-toggle="pill"]',function(b){b.preventDefault(),a(this).tab("show")})}(window.jQuery),+function(a){"use strict";var b=function(c,d){this.options=a.extend({},b.DEFAULTS,d),this.$window=a(window).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(c),this.affixed=this.unpin=null,this.checkPosition()};b.RESET="affix affix-top affix-bottom",b.DEFAULTS={offset:0},b.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},b.prototype.checkPosition=function(){if(this.$element.is(":visible")){var c=a(document).height(),d=this.$window.scrollTop(),e=this.$element.offset(),f=this.options.offset,g=f.top,h=f.bottom;"object"!=typeof f&&(h=g=f),"function"==typeof g&&(g=f.top()),"function"==typeof h&&(h=f.bottom());var i=null!=this.unpin&&d+this.unpin<=e.top?!1:null!=h&&e.top+this.$element.height()>=c-h?"bottom":null!=g&&g>=d?"top":!1;this.affixed!==i&&(this.unpin&&this.$element.css("top",""),this.affixed=i,this.unpin="bottom"==i?e.top-d:null,this.$element.removeClass(b.RESET).addClass("affix"+(i?"-"+i:"")),"bottom"==i&&this.$element.offset({top:document.body.offsetHeight-h-this.$element.height()}))}};var c=a.fn.affix;a.fn.affix=function(c){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof c&&c;e||d.data("bs.affix",e=new b(this,f)),"string"==typeof c&&e[c]()})},a.fn.affix.Constructor=b,a.fn.affix.noConflict=function(){return a.fn.affix=c,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var b=a(this),c=b.data();c.offset=c.offset||{},c.offsetBottom&&(c.offset.bottom=c.offsetBottom),c.offsetTop&&(c.offset.top=c.offsetTop),b.affix(c)})})}(window.jQuery);

//js jquery.cookie.js
jQuery.cookie = function(name, value, options) {
    if (typeof value != 'undefined') { // name and value given, set cookie
        options = options || {};
        if (value === null) {
            value = '';
            options = $.extend({}, options); // clone object since it's unexpected behavior if the expired property were changed
            options.expires = -1;
        }
        var expires = '';
        if (options.expires && (typeof options.expires == 'number' || options.expires.toUTCString)) {
            var date;
            if (typeof options.expires == 'number') {
                date = new Date();
                date.setTime(date.getTime() + (options.expires * 24 * 60 * 60 * 1000));
            } else {
                date = options.expires;
            }
            expires = '; expires=' + date.toUTCString(); // use expires attribute, max-age is not supported by IE
        }
        // NOTE Needed to parenthesize options.path and options.domain
        // in the following expressions, otherwise they evaluate to undefined
        // in the packed version for some reason...
        var path = options.path ? '; path=' + (options.path) : '';
        var domain = options.domain ? '; domain=' + (options.domain) : '';
        var secure = options.secure ? '; secure' : '';
        document.cookie = [name, '=', encodeURIComponent(value), expires, path, domain, secure].join('');
    } else { // only name given, get cookie
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
};

//js jquery-te-1.3.2.2.min.js
(function(e){e.fn.jqte=function(t){function f(e,t,n,r,i){var s=a.length+1;return a.push({name:e,cls:s,command:t,key:n,tag:r,emphasis:i})}var n=[{title:"Font Size"},{title:"Color"},{title:"Bold",hotkey:"B"},{title:"Italic",hotkey:"I"},{title:"Underline",hotkey:"U"},{title:"Ordered List",hotkey:"."},{title:"Unordered List",hotkey:","},{title:"Subscript",hotkey:"down arrow"},{title:"Superscript",hotkey:"up arrow"},{title:"Outdent",hotkey:"left arrow"},{title:"Indent",hotkey:"right arrow"},{title:"Justify Left"},{title:"Justify Center"},{title:"Justify Right"},{title:"Strike Through",hotkey:"K"},{title:"Add Link",hotkey:"L"},{title:"Remove Link"}];var r=["10","12","16","18","20","24","28"];var i=["0,0,0","68,68,68","102,102,102","153,153,153","204,204,204","238,238,238","243,243,243","255,255,255","244,204,204","252,229,205","255,242,204","217,234,211","208,224,227","207,226,243","217,210,233","234,209,220","234,153,153","249,203,156","255,229,153","182,215,168","162,196,201","159,197,232","180,167,214","213,166,189","224,102,102","246,178,107","255,217,102","147,196,125","118,165,175","111,168,220","142,124,195","194,123,160","255,0,0","255,153,0","255,255,0","0,255,0","0,255,255","0,0,255","153,0,255","255,0,255","204,0,0","230,145,56","241,194,50","106,168,79","69,129,142","61,133,198","103,78,167","166,77,121","153,0,0","180,95,6","191,144,0","56,118,29","19,79,92","11,83,148","53,28,117","116,27,71","102,0,0","120,63,4","127,96,0","39,78,19","12,52,61","7,55,99","32,18,77","76,17,48"];var s=["Web Address","E-mail Address","Picture URL"];var o=e.extend({css:"jqte",title:true,titletext:n,button:"OK",fsize:true,fsizes:r,funit:"px",color:true,linktypes:s,b:true,i:true,u:true,ol:true,ul:true,sub:true,sup:true,outdent:true,indent:true,left:true,center:true,right:true,strike:true,link:true,unlink:true},t);var u=navigator.userAgent.toLowerCase();if(/msie [1-7]./.test(u))o.title=false;var a=[];f("fsize","fSize","","",false);f("color","colors","","",false);f("b","Bold","B",["b","strong"],true);f("i","Italic","I",["i","em"],true);f("u","Underline","U",["u"],true);f("ol","insertorderedlist","¾",["ol"],true);f("ul","insertunorderedlist","¼",["ul"],true);f("sub","subscript","(",["sub"],true);f("sup","superscript","&",["sup"],true);f("outdent","outdent","%",["blockquote"],false);f("indent","indent","'",["blockquote"],true);f("left","justifyLeft","","",false);f("center","justifyCenter","","",false);f("right","justifyRight","","",false);f("strike","strikeThrough","K",["strike"],true);f("link","linkcreator","L",["a"],true);f("unlink","unlink","",["a"],false);return this.each(function(){function O(){if(window.getSelection){return window.getSelection()}else if(document.selection&&document.selection.createRange&&document.selection.type!="None"){return document.selection.createRange()}}function M(e,t){var n,r=O();if(window.getSelection){if(r.anchorNode&&r.getRangeAt)n=r.getRangeAt(0);if(n){r.removeAllRanges();r.addRange(n)}if(!u.match(/msie/))document.execCommand("StyleWithCSS",false,false);document.execCommand(e,false,t)}else if(document.selection&&document.selection.createRange&&document.selection.type!="None"){n=document.selection.createRange();n.execCommand(e,false,t)}P(false,false)}function _(t,n,r){if(c.not(":focus"))c.focus();if(window.getSelection){var i=O(),s,o,u;if(i.anchorNode&&i.getRangeAt){s=i.getRangeAt(0);o=document.createElement(t);e(o).attr(n,r);u=s.extractContents();o.appendChild(u);s.insertNode(o);i.removeAllRanges();if(n=="style")P(e(o),r);else P(e(o),false)}}else if(document.selection&&document.selection.createRange&&document.selection.type!="None"){var a=document.selection.createRange();var f=a.htmlText;var l="<"+t+" "+n+'="'+r+'">'+f+"</"+t+">";document.selection.createRange().pasteHTML(l)}}function P(e,t){var n=D();n=n?n:e;if(n&&t==false){if(n.parent().is("[style]"))n.attr("style",n.parent().attr("style"));if(n.is("[style]"))n.find("*").attr("style",n.attr("style"))}else if(e&&t&&e.is("[style]")){var r=t.split(";");r=r[0].split(":");if(e.is("[style*="+r[0]+"]"))e.find("*").css(r[0],r[1])}H(n)}function H(t){if(t){var t=t[0];if(document.body.createTextRange){var n=document.body.createTextRange();n.moveToElementText(t);n.select()}else if(window.getSelection){var r=window.getSelection();var n=document.createRange();if(t!="undefined"&&t!=null){n.selectNodeContents(t);r.removeAllRanges();r.addRange(n);if(e(t).is(":empty")){e(t).append(" ");H(e(t))}}}}}function B(){if(!f.data("sourceOpened")){var t=D();var n="http://";F(true);if(t){var r=t.prop("tagName").toLowerCase();if(r=="a"&&t.is("[href]")){n=t.attr("href");t.attr(y,"")}else{_("a",y,"")}}else d.val(n).focus();p.click(function(t){if(e(t.target).hasClass(o.css+"_linktypetext")||e(t.target).hasClass(o.css+"_linktypearrow"))I(true)});m.find("a").click(function(){var t=e(this).attr(o.css+"-linktype");m.data("linktype",t);g.find("."+o.css+"_linktypetext").html(m.find("a:eq("+m.data("linktype")+")").text());q(n);I()});q(n);d.val(n).bind("keypress keyup",function(e){if(e.keyCode==13){j(s.find("["+y+"]"));return false}}).focus();v.click(function(){j(s.find("["+y+"]"))})}else F(false)}function j(t){d.focus();H(t);t.removeAttr(y);if(m.data("linktype")!="2")M("createlink",d.val());else{M("insertImage",d.val());c.find("img").each(function(){var t=e(this).prev("a");var n=e(this).next("a");if(t.length>0&&t.html()=="")t.remove();else if(n.length>0&&n.html()=="")n.remove();if(!e(this).is("[style*=float]"))e(this).css({"float":"left",margin:"0 10px 10px 0"})})}F();V()}function F(e){z("["+y+"]:not([href])");s.find("["+y+"][href]").removeAttr(y);if(e){f.data("linkOpened",true);l.slideDown(100)}else{f.data("linkOpened",false);l.slideUp(100)}I()}function I(e){if(e)m.stop(true,true).delay(100).slideToggle(100);else m.stop(true,true).delay(100).slideUp(100)}function q(e){var t=m.data("linktype");if(t=="1"&&(d.val()=="http://"||d.is("[value^=http://]")||!d.is("[value^=mailto]")))d.val("mailto:");else if(t!="1"&&!d.is("[value^=http://]"))d.val("http://");else d.val(e)}function R(t){if(!f.data("sourceOpened")){if(t=="fSize")styleField=L;else if(t=="colors")styleField=A;U(styleField,true);styleField.find("a").click(function(){var n=e(this).attr(o.css+"-styleval");if(t=="fSize"){styleType="font-size";n=n+o.funit}else if(t=="colors"){styleType="color";n="rgb("+n+")"}var r=W(styleType);_("span","style",styleType+":"+n+";"+r);U("",false);e("."+o.css+"_title").remove();V()})}else U(styleField,false);F(false)}function U(e,t){var n="",r=[{d:"fsizeOpened",f:L},{d:"cpallOpened",f:A}];if(e!=""){for(var i=0;i<r.length;i++){if(e==r[i]["f"])n=r[i]}}if(t){f.data(n["d"],true);n["f"].slideDown(100);for(var i=0;i<r.length;i++){if(n["d"]!=r[i]["d"]){f.data(r[i]["d"],false);r[i]["f"].slideUp(100)}}}else{for(var i=0;i<r.length;i++){f.data(r[i]["d"],false);r[i]["f"].slideUp(100)}}}function z(t){s.find(t).each(function(){e(this).before(e(this).html()).remove()})}function W(e){var t=D();if(t&&t.is("[style]")&&t.css(e)!=""){var n=t.css(e);t.css(e,"");var r=t.attr("style");t.css(e,n);return r}else return""}function X(e){var t,n,r;t=e.replace(/\n/g,"").replace(/\r/g,"").replace(/\t/g,"").replace(/ /g," ");n=[/\<div>(.*?)\<\/div>/ig,/\<br>(.*?)\<br>/ig,/\<br\/>(.*?)\<br\/>/ig,/\<strong>(.*?)\<\/strong>/ig,/\<em>(.*?)\<\/em>/ig];r=["<p>$1</p>","<p>$1</p>","<p>$1</p>","<b>$1</b>","<i>$1</i>"];for(var i=0;i<n.length;i++){t=t.replace(n[i],r[i])}return t}function V(){t.val(X(c.html()))}function J(){c.html(X(t.val()))}function K(t){var n=false,r=D(),i;if(r){e.each(t,function(t,s){i=r.prop("tagName").toLowerCase();if(i==s)n=true;else{r.parents().each(function(){i=e(this).prop("tagName").toLowerCase();if(i==s)n=true})}});return n}else return false}function Q(t){for(var n=0;n<a.length;n++){if(o[a[n].name]&&a[n].emphasis&&a[n].tag!="")K(a[n].tag)?f.find("."+o.css+"_tool_"+a[n].cls).addClass(h):e("."+o.css+"_tool_"+a[n].cls).removeClass(h);U("",false)}}var t=e(this);var n=e(this).is("[value]")||e(this).prop("tagName").toLowerCase()=="textarea"?e(this).val():e(this).html();var r=e(this).is("[name]")?' name="'+e(this).attr("name")+'"':"";t.after('<div class="'+o.css+'" ></div>');var s=t.next("."+o.css);s.html('<div class="'+o.css+"_toolbar"+'" role="toolbar" unselectable></div><div class="'+o.css+'_linkform" style="display:none" role="dialog"></div><div class="'+o.css+"_editor"+'"></div>');var f=s.find("."+o.css+"_toolbar");var l=s.find("."+o.css+"_linkform");var c=s.find("."+o.css+"_editor");var h=o.css+"_tool_depressed";l.append('<div class="'+o.css+'_linktypeselect" unselectable></div> <input class="'+o.css+'_linkinput" type="text/css" value=""> <div class="'+o.css+'_linkbutton" unselectable>'+o.button+'</div> <div style="height:1px;float:none;clear:both"></div>');var p=l.find("."+o.css+"_linktypeselect");var d=l.find("."+o.css+"_linkinput");var v=l.find("."+o.css+"_linkbutton");p.append('<div class="'+o.css+'_linktypeview" unselectable></div><div class="'+o.css+'_linktypes" role="menu" unselectable></div>');var m=p.find("."+o.css+"_linktypes");var g=p.find("."+o.css+"_linktypeview");var y=o.css+"-setlink";var b=o.css+"-setfsize";c.after('<div class="'+o.css+"_source "+o.css+'_hiddenField"><textarea '+r+">"+n+"</textarea></div>");t.remove();var w=s.find("."+o.css+"_source");t=w.find("textarea");c.attr("contenteditable","true").html(n);for(var E=0;E<a.length;E++){if(o[a[E].name]){var S=a[E].key.length>0?o.titletext[E].hotkey!=null&&o.titletext[E].hotkey!="undefined"&&o.titletext[E].hotkey!=""?" (Ctrl+"+o.titletext[E].hotkey+")":"":"";var x=o.titletext[E].title!=null&&o.titletext[E].title!="undefined"&&o.titletext[E].title!=""?o.titletext[E].title+S:"";f.append('<div class="'+o.css+"_tool "+o.css+"_tool_"+a[E].cls+'" role="button" data-tool="'+E+'" unselectable><a class="'+o.css+'_tool_icon" unselectable></a></div>');f.find("."+o.css+"_tool[data-tool="+E+"]").data({tag:a[E].tag,command:a[E].command,emphasis:a[E].emphasis,title:x});if(a[E].name=="fsize"){f.find("."+o.css+"_tool_"+a[E].cls).append('<div class="'+o.css+'_fontsizes" unselectable style="position:absolute;display:none"></div>');for(var T=0;T<o.fsizes.length;T++){f.find("."+o.css+"_fontsizes").append("<a "+o.css+'-styleval="'+o.fsizes[T]+'" class="'+o.css+"_fontsize"+'" style="font-size:'+o.fsizes[T]+o.funit+'" role="menuitem" unselectable>Abcdefgh...</a>')}}else if(a[E].name=="color"){f.find("."+o.css+"_tool_"+a[E].cls).append('<div class="'+o.css+'_cpalette" unselectable style="position:absolute;display:none"></div>');for(var N=0;N<i.length;N++){f.find("."+o.css+"_cpalette").append("<a "+o.css+'-styleval="'+i[N]+'" class="'+o.css+"_color"+'" style="background-color: rgb('+i[N]+')" role="gridcell" unselectable></a>')}}}}m.data("linktype","0");for(var E=0;E<3;E++){m.append("<a "+o.css+'-linktype="'+E+'" unselectable>'+o.linktypes[E]+"</a>");g.html('<div class="'+o.css+'_linktypearrow" unselectable></div><div class="'+o.css+'_linktypetext">'+m.find("a:eq("+m.data("linktype")+")").text()+"</div>")}var C="";if(/msie/.test(u))C="-ms-";else if(/chrome/.test(u)||/safari/.test(u)||/yandex/.test(u))C="-webkit-";else if(/mozilla/.test(u))C="-moz-";else if(/opera/.test(u))C="-o-";else if(/konqueror/.test(u))C="-khtml-";s.find("[unselectable]").css(C+"user-select","none").attr("unselectable","on").on("selectstart mousedown",function(e){e.preventDefault()});var k=f.find("."+o.css+"_tool");var L=f.find("."+o.css+"_fontsizes");var A=f.find("."+o.css+"_cpalette");var D=function(){var t,n;if(window.getSelection){n=getSelection();t=n.anchorNode}if(!t&&document.selection&&document.selection.createRange&&document.selection.type!="None"){n=document.selection;var r=n.getRangeAt?n.getRangeAt(0):n.createRange();t=r.commonAncestorContainer?r.commonAncestorContainer:r.parentElement?r.parentElement():r.item(0)}if(t){return t.nodeName=="#text"?e(t.parentNode):e(t)}else return false};k.click(function(n){if(e(this).data("command")=="displaysource"&&!f.data("sourceOpened")){f.find("."+o.css+"_tool").addClass(o.css+"_hiddenField");e(this).removeClass(o.css+"_hiddenField");f.data("sourceOpened",true);t.css("height",c.outerHeight());w.removeClass(o.css+"_hiddenField");c.addClass(o.css+"_hiddenField");t.focus();V();F(false);U("",false)}else{if(!f.data("sourceOpened")){if(e(this).data("command")=="linkcreator"){if(!f.data("linkOpened"))B();else{F(false)}}else if(e(this).data("command")=="fSize"&&!e(n.target).hasClass(o.css+"_fontsize")||e(this).data("command")=="colors"&&!e(n.target).hasClass(o.css+"_color")){R(e(this).data("command"))}else{if(c.not(":focus"))c.focus();M(e(this).data("command"),null);F(false);U("",false);e(this).data("emphasis")==true&&!e(this).hasClass(h)?e(this).addClass(h):e(this).removeClass(h);w.addClass(o.css+"_hiddenField");c.removeClass(o.css+"_hiddenField");V()}}else{f.data("sourceOpened",false);f.find("."+o.css+"_tool").removeClass(o.css+"_hiddenField");w.addClass(o.css+"_hiddenField");c.removeClass(o.css+"_hiddenField");if(c.not(":focus"))c.focus()}}}).hover(function(t){if(o.title&&e(this).data("title")!=""&&(e(t.target).hasClass(o.css+"_tool")||e(t.target).hasClass(o.css+"_tool_icon"))){e("."+o.css+"_title").remove();s.append('<div class="'+o.css+'_title"><div class="'+o.css+'_titleArrow"><div class="'+o.css+'_titleArrowIcon"></div></div><div class="'+o.css+'_titleText">'+e(this).data("title")+"</div></div>");var n=e("."+o.css+"_title:first");var r=n.find("."+o.css+"_titleArrowIcon");var i=e(this).position();var u=i.left+e(this).outerWidth()-n.outerWidth()/2-e(this).outerWidth()/2;var a=i.top+e(this).outerHeight()+5;n.delay(400).css({top:a,left:u}).fadeIn(200)}},function(){e("."+o.css+"_title").remove()});c.bind("keypress keyup keydown drop cut copy paste DOMCharacterDataModified DOMSubtreeModified",function(){if(!f.data("sourceOpened"))e(this).trigger("change");I()}).bind("change",function(){if(!f.data("sourceOpened"))setTimeout(V,0)}).keydown(function(e){if(e.ctrlKey){for(var t=0;t<a.length;t++){if(o[a[t].name]&&e.keyCode==a[t].key.charCodeAt(0)){if(a[t].command!=""&&a[t].command!="linkcreator")M(a[t].command,null);else if(a[t].command=="linkcreator")B();return false}}}}).bind("mouseup keyup",Q).focus(function(){I()}).focusout(function(){k.removeClass(h);U("",false);I()});t.bind("keydown keyup",function(){setTimeout(J,0);e(this).height(e(this)[0].scrollHeight);if(e(this).val()=="")e(this).height(0)})})}})(jQuery)

//js js/cgjs/jscal2.js
Calendar=function(){function t(e){var n,a,s
e=e||{},this.args=e=E(e,{animation:!ce,cont:null,bottomBar:!0,date:!0,fdow:_("fdow"),min:null,max:null,reverseWheel:!1,selection:[],selectionType:t.SEL_SINGLE,weekNumbers:!1,align:"Bl/ / /T/r",inputField:null,trigger:null,dateFormat:"%Y-%m-%d",multiCtrl:!0,fixed:!1,opacity:le?1:3,titleFormat:"%b %Y",showTime:!1,timePos:"right",time:!0,minuteStep:5,noScroll:!1,disabled:se,checkRange:!1,dateInfo:se,onChange:se,onSelect:se,onTimeChange:se,onFocus:se,onBlur:se,onClose:se}),this.handlers={},n=this,a=new Date,e.min=k(e.min),e.max=k(e.max),e.date===!0&&(e.date=a),e.time===!0&&(e.time=a.getHours()*100+Math.floor(a.getMinutes()/e.minuteStep)*e.minuteStep),this.date=k(e.date),this.time=e.time,this.fdow=e.fdow,q("onChange onSelect onTimeChange onFocus onBlur onClose".split(/\s+/),function(t){var a=e[t]
a instanceof Array||(a=[a]),n.handlers[t]=a}),this.selection=new t.Selection(e.selection,e.selectionType,C,this),s=l(this),e.cont&&J(e.cont).appendChild(s),e.trigger&&this.manageFields(e.trigger,e.inputField,e.dateFormat)}function e(t){var e,n=["<table",Q,"><tr>"],a=0
for(t.args.weekNumbers&&n.push("<td><div class='DynarchCalendar-weekNumber'>",_("wk"),"</div></td>");7>a;)e=(a++ +t.fdow)%7,n.push("<td><div",_("weekend").indexOf(e)<0?">":" class='DynarchCalendar-weekend'>",_("sdn")[e],"</div></td>")
return n.push("</tr></table>"),n.join("")}function n(t,e,n){var a,s,i,r,o,l,c,h,u,d,f,y,m,p,g,v,D
for(e=e||t.date,n=n||t.fdow,e=new Date(e.getFullYear(),e.getMonth(),e.getDate(),12,0,0,0),a=e.getMonth(),s=[],i=0,r=t.args.weekNumbers,e.setDate(1),o=(e.getDay()-n)%7,0>o&&(o+=7),e.setDate(0-o),e.setDate(e.getDate()+1),l=new Date,c=l.getDate(),h=l.getMonth(),u=l.getFullYear(),s[i++]="<table class='DynarchCalendar-bodyTable'"+Q+">",d=0;6>d;++d){for(s[i++]="<tr class='DynarchCalendar-week",0==d&&(s[i++]=" DynarchCalendar-first-row"),5==d&&(s[i++]=" DynarchCalendar-last-row"),s[i++]="'>",r&&(s[i++]="<td class='DynarchCalendar-first-col'><div class='DynarchCalendar-weekNumber'>"+T(e)+"</div></td>"),f=0;7>f;++f)y=e.getDate(),m=e.getMonth(),p=e.getFullYear(),g=1e4*p+100*(m+1)+y,v=t.selection.isSelected(g),D=t.isDisabled(e),s[i++]="<td class='",0!=f||r||(s[i++]=" DynarchCalendar-first-col"),0==f&&0==d&&(t._firstDateVisible=g),6==f&&(s[i++]=" DynarchCalendar-last-col",5==d&&(t._lastDateVisible=g)),v&&(s[i++]=" DynarchCalendar-td-selected"),s[i++]="'><div dyc-type='date' unselectable='on' dyc-date='"+g+"' ",D&&(s[i++]="disabled='1' "),s[i++]="class='DynarchCalendar-day",_("weekend").indexOf(e.getDay())<0||(s[i++]=" DynarchCalendar-weekend"),m!=a&&(s[i++]=" DynarchCalendar-day-othermonth"),y==c&&m==h&&p==u&&(s[i++]=" DynarchCalendar-day-today"),D&&(s[i++]=" DynarchCalendar-day-disabled"),v&&(s[i++]=" DynarchCalendar-day-selected"),D=t.args.dateInfo(e),D&&D.klass&&(s[i++]=" "+D.klass),s[i++]="'>"+y+"</div></td>",e=new Date(p,m,y+1,12,0,0,0)
s[i++]="</tr>"}return s[i++]="</table>",s.join("")}function a(t){var n=["<table class='DynarchCalendar-topCont'",Q,"><tr><td>","<div class='DynarchCalendar'>",le?"<a class='DynarchCalendar-focusLink' href='#'></a>":"<button class='DynarchCalendar-focusLink'></button>","<div class='DynarchCalendar-topBar'>","<div dyc-type='nav' dyc-btn='-Y' dyc-cls='hover-navBtn,pressed-navBtn' ","class='DynarchCalendar-navBtn DynarchCalendar-prevYear'><div></div></div>","<div dyc-type='nav' dyc-btn='+Y' dyc-cls='hover-navBtn,pressed-navBtn' ","class='DynarchCalendar-navBtn DynarchCalendar-nextYear'><div></div></div>","<div dyc-type='nav' dyc-btn='-M' dyc-cls='hover-navBtn,pressed-navBtn' ","class='DynarchCalendar-navBtn DynarchCalendar-prevMonth'><div></div></div>","<div dyc-type='nav' dyc-btn='+M' dyc-cls='hover-navBtn,pressed-navBtn' ","class='DynarchCalendar-navBtn DynarchCalendar-nextMonth'><div></div></div>","<table class='DynarchCalendar-titleCont'",Q,"><tr><td>","<div dyc-type='title' dyc-btn='menu' dyc-cls='hover-title,pressed-title' class='DynarchCalendar-title'>",s(t),"</div></td></tr></table>","<div class='DynarchCalendar-dayNames'>",e(t),"</div>","</div>","<div class='DynarchCalendar-body'></div>"]
return(t.args.bottomBar||t.args.showTime)&&n.push("<div class='DynarchCalendar-bottomBar'>",o(t),"</div>"),n.push("<div class='DynarchCalendar-menu' style='display: none'>",i(t),"</div>","<div class='DynarchCalendar-tooltip'></div>","</div>","</td></tr></table>"),n.join("")}function s(t){return"<div unselectable='on'>"+S(t.date,t.args.titleFormat)+"</div>"}function i(t){for(var e,n=["<table height='100%'",Q,"><tr><td>","<table style='margin-top: 1.5em'",Q,">","<tr><td colspan='3'><input dyc-btn='year' class='DynarchCalendar-menu-year' size='6' value='",t.date.getFullYear(),"' /></td></tr>","<tr><td><div dyc-type='menubtn' dyc-cls='hover-navBtn,pressed-navBtn' dyc-btn='today'>",_("goToday"),"</div></td></tr>","</table>","<p class='DynarchCalendar-menu-sep'>&nbsp;</p>","<table class='DynarchCalendar-menu-mtable'",Q,">"],a=_("smn"),s=0,i=n.length;12>s;){for(n[i++]="<tr>",e=4;--e>0;)n[i++]="<td><div dyc-type='menubtn' dyc-cls='hover-navBtn,pressed-navBtn' dyc-btn='m"+s+"' class='DynarchCalendar-menu-month'>"+a[s++]+"</div></td>"
n[i++]="</tr>"}return n[i++]="</table></td></tr></table>",n.join("")}function r(t,e){e.push("<table class='DynarchCalendar-time'"+Q+"><tr>","<td rowspan='2'><div dyc-type='time-hour' dyc-cls='hover-time,pressed-time' class='DynarchCalendar-time-hour'></div></td>","<td dyc-type='time-hour+' dyc-cls='hover-time,pressed-time' class='DynarchCalendar-time-up'></td>","<td rowspan='2' class='DynarchCalendar-time-sep'></td>","<td rowspan='2'><div dyc-type='time-min' dyc-cls='hover-time,pressed-time' class='DynarchCalendar-time-minute'></div></td>","<td dyc-type='time-min+' dyc-cls='hover-time,pressed-time' class='DynarchCalendar-time-up'></td>"),t.args.showTime==12&&e.push("<td rowspan='2' class='DynarchCalendar-time-sep'></td>","<td rowspan='2'><div class='DynarchCalendar-time-am' dyc-type='time-am' dyc-cls='hover-time,pressed-time'></div></td>"),e.push("</tr><tr>","<td dyc-type='time-hour-' dyc-cls='hover-time,pressed-time' class='DynarchCalendar-time-down'></td>","<td dyc-type='time-min-' dyc-cls='hover-time,pressed-time' class='DynarchCalendar-time-down'></td>","</tr></table>")}function o(t){function e(){a.showTime&&(n.push("<td>"),r(t,n),n.push("</td>"))}var n=[],a=t.args
return n.push("<table",Q," style='width:100%'><tr>"),a.timePos=="left"&&e(),a.bottomBar&&(n.push("<td>"),n.push("<table",Q,"><tr><td>","<div dyc-btn='today' dyc-cls='hover-bottomBar-today,pressed-bottomBar-today' dyc-type='bottomBar-today' ","class='DynarchCalendar-bottomBar-today'>",_("today"),"</div>","</td></tr></table>"),n.push("</td>")),a.timePos=="right"&&e(),n.push("</tr></table>"),n.join("")}function l(t){var e=V("div"),n=t.els={},s={mousedown:O(m,t,!0),mouseup:O(m,t,!1),mouseover:O(D,t,!0),mouseout:O(D,t,!1),keypress:O(w,t)}
return t.args.noScroll||(s[he?"DOMMouseScroll":"mousewheel"]=O(b,t)),le&&(s.dblclick=s.mousedown,s.keydown=s.keypress),e.innerHTML=a(t),W(e.firstChild,function(t){var e=Z[t.className]
e&&(n[e]=t),le&&t.setAttribute("unselectable","on")}),B(n.main,s),B([n.focusLink,n.yearInput],t._focusEvents={focus:O(c,t),blur:O(u,t)}),t.moveTo(t.date,!1),t.setTime(null,!0),n.topCont}function c(){this._bluringTimeout&&clearTimeout(this._bluringTimeout),this.focused=!0,P(this.els.main,"DynarchCalendar-focused"),this.callHooks("onFocus",this)}function h(){this.focused=!1,Y(this.els.main,"DynarchCalendar-focused"),this._menuVisible&&y(this,!1),this.args.cont||this.hide(),this.callHooks("onBlur",this)}function u(){this._bluringTimeout=setTimeout(O(h,this),50)}function d(t){switch(t){case"time-hour+":this.setHours(this.getHours()+1)
break
case"time-hour-":this.setHours(this.getHours()-1)
break
case"time-min+":this.setMinutes(this.getMinutes()+this.args.minuteStep)
break
case"time-min-":this.setMinutes(this.getMinutes()-this.args.minuteStep)
break
default:return}}function f(t,e,n){this._bodyAnim&&this._bodyAnim.stop()
var a
if(0!=e)switch(a=new Date(t.date),a.setDate(1),e){case"-Y":case-2:a.setFullYear(a.getFullYear()-1)
break
case"+Y":case 2:a.setFullYear(a.getFullYear()+1)
break
case"-M":case-1:a.setMonth(a.getMonth()-1)
break
case"+M":case 1:a.setMonth(a.getMonth()+1)}else a=new Date
return t.moveTo(a,!n)}function y(t,e){var n,a
t._menuVisible=e,R(e,t.els.title,"DynarchCalendar-pressed-title"),n=t.els.menu,ce&&(n.style.height=t.els.main.offsetHeight+"px"),t.args.animation?(t._menuAnim&&t._menuAnim.stop(),a=t.els.main.offsetHeight,ce&&(n.style.width=t.els.topBar.offsetWidth+"px"),e&&(n.firstChild.style.marginTop=-a+"px",t.args.opacity>0&&$(n,0),j(n,!0)),t._menuAnim=K({onUpdate:function(s,i){n.firstChild.style.marginTop=i(ae.accel_b(s),-a,0,!e)+"px",t.args.opacity>0&&$(n,i(ae.accel_b(s),0,.85,!e))},onStop:function(){t.args.opacity>0&&$(n,.85),n.firstChild.style.marginTop="",t._menuAnim=null,e||(j(n,!1),t.focused&&t.focus())}})):(j(n,e),t.focused&&t.focus())}function m(e,n){var a,s,i,r,o,l,c,h,u
n=n||window.event,a=g(n),a&&!a.getAttribute("disabled")&&(i=a.getAttribute("dyc-btn"),r=a.getAttribute("dyc-type"),o=a.getAttribute("dyc-date"),l=this.selection,c={mouseover:N,mousemove:N,mouseup:function(){var t=a.getAttribute("dyc-cls")
t&&Y(a,v(t,1)),clearTimeout(s),F(document,c,!0),c=null}},e?(setTimeout(O(this.focus,this),1),h=a.getAttribute("dyc-cls"),h&&P(a,v(h,1)),"menu"==i?this.toggleMenu():a&&/^[+-][MY]$/.test(i)?f(this,i)?(u=O(function(){f(this,i,!0)?s=setTimeout(u,40):(c.mouseup(),f(this,i))},this),s=setTimeout(u,350),B(document,c,!0)):c.mouseup():"year"==i?(this.els.yearInput.focus(),this.els.yearInput.select()):"time-am"==r?B(document,c,!0):/^time/.test(r)?(u=O(function(t){d.call(this,t),s=setTimeout(u,100)},this,r),d.call(this,r),s=setTimeout(u,350),B(document,c,!0)):(o&&l.type&&(l.type==t.SEL_MULTIPLE?n.shiftKey&&this._selRangeStart?l.selectRange(this._selRangeStart,o):(n.ctrlKey||l.isSelected(o)||!this.args.multiCtrl||l.clear(!0),l.set(o,!0),this._selRangeStart=o):(l.set(o),this.moveTo(A(o),2)),a=this._getDateDiv(o),D.call(this,!0,{target:a})),B(document,c,!0)),le&&c&&/dbl/i.test(n.type)&&c.mouseup(),this.args.fixed||!/^(DynarchCalendar-(topBar|bottomBar|weekend|weekNumber|menu(-sep)?))?$/.test(a.className)||this.args.cont||(c.mousemove=O(p,this),this._mouseDiff=z(n,G(this.els.topCont)),B(document,c,!0))):"today"==i?(this._menuVisible||l.type!=t.SEL_SINGLE||l.set(new Date),this.moveTo(new Date,!0),y(this,!1)):/^m([0-9]+)/.test(i)?(o=new Date(this.date),o.setDate(1),o.setMonth(RegExp.$1),o.setFullYear(this._getInputYear()),this.moveTo(o,!0),y(this,!1)):"time-am"==r&&this.setHours(this.getHours()+12),le||N(n))}function p(t){t=t||window.event
var e=this.els.topCont.style,n=z(t,this._mouseDiff)
e.left=n.x+"px",e.top=n.y+"px"}function g(t){for(var e=t.target||t.srcElement,n=e;e&&e.getAttribute&&!e.getAttribute("dyc-type");)e=e.parentNode
return e.getAttribute&&e||n}function v(t,e){return"DynarchCalendar-"+t.split(/,/)[e]}function D(t,e){var n,a,s
e=e||window.event,n=g(e),n&&(a=n.getAttribute("dyc-type"),a&&!n.getAttribute("disabled")&&(t&&this._bodyAnim&&"date"==a||(s=n.getAttribute("dyc-cls"),s=s?v(s,0):"DynarchCalendar-hover-"+a,("date"!=a||this.selection.type)&&R(t,n,s),"date"==a&&(R(t,n.parentNode.parentNode,"DynarchCalendar-hover-week"),this._showTooltip(n.getAttribute("dyc-date"))),/^time-hour/.test(a)&&R(t,this.els.timeHour,"DynarchCalendar-hover-time"),/^time-min/.test(a)&&R(t,this.els.timeMinute,"DynarchCalendar-hover-time"),Y(this._getDateDiv(this._lastHoverDate),"DynarchCalendar-hover-date"),this._lastHoverDate=null))),t||this._showTooltip()}function b(t){var e,n,a,s
if(t=t||window.event,e=g(t))if(n=e.getAttribute("dyc-btn"),a=e.getAttribute("dyc-type"),s=t.wheelDelta?t.wheelDelta/120:-t.detail/3,s=0>s?-1:s>0?1:0,this.args.reverseWheel&&(s=-s),/^(time-(hour|min))/.test(a)){switch(RegExp.$1){case"time-hour":this.setHours(this.getHours()+s)
break
case"time-min":this.setMinutes(this.getMinutes()+this.args.minuteStep*s)}N(t)}else/Y/i.test(n)&&(s*=2),f(this,-s),N(t)}function C(){var t,e,n
this.refresh(),t=this.inputField,e=this.selection,t&&(n=e.print(this.dateFormat),/input|textarea/i.test(t.tagName)?t.value=n:t.innerHTML=n),this.callHooks("onSelect",this,e)}function w(e){var n,a,s,i,r,o,l,c,h,u,d,m,p
if(!this._menuAnim){if(e=e||window.event,n=e.target||e.srcElement,a=n.getAttribute("dyc-btn"),s=e.keyCode,i=e.charCode||s,r=ee[s],"year"==a&&13==s)return o=new Date(this.date),o.setDate(1),o.setFullYear(this._getInputYear()),this.moveTo(o,!0),y(this,!1),N(e)
if(this._menuVisible){if(27==s)return y(this,!1),N(e)}else{if(e.ctrlKey||(r=null),null!=r||e.ctrlKey||(r=ne[s]),36==s&&(r=0),null!=r)return f(this,r),N(e)
if(i=String.fromCharCode(i).toLowerCase(),l=this.els.yearInput,c=this.selection," "==i)return y(this,!0),this.focus(),l.focus(),l.select(),N(e)
if(i>="0"&&"9">=i)return y(this,!0),this.focus(),l.value=i,l.focus(),N(e)
for(u=_("mn"),d=e.shiftKey?-1:this.date.getMonth(),m=0;++m<12;)if(h=u[(d+m)%12].toLowerCase(),h.indexOf(i)==0)return o=new Date(this.date),o.setDate(1),o.setMonth((d+m)%12),this.moveTo(o,!0),N(e)
if(s>=37&&40>=s){if(o=this._lastHoverDate,o||c.isEmpty()||(o=39>s?c.getFirstDate():c.getLastDate(),(o<this._firstDateVisible||o>this._lastDateVisible)&&(o=null)),o){for(p=o,o=A(o),d=100;d-->0;){switch(s){case 37:o.setDate(o.getDate()-1)
break
case 38:o.setDate(o.getDate()-7)
break
case 39:o.setDate(o.getDate()+1)
break
case 40:o.setDate(o.getDate()+7)}if(!this.isDisabled(o))break}o=L(o),(o<this._firstDateVisible||o>this._lastDateVisible)&&this.moveTo(o)}else o=39>s?this._lastDateVisible:this._firstDateVisible
return Y(this._getDateDiv(p),P(this._getDateDiv(o),"DynarchCalendar-hover-date")),this._lastHoverDate=o,N(e)}if(13==s&&this._lastHoverDate)return c.type==t.SEL_MULTIPLE&&(e.shiftKey||e.ctrlKey)?(e.shiftKey&&this._selRangeStart&&(c.clear(!0),c.selectRange(this._selRangeStart,this._lastHoverDate)),e.ctrlKey&&c.set(this._selRangeStart=this._lastHoverDate,!0)):c.reset(this._selRangeStart=this._lastHoverDate),N(e)
27!=s||this.args.cont||this.hide()}}}function M(t,e){return t.replace(/\$\{([^:\}]+)(:[^\}]+)?\}/g,function(t,n,a){var s,i=e[n]
return a&&(s=a.substr(1).split(/\s*\|\s*/),i=(i<s.length?s[i]:s[s.length-1]).replace(/##?/g,function(t){return t.length==2?"#":i})),i})}function _(t,e){var n=de.__.data[t]
return e&&"string"==typeof n&&(n=M(n,e)),n}function T(t){var e,n
return t=new Date(t.getFullYear(),t.getMonth(),t.getDate(),12,0,0),e=t.getDay(),t.setDate(t.getDate()-(e+6)%7+3),n=t.valueOf(),t.setMonth(0),t.setDate(4),Math.round((n-t.valueOf())/6048e5)+1}function x(t){var e,n
return t=new Date(t.getFullYear(),t.getMonth(),t.getDate(),12,0,0),e=new Date(t.getFullYear(),0,1,12,0,0),n=t-e,Math.floor(n/864e5)}function L(t){return t instanceof Date?1e4*t.getFullYear()+100*(t.getMonth()+1)+t.getDate():"string"==typeof t?parseInt(t,10):t}function A(t,e,n,a,s){var i,r
return t instanceof Date||(t=parseInt(t,10),i=Math.floor(t/1e4),t%=1e4,r=Math.floor(t/100),t%=100,t=new Date(i,r-1,t,null==e?12:e,null==n?0:n,null==a?0:a,null==s?0:s)),t}function H(t,e,n){var a=t.getFullYear(),s=t.getMonth(),i=t.getDate(),r=e.getFullYear(),o=e.getMonth(),l=e.getDate()
return r>a?-3:a>r?3:o>s?-2:s>o?2:n?0:l>i?-1:i>l?1:0}function S(t,e){var n,a=t.getMonth(),s=t.getDate(),i=t.getFullYear(),r=T(t),o=t.getDay(),l=t.getHours(),c=l>=12,h=c?l-12:l,u=x(t),d=t.getMinutes(),f=t.getSeconds(),y=/%./g
return 0===h&&(h=12),n={"%a":_("sdn")[o],"%A":_("dn")[o],"%b":_("smn")[a],"%B":_("mn")[a],"%C":1+Math.floor(i/100),"%d":10>s?"0"+s:s,"%e":s,"%H":10>l?"0"+l:l,"%I":10>h?"0"+h:h,"%j":10>u?"00"+u:100>u?"0"+u:u,"%k":l,"%l":h,"%m":9>a?"0"+(1+a):1+a,"%o":1+a,"%M":10>d?"0"+d:d,"%n":"\n","%p":c?"PM":"AM","%P":c?"pm":"am","%s":Math.floor(t.getTime()/1e3),"%S":10>f?"0"+f:f,"%t":"	","%U":10>r?"0"+r:r,"%W":10>r?"0"+r:r,"%V":10>r?"0"+r:r,"%u":o+1,"%w":o,"%y":(""+i).substr(2,2),"%Y":i,"%%":"%"},e.replace(y,function(t){return n.hasOwnProperty(t)?n[t]:t})}function k(t){if(t){if("number"==typeof t)return A(t)
if(!(t instanceof Date)){var e=t.split(/-/)
return new Date(parseInt(e[0],10),parseInt(e[1],10)-1,parseInt(e[2],10),12,0,0,0)}}return t}function I(t){function e(e){for(var n=e.length;--n>=0;)if(e[n].toLowerCase().indexOf(t)==0)return n+1}return/\S/.test(t)?(t=t.toLowerCase(),e(_("smn"))||e(_("mn"))):void 0}function E(t,e,n,a){a={}
for(n in e)e.hasOwnProperty(n)&&(a[n]=e[n])
for(n in t)t.hasOwnProperty(n)&&(a[n]=t[n])
return a}function B(t,e,n,a){var s
if(t instanceof Array)for(s=t.length;--s>=0;)B(t[s],e,n,a)
else if("object"==typeof e)for(s in e)e.hasOwnProperty(s)&&B(t,s,e[s],n)
else t.addEventListener?t.addEventListener(e,n,le?!0:!!a):t.attachEvent?t.attachEvent("on"+e,n):t["on"+e]=n}function F(t,e,n,a){var s
if(t instanceof Array)for(s=t.length;--s>=0;)F(t[s],e,n)
else if("object"==typeof e)for(s in e)e.hasOwnProperty(s)&&F(t,s,e[s],n)
else t.removeEventListener?t.removeEventListener(e,n,le?!0:!!a):t.detachEvent?t.detachEvent("on"+e,n):t["on"+e]=null}function N(t){return t=t||window.event,le?(t.cancelBubble=!0,t.returnValue=!1):(t.preventDefault(),t.stopPropagation()),!1}function Y(t,e,n){if(t){var a,s=t.className.replace(/^\s+|\s+$/,"").split(/\x20/),i=[]
for(a=s.length;a>0;)s[--a]!=e&&i.push(s[a])
n&&i.push(n),t.className=i.join(" ")}return n}function P(t,e){return Y(t,e,e)}function R(t,e,n){if(e instanceof Array)for(var a=e.length;--a>=0;)R(t,e[a],n)
else Y(e,n,t?n:null)
return t}function V(t,e,n){var a=null
return a=document.createElementNS?document.createElementNS("http://www.w3.org/1999/xhtml",t):document.createElement(t),e&&(a.className=e),n&&n.appendChild(a),a}function U(t,e){null==e&&(e=0)
var n,a,s
try{n=Array.prototype.slice.call(t,e)}catch(i){for(n=Array(t.length-e),a=e,s=0;a<t.length;++a,++s)n[s]=t[a]}return n}function O(t,e){var n=U(arguments,2)
return void 0==e?function(){return t.apply(this,n.concat(U(arguments)))}:function(){return t.apply(e,n.concat(U(arguments)))}}function W(t,e){if(!e(t))for(var n=t.firstChild;n;n=n.nextSibling)n.nodeType==1&&W(n,e)}function K(t,e,n){function a(t,e,n,a){return a?n+t*(e-n):e+t*(n-e)}function s(){e&&i(),n=0,e=setInterval(r,1e3/t.fps)}function i(){e&&(clearInterval(e),e=null),t.onStop(n/t.len,a)}function r(){var e=t.len
t.onUpdate(n/e,a),n==e&&i(),++n}return t=E(t,{fps:50,len:15,onUpdate:se,onStop:se}),le&&(t.len=Math.round(t.len/2)),s(),{start:s,stop:i,update:r,args:t,map:a}}function $(t,e){return""===e?le?t.style.filter="":t.style.opacity="":null!=e?le?t.style.filter="alpha(opacity="+100*e+")":t.style.opacity=e:le?/alpha\(opacity=([0-9.])+\)/.test(t.style.opacity)&&(e=parseFloat(RegExp.$1)/100):e=parseFloat(t.style.opacity),e}function j(t,e){var n=t.style
return null!=e&&(n.display=e?"":"none"),n.display!="none"}function z(t,e){var n=le?t.clientX+document.body.scrollLeft:t.pageX,a=le?t.clientY+document.body.scrollTop:t.pageY
return e&&(n-=e.x,a-=e.y),{x:n,y:a}}function G(t){var e,n,a
if(t.getBoundingClientRect)return e=t.getBoundingClientRect(),{x:e.left-document.documentElement.clientLeft+document.body.scrollLeft,y:e.top-document.documentElement.clientTop+document.body.scrollTop}
n=0,a=0
do n+=t.offsetLeft-t.scrollLeft,a+=t.offsetTop-t.scrollTop
while(t=t.offsetParent)
return{x:n,x:a}}function X(){var t=document.documentElement,e=document.body
return{x:t.scrollLeft||e.scrollLeft,y:t.scrollTop||e.scrollTop,w:t.clientWidth||window.innerWidth||e.clientWidth,h:t.clientHeight||window.innerHeight||e.clientHeight}}function q(t,e,n){for(n=0;n<t.length;++n)e(t[n])}function J(t){return"string"==typeof t&&(t=document.getElementById(t)),t}var Q,Z,te,ee,ne,ae,se,ie=navigator.userAgent,re=/opera/i.test(ie),oe=/Konqueror|Safari|KHTML/i.test(ie),le=/msie/i.test(ie)&&!re&&!/mac_powerpc/i.test(ie),ce=le&&/msie 6/i.test(ie),he=/gecko/i.test(ie)&&!oe&&!re&&!le,ue=t.prototype,de=t.I18N={}
return t.SEL_NONE=0,t.SEL_SINGLE=1,t.SEL_MULTIPLE=2,t.SEL_WEEK=3,t.dateToInt=L,t.intToDate=A,t.printDate=S,t.formatString=M,t.i18n=_,t.LANG=function(t,e,n){de.__=de[t]={name:e,data:n}},t.setup=function(e){return new t(e)},ue.moveTo=function(t,e){var a,s,i,r,o,l,c,h,u,d,f,y,m,p,g,v,D=this
return t=k(t),s=H(t,D.date,!0),i=D.args,r=i.min&&H(t,i.min),o=i.max&&H(t,i.max),i.animation||(e=!1),R(null!=r&&1>=r,[D.els.navPrevMonth,D.els.navPrevYear],"DynarchCalendar-navDisabled"),R(null!=o&&o>=-1,[D.els.navNextMonth,D.els.navNextYear],"DynarchCalendar-navDisabled"),-1>r&&(t=i.min,a=1,s=0),o>1&&(t=i.max,a=2,s=0),D.date=t,D.refresh(!!e),D.callHooks("onChange",D,t,e),!e||0==s&&2==e||(D._bodyAnim&&D._bodyAnim.stop(),l=D.els.body,c=V("div","DynarchCalendar-animBody-"+te[s],l),h=l.firstChild,$(h)||.7,u=a?ae.brakes:0==s?ae.shake:ae.accel_ab2,d=s*s>4,f=d?h.offsetTop:h.offsetLeft,y=c.style,m=d?l.offsetHeight:l.offsetWidth,0>s?m+=f:s>0?m=f-m:(m=Math.round(m/7),2==a&&(m=-m)),a||0==s||(p=c.cloneNode(!0),g=p.style,v=2*m,p.appendChild(h.cloneNode(!0)),g[d?"marginTop":"marginLeft"]=m+"px",l.appendChild(p)),h.style.visibility="hidden",c.innerHTML=n(D),D._bodyAnim=K({onUpdate:function(t,e){var n,i=u(t)
p&&(n=e(i,m,v)+"px"),a?y[d?"marginTop":"marginLeft"]=e(i,m,0)+"px":((d||0==s)&&(y.marginTop=e(0==s?u(t*t):i,0,m)+"px",0!=s&&(g.marginTop=n)),d&&0!=s||(y.marginLeft=e(i,0,m)+"px",0!=s&&(g.marginLeft=n))),D.args.opacity>2&&p&&($(p,1-i),$(c,i))},onStop:function(){l.innerHTML=n(D,t),D._bodyAnim=null}})),D._lastHoverDate=null,r>=-1&&1>=o},ue.isDisabled=function(t){var e=this.args
return e.min&&H(t,e.min)<0||e.max&&H(t,e.max)>0||e.disabled(t)},ue.toggleMenu=function(){y(this,!this._menuVisible)},ue.refresh=function(t){var e=this.els
t||(e.body.innerHTML=n(this)),e.title.innerHTML=s(this),e.yearInput.value=this.date.getFullYear()},ue.redraw=function(){var t=this,n=t.els
t.refresh(),n.dayNames.innerHTML=e(t),n.menu.innerHTML=i(t),n.bottomBar&&(n.bottomBar.innerHTML=o(t)),W(n.topCont,function(e){var a=Z[e.className]
a&&(n[a]=e),e.className=="DynarchCalendar-menu-year"?(B(e,t._focusEvents),n.yearInput=e):le&&e.setAttribute("unselectable","on")}),t.setTime(null,!0)},ue.setLanguage=function(e){var n=t.setLanguage(e)
n&&(this.fdow=n.data.fdow,this.redraw())},t.setLanguage=function(t){var e=de[t]
return e&&(de.__=e),e},ue.focus=function(){try{this.els[this._menuVisible?"yearInput":"focusLink"].focus()}catch(t){}c.call(this)},ue.blur=function(){this.els.focusLink.blur(),this.els.yearInput.blur(),h.call(this)},ue.showAt=function(t,e,n){this._showAnim&&this._showAnim.stop(),n=n&&this.args.animation
var a=this.els.topCont,s=this,i=this.els.body.firstChild,r=i.offsetHeight,o=a.style
o.position="absolute",o.left=t+"px",o.top=e+"px",o.zIndex=1e4,o.display="",n&&(i.style.marginTop=-r+"px",this.args.opacity>1&&$(a,0),this._showAnim=K({onUpdate:function(t,e){i.style.marginTop=-e(ae.accel_b(t),r,0)+"px",s.args.opacity>1&&$(a,t)},onStop:function(){s.args.opacity>1&&$(a,""),s._showAnim=null}}))},ue.hide=function(){this.callHooks("onClose",this)
var t=this.els.topCont,e=this,n=this.els.body.firstChild,a=n.offsetHeight,s=G(t).y
this.args.animation?(this._showAnim&&this._showAnim.stop(),this._showAnim=K({onUpdate:function(i,r){e.args.opacity>1&&$(t,1-i),n.style.marginTop=-r(ae.accel_b(i),0,a)+"px",t.style.top=r(ae.accel_ab(i),s,s-10)+"px"},onStop:function(){t.style.display="none",n.style.marginTop="",e.args.opacity>1&&$(t,""),e._showAnim=null}})):t.style.display="none",this.inputField=null},ue.popup=function(t,e){function n(e){var n={x:l.x,y:l.y}
return e?(/B/.test(e)&&(n.y+=t.offsetHeight),/b/.test(e)&&(n.y+=t.offsetHeight-a.y),/T/.test(e)&&(n.y-=a.y),/l/.test(e)&&(n.x-=a.x-t.offsetWidth),/L/.test(e)&&(n.x-=a.x),/R/.test(e)&&(n.x+=t.offsetWidth),/c/i.test(e)&&(n.x+=(t.offsetWidth-a.x)/2),/m/i.test(e)&&(n.y+=(t.offsetHeight-a.y)/2),n):n}var a,s,i,r,o,l
t=J(t),e||(e=this.args.align),e=e.split(/\x2f/),s=G(t),i=this.els.topCont,r=i.style,o=X(),r.visibility="hidden",r.display="",this.showAt(0,0),document.body.appendChild(i),a={x:i.offsetWidth,y:i.offsetHeight},l=s,l=n(e[0]),l.y<o.y&&(l.y=s.y,l=n(e[1])),l.x+a.x>o.x+o.w&&(l.x=s.x,l=n(e[2])),l.y+a.y>o.y+o.h&&(l.y=s.y,l=n(e[3])),l.x<o.x&&(l.x=s.x,l=n(e[4])),this.showAt(l.x,l.y,!0),r.visibility="",this.focus()},ue.popupForField=function(e,n,a){var s,i,r,o,l=this
n=J(n),e=J(e),l.inputField=n,l.dateFormat=a,l.selection.type==t.SEL_SINGLE&&(s=/input|textarea/i.test(n.tagName)?n.value:n.innerText||n.textContent,s&&(i=/(^|[^%])%[bBmo]/.exec(a),r=/(^|[^%])%[de]/.exec(a),i&&r&&(o=i.index<r.index),s=Calendar.parseDate(s,o),s&&(l.selection.set(s,!1,!0),l.args.showTime&&(l.setHours(s.getHours()),l.setMinutes(s.getMinutes())),l.moveTo(s)))),l.popup(e)},ue.manageFields=function(t,e,n){var a=this
e=J(e),t=J(t),/^button$/i.test(t.tagName)&&t.setAttribute("type","button"),B(t,"click",function(s){return a.popupForField(t,e,n),N(s)})},ue.callHooks=function(t){for(var e=U(arguments,1),n=this.handlers[t],a=0;a<n.length;++a)n[a].apply(this,e)},ue.addEventListener=function(t,e){this.handlers[t].push(e)},ue.removeEventListener=function(t,e){for(var n=this.handlers[t],a=n.length;--a>=0;)n[a]===e&&n.splice(a,1)},ue.getTime=function(){return this.time},ue.setTime=function(t,e){var n,a,s,i,r,o
this.args.showTime&&(t=null!=t?t:this.time,this.time=t,n=this.getHours(),a=this.getMinutes(),s=12>n,this.args.showTime==12&&(0==n&&(n=12),n>12&&(n-=12),this.els.timeAM.innerHTML=_(s?"AM":"PM")),10>n&&(n="0"+n),10>a&&(a="0"+a),this.els.timeHour.innerHTML=n,this.els.timeMinute.innerHTML=a,e||(this.callHooks("onTimeChange",this,t),i=this.inputField,r=this.selection,i&&(o=r.print(this.dateFormat),/input|textarea/i.test(i.tagName)?i.value=o:i.innerHTML=o)))},ue.getHours=function(){return Math.floor(this.time/100)},ue.getMinutes=function(){return this.time%100},ue.setHours=function(t){0>t&&(t+=24),this.setTime(100*(t%24)+this.time%100)},ue.setMinutes=function(t){0>t&&(t+=60),t=Math.floor(t/this.args.minuteStep)*this.args.minuteStep,this.setTime(100*this.getHours()+t%60)},ue._getInputYear=function(){var t=parseInt(this.els.yearInput.value,10)
return isNaN(t)&&(t=this.date.getFullYear()),t},ue._showTooltip=function(t){var e,n="",a=this.els.tooltip
t&&(t=A(t),e=this.args.dateInfo(t),e&&e.tooltip&&(n="<div class='DynarchCalendar-tooltipCont'>"+S(t,e.tooltip)+"</div>")),a.innerHTML=n},Q=" align='center' cellspacing='0' cellpadding='0'",Z={"DynarchCalendar-topCont":"topCont","DynarchCalendar-focusLink":"focusLink",DynarchCalendar:"main","DynarchCalendar-topBar":"topBar","DynarchCalendar-title":"title","DynarchCalendar-dayNames":"dayNames","DynarchCalendar-body":"body","DynarchCalendar-menu":"menu","DynarchCalendar-menu-year":"yearInput","DynarchCalendar-bottomBar":"bottomBar","DynarchCalendar-tooltip":"tooltip","DynarchCalendar-time-hour":"timeHour","DynarchCalendar-time-minute":"timeMinute","DynarchCalendar-time-am":"timeAM","DynarchCalendar-navBtn DynarchCalendar-prevYear":"navPrevYear","DynarchCalendar-navBtn DynarchCalendar-nextYear":"navNextYear","DynarchCalendar-navBtn DynarchCalendar-prevMonth":"navPrevMonth","DynarchCalendar-navBtn DynarchCalendar-nextMonth":"navNextMonth"},te={"-3":"backYear","-2":"back",0:"now",2:"fwd",3:"fwdYear"},ee={37:-1,38:-2,39:1,40:2},ne={33:-1,34:1},ue._getDateDiv=function(t){var e=null
if(t)try{W(this.els.body,function(n){if(n.getAttribute("dyc-date")==t)throw e=n})}catch(n){}return e},(t.Selection=function(t,e,n,a){this.type=e,this.sel=t instanceof Array?t:[t],this.onChange=O(n,a),this.cal=a}).prototype={get:function(){return this.type==t.SEL_SINGLE?this.sel[0]:this.sel},isEmpty:function(){return this.sel.length==0},set:function(e,n,a){var s=this.type==t.SEL_SINGLE
e instanceof Array?(this.sel=e,this.normalize(),a||this.onChange(this)):(e=L(e),s||!this.isSelected(e)?(s?this.sel=[e]:this.sel.splice(this.findInsertPos(e),0,e),this.normalize(),a||this.onChange(this)):n&&this.unselect(e,a))},reset:function(){this.sel=[],this.set.apply(this,arguments)},countDays:function(){for(var t,e,n,a=0,s=this.sel,i=s.length;--i>=0;)t=s[i],t instanceof Array&&(e=A(t[0]),n=A(t[1]),a+=Math.round(Math.abs(n.getTime()-e.getTime())/864e5)),++a
return a},unselect:function(t,e){var n,a,s,i,r,o,l
for(t=L(t),n=!1,s=this.sel,i=s.length;--i>=0;)a=s[i],a instanceof Array?t<a[0]||t>a[1]||(r=A(t),o=r.getDate(),t==a[0]?(r.setDate(o+1),a[0]=L(r),n=!0):t==a[1]?(r.setDate(o-1),a[1]=L(r),n=!0):(l=new Date(r),l.setDate(o+1),r.setDate(o-1),s.splice(i+1,0,[L(l),a[1]]),a[1]=L(r),n=!0)):t==a&&(s.splice(i,1),n=!0)
n&&(this.normalize(),e||this.onChange(this))},normalize:function(){var t,e,n,a,s,i,r
for(this.sel=this.sel.sort(function(t,e){return t instanceof Array&&(t=t[0]),e instanceof Array&&(e=e[0]),t-e}),n=this.sel,a=n.length;--a>=0;){if(t=n[a],t instanceof Array){if(t[0]>t[1]){n.splice(a,1)
continue}t[0]==t[1]&&(t=n[a]=t[0])}e&&(s=e,i=t instanceof Array?t[1]:t,i=A(i),i.setDate(i.getDate()+1),i=L(i),s>i||(r=n[a+1],t instanceof Array&&r instanceof Array?(t[1]=r[1],n.splice(a+1,1)):t instanceof Array?(t[1]=e,n.splice(a+1,1)):r instanceof Array?(r[0]=t,n.splice(a,1)):(n[a]=[t,r],n.splice(a+1,1)))),e=t instanceof Array?t[0]:t}},findInsertPos:function(t){for(var e,n=this.sel,a=n.length;--a>=0&&(e=n[a],e instanceof Array&&(e=e[0]),t<e););return a+1},clear:function(t){this.sel=[],t||this.onChange(this)},selectRange:function(e,n){var a,s
if(e=L(e),n=L(n),e>n&&(a=e,e=n,n=a),s=this.cal.args.checkRange,!s)return this._do_selectRange(e,n)
try{q(new t.Selection([[e,n]],t.SEL_MULTIPLE,se).getDates(),O(function(t){if(this.isDisabled(t))throw s instanceof Function&&s(t,this),"OUT"},this.cal)),this._do_selectRange(e,n)}catch(i){}},_do_selectRange:function(t,e){this.sel.push([t,e]),this.normalize(),this.onChange(this)},isSelected:function(t){for(var e,n=this.sel.length;--n>=0;)if(e=this.sel[n],e instanceof Array&&t>=e[0]&&t<=e[1]||t==e)return!0
return!1},getFirstDate:function(){var t=this.sel[0]
return t&&t instanceof Array&&(t=t[0]),t},getLastDate:function(){if(this.sel.length>0){var t=this.sel[this.sel.length-1]
return t&&t instanceof Array&&(t=t[1]),t}},print:function(t,e){var n,a=[],s=0,i=this.cal.getHours(),r=this.cal.getMinutes()
for(e||(e=" -> ");s<this.sel.length;)n=this.sel[s++],n instanceof Array?a.push(S(A(n[0],i,r),t)+e+S(A(n[1],i,r),t)):a.push(S(A(n,i,r),t))
return a},getDates:function(t){for(var e,n,a=[],s=0;s<this.sel.length;){if(n=this.sel[s++],n instanceof Array)for(e=A(n[0]),n=n[1];L(e)<n;)a.push(t?S(e,t):new Date(e)),e.setDate(e.getDate()+1)
else e=A(n)
a.push(t?S(e,t):e)}return a}},t.isUnicodeLetter=function(t){return t.toUpperCase()!=t.toLowerCase()},t.parseDate=function(e,n,a){var s,i,r,o,l,c,h,u,d,f,y
if(!/\S/.test(e))return""
for(e=e.replace(/^\s+/,"").replace(/\s+$/,""),a=a||new Date,s=null,i=null,r=null,o=null,l=null,c=null,h=e.match(/([0-9]{1,2}):([0-9]{1,2})(:[0-9]{1,2})?\s*(am|pm)?/i),h&&(o=parseInt(h[1],10),l=parseInt(h[2],10),c=h[3]?parseInt(h[3].substr(1),10):0,e=e.substring(0,h.index)+e.substr(h.index+h[0].length),h[4]&&(h[4].toLowerCase()=="pm"&&12>o?o+=12:h[4].toLowerCase()!="am"||12>o||(o-=12))),u=function(){function n(){return e.charAt(l++)}function a(){return e.charAt(l)}function s(t){for(;a()&&h(a());)t+=n()
return t}function i(){for(var t="";a()&&/[0-9]/.test(a());)t+=n()
return h(a())?s(t):parseInt(t,10)}function r(t){c.push(t)}for(var o,l=0,c=[],h=t.isUnicodeLetter;l<e.length;)o=a(),h(o)?r(s("")):/[0-9]/.test(o)?r(i()):n()
return c}(),d=[],f=0;f<u.length;++f)y=u[f],/^[0-9]{4}$/.test(y)?(s=parseInt(y,10),null==i&&null==r&&null==n&&(n=!0)):/^[0-9]{1,2}$/.test(y)?(y=parseInt(y,10),60>y?0>y||y>12?1>y||y>31||(r=y):d.push(y):s=y):null==i&&(i=I(y))
return d.length<2?d.length==1&&(null==r?r=d.shift():null==i&&(i=d.shift())):n?(null==i&&(i=d.shift()),null==r&&(r=d.shift())):(null==r&&(r=d.shift()),null==i&&(i=d.shift())),null==s&&(s=d.length>0?d.shift():a.getFullYear()),30>s?s+=2e3:99>s&&(s+=1900),null==i&&(i=a.getMonth()+1),null!=s&&null!=i&&null!=r?new Date(s,i-1,r,o,l,c):null},ae={elastic_b:function(t){return 1-Math.cos(5.5*-t*Math.PI)/Math.pow(2,7*t)},magnetic:function(t){return 1-Math.cos(10.5*t*t*t*Math.PI)/Math.exp(4*t)},accel_b:function(t){return t=1-t,1-t*t*t*t},accel_a:function(t){return t*t*t},accel_ab:function(t){return t=1-t,1-Math.sin(t*t*Math.PI/2)},accel_ab2:function(t){return(t/=.5)<1?.5*t*t:-0.5*(--t*(t-2)-1)},brakes:function(t){return t=1-t,1-Math.sin(t*t*Math.PI)},shake:function(t){return.5>t?-Math.cos(11*t*Math.PI)*t*t:(t=1-t,Math.cos(11*t*Math.PI)*t*t)}},se=Function(),t}()


//js js/cgjs/en.js
Calendar.LANG("en", "English", {

        fdow: 1,                // first day of week for this locale; 0 = Sunday, 1 = Monday, etc.

        goToday: "Go Today",

        today: "Today",         // appears in bottom bar

        wk: "wk",

        weekend: "0,6",         // 0 = Sunday, 1 = Monday, etc.

        AM: "am",

        PM: "pm",

        mn : [ "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December" ],

        smn : [ "Jan",
                "Feb",
                "Mar",
                "Apr",
                "May",
                "Jun",
                "Jul",
                "Aug",
                "Sep",
                "Oct",
                "Nov",
                "Dec" ],

        dn : [ "Sunday",
               "Monday",
               "Tuesday",
               "Wednesday",
               "Thursday",
               "Friday",
               "Saturday",
               "Sunday" ],

        sdn : [ "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa",
                "Su" ]

});


//js common.js
/*=====================================demo schedule ==========================================*/

var multipleInterviewInviteId="";
function clearDemoScheduleField(){
	dwr.util.setValues({demoDate:null,demoTime:null,meridiem:null,location:null,disSchCmnt:null,timezone:null,userId:null,demostatus:null});
	$('#demoDate').css("background-color", "");
	$('#demoTime').css("background-color", "");
	$('#meridiem').css("background-color", "");
	$('#meridiem').css("background-color", "");
	$('#timezone').css("background-color", "");
	$('#location').css("background-color", "");
	$('#userId').css("background-color", "");

	$('#demostatus').attr('checked', false);
	$('#demostatus').attr("disabled", false);
	$('#demoDate').attr("disabled", false);
	$('#demoTime').attr("disabled", false);
	$('#meridiem').attr("disabled", false);
	$('#meridiem').attr("disabled", false);
	$('#timezone').attr("disabled", false);
	$('#location').attr("disabled", false);
	$('#userId').attr("disabled",false);
	$('#disSchCmnt').attr("disabled",false);
	$('#canEvtBtn').attr("disabled",false);
	$('#doneBtn').attr("disabled",false);
	$('#addAttHref').attr("disabled",false);

	if(document.getElementById("demoSchoolName"))
	{
		$('#demoSchoolName').val("");
		$('#schoolId').val("");
	}
}
function showAttendeeDiv()
{
	$("#demoDistrictdiv").show();
	$("#demoSchooldiv").show();
	$("#demoUserdiv").show();
}
var arr =[];
function getDemoSchedule(teacherId,jobId,isDemo)
{
	
	$("html, body").animate({ scrollTop: 0 }, "slow");
	clearDemoScheduleField();
	arr = [];
	$('#demoId').val("");
	$('#attendeeDiv').html("");
	var teacherDetail = {teacherId:teacherId};
	var jobOrder = {jobId:jobId};
	if(isDemo)
	{

		CandidateGridSubAjax.displayAttendee(teacherDetail,jobOrder,isDemo,{ 
			async: true,
			callback: function(data)
			{ 
			//alert(data.attendeeList);
			if(data.attendeeList!=undefined)
			{

				$('#demoDate').val(data.demoDate);
				$('#demoTime').val(data.demoTime);
				$('#attendeeDiv').html(data.attendees);
				$('#location').val(data.demoClassAddress);
				$('#disSchCmnt').val(data.demoDescription);
				$('#meridiem').val(data.timeFormat);
				$('#timezone').val(data.timeZoneId);
				$('#demoId').val(data.demoId);
				$('#demoNoteId').val(data.demoId);
				$('#demoCreatedDate').val(data.demoDate);
				
				arr = data.attendeeList;

				if(data.demoStatus=="Completed")
				{
					$('#demostatus').attr('checked', true);
					$('#demostatus').attr("disabled", "disabled");
					$('#demoDate').attr("disabled", "disabled");
					$('#demoTime').attr("disabled", "disabled");
					$('#meridiem').attr("disabled", "disabled");
					$('#meridiem').attr("disabled", "disabled");
					$('#timezone').attr("disabled", "disabled");
					$('#location').attr("disabled", "disabled");
					$('#userId').attr("disabled", "disabled");
					$('#disSchCmnt').attr("disabled", "disabled");
					$('#canEvtBtn').attr("disabled", "disabled");
					$('#doneBtn').attr("disabled", "disabled");
					$('#addAttHref').attr("disabled", "disabled");
				}

				if(data.demoNoteStatus==true){
					document.getElementById("democlassNote").innerHTML="<a href='javascript:void(0);' onclick='getNotesTimelineDivDiv(true,"+teacherId+","+jobId+");'><img src='images/note_com.png'/></a>"; 
				}else{
					document.getElementById("democlassNote").innerHTML="<a href='javascript:void(0);' onclick='getNotesTimelineDivDiv(true,"+teacherId+","+jobId+");'><img src='images/note_com1.png'/></a>";
				}
			}
			
			},
		});
	}
	
	if(document.getElementById("demoSchoolName"))
		getUsersByDistrict();

	document.getElementById("attendeeDiv").style.display='inline';

	$("#teacherId").val(teacherId);
	$("#jobId").val(jobId);

	$("#demoDistrictdiv").hide();
	$("#demoSchooldiv").hide();
	dwr.util.setValues({demoUserId:null});
	$('#errordiv').empty();
	
	try{$('#myModalDemoSchedule').modal('show');}catch(e){}
	

}
function getDemoNotesDiv(teacherId,jobId)
{
	removeNotesFile();
	hideCommunicationsDiv();
	currentPageFlag='notes';
	document.getElementById("teacherId").value=teacherId;
	document.getElementById("teacherIdForNote").value=teacherId;
	$('#demodivTxtNode').find(".jqte_editor").html("");		
	$('#demoModalDemoNotes').modal('show');	
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	$('#errordivNotes').empty();
	$('#myModalDemoNotes').modal('hide');
	//CandidateGridAjax.getDemoNotesDetail(teacherId,jobId,page,noOfRows,sortOrderStr,sortOrderType,
	CandidateGridAjax.getDemoNotes(true,teacherId,jobId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("demoNotesTxt").innerHTML=data;
			applyScrollOnNotesTbl();
		}
	});
	
}

function getNotesTimelineDivDiv(commDivFlag,teacherId,jobId)
{	
	//document.getElementById("commDivFlag").value=commDivFlag;
	//document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	CandidateGridAjax.getDemoNotes(commDivFlag,teacherId,jobId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		
		
		try{
			$("#demoNotesTxt").html(data);
			$('#myModalDemoSchedule').modal('hide');
			$('#myModalDemoNotes').modal('show');
		}catch(e){}
	}
	});	
}



/*======================save demo schedule data ==============================*/
function saveDemoSchedule(checkedFlag){

	$('#demoDate').css("background-color", "");
	$('#demoTime').css("background-color", "");
	$('#meridiem').css("background-color", "");
	$('#meridiem').css("background-color", "");
	$('#timezone').css("background-color", "");
	$('#location').css("background-color", "");
	$('#userId').css("background-color", "");

	//var demoDate 		=		new Date(trim(document.getElementById("demoDate").value));
	var demoDateVal 	=		trim(document.getElementById("demoDate").value);
	var demoTime		=		trim(document.getElementById("demoTime").value);
	var meridiem		=		trim(document.getElementById("meridiem").value);
	var location		=		trim(document.getElementById("location").value);
	var disSchCmnt		=		trim(document.getElementById("disSchCmnt").value);
	var t				=		document.getElementById("timezone");
	var timezone		=		trim(t.options[t.selectedIndex].value);
	var e				=		document.getElementById("demoUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var demoStatus		=		document.getElementById("demostatus");

	var teacherId		=		trim(document.getElementById("teacherId").value);
	var jobId		=		trim(document.getElementById("jobId").value);
	var demoId = $('#demoId').val();
	var counter=0;
	var focus=0;	

	$('#errordiv').empty();
	/*if (demoDate=="Invalid Date")
	{
		$('#errordiv').append("&#149; Please enter Demo Date<br>");
		if(focus==0)
			$('#demoDate').focus();
		$('#demoDate').css("background-color", "#F5E7E1");
		counter++;
	}*/
	if (demoDateVal=="" || demoDate=="Invalid Date")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgDemoDate+"<br>");
		if(focus==0)
			$('#demoDate').focus();
		$('#demoDate').css("background-color", "#F5E7E1");
		counter++;
	}

	if(demoDateVal!=""){

		try{
			var  date1 = demoDateVal.split("-");
			var currentDate = "";
			var dateMsg = "Current";
			if(demoId=="")
			  currentDate = document.getElementById("currDate").value;
			else
			{
				currentDate = document.getElementById("currDate").value;
			  //currentDate = document.getElementById("demoCreatedDate").value;
			  //dateMsg = "Demo Created";
			}
			
			var	date2 = currentDate.split("-");
			var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
			var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
			if(sDate < eDate){
				$('#errordiv').append("&#149; "+resourceJSON.msgDemoDateGreater+" "+dateMsg+" "+resourceJSON.MsgDate+"<br>");
				if(focus==0)
					$('#demoDate').focus();
				$('#demoDate').css("background-color", "#F5E7E1");
				counter++;

			} 
		}catch(err){}
	}
	if (demoTime=="Select Time")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgDemoTime+"<br>");
		if(focus==0)
			$('#demoTime').focus();
		$('#demoTime').css("background-color", "#F5E7E1");
		counter++;
	}

	if (meridiem=="Select AM/PM")
	{
		$('#errordiv').append("&#149; "+resourceJSON.MsgSelectAmOrPm+"<br>");
		if(focus==0)
			$('#meridiem').focus();
		$('#meridiem').css("background-color", "#F5E7E1");
		counter++;
	}
	if (timezone=="Select Timezone")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgSelectTimeZone+"<br>");
		if(focus==0)
			$('#timezone').focus();
		$('#timezone').css("background-color", "#F5E7E1");
		counter++;
	}
	if (location=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPlzenterLocation+"<br>");
		if(focus==0)
			$('#location').focus();
		$('#location').css("background-color", "#F5E7E1");
		counter++;
	}
	//if (userId=="Select Attendee" || arr.length==0)
	if (arr.length==0)
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzSlctAttendee+"<br>");
		if(focus==0)
			$('#userId').focus();
		$('#userId').css("background-color", "#F5E7E1");
		counter++;
	}

	if(demoStatus.checked && !checkedFlag)
	{
		$('#message2showConfirm').html(""+resourceJSON.msgwanttomarkthisdemo+"");
		$('#footerbtn').html("<button class='btn btn-primary' onclick='saveDemoSchedule(true)' >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');

		return;
	}

	if(counter==0){
		demoId=demoId==""?"0":demoId;

		CandidateGridSubAjax.saveDemoSchedule(demoDateVal,demoTime,meridiem,timezone,location,disSchCmnt,
				teacherId,jobId,demoStatus.checked,arr,demoId,{ 
			async: true,
			callback: function(data)
			{
			clearDemoScheduleField();
			arr = [];
			//document.getElementById("attendeeDiv").style.display='none';
			getCandidateGrid();
			refreshStatus();
			$('#myModalDemoSchedule').modal('hide');
			$('#myModal3').modal('hide');
			$('#message2show').html(""+resourceJSON.msgDemoLessonscheduled+".");
			$('#myModal2').modal('show');

			},
		});
	}
}

function addAttendee(){
	var e				=		document.getElementById("demoUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var userName		=		trim(e.options[e.selectedIndex].text);
	var attendeeDiv		=		document.getElementById("attendeeDiv");
	var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoAttendeeFound+"</div>");
	if(attendeeDiv.innerHTML==strInnerHtml){
		attendeeDiv.innerHTML="";
	}
	
	if(e.selectedIndex==0)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgvalidAttendee+"<br>");
		$('#userId').focus();
		return;
	}
	getAllAttendeeDivId();
	$('#errordiv').empty();
	if($.inArray(parseInt(userId), arr)==-1)
	{
		arr.push(parseInt(userId));
		var newAttendee=attendeeDiv.innerHTML +"<div id='"+userId+"' class='row col-sm-4 col-md-4'>"+userName
		+"&nbsp;<a href='javascript:void(0);' onclick='deleteAttendee(\""+userId+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";

		$('#attendeeDiv').html(newAttendee);

	}else
	{	
		$('#errordiv').append("&#149; "+resourceJSON.msgAttendeealreadyadded+"<br>");
		$('#userId').focus();
		//$('#userId').css("background-color", "#F5E7E1");
	}
	$('#attendeeDiv').show();
}

/*========================================delete Attendee ==========================================*/
function deleteAttendee(attendeeId){

	$("#"+attendeeId).remove();
	var i = arr.indexOf(parseInt(attendeeId));

	if(i != -1) {
		arr.splice(i, 1);
	}
}
function getAllAttendeeDivId(){
	var all = $("#attendeeDiv > div").map(function() {
		//alert(this.id);
		return this.id;
	}).get();
}
/*========================================get user by school ==========================================*/
function getUsersBySchool(forWhich){

	var schoolId="schoolId";
	if(forWhich=="panel")
		 schoolId="panelSchoolId";
	
	if(document.getElementById("demoSchoolId").value!="")
	{
		var schoolId=trim(document.getElementById("demoSchoolId").value);
	
		CandidateGridSubAjax.getUsersBySchool(schoolId,false,{ 
			async: true,
			callback: function(data)
			{
			if(forWhich=="panel")
				$('#panelUserId').html(data);
			else
				$('#demoUserId').html(data);
			},
		});
	}else
		getUsersByDistrict(forWhich);

}

function getUsersByDistrict(forWhich){
	var districtId=0;
	CandidateGridSubAjax.getUsersByDistrict(districtId,false,{ 
		async: true,
		callback: function(data)
		{
			if(forWhich=="panel")
			$('#panelUserId').html(data);
			else
			$('#demoUserId').html(data);
		},
	});
}

function cancelEvent()
{
	if($('#demoId').val()=="")
	{
		$('#errordiv').append("&#149;"+resourceJSON.msgEventCancelled+"<br>");
	}else
	{
		$('#message2showConfirm').html(resourceJSON.msgWantToCancelled);
		var demoId = $('#demoId').val();
		$('#footerbtn').html("<button class='btn btn-primary' onclick=\"cancelEventOk("+demoId+")\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');
	}
}
function cancelEventOk(demoId)
{
	$('#myModal3').modal('hide');
	$('#myModalDemoSchedule').modal('hide');
	//$('#loadingDiv').modal('show');

	CandidateGridSubAjax.cancelEvent(demoId,{ 
		async: true,
		callback: function(data)
		{

		clearDemoScheduleField();
		arr = [];
		//document.getElementById("attendeeDiv").style.display='none';
		getCandidateGrid();
		refreshStatus();
		$('#message2show').html(resourceJSON.msgEventCancelled1);
		$('#myModal2').modal('show');
		//$('#loadingDiv').modal('hide');
		},
	});
}

function downloadOrOpenDemoNoteFile(filePath,fileName,linkId)
{
	//alert("downloadOrOpenNoteFile :"+downloadOrOpenNoteFile+"  demoid :"+demoid);
	CandidateGridAjax.downloadOrOpenDemoNoteFile(filePath,fileName,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if (data.indexOf(".doc") !=-1 || data.indexOf(".xls") !=-1) {
			    document.getElementById('ifrmTrans').src = ""+data+"";
			}else{
				document.getElementById(linkId).href = data; 
				return false;
			}
		}
	});
}

function saveDemoNotes(){
	var noteFileName="";
	var notes = $('#demodivTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var demoId = document.getElementById("demoId").value;
	var districtId = document.getElementById("districtId").value;
	var schoolId = document.getElementById("schoolId").value;
	
	var fileNote=null;
	
	try{
		fileNote=document.getElementById("demoNotesfile").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
	
	$('#demoerrordivNotes').empty();
	if ($('#demodivTxtNode').find(".jqte_editor").text().trim()=="")
	{
		$('#demoerrordivNotes').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#demodivTxtNode').find(".jqte_editor").focus();
		$('#demodivTxtNode').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#demoerrordivNotes').show();
		$('#demoerrordivNotes').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="demo_note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("demoNotesfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("demoNotesfile").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#demoerrordivNotes').show();
			$('#demoerrordivNotes').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#demoerrordivNotes').show();
			$('#demoerrordivNotes').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null)
			{
				document.getElementById("demofrmNoteUpload").submit();
			}
			else
			{
							
				CandidateGridAjax.saveDemoNotes(demoId,teacherId,jobId,districtId,schoolId,notes,noteFileName,{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{	
						$('#loadingDiv').hide();
						getNotesTimelineDivDiv(true,teacherId,jobId);
						showDemoNotesDiv();
					}
			});	
			}
		}catch(err){}
	}
}

function saveDemoNoteFile(noteDateTime)
{	
	//alert(saveDemoNoteFile);
	var notes = $('#demodivTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var demoId = document.getElementById("demoId").value;
	var districtId = document.getElementById("districtId").value;
	var schoolId = document.getElementById("schoolId").value;
	
	var fileNote=null;
	try{
		fileNote=document.getElementById("demoNotesfile").value;
	}catch(e){}
	
	var noteFileName="";
	
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="demo_note"+noteDateTime+"."+ext;
	}
	
	CandidateGridAjax.saveDemoNotes(demoId,teacherId,jobId,districtId,schoolId,notes,noteFileName,{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			getNotesTimelineDivDiv(true,teacherId,jobId);
			showDemoNotesDiv();
		}
	});
	
}


function closeDemoNotes()
{
	$('#demoNotesfile').val("" );
	$('#demotxtNotes').val("");
	$('#myModalDemoNotes').modal('show');
}

function addDemoNoteFileType()
{
	$('#demofileNotes').empty();
	$('#demofileNotes').html("<a href='javascript:void(0);' onclick='removeDemoNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='demoNotesfile' name='demoNotesfile' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeDemoNotesFile()
{
	$('#demofileNotes').empty();
	$('#demofileNotes').html("<a href='javascript:void(0);' onclick='addDemoNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addDemoNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function showDemoNotesDiv()
{
	$('#myModalMessage').modal('hide');	
	$('#myModalNotes').modal('hide');
	$("#saveToFolderDiv").modal("hide");
	$("#demoModalDemoNotes").modal("hide");
	$('#myModalDemoNotes').modal('show');
}

function showPanelistDiv()
{
	if(document.getElementById("choice3").checked)
	{
		$("#panelDistrictdiv").show();
		$("#panelSchooldiv").show();
		$("#panelUserdiv").show();
	}else
	{
		$("#panelDistrictdiv").hide();
		$("#panelSchooldiv").hide();
		$("#panelUserdiv").hide();
	}
}

var arrPanel =[];
function getPanel(teacherId,jobId,isPanel)
{
	//alert($('#jPanelStauts').val());
	$("html, body").animate({ scrollTop: 0 }, "slow");
	clearPanelField();
	
	$('#addPanelMemberDiv').hide();
	clearNewPanelMember();
	for(i=1;i<=3;i++)
		document.getElementById("choice"+i).checked=false;
	
	arrPanel = [];
	$('#panelId').val("");
	$('#panelistDiv').html("");
	var teacherDetail = {teacherId:teacherId};
	var jobOrder = {jobId:jobId};
	
	/*if(isPanel)
	{*/
		//CandidateGridSubAjax.displayPannel(teacherDetail,jobOrder,isPanel,$('#jPanelStauts').val(),{
	if(teacherId!=null && teacherId!=""){
		CandidateGridSubAjax.displayPannel(teacherDetail,jobOrder,true,$('#jPanelStauts').val(),{ 
			async: true,
			callback: function(data)
			{ 
			
			if(data.panelAttendeeList!=undefined)
			{
				
				$('#panelDate').val(data.panelDate);
				$('#panelTime').val(data.panelTime);
				
				$('#panelistDiv').html(data.panelAttendees);
				$('#panelistDiv').show();
				$('#panelLocation').val(data.panelAddress);
				$('#panelDisSchCmnt').val(data.panelDescription);
				$('#panelMeridiem').val(data.timeFormat);
				$('#panelTimezone').val(data.timeZoneId);
				$('#panelId').val(data.panelId);
				//$('#demoNoteId').val(data.demoId);
				$('#panelCreatedDate').val(data.panelDate);
				
				arrPanel = data.panelAttendeeList;
				//alert(arrPanel.length);
				if(data.panelStatus=="Completed")
				{
					$('#panelStatus').attr('checked', true);
					$('#panelDtatus').attr("disabled", "disabled");
					$('#panelDate').attr("disabled", "disabled");
					$('#panelTime').attr("disabled", "disabled");
					$('#panelMeridiem').attr("disabled", "disabled");
					$('#panelTimezone').attr("disabled", "disabled");
					$('#panelLocation').attr("disabled", "disabled");
					$('#panelUserId').attr("disabled", "disabled");
					$('#panelDisSchCmnt').attr("disabled", "disabled");
					//$('#canEvtBtn').attr("disabled", "disabled");
					$('#doneBtn').attr("disabled", "disabled");
					$('#addAttHref').attr("disabled", "disabled");
				}
			  }
			},
		});
	}
	//}
	
	if(document.getElementById("panelSchoolName"))
		getUsersByDistrictForPanel();

	document.getElementById("attendeeDiv").style.display='inline';

	$("#panelDistrictdiv").hide();
	$("#panelSchooldiv").hide();
	$("#panelUserdiv").hide();
	
	dwr.util.setValues({panelUserId:null});
	$("#pteacherId").val(teacherId);
	$("#jobId").val(jobId);
	$('#panelErrordiv').empty();
	$('#myModalPanel').modal('show');

}

function getUsersBySchoolForPanel(){

	if(document.getElementById('schoolId').value!="")
	{
		var schoolId=trim(document.getElementById('schoolId').value);
		CandidateGridSubAjax.getUsersBySchool(schoolId,true,{ 
			async: true,
			callback: function(data)
			{
			$('#panelUserId').html(data);
			},
		});
	}else
		getUsersByDistrictForPanel();

}

function getUsersByDistrictForPanel(){
	var districtId=0;
	CandidateGridSubAjax.getUsersByDistrict(districtId,true,{ 
		async: true,
		callback: function(data)
		{
		//alert(data);
		$('#panelUserId').html(data);
		},
	});
}

/*======================save demo schedule data ==============================*/
function savePanel(checkedFlag){

	$('#panelDate').css("background-color", "");
	$('#panelTime').css("background-color", "");
	$('#panelMeridiem').css("background-color", "");
	$('#panelTimezone').css("background-color", "");
	$('#panelLocation').css("background-color", "");
	$('#panelUserId').css("background-color", "");

	//var panelDate 		=		new Date(trim(document.getElementById("panelDate").value));
	var panelDateVal 	=		trim(document.getElementById("panelDate").value);
	var panelTime		=		trim(document.getElementById("panelTime").value);
	var panelMeridiem		=		trim(document.getElementById("panelMeridiem").value);
	var panelLocation		=		trim(document.getElementById("panelLocation").value);
	var disSchCmnt		=		trim(document.getElementById("panelDisSchCmnt").value);
	var t				=		document.getElementById("panelTimezone");
	var panelTimezone		=		trim(t.options[t.selectedIndex].value);
	var e				=		document.getElementById("panelUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var panelStatus		=		document.getElementById("panelStatus");
	
	var teacherId		=		trim(document.getElementById("pteacherId").value);
	var jobId		=		trim(document.getElementById("jobId").value);
	var panelId = $('#panelId').val();
	var counter=0;
	var focus=0;	

	$('#panelErrordiv').empty();
	
	if (panelDateVal=="" || panelDate=="Invalid Date")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanelDate+"<br>");
		if(focus==0)
			$('#panelDate').focus();
		$('#panelDate').css("background-color", "#F5E7E1");
		counter++;
	}

	if(panelDateVal!=""){

		try{
			var  date1 = panelDateVal.split("-");
			var currentDate = "";
			var dateMsg = "Current";
			if(panelId=="")
			  currentDate = document.getElementById("currDate").value;
			else
			{
				currentDate = document.getElementById("currDate").value;
			  //currentDate = document.getElementById("panelCreatedDate").value;
			  //dateMsg = "panel Created";
			}
			
			var	date2 = currentDate.split("-");
			var sDate = new Date(date1[0]+"/"+date1[1]+"/"+date1[2]);
			var eDate = new Date(date2[0]+"/"+date2[1]+"/"+date2[2]);
			if(sDate < eDate){
				$('#panelErrordiv').append("&#149; "+resourceJSON.MsgDateGreaterOrEqual+" "+dateMsg+" "+resourceJSON.MsgDate+"<br>");
				if(focus==0)
					$('#panelDate').focus();
				$('#panelDate').css("background-color", "#F5E7E1");
				counter++;

			} 
		}catch(err){}
	}
	if (panelTime=="Select Time")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanelTime+"<br>");
		if(focus==0)
			$('#panelTime').focus();
		$('#panelTime').css("background-color", "#F5E7E1");
		counter++;
	}

	if (panelMeridiem=="Select AM/PM")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.MsgSelectAmOrPm+"<br>");
		if(focus==0)
			$('#panelMeridiem').focus();
		$('#panelMeridiem').css("background-color", "#F5E7E1");
		counter++;
	}
	if (panelTimezone=="Select Timezone")
	{
		$('#panelErrordiv').append("&#149;"+resourceJSON.PlzSlctPanelTimeZone+"<br>");
		if(focus==0)
			$('#panelTimezone').focus();
		$('#panelTimezone').css("background-color", "#F5E7E1");
		counter++;
	}
	if (panelLocation=="")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzEtrPanlLocation+"<br>");
		if(focus==0)
			$('#panelLocation').focus();
		$('#panelLocation').css("background-color", "#F5E7E1");
		counter++;
	}
	//if (userId=="Select Attendee" || arrPanel.length==0)
	if (arrPanel.length==0)
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.PlzSlctAttendee+"<br>");
		if(focus==0)
			$('#userId').focus();
		$('#panelUserId').css("background-color", "#F5E7E1");
		counter++;
	}

	if(panelStatus.checked && !checkedFlag)
	{
		$('#message2showConfirm').html(resourceJSON.MsgMarkPanelCompleted);
		$('#footerbtn').html("<button class='btn btn-primary' onclick='savePanel(true)' >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');

		return;
	}

	if(counter==0){
		panelId=panelId==""?"0":panelId;
		
		CandidateGridSubAjax.savePanel(panelDateVal,panelTime,panelMeridiem,panelTimezone,panelLocation,disSchCmnt,
				teacherId,jobId,panelStatus.checked,arrPanel,panelId,$('#jPanelStauts').val(),{
			async: true,
			callback: function(data)
			{
			
			clearPanelField();
			arrPanel = [];
			//document.getElementById("attendeeDiv").style.display='none';
			$('#myModalPanel').modal('hide');
			$('#myModal3').modal('hide');
			$('#message2show').html(resourceJSON.MsgPanelScheduled);
			$('#myModal2').modal('show');
			getCandidateGrid();
			refreshStatus();
			
			},
		});
	}
}

//addPanelist
function addPanelist(){
	var e				=		document.getElementById("panelUserId");
	var userId			=		trim(e.options[e.selectedIndex].value);
	var userName		=		trim(e.options[e.selectedIndex].text);
	var panelistDiv		=		document.getElementById("panelistDiv");
	
	var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoPanelistFound+"</div>");
	if(panelistDiv.innerHTML==strInnerHtml){
		panelistDiv.innerHTML="";
	}
	
	if(e.selectedIndex==0)
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.msgNoPanelistFound+"<br>");
		$('#userId').focus();
		return;
	}
	
	getAllPanelistDivId();
	$('#panelErrordiv').empty();
	if($.inArray(parseInt(userId), arrPanel)==-1)
	{
		
		arrPanel.push(parseInt(userId));

		var newAttendee=panelistDiv.innerHTML +"<div id='P"+userId+"' class='row col-sm-4 col-md-4'>"+userName
		+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+userId+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
		$('#panelistDiv').html(newAttendee);

	}else
	{	
		$('#panelErrordiv').append("&#149;"+resourceJSON.msgPanelistalreadyadded+"<br>");
		$('#userId').focus();
		//$('#userId').css("background-color", "#F5E7E1");
	}
	$('#panelistDiv').show();
}
/*========================================delete Panelist ==========================================*/
function deletePanelist(attendeeId){

	$("#P"+attendeeId).remove();
	var i = arrPanel.indexOf(parseInt(attendeeId));

	if(i != -1) {
		arrPanel.splice(i, 1);
	}
}
function getAllPanelistDivId(){
	var all = $("#panelistDiv > div").map(function() {
		//alert(this.id);
		return this.id;
	}).get();
}
function clearPanelField(){
	
	dwr.util.setValues({panelDate:null,panelTime:null,panelMeridiem:null,location:null,panelDisSchCmnt:null,panelTimezone:null,userId:null,panelstatus:null,panelLocation:null});
	$('#panelDate').css("background-color", "");
	$('#panelTime').css("background-color", "");
	$('#panelMeridiem').css("background-color", "");
	$('#panelTimezone').css("background-color", "");
	$('#panelLocation').css("background-color", "");
	$('#panelUserId').css("background-color", "");

	$('#panelstatus').attr('checked', false);
	$('#panelstatus').attr("disabled", false);
	$('#panelDate').attr("disabled", false);
	$('#panelTime').attr("disabled", false);
	$('#panelMeridiem').attr("disabled", false);
	$('#panelTimezone').attr("disabled", false);
	$('#panelLocation').attr("disabled", false);
	$('#panelUserId').attr("disabled",false);
	$('#panelDisSchCmnt').attr("disabled",false);
	//$('#canEvtBtn').attr("disabled",false);
	$('#doneBtn').attr("disabled",false);
	$('#addAttHref').attr("disabled",false);

	if(document.getElementById("panelSchoolName"))
	{
		$('#panelSchoolName').val("");
		$('#schoolId').val("");
	}
	$('#panelDiv').hide();
}

function cancelPanelEvent()
{
	if($('#panelId').val()=="")
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.msgEventCancelled+"<br>");
	}else
	{
		$('#message2showConfirm').html(""+resourceJSON.msgWantToCancelled+"");
		var panelId = $('#panelId').val();
		$('#footerbtn').html("<button class='btn btn-primary' onclick=\"cancelPanelEventOk("+panelId+")\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');
	}
}

function cancelPanelEventOk(panelId)
{
	$('#myModal3').modal('hide');
	$('#myModalPanel').modal('hide');
	//$('#loadingDiv').modal('show');

	CandidateGridSubAjax.cancelPanelEvent(panelId,{ 
		async: true,
		callback: function(data)
		{

		clearPanelField();
		arrPanel = [];
		//document.getElementById("attendeeDiv").style.display='none';
		getCandidateGrid();
		refreshStatus();
		$('#message2show').html(resourceJSON.msgEventCancelled1);
		$('#myModal2').modal('show');
		//$('#loadingDiv').modal('hide');
		},
	});
}
function showPanels(){
	
	if(document.getElementById("choice1").checked)
	{
		var districtId=0;
		CandidateGridSubAjax.showPanels(districtId,{ 
			async: true,
			callback: function(data)
			{
				$('#panelsId').html(data);
				$('#panelDiv').show();
				
			},
		});
	}else
	{
		//$('#panelsId').html("");
		$('#panelDiv').hide();
	}
}
function addAllPanelMebersFromPanel()
{
	$('#panelErrordiv').empty();
	$('#panelMemberErrordiv').hide();
	var panelsId=$("#panelsId").val();
	if(panelsId=='0')
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.msgleastonepanel1+".<br>");
		$('#panelErrordiv').show();
		return;
	}
	CandidateGridSubAjax.addAllPanelMebersFromPanel(panelsId,{ 
		async: true,
		callback: function(data)
		{
			if(data.length!=0)
			{
				
				var panelistDiv		=		document.getElementById("panelistDiv");
				
				var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoPanelistFound+"</div>");
				if(panelistDiv.innerHTML==strInnerHtml){
					panelistDiv.innerHTML="";
				}
				
				
				var arr = [];
				var nameArr = [];
				for(i=0;i<data.length;i++)
				{
					var myData = data[i].split("#@");
					/*var uId=data[i].split("#@")[0];
					var uName=data[i].split("#@")[1];*/
					if($.inArray(parseInt(myData[0]), arrPanel)==-1)
					{
						arr.push(myData[0]);
						nameArr.push(myData[1]);
					}
				}
				
				getAllPanelistDivId();
				$('#panelErrordiv').empty();
				
				$.each(arr, function( index, value ) {
					  //alert( index + ": " + value );
					  arrPanel.push(parseInt(value));
					 
					  var newAttendee=panelistDiv.innerHTML +"<div id='P"+value+"' class='col-sm-3 col-md-3'>"+nameArr[index]
						+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+value+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
						$('#panelistDiv').html(newAttendee);
					});
				$('#panelistDiv').show();
				$('#panelDiv').hide();
				document.getElementById("choice1").checked=false; 
			}else
			{
				$('#panelErrordiv').append("&#149;  "+resourceJSON.msgNoanyPanelMemember+"<br>");
				$('#panelErrordiv').show();
			}
			
		},
	});
}
function viewPanelMembers(){

	
	$('#panelErrordiv').empty();
	$('#panelMemberErrordiv').hide();
	var panelsId=$("#panelsId").val();
	if(panelsId=='Select Panel')
	{
		$('#panelErrordiv').append("&#149; "+resourceJSON.msg1PaneltoView+"<br>");
		$('#panelErrordiv').show();
		return;
	}
	CandidateGridSubAjax.viewPanelMembers(panelsId,{ 
		async: true,
		callback: function(data)
		{
			if(data!=0)
			{
				$('#panelMemberData').html(data);
				//$('#panelMemberDiv').show();
				try{$('#panelMemberDiv').modal('show');}catch(e){}
				try{$('#myModalPanel').modal('hide');}catch(e){}
			}else
			{
				$('#panelErrordiv').append("&#149; "+resourceJSON.msgNoanyPanelMemember+"<br>");
				$('#panelErrordiv').show();
			}
			
		},
	});
}
function checkAll(flg)
{
	var checkBox = document.getElementsByName("case");
	for(i=0;i<checkBox.length;i++)
		checkBox[i].checked=flg;
}

function addPanelistFromPanel(){
	$('#panelMemberErrordiv').hide();
	if($('input[name=case]:checkbox:checked').length == 0 )
	{
		$('#panelMemberErrordiv').append("&#149; "+resourceJSON.msgpanelmemberattach+"<br>");
		$('#panelMemberErrordiv').show();
		return;
	} 
	
	var panelistDiv		=		document.getElementById("panelistDiv");
	
	var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoPanelistFound+"</div>");
	if(panelistDiv.innerHTML==strInnerHtml){
		panelistDiv.innerHTML="";
	}
	
	var checkBox = document.getElementsByName("case");
	var arr = [];
	for(i=0;i<checkBox.length;i++)
	{
		if(checkBox[i].checked==true)
		{
			//alert(checkBox[i].value);
			if($.inArray(parseInt(checkBox[i].value), arrPanel)==-1)
			{
				arr.push(checkBox[i].value);
			}
		}
	}
	
	getAllPanelistDivId();
	$('#panelErrordiv').empty();
	
	$.each(arr, function( index, value ) {
		  //alert( index + ": " + value );
		  arrPanel.push(parseInt(value));
		  var newAttendee=panelistDiv.innerHTML +"<div id='P"+value+"' class='row col-sm-4 col-md-4'>"+$("#u"+value).html()
			+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+value+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
			$('#panelistDiv').html(newAttendee);
		});
	
	$('#panelistDiv').show();
	$('#panelMemberDiv').hide();
	hideMemberPanel();
	$('#panelDiv').hide();
	document.getElementById("choice1").checked=false; 
}
function hideMemberPanel()
{
	try{$('#panelMemberDiv').modal('hide');}catch(e){}
	try{$('#myModalPanel').modal('show');}catch(e){}
}
function hideAddMemberPanel()
{
	try{$('#myModalPanel').modal('show');}catch(e){}
	try{$('#addPanelMemberDiv').modal('hide');}catch(e){}
	clearNewPanelMember();
}
function addNewPanelist()
{
	/*try{$('#addPanelMemberDiv').modal('show');}catch(e){}
	try{$('#myModalPanel').modal('hide');}catch(e){}*/
	if(document.getElementById("choice2").checked)
	{
		$('#addPanelMemberDiv').show();
	}else
	{
		$('#addPanelMemberDiv').hide();
	}
	clearNewPanelMember();
}
function clearNewPanelMember()
{
	$('#panelMemberFirstName').css("background-color", "");
	$('#panelMemberLastName').css("background-color", "");
	$('#panelMemberEmail').css("background-color", "");
	$('#panelMemberFirstName').val("");
	$('#panelMemberLastName').val("");
	$('#panelMemberEmail').val("");
	$('#panelAddMemberErrordiv').empty();
}
function addNewPanelMember()
{

	$('#panelMemberFirstName').css("background-color", "");
	$('#panelMemberLastName').css("background-color", "");
	$('#panelMemberEmail').css("background-color", "");
	var panelMemberFirstName =	trim(document.getElementById("panelMemberFirstName").value);
	var panelMemberLastName =	trim(document.getElementById("panelMemberLastName").value);
	var panelMemberEmail =	trim(document.getElementById("panelMemberEmail").value);
	$('#panelAddMemberErrordiv').empty();
	
	var counter=0;
	var focus=0;	
	
	if (panelMemberFirstName=="")
	{
		$('#panelAddMemberErrordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focus==0)
			document.getElementById("panelMemberFirstName").focus();
		
		$('#panelMemberFirstName').css("background-color", "#F5E7E1");
		counter++;
		focus++;
	}
	if (panelMemberLastName=="")
	{
		$('#panelAddMemberErrordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focus==0)
			document.getElementById("panelMemberLastName").focus();
		$('#panelMemberLastName').css("background-color", "#F5E7E1");
		counter++;
		focus++;
	}
	if (panelMemberEmail=="")
	{
		$('#panelAddMemberErrordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focus==0)
			document.getElementById("panelMemberEmail").focus();
		$('#panelMemberEmail').css("background-color", "#F5E7E1");
		counter++;
		focus++;
	}else if(!isEmailAddress(panelMemberEmail))
	{		
		$('#panelAddMemberErrordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focus==0)
			document.getElementById("panelMemberEmail").focus();
		$('#panelMemberEmail').css("background-color", "#F5E7E1");
		counter++;
		focus++;
	}
	
	if(counter>0)
	{
		$('#panelAddMemberErrordiv').show();
		return;
	}
	
	CandidateGridSubAjax.addNewPanelMember(panelMemberFirstName,panelMemberLastName,panelMemberEmail,{ 
		async: true,
		callback: function(data)
		{
			addNewPanelistMember(data[0],data[1],data[2]);
			//hideAddMemberPanel();
			clearNewPanelMember();
			$('#addPanelMemberDiv').hide();
			document.getElementById("choice2").checked=false; 
		},
	});
}

function addNewPanelistMember(userId,firstName,lastName){
	var userName		=		firstName+" "+lastName;
	var panelistDiv		=		document.getElementById("panelistDiv");
	
	var strInnerHtml	=		trim("<div class=\"span3\">"+resourceJSON.msgNoPanelistFound+"</div>");
	if(panelistDiv.innerHTML==strInnerHtml){
		panelistDiv.innerHTML="";
	}
	
	getAllPanelistDivId();
	$('#panelErrordiv').empty();
	if($.inArray(parseInt(userId), arrPanel)==-1)
	{
		
		arrPanel.push(parseInt(userId));

		var newAttendee=panelistDiv.innerHTML +"<div id='P"+userId+"' class='span3'>"+userName
		+"&nbsp;<a href='javascript:void(0);' onclick='deletePanelist(\""+userId+"\");'><img width='15' height='15' class='can' src='images/can-icon.png' alt=''></a></div>";
		$('#panelistDiv').html(newAttendee);

	}else
	{	
		$('#panelErrordiv').append("&#149; "+resourceJSON.msgPanelistalreadyadded+"<br>");
	}
	$('#panelistDiv').show();
	//$('#addPanelMemberDiv').hide();
	clearNewPanelMember();
	
}
var tId;
var jId;
var isPanelReq;
function getPanelByJobWiseStatus(teacherId,jobId,isPanel)
{
	isPanel=true;
	CandidateGridSubAjax.getPanelByJobWiseStatus(jobId,{ 
		async: true,
		callback: function(data)
		{
			if(data[0]==1)
			{	
				$('#errordivStauts').hide();
				$('#jobPanelStatusId').html(data[1]);
				$('#myJobWiseStatusDiv').modal('show');
				tId=teacherId;
				jId=jobId;
				isPanelReq=isPanel;
			}else
			{
				$('#myJobWiseStatusDiv').modal('hide');
				$('#jPanelStauts').val(data[1]);
				getPanel(teacherId,jobId,isPanel,data[1]);
			}
		},
	});
	//getPanel(teacherId,jobId,isPanel);
}

function showPanel()
{
	if($('#jobPanelStatusId').val()=="Select Status")
	{
		$('#errordivStauts').empty();
		$('#errordivStauts').html("Please select a Stauts");
		$('#errordivStauts').show();
		
	}else
	{
		$('#myJobWiseStatusDiv').modal('hide');
		$('#jPanelStauts').val($('#jobPanelStatusId').val());
		getPanel(tId,jId,isPanelReq,$('#jobPanelStatusId').val());
	}
}
function cancelOne()
{
	$('#panelDiv').hide();
	document.getElementById("choice1").checked=false; 
	$('#panelErrordiv').hide();
}
function cancelTwo()
{
	clearNewPanelMember();
	$('#addPanelMemberDiv').hide();
	document.getElementById("choice2").checked=false; 
	$('#panelErrordiv').hide();
}
//************** Mukesh Phone Interview Code Start******************************
function clearColorEvents(){
	$('#eventArrordiv').empty();
	$('#eventName').css("background-color", "");
	$('#eventDescriptionCG').find(".jqte_editor").css("background-color", "");
	$('#subjectforParticipants').css("background-color","");
	$('#msgforParticipantsCG').find(".jqte_editor").css("background-color","");
	$('#subjectforFacilator').css("background-color","");
	$('#messtoFaciliatatorCG').find(".jqte_editor").css("background-color","");
}

function validateEvents(){
	
	clearColorEvents();
	var cnt=0;
	var focs=0;
	if (trim(document.getElementById("eventName").value)==""){
		$('#eventArrordiv').append("&#149; "+resourceJSON.msgEventName+"<br>");
		if(focs==0)
			$('#eventName').focus();
		$('#eventName').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	//schedule type validation
	var intschedule_Id = document.getElementsByName('intscheduleId');
	var intscheduleP="";
	for(var i = 0; i < intschedule_Id.length; i++){
	    if(intschedule_Id[i].checked){
	        intscheduleP = intschedule_Id[i].value;
	    }
	}
	if (intscheduleP == null || intscheduleP == "") {
		$('#eventArrordiv').append("&#149; "+resourceJSON.msgScheduleType+"<br>");
		if (focs==0)
			$('input:radio[name=intscheduleId]')[0].focus();
		cnt++;focs++;
	}
	//description validation
/*	if ($('#eventDescriptionCG').find(".jqte_editor").text().trim()==""){
		$('#eventArrordiv').append("&#149; Please enter Description<br>");
		if(focs==0)
			$('#eventDescriptionCG').find(".jqte_editor").focus();
		$('#eventDescriptionCG').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}*/
	//participants validation
	if (trim(document.getElementById("subjectforParticipants").value)==""){
		$('#eventArrordiv').append("&#149; "+resourceJSON.msgSubjecttoCandidate+"<br>");
		if(focs==0)
			$('#subjectforParticipants').focus();
		$('#subjectforParticipants').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if ($('#msgforParticipantsCG').find(".jqte_editor").text().trim()==""){
		$('#eventArrordiv').append("&#149;  "+resourceJSON.msgMessagetoCandidates+"<br>");
		if(focs==0)
			$('#msgforParticipantsCG').find(".jqte_editor").focus();
		$('#msgforParticipantsCG').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if (trim(document.getElementById("subjectforFacilator").value)==""){
		$('#eventArrordiv').append("&#149; "+resourceJSON.msgSubjecttoFacilitators+"<br>");
		if(focs==0)
			$('#subjectforFacilator').focus();
		$('#subjectforFacilator').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if ($('#messtoFaciliatatorCG').find(".jqte_editor").text().trim()==""){
		$('#eventArrordiv').append("&#149;  "+resourceJSON.msgtofacilators+"<br>");
		if(focs==0)
			$('#messtoFaciliatatorCG').find(".jqte_editor").focus();
		$('#messtoFaciliatatorCG').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#eventArrordiv').show();
		return false;
	}
}
function clearPhoneInterviewData(){
	dwr.util.setValues({eventId:null,eventName:null,msgtoparticipants:null,subjectforParticipants:null,subjectforFacilator:null,msgtofacilitators:null});
	$('#msgforParticipantsCG').find(".jqte_editor").html("");
	$('#messtoFaciliatatorCG').find(".jqte_editor").html("");
	$('#eventDescriptionCG').find(".jqte_editor").html("");
}

function addPhoneInterview(teacherId,jobId,isDemo)
{
	clearColorEvents();
	clearPhoneInterviewData();
	
	$('#evtsch').tooltip();
	$("html, body").animate({ scrollTop: 0 }, "slow");
	$('#eventArrordiv').empty();
	$('#addEditEventDiv').show();
	$('#facilitatorAndScheduleDiv').hide();
	document.getElementById("teacherIdforEvent").value=teacherId;
	getEventSchedule(6);
	getEmailTemplatesFacilitatorsByDistrict(document.getElementById('districtIdforEvent').value);
	getEmailTemplatesByDistrictId(document.getElementById('districtIdforEvent').value);
	try{$('#myModalPhoneInterview').modal('show');}catch(e){}
	$('#eventName').focus();
}


function savePhoneEvent(){
	if(!validateEvents())
		return;
	var eventDetails = {eventId:null,eventName:null,msgtoparticipants:null,subjectforParticipants:null,subjectforFacilator:null,msgtofacilitators:null};
	var districtId = document.getElementById('districtIdforEvent').value;
	//alert("districtId :: "+districtId)
	var intschedule_Id = document.getElementsByName('intscheduleId');
	var intschedule="";
	for(var i = 0; i < intschedule_Id.length; i++){
	    if(intschedule_Id[i].checked){
	        intschedule = intschedule_Id[i].value;
	    }
	}	
	var teacherId= document.getElementById("teacherIdforEvent").value
		
	dwr.util.getValues(eventDetails);
	dwr.engine.beginBatch(); 
	eventDetails.description=dwr.util.getValue("eventDescription");
	
	var jobOrder = {jobId:dwr.util.getValue("jobId")};
	eventDetails.jobOrder = jobOrder;
	
	EventAjax.saveCGPhoneEvent(eventDetails,districtId,intschedule,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
		//exitfrompageforEventOnCG();
		 document.getElementById('eventId').value=data.eventId;
		 if(data.eventSchedulelId.eventScheduleName.toUpperCase()=="Fixed Date and Time".toUpperCase())
			 document.getElementById('chkDATFlag').value=1;
		 else
			 document.getElementById('chkDATFlag').value=3;
		 
		 $('#addEditEventDiv').hide();
		 $('#facilitatorAndScheduleDiv').show();
		 getDistrictUser(districtId);
		 displayPhoneInterviewFacilitator(data.eventId)
		 displayEventSchedule();
		}
	});
	dwr.engine.endBatch();
}


function getDistrictUser(districtId)
{
	EventAjax.getDistrictUserList(districtId,{
		async : true,
		errorHandler : handleError,
		callback : function(data) {
		  $("#facilitatorId").html(data);
		}
	});
}

function addFacilitatorCG(){
	var eventId = document.getElementById('eventId').value;
	var userId = document.getElementById('facilitatorId').value;
	
	if(userId!=null && userId!='' && userId!="0"){
		ManageFacilitatorsAjax.saveFacilitatorByUserId(userId,eventId,
			{
			async: true,
			callback: function(data)
			{
				if(data=="success"){
					displayPhoneInterviewFacilitator(eventId);
				 }	
			},
	      errorHandler:handleError	
		});	
	}
}


function displayPhoneInterviewFacilitator(eventId){
	ManageFacilitatorsAjax.getFacilitatorGridNew(eventId,{
		async : true,
		callback : function(data) {
		$('#facilitatorNewDivCG').html(data.split("####")[0]);
		var cnt = data.split("####")[1];
			for(i=1;i<cnt;i++)
			{
				$('#rmId'+i+'').tooltip();
			}
		},
		errorHandler : handleError
	});
	
}

function deleteFacilitator(facilitatorId)
{
	document.getElementById("facilitatordelId").value=facilitatorId;
	$('#myModalactMsgShow').modal('show');
}
function removeFacilitatorById()
{
 var eventId=document.getElementById('eventId').value
 var facilitatorId=document.getElementById("facilitatordelId").value;
 ManageFacilitatorsAjax.removeFacilitator(facilitatorId,
		{
		  async: true,
		  callback: function(data){
	      displayPhoneInterviewFacilitator(eventId);
		  },
		 errorHandler:handleError		
		});	
 $('#myModalactMsgShow').modal('hide');	
}



function getEventSchedule(evntType) 
{
	EventAjax.getEventSchedule(evntType,{
		async : false,
		callback : function(data) {
			$('#intschedule').html(data);
		},
		errorHandler : handleError
	});
}

function getEmailTemplatesFacilitatorsByDistrict(districtId) {
	EventAjax.getEmailMessageTempFacilitators(districtId,0,0, {
		async : true,
		errorHandler : handleError,
		callback : function(data) {
			// alert(" data : "+data+" Length : "+data.length);
		if (data != "" && data.length > 1) {
			$("#msgtofacilitatorsDiv1").show();
			$("#msgtofacilitatorss").html(data);
		} else {
			$("#msgtofacilitatorsDiv1").hide();
		}
	}
 });
}
function getEmailDescription1() {
	var descId = document.getElementById('msgtofacilitatorss').value;
	if (descId != null && descId != '') {
		EventAjax.getFacilitatorsEmailMessageTempBody(descId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
			$('#messtoFaciliatatorCG').find(".jqte_editor").html(
					data.split("@@@@####@@@@")[0]);
			document.getElementById('subjectforFacilator').value = data
					.split("@@@@####@@@@")[1];
		}
		});
	} else {
		$('#messtoFaciliatatorCG').find(".jqte_editor").html("");
		document.getElementById('subjectforFacilator').value = "";
	}
}


function getEmailTemplatesByDistrictId(districtId) {
	EventAjax.getEventEmailMessageTemp(districtId,0,0, {
		async : true,
		errorHandler : handleError,
		callback : function(data) {
		if (data != "" || data.length > 0) {
			$("#messagetoprinciple").html(data);
		} else {
			$("#msgtopart").hide();
			$("#messagetoprincipleDiv").hide();
		}
	 }
	});
}

function getEmailDescription() {
	var descId = document.getElementById('messagetoprinciple').value;
	if (descId != null && descId != '') {
		EventAjax.getEventEmailMessageTempBody(descId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				$('#msgforParticipantsCG').find(".jqte_editor").html(
						data.split("@@@@####@@@@")[0]);
				document.getElementById('subjectforParticipants').value = data
						.split("@@@@####@@@@")[1];
			}
		});
	} else {
		$('#msgforParticipantsCG').find(".jqte_editor").html("");
		document.getElementById('subjectforParticipants').value = "";
	}
}


function editPhoneInterview(eventid) 
{
	$('#loadingDiv').fadeIn();
//	delay(1);
	EventAjax.getEventByEventId(eventid, {
		async : true,
		errorHandler : handleError,
		callback : function(data) 
		{
		
		$('#myModalPhoneInterview').modal('show');
		clearColorEvents();
		$('#eventName').focus();
		$('#evtsch').tooltip();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('#eventArrordiv').empty();
		$('#addEditEventDiv').show();
		$('#facilitatorAndScheduleDiv').hide();
	 	dwr.util.setValues(data);
		$('#msgforParticipantsCG').find(".jqte_editor").html(data.msgtoparticipants);
		$('#messtoFaciliatatorCG').find(".jqte_editor").html(data.msgtofacilitators);
		$('#eventDescriptionCG').find(".jqte_editor").html(data.description);
		getEventSchedule(6);
		var intschedule_Id = document.getElementsByName('intscheduleId');
		$('input[name=intscheduleId][value='+data.eventSchedulelId.eventScheduleId+']').attr('checked', true);
		$('#loadingDiv').hide();
	 }
	});
	displayCommanMethods();
	
	
}

function displayCommanMethods(){
	var districtId=document.getElementById('districtIdforEvent').value
	getEmailTemplatesFacilitatorsByDistrict(districtId);
	getEmailTemplatesByDistrictId(districtId);
}

function showAddEditPhoneEvents(){
	$('#addEditEventDiv').show();
	$('#facilitatorAndScheduleDiv').hide();
}

///////////////// EventSchedule ///////////////////////////
function displayEventSchedule()
{
	$('#eventScheduleDivMain').html("");
	var eventId = document.getElementById('eventId').value;
		var chkDATFlag = document.getElementById('chkDATFlag').value;
		if(chkDATFlag!=1)
		{
			if(chkDATFlag==2)
			{
				$("#addScheduleLink").show();
				$("#addScheduleLinkForSlots").hide();
			}
			else if(chkDATFlag==3)
			{
				$("#addScheduleLink").hide();
				$("#addScheduleLinkForSlots").show();
			}
		}
		else
		{
			$("#addScheduleLink").hide();
			$("#addScheduleLinkForSlots").hide();
		}
		try{
		
			EventScheduleAjax.geteventSchedule(eventId,noOfRows,page,sortOrderStr,sortOrderType,{
				async: false,
				callback: function(data)
				{
					try{$('#eventScheduleDivMain').html(data[0]);
					}catch(e){}
					$('#noofcandidate').tooltip();
					//document.getElementById("eventScheduleDivMain").innerHTML=data[0];
					$('#inputDivCount').val(data[1]);
					var cnt = data[1];
					
					
						for(i=1;i<cnt;i++)
						{
							try{
							var cal = Calendar.setup({ onSelect: function(cal) { cal.hide()}, showTime: true});
							cal.manageFields('eventStartDate'+i+'', 'eventStartDate'+i+'', '%m-%d-%Y');
							cal.manageFields('eventEndDate'+i+'', 'eventEndDate'+i+'', '%m-%d-%Y');
							}catch(e){}
						}
					
				},
				errorHandler:handleError
			});
			
		}catch(e){alert(e);}
	
}


function addSchedule()
{
	$('#eventArrordiv').empty();
	$('#eventScheduleDivMain').show();
	
	var eventId = $("#eventId").val().trim();
	
	var inputDivCount = $("#inputDivCount").val();
	
	var chkDATFlag = trim(document.getElementById("chkDATFlag").value);

	EventScheduleAjax.createAddScheduleDiv(eventId,inputDivCount,{
		async: true,
		callback: function(data)
		{
			$("#eventScheduleDivMain").append(data);
			if(chkDATFlag==1)
				$("#inputDivCount").val(1);
			else
				$("#inputDivCount").val(parseInt(inputDivCount)+1);
		},
		errorHandler:handleError	
	});
}

function saveSchedule()
{
	$('#eventArrordiv').empty();	
	var counter=0;
	
	var inputDivCount = trim(document.getElementById("inputDivCount").value);
	var timezone = trim(document.getElementById("timezoneforevent").value);
	var eventId = trim(document.getElementById("eventId").value);
	var chkDATFlag = trim(document.getElementById("chkDATFlag").value);
	var inttype	= 6;//trim(document.getElementById("inttype").value);
	var singlebookingonly=false;
	if(document.getElementById("singlebookingonly")!=null){
		if(document.getElementById("singlebookingonly").checked){
			singlebookingonly=true
		}
	}
	var noOfCandiadte="";
	try{noOfCandiadte=document.getElementById("noofcandidateperslot").value;}catch(e){}
	if(inttype==1)
	{
		//This is vvi check
	}
	else
	{
		var scheduleIdArr = "";
		var eventStartDateArr = "";
		var starthrArr = ""; 
		var starttimeformArr = "";
		var endhrArr = ""; 
		var endtimeformArr = "";
		var locationArr = "";
		var currentDate = new Date();
		var countRecord = 0;
		
		if(chkDATFlag=='1')
			inputDivCount=1;
		
		
			for(var i=0;i<inputDivCount;i++)
			{
				if(i==0){
					eventStartDateArr 	= eventStartDateArr+trim(document.getElementById("eventStartDate").value)+",";
					starthrArr 			= starthrArr+trim(document.getElementById("starthr").value)+",";
					starttimeformArr 	= starttimeformArr+trim(document.getElementById("starttimeform").value)+",";
					endhrArr	 		= endhrArr+trim(document.getElementById("endhr").value)+",";
					endtimeformArr 		= endtimeformArr+trim(document.getElementById("endtimeform").value)+",";
					
					if(trim(document.getElementById("locationfirst").value)=="")
						locationArr 		= locationArr+null+"##";
					else
						locationArr 		= locationArr+trim(document.getElementById("locationfirst").value)+"##";

					scheduleIdArr		= scheduleIdArr+trim(document.getElementById("eventScheduleId").value)+",";
				}
				else{
					eventStartDateArr = eventStartDateArr+trim(document.getElementById("eventStartDate"+i).value)+",";
					starthrArr 			= starthrArr+trim(document.getElementById("starthr"+i).value)+",";
					starttimeformArr 	= starttimeformArr+trim(document.getElementById("starttimeform"+i).value)+",";
					endhrArr	 		= endhrArr+trim(document.getElementById("endhr"+i).value)+",";
					endtimeformArr 		= endtimeformArr+trim(document.getElementById("endtimeform"+i).value)+",";
					
					if(trim(document.getElementById("location"+i).value)=="")
						locationArr 		= locationArr+null+"##";
					else
						locationArr 		= locationArr+trim(document.getElementById("location"+i).value)+"##";

					scheduleIdArr		= scheduleIdArr+trim(document.getElementById("eventScheduleId"+i).value)+",";
				}
			}
			
			
			clearAllFieldsColor();
			
			//=============== Error Msg=========================		
				
				if (timezone == '') {
					if (counter == 0) {
						$('#timezoneforevent').focus();
					}
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgTimeZone1+"<br>");
					$('#timezoneforevent').css("background-color", "#F5E7E1");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
			
				var eSdArrFlag = false;
				var comStartWithCurrentDateFlag = false;
				var comStartAndEndTimeFlag = false;
				var eShrArrFlag = false; 
				var eStimeformArrFlag = false;
				var eEhrArrFlag = false; 
				var eEtimeformArrFlag = false;
				
				for(var i=0;i<inputDivCount;i++)
				{
					var eSdArr = eventStartDateArr.split(",");
					var eShrArr = starthrArr.split(","); 
					var eStimeformArr = starttimeformArr.split(",");
					var eEhrArr = endhrArr.split(","); 
					var eEtimeformArr = endtimeformArr.split(",");
					var chkOneRow = false;
					
					if(i==0)
					{
						if(eSdArr[0]=='' && eShrArr[0]=='' && eEhrArr[0]=='')
			             {
							if(inputDivCount==1)
								chkOneRow=true;
			             }
						else
							chkOneRow=true;
						
						if(chkOneRow)
						{
							var startTimeDate = new Date();
							var endTimeDate = new Date();
							var startTimeHHMM = "";
							var endTimeHHMM = "";
							
							var retValue = true;
			                var stdate=eSdArr[0].split('-');
			                var dstart = new Date();
			                dstart.setFullYear(stdate[2]);
			                dstart.setMonth(stdate[0]-1);
			                dstart.setDate(stdate[1]);
			                var curdate=new Date();
							
							if (eSdArr[0] == '') {
								$('#eventStartDate').css("background-color", "#F5E7E1");
								$('#eventStartDate').focus();
								eSdArrFlag=true;
							}
							else if(curdate>dstart)
							{
								$('#eventStartDate').css("background-color", "#F5E7E1");
								$('#eventStartDate').focus();
								comStartWithCurrentDateFlag=true;
							}
								
							
							if (eShrArr[0] == '') {
								$('#starthr').focus();
								$('#starthr').css("background-color", "#F5E7E1");
								eShrArrFlag=true;
							}
							
							if (eStimeformArr[0] == '') {
								$('#starttimeform').focus();
								$('#starttimeform').css("background-color", "#F5E7E1");
								eStimeformArrFlag=true;
							}
							
							if (eEhrArr[0] == '') {
								$('#endhr').focus();
								$('#endhr').css("background-color", "#F5E7E1");
								eEhrArrFlag=true;
							}
							
							if (eEtimeformArr[0] == '') {
								$('#endtimeform').focus();
								$('#endtimeform').css("background-color", "#F5E7E1");
								eEtimeformArrFlag=true;
							}
							
							if(eShrArr[0]!='' && eStimeformArr[0]!='' && eEhrArr[0]!='' && eEtimeformArr[0]!='')
							{
								startTimeHHMM = eShrArr[0].split(":");
								endTimeHHMM = eEhrArr[0].split(":");
								if(eStimeformArr[0]=='PM')
								{
									startTimeHHMM[0] = parseInt(startTimeHHMM[0])+12;
								}
								if(eEtimeformArr[0]=='PM')
								{
									endTimeHHMM[0] = parseInt(endTimeHHMM[0])+12;
								}
								
								startTimeDate.setHours(startTimeHHMM[0], startTimeHHMM[1], '00');
								
								endTimeDate.setHours(endTimeHHMM[0], endTimeHHMM[1], '00');
								
								if(startTimeDate > endTimeDate)
								 {
									$('#starthr').focus();
									$('#starthr').css("background-color", "#F5E7E1");
									$('#starttimeform').focus();
									$('#starttimeform').css("background-color", "#F5E7E1");
									$('#endhr').focus();
									$('#endhr').css("background-color", "#F5E7E1");
									$('#endtimeform').focus();
									$('#endtimeform').css("background-color", "#F5E7E1");
									comStartAndEndTimeFlag=true;
								 }
							}
							countRecord = countRecord+1;
						}
						
					}
					else
					{
						 if(eSdArr[i]=='' && eShrArr[i]=='' && eEhrArr[i]=='')
			             {
							 
			             }else
			             {
			            	 var startTimeDate = new Date();
								var endTimeDate = new Date();
								var startTimeHHMM = "";
								var endTimeHHMM = "";
								
								var retValue = true;
				                var stdate=eSdArr[i].split('-');
				                var dstart = new Date();
				                dstart.setFullYear(stdate[2]);
				                dstart.setMonth(stdate[0]-1);
				                dstart.setDate(stdate[1]);
				                var curdate=new Date();
								
								
								
								if (eSdArr[i] == '') {
									$('#eventStartDate'+i).css("background-color", "#F5E7E1");
									$('#eventStartDate'+i).focus();
									eSdArrFlag=true;
								}else if(curdate>dstart)
								{
									$('#eventStartDate'+i).css("background-color", "#F5E7E1");
									$('#eventStartDate'+i).focus();
									comStartWithCurrentDateFlag=true;
								}
								
								if (eShrArr[i] == '') {
									$('#starthr'+i).focus();
									$('#starthr'+i).css("background-color", "#F5E7E1");
									eShrArrFlag=true;
								}
								
								if (eStimeformArr[i] == '') {
									$('#starttimeform'+i).focus();
									$('#starttimeform'+i).css("background-color", "#F5E7E1");
									eStimeformArrFlag=true;
								}
								
								
								if (eEhrArr[i] == '') {
									$('#endhr'+i).focus();
									$('#endhr'+i).css("background-color", "#F5E7E1");
									eEhrArrFlag=true;
								}
								
								if (eEtimeformArr[i] == '') {
									$('#endtimeform'+i).focus();
									$('#endtimeform'+i).css("background-color", "#F5E7E1");
									eEtimeformArrFlag=true;
								}
								
								if(eShrArr[i]!='' && eStimeformArr[i]!='' && eEhrArr[i]!='' && eEtimeformArr[i]!='')
								{
									startTimeHHMM = eShrArr[i].split(":");
									endTimeHHMM = eEhrArr[i].split(":");
									if(eStimeformArr[i]=='PM')
									{
										startTimeHHMM[i] = parseInt(startTimeHHMM[i])+12;
									}
									if(eEtimeformArr[i]=='PM')
									{
										endTimeHHMM[i] = parseInt(endTimeHHMM[i])+12;
									}
									
									startTimeDate.setHours(startTimeHHMM[0], startTimeHHMM[1], '00');
									
									endTimeDate.setHours(endTimeHHMM[0], endTimeHHMM[1], '00');
									
									if(startTimeDate > endTimeDate)
									 {
										$('#starthr'+i).focus();
										$('#starthr'+i).css("background-color", "#F5E7E1");
										$('#starttimeform'+i).focus();
										$('#starttimeform'+i).css("background-color", "#F5E7E1");
										$('#endhr'+i).focus();
										$('#endhr'+i).css("background-color", "#F5E7E1");
										$('#endtimeform'+i).focus();
										$('#endtimeform'+i).css("background-color", "#F5E7E1");
										comStartAndEndTimeFlag=true;
									 }
								}
								
								countRecord =countRecord+1;
			             }
					}
				}
				
				if(chkDATFlag==3)
				{
					if(countRecord<2)
					{
						$('#eventArrordiv').append("&#149; "+resourceJSON.msgschedulesAtLeast2+"<br>");
						$('#eventArrordiv').show();
						counter = counter + 1;
					}
				}
				
				if(chkDATFlag==2)
				{
					if(countRecord<1)
					{
						$('#eventArrordiv').append("&#149; "+resourceJSON.msgschedulesAtLeast1+"<br>");
						$('#eventArrordiv').show();
						counter = counter + 1;
					}
				}
				
				
				if(eSdArrFlag)
				{
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgPlzenterDate+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				if(comStartWithCurrentDateFlag)
				{
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgStDatelessCurrDate+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				if(eShrArrFlag)
				{
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgPlzenterStTime+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
						
				if(eStimeformArrFlag) {
					$('#eventArrordiv').append("&#149;"+resourceJSON.msgStartTimeFormat+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				
				if(eEhrArrFlag){
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgEndTime1+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				
				if(eEtimeformArrFlag){
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgEndTimeFormat+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
				
				if(comStartAndEndTimeFlag)
				{
					$('#eventArrordiv').append("&#149; "+resourceJSON.msgEndTimeLessStTime+"<br>");
					$('#eventArrordiv').show();
					counter = counter + 1;
				}
		//================================================
	     //check for participants			
		try{
			if(counter==0){
				EventAjax.checkAvailFacilitator(eventId,{
				async: false,
				callback: function(data)
				{
				  $('#loadingDiv').hide();
				  if(data==false){
					 $('#facilitatorId').focus();
					 $('#facilitatorId').css("background-color", "#F5E7E1");
					  counter = counter + 1;
					 $('#eventArrordiv').show();
					 $('#eventArrordiv').empty();
					 $('#eventArrordiv').append("&#149;Please add Facilitators.<br>");
				  }
				},
				 errorHandler:handleError	
				});
			}
		  }catch(e){alert(e)}		
		//================================================	
		try{
			if(counter==0)
			{
				EventScheduleAjax.saveSchedule(scheduleIdArr,eventId,timezone,eventStartDateArr,starthrArr,starttimeformArr,locationArr,null,endhrArr,endtimeformArr,singlebookingonly,noOfCandiadte,
				{
							async: false,
							callback: function(data)
							{
								$("#eventTypeWiseSchedule").append("");
								//alert("data :: "+data)
								if(data=="success")
								  sendPhoneInterviewFromCG();
								/*if(cont==0)
									window.location.href="manageevents.do";
								else if(cont==1)
									window.location.href="managefacilitators.do?eventId="+eventId;*/
								
							},
							errorHandler:handleError	
						});
			}
			}catch(e){alert(e)}
	}
}

function clearAllFieldsColor()
{
	var inputDivCount = $('#inputDivCount').val();
	$('#timezoneforevent').css("background-color", "");
	for(var i=0;i<inputDivCount;i++)
	{
		if(i==0){
			$('#eventStartDate').css("background-color", "");
			$('#starthr').css("background-color", "");
			$('#starttimeform').css("background-color", "");
			$('#endhr').css("background-color", "");
			$('#endtimeform').css("background-color", "");
		}
		else{
			$('#eventStartDate'+i).css("background-color", "");
			$('#starthr'+i).css("background-color", "");
			$('#starttimeform'+i).css("background-color", "");
			$('#endhr'+i).css("background-color", "");
			$('#endtimeform'+i).css("background-color", "");
		}
	}
}

///////////////////////////////////////////////////////////
function sendPhoneInterviewFromCG(){	
	$('#loadingDiv').fadeIn();
	var teacherIds="";
	
$('input[name="cgCBX"]:checked').each(function() {
		
		if($(this).attr("teacherid") !=null && ($(this).attr("teacherid")!="") )
		{
			if(teacherIds=="")
				teacherIds = teacherIds + $(this).attr("teacherid");
			else
				teacherIds = teacherIds + "," + $(this).attr("teacherid");
		}
	});
	
	var eventId=$("#eventId").val();
	if(teacherIds!=null)
	{
		try{
			ParticipantsAjax.sendEmailWithTeacherId(eventId,teacherIds,
		    {
				async: true,
				callback: function(data){ 
				  $('#loadingDiv').hide();
				  if(data==false){
					 $('#eventArrordiv').show();
					 $('#eventArrordiv').empty();
					 $('#eventArrordiv').append("&#149;"+resourceJSON.msgAddfacilators+"<br>");
				  }else{
					  $('#succesEmailMsg').html("Great! Event has been scheduled");
					  $('#sendEmailtoParticipants').modal('show');
				  }			
			}		
		     });	
		}catch(e)
		{
		 alert(e);	
		}
		
		
	}
	else
	{
	
	try{
			ParticipantsAjax.sendEmail(eventId,
		    {
				async: true,
				callback: function(data){ 
				  $('#loadingDiv').hide();
				  if(data==false){
					 $('#eventArrordiv').show();
					 $('#eventArrordiv').empty();
					 $('#eventArrordiv').append("&#149;"+resourceJSON.msgAddfacilators+"<br>");
				  }else{
					  $('#succesEmailMsg').html("Great! Event has been scheduled");
					  $('#sendEmailtoParticipants').modal('show');
				  }			
			}		
		     });	
		}catch(e)
		{
		 alert(e);	
		}
	}
}

function exitfrompageforEventOnCG(){
	try{$('#myModalPhoneInterview').modal('hide');}catch(e){}
	//getCandidateGrid();
}

function refreshCGonEvent(){
	$('#myModalPhoneInterview').modal('hide');
	getCandidateGrid();
	refreshStatus();
}

function closeListFromCG()
{
	$('#alertMsgForApply').modal('hide');
	$('#myJobDetailListFromCG').modal('hide');
	getCandidateGrid();
	refreshStatus();
}



function resendInvitePopUpConfirmed()
{
	var jobId=$("#jobId").val();
	if($('input[name=cgCBX]:checkbox:checked').length == 0 )
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
	}else
	{
		$('#loadingDiv').fadeIn();
		var teacherIds = "";
		$('input[name="cgCBX"]:checked').each(function(){
			var teacherId = $(this).attr("teacherid");//teacherid
			if(teacherIds=="")
				teacherIds=teacherIds+teacherId;
			else
				teacherIds=teacherIds+","+teacherId;
		});
		CandidateGridSubAjax.sendAgainInviteToCandidates(jobId,teacherIds,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
				$('#loadingDiv').fadeOut();
				$('#message2showConfirm').html(""+resourceJSON.MsgMailSentSuccessfully+"");
				$('#footerbtn').html("<button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
				$('#myModal3').modal('show');
		     }
		});		
	}
}

function resendInvitePopUp()
{
	if($('input[name=cgCBX]:checkbox:checked').length == 0 )
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
	}else
	{
		$('#message2showConfirm').html(""+resourceJSON.msgConfirmMail+"");
		$('#footerbtn').html("<button class='btn btn-primary' onclick=\"resendInvitePopUpConfirmed()\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');
	}

}

function senMultiInvite()
{	
	var teacherIds="";
	var eventid="";
	try
	{
		eventid=document.getElementById("interviewInviteMulti").value;
	}
	catch(e){}
	
	var jobId="";
	$('input[name="cgCBX"]:checked').each(function() {
		
		if($(this).attr("teacherid") !=null && ($(this).attr("teacherid")!="") )
		{
			if(teacherIds=="")
				teacherIds = teacherIds + $(this).attr("teacherid");
			else
				teacherIds = teacherIds + "," + $(this).attr("teacherid");
		}
	});	

	if(teacherIds!="")
	{
		document.getElementById("teacherIdforEvent").value=teacherIds;
		if(eventid!=null && eventid!="" )
		{
			$('#loadingDiv').fadeIn();
			EventAjax.getEventByEventId(eventid, {
				async : true,
				errorHandler : handleError,
				callback : function(data) 
				{				
				$('#myModalPhoneInterview').modal('show');
				clearColorEvents();
				$('#eventName').focus();
				$('#evtsch').tooltip();
				$("html, body").animate({ scrollTop: 0 }, "slow");
				$('#eventArrordiv').empty();
				$('#addEditEventDiv').show();
				$('#facilitatorAndScheduleDiv').hide();
			 	dwr.util.setValues(data);
				$('#msgforParticipantsCG').find(".jqte_editor").html(data.msgtoparticipants);
				$('#messtoFaciliatatorCG').find(".jqte_editor").html(data.msgtofacilitators);
				$('#eventDescriptionCG').find(".jqte_editor").html(data.description);
				getEventSchedule(6);
				var intschedule_Id = document.getElementsByName('intscheduleId');
				$('input[name=intscheduleId][value='+data.eventSchedulelId.eventScheduleId+']').attr('checked', true);
				$('#loadingDiv').hide();
			 }
			});
			displayCommanMethods();			
		}
		else
		{			
		clearColorEvents();
		clearPhoneInterviewData();
		$('#evtsch').tooltip();
		$("html, body").animate({ scrollTop: 0 }, "slow");
		$('#eventArrordiv').empty();
		$('#addEditEventDiv').show();
		$('#facilitatorAndScheduleDiv').hide();	
		getEventSchedule(6);
		getEmailTemplatesFacilitatorsByDistrict(document.getElementById('districtIdforEvent').value);
		getEmailTemplatesByDistrictId(document.getElementById('districtIdforEvent').value);
		try{$('#myModalPhoneInterview').modal('show');}catch(e){}
		$('#eventName').focus();		
		}
	}
	else
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
		
	}
	
}

function resendTalentPopUpConfirmed()
{
	var jobId=$("#jobId").val();
	if($('input[name=cgCBX]:checkbox:checked').length == 0 )
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
	}else
	{
		$('#loadingDiv').fadeIn();
		var teacherIds = "";
		$('input[name="cgCBX"]:checked').each(function(){
			var teacherId = $(this).attr("teacherid");//teacherid
			if(teacherIds=="")
				teacherIds=teacherIds+teacherId;
			else
				teacherIds=teacherIds+","+teacherId;
		});
		CandidateGridSubAjax.reSendTalentSignUp(jobId,teacherIds,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
				$('#loadingDiv').fadeOut();
				$('#message2showConfirm').html(""+resourceJSON.MsgAuthMailSentSuccessfully+"");
				$('#footerbtn').html("<button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
				$('#myModal3').modal('show');
		     }
		});		
	}
}

function resendTalentPopUp()
{
	if($('input[name=cgCBX]:checkbox:checked').length == 0 )
	{
		 $("#alertmsgToShow").html(resourceJSON.PlzSlctAtlstOneCndidate);
		 $('#alertMsgForApply').modal('show');
	}else
	{
		$('#message2showConfirm').html(""+resourceJSON.msgConfirmAuthMail+"");
		$('#footerbtn').html("<button class='btn btn-primary' onclick=\"resendTalentPopUpConfirmed()\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
		$('#myModal3').modal('show');
	}
}

function checkforSingleBooking(){
	if(document.getElementById("singlebookingonly").checked==true){
		$("#noofcandidateDiv").hide();
		document.getElementById("noofcandidateperslot").value='';
	}else{
		$("#noofcandidateDiv").show();
		document.getElementById("noofcandidateperslot").value='';
	}
}

function deleteSchedulePopUp(scheduleId)
{
	document.getElementById("deleteScheduleId").value=scheduleId;
	$('#succesEmailMsg').html("Do you really want to remove this slot?");
	$('#popupFooter').html("<button class='btn btn-primary' onclick=\"deleteScheduleByIdOnConfirmed()\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#sendEmailtoParticipants').modal('show');
	
}

function deleteScheduleByIdOnConfirmed()
{
	var scheduleId=document.getElementById("deleteScheduleId").value;
	$("#loadingDiv").fadeIn();	
	EventScheduleAjax.deleteEventScheduleById(scheduleId,{
		async: false,
		callback: function(data)
		{
			$("#loadingDiv").fadeOut();	
			displayEventSchedule();
			//html(""+resourceJSON.MsgMailSentSuccessfully+"");
			$('#succesEmailMsg').html("Slot has removed successfully");
			$('#popupFooter').html("<button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#sendEmailtoParticipants').modal('show');
			
		},
		errorHandler:handleError	
	});	
}

function getSchoolList(jobId)
{
	document.getElementById("jobIdSchool").value=jobId; 
	$("#loadingDiv").fadeIn();	
	CandidateGridSubAjax.getSchoolListByJob(jobId,noOfRowsSchool,pageforSchool,sortOrderStrSchool,sortOrderTypeSchool,{
		async: true,
		callback: function(data)
		{
		    $("#loadingDiv").hide();
			$('#myModalforSchholList').modal('show');
			document.getElementById("schoolListDivNew").innerHTML=data;
			applyScrollOnTb();
		},
		errorHandler:handleError	
	});	
}

//js selfservice common.js
function globalSearchSection(entityDiv,headQuarterDiv,branchDiv,districtDiv,btnDiv)
{
$(".modalTM").show();
$("#entityTypeDivMaster").html(entityDiv);
$("#headQuarterClassMaster").html(headQuarterDiv);
$("#branchClassMaster").html(branchDiv);
$("#districtClassMaster").html(districtDiv);
$("#searchBtnMaster").html(btnDiv);
}
function showSearchAgainMaster()
{
	$("#searchMasterDiv").show();
	$("#searchIdMaster").hide();
}
function hideSearchAgainMaster()
{
	$("#searchIdMaster").show();
	$("#searchMasterDiv").hide();
}

//******************** For Grid shadow **********************
function onHoverHighLight(divId){
	$("#"+divId.id).addClass("gridDivHighLight");
}
function onOutHighLight(divId){
	$("#"+divId.id).removeClass("gridDivHighLight");
}
//js referencecheck.js

var certificationDivCount=0;
var schoolDivCount=0;
var schoolDivCountCheck=0;
var schoolDivVal="";

var arrNewRequisition=[];

/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/* ======= Save AddEditJobOrder on Press Enter Key ========= */
function chkForEnterAddEditJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if((charCode==13) && (evt.srcElement.className!='jqte_editor'))
	{
		validateAddEditJobOrder();
	}	
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function checkJobStartDate()
{
	if($("#jobId").val()==""){
		var month = new Array();
		month[0] = resourceJSON.January;
		month[1] = resourceJSON.February;
		month[2] = resourceJSON.March;
		month[3] = resourceJSON.April;
		month[4] = resourceJSON.May;
		month[5] = resourceJSON.June;
		month[6] = resourceJSON.July;
		month[7] = resourceJSON.August;
		month[8] = resourceJSON.September;
		month[9] = resourceJSON.October;
		month[10] = resourceJSON.November;
		month[11] = resourceJSON.December;
		$('#errordiv').empty();
		$('#jobStartDate').css("background-color", "");
		var jobStartDate	=	new Date(trim(document.getElementById("jobStartDate").value));
		if(trim(document.getElementById("jobStartDate").value)==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPostingStartDate+"");
			$('#jobStartDate').focus();
			$('#jobStartDate').css("background-color", "#F5E7E1");
			$("#jsiDiv").hide();
		}else{
			$("#jsiDiv").fadeIn();
			var twoDaysAgoDate  =	new Date();
			twoDaysAgoDate = new Date(twoDaysAgoDate.setDate(twoDaysAgoDate.getDate()+2));
			var diffDays =  twoDaysAgoDate.getTime()-jobStartDate.getTime();
			diffDays=parseInt(diffDays / (1000 * 60 * 60 * 24));
			if(!(diffDays<=0)){
				$("#jsiDiv").hide();
				document.getElementById("textJsiDateMsg").innerHTML=""+resourceJSON.msgJobStratDateless+" "+month[twoDaysAgoDate.getMonth()]+" "+twoDaysAgoDate.getDate()+", "+twoDaysAgoDate.getFullYear()+""
				$("#textJsiDate").modal("show");
				$('#jobStartDate').focus();
				$('#jobStartDate').css("background-color", "#F5E7E1");
			}else{
				$("#jsiDiv").fadeIn();
			}
		}	
	}
}

function showHideScoreDiv(id){
	var maxScore = document.getElementById("MaxScore"+id).checked;
	if(maxScore == true){
		document.getElementById("MaxScoreDiv"+id).style.display="none";
	} else {
		document.getElementById("MaxScoreDiv"+id).style.display="block";
	}
}

var isFormSubmitted = false;
function showReferenceRecord() {
	//$('#loadingDiv').show();
	var referenceId	=	$("#elerefAutoId").val();
	var headQuarterId = $("#headQuarterId").val();
	var districtId	=	$("#districtId").val();
	var jobId		=	$("#jobId").val();
	var teacherId	=	$("#teacherId").val();
	var isAffilated	=	0;

	ReferenceCheckAjax.getDistrictSpecificQuestion(referenceId,headQuarterId, districtId, jobId, teacherId, isAffilated,{
		async: true,
		errorHandler:handleError,
		callback: function(data) {

			if(data!=null) {
				try {
					var dataArray 	= data.split("@##@");
					totalQuestions 	= dataArray[1];
					var textmsg = dataArray[2];

					if(textmsg!=null && textmsg!="" && textmsg!='null')
						$('#textForDistrictSpecificQuestions').html(textmsg);

					if(isFormSubmitted==false) {
						isFormSubmitted = true;
						if(totalQuestions==0) {
							document.getElementById("frmApplyJob").submit();
							return;
						}else if(totalQuestions!=undefined) {
							$('#tblGrid').html(dataArray[0]);
							//$('#loadingDiv_dspq_ie').hide();
							//$('#myModalDASpecificQuestions').modal('show');
						}
					}
				}catch(err){}
			} else {
				document.getElementById("frmApplyJob").submit();
			}
		}
	});	
}

function setDefColorForErrorMsg(){
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#email').css("background-color","");
}


function setDistrictQuestions(fromWhere) {

	$('#errordiv4question').empty();
	setDefColorForErrorMsg();
	var referenceId	=	$("#elerefAutoId").val();
	var districtId	=	$("#districtId").val();
	var headQuarterId = $("#headQuarterId").val();
	var jobId		=	$("#jobId").val();
	var teacherId	=	$("#teacherId").val();
	var iframeNorm 		= 	"";
	var innerNorm 		= 	"";
	var dsliderchkVal	= 	"";
	var inputNormScore 	= 	"";
	//var MaxScore		=	"";
	var count = 0;
	var cnt=0;
	var focs=0;
	
	var firstName	=	$("#firstName").val();
	var lastName	=	$("#lastName").val();
	var email		=	$("#email").val();
	var designation	=	$("#designation").val();
	var organization=	$("#organization").val();
	var contactnumber=	$("#contactnumber").val();
	var secondaryPhone=	$("#secondaryPhone").val();
	var relationship=	$("#relationship").val();
	var length		=	$("#length").val();
	var reference	=	$("#reference").val();
	var manager		=	$("#manager").val();

	var arr 		=	[];
	var arrUserInfo =	[];
	
	if(firstName=="")
	{
		$('#errordiv4question').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#firstName').focus();

		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(lastName=="")
	{
		$('#errordiv4question').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lastName').focus();

		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(email=="")
	{
		$('#errordiv4question').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#email').focus();

		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	} else if(!isEmailAddress(email)) {		
		$('#errordiv4question').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	var arrUserInfo = [firstName,lastName,email,designation,organization,contactnumber,secondaryPhone,relationship,length,reference,manager];
	
	for(i=1;i<=totalQuestions;i++) {
		
		var districtSpecificRefChkQuestion 	= 	{questionId:dwr.util.getValue("Q"+i+"questionId")};
		var questionTypeShortName 			= 	dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionTypeMaster 				= 	{questionTypeId:dwr.util.getValue("Q"+i+"questionTypeId")};
		var qType 							= 	dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionMaxScore				=	"";
		try{
			 iframeNorm 		= 	document.getElementById("Q"+i+"ifrmStatusNote");
			 innerNorm 			= 	iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			 inputNormScore 	= 	innerNorm.getElementById('statusNoteFrm');
			 questionMaxScore	=	inputNormScore.value;
			 dsliderchkVal 		= 	document.getElementById("Q"+i+"dsliderchk").value;
			 var MaxScore		=	document.getElementById("MaxScore"+i);
			
		}catch(err){}
		try{
			if(MaxScore.checked == false){
				if(dsliderchkVal == 1){
					if(questionMaxScore == "0"){
						count = count+1;
					}
				}
			} else { questionMaxScore = 999; }
		}catch(errr){}
		if(qType=='tf' || qType=='slsel') {
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {

				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} else {
				$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				return;
			}

			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
			});

		} else if(qType=='ml') {
			var insertedText = dwr.util.getValue("Q"+i+"opt");
			//if(insertedText!=null && insertedText!="") {
				arr.push({
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionMaxScore" : questionMaxScore,
					"questionType" : questionTypeShortName,
					"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				});
			/*} else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}*/
		} else if(qType=='et') {
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} else {
				$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				return;
			}

			var insertedText = dwr.util.getValue("Q"+i+"optet");
			var isValidAnswer = dwr.util.getValue("Q"+optId+"validQuestion");

			if(isValidAnswer=="false" && insertedText.trim()=="") {
				$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				return;
			}

			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : isValidAnswer,
			});
		} else if(qType=='mlsel'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				}
				 
				if(multiSelectArray!=""){

					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("Q"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionMaxScore" : questionMaxScore,
						"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
						"isValidAnswer" : isValidAnswer,
					});
				} else {
						$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						return;
				}
			}catch(err){}
		} if(qType=='mloet'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("Q"+i+"optmloet");
				if(multiSelectArray!=""){

					arr.push({
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("Q"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionMaxScore" : questionMaxScore,
						"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
						"isValidAnswer" : isValidAnswer,
					});
				}else{
						$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						return;
				}
			}catch(err){}
		} else if(qType=='SLD') {
			var insertedText = dwr.util.getValue("Q"+i+"opt");
				arr.push({
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionMaxScore" : questionMaxScore,
					"questionType" : questionTypeShortName,
					"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				});
		} else if(qType=='sloet') {
			var optId="";
			var insertedText = dwr.util.getValue("Q"+i+"optet");
			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {

				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} else {
				$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				return;
			}
			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
			});

		}
	}
	//alert(arr[0].insertedText);return false;
	if(fromWhere == "dashboard"){
		if(count > 0 ){
			$('#myModalReferenceCheckConfirm').modal('show');
			return false;
		}
	}
	if(arr.length == totalQuestions) {
		if(headQuarterId=="") headQuarterId=0;
		if(districtId=="")   districtId  =  0;
	//	 console.log ( 'Hiiiiiii       headQuarterId=='+headQuarterId +'***********  district Id =='+districtId );
		ReferenceCheckAjax.setDistrictQuestions(referenceId,headQuarterId, districtId, jobId, teacherId,arr,arrUserInfo,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				
				// Answer Save Successfully referencecheckspecificquestions.do
				window.location="referencecheckfinal.do";
				arr =[];
			 }
		  }
		});	
	} else {
		$("#errordiv4question").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
		return;
	}
}

function saveDistrictSpecificQuestion() {

	var isaffilatedstatus=document.getElementById("isaffilatedstatushiddenId").value;
	var jobId = document.getElementById("jobId").value;
	if(isaffilatedstatus==1) {
		try {
			try{$('#myModalv').modal('hide');}catch(err){}
			$('#myModalDASpecificQuestions').modal('hide');
			//$('#epiIncompAlert').modal('show');
		}catch(err) {}
	}
	ReferenceCheckAjax.saveJobForTeacher(jobId,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1) {
			try {
				$('#myModalDASpecificQuestions').modal('hide');
			}catch(err) {}
				// Start ... Dynamic portfolio for external job
				var completeNow = document.getElementById("completeNow").value;
				
				if(completeNow==1) {
					continueCompleteNow();
				} else {
					getTeacherJobDone(jobId);
					$('#myModalCL').modal('hide');
				}
				// End ... Dynamic portfolio for external job
			}
		}
	});	
}

function closeReferenceCheck(id){
	$('#myModalReferenceCheckConfirm').modal('hide');
}


/**********************Add BY Ram Nath***************************************/
$(function(){	
	$('.confirmFalse').click(function(){
		$('#eRef').hide();		
		$('#jWTeacherStatusNotesDiv').show();
		try{
			if($('#cg').val()==1){
				$('#jWTeacherStatusNotesDiv').hide();	
				$('#myModalReferenceCheck').show();
			}
		}catch(e){}
	});
});


function openQuestionEReference(val){
//alert('Enter'+val);
$('#tblGrid_eref').html('');
$('#eRef #errordiv4question').html('');
$('#eRef #elerefAutoId').val('');
$('#eRef #districtId').val('');
$('#eRef #jobId').val('');
$('#eRef #teacherId').val('');
var referenceJobId		=	document.getElementById("referenceJobId").value
var districtId  		=	"";
  try{districtId=document.getElementById("districtId").value;}catch (e) {}
var headQuarerId = "";
try{headQuarerId 	=	document.getElementById("headQuarerId").value;}catch (e) {}
//eId1+","+tId1+","+uId;//referenceJobId,districtId
//alert(val+","+referenceJobId+","+districtId);
var allInfo=val.split(",");
var referenceId=allInfo[0];
var teacherId=allInfo[1];
var userId=allInfo[2];
$('#eRef #elerefAutoId').val(referenceId);
$('#eRef #districtId').val(districtId);
$('#eRef #jobId').val(referenceJobId);
$('#eRef #teacherId').val(teacherId);
$('#eRef #userId').val(userId);
				
$('#myModalLabel').html($('#jWTeacherStatusNotesDiv .modal-dialog #myModalLabel #status_title').html());
$('#eRef').show();
$('#jWTeacherStatusNotesDiv').hide();
try{
$('#myModalReferenceCheck').hide();
}catch(e){}
var isAffilated	=	0;

	ReferenceCheckAjax.getDistrictSpecificQuestion(referenceId,headQuarerId, districtId, referenceJobId, teacherId, isAffilated,{
		async: true,
		errorHandler:handleError,
		callback: function(data) {
		//alert(data);
		$('#tblGrid_eref').html(data);
		////setTimeout(function(){
		//$("html, body").delay(2000).animate({scrollTop: $('#eRef #firstName').offset().top }, 2000);
		//$('#eRef #firstName').offset()},10);
		//$('#eRef #tblGrid').(data);			
		}
	});	
}

function setDistrictQuestions1(fromWhere) {
	$('#errordiv4question').empty();
	var fitScore			=	"";
	var statusId			=	"";
	var secondaryStatusId	= "";
	var cg=0;
	var teacherDetails="";
	var teacherId="";
	try{
	fitScore			=	document.getElementById("fitScoreForStatusNote").value;
	statusId			=	document.getElementById("statusId").value;
	secondaryStatusId	=	document.getElementById("secondaryStatusId").value;
	}catch(e){}
	
	try{
		if($('#cg').val()==1){
			cg=1;
			teacherDetails=$('#teacherDetails').val();
			teacherId=$('#teacherId').val();
		}
	}catch(e){}
	
	//alert(fitScore+"     "+statusId+"     "+secondaryStatusId);
	var referenceId	=	$("#eRef #elerefAutoId").val();
	var headQuarterId = $("#headQuarterId").val();
	var districtId	=	$("#eRef #districtId").val();
	var jobId		=	$("#eRef #jobId").val();
	var teacherId	=	$("#eRef #teacherId").val();
	var userId		=	$("#eRef #userId").val();
	var iframeNorm 		= 	"";
	var innerNorm 		= 	"";
	var dsliderchkVal	= 	"";
	var inputNormScore 	= 	"";
	//var MaxScore		=	"";
	var count = 0;
	var cnt=0;
	var focs=0;
	
	var firstName	=	$("#eRef #firstName").val();
	var lastName	=	$("#eRef #lastName").val();
	var email		=	$("#eRef #email").val();
	var designation	=	$("#eRef #designation").val();
	var organization=	$("#eRef #organization").val();
	var contactnumber=	$("#eRef #contactnumber").val();
	var secondaryPhone=	$("#eRef #secondaryPhone").val();
	var relationship=	$("#eRef #relationship").val();
	var length		=	$("#eRef #length").val();
	var reference	=	$("#eRef #reference").val();
	var manager		=	$("#eRef #manager").val();
	var totalQuestions =parseInt($("#eRef #totalQuestions").val());
	//alert('totalQuestions===='+totalQuestions+"  referenceId=="+referenceId+" districtId==="+districtId+" jobId==="+jobId+" teacherId=="+teacherId); 

	var arr 		=	[];
	var arrUserInfo =	[];
	
	if(firstName=="")
	{
		$('#errordiv4question').append("&#149; Please enter First Name<br>");
		if(focs==0)
			$('#firstName').focus();

		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(lastName=="")
	{
		$('#errordiv4question').append("&#149; Please enter Last Name<br>");
		if(focs==0)
			$('#lastName').focus();

		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(email=="")
	{
		$('#errordiv4question').append("&#149; Please enter Email<br>");
		if(focs==0)
			$('#email').focus();

		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	} else if(!isEmailAddress(email)) {		
		$('#errordiv4question').append("&#149; Please enter valid Email<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	var arrUserInfo = [firstName,lastName,email,designation,organization,contactnumber,secondaryPhone,relationship,length,reference,manager,userId];
	
	//alert("firstName===="+firstName+"  lastName====="+lastName+"  email==="+email+"  designation====="+designation+"  organization====="+organization+"  contactnumber====="+contactnumber+"  secondaryPhone====="+secondaryPhone+"  relationship====="+relationship+"  length====="+length+"  reference====="+reference+"  manager====="+manager);
	
	//alert('totalQuestions==1=='+totalQuestions);
	for(i=1;i<=totalQuestions;i++) {
		
		var districtSpecificRefChkQuestion 	= 	{questionId:dwr.util.getValue("Q"+i+"questionId")};
		var questionTypeShortName 			= 	dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionTypeMaster 				= 	{questionTypeId:dwr.util.getValue("Q"+i+"questionTypeId")};
		var qType 							= 	dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionMaxScore				=	"";
		try{
			 iframeNorm 		= 	document.getElementById("Q"+i+"ifrmStatusNote");
			 innerNorm 			= 	iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			 inputNormScore 	= 	innerNorm.getElementById('statusNoteFrm');
			 questionMaxScore	=	inputNormScore.value;
			 dsliderchkVal 		= 	document.getElementById("Q"+i+"dsliderchk").value;
			 var MaxScore		=	document.getElementById("MaxScore"+i);
			
		}catch(err){}
		try{
			if(MaxScore.checked == false){
				if(dsliderchkVal == 1){
					if(questionMaxScore == "0"){
						count = count+1;
					}
				}
			} else { questionMaxScore = 999; }
		}catch(errr){}
		if(qType=='tf' || qType=='slsel') {
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {

				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} 
			else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}
		//alert('totalQuestions==2=='+totalQuestions);
			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
			});

		} else if(qType=='ml') {
			var insertedText = dwr.util.getValue("Q"+i+"opt");
			//if(insertedText!=null && insertedText!="") {
				arr.push({
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionMaxScore" : questionMaxScore,
					"questionType" : questionTypeShortName,
					"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				});
			/*} else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}*/
		} else if(qType=='et') {
			var optId="";
		//alert('totalQuestions==3=='+totalQuestions);
			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} 
			else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}

			var insertedText = dwr.util.getValue("Q"+i+"optet");
			var isValidAnswer = dwr.util.getValue("Q"+optId+"validQuestion");

			if(isValidAnswer=="false" && insertedText.trim()=="") {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}

			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : isValidAnswer,
			});
		} else if(qType=='mlsel'){
		//alert('totalQuestions==4=='+totalQuestions);
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				}
				 
				if(multiSelectArray!=""){

					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("Q"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionMaxScore" : questionMaxScore,
						"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
						"isValidAnswer" : isValidAnswer,
					});
				} 
				else {
						$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
						return;
				}
			}catch(err){}
		} if(qType=='mloet'){
		//alert('totalQuestions==6=='+totalQuestions);
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("Q"+i+"optmloet");
				if(multiSelectArray!=""){

					arr.push({
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("Q"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionMaxScore" : questionMaxScore,
						"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
						"isValidAnswer" : isValidAnswer,
					});
				}
				else{
						$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
						return;
				}
			}catch(err){}
		} else if(qType=='SLD') {
		//alert('totalQuestions=7==='+totalQuestions);
			var insertedText = dwr.util.getValue("Q"+i+"opt");
				arr.push({
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionMaxScore" : questionMaxScore,
					"questionType" : questionTypeShortName,
					"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				});
		} else if(qType=='sloet') {
			var optId="";
			var insertedText = dwr.util.getValue("Q"+i+"optet");
			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {

				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} 
			else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}
			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
			});

		}
	}
	//alert(arr[0].insertedText);return false;
	//alert(fromWhere);
	if(fromWhere == "dashboard"){
		if(count > 0 ){
			$('#eRef').hide();
			$('#myModalReferenceCheckConfirm1').show();
			return false;
		}
	}
	
	//alert("arr.length=============="+arr+"      ------    "+totalQuestions);
	//alert(referenceId+"==="+districtId+"==="+jobId+"==="+teacherId+"==="+arr+"==="+arrUserInfo);
	if(arr.length == totalQuestions) {
		ReferenceCheckAjax.setDistrictQuestions(referenceId,headQuarterId , districtId, jobId, teacherId,arr,arrUserInfo,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				$('#eRef').hide();				
				if(cg==1){
					$('#myModalReferenceCheckConfirm1').hide();
					$('#myModalReferenceCheck').show();
					getReferenceCheck(teacherDetails,teacherId);
				}else{	
					$('#jWTeacherStatusNotesDiv').show();
					jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
				}
				// Answer Save Successfully referencecheckspecificquestions.do
				//alert('enter');
				//window.location="referencecheckfinal.do";
				arr =[];
			 }
		  }
		});	
	} 
	else {
		$("#errordiv4question").html("&#149; Please provide responses to all the questions since they are required.<br>");
		return;
	}
}
$(function(){

$('.cancelERef').click(function(){
	$('#myModalReferenceCheckConfirm1').hide();
	$('#eRef').show();
});
});
/*************************************end ****************************************/

//js cgstatus_massNew.js


function applyScrollOnStatusNote_mass()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblStatusNote_mass').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 655,
        minWidth: null,
        minWidthAuto: false,
        colratio:[380,76,100,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function cgMassUpdate(clickFlag){
	$('#loadingDiv').show();
	var jobForTeacherIds_mass = new Array();
	jobForTeacherIds_mass	=	getSendMessageTeacherIds();
	//TeacherSendMessageAjax
	CGServiceAjax.getTeacherIds(jobForTeacherIds_mass,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				document.getElementById("jobForTeacherIds_mass").value=jobForTeacherIds_mass;
				document.getElementById("teacherIds_mass").value=data;
				
				var jobId = document.getElementById("jobId").value;
				
				document.getElementById("statusNotesTeacherIds_CGMass").value=data;
				document.getElementById("statusNotesJobId_CGMass").value=jobId;
				
				$('#myModalStatus_mass').modal('show');
				document.getElementById("status_l_title_mass").innerHTML=""+resourceJSON.MsgStatusLifeCycle+"";
				displayStatusDashboard_mass(clickFlag);
				finalizeOrNot(doActivity);
				
			}
			else
			{
				$('#loadingDiv').hide();
			}
		}
	});
}


function displayStatusDashboard_mass(clickFlag){
	var jobId = document.getElementById("jobId").value;
	MassStatusUpdateCGAjax.displayStatusDashboard_CGMass(jobId,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	$('#loadingDiv').hide();
			document.getElementById("divStatus_mass").innerHTML=data.toString();	
		}
	});
}

function hideStatus_mass()
{
	
	try { $('#myModalStatus_mass').modal('hide'); } catch (e) {}
	try { $('#myModalStatusInfo_mass').modal('hide'); } catch (e) {}
	try { $('#myModalEmail_mass').modal('hide'); } catch (e) {}
	try { $('#myModalPreCheckInfoCGMass').modal('hide'); } catch (e) {}
	try { $('#myModalConfirmInfoCGMass').modal('hide'); } catch (e) {}
	try { $('#myModalMsgShowSuccessCGMass').modal('hide'); } catch (e) {}
	
	var refreshGridCGMass = document.getElementById("refreshGridCGMass").value;
	if(refreshGridCGMass=='1')
	{
		document.getElementById("refreshGridCGMass").value="0";
		getCandidateGrid();
		refreshStatus();
	}
}

function showStatusDetailsOnClose(){
	try{
		$('#myModalStatusInfo_mass').modal('hide');
		document.getElementById("noteMainDiv_mass").style.display="none";
		$('#myModalStatus_mass').modal('show');
	}
	catch(err){}
}

function showStatusDetails_mass(statusName,statusId,secondaryStatusId)
{
	$('#schoolName_mass1').hide();
	var entityType=$('[name="entityType"]').val();
	document.getElementById("schoolAutoSuggestDivId_mass").style.display="none";
	$('#loadingDiv').show();
	$('#errorStatusNote_mass').empty();
	try{
	$('#divStatusNoteGrid_onlineActivity').remove();
	}catch(e){}
	
	document.getElementById("userActionCGMass").value=0;
	
	try {
		document.getElementById("showTemplateslist_mass").style.display="none";
	} catch (e) {}
	
	document.getElementById("noteMainDiv_mass").style.display="none";
	
	var teacherIds_mass=document.getElementById("teacherIds_mass").value;
	var jobId = document.getElementById("jobId").value;
	
	try { document.getElementById("statusId_mass").value=statusId; } catch (e) {}
	try { document.getElementById("secondaryStatusId_mass").value=secondaryStatusId; } catch (e) {}
	
	document.getElementById("dispalyStatusName_mass").value=statusName;
	var districtId=document.getElementById("districtId").value;
	MassStatusUpdateCGAjax.getStatusDetailsInfo_CGMass(teacherIds_mass,jobId,statusId,secondaryStatusId,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				$('#myModalStatus_mass').modal('hide');
				$('#loadingDiv').hide();
				document.getElementById("status_title_mass").innerHTML=statusName;
				document.getElementById("divStatusNoteGrid_mass").innerHTML=data[0];
				applyScrollOnStatusNote_mass();
				document.getElementById("checkOptions_mass").style.display="inline";
				
				var txtschoolCount=data[10];
				if(txtschoolCount > 1)
				{
					document.getElementById("txtschoolCount_mass").value=txtschoolCount;
					document.getElementById("schoolAutoSuggestDivId_mass").style.display="block";
				}
				else
				{
					document.getElementById("schoolAutoSuggestDivId_mass").style.display="none";
				}
				
				
				finalizeOrNot_mass(data[3]);
				
				if(statusName == "Withdrew" || statusName == "Declined"){
					$("#timeAndReason_mass").show();
				}
				
				if(data[6]==0 && (statusName=="Hired" || statusName=="Offer Ready"))
				{
					requisitionNumbers_mass();
				}
				else
				{
					document.getElementById("requisitionNumbers_mass").innerHTML="";
					document.getElementById("requisitionNumbersGrid_mass").style.display="none";
				}
				
				if(data[11]=='1')
				   document.getElementById('email_da_sa_mass').checked=true;
				else
					document.getElementById('email_da_sa_mass').checked=false;
				
				if(data[1]==1)
				{
					document.getElementById("myModalPreCheckInfoCGMassLabel").innerHTML=statusName;
					$('#myModalStatusInfo_mass').modal('hide');
					$('#myModalPreCheckInfoCGMass').modal('show');
					$('#myModalStatusInfo_mass').modal('hide');
					document.getElementById("errordivCGMass").innerHTML=data[2];
					
					if(data[12]==1)
						document.getElementById("mass_cgInfo_Continue").style.display="inline";
					else
						document.getElementById("mass_cgInfo_Continue").style.display="none";
					
				}
				else
				{
					//added by 08-04-2015
					if(statusName=="Online Activity"){
						CGServiceAjax.displayMultipleOnlineActivityForCG(teacherIds_mass,jobId,districtId,
								{ 
									async: false,
									errorHandler:handleError,
									callback:function(data)
									{	
								var tableDiv="<div id='divStatusNoteGrid_onlineActivity' style='width:650px;overflow:hidden;'>";
								var tableDivEnd="</div>";
									$('#divStatusNoteGrid_mass').before(tableDiv+data+tableDivEnd);
										applyMultiplyOnlineActivity(); 
									}
								});
						
					}
					$('#myModalStatusInfo_mass .row').css('width','100%');
					//ended by 08-04-2015					
					$('#myModalStatusInfo_mass').modal('show');	
				}
				//added by 12-06-2015
				//alert("$('#isRequiredSchoolIdCheck').val()===="+$('#isRequiredSchoolIdCheck').val());
				//alert("$('#isSchoolOnJobId').val()===="+$('#isSchoolOnJobId').val());
				if($('#isRequiredSchoolIdCheck').val()==1 && $('#isSchoolOnJobId').val()==1 && entityType== 2)
				{	
					document.getElementById("schoolAutoSuggestDivId_mass").style.display="block";
					$('#schoolName_mass').hide();
					$('#schoolName_mass1').show();
					$('#schoolName_mass1').prop('disabled', false);
					document.getElementById("schoolName_mass1").value="";
					document.getElementById("schoolId_mass").value="0";
					if($('#alreadyExistsSchoolInStatus').val()==1){						
						if(entityType== 2)
						{		
							document.getElementById("schoolName_mass1").value = $('#schoolNameValue').val().replace(/!!/g , " ");
							document.getElementById("schoolId_mass").value = $('#schoolIdValue').val();
							$('#schoolName_mass1').prop('disabled', true);
						}
					}
				}
				//ended by 12-06-2015
				
				$('textarea').jqte();
				
				
				//Only for Jeffco added Date 10-09-2015
				//alert($("[name='slcStartDateOnOrOff']").val()+"   "+$("#districtId").val()+"      "+statusName);
				try{
					try{
						$('#startDateDiv').remove();
					}catch(e){}
					if($("[name='slcStartDateOnOrOff']").val()=="ON" && $("#districtId").val()=='804800' && statusName=='Offer Made'){
						$('#schoolAutoSuggestDivId_mass').after('<div class="row mt5" id="startDateDiv"><div class="span12 top5 col-md-2 form-group" style="width:100%;">'+
								'<label for="text" style="float:left;margin-right:5px;padding-top:7px;">Start Date<span class="required">*</span></label>'+
								'<div><input type="text" value="" id="startDateId_mass" name="startDate_mass" class="form-control" maxlength="50" onmouseup="setDateField(this);" onkeydown="return keyNotWork(event);" oncontextmenu="return false;" style="width:150px;"></div>'+
								'</div></div>');
						//$('[name="startDate"]').val($('[name="isStartDate"]').val());
					}
					}catch(e){}
				//ENDed Only for Jeffco added Date 10-09-2015
					
					
			}
			//********************************added by 17-06-2015******************************************//
			try{
				//alert(statusName+"   "+entityType+"   districtId==="+$("#districtId").val());
				if(statusName=='Credential Review' && entityType==3 && $("#districtId").val()=='1201470'){
					var offerShowFlag="0";
					offerShowFlag=document.getElementById("doActivityForOfferReady").value;
					if(offerShowFlag=="0" || offerShowFlag==""){
						finalizeOrNoForOfferReady_mass(offerShowFlag);
					}
				}
			}catch(err){}
			//****************************************added by 17-06-2015************************************//
			setButton();
		}
	});
	
	
}

//********************************added by 17-06-2015******************************************//
function finalizeOrNoForOfferReady_mass(doActivity){
	document.getElementById("doActivityForOfferReady").value=doActivity;
	if(doActivity=='1'){
		document.getElementById("statusSave_mass").style.display="inline";
		document.getElementById("statusFinalize_mass").style.display="inline";
	}else{
		document.getElementById("statusSave_mass").style.display="none";
		document.getElementById("statusFinalize_mass").style.display="none";
	}
	document.getElementById('email_da_sa_mass').checked=false;
}
//********************************ended by 17-06-2015******************************************//

//added by 08-04-2015
function sendMultipleOnlineActivityLink(obj,questionSetId,teacherIds_mass)
{
	var jobId = document.getElementById("jobId").value;	
	MassStatusUpdateCGAjax.sendMultipleOnlineActivityLink(teacherIds_mass,jobId,questionSetId,{ 
 		async: true,
 		errorHandler:handleError,
 		callback:function(data)
 		{
		$(obj).html('Resend');		
 		}
     });
}

function applyMultiplyOnlineActivity()
{
	var $j=jQuery.noConflict();	
        $j(document).ready(function() {
        $j('#tblOnlineActivityMass').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 650,
        minWidth: null,
        minWidthAuto: false,
        colratio:[550,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        }); 
}
//ended by 08-04-2015

function showStatusInfo_CGMass()
{
	try{
		$('#myModalPreCheckInfoCGMass').modal('hide');
		$('#myModalStatusInfo_mass').modal('show');
	}
	catch(err){}
}

function openNoteDiv_mass(DistrictId){
	$('#statusNotes_mass').find(".jqte_editor").html("");
	ManageStatusAjax.statusWiseSectionList(DistrictId,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	if(data!=null){
				document.getElementById("templateDivStatus_mass").style.display="block";
				$('#statuswisesection_mass').html(data);
		}
		}
	});
	
	document.getElementById("noteMainDiv_mass").style.display="inline";
	document.getElementById("showStatusNoteFile_mass").innerHTML="";
	document.getElementById("showTemplateslist_mass").style.display="none";
	removeStatusNoteFile_mass();
	$('#errorStatusNote_mass').hide();
}

function getsectionwiselist_mass(value){
	document.getElementById("showTemplateslist_mass").style.display="none";
	$('#statusNotes_mass').find(".jqte_editor").html("");
	if(value!=0){
						 
		MassStatusUpdateCGAjax.getSectionWiseTemaplteslist_CGMass(value,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			
			document.getElementById("addTemplateslist_mass").innerHTML=data;
			document.getElementById("showTemplateslist_mass").style.display="block";
			}
		});
	}
}

function addStatusNoteFileType_mass()
{
	$('#statusNoteFileNames_mass').empty();
	$('#statusNoteFileNames_mass').html("<a href='javascript:void(0);' onclick='removeStatusNoteFile_mass();'><img src='images/can-icon.png' title='remove'/></a><br> <input id='statusNoteFileName_mass' name='statusNoteFileName_mass' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeStatusNoteFile_mass()
{
	$('#statusNoteFileNames_mass').empty();
	$('#statusNoteFileNames_mass').html("<a href='javascript:void(0);' onclick='addStatusNoteFileType_mass();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addStatusNoteFileType_mass();'>"+resourceJSON.AttachFile+"</a>");
}

function setTemplateByLIstId_mass(templateId){	
	$('#statusNotes_mass').find(".jqte_editor").html("");
	if(templateId!=0){
		ManageStatusAjax.getTemplateByLIst(templateId,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			$('#statusNotes_mass').find(".jqte_editor").html(data);
			}
		});
	}
}


function getStatusWiseEmailForAdmin_mass()
{
	var jobId = document.getElementById("jobId").value;
	var statusId=0,secondaryStatusId=0;
	try { statusId=document.getElementById("statusId_mass").value; } catch (e) {}
	try { secondaryStatusId=document.getElementById("secondaryStatusId_mass").value; } catch (e) {}
	
	try{ $('#myModalStatusInfo_mass').modal('hide'); }catch(e){}
	
	$('#loadingDiv').show();
	MassStatusUpdateCGAjax.getStatusWiseEmailForAdmin_CGMass(jobId,statusId,secondaryStatusId,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		    $('#loadingDiv').hide();
			if(data!=null)
			{
				$('#subjectLine_mass').val(data.subjectLine);
				$('#mailBody_mass').find(".jqte_editor").html(data.templateBody);
				try{  
					//$('#myModalStatusInfo_mass').modal('hide');
					document.getElementById("noteMainDiv_mass").style.display="none";
				}catch(e){}
				
				try{  
					$('#myModalEmail_mass').modal('show');
				}catch(e){}
			}
			else
			{
				$('#myModalStatusInfo_mass').modal('show');
				document.getElementById("email_da_sa_mass").disabled=true;
			}
		}
	});
}


function sendOriginalEmail_mass()
{
	$('#isEmailTemplateChanged_mass').val(0);
	emailClose_mass();
}

function emailClose_mass(){
	try{
		$('#myModalEmail_mass').modal('hide'); 
		$('#myModalStatusInfo_mass').modal('show');	
	}
	catch(err){}
}

function sendChangedEmail_mass()
{
	$('#isEmailTemplateChanged_mass').val(1);
	var msgSubject=$('#subjectLine_mass').val();
	var charCount	=	$('#mailBody_mass').find(".jqte_editor").text();
			
	$('#errordivEmail_mass').empty();
	$('#subjectLine_mass').css("background-color", "");
	$('#mailBody_mass').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	if(trim(msgSubject)=="")
	{
		$('#errordivEmail_mass').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
		if(focs==0)
			$('#subjectLine_mass').focus();
		$('#subjectLine_mass').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(charCount.length==0)
	{
		$('#errordivEmail_mass').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
		if(focs==0)
			$('#mailBody_mass').find(".jqte_editor").focus();
		$('#mailBody_mass').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
	{
		emailClose_mass();
		return false;
	
	}
	else
	{
		$('#errordivEmail_mass').show();
		return false;
	}
}

function finalizeOrNot_mass(doActivity){
	if(doActivity=='1'){
		document.getElementById("statusSave_mass").style.display="none";
		document.getElementById("statusFinalize_mass").style.display="inline";
	}else{
		document.getElementById("statusSave_mass").style.display="none";
		document.getElementById("statusFinalize_mass").style.display="none";
	}
	document.getElementById('email_da_sa').checked=false;
}

function preCheckClose_mass(){
	try{
		$('#myModalPreCheckInfoCGMass').modal('hide'); 
		$('#myModalStatus_mass').modal('show');	
	}
	catch(err){}
}

function requisitionNumbers_mass()
{	
	var schoolId;
	var jobId=document.getElementById("jobId").value;
	var locationurl=window.location.href;
	if(locationurl.indexOf("teacherinfo") > -1)
	{
		schoolId=document.getElementById("schoolId").value;
	}
	var reqNumber=document.getElementById("reqNumber").value;
	var offerReady=0;
	try{
		offerReady=document.getElementById("offerReady").value;
	}catch(err){}
	
	ManageStatusAjax.requisitionNumbers(jobId,schoolId,offerReady,reqNumber,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				if(data!=""){
					document.getElementById("requisitionNumbers_mass").innerHTML=data;
					document.getElementById("requisitionNumbersGrid_mass").style.display="inline";
					try{
						document.getElementById("requisitionNumber").style.display="inline";
					}catch(err){}
				}else{
					document.getElementById("requisitionNumbers_mass").innerHTML="";
					document.getElementById("requisitionNumbersGrid_mass").style.display="none";
				}
			}catch(err){}
		}
	});
}


function validateStatusInfo_cgmass_filename(filenameCGMass)
{
	//alert("filenameCGMass "+filenameCGMass);
	
	document.getElementById("statusnoteFileNameCGMassTemp").value=filenameCGMass;
	validateStatusInfo_cgmass(99);
}

//****************************************Confirm Box *******************************************//
function getConfirm(header,confirmMessage,allTeacherId,buttonNameChange,callback){
	var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>-->"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<button type='button' class='btn btn-default' id='confirmFalse'>Cancel</button>"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#myModalStatusInfo_mass').after(div);
    confirmMessage = confirmMessage || '';
    $('#myModalStatusInfo_mass').modal('hide');
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#confirmbox #confirmMessage').html(confirmMessage);
    $('#confirmbox #myModalLabel').html(header);
    $('#confirmbox #confirmTrue').html(buttonNameChange);
    if(allTeacherId=='') $('#confirmFalse').hide();
    $('#confirmFalse').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        minOneScoreSliderSetFlag=2;
        $('#myModalStatusInfo_mass').modal('show');
        if (callback) callback(false);
    });
    $('#confirmTrue').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        document.getElementById("teacherIdsOSCEOLA_mass").value=allTeacherId;
        minOneScoreSliderSetFlag=1;
        if (callback) callback(true);
    });
}
//***************************************ended by 17-06-2015************************************//

function validateStatusInfo_cgmass(actionFlag)
{
	//alert("validateStatusInfo_cgmass actionFlag "+actionFlag)
	
	
	$('#errorStatusNote_mass').empty();
	var actionFlagTemp=actionFlag;
	if(actionFlag!="99")
	{
		//alert("!99");
		document.getElementById("isFinalizeCGMass").value=actionFlag;
		document.getElementById("statusnoteFileNameCGMassTemp").value="";
	}
	else
	{
		//document.getElementById("statusnoteFileNameCGMassTemp").value="";
		//alert("99");
	}
	
	var userActionCGMass=document.getElementById("userActionCGMass").value;
	
	var teacherIds_mass="";
	if(minOneScoreSliderSetFlag==1)// For Offer made OSCEOLA(1201470)
	teacherIds_mass=document.getElementById("teacherIdsOSCEOLA_mass").value;
	else
	teacherIds_mass=document.getElementById("teacherIds_mass").value;
	
	var jobId = document.getElementById("jobId").value;
	
	//alert("teacherIds_mass "+teacherIds_mass+" jobId "+jobId)
	
	var statusId=0,secondaryStatusId=0;
	try { statusId=document.getElementById("statusId_mass").value; } catch (e) {}
	try { secondaryStatusId=document.getElementById("secondaryStatusId_mass").value; } catch (e) {}
	
	var bSentEmailToDASA=false;
	try { bSentEmailToDASA=document.getElementById('email_da_sa_mass').checked; } catch (e) {}
	
	var statusFlag=$("#statusNoteflag_CGMass").val()=="true"?true:false;
	var topSliderDisable_CGMass=trim(document.getElementById("topSliderDisable_CGMass").value);
	var countQuestionSlider_CGMass=trim(document.getElementById("countQuestionSlider_CGMass").value);
	
	try{
		var iframeNorm = document.getElementById('ifrmTopSliderCGMass');
		if(iframeNorm!=null)
		{
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			var inputNormScore = innerNorm.getElementById('topSliderCGMass');
			document.getElementById("scoreProvided_CGMass").value=inputNormScore.value;
		}
		else
		{
			document.getElementById("scoreProvided_CGMass").value="0";
		}
		
	}catch(e){}

	var scoreProvided_CGMass=trim(document.getElementById("scoreProvided_CGMass").value);

	var answerId_arrary = new Array();
	var answerScore_arrary = new Array();

	var notSetScore_NoOfQuestions=0;
	var sumAnswerScore=0;
	var sumAnswerScore=0;
	if(countQuestionSlider_CGMass>0)
	{
		for(var i=1;i<= countQuestionSlider_CGMass;i++)
		{
			var questionscore=0;
			var answerId=0;
			
			try{
				var ifrmQuestion = document.getElementById('ifrmQuestion_msu_'+i);
				if(ifrmQuestion!=null)
				{
					var innerQuestion = ifrmQuestion.contentDocument || ifrmQuestion.contentWindow.document;
					var inputQuestionFrm = innerQuestion.getElementById('questionFrm_msu');
					var answerIdFrm = innerQuestion.getElementById('answerId_msu');
					questionscore=inputQuestionFrm.value;
					answerId=answerIdFrm.value;
					
					sumAnswerScore=(parseInt(sumAnswerScore)+parseInt(questionscore));
					
					answerId_arrary[i-1]=answerId;
					answerScore_arrary[i-1]=questionscore;
				}
			}catch(e){}
			
			if(questionscore==0)
			{
				notSetScore_NoOfQuestions++;
			}

		}
	}
	
	var bNoteEditervisible=false;
	var bNoteText=false;
	var statusNotes_mass="";
	try{
		if(document.getElementById("noteMainDiv_mass").style.display=="inline")
		{
			bNoteEditervisible=true;
			statusNotes_mass=$('#statusNotes_mass').find(".jqte_editor").text().trim();
			if(statusNotes_mass!=null && statusNotes_mass!="")
				 bNoteText=true;
			else
				 bNoteText=false;
		}
		else
		{
			bNoteEditervisible=false;
		}
	}catch(err){}
	
	var bReadyToNoteAttach=false;
	var bNoteAttach=false;
	var statusNoteFileName_mass=null;
	try{  
		statusNoteFileName_mass=document.getElementById("statusNoteFileName_mass").value;
		if(statusNoteFileName_mass!=null)
			bReadyToNoteAttach=true;
		
		if(statusNoteFileName_mass!="")
			bNoteAttach=true;
			
	}catch(e){}
	
	var masterQuestionIds=null;
	try{  
		masterQuestionIds=document.getElementById("masterQuestionId").value;
	}catch(e){}
	
	
	var masterQuestionIds_arrary = new Array();
	masterQuestionIds_arrary = masterQuestionIds.split("#");
	var sQuestionIds_Note_arrary = new Array(masterQuestionIds_arrary.length);
	if(masterQuestionIds_arrary!=null && masterQuestionIds_arrary.length > 0)
	{
		for(var i=0;i<masterQuestionIds_arrary.length;i++)
		{
			var qID="questionNotes"+masterQuestionIds_arrary[i];
			var sQuestion_Notes_temp=$('#'+qID).find(".jqte_editor").html();
			sQuestionIds_Note_arrary[i]=sQuestion_Notes_temp;
		}
	}
	
	
	var isEmailTemplateChanged_CGMass=0;
	var msgSubject_CGMass	="";	
	var adminmailBody_CGMass=""; 
	
	try { isEmailTemplateChanged_CGMass=$('#isEmailTemplateChanged_mass').val(); } catch (e) {}
	if(isEmailTemplateChanged_CGMass==1)
	{
		try { msgSubject_CGMass=$('#subjectLine_mass').val(); } catch (e) {}
		try { adminmailBody_CGMass=$('#mailBody_mass').find(".jqte_editor").html(); } catch (e) {}
	}

	var cnt_mass=0;
	var focs_mass=0;	
	
	var bFinalize=false;
	var isFinalizeCGMass=document.getElementById("isFinalizeCGMass").value;
	if(isFinalizeCGMass=="1")
		bFinalize=true;
	
	var bTopSliderEnable=false;
	if(topSliderDisable_CGMass=="1")
		bTopSliderEnable=true;
	
	var bTopSlider_NotSet=false;
	if(bTopSliderEnable && scoreProvided_CGMass == 0)
		bTopSlider_NotSet=true;
	
	var bTopSlider_SetValue=false;
	if(bTopSliderEnable && scoreProvided_CGMass > 0)
		bTopSlider_SetValue=true;
	
	var bSlider=false;
	if(countQuestionSlider_CGMass >0)
		bSlider=true;
	
	var bAllSetSlider=false;
	if(bSlider && notSetScore_NoOfQuestions==0)
		bAllSetSlider=true;
	
	var bPartialSetSlider=false;
	if(bSlider && sumAnswerScore > 0)
		bPartialSetSlider=true;
	
	var bNoSetSlider=false;
	if(bSlider && sumAnswerScore == 0)
		bNoSetSlider=true;
	
	if((bTopSlider_NotSet || bNoSetSlider) && !bNoteText )
	{
		$('#errorStatusNote_mass').show();
		if(bTopSlider_NotSet)
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNote+".<br>");
		if(bNoSetSlider)
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNoteForAttribute+".<br>");
		
		cnt_mass=1;
	}
	else if(statusFlag && !bTopSlider_NotSet && !bTopSlider_SetValue && !bSlider && !bNoteText)
	{
		$('#errorStatusNote_mass').show();
		$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		cnt_mass=1;
	}
	/*else if(statusFlag && !bTopSlider_NotSet && bTopSlider_SetValue && !bSlider && !bNoteText)
	{
		$('#errorStatusNote_mass').show();
		$('#errorStatusNote_mass').append("&#149; Please enter Note.<br>");
		cnt_mass=1;
	}*/
	else if(bFinalize && bSlider && !bAllSetSlider && bPartialSetSlider)
	{
		$('#errorStatusNote_mass').show();
		$('#errorStatusNote_mass').append("&#149; "+resourceJSON.SetScoreForAllAttributeToFinalize+".<br>");
		cnt_mass=1;
	}
	
	if(bNoteText)
	{
		var charCount = statusNotes_mass.length;
		if(charCount > 4000)
		{
			$('#errorStatusNote_mass').show();
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
			cnt_mass=1;
		}
		else if(bReadyToNoteAttach && !bNoteAttach)
		{
			$('#errorStatusNote_mass').show();
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
			cnt_mass++;
		}
		else if(bReadyToNoteAttach && bNoteAttach)
		{
			var ext = statusNoteFileName_mass.substr(statusNoteFileName_mass.lastIndexOf('.') + 1).toLowerCase();
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("statusNoteFileName_mass").files[0]!=undefined)
				{
					fileSize = document.getElementById("statusNoteFileName_mass").files[0].size;
				}
			}
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
			{
				$('#errorStatusNote_mass').show();
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzSlctAcceptableNoteFormats+".<br>");
				cnt_mass++;
			}
			else if(fileSize>=10485760)
			{
				$('#errorStatusNote_mass').show();
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.msgfilesizelessthan+".<br>");
				cnt_mass++;
			}
		}
	}
	
	var dispalyStatusName_mass=document.getElementById("dispalyStatusName_mass").value;
	var bOfferReady=false;
	var bHired=false;
	if(dispalyStatusName_mass=="Offer Ready")
		bOfferReady=true;
	if(dispalyStatusName_mass=="Hired")
		bHired=true;
	
	var requisitionNumberText_mass="";
	var requisitionNumberID_mass=null;
	var bRequisitionNumberDivShow=false;
	var bRNunber=false;
	try{
		if(document.getElementById("requisitionNumbersGrid_mass").style.display!='none')
		{
			bRequisitionNumberDivShow=true;
			try{  
				requisitionNumberID_mass=document.getElementById("requisitionNumber").value;
				
				var x=document.getElementById("requisitionNumber").selectedIndex;
				var y=document.getElementById("requisitionNumber").options;
				
				requisitionNumberText_mass=y[x].text;
				
				if(requisitionNumberID_mass > 0)
					bRNunber=true;
			}catch(e){}
		}
	}catch(err){}
	
	var isReqNoForHiring=0;
	try{
		isReqNoForHiring=document.getElementById("isReqNoForHiring").value;
	}catch(err){}
	if(isReqNoForHiring==0){
		try{
			document.getElementById("reqmassstar").style.display="none";
		}catch(err){}
	}else{
		try{
			document.getElementById("reqmassstar").style.display="inline";
		}catch(err){}
	}
	var reqLength=0;
	try{
		reqLength = document.getElementById("requisitionNumber").length;
	}catch(err){}
	
	try{
		if(bOfferReady && requisitionNumberID_mass==null){
			$('#errorStatusNote_mass').show();
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgNoPositionNumberFound+".<br>");
			cnt=1;
		}
	}catch(e){}
	var txtschoolCount_mass=0;
	var schoolId=0;
	if(bFinalize)
	{
		try{
			if((bOfferReady || bHired) && isReqNoForHiring==1 ){
				if(bRequisitionNumberDivShow && !bRNunber){
					if(reqLength>1){
						$('#errorStatusNote_mass').show();
						$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzSlctPosition+".<br>");
					}else if(bOfferReady){
						$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgNoPositionFound+".<br>");
					}else{
						$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgPositionIsMandatory+".<br>");
					}
					cnt_mass++;
				}else if(bRequisitionNumberDivShow==false){
					cnt_mass++;
					$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgPositionIsMandatory+".<br>");
				}
			}
		}catch(err){}
		try{
			txtschoolCount_mass=document.getElementById("txtschoolCount_mass").value;
		}catch(err){}
		
		var bSchoolDivCheck_mass=false;
		try{
			if(document.getElementById("schoolAutoSuggestDivId_mass").style.display!="none")
				bSchoolDivCheck_mass=true;
		}catch(err){}
		
		if(txtschoolCount_mass > 1 && bSchoolDivCheck_mass)
		{
			schoolId=document.getElementById("schoolId").value;
			if((schoolId=='' || schoolId == 0) && bHired)
			{
				$('#errorStatusNote_mass').show();
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
				$('#schoolName').focus();
				cnt_mass++;
			}
		}
	}
	//alert('schoolId=============='+$('#schoolId_mass').val()+"actionFlag==="+actionFlag+" $(entity.val()==="+$('[name="entityType"]').val()+"  $('#isSchoolOnJobId').val()==="+$('#isSchoolOnJobId').val()+" $('#isRequiredSchoolIdCheck').val()==="+$('#isRequiredSchoolIdCheck').val());
	if($('#isSchoolOnJobId').val()==1 && $('#isRequiredSchoolIdCheck').val()==1 && actionFlag==1 && $('[name="entityType"]').val() == 2){
		schoolId=document.getElementById("schoolId_mass").value;
		//alert("schoolId========"+schoolId);
		if(schoolId=='' || schoolId == 0 )
		{
			$('#errorStatusNote_mass').show();
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
			$('#schoolName_mass1').focus();
			cnt_mass++;
		}
	}
	
	/*SWADESH*/
	var timingforDclWrdw='';
	var reasonforDclWrdw='';
	if(actionFlag==1){
	if(dispalyStatusName_mass  == "Withdrew" || dispalyStatusName_mass  == "Declined"){
		timingforDclWrdw = document.getElementById("timingforDclWrdw_mass").value;
		reasonforDclWrdw = document.getElementById("reasonforDclWrdw_mass").value;
		if(timingforDclWrdw == "0" || timingforDclWrdw == '' || reasonforDclWrdw == "0" || reasonforDclWrdw == ''){
			$('#errorStatusNote').show();
			if(timingforDclWrdw == "0" || timingforDclWrdw == '')
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzSelectHireddate+" 1.<br>");
			if(reasonforDclWrdw == "0" || reasonforDclWrdw == '')
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzSelectHireddate+" 2.<br>");
			return false;
		}
	}
	}
	
	//Only for Jeffco Start Date
	var startDate="";
	try{
	if($("[name='slcStartDateOnOrOff']").val()=="ON"){
	startDate=$('[name="startDate_mass"]').val();
	if(actionFlag==1 && $("#districtId").val()=='804800' && dispalyStatusName_mass=='Offer Made' && startDate==''){
		$('#errorStatusNote_mass').show();
		$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrSTDateMSG+".<br>");
		$('[name="startDate_mass"]').focus();
		cnt_mass++;
	}
	}
	}catch(e){}
	//Ended only for jeffco Start Date
	
	//*************************************for OSCEOLA(1201470) *********************************************************//
	var flagforNextStep=true;
	var districtIdOSCEOLA=1201470;
	//alert("dispalyStatusName_mass======================"+dispalyStatusName_mass);
	if(actionFlag==1 && cnt_mass==0 && $("#districtId").val()==districtIdOSCEOLA && dispalyStatusName_mass=='Offer Made' && minOneScoreSliderSetFlag==2 ){
		var resData="";
		MassStatusUpdateCGAjax.getOnlyCredentialReview(teacherIds_mass,jobId,"Credential Review",districtIdOSCEOLA,{
			async: false,
			errorHandler:handleError,
			callback:function(data){
			cnt_mass++;
			var confirmMessage="We found that the \"Credential Review\" node is not set for one or more candidate. Click on \"Ok\" button if you want to send Conditional Offer to that candidate who have \"Credential Review\" node finalized, otherwise click on \"Cancel\" button to wait for the change of \"Credential Review\" node of candidates.";
			if(data==""){
				confirmMessage="\"Credential Review\" node is not set in selected candidate list.";
			}
			getConfirm($('#myModalStatusInfo_mass .modal-dialog #myModalLabel #status_title_mass').html(),confirmMessage,data,'Ok',function(data){
			if(data){
				validateStatusInfo_cgmass(actionFlag);	
			}
			});	
			}
		});
	}
	if(minOneScoreSliderSetFlag == 1 && teacherIds_mass==""){
		minOneScoreSliderSetFlag=2;
		flagforNextStep=false;
		$('#myModalStatusInfo_mass').modal('hide');
		$('#myModalStatus_mass').modal('show');
	}
	
	//*************************************ended OSCEOLA(1201470) *********************************************************//
	
	if(cnt_mass==0 && flagforNextStep)
	{
		//showConfirmationDivForCG_mass();
		$('#myModalStatusInfo_mass').modal('hide');
		try {
			$('#loadingDiv').show();
		} catch (e) {}
		if(statusNoteFileName_mass!="" && statusNoteFileName_mass!=null && actionFlagTemp!="99")
		{
			//alert("try to submit");
			document.getElementById("frmStatusNote_mass").submit();
			
		}
		else
		{
			//alert("try to save statusNoteFileName_mass "+statusNoteFileName_mass);
			minOneScoreSliderSetFlag=2;
			statusNoteFileName_mass=document.getElementById("statusnoteFileNameCGMassTemp").value;
			MassStatusUpdateCGAjax.massPersistentData(userActionCGMass,bFinalize,teacherIds_mass,jobId,statusId,secondaryStatusId,bTopSliderEnable,scoreProvided_CGMass,bSlider,answerId_arrary,answerScore_arrary,masterQuestionIds_arrary,sQuestionIds_Note_arrary,statusNotes_mass,statusNoteFileName_mass,requisitionNumberID_mass,requisitionNumberText_mass,schoolId,bSentEmailToDASA,isEmailTemplateChanged_CGMass,msgSubject_CGMass,adminmailBody_CGMass,schoolId,startDate,timingforDclWrdw,reasonforDclWrdw,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				if(data!=null)
				{
					if(data[1]==1)
					{
						try {
							$('#loadingDiv').hide();
						} catch (e) {}
						
						//$('#myModalStatusInfo_mass').modal('hide');
						document.getElementById("mass_cgInfo_Override").style.display="inline";
						document.getElementById("mass_cgInfo_Skip").style.display="inline";
						$('#errordivConfirmCGMass').empty();
						document.getElementById("errordivConfirmCGMass").style.display="inline";
						$('#errordivConfirmCGMass').append(data[0]);
						$('#myModalConfirmInfoCGMass').modal('show');
					}
					else if(data[1]==0)
					{
						try {
							$('#loadingDiv').hide();
						} catch (e) {}
						try{
							var teachers = "", tempTeacher ="";
							if(typeof(teacherIds_mass) == 'string')
								tempTeacher=teacherIds_mass.split(",");
							else if(typeof(teacherIds_mass) == 'array'|| typeof(teacherIds_mass) == 'object')
								tempTeacher = teacherIds_mass;
							for(var i=0; i<tempTeacher.length ; i++){
								if(teachers == "")
									teachers = tempTeacher[i];
								else
									teachers+="##"+tempTeacher[i];
							}
							candidateNotReviewedUtil.setNotReviewedFlag(teachers,"ApplyGlobalTag3");
						}catch(err) {	
							console.log("Exception in MassStatusUpdateCGAjax.massPersistentData:1124  "+err.message);
						}
						
						//$('#myModalStatusInfo_mass').modal('hide'); 
						$('#myModalMsgShowSuccessCGMass').modal('show');
						if(data[2]=="1"){
							$('#message2showConfirm').html("Would you like to invite applicants to an event? ");
							$('#footerbtn').html("<button class='btn btn-primary' onclick=\"redirectToEventPage("+jobId+",'"+teacherIds_mass+"')\" >Yes</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>No</button>");
							$('#myModal3').modal('show');	
						}				
					}
				}
			}
			});
		
		}
		
	}

}


function statusDetailsClose_mass()
{
	try{ $('#myModalConfirmInfoCGMass').modal('hide'); } catch(err){}
	try{ $('#myModalStatusInfo_mass').modal('show');}catch(err){}
}

function showConfirmationDivForCG_mass()
{
	try{
		$('#myModalStatusInfo_mass').modal('hide'); 
		$('#myModalConfirmInfoCGMass').modal('show');	
	}
	catch(err){}
	
	$('#mass_cgInfo_Override').modal('show');
	$('#mass_cgInfo_Skip').modal('show');
	
}

function successClose_mass()
{
	try{
		$('#myModalMsgShowSuccessCGMass').modal('hide');
		$('#myModalStatus_mass').modal('show');
	}
	catch(err){}
	document.getElementById("refreshGridCGMass").value="1";
}

function userConfirmationCGMss(userInput)
{
	try{
		$('#myModalConfirmInfoCGMass').modal('hide');
	}
	catch(err){}
	
	document.getElementById("userActionCGMass").value=userInput;
	var isFinalizeCGMass=document.getElementById("isFinalizeCGMass").value;
	validateStatusInfo_cgmass(isFinalizeCGMass);
}

function setButton(){
	var count=0;
	 $('#jWTeacherStatusNotesDiv').is(':visible') 
	 {
		 $('.jqte').each(function(){
			  $(this).is(':visible') 
			  count++;
			  if($(this).find("textarea").attr("name")!='statusNotes' && $(this).find("textarea").attr("name")!=''){
				  $(this).find(".jqte_toolbar").append('<a href="#" class="pull-right plus-icons" id="newIcon'+count+'" onClick="shapeChange(this,'+count+');"><i class="fa fa-plus-square"></i></a>');
				  $(this).find(".jqte_editor").css("min-height", "30px");
				  $(this).find(".jqte_editor").css("height", "30px");
			  }
		  });
		 	$('[name="statusNotes"]').parents().parents('.jqte').find(".jqte_editor").css("min-height", "30px");
			$('[name="statusNotes"]').parents().parents('.jqte').find(".jqte_editor").css("height", "30px");
			$('[name=""]').parents().parents('.jqte').find(".jqte_editor").css("min-height", "30px");
			$('[name=""]').parents().parents('.jqte').find(".jqte_editor").css("height", "30px");
	}
}
$(document).ready(function(){
	//$('[name="statusNotes"]').parents().parents('.jqte').find(".jqte_toolbar").append('<a href="#" class="pull-right plus-icons" id="newIconAddNotesLast" onClick="shapeChange(this,\'AddNotesLast\');"><i class="fa fa-plus-square"></i></a>');
});

function shapeChange(obj,count){
	var id=count;
	var p=$('#newIcon'+id).parents().parents('.jqte');
	if(p.find('.jqte_editor').height()==100)
	{
		p.find(".jqte_editor").css("min-height", "30px");
		p.find(".jqte_editor").css("height", "30px");
	}
	else if(p.find('.jqte_editor').height()==10)
	{
		p.find('.jqte_editor').css("mix-height", "900px");
		p.find('.jqte_editor').css("height", "120px");
	}
}



//js schoolAutoSuggestForDJO.js
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";

function getSchoolAuto_djo(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey_djo(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey_djo(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray_djo(txtSearch.value);
		fatchData_djo(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray_djo(schoolName){
	var searchArray = new Array();
	var jobOrderId	=	document.getElementById("jobId").value;
	ManageStatusAjax.getSchoolInJobOrder(jobOrderId,schoolName,{
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++)
			{
				var location ="";
				if(data[i].schoolId.locationCode!=null && data[i].schoolId.locationCode!="") 
				{	
					location = " ("+data[i].schoolId.locationCode+")";
				}
				
				searchArray[i]=data[i].schoolId.schoolName+location;
				showDataArray[i]=data[i].schoolId.schoolName+location;
				hiddenDataArray[i]=data[i].schoolId.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}
function getSchoolAuto_Hired(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey_djo(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey_djo(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray_Hired(txtSearch.value);
		fatchData_djo(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray_Hired(schoolName){
	var searchArray = new Array();
	var jobOrderId	=	document.getElementById("jobId").value;
	var statusTeacherId=document.getElementById("statusTeacherId").value;
	//alert('statusTeacherId'+statusTeacherId);
	ManageStatusAjax.getSchoolInJobOrderForHired(jobOrderId,schoolName,statusTeacherId,{
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++)
			{
				var location ="";
				if(data[i].schoolId.locationCode!=null && data[i].schoolId.locationCode!="") 
				{	
					location = " ("+data[i].schoolId.locationCode+")";
				}
				
				searchArray[i]=data[i].schoolId.schoolName+location;
				showDataArray[i]=data[i].schoolId.schoolName+location;
				hiddenDataArray[i]=data[i].schoolId.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}



selectFirst="";

var fatchData_djo= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv_djo(dis,hiddenId,divId)
{
	try{document.getElementById("schoolId").value="";}catch (err){}
    try{document.getElementById("schoolIdCG").value="";}catch (err){}

	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			 try{document.getElementById("schoolId").value=hiddenDataArray[index];}catch (err){}
             try{document.getElementById("schoolIdCG").value=hiddenDataArray[index];}catch (err){}
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey_djo = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey_djo = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function mouseOverChk_djo(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent_djo);
	}
}


function fireMouseOverEvent_djo(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

//js cgTranscriptOrCertificationsNew.js

/*========= Paging and Sorting ===========*/
function defaultSet(){
	pageForTC = 1;
	noOfRowsForTC = 100;
	sortOrderStrForTC="";
	sortOrderTypeForTC="";
	currentPageFlag="";
}
var txtBgColor="#F5E7E1";
var chkCallFrom=0;
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}


function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	if (decNum.length > 1)//here is the key u can just change from 2 to 3,45 etc to restict no of digits aftre decimal
	{
	//alert("Invalid more than 2 digits after decimal")
		//count=false;
	}
	//alert(count);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
    
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function getTranscriptDetail(tId)
{	
	defaultSet();
	currentPageFlag='tran';
	teacherId=tId;
	getTranscriptGrid();
}

function getTranscriptGrid()
{
	//CandidateReportAjax
	CGServiceAjax.getTranscriptGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{		
		$('#myModalTranscript').modal('show');
		document.getElementById("divTranscript").innerHTML=data;
		applyScrollOnTranscript();
	}});
}
function getTranscriptDetailNew(tId)
{	
	defaultSet();
	currentPageFlag='tran';
	teacherId=tId;
	getTranscriptGridNew();
}
function getTranscriptGridNew()
{
	CandidateGridAjax.getTranscriptGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{		
		$('#myModalTranscript').modal('show');
		document.getElementById("divTranscript").innerHTML=data;
		applyScrollOnTranscript();
	}});
}
function getCertificationDetail(tId)
{	
	defaultSet();
	currentPageFlag='cert';
	teacherId=tId;
	getCertificationGrid();
	
}

function getCertificationGrid()
{
	//CandidateReportAjax
	CGServiceAjax.getCertificationGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{		
		$('#myModalCertification').modal('show');
		document.getElementById("divCertification").innerHTML=data;
		applyScrollOnCertification();
	}});
}
function getCertificationDetailNew(tId)
{	
	defaultSet();
	currentPageFlag='cert';
	teacherId=tId;
	getCertificationGridNew();
	
}
function getCertificationGridNew()
{
	CandidateGridAjax.getCertificationGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{		
		$('#myModalCertification').modal('show');
		document.getElementById("divCertification").innerHTML=data;
		applyScrollOnCertification();
	}});
}
function sendToOtherPager()
{
	
	$('#loadingDiv').hide();
	if(chkCallFrom==1)
	{
		window.location.href="certification.do";
	}
}

function downloadTranscript(academicId,linkid)
{	
	$("#myModalLabelText").html(resourceJSON.MsgTranscript);
	//CandidateReportAjax
	CGServiceAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
		if(deviceType)
		{
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
			    if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 $('#modalDownloadsTranscript').modal('hide');
					 document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkid).href = data;	
				}	
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#modalDownloadsTranscript').modal('hide');
			 document.getElementById('ifrmTrans').src = ""+data+"";
		}
		else
		{
			try{
				document.getElementById('ifrmTransCommon').src = ""+data+"";
				$('#modalDownloadsCommon').modal('show');
			}catch(err)
			  {}	
		}		
	   }
	});
}
function downloadTranscriptNew(academicId)
{	
	CandidateGridAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{		
			if (data.indexOf(".doc") !=-1) {				
			    $('#modalDownloadsTranscript').modal('hide');
			    document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById('ifrmTrans').src = ""+data+"";
				$("#myModalTranscript").css({"z-index":"1000"});
				try{
					$('#modalDownloadsTranscript').modal('show');
				}catch(err)
				  {}
			}
		}
	});
}
function setZIndexTrans(){
	document.getElementById('ifrmTrans').src = "a.html";
	$("#myModalTranscript").css({"z-index":"2000"});	
	$("#myModalCertification").css({"z-index":"2000"});	
}


function chkForEnter(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate();
	}	
}

//js certtypeautocomplete.js
function getAllCertificateTypeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getAllCertificateTypeArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		//alert(divid);
		document.getElementById(divid).style.display='none';
	}
}

function getAllCertificateTypeArray(certType){
	
	var searchArray = new Array();	
	DWRAutoComplete.getAllCertificateTypeList(certType,{ 
		async: false,
		callback: function(data){
		//alert(data);
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
		}
	}
	});	

	return searchArray;
}
function hideAllCertificateTypeDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			//setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.MsgEtrValidCertificate+"<br>");
			if(focs==0)
				$('#certName').focus();
			
			$('#certName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		else
		{
			$('#errordiv').empty();	
			//setDefColortoErrorMsg();
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function getFilterCertificateTypeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getFilterCertificateTypeArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFilterCertificateTypeArray(certType){
	
	var stateId	=0;
	try{
		stateId=document.getElementById("stateId").value;
	}catch(e){}
	var searchArray = new Array();	
	CGServiceAjax.getFilterCertificateTypeList(certType,stateId,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
		}
	}
	});	

	return searchArray;
}


/**************** Start :: For candidate List **************/
function getFilterCertificateTypeAutoCompForCan(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getFilterCertificateTypeArrayForCan(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFilterCertificateTypeArrayForCan(certType){
	
	var stateId	=0;
	try{
		stateId=document.getElementById("stateIdForcandidate").value;
	}catch(e){}
	var searchArray = new Array();	
	CGServiceAjax.getFilterCertificateTypeList(certType,stateId,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
		}
	}
	});	

	return searchArray;
}
/**************** End :: For candidate List ****************/






function hideFilterCertificateTypeDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.MsgEtrValidCertificate+"<br>");
			if(focs==0)
				$('#certName').focus();
			
			$('#certName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		else
		{
			$('#errordiv').empty();	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

// cginterviewques.js
//jobId,teacherId,i4QuesNo
function displayIIQuesForRed(jobId,teacherId)
{
	var iiFlag = "red";
	CGInviteInterviewAjax.displayInviteInterviewQues(jobId,teacherId,iiFlag,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesRed').modal('show');
			$('#interviewQuesRedBody').html(data);
			
			if($("#quesSize").val()=="0")
				$("#inviteOK").hide();
			else
				$("#inviteOK").show();
		}
	});
	
}

function displayIIQuesForGray(jobId,teacherId)
{
	var iiFlag = "grey";
	
	CGInviteInterviewAjax.displayInviteInterviewQues(jobId,teacherId,iiFlag,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesGrey').modal('show');
			$('#interviewQuesGreyBody').html(data);
			
			if($("#quesSize").val()=="0")
				$("#reInviteOK").hide();
			else
				$("#reInviteOK").show();
		}
	});
	
}

function displayIIQuesForQrange(jobId,teacherId)
{
	
	var iiFlag = "orange";
	
	CGInviteInterviewAjax.displayInviteInterviewQues(jobId,teacherId,iiFlag,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesOrange').modal('show');
			$('#interviewQuesOrangeBody').html(data);
		}
	});
}

function saveIIStatus()
{
	 var teacherIdForII = $("#teacherIdForII").val();
	 var jobIdForII		= $("#jobIdForII").val();
	 var quesSetId		= $("#quesSetId").val();
	
	 CGInviteInterviewAjax.saveIIStatus(jobIdForII,teacherIdForII,quesSetId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesRed').modal('hide');
			getCandidateGrid();
			refreshStatus();
		}
	});
}

function saveIIStatusForGrey()
{
	 var teacherIdForII = $("#teacherIdForII").val();
	 var jobIdForII		= $("#jobIdForII").val();
	 var quesSetId		= $("#quesSetId").val();
	
	 CGInviteInterviewAjax.saveIIStatus(jobIdForII,teacherIdForII,quesSetId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#interviewQuesGrey').modal('hide');
			getCandidateGrid();
			refreshStatus();
		}
	});
}


function openInterViewLink(jobId,teacherId,linkId)
{
	
	//alert(" jobId "+jobId+" teacherId "+teacherId+" linkId "+linkId);
	
	CGInviteInterviewAjax.openInterviewURL(jobId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#videoInterViewLinkDiv').modal('show');
			document.getElementById("interViewDiv").innerHTML=data[0];
			
			//alert(data[1]);
			
			if(data[1]=='true')
				$("#saveVVIId").show();
			else if(data[1]=='false')
				$("#saveVVIId").hide();
		}
	});
}

function saveInterviewValue()
{
	var iframeNorm = document.getElementById('ifrmForVideoII');
	var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
	var inputNormScore = innerNorm.getElementById('normScoreFrm');
	document.getElementById("ifrmForVideoII").value=inputNormScore.value;
	
	var inviteInterviewID = $("#inviteInterviewID").val();
	
	CGInviteInterviewAjax.saveInterviewValue(inviteInterviewID,inputNormScore.value,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#videoInterViewLinkDiv').modal('hide');
		}
	});
}

function getVideoURL(vviUrl)
{
	$('#loadingDiv').show();
	CGInviteInterviewAjax.getPathOfVideoUrl(vviUrl,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null && data!=""){
				$('#videoInterViewLinkDiv').modal('show');
				document.getElementById("ifrmTrans").src=data;
				$('#loadingDiv').hide();
			}
		}
	});
}

