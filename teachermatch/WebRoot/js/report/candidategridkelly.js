function getPaging(pageno)
{
	if($("#gridNo").val()==2)
	{
		if(pageno!='')
		{
			pageforSchool=pageno;	
		}
		else
		{
			pageforSchool=1;
		}
		
		
		noOfRowsSchool = document.getElementById("pageSize1").value;
		showSchoolList(schoolIdsStore);
		return;
	}	
	
	if(currentPageFlag=="statusNote"){
		if(pageno!='')
		{
			pageSNote=pageno;	
		}
		else
		{
			pageSNote=1;
		}
		noOfRowsSNote = document.getElementById("pageSize3").value;
		var statusId=document.getElementById("statusIdForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		var fitScore=document.getElementById("fitScoreForStatusNote").value;
		jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
	}else if(currentPageFlag=="job"){
		if(pageno!='')
		{
			pageJob=pageno;	
		}
		else
		{
			pageJob=1;
		}
		noOfRowsForJob = document.getElementById("pageSize1").value;
		getJobOrderList();
	}
	else if(currentPageFlag=="workExp"){
		if(pageno!='')
		{
			pageworkExp=pageno;	
		}
		else
		{
			pageworkExp=1;
		}
		noOfRowsworkExp = document.getElementById("pageSize11").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="References"){
		if(pageno!='')
		{
			pageReferences=pageno;	
		}
		else
		{
			pageReferences=1;
		}
		noOfRowsReferences = document.getElementById("pageSize12").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid)
		
	}
	else if(currentPageFlag=="videoLink"){
		if(pageno!='')
		{
			pagevideoLink=pageno;
		}
		else
		{
			pagevideoLink=1;
		}
		noOfRowsvideoLink = document.getElementById("pageSize13").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!='')
		{
			pageaddDoc=pageno;
		}
		else
		{
			pageaddDoc=1;
		}
		noOfRowsaddDoc = document.getElementById("pageSize15").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		if(pageno!='')
		{
			pageLang=pageno;
		}
		else
		{
			pageLang=1;
		}
		noOfRowsLang = document.getElementById("pageLangProf").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="tpvh"){
		if(pageno!='')
		{
			pagetpvh=pageno;
		}
		else
		{
			pagetpvh=1;
		}
		noOfRowstpvh = document.getElementById("pageSize14").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherAce"){
		if(pageno!='')
		{
			pageteacherAce=pageno;
		}
		else
		{
			pageteacherAce=1;
		}
		noOfRowsteacherAce = document.getElementById("pageSize10").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherCerti"){
		if(pageno!='')
		{
			pageteacherCerti=pageno;
		}
		else
		{
			pageteacherCerti=1;
		}
		noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="us"){
		if(pageno!='')
		{
			pageUS=pageno;	
		}
		else
		{
			pageUS=1;
		}
		noOfRowsUS = document.getElementById("pageSize3").value;
		
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			var jftTeacherIdF=document.getElementById("teacherIdFromSharePoPUp").value;
			var jftTxtFlagF=document.getElementById("txtteacherIdShareflagpopover").value;
			displayUsergrid(jftTeacherIdF,jftTxtFlagF);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			var jftTeacherId=document.getElementById("JFTteachetIdFromSharePoPUp").value;
			var jftTxtFlag=document.getElementById("txtflagSharepopover").value;
			displayShareFolder(jftTeacherId,jftTxtFlag);
		}
	}else if(currentPageFlag=="stt"){
		if(pageno!='')
		{
			pageSTT=pageno;	
		}
		else
		{
			pageSTT=1;
		}
		noOfRowsSTT = document.getElementById("pageSize3").value;
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		var pageFlag= document.getElementById("pageFlag").value;
		if(pageFlag==1)
		{
			folderId = document.getElementById("frame_folderId").value;
		}	
		
		displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag);
	}else if(currentPageFlag=="phone"){
		if(pageno!='')
		{
			pagePhone=pageno;	
		}
		else
		{
			pagePhone=1;
		}
		noOfRowsPhone = document.getElementById("pageSize3").value;
		var teacherIdp = document.getElementById("teacherDetailId").value;
		jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
		getPhoneDetail(teacherIdp,jobForTeacherGId,0);
	}else if(currentPageFlag=="SS"){
		if(pageno!='')
		{
			pageSS=pageno;	
		}
		else
		{
			pageSS=1;
		}
		noOfRowsSS = document.getElementById("pageSize3").value;
		var teacherIdp = document.getElementById("teacherIdForSchoolSelection").value;
		var jobIdp=document.getElementById("jobId").value;
		getSelectedSchoolsList(jobIdp,teacherIdp);
	}else if(currentPageFlag=="msg"){
		if(pageno!='')
		{
			pageMsg=pageno;	
		}
		else
		{
			pageMsg=1;
		}
		noOfRowsMsg = document.getElementById("pageSize3").value;
		var teacherIdp = document.getElementById("teacherDetailId").value;
		var jobIdp=document.getElementById("jobId").value;
		var emailIdp=document.getElementById("emailId").value;
		getMessageDiv(teacherIdp,emailIdp,jobIdp,0);
	}else if(currentPageFlag=="referenceCheck"){
		if(pageno!='')
		{
			pageRefChk=pageno;	
		}
		else
		{
			pageRefChk=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
		teacherDetails	= document.getElementById("teacherDetailsForReference").value;
		noOfRowsRefChk 	= document.getElementById("pageSize3").value;
		getReferenceCheck(teacherDetails,teacherId);
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!='')
		{
			pageASMT=pageno;	
		}
		else
		{
			pageASMT=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
	//	teacherDetails	= document.getElementById("teacherDetailsForReference").value;
		noOfRowsASMT 	= document.getElementById("pageSize3").value;
		getdistrictAssessmetGrid_DivProfile(teacherId);
	}  
	else if(currentPageFlag=="teacherInvolve"){
		if(pageno!='')
		{
			dp_Involvement_page=pageno;	
		}
		else
		{
			dp_Involvement_page=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
		dp_Involvement_Rows 	= document.getElementById("pageSize16").value;
		getInvolvementGrid(teacherId);
		//getdistrictAssessmetGrid_DivProfile(teacherId);
	}else if(currentPageFlag=="jobApplyFromCG"){
		if(pageno!='')
		{
			pageJobDetail =pageno;	
		}
		else
		{
			pageJobDetail =1;
		}
		noOfRowsJobDetail 	= document.getElementById("pageSize12").value;
		getJobDetailListFromCG();
	} 
	else {

		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
	
		noOfRows = document.getElementById("pageSize3").value;
		teacherId = document.getElementById("teacherId").value;
		getNotesDiv(teacherId);
	}
	
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp){
	if($("#gridNo").val()==2)
	{
		if(pageno!=''){
			pageforSchool=pageno;	
		}else{
			pageforSchool=1;
		}
		sortOrderStrSchool	=	sortOrder;
		sortOrderTypeSchool	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRowsSchool = document.getElementById("pageSize1").value;
		}else{
			noOfRowsSchool=50;
		}
		showSchoolList(schoolIdsStore);
		return;
	}	
	
	if(currentPageFlag=="tran" || currentPageFlag=="cert"){
		if(pageno!=''){
			pageForTC=pageno;	
		}else{
			pageForTC=1;
		}
		sortOrderStrForTC	=	sortOrder;
		sortOrderTypeForTC	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsTranCert = document.getElementById("pageSize3").value;
		}else{
			noOfRowsTranCert=100;
		}
		if(currentPageFlag=="tran"){
			getTranscriptGridNew();
		}else{
			getCertificationGridNew();
		}
	}else if(currentPageFlag=="SS"){
		if(pageno!=''){
			pageSS=pageno;	
		}else{
			pageSS=1;
		}
		sortOrderStrSS	=	sortOrder;
		sortOrderTypeSS	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsSS = document.getElementById("pageSize3").value;
		}else{
			noOfRowsSS=10;
		}
		var teacherIdp = document.getElementById("teacherIdForSchoolSelection").value;
		var jobIdp=document.getElementById("jobId").value;
		getSelectedSchoolsList(jobIdp,teacherIdp);
	}else if(currentPageFlag=="msg"){
		if(pageno!=''){
			pageMsg=pageno;	
		}else{
			pageMsg=1;
		}
		sortOrderStrMsg	=	sortOrder;
		sortOrderTypeMsg	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsMsg = document.getElementById("pageSize3").value;
		}else{
			noOfRowsMsg=10;
		}
		var teacherIdp = document.getElementById("teacherDetailId").value;
		var jobIdp=document.getElementById("jobId").value;
		var emailIdp=document.getElementById("emailId").value;
		getMessageDiv(teacherIdp,emailIdp,jobIdp,0);
	}else if(currentPageFlag=="phone"){
		if(pageno!=''){
			pagePhone=pageno;	
		}else{
			pagePhone=1;
		}
		sortOrderStrPhone	=	sortOrder;
		sortOrderTypePhone	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsPhone = document.getElementById("pageSize3").value;
		}else{
			noOfRowsPhone=10;
		}
		var teacherIdp = document.getElementById("teacherDetailId").value;
		jobForTeacherGId=document.getElementById("jobForTeacherGId").value;
		getPhoneDetail(teacherIdp,jobForTeacherGId,0);
	}else if(currentPageFlag=="stt"){
		if(pageno!=''){
			pageSTT=pageno;	
		}else{
			pageSTT=1;
		}
		sortOrderStrSTT	=	sortOrder;
		sortOrderTypeSTT	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsSTT = document.getElementById("pageSize3").value;
		}else{
			noOfRowsSTT=10;
		}
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		var pageFlag= document.getElementById("pageFlag").value;
		if(pageFlag==1)
		{
			folderId = document.getElementById("frame_folderId").value;
		}	
		
		//alert(" folderId "+folderId+" pageFlag "+pageFlag);
		displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag);
	}else if(currentPageFlag=="us"){
		if(pageno!=''){
			pageUS=pageno;	
		}else{
			pageUS=1;
		}
		sortOrderStrUS	=	sortOrder;
		sortOrderTypeUS	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsUS = document.getElementById("pageSize3").value;
		}else{
			noOfRowsUS=10;
		}
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			var jftTeacherIdF=document.getElementById("teacherIdFromSharePoPUp").value;
			var jftTxtFlagF=document.getElementById("txtteacherIdShareflagpopover").value;
			displayUsergrid(jftTeacherIdF,jftTxtFlagF);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			var jftTeacherId=document.getElementById("JFTteachetIdFromSharePoPUp").value;
			var jftTxtFlag=document.getElementById("txtflagSharepopover").value;
			displayShareFolder(jftTeacherId,jftTxtFlag);
		}
	}else if(currentPageFlag=="job"){
		if(pageno!=''){
			pageJob=pageno;	
		}else{
			pageJob=1;
		}
		sortOrderStrJob	=	sortOrder;
		sortOrderTypeJob	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRowsJob = document.getElementById("pageSize1").value;
		}else{
			noOfRowsJob=10;
		}
		
		getJobOrderList();
	}else if(currentPageFlag=="statusNote"){
		if(pageno!=''){
			pageSNote=pageno;	
		}else{
			pageSNote=1;
		}
		sortOrderStrSNote=	sortOrder;
		sortOrderTypeSNote	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsJobSNote = document.getElementById("pageSize3").value;
		}else{
			noOfRowsJobSNote=10;
		}
		var statusId=document.getElementById("statusIdForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		var fitScore=document.getElementById("fitScoreForStatusNote").value;
		jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);

	}else if(currentPageFlag=="workExp"){
		if(pageno!=''){
			pageworkExp=pageno;	
		}else{
			pageworkExp=1;
		}
		sortOrderStrworkExp	=	sortOrder;
		sortOrderTypeworkExp	=	sortOrderTyp;
		if(document.getElementById("pageSize11")!=null){
			noOfRowsworkExp = document.getElementById("pageSize11").value;
		}else{
			noOfRowsworkExp=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="References"){
		if(pageno!=''){
			pageReferences=pageno;	
		}else{
			pageReferences=1;
		}
		sortOrderStrReferences	=	sortOrder;
		sortOrderTypeReferences	=	sortOrderTyp;
		if(document.getElementById("pageSize12")!=null){
			noOfRowsReferences = document.getElementById("pageSize12").value;
		}else{
			noOfRowsReferences=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="videoLink"){
		if(pageno!=''){
			pagevideoLink=pageno;	
		}else{
			pagevideoLink=1;
		}
		sortOrderStrvideoLink	=	sortOrder;
		sortOrderTypevideoLink	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsvideoLink = document.getElementById("pageSize13").value;
		}else{
			noOfRowsvideoLink=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!=''){
			pageaddDoc=pageno;	
		}else{
			pageaddDoc=1;
		}
		sortOrderStraddDoc	=	sortOrder;
		sortOrderTypeaddDoc	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsaddDoc = document.getElementById("pageSize15").value;
		}else{
			noOfRowsaddDoc=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		if(pageno!=''){
			pageLang=pageno;	
		}else{
			pageLang=1;
		}
		sortOrderStrLang	=	sortOrder;
		sortOrderTypeLang	=	sortOrderTyp;
		if(document.getElementById("pageLangProf")!=null){
			noOfRowsLang = document.getElementById("pageLangProf").value;
		}else{
			noOfRowsLang=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="tpvh"){
		if(pageno!=''){
			pagetpvh=pageno;	
		}else{
			pagetpvh=1;
		}
		sortOrderStrtpvh	=	sortOrder;
		sortOrderTypetpvh	=	sortOrderTyp;
		if(document.getElementById("pageSize14")!=null){
			noOfRowstpvh = document.getElementById("pageSize14").value;
		}else{
			noOfRowstpvh=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherAce"){
		if(pageno!=''){
			pageteacherAce=pageno;	
		}else{
			pageteacherAce=1;
		}
		sortOrderStrteacherAce	=	sortOrder;
		sortOrderTypeteacherAce	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherAce = document.getElementById("pageSize10").value;
		}else{
			noOfRowsteacherAce=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherCerti"){
		if(pageno!=''){
			pageteacherCerti=pageno;	
		}else{
			pageteacherCerti=1;
		}
		sortOrderStrteacherCerti	=	sortOrder;
		sortOrderTypeteacherCerti	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		}else{
			noOfRowsteacherCerti=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!=''){
			pageASMT=pageno;	
		}else{
			pageASMT=1;
		}
		sortOrderStrASMT	=	sortOrder;
		sortOrderTypeASMT	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsASMT = document.getElementById("pageSize3").value;
		}else{
			noOfRowsASMT=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherInvolve"){
		if(pageno!=''){
			dp_Involvement_page=pageno;	
		}else{
			dp_Involvement_page=1;
		}
		dp_Involvement_sortOrderStr	=	sortOrder;
		dp_Involvement_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize16")!=null){
			dp_Involvement_Rows = document.getElementById("pageSize16").value;
		}else{
			dp_Involvement_Rows=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getInvolvementGrid(teacherIdForprofileGrid);
		//getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="jobApplyFromCG"){
		if(pageno!=''){
			pageJobDetail =pageno;	
		}else{
			pageJobDetail =1;
		}
		sortOrderStrJobDetail	=	sortOrder;
		sortOrderTypeJobDetail 	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsJobDetail = document.getElementById("pageSize12").value;
		}else{
			noOfRowsJobDetail=50;
		}
		getJobDetailListFromCG();
	}
	else if(currentPageFlag=="teacherAssessment"){
		if(pageno!=''){
			pageEJ=pageno;	
		}else{
			pageEJ=1;
		}
		sortOrderStrEJ=sortOrder;
		sortOrderTypeEJ=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRowsEJ = document.getElementById("pageSize").value;
		}else{
			noOfRowsEJ=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAssessmentGrid_DivProfile(teacherIdForprofileGrid);
	}
	else
		{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRows = document.getElementById("pageSize3").value;
		}else{
			noOfRows=10;
		}
		var teacherIdp =document.getElementById("teacherId").value;
		getNotesDiv(teacherIdp);
	}
}

function getCandidateGridLoad()
{
	arrTagCall =[];
	var callbreakup=false;
	var callNoofRecords=10;
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	CandidateGridAjaxNew.getCandidateReportGrid(jobId,"","","","0","0","0","0","5","","","5","0","0","0",orderColumn, sortingOrder,callbreakup,callNoofRecords,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			try{
				$('#myModalSearch').modal("hide");
			}catch(err)
			  {}
			
			document.getElementById("divReportGrid").innerHTML=data;
			tpJbIDisable();
			tpJbIEnable();
			displayTotalCGRecord(1,0);
			var txtgridloadbreakup=document.getElementById("txtgridloadbreakup").value;
			if(txtgridloadbreakup=="yes")
			{
				var txtjftcount=document.getElementById("txtjftcount").value;
				var txtgridloadInterval=document.getElementById("txtgridloadInterval").value;
				var txtloopCallTimes=document.getElementById("txtloopCallTimes").value;
				var totjft = parseInt(txtjftcount);
				var gridInterval = parseInt(txtgridloadInterval);
				var loopCallTimes = parseInt(txtloopCallTimes);
				
				for(var i=0; i < loopCallTimes-1; i++)
				{
					var lasttrId="#avltr_"+callNoofRecords;
					var sessionList="lstAbvGrdData";//add by ram nath
					CGServiceAjax.getBreakUpData(i,sessionList, { 
						async: true,
						errorHandler:handleError,
						callback:function(data)
						{
							$(lasttrId).after(data);
							
							if(loopCallTimes-1==i)
							{
								tpJbIDisable();
							}
						}
					});	
				}
			}
		}});
	
}
function displayTotalCGRecord(sectionFlag,minusNo)
{
	if(sectionFlag==1){
		totalNoOfRecord=document.getElementById('totalNoOfRecord').value;
	}else if(sectionFlag==2){
		sectionRecordInter=document.getElementById('sectionRecordInter').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordInter').value;
		}else{
			totalNoOfRecord-=sectionRecordInter;
		}
	}else if(sectionFlag==3){
		sectionRecordVlt=document.getElementById('sectionRecordVlt').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordVlt').value;
		}else{
			totalNoOfRecord-=sectionRecordVlt;
		}
	}else if(sectionFlag==4){
		sectionRecordRemove=document.getElementById('sectionRecordRemove').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordRemove').value;
		}else{
			totalNoOfRecord-=sectionRecordRemove;
		}
	}else if(sectionFlag==5){
		sectionRecordInComp=document.getElementById('sectionRecordInComp').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordInComp').value;
		}else{
			totalNoOfRecord-=sectionRecordInComp;
		}
	}else if(sectionFlag==6){
		sectionRecordWithDr=document.getElementById('sectionRecordWithDr').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordWithDr').value;
		}else{
			totalNoOfRecord-=sectionRecordWithDr;
		}
	}else if(sectionFlag==7){
		sectionRecordHired=document.getElementById('sectionRecordHired').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordHired').value;
		}else{
			totalNoOfRecord-=sectionRecordHired;
		}
	}
	else if(sectionFlag==8){
		sectionRecordHidden=document.getElementById('sectionRecordHidden').value;
		if(minusNo==0){
			totalNoOfRecord=document.getElementById('totalNoOfRecordHidden').value;
		}else{
			totalNoOfRecord-=sectionRecordHidden;
		}
	}
}
function getRemoveCandidateGrid()
{	
	$('#removeWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateGridKellyReportAjax.getInactiveCandidateGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn,sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		
		document.getElementById("divInactiveCGGrid").innerHTML=data;
		
		//add by Ram Nath
		var table="divInactiveCGGrid";//add by Ram Nath
		var listLength=document.getElementById("rejectedListSize").value;
		$('#noOfApp4').html($('#sectionRecordRemove').val());
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'Rejectedtr_',$("[name='sessionListNameRejected"+jobId+"']").val(),1);
		//end by Ram Nath
			
			displayTotalCGRecord(4,0);
			var startPoint=totalNoOfRecord-sectionRecordRemove;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPositionNew();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
				
			}
			
			getRejectScoreDetails();
			
		}});
}
function hideRemoveCandidateGrid()
{		
	document.getElementById("divInactiveCGGrid").innerHTML="<div id='removeWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function hideInCompleteCandidateGrid()
{		
	document.getElementById("divInCompCGGrid").innerHTML="<div id='InCompWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}

function getWithdrawnCandidateGrid()
{	
	$('#WithDrWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateGridKellyReportAjax.getWithdrawnCandidateGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{		
		document.getElementById("divWithDrCGGrid").innerHTML=data;
		
		//add by Ram Nath
		var table="divWithDrCGGrid";//add by Ram Nath
		var listLength=document.getElementById("withDrawListSize").value;
		$('#noOfApp6').html($('#sectionRecordWithDr').val());
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'withDrawtr_',$("[name='sessionListNameWithDraw"+jobId+"']").val(),1);
		//end by Ram Nath
			
			displayTotalCGRecord(6,0);
			var startPoint=totalNoOfRecord-sectionRecordWithDr;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			
			getWithdrawnScoreDetails();
			
		}});
}
function hideWithdrawnCandidateGrid()
{		
	document.getElementById("divWithDrCGGrid").innerHTML="<div id='WithDrWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}


function getHiredCandidateGrid()
{	
	
	$('#hiredWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateGridKellyReportAjax.getHiredCandidateGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{		
		document.getElementById("divHiredCGGrid").innerHTML=data;
		
		//add by ram nath for 10-10 record
		var table="divHiredCGGrid";//add by Ram Nath
		var listLength=document.getElementById("hiredListSize").value;
		$('#noOfApp2').html($('#sectionRecordHired').val());
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'Hiredtr_',$("[name='sessionListNameHired"+jobId+"']").val(),1);
		//end by ram nath for 10-10 record
			
			displayTotalCGRecord(7,0)
			var startPoint=totalNoOfRecord-sectionRecordHired;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;	
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
				
			}
			
			getHiredScoreDetails();
		}});
}
function hideHiredCandidateGrid()
{		
	document.getElementById("divHiredCGGrid").innerHTML="<div id='hiredWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function getOnBoardingCandidateGrid()
{	
	$('#onboardingWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}

	
     CandidateGridKellyReportAjax.getOnBoardingCandidateGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		document.getElementById("divOnboardingGrid").innerHTML=data;
		
		//add by ram nath for 10-10 record
		var table="divOnboardingGrid";//add by Ram Nath
		var listLength=document.getElementById("timeOutListSize").value;
		$('#noOfApp3').html($('#sectionRecordVlt').val());
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'TimeOuttr_',$("[name='sessionListNameTimeOut"+jobId+"']").val(),1);
		//end by ram nath for 10-10 record
			
			displayTotalCGRecord(3,0)
			var startPoint=totalNoOfRecord-sectionRecordVlt;
			
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;	
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			getOnBoardingScoreDetails();
		}});
}
function hideOnBoardingCandidateGrid()
{		
	document.getElementById("divOnboardingGrid").innerHTML="<div id='onboardingWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}
function tpJbIEnable()
{
	expanddiv=0;
	var noOrRow = document.getElementById("totalNoOfRecord").value;
	$('#hrefPDF').tooltip();
	$('#tpSearch').tooltip();
	$('#tpLegend').tooltip();
	$('#datailsGrid').tooltip();
	$('#scoreGrid').tooltip();
	$('#normCurve').tooltip();
	$('#tpexport').tooltip();
	$('#jobHired').tooltip();
	
	for(var i=1;i<=7;i++){
		$('#jobApplied'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore1'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore2'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore3'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore4'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore5'+i).tooltip();
	}
	for(var i=1;i<=7;i++){
		$('#assessScore6'+i).tooltip();
	}


	for(var i=1;i<=3;i++){
		$('#oaScore1'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore2'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore3'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore4'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore5'+i).tooltip();
	}
	for(var i=1;i<=3;i++){
		$('#oaScore6'+i).tooltip();
	}
	
	
	for(var i=1;i<=6;i++){
		$('#HNS'+i).tooltip();
	}
	$(document).ready(function(){
		 
		/*$("#tpLegend").click(function(){
	    	$("#divToggel").slideToggle("slow");			
	  	});*/
		
		var delayVal=1000;
		var delayMin=100;
		
		$("#scoreGrid").click(function(){ 
				if(document.getElementById("expandchk").value==0){
					expanddiv++;
					if(expanddiv>=2){
						document.getElementById("expandchk").value=1;
						document.getElementById("scoredf").colSpan="2";
						if($("#detailsdf").length>0)
						document.getElementById("detailsdf").colSpan="1";
						document.getElementById("tdscore0tbl").style.textIndent="0px";
						//document.getElementById("compositeScoreGrid").style.textIndent="0px";
						//document.getElementById("scoreright").style.display='inline';
						//document.getElementById("scoreleft").style.display='none';
						$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumn).tooltip('show');
						
							 $( ".tdscore0:first" ).animate({
							    left :0,
								 width:256
							  }, {
							    duration: 1000
							  });
							 
							 $( "#tdscoremean").delay(delayVal).fadeIn(delayVal);
								
								setTimeout(function(){
								jQuery("#tblGrid tr:first").addClass("bg");
								$("#tdscore0").show();
								jQuery("#scoreright").show();
								jQuery("#scoreleft").hide();
								},1001);
								
						for(var j=1;j<=noOrRow;j++)
						{
							$( "#tdscore"+j ).delay(delayVal).fadeIn(delayVal);
						}
						getRejectScoreDetails(); 
						getWithdrawnScoreDetails();
						getOnBoardingScoreDetails();
						getHiredScoreDetails();
						getHiddenScoreDetails();
						
						$( "#tdscoremean").delay(delayVal).fadeIn(delayVal);
					}else{
						document.getElementById("expandchk").value=1;
						document.getElementById("scoredf").colSpan="2";
						document.getElementById("tdscore0tbl").style.textIndent="0px";
						//document.getElementById("compositeScoreGrid").style.textIndent="0px";
						//document.getElementById("scoreright").style.display='inline';
						//document.getElementById("scoreleft").style.display='none';
						$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumn).tooltip('show');
							 $( ".tdscore0:first" ).animate({
							    left :0,
								 width:256
							  }, {
							    duration: 1000
							  });
							 setTimeout(function(){
									
									jQuery("#tblGrid tr:first").addClass("bg");
									$("#tdscore0").show();
									jQuery("#scoreright").show();
									jQuery("#scoreleft").hide();
									
								},1001);
						for(var j=1;j<=noOrRow;j++)
						{
							$( "#tdscore"+j ).delay(delayVal).fadeIn(delayVal);
							
							
						}
						
						getRejectScoreDetails(); 
						getWithdrawnScoreDetails();
						getOnBoardingScoreDetails();
						getHiredScoreDetails();
						getHiddenScoreDetails();
						$( "#tdscoremean").delay(delayVal).fadeIn(delayVal);
					}
				}else{
					expanddiv--;
					document.getElementById("expandchk").value=0;
					document.getElementById("scoredf").colSpan="2";
					document.getElementById("tdscore0tbl").style.textIndent="99999px";
					//document.getElementById("compositeScoreGrid").style.textIndent="99999px";
					document.getElementById("scoreright").style.display='none';
					document.getElementById("scoreleft").style.display='inline';
					$("#scoreGrid").attr('data-original-title', resourceJSON.MsgCollapseTheColumn).tooltip('show');
					
					 $( ".tdscore0:first" ).animate({
						    left :0,
							 width:1
						  }, {
						    duration: 1000
						  });
					 setTimeout(function(){
						   $("#tdscore0").hide();
						   jQuery("#tblGrid tr:first").removeClass("bg");
						    $("#scoredefhead").attr("colspan","2");
							},500);
					for(var j=1;j<=noOrRow;j++)
					{
						$( "#tdscore"+j ).delay(delayMin).fadeOut(delayMin);
						
					}
					getRejectScoreDetails();
					getWithdrawnScoreDetails();
					getOnBoardingScoreDetails();
					getHiredScoreDetails();
					getHiddenScoreDetails();
					$( "#tdscoremean").delay(delayMin).fadeOut(delayMin);
				}
				
	  	});
		
		$("#inactiveGrid").click(function(){
			if(document.getElementById("expandremovechk").value==0){
				getRemoveCandidateGrid();
				document.getElementById("expandremovechk").value=1;
				document.getElementById("removeasc").style.display='none';
				document.getElementById("removedsc").style.display='inline';
			}else{
				displayTotalCGRecord(4,1);
				document.getElementById("expandremovechk").value=0;
				document.getElementById("removeasc").style.display='inline';
				document.getElementById("removedsc").style.display='none';
				hideRemoveCandidateGrid();
			}
			
		});
		
		$("#hiredGrid").click(function(){
			if(document.getElementById("expandhiredchk").value==0){
				getHiredCandidateGrid();
				document.getElementById("expandhiredchk").value=1;
				document.getElementById("hiredasc").style.display='none';
				document.getElementById("hireddsc").style.display='inline';
			}else{
				displayTotalCGRecord(7,1);
				document.getElementById("expandhiredchk").value=0;
				document.getElementById("hiredasc").style.display='inline';
				document.getElementById("hireddsc").style.display='none';
				hideHiredCandidateGrid();
			}
			
		});
		$("#onboardingGrid").click(function(){
			if(document.getElementById("expandvltchk").value==0){
				getOnBoardingCandidateGrid();
				document.getElementById("expandvltchk").value=1;
				document.getElementById("vltasc").style.display='none';
				document.getElementById("vltdsc").style.display='inline';
			}else{
				displayTotalCGRecord(3,1);
				document.getElementById("expandvltchk").value=0;
				document.getElementById("vltasc").style.display='inline';
				document.getElementById("vltdsc").style.display='none';
				hideOnBoardingCandidateGrid();
			}
		});
		$("#withdrGrid").click(function(){
			if(document.getElementById("expandwithdrchk").value==0){
				getWithdrawnCandidateGrid();
				document.getElementById("expandwithdrchk").value=1;
				document.getElementById("withdrasc").style.display='none';
				document.getElementById("withdrdsc").style.display='inline';
			}else{
				displayTotalCGRecord(6,1);
				document.getElementById("expandwithdrchk").value=0;
				document.getElementById("withdrasc").style.display='inline';
				document.getElementById("withdrdsc").style.display='none';
				hideWithdrawnCandidateGrid();
			}
		});
		
		$("#hiddenGrid").click(function(){
			if(document.getElementById("expandhiddenchk").value==0){
				getHiddenCandidateGrid();
				document.getElementById("expandhiddenchk").value=1;
				document.getElementById("hiddenasc").style.display='none';
				document.getElementById("hiddendsc").style.display='inline';
			}else{
				displayTotalCGRecord(8,1);
				document.getElementById("expandhiddenchk").value=0;
				document.getElementById("hiddenasc").style.display='inline';
				document.getElementById("hiddendsc").style.display='none';
				hideHiddenCandidateGrid();
			}
		});
		
	});
	
	
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#hireTp'+j).tooltip();
		$('#tpJSA'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		$('#tpCL'+j).tooltip();
		$('#tpAct'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();		
		$('#tpTrans'+j).tooltip();
		$('#tpCert'+j).tooltip();
		$('#tpMessage'+j).tooltip();
		$('#tpPhone'+j).tooltip();
		$('#aNotes'+j).tooltip();
		$('#tpref'+j).tooltip();
		$('#tpInv'+j).tooltip();
		$('#tprr'+j).tooltip();	
		$('#ftsbu'+j).tooltip();
		$('#tooltipPreview'+j).tooltip()
		$('#removeStatus'+j).tooltip();
		$('#teacherName_'+j).tooltip();
		$('#i4Ques'+j).tooltip();
		$('#teacher_PNQ'+j).tooltip();
		try { $('#alum'+j).tooltip(); } catch (e) {}
		try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
		try{$('#tpRefChk'+j).tooltip(); }catch(e){}
		try{$('#dtFailed'+j).tooltip(); }catch(e){}
		try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
		try{$('#pc'+j).tooltip(); }catch(e){}
	}
}
function refreshStatus(){
	//alert("refreshStatus");
	document.getElementById("expandremovechk").value=0;
	document.getElementById("expandincompchk").value=0;
	document.getElementById("expandinterchk").value=0;
	document.getElementById("expandvltchk").value=0;
	document.getElementById("expandwithdrchk").value=0;
	document.getElementById("expandhiredchk").value=0;
	document.getElementById("expandhiddenchk").value=0;
}

function getRejectScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_reject:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
		 setTimeout(function(){
				jQuery("#tblGrid .removeCandidate").addClass("bg");
				$("#tdscore0tbl_reject").show();
					
			  },1001);
		 
		try {
		document.getElementById("tdscore0tbl_reject").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordRemove;i++)
		{
			try {
				document.getElementById("tdscore_reject"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_reject:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 	setTimeout(function(){
				jQuery("#tblGrid .removeCandidate").removeClass("bg");
				$("#tdscore0tbl_reject").hide();
					
			  },500);
		
		 try {
			 document.getElementById("tdscore0tbl_reject").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordRemove;i++)
		{
			try {
				document.getElementById("tdscore_reject"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}
function getHiddenScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_hidden:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		 setTimeout(function(){
				jQuery("#tblGrid .hiddenCandidate").addClass("bg");
				$("#tdscore0tbl_hidden").show();
					
			  },1001);
		try {
		document.getElementById("tdscore0tbl_hidden").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordHidden;i++)
		{
			try {
				document.getElementById("tdscore_hidden"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_hidden:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 setTimeout(function(){
				jQuery("#tblGrid .hiddenCandidate").addClass("bg");
				$("#tdscore0tbl_hidden").hide();
					
			  },500);
		 try {
			 document.getElementById("tdscore0tbl_hidden").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordHidden;i++)
		{
			try {
				document.getElementById("tdscore_hidden"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}

function getWithdrawnScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_withdrawn:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
		
		 setTimeout(function(){
				jQuery("#tblGrid .withdrawnCandidate").addClass("bg");
				$("#tdscore0tbl_withdrawn").show();
					
			  },1001);
		 
		 
		try {
		document.getElementById("tdscore0tbl_withdrawn").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordWithDr;i++)
		{
			try {
				document.getElementById("tdscore_withdrawn"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_withdrawn:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 setTimeout(function(){
				jQuery("#tblGrid .withdrawnCandidate").removeClass("bg");
				$("#tdscore0tbl_withdrawn").hide();
					
			  },500);
		
		 try {
			 document.getElementById("tdscore0tbl_withdrawn").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordWithDr;i++)
		{
			try {
				document.getElementById("tdscore_withdrawn"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
			
			
		}
	}
}
function getHiredScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_hired:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
		 setTimeout(function(){
				jQuery("#tblGrid .hiredCandidate").addClass("bg");
				$("#tdscore0_hired").show();
					
			  },1001);
		 
		try {
		document.getElementById("tdscore0tbl_hired").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordHired;i++)
		{
			try {
				document.getElementById("tdscore_hired"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_hired:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 setTimeout(function(){
				jQuery("#tblGrid .hiredCandidate").removeClass("bg");
				$("#tdscore0_hired").hide();
				jQuery(".hiredCandidate th:last").attr("colspan","2");	
			  },500);
		
		 try {
			 document.getElementById("tdscore0tbl_hired").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordHired;i++)
		{
			try {
				document.getElementById("tdscore_hired"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
}

function getOnBoardingScoreDetails()
{
	var expandchk=document.getElementById("expandchk").value;
	if(expandchk >= 1)
	{
		$( ".tdscore0_timeout:first" ).animate({
		    left :0,
			 width:256
		  }, {
		    duration: 1000
		  });
		
	
		 setTimeout(function(){
				jQuery("#tblGrid .timeoutCandidate").addClass("bg");
				$("#tdscore0tbl_timeout").show();
					
			  },1001);
		
		try {
		document.getElementById("tdscore0tbl_timeout").style.textIndent="0px";
		} catch (e) {
			// TODO: handle exception
		}
		for(var i=1;i<=sectionRecordVlt;i++)
		{
			try {
				document.getElementById("tdscore_timeout"+i).style.display="inline";
			} catch (e) {
				// TODO: handle exception
			}
			
		}
	}
	else
	{
		 $( ".tdscore0_timeout:first" ).animate({
			    left :0,
				 width:1
			  }, {
			    duration: 1000
			  });
		 
		 setTimeout(function(){
				jQuery("#tblGrid .timeoutCandidate").removeClass("bg");
				$("#tdscore0tbl_timeout").hide();
					
			  },500);
		
		 try {
			 document.getElementById("tdscore0tbl_timeout").style.textIndent="-99999px";
		 } catch (e) {
				// TODO: handle exception
			}
		for(var i=1;i<=sectionRecordVlt;i++)
		{
			try {
				document.getElementById("tdscore_timeout"+i).style.display="none";
			} catch (e) {
				// TODO: handle exception
			}
		}
	}
}


// start Vikas Kumar
function checkActionMenu(){
	var inputs = document.getElementsByTagName("input"); 
	var chkCount=0;
	var chkCheckedCount=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	chkCount++;
	            if(inputs[i].checked){
	            	chkCheckedCount++;
	            }
	        }
	 } 
	 if(chkCheckedCount>0){
		 document.getElementById("actionDiv").style.display="inline";
	 }else{
		 document.getElementById("actionDiv").style.display="none";
	 }

}

function checkAllCandidate(){
	var flag = document.getElementById("checkAllCandidate").checked;
   	
	 if(flag==true)
	 { 
		$('#divReportGrid input[internalCand="interDivHidden"]').each(function() {
			 $(this).attr('checked', 'checked');
			checkActionMenu();
		});
	 }
	 else{		 
		 $('#divReportGrid input[internalCand="interDivHidden"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
			 
				});
	 }
}

function checkAllCandidateForInComp(){
	var flag = document.getElementById("checkAllCandidateInComp").checked;
   	
	 if(flag==true)
	 { 
		$('#divInCompCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divInCompCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}

function checkAllCandidateForWithCand(){
	var flag = document.getElementById("checkAllCandidateWithCand").checked;
   	
	 if(flag==true)
	 { 
		$('#divWithDrCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divWithDrCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}

function checkAllForRejectedCand(){
	var flag = document.getElementById("checkAllCandidateRejected").checked;
   	
	 if(flag==true)
	 { 
		$('#divInactiveCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divInactiveCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}

function checkAllCandidateForTimedOut(){
	var flag = document.getElementById("checkAllCandidateTimedOut").checked;
   	
	 if(flag==true)
	 { 
		$('#divOnboardingGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divOnboardingGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}

function checkAllCandidateForHired(){
	var flag = document.getElementById("checkAllCandidateHired").checked;
   	
	 if(flag==true)
	 { 
		$('#divHiredCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divHiredCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}
//end Vikas Kumar>>>>>>> 1.51


function getHiredCandidateReports()
{	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
     //alert(' Before Call');
     CandidateReportAjax.getHiredForKellyCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 try{
  			document.getElementById("noOfApp2").innerHTML=data;
  		}catch(err)
  		  {}
  			getInactiveCandidateReports();
		}});
}


function getInactiveCandidateReports()
{	
	//alert(':::::::::::::::::::::getRemoveCandidateReports:::::::::::::::');
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateReportAjax.getInactiveCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn,sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	// alert('data::::::::::::'+data);
    	 try{
	  			document.getElementById("noOfApp4").innerHTML=data;
	  		}catch(err)
	  		  {}
	  		getHiddenCandidateReports();
		}});
}
function getOnBoardingCandidateReports()
{	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}

	
     CandidateReportAjax.getOnBoardingCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 try{
	  			document.getElementById("noOfApp3").innerHTML=data;
	  		}catch(err)
	  		  {}
	  		getHiredCandidateReports();
		}});
}
function getWithdrawnCandidateReports()
{	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateReportAjax.getWithdrawnCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 try{
	  			document.getElementById("noOfApp6").innerHTML=data;
	  		}catch(err)
	  		  {}
		}});
}

function getHiddenCandidateGrid()
{	
	$('#HiddenWait').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateGridKellyReportAjax.getHiddenCandidateGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		document.getElementById("divHiddenCGGrid").innerHTML=data;
		
		
		var table="divHiddenCGGrid";
		var listLength=document.getElementById("hiddenListSize").value;
		$('#hiddenTotal').html($('#sectionRecordHidden').val());
		if(callbreakup)
			if(listLength>1)
				getBreakUpRecords(table,listLength,'hiddentr_',$("[name='sessionListNameHidden"+jobId+"']").val(),1);
		
			
			displayTotalCGRecord(8,0);
			var startPoint=totalNoOfRecord-sectionRecordHidden;
			for(var j=startPoint;j<=totalNoOfRecord;j++)
			{	
				$('#tpResume'+j).tooltip();
				$('#tpPortfolio'+j).tooltip();
				$('#tpJSA'+j).tooltip();
				$('#hireTp'+j).tooltip();
				$('#tpPDReport'+j).tooltip();
				$('#tpCL'+j).tooltip();
				$('#tpAct'+j).tooltip();
				$('#tpMsg'+j).tooltip();
				$('#tpNotes'+j).tooltip();		
				$('#tpTrans'+j).tooltip();
				$('#tpCert'+j).tooltip();
				$('#tpMessage'+j).tooltip();
				$('#tpPhone'+j).tooltip();
				$('#tpInv'+j).tooltip();
				$('#tprr'+j).tooltip();	
				$('#ftsbu'+j).tooltip();
				$('#tooltipPreview'+j).tooltip()
				$('#tpref'+j).tooltip();
				$('#teacherName_'+j).tooltip();
				$('#i4Ques'+j).tooltip();
				$('#teacher_PNQ'+j).tooltip();
				try { $('#alum'+j).tooltip(); } catch (e) {}
				try{ $('#tpInternalCandidate'+j).tooltip(); }catch(e){}
				try{$('#tpRefChk'+j).tooltip(); }catch(e){}
				try{$('#dtFailed'+j).tooltip(); }catch(e){}
				try{$('#tpAssessmentinvite'+j).tooltip(); }catch(e){}
				try{$('#pc'+j).tooltip(); }catch(e){}
				var candidateFlag=document.getElementById("candidateflag").value;
				if(candidateFlag==1)
				{
					var noOfTagsFile=document.getElementById("noOfTagsFile").value;
					if(noOfTagsFile>15){
						iWidth=600;
					}
					else
					{
						iWidth=300;
					}
				}
				else
				{
					iWidth=300;
				}
				$('#tag'+j).popover({ 
				    html : true,
				    placement: 'right',
				    content: $('#tagDiv'+j).html(),
				  }).on("mouseenter", function () {
					  var height=setDialogPosition();
					  height=height+80;
					  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px;width: '+iWidth+'px; }</style>')
						$('html > head').append(style);
					  	
				        var _this = this;
				        $(this).popover("show");
				        $(this).siblings(".popover").on("mouseleave", function () {
				            //$(_this).popover('hide');
				        });
				    }).on("mouseleave", function () {
				        var _this = this;
				        setTimeout(function () {
				            if (!$(".popover:hover").length) {
				                //$(_this).popover("hide")
				            }
				        }, 100);
				    });
			}
			
			
		}});
}
function hideHiddenCandidateGrid()
{		
	document.getElementById("divHiddenCGGrid").innerHTML="<div id='HiddenWait' style='display:none;text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>";
}

function checkAllCandidateHidden(){
	var flag = document.getElementById("checkAllCandidateHidden").checked;
   	
	 if(flag==true)
	 { 
		$('#divHiddenCGGrid input[name="cgCBX"]').each(function() {
			 $(this).attr('checked', 'checked');
			 checkActionMenu();
		});
	 }
	 else{
		 
		 $('#divHiddenCGGrid input[name="cgCBX"]').each(function() {
			 $(this).prop('checked', false);
			 checkActionMenu();
				});
	 }
}
function getHiddenCandidateReports()
{	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateReportAjax.getHiddenCandidateReports(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,tagsearchId,callbreakup,callNoofRecords,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
    	 try{
	  			document.getElementById("hiddenTotal").innerHTML=data;
	  		}catch(err)
	  		  {}
	  		getWithdrawnCandidateReports();
		}});
}

function getCandidateGrid()
{
	$('#divReportGrid').html("<table align='center' id='loaddiv'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	
	var pageNo = 0;
	var pageSize=50;
	var chkstr="";
	jQuery("input[name='cgCBX']").each(function(){
		if(jQuery(this).is(":checked"))
		chkstr += "~"+jQuery(this).val();
	});

	arrTagCall =[];
	// by alok for dynamic div width
	try
	{
	   var mydiv = document.getElementById("mydiv");
	   var curr_width = parseInt(mydiv.style.width);
	  
	   if(deviceType)
	   {
		   mydiv.style.width = 630+"px";  
	   }
	   else
	   {
		   mydiv.style.width = 647+"px";  
	   } 
	}
	catch(e){alert(e)}
	/*.........................................*/
	//alert("1");
	 
	$('#loadingDiv').show();
	
	
	//$("#loadingDiv").modal('show');
	//alert("1");
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	document.getElementById("contacted").value;
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;

	try{
		var iframeNorm = document.getElementById('ifrmNorm');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('normScoreFrm');
		document.getElementById("normScore").value=inputNormScore.value;
	}catch(e){}	
	try{
		var iframeCGPA = document.getElementById('ifrmCGPA');
		var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
		var inputCGPA = innerCGPA.getElementById('CGPAFrm');
		document.getElementById("CGPA").value=inputCGPA.value;
	}catch(e){}
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);

	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);

	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
	var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
	 try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
	var epiToDate	=	trim(document.getElementById("epiToDate").value);
	 }catch(err){}
	 
	 var gridName = document.getElementById("sortingGrid").value; 
	 
	 CandidateGridKellyReportAjax.getCandidateKellyGrid(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,callbreakup,callNoofRecords,tagsearchId,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,chkstr,pageNo,pageSize,gridName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#loadingDiv').hide();
		try{
			$('#myModalSearch').modal("hide");
		}catch(err)
		  {}
	 
		document.getElementById("divReportGrid").innerHTML=data[0];
		try{
			var totalNoOfRecordVal=document.getElementById("totalNoOfRecord").value;
			document.getElementById("noOfApp1").innerHTML=totalNoOfRecordVal;
			getOnBoardingCandidateReports();
		}catch(err)
		  {}
		
		tpJbIDisable();
		tpJbIEnable();
		displayTotalCGRecord(1,0);
		var txtgridloadbreakup="";
		try{
			txtgridloadbreakup=document.getElementById("txtgridloadbreakup").value;
		}catch(err){}
		
		if(txtgridloadbreakup=="yes")
		{
			var txtjftcount=document.getElementById("txtjftcount").value;
			var txtgridloadInterval=document.getElementById("txtgridloadInterval").value;
			var txtloopCallTimes=document.getElementById("txtloopCallTimes").value;
			var totjft = parseInt(txtjftcount);
			var gridInterval = parseInt(txtgridloadInterval);
			var loopCallTimes = parseInt(txtloopCallTimes);
			
			var table="divReportGrid";//add by Ram Nath
			getBreakUpRecords(table,(loopCallTimes-1),'avltr_',$("[name='sessionListNameAvl"+jobId+"']").val(),0); //add by Ram Nath
			/*for(var i=0; i < loopCallTimes-1; i++)
			{
				var lasttrId="#avltr_"+callNoofRecords;
				
				CGServiceAjax.getBreakUpData(i,document.getElementById("sessionListNameAvl").value, { 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
							$(lasttrId).after(data);	
						if(loopCallTimes-1==i)
						{
							tpJbIDisable();
						}
					}
				});	
				
			}*/
		}

		/*if(parseInt(data[1])>0){  //alert('calling new Pagination ::::::'+data[1]+'----'+pageSize);
			
			//alert((parseInt(data[2])+1)+"---------"+pageSize+"--------------"+parseInt(data[1]));
			getCandidateKellyGridPage((parseInt(data[2])+1),pageSize,parseInt(data[1]));
		}*/
		}});
}



function getCandidateKellyGridPage(pageNo,pageSize,noOfRecordCheck){
	 
	//alert(pageNo+"---------------"+pageSize+"________"+noOfRecordCheck)
	// alert("pagination called");
	var chkstr="";
	jQuery("input[name='cgCBX']").each(function(){
		if(jQuery(this).is(":checked"))
		chkstr += "~"+jQuery(this).val();
	});
 
	
	//$("#loadingDiv").modal('show');
	//alert("1");
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	document.getElementById("contacted").value;
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;


	try{
		var iframeNorm = document.getElementById('ifrmNorm');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('normScoreFrm');
		document.getElementById("normScore").value=inputNormScore.value;
	}catch(e){}	
	try{
		var iframeCGPA = document.getElementById('ifrmCGPA');
		var innerCGPA = iframeCGPA.contentDocument || iframeCGPA.contentWindow.document;
		var inputCGPA = innerCGPA.getElementById('CGPAFrm');
		document.getElementById("CGPA").value=inputCGPA.value;
	}catch(e){}
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);

	var tagsearchId		=	trim(document.getElementById("tagsearchId").value);

	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
	var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
	 try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
	var epiToDate	=	trim(document.getElementById("epiToDate").value);
	 }catch(err){}
	 CandidateGridKellyReportAjax.getCandidateKellyGridPage(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,callbreakup,callNoofRecords,tagsearchId,
			certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,chkstr,pageNo,pageSize,noOfRecordCheck,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		
			/*$('#loadingDiv').hide();
			try{
				$('#myModalSearch').modal("hide");
			}catch(err)
			  {}
			
			document.getElementById("divReportGrid").innerHTML=data;
			tpJbIDisable();
			tpJbIEnable();
			displayTotalCGRecord(1,0);
			*/
	
		
		 
		try{
			$('#myModalSearch').modal("hide");
		}catch(err)
		  {}
	   
		
		if(parseInt(data[1])>0){
			if($('#meanRow').length==1){
				$('#meanRow').remove();
			}

			var arr = data[0].split("[new#*#Row]");
			var storeval="";
			var totalRecords=arr.length;
			var counter=0;
			for (i = 0; i < arr.length; i++) { 
				counter++;
				storeval+=arr[i];
				if(counter%20==0){
					document.getElementById("cgTbody").innerHTML+=storeval;
					storeval="";
					totalRecords =totalRecords-counter;
					counter=0;
				}else if(totalRecords-counter){
					document.getElementById("cgTbody").innerHTML+=storeval;
					storeval="";
					counter=0;
				}					

			}
			document.getElementById("noOfApp1").innerHTML=data[1];
			//alert((parseInt(data[2])+1)+"---------"+pageSize+"--------------"+parseInt(data[1]));
			getCandidateKellyGridPage((parseInt(data[2])+1),pageSize,parseInt(data[1]));
			
		}
			
		}});

}

//Gaurav Kumar
function downloadCandidateGridReportForKelly()
{
	$('#loadingDiv').show();
	
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateGridKellyReportAjax.downloadCandidateGridReport(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
	normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,false,null,certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{
	async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			//window.open(data);
			//openWindow(data,'Support',650,490);
			
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadCGR').modal('hide');
					document.getElementById(hrefId).href=data;				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  try{
					    $('#modalDownloadCGR').modal('hide');
						}catch(err)
						  {}
					    document.getElementById('ifrmCGR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadCGR').modal('hide');
					document.getElementById(hrefId).href=data;					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  try{
				    	$('#modalDownloadCGR').modal('hide');
					}catch(err)
					  {}
				    document.getElementById('ifrmCGR').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadCGR').modal('hide');
				document.getElementById('ifrmCGR').src = ""+data+"";
				try{
					$('#modalDownloadCGR').modal('show');
				}catch(err)
				  {}
				
			}		
			
			return false;
		}});
}
//Gaurav Kumar
function generateCGExcelForKelly()
{
	$('#loadingDiv').show();
	var jobId=document.getElementById("jobId").value;
	var firstName		=	trim(document.getElementById("firstName").value); 
	var lastName		=	trim(document.getElementById("lastName").value);
	var emailAddress	=	trim(document.getElementById("emailAddress").value);
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	var regionId		=	trim(document.getElementById("regionId").value);
	var degreeId		=	trim(document.getElementById("degreeId").value);
	var universityId		=	trim(document.getElementById("universityId").value);
	var normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
	var normScore		=	trim(document.getElementById("normScore").value);
	var CGPA		=	trim(document.getElementById("CGPA").value);
	var CGPASelectVal		=	trim(document.getElementById("CGPASelectVal").value);
	var contacted		=	trim(document.getElementById("contacted").value);
	var candidateStatus		=	trim(document.getElementById("candidateStatus").value);
	var stateId=document.getElementById("stateId").value;
	try{
		var ifrmYOTE = document.getElementById('ifrmYOTE');
		var innerYOTE = ifrmYOTE.contentDocument || ifrmYOTE.contentWindow.document;
		var inputYOTE = innerYOTE.getElementById('YOTEFrm');
		document.getElementById("YOTE").value=inputYOTE.value;
	}catch(e){}
	var YOTESelectVal		=	trim(document.getElementById("YOTESelectVal").value);
	var YOTE				=	document.getElementById("YOTE").value;
	var hiddenCertTypeIds 	=	document.getElementsByName("hiddenCertTypeId");
	var stateId2	=	trim(document.getElementById("stateId2").value);
    var zipCode		=	trim(document.getElementById("zipCode").value);
	var certIds =[];
	for(var i=0;i<hiddenCertTypeIds.length;i++)
	{
		certIds.push(hiddenCertTypeIds[i].value);
	}
	var epiFromDate = "";
	var epiToDate = "";
     try{
	var epiFromDate	=	trim(document.getElementById("epiFromDate").value);
    var epiToDate	=	trim(document.getElementById("epiToDate").value);
     }catch(err){}
	
     CandidateGridKellyReportAjax.generateCGExcel(jobId,firstName,lastName,emailAddress,certType,regionId,degreeId,universityId,normScoreSelectVal,
			normScore,CGPA,CGPASelectVal,contacted,candidateStatus,stateId,orderColumn, sortingOrder,totalNoOfRecord,false,null,certIds,YOTESelectVal,YOTE,stateId2,zipCode,epiFromDate,epiToDate,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{		
		$('#loadingDiv').hide();
		if(deviceType)
		{
			if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}		
		}	
		else
		{
			document.getElementById('ifrmTrans').src = "candidate/"+data+"";	
		}	
		}});

}

function getJobInformationHeaderCG(){
	$('#loadingDiv').show();
	$('#loaddiv').hide();
	var jobId=document.getElementById("jobId").value;
	
		CandidateGridSubAjax.getJobInformationHeaderCG(jobId, { 
			async:true,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data!=null){
					//console.log(data);
					$(".tableContentJobHeader").html(data);
					$('#loadingDiv').hide();
					$('#loaddiv').show();
				}
			}
		});
}