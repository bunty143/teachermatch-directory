var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var digitToString=new Array("","one","two","three","four","five","six","seven","eight","nine","ten");
var dspqCalInstance=null;
var isJobAplied=false;
var pagetpvh=1;
var sortOrderStrtpvh="";
var sortOrderTypetpvh="";
var noOfRowstpvh=10;
/*========= Start ... Paging and Sorting ===========*/
var dp_Academics_page = 1;
var dp_Academics_noOfRows = 10;
var dp_Academics_sortOrderStr="";
var dp_Academics_sortOrderType="";

var dp_Certification_page = 1;
var dp_Certification_noOfRows = 10;
var dp_Certification_sortOrderStr="";
var dp_Certification_sortOrderType="";

var dp_Reference_page = 1;
var dp_Reference_noOfRows = 10;
var dp_Reference_sortOrderStr="";
var dp_Reference_sortOrderType="";

var dp_AdditionalDocuments_page = 1;
var dp_AdditionalDocuments_noOfRows = 10;
var dp_AdditionalDocuments_sortOrderStr="";
var dp_AdditionalDocuments_sortOrderType="";

var dp_Employment_page=1;
var dp_Employment_noOfRows=100;
var dp_Employment_sortOrderStr="";
var dp_Employment_sortOrderType="";

var dp_SubjectArea_page = 1;
var dp_SubjectArea_noOfRows = 10;
var dp_SubjectArea_sortOrderStr="";
var dp_SubjectArea_sortOrderType="";

var dp_VideoLink_Rows=10
var dp_VideoLink_page=1
var dp_VideoLink_sortOrderStr=""
var dp_VideoLink_sortOrderType=""

var dp_StdTchrExp_Rows=10;
var dp_StdTchrExp_page=1;
var dp_StdTchrExp_sortOrderStr="";
var dp_StdTchrExp_sortOrderType="";

var dp_Residency_Rows=10;
var dp_Residency_page=1;
var dp_Residency_sortOrderStr="";
var dp_Residency_sortOrderType="";

var dp_Involvement_Rows=10;
var dp_Involvement_page=1;
var dp_Involvement_sortOrderStr="";
var dp_Involvement_sortOrderType="";

var dp_TchrLang_Rows=10;
var dp_TchrLang_page=1;
var dp_TchrLang_sortOrderStr="";
var dp_TchrLang_sortOrderType="";
var txtBgColor="#F5E7E1";
var currentPageFlag="";
//********************************************************************
function setGridNameFlag(gridName){
	document.getElementById("gridNameFlag").value=gridName;
}
function getPagingAndSortingSSPF(pageno,sortOrder,sortOrderTyp)
{
	var teacherId=document.getElementById("candidateProfileTeacherId").value;
	var portfolioId=document.getElementById("candidateProfilePortfolioId").value;
	var candidateType=document.getElementById("candidateProfileCandidateType").value;
	var gridNameFlag = document.getElementById("gridNameFlag").value;
	if(gridNameFlag=="additionalDocuments"){
		if(pageno!=''){
			dp_AdditionalDocuments_page=pageno;	
		}else{
			dp_AdditionalDocuments_page=1;
		}
		dp_AdditionalDocuments_sortOrderStr	=	sortOrder;
		dp_AdditionalDocuments_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_AdditionalDocuments_noOfRows=10;
		}
		//showGridAdditionalDocuments();
		additionalDocumentsProfiler(teacherId,portfolioId,candidateType,11);
	}else if(gridNameFlag=="academics"){
		if(pageno!=''){
			dp_Academics_page=pageno;	
		}else{
			dp_Academics_page=1;
		}
		dp_Academics_sortOrderStr	=	sortOrder;
		dp_Academics_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Academics_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Academics_noOfRows=10;
		}
		//showGridAcademics();
		getTeacherAcademicsGrid_DivProfile_sspf(teacherId,portfolioId,candidateType,6);
	}else if(gridNameFlag=="certification"){
		if(pageno!=''){
			dp_Certification_page=pageno;	
		}else{
			dp_Certification_page=1;
		}
		dp_Certification_sortOrderStr	=	sortOrder;
		dp_Certification_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			dp_Certification_noOfRows = document.getElementById("pageSize3").value;
		}else{
			dp_Certification_noOfRows=10;
		}
		//showGridCredentials();
		teacherCertificationsGrid(teacherId,portfolioId,candidateType,8);
	}else if(gridNameFlag=="reference"){
		if(pageno!=''){
			dp_Reference_page=pageno;	
		}else{
			dp_Reference_page=1;
		}
		dp_Reference_sortOrderStr	=	sortOrder;
		dp_Reference_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Reference_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Reference_noOfRows=10;
		}
		//getElectronicReferencesGrid();
		referenceGridProfiler(teacherId,portfolioId,candidateType,9);
	}else if(gridNameFlag=="workExperience"){
		if(pageno!=''){
			dp_Employment_page=pageno;	
		}else{
			dp_Employment_page=1;
		}
		dp_Employment_sortOrderStr	=	sortOrder;
		dp_Employment_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Employment_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Employment_noOfRows=10;
		}
		//getPFEmploymentDataGrid();
		employmentGridProfiler(teacherId,portfolioId,candidateType,13);
	}else if(gridNameFlag=="videolinks"){

		if(pageno!=''){
			dp_VideoLink_page=pageno;	
		}else{
			dp_VideoLink_page=1;
		}
		dp_VideoLink_sortOrderStr	=	sortOrder;
		dp_VideoLink_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_VideoLink_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_VideoLink_Rows=10;
		}
		//getVideoLinksGrid();
		videoLinkGridProfiler(teacherId,portfolioId,candidateType,10);
	}else if(gridNameFlag=="involvement"){
		if(pageno!=''){
			dp_Involvement_page=pageno;	
		}else{
			dp_Involvement_page=1;
		}
		dp_Involvement_sortOrderStr	=	sortOrder;
		dp_Involvement_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Involvement_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_Involvement_Rows=10;
		}
		//getInvolvementGrid();
		involvementGridNew(teacherId,portfolioId,candidateType,14);
	}else if(gridNameFlag=="language"){

		if(pageno!=''){
			dp_TchrLang_page=pageno;	
		}else{
			dp_TchrLang_page=1;
		}
		dp_TchrLang_sortOrderStr	=	sortOrder;
		dp_TchrLang_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_TchrLang_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_TchrLang_Rows=10;
		}
		//displayTchrLanguage();
		languageProfiencyProfiler(teacherId,portfolioId,candidateType,18);
	}else if(currentPageFlag=="tpvh"){
		if(pageno!=''){
			pagetpvh=pageno;	
		}else{
			pagetpvh=1;
		}
		sortOrderStrtpvh	=	sortOrder;
		sortOrderTypetpvh	=	sortOrderTyp;
		if(document.getElementById("pageSize14")!=null){
			noOfRowstpvh = document.getElementById("pageSize14").value;
		}else{
			noOfRowstpvh=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShowNew(teacherIdForprofileGrid);
	}
}

function showProfileContentCloseNew(){	
	try{
		$('#cgTeacherMasterDiv').modal('hide'); 
	}catch (e) {
		var $pc=jQuery.noConflict();
		$pc('#cgTeacherMasterDiv').modal('hide'); 
	}
	    document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";
}
var showGirdType="";
function showProfileContentNew(dis,jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,event,name,portfolioIdAndType,showType)
{
	var width=$(window).width();
	var height=$(window).height();
	//alert("Window width :::: "+width+" Window Height ::: "+height);
	if(width>1300 && height>600){
		$("#sspfDiv").attr("style","width: 1285px;padding-left: 6%;");
	}else if(width>1200 && height>400){
		$("#sspfDiv").attr("style","width: 1200px;padding-left: 6%;");
	}else if(width<1200 && width>=1000){
		$("#sspfDiv").attr("style","width: 979px;padding-left: 5%;");
	}else{
		$("#sspfDiv").attr("style","width: 781px;padding-left: 1%;");
	}
	
	showGirdType=showType;
	var portfolioIds;
	try{
		$("#showHideType").val("1");
		$('#loadingDiv').show();
	}catch (e) {
		var $j=jQuery.noConflict();
		$j("#showHideType").val("1");
		$j('#loadingDiv').show();
	}
		 setTimeout(function() {
			document.getElementById("teacherIdForprofileGrid").value=teacherId;	
			name=name.replace("$","'");
			try{
				portfolioIds=$("#portfolioIds").val();
				$("#cgTeacherMasterDiv").modal('show');
				$("#candidateName").html(name+"'s Profile");
			}catch (e) {
				var $j=jQuery.noConflict();
				portfolioIds=$j("#portfolioIds").val();
				$j("#cgTeacherMasterDiv").modal('show');
				$j("#candidateName").html(name+"'s Profile");
			}
			SelfServiceCandidateProfileService.showProfileContentNewSSPF(jobforteacherId,teacherId,jobId,noOfRecordCheck,0,portfolioIdAndType,showType,{ 
			async: false,
			callback: function(data){ 
				try{
					$("#cgTeacherBodyDiv").html(data);
					if(showType!='cg')
					{
						showSelfServiceOldCustomQuestion(teacherId);
					}
				}catch (e) {
					var $j=jQuery.noConflict();
					$j("#cgTeacherBodyDiv").html(data);
					if(showType!='cg')
					{
						showSelfServiceOldCustomQuestion(teacherId);
					}
				}
			},
			errorHandler:handleError  
			});
			try{
				$('#loadingDiv').hide();
			}catch (e) {
				var $j=jQuery.noConflict();
				$j('#loadingDiv').hide();
			}
		 }, 10);
}
function personalInfoProfiler(jobId,portfoiloTypeAndId,teacherId,showType,portfolioIds){
	try{
		$("#personalSection").show();
	}catch (e) {
		//var $j1=jQuery.noConflict();
		jQuery.noConflict();
		jQuery("#personalSection").show();
	}
	SelfServiceCandidateProfileService.getPersonalInfoProfiler(jobId,portfoiloTypeAndId,teacherId,showType,portfolioIds,{ 
		async: true,
		callback: function(data){
			try{
				$("#personalSection").html(data);
			}catch (e) {
				//var $j2=jQuery.noConflict();
				jQuery.noConflict();
				jQuery("#personalSection").html(data);
			}
		},
		errorHandler:handleError  
		});
}

function academicsSectionProfiler(teacherId,portfoiloTypeAndId,displayName){
	try{
		$("#academicsSection").show();
	}catch (e) {
		var $j21=jQuery.noConflict();
		$j21("#academicsSection").show();
	}
	SelfServiceCandidateProfileService.getAcademicsSectionProfiler(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		callback: function(data){ 
			try{
				$("#academicsSection").html(data);
			}catch (e) {
				var $j22=jQuery.noConflict();
				$j22("#academicsSection").html(data);
			}
		},
		errorHandler:handleError  
		});
}
function getTeacherAcademicsGrid_DivProfile_sspf(teacherId,portfolioId,candidateType,sectionId)
{
	var isMiami=0;
	var dspqjobflowdistrictId="";
	var transcriptFlag="";
	try{
		$('#gridDataTeacherAcademics').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $j23=jQuery.noConflict();
		$j23('#gridDataTeacherAcademics').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	DSPQServiceAjax.getPFAcademinGridForDspq(dp_Academics_noOfRows,dp_Academics_page,dp_Academics_sortOrderStr,dp_Academics_sortOrderType,isMiami,transcriptFlag,teacherId,portfolioId,candidateType,sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#gridDataTeacherAcademics').html(data);
		}catch (e) {
			var $j24=jQuery.noConflict();
			$j24('#gridDataTeacherAcademics').html(data);
		}finally {
			applyScrollOnTblTeacherAcademics_profile_sspf();
		}
		}});
}
function generalKnowledgeExamSectionProfiler(teacherId,portfoiloTypeAndId,displayName){
	try{
		$("#credentialSection").show();
		$('#credentialSection').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $j25=jQuery.noConflict();
		$j25("#credentialSection").show();
		$j25('#credentialSection').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	SelfServiceCandidateProfileService.getGeneralKnowladgeExamProfiler(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		callback: function(data){  
		try{
			$("#credentialSection").html(data);
		}catch (e) {
			var $j26=jQuery.noConflict();
			$j26("#credentialSection").html(data);
		}
		},
		errorHandler:handleError  
		});
}
function teacherHonors(teacherId,portfoiloTypeAndId,displayName){
	try{
		$("#honorsGridWithoutAction").show();
	}catch (e) {
		var $j31=jQuery.noConflict();
		$j31("#honorsGridWithoutAction").show();
	}
	SelfServiceCandidateProfileService.getTeacherHonors(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		callback: function(data){ 
		try{
			$("#honorsGridWithoutAction").html(data);
		}catch (e) {
			var $j32=jQuery.noConflict();
			$j32("#honorsGridWithoutAction").html(data);
		}
		},
		errorHandler:handleError  
		});	
}

function honorsGridWithoutActionProfiler(teacherId,portfolioId,candidateType,sectionId)
{
	try{
		$('#gridDataTeacherHonors').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $j41=jQuery.noConflict();
		$j41('#gridDataTeacherHonors').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	DSPQServiceAjax.getHonorsGrid(teacherId,portfolioId,candidateType,sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#gridDataTeacherHonors').html(data);
		}catch (e) {
			var $j42=jQuery.noConflict();
			$j42('#gridDataTeacherHonors').html(data);
		}finally{
			applyScrollOnHonors_sspf();
		}
			
		}});
}
function involvementWorkGridProfiler(teacherId,portfoiloTypeAndId,displayName)
{
	try{
		$("#InvolvementWorkGrid").show();
	}catch (e) {
		var $j61=jQuery.noConflict();
		$j61("#InvolvementWorkGrid").show();
	}
	SelfServiceCandidateProfileService.getInvolvementWorkGridProfiler(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#InvolvementWorkGrid').html(data);
		}catch (e) {
			var $j62=jQuery.noConflict();
			$j62('#InvolvementWorkGrid').html(data);
		}
		}});
}

function involvementGridNew(teacherId,portfolioId,candidateType,sectionId)
{
	try{
		$('#gridDataTeacherInvolvementOrVolunteerWork').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $j63=jQuery.noConflict();
		$j63('#gridDataTeacherInvolvementOrVolunteerWork').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	DSPQServiceAjax.getInvolvementGrid(dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,teacherId,portfolioId,candidateType,sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#gridDataTeacherInvolvementOrVolunteerWork').html(data);
			}catch (e) {
				var $j64=jQuery.noConflict();
				$j64('#gridDataTeacherInvolvementOrVolunteerWork').html(data);
			}finally{
				applyScrollOnInvl_sspf();
			}
		}});
}
function employmentGrid(teacherId,portfoiloTypeAndId,displayName){
	try{
		$("#employmentGridDiv").show();
	}catch (e) {
		var $j71=jQuery.noConflict();
		$j71("#employmentGridDiv").show();
	}
	SelfServiceCandidateProfileService.getEmploymentGrid(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#employmentGridDiv').html(data);
		}catch (e) {
			var $j72=jQuery.noConflict();
			$j72('#employmentGridDiv').html(data);
		}
		}});
}
function employmentGridProfiler(teacherId,portfolioId,candidateType,sectionId)
{
	try{
		$('#gridDataWorkExpEmployment').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $j73=jQuery.noConflict();
		$j73('#gridDataWorkExpEmployment').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");// TODO: handle exception
	}
	DSPQServiceAjax.getPFEmploymentGrid(dp_Employment_noOfRows,dp_Employment_page,dp_Employment_sortOrderStr,dp_Employment_sortOrderType,teacherId,portfolioId,candidateType,sectionId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#gridDataWorkExpEmployment').html(data);
		}catch (e) {
			var $j74=jQuery.noConflict();
			$j74('#gridDataWorkExpEmployment').html(data);
		}finally{
			applyScrollOnTblWorkExp_sspf();
		}
		}});
}
function referenceGrid(teacherId,portfoiloTypeAndId,displayName){
	try{
		$("#referenceGridDiv").show();
	}catch (e) {
		var $j81=jQuery.noConflict();
		$j81("#referenceGridDiv").show();
	}
	SelfServiceCandidateProfileService.getReferenceGrid(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#referenceGridDiv').html(data);
		}catch (e) {
			var $j82=jQuery.noConflict();
			$j82('#referenceGridDiv').html(data);
		}
		}});
}
function referenceGridProfiler(teacherId,portfolioId,candidateType,sectionId)
{
	try{
		$('#gridDataReference').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $j83=jQuery.noConflict();
		$j83('#gridDataReference').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	DSPQServiceAjax.getElectronicReferencesGrid(dp_Reference_noOfRows,dp_Reference_page,dp_Reference_sortOrderStr,dp_Reference_sortOrderType,teacherId,portfolioId,candidateType,sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#gridDataReference').html(data);
			}catch (e) {
				var $j84=jQuery.noConflict();
				$j84('#gridDataReference').html(data);
			}finally{
				applyScrollOnTblEleRef_profile_sspf();
			}
		}});
}
function videoLinkGrid(teacherId,portfoiloTypeAndId,displayName,instructions){
	try{
		$("#videoLinkGridDiv").show();
	}catch (e) {
		var $j91=jQuery.noConflict();
		$j91("#videoLinkGridDiv").show();
	}
	SelfServiceCandidateProfileService.getVideoLinkGrid(teacherId,portfoiloTypeAndId,displayName,instructions,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#videoLinkGridDiv').html(data);
			}catch (e) {
				var $j92=jQuery.noConflict();
				$j92('#videoLinkGridDiv').html(data);
			}
		}});	
}
function videoLinkGridProfiler(teacherId,portfolioId,candidateType,sectionId)
{
	try{
		$('#gridDataVideoLink').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $j93=jQuery.noConflict();
		$j93('#gridDataVideoLink').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	DSPQServiceAjax.getVideoLinksGrid(dp_VideoLink_Rows,dp_VideoLink_page,dp_VideoLink_sortOrderStr,dp_VideoLink_sortOrderType,teacherId,portfolioId,candidateType,sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#gridDataVideoLink').html(data);
		}catch (e) {
			var $j94=jQuery.noConflict();
			$j94('#gridDataVideoLink').html(data);
		}finally{
			applyScrollOnTblVideoLinks_profile_sspf();
		}
		}});
}
function languageProfiency(teacherId,portfoiloTypeAndId,displayName){
	try{
		$("#languageProfiencyDiv").show();
	}catch (e) {
		var $jl1=jQuery.noConflict();
		$jl1("#languageProfiencyDiv").show();
	}
	SelfServiceCandidateProfileService.getlanguageProfiency(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#languageProfiencyDiv').html(data);
		}catch (e) {
			var $jl2=jQuery.noConflict();
			$jl2('#languageProfiencyDiv').html(data);
		}
		}});	
}
function languageProfiencyProfiler(teacherId,portfolioId,candidateType,sectionId){
	try{
		$('#divGridlanguageProfiency').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $jl3=jQuery.noConflict();
		$jl3('#divGridlanguageProfiency').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	DSPQServiceAjax.displayTeacherLanguage(dp_TchrLang_Rows,dp_TchrLang_page,dp_TchrLang_sortOrderStr,dp_TchrLang_sortOrderType,teacherId,portfolioId,candidateType,sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
		try{
			$("#divGridlanguageProfiency").html(data);
		}catch (e) {
			var $jl4=jQuery.noConflict();
			$jl4("#divGridlanguageProfiency").html(data);
		}finally{
			applyScrollOnTblLanguage_sspf();
		}
		}
	});
}
function additionalDocuments(teacherId,portfoiloTypeAndId,displayName){
	try{
		$("#additionalDocumentsDiv").show();
	}catch (e) {
		var $ja1=jQuery.noConflict();
		$ja1("#additionalDocumentsDiv").show();
	}
	SelfServiceCandidateProfileService.getAdditionalDocuments(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#additionalDocumentsDiv').html(data);
		}catch (e) {
			var $ja2=jQuery.noConflict();
			$ja2('#additionalDocumentsDiv').html(data);
		}
		}});	
}
function additionalDocumentsProfiler(teacherId,portfolioId,candidateType,sectionId)
{
	try{
		$('#divGridAdditionalDocuments').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $ja3=jQuery.noConflict();
		$ja3('#divGridAdditionalDocuments').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	DSPQServiceAjax.getAdditionalDocumentsGrid(dp_AdditionalDocuments_noOfRows,dp_AdditionalDocuments_page,dp_AdditionalDocuments_sortOrderStr,dp_AdditionalDocuments_sortOrderType,teacherId,portfolioId,candidateType,sectionId,{  
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#divGridAdditionalDocuments').html(data);
		}catch (e) {
			var $ja4=jQuery.noConflict();
			$ja4('#divGridAdditionalDocuments').html(data);
		}finally{
			applyScrollOnTbl_AdditionalDocuments_sspf();
		}
		}});
}
function certificationLicense(teacherId,portfoiloTypeAndId,displayName){
	try{
		$("#certificationLicenseDiv").show();
	}catch (e) {
		var $jc1=jQuery.noConflict();
		$jc1("#certificationLicenseDiv").show();
	}
	SelfServiceCandidateProfileService.getCertificationLicense(teacherId,portfoiloTypeAndId,displayName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#certificationLicenseDiv').html(data);
		}catch (e) {
			var $jc2=jQuery.noConflict();
			$jc2('#certificationLicenseDiv').html(data);
		}
		}});	
}
function teacherCertificationsGrid(teacherId,portfolioId,candidateType,sectionId)
{
	try{
		$('#gridDataTeacherCertifications').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $jc3=jQuery.noConflict();
		$jc3('#gridDataTeacherCertifications').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	DSPQServiceAjax.getPFCertificationsGrid(dp_Certification_noOfRows,dp_Certification_page,dp_Certification_sortOrderStr,dp_Certification_sortOrderType,teacherId,portfolioId,candidateType,sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#gridDataTeacherCertifications').html(data);
			}catch (e) {
				var $jc4=jQuery.noConflict();
				$jc4('#gridDataTeacherCertifications').html(data);
			}finally{
				applyScrollOnTblTeacherCertifications_profile_sspf();
			}
		}
	});
}

function showFooterGrid(jobForTeacherId,teacherId,jobId,noOfRecordCheck,displayMode){
	try{
		$('#gridFooterDiv').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $dd=jQuery.noConflict();
		$dd('#gridFooterDiv').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	SelfServiceCandidateProfileService.getGridFooter(jobForTeacherId,teacherId,jobId,noOfRecordCheck,displayMode,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#gridFooterDiv').html(data);
			}catch (e) {
				var $kk=jQuery.noConflict();
				$kk('#gridFooterDiv').html(data);
			}
		}});	
}
function personalFixedArea(teacherId,jobId){
	try{
		$('#personalFixedAreaDiv').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $k2=jQuery.noConflict();
		$k2('#personalFixedAreaDiv').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	SelfServiceCandidateProfileService.getPersonalSectionFixedArea(teacherId,jobId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#personalFixedAreaDiv').html(data);
			}catch (e) {
				var $k1=jQuery.noConflict();
				$k1('#personalFixedAreaDiv').html(data);
			}
		}});
}

function customQuestion(jobId,portfolioId,candidateType,sectionId,teacherId,showType,portfolioIds)
{
	try{
		$('#customQuestions').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $k3=jQuery.noConflict();
		$k3('#customQuestions').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	SelfServiceCandidateProfileService.getCustomQuestion(jobId,portfolioId,candidateType,sectionId,teacherId,showType,portfolioIds,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#customQuestions').html(data);
			}catch (e) {
				var $k4=jQuery.noConflict();
				$k4('#customQuestions').html(data);
			}
		}});
}

function customQuestionMaster(jobId,portfolioId,candidateType,groupId,sectionId,teacherId,showType,portfolioIds)
{
	//alert("showType ::::::::  "+showType);
	SelfServiceCandidateProfileService.getCustomQuestionMaster(jobId,portfolioId,candidateType,groupId,sectionId,teacherId,showType,portfolioIds,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(sectionId==6){
				try{
					$('#customQuestionsAcademics').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsAcademics').html(data);
				}
			}else if(sectionId==8){
				try{
					$('#customQuestionsCertification').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsCertification').html(data);
				}
			}else if(sectionId==9){
				try{
					$('#customQuestionsReference').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsReference').html(data);
				}
			}else if(sectionId==10){
				try{
					$('#customQuestionsVideoLink').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsVideoLink').html(data);
				}
			}else if(sectionId==11){
				try{
					$('#customQuestionsAdditionalDocuments').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsAdditionalDocuments').html(data);
				}
			}else if(sectionId==13){
				try{
					$('#customQuestionsEmployment').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsEmployment').html(data);
				}
			}else if(sectionId==14){
				try{
					$('#customQuestionsInvolvement').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsInvolvement').html(data);
				}
			}else if(sectionId==15){
				try{
					$('#customQuestionsHonors').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsHonors').html(data);
				}
			}else if(sectionId==18){
				try{
					$('#customQuestionsLanguage').html(data);
				}catch (e) {
					var $i1=jQuery.noConflict();
					$i1('#customQuestionsLanguage').html(data);
				}
			}
		}});
}
function achievementScoreProfiler(achievementScore,entityID,jobId,teacherId){
	try{
		$('#achievementScoreDiv').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $ka1=jQuery.noConflict();
		$ka('#achievementScoreDiv').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;padding-left: 534px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	SelfServiceCandidateProfileService.getAcademicAchievement(achievementScore,entityID,jobId,teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$('#achievementScoreDiv').html(data);
		}catch (e) {
			var $ka2=jQuery.noConflict();
			$ka2('#achievementScoreDiv').html(data);
		}
		}});
}

//****************************************************************************************************
function getRefNotesDivNew(eleRefId)
{
	var districtMaster = null;
	var schoolMaster = null;
	hideProfilePopoverNew();
	document.getElementById("eleRefId").value=eleRefId;
	try{
		$('#myModalReferenceNotesEditorNew').modal('hide');
	}catch (e) {
		var $ref=jQuery.noConflict();
		$ref('#myModalReferenceNotesEditorNew').modal('hide');
	}
	var jobId = document.getElementById("jobId").value;
	SelfServiceCandidateProfileService.getRefeNotesNew(eleRefId,jobId,districtMaster,schoolMaster,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		try{
			$("#divRefNotesInnerNew").html(data);
			$('#myModalReferenceNoteViewNew').modal('show');
			$('#refeNotesIdNew').tooltip();
		}catch (e) {
			var $ref=jQuery.noConflict();
			$ref("#divRefNotesInnerNew").html(data);
			$ref('#myModalReferenceNoteViewNew').modal('show');
		}
	}
	});
}
function showProfilePopoverNew()
{
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	try{
		$("#cgTeacherMasterDiv").modal('show');
		$('#tpResumeprofile').tooltip();
		$('#tpPhoneprofile').tooltip();
		$('#tpPDReportprofile').tooltip();
		$('#tpJSAProfile').tooltip();
		$('#tpTeacherProfileVisitHistory').tooltip();
	}catch (e) {
		var $sp=jQuery.noConflict();
		$sp("#cgTeacherMasterDiv").modal('show');
		$sp('#tpResumeprofile').tooltip();
		$sp('#tpPhoneprofile').tooltip();
		$sp('#tpPDReportprofile').tooltip();
		$sp('#tpJSAProfile').tooltip();
		$sp('#tpTeacherProfileVisitHistory').tooltip();
	}
}
function hideProfilePopoverNew()
{
	try{
		$("#cgTeacherMasterDiv").modal('hide');
	}catch (e) {
		var $hp=jQuery.noConflict();
		$hp("#cgTeacherMasterDiv").modal('hide');
	}
}

function getNotesEditorDivNew()
{
	try{
		$('#myModalReferenceNoteViewNew').modal('hide');
		$('#errordivNotes_refNew').empty();
		$('#divTxtNode_refNew').find(".jqte_editor").html("");	
		$('#divTxtNode_refNew').find(".jqte_editor").css("background-color", "");
		$('#myModalReferenceNotesEditorNew').modal('show');
	}catch (e) {
		var $ne=jQuery.noConflict();
		$ne('#myModalReferenceNoteViewNew').modal('hide');
		$ne('#errordivNotes_refNew').empty();
		$ne('#divTxtNode_refNew').find(".jqte_editor").html("");	
		$ne('#divTxtNode_refNew').find(".jqte_editor").css("background-color", "");
		$ne('#myModalReferenceNotesEditorNew').modal('show');
	}
	removeRefeNotesFileNew();
}
function closeRefNotesDivEditorNew()
{
	var eleRefId=document.getElementById("eleRefId").value;
	getRefNotesDivNew(eleRefId);
}
function removeRefeNotesFileNew()
{
	try{
		$('#fileRefNotesNew').empty();
		$('#fileRefNotesNew').html("<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
	}catch (e) {
		var $jk=jQuery.noConflict();
		$jk('#fileRefNotesNew').empty();
		$jk('#fileRefNotesNew').html("<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
	}
}
function saveReferenceNotesNew(){
	var noteFileName="";
	var notes = "";
	try{
		notes = $('#divTxtNode_refNew').find(".jqte_editor").html();
	}catch (e) {
		var $rn=jQuery.noConflict();
		notes = $rn('#divTxtNode_refNew').find(".jqte_editor").html();
	}
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	var cnt=0;
	var focs=0;
	var notedata="";
	try{
		notedata=$('#divTxtNode_refNew').find(".jqte_editor").text().trim();
		$('#errordivNotes_refNew').empty();
	}catch (e) {
		var $rn0=jQuery.noConflict();
		notedata=$rn0('#divTxtNode_refNew').find(".jqte_editor").text().trim();
		$rn0('#errordivNotes_refNew').empty();
	}
	if (notedata=="")
	{
		try{
			$('#errordivNotes_refNew').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
			if(focs==0)
				$('#divTxtNode_refNew').find(".jqte_editor").focus();
			$('#divTxtNode_refNew').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;
		}catch (e) {
			var $rn1=jQuery.noConflict();
			$rn1('#errordivNotes_refNew').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
			if(focs==0)
				$rn1('#divTxtNode_refNew').find(".jqte_editor").focus();
			$rn1('#divTxtNode_refNew').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;
		}
	}else if(fileNote==""){
		try{
			$('#errordivNotes_refNew').show();
			$('#errordivNotes_refNew').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
			cnt++;
		}catch (e) {
			var $rn2=jQuery.noConflict();
			$rn2('#errordivNotes_refNew').show();
			$rn2('#errordivNotes_refNew').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
			cnt++;
		}
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote_ref").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote_ref").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			try{
				$('#errordivNotes_refNew').show();
				$('#errordivNotes_refNew').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
				cnt++;
			}catch (e) {
				var $rn3=jQuery.noConflict();
				$rn3('#errordivNotes_refNew').show();
				$rn3('#errordivNotes_refNew').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
				cnt++;
			}
		}
		else if(fileSize>=10485760)
		{
			try{
				$('#errordivNotes_refNew').show();
				$('#errordivNotes_refNew').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
				cnt++;
			}catch (e) {
				var $rn4=jQuery.noConflict();
				$rn4('#errordivNotes_refNew').show();
				$rn4('#errordivNotes_refNew').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
				cnt++;
			}
		}
	}
	if(cnt==0){	
		try{
			try{
				$('#loadingDiv').show();
			}catch (e) {
				var $rn5=jQuery.noConflict();
				$rn5('#loadingDiv').show();
			}
			if(fileNote!="" && fileNote!=null)
			{
				document.getElementById("frmNoteUpload_ref").submit();
			}
			else
			{
				SelfServiceCandidateProfileService.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						try{
							$('#loadingDiv').hide();
						}catch (e) {
							var $rn6=jQuery.noConflict();
							$rn6('#loadingDiv').hide();
						}finally{
							getRefNotesDivNew(eleRefId);
						}
					}
				});	
			}
		}catch(err){}
	}
}

function getTeacherProfileVisitHistoryShow_FTimeNew(teacherId){
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	currentPageFlag="tpvh";
	document.getElementById("gridNameFlag").value="tpvh";
	getPagingAndSorting('1',"visitedDateTime",1);
	hideProfilePopoverNew();
}
function getJobListNew(tId){
	hideProfilePopoverNew();
	defaultSetNew();
	currentPageFlag='job';
	teacherId=tId;
	//document.getElementById("loadingDiv").style.display="block";
	//$('#loadingDiv').show();
	getJobOrderListNew();
}
function defaultSetNew(){
	pageForTC = 1;
	noOfRowsForTC =100;
	noOfRowsForJob=10;
	sortOrderStrForTC="";
	sortOrderTypeForTC="";
	currentPageFlag="";
}
function getJobOrderListNew(){
	try{
		$('#loadingDiv').show();
	}catch (e) {
		var $jol=jQuery.noConflict();
		$jol('#loadingDiv').show();
	}
	//document.getElementById("loadingDiv").style.display="block";
	currentPageFlag='job';
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	alert
	if(visitLocation=='My Folders'){
		//TeacherInfotAjax
		CGServiceAjax.getJobListByTeacherByProfile(teacherId,noOfRowsForJob,pageJob,sortOrderStrJob,sortOrderTypeJob,false,visitLocation,0,0, { 
			async: true,
			callback: function(data)
			{
			try{
				$('#loadingDiv').hide();	
				$('#myModalJobListNew').modal('show');
				$("#divJobNew").html(data);
			}catch(err){
				var $jol1=jQuery.noConflict();
				$jol1('#loadingDiv').hide();	
				$jol1('#myModalJobListNew').modal('show');
				$jol1("#divJobNew").html(data);
			}
			applyScrollOnJob_sspf();
			jobEnableNew_sspf();
			try{
				$('#loadingDiv').hide();
			}catch (e) {
				var $jol4=jQuery.noConflict();
				$jol4('#loadingDiv').hide();
			}
			},
			errorHandler:handleError 
		});
	}else{
		//TeacherInfotAjax
		CGServiceAjax.getJobListByTeacher(teacherId,noOfRowsForJob,pageJob,sortOrderStrJob,sortOrderTypeJob,false, { 
			async: true,
			callback: function(data)
			{
				try{
					$('#loadingDiv').hide();	
					$('#myModalJobListNew').modal('show');
					$("#divJobNew").html(data);
				}catch(err){
					var $jol2=jQuery.noConflict();
					$jol2('#loadingDiv').hide();	
					$jol2('#myModalJobListNew').modal('show');
					$jol2("#divJobNew").html(data);
				}
				//document.getElementById("myModalJobListNew").style.display="block";
				//document.getElementById("divJobNew").innerHTML=data;
				applyScrollOnJob_sspf();
				jobEnableNew_sspf();
				try{
					$('#loadingDiv').hide();
				}catch (e) {
					var $jol3=jQuery.noConflict();
					$jol3('#loadingDiv').hide();
				}
				
			},
			errorHandler:handleError 
		});
	}
}
function jobEnableNew_sspf()
{
	var noOrRow = document.getElementById("jobTable").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{ 
		try{
			$('#jobCom'+j).tooltip();
			$('#tpJSA'+j).tooltip();
			$('#hireTp'+j).tooltip();
			$('#jobCL'+j).tooltip();
			$('#jobTrans'+j).tooltip();
			$('#jobCert'+j).tooltip();
			$('#jobJobId'+j).tooltip();
			$('#jobJobTitle'+j).tooltip();
		}catch (e) {
			var $jE=jQuery.noConflict();
			$jE('#jobCom'+j).tooltip();
			$jE('#tpJSA'+j).tooltip();
			$jE('#hireTp'+j).tooltip();
			$jE('#jobCL'+j).tooltip();
			$jE('#jobTrans'+j).tooltip();
			$jE('#jobCert'+j).tooltip();
			$jE('#jobJobId'+j).tooltip();
			$jE('#jobJobTitle'+j).tooltip();
		}
	}
}

function setPageFlagNew()
{	
	try{
		$('#myModalJobListNew').hide();
	}catch (e) {
		var $pf=jQuery.noConflict();
		$pf('#myModalJobListNew').hide();
	}
	//document.getElementById("myModalJobListNew").style.display="none";
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	if(teacherIdForprofileGrid!='')
	{
		showProfilePopoverNew();
	}
}

function getTeacherProfileVisitHistoryShowNew(teacherId){
	//document.getElementById("loadingDiv").style.display="block";
	currentPageFlag="tpvh";
	try{
		$('#loadingDiv').show();
		$('#myModalProfileVisitHistoryShowNew').modal('show');
		$("#gridNameFlag").val("tpvh");
	}catch (e) {
		var $tf=jQuery.noConflict();
		$tf('#loadingDiv').show();
		$tf('#myModalProfileVisitHistoryShowNew').modal('show');
		$tf("#gridNameFlag").val("tpvh");
	}
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	SelfServiceCandidateProfileService.getGridDataForTeacherProfileVisitHistoryByTeacher(teacherId,noOfRowstpvh,pagetpvh,sortOrderStrtpvh,sortOrderTypetpvh,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#divteacherprofilevisithistoryNew').html(data);
				$('#loadingDiv').hide();
			}catch(e){
				var $tf0=jQuery.noConflict();
				$tf0('#divteacherprofilevisithistoryNew').html(data);
				$tf0('#loadingDiv').hide();
			}finally{
				applyScrollOnTblProfileVisitHistory_sspf();
			}
		}});
}
function updateAScoreNew(teacherId,districtId,jobId,achievementScore,entityID){
	
	var slider_aca_ach=0;
	var slider_lea_res=0;
	try{
		var ifrm_aca_ach = document.getElementById('ifrm_aca_ach');
		var innerNorm = ifrm_aca_ach.contentDocument || ifrm_aca_ach.contentWindow.document;
		if(innerNorm.getElementById('slider_aca_ach').value!=''){
			slider_aca_ach=innerNorm.getElementById('slider_aca_ach').value;
		}
		}catch(e){}
		
		
	try{
		var ifrm_lea_res = document.getElementById('ifrm_lea_res');
		var innerNorm1 = ifrm_lea_res.contentDocument || ifrm_lea_res.contentWindow.document;
		if(innerNorm1.getElementById('slider_lea_res').value!=''){
			slider_lea_res=innerNorm1.getElementById('slider_lea_res').value;
		}
		}catch(e){}
	
		var txt_sl_slider_aca_ach = document.getElementById('txt_sl_slider_aca_ach').value;
		var txt_sl_slider_lea_res = document.getElementById('txt_sl_slider_lea_res').value;
		
		if(txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res){
			SelfServiceCandidateProfileService.getUpdateAndSaveAScore(teacherId,districtId,slider_aca_ach,slider_lea_res,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data!="")
					{
						document.getElementById('txt_sl_slider_aca_ach').value=slider_aca_ach;
						document.getElementById('txt_sl_slider_lea_res').value=slider_lea_res;
						try{
							$('#ascore_profile').html(data[0]);
							$('#lrscore_profile').html(data[1]);
							$("#tdCGViewAScore_"+teacherId).html(data[0]);
							$("#tdCGViewlrScore_"+teacherId).html(data[1]);
						}catch (e) {
							var $us=jQuery.noConflict();
							$us('#ascore_profile').html(data[0]);
							$us('#lrscore_profile').html(data[1]);
							$us("#tdCGViewAScore_"+teacherId).html(data[0]);
							$us("#tdCGViewlrScore_"+teacherId).html(data[1]);
						}finally{
						achievementScoreProfiler(achievementScore,entityID,jobId,teacherId);
						//personalFixedArea(teacherId,jobId);
						}
					}
					
				}});
		}
}
//**************** Adding video link and reference  **********************************************************
function showVideoLinksFormSS()
{
	try{
		$("#divvideoLinks").show();
		$('#frmvideoLinks')[0].reset();
	}catch (e) {
		var $j=jQuery.noConflict();
		$j("#divvideoLinks").show();
		$j('#frmvideoLinks')[0].reset();

	}
}
function hideVideoLinksFormSS(){
	try{
		$("#divvideoLinks").hide();
	}catch (e) {
		var $k=jQuery.noConflict();
		$k("#divvideoLinks").hide();
	}
}
function showElectronicReferencesFormSS(){
	try{
		$("#addReferenceDiv").show();
		$("#divMainForm_ref").show();
	}catch (e) {
		var $l=jQuery.noConflict();
		$l("#addReferenceDiv").show();
		$l("#divMainForm_ref").show();
	}
}
function hideElectronicReferencesFormSS(){
	try{
		$("#addReferenceDiv").hide();
		$("#divMainForm_ref").hide();
	}catch (e) {
		var $m=jQuery.noConflict();
		$m("#addReferenceDiv").hide();
		$m("#divMainForm_ref").hide();
	}
}
//***************** For the reference *********************************************
function saveReference(fileName,sbtsource)
{
	insertOrUpdateElectronicReferencesNew(fileName);
}
function insertOrUpdateElectronicReferencesNew(UploadedFileName)
{
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	var Ref_Section=0;
	var Ref_Section_Name="tooltipSection09";
	var statusFlag=false;
	try{
		statusFlag=$("#divMainForm_ref").is(':visible');
	}catch (e) {
		var $ue1=jQuery.noConflict();
		statusFlag=$ue1("#divMainForm_ref").is(':visible');
	}
	if(statusFlag==true)
	{
		var InputCtrlId_elerefAutoId="elerefAutoId";
		var elerefAutoId=getInputCtrlTextValue(InputCtrlId_elerefAutoId);
		if(elerefAutoId<=0){
			try{
				elerefAutoId=$("#teacherIdRefSS").val();
			}catch (e) {
				var $ue=jQuery.noConflict();
				elerefAutoId=$ue("#teacherIdRefSS").val();
			}
		}
		var InputCtrlId_salutation="salutation";
		var salutation=getInputCtrlTextValue(InputCtrlId_salutation);
		iReturnValidateValue=validateInputListControlStr(salutation,InputCtrlId_salutation,"lblFieldId_"+InputCtrlId_salutation,"lblFieldIdWarning_"+InputCtrlId_salutation);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_firstName="firstName";
		var firstName=getInputCtrlTextValue(InputCtrlId_firstName);
		iReturnValidateValue=validateInputTextControlStr(firstName,InputCtrlId_firstName,"lblFieldId_"+InputCtrlId_firstName,"lblFieldIdWarning_"+InputCtrlId_firstName);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_lastName="lastName";
		var lastName=getInputCtrlTextValue(InputCtrlId_lastName);
		iReturnValidateValue=validateInputTextControlStr(lastName,InputCtrlId_lastName,"lblFieldId_"+InputCtrlId_lastName,"lblFieldIdWarning_"+InputCtrlId_lastName);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_designation="designation";
		var designation=getInputCtrlTextValue(InputCtrlId_designation);
		iReturnValidateValue=validateInputTextControlStr(designation,InputCtrlId_designation,"lblFieldId_"+InputCtrlId_designation,"lblFieldIdWarning_"+InputCtrlId_designation);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_organization="organization";
		var organization=getInputCtrlTextValue(InputCtrlId_organization);
		iReturnValidateValue=validateInputTextControlStr(organization,InputCtrlId_organization,"lblFieldId_"+InputCtrlId_organization,"lblFieldIdWarning_"+InputCtrlId_organization);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var InputCtrlId_contactnumber="contactnumber";
		var contactnumber=getInputCtrlTextValue(InputCtrlId_contactnumber);
		iReturnValidateValue=validateInputTextControlStr(contactnumber,InputCtrlId_contactnumber,"lblFieldId_"+InputCtrlId_contactnumber,"lblFieldIdWarning_"+InputCtrlId_contactnumber);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		
		var InputCtrlId_email="email";
		var email=getInputCtrlTextValue(InputCtrlId_email);
		iReturnValidateValue=validateInputTextControlStrEmailAddress(email,InputCtrlId_email,"lblFieldId_"+InputCtrlId_email,"lblFieldIdWarning_"+InputCtrlId_email);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var pathOfReferenceFile="";
		if(UploadedFileName==undefined)
		{
			var pathOfReference=getInputCtrlTextValue("pathOfReference");
			var InputCtrlId_pathOfReferenceFile="pathOfReferenceFile";
			pathOfReferenceFile=getInputCtrlTextValue(InputCtrlId_pathOfReferenceFile);
			iReturnValidateValue=validateInputFileControlStr(pathOfReferenceFile,InputCtrlId_pathOfReferenceFile,"lblFieldId_"+InputCtrlId_pathOfReferenceFile,"lblFieldIdWarning_"+InputCtrlId_pathOfReferenceFile,pathOfReference);
			if(iReturnValidateValue=="1")
			{
				Ref_Section=Ref_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
			}
		}
		
		var InputCtrlId_rdcontacted="rdcontacted";
		var rdcontacted=getInputCtrlRadioValue(InputCtrlId_rdcontacted);
		iReturnValidateValue=validateInputRadioControlStr(rdcontacted,InputCtrlId_rdcontacted,"lblFieldId_"+InputCtrlId_rdcontacted,"lblFieldIdWarning_"+InputCtrlId_rdcontacted,"hdn_"+InputCtrlId_rdcontacted);
		if(iReturnValidateValue=="1")
		{
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		var rdcontacted_value=false;
		if(rdcontacted=="1")
			rdcontacted_value=true;
		
		var InputCtrlId_longHaveYouKnow="longHaveYouKnow";
		var longHaveYouKnow=getInputCtrlTextValue(InputCtrlId_longHaveYouKnow);
		iReturnValidateValue=validateInputTextControlStr(longHaveYouKnow,InputCtrlId_longHaveYouKnow,"lblFieldId_"+InputCtrlId_longHaveYouKnow,"lblFieldIdWarning_"+InputCtrlId_longHaveYouKnow);
		if(iReturnValidateValue=="1"){
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		
		var InputCtrlId_referenceDetailText="referenceDetailText";
		var referenceDetailText=getTextAreaEditorCtrlTextValue(InputCtrlId_referenceDetailText);
		iReturnValidateValue=validateInputTextAreaControlStr(referenceDetailText,InputCtrlId_referenceDetailText,"lblFieldId_"+InputCtrlId_referenceDetailText,"lblFieldIdWarning_"+InputCtrlId_referenceDetailText);
		if(iReturnValidateValue=="1"){
			Ref_Section=Ref_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Ref_Section;
		}
		
		
		if(Ref_Section>0){
			window.location.hash = '#lblSectionId_'+Ref_Section_Name;
			try{
				$("#lblSectionId_"+Ref_Section_Name).addClass("DSPQRequired12");
			}catch (e) {
				var $ue2=jQuery.noConflict();
				$ue2("#lblSectionId_"+Ref_Section_Name).addClass("DSPQRequired12");
			}
			Ref_Section=0;
		}else{
			try{
				$("#lblSectionId_"+Ref_Section_Name).removeClass("DSPQRequired12");
			}catch (e) {
				var $ue3=jQuery.noConflict();
				$ue3("#lblSectionId_"+Ref_Section_Name).removeClass("DSPQRequired12");
			}
		}
		var visibleflag=false;
		try{
			visibleflag=$("#divMainForm_ref").is(':visible');
		}catch (e) {
			var $ue5=jQuery.noConflict();
			visibleflag=$ue5("#divMainForm_ref").is(':visible');
		}
		if(iReturnValidateTotalValue==0 && visibleflag==true)
		{
			var portfolioId=0;
			try{
				$("#candidateProfilePortfolioId").val();
			}catch (e) {
				var $ue4=jQuery.noConflict();
				$ue4("#candidateProfilePortfolioId").val();
			}
			
			var candidateType="E";
			try{
				$("#candidateProfileCandidateType").val();
				$('#loadingDiv').show();
			}catch (e) {
				var $ue6=jQuery.noConflict();
				$ue6("#candidateProfileCandidateType").val();
				$ue6('#loadingDiv').show();
			}
			if(pathOfReferenceFile!=""){
				document.getElementById("frmElectronicReferences").submit();
			}else{
				var teacherIdRefSS=0;
				try{
					teacherIdRefSS=$("#teacherIdRefSS").val();
				}catch (e) {
					var $ue7=jQuery.noConflict();
					teacherIdRefSS=$ue7("#teacherIdRefSS").val();
				}
				var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,canContOnOffer:null,referenceDetails:null,longHaveYouKnow:null};
				dwr.engine.beginBatch();
				dwr.util.getValues(teacherElectronicReferences);
				
				/*if(elerefAutoId >0)
					teacherElectronicReferences.elerefAutoId=elerefAutoId;*/
				
				teacherElectronicReferences.salutation=salutation;
				teacherElectronicReferences.firstName=firstName;
				teacherElectronicReferences.lastName=lastName;;
				teacherElectronicReferences.designation=designation;
				teacherElectronicReferences.organization=organization;
				teacherElectronicReferences.email=email;
				teacherElectronicReferences.contactnumber=contactnumber;
				teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
				teacherElectronicReferences.referenceDetails=referenceDetailText;
				teacherElectronicReferences.rdcontacted=rdcontacted_value;
				teacherElectronicReferences.pathOfReference=UploadedFileName;
				
				SelfServiceCandidateProfileService.saveOrUpdateElectronicReferencesProfiler(teacherElectronicReferences,elerefAutoId,{ 
					async: true,
					errorHandler:handleError,
					callback: function(data)
					{
					    hideElectronicReferencesFormSS();
					    referenceGridProfiler(elerefAutoId,portfolioId,candidateType,9);
					}
				});
				dwr.engine.endBatch();
			}
			try{
				$('#loadingDiv').hide();
			}catch (e) {
				var $ue8=jQuery.noConflict();
				$ue8('#loadingDiv').hide();
			}
		}
	}
	return false;
}
//********************** For the video link *****************************************************
function saveVideo(UploadedFileName){
	insertOrUpdatevideoLinksNew(UploadedFileName);
}

function insertOrUpdatevideoLinksNew(UploadedFileName)
{
	isSetFocus=false;
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	var Video_Section=0;
	var Video_Section_Name="tooltipSection10";
	var portfolioId=0;
	var candidateType="";
	var visibleStatus=false;
	try{
		portfolioId=$("#candidateProfilePortfolioId").val();
		candidateType=$("#candidateProfileCandidateType").val();
		visibleStatus=$("#divvideoLinks").is(':visible');
	}catch (e) {
		var $uv=jQuery.noConflict();
		portfolioId=$uv("#candidateProfilePortfolioId").val();
		candidateType=$uv("#candidateProfileCandidateType").val();
		visibleStatus=$uv("#divvideoLinks").is(':visible');
	}
	if(visibleStatus)
	{
		var videolinkAutoId=getInputCtrlTextValue("videolinkAutoId");
		var teacherId=0;
		try{
			teacherId=$("#teacherIdVideoSS").val();
		}catch (e) {
			var $uv1=jQuery.noConflict();
			teacherId=$uv1("#teacherIdVideoSS").val();
		}
		var InputCtrlId_videourl="videourl";
		var videourl=getInputCtrlTextValue(InputCtrlId_videourl);
		iReturnValidateValue=validateInputTextControlStr(videourl,InputCtrlId_videourl,"lblFieldId_"+InputCtrlId_videourl,"lblFieldIdWarning_"+InputCtrlId_videourl);
		if(iReturnValidateValue=="1"){
			Video_Section=Video_Section+1;
			iReturnValidateTotalValue=iReturnValidateTotalValue+Video_Section;
		}
		
		var videofile="";
		if(UploadedFileName==undefined){
			var dbvideofile=getInputCtrlTextValue("dbvideofile");
			var InputCtrlId_videofile="videofile";
			videofile=getInputCtrlTextValue(InputCtrlId_videofile);
			iReturnValidateValue=validateInputVideoFileControlStr(videofile,InputCtrlId_videofile,"lblFieldId_"+InputCtrlId_videofile,"lblFieldIdWarning_"+InputCtrlId_videofile,dbvideofile);
			if(iReturnValidateValue=="1"){
				Video_Section=Video_Section+1;
				iReturnValidateTotalValue=iReturnValidateTotalValue+Video_Section;
			}
		}
		
		if(Video_Section>0){
			try{
				$("#lblSectionId_"+Video_Section_Name).addClass("DSPQRequired12");
			}catch (e) {
				var $uv2=jQuery.noConflict();
				$uv2("#lblSectionId_"+Video_Section_Name).addClass("DSPQRequired12");
			}
			Video_Section=0;
		}else{
			try{
				$("#lblSectionId_"+Video_Section_Name).removeClass("DSPQRequired12");
			}catch (e) {
				var $uv3=jQuery.noConflict();
				$uv3("#lblSectionId_"+Video_Section_Name).removeClass("DSPQRequired12");
			}
		}
		var videovisible=$("#divvideoLinks").is(':visible');
		if(iReturnValidateTotalValue==0 && videovisible==true){
			try{
				$('#loadingDiv').show();
			}catch (e) {
				var $uv4=jQuery.noConflict();
				$uv4('#loadingDiv').show();
			}
			if(videofile!=""){
				document.getElementById("frmvideoLinks").submit();
			}else{
				if(UploadedFileName==null || UploadedFileName=="" || UploadedFileName==undefined)
					UploadedFileName=dbvideofile;
				
				var teacherVideoLink = {videolinkAutoId:null,videourl:null, video:null, createdDate:null};
				dwr.engine.beginBatch();
				dwr.util.getValues(teacherVideoLink);
				teacherVideoLink.videolinkAutoId=videolinkAutoId;
				teacherVideoLink.videourl=videourl;		
				teacherVideoLink.video=	UploadedFileName;	
				SelfServiceCandidateProfileService.saveOrUpdateVideoLinksProfiler(teacherVideoLink,videolinkAutoId,teacherId,{ 
					async: false,
					errorHandler:handleError,
					callback: function(data){								
						hideVideoLinksFormSS();
						videoLinkGridProfiler(teacherId,portfolioId,candidateType,10);
						//$('#loadingDiv').hide();
					}
				});
				dwr.engine.endBatch();
				return true;
			
			}
			try{
				$('#loadingDiv').hide();
			}catch (e) {
				var $uv5=jQuery.noConflict();
				$uv5('#loadingDiv').hide();
			}
		}
	}
	return iReturnValidateTotalValue;
}
function getInputCtrlTextValue(InputCtrlTextId)
{
	var inputCtrlValue="";
	try { 
			inputCtrlValue=trim($("#"+InputCtrlTextId).val());
		} catch (e) {
			var $ctv=jQuery.noConflict();
			inputCtrlValue=trim($ctv("#"+InputCtrlTextId).val());
		}
	return inputCtrlValue;
}
function validateInputTextControlStr(InputValue,Id1,Id2,Id3)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try { 
			InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); 
		}catch (e){
			var $it=jQuery.noConflict();
			InputValue_dspqRequired=$it("#"+Id1).attr("dspqRequired");
		}
	if(InputValue_dspqRequired=="1")
	{
		if(InputValue=="")
		{
			try{
				$("#"+Id2).addClass("DSPQRequired12");
				$("#"+Id3).addClass("icon-warning DSPQRequired12");
				$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
				$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			}catch (e) {
				var $it1=jQuery.noConflict();
				$it1("#"+Id2).addClass("DSPQRequired12");
				$it1("#"+Id3).addClass("icon-warning DSPQRequired12");
				$it1("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
				$it1("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
			}
			iReturnValidateValue=1;
			
			if(!isSetFocus)
			{
				try{
					$("#"+Id1).focus();
				}catch (e) {
					var $it2=jQuery.noConflict();
					$it2("#"+Id1).focus();
				}
				isSetFocus=true;
			}
		}
		else
		{
			try{
				$("#"+Id2).removeClass("DSPQRequired12");
				$("#"+Id3).removeClass("icon-warning DSPQRequired12");
				$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
				$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			}catch (e) {
				var $it3=jQuery.noConflict();
				$it3("#"+Id2).removeClass("DSPQRequired12");
				$it3("#"+Id3).removeClass("icon-warning DSPQRequired12");
				$it3("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
				$it3("#lblFieldIdWarningAfterSpace_"+Id1).html("");
			}
			iReturnValidateValue=0;
		}
	}
	return iReturnValidateValue;
}
function validateInputVideoFileControlStr(InputValue,Id1,Id2,Id3,edithidden_file)
{
	var iReturnValidateValue=0;
	var InputValue_dspqRequired=0;
	try{
		InputValue_dspqRequired=$("#"+Id1).attr("dspqRequired"); 
		}catch (e){
			var $iv=jQuery.noConflict();
			InputValue_dspqRequired=$iv("#"+Id1).attr("dspqRequired");
		}
	if(InputValue_dspqRequired=="1")
	{
		if(InputValue=="" && (edithidden_file=="0" || edithidden_file==""))
		{
			iReturnValidateValue=1;
		}
	}
		
	if(InputValue!="")
	{
		var ext = InputValue.substr(InputValue.lastIndexOf('.') + 1).toLowerCase();
		var fileSize=0;		
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{		
			if(document.getElementById(Id1).files[0]!=undefined)
				fileSize = document.getElementById(Id1).files[0].size;
		}
		
		if(ext!="")
		{
			if(!(ext=="mp4"||ext=="MP4"||ext=="ogv"||ext=="ogg"||ext=="webm"||ext=="wmv"))
			{
				
				iReturnValidateValue=1;
			}	
			else if(fileSize>=10485760)
			{
				iReturnValidateValue=1;
			}
			else
			{
				iReturnValidateValue=0;
			}
		}
	}
	
	if(iReturnValidateValue==0)
	{
		try{
			$("#"+Id2).removeClass("DSPQRequired12");
			$("#"+Id3).removeClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("");
		}catch (e) {
			var $iv1=jQuery.noConflict();
			$iv1("#"+Id2).removeClass("DSPQRequired12");
			$iv1("#"+Id3).removeClass("icon-warning DSPQRequired12");
			$iv1("#lblFieldIdWarningBeforeSpace_"+Id1).html("");
			$iv1("#lblFieldIdWarningAfterSpace_"+Id1).html("");
		}
		
		iReturnValidateValue=0;
	}
	else
	{
		try{
			$("#"+Id2).addClass("DSPQRequired12");
			$("#"+Id3).addClass("icon-warning DSPQRequired12");
			$("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
		}catch (e) {
			var $iv2=jQuery.noConflict();
			$iv2("#"+Id2).addClass("DSPQRequired12");
			$iv2("#"+Id3).addClass("icon-warning DSPQRequired12");
			$iv2("#lblFieldIdWarningBeforeSpace_"+Id1).html("&nbsp;");
			$iv2("#lblFieldIdWarningAfterSpace_"+Id1).html("&nbsp;");
		}
		if(!isSetFocus)
		{
			try{
				$("#"+Id1).focus();
			}catch (e) {
				var $iv3=jQuery.noConflict();
				$iv3("#"+Id1).focus();
			}
			isSetFocus=true;
		}
	}
	
	return iReturnValidateValue;
	
}
//***********************************************************************************************
function openSectionCertificationLicense(jobId,portfolioId,candidateType,sectionId,teacherId,showType,portfolioIds){
	try{
		$("#openSectionCertification").show();
	}catch (e) {
		var $jO0=jQuery.noConflict();
		$jO0("#openSectionCertification").show();
	}
	SelfServiceCandidateProfileService.getGroup3OpentextProfiler(jobId,portfolioId,candidateType,sectionId,teacherId,showType,portfolioIds,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				$('#openSectionCertification').html(data);
			}catch (e) {
				var $jO1=jQuery.noConflict();
				$jO1('#openSectionCertification').html(data);
			}
		}});
}
//******************** Grid header functions *************************************
function applyScrollOnTblTeacherCertifications_profile_sspf()
{
    var gridValue=checkWindowWidth('Certifications');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#tblGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
        colratio:colArr, 
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });
	}else{
		try{
		$(document).ready(function() {
	        $('#tblGridCertifications').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 380,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:colArr, 
		    addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });
	            
	        });
		}catch (e) {
			var $jO3=jQuery.noConflict();
			$jO3(document).ready(function() {
			$jO3('#tblGridCertifications').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 380,
		        width: gridSize,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:colArr, 
			    addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
		        });
		            
		        });
		 }
	}
}
function applyScrollOnTblEleRef_profile_sspf()
{
    var gridValue=checkWindowWidth('References');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#eleReferencesGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
          colratio:colArr, 
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });	
	}else{
		try{
	        $(document).ready(function() {
	        $('#eleReferencesGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 350,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	          colratio:colArr, 
		    addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 70,
	        wrapper: false
	        });
	            
	        });
		}catch (e) {
				var $j80=jQuery.noConflict();
				$j80(document).ready(function() {
		        $j80('#eleReferencesGrid').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 350,
		        width: gridSize,
		        minWidth: null,
		        minWidthAuto: false,
		          colratio:colArr, 
			    addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 70,
		        wrapper: false
		        });
		        });
		}
	}
}
function applyScrollOnTblVideoLinks_profile_sspf()
{
    var gridValue=checkWindowWidth('Video');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#videoLinktblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
        colratio:colArr,
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });		
	}else{
		try{
		        $(document).ready(function() {
		        $('#videoLinktblGrid').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 350,
		        width: gridSize,
		        minWidth: null,
		        minWidthAuto: false,
		          colratio:colArr,
			    addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 70,
		        wrapper: false
		        });
		            
		        });	
		}catch (e) {
				var $j94=jQuery.noConflict();
				$j94(document).ready(function() {
			        $j94('#videoLinktblGrid').fixheadertable({ //table id 
			        caption: '',
			        showhide: false,
			        theme: 'ui',
			        height: 350,
			        width: gridSize,
			        minWidth: null,
			        minWidthAuto: false,
			          colratio:colArr,
				    addTitles: false,
			        zebra: true,
			        zebraClass: 'net-alternative-row',
			        sortable: false,
			        sortedColId: null,
			        //sortType:[],
			        dateFormat: 'd-m-y',
			        pager: false,
			        rowsPerPage: 10,
			        resizeCol: false,
			        minColWidth: 70,
			        wrapper: false
			        });
			            
			        });	
		}
	}
}
function applyScrollOnTbl_AdditionalDocuments_sspf()
{
    var gridValue=checkWindowWidth('Additional');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
        colratio:colArr,
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });		
	}else{
		try{
	        $(document).ready(function() {
	        $('#additionalDocumentsGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 380,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:colArr,
		    addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });
	        });		
		}catch (e) {
				var $ja0=jQuery.noConflict();
				$ja0(document).ready(function() {
		        $ja0('#additionalDocumentsGrid').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 380,
		        width: gridSize,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:colArr,
			    addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
		        });
		        });		
		}
	}
}
function applyScrollOnTblTeacherAcademics_profile_sspf()
{
    
    var gridValue=checkWindowWidth('Academics');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#academicGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
        colratio:colArr,
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });		
	}else{
		try{
		$(document).ready(function() {
		$('#academicGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
        colratio:colArr,
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });		
	}catch (e) {
		var $j55=jQuery.noConflict();
			$j55(document).ready(function() {
			$j55('#academicGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 380,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:colArr,
		    addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });
	        });		
	}
 }
}
function applyScrollOnTblLanguage_sspf()
{
    var gridValue=checkWindowWidth('Languages');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
	    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
          colratio:colArr,
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
        });	
	}else{
		try{
	        $(document).ready(function() {
	        $('#teacherLanguageGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 350,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	          colratio:colArr,
		    addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 70,
	        wrapper: false
	        });
	            
	        });	
		}catch (e) {
				var $jl0=jQuery.noConflict();
					$jl0(document).ready(function() {
			        $jl0('#teacherLanguageGrid').fixheadertable({ //table id 
			        caption: '',
			        showhide: false,
			        theme: 'ui',
			        height: 350,
			        width: gridSize,
			        minWidth: null,
			        minWidthAuto: false,
			          colratio:colArr,
				    addTitles: false,
			        zebra: true,
			        zebraClass: 'net-alternative-row',
			        sortable: false,
			        sortedColId: null,
			        //sortType:[],
			        dateFormat: 'd-m-y',
			        pager: false,
			        rowsPerPage: 10,
			        resizeCol: false,
			        minColWidth: 70,
			        wrapper: false
			        });
			        });	
			}
		}
}
function applyScrollOnTblWorkExp_sspf()
{
    var gridValue=checkWindowWidth('Employment');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#employeementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
         colratio:colArr,
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });	
	}else{
		try{
	        $(document).ready(function() {
	        $('#employeementGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 350,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	         colratio:colArr,
		    addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });
	            
	        });	
		}catch (e) {
			var $j70=jQuery.noConflict();
				$j70(document).ready(function() {
		        $j70('#employeementGrid').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 350,
		        width: gridSize,
		        minWidth: null,
		        minWidthAuto: false,
		         colratio:colArr,
			    addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
		        });
		        });	
		}
	}
}
function applyScrollOnInvl_sspf()
{
    var gridValue=checkWindowWidth('Involvement');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#involvementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
        colratio:colArr,
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
	}else{
		try{
	        $(document).ready(function() {
	        $('#involvementGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 350,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:colArr,
	        addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });            
	        });
		}catch (e) {
			var $j60=jQuery.noConflict();
				$j60(document).ready(function() {
		        $j60('#involvementGrid').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 350,
		        width: gridSize,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:colArr,
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
		        });            
		        });
		}
	}
}
function applyScrollOnHonors_sspf()
{
    var gridValue=checkWindowWidth('Honors');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
	if(showGirdType=="pool" || showGirdType=="cg"){
		jQuery.noConflict();
		jQuery(document).ready(function() {
		jQuery('#honorsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
        colratio:colArr,
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
	}else{
		try{
	        $(document).ready(function() {
	        $('#honorsGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 350,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:colArr,
	        addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });            
	        });
	}catch (e) {
			var $j40=jQuery.noConflict();
			$j40(document).ready(function() {
	        $j40('#honorsGrid').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 350,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:colArr,
	        addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });            
	        });
	}
 }
}
function applyScrollOnTblProfileVisitHistory_sspf()
{
    var gridValue=checkWindowWidth('ProfileVisitHistory');
    var sizeAndCol=gridValue.split(":");
    var gridSize=sizeAndCol[0];
    var colArr=sizeAndCol[1];
    
	if(showGirdType=="pool" || showGirdType=="cg"){
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblProfileVisitHistory_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 500,
        width: gridSize,
        minWidth: null,
        minWidthAuto: false,
          colratio:colArr,
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
	}else{
		try{
			$(document).ready(function() {
	        $('#tblProfileVisitHistory_Profile').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        //height: 500,
	        width: gridSize,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:colArr,
		    addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });
	        });	
		}catch (e) {
			var $act=jQuery.noConflict();
			$act(document).ready(function() {
		        $act('#tblProfileVisitHistory_Profile').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        //height: 500,
		        width: gridSize,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:colArr,
			    addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
		        });
		            
		        });	
		}
	}
}
function applyScrollOnJob_sspf()
{
	try{
        $(document).ready(function() {
        $('#jobTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 235,
        width: 810,
        minWidth: null,
        minWidthAuto: false,
        colratio:[25,30,140,100,90,80,100,75,100,70], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 50,
        wrapper: false
        });
        });	
	}catch (e) {
	var $acg=jQuery.noConflict();
        $acg(document).ready(function() {
        $acg('#jobTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 235,
        width: 810,
        minWidth: null,
        minWidthAuto: false,
        colratio:[25,30,140,100,90,80,100,75,100,70], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 50,
        wrapper: false
        });
        });	
	}
}

//******************** Custom Question save Section ******************************
function showSelfServiceOldCustomQuestion(teacherId){
	try{
		$("#selfServiceProfileOldCustomQuestion").show();
		
	}catch (e) {
		var $oc=jQuery.noConflict();
		$oc("#selfServiceProfileOldCustomQuestion").show();
		
	}
	
	try{
		$('#selfServiceProfileOldCustomQuestion').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}catch (e) {
		var $k3=jQuery.noConflict();
		$k3('#selfServiceProfileOldCustomQuestion').html("<table align='center'><tr><td style='padding-top: 10px;' align='center'><!--<img src='images/please.jpg'/>--></td></tr><tr><td style='padding-top: 0px;' align='center'><img src='images/loadingAnimation.gif'/></td></tr></table>");
	}
	
	SelfServiceCandidateProfileService.getOldCustomQuestionByDistrict(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		try{
			$("#selfServiceProfileOldCustomQuestion").html(data);
		}catch (e) {
			var $oc=jQuery.noConflict();
			$oc("#selfServiceProfileOldCustomQuestion").html(data);
		}
		}});
}
function saveAndValidateCustomQuestion(jobId,teacherId){
	var question=document.getElementById("allCustomQuestion").value;
	var totalQuestion=question.split(",");
	getUnique(totalQuestion);	
	var iReturnValidateTotalValue=0;
	var iReturnValidateValue=0;
	var SubjectArea_Section_Name="tooltipSection12";
	var SubjectArea_Section_Name1=validateAndSaveCustomAll(totalQuestion,jobId,teacherId);
	iReturnValidateTotalValue=iReturnValidateTotalValue+SubjectArea_Section_Name1;
	
	/*if(SubjectArea_Section_Name1>0)
	{
		window.location.hash = '#lblSectionId_'+SubjectArea_Section_Name;
		$("#lblSectionId_"+SubjectArea_Section_Name).addClass("DSPQRequired12");
	}*/
	if(iReturnValidateTotalValue>0)
	{
		//alert("1")
	}
	else
	{
		$("#editSaveCustomQuestion").show();
	}
}
function validateAndSaveCustomAll(totalQuestion,JobId,teacherId)
{
	//alert("totalQuestion "+totalQuestion +" JobId "+ JobId+ " teacherId "+ teacherId)
	var PI_Section_Name="";
	var arrSection=[];
	var jobId=""; 
	try { jobId=JobId; } catch (e) {}
	
	var IsGoNext=0;
	var IsSave=0;
	//var Section_customFldCount=getInputCtrlTextValue("customFld_"+PI_Section_Name);
	for(var i=0;i<=totalQuestion.length-1; i++)
	{
		var iQuestionId=totalQuestion[i];
		IsSave=1;
		var customFieldQNo=0;
		try { customFieldQNo=$("#lblCustomFieldId_"+iQuestionId).attr("customFieldQNo"); } catch (e) {}
		var customFieldShortCode="";
		try { customFieldShortCode=$("#lblCustomFieldId_"+iQuestionId).attr("customFieldShortCode"); } catch (e) {}
		
		var customFieldRequired=0;
		try { customFieldRequired=$("#lblCustomFieldId_"+iQuestionId).attr("customFieldRequired"); } catch (e) {}
		
		var customFieldOtionCount=0;
		try { customFieldOtionCount=$("#lblCustomFieldId_"+iQuestionId).attr("customFieldOtionCount"); } catch (e) {}
		
		var customFieldQuestionTypeId=0;
		try { customFieldQuestionTypeId=$("#lblCustomFieldId_"+iQuestionId).attr("customFieldQuestionTypeId"); } catch (e) {}
		
		var customFieldLabelAnswer="";
		try { customFieldLabelAnswer=$("#lblCustomFieldId_"+iQuestionId).attr("customFieldLabelAnswer"); } catch (e) {}
		
		var customFieldAnswerID=0;
		try { customFieldAnswerID=$("#lblCustomFieldId_"+iQuestionId).attr("customFieldAnswerID"); } catch (e) {}
		
		
		//alert("customFieldQuestionTypeId "+customFieldQuestionTypeId +" customFieldLabelAnswer "+customFieldLabelAnswer);
		
		//alert("customFieldQNo "+customFieldQNo +" customFieldShortCode "+customFieldShortCode +" customFieldRequired "+customFieldRequired +" customFieldOtionCount "+customFieldOtionCount);
		//alert("101");
		
		var UserOptionId="";
		var UserTextArea="";
		var sCustomFileName="";
		var bNeedToUpload=false;
		
		var custonFileData = "";
	    var formdata = new FormData();
	    
		if(customFieldShortCode=='et' || customFieldShortCode=='ml')
		{
			var txtAreaValue_et_ml=getTextAreaValueBy_CustFld(customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, txtAreaValue_et_ml,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserTextArea=txtAreaValue_et_ml;
		}
		else if(customFieldShortCode=='mlsel')
		{
			var checkBoxValue_mlsel=getInputCtrlCheckBoxValue("opt_"+customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_mlsel,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=checkBoxValue_mlsel;
		}
		else if(customFieldShortCode=='mloet')
		{
			var checkBoxValue_mloet=getInputCtrlCheckBoxValue("opt_"+customFieldQNo);
			var txtAreaValue_mloet=getTextAreaValueBy_CustFld(customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_mloet,txtAreaValue_mloet, customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserTextArea=txtAreaValue_mloet;
			UserOptionId=checkBoxValue_mloet;
		}
		else if(customFieldShortCode=='sloet')
		{
			var checkBoxValue_sloet=getInputCtrlRadioValue("opt_"+customFieldQNo);
			var txtAreaValue_sloet=getTextAreaValueBy_CustFld(customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_sloet,txtAreaValue_sloet, customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserTextArea=txtAreaValue_sloet;
			UserOptionId=checkBoxValue_sloet;
		}
		else if(customFieldShortCode=='tf' || customFieldShortCode=='slsel')
		{
			var checkBoxValue_tf_slsel=getInputCtrlRadioValue("opt_"+customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_tf_slsel,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			//alert("checkBoxValue_tf_slsel "+checkBoxValue_tf_slsel +" iReturnValidateValue "+iReturnValidateValue);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=checkBoxValue_tf_slsel;
		 }
		else if(customFieldShortCode=='drsls' || customFieldShortCode=="DD")
		{
			var listBoxValue_drsls=getInputCtrlTextValue("opt_"+customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, listBoxValue_drsls,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=listBoxValue_drsls;
		 }
		else if(customFieldShortCode=='dt')
		{
			var textValue_drsls=getInputCtrlTextValue("opt_"+customFieldQNo);			
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, textValue_drsls,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=textValue_drsls;
		 }
		else if(customFieldShortCode=='sl')
		{
			var textValue_drsls=getInputCtrlTextValue("opt_"+customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, textValue_drsls,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserTextArea=textValue_drsls;
		 }
		else if(customFieldShortCode=='UAT')
		{
			var db_InputCtrlId_CustFld=getInputCtrlTextValue("opt_"+customFieldQNo+"DBFileName");
			var InputCtrlId_CustFld="opt_"+customFieldQNo+"File";
			var sFileNameCustFld=getInputCtrlTextValue(InputCtrlId_CustFld);
			
			iReturnValidateValue=validateCustomFldFileUploadControlStr(customFieldShortCode, sFileNameCustFld,"", customFieldRequired, customFieldQNo,PI_Section_Name,i,db_InputCtrlId_CustFld,InputCtrlId_CustFld);
			if(iReturnValidateValue=="1")
			{
				IsGoNext++;
			}
			else
			{
				if(sFileNameCustFld!=null && sFileNameCustFld!="")
					bNeedToUpload=true;
				
				custonFileData = document.getElementById(InputCtrlId_CustFld).files[0];
			    formdata.append(InputCtrlId_CustFld, custonFileData);
			    
			}
			sCustomFileName=db_InputCtrlId_CustFld;
		 }
		else if(customFieldShortCode=='UATEF')
		{
			var db_InputCtrlId_CustFld=getInputCtrlTextValue("opt_"+customFieldQNo+"DBFileName");
			var InputCtrlId_CustFld="opt_"+customFieldQNo+"File";
			var sFileNameCustFld=getInputCtrlTextValue(InputCtrlId_CustFld);
			
			var	txtAreaValue_FileUpload=getTextAreaValueBy_CustFld(customFieldQNo);
			
			iReturnValidateValue=validateCustomFldFileUploadControlStr(customFieldShortCode, sFileNameCustFld,txtAreaValue_FileUpload, customFieldRequired, customFieldQNo,PI_Section_Name,i,db_InputCtrlId_CustFld,InputCtrlId_CustFld);
			if(iReturnValidateValue=="1")
			{
				IsGoNext++;
			}
			else
			{
				if(sFileNameCustFld!=null && sFileNameCustFld!="")
					bNeedToUpload=true;
				
				custonFileData = document.getElementById(InputCtrlId_CustFld).files[0];
			    formdata.append(InputCtrlId_CustFld, custonFileData);

			}
			sCustomFileName=db_InputCtrlId_CustFld;
			UserTextArea=txtAreaValue_FileUpload;
		 }
		else if(customFieldShortCode=='OSONP')
		{
			var checkBoxValue_tf_slsel=getInputCtrlRadioValue("opt_"+customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_tf_slsel,"", customFieldRequired, customFieldQNo,PI_Section_Name,i);
			//alert("checkBoxValue_tf_slsel "+checkBoxValue_tf_slsel +" iReturnValidateValue "+iReturnValidateValue);
			if(iReturnValidateValue=="1")
			{
				//PI_Section=PI_Section+1;
				//iReturnValidateTotalValue=iReturnValidateTotalValue+PI_Section;
				IsGoNext++;
			}
			UserOptionId=checkBoxValue_tf_slsel;
		 }
		else if(customFieldShortCode=='sswc')
		{
			var checkBoxValue_mloet=getInputCtrlRadioValue("opt_"+customFieldQNo);
			var txtValue_sswc=getInputCtrlTextValue("opt_"+customFieldQNo);
			iReturnValidateValue=validateCustomFldControlStr(customFieldShortCode, checkBoxValue_mloet,txtAreaValue_mloet, customFieldRequired, customFieldQNo,PI_Section_Name,i);
			if(iReturnValidateValue=="1")
			{				
				IsGoNext++;
			}
			UserTextArea=txtValue_sswc;
			UserOptionId=checkBoxValue_mloet;
		}
		
		if(bNeedToUpload)
		{
			try
			{
			    /*var xhr = new XMLHttpRequest();       
			    xhr.open("POST","selfServiceCustomFieldFileUploadServlet.do", true);
			    xhr.send(formdata);	
			    xhr.onload = function(e) 
			    {
			        if (this.status == 200) 
			        {
			        	if(this.responseText!="Error")
			        		sCustomFileName=this.responseText;
			        }
			    };*/
			    
				sCustomFileName=$.ajax({
		    	  url : 'selfServiceCustomFieldFileUploadServlet.do',
		    	        type : 'POST',
		    	        async: false,
		    	        cache: false,
		    	        contentType: false,
		    	        processData: false,
		    	        data : formdata,
		    	        success : function(data) { 
		    	        }
		    	 }).responseText;
			    
			}catch(err){
				alert(err);
			}
		}
		
		var jobOrder = {jobId:jobId};
		var questionTypeMaster = {questionTypeId:customFieldQuestionTypeId};
		var districtSpecificQuestion = {questionId:customFieldQNo};
		
		arrSection.push({ 
			"selectedOptions"  : UserOptionId,
			"question"  : customFieldLabelAnswer,
			"questionTypeMaster" : questionTypeMaster,
			"questionType" : customFieldShortCode,
			"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
			"jobOrder" : jobOrder,
			"insertedText" : UserTextArea,
			"answerId" :customFieldAnswerID,
			"fileName" : sCustomFileName
		});
		
	}
	if(IsGoNext==0 && IsSave==1)
	{
		SelfServiceCandidateProfileService.saveProfileCustomFieldAnswer(arrSection,teacherId,{
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data=="error")
				{
					IsGoNext++;
				}
			}
		  });
	}
	
	return IsGoNext;
}
function getUnique(questionArray) {
    var a = [], // uniques get placed into here
        b = 0; // counter to test if value is already in array 'a'

    for ( i = 0; i < questionArray.length; i++ ) {
        var current = questionArray[i]; // get a value from the original array

        for ( j = 0; j < a.length; j++ ) { // loop and check if value is in new array 
            if ( current != a[j] ) {
                b++; // if its not in the new array increase counter
            }
        }

        if ( b == a.length ) { // if the counter increased on all values 
                               // then its not in the new array yet
            a.push( current ); // put it in
        }

        b = 0; // reset counter
    }

    questionArray.length = 0; // after new array is finished creating delete the original array
    for ( i = 0; i < a.length; i++ ) {
    	questionArray.push( a[i] ); // push all the new values into the original
    }

    return questionArray; // return this to allow method chaining
}
function getTextAreaValueBy_CustFld(InputCtrlId)
{
	var inputCtrlValue="";
	try { inputCtrlValue=trim($('#divOpt_'+InputCtrlId).find(".jqte_editor").text().trim()); } catch (e) {}
	if(inputCtrlValue==null || inputCtrlValue=="")
	{
		try { inputCtrlValue=$('#txtAreaOpt_'+InputCtrlId).val(); } catch (e) {}
	}
	return inputCtrlValue;
}
function validateCustomFldControlStr(ShortCode,InputValue1,InputValue2,Required,CustomFieldAutoId,SectionNameId,CustFldCounter)
{
	return validateCustomFldFileUploadControlStr(ShortCode, InputValue1, InputValue2, Required, CustomFieldAutoId, SectionNameId, CustFldCounter,"",null);
}
function getInputCtrlCheckBoxValue(InputCtrlTextId)
{
	var inputCtrlValue="";
	try {
		var elements = document.getElementsByName(InputCtrlTextId);
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  if(inputCtrlValue=="")
				  inputCtrlValue=elements[i].value;
			  else
				  inputCtrlValue=inputCtrlValue+","+elements[i].value;
		  }
		}
	} catch (e) {}
	
	return inputCtrlValue;
}
function getInputCtrlRadioValue(InputCtrlTextId)
{
	var inputCtrlValue="-1";
	try {
		var elements = document.getElementsByName(InputCtrlTextId);
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  inputCtrlValue=elements[i].value;
		  }
		}
	} catch (e) {}
	
	return inputCtrlValue;
}
function getInputCtrlTextValue(InputCtrlTextId)
{
	var inputCtrlValue="";
	try { inputCtrlValue=trim($("#"+InputCtrlTextId).val()); } catch (e) {}
	return inputCtrlValue;
}
function validateCustomFldFileUploadControlStr(ShortCode,InputValue1,InputValue2,Required,CustomFieldAutoId,SectionNameId,CustFldCounter,InputValue3,InputCtrlFileUploadName)
{
	var iReturnValidateValue=0;
	if(Required=="1")
	{
		if(ShortCode=="et" || ShortCode=="ml")
		{
			if(InputValue1=="")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF=jQuery.noConflict();
					$VF("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					//$("#"+CustomFieldAutoId).focus();
					try{
						$("#divOpt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF0=jQuery.noConflict();
						$VF0("#divOpt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF1=jQuery.noConflict();
					$VF1("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF1("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF1("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF1("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="mlsel")
		{
			if(InputValue1=="")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF2=jQuery.noConflict();
					$VF2("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF2("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF2("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF2("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF3=jQuery.noConflict();
						$VF3("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF4=jQuery.noConflict();
					$VF4("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF4("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF4("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF4("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="mloet")
		{
			if(InputValue1=="" || InputValue2=="")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF5=jQuery.noConflict();
					 $VF5("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					 $VF5("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					 $VF5("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					 $VF5("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF6=jQuery.noConflict();
						$VF6("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF7=jQuery.noConflict();
					$VF7("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF7("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF7("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF7("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="sloet")
		{
			if(InputValue1=="-1" || InputValue2=="")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF8=jQuery.noConflict();
					$VF8("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF8("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF8("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF8("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF9=jQuery.noConflict();
						$VF9("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF10=jQuery.noConflict();
					$VF10("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF10("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF10("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF10("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="tf" || ShortCode=="slsel")
		{
			if(InputValue1=="-1")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF11=jQuery.noConflict();
					$VF11("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF11("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF11("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF11("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF12=jQuery.noConflict();
						$VF12("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF13=jQuery.noConflict();
					$VF13("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF13("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF13("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF13("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="drsls" || ShortCode=="DD")
		{
			if(InputValue1=="0")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF14=jQuery.noConflict();
					$VF14("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF14("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF14("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF14("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF15=jQuery.noConflict();
						$VF15("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF16=jQuery.noConflict();
					$VF16("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF16("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF16("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF16("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="dt" || ShortCode=="sl")
		{
			if(InputValue1=="")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF17=jQuery.noConflict();
					$VF17("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF17("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF17("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF17("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				//alert("isSetFocus1 "+isSetFocus +" CustomFieldAutoId "+CustomFieldAutoId);
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF18=jQuery.noConflict();
						$VF18("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
					//alert("isSetFocus2 "+isSetFocus);
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF19=jQuery.noConflict();
					$VF19("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF19("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF19("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF19("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="UAT")
		{
			var tempReturnaValue=validateCustFileName(InputCtrlFileUploadName);
			if(tempReturnaValue=="1")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF20=jQuery.noConflict();
					$VF20("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF20("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF20("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF20("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				if(!isSetFocus)
				{
					
					try{
					$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF21=jQuery.noConflict();
						$VF21("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else if((InputValue1=="" && InputValue3=="") || tempReturnaValue=="1")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF22=jQuery.noConflict();
					$VF22("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF22("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF22("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF22("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF23=jQuery.noConflict();
						$VF23("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF24=jQuery.noConflict();
					$VF24("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF24("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF24("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF24("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
				
			}
		}
		else if(ShortCode=="UATEF")
		{
			var sTempFileName="";
			if(InputValue1!="")
			{
				sTempFileName=InputValue1;
			}
			else if(InputValue3!="")
			{
				sTempFileName=InputValue3;
			}
			
			var tempReturnaValue=validateCustFileName(InputCtrlFileUploadName);
			if(tempReturnaValue=="1")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF25=jQuery.noConflict();
					$VF25("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF25("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF25("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF25("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF26=jQuery.noConflict();
						$VF26("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else if(InputValue2==undefined || InputValue2=="" || sTempFileName=="" || sTempFileName=="1")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF27=jQuery.noConflict();
					$VF27("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF27("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF27("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF27("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF28=jQuery.noConflict();
						$VF28("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF29=jQuery.noConflict();
					$VF29("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF29("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF29("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF29("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		else if(ShortCode=="OSONP")
		{
			if(InputValue1=="-1")
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}catch (e) {
					var $VF30=jQuery.noConflict();
					$VF30("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).addClass("DSPQRequired12");
					$VF30("#lblCustomFieldIdWarning_"+CustomFieldAutoId).addClass("icon-warning DSPQRequired12");
					$VF30("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("&nbsp;");
					$VF30("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("&nbsp;");
				}
				iReturnValidateValue=1;
				
				if(!isSetFocus)
				{
					try{
						$("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}catch (e) {
						var $VF31=jQuery.noConflict();
						$VF31("#opt_"+CustomFieldAutoId+"_"+CustFldCounter).focus();
					}
					isSetFocus=true;
				}
				
			}
			else
			{
				try{
					$("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}catch (e) {
					var $VF32=jQuery.noConflict();
					$VF32("#lblCustomFieldId_"+SectionNameId+"_"+CustFldCounter).removeClass("DSPQRequired12");
					$VF32("#lblCustomFieldIdWarning_"+CustomFieldAutoId).removeClass("icon-warning DSPQRequired12");
					$VF32("#lblCustomFieldIdWarningBeforeSpace_"+CustomFieldAutoId).html("");
					$VF32("#lblCustomFieldIdWarningAfterSpace_"+CustomFieldAutoId).html("");
				}
				iReturnValidateValue=0;
			}
		}
		
	}
	
	return iReturnValidateValue;
}
function closeSaveCustomDiv()
{
	try{
		$("#editSaveCustomQuestion").hide();
	}catch (e) {
		var $VF33=jQuery.noConflict();
		$VF33("#editSaveCustomQuestion").hide();
	}
}
function checkWindowWidth(gridName)
{
	var width=0;
	var gridSize=0;
    var colArr=[];
    try{
    	width=$(window).width();
    }catch(e){
    	var $jg=jQuery.noConflict();
    	width=$jg(window).width();
    }
	if(width>1200){
		gridSize=1120;
		if(gridName=="Academics")
			colArr=[250,130,220,150,250,120]; 
		else if(gridName=="Certifications")
			colArr=[220,125,150,325,300];
		else if(gridName=="References")
			colArr=[230,160,160,200,190,120,60];
		else if(gridName=="Video")
			colArr=[600,400,120];
		else if(gridName=="Additional")
			colArr=[610,510];
		else if(gridName=="Employment")
			colArr=[300,250,300,120,150];
		else if(gridName=="Languages")
			colArr=[450,470,200];
		else if(gridName=="Involvement")
			colArr=[350,350,250,170];
		else if(gridName=="Honors")
			colArr=[710,410];
		else if(gridName=="ProfileVisitHistory")
			colArr=[810,310];
	}else if(width<1200 && width>=1000){
		gridSize=850;
		if(gridName=="Academics")
			colArr=[180,110,140,140,160,120];
		else if(gridName=="Certifications")
			colArr=[230,100,115,220,185];
		else if(gridName=="Video")
			colArr=[130,100,120,170,120,110,100];
		else if(gridName=="Video")
			colArr=[450,250,150];
		else if(gridName=="Additional")
			colArr=[530,320];
		else if(gridName=="Employment")
			colArr=[200,220,220,100,110];
		else if(gridName=="Languages")
			colArr=[330,320,200];
		else if(gridName=="Involvement")
			colArr=[270,260,160,160];
		else if(gridName=="Honors")
			colArr=[530,320];
		else if(gridName=="ProfileVisitHistory")
			colArr=[480,370];
	}else{
		gridSize=700;
		if(gridName=="Academics")
			colArr=[140,110,100,120,120,120];
		else if(gridName=="Certifications")
			colArr=[200,80,95,180,145];
		else if(gridName=="References")
			colArr=[100,80,100,130,100,100,90];
		else if(gridName=="Video")
			colArr=[400,200,100];
		else if(gridName=="Additional")
			colArr=[400,300];
		else if(gridName=="Employment")
			colArr=[160,190,150,100,100];
		else if(gridName=="Languages")
			colArr=[280,270,150];
		else if(gridName=="Involvement")
			colArr=[230,220,130,120];
		else if(gridName=="Honors")
			colArr=[500,200];
		else if(gridName=="ProfileVisitHistory")
			colArr=[400,300];
	}
	return (gridSize+":"+colArr);
}