var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/ipad|Android/i.test(navigator.userAgent.toLowerCase()));
/*========= Paging and Sorting ===========*/
function defaultSet(){
	pageForTC = 1;
	noOfRowsForTC = 100;
	sortOrderStrForTC="";
	sortOrderTypeForTC="";
	currentPageFlag="";
}
var txtBgColor="#F5E7E1";
var chkCallFrom=0;
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}


function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	if (decNum.length > 1)//here is the key u can just change from 2 to 3,45 etc to restict no of digits aftre decimal
	{
	//alert("Invalid more than 2 digits after decimal")
		//count=false;
	}
	//alert(count);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
    
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function getTranscriptDetail(tId)
{	
	defaultSet();
	currentPageFlag='tran';
	teacherId=tId;
	getTranscriptGrid();
}

function getTranscriptGrid()
{
	CandidateReportAjax.getTranscriptGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{		
		$('#myModalTranscript').modal('show');
		document.getElementById("divTranscript").innerHTML=data;
		applyScrollOnTranscript();
	}});
}
function getTranscriptDetailNew(tId)
{	
	defaultSet();
	currentPageFlag='tran';
	teacherId=tId;
	getTranscriptGridNew();
}
function getTranscriptGridNew()
{
	CandidateGridAjax.getTranscriptGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{		
		$('#myModalTranscript').modal('show');
		document.getElementById("divTranscript").innerHTML=data;
		applyScrollOnTranscript();
	}});
}
function getCertificationDetail(tId)
{	
	defaultSet();
	currentPageFlag='cert';
	teacherId=tId;
	getCertificationGrid();
	
}

function getCertificationGrid()
{
	CandidateReportAjax.getCertificationGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{		
		$('#myModalCertification').modal('show');
		document.getElementById("divCertification").innerHTML=data;
		applyScrollOnCertification();
	}});
}
function getCertificationDetailNew(tId)
{	
	defaultSet();
	currentPageFlag='cert';
	teacherId=tId;
	getCertificationGridNew();
	
}
function getCertificationGridNew()
{
	CandidateGridAjax.getCertificationGrid(teacherId,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{		
		$('#myModalCertification').modal('show');
		document.getElementById("divCertification").innerHTML=data;
		applyScrollOnCertification();
	}});
}
function sendToOtherPager()
{
	
	$('#loadingDiv').hide();
	if(chkCallFrom==1)
	{
		window.location.href="certification.do";
	}
}

function downloadTranscript(academicId,linkid)
{	
	$("#myModalLabelText").html(resourceJSON.MsgTranscript);	
	CandidateReportAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
		if(deviceType)
		{
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
			    if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 $('#modalDownloadsTranscript').modal('hide');
					 document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkid).href = data;	
				}	
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#modalDownloadsTranscript').modal('hide');
			 document.getElementById('ifrmTrans').src = ""+data+"";
		}
		else
		{
			try{
				document.getElementById('ifrmTransCommon').src = ""+data+"";
				$('#modalDownloadsCommon').modal('show');
			}catch(err)
			  {}	
		}		
	   }
	});
}
function downloadTranscriptNew(academicId)
{	
	CandidateGridAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{		
			if (data.indexOf(".doc") !=-1) {				
			    $('#modalDownloadsTranscript').modal('hide');
			    document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById('ifrmTrans').src = ""+data+"";
				$("#myModalTranscript").css({"z-index":"1000"});
				try{
					$('#modalDownloadsTranscript').modal('show');
				}catch(err)
				  {}
			}
		}
	});
}
function setZIndexTrans(){
	document.getElementById('ifrmTrans').src = "a.html";
	$("#myModalTranscript").css({"z-index":"2000"});	
	$("#myModalCertification").css({"z-index":"2000"});	
}


function chkForEnter(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate();
	}	
}