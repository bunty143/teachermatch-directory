var outTeacherName=null;
var outerDoActivity=null;
var outerCgStatus=null;
var outerStatus=null;
var outerTeacherAssessmentStatusId=null;
var outerJobForTeacherId=null;
var outerTeacherId=null;

function showOuterStatus(teacherName,doActivity,cgStatus,status,teacherAssessmentStatusId,jobForTeacherId,teacherId)
{
	 outTeacherName=teacherName;
	 outerDoActivity=doActivity;
	 outerCgStatus=cgStatus;
	 outerStatus=status;
	 outerTeacherAssessmentStatusId=teacherAssessmentStatusId;
	 outerJobForTeacherId=jobForTeacherId;
	 outerTeacherId=teacherId;
	 document.getElementById("outer_status_l_title").innerHTML=""+resourceJSON.MsgStatusLifeCycleFor+" "+teacherName;
	$('#outerStatusModal').modal('show');
}

function outerStatusClose(){
	$('#outerStatusModal').modal('hide');
	 document.getElementById("outer_status_l_title").innerHTML="";
	showStatus(outTeacherName,outerDoActivity,outerCgStatus,outerStatus,outerTeacherAssessmentStatusId,outerJobForTeacherId,outerTeacherId)
	 outTeacherName=null;
	 outerDoActivity=null;
	 outerCgStatus=null;
	 outerStatus=null;
	 outerTeacherAssessmentStatusId=null;
	 outerJobForTeacherId=null;
	 outerTeacherId=null;
}
function showStatus(teacherName,doActivity,cgStatus,status,teacherAssessmentStatusId,jobForTeacherId,teacherId)
{
	document.getElementById("status_l_title").innerHTML=""+resourceJSON.MsgStatusLifeCycleFor+" "+teacherName;
	$('#myModalStatus').modal('show');
	document.getElementById("statusTeacherId").value=teacherId;
	document.getElementById("jobForTeacherIdForSNote").value=jobForTeacherId;
	document.getElementById("doActivity").value=doActivity;
	
	displayStatusDashboard(0);
	finalizeOrNot(doActivity);
}
function finalizeOrNot(doActivity){
	if(doActivity=='true'){
		document.getElementById("statusSave").style.display="inline";
		document.getElementById("statusFinalize").style.display="inline";
	}else{
		document.getElementById("statusSave").style.display="none";
		document.getElementById("statusFinalize").style.display="none";
	}
	
	document.getElementById('email_da_sa').checked=false;
	//document.getElementById('email_ca').checked=false;
}

function displayStatusDashboard(clickFlag){
	var jobId = document.getElementById("jobId").value;
	var teacherId = document.getElementById("statusTeacherId").value;
	$('#loadingDiv').show();
	ManageStatusAjax.displayStatusDashboardCG(teacherId,jobId,clickFlag,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	$('#loadingDiv').hide();
			document.getElementById("divStatus").innerHTML=data.toString();	
		}
	});
}
function applyScrollOnStatusNote()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblStatusNote').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 670,
        minWidth: null,
        minWidthAuto: false,
        colratio:[220,100,120,120,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function openNoteDiv(DistrictId){
	$('#statusNotes').find(".jqte_editor").html("");
	var statusName = $('#statusName').val();
	ManageStatusAjax.statusWiseSectionList(DistrictId,statusName,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	if(data!=null){
				document.getElementById("templateDivStatus").style.display="block";
				$('#statuswisesection').html(data);
				var selectId = $("#statuswisesection option:selected").val();
				getsectionwiselist(selectId);
		}
		}
	});
	
	document.getElementById("noteMainDiv").style.display="inline";
	document.getElementById("showStatusNoteFile").innerHTML="";
	document.getElementById("showTemplateslist").style.display="none";
	removeStatusNoteFile();
	$('#errorStatusNote').hide();
}

/* ==== Gourav  : [Start  Js Method for the functinality of status wise email templates list]*/
function getsectionwiselist(value){
	document.getElementById("showTemplateslist").style.display="none";
	//document.getElementById("changeTemplateBody").style.display="none";
	$('#statusNotes').find(".jqte_editor").html("");
	if(value!=0){
		ManageStatusAjax.getSectionWiseTemaplteslist(value,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			
			document.getElementById("addTemplateslist").innerHTML=data;
			document.getElementById("showTemplateslist").style.display="block";
			}
		});
	}
}

function setTemplateByLIstId(templateId){
	$('#statusNotes').find(".jqte_editor").html("");
	if(templateId!=0){
		ManageStatusAjax.getTemplateByLIst(templateId,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			$('#statusNotes').find(".jqte_editor").html(data);
			}
		});
	}
}

function getTemplateByLIst(){
	var value=document.getElementById("templateId").value;
	if(value!=0){
		ManageStatusAjax.getTemplateByLIst(value,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			$('#statusNotes').find(".jqte_editor").html(data);
			}
		});
	}
}
/* ==== Gourav  : [Start  Js Method for the functinality of status wise email templates list end]*/

/* ==== Gagan  : [Start  Js Method for the functinality of status wise email templates ]*/

function getStatusWiseEmailForAdmin()
{
	//alert(" getStatusWiseEmailForAdmin ------------------------------");
	try{  
		$('#myModalEmailTeacher').modal('hide');
	}catch(e){}
	var statusName	=	$("#statusName").val();
	//alert(" statusName "+statusName);
	var statusShortName =	$("#statusShortName").val();
	var jftIdForSNote =	$("#jobForTeacherIdForSNote").val();
	var statusIdForStatusNote =	$("#statusIdForStatusNote").val();
	$('#loadingDiv').show();
	ManageStatusAjax.getStatusWiseEmailForAdmin(statusName,statusShortName,jftIdForSNote,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		    $('#loadingDiv').hide();
			if(data!=null)
			{				
				$('#subjectLine').val(data.subjectLine);
				$('#mailBody').find(".jqte_editor").html(data.templateBody);
				try{  
					$('#jWTeacherStatusNotesDiv').modal('hide');
					document.getElementById("noteMainDiv").style.display="none";
				}catch(e){}
				
				try{  
					$('#myModalEmail').modal('show');
				}catch(e){}
			}
			else
			{
				document.getElementById("email_da_sa").disabled=true;
			}
		}
	});
}

/* ==== Gagan  : [Start  Js Method for the functinality of status wise email templates ]*/

function getStatusWiseEmailForTeacher()
{
	//alert(" getStatusWiseEmailForTeacher() ");
	try{  
		$('#myModalEmail').modal('hide');
	}catch(e){}
	var statusName	=	$("#statusName").val();
	var statusShortName =	$("#statusShortName").val();
	var jftIdForSNote =	$("#jobForTeacherIdForSNote").val();
	var statusIdForStatusNote =	$("#statusIdForStatusNote").val();
	$('#loadingDiv').show();
	ManageStatusAjax.getStatusWiseEmailForTeacher(statusName,statusShortName,jftIdForSNote,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		    $('#loadingDiv').hide();
			if(data!=null)
			{
				$('#subjectLineTeacher').val(data.subjectLine);
				$('#mailBodyTeacher').find(".jqte_editor").html(data.templateBody);
				try{  
					$('#jWTeacherStatusNotesDiv').modal('hide');
					document.getElementById("noteMainDiv").style.display="none";
				}catch(e){}
				try{  
					$('#myModalEmailTeacher').modal('show');
				}catch(e){}
				
			}
		}
	});
}
function sendChangedEmail()
{
	$('#isEmailTemplateChanged').val(1);
	var msgSubject=$('#subjectLine').val();
	var charCount	=	$('#mailBody').find(".jqte_editor").text();
			
	$('#errordivEmail').empty();
	$('#subjectLine').css("background-color", "");
	$('#mailBody').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	if(trim(msgSubject)=="")
	{
		$('#errordivEmail').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
		if(focs==0)
			$('#subjectLine').focus();
		$('#subjectLine').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(charCount.length==0)
	{
		$('#errordivEmail').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
		if(focs==0)
			$('#mailBody').find(".jqte_editor").focus();
		$('#mailBody').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
	{
		emailClose();
		return false;
	
	}
	else
	{
		$('#errordivEmail').show();
		return false;
	}
}

function sendChangedEmailTeacher()
{
	
	$('#isEmailTemplateChangedTeacher').val(1);
	var msgSubjectTeacher=$('#subjectLineTeacher').val();
	var charCount	=	$('#mailBodyTeacher').find(".jqte_editor").text();
			
	$('#errordivEmailTeacher').empty();
	$('#subjectLineTeacher').css("background-color", "");
	$('#mailBodyTeacher').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	if(trim(msgSubjectTeacher)=="")
	{
		$('#errordivEmailTeacher').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
		if(focs==0)
			$('#subjectLineTeacher').focus();
		$('#subjectLineTeacher').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(charCount.length==0)
	{
		$('#errordivEmailTeacher').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
		if(focs==0)
			$('#mailBodyTeacherTeacher').find(".jqte_editor").focus();
		$('#mailBodyTeacher').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
	{
		emailClose();
		return false;
	
	}
	else
	{
		$('#errordivEmailTeacher').show();
		return false;
	}
}


function requisitionNumbers()
{
	var jobId=document.getElementById("jobId").value;
	var schoolId=document.getElementById("schoolIdCG").value;
	var reqNumber=document.getElementById("reqNumber").value;
	var offerReady=0;
	try{
		offerReady=document.getElementById("offerReady").value;
	}catch(err){}
	var teacherId=document.getElementById("statusTeacherId").value;
	ManageStatusAjax.requisitionNumbers(jobId,schoolId,offerReady,reqNumber,teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				if(data!=""){
					document.getElementById("requisitionNumbers").innerHTML=data;
					document.getElementById("requisitionNumbersGrid").style.display="inline";
					try{
						document.getElementById("requisitionNumber").style.display="inline";
					}catch(err){}
				}else{
					document.getElementById("requisitionNumbers").innerHTML="";
					document.getElementById("requisitionNumbersGrid").style.display="none";
				}
			}catch(err){}
		}
	});
}





function setStatusTilteFor2ndDiv(teacherDetails,statusName,statusShortName){
	document.getElementById("status_title").innerHTML=statusName +" Evaluation for "+teacherDetails;
	$("#statusName").val(statusName);
	$("#statusShortName").val(statusShortName);
	$('#hiredDateDiv').hide();
	//document.getElementById("setHiredDate").value = "";
	if(statusName == "Hired"){
		$('#hiredDateDiv').show();
	}
	try{document.getElementById("offerReady").value=""}catch(e){}
}
/* ==== Gagan  : [END  Js Method for the functinality of status wise email templates ]*/

function checkOfferAccepted(offerAccepted){
	document.getElementById("offerAccepted").value=offerAccepted;
}
function checkReqNumber(schoolId,offerReady,reqNumber){
	document.getElementById("schoolIdCG").value=schoolId;
	document.getElementById("offerReady").value=offerReady;
	document.getElementById("reqNumber").value=reqNumber;
}

function jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId){
	var offerReady="";
	try{
		offerReady=document.getElementById("offerReady").value;
	}catch(err){}
	if(offerReady==""){
		try{
			document.getElementById("schoolIdCG").value=0;
		}catch(err){}
		try{
			document.getElementById("schoolNameCG").value="";
		}catch(err){}
	}

	var isReqNoForHiring=0;
	try{
		isReqNoForHiring=document.getElementById("isReqNoForHiring").value;
	}catch(err){}
	if(isReqNoForHiring==0){
		try{
			document.getElementById("reqstar").style.display="none";
		}catch(err){}
	}else{
		try{
			document.getElementById("reqstar").style.display="inline";
		}catch(err){}
	}
	
	
	var jobForTeacherId=document.getElementById("jobForTeacherIdForSNote").value;
	currentPageFlag="statusNote";
	document.getElementById("statusIdForStatusNote").value=statusId;
	document.getElementById("fitScoreForStatusNote").value=fitScore;
	document.getElementById("secondaryStatusIdForStatusNote").value=secondaryStatusId;
	var teacherId = document.getElementById("statusTeacherId").value;
	var jobId = document.getElementById("jobId").value;
	$('#myModalStatus').modal('hide');
	$('#errorStatusNote').hide();
	$('#loadingDiv').show();
	var doNotShowPanelinst=0;
	var offerReq=0;
	try{
		if(document.getElementById("panelOpenFlag"))
			doNotShowPanelinst = 1;
	}catch(e){}
	PanelAjax.getStatusNoteForPanel(jobForTeacherId,fitScore,teacherId,jobId,statusId,secondaryStatusId,pageSNote,noOfRowsSNote,sortOrderStrSNote,sortOrderTypeSNote,doNotShowPanelinst,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("jobCategoryDiv").value=2;
			$('#loadingDiv').hide();
			if(data==1)
				document.getElementById("divStatusNoteGrid").innerHTML=""+resourceJSON.MsgNotStartedOrNotCompleted+"";
			else if(data==2)
				document.getElementById("divStatusNoteGrid").innerHTML=""+resourceJSON.MsgTimeOutTheJSI+"";
			else
				document.getElementById("divStatusNoteGrid").innerHTML=data.split("##")[0];
			
			var disFlag=0;
			if(document.getElementById("panelOpenFlag"))
			{
				
			}
			else
			$('#jWTeacherStatusNotesDiv').modal('show');			
			document.getElementById("checkOptions").style.display="inline";
			finalizeOrNot(document.getElementById("doActivity").value);
			if(data.split("##")[1]!='null' && data.split("##")[1]!='JSI' && data.split("##")[1]!='Offer Ready'){
				if(document.getElementById("statusSave").style.display=='inline'){
					if(data.split("##")[1]=='H'){
						disFlag=1;
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="inline";
					}else if(data.split("##")[1]=='D'){
						document.getElementById("unRemove").style.display="none";
						document.getElementById("unDecline").style.display="inline";
						document.getElementById("unHire").style.display="none";
					}else if(data.split("##")[1]=='R'){
						document.getElementById("unRemove").style.display="inline";
						document.getElementById("unDecline").style.display="none";
						document.getElementById("unHire").style.display="none";
					}
				}
			}else{
				if(data.split("##")[1]=='JSI'){
					document.getElementById("statusFinalize").style.display="none";
					document.getElementById("statusSave").style.display="none";
					document.getElementById("checkOptions").style.display="none";
				}
				if(data.split("##")[1]=='Offer Ready'){
					requisitionNumbers();
					offerReq=1;
				}
				document.getElementById("unRemove").style.display="none";
				document.getElementById("unDecline").style.display="none";
				document.getElementById("unHire").style.display="none";
			}
			scroeGrid();
			applyScrollOnStatusNote();
			applyScrollOnTblTrans();
			applyScrollOnTblTransAcademic();
			applyScrollOnTblReferenceCheck();
			//Hanzala Subhani
			
			var hiredFlag=0;
			if(document.getElementById("statusIdForStatusNote").value!=null){
				if(document.getElementById("statusIdForStatusNote").value==6){
					hiredFlag=1;
				}
			}
			try{
				if(hiredFlag==0 && offerReq==0){
					document.getElementById("requisitionNumbers").innerHTML="";
					document.getElementById("requisitionNumbersGrid").style.display="none";
				}
			}catch(err){}
			
			document.getElementById("hireFlag").value=disFlag;
			var txtschoolCount=0;
			try { 
					txtschoolCount=document.getElementById("txtschoolCount").value;
				} catch (e) {}
				
			if(txtschoolCount > 1 && disFlag==0){
				if(hiredFlag==1)
				requisitionNumbers();
				document.getElementById("schoolAutoSuggestDivId").style.display="block";
				try{
					document.getElementById("requisitionNumber").style.display="block";
				}catch(err){}
			}else{
				if(disFlag==0){
					if(hiredFlag==1)
					requisitionNumbers();
				}else{
					if(offerReq==0){
						try{
							document.getElementById("requisitionNumbersGrid").style.display="none";
						}catch(err){}
						try{
							document.getElementById("requisitionNumber").style.display="none";
						}catch(err){}
					}
				}
				document.getElementById("schoolAutoSuggestDivId").style.display="none";
			}			
			var txtOverride=0;
			try { 
				txtOverride=document.getElementById("txtOverride").value;
				} catch (e) {}
			
				try{
					if(data.split("##")[2]!=null && data.split("##")[2]!='null'){
						try{
							document.getElementById("schoolAutoSuggestDivId").style.display="none";
						}catch(err){}
						/*try{
							document.getElementById("requisitionNumbersGrid").style.display="none";
						}catch(err){}
						try{
							document.getElementById("requisitionNumber").style.display="none";
						}catch(err){}*/
					}
				}catch(err){}
				
				
			if(txtOverride==1)
			{
				if(document.getElementById("panelOpenFlag"))
				{
				}
				else
				{
					if(offerReady=="")
					{
						$('#spanOverride').modal('show');
					}
					
				}
			}
			else
			{
					$('#spanOverride').modal('hide');
			
			}			
			if(document.getElementById("panelOpenFlag"))
			{			
				$('#checkOptions').hide();
				$('#statusSave').hide();
			}
			try{
				if(data.split("##")[4]!=null && data.split("##")[4]!='null'){
					try{
						document.getElementById("jobCategoryFlag").value=data.split("##")[4];
					}catch(err){}
				}
			}catch(err){}
			
			$('textarea').jqte();
			getQuestionNotesIds();
			
			//mukesh
			if(data.split("##")[3]=='true')
			{
			   document.getElementById('email_da_sa').checked=true;
			}
		}
		
	});
}

function scroeGrid(){
	$('#noFitScore').tooltip();
	$('#attachInstructionFileName').tooltip();
	$('.statusscore').popover({ 
	 	trigger: "manual",
	    html : true,
	    placement: 'left',
	    template: '<div class="popover" style="width: 400px;top: 80%;"><div class="arrow" style="top:10px;"></div><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>',
	    content: $('#scoreList').html(),
	  }).on("mouseenter", function () {
		  
		  var height=setDialogPosition();
		    height=height+100;
		  	var style = $('<style>.popover { position: absolute;top: '+height+'px !important;left: 0px; }</style>')
			$('html > head').append(style);
		  	
	        var _this = this;
	        $(this).popover("show");
	        $(this).siblings(".popover").on("mouseleave", function () {
	            $(_this).popover('hide');
	        });
	    }).on("mouseleave", function () {
	        var _this = this;
	        setTimeout(function () {
	            if (!$(".popover:hover").length) {
	                $(_this).popover("hide")
	            }
	        }, 100);
	    });
}
function setDialogPosition()
{
	var scrollTopPosition = $(window).scrollTop();
	var x=event.clientX; 
	var y=event.clientY;
	var z=y-180+scrollTopPosition;	
	return z;
}
function jWTeacherStatusNotesOnClose(){
	try{
		$('#jWTeacherStatusNotesDiv').modal('hide');
		document.getElementById("noteMainDiv").style.display="none";
		document.getElementById("scoreProvided").value="";
		$('#messageDiv').modal('show');	
	}
	catch(err){}
}
function sendOriginalEmail()
{
	$('#isEmailTemplateChanged').val(0);
	emailClose();
}

function sendOriginalEmailTeacher()
{
	$('#isEmailTemplateChangedTeacher').val(0);
	emailClose();
}

function emailClose(){
	try{
		$('#myModalEmail').modal('hide'); 
		$('#myModalEmailTeacher').modal('hide'); 
		$('#jWTeacherStatusNotesDiv').modal('show');	
	}
	catch(err){}
	displayStatusDashboard(1);
}
function addStatusNoteFileType()
{
	$('#statusNoteFileNames').empty();
	$('#statusNoteFileNames').html("<a href='javascript:void(0);' onclick='removeStatusNoteFile();'><img src='images/can-icon.png' title='remove'/></a><br> <input id='statusNoteFileName' name='statusNoteFileName' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeStatusNoteFile()
{
	$('#statusNoteFileNames').empty();
	$('#statusNoteFileNames').html("<a href='javascript:void(0);' onclick='addStatusNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addStatusNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function updateStatusHDR(flagValue){
	document.getElementById("updateStatusHDRFlag").value=flagValue;
	
	var statusFlag=$("#statusNotesForStatus").val()=="true"?true:false;
	var statusNotes = $('#statusNotes').find(".jqte_editor").html();
	var cnt=0;
	var focs=0;	
	var statusNoteFileName=null;
	try{  
		statusNoteFileName=document.getElementById("statusNoteFileName").value;
	}catch(e){}
	
	var noteDiv=0;
	try{
		if(document.getElementById("noteMainDiv").style.display=="inline"){
			noteDiv=1;
		}
	}catch(err){}
	var dsliderchk=1;
	try{
		dsliderchk=document.getElementById("dsliderchk").value;
	}catch(err){}
	try{
		var iframeNorm = document.getElementById('ifrmStatusNote');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('statusNoteFrm');
		document.getElementById("scoreProvided").value=inputNormScore.value;
	}catch(e){}	
	var scoreProvided=trim(document.getElementById("scoreProvided").value);
		$('#errorStatusNote').empty();
		if(statusFlag){
			if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && scoreProvided==0 && noteDiv==0 && dsliderchk==0){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNoteOrScore+".<br>");
				cnt=1;
			}else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && (noteDiv==1 || (dsliderchk==-1 || dsliderchk==1))){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
				cnt=1;
			}else if($('#statusNotes').find(".jqte_editor").text().trim()!=""){
				var charCount=$('#statusNotes').find(".jqte_editor").text().trim();
				var count = charCount.length;
				if(count>4000)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
					cnt=1;
				}
			}
			if(statusNoteFileName=="" && cnt==0){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectFileToUpload+".<br>");
				cnt++;
			}
		}
		
		if(cnt==0){
			$('#jWTeacherStatusNotesDiv').modal('hide');
			if(flagValue==2){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgUnhireTheCandidate+"";
			}else if(flagValue==3){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgUndeclinedTheCandidate+"";
			}else if(flagValue==4){
				document.getElementById("updateStatusNoteMsg").innerHTML=""+resourceJSON.MsgUnRejectTheCandidate+"";
			}
			$('#modalUpdateStatusNoteMsg').modal('show');
		}
}
function closeUpdateStatusNoteMsg(flag){
	$('#modalUpdateStatusNoteMsg').modal('hide');
	if(flag==1){
		var updateStatusHDRFlag=document.getElementById("updateStatusHDRFlag").value;
		saveStatusNote(updateStatusHDRFlag);
	}else{
		$('#jWTeacherStatusNotesDiv').modal('show');
	}
}
function offerAcceptedMsg(){
	$('#jWTeacherStatusNotesDiv').modal('hide');
	$('#offerAcceptedModal').modal('show');
	
}
function offerAcceptedOK(){
	document.getElementById("offerAccepted").value=1;
	$('#offerAcceptedModal').modal('hide');
	$('#jWTeacherStatusNotesDiv').modal('show');
	saveStatusNote(1);
}
function cancelOfferAccepted(){
	$('#offerAcceptedModal').modal('hide');
	jWTeacherStatusNotesOnClose();
}
function offerReadyMsg(flag,offerMsg){
	if(flag==0){
		document.getElementById("offer_Ready_title").innerHTML=""+resourceJSON.MsgOfferReady+"";
	}else{
		document.getElementById("offer_Ready_title").innerHTML=""+resourceJSON.MsgStatus+"";
	}
	document.getElementById("offerReadyDiv").innerHTML=offerMsg;
	$('#myModalStatus').modal('hide');
	$('#offerReadyModal').modal('show');
}
function offerReadyCheck(){
	document.getElementById("offerReadyDiv").innerHTML="";
	$('#offerReadyModal').modal('hide');
	$('#myModalStatus').modal('show');
}
function saveStatusNote(finalizeStatus)
{
		$('#errorStatusNote').empty();
		var statusFlag=$("#statusNotesForStatus").val()=="true"?true:false;
		var txtPanel=document.getElementById("txtPanel").value;
		var txtOverride=document.getElementById("txtOverride").value;
		var txtLoggedInUserPanelAttendees=document.getElementById("txtLoggedInUserPanelAttendees").value;
		var iQdslider=document.getElementById("iQdslider").value;
		var hireFlag=document.getElementById("hireFlag").value;
		var requisitionNumberId=null;
		var chkOverride=false;
		if(txtOverride==1)
		{
			chkOverride=document.getElementById('chkOverride').checked;
		}
		
		var statusName = document.getElementById("statusName").value;

		var setHiredDate ="";
	
		/*
		 * 		Date	:	07-08-2014
		 * 		By		:	Hanzala Subhani
		 * 		Purpose	:	Validate Status from teacheracademic 
		 */
		if(statusName == 'Ofcl Trans / Veteran Pref'){
			var teacherAcademicsStatus	 	=	document.getElementById("teacherAcademicsStatus").value;
			var split =	teacherAcademicsStatus.split(",");
			var counter = 0;
			for(var i=1; i < split.length; i++){
				if(split[i] == "" || split[i] == null || split[i] == "P"){
					counter	=	counter+1;
				}
			}
			if(counter > 0){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSetStatusOfAllTranscript+".<br>");
				return false;
			}
		}
		/*====== Gagan [ Start ] : variable for status wise email templates ================*/
		var isEmailTemplateChanged	=	$('#isEmailTemplateChanged').val();
		var statusShortName	=	$("#statusShortName").val();
		var msgSubject	=	$('#subjectLine').val();
		//var charCount	=	$('#mailBody').find(".jqte_editor").text();
		var isEmailTemplateChangedTeacher	=	$('#isEmailTemplateChangedTeacher').val();
		var msgSubjectTeacher	=	$('#subjectLineTeacher').val();
		//var charCountTeacher	=	$('#mailBodyTeacher').find(".jqte_editor").text();
		/*====== Gagan [ END ] : variable for status wise email templates ================*/
		
		// Start for save Question score
		try{
			var iframeNorm = document.getElementById('ifrmStatusNote');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			var inputNormScore = innerNorm.getElementById('statusNoteFrm');
			document.getElementById("scoreProvided").value=inputNormScore.value;
		}catch(e){}	
		var scoreProvided=trim(document.getElementById("scoreProvided").value);
		
		var answerId_arrary = new Array();
		var answerScore_arrary = new Array();
		
		var answerFinalize=0;
		var sumAnswerScore=0;
		var sumAnswerScore=0;
		var iCountScoreSlider=document.getElementById("iCountScoreSlider").value;
		if(iCountScoreSlider>0)
		{
			for(var i=1;i<= iCountScoreSlider;i++)
			{
				var questionscore=0;
				var answerId=0;
				
				try{
					var ifrmQuestion = document.getElementById('ifrmQuestion_'+i);
					if(ifrmQuestion!=null)
					{
						var innerQuestion = ifrmQuestion.contentDocument || ifrmQuestion.contentWindow.document;
						var inputQuestionFrm = innerQuestion.getElementById('questionFrm');
						var answerIdFrm = innerQuestion.getElementById('answerId');
						questionscore=inputQuestionFrm.value;
						answerId=answerIdFrm.value;
						
						sumAnswerScore=(parseInt(sumAnswerScore)+parseInt(questionscore));
						
						answerId_arrary[i-1]=answerId;
						answerScore_arrary[i-1]=questionscore;
					}
				}catch(e){}
				
				if(questionscore==0)
				{
					answerFinalize++;
				}
	
			}
		}
		var iCountScoreSlider=document.getElementById("iCountScoreSlider").value;
		var isQuestionEnable=document.getElementById("isQuestionEnable").value;
		// End for save Question score
		
		var jobForTeacherId =document.getElementById("jobForTeacherIdForSNote").value;
		var fitScore=document.getElementById("fitScoreForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		document.getElementById("finalizeStatus").value=finalizeStatus;
		var statusNotes = $('#statusNotes').find(".jqte_editor").html();
		var teacherId = document.getElementById("statusTeacherId").value;
		var jobId = document.getElementById("jobId").value;
		var emailSentToD =0;// document.getElementById("emailSentToD").checked;
		var emailSentToS =0;// document.getElementById("emailSentToS").checked;
		var statusId=document.getElementById("statusIdForStatusNote").value;
		var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
		var teacherStatusNoteId=document.getElementById("teacherStatusNoteId").value;
		var bEmailSendFor_DA_SA=document.getElementById('email_da_sa').checked;
		var bEmailSendFor_CA=false;//document.getElementById('email_ca').checked;
		
		//@AShish :: define questionsDetails and questionAssessmentIds
		var questionAssessmentIds = document.getElementById('questionAssessmentIds').value;
		var qqNoteIds = document.getElementById('qqNoteIds').value;
		var qqNoteIdsvalue_arr = qqNoteIds.split("#");
		var qqNoteIdsvalue_str ="";
		if(qqNoteIds!=null && qqNoteIds!="")
		{
			for(var i=0;i<qqNoteIdsvalue_arr.length;i++)
			{
				if(qqNoteIdsvalue_str!="")
					qqNoteIdsvalue_str = qqNoteIdsvalue_str+"#"+$('#'+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html();
				else
					qqNoteIdsvalue_str = $('#'+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html();
			}
		}
		//alert(qqNoteIdsvalue_str+"    "+qqNoteIdsvalue_arr);
		
		
		var dsliderchk=1;
		try{
			dsliderchk=document.getElementById("dsliderchk").value;
		}catch(err){}
	
		var statusNoteFileName=null;
		try{  
			statusNoteFileName=document.getElementById("statusNoteFileName").value;
		}catch(e){}
		
		var noteDiv=0;
		try{
			if(document.getElementById("noteMainDiv").style.display=="inline"){
				noteDiv=1;
			}
		}catch(err){}
		try{  
			requisitionNumberId=document.getElementById("requisitionNumber").value;
		}catch(e){}
		
		var cnt=0;
		var focs=0;	
		$('#errorStatusNote').empty();
		try{
			if(offerReady.value=="Offer Ready" && requisitionNumberId==null){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSetStatusOfAllTranscript+".<br>");
				cnt=1;
			}
		}catch(e){}
		if(!statusFlag){
			var score=0;
			if (scoreProvided==0 && dsliderchk==0)
			{
			
			}else if(iQdslider==1 && answerFinalize>0 && iCountScoreSlider!=answerFinalize){
				score=1;
			}else if(iQdslider==1 && iCountScoreSlider!=0 && sumAnswerScore==0 && (dsliderchk==-1 || dsliderchk==1)){
				score=1;
			}
			if ((scoreProvided==0 && dsliderchk==0)|| score==1){
				if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && scoreProvided==0 && noteDiv==0 && dsliderchk==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNoteOrScore+".<br>");
					cnt=1;
				}
				else if(finalizeStatus==1 && answerFinalize>0 && iCountScoreSlider!=answerFinalize)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.SetScoreForAllAttributeToFinalize+".<br>");
					cnt=1;
				}
				else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && (noteDiv==1 || (dsliderchk==-1 || dsliderchk==1)))
				{
					if(isQuestionEnable=='false' || iCountScoreSlider==0)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
						cnt=1;
					}
					else if(sumAnswerScore==0)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNoteForAttribute+".<br>");
						cnt=1;
					}
				}else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && noteDiv==0&& scoreProvided==0 && dsliderchk==-2)
				{
					if(isQuestionEnable=='false' || iCountScoreSlider==0)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
						cnt=1;
					}
				}
				else if($('#statusNotes').find(".jqte_editor").text().trim()!="")
				{
					var charCount=$('#statusNotes').find(".jqte_editor").text().trim();
					var count = charCount.length;
					if(count>4000)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
						cnt=1;
					}
				}
				if(statusNoteFileName=="" && cnt==0){
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectFileToUpload+".<br>");
					cnt++;
				}else if(statusNoteFileName!="" && statusNoteFileName!=null)
				{
					var ext = statusNoteFileName.substr(statusNoteFileName.lastIndexOf('.') + 1).toLowerCase();	
					var fileSize = 0;
					if ($.browser.msie==true)
				 	{	
					    fileSize = 0;	   
					}
					else
					{
						if(document.getElementById("statusNoteFileName").files[0]!=undefined)
						{
							fileSize = document.getElementById("statusNoteFileName").files[0].size;
						}
					}
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSlctAcceptableNoteFormats+".<br>");
						cnt++;
					}
					else if(fileSize>=10485760)
					{
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
						cnt++;
					}
				}
			}
		}else{ //End statusFlag	
			if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && scoreProvided==0 && noteDiv==0 && dsliderchk==0)
			{
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNoteOrScore+".<br>");
				cnt=1;
			}
			else if(finalizeStatus==1 && answerFinalize>0 && iCountScoreSlider!=answerFinalize)
			{
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.SetScoreForAllAttributeToFinalize+".<br>");
				cnt=1;
			}
			else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && (noteDiv==1 || (dsliderchk==-1 || dsliderchk==1)))
			{
				if(isQuestionEnable=='false' || iCountScoreSlider==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
					cnt=1;
				}
				else if(sumAnswerScore==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNoteForAttribute+".<br>");
					cnt=1;
				}
			}else if ($('#statusNotes').find(".jqte_editor").text().trim()=="" && noteDiv==0&& scoreProvided==0 && dsliderchk==-2)
			{
				if(isQuestionEnable=='false' || iCountScoreSlider==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
					cnt=1;
				}
			}
			else if($('#statusNotes').find(".jqte_editor").text().trim()!="")
			{
				var charCount=$('#statusNotes').find(".jqte_editor").text().trim();
				var count = charCount.length;
				if(count>4000)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
					cnt=1;
				}
			}
			if(statusNoteFileName=="" && cnt==0){
				$('#errorStatusNote').show();
				$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSelectFileToUpload+".<br>");
				cnt++;
			}else if(statusNoteFileName!="" && statusNoteFileName!=null)
			{
				var ext = statusNoteFileName.substr(statusNoteFileName.lastIndexOf('.') + 1).toLowerCase();	
				var fileSize = 0;
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{
					if(document.getElementById("statusNoteFileName").files[0]!=undefined)
					{
						fileSize = document.getElementById("statusNoteFileName").files[0].size;
					}
				}
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSlctAcceptableNoteFormats+".<br>");
					cnt++;
				}
				else if(fileSize>=10485760)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
					cnt++;
				}
			}
		}
		
		var txtschoolCount=0;
		var schoolId=0;
		var isReqNoForHiring=0;
		if(finalizeStatus==1){
			try{
				isReqNoForHiring=document.getElementById("isReqNoForHiring").value;
			}catch(err){}
			try{
				txtschoolCount=document.getElementById("txtschoolCount").value;
			}catch(err){}
			var schoolDivCheck=0;
			try{
				if(document.getElementById("schoolAutoSuggestDivId").style.display!="none"){
					schoolDivCheck=1;
				}
			}catch(err){}
			
			if(txtschoolCount > 1 && schoolDivCheck==1)
			{
				schoolId=document.getElementById("schoolIdCG").value;
				//alert("schoolId "+schoolId);
				if(schoolId=='' || schoolId == 0 && hireFlag==0)
				{
					$('#errorStatusNote').show();
					$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
					$('#schoolNameCG').focus();
					cnt++;
				}
			}
			var requisitionNumberShow=0;
			try{
				if(document.getElementById("requisitionNumbersGrid").style.display!='none'){
					requisitionNumberShow=1;
				}
			}catch(err){}
			var reqLength=0;
			try{
				reqLength = document.getElementById("requisitionNumber").length;
			}catch(err){}
			
			try{
				if((statusName=="Offer Ready" || statusName=="Hired") && isReqNoForHiring==1){
					if(isReqNoForHiring==1 && requisitionNumberId==0 && requisitionNumberShow==1 && hireFlag==0 && reqLength>1){
						$('#errorStatusNote').show();
						$('#errorStatusNote').append("&#149; "+resourceJSON.PlzSlctPosition+".<br>");
						cnt++;
					}else if(reqLength<=1 &&((isReqNoForHiring==1 && requisitionNumberShow==0 && requisitionNumberId==null)||(isReqNoForHiring==1 && requisitionNumberShow==1 && requisitionNumberId==0 ))){
						$('#errorStatusNote').show();
						if(offerReady.value=="Offer Ready"){
							$('#errorStatusNote').append("&#149; "+resourceJSON.MsgNoPositionFound+".<br>");
						}else{
							$('#errorStatusNote').append("&#149; "+resourceJSON.MsgPositionIsMandatory+".<br>");
						}
						cnt++;
					}
				}
			}catch(err){}
		}		
		
		var jobCategoryFlag=document.getElementById("jobCategoryFlag").value;
		var jobCategoryDiv=document.getElementById("jobCategoryDiv").value;
		var offerAccepted=document.getElementById("offerAccepted").value;
		var statusId =document.getElementById("statusIdForStatusNote").value;
		var offerFlag=0;
		try{
			if(offerAccepted==0 && statusId==18 && finalizeStatus==1 && cnt==0){
				offerAcceptedMsg();
				offerFlag=1;
			}
		}catch(err){}
		
		var jobCFlag=0;
		//alert('jobCategoryDiv::'+jobCategoryDiv);
		//alert("jobCategoryFlag:::"+jobCategoryFlag);
		try{
			if(jobCategoryDiv==2 && jobCategoryFlag!=2  && cnt==0 && finalizeStatus==1){
				jobCFlag=1;
				$('#jWTeacherStatusNotesDiv').modal('hide');
				$('#updateStatusModal').modal('show');
			}
		}catch(err){}
		var isOCPCChecked = true;
		if(offerFlag==0 && cnt==0 && jobCFlag==0){		
		if(cnt==0){	
			try{
				if(statusName=="Offer Ready"){
					try{
						ManageStatusAjax.checkOCPCStatus(jobForTeacherId,requisitionNumberId,
						{
							async:false,
							errorHandler:handleError,
							callback:function(data)
							{
								if(data=="oc")
								{
									$('#errorStatusNote').show();
									$('#errorStatusNote').append("&#149; "+resourceJSON.MsgApplicantIsNotApplicable+" personnel@dadeschools.net.<br>");
									isOCPCChecked = false;
								}else if(data=="pc")
								{
									$('#errorStatusNote').show();
									$('#errorStatusNote').append("&#149; "+resourceJSON.MsgApplicantIsNotApplicableForAnyEmployment+" personnel@dadeschools.net.<br>");
									isOCPCChecked = false;
								}
							}
						});
					}catch(err){}
				}
				if(isOCPCChecked){
					$('#loadingDiv').show();
					if(statusNoteFileName!="" && statusNoteFileName!=null){
						document.getElementById("frmStatusNote").submit();
					}else{					
						PanelAjax.saveStatusNoteForPanel(jobForTeacherId,fitScore,teacherStatusNoteId,teacherId,jobId,statusId,secondaryStatusId,setHiredDate,statusNotes,statusNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_arrary,answerScore_arrary,isEmailTemplateChanged,statusShortName,msgSubject,$('#mailBody').find(".jqte_editor").html(),txtschoolCount,schoolId,isEmailTemplateChangedTeacher,msgSubjectTeacher,$('#mailBodyTeacher').find(".jqte_editor").html(),chkOverride,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryDiv,
						{
							async:false,
							errorHandler:handleError,
							callback:function(data)
							{
								jWTeacherStatusNotesOnClose();
								getStatusForPanel();
							}
						});	
					}
				}
			}catch(err){}
		}
	}
}
function saveStatusNoteFile(statusNoteDateTime)
{
	var jobCategoryDiv=document.getElementById("jobCategoryDiv").value;
	var txtPanel=document.getElementById("txtPanel").value;
	var txtOverride=document.getElementById("txtOverride").value;
	var txtLoggedInUserPanelAttendees=document.getElementById("txtLoggedInUserPanelAttendees").value;
	var iQdslider=document.getElementById("iQdslider").value;
	var chkOverride=false;
	if(txtOverride==1)
	{
		chkOverride=document.getElementById('chkOverride').checked;
	}
	var questionAssessmentIds = document.getElementById('questionAssessmentIds').value;
	var qqNoteIds = document.getElementById('qqNoteIds').value;
	var qqNoteIdsvalue_arr = qqNoteIds.split("#");
	var qqNoteIdsvalue_str ="";
	
	if(qqNoteIds!=null && qqNoteIds!="")
	{
		for(var i=0;i<qqNoteIdsvalue_arr.length;i++)
		{
			if(qqNoteIdsvalue_str!="")
				qqNoteIdsvalue_str = qqNoteIdsvalue_str+"#"+$('#'+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html();
			else
				qqNoteIdsvalue_str = $('#'+qqNoteIdsvalue_arr[i]).find(".jqte_editor").html();
		}
	}
	/*====== Gagan [ Start ] : variable for status wise email templates ================*/
	var isEmailTemplateChanged	=	$('#isEmailTemplateChanged').val();
	var statusShortName	=	$("#statusShortName").val(statusShortName);
	var msgSubject	=	$('#subjectLine').val();
	//var charCount	=	$('#mailBody').find(".jqte_editor").text();
	var isEmailTemplateChangedTeacher	=	$('#isEmailTemplateChangedTeacher').val();
	var msgSubjectTeacher	=	$('#subjectLineTeacher').val();
	/*====== Gagan [ END ] : variable for status wise email templates ================*/
	// Start for save Question score
	
	try{
		var iframeNorm = document.getElementById('ifrmStatusNote');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('statusNoteFrm');
		document.getElementById("scoreProvided").value=inputNormScore.value;
	}catch(e){}	
	var scoreProvided=trim(document.getElementById("scoreProvided").value);
	
	var requisitionNumberId=null;
	try{  
		requisitionNumberId=document.getElementById("requisitionNumber").value;
	}catch(e){}
	
	var answerId_arrary = new Array();
	var answerScore_arrary = new Array();
	
	
	//@AShish :: define questionsDetails and questionAssessmentIds
	var questionAssessmentIds = document.getElementById('questionAssessmentIds').value;
	
	var answerFinalize=0;
	var sumAnswerScore=0;
	var sumAnswerScore=0;
	var iCountScoreSlider=document.getElementById("iCountScoreSlider").value;
	if(iCountScoreSlider>0)
	{
		for(var i=1;i<= iCountScoreSlider;i++)
		{
			var questionscore=0;
			var answerId=0;
			
			try{
				var ifrmQuestion = document.getElementById('ifrmQuestion_'+i);
				if(ifrmQuestion!=null)
				{
					var innerQuestion = ifrmQuestion.contentDocument || ifrmQuestion.contentWindow.document;
					var inputQuestionFrm = innerQuestion.getElementById('questionFrm');
					var answerIdFrm = innerQuestion.getElementById('answerId');
					questionscore=inputQuestionFrm.value;
					answerId=answerIdFrm.value;
					
					sumAnswerScore=(parseInt(sumAnswerScore)+parseInt(questionscore));
					
					answerId_arrary[i-1]=answerId;
					answerScore_arrary[i-1]=questionscore;
					
					if(questionscore==0)
					{
						answerFinalize++;
					}
				}
			}catch(e){}	
		}
	}
	var iCountScoreSlider=document.getElementById("iCountScoreSlider").value;
	var isQuestionEnable=document.getElementById("isQuestionEnable").value;
	// End for save Question score
	
	
	var jobForTeacherId =document.getElementById("jobForTeacherIdForSNote").value;
	var fitScore=document.getElementById("fitScoreForStatusNote").value;
	var statusNotes = $('#statusNotes').find(".jqte_editor").html();
	var teacherId = document.getElementById("statusTeacherId").value;
	var jobId = document.getElementById("jobId").value;
	var emailSentToD =0;// document.getElementById("emailSentToD").checked;
	var emailSentToS =0;// document.getElementById("emailSentToS").checked;
	var statusId=document.getElementById("statusIdForStatusNote").value;
	var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
	var finalizeStatus=document.getElementById("finalizeStatus").value;
	var teacherStatusNoteId=document.getElementById("teacherStatusNoteId").value;
	var statusNoteFileName=null;
	try{
		statusNoteFileName=document.getElementById("statusNoteFileName").value;
	}catch(e){}
	var scoreProvided=trim(document.getElementById("scoreProvided").value);
	var sNoteFileName="";
	var bEmailSendFor_DA_SA=document.getElementById('email_da_sa').checked;
	var bEmailSendFor_CA=false;//document.getElementById('email_ca').checked;
	
	var txtschoolCount=0;
	var schoolId=0;
	if(finalizeStatus==1)
	{
		try{
			txtschoolCount=document.getElementById("txtschoolCount").value;
		}catch(err){}
		if(txtschoolCount > 1)
		{
			try{
				schoolId=document.getElementById("schoolIdCG").value;
			}catch(err){}
		}
	}
	
	var statusName = document.getElementById("statusName").value;
	var setHiredDate ="";
	try{
		if(statusNoteFileName!="" && statusNoteFileName!=null)
		{
			var ext = statusNoteFileName.substr(statusNoteFileName.lastIndexOf('.') + 1).toLowerCase();	
			sNoteFileName="statusNote"+statusNoteDateTime+"."+ext;
		}
		PanelAjax.saveStatusNoteForPanel(jobForTeacherId,fitScore,teacherStatusNoteId,teacherId,jobId,statusId,secondaryStatusId,setHiredDate,statusNotes,sNoteFileName,emailSentToD,emailSentToS,scoreProvided,finalizeStatus,bEmailSendFor_DA_SA,bEmailSendFor_CA,answerId_arrary,answerScore_arrary,isEmailTemplateChanged,statusShortName,msgSubject,$('#mailBody').find(".jqte_editor").html(),txtschoolCount,schoolId,isEmailTemplateChangedTeacher,msgSubjectTeacher,$('#mailBodyTeacher').find(".jqte_editor").html(),chkOverride,requisitionNumberId,qqNoteIdsvalue_str,questionAssessmentIds,jobCategoryDiv,
			{
				async:false,
				errorHandler:handleError,
				callback:function(data)
				{
				jWTeacherStatusNotesOnClose();
				getStatusForPanel();
			}
		});	
		}catch(err){}
}
function editShowStatusNote(teacherStatusNoteId)
{
	$('#errorStatusNote').empty();
	var fitScore=document.getElementById("fitScoreForStatusNote").value;
	ManageStatusAjax.editShowStatusNote(teacherStatusNoteId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("teacherStatusNoteId").value=data.teacherStatusNoteId;
			var fileName=null;
			var filePath=null;
			try
			{
				fileName=data.statusNoteFileName;
				filePath="statusNote/"+data.teacherDetail.teacherId;
				if(fileName!=null&& fileName!=""){
					document.getElementById("showStatusNoteFile").innerHTML="<a href=\"javascript:void(0)\" id=\"commfileforedit\" onclick=\"downloadCommunication('"+filePath+"','"+fileName+"','commfileforedit');if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;\">"+fileName+"</a>&nbsp;<a href=\"#\" onclick=\"return deleteStatusNoteChk("+data.teacherStatusNoteId+",'attach')\"> "+resourceJSON.BtnRemove+"</a>";
				}else{
					document.getElementById("showStatusNoteFile").innerHTML="";
				}
			}catch(err){ alert(err);}
			
			$('#statusNotes').find(".jqte_editor").html(data.statusNotes);
			$('#statusNotes').find(".jqte_editor").focus();
			document.getElementById("noteMainDiv").style.display="inline";
		}
	});
}
function deleteStatusNoteChk(teacherStatusNoteId,deleteType)
{
	$('#jWTeacherStatusNotesDiv').modal('hide');
	document.getElementById("statusNoteDeleteType").value=deleteType;
	document.getElementById("teacherStatusNoteId").value=teacherStatusNoteId;
	if(deleteType=="attach"){
			document.getElementById("statusNoteMsg").innerHTML=""+resourceJSON.MsgAreUSure+"";	
	}else{
		document.getElementById("statusNoteMsg").innerHTML=""+resourceJSON.MsgRemoveTheNote+"";
	}
	$('#modalStatusNoteMsg').modal('show');
}
function closeStatusNoteMsg(flag){
	$('#modalStatusNoteMsg').modal('hide');
	var deleteType=document.getElementById("statusNoteDeleteType").value;
	var teacherStatusNoteId=document.getElementById("teacherStatusNoteId").value;
	if(flag==1){
		deleteStatusNote(teacherStatusNoteId,deleteType);
	}else{
		$('#jWTeacherStatusNotesDiv').modal('show');
	}
}
function deleteStatusNote(teacherStatusNoteId,deleteType)
{
	$('#loadingDiv').show();
	ManageStatusAjax.deleteStatusNote(teacherStatusNoteId,deleteType,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			$('#loadingDiv').hide();
			document.getElementById("showStatusNoteFile").innerHTML="";
			var statusId=document.getElementById("statusIdForStatusNote").value;
			var secondaryStatusId=document.getElementById("secondaryStatusIdForStatusNote").value;
			var fitScore=document.getElementById("fitScoreForStatusNote").value;
			jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
			removeStatusNoteFile();
			var deleteTypeChk=document.getElementById("statusNoteDeleteType").value;
			if(deleteTypeChk=="attach"){
				/*if(document.getElementById("sliderStatusNote").style.display=="inline"){
					try{
						document.getElementById("ifrmStatusNote").src="slideract.do?name=statusNoteFrm&tickInterval=5&max="+fitScore+"&swidth=580&svalue="+data.scoreProvided;
					}catch(e){}
				}*/
			}
			
			if(deleteTypeChk!="attach"){
				document.getElementById("noteMainDiv").style.display="none";
				document.getElementById("teacherStatusNoteId").value=0;
				$('#statusNotes').find(".jqte_editor").html("");
				document.getElementById("showStatusNoteFile").innerHTML="";
			}
		}});
	return false;
}



function updateStatusSpecificScore(answerId,score)
{
	ManageStatusAjax.updateStatusSpecificScore(answerId,score,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			/*var scoreMaster=0;
			try{
				var iframeNorm = top.document.getElementById('ifrmStatusNote');
				if(iframeNorm!=null)
				{
					var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
					var inputNormScore = innerNorm.getElementById('statusNoteFrm');
					scoreMaster=inputNormScore.value;
				}
			}catch(e){}	
			
			var maxScoreValue=top.document.getElementById("fitScoreForStatusNote").value;
			var finalMaxScore=(parseInt(max)+parseInt(maxScoreValue));
			var finalScore=(parseInt(scoreMaster)+parseInt(score));
			var strUrl="slideract.do?name=statusNoteFrm&tickInterval=5&max="+finalMaxScore+"&swidth=536&dslider=1&svalue="+finalScore+"&step=1";
			top.document.getElementById('ifrmStatusNote').src = strUrl;*/
		}
	});
}

function getStatusForPanel()
{
		$('#loadingDiv').show();
		ManageStatusAjax.getStatusForPanel($("#panelId").val(),$("#userId").val(),
		{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				setStatusTilteFor2ndDiv(data[0],data[1],data[2]);
				document.getElementById("jobForTeacherIdForSNote").value=data[6];
				jWTeacherStatusNotesOnOpen(data[3],data[4],data[5]);
				$('#loadingDiv').hide();
				//setStatusTilteFor2ndDiv('Ron Burgundy','Screening Complete','oth');jWTeacherStatusNotesOnOpen('15','16','1428');
			}
		});
}


//@Ashish :: for Get Qualification Question Note Ids
function getQuestionNotesIds()
{
	try{
		var qqNote = document.getElementById("qqNoteIds").value;
		if(qqNote!="")
		{
			var qqN = qqNote.split("#");
			for(var i=0;i<qqN.length;i++)
			{
				$('#'+qqN[i]).find(".jqte").width(662);
			}
		}
	}catch(err){}

}

function updateStatusOverrideSkip(param){
	document.getElementById("jobCategoryDiv").value=param;
	$('#updateStatusModal').modal('hide');
	jWTeacherStatusNotesOnClose();
	saveStatusNote(1);
}
function cancelUpdateStatus(){
	$('#updateStatusModal').modal('hide');
	jWTeacherStatusNotesOnClose();
}

/*	
 * Dated		:	30-07-2014
 * Edited By	:	Hanzala Subhani
 * Purpose		:	Open Transcript DIV
 * 
 * */
function applyScrollOnTblTrans()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#transTableHistory').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 653,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,150,80,120,210],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function applyScrollOnTblTransAcademic()
{
	var $j=jQuery.noConflict();
	    $j(document).ready(function() {
	    $j('#academicGridAcademic').fixheadertable({ //table id 
	    caption: '',
	    showhide: false,
	    theme: 'ui',
	    height: 150,
	    width: 670,
	    //width: 825,
	    minWidth: null,
	    minWidthAuto: false,
	    colratio:[30,133,120,150,150,87],
	    addTitles: false,
	    zebra: true,
	    zebraClass: 'net-alternative-row',
	    sortable: false,
	    sortedColId: null,
	    dateFormat: 'd-m-y',
	    pager: false,
	    rowsPerPage: 10,
	    resizeCol: false,
	    minColWidth: 100,
	    wrapper: false
	    });
    });	
}

function applyScrollOnTblReferenceCheck()
{
	var $j=jQuery.noConflict();
	    $j(document).ready(function() {
	    $j('#referenceCheckGrid').fixheadertable({ //table id 
	    caption: '',
	    showhide: false,
	    theme: 'ui',
	    height: 150,
	    width: 670,
	    minWidth: null,
	    minWidthAuto: false,
	    colratio:[30,150,230,130,130],
	    addTitles: false,
	    zebra: true,
	    zebraClass: 'net-alternative-row',
	    sortable: false,
	    sortedColId: null,
	    dateFormat: 'd-m-y',
	    pager: false,
	    rowsPerPage: 10,
	    resizeCol: false,
	    minColWidth: 100,
	    wrapper: false
	    });
    });	
}

var pageForDT 			= 	1;
var noOfRowsForDT 		= 	10;
var sortOrderStrForDT	=	"";
var sortOrderTypeForDT	=	"";

function defaultSet(){
	pageForTC 			=	1;
	noOfRowsForTC 		=	100;
	noOfRowsForJob		=	50;
	sortOrderStrForTC	=	"";
	sortOrderTypeForTC	=	"";
	currentPageFlag		=	"";
}

/*function open_div_trans(t_id,j_id,e_id) {
	defaultSet();
	TransDivOpen(t_id,j_id,e_id);
}

function TransDivOpen(t_id,j_id,e_id)
{
	currentPageFlag='ts';
	ManageStatusAjax.TransDivOpen(t_id,j_id,e_id,noOfRowsForTC,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,currentPageFlag,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		document.getElementById("divTransAcademic").innerHTML=data;
		applyScrollOnTblTransAcademic();
	}});
}*/

function showAcademicQues(){
	var counter = 0;
	var transcript_data	=	document.getElementsByName("transcript_data[]");
	for(var i=0; i < transcript_data.length; i++){
		if(transcript_data[i].checked == true){			
			counter++;
		}
	}
	//alert("counter== "+counter);

	if(counter > 0){ 
		$('textarea').jqte();
		document.getElementById("academicGridAcademicNotes").style.display="block";
	} else {
		$('#academicGridAcademicNotes').hide();
	}
}

function showReferenceCheck(){
	var counter = 0;
	var referenceData	=	document.getElementsByName("referenceData[]");
	for(var i=0; i < referenceData.length; i++){
		if(referenceData[i].checked == true){			
			counter++;
		}
	}
	//alert("counter== "+counter);

	if(counter > 0){ 
		$('textarea').jqte();
		document.getElementById("referenceGridSendEmail").style.display="block";
	} else {
		$('#referenceGridSendEmail').hide();
	}
}

/*
 * 	Dated		:	31-07-2014
 * 	By			:	Hanzala Subhani
 * 	Purpose		:	Save Transcript Data
 */

function saveTransAcademic(id){
	$('#errorStatusNote').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var AcademicIDs	=	"";
	var status 		= 	id;
	document.getElementById("formId").value = id;

	var teacherIdTrans		=	document.getElementById("teacherIdTrans").value
	var eligibilityIdTrans	=	document.getElementById("eligibilityIdTrans").value
	var jobIdTrans			=	document.getElementById("jobIdTrans").value

	var StatusMaster3		=	document.getElementById("StatusMaster3").value
	var split				=	StatusMaster3.split("#$#");
	var graduationDate		=	"";
	var transcript_data		=	document.getElementsByName("transcript_data[]");
	var counter;

	var teacherFName		=	document.getElementById("teacherFName").value;
	var teacherLName		=	document.getElementById("teacherLName").value;
	var secondaryStatus		=	document.getElementById("secondaryStatus").value;
	
	//var fitScore			=	document.getElementById("fitScore").value;

	var fitScore			=	document.getElementById("fitScoreForStatusNote").value;
	
	
	var statusId			=	document.getElementById("statusId").value;
	var secondaryStatusId	=	document.getElementById("secondaryStatusId").value;
	
	var fullTeacherName		=	teacherFName+" "+teacherLName;
	var statusName			=	'Ofcl Trans / Veteran Pref';
	var statusShortName		=	'oth';
	var graduationCounter	=	0;
	for(var i=0; i < transcript_data.length; i++){

		if(transcript_data[i].checked == true){
			graduationDate	=	document.getElementById("graduationDate_"+transcript_data[i].value).value;
			AcademicIDs	 =	AcademicIDs+"##"+transcript_data[i].value;
			if(id == 22 && (graduationDate == "" || graduationDate == null)){
				if(counter == 0){
					$('#errorStatusNote').append("&#149; "+resourceJSON.MsgDegreeConferredDate+".<br>");
					$("#graduationDate_"+transcript_data[i].value).focus();
					$('#errorStatusNote').show();
				}

				counter++;
				focus++;

			}else if(id == 22 && (graduationDate != "" || graduationDate != null)){
				if(counter == 0){
					saveGraduationDate(transcript_data[i].value);
					if (isDate(graduationDate)==false){
						graduationCounter	=	graduationCounter+1;
					}
				}
			}
		}
	}
	if(counter == 0 && graduationCounter == 0){
		ManageStatusAjax.saveNotesTrans(status,teacherIdTrans,eligibilityIdTrans,jobIdTrans,AcademicIDs,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
			}});
	}
}

function saveGraduationDate(id){
	var graduationDate	=	document.getElementById("graduationDate_"+id).value;
	if (isDate(graduationDate)==false){
		//dt.focus()
		document.getElementById("graduationDate_"+id).focus();
		return false;
	} else {
		ONRAjax.saveGraduationDate(id,graduationDate,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				// $('.onrNotesShowTrans').modal('hide');
			}});
	}
}

var dtCh= "-";
var minYear=1951;
var d = new Date();
var maxYear=d.getFullYear();

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	$('#errorStatusNote').empty();
	var daysInMonth = 	DaysArray(12)
	var pos1		=	dtStr.indexOf(dtCh)
	var pos2		=	dtStr.indexOf(dtCh,pos1+1)
	var strMonth	=	dtStr.substring(0,pos1)
	var strDay		=	dtStr.substring(pos1+1,pos2)
	var strYear		=	dtStr.substring(pos2+1)
	strYr			=	strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){

		$('#errorStatusNote').append("&#149; "+resourceJSON.MsgDateFormat+".<br>");
		$('#errorStatusNote').show();
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){

		$('#errorStatusNote').append("&#149; "+resourceJSON.PlzEtrValidMonth+".<br>");
		$('#errorStatusNote').show();
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
	
		$('#errorStatusNote').append("&#149; "+resourceJSON.PlzValidDate+".<br>");
		$('#errorStatusNote').show();
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		
		$('#errorStatusNote').append('&#149; '+resourceJSON.PlzEtrValid4DigitYear+' '+minYear+' '+resourceJSON.MsgAnd+' '+maxYear+'.<br>');
		$('#errorStatusNote').show();
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		
		$('#errorStatusNote').append("&#149; "+resourceJSON.PlzValidDate+".<br>");
		$('#errorStatusNote').show();
		return false
	}
	return true
}

function canleGraduationDate(id){
	var graduationDate	=	document.getElementById("graduationDate_"+id).value;
	document.getElementById("graduationDate_"+id).value = "";
	if(graduationDate != null || graduationDate != ""){
	ONRAjax.canleGraduationDate(id,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			//$('.onrNotesShowTrans').modal('hide');
			//document.getElementById("divTransAcademic").innerHTML=data;
		}});
	}
}

