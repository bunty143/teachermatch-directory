
function applyScrollOnStatusNote_mass()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblStatusNote_mass').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 655,
        minWidth: null,
        minWidthAuto: false,
        colratio:[380,76,100,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function cgMassUpdate(clickFlag){
	$('#loadingDiv').show();
	var jobForTeacherIds_mass = new Array();
	jobForTeacherIds_mass	=	getSendMessageTeacherIds();
	//TeacherSendMessageAjax
	CGServiceAjax.getTeacherIds(jobForTeacherIds_mass,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				document.getElementById("jobForTeacherIds_mass").value=jobForTeacherIds_mass;
				document.getElementById("teacherIds_mass").value=data;
				
				var jobId = document.getElementById("jobId").value;
				
				document.getElementById("statusNotesTeacherIds_CGMass").value=data;
				document.getElementById("statusNotesJobId_CGMass").value=jobId;
				
				$('#myModalStatus_mass').modal('show');
				document.getElementById("status_l_title_mass").innerHTML=""+resourceJSON.MsgStatusLifeCycle+"";
				displayStatusDashboard_mass(clickFlag);
				finalizeOrNot(doActivity);
				
			}
			else
			{
				$('#loadingDiv').hide();
			}
		}
	});
}


function displayStatusDashboard_mass(clickFlag){
	var jobId = document.getElementById("jobId").value;
	MassStatusUpdateCGAjax.displayStatusDashboard_CGMass(jobId,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	$('#loadingDiv').hide();
			document.getElementById("divStatus_mass").innerHTML=data.toString();	
		}
	});
}

function hideStatus_mass()
{
	
	try { $('#myModalStatus_mass').modal('hide'); } catch (e) {}
	try { $('#myModalStatusInfo_mass').modal('hide'); } catch (e) {}
	try { $('#myModalEmail_mass').modal('hide'); } catch (e) {}
	try { $('#myModalPreCheckInfoCGMass').modal('hide'); } catch (e) {}
	try { $('#myModalConfirmInfoCGMass').modal('hide'); } catch (e) {}
	try { $('#myModalMsgShowSuccessCGMass').modal('hide'); } catch (e) {}
	
	var refreshGridCGMass = document.getElementById("refreshGridCGMass").value;
	if(refreshGridCGMass=='1')
	{
		document.getElementById("refreshGridCGMass").value="0";
		getCandidateGrid();
		refreshStatus();
	}
}

function showStatusDetailsOnClose(){
	try{
		$('#myModalStatusInfo_mass').modal('hide');
		document.getElementById("noteMainDiv_mass").style.display="none";
		$('#myModalStatus_mass').modal('show');
	}
	catch(err){}
}

function showStatusDetails_mass(statusName,statusId,secondaryStatusId)
{
	$('#schoolName_mass1').hide();
	var entityType=$('[name="entityType"]').val();
	document.getElementById("schoolAutoSuggestDivId_mass").style.display="none";
	$('#loadingDiv').show();
	$('#errorStatusNote_mass').empty();
	try{
	$('#divStatusNoteGrid_onlineActivity').remove();
	}catch(e){}
	
	document.getElementById("userActionCGMass").value=0;
	
	try {
		document.getElementById("showTemplateslist_mass").style.display="none";
	} catch (e) {}
	
	document.getElementById("noteMainDiv_mass").style.display="none";
	
	var teacherIds_mass=document.getElementById("teacherIds_mass").value;
	var jobId = document.getElementById("jobId").value;
	
	try { document.getElementById("statusId_mass").value=statusId; } catch (e) {}
	try { document.getElementById("secondaryStatusId_mass").value=secondaryStatusId; } catch (e) {}
	
	document.getElementById("dispalyStatusName_mass").value=statusName;
	var districtId=document.getElementById("districtId").value;
	MassStatusUpdateCGAjax.getStatusDetailsInfo_CGMass(teacherIds_mass,jobId,statusId,secondaryStatusId,
	{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				$('#myModalStatus_mass').modal('hide');
				$('#loadingDiv').hide();
				document.getElementById("status_title_mass").innerHTML=statusName;
				document.getElementById("divStatusNoteGrid_mass").innerHTML=data[0];
				applyScrollOnStatusNote_mass();
				document.getElementById("checkOptions_mass").style.display="inline";
				
				var txtschoolCount=data[10];
				if(txtschoolCount > 1)
				{
					document.getElementById("txtschoolCount_mass").value=txtschoolCount;
					document.getElementById("schoolAutoSuggestDivId_mass").style.display="block";
				}
				else
				{
					document.getElementById("schoolAutoSuggestDivId_mass").style.display="none";
				}
				
				
				finalizeOrNot_mass(data[3]);
				
				if(statusName == "Withdrew" || statusName == "Declined"){
					$("#timeAndReason_mass").show();
				}
				
				if(data[6]==0 && (statusName=="Hired" || statusName=="Offer Ready"))
				{
					requisitionNumbers_mass();
				}
				else
				{
					document.getElementById("requisitionNumbers_mass").innerHTML="";
					document.getElementById("requisitionNumbersGrid_mass").style.display="none";
				}
				
				if(data[11]=='1')
				   document.getElementById('email_da_sa_mass').checked=true;
				else
					document.getElementById('email_da_sa_mass').checked=false;
				
				if(data[1]==1)
				{
					document.getElementById("myModalPreCheckInfoCGMassLabel").innerHTML=statusName;
					$('#myModalStatusInfo_mass').modal('hide');
					$('#myModalPreCheckInfoCGMass').modal('show');
					$('#myModalStatusInfo_mass').modal('hide');
					document.getElementById("errordivCGMass").innerHTML=data[2];
					
					if(data[12]==1)
						document.getElementById("mass_cgInfo_Continue").style.display="inline";
					else
						document.getElementById("mass_cgInfo_Continue").style.display="none";
					
				}
				else
				{
					//added by 08-04-2015
					if(statusName=="Online Activity"){
						CGServiceAjax.displayMultipleOnlineActivityForCG(teacherIds_mass,jobId,districtId,
								{ 
									async: false,
									errorHandler:handleError,
									callback:function(data)
									{	
								var tableDiv="<div id='divStatusNoteGrid_onlineActivity' style='width:650px;overflow:hidden;'>";
								var tableDivEnd="</div>";
									$('#divStatusNoteGrid_mass').before(tableDiv+data+tableDivEnd);
										applyMultiplyOnlineActivity(); 
									}
								});
						
					}
					$('#myModalStatusInfo_mass .row').css('width','100%');
					//ended by 08-04-2015					
					$('#myModalStatusInfo_mass').modal('show');	
				}
				//added by 12-06-2015
				//alert("$('#isRequiredSchoolIdCheck').val()===="+$('#isRequiredSchoolIdCheck').val());
				//alert("$('#isSchoolOnJobId').val()===="+$('#isSchoolOnJobId').val());
				if($('#isRequiredSchoolIdCheck').val()==1 && $('#isSchoolOnJobId').val()==1 && entityType== 2)
				{	
					document.getElementById("schoolAutoSuggestDivId_mass").style.display="block";
					$('#schoolName_mass').hide();
					$('#schoolName_mass1').show();
					$('#schoolName_mass1').prop('disabled', false);
					document.getElementById("schoolName_mass1").value="";
					document.getElementById("schoolId_mass").value="0";
					if($('#alreadyExistsSchoolInStatus').val()==1){						
						if(entityType== 2)
						{		
							document.getElementById("schoolName_mass1").value = $('#schoolNameValue').val().replace(/!!/g , " ");
							document.getElementById("schoolId_mass").value = $('#schoolIdValue').val();
							$('#schoolName_mass1').prop('disabled', true);
						}
					}
				}
				//ended by 12-06-2015
				
				$('textarea').jqte();
				
				
				//Only for Jeffco added Date 10-09-2015
				//alert($("[name='slcStartDateOnOrOff']").val()+"   "+$("#districtId").val()+"      "+statusName);
				try{
					try{
						$('#startDateDiv').remove();
					}catch(e){}
					if($("[name='slcStartDateOnOrOff']").val()=="ON" && $("#districtId").val()=='804800' && statusName=='Offer Made'){
						$('#schoolAutoSuggestDivId_mass').after('<div class="row mt5" id="startDateDiv"><div class="span12 top5 col-md-2 form-group" style="width:100%;">'+
								'<label for="text" style="float:left;margin-right:5px;padding-top:7px;">Start Date<span class="required">*</span></label>'+
								'<div><input type="text" value="" id="startDateId_mass" name="startDate_mass" class="form-control" maxlength="50" onmouseup="setDateField(this);" onkeydown="return keyNotWork(event);" oncontextmenu="return false;" style="width:150px;"></div>'+
								'</div></div>');
						//$('[name="startDate"]').val($('[name="isStartDate"]').val());
					}
					}catch(e){}
				//ENDed Only for Jeffco added Date 10-09-2015
					
					
			}
			//********************************added by 17-06-2015******************************************//
			try{
				//alert(statusName+"   "+entityType+"   districtId==="+$("#districtId").val());
				if(statusName=='Credential Review' && entityType==3 && $("#districtId").val()=='1201470'){
					var offerShowFlag="0";
					offerShowFlag=document.getElementById("doActivityForOfferReady").value;
					if(offerShowFlag=="0" || offerShowFlag==""){
						finalizeOrNoForOfferReady_mass(offerShowFlag);
					}
				}
			}catch(err){}
			//****************************************added by 17-06-2015************************************//
			setButton();
		}
	});
	
	
}

//********************************added by 17-06-2015******************************************//
function finalizeOrNoForOfferReady_mass(doActivity){
	document.getElementById("doActivityForOfferReady").value=doActivity;
	if(doActivity=='1'){
		document.getElementById("statusSave_mass").style.display="inline";
		document.getElementById("statusFinalize_mass").style.display="inline";
	}else{
		document.getElementById("statusSave_mass").style.display="none";
		document.getElementById("statusFinalize_mass").style.display="none";
	}
	document.getElementById('email_da_sa_mass').checked=false;
}
//********************************ended by 17-06-2015******************************************//

//added by 08-04-2015
function sendMultipleOnlineActivityLink(obj,questionSetId,teacherIds_mass)
{
	var jobId = document.getElementById("jobId").value;	
	MassStatusUpdateCGAjax.sendMultipleOnlineActivityLink(teacherIds_mass,jobId,questionSetId,{ 
 		async: true,
 		errorHandler:handleError,
 		callback:function(data)
 		{
		$(obj).html('Resend');		
 		}
     });
}

function applyMultiplyOnlineActivity()
{
	var $j=jQuery.noConflict();	
        $j(document).ready(function() {
        $j('#tblOnlineActivityMass').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 650,
        minWidth: null,
        minWidthAuto: false,
        colratio:[550,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        }); 
}
//ended by 08-04-2015

function showStatusInfo_CGMass()
{
	try{
		$('#myModalPreCheckInfoCGMass').modal('hide');
		$('#myModalStatusInfo_mass').modal('show');
	}
	catch(err){}
}

function openNoteDiv_mass(DistrictId){
	$('#statusNotes_mass').find(".jqte_editor").html("");
	ManageStatusAjax.statusWiseSectionList(DistrictId,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	if(data!=null){
				document.getElementById("templateDivStatus_mass").style.display="block";
				$('#statuswisesection_mass').html(data);
		}
		}
	});
	
	document.getElementById("noteMainDiv_mass").style.display="inline";
	document.getElementById("showStatusNoteFile_mass").innerHTML="";
	document.getElementById("showTemplateslist_mass").style.display="none";
	removeStatusNoteFile_mass();
	$('#errorStatusNote_mass').hide();
}

function getsectionwiselist_mass(value){
	document.getElementById("showTemplateslist_mass").style.display="none";
	$('#statusNotes_mass').find(".jqte_editor").html("");
	if(value!=0){
						 
		MassStatusUpdateCGAjax.getSectionWiseTemaplteslist_CGMass(value,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			
			document.getElementById("addTemplateslist_mass").innerHTML=data;
			document.getElementById("showTemplateslist_mass").style.display="block";
			}
		});
	}
}

function addStatusNoteFileType_mass()
{
	$('#statusNoteFileNames_mass').empty();
	$('#statusNoteFileNames_mass').html("<a href='javascript:void(0);' onclick='removeStatusNoteFile_mass();'><img src='images/can-icon.png' title='remove'/></a><br> <input id='statusNoteFileName_mass' name='statusNoteFileName_mass' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeStatusNoteFile_mass()
{
	$('#statusNoteFileNames_mass').empty();
	$('#statusNoteFileNames_mass').html("<a href='javascript:void(0);' onclick='addStatusNoteFileType_mass();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addStatusNoteFileType_mass();'>"+resourceJSON.AttachFile+"</a>");
}

function setTemplateByLIstId_mass(templateId){	
	$('#statusNotes_mass').find(".jqte_editor").html("");
	if(templateId!=0){
		ManageStatusAjax.getTemplateByLIst(templateId,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			$('#statusNotes_mass').find(".jqte_editor").html(data);
			}
		});
	}
}


function getStatusWiseEmailForAdmin_mass()
{
	var jobId = document.getElementById("jobId").value;
	var statusId=0,secondaryStatusId=0;
	try { statusId=document.getElementById("statusId_mass").value; } catch (e) {}
	try { secondaryStatusId=document.getElementById("secondaryStatusId_mass").value; } catch (e) {}
	
	try{ $('#myModalStatusInfo_mass').modal('hide'); }catch(e){}
	
	$('#loadingDiv').show();
	MassStatusUpdateCGAjax.getStatusWiseEmailForAdmin_CGMass(jobId,statusId,secondaryStatusId,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		    $('#loadingDiv').hide();
			if(data!=null)
			{
				$('#subjectLine_mass').val(data.subjectLine);
				$('#mailBody_mass').find(".jqte_editor").html(data.templateBody);
				try{  
					//$('#myModalStatusInfo_mass').modal('hide');
					document.getElementById("noteMainDiv_mass").style.display="none";
				}catch(e){}
				
				try{  
					$('#myModalEmail_mass').modal('show');
				}catch(e){}
			}
			else
			{
				$('#myModalStatusInfo_mass').modal('show');
				document.getElementById("email_da_sa_mass").disabled=true;
			}
		}
	});
}


function sendOriginalEmail_mass()
{
	$('#isEmailTemplateChanged_mass').val(0);
	emailClose_mass();
}

function emailClose_mass(){
	try{
		$('#myModalEmail_mass').modal('hide'); 
		$('#myModalStatusInfo_mass').modal('show');	
	}
	catch(err){}
}

function sendChangedEmail_mass()
{
	$('#isEmailTemplateChanged_mass').val(1);
	var msgSubject=$('#subjectLine_mass').val();
	var charCount	=	$('#mailBody_mass').find(".jqte_editor").text();
			
	$('#errordivEmail_mass').empty();
	$('#subjectLine_mass').css("background-color", "");
	$('#mailBody_mass').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	if(trim(msgSubject)=="")
	{
		$('#errordivEmail_mass').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
		if(focs==0)
			$('#subjectLine_mass').focus();
		$('#subjectLine_mass').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(charCount.length==0)
	{
		$('#errordivEmail_mass').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
		if(focs==0)
			$('#mailBody_mass').find(".jqte_editor").focus();
		$('#mailBody_mass').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(cnt==0)
	{
		emailClose_mass();
		return false;
	
	}
	else
	{
		$('#errordivEmail_mass').show();
		return false;
	}
}

function finalizeOrNot_mass(doActivity){
	if(doActivity=='1'){
		document.getElementById("statusSave_mass").style.display="none";
		document.getElementById("statusFinalize_mass").style.display="inline";
	}else{
		document.getElementById("statusSave_mass").style.display="none";
		document.getElementById("statusFinalize_mass").style.display="none";
	}
	document.getElementById('email_da_sa').checked=false;
}

function preCheckClose_mass(){
	try{
		$('#myModalPreCheckInfoCGMass').modal('hide'); 
		$('#myModalStatus_mass').modal('show');	
	}
	catch(err){}
}

function requisitionNumbers_mass()
{	
	var schoolId;
	var jobId=document.getElementById("jobId").value;
	var locationurl=window.location.href;
	if(locationurl.indexOf("teacherinfo") > -1)
	{
		schoolId=document.getElementById("schoolId").value;
	}
	var reqNumber=document.getElementById("reqNumber").value;
	var offerReady=0;
	try{
		offerReady=document.getElementById("offerReady").value;
	}catch(err){}
	
	ManageStatusAjax.requisitionNumbers(jobId,schoolId,offerReady,reqNumber,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			try{
				if(data!=""){
					document.getElementById("requisitionNumbers_mass").innerHTML=data;
					document.getElementById("requisitionNumbersGrid_mass").style.display="inline";
					try{
						document.getElementById("requisitionNumber").style.display="inline";
					}catch(err){}
				}else{
					document.getElementById("requisitionNumbers_mass").innerHTML="";
					document.getElementById("requisitionNumbersGrid_mass").style.display="none";
				}
			}catch(err){}
		}
	});
}


function validateStatusInfo_cgmass_filename(filenameCGMass)
{
	//alert("filenameCGMass "+filenameCGMass);
	
	document.getElementById("statusnoteFileNameCGMassTemp").value=filenameCGMass;
	validateStatusInfo_cgmass(99);
}

//****************************************Confirm Box *******************************************//
function getConfirm(header,confirmMessage,allTeacherId,buttonNameChange,callback){
	var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>-->"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<button type='button' class='btn btn-default' id='confirmFalse'>Cancel</button>"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#myModalStatusInfo_mass').after(div);
    confirmMessage = confirmMessage || '';
    $('#myModalStatusInfo_mass').modal('hide');
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#confirmbox #confirmMessage').html(confirmMessage);
    $('#confirmbox #myModalLabel').html(header);
    $('#confirmbox #confirmTrue').html(buttonNameChange);
    if(allTeacherId=='') $('#confirmFalse').hide();
    $('#confirmFalse').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        minOneScoreSliderSetFlag=2;
        $('#myModalStatusInfo_mass').modal('show');
        if (callback) callback(false);
    });
    $('#confirmTrue').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        document.getElementById("teacherIdsOSCEOLA_mass").value=allTeacherId;
        minOneScoreSliderSetFlag=1;
        if (callback) callback(true);
    });
}
//***************************************ended by 17-06-2015************************************//

function validateStatusInfo_cgmass(actionFlag)
{
	//alert("validateStatusInfo_cgmass actionFlag "+actionFlag)
	
	
	$('#errorStatusNote_mass').empty();
	var actionFlagTemp=actionFlag;
	if(actionFlag!="99")
	{
		//alert("!99");
		document.getElementById("isFinalizeCGMass").value=actionFlag;
		document.getElementById("statusnoteFileNameCGMassTemp").value="";
	}
	else
	{
		//document.getElementById("statusnoteFileNameCGMassTemp").value="";
		//alert("99");
	}
	
	var userActionCGMass=document.getElementById("userActionCGMass").value;
	
	var teacherIds_mass="";
	if(minOneScoreSliderSetFlag==1)// For Offer made OSCEOLA(1201470)
	teacherIds_mass=document.getElementById("teacherIdsOSCEOLA_mass").value;
	else
	teacherIds_mass=document.getElementById("teacherIds_mass").value;
	
	var jobId = document.getElementById("jobId").value;
	
	//alert("teacherIds_mass "+teacherIds_mass+" jobId "+jobId)
	
	var statusId=0,secondaryStatusId=0;
	try { statusId=document.getElementById("statusId_mass").value; } catch (e) {}
	try { secondaryStatusId=document.getElementById("secondaryStatusId_mass").value; } catch (e) {}
	
	var bSentEmailToDASA=false;
	try { bSentEmailToDASA=document.getElementById('email_da_sa_mass').checked; } catch (e) {}
	
	var statusFlag=$("#statusNoteflag_CGMass").val()=="true"?true:false;
	var topSliderDisable_CGMass=trim(document.getElementById("topSliderDisable_CGMass").value);
	var countQuestionSlider_CGMass=trim(document.getElementById("countQuestionSlider_CGMass").value);
	
	try{
		var iframeNorm = document.getElementById('ifrmTopSliderCGMass');
		if(iframeNorm!=null)
		{
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			var inputNormScore = innerNorm.getElementById('topSliderCGMass');
			document.getElementById("scoreProvided_CGMass").value=inputNormScore.value;
		}
		else
		{
			document.getElementById("scoreProvided_CGMass").value="0";
		}
		
	}catch(e){}

	var scoreProvided_CGMass=trim(document.getElementById("scoreProvided_CGMass").value);

	var answerId_arrary = new Array();
	var answerScore_arrary = new Array();

	var notSetScore_NoOfQuestions=0;
	var sumAnswerScore=0;
	var sumAnswerScore=0;
	if(countQuestionSlider_CGMass>0)
	{
		for(var i=1;i<= countQuestionSlider_CGMass;i++)
		{
			var questionscore=0;
			var answerId=0;
			
			try{
				var ifrmQuestion = document.getElementById('ifrmQuestion_msu_'+i);
				if(ifrmQuestion!=null)
				{
					var innerQuestion = ifrmQuestion.contentDocument || ifrmQuestion.contentWindow.document;
					var inputQuestionFrm = innerQuestion.getElementById('questionFrm_msu');
					var answerIdFrm = innerQuestion.getElementById('answerId_msu');
					questionscore=inputQuestionFrm.value;
					answerId=answerIdFrm.value;
					
					sumAnswerScore=(parseInt(sumAnswerScore)+parseInt(questionscore));
					
					answerId_arrary[i-1]=answerId;
					answerScore_arrary[i-1]=questionscore;
				}
			}catch(e){}
			
			if(questionscore==0)
			{
				notSetScore_NoOfQuestions++;
			}

		}
	}
	
	var bNoteEditervisible=false;
	var bNoteText=false;
	var statusNotes_mass="";
	try{
		if(document.getElementById("noteMainDiv_mass").style.display=="inline")
		{
			bNoteEditervisible=true;
			statusNotes_mass=$('#statusNotes_mass').find(".jqte_editor").text().trim();
			if(statusNotes_mass!=null && statusNotes_mass!="")
				 bNoteText=true;
			else
				 bNoteText=false;
		}
		else
		{
			bNoteEditervisible=false;
		}
	}catch(err){}
	
	var bReadyToNoteAttach=false;
	var bNoteAttach=false;
	var statusNoteFileName_mass=null;
	try{  
		statusNoteFileName_mass=document.getElementById("statusNoteFileName_mass").value;
		if(statusNoteFileName_mass!=null)
			bReadyToNoteAttach=true;
		
		if(statusNoteFileName_mass!="")
			bNoteAttach=true;
			
	}catch(e){}
	
	var masterQuestionIds=null;
	try{  
		masterQuestionIds=document.getElementById("masterQuestionId").value;
	}catch(e){}
	
	
	var masterQuestionIds_arrary = new Array();
	masterQuestionIds_arrary = masterQuestionIds.split("#");
	var sQuestionIds_Note_arrary = new Array(masterQuestionIds_arrary.length);
	if(masterQuestionIds_arrary!=null && masterQuestionIds_arrary.length > 0)
	{
		for(var i=0;i<masterQuestionIds_arrary.length;i++)
		{
			var qID="questionNotes"+masterQuestionIds_arrary[i];
			var sQuestion_Notes_temp=$('#'+qID).find(".jqte_editor").html();
			sQuestionIds_Note_arrary[i]=sQuestion_Notes_temp;
		}
	}
	
	
	var isEmailTemplateChanged_CGMass=0;
	var msgSubject_CGMass	="";	
	var adminmailBody_CGMass=""; 
	
	try { isEmailTemplateChanged_CGMass=$('#isEmailTemplateChanged_mass').val(); } catch (e) {}
	if(isEmailTemplateChanged_CGMass==1)
	{
		try { msgSubject_CGMass=$('#subjectLine_mass').val(); } catch (e) {}
		try { adminmailBody_CGMass=$('#mailBody_mass').find(".jqte_editor").html(); } catch (e) {}
	}

	var cnt_mass=0;
	var focs_mass=0;	
	
	var bFinalize=false;
	var isFinalizeCGMass=document.getElementById("isFinalizeCGMass").value;
	if(isFinalizeCGMass=="1")
		bFinalize=true;
	
	var bTopSliderEnable=false;
	if(topSliderDisable_CGMass=="1")
		bTopSliderEnable=true;
	
	var bTopSlider_NotSet=false;
	if(bTopSliderEnable && scoreProvided_CGMass == 0)
		bTopSlider_NotSet=true;
	
	var bTopSlider_SetValue=false;
	if(bTopSliderEnable && scoreProvided_CGMass > 0)
		bTopSlider_SetValue=true;
	
	var bSlider=false;
	if(countQuestionSlider_CGMass >0)
		bSlider=true;
	
	var bAllSetSlider=false;
	if(bSlider && notSetScore_NoOfQuestions==0)
		bAllSetSlider=true;
	
	var bPartialSetSlider=false;
	if(bSlider && sumAnswerScore > 0)
		bPartialSetSlider=true;
	
	var bNoSetSlider=false;
	if(bSlider && sumAnswerScore == 0)
		bNoSetSlider=true;
	
	if((bTopSlider_NotSet || bNoSetSlider) && !bNoteText )
	{
		$('#errorStatusNote_mass').show();
		if(bTopSlider_NotSet)
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNote+".<br>");
		if(bNoSetSlider)
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgSetScoreOrEnterNoteForAttribute+".<br>");
		
		cnt_mass=1;
	}
	else if(statusFlag && !bTopSlider_NotSet && !bTopSlider_SetValue && !bSlider && !bNoteText)
	{
		$('#errorStatusNote_mass').show();
		$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		cnt_mass=1;
	}
	/*else if(statusFlag && !bTopSlider_NotSet && bTopSlider_SetValue && !bSlider && !bNoteText)
	{
		$('#errorStatusNote_mass').show();
		$('#errorStatusNote_mass').append("&#149; Please enter Note.<br>");
		cnt_mass=1;
	}*/
	else if(bFinalize && bSlider && !bAllSetSlider && bPartialSetSlider)
	{
		$('#errorStatusNote_mass').show();
		$('#errorStatusNote_mass').append("&#149; "+resourceJSON.SetScoreForAllAttributeToFinalize+".<br>");
		cnt_mass=1;
	}
	
	if(bNoteText)
	{
		var charCount = statusNotes_mass.length;
		if(charCount > 4000)
		{
			$('#errorStatusNote_mass').show();
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgLengthnotExceed+".<br>");
			cnt_mass=1;
		}
		else if(bReadyToNoteAttach && !bNoteAttach)
		{
			$('#errorStatusNote_mass').show();
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
			cnt_mass++;
		}
		else if(bReadyToNoteAttach && bNoteAttach)
		{
			var ext = statusNoteFileName_mass.substr(statusNoteFileName_mass.lastIndexOf('.') + 1).toLowerCase();
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("statusNoteFileName_mass").files[0]!=undefined)
				{
					fileSize = document.getElementById("statusNoteFileName_mass").files[0].size;
				}
			}
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
			{
				$('#errorStatusNote_mass').show();
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzSlctAcceptableNoteFormats+".<br>");
				cnt_mass++;
			}
			else if(fileSize>=10485760)
			{
				$('#errorStatusNote_mass').show();
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.msgfilesizelessthan+".<br>");
				cnt_mass++;
			}
		}
	}
	
	var dispalyStatusName_mass=document.getElementById("dispalyStatusName_mass").value;
	var bOfferReady=false;
	var bHired=false;
	if(dispalyStatusName_mass=="Offer Ready")
		bOfferReady=true;
	if(dispalyStatusName_mass=="Hired")
		bHired=true;
	
	var requisitionNumberText_mass="";
	var requisitionNumberID_mass=null;
	var bRequisitionNumberDivShow=false;
	var bRNunber=false;
	try{
		if(document.getElementById("requisitionNumbersGrid_mass").style.display!='none')
		{
			bRequisitionNumberDivShow=true;
			try{  
				requisitionNumberID_mass=document.getElementById("requisitionNumber").value;
				
				var x=document.getElementById("requisitionNumber").selectedIndex;
				var y=document.getElementById("requisitionNumber").options;
				
				requisitionNumberText_mass=y[x].text;
				
				if(requisitionNumberID_mass > 0)
					bRNunber=true;
			}catch(e){}
		}
	}catch(err){}
	
	var isReqNoForHiring=0;
	try{
		isReqNoForHiring=document.getElementById("isReqNoForHiring").value;
	}catch(err){}
	if(isReqNoForHiring==0){
		try{
			document.getElementById("reqmassstar").style.display="none";
		}catch(err){}
	}else{
		try{
			document.getElementById("reqmassstar").style.display="inline";
		}catch(err){}
	}
	var reqLength=0;
	try{
		reqLength = document.getElementById("requisitionNumber").length;
	}catch(err){}
	
	try{
		if(bOfferReady && requisitionNumberID_mass==null){
			$('#errorStatusNote_mass').show();
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgNoPositionNumberFound+".<br>");
			cnt=1;
		}
	}catch(e){}
	var txtschoolCount_mass=0;
	var schoolId=0;
	if(bFinalize)
	{
		try{
			if((bOfferReady || bHired) && isReqNoForHiring==1 ){
				if(bRequisitionNumberDivShow && !bRNunber){
					if(reqLength>1){
						$('#errorStatusNote_mass').show();
						$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzSlctPosition+".<br>");
					}else if(bOfferReady){
						$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgNoPositionFound+".<br>");
					}else{
						$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgPositionIsMandatory+".<br>");
					}
					cnt_mass++;
				}else if(bRequisitionNumberDivShow==false){
					cnt_mass++;
					$('#errorStatusNote_mass').append("&#149; "+resourceJSON.MsgPositionIsMandatory+".<br>");
				}
			}
		}catch(err){}
		try{
			txtschoolCount_mass=document.getElementById("txtschoolCount_mass").value;
		}catch(err){}
		
		var bSchoolDivCheck_mass=false;
		try{
			if(document.getElementById("schoolAutoSuggestDivId_mass").style.display!="none")
				bSchoolDivCheck_mass=true;
		}catch(err){}
		
		if(txtschoolCount_mass > 1 && bSchoolDivCheck_mass)
		{
			schoolId=document.getElementById("schoolId").value;
			if((schoolId=='' || schoolId == 0) && bHired)
			{
				$('#errorStatusNote_mass').show();
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
				$('#schoolName').focus();
				cnt_mass++;
			}
		}
	}
	//alert('schoolId=============='+$('#schoolId_mass').val()+"actionFlag==="+actionFlag+" $(entity.val()==="+$('[name="entityType"]').val()+"  $('#isSchoolOnJobId').val()==="+$('#isSchoolOnJobId').val()+" $('#isRequiredSchoolIdCheck').val()==="+$('#isRequiredSchoolIdCheck').val());
	if($('#isSchoolOnJobId').val()==1 && $('#isRequiredSchoolIdCheck').val()==1 && actionFlag==1 && $('[name="entityType"]').val() == 2){
		schoolId=document.getElementById("schoolId_mass").value;
		//alert("schoolId========"+schoolId);
		if(schoolId=='' || schoolId == 0 )
		{
			$('#errorStatusNote_mass').show();
			$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrSchool+".<br>");
			$('#schoolName_mass1').focus();
			cnt_mass++;
		}
	}
	
	/*SWADESH*/
	var timingforDclWrdw='';
	var reasonforDclWrdw='';
	if(actionFlag==1){
	if(dispalyStatusName_mass  == "Withdrew" || dispalyStatusName_mass  == "Declined"){
		timingforDclWrdw = document.getElementById("timingforDclWrdw_mass").value;
		reasonforDclWrdw = document.getElementById("reasonforDclWrdw_mass").value;
		if(timingforDclWrdw == "0" || timingforDclWrdw == '' || reasonforDclWrdw == "0" || reasonforDclWrdw == ''){
			$('#errorStatusNote').show();
			if(timingforDclWrdw == "0" || timingforDclWrdw == '')
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzSelectHireddate+" 1.<br>");
			if(reasonforDclWrdw == "0" || reasonforDclWrdw == '')
				$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzSelectHireddate+" 2.<br>");
			return false;
		}
	}
	}
	
	//Only for Jeffco Start Date
	var startDate="";
	try{
	if($("[name='slcStartDateOnOrOff']").val()=="ON"){
	startDate=$('[name="startDate_mass"]').val();
	if(actionFlag==1 && $("#districtId").val()=='804800' && dispalyStatusName_mass=='Offer Made' && startDate==''){
		$('#errorStatusNote_mass').show();
		$('#errorStatusNote_mass').append("&#149; "+resourceJSON.PlzEtrSTDateMSG+".<br>");
		$('[name="startDate_mass"]').focus();
		cnt_mass++;
	}
	}
	}catch(e){}
	//Ended only for jeffco Start Date
	
	//*************************************for OSCEOLA(1201470) *********************************************************//
	var flagforNextStep=true;
	var districtIdOSCEOLA=1201470;
	//alert("dispalyStatusName_mass======================"+dispalyStatusName_mass);
	if(actionFlag==1 && cnt_mass==0 && $("#districtId").val()==districtIdOSCEOLA && dispalyStatusName_mass=='Offer Made' && minOneScoreSliderSetFlag==2 ){
		var resData="";
		MassStatusUpdateCGAjax.getOnlyCredentialReview(teacherIds_mass,jobId,"Credential Review",districtIdOSCEOLA,{
			async: false,
			errorHandler:handleError,
			callback:function(data){
			cnt_mass++;
			var confirmMessage="We found that the \"Credential Review\" node is not set for one or more candidate. Click on \"Ok\" button if you want to send Conditional Offer to that candidate who have \"Credential Review\" node finalized, otherwise click on \"Cancel\" button to wait for the change of \"Credential Review\" node of candidates.";
			if(data==""){
				confirmMessage="\"Credential Review\" node is not set in selected candidate list.";
			}
			getConfirm($('#myModalStatusInfo_mass .modal-dialog #myModalLabel #status_title_mass').html(),confirmMessage,data,'Ok',function(data){
			if(data){
				validateStatusInfo_cgmass(actionFlag);	
			}
			});	
			}
		});
	}
	if(minOneScoreSliderSetFlag == 1 && teacherIds_mass==""){
		minOneScoreSliderSetFlag=2;
		flagforNextStep=false;
		$('#myModalStatusInfo_mass').modal('hide');
		$('#myModalStatus_mass').modal('show');
	}
	
	//*************************************ended OSCEOLA(1201470) *********************************************************//
	
	if(cnt_mass==0 && flagforNextStep)
	{
		//showConfirmationDivForCG_mass();
		$('#myModalStatusInfo_mass').modal('hide');
		try {
			$('#loadingDiv').show();
		} catch (e) {}
		if(statusNoteFileName_mass!="" && statusNoteFileName_mass!=null && actionFlagTemp!="99")
		{
			//alert("try to submit");
			document.getElementById("frmStatusNote_mass").submit();
			
		}
		else
		{
			//alert("try to save statusNoteFileName_mass "+statusNoteFileName_mass);
			minOneScoreSliderSetFlag=2;
			statusNoteFileName_mass=document.getElementById("statusnoteFileNameCGMassTemp").value;
			MassStatusUpdateCGAjax.massPersistentData(userActionCGMass,bFinalize,teacherIds_mass,jobId,statusId,secondaryStatusId,bTopSliderEnable,scoreProvided_CGMass,bSlider,answerId_arrary,answerScore_arrary,masterQuestionIds_arrary,sQuestionIds_Note_arrary,statusNotes_mass,statusNoteFileName_mass,requisitionNumberID_mass,requisitionNumberText_mass,schoolId,bSentEmailToDASA,isEmailTemplateChanged_CGMass,msgSubject_CGMass,adminmailBody_CGMass,schoolId,startDate,timingforDclWrdw,reasonforDclWrdw,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				if(data!=null)
				{
					if(data[1]==1)
					{
						try {
							$('#loadingDiv').hide();
						} catch (e) {}
						
						//$('#myModalStatusInfo_mass').modal('hide');
						document.getElementById("mass_cgInfo_Override").style.display="inline";
						document.getElementById("mass_cgInfo_Skip").style.display="inline";
						$('#errordivConfirmCGMass').empty();
						document.getElementById("errordivConfirmCGMass").style.display="inline";
						$('#errordivConfirmCGMass').append(data[0]);
						$('#myModalConfirmInfoCGMass').modal('show');
					}
					else if(data[1]==0)
					{
						try {
							$('#loadingDiv').hide();
						} catch (e) {}
						try{
							var teachers = "", tempTeacher ="";
							if(typeof(teacherIds_mass) == 'string')
								tempTeacher=teacherIds_mass.split(",");
							else if(typeof(teacherIds_mass) == 'array'|| typeof(teacherIds_mass) == 'object')
								tempTeacher = teacherIds_mass;
							for(var i=0; i<tempTeacher.length ; i++){
								if(teachers == "")
									teachers = tempTeacher[i];
								else
									teachers+="##"+tempTeacher[i];
							}
							candidateNotReviewedUtil.setNotReviewedFlag(teachers,"ApplyGlobalTag3");
						}catch(err) {	
							console.log("Exception in MassStatusUpdateCGAjax.massPersistentData:1124  "+err.message);
						}
						
						//$('#myModalStatusInfo_mass').modal('hide'); 
						$('#myModalMsgShowSuccessCGMass').modal('show');
						if(data[2]=="1"){
							$('#message2showConfirm').html("Would you like to invite applicants to an event? ");
							$('#footerbtn').html("<button class='btn btn-primary' onclick=\"redirectToEventPage("+jobId+",'"+teacherIds_mass+"')\" >Yes</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>No</button>");
							$('#myModal3').modal('show');	
						}				
					}
				}
			}
			});
		
		}
		
	}

}


function statusDetailsClose_mass()
{
	try{ $('#myModalConfirmInfoCGMass').modal('hide'); } catch(err){}
	try{ $('#myModalStatusInfo_mass').modal('show');}catch(err){}
}

function showConfirmationDivForCG_mass()
{
	try{
		$('#myModalStatusInfo_mass').modal('hide'); 
		$('#myModalConfirmInfoCGMass').modal('show');	
	}
	catch(err){}
	
	$('#mass_cgInfo_Override').modal('show');
	$('#mass_cgInfo_Skip').modal('show');
	
}

function successClose_mass()
{
	try{
		$('#myModalMsgShowSuccessCGMass').modal('hide');
		$('#myModalStatus_mass').modal('show');
	}
	catch(err){}
	document.getElementById("refreshGridCGMass").value="1";
}

function userConfirmationCGMss(userInput)
{
	try{
		$('#myModalConfirmInfoCGMass').modal('hide');
	}
	catch(err){}
	
	document.getElementById("userActionCGMass").value=userInput;
	var isFinalizeCGMass=document.getElementById("isFinalizeCGMass").value;
	validateStatusInfo_cgmass(isFinalizeCGMass);
}

function setButton(){
	var count=0;
	 $('#jWTeacherStatusNotesDiv').is(':visible') 
	 {
		 $('.jqte').each(function(){
			  $(this).is(':visible') 
			  count++;
			  if($(this).find("textarea").attr("name")!='statusNotes' && $(this).find("textarea").attr("name")!=''){
				  $(this).find(".jqte_toolbar").append('<a href="#" class="pull-right plus-icons" id="newIcon'+count+'" onClick="shapeChange(this,'+count+');"><i class="fa fa-plus-square"></i></a>');
				  $(this).find(".jqte_editor").css("min-height", "30px");
				  $(this).find(".jqte_editor").css("height", "30px");
			  }
		  });
		 	$('[name="statusNotes"]').parents().parents('.jqte').find(".jqte_editor").css("min-height", "30px");
			$('[name="statusNotes"]').parents().parents('.jqte').find(".jqte_editor").css("height", "30px");
			$('[name=""]').parents().parents('.jqte').find(".jqte_editor").css("min-height", "30px");
			$('[name=""]').parents().parents('.jqte').find(".jqte_editor").css("height", "30px");
	}
}
$(document).ready(function(){
	//$('[name="statusNotes"]').parents().parents('.jqte').find(".jqte_toolbar").append('<a href="#" class="pull-right plus-icons" id="newIconAddNotesLast" onClick="shapeChange(this,\'AddNotesLast\');"><i class="fa fa-plus-square"></i></a>');
});

function shapeChange(obj,count){
	var id=count;
	var p=$('#newIcon'+id).parents().parents('.jqte');
	if(p.find('.jqte_editor').height()==100)
	{
		p.find(".jqte_editor").css("min-height", "30px");
		p.find(".jqte_editor").css("height", "30px");
	}
	else if(p.find('.jqte_editor').height()==10)
	{
		p.find('.jqte_editor').css("mix-height", "900px");
		p.find('.jqte_editor').css("height", "120px");
	}
}

