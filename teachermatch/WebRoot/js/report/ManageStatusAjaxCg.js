if (window['dojo']) dojo.provide('dwr.interface.ManageStatusAjax');
if (typeof this['ManageStatusAjax'] == 'undefined') ManageStatusAjax = {};
ManageStatusAjax._path = '/dwr';
ManageStatusAjax.saveNotesTrans = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'saveNotesTrans', arguments);
};
ManageStatusAjax.requisitionNumbers = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'requisitionNumbers', arguments);
};
ManageStatusAjax.copyPasteFolder = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'copyPasteFolder', arguments);
};
ManageStatusAjax.cutPasteFolder = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'cutPasteFolder', arguments);
};
ManageStatusAjax.displayTreeStructure = function(callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayTreeStructure', arguments);
};
ManageStatusAjax.statusCheck = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'statusCheck', arguments);
};
ManageStatusAjax.createStatusFolder = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'createStatusFolder', arguments);
};
ManageStatusAjax.renameStatusFolder = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'renameStatusFolder', arguments);
};
ManageStatusAjax.displayStatusDashboard = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayStatusDashboard', arguments);
};
ManageStatusAjax.deleteStatusFolder = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'deleteStatusFolder', arguments);
};
ManageStatusAjax.checkFolderIsDeleted = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'checkFolderIsDeleted', arguments);
};
ManageStatusAjax.isOverrideFinalizeStatus = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'isOverrideFinalizeStatus', arguments);
};
ManageStatusAjax.statusWiseSectionList = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'statusWiseSectionList', arguments);
};
ManageStatusAjax.getStatusWiseEmailForAdmin = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getStatusWiseEmailForAdmin', arguments);
};
ManageStatusAjax.getSectionWiseTemaplteslist = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getSectionWiseTemaplteslist', arguments);
};
ManageStatusAjax.getTemplateByLIst = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getTemplateByLIst', arguments);
};
ManageStatusAjax.getStatusWiseEmailForTeacher = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getStatusWiseEmailForTeacher', arguments);
};
ManageStatusAjax.saveStatusNote = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37, p38, p39, p40, p41, p42, p43, p44, p45, p46, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'saveStatusNote', arguments);
};
ManageStatusAjax.sendReferenceMailOnFinalize = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'sendReferenceMailOnFinalize', arguments);
};
ManageStatusAjax.getStatusNote = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getStatusNote', arguments);
};
ManageStatusAjax.editShowStatusNote = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'editShowStatusNote', arguments);
};
ManageStatusAjax.deleteStatusNote = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'deleteStatusNote', arguments);
};
ManageStatusAjax.displayJobCategoryByDistrict = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayJobCategoryByDistrict', arguments);
};
ManageStatusAjax.displaySecondaryStatusByDistrictOrJobCategory = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displaySecondaryStatusByDistrictOrJobCategory', arguments);
};
ManageStatusAjax.saveOrUpdateQuestion = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'saveOrUpdateQuestion', arguments);
};
ManageStatusAjax.getQuestionListByDistrictAndJobCategory = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getQuestionListByDistrictAndJobCategory', arguments);
};
ManageStatusAjax.editQuestion = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'editQuestion', arguments);
};
ManageStatusAjax.activateDeactivateQuestion = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'activateDeactivateQuestion', arguments);
};
ManageStatusAjax.copyDataFromDistrictToJobCategory = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'copyDataFromDistrictToJobCategory', arguments);
};
ManageStatusAjax.updateStatusSpecificScore = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'updateStatusSpecificScore', arguments);
};
ManageStatusAjax.getJobCategoryWiseStatusPrivilege = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getJobCategoryWiseStatusPrivilege', arguments);
};
ManageStatusAjax.isQuestionAdd = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'isQuestionAdd', arguments);
};
ManageStatusAjax.downloadAttachInstructionFile = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'downloadAttachInstructionFile', arguments);
};
ManageStatusAjax.removeInstructionFile = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'removeInstructionFile', arguments);
};
ManageStatusAjax.getSchoolInJobOrder = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getSchoolInJobOrder', arguments);
};
ManageStatusAjax.getSchoolInJobOrderForHired = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getSchoolInJobOrderForHired', arguments);
};
ManageStatusAjax.getStatusForPanel = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getStatusForPanel', arguments);
};
ManageStatusAjax.sendReferenceMail = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'sendReferenceMail', arguments);
};
ManageStatusAjax.sendReferenceMailPool = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'sendReferenceMailPool', arguments);
};
ManageStatusAjax.getSectionWiseTemaplteslistTPGolu = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getSectionWiseTemaplteslistTPGolu', arguments);
};
ManageStatusAjax.checkOCPCStatus = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'checkOCPCStatus', arguments);
};
ManageStatusAjax.getReferenceCheck = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getReferenceCheck', arguments);
};
ManageStatusAjax.showReferenceDetail = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'showReferenceDetail', arguments);
};
ManageStatusAjax.saveMaxScore = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'saveMaxScore', arguments);
};
ManageStatusAjax.saveMaxScoreForPool = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'saveMaxScoreForPool', arguments);
};
ManageStatusAjax.sendOnlineActivityLink = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'sendOnlineActivityLink', arguments);
};
ManageStatusAjax.displayAllAndSelecedDist = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayAllAndSelecedDist', arguments);
};
ManageStatusAjax.printReferenceData = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'printReferenceData', arguments);
};
ManageStatusAjax.setAndGetTeacherStatusSlider = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'setAndGetTeacherStatusSlider', arguments);
};
ManageStatusAjax.editAndGetTeacherStatusNote = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'editAndGetTeacherStatusNote', arguments);
};
ManageStatusAjax.addTeacherStatusNote = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'addTeacherStatusNote', arguments);
};
ManageStatusAjax.sendPortfolioReminderMail = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'sendPortfolioReminderMail', arguments);
};
ManageStatusAjax.sendPortfolioReminderBulkMail = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'sendPortfolioReminderBulkMail', arguments);
};
ManageStatusAjax.getJobSubCateForSeach = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getJobSubCateForSeach', arguments);
};
ManageStatusAjax.sendFinelizeSpecificMailToTeacher = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'sendFinelizeSpecificMailToTeacher', arguments);
};
ManageStatusAjax.getFinalizeSpecificTemplateList = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getFinalizeSpecificTemplateList', arguments);
};
ManageStatusAjax.displayAllAndSelecedHQ = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayAllAndSelecedHQ', arguments);
};
ManageStatusAjax.displayAllActiveHQAdmins = function(callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayAllActiveHQAdmins', arguments);
};
ManageStatusAjax.displayAllActiveBranchAdmins = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayAllActiveBranchAdmins', arguments);
};
ManageStatusAjax.saveHQAttributes = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'saveHQAttributes', arguments);
};
ManageStatusAjax.changeKSNNumberOfTeacher = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'changeKSNNumberOfTeacher', arguments);
};
ManageStatusAjax.inactiveKSNNumberOfTalentKSMDetail = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'inactiveKSNNumberOfTalentKSMDetail', arguments);
};
ManageStatusAjax.displayStatusDashboardCGOpenSlc = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayStatusDashboardCGOpenSlc', arguments);
};
ManageStatusAjax.disconnectQueue = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'disconnectQueue', arguments);
};
ManageStatusAjax.callAPIMethod = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'callAPIMethod', arguments);
};
ManageStatusAjax.getTalentKSNDetail = function(p0, p1, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getTalentKSNDetail', arguments);
};
ManageStatusAjax.displayStatusDashboardCG = function(p0, p1, p2, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayStatusDashboardCG', arguments);
};
ManageStatusAjax.displaySecondaryStatusByDistrictOrJobCategoryOp = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displaySecondaryStatusByDistrictOrJobCategoryOp', arguments);
};
ManageStatusAjax.displayStatusDashboardOp = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'displayStatusDashboardOp', arguments);
};
ManageStatusAjax.copyDataFromDistrictToJobCategoryOp = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'copyDataFromDistrictToJobCategoryOp', arguments);
};
ManageStatusAjax.saveStatusNoteOp = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37, p38, p39, p40, p41, p42, p43, p44, p45, p46, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'saveStatusNoteOp', arguments);
};
ManageStatusAjax.getStatusNote_op = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getStatusNote_op', arguments);
};
ManageStatusAjax.getLinktoKSNResponse = function(p0, callback) {
return dwr.engine._execute(ManageStatusAjax._path, 'ManageStatusAjax', 'getLinktoKSNResponse', arguments);
};
