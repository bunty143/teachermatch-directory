var url =window.location.href;
var path ="/dwr";
if(url.indexOf("/teachermatch/") > -1)
	path="/teachermatch/dwr";

//--------------- CandidateGridSubAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.CandidateGridSubAjax');
if (typeof this['CandidateGridSubAjax'] == 'undefined') CandidateGridSubAjax = {};
CandidateGridSubAjax._path = path;
CandidateGridSubAjax.getDistrictSpecificPortfolioQuestions = function(p0, p1, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'getDistrictSpecificPortfolioQuestions', arguments);
};
CandidateGridSubAjax.cancelPanelEvent = function(p0, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'cancelPanelEvent', arguments);
};
CandidateGridSubAjax.displayAttendee = function(p0, p1, p2, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'displayAttendee', arguments);
};
CandidateGridSubAjax.saveDemoSchedule = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'saveDemoSchedule', arguments);
};
CandidateGridSubAjax.getUsersBySchool = function(p0, p1, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'getUsersBySchool', arguments);
};
CandidateGridSubAjax.getUsersByDistrict = function(p0, p1, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'getUsersByDistrict', arguments);
};
CandidateGridSubAjax.cancelEvent = function(p0, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'cancelEvent', arguments);
};
CandidateGridSubAjax.displayPannel = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'displayPannel', arguments);
};
CandidateGridSubAjax.savePanel = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'savePanel', arguments);
};
CandidateGridSubAjax.showPanels = function(p0, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'showPanels', arguments);
};
CandidateGridSubAjax.viewPanelMembers = function(p0, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'viewPanelMembers', arguments);
};
CandidateGridSubAjax.addNewPanelMember = function(p0, p1, p2, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'addNewPanelMember', arguments);
};
CandidateGridSubAjax.getPanelByJobWiseStatus = function(p0, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'getPanelByJobWiseStatus', arguments);
};
CandidateGridSubAjax.addAllPanelMebersFromPanel = function(p0, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'addAllPanelMebersFromPanel', arguments);
};
CandidateGridSubAjax.setDistrictQPortfoliouestions = function(p0, p1, p2, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'setDistrictQPortfoliouestions', arguments);
};
CandidateGridSubAjax.saveCommonPanel = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'saveCommonPanel', arguments);
};
CandidateGridSubAjax.sendAgainInviteToCandidates = function(p0, p1, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'sendAgainInviteToCandidates', arguments);
};
CandidateGridSubAjax.reSendTalentSignUp = function(p0, p1, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'reSendTalentSignUp', arguments);
};
CandidateGridSubAjax.getSchoolListByJob = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'getSchoolListByJob', arguments);
};
CandidateGridSubAjax.updateAppliedJobsonCG = function(p0, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'updateAppliedJobsonCG', arguments);
};
CandidateGridSubAjax.saveTempCGEventDetail = function(p0, p1, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'saveTempCGEventDetail', arguments);
};
CandidateGridSubAjax.callMessageQueueAPI = function(p0, p1, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'callMessageQueueAPI', arguments);
};
CandidateGridSubAjax.getJobInformationHeaderCG = function(p0, callback) {
return dwr.engine._execute(CandidateGridSubAjax._path, 'CandidateGridSubAjax', 'getJobInformationHeaderCG', arguments);
};

//--------------- CandidateReportAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.CandidateReportAjax');
if (typeof this['CandidateReportAjax'] == 'undefined') CandidateReportAjax = {};
CandidateReportAjax._path = path;

CandidateReportAjax.saveNotes = function(p0, p1, p2, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'saveNotes', arguments);
};
CandidateReportAjax.getCandidateReportGrid = function(p0, p1, p2, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getCandidateReportGrid', arguments);
};
CandidateReportAjax.getHiredCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getHiredCandidateReports', arguments);
};
CandidateReportAjax.getInCompleteCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getInCompleteCandidateReports', arguments);
};
CandidateReportAjax.getRemoveCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getRemoveCandidateReports', arguments);
};
CandidateReportAjax.getVltCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getVltCandidateReports', arguments);
};
CandidateReportAjax.getWithdrawnCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getWithdrawnCandidateReports', arguments);
};
CandidateReportAjax.showActDiv = function(p0, p1, p2, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'showActDiv', arguments);
};
CandidateReportAjax.saveMessage = function(p0, p1, p2, p3, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'saveMessage', arguments);
};
CandidateReportAjax.hireTeacher = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'hireTeacher', arguments);
};
CandidateReportAjax.getPhoneDetail = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getPhoneDetail', arguments);
};
CandidateReportAjax.getCertificationGrid = function(p0, p1, p2, p3, p4, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getCertificationGrid', arguments);
};
CandidateReportAjax.getTranscriptGrid = function(p0, p1, p2, p3, p4, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getTranscriptGrid', arguments);
};
CandidateReportAjax.downloadTranscript = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'downloadTranscript', arguments);
};
CandidateReportAjax.unHireTeacher = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'unHireTeacher', arguments);
};
CandidateReportAjax.removeTeacher = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'removeTeacher', arguments);
};
CandidateReportAjax.downloadResume = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'downloadResume', arguments);
};
CandidateReportAjax.downloadCandidateGridReport = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'downloadCandidateGridReport', arguments);
};
CandidateReportAjax.downloadPortfolioReport = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'downloadPortfolioReport', arguments);
};
CandidateReportAjax.getCoverLetter = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getCoverLetter', arguments);
};
CandidateReportAjax.getTranscriptText = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getTranscriptText', arguments);
};
CandidateReportAjax.getNotesDetail = function(p0, p1, p2, p3, p4, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getNotesDetail', arguments);
};
CandidateReportAjax.showEditNotes = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'showEditNotes', arguments);
};
CandidateReportAjax.saveOrUpdateTeacherSecStatus = function(p0, p1, p2, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'saveOrUpdateTeacherSecStatus', arguments);
};
CandidateReportAjax.generatePDReport = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'generatePDReport', arguments);
};
CandidateReportAjax.generatePDPReportNew = function(p1, p2, p3, p4, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'generatePDPReportNew', arguments);
};
CandidateReportAjax.generatePilarReport = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'generatePilarReport', arguments);
};
CandidateReportAjax.generateJSAReport = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'generateJSAReport', arguments);
};
CandidateReportAjax.checkBaseCompeleted = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'checkBaseCompeleted', arguments);
};
CandidateReportAjax.notifyTMAdmins = function(p0, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'notifyTMAdmins', arguments);
};
CandidateReportAjax.downloadGeneralKnowledge = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'downloadGeneralKnowledge', arguments);
};
CandidateReportAjax.downloadSubjectAreaExam = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'downloadSubjectAreaExam', arguments);
};
CandidateReportAjax.getDspqQuestions = function(p0, p1, p2, p3, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getDspqQuestions', arguments);
};
CandidateReportAjax.getHiddenCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getHiddenCandidateReports', arguments);
};
CandidateReportAjax.getOnBoardingCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getOnBoardingCandidateReports', arguments);
};
CandidateReportAjax.getInactiveCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getInactiveCandidateReports', arguments);
};
CandidateReportAjax.generatePDPReportForMultipleJobApplyDistrict = function(p0, p1, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'generatePDPReportForMultipleJobApplyDistrict', arguments);
};
CandidateReportAjax.getHiredForKellyCandidateReports = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, callback) {
   return dwr.engine._execute(CandidateReportAjax._path, 'CandidateReportAjax', 'getHiredForKellyCandidateReports', arguments);
};

//--------------- CGInviteInterviewAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.CGInviteInterviewAjax');
if (typeof this['CGInviteInterviewAjax'] == 'undefined') CGInviteInterviewAjax = {};
CGInviteInterviewAjax._path = path;
CGInviteInterviewAjax.displayInviteInterviewQues = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'displayInviteInterviewQues', arguments);
};
CGInviteInterviewAjax.saveIIStatus = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'saveIIStatus', arguments);
};

CGInviteInterviewAjax.openInterviewURL = function(p0, p1, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'openInterviewURL', arguments);
};

CGInviteInterviewAjax.saveInterviewValue = function(p0, p1, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'saveInterviewValue', arguments);
};

CGInviteInterviewAjax.saveIIStatusForEvent = function(p0, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'saveIIStatusForEvent', arguments);
};

CGInviteInterviewAjax.getPathOfVideoUrl = function(p0, callback) {
return dwr.engine._execute(CGInviteInterviewAjax._path, 'CGInviteInterviewAjax', 'getPathOfVideoUrl', arguments);
};


//--------------- PFExperiences --------------//
if (window['dojo']) dojo.provide('dwr.interface.PFExperiences');
if (typeof this['PFExperiences'] == 'undefined') PFExperiences = {};
PFExperiences._path = path;
PFExperiences.downloadResume = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'downloadResume', arguments);
};


PFExperiences.getSeniorityNumber = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getSeniorityNumber', arguments);
};
PFExperiences.addOrEditResume = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'addOrEditResume', arguments);
};
PFExperiences.addOrEditBackgroundCheck = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'addOrEditBackgroundCheck', arguments);
};

PFExperiences.deleteResume = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'deleteResume', arguments);
};

PFExperiences.downloadBackgroundCheck = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'downloadBackgroundCheck', arguments);
};
PFExperiences.getPFEmploymentGrid = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getPFEmploymentGrid', arguments);
};
PFExperiences.saveOrUpdateEmployment = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'saveOrUpdateEmployment', arguments);
};
PFExperiences.showEditFormEmployment = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'showEditFormEmployment', arguments);
};
PFExperiences.deleteEmployment = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'deleteEmployment', arguments);
};
PFExperiences.getInvolvementGrid = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getInvolvementGrid', arguments);
};

PFExperiences.getInvolvementGridDistrictSpecific = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getInvolvementGridDistrictSpecific', arguments);
};
PFExperiences.saveOrUpdateInvolvement = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'saveOrUpdateInvolvement', arguments);
};
PFExperiences.showEditFormInvolvement = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'showEditFormInvolvement', arguments);
};
PFExperiences.deleteRecordInvolvment = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'deleteRecordInvolvment', arguments);
};

PFExperiences.getHonorsGrid = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getHonorsGrid', arguments);
};
PFExperiences.saveOrUpdateHonors = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'saveOrUpdateHonors', arguments);
};
PFExperiences.showEditHonors = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'showEditHonors', arguments);
};
PFExperiences.deleteRecordHonors = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'deleteRecordHonors', arguments);
};
PFExperiences.insertOrUpdateTeacherTFA = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37, p38, p39, p40, p41, p42, p43, p44, p45, p46, p47, p48, p49, p50, p51, p52, p53, p54, p55, p56, p57, p58, p59, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'insertOrUpdateTeacherTFA', arguments);
};
PFExperiences.getAffidavitValue = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getAffidavitValue', arguments);
};
PFExperiences.getTFAValues = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getTFAValues', arguments);
};
PFExperiences.updatePortfolioStatus = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updatePortfolioStatus', arguments);
};
PFExperiences.getTeacherPersonalInfoValues = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getTeacherPersonalInfoValues', arguments);
};

PFExperiences.chkDupCandidate = function(p0, p1, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'chkDupCandidate', arguments);
};

PFExperiences.getDistrictSpecificTFAValues = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getDistrictSpecificTFAValues', arguments);
};

PFExperiences.setDistrictSpecificTFAValues = function(p0, p1, p2, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'setDistrictSpecificTFAValues', arguments);
};

PFExperiences.insertOrUpdateStdTchrExp = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'insertOrUpdateStdTchrExp', arguments);
};

PFExperiences.displayStdTchrExp = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'displayStdTchrExp', arguments);
};
PFExperiences.delStdTchrExp = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'delStdTchrExp', arguments);
};
PFExperiences.editFormStdTchrExp = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'editFormStdTchrExp', arguments);
};
PFExperiences.getTeacherVeteran = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getTeacherVeteran', arguments);
};
PFExperiences.displayTeacherLanguage = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'displayTeacherLanguage', arguments);
};
PFExperiences.insertOrUpdateTchrLang = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'insertOrUpdateTchrLang', arguments);
};
PFExperiences.delTeacherLanguage = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'delTeacherLanguage', arguments);
};
PFExperiences.showEditLang = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'showEditLang', arguments);
};

PFExperiences.updateLangeuage = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updateLangeuage', arguments);
};
PFExperiences.checkKnowLanguage = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'checkKnowLanguage', arguments);
};
PFExperiences.downloadAnswer = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'downloadAnswer', arguments);
};
PFExperiences.saveOctDetails = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'saveOctDetails', arguments);
};
PFExperiences.getOctDetails = function(callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getOctDetails', arguments);
};
PFExperiences.downloadOctUpload = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'downloadOctUpload', arguments);
};
PFExperiences.addSeniorityNumber = function(p0, p1, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'addSeniorityNumber', arguments);
};

PFExperiences.insertOrUpdateTeacherPersonaLinForRem = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, p36, p37, p38, p39, p40, p41, p42, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'insertOrUpdateTeacherPersonaLinForRem', arguments);
};
PFExperiences.updateTeacherEmpAndZip = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updateTeacherEmpAndZip', arguments);
};
PFExperiences.getResidencyGrid = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'getResidencyGrid', arguments);
};
PFExperiences.insertOrUpdateResidency = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'insertOrUpdateResidency', arguments);
};
PFExperiences.editResidency = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'editResidency', arguments);
};
PFExperiences.deleteResidency = function(p0, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'deleteResidency', arguments);
};

PFExperiences.updateTeacherInfo = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updateTeacherInfo', arguments);
};
PFExperiences.updateTeacherFormerEmpInfo = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updateTeacherFormerEmpInfo', arguments);
};
PFExperiences.updateTeacherCurrentEmployee = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updateTeacherCurrentEmployee', arguments);
};
PFExperiences.updateTeacherFormalEmployee = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updateTeacherFormalEmployee', arguments);
};
PFExperiences.updateTeacherRetiredEmployee = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updateTeacherRetiredEmployee', arguments);
};

PFExperiences.updateTeacherCurrentSubstituteEmployee = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(PFExperiences._path, 'PFExperiences', 'updateTeacherCurrentSubstituteEmployee', arguments);
};


//--------------- PrintCertificateAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.PrintCertificateAjax');
if (typeof this['PrintCertificateAjax'] == 'undefined') PrintCertificateAjax = {};
PrintCertificateAjax._path = path;
PrintCertificateAjax.generateSkillPDF = function(p0, p1, callback) {
return dwr.engine._execute(PrintCertificateAjax._path, 'PrintCertificateAjax', 'generateSkillPDF', arguments);
};


//--------------- ReferenceCheckAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.ReferenceCheckAjax');
if (typeof this['ReferenceCheckAjax'] == 'undefined') ReferenceCheckAjax = {};
ReferenceCheckAjax._path = path;

ReferenceCheckAjax.getDistrictSpecificQuestion = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(ReferenceCheckAjax._path, 'ReferenceCheckAjax', 'getDistrictSpecificQuestion', arguments);
};
ReferenceCheckAjax.setDistrictQuestions = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(ReferenceCheckAjax._path, 'ReferenceCheckAjax', 'setDistrictQuestions', arguments);
};


//--------------- CGServiceAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.CGServiceAjax');
if (typeof this['CGServiceAjax'] == 'undefined') CGServiceAjax = {};
CGServiceAjax._path = path;
CGServiceAjax.getJobDescription = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobDescription', arguments);
};
CGServiceAjax.getEmailAddress = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getEmailAddress', arguments);
};
CGServiceAjax.getFieldOfSchoolList = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFieldOfSchoolList', arguments);
};
CGServiceAjax.getFieldOfSchoolList = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFieldOfSchoolList', arguments);
};
CGServiceAjax.getFieldOfSchoolList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFieldOfSchoolList', arguments);
};
CGServiceAjax.saveGraduationDate = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveGraduationDate', arguments);
};
CGServiceAjax.canleGraduationDate = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'canleGraduationDate', arguments);
};
CGServiceAjax.getFieldOfDistrictList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFieldOfDistrictList', arguments);
};
CGServiceAjax.saveMessage = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveMessage', arguments);
};
CGServiceAjax.getPhoneDetail = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getPhoneDetail', arguments);
};
CGServiceAjax.getCertificationGrid = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getCertificationGrid', arguments);
};
CGServiceAjax.getTranscriptGrid = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getTranscriptGrid', arguments);
};
CGServiceAjax.downloadTranscript = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadTranscript', arguments);
};
CGServiceAjax.downloadResume = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadResume', arguments);
};
CGServiceAjax.downloadPortfolioReport = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadPortfolioReport', arguments);
};
CGServiceAjax.getCoverLetter = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getCoverLetter', arguments);
};
CGServiceAjax.generatePDReport = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'generatePDReport', arguments);
};
CGServiceAjax.checkBaseCompeleted = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'checkBaseCompeleted', arguments);
};
CGServiceAjax.downloadGeneralKnowledge = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadGeneralKnowledge', arguments);
};
CGServiceAjax.downloadSubjectAreaExam = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadSubjectAreaExam', arguments);
};
CGServiceAjax.getIncompleteInfoHtml = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getIncompleteInfoHtml', arguments);
};
CGServiceAjax.checkSpCompleted = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'checkSpCompleted', arguments);
};
CGServiceAjax.checkAssessmentCompleted = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'checkAssessmentCompleted', arguments);
};
CGServiceAjax.hireVueIntegration = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'hireVueIntegration', arguments);
};
CGServiceAjax.addACandidateToAPosition = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'addACandidateToAPosition', arguments);
};
CGServiceAjax.generatePDPReportForMultipleJobApplyDistrict = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'generatePDPReportForMultipleJobApplyDistrict', arguments);
};
CGServiceAjax.setDistrictQPortfoliouestions = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'setDistrictQPortfoliouestions', arguments);
};
CGServiceAjax.downloadReferenceForCandidate = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadReferenceForCandidate', arguments);
};
CGServiceAjax.changeStatusElectronicReferences = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'changeStatusElectronicReferences', arguments);
};
CGServiceAjax.editElectronicReferences = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'editElectronicReferences', arguments);
};
CGServiceAjax.deleteVIdeoLink = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'deleteVIdeoLink', arguments);
};
CGServiceAjax.editVideoLink = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'editVideoLink', arguments);
};
CGServiceAjax.downloadUploadedDocumentForCg = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'downloadUploadedDocumentForCg', arguments);
};
CGServiceAjax.saveUpdateElectronicReferences = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveUpdateElectronicReferences', arguments);
};
CGServiceAjax.findDuplicateReferences = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'findDuplicateReferences', arguments);
};
CGServiceAjax.saveOrUpdateVideoLink = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveOrUpdateVideoLink', arguments);
};
CGServiceAjax.removeReferencesForNoble = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'removeReferencesForNoble', arguments);
};
CGServiceAjax.getVideoPlayDivForProfile = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getVideoPlayDivForProfile', arguments);
};
CGServiceAjax.getDegreeMasterList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getDegreeMasterList', arguments);
};
CGServiceAjax.getRegionMasterList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getRegionMasterList', arguments);
};
CGServiceAjax.getUniversityMasterList = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getUniversityMasterList', arguments);
};
CGServiceAjax.getFilterCertificateTypeList = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getFilterCertificateTypeList', arguments);
};
CGServiceAjax.getSubjectByDistrict = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getSubjectByDistrict', arguments);
};
CGServiceAjax.getJobCategoryByDistrict = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobCategoryByDistrict', arguments);
};
CGServiceAjax.getJobCategoryByHQAndBranch = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobCategoryByHQAndBranch', arguments);
};
CGServiceAjax.getJobSubCategoryByCategory = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobSubCategoryByCategory', arguments);
};
CGServiceAjax.getJobListByTeacher = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobListByTeacher', arguments);
};
CGServiceAjax.getJobListByTeacherByProfile = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getJobListByTeacherByProfile', arguments);
};
CGServiceAjax.saveTags = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveTags', arguments);
};
CGServiceAjax.updateTFAByUser = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'updateTFAByUser', arguments);
};
CGServiceAjax.displayselectedTFA = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayselectedTFA', arguments);
};
CGServiceAjax.getOnlineActivityData = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getOnlineActivityData', arguments);
};
CGServiceAjax.changeExpiryActivityStatus = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'changeExpiryActivityStatus', arguments);
};
CGServiceAjax.getZoneListAll = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getZoneListAll', arguments);
};
CGServiceAjax.displayGeoZoneSchoolGrid = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayGeoZoneSchoolGrid', arguments);
};
CGServiceAjax.getSelectedSchoolsList = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getSelectedSchoolsList', arguments);
};
CGServiceAjax.showLetterOfIntentForCG = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'showLetterOfIntentForCG', arguments);
};
CGServiceAjax.getOnlineActivityQuestionsForCG = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getOnlineActivityQuestionsForCG', arguments);
};
CGServiceAjax.saveOnlineActivity = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveOnlineActivity', arguments);
};
CGServiceAjax.displayOnlineActivityForCG = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayOnlineActivityForCG', arguments);
};
CGServiceAjax.displayMultipleOnlineActivityForCG = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayMultipleOnlineActivityForCG', arguments);
};
CGServiceAjax.displayOnlineActivityNotesForCG = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayOnlineActivityNotesForCG', arguments);
};
CGServiceAjax.makeEPIIncomplete = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'makeEPIIncomplete', arguments);
};
CGServiceAjax.makeSpIncomplete = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'makeSpIncomplete', arguments);
};
CGServiceAjax.makeAssessmentIncomplete = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'makeAssessmentIncomplete', arguments);
};
CGServiceAjax.showAssessmentDetails = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'showAssessmentDetails', arguments);
};
CGServiceAjax.getTeacherIds = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getTeacherIds', arguments);
};
CGServiceAjax.getSlider = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getSlider', arguments);
};
CGServiceAjax.getiFrameForTree = function(callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getiFrameForTree', arguments);
};
CGServiceAjax.getBreakUpData = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getBreakUpData', arguments);
};
CGServiceAjax.getDistrictQPortfoliouestions = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getDistrictQPortfoliouestions', arguments);
};
CGServiceAjax.getDistrictAssessmentDetails = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getDistrictAssessmentDetails', arguments);
};
CGServiceAjax.saveAssessmentInvite = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'saveAssessmentInvite', arguments);
};
CGServiceAjax.getAssessmentInviteQuestionAnswers = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getAssessmentInviteQuestionAnswers', arguments);
};
CGServiceAjax.getAssessmentDetails = function(p0, p1, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getAssessmentDetails', arguments);
};
CGServiceAjax.sendAssessmentInvite = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'sendAssessmentInvite', arguments);
};
CGServiceAjax.displayExamCalendar = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'displayExamCalendar', arguments);
};
CGServiceAjax.getColorAndEvent = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getColorAndEvent', arguments);
};
CGServiceAjax.getDisabledSlotIds = function(callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getDisabledSlotIds', arguments);
};
CGServiceAjax.getStartEndDateByAssessment = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getStartEndDateByAssessment', arguments);
};
CGServiceAjax.getMultipleInviteAssessment = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getMultipleInviteAssessment', arguments);
};
CGServiceAjax.sendMultipleAssessmentInvite = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'sendMultipleAssessmentInvite', arguments);
};
CGServiceAjax.getScoreForQuestions = function(p0, p1, p2, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getScoreForQuestions', arguments);
};
CGServiceAjax.getAvailability = function(p0, callback) {
return dwr.engine._execute(CGServiceAjax._path, 'CGServiceAjax', 'getAvailability', arguments);
};
//--------------- TeacherProfileViewInDivAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.TeacherProfileViewInDivAjax');
if (typeof this['TeacherProfileViewInDivAjax'] == 'undefined') TeacherProfileViewInDivAjax = {};
TeacherProfileViewInDivAjax._path = path;
TeacherProfileViewInDivAjax.displayRecordsBySSNForDate = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'displayRecordsBySSNForDate', arguments);
};
TeacherProfileViewInDivAjax.getDistrictSpecificPortfolioQuestions = function(p0, p1, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getDistrictSpecificPortfolioQuestions', arguments);
};
TeacherProfileViewInDivAjax.getElectronicReferencesGrid = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getElectronicReferencesGrid', arguments);
};
TeacherProfileViewInDivAjax.getVideoLinksGrid = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getVideoLinksGrid', arguments);
};
TeacherProfileViewInDivAjax.getAdditionalDocumentsGrid = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getAdditionalDocumentsGrid', arguments);
};
TeacherProfileViewInDivAjax.getInvolvementGrid = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getInvolvementGrid', arguments);
};
TeacherProfileViewInDivAjax.getOctDetails = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getOctDetails', arguments);
};
TeacherProfileViewInDivAjax.downloadOctUpload = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'downloadOctUpload', arguments);
};
TeacherProfileViewInDivAjax.getPhoneDetailByDiv = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getPhoneDetailByDiv', arguments);
};
TeacherProfileViewInDivAjax.getTeacherAcademicsGrid = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getTeacherAcademicsGrid', arguments);
};
TeacherProfileViewInDivAjax.getGridDataWorkExpEmployment = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getGridDataWorkExpEmployment', arguments);
};
TeacherProfileViewInDivAjax.getTeacherCertificationsGrid = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getTeacherCertificationsGrid', arguments);
};
TeacherProfileViewInDivAjax.getdistrictAssessmetGrid = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getdistrictAssessmetGrid', arguments);
};
TeacherProfileViewInDivAjax.getGridDataForTeacherProfileVisitHistoryByTeacher = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getGridDataForTeacherProfileVisitHistoryByTeacher', arguments);
};
TeacherProfileViewInDivAjax.getUpdateAndSaveAScore = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getUpdateAndSaveAScore', arguments);
};
TeacherProfileViewInDivAjax.showProfileContent = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'showProfileContent', arguments);
};
TeacherProfileViewInDivAjax.openEmployeeNumberInfo = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'openEmployeeNumberInfo', arguments);
};
TeacherProfileViewInDivAjax.openEmployeeNumberInfoOther = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'openEmployeeNumberInfoOther', arguments);
};
TeacherProfileViewInDivAjax.showProfileContentForTeacher = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'showProfileContentForTeacher', arguments);
};
TeacherProfileViewInDivAjax.getRefeNotes = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getRefeNotes', arguments);
};
TeacherProfileViewInDivAjax.saveReferenceNotesByAjax = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'saveReferenceNotesByAjax', arguments);
};
TeacherProfileViewInDivAjax.downloadReferenceNotes = function(p0, p1, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'downloadReferenceNotes', arguments);
};
TeacherProfileViewInDivAjax.getDistrictSpecificQuestionGrid = function(p0, p1, p2, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getDistrictSpecificQuestionGrid', arguments);
};
TeacherProfileViewInDivAjax.getDistrictSpecificQuestionGrid_ByDistrict = function(p0, p1, p2, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getDistrictSpecificQuestionGrid_ByDistrict', arguments);
};
TeacherProfileViewInDivAjax.getUpdateAndSaveAScoreFromTeacherPool = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getUpdateAndSaveAScoreFromTeacherPool', arguments);
};
TeacherProfileViewInDivAjax.downloadCertificationforCG = function(p0, p1, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'downloadCertificationforCG', arguments);
};
TeacherProfileViewInDivAjax.updatePNQByUser = function(p0, p1, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'updatePNQByUser', arguments);
};
TeacherProfileViewInDivAjax.displayTeacherLanguageByTeacherId = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'displayTeacherLanguageByTeacherId', arguments);
};
TeacherProfileViewInDivAjax.getHonorsGridWithoutAction = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getHonorsGridWithoutAction', arguments);
};
TeacherProfileViewInDivAjax.getMemberQualification = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getMemberQualification', arguments);
};
TeacherProfileViewInDivAjax.getTeacherEducationGrid = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getTeacherEducationGrid', arguments);
};
TeacherProfileViewInDivAjax.getTeacherAssessmentGrid = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getTeacherAssessmentGrid', arguments);
};
TeacherProfileViewInDivAjax.getLEACandidatePortfolio = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getLEACandidatePortfolio', arguments);
};
TeacherProfileViewInDivAjax.verifyTranscript = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'verifyTranscript', arguments);
};
TeacherProfileViewInDivAjax.updateTranscript = function(p0, p1, p2, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'updateTranscript', arguments);
};
TeacherProfileViewInDivAjax.showProfileContentForTeacherNew = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'showProfileContentForTeacherNew', arguments);
};
TeacherProfileViewInDivAjax.showProfilePartialData = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'showProfilePartialData', arguments);
};
TeacherProfileViewInDivAjax.showProfileContentForTeacherNewMul = function(p0, p1, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'showProfileContentForTeacherNewMul', arguments);
};
TeacherProfileViewInDivAjax.getBackgroundCheckGrid_DivProfile = function(p0, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getBackgroundCheckGrid_DivProfile', arguments);
};
TeacherProfileViewInDivAjax.getTeacherAssessmentGridSP = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'getTeacherAssessmentGridSP', arguments);
};
TeacherProfileViewInDivAjax.showProfileContent_new = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(TeacherProfileViewInDivAjax._path, 'TeacherProfileViewInDivAjax', 'showProfileContent_new', arguments);
};

//--------------- DSPQServiceAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.DSPQServiceAjax');
if (typeof this['DSPQServiceAjax'] == 'undefined') DSPQServiceAjax = {};
DSPQServiceAjax._path = path;

DSPQServiceAjax.getFieldOfDistrictList = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getFieldOfDistrictList', arguments);
};
DSPQServiceAjax.downloadTranscript = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'downloadTranscript', arguments);
};
DSPQServiceAjax.downloadResume = function(callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'downloadResume', arguments);
};
DSPQServiceAjax.downloadSubjectAreaExam = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'downloadSubjectAreaExam', arguments);
};
DSPQServiceAjax.getPFAcademinGridForDspq = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getPFAcademinGridForDspq', arguments);
};
DSPQServiceAjax.showEditAcademics = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'showEditAcademics', arguments);
};
DSPQServiceAjax.saveOrUpdateAcademics = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateAcademics', arguments);
};
DSPQServiceAjax.removeTranscript = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'removeTranscript', arguments);
};
DSPQServiceAjax.deletePFAcademinGrid = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'deletePFAcademinGrid', arguments);
};
DSPQServiceAjax.getPFAcademinGridForDspqWOT = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getPFAcademinGridForDspqWOT', arguments);
};
DSPQServiceAjax.getPFCertificationsGrid = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getPFCertificationsGrid', arguments);
};
DSPQServiceAjax.getPraxis = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getPraxis', arguments);
};
DSPQServiceAjax.removeReferences = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'removeReferences', arguments);
};
DSPQServiceAjax.downloadReference = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'downloadReference', arguments);
};
DSPQServiceAjax.downloadCertification = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'downloadCertification', arguments);
};
DSPQServiceAjax.saveOrUpdateElectronicReferences = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateElectronicReferences', arguments);
};
DSPQServiceAjax.getElectronicReferencesGrid = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getElectronicReferencesGrid', arguments);
};
DSPQServiceAjax.changeStatusElectronicReferences = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'changeStatusElectronicReferences', arguments);
};
DSPQServiceAjax.editElectronicReferences = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'editElectronicReferences', arguments);
};
DSPQServiceAjax.getVideoLinksGrid = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getVideoLinksGrid', arguments);
};
DSPQServiceAjax.saveOrUpdateVideoLinks = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateVideoLinks', arguments);
};
DSPQServiceAjax.editVideoLink = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'editVideoLink', arguments);
};
DSPQServiceAjax.downloadUploadedDocument = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'downloadUploadedDocument', arguments);
};
DSPQServiceAjax.getAdditionalDocumentsGrid = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getAdditionalDocumentsGrid', arguments);
};
DSPQServiceAjax.showEditTeacherAdditionalDocuments = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'showEditTeacherAdditionalDocuments', arguments);
};
DSPQServiceAjax.removeUploadedDocument = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'removeUploadedDocument', arguments);
};
DSPQServiceAjax.saveOrUpdateAdditionalDocuments = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateAdditionalDocuments', arguments);
};
DSPQServiceAjax.getSubjectAreasGrid = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getSubjectAreasGrid', arguments);
};
DSPQServiceAjax.deleteSubjectArea = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'deleteSubjectArea', arguments);
};
DSPQServiceAjax.showEditSubject = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'showEditSubject', arguments);
};
DSPQServiceAjax.getPFCertificationsGridDspqNoble = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getPFCertificationsGridDspqNoble', arguments);
};
DSPQServiceAjax.getElectronicReferencesGridNoble = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getElectronicReferencesGridNoble', arguments);
};
DSPQServiceAjax.getVideoPlayDiv = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getVideoPlayDiv', arguments);
};
DSPQServiceAjax.getPFEmploymentGrid = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getPFEmploymentGrid', arguments);
};
DSPQServiceAjax.saveOrUpdateEmployment = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateEmployment', arguments);
};
DSPQServiceAjax.showEditFormEmployment = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'showEditFormEmployment', arguments);
};
DSPQServiceAjax.deleteEmployment = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'deleteEmployment', arguments);
};
DSPQServiceAjax.getInvolvementGrid = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getInvolvementGrid', arguments);
};
DSPQServiceAjax.getInvolvementGridDistrictSpecific = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getInvolvementGridDistrictSpecific', arguments);
};
DSPQServiceAjax.saveOrUpdateInvolvement = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateInvolvement', arguments);
};
DSPQServiceAjax.showEditFormInvolvement = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'showEditFormInvolvement', arguments);
};
DSPQServiceAjax.deleteRecordInvolvment = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'deleteRecordInvolvment', arguments);
};
DSPQServiceAjax.getHonorsGrid = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getHonorsGrid', arguments);
};
DSPQServiceAjax.saveOrUpdateHonors = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateHonors', arguments);
};
DSPQServiceAjax.showEditHonors = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'showEditHonors', arguments);
};
DSPQServiceAjax.deleteRecordHonors = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'deleteRecordHonors', arguments);
};
DSPQServiceAjax.getTeacherVeteran = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getTeacherVeteran', arguments);
};
DSPQServiceAjax.displayTeacherLanguage = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'displayTeacherLanguage', arguments);
};
DSPQServiceAjax.insertOrUpdateTchrLang = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'insertOrUpdateTchrLang', arguments);
};
DSPQServiceAjax.delTeacherLanguage = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'delTeacherLanguage', arguments);
};
DSPQServiceAjax.showEditLang = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'showEditLang', arguments);
};
DSPQServiceAjax.getStateByCountry = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getStateByCountry', arguments);
};
DSPQServiceAjax.getStateByCountryPresent = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getStateByCountryPresent', arguments);
};
DSPQServiceAjax.getLatestCoverLetter = function(callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getLatestCoverLetter', arguments);
};
DSPQServiceAjax.getDistrictSpecificQuestion = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getDistrictSpecificQuestion', arguments);
};
DSPQServiceAjax.openEmployeeNumberInfo = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'openEmployeeNumberInfo', arguments);
};
DSPQServiceAjax.getDoNotHireStatusByEmpoloyeeId = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getDoNotHireStatusByEmpoloyeeId', arguments);
};
DSPQServiceAjax.doNotHiredForJeffco = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'doNotHiredForJeffco', arguments);
};
DSPQServiceAjax.getPortfolioHeader = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getPortfolioHeader', arguments);
};
DSPQServiceAjax.callNewSelfServiceApplicationFlow = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'callNewSelfServiceApplicationFlow', arguments);
};
DSPQServiceAjax.getGroup01Section01 = function(p0, p1, p2, p3, p4, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup01Section01', arguments);
};
DSPQServiceAjax.getCustomFieldBySectionId = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getCustomFieldBySectionId', arguments);
};
DSPQServiceAjax.getGroup01 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup01', arguments);
};
DSPQServiceAjax.getGroup02 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup02', arguments);
};
DSPQServiceAjax.getGroup02_2 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup02_2', arguments);
};
DSPQServiceAjax.getGroup03 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup03', arguments);
};
DSPQServiceAjax.getGroup03_8 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup03_8', arguments);
};
DSPQServiceAjax.getGroup03_9 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup03_9', arguments);
};
DSPQServiceAjax.getGroup03_10 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup03_10', arguments);
};
DSPQServiceAjax.getGroup03_11 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup03_11', arguments);
};
DSPQServiceAjax.getGroup03_21 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup03_21', arguments);
};
DSPQServiceAjax.getGroup03_27 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup03_27', arguments);
};
DSPQServiceAjax.getGroup03_28 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup03_28', arguments);
};
DSPQServiceAjax.getGroup04 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup04', arguments);
};
DSPQServiceAjax.getGroup04Section25Name = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup04Section25Name', arguments);
};
DSPQServiceAjax.getGroup04Section25 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup04Section25', arguments);
};
DSPQServiceAjax.getGroup04_12 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup04_12', arguments);
};
DSPQServiceAjax.getGroup04_13 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup04_13', arguments);
};
DSPQServiceAjax.getGroup04_14 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup04_14', arguments);
};
DSPQServiceAjax.getGroup04_15 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup04_15', arguments);
};
DSPQServiceAjax.saveGroup01Section01CoverLetter = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveGroup01Section01CoverLetter', arguments);
};
DSPQServiceAjax.getCLData = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getCLData', arguments);
};
DSPQServiceAjax.savePersonalInfoData = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, p26, p27, p28, p29, p30, p31, p32, p33, p34, p35, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'savePersonalInfoData', arguments);
};
DSPQServiceAjax.saveCustomFieldAnswer = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveCustomFieldAnswer', arguments);
};
DSPQServiceAjax.updatedspqJobWiseStatusAcademic = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'updatedspqJobWiseStatusAcademic', arguments);
};
DSPQServiceAjax.updateCredentialsGroup = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'updateCredentialsGroup', arguments);
};
DSPQServiceAjax.saveOrUpdateCert = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateCert', arguments);
};
DSPQServiceAjax.deleteRecordCert = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'deleteRecordCert', arguments);
};
DSPQServiceAjax.showEditFormCert = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'showEditFormCert', arguments);
};
DSPQServiceAjax.deleteVideoLink = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'deleteVideoLink', arguments);
};
DSPQServiceAjax.UpdateResume = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'UpdateResume', arguments);
};
DSPQServiceAjax.updateProfessionalGroup = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'updateProfessionalGroup', arguments);
};
DSPQServiceAjax.updateGroup04Section25 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'updateGroup04Section25', arguments);
};
DSPQServiceAjax.getGroup01_SectionName01 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getGroup01_SectionName01', arguments);
};
DSPQServiceAjax.completedspqJobWiseStatus = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'completedspqJobWiseStatus', arguments);
};
DSPQServiceAjax.getPreScreenFooter = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getPreScreenFooter', arguments);
};
DSPQServiceAjax.getEPIFooter = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getEPIFooter', arguments);
};
DSPQServiceAjax.getJSIFooter = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getJSIFooter', arguments);
};
DSPQServiceAjax.getJSIFooterL2 = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getJSIFooterL2', arguments);
};
DSPQServiceAjax.savePreScreenAnswerBySet = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'savePreScreenAnswerBySet', arguments);
};
DSPQServiceAjax.getEPIServiceDataL1 = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getEPIServiceDataL1', arguments);
};
DSPQServiceAjax.getJSIServiceDataL1 = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getJSIServiceDataL1', arguments);
};
DSPQServiceAjax.jafCheckInventory = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafCheckInventory', arguments);
};
DSPQServiceAjax.jafLoadAssessmentQuestions = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafLoadAssessmentQuestions', arguments);
};
DSPQServiceAjax.jafinsertTeacherAssessmentAttempt = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafinsertTeacherAssessmentAttempt', arguments);
};
DSPQServiceAjax.jafinsertLastAttempt = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafinsertLastAttempt', arguments);
};
DSPQServiceAjax.jafgetAssessmentSectionCampaignQuestions = function(p0, p1, p2, p3, p4, p5, p6, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafgetAssessmentSectionCampaignQuestions', arguments);
};
DSPQServiceAjax.jafFinishAssessment = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafFinishAssessment', arguments);
};
DSPQServiceAjax.jafCheckAssessmentDone = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafCheckAssessmentDone', arguments);
};
DSPQServiceAjax.jafSaveStrike = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafSaveStrike', arguments);
};
DSPQServiceAjax.jafValidationInventoryEPI = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafValidationInventoryEPI', arguments);
};
DSPQServiceAjax.jafValidationInventoryJSI = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafValidationInventoryJSI', arguments);
};
DSPQServiceAjax.getJAFCompleteProcess = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'getJAFCompleteProcess', arguments);
};
DSPQServiceAjax.validateJAFCompleteProcess = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'validateJAFCompleteProcess', arguments);
};
DSPQServiceAjax.downloadCLUploadFile = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'downloadCLUploadFile', arguments);
};
DSPQServiceAjax.jafCheckEmployeeDetail = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafCheckEmployeeDetail', arguments);
};
DSPQServiceAjax.jafNotifyUsers = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafNotifyUsers', arguments);
};
DSPQServiceAjax.jafMiamiValidation = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafMiamiValidation', arguments);
};
DSPQServiceAjax.jeffcoGetEmployeeNumberInfo = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jeffcoGetEmployeeNumberInfo', arguments);
};
DSPQServiceAjax.oneBtnJobApply = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'oneBtnJobApply', arguments);
};
DSPQServiceAjax.callSelfServiceApplicationFlowOuter = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'callSelfServiceApplicationFlowOuter', arguments);
};
DSPQServiceAjax.setDistrictSpecificVeteranValues = function(p0, p1, p2, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'setDistrictSpecificVeteranValues', arguments);
};
DSPQServiceAjax.saveGeneralKnw = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveGeneralKnw', arguments);
};
DSPQServiceAjax.jafDownloadGeneralKnowledge = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'jafDownloadGeneralKnowledge', arguments);
};
DSPQServiceAjax.downloadCustomFileUpload = function(p0, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'downloadCustomFileUpload', arguments);
};
DSPQServiceAjax.saveOrUpdateSubjectAreaExam = function(p0, p1, callback) {
return dwr.engine._execute(DSPQServiceAjax._path, 'DSPQServiceAjax', 'saveOrUpdateSubjectAreaExam', arguments);
};
//--------------- DWRAutoComplete --------------//
if (window['dojo']) dojo.provide('dwr.interface.DWRAutoComplete');
if (typeof this['DWRAutoComplete'] == 'undefined') DWRAutoComplete = {};
DWRAutoComplete._path = path;

DWRAutoComplete.getDegreeMasterList = function(p0, callback) {
return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getDegreeMasterList', arguments);
};
DWRAutoComplete.getRegionMasterList = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getRegionMasterList', arguments);};
DWRAutoComplete.getFieldOfStudyList = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getFieldOfStudyList', arguments);};
DWRAutoComplete.getSubjectList = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getSubjectList', arguments);};
DWRAutoComplete.getRequisationList = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getRequisationList', arguments);};
DWRAutoComplete.getUniversityMasterList = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getUniversityMasterList', arguments);};
DWRAutoComplete.getCertificateTypeList = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getCertificateTypeList', arguments);};
DWRAutoComplete.getCertificateNameList = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getCertificateNameList', arguments);};
DWRAutoComplete.getAllCertificateNameList = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getAllCertificateNameList', arguments);};
DWRAutoComplete.getAllCertificateTypeList = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getAllCertificateTypeList', arguments);};
DWRAutoComplete.getAllCertificateTypeList = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getAllCertificateTypeList', arguments);};
DWRAutoComplete.getDistrictMasterList = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getDistrictMasterList', arguments);};
DWRAutoComplete.getDistrictMasterList = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getDistrictMasterList', arguments);};
DWRAutoComplete.getSchoolMasterList = function(p0, p1, p2, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getSchoolMasterList', arguments);};
DWRAutoComplete.getSchoolMasterList = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getSchoolMasterList', arguments);};
DWRAutoComplete.getFilterCertificateTypeList = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getFilterCertificateTypeList', arguments);};
DWRAutoComplete.selectedCityByStateId = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'selectedCityByStateId', arguments);};
DWRAutoComplete.displayStateData = function(callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'displayStateData', arguments);};
DWRAutoComplete.displaySchoolList = function(p0, p1, p2, p3, p4, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'displaySchoolList', arguments);};
DWRAutoComplete.getSubjectByDistrict = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getSubjectByDistrict', arguments);};
DWRAutoComplete.getJobCategoryByDistrict = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getJobCategoryByDistrict', arguments);};
DWRAutoComplete.getSubjectByDistrictForAddJobOrder = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getSubjectByDistrictForAddJobOrder', arguments);};
DWRAutoComplete.getSubjectByDistrictForDSPQ = function(p0, p1, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getSubjectByDistrictForDSPQ', arguments);};
DWRAutoComplete.displayUSAStateData = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'displayUSAStateData', arguments);};
DWRAutoComplete.getDegreeTypeMasterList = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getDegreeTypeMasterList', arguments);};
DWRAutoComplete.getEeocDetail = function(p0, callback) {return dwr.engine._execute(DWRAutoComplete._path, 'DWRAutoComplete', 'getEeocDetail', arguments);};


//--------------- EventAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.EventAjax');
if (typeof this['EventAjax'] == 'undefined') EventAjax = {};
EventAjax._path = path;
EventAjax.getEventType = function(callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEventType', arguments);};
EventAjax.displayRecordsByEntityTypeEXL = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'displayRecordsByEntityTypeEXL', arguments);};
EventAjax.displayRecordsByEntityTypePDF = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'displayRecordsByEntityTypePDF', arguments);};
EventAjax.displayRecordsByEntityTypePrintPreview = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'displayRecordsByEntityTypePrintPreview', arguments);};
EventAjax.getFieldOfDistrictList = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getFieldOfDistrictList', arguments);};
EventAjax.getTemplatesByDistrictId = function(p0, p1, p2, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getTemplatesByDistrictId', arguments);};
EventAjax.cancelEvent = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'cancelEvent', arguments);};
EventAjax.displayEvents = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'displayEvents', arguments);};
EventAjax.getEventFormat = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEventFormat', arguments);};
EventAjax.getEventChannel = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEventChannel', arguments);};
EventAjax.getEventSchedule = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEventSchedule', arguments);};
EventAjax.saveEvent = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'saveEvent', arguments);};
EventAjax.deleteEvent = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'deleteEvent', arguments);};
EventAjax.updateEventStatus = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'updateEventStatus', arguments);};
EventAjax.checkDuplicacy = function(p0, p1, p2, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'checkDuplicacy', arguments);};
EventAjax.getTemplateDescription = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getTemplateDescription', arguments);};
EventAjax.getEventEmailMessageTemp = function(p0, p1, p2, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEventEmailMessageTemp', arguments);};
EventAjax.getEventEmailMessageTempBody = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEventEmailMessageTempBody', arguments);};
EventAjax.getEventById = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEventById', arguments);};
EventAjax.printEventDetails = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'printEventDetails', arguments);};
EventAjax.getEmailMessageTempFacilitators = function(p0, p1, p2, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEmailMessageTempFacilitators', arguments);};
EventAjax.getFacilitatorsEmailMessageTempBody = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getFacilitatorsEmailMessageTempBody', arguments);};
EventAjax.showVirtualVideoInterview = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'showVirtualVideoInterview', arguments);};
EventAjax.saveCGPhoneEvent = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'saveCGPhoneEvent', arguments);};
EventAjax.getDistrictUserList = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getDistrictUserList', arguments);};
EventAjax.getEventByEventId = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'getEventByEventId', arguments);};
EventAjax.checkAvailFacilitator = function(p0, callback) {return dwr.engine._execute(EventAjax._path, 'EventAjax', 'checkAvailFacilitator', arguments);};


//--------------- EventScheduleAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.EventScheduleAjax');
if (typeof this['EventScheduleAjax'] == 'undefined') EventScheduleAjax = {};
EventScheduleAjax._path = path;
EventScheduleAjax.getTimeZone = function(callback) {
return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'getTimeZone', arguments);
};
EventScheduleAjax.saveSchedule = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'saveSchedule', arguments);};
EventScheduleAjax.geteventSchedule = function(p0, p1, p2, p3, p4, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'geteventSchedule', arguments);};
EventScheduleAjax.deleteEventScheduleById = function(p0, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'deleteEventScheduleById', arguments);};
EventScheduleAjax.getScheduleById = function(p0, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'getScheduleById', arguments);};
EventScheduleAjax.checkScheduleByEventId = function(p0, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'checkScheduleByEventId', arguments);};
EventScheduleAjax.saveVirtualVideoSchedule = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'saveVirtualVideoSchedule', arguments);};
EventScheduleAjax.getCandidateSlotBySccheduleId = function(p0, p1, p2, p3, p4, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'getCandidateSlotBySccheduleId', arguments);};
EventScheduleAjax.createAddScheduleDiv = function(p0, p1, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'createAddScheduleDiv', arguments);};
EventScheduleAjax.getVVIDays = function(p0, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'getVVIDays', arguments);};
EventScheduleAjax.getCandidateByEvent = function(p0, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'getCandidateByEvent', arguments);};
EventScheduleAjax.addCandidateToSlot = function(p0, p1, callback) {return dwr.engine._execute(EventScheduleAjax._path, 'EventScheduleAjax', 'addCandidateToSlot', arguments);};


//--------------- ManageFacilitatorsAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.ManageFacilitatorsAjax');
if (typeof this['ManageFacilitatorsAjax'] == 'undefined') ManageFacilitatorsAjax = {};
ManageFacilitatorsAjax._path = path;
ManageFacilitatorsAjax.getUserByDistrict = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {
return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'getUserByDistrict', arguments);
};
ManageFacilitatorsAjax.saveFacilitatorByUserId = function(p0, p1, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'saveFacilitatorByUserId', arguments);};
ManageFacilitatorsAjax.getFacilitatorGrid = function(p0, p1, p2, p3, p4, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'getFacilitatorGrid', arguments);};
ManageFacilitatorsAjax.removeFacilitator = function(p0, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'removeFacilitator', arguments);};
ManageFacilitatorsAjax.saveFacilitatorTemp = function(p0, p1, p2, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'saveFacilitatorTemp', arguments);};
ManageFacilitatorsAjax.saveFacilitator = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'saveFacilitator', arguments);};
ManageFacilitatorsAjax.viewFacilitatorExcelFile = function(p0, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'viewFacilitatorExcelFile', arguments);};
ManageFacilitatorsAjax.getTempFacilitatorList = function(callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'getTempFacilitatorList', arguments);};
ManageFacilitatorsAjax.saveFacilitatorByExcel = function(p0, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'saveFacilitatorByExcel', arguments);};
ManageFacilitatorsAjax.truncateFacilitatorTemp = function(callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'truncateFacilitatorTemp', arguments);};
ManageFacilitatorsAjax.getFieldOfUserList = function(p0, p1, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'getFieldOfUserList', arguments);};
ManageFacilitatorsAjax.getFacilitatorGridNew = function(p0, callback) {return dwr.engine._execute(ManageFacilitatorsAjax._path, 'ManageFacilitatorsAjax', 'getFacilitatorGridNew', arguments);};


//--------------- ParticipantsAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.ParticipantsAjax');
if (typeof this['ParticipantsAjax'] == 'undefined') ParticipantsAjax = {};
ParticipantsAjax._path = path;

ParticipantsAjax.saveCandidate = function(p0, p1, callback) {
return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'saveCandidate', arguments);
};
ParticipantsAjax.sendEmail = function(p0, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'sendEmail', arguments);};
ParticipantsAjax.displayParticipantsGrid = function(p0, p1, p2, p3, p4, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'displayParticipantsGrid', arguments);};
ParticipantsAjax.displayCandidatesOnSearch = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'displayCandidatesOnSearch', arguments);};
ParticipantsAjax.displayTempGrid = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'displayTempGrid', arguments);};
ParticipantsAjax.addTempProspectCand = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'addTempProspectCand', arguments);};
ParticipantsAjax.saveParticipant = function(p0, p1, p2, p3, p4, p5, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'saveParticipant', arguments);};
ParticipantsAjax.delParticipant = function(p0, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'delParticipant', arguments);};
ParticipantsAjax.saveImportList = function(p0, p1, p2, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'saveImportList', arguments);};
ParticipantsAjax.acceptImport = function(p0, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'acceptImport', arguments);};
ParticipantsAjax.clearAllTempRec = function(p0, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'clearAllTempRec', arguments);};
ParticipantsAjax.checkCandidates = function(p0, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'checkCandidates', arguments);};
ParticipantsAjax.sendEmailWithTeacherId = function(p0, p1, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'sendEmailWithTeacherId', arguments);};
ParticipantsAjax.checkInviteInterviewButton = function(p0, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'checkInviteInterviewButton', arguments);};
ParticipantsAjax.getJobAppliedTeachers = function(p0, p1, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'getJobAppliedTeachers', arguments);};
ParticipantsAjax.getPropectsArray = function(p0, p1, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'getPropectsArray', arguments);};
ParticipantsAjax.displayParticipantsGridNew = function(p0, callback) {return dwr.engine._execute(ParticipantsAjax._path, 'ParticipantsAjax', 'displayParticipantsGridNew', arguments);};


//--------------- SelfServiceCandidateProfileService --------------//
if (window['dojo']) dojo.provide('dwr.interface.SelfServiceCandidateProfileService');
if (typeof this['SelfServiceCandidateProfileService'] == 'undefined') SelfServiceCandidateProfileService = {};
SelfServiceCandidateProfileService._path = path;

SelfServiceCandidateProfileService.getGridDataForTeacherProfileVisitHistoryByTeacher = function(p0, p1, p2, p3, p4, p5, callback) {
return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getGridDataForTeacherProfileVisitHistoryByTeacher', arguments);
};
SelfServiceCandidateProfileService.getUpdateAndSaveAScore = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getUpdateAndSaveAScore', arguments);};
SelfServiceCandidateProfileService.getAdditionalDocuments = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getAdditionalDocuments', arguments);};
SelfServiceCandidateProfileService.saveReferenceNotesByAjax = function(p0, p1, p2, p3, p4, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'saveReferenceNotesByAjax', arguments);};
SelfServiceCandidateProfileService.getCustomQuestion = function(p0, p1, p2, p3, p4, p5, p6, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getCustomQuestion', arguments);};
SelfServiceCandidateProfileService.showProfileContentNewSSPF = function(p0, p1, p2, p3, p4, p5, p6, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'showProfileContentNewSSPF', arguments);};
SelfServiceCandidateProfileService.getGridFooter = function(p0, p1, p2, p3, p4, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getGridFooter', arguments);};
SelfServiceCandidateProfileService.getPersonalInfoProfiler = function(p0, p1, p2, p3, p4, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getPersonalInfoProfiler', arguments);};
SelfServiceCandidateProfileService.getAcademicsSectionProfiler = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getAcademicsSectionProfiler', arguments);};
SelfServiceCandidateProfileService.getGeneralKnowladgeExamProfiler = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getGeneralKnowladgeExamProfiler', arguments);};
SelfServiceCandidateProfileService.getTeacherHonors = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getTeacherHonors', arguments);};
SelfServiceCandidateProfileService.getInvolvementWorkGridProfiler = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getInvolvementWorkGridProfiler', arguments);};
SelfServiceCandidateProfileService.getEmploymentGrid = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getEmploymentGrid', arguments);};
SelfServiceCandidateProfileService.getReferenceGrid = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getReferenceGrid', arguments);};
SelfServiceCandidateProfileService.getVideoLinkGrid = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getVideoLinkGrid', arguments);};
SelfServiceCandidateProfileService.getlanguageProfiency = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getlanguageProfiency', arguments);};
SelfServiceCandidateProfileService.getPersonalSectionFixedArea = function(p0, p1, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getPersonalSectionFixedArea', arguments);};
SelfServiceCandidateProfileService.getAcademicAchievement = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getAcademicAchievement', arguments);};
SelfServiceCandidateProfileService.getCertificationLicense = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getCertificationLicense', arguments);};
SelfServiceCandidateProfileService.getRefeNotesNew = function(p0, p1, p2, p3, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getRefeNotesNew', arguments);};
SelfServiceCandidateProfileService.saveOrUpdateElectronicReferencesProfiler = function(p0, p1, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'saveOrUpdateElectronicReferencesProfiler', arguments);};
SelfServiceCandidateProfileService.saveOrUpdateVideoLinksProfiler = function(p0, p1, p2, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'saveOrUpdateVideoLinksProfiler', arguments);};
SelfServiceCandidateProfileService.getGroup3OpentextProfiler = function(p0, p1, p2, p3, p4, p5, p6, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getGroup3OpentextProfiler', arguments);};
SelfServiceCandidateProfileService.getCustomQuestionMaster = function(p0, p1, p2, p3, p4, p5, p6, p7, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getCustomQuestionMaster', arguments);};
SelfServiceCandidateProfileService.getOldCustomQuestionByDistrict = function(p0, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'getOldCustomQuestionByDistrict', arguments);};
SelfServiceCandidateProfileService.saveProfileCustomFieldAnswer = function(p0, p1, callback) {return dwr.engine._execute(SelfServiceCandidateProfileService._path, 'SelfServiceCandidateProfileService', 'saveProfileCustomFieldAnswer', arguments);};

//--------------- MassStatusUpdateCGAjax --------------//
if (window['dojo']) dojo.provide('dwr.interface.MassStatusUpdateCGAjax');
if (typeof this['MassStatusUpdateCGAjax'] == 'undefined') MassStatusUpdateCGAjax = {};
MassStatusUpdateCGAjax._path = path;
MassStatusUpdateCGAjax.displayStatusDashboard_CGMass = function(p0, callback) {
return dwr.engine._execute(MassStatusUpdateCGAjax._path, 'MassStatusUpdateCGAjax', 'displayStatusDashboard_CGMass', arguments);
};
MassStatusUpdateCGAjax.getStatusDetailsInfo_CGMass = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(MassStatusUpdateCGAjax._path, 'MassStatusUpdateCGAjax', 'getStatusDetailsInfo_CGMass', arguments);
};
MassStatusUpdateCGAjax.getSectionWiseTemaplteslist_CGMass = function(p0, callback) {
return dwr.engine._execute(MassStatusUpdateCGAjax._path, 'MassStatusUpdateCGAjax', 'getSectionWiseTemaplteslist_CGMass', arguments);
};
MassStatusUpdateCGAjax.getStatusWiseEmailForAdmin_CGMass = function(p0, p1, p2, callback) {
return dwr.engine._execute(MassStatusUpdateCGAjax._path, 'MassStatusUpdateCGAjax', 'getStatusWiseEmailForAdmin_CGMass', arguments);
};
MassStatusUpdateCGAjax.massPersistentData = function(p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15, p16, p17, p18, p19, p20, p21, p22, p23, p24, p25, callback) {
return dwr.engine._execute(MassStatusUpdateCGAjax._path, 'MassStatusUpdateCGAjax', 'massPersistentData', arguments);
};

MassStatusUpdateCGAjax.sendMultipleOnlineActivityLink = function(p0, p1, p2, callback) {
return dwr.engine._execute(MassStatusUpdateCGAjax._path, 'MassStatusUpdateCGAjax', 'sendMultipleOnlineActivityLink', arguments);
};

MassStatusUpdateCGAjax.getOnlyCredentialReview = function(p0, p1, p2, p3, callback) {
return dwr.engine._execute(MassStatusUpdateCGAjax._path, 'MassStatusUpdateCGAjax', 'getOnlyCredentialReview', arguments);
};

