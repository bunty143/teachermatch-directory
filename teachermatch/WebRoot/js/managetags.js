var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{	
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	var entityID	=	document.getElementById("entityID").value;
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}	
	displayTags();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var entityID	=	document.getElementById("entityID").value;
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}
	displayTags();
}
function addNewTags()
{
	$('#tagsDiv').show();
	$("#removeTagsIcon").hide();
	
	$('#tags').val("");
	$('#secStatusId').val("");
	$('#districtName').val("");
	$('#tags').css("background-color", "");
	$('#districtName').css("background-color", "");
	document.getElementById('pathOfTagsIconFile').value="";
	document.getElementById('pathOfTagsIcon').value="";
	if($('#entityID').val()==1){
		$('#districtName').focus();
		document.getElementById('districtId').value="";
	}else{
		$('#tags').focus();
	}
}
function cancelTagsDiv()
{
	$('#tagsDiv').hide();
	$('#tags').val("");
	$('#districtName').val("");
	$('#errordiv').hide();
	$('#tags').css("background-color", "");
	$('#districtName').css("background-color", "");
}
/*========  Search District ===============*/
function displayOrHideSearchBox()
{ 
	districtNameFilter=1;
	var entityID	=	document.getElementById("entityID").value;
	if(entityID==1){
		document.getElementById("districtNameFilter").value="";
		document.getElementById("districtIdFilter").value="";
		var op=document.getElementById("entityType");
		var searchEntityType=op.options[op.selectedIndex].value;
		if(searchEntityType == 2)
		{
			document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrict;
			$("#districtClassMaster").show();
			$("#Searchbox").show();
			$('#districtNameFilter').focus();
		}else{
			$("#Searchbox").hide();
			$("#districtClassMaster").hide();
		}
	}
		
}
function chkForEnterSearchTags(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		page = 1;
		displayTags();
	}	
}
function chkForEnterSaveTags(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveAndUploadTagsIcon();
	}	
}
function searchTags()
{
	page = 1;
	var op=document.getElementById("entityType");
	searchEntityType=op.options[op.selectedIndex].value;
	if(searchEntityType == 1){
		$("#districtMasterId").val(0);
	}
	displayTags();
}
function displayTags()
{	
	var resultFlag=false;
	var entityID	=	document.getElementById("entityID").value;
	var hqId	=	document.getElementById("hqId").value;
	var branchId = document.getElementById("branchId").value;
	var searchTextId=0;
	var searchEntityType
	if(entityID==1){
		var op=document.getElementById("entityType");
		searchEntityType=op.options[op.selectedIndex].value;
		if(searchEntityType == 2)
		{
			if($("#districtIdFilter").val()!=undefined || $("#districtIdFilter").val()!=""){
			searchTextId	=	document.getElementById("districtIdFilter").value;
			$('#districtId').val($("#districtIdFilter").val());
			}else{
				searchTextId	=	$("#districtMasterId").val();
				$('#districtId').val($("#districtMasterId").val());
			}
		}else{
			if($("#districtMasterId").val()==0){
			searchTextId=0;
			resultFlag=false;	
			}else{
				if($("#showSessionStatus").val()==1){
				searchTextId	=	$("#districtMasterId").val();
				$('#districtId').val($("#districtMasterId").val());
				resultFlag=true;
				}else{
					searchTextId=0;
					resultFlag=true;
				}
			}
		}
	}else{
		searchTextId=0;
		resultFlag=true;
	}
	ManageTagsAjax.displayTags(resultFlag,searchEntityType,entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType,hqId,branchId,{ 
		async: true,
		callback: function(data){
		$('#tagsGrid').html(data);
		applyScrollOnTbl();
	},
	});
	hideSearchAgainMaster();
}

function saveAndUploadTagsIcon(){
	var secStsName=trim(document.getElementById('tags').value);
	var secStatusId=trim(document.getElementById('secStatusId').value);
	var districtId="";
	var entityID=trim(document.getElementById('entityID').value);
	var baseStatus;
	
	var pathOfTagsIconFile = document.getElementById("pathOfTagsIconFile");
	var fileName = pathOfTagsIconFile.value;
	var hqId	=	document.getElementById("hqId").value;
	var branchId=trim(document.getElementById('branchId').value);
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	
	if(entityID!=5 && entityID!=6){
		districtId=trim(document.getElementById('districtId').value);
	}
	var fileSize=0;		
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{		
		if(pathOfTagsIconFile.files[0]!=undefined)
		fileSize = pathOfTagsIconFile.files[0].size;
	}
	
	var counter				=	0;
	var focuscount			=	0;
	$('#errordiv').empty();
	$('#tags').css("background-color", "");
	
	
	if(entityID==1){
		$('#districtName').css("background-color", "");
		var districtName=trim(document.getElementById('districtName').value);
		if(districtName!=""){
				if(districtId==""){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
				$('#districtName').focus();
				$('#districtName').css("background-color", "#F5E7E1");
				counter++;
				focuscount++;
			}
		}
	}
	
	if (secStsName	==	"")
	{	
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgTagName+"<br>");
		if(focuscount	==	0)
			$('#tags').focus();
			$('#tags').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	if(ext!="")
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png'))
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgAcceptableFileFormats+"<br>");
				if(focs==0)
					$('#pathOfTagsIconFile').focus();
				
				$('#pathOfTagsIconFile').css("background-color",txtBgColor);
				cnt++;focs++;	
				return false;
		}	
		else if(fileSize>=10485760)
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgDocSize10mb+"<br>");
				if(focs==0)
					$('#pathOfTagsIconFile').focus();
				
				$('#pathOfTagsIconFile').css("background-color",txtBgColor);
				cnt++;focs++;	
				return false;
		}
		if(fileName==""){
			fileName= document.getElementById("pathOfTagsIcon").value;
		}	
		/*if (fileName	==	"")
		{	
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please select Tag Icon<br>");
			if(focuscount	==	0)
				$('#pathOfTagsIconFile').focus();
				$('#pathOfTagsIconFile').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}*/
		
	if(counter==0){
		var tagFile=pathOfTagsIconFile.value;
		if(tagFile!=""){
			ManageTagsAjax.findDuplicateTags(secStatusId,secStsName,districtId,fileName,hqId,branchId,{ 
				async: false,
				callback: function(data){
					if(data==true){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgDuplicateTags+"<br>");
						if(counter	==	0)
							$('#tags').focus();
							$('#tags').css("background-color", "#F5E7E1");
						counter++;
					}else{
						$('#loadingDiv').show();
						document.getElementById("frmTagsIcon").submit();
					}	
				},
			});
		}else{
			ManageTagsAjax.saveTags(secStatusId,secStsName,districtId,fileName,hqId,branchId,{ 
				async: true,
				callback: function(data){
					if(data==true){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgDuplicateTags+"<br>");
						if(counter	==	0)
							$('#tags').focus();
							$('#tags').css("background-color", "#F5E7E1");
						counter++;
					}else{
						cancelTagsDiv();
					}	
					displayTags();
				},
			});
		}
	}
}

function saveTags(fileName)
{
	var secStsName=trim(document.getElementById('tags').value);
	var secStatusId=trim(document.getElementById('secStatusId').value);
	var districtId="";
	var branchId=trim(document.getElementById('branchId').value);
	var entityID=trim(document.getElementById('entityID').value);
	var hqId	=	document.getElementById("hqId").value;
	
	if(entityID!=5 && entityID!=6)
		districtId=trim(document.getElementById('districtId').value);
		
	if(fileName==""){
		fileName= document.getElementById("pathOfTagsIcon").value;
	}
	ManageTagsAjax.saveTags(secStatusId,secStsName,districtId,fileName,hqId,branchId,{ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			cancelTagsDiv();
			displayTags();
		},
	});
}

/*function removeTagsIconFile()
{
	var certId = document.getElementById("secStatusId").value;
	if(window.confirm("Are you sure, you would like to delete the TagsIcon."))
	{
		PFCertifications.removeTagsIcon(secStatusId,{ 
			async: false,
			callback: function(data){	
				$("#removeTagsIcon").hide();
				showGrid();
				hideForm();
			},errorHandler:handleError
		});
	}
}*/

function downloadTagsIcon(secStsId, linkId)
{	
	ManageTagsAjax.downloadTagsIcon(secStsId,{
		async: false,
		errorHandler:handleError,
		callback:function(data){ 
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmCert").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function editSecStatus(secStatusId)
{
	
	$('#errordiv').empty();
	document.getElementById('pathOfTagsIconFile').value="";
	$('#pathOfTagsIconFile').css("background-color", "");
	$('#tags').css("background-color", "");
	$('#districtName').css("background-color", "");
	addNewTags();
	
	ManageTagsAjax.getSecStsId(secStatusId,{ 
		async: true,
		callback: function(data){
		document.getElementById('tags').value=data.secStatusName;
		document.getElementById('secStatusId').value=data.secStatusId;
		
		if(data.districtMaster!=null){
			if(document.getElementById('entityID').value==1){
				document.getElementById('districtName').value=data.districtMaster.districtName;
			}
			if(document.getElementById('districtId')){
				document.getElementById('districtId').value=data.districtMaster.districtId;		
			length=1;
			}
		}
		
		if(document.getElementById('entityID').value==1){
			if(data.pathOfTagsIcon!=null){
				$("#removeTagsIcon").show();
				document.getElementById("pathOfTagsIcon").value=data.pathOfTagsIcon;
				document.getElementById("divTagsName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadTagsIcon('"+data.secStatusId+"','hrefEditRef');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=400,height=300,toolbar=1,resizable=0');" +
				"return false;\">"+data.pathOfTagsIcon+"</a>";
			}
		}else{
			if(data.pathOfTagsIcon!=null){
				$("#removeTagsIcon").show();
				document.getElementById("pathOfTagsIcon").value=data.pathOfTagsIcon;
				document.getElementById("divTagsName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadTagsIcon('"+data.secStatusId+"','hrefEditRef');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=400,height=300,toolbar=1,resizable=0');" +
				"return false;\">"+data.pathOfTagsIcon+"</a>";
			}
		}	
		
		},
	});
}

function activateDeactivateSecStatus(secStsId,status)
{	
	ManageTagsAjax.activateDeactivateSecStatus(secStsId,status,{ 
		async: true,
		callback: function(data){
		displayTags();
		},
	});
}


/********************************** district auto completer ***********************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


function clearTagsFile()
{
	// line added by ashish ratan for IE 10-11
	// replace field with old html
	// comment by ashish ratan
		$('#pathOfTagsIconFile')
		.replaceWith(
				"<input id='pathOfTagsIconFile' name='pathOfTagsIconFile' type='file' width='20px;' />");	 
	document.getElementById("pathOfTagsIconFile").value=null;
	document.getElementById("pathOfTagsIconFile").reset();
	$('#pathOfTagsIconFile').css("background-color","");	
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

	function setEntityType()
  	{
  		var entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="1" selected="selected">TM</option><option value="2">District</option>';
  		var district="<div  id='Searchbox' class='col-sm-12 col-md-12 <c:out value='${hide}'/><label id='captionDistrictOrSchool'>District</label>"+
			             		"<input type='text' id='districtNameFilter' name='districtNameFilter' class='form-control'"+
			             		"onfocus=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\""+
								"onkeyup=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\""+
								"onblur=\"hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');\"/>"+
								"<input type='hidden' id='districtIdFilter' value='${userMaster.districtId.districtId}'/>"+
								"<div id='divTxtShowDataFilter'  onmouseover=\"mouseOverChk('divTxtShowDataFilter','districtNameFilter');\" style=' display:none;position:absolute;z-index: 5000' class='result' ></div></div>";
  		var btnRecord='<button class="btn btn-primary" type="button" onclick="searchTags()">Search <i class="icon"></i></button>';
  		globalSearchSection(entityString,"","",district,btnRecord);
  		setTimeout(function(){ 
  		if($("#districtMasterId").val()!=0){
  			$("select#entityType").prop('selectedIndex', 1);
  			displayOrHideSearchBox();
  			$("#districtNameFilter").val($("#districtMasterName").val());
  			hideSearchAgainMaster();
  		}else{
  			showSearchAgainMaster();
  		}
  		}, 50);
  	}