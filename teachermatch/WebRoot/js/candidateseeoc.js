var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			//alert(" Hi ");
			$('#schoolName').attr('readonly', true);
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
			
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*--- mukesh ------------------------------School Auto Complete Js Starts ----------------------------------------------------------------*/

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	//alert(schoolName);
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
	//alert(districtId);
	CandidatesEECDataAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr +exception.javaClassName);}
}



// for paging and sorting
function getPaging(pageno)
{
        //alert("Paging");
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		searchRecordsByEntityTypeEEC();
	
}


function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	  if(pageno!=''){
			page=pageno;
			//alert("page :: "+page);
		}else{
			page=1;
			//alert("default");
		}
		sortOrderStr=sortOrder;
		//alert("sortOrderStr :: "+sortOrderStr);
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			//alert("AA")
			noOfRows = document.getElementById("pageSize").value;
		}else{
		//	alert("BB");
			noOfRows=50;
		}
		searchRecordsByEntityTypeEEC();
		
	
}

var JobOrderType
var searchTextId
var schoolId
var jobOrderId
var	firstName
var	lastName
var	emailAddress
var status
var jobReqNoId
var resultFlag
var resultFlag=true

function searchRecordsByEntityTypeEEC()
{ 
	$('#loadingDiv').fadeIn();
	 JobOrderType     =   document.getElementById("JobOrderType").value;
	 searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	 schoolId		 =	document.getElementById("schoolId").value;
	 jobOrderId	     =	document.getElementById("jobOrderId").value;
	 firstName        =	document.getElementById("firstName").value;
	 lastName         =	document.getElementById("lastName").value;
	 emailAddress     =	document.getElementById("emailAddress").value;
	 status	=	0;
	 jobReqNoId=0;
	
	CandidatesEECDataAjax.displayRecordsByEntityType(resultFlag,JobOrderType,searchTextId,schoolId,"",
			"",jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,firstName,lastName,emailAddress,{ 
		async: true,
		callback: function(data)
		{	
		
		document.getElementById("divMainEEC").innerHTML	=data;
		$('#loadingDiv').hide();
	    applyScrollOnTblEEC();
		},
	errorHandler:handleError
	});
}




function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGridEEC").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#cg'+j).tooltip();
		$('#cgn'+j).tooltip();
	}
}


function generateEECExcel()
{
	$('#loadingDiv').fadeIn();
			

	CandidatesEECDataAjax.displayRecordsByEntityTypeForEXCEL(resultFlag,JobOrderType,searchTextId,schoolId,"",
			"",jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,firstName,lastName,emailAddress,{ 
		async: true,
		callback: function(data)
		{
		$('#loadingDiv').hide();
			if(deviceType)
			{				
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();						
			}
			else
			{
				try
				{
					document.getElementById('ifrmTrans').src = "eeocreport/"+data+"";
				}
				catch(e)
				{alert(e);}
			}
		},
	errorHandler:handleError
	});
}


function downloadCandidateEEOCReport()
{
	$('#loadingDiv').fadeIn();
		

	CandidatesEECDataAjax.downloadCandidateEEOCReport(resultFlag,JobOrderType,searchTextId,schoolId,"",
			"",jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,firstName,lastName,emailAddress,{ 
	async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#loadingDiv').hide();
		if(deviceType || deviceTypeAndroid)
		{
			window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
		}		
		else
		{
			$('#modalDownloadEEOC').modal('hide');
			document.getElementById('ifrmEEOC').src = ""+data+"";
			try{
				$('#modalDownloadEEOC').modal('show');
			}catch(err)
			  {}	
			
		}		
		return false;
	}});
}

 function generateEECPrintPre()
 {
	 
	
	$('#loadingDiv').fadeIn();
	
	CandidatesEECDataAjax.displayRecordsByEntityTypePrintPre(resultFlag,JobOrderType,searchTextId,schoolId,"",
			"",jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,firstName,lastName,emailAddress,{ 
		async: true,
		callback: function(data)
		{	
		$('#pritEEOCDataTableDiv').html(data);
		applyScrollOnPrintTable();
		$("#printEEOCDiv").modal('show');
		
		//$('#loadingDiv').hide();
	    
		},
	errorHandler:handleError
	});
	
 }

 function printEEOCDATA()
 {
	
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	CandidatesEECDataAjax.displayRecordsByEntityTypePrintPre(resultFlag,JobOrderType,searchTextId,schoolId,"",
			"",jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,firstName,lastName,emailAddress,{ 
		async: true,
		callback: function(data)
		{	
			try{
			 if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 }
			}catch (e) 
			{
				$('#printmessage1').modal('show');							 
			}
	    
		},
	errorHandler:handleError
	});
	
 }
 
 
 var tmstyle={
			color: '#007AB4',
			fontSize: '10px',
			fontfamily:'Century Gothic'
		}
 
function generateGenderChart()
 {
	

	$("#gendercontainer").html("<div class='img-responsive' style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	$("#genderChartEEOCDiv").modal('show');
			
	// alert($('#teacherMIds').val())
	 var teachrIds=$('#teacherMIds').val();
	CandidatesEECDataAjax.displayRecordsByGender(teachrIds,{ 
		async: true,
		callback: function(data)
		{
		
		
	var malecountprcts				  	=	data[0];
	var femalecountprcts			    =	data[1];
    var declinecountprcts			    =	data[2];
    var noresponsecountprcts		    =	data[3];

   var overallmalecountprcts		    =	data[4];
   var overallfemalecountprcts			=	data[5];
   var overalldeclinecountprcts	   		=	data[6];
   var overallnoresponsecountprcts  	=   data[7];
		
		
		
		var  categories = [resourceJSON.msgMale, resourceJSON.msgFemale, resourceJSON.lblDeclineAnswer, resourceJSON.lblNoResponse],
        name = resourceJSON.msgGenderStats,
        data = [{
                y: malecountprcts,
                color: '#ff0000',
                drilldown: {
                    name: resourceJSON.msgMale,
                    categories: [resourceJSON.msgMale],
                    data: [overallmalecountprcts],
                    color: '#ff0000'
                }
            }, {
                y: femalecountprcts,
                color: '#ffd700',
                drilldown: {
                    name: resourceJSON.msgFemale,
                    categories: [resourceJSON.msgFemale],
                    data: [overallfemalecountprcts],
                    color: '#ffd700'
                }
            }, {
                y: declinecountprcts,
                color: "#8bbc21",
                drilldown: {
                    name: resourceJSON.lblDeclineAnswer,
                    categories: [resourceJSON.lblDeclineAnswer],
                    data: [overalldeclinecountprcts],
                    color: "#8bbc21"
                }           
            }, {
                y: noresponsecountprcts,
                color: "#7cb5ec",
                drilldown: {
                    name: resourceJSON.lblNoResponse,
                    categories: [resourceJSON.lblNoResponse],
                    data: [overallnoresponsecountprcts],
                   color: "#7cb5ec"
                }           
            }
            
            ];

    // Build the data arrays
    var browserData = [];
    var versionsData = [];
    for (var i = 0; i < data.length; i++) {
        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        for (var j = 0; j < data[i].drilldown.data.length; j++) {
            var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
               // color: Highcharts.Color(data[i].color).brighten(brightness).get()
			    color: data[i].color
            });
        }
    }
		
	// Create the chart
    var chartOptions = {
	    	chart: {
	        type: 'pie',
	        renderTo: 'gendercontainer',
	        events: {click:function () {
	            hs.htmlExpand(document.getElementById(chartOptions.chart.renderTo), {
	                width: 9999,
	                height: 9999,
	                allowWidthReduction: true,
	                preserveContent: false
	            }, {
	                chartOptions: chartOptions
	            });
	        }}
	    },
	    exporting: {
            enabled: false
        },
        title: {
            text: resourceJSON.msgGenderStats,
            align: "left",
            style:tmstyle
        },
        plotOptions: {
        	 dataLabels: {
             enabled: true
             },
            pie: {
                shadow: false,
                center: ['53%', '47%']
            }
        },
        tooltip: {
            valueSuffix: '% ',
            style: tmstyle
        },
        credits: {
            enabled: false
        },
        series: [{
            name: resourceJSON.msgCAndWithinDist,
            data: browserData,
            size: '60%',
            style:tmstyle,
            dataLabels: {
        	    enabled: false,
                formatter: function() {
                    return this.y > 5 ? this.point.name : null;
                },
                color: 'white',
                distance: -30
            }
        }, {
            name: 'All Candidates',
            data: versionsData,
            style:tmstyle,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function() {
                    // display only if larger than 1
                    return this.y >=0 ? ''+ this.point.name +': '+ this.y +'%'  : null;
                },style:tmstyle
            }
        }]
    };
    
    var chart = new Highcharts.Chart(chartOptions);
			},
	errorHandler:handleError
	});
	
}
 
function generateEthnicityChart()
{

	$("#ethnicitycontainer").html("<div class='img-responsive' style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	var teachrIds=$('#teacherMIds').val();		
	
	CandidatesEECDataAjax.displayRecordsByEthinicity(teachrIds,{ 
		async: true,
		callback: function(data)
		{
		
		
	var hisponiccountprcts				  	=	data[0];
	var nonhisponiccountprcts			    =	data[1];
    var declinecountprcts			        =	data[2];
    var noresponse							=   data[3];
    

   var overallhisponiccountprcts		    =	data[4];
   var overallnonhisponiccountprcts			=	data[5];
   var overalldeclinecountprcts	   		    =	data[6];
   var overallnoresponsecountprcts	   	    =	data[7];
 
		
		
		
		var  categories = [resourceJSON.msgHispanicLatino, resourceJSON.msgNotHispanicLatino, resourceJSON.lblDeclineAnswer,resourceJSON.lblNoResponse],
        name = 'Ethnicity Stats',
        data = [{
                y: hisponiccountprcts,
                color: '#ff0000',
                drilldown: {
                    name: resourceJSON.msgHispanicLatino,
                    categories: [resourceJSON.msgHispanicLatino],
                    data: [overallhisponiccountprcts],
                    color: '#ff0000'
                }
            }, {
                y: nonhisponiccountprcts,
                color: '#ffd700',
                drilldown: {
                    name: resourceJSON.msgNotHispanicLatino,
                    categories: [resourceJSON.msgNotHispanicLatino],
                    data: [overallnonhisponiccountprcts],
                    color: '#ffd700'
                }
            }, {
                y: declinecountprcts,
                color: "#8bbc21",
                drilldown: {
                    name: resourceJSON.lblDeclineAnswer,
                    categories: [resourceJSON.lblDeclineAnswer],
                    data: [overalldeclinecountprcts],
                    color: "#8bbc21"
                }           
            },{
                y: noresponse,
                color: "#7cb5ec",
                drilldown: {
                    name: resourceJSON.lblNoResponse,
                    categories: [resourceJSON.lblNoResponse],
                    data: [overallnoresponsecountprcts],
                    color: "#7cb5ec"
                }           
            },  
            
            ];

    // Build the data arrays
    var browserData = [];
    var versionsData = [];
    for (var i = 0; i < data.length; i++) {
        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        for (var j = 0; j < data[i].drilldown.data.length; j++) {
            var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
               // color: Highcharts.Color(data[i].color).brighten(brightness).get()
			    color: data[i].color
            });
        }
    }
		
	// Create the chart
    var chartOptions = {
	    	chart: {
	        type: 'pie',
	        renderTo: 'ethnicitycontainer',
	        events: {click:function () {
	            hs.htmlExpand(document.getElementById(chartOptions.chart.renderTo), {
	                width: 9999,
	                height: 9999,
	                allowWidthReduction: true,
	                preserveContent: false
	            }, {
	                chartOptions: chartOptions
	            });
	        }}
	    },
	    exporting: {
            enabled: false
        },
        title: {
            text: 'Ethnicity Stats',
            align: "left",
            style:tmstyle
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['53%', '47%']
            }
        },
        tooltip: {
            valueSuffix: '% ',
            style: tmstyle
        },
        credits: {
            enabled: false
        },
        series: [{
            name: resourceJSON.msgCAndWithinDist,
            data: browserData,
            size: '60%',
            style:tmstyle,
            dataLabels: {
        	  enabled: false,
                formatter: function() {
                    return this.y > 5 ? this.point.name : null;
                },
                color: 'white',
                distance: -30
            }
        }, {
            name: 'All Candidates',
            data: versionsData,
            style:tmstyle,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function() {
                    // display only if larger than 1
                    return this.y >=0 ? ''+ this.point.name +': '+ this.y +'%'  : null;
                },style:tmstyle
            }
        }]
    };
    
    var chart = new Highcharts.Chart(chartOptions);
			},
	errorHandler:handleError
	});


}
//race
function generateRaceChart()
{
	
	$("#racecontainer").html("<div class='img-responsive' style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	var teachrIds=$('#teacherMIds').val();	
	CandidatesEECDataAjax.displayRecordsByRaces(teachrIds,{ 
		async: true,
		callback: function(data)
		{
		
	    var noresponse      =   data[0];
		var black   		=	data[1];
		var asian			=	data[2];
		var american 	    =	data[3];
		var hispanic		=	data[4];
		var white 			=   data[5];
		var decline			=	data[6];
		var nativeHawaiian  =   data[7];
		
		var noresponseAll   =   data[8];
		var blackAll   		=	data[9];
		var asianAll		=	data[10];
		var americanAll 	=	data[11];
		var hispanicAll		=	data[12];
		var whiteAll 	    =   data[13];
		var declineAll		=	data[14];
		var nativeHawaiianAll = data[15];
 
		
		
		
		var  categories = [resourceJSON.lblNoResponse, resourceJSON.lblBlackNotHis, resourceJSON.lblAsianorPacific,resourceJSON.lblAmericanIND,resourceJSON.lblHispanic,resourceJSON.lblWhiteNotHis,resourceJSON.lblDeclineAnswer, resourceJSON.lblNativeHawaiian],
        name = resourceJSON.lblRaceStat,
        data = [{
                y: noresponse,
                color: '#7cb5ec',
                drilldown: {
                    name: resourceJSON.lblNoResponse,
                    categories: ['"+resourceJSON.lblNoResponse+"'],
                    data: [noresponseAll],
                    color: '#7cb5ec'
                }
            }, {
                y: black,
                color: '#ffd700',
                drilldown: {
                    name: resourceJSON.lblBlackNotHis,
                    categories: [resourceJSON.lblBlackNotHis],
                    data: [blackAll],
                    color: '#ffd700'
                }
            }, {
                y: asian,
                color: "#8bbc21",
                drilldown: {
                    name: resourceJSON.lblAsianorPacific,
                    categories: [resourceJSON.lblAsianorPacific],
                    data: [asianAll],
                    color: "#8bbc21"
                } //mukesh color          
            }, {
                y: american,
                color: "#ff0000",
                drilldown: {
                    name: resourceJSON.lblAmericanIND,
                    categories: [resourceJSON.lblAmericanIND],
                    data: [americanAll],
                    color: "#ff0000"
                }           
            },{
                y: hispanic,
                color: "#434348",
                drilldown: {
                    name: resourceJSON.lblHispanic,
                    categories: [resourceJSON.lblHispanic],
                    data: [hispanicAll],
                    color: "#434348"
                }           
            },{
                y: white,
                color: "#90ed7d",
                drilldown: {
                    name: resourceJSON.lblWhiteNotHis,
                    categories: [resourceJSON.lblWhiteNotHis],
                    data: [whiteAll],
                    color: "#90ed7d"
                }           
            },{
                y: decline,
                color: "#f7a35c",
                drilldown: {
                    name: resourceJSON.lblDeclineAnswer,
                    categories: [resourceJSON.lblDeclineAnswer],
                    data: [declineAll],
                    color: "#f7a35c"
                }           
            },{
                y: nativeHawaiian,
                color: "#8085e9",
                drilldown: {
                    name: resourceJSON.lblNativeHawaiian,
                    categories: [resourceJSON.lblNativeHawaiian],
                    data: [nativeHawaiianAll],
                    color: "#8085e9"
                }           
            },
            
            ];

    // Build the data arrays
    var browserData = [];
    var versionsData = [];
    for (var i = 0; i < data.length; i++) {
        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        for (var j = 0; j < data[i].drilldown.data.length; j++) {
            var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
               // color: Highcharts.Color(data[i].color).brighten(brightness).get()
			    color: data[i].color
            });
        }
    }
		
	// Create the chart
    var chartOptions = {
	    	chart: {
	        type: 'pie',
	        renderTo: 'racecontainer',
	        events: {click:function () {
	            hs.htmlExpand(document.getElementById(chartOptions.chart.renderTo), {
	                width: 9999,
	                height: 9999,
	                allowWidthReduction: true,
	                preserveContent: false
	            }, {
	                chartOptions: chartOptions
	            });
	        }}
	    },
	    exporting: {
            enabled: false
        },
        title: {
            text: resourceJSON.lblRaceStat ,
            align: "left",
            style:tmstyle
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['53%', '47%']
            }
        },
        tooltip: {
            valueSuffix: '% ',
            style: tmstyle
        },
        credits: {
            enabled: false
        },
        series: [{
            name:  resourceJSON.msgCAndWithinDist ,
            data: browserData,
            size: '60%',
            style:tmstyle,
            dataLabels: {
        	 enabled: false,
                formatter: function() {
                    return this.y > 5 ? this.point.name : null;
                },
                color: 'white',
                distance: -30
            }
        }, {
            name: 'All Candidates',
            data: versionsData,
            style:tmstyle,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function() {
                    // display only if larger than 1
                    return this.y >=0 ? ''+ this.point.name +': '+ this.y +'%'  : null;
                },style:tmstyle
            }
        }]
    };
    
    var chart = new Highcharts.Chart(chartOptions);
	  },
	errorHandler:handleError
	});


}
function generateEthnicOriginChart()
{
	
	$("#ethnicorigincontainer").html("<div class='img-responsive' style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	var teachrIds=$('#teacherMIds').val();
	CandidatesEECDataAjax.displayRecordsByEthinicOrigin(teachrIds,{ 
		async: true,
		callback: function(data)
		{
		
		var aa1			=	data[0];
		var bb2			=	data[1];
	    var cc3			=	data[2];
	    var dd4		    =	data[3];
	    var ee5			=	data[4];
	    var ff6	   		=	data[5];
	    var gg7		    =	data[6];
	    var hh8			=	data[7];
	    var D19	   		=	data[8];
	    var NN			=   data[9];
	    
	    var pp1			=	data[10];
		var qq2			=	data[11];
	    var rr3			=	data[12];
	    var ss4		    =	data[13];
	    var tt5			=	data[14];
	    var uu6	   		=	data[15];
	    var vv7		    =	data[16];
	    var ww8			=	data[17];
	    var D29	   		=	data[18];
	    var NN1	   		=	data[19];
	    
	    
		var  categories = [msgWhHispanicorigin, resourceJSON.msgBkHispanicorigin, resourceJSON.lblHispanic,resourceJSON.lblAsianorPacific,resourceJSON.lblAmericanIND,resourceJSON.msgNativeHawaiian,resourceJSON.msgHispanicWhiteOnly,resourceJSON.msgHispanicOther,resourceJSON.lblDeclineAnswer,resourceJSON.lblNoResponse],
        name = resourceJSON.msgEthnicOriginStats,
        data = [{
                y: aa1,
                color: '#ff0000',
                drilldown: {
                    name: msgWhHispanicorigin,
                    categories: [msgWhHispanicorigin],
                    data: [pp1],
                    color: '#ff0000'
                }
            }, {
                y: bb2,
                color: '#ffd700',
                drilldown: {
                    name: resourceJSON.msgBkHispanicorigin,
                    categories: [resourceJSON.msgBkHispanicorigin],
                    data: [qq2],
                    color: '#ffd700'
                }
            }, {
                y: cc3,
                color: "#8bbc21",
                drilldown: {
                    name: resourceJSON.lblHispanic,
                    categories: [resourceJSON.lblHispanic],
                    data: [rr3],
                    color: "#8bbc21"
                }           
            }, {                                     //mukesh4
                y: dd4,
                color: "#50B432",
                drilldown: {
                    name: resourceJSON.lblAsianorPacific,
                    categories: [resourceJSON.lblAsianorPacific],
                    data: [ss4],
                    color: "#50B432"
                }           
            }, {                           
                y: ee5,
                color: "#ED561B",
                drilldown: {
                    name: resourceJSON.lblAmericanIND,
                    categories: [resourceJSON.lblAmericanIND],
                    data: [tt5],
                    color: "#ED561B"
                }           
            },{
                y: ff6,
                color: "#DDDF00",
                drilldown: {
                    name: resourceJSON.msgNativeHawaiian,
                    categories: [resourceJSON.msgNativeHawaiian],
                    data: [uu6],
                    color: "#DDDF00"
                }           
            },{
                y: gg7,
                color: "#24CBE5",
                drilldown: {
                    name: resourceJSON.msgHispanicWhiteOnly,
                    categories: [resourceJSON.msgHispanicWhiteOnly],
                    data: [vv7],
                    color: "#24CBE5"
                }           
            },{
                y: hh8,
                color: "#64E572",
                drilldown: {
                    name: resourceJSON.msgHispanicOther,
                    categories: [resourceJSON.msgHispanicOther],
                    data: [ww8],
                    color: "#64E572"
                }           
            },{
                y: D19,
                color: "#6AF9C4",
                drilldown: {
                    name: resourceJSON.lblDeclineAnswer,
                    categories: [resourceJSON.lblDeclineAnswer],
                    data: [D29],
                    color: "#6AF9C4"
                }           
            },{
                y: NN,
                color: "#7cb5ec",
                drilldown: {
                    name: resourceJSON.lblNoResponse,
                    categories: [resourceJSON.lblNoResponse],
                    data: [NN1],
                    color: "#7cb5ec"
                }           
            },
            
            ];

    // Build the data arrays
    var browserData = [];
    var versionsData = [];
    for (var i = 0; i < data.length; i++) {
        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        for (var j = 0; j < data[i].drilldown.data.length; j++) {
            var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
               // color: Highcharts.Color(data[i].color).brighten(brightness).get()
			    color: data[i].color
            });
        }
    }
		
	// Create the chart
    var chartOptions = {
	    	chart: {
	        type: 'pie',
	        renderTo: 'ethnicorigincontainer',
	        events: {click:function () {
	            hs.htmlExpand(document.getElementById(chartOptions.chart.renderTo), {
	                width: 9999,
	                height: 9999,
	                allowWidthReduction: true,
	                preserveContent: false
	            }, {
	                chartOptions: chartOptions
	            });
	        }}
	    },
	    exporting: {
            enabled: false
        },
        title: {
            text: resourceJSON.msgEthnicOriginStats,
            align: "left",
            style:tmstyle
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['53%', '47%']
            }
        },
        tooltip: {
            valueSuffix: '% ',
            style: tmstyle
        },
        credits: {
            enabled: false
        },
        series: [{
            name: resourceJSON.msgCAndWithinDist,
            data: browserData,
            size: '60%',
            style:tmstyle,
            dataLabels: {
        	 enabled: false,
                formatter: function() {
                    return this.y > 5 ? this.point.name : null;
                },
                color: 'white',
                distance: -30
            }
        }, {
            name: 'All Candidates',
            data: versionsData,
            style:tmstyle,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function() {
                    // display only if larger than 1
                    return this.y >=0 ? ''+ this.point.name +': '+ this.y +'%'  : null;
                },style:tmstyle
            }
        }]
    };
    
    var chart = new Highcharts.Chart(chartOptions);
			},
	errorHandler:handleError
	});

}
 
 
 function canelPrintEECO()
 {
	 $('#printEEOCDiv').hide();
	 $('#loadingDiv').hide();
		
 }

 function chkschoolBydistrict()
 {
 	
 	var districtid = trim(document.getElementById('districtORSchoolName').value);
 	
 	 
 	if($('#entityType').val()==1)
 	if(districtid=="")
 	{
 		document.getElementById('schoolName').disabled=true;
 		
 	}
 	else
 	{
 		document.getElementById('schoolName').disabled=false;
 		
 	}
 }

 










