var deviceTypeAndroid = $.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i
		.test(navigator.userAgent.toLowerCase()));
var deviceType = $.browser.device = (/iPhone|iPad|iPod/i
		.test(navigator.userAgent.toLowerCase()));
var isSafari = /Safari/.test(navigator.userAgent)
		&& /Apple Computer/.test(navigator.vendor);

var page = 1;
var noOfRows = 10;
var sortOrderStr = "";
var sortOrderType = "";
var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr = "";
var domainsortOrderType = "";

var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();

var index=-1;
var length=0;

var divid='';
var txtid='';
var hiddenId="";

function handleError(message, exception) {
	if (exception.javaClassName == "java.lang.IllegalStateException") {
		alert(resourceJSON.oops);
		document.location = 'signin.do';
	} else {
		alert(resourceJSON.msgSomeerror+": " + exception.javaClassName);
	}
}

function getPaging(pageno) {
	if (pageno != '') {
		page = pageno;
	} else {
		page = 1;
	}

	noOfRows = document.getElementById("pageSize").value;
	displayEventGrids();
}

function getPagingAndSorting(pageno, sortOrder, sortOrderTyp) {
	
	var gridNo = document.getElementById("gridNo").value;

	if (gridNo == 3) {
		if (pageno != '') {
			domainpage = pageno;
		} else {
			domainpage = 1;
		}
		domainsortOrderStr = sortOrder;
		domainsortOrderType = sortOrderTyp;
		if (document.getElementById("pageSize3") != null) {
			domainnoOfRows = document.getElementById("pageSize3").value;
		} else {
			domainnoOfRows = 10;
		}
		displayEventGrids();
	} else if (gridNo == 1) {
		if (pageno != '') {
			page = pageno;
		} else {
			page = 1;
		}
		sortOrderStr = sortOrder;
		sortOrderType = sortOrderTyp;
		if (document.getElementById("pageSize") != null) {
			noOfRows = document.getElementById("pageSize").value;
		} else {
			noOfRows = 10;
		}
		displayEventGrids();
	} else {

		if (pageno != '') {
			page = pageno;
		} else {
			page = 1;
		}

		sortOrderStr = sortOrder;
		sortOrderType = sortOrderTyp;

		if (document.getElementById("pageSize") != null && document.getElementById("pageSize") != "") {
			
			noOfRows = document.getElementById("pageSize").value;

		} else {
			noOfRows = 10;
		}
		displayEventGrids();
	}
}
function addEvent() 
{
	$("#maineventtableDiv").hide();
	$("#xportdataPdfExlPrint").hide();
	$("#eventsearchDiv").hide();
	$("#eventsearchDivDistrictSearch").hide();
	$("#eventsearchDiv1").hide();
	$('#evtsch').tooltip();
	$('#evtType').tooltip();
	
	$('#tooltip1').tooltip();
	$('#tooltip2').tooltip();
	
	clrErrColor();
	$("#questionSet").hide();
	$('textarea').jqte();
	document.getElementById('interactionType').disabled = false;
	// getTemplatesByDistrictId();
	// getEmailTemplatesByDistrictId();
	clrEventDiv();
	$('#descriptiontext').find(".jqte_editor").html('');
	$('#messagetext').find(".jqte_editor").html('');
	$('#facilitatorsmessagetext').find(".jqte_editor").html('');
	$('#errordiv').empty();
	var entity = document.getElementById("entityTypeId").value;

	$("#addeditevent").show();
	if (entity == "1") {
		document.getElementById("districtName").value = "";
		document.getElementById("districtId").value = "";
		$('#districtName').focus();
	}else if(entity=="5"){
		document.getElementById("branchName").value = "";
		document.getElementById("branchId").value = "";
		$('#branchName').focus();
	}
	else {
		$('#eventtitle').focus();
	}

	// document.getElementById("quesId").value=""("Select District for relevant
	// Question Set list");
	// document.getElementById("quesName").value="";

	$('#eventUrl').html("");
	$('#qrcode').html("");
}

function cancelEvent() {
	$("#maineventtableDiv").show();
	$("#xportdataPdfExlPrint").show();
	$("#eventsearchDiv").show();
	$("#eventsearchDivDistrictSearch").show();
	$("#eventsearchDiv1").show();
	$('#errordiv').empty();
	$("#addeditevent").hide();
}

function clrEventDiv() {
	
	$("#scheduleTypeDiv").hide();
	$("#msgtoparticipantdiv").show();
	$("#messagetoprincipleDiv").show();
	$("#msgtofacilitatorsDiv1").show();
	$("#msgtofacilitatorsDiv2").show();
	$("#subjectforFacilatorDiv").show();
	$("#subjectDiv").show();
	$("#desDiv").show();
	$("#logoDiv").show();
	
	document.getElementById("logo").value = "";
	document.getElementById("sendmessage").checked = false;
	document.getElementById("eventId").value = "";
	document.getElementById("eventtitle").value = "";
	document.getElementById("interactionType").value = "";
	
	
	//document.getElementById("intschedule").innerHTML = "</br><b style='color: red;'>Schedule Type will be appear after selection of Type of Event.</b>";
	
	document.getElementById('subjectforFacilator').value = "";
	document.getElementById('subjectforParticipants').value = "";
	document.getElementById("msgtofacilitators").value = '';
	document.getElementById("messagetoprinciple").value = '';
	document.getElementById("description").value = '';
	if (document.getElementById("entityTypeId").value == '1')
		document.getElementById("quesId").innerHTML = resourceJSON.msgRelevantQuestionSet;

	$("#qrAndurlheading").hide();

}

function displayEventGrids() 
{
	$("#maineventtableDiv").show();
	$("#xportdataPdfExlPrint").show();
	$('#loadingDiv').fadeIn();
	
	var districtId=0;
	var headQuarterId=0;
	var branchId=0;
	var schoolId=0;
	
	var entityType=document.getElementById("entityTypeId").value;
	
	if(entityType==5 || entityType==6){
		headQuarterId=document.getElementById("headQuarterId").value;	
		branchId=document.getElementById("branchIdForSearch").value;	
	}else{
	   districtId=document.getElementById("districtIdForSearch").value;	
	   schoolId=document.getElementById("schoolId").value;	
	}
    
    var eventName=document.getElementById("eventName").value;
    var eventTypeId=document.getElementById("interactionTypeForSearch").value;
    
		EventAjax.displayEvents(noOfRows, page, sortOrderStr, sortOrderType,districtId,eventName,eventTypeId,headQuarterId,branchId,schoolId,{
			async : true,
			callback : function(data) {
				
				$('#eventGrid').html(data);
				 applyScrollOnTblEvent();
				$("#loadingDiv").hide();
			},
			errorHandler : handleError
		});
}


function generateManageEventsEXL()
{
$('#loadingDiv').fadeIn();
	
	var districtId=0;
	var headQuarterId=0;
	var branchId=0;
	var schoolId=0;
	
	var entityType=document.getElementById("entityTypeId").value;
	
	if(entityType==5 || entityType==6){
		headQuarterId=document.getElementById("headQuarterId").value;	
		branchId=document.getElementById("branchIdForSearch").value;	
	}else{
	   districtId=document.getElementById("districtIdForSearch").value;	
	   schoolId=document.getElementById("schoolId").value;	
	}
    
    var eventName=document.getElementById("eventName").value;
    var eventTypeId=document.getElementById("interactionTypeForSearch").value;
    
		EventAjax.displayRecordsByEntityTypeEXL(noOfRows, page, sortOrderStr, sortOrderType,districtId,eventName,eventTypeId,headQuarterId,branchId,schoolId,{
			async : true,
			callback: function(data)
			{
			
			    $('#loadingDiv').hide();
				if(deviceType)
				{					
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
						
				}
				else
				{
					try
					{
						document.getElementById('ifrmTrans').src = "manageevents/"+data+"";
					}
					catch(e)
					{alert(e);}
				}
			},
			errorHandler : handleError
		});

}


function generateManageEventsPDF()
{
$('#loadingDiv').fadeIn();
	
	var districtId=0;
	var headQuarterId=0;
	var branchId=0;
	var schoolId=0;
	
	var entityType=document.getElementById("entityTypeId").value;
	
	if(entityType==5 || entityType==6){
		headQuarterId=document.getElementById("headQuarterId").value;	
		branchId=document.getElementById("branchIdForSearch").value;	
	}else{
	   districtId=document.getElementById("districtIdForSearch").value;	
	   schoolId=document.getElementById("schoolId").value;	
	}
    
    var eventName=document.getElementById("eventName").value;
    var eventTypeId=document.getElementById("interactionTypeForSearch").value;
    
		EventAjax.displayRecordsByEntityTypePDF(noOfRows, page, sortOrderStr, sortOrderType,districtId,eventName,eventTypeId,headQuarterId,branchId,schoolId,{
			async : true,
			callback: function(data)
			{		
			$('#loadingDiv').hide();	
			if(deviceType || deviceTypeAndroid)
			{
				window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
			}		
			else
			{
				$('#modalDownloadHiredCandidates').modal('hide');
				document.getElementById('ifrmCJS').src = ""+data+"";
				try{
					$('#modalDownloadHiredCandidates').modal('show');
				}catch(err)
				{}		
		     }		
		     return false;
	},
	errorHandler : handleError
		});
}


function generateManageEventsPrintPre()
{
	
	$('#loadingDiv').fadeIn();
	var districtId=0;
	var headQuarterId=0;
	var branchId=0;
	var schoolId=0;
	
	var entityType=document.getElementById("entityTypeId").value;
	
	if(entityType==5 || entityType==6){
		headQuarterId=document.getElementById("headQuarterId").value;	
		branchId=document.getElementById("branchIdForSearch").value;	
	}else{
	   districtId=document.getElementById("districtIdForSearch").value;	
	   schoolId=document.getElementById("schoolId").value;	
	}
    
    var eventName=document.getElementById("eventName").value;
    var eventTypeId=document.getElementById("interactionTypeForSearch").value;
    
    EventAjax.displayRecordsByEntityTypePrintPreview(noOfRows, page, sortOrderStr, sortOrderType,districtId,eventName,eventTypeId,headQuarterId,branchId,schoolId,{
		async: true,
		callback: function(data)
		{	
			$('#loadingDiv').hide();
			$('#printhiredApplicantsDataTableDiv').html(data);
			$("[data-toggle='tooltip']").tooltip();
			applyScrollOnTblEventPreview();
			$("#printHiredApplicantsDiv").modal('show');
		},
	errorHandler:handleError
	});
} 


var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
function printManageEventsDATA()
{
	
	$('#loadingDiv').fadeIn();
	var districtId=0;
	var headQuarterId=0;
	var branchId=0;
	var schoolId=0;
	
	var entityType=document.getElementById("entityTypeId").value;
	
	if(entityType==5 || entityType==6){
		headQuarterId=document.getElementById("headQuarterId").value;	
		branchId=document.getElementById("branchIdForSearch").value;	
	}else{
	   districtId=document.getElementById("districtIdForSearch").value;	
	   schoolId=document.getElementById("schoolId").value;	
	}
    
    var eventName=document.getElementById("eventName").value;
    var eventTypeId=document.getElementById("interactionTypeForSearch").value;
    
    EventAjax.displayRecordsByEntityTypePrintPreview(noOfRows, page, sortOrderStr, sortOrderType,districtId,eventName,eventTypeId,headQuarterId,branchId,schoolId,{
		async: true,
		callback: function(data)
		{	
			$('#loadingDiv').hide();
			try{ 
			if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				 } 
			}catch (e) 
			{
				$('#printmessage1').modal('show');							 
			}
			
		},
	errorHandler:handleError
	});
}

function getEventType() {
	try {

		EventAjax.getEventType( {
			async : true,
			callback : function(data) {
				$('#interactionType').html(data);
				$('#interactionTypeForSearch').html(data);
			},
			errorHandler : handleError
		});
	} catch (e) {
		alert(e);
	}
}
function getEventSchedule(evntType) {
	try {

		EventAjax.getEventSchedule(evntType, {
			async : true,
			callback : function(data) {
				$('#intschedule').html(data);
			},
			errorHandler : handleError
		});
	} catch (e) {
		alert(e);
	}

}

function checkDuplicacy(savecont) {
	saveEvent(savecont);
	/*
	var districtId = document.getElementById("districtId").value;
	var eventId = document.getElementById("eventId").value;
	var title = document.getElementById("eventtitle").value;
	EventAjax
			.checkDuplicacy(
					title,
					eventId,
					districtId,
					{
						async : true,
						callback : function(data) {
							if (data == true) {
								$('#errordiv')
										.append(
												"&#149; Event Name already exist for this district<br>");
								$('#errordiv').show();
							} else {
								saveEvent(savecont)
							}
						}
					});
*/}
function saveEvent(savecont) {
	$('#errordiv').empty();
	var counter = 0;
	var eventlogo = document.getElementById("logo").value;
	eventlogo = eventlogo.replace(/.*(\/|\\)/, '');
	var districtId = document.getElementById("districtId").value;
	
	var headQuarterId = document.getElementById("headQuarterId").value;
	var branchId = document.getElementById("branchId").value;
	
	
	var eventId = document.getElementById("eventId").value;
	var title = trim(document.getElementById("eventtitle").value);
	var interactionType = document.getElementById("interactionType").value;
	//var formatEvent = document.getElementById("formatEvent").value;
	//var channel = document.getElementById("channel").value;
	//var intschedule = document.getElementById("intscheduleId").value;
	
	var intschedule_Id = document.getElementsByName('intscheduleId');
	
	var intschedule="";
	for(var i = 0; i < intschedule_Id.length; i++){
	    if(intschedule_Id[i].checked){
	        intschedule = intschedule_Id[i].value;
	    }
	}
	
	//alert("intschedule :: "+intschedule);
	
	var messtofacilitators = $('#facilitatorsmessagetext').find(".jqte_editor").html();

	var messtoparticipant = $('#messagetext').find(".jqte_editor").html();
	var description = $('#descriptiontext').find(".jqte_editor").html();
	var messagetoprinciple = document.getElementById("messagetoprinciple").value;
	var quesId = "";
	var sendmessage = 0;
	if (document.getElementById("sendmessage").checked) {
		sendmessage = 1;
	}

	// --Validation---------
	var entityType=document.getElementById("entityTypeId").value;
	
	
	if(entityType=='1' || entityType=='2')
	if (districtId == null || districtId == '') {
		counter = counter + 1;
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		if (counter == 1)
			$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	}
	if (title == null || title == '') {
		counter = counter + 1;
		$('#errordiv').append("&#149; "+resourceJSON.msgEventName+"<br>");
		if (counter == 1)
			$('#eventtitle').focus();
		$('#eventtitle').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	}
	if (interactionType == null || interactionType == '') {
		counter = counter + 1;
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseSelectTypeEvent+"<br>");
		if (counter == 1)
			$('#interactionType').focus();
		$('#interactionType').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	}
	
	if (districtId != "") {
		if (interactionType == 1) {
			quesId = document.getElementById("quesId").value;

			if (quesId == "") {
				counter = counter + 1;
				$('#errordiv').append("&#149; "+resourceJSON.msgPleaseQuestionSet+"<br>");
				if (counter == 1)
					$('#quesId').focus();
				$('#quesId').css("background-color", "#F5E7E1");
				$('#errordiv').show();
			}
		}
	}
	
	if (intschedule == null || intschedule == '') {
		counter = counter + 1;
		$('#errordiv').append("&#149; "+resourceJSON.msgScheduleType+"<br>");
		if (counter == 1)
			$('#intscheduleId').focus();
		$('#intscheduleId').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	}

	

	var logodiscriptionflag = false;
	if (interactionType == 3 || interactionType == 4) {
		logodiscriptionflag = true;
		// alert(interactionType+" @@ " )

	}
	
	var fileSize = 0;

	if (logodiscriptionflag) {
		
		if (eventId == null || eventId == '') {
			if (eventlogo == null || eventlogo == '') {
				counter = counter + 1;
				$('#errordiv').append("&#149; "+resourceJSON.msgPleaseUploadLogo+"<br>");
				if (counter == 1)
					$('#logo').focus();
				$('#logo').css("background-color", "#F5E7E1");
				$('#errordiv').show();
			}
		}
		
		if ($.browser.msie == true) {
			fileSize = 0;
		} else {
			if (logo.files[0] != undefined)
				fileSize = logo.files[0].size;
		}
		if (fileSize >= 10485760) {
			counter = counter + 1;
			$('#errordiv').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
			if (counter == 1)
				$('#logo').focus();
			$('#logo').css("background-color", "#F5E7E1");
			$('#errordiv').show();
		//	return false;
		}
		
		
		if (description == null || description == '') {
			counter = counter + 1;
			$('#errordiv').append("&#149; "+resourceJSON.msgPleaseEnterDescription+"<br>");
			if (counter == 1)
				$('#descriptiontext').focus();
			$('#errordiv').show();
		}
	}
	
	var subjectforParticipants = trim(document.getElementById('subjectforParticipants').value);

	var subjectforFacilator = trim(document.getElementById('subjectforFacilator').value);
	if (interactionType != 1) {
		
		if (subjectforParticipants == null || subjectforParticipants == '') {
			counter = counter + 1;
			$('#errordiv').append(
					"&#149; "+resourceJSON.msgSubjecttoCandidate+"<br>");
			if (counter == 1)
				$('#subjectforParticipants').focus();
			$('#subjectforParticipants').css("background-color", "#F5E7E1");
			$('#errordiv').show();
		}
		if (messtoparticipant == null || messtoparticipant == '') {
			counter = counter + 1;
			$('#errordiv').append(
					"&#149; "+resourceJSON.msgMessagetoCandidates+"<br>");
			if (counter == 1)
				$('#messagetext').focus();
			$('#errordiv').show();
		}
		
		if (subjectforFacilator == null || subjectforFacilator == '') {
			counter = counter + 1;
			$('#errordiv').append("&#149; "+resourceJSON.msgSubjecttoFacilitators+"<br>");
			if (counter == 1)
				$('#subjectforFacilator').focus();
			$('#subjectforFacilator').css("background-color", "#F5E7E1");
			$('#errordiv').show();
		}
		if (messtofacilitators == null || messtofacilitators == '') {
			counter = counter + 1;
			$('#errordiv').append(
					"&#149; "+resourceJSON.msgtofacilators+"<br>");
			if (counter == 1)
				$('#messagetext').focus();
			$('#errordiv').show();
		}
		
	} else {
		messtofacilitators = "";
		subjectforFacilator = "";
		messtoparticipant = "";
		subjectforParticipants = "";
	}

	// -----validation end--------
	if (counter == 0) {
		EventAjax
				.saveEvent(
						eventId,
						title,
						districtId,
						interactionType,
						/*formatEvent,
						channel,*/
						intschedule,
						messtoparticipant,
						description,
						messagetoprinciple,
						eventlogo,
						sendmessage,
						quesId,
						messtofacilitators,
						subjectforFacilator,subjectforParticipants,headQuarterId,branchId,
{
							async : true,
							callback : function(data) {
								if (data[1] == '1') {
									$('#errordiv').show();
									$('#errordiv')
											.append(
													"&#149; "+resourceJSON.msgQuestiondoesnotExistDistrict+"<br>");
								} else {
									uploadLogo(districtId, data[0],savecont,headQuarterId,branchId);
								}
							}
						});
	}
}

// ------uploadfile-----------
function uploadLogo(districtId, eventId, savecont,headQuarterId,branchId) {
	url = "./eventLogoUploadServlet.do";
	var file_data = $("#logo").prop("files")[0];
	var form_data = new FormData();
	// form_data.append("dateString", dateString)
	form_data.append("districtId", districtId)
	form_data.append("headQuarterId", headQuarterId)
	form_data.append("branchId", branchId)
	form_data.append("eventId", eventId)
	form_data.append("file", file_data)

	$.ajax( {
		url : url,
		dataType : 'text',
		cache : false,
		contentType : false,
		processData : false,
		data : form_data,
		type : 'post',
		success : function(response, data123, xhr)
		{
			if(response!=null && response!="")
				fileContainsVirusDiv(response);
			else
			if (savecont == 0)
			{
				$("#addeditevent").hide();
				$("#maineventtableDiv").show();
				$("#xportdataPdfExlPrint").show();
				$('#eventsearchDiv').show();
				clrEventDiv();
				
				document.getElementById("eventName").value='';
			    document.getElementById("interactionTypeForSearch").value='';
				
				displayEventGrids();
				cancelEvent();
			}
			else
			{
				window.location.href = "eventschedule.do?eventId="+ eventId;
			}
		}
	})
}

// ---------------------------


/**************************************************************************************************************************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtNameForSearch").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	/*====  District Auto Complete will show for each case ========*/
	EventAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="") {

			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;

				if(count==10)
					break;
			}
		} else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}

		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtIdForSearch").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			$('#schoolName').attr('readonly', true);
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
		} else if(showDataArray && showDataArray[index]) {
			dis.value=showDataArray[index];
			document.getElementById("districtIdForSearch").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	} else {
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}

	if(document.getElementById(divId)) {
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*---------------------------------School Auto Complete Js Starts ----------------------------------------------------------------*/
function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	
	var districtId	=	document.getElementById("districtIdForSearch").value;
		BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*--------------------------------- End  School Auto Complete Js  ----------------------------------------------------------------*/


var downArrowKey = function(txtdivid) {
	if (txtdivid) {
		if (index < length - 1) {
			for ( var i = 0; i < 10; i++) {
				{
					if (document.getElementById('divResult' + txtdivid + i))
						var div_id = document.getElementById('divResult'
								+ txtdivid + i);
					if (div_id) {
						if (div_id.className == 'over')
							index = div_id.id.split('divResult' + txtdivid)[1];
						div_id.className = 'normal';
					}
				}
			}
			index++;
			if (document.getElementById('divResult' + txtdivid + index)) {
				var div_id = document.getElementById('divResult' + txtdivid
						+ index);
				div_id.className = 'over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst = div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid) {
	if (txtdivid) {
		if (index > 0) {
			for ( var i = 0; i < length; i++) {

				var div_id = document
						.getElementById('divResult' + txtdivid + i);
				if (div_id) {
					if (div_id.className == 'over')
						index = div_id.id.split('divResult' + txtdivid)[1];
					div_id.className = 'normal';
				}
			}
			index--;
			if (document.getElementById('divResult' + txtdivid + index))
				document.getElementById('divResult' + txtdivid + index).className = 'over';
			if (txtid
					&& document.getElementById('divResult' + txtdivid + index)) {
				document.getElementById(txtid).value = document
						.getElementById('divResult' + txtdivid + index).innerHTML;
				selectFirst = document.getElementById('divResult' + txtdivid
						+ index).innerHTML;
			}
		}
	}
}

var overText = function(div_value, txtdivid) {

	for ( var i = 0; i < length; i++) {
		if (document.getElementById('divResult' + i)) {
			var div_id = document.getElementById('divResult' + i);

			if (div_id.className == 'over')
				index = div_id.id.split(txtdivid)[1];
			div_id.className = 'normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value = div_value.innerHTML;
}


function trim(s) {
	while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n')
			|| (s.substring(0, 1) == '\r')) {
		s = s.substring(1, s.length);
	}
	while ((s.substring(s.length - 1, s.length) == ' ')
			|| (s.substring(s.length - 1, s.length) == '\n')
			|| (s.substring(s.length - 1, s.length) == '\r')) {
		s = s.substring(0, s.length - 1);
	}
	return s;
}

function mouseOverChk(txtdivid, txtboxId) {
	for ( var i = 0; i < length; i++) {
		$('#divResult' + txtdivid + i).mouseover( {
			param1 : i,
			param2 : txtdivid,
			param3 : txtboxId
		}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event) {
	for ( var i = 0; i < length; i++) {
		document.getElementById('divResult' + event.data.param2 + i).className = 'normal';
	}
	document
			.getElementById('divResult' + event.data.param2 + event.data.param1).className = 'over';
	document.getElementById(event.data.param3).value = $(
			'#divResult' + event.data.param2 + event.data.param1).text();
	index = event.data.param1;
}

// ----------------------------------------
// --------getTemplate---------------------
function getTemplatesByDistrictId() {
	
	var distictId = document.getElementById('districtId').value;
	var headQuarterId = document.getElementById("headQuarterId").value;
	var branchId = document.getElementById("branchId").value;
	
		EventAjax.getTemplatesByDistrictId(distictId,headQuarterId,branchId,{
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				// alert(" data : "+data+" Length : "+data.length);
			if (data != "" || data.length > 0) {
				$("#selectdesc").show();
				$("#description").html(data);
			} else {
				$("#selectdesc").hide();
			}
		}
		});
}
// ----------------------------------------
function getDescription() {
	var descId = document.getElementById('description').value;
	if (descId != null && descId != '') {
		EventAjax.getTemplateDescription(descId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				$('#descriptiontext').find(".jqte_editor").html(data);
			}
		});
	}
}
// -----------------Get Email Template for Candiadtes------------------
function getEmailTemplatesByDistrictId() {
	var districtId=	document.getElementById('districtId').value;
	
	var headQuarterId = document.getElementById("headQuarterId").value;
	var branchId = document.getElementById("branchId").value;
	try {
		EventAjax.getEventEmailMessageTemp(districtId,headQuarterId, branchId,{
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				// alert(" data : "+data+" Length : "+data.length);
			if (data != "" || data.length > 0) {
				$("#messagetoprincipleDiv").show();
				$("#messagetoprinciple").html(data);
			} else {
				$("#msgtopart").hide();
				$("#messagetoprincipleDiv").hide();
			}
		}
		});
	} catch (e) {
		alert("e=" + e)
	}
}
function getEmailDescription() {
	var descId = document.getElementById('messagetoprinciple').value;
	if (descId != null && descId != '') {
		EventAjax.getEventEmailMessageTempBody(descId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				$('#messagetext').find(".jqte_editor").html(
						data.split("@@@@####@@@@")[0]);
				document.getElementById('subjectforParticipants').value = data
						.split("@@@@####@@@@")[1];
			}
		});
	} else {
		$('#messagetext').find(".jqte_editor").html("");
		document.getElementById('subjectforParticipants').value = "";
	}
}
// -----------------------------------------------------

function checkType(param) {
	$("#scheduleTypeDiv").show();
	var inttype = document.getElementById("interactionType").value;
	if (param == 0) {
		if(inttype=='')
		 $("#scheduleTypeDiv").hide();
		getEventSchedule(inttype);
	}
	//getEventFormat(inttype);
	//getEventSchedule(inttype);
	//getEventChannel(inttype)
	if (inttype == "1") {
		if(inttype == "1")
			$("#questionSet").show();
		else
			$("#questionSet").hide();
		
		$("#msgtoparticipantdiv").hide();
		$("#messagetoprincipleDiv").hide();
		$("#msgtofacilitatorsDiv2").hide();
		$("#msgtofacilitatorsDiv1").hide();
		$("#subjectforFacilatorDiv").hide();
		$("#subjectDiv").hide();
		$("#desDiv").hide();
		$("#logoDiv").hide();
		$("#selectdesc").hide();
		
		getQuestionSetByDistrict();
	}else if(inttype!="3" && inttype!="4" && inttype!="1"){
		//$("#selectdesc").show();
		$("#questionSet").hide();
		$("#msgtoparticipantdiv").show();
		$("#messagetoprincipleDiv").show();
		$("#msgtofacilitatorsDiv1").show();
		$("#msgtofacilitatorsDiv2").show();
		$("#subjectforFacilatorDiv").show();
		$("#subjectDiv").show();
		$("#desDiv").hide();
		$("#logoDiv").hide();
		$("#selectdesc").hide();
		
	}else {
		//$("#selectdesc").show();
		$("#questionSet").hide();
		$("#msgtoparticipantdiv").show();
		$("#messagetoprincipleDiv").show();
		$("#msgtofacilitatorsDiv1").show();
		$("#msgtofacilitatorsDiv2").show();
		$("#subjectforFacilatorDiv").show();
		$("#subjectDiv").show();
		$("#desDiv").show();
		
		$("#selectdesc").show();
		$("#logoDiv").show();
	}
	
	
}

function showEditEvent(eventId) {
	clrErrColor();
	$("#maineventtableDiv").hide();
	$("#xportdataPdfExlPrint").hide();
	$("#eventsearchDiv").hide();
	$("#eventsearchDivDistrictSearch").hide();
	$("#eventsearchDiv1").hide();
	$('#evtsch').tooltip();
	$('#evtType').tooltip();
	
	$('#tooltip1').tooltip();
	$('#tooltip2').tooltip();
	
	var entity = document.getElementById("entityTypeId").value;
	$('#errordiv').empty();
	$("#loadingDiv").show();
	if(entity=='5'){
		document.getElementById("branchName").value = "";
		document.getElementById("branchId").value = "";
	}
	
	EventAjax.getEventById(eventId, {
		async : true,
		callback : function(data) {
			$("#addeditevent").show();
			$("#loadingDiv").hide();

			var editdata = data[0].split('||||');
			var eventdetail=data[1];
			
			document.getElementById('eventId').value = eventId;
			document.getElementById('eventtitle').value = editdata[0];
			document.getElementById('interactionType').value = editdata[1];
			document.getElementById('interactionType').disabled = true;
			
			checkType(1);
			
		    $("#intschedule").html(editdata[2]);

			if (entity == '1') {
				document.getElementById("districtName").value = editdata[4];
				document.getElementById("districtId").value = editdata[3];
			}if(entity == '5'){
				if(eventdetail.branchMaster!=null){
					document.getElementById("branchName").value = eventdetail.branchMaster.branchName;
					document.getElementById("branchId").value = eventdetail.branchMaster.branchId;
				}
			}
			
			$('textarea').jqte();
			$('#descriptiontext').find(".jqte_editor").html(editdata[5]);
			$('#messagetext').find(".jqte_editor").html(editdata[6]);

			$("#addeditevent").show();
			$('#eventUrl').html(editdata[7]);
			$('#qrcode').html(editdata[8]);

			if (editdata[1] == 1) {
				if (editdata[3] > 0) {
					getQuestionSetByDistrict();
					document.getElementById("quesId").value = editdata[9];
					// document.getElementById("quesName").value=editdata[12];
				}
			}

		$('#facilitatorsmessagetext').find(".jqte_editor").html(editdata[11]);
		document.getElementById('subjectforFacilator').value = editdata[12];
		document.getElementById('subjectforParticipants').value = editdata[13];
		
		if (editdata[1] == 3) {
			$('#qrAndurlheading').show();
			$('#qrAndurlheading').html(resourceJSON.msgRegistrationQRCodeURL);
		} else {
			$('#qrAndurlheading').hide();
		}
	},
	 errorHandler : handleError
	});
}

function printPreEventDetails(eventId) {
	document.getElementById('eventIdM').value = eventId;
	$('#loadingDiv').fadeIn();
	EventAjax.printEventDetails(eventId, {
		async : true,
		callback : function(data) {
			$('#loadingDiv').hide();
			$('#pritEventDataTableDiv').html(data);
			$("#printEventDiv").modal('show');
		},
		errorHandler : handleError
	});
}

function printEventDetails() {
	$('#loadingDiv').fadeIn();
	EventAjax.printEventDetails(document.getElementById('eventIdM').value, {
		async : true,
		callback : function(data) {
			$('#loadingDiv').hide();
			try {
				if (isSafari && !deviceType) {
					window.document.write(data);
					window.print();
				} else {
					var newWindow = window.open();
					newWindow.document.write(data);
					newWindow.print();
				}
			} catch (e) {
				$('#printmessage1').modal('show');
			}
		},
		errorHandler : handleError
	});
}

function canelPrint() {
	//$('#printEventDiv').hide();
	$('#printmessage1').hide();
	$('#loadingDiv').hide();
}

function getDistrictEventTemplate() {
	
	var distictId = document.getElementById('districtId').value;

	getTemplatesByDistrictId();
	
	if (distictId != "0" && distictId != '') {
		if(document.getElementById('interactionType').value!='1')
			getEmailTemplatesFacilitatorsByDistrict();
			

	} else {
		$("#msgtofacilitatorsDiv1").hide();
		//$("#selectdesc").hide();
		// $("#messagetoprinciple").html("<select id='messagetoprinciple'
		// onchange='getEmailDescription()'><option value=''>Select
		// Message</option></select>");
	}
}




function getEmailTemplatesFacilitatorsByDistrict() {
	try {
		
		var districtId=	document.getElementById('districtId').value;
		var headQuarterId = document.getElementById("headQuarterId").value;
		var branchId = document.getElementById("branchId").value;
		//alert("districtId :: "+districtId+" headQuarterId :: "+headQuarterId +" branchId :: "+branchId)
		EventAjax.getEmailMessageTempFacilitators(districtId,headQuarterId,branchId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				// alert(" data : "+data+" Length : "+data.length);
			if (data != "" && data.length > 1) {
				$("#msgtofacilitatorsDiv1").show();
				$("#msgtofacilitators").html(data);
			} else {
				$("#msgtofacilitatorsDiv1").hide();
			}
		}
		});
	} catch (e) {
		alert("e=" + e)
	}
}

function getEmailDescription1() {
	var descId = document.getElementById('msgtofacilitators').value;
	if (descId != null && descId != '') {
		EventAjax.getFacilitatorsEmailMessageTempBody(descId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				// alert(data.split("@@@@####@@@@")[0])
			$('#facilitatorsmessagetext').find(".jqte_editor").html(
					data.split("@@@@####@@@@")[0]);
			document.getElementById('subjectforFacilator').value = data
					.split("@@@@####@@@@")[1];
			// alert("BBB "+data.split("@@@@####@@@@")[1]);
		}
		});
	} else {
		$('#facilitatorsmessagetext').find(".jqte_editor").html("");
		document.getElementById('subjectforFacilator').value = "";
	}
}
/*************************************************************************************************/

function showVirtualVideoInterview(evtParticipantId) {
	$("#loadingDiv").fadeIn();
	if (evtParticipantId != "") {
		EventAjax.showVirtualVideoInterview(evtParticipantId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				$("#vviDiv").modal('show');
				document.getElementById("interViewDiv").innerHTML = data[0];
				setTimeout(function() {
					$("#loadingDiv").hide();
				}, 5000);
			}
		});
	}
}

function getQuestionSetByDistrict() {
	var districtId = $("#districtId").val();
	var eventId = $("#eventId").val();
	var inttype = document.getElementById("interactionType").value;

	if (inttype == "1") {
		if (districtId != "") {
			I4QuestionSetAjax.getQuestionSetByDistrict(districtId, eventId, {
				async : true,
				errorHandler : handleError,
				callback : function(data) {
					document.getElementById("quesId").innerHTML = data;
				}
			});
		}
	}
}

function clrErrColor()
{
	$('#branchName').css("background-color", "");
	$('#districtName').css("background-color", "");
	$('#eventtitle').css("background-color", "");
	$('#interactionType').css("background-color", "");
	/*$('#formatEvent').css("background-color", "");
	$('#channel').css("background-color", "");*/
	$('#intschedule').css("background-color", "");
	$('#quesId').css("background-color", "");
	$('#logo').css("background-color", "");
	$('#subjectforParticipants').css("background-color", "");
	$('#subjectforFacilator').css("background-color", "");
}
function searchEventByButton()
{
   displayEventGrids();
   page=1;
}

function deleteEvent()
{
	$("#loadingDiv").fadeIn();
	eventId = $("#cancelEventpopupId").attr("cancelEventId");
	if (eventId!=null && eventId != "") {
		EventAjax.cancelEvent(eventId, {
			async : true,
			errorHandler : handleError,
			callback : function(data) {
				$("#loadingDiv").fadeOut();
				searchEventByButton();
			}
		});
	}
	else
		$("#loadingDiv").fadeOut();
}

function showCancelEventPopup(eventId)
{
	$('#loadingDiv').hide();
	$("#cancelEventpopupId").modal('show');
	$("#cancelEventpopupId").attr("cancelEventId",eventId);
}

function getVideoURL(vviUrl)
{
	$('#loadingDiv').show();
	CGInviteInterviewAjax.getPathOfVideoUrl(vviUrl,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#vviDiv').modal('show');
			document.getElementById("ifrmTrans").src=data;
			$('#loadingDiv').hide();
		}
	});
}

