function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else if(exception.javaClassName=='undefined'){}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function searchData()
{
	$("#tblGrid").show();
	$("#qdetails").show();
	$("#tblGridQInsID").show();
	$("#managedqCLine").show();
	$("#managedqIcon").show();
	$("#managedqText").show();
	$("#saveButton").show();
	$("#saveButton1").show();
	$('#districtName').css("background-color", "");
	if($('#districtId').val()==0)
	{
		$("#tblGrid").hide();
		$("#qdetails").hide();
		$("#tblGridQInsID").hide();
		$("#managedqCLine").hide();
		$("#addQuestionInstrunction").hide();
		$("#managedqIcon").hide();
		$("#saveButton").hide();
		
		$('#errordiv').html("&#149; "+resourceJSON.PlzEtrAnyDistrictName+"<br>");
		$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
		return;
	}
	
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
	$('#closePan').html("<a style='padding-right: 6px;' onclick='hideSearchPan();' href='javascript:void(0);' ><i class='icon-remove icon-large' ></i></a>");
	getPortfolioDetail();
}

function getPortfolioDetail() {
	$('#loadingDiv').show();
	var districtId = document.getElementById("districtId").value;
	PortfolioReminderAjax.getPortfolioDetail(districtId,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#tblGridQIns').show();
		$('#tblGrid').html(data);			
		/************* TPL-3973 Candidate Pool Refresh (Applicant Management) Starts *********************/
		candidatePoolRefreshIntialize();		
		/************* TPL-3973 Candidate Pool Refresh (Applicant Management) Ends *********************/
		setAccordion();
		$('#loadingDiv').hide();
	},
	errorHandler:handleError  
	});
}

function setAccordion(){
      $(function ($) {

               var val2= $('#collapseOne,#collapseTwo,#collapseThree');
                val2.on('show.bs.collapse hidden.bs.collapse', function () {
                    $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
                })
            });
                  $('.accordion').collapse();
                  $('.accordion').on('show', function (e) {
                  $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
                  });
                  $('.accordion').on('hide', function (e) {
                  $(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
      });
}

function savePortfolioReminder(){
		var districtId = document.getElementById("districtId").value;
	
		var firstName 		= 	document.getElementById("firstName").checked;
		var lastName 		= 	document.getElementById("lastName").checked;
		var addressLine1 	= 	document.getElementById("addressLine1").checked;
		var phoneNumber 	= 	document.getElementById("phoneNumber").checked;
		var mobileNumber 	= 	document.getElementById("mobileNumber").checked;
		var dob 			= 	document.getElementById("dob").checked;
		
		var genderId 		= 	document.getElementById("genderId").checked;
		var ethnicityId 	= 	document.getElementById("ethnicityId").checked;
		var ethnicOriginId 	= 	document.getElementById("ethnicOriginId").checked;
		var raceId 			= 	document.getElementById("raceId").checked;
		
		var academics		= 	document.getElementById("academics").checked;
		var	resume			=	document.getElementById("resume").checked;
		var	SSN				=	document.getElementById("SSN").checked;
		var	veteran			=	document.getElementById("veteran").checked;
		var	retirementnumber=	document.getElementById("retirementnumber").checked;
		var	videoLink		=	document.getElementById("videoLink").checked;
		
		var	certificationPortfolio	=	document.getElementById("certificationPortfolio").checked;
		var	employmentHistory		=	document.getElementById("employmentHistory").checked;
		var	additionalDocuments		=	document.getElementById("additionalDocuments").checked;
		var	tfaAffiliatePortfolio	=	document.getElementById("tfaAffiliatePortfolio").checked;
		var	references				=	document.getElementById("references").checked;
		var	substituteTeacher		=	document.getElementById("substituteTeacher").checked;
		var	honors					=	document.getElementById("honors").checked;
		
		var academicsVal = 0,resumeVal = 0,highNeedSchoolVal = 0,SSNVal = 0,veteranVal = 0,retirementnumberVal = 0,videoLinkVal = 0;
		var certificationPortfolioVal = 0,employmentHistoryVal=0,additionalDocumentsVal=0,referencesVal=0,tfaAffiliatePortfolioVal=0,substituteTeacherVal=0;
		var honorsVal = 0;
		
		if(academics == true)
			var academicsVal = 1;
		if(resume == true)
			var resumeVal = 1;
		if(honors == true)
			var honorsVal = 1;
		if(SSN == true)
			var SSNVal = 1;
		if(veteran == true)
			var veteranVal = 1;
		if(retirementnumber == true)
			var retirementnumberVal = 1;
		if(videoLink == true)
			var videoLinkVal = 1;
		
		if(certificationPortfolio == true)
			var certificationPortfolioVal = 1;
		if(employmentHistory == true)
			var employmentHistoryVal = 1;
		if(additionalDocuments == true)
			var additionalDocumentsVal = 1;
		if(references == true)
			var referencesVal = 1;
		if(tfaAffiliatePortfolio == true)
			var tfaAffiliatePortfolioVal = 1;
		if(substituteTeacher == true)
			var substituteTeacherVal = 1;
		
		//var	dspqPortfolio	=	document.getElementById("dspqPortfolio").checked;
		
		try{
			var coverLetter		=	document.getElementById("coverLetter").checked;
		}catch(e){}
		
		try{
			var academicDspq		=	document.getElementById("academicDspq").checked;
		}catch(e){}
		
		try{
			var referenceDspq		=	document.getElementById("referenceDspq").checked;
		}catch(e){}
		
		try{
			var	certification	=	document.getElementById("certification").checked;
		} catch(e){}
		
		try{
			var	tfaAffiliate	=	document.getElementById("tfaAffiliate").checked;
		} catch(e){}
		
		try{
			var	candidateType	=	document.getElementById("candidateType").checked;
		} catch(e){}
		
		try{
			var	affidavit		=	document.getElementById("affidavit").checked;
		} catch(e){}
		
		try{
			var	employment		=	document.getElementById("employment").checked;
		} catch(e){}
		
		try{
			var	formeremployee	=	document.getElementById("formeremployee").checked;
		} catch(e){}
		
		try{
			var	subjectAreaExam	=	document.getElementById("subjectAreaExam").checked;
		} catch(e){}
		
		try{
			var	involvement		=	document.getElementById("involvement").checked;
		} catch(e){}
		
		try{
			var	academicTranscript = document.getElementById("academicTranscript").checked;
		} catch(e){}
		
		try{
			var	proofOfCertification =	document.getElementById("proofOfCertification").checked;
		} catch(e){}
		
		try{
			var	referenceLettersOfRecommendation=	document.getElementById("referenceLettersOfRecommendation").checked;
		} catch(e){}
		
		try{
			var	willingAsSubstituteTeacher	=	document.getElementById("willingAsSubstituteTeacher").checked;
		} catch(e){}
		
		try{
			var	expCertTeacherTraining	= document.getElementById("expCertTeacherTraining").checked;
		} catch(e){}
		
		try{
			var	nationalBoardCert =	document.getElementById("nationalBoardCert").checked;
		} catch(e){}
		
		try{
			var	generalKnowledgeExam =	document.getElementById("generalKnowledgeExam").checked;
		} catch(e){}

		
	var arrProfile 	= [];
	var arrEEOC 	= [];
	var arrDSPQ 	= [];
	
	 $('#divProfileGrid input[candPortfolio="portDivHidden"]').each(function() {
		 if($(this).is(":checked")){
			 arrProfile.push("1");
		 } else {
			 arrProfile.push("0");
		 }
	 });
	 
	 $('#divEEOCGrid input[candeeoc="eeocDivHidden"]').each(function() {
		 if($(this).is(":checked")){
			 arrEEOC.push("1");
		 } else {
			 arrEEOC.push("0");
		 }
	 });
	 
	 $('#divDSPQGrid input[canddspq="dspqDivHidden"]').each(function() {
		 if($(this).is(":checked")){
			 arrDSPQ.push("1");
		 } else {
			 arrDSPQ.push("0");
		 }
	 });
	 
	 /*Create Array For District Specific Questions*/
	var dspQuestionArray = "";
	var inputValue;
	var inputs = document.getElementsByName("dspqPortfolioQuestion");
	 for (var i = 0; i < inputs.length; i++) {
		 if (inputs[i].type === 'checkbox') {
			 if(inputs[i].checked){
				 inputValue		=	inputs[i].value;
				 dspQuestionArray+=inputValue+",";
			 }
		 }
	 }
	 /** TPL-3973 Candidate Pool Refresh (Applicant Management) Starts *********************/
	savecandidatepoolrefresh();
	/** TPL-3973 Candidate Pool Refresh (Applicant Management) Ends *********************/

 // PortfolioReminderAjax.savePortfolioDetail(districtId,academicsVal,resumeVal,highNeedSchoolVal,SSNVal,veteranVal,retirementnumberVal,videoLinkVal,arrProfile,arrEEOC,coverLetter,certification,tfaAffiliate,candidateType,affidavit,employment,formeremployee,subjectAreaExam,involvement,academicTranscript,proofOfCertification,referenceLettersOfRecommendation,willingAsSubstituteTeacher,expCertTeacherTraining,nationalBoardCert,generalKnowledgeExam,{
// 	PortfolioReminderAjax.savePortfolioDetail(districtId,academicsVal,resumeVal,highNeedSchoolVal,SSNVal,veteranVal,retirementnumberVal,videoLinkVal,certificationPortfolioVal,employmentHistoryVal,additionalDocumentsVal,referencesVal,tfaAffiliatePortfolioVal,substituteTeacherVal,arrProfile,arrEEOC,coverLetter,certification,tfaAffiliate,candidateType,affidavit,employment,formeremployee,subjectAreaExam,involvement,academicTranscript,proofOfCertification,referenceLettersOfRecommendation,willingAsSubstituteTeacher,expCertTeacherTraining,nationalBoardCert,generalKnowledgeExam,dspQuestionArray,{
	PortfolioReminderAjax.savePortfolioDetail(districtId,academicsVal,resumeVal,highNeedSchoolVal,SSNVal,veteranVal,retirementnumberVal,videoLinkVal,certificationPortfolioVal,employmentHistoryVal,additionalDocumentsVal,referencesVal,tfaAffiliatePortfolioVal,substituteTeacherVal,arrProfile,arrEEOC,coverLetter,certification,tfaAffiliate,candidateType,affidavit,employment,formeremployee,subjectAreaExam,involvement,academicTranscript,proofOfCertification,referenceLettersOfRecommendation,willingAsSubstituteTeacher,expCertTeacherTraining,nationalBoardCert,generalKnowledgeExam,dspQuestionArray,honorsVal,academicDspq,referenceDspq,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
		//successFullMessage(data);
		window.location="portfoliofinal.do";
	},
	errorHandler:handleError
	});
}


function clearOptions() {
	for(i=1;i<=6;i++) {
		$('#opt'+i).attr("value","");
		$('#valid'+i).attr("checked",false);
	}
}

function redirectTo(redirectURL) {	
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
}

function hideSearchPan() {
	$('#districtName').css("background-color", "");
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
}

function getSearchPan() {
	$('#errordiv').hide();
	$('#districtName').css("background-color", "");
	$('#searchItem').fadeIn(1000);
	$('#sa').fadeOut(1200);
}

function checkAllPortfolio(){
	var flag = document.getElementById("persionalInfo").checked;

	 if(flag==true) {
		 $('#divProfileGrid input[candPortfolio="portDivHidden"]').each(function() {
			$(this).attr('checked', 'checked');
		});
	 } else {	 
		 $('#divProfileGrid input[candPortfolio="portDivHidden"]').each(function() {
			$(this).prop('checked', false);
		});
	 }
}

function checkAllEEOC(){
	var flag = document.getElementById("eeoc").checked;

	 if(flag==true) {
		 $('#divEEOCGrid input[candEEOC="eeocDivHidden"]').each(function() {
			$(this).attr('checked', 'checked');
		});
	 } else {	 
		 $('#divEEOCGrid input[candEEOC="eeocDivHidden"]').each(function() {
			$(this).prop('checked', false);
		});
	 }
}

function checkAllDSPQ(){
	var flag = document.getElementById("dspqPortfolio").checked;

	 if(flag==true) {
		 $('#divDSPQGrid input[candDSPQ="dspqDivHidden"]').each(function() {
			$(this).attr('checked', 'checked');
		});
	 } else {	 
		 $('#divDSPQGrid input[candDSPQ="dspqDivHidden"]').each(function() {
			$(this).prop('checked', false);
		});
	 }
}

function successFullMessage(data){
	var messageString = "";
	if(data == "1"){
		messageString = resourceJSON.MsgDetailInsertedSuccessfully;
	} else {
		messageString = resourceJSON.MsgDetailUpdatedSuccessfully;
	}
	document.getElementById("updateMsg").innerHTML = messageString; 
	$('#myModalUpdateMsg').modal('show');
}

//////////////////////////////////////////////////////////////////
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray 	= 	new Array();
var showDataArray 		= 	new Array();
var degreeTypeArray 	= 	new Array();
var hiddenId			=	"";

function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type) {

	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	}
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();

	}
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
		BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="") {

			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		} else {

	}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId) {

	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid) {

		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}
