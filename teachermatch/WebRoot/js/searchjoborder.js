var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function GetXMLHttp()
{
	var temp=null;
	if (window.ActiveXObject) 
		temp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest) 
		temp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
	return temp;   		
}

var autoHttpxmlRequest='';

//=========== In Case of final =====================
//For State Text Box

var hiddenId="";


function showMiamiPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}


function showPhiladelphiaPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("images/District_Map_LN_8x11_2014_09_05%5B1%5D%20copy.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}

function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getUniversityArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	
	var searchArray = new Array();
	DWRAutoComplete.getDistrictMasterList(districtName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
			showDataArray[i]=data[i].districtName;
		}
	}
	});	

	return searchArray;
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	//setDefColortoErrorMsg();
	//$('#errordiv').empty();	
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		//if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		//}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}





//----

function getSchoolMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		//searchArray = getUniversityArray(txtSearch.value);
		//fatchData(txtSearch,searchArray,txtId,txtdivid);
		document.getElementById("jobCategoryId").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';	
		searchArray = getSchoolArray(txtSearch.value);		
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
	
}

function getSchoolArray(schoolName){

	var districtId = document.getElementById("districtId").value;
	
	var searchArray = new Array();
	DWRAutoComplete.getSchoolMasterList(schoolName,districtId,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			
			searchArray[i]=data[i].schoolMaster.schoolName;			
			hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			showDataArray[i]=data[i].schoolMaster.schoolName;
		}
	}
	});	

	return searchArray;
}
function hidedivTxtSchoolDataDiv()
{
	document.getElementById("divTxtSchoolData").style.display='none';	
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
//----
function hideSchoolMasterDiv1(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}





var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function __mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		document.getElementById('divResult'+txtdivid+i).className='normal';		
		if ($('#divResult'+txtdivid+i).is(':hover')) 
		{
			document.getElementById('divResult'+txtdivid+i).className='over';	       
	       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
	        index=i;
	    }
	}

}

var overText = function (div_value,txtdivid) 
{
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

/* @Start
 * @Ashish Kumar
 * @Description :: Display Selected Cities by state and Get All Sates
 * */
function selectCityByState()
{
	var stateId = $("#stateId").val();
	
			DWRAutoComplete.selectedCityByStateId(stateId,{ 
				async: false,		
				callback: function(data){
					document.getElementById("cityDivId").innerHTML=data;
				}
			});
}

function displayState(){

	DWRAutoComplete.displayStateData({ 
		async: false,		
		callback: function(data){
			document.getElementById("stateDivId").innerHTML=data;
			searchJob();
		}
	});
}
/* @End
 * @Ashish Kumar
 * @Description :: Display Selected Cities by state and Get All Sates
 * */


function getZoneDAForEJob(dsId)
{
	var districtOrSchooHiddenlId=dsId;
	ManageJobOrdersAjax.getZoneListAllForEJob(districtOrSchooHiddenlId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=null && data!="")
			{
				$("#isZoneAvailable").val(1);			
				$("#displayZone").show();			
				$("#zone").html(data);
			}
			else{
				$("#displayZone").hide();
				$("#zone").html(data);				
				$("#isZoneAvailable").val(0);
			}	
		
		},
	});
}

function getJobSubCateList()
{
	var districtId = $("#districtId1").val();
	var jobCategoryId = $("#jobCategoryId").val();
	
	if(districtId=='')
		districtId=0;
	if(jobCategoryId=='')
		jobCategoryId=0;
	
	ManageJobOrdersAjax.getJobSubCateForSeach(jobCategoryId,districtId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=null && data!="")
			{
				$("#jobSubCateDiv").html(data);
				//shadab commented below lines
				//searchJob();
				esSearchdistrictjobboard();
			}
		},
	});
}

function getJobCategoryByDistrict()
{
	var headQuarterId=$("#headQuarterId").val();
	if(headQuarterId==1)
	{
		return false;
	}	
	var districtId = document.getElementById("districtId").value;
	DWRAutoComplete.getJobCategoryByDistrict(districtId,{ 
		async: true,		
			callback: function(data)
			{
		//alert(data);
				document.getElementById("jobCategoryId").innerHTML=data;
			}
		});
	
	
}

function setJobBoardReferralURL(){
//	alert("document.referrer : "+document.referrer);
	var referrer = document.referrer!=""?document.referrer:null;
	if(referrer=='' || referrer=='null' || referrer==null){
		referrer=null;
	}
	
	ManageJobOrdersAjax.setJobBoardReferralURL(referrer,{ 
		async: true,
		callback: function(data)
		{
			
		},
	});
}