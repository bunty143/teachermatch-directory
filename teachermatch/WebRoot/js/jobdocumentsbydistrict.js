var txtBgColor = "#F5E7E1";
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var uploadedFiles = [];
/*
 * 
 * 
 */
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function addFlag()
{
	document.getElementById('addEditFlag').value = 0;
}
function openJobDocumentDiv()
{
	$('#jobDocumentDiv').fadeIn();
	$('#urlInstruction').tooltip();
	document.getElementById('jobdocumentLink').value="";
	var entityType		=	document.getElementById("entityType").value;
	var flag = document.getElementById('addEditFlag').value;
	if(flag == 0)
	{
		publishJobDocuments();
	}
	if(entityType==1 && flag==0)
	{		
	$("#avlbList").prop('disabled', true);
	$("#attachedList").prop('disabled', true);
	$("#onlyDistrictName").prop('disabled', false);
	$("#onlyDistrictName").focus();
	resetJobCategories();
	}
	else if(entityType==1 && flag==1)
	{
		$("#onlyDistrictName").prop('disabled', true);
		document.getElementById('attachedList').innerHTML="";
		$("#documentName").focus();
	}
	else
	{
		document.getElementById('docCaption').style.display="none";
		//$("#documentName").focus();
	}
	 document.getElementById('save').style.display="block";
	 document.getElementById('update').style.display="none";
	 for(var i=0;i<uploadedFiles.length;i++)
		{
			uploadedFiles.pop();
		}
		document.getElementById('uploadedFiles').innerHTML="";
	$('#uploadedDiv').hide();
	$('.add-employment1').hide();
	
	getDocsDiv();
	
}
function cancelJobDocument()
{
	$('#jobDocumentDiv').hide();
	$('#uploadedDiv').hide();
	resetJobCategories();
	resetForm();
	$('.add-employment1').show();
}
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayJobsDocsRecord();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayJobsDocsRecord();
}
/*======= Save AddEditDocument on Press Enter Key ========= */
function chkForEnterAddEditDocument(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		validateAddEditDocument();
	}	
}

function getUploadedFileName(uploadedFileName)
{
	$('#loadingDiv').show();
	if(uploadedFileName!=null && uploadedFileName!='')
	{
			document.getElementById("uploadedFileName").value = uploadedFileName;
			var uploadedBy = document.getElementById("userId").value;
			var flag = document.getElementById("addEditFlag").value;
			var jobdocument		=	document.getElementById("jobdocument").value;
			var documentName		=	document.getElementById("documentName").value;
			var entityType		=	document.getElementById("entityType").value;
			var districtHiddenId		=	document.getElementById("districtHiddenId").value;
			var  jobDocsId = document.getElementById("jobDocId").value;
			//alert(" jobDocsId :: "+jobDocsId);
			var opts = document.getElementById('attachedList').options;
			var arrReqNum = "";
			var arrReqNumVal=0;
				if(opts!=null && opts!='')
				{
						for(var i = 0, j = opts.length; i < j; i++)
						{
							arrReqNum+=opts[i].value+"#";
						}
						if(arrReqNum!="")
							arrReqNumVal=(arrReqNum.split("#").length-1);
				}

			try
			{
			JobDocumentsByDistrictAjax.saveJobDocuments(districtHiddenId,uploadedBy, entityType, uploadedFileName, arrReqNum,documentName,flag,jobDocsId,{
					async: true,
				callback: function(data)
				{
					if(data=="save")
					{
						$('#loadingDiv').hide();
						$('#textMessageModal').modal('show');
						document.getElementById('textMessage').innerHTML = resourceJSON.MsgSuccessfullyAddedThatJob ;
						resetJobCategories();
						resetForm();
						cancelJobDocument();
						if(entityType==1)
						{							
							document.getElementById('districtHiddenId').value ="";
						}
						displayJobsDocsRecord();
					}
					else if(data=="update")
					{
						$('#loadingDiv').hide();
						$('#textMessageModal').modal('show');
						document.getElementById('textMessage').innerHTML = resourceJSON.MsgSuccessfullyUpdatedThatJob;
						resetJobCategories();
						cancelJobDocument();
						if(entityType==1)
						{							
							document.getElementById('districtHiddenId').value ="";
						}
						displayJobsDocsRecord();
					}
					else
					{
						$('#loadingDiv').hide();						
					}					
				},
			errorHandler:handleError 
		});
	}
	catch(e){alert(e);}
	}
}

function viewUploadedFiles(uploadedFileName)
{
	if(uploadedFileName!="")
	{
		uploadedFiles.push(uploadedFileName);
		displayNewUploadedDocuments(uploadedFiles);
	}
}
function removeUploadedFiles(uploadedFileName)
{
	//alert(" uploadedFileName :: "+uploadedFileName);
	//alert(" uploadedFiles :: "+uploadedFiles.length);
	var i = uploadedFiles.indexOf(uploadedFileName);
	if(i != -1) {
	uploadedFiles.splice(i,1);
	//alert(" "+resourceJSON.msguploadedFiles+" "+uploadedFiles.length);
	displayNewUploadedDocuments(uploadedFiles);
	}
}
function displayUploadedDocuments(uploadedFiles)
{
	try
	{			
		JobDocumentsByDistrictAjax.displayUploadedFiles(uploadedFiles,{
		async: true,
		callback: function(data)
		{
			if(data!="")
			{
				document.getElementById('uploadedFiles').innerHTML="";
				document.getElementById('uploadedFiles').innerHTML=data;	
				document.getElementById("jobdocument").value ="";
				for(var i=0;i<uploadedFiles.length;i++)
				{
					uploadedFiles.pop();
				}
			}
			else
			{
				document.getElementById('uploadedFiles').innerHTML="";							
			}					
		},
	errorHandler:handleError 
});
	}
	catch(e){alert(e);}
}
function displayNewUploadedDocuments(uploadedFiles)
{
	try
	{			
		JobDocumentsByDistrictAjax.displayUploadedFiles(uploadedFiles,{
		async: true,
		callback: function(data)
		{
			if(data!="")
			{
				document.getElementById('uploadedFiles').innerHTML="";
				document.getElementById('uploadedFiles').innerHTML=data;	
				document.getElementById("jobdocument").value ="";
				
			}
			else
			{
				document.getElementById('uploadedFiles').innerHTML="";							
			}					
		},
	errorHandler:handleError 
});
}
catch(e){alert(e);}
}
function resetForm()
{
	var entityId = document.getElementById("entityType").value;
	if(entityId==1)
	{
		document.getElementById("onlyDistrictName").value = "";	
	}	
	document.getElementById("addEditFlag").value = 0;
	document.getElementById("jobDocId").value = 0;
	document.getElementById('jobdocument').value="";
	document.getElementById('newDocName').value="";
	$('#newDocName').css("background-color","#eeeeee");
	document.getElementById('newDocName').disabled=true;
	$('input:radio[name=expDoc]')[1].checked = true;
	document.getElementById('defDoc').style.display="none";
	document.getElementById('newDoc').checked=false;
	document.getElementById('defaultDoc').checked=false;
	$('#jobDocsList').css("background-color","#eeeeee");
	document.getElementById('jobDocsList').disabled=true;
	document.getElementById('docExpire').style.display="none";
	document.getElementById('docExpFlag').style.display="none";
	document.getElementById('docExpFlag').value = "No";
	//document.getElementById('newName').style.display = "none";
	document.getElementById('newName').value = "";
	document.getElementById('expDate').value = "";
	document.getElementById('expDateDiv').style.display="none";
	//document.getElementById('docCaption').style.display="none";
	if(entityId==2)
	{
		$('#expDate').css("background-color","white");
		$('#newName').css("background-color","white");
		$('#attachedList').css("background-color","white");
	}
	else if(entitType==1)
	{
		$('#onlyDistrictName').css("background-color","white");
		$('#expDate').css("background-color","white");
		$('#newName').css("background-color","white");
		$('#attachedList').css("background-color","white");
	}
	for(var i=0;i<uploadedFiles.length;i++)
	{
		uploadedFiles.pop();
	}
	$('#errordiv').empty();
}

function resetJobCategories()
{
	try
	{
		document.getElementById('attachedList').innerHTML="";
		var districtId		=	document.getElementById("districtHiddenId").value;
		JobDocumentsByDistrictAjax.getJobCategories(districtId,{
		async: true,
		callback: function(data)
		{
			if(data!="")
			{
				$('#loadingDiv').hide();
				document.getElementById('avlbList').innerHTML=data;				
			}
			else
			{
				$('#loadingDiv').hide();						
			}					
		},
	errorHandler:handleError 
});
}
catch(e){alert(e);}
}

function validateAddEditDocument()
{
	$('#errordiv').empty();
	$('#loadingDiv').show();
	var errorCount = 0;
	var documentName = "";
	var expDate = "";
	var defaultDocId = "";
	var docFlag = 0;   // for default or new
	var expDateReq = document.getElementById('docExpFlag').value;
	var defaultDoc = document.getElementById('defaultDoc').checked;
	var newDoc = document.getElementById('newDoc').checked;
	var uploadedBy = document.getElementById("userId").value;
	var jobdocument		=	document.getElementById("jobdocument").value;
	var  jobDocsId = document.getElementById("jobDocId").value;
	var entityType		=	document.getElementById("entityType").value;
	var districtHiddenId	=	document.getElementById("districtHiddenId").value;
	var dateTime=trim(document.getElementById("dateTime").value);
	var flag = document.getElementById("addEditFlag").value;
	expDate = document.getElementById('expDate').value;
	alert(" jobDocsId :: "+jobDocsId+" flag :: "+flag+" uploadedFiles :: "+uploadedFiles.length+" expDate :: "+expDate);
	defaultDocId = $("#jobDocsList option:selected").val();
	var arrReqNum = "";
	var arrReqNumVal=0;
	var jobDocumentVal = "";
	var opts = document.getElementById('attachedList').options;
	if(opts!=null && opts!='')
	{
			for(var i = 0, j = opts.length; i < j; i++)
			{
				arrReqNum+=opts[i].value+"#";
			}
			if(arrReqNum!="")
				arrReqNumVal=(arrReqNum.split("#").length-1);
	}
	var ext = jobdocument.substr(jobdocument.lastIndexOf('.') + 1).toLowerCase();	
	var fileSize = 0;
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{
		if(document.getElementById("jobdocument").files[0]!=undefined)
		{
			fileSize = document.getElementById("jobdocument").files[0].size;
		}
	}
	if(defaultDoc)
	{
		//docFlag = 0;
		documentName = document.getElementById('newName').value;
		//alert(" defaultDocId :: "+defaultDocId);
		if(defaultDocId=="" || defaultDocId==-1)
		{
			$('#loadingDiv').hide();
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.PlzSlctDefaultDocument+"<br>");
			$('#jobDocsList').focus();
			$('#jobDocsList').css("background-color",txtBgColor);
			errorCount++;
		}	
		else if(documentName=="")
		{
			$('#loadingDiv').hide();
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDocumentCaption+"<br>");
			$('#newName').focus();
			$('#newName').css("background-color",txtBgColor);
			errorCount++;
		}
		if(expDateReq=="Yes")
		{
			
			if(expDate=="")
			{
				$('#loadingDiv').hide();
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.PlzSlctDocumentExpireDate+".<br>");
				$('#expDate').focus();
				$('#expDate').css("background-color",txtBgColor);
				errorCount++;
			}
		}
		defaultDocId = document.getElementById('jobDocsList').value;
		
	}
	if(newDoc)
	{
		docFlag = 1;
		documentName = document.getElementById('newDocName').value;
		if(documentName=="")
		{
			$('#loadingDiv').hide();
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDocumentName+"<br>");
			$('#newDocName').focus();
			$('#newDocName').css("background-color",txtBgColor);
			errorCount++;
		}
		/*if(uploadedFiles.length==0)
		{
			$('#loadingDiv').hide();
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please select Document.<br>");
			errorCount++;
		}	*/
		
		expDate="";
	}
	if(entityType==1)
	{
		if(districtHiddenId=="")
		{
			$('#loadingDiv').hide();
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgEnterDistName+"<br>");
			errorCount++;
		}
	}
	/*if(documentName=="")
	{	
		$('#loadingDiv').hide();
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please enter Document Name.<br>");
		errorCount++;
	}*/
	if(arrReqNum=="")
	{		
		$('#loadingDiv').hide();
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzSlctAtleastOneJobCategory+"<br>");
		$('#attachedList').focus();
		$('#attachedList').css("background-color",txtBgColor);
		errorCount++;
	}
	if(arrReqNum=="-1#")
	{		
		$('#loadingDiv').hide();
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgNoJobCategoryExists+"<br>");
		errorCount++;
	}	

	if(errorCount == 0)
	{	
		//check for insert/update

		var jobcatIdOnEdit="";
		if(flag==0){
			jobcatIdOnEdit='0';
		}else{
			jobcatIdOnEdit=document.getElementById('jobcatIdOnEdit').value;
		}
		if(defaultDoc){
			//defaultDocId//
			alert(" defaultDocId :: "+defaultDocId);
			JobDocumentsByDistrictAjax.checkforDuplicasy(districtHiddenId,defaultDocId,arrReqNum,flag,jobcatIdOnEdit,{ 
				async: false,
				callback: function(data)
				{	
				 if(data=="success"){
					 
				 }else{
					 errorCount++;
						$('#loadingDiv').hide();
						$('#errordiv').show();
						$('#errordiv').append("&#149;"+ data);
						$('#attachedList').focus();
						$('#attachedList').css("background-color",txtBgColor);
						return false;
				  }
				},
			errorHandler:handleError
			});	
			
		}		

		
		if(errorCount == 0)
		{
		try
		{
			JobDocumentsByDistrictAjax.saveJobDocuments(docFlag,districtHiddenId,uploadedBy, entityType, arrReqNum,documentName,flag,jobDocsId,expDate,defaultDocId,uploadedFiles,{
				async: true,
			callback: function(data)
			{
				if(data=="save")
				{
					$('#loadingDiv').hide();
					$('#textMessageModal').modal('show');
					document.getElementById('textMessage').innerHTML = resourceJSON.MsgSuccessfullyAddedThatJob;
					resetJobCategories();
					resetForm();
					cancelJobDocument();
					if(entityType==1)
					{							
						document.getElementById('districtHiddenId').value ="";
					}
					displayJobsDocsRecord();
				}
				else if(data=="update")
				{
					$('#loadingDiv').hide();
					$('#textMessageModal').modal('show');
					document.getElementById('textMessage').innerHTML = resourceJSON.MsgSuccessfullyUpdatedThatJob;
					resetJobCategories();
					cancelJobDocument();
					if(entityType==1)
					{							
						document.getElementById('districtHiddenId').value ="";
					}
					displayJobsDocsRecord();
				}
				else
				{
					$('#loadingDiv').hide();						
				}			
			},
		errorHandler:handleError 
	});
}
catch(e){alert(e);}
}
}
}

function uploadFile()
{
	var jobdocument		=	document.getElementById("jobdocument").value;
	var errorCount = 0;
	var ext = jobdocument.substr(jobdocument.lastIndexOf('.') + 1).toLowerCase();	
	var fileSize = 0;
	
	$("#hiddenIdDistrict").val($("#districtHiddenId").val());
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{
		if(document.getElementById("jobdocument").files[0]!=undefined)
		{
			fileSize = document.getElementById("jobdocument").files[0].size;
		}
	}
	if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
	{
		$('#loadingDiv').hide();
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgacceptabledocformate+"<br>");
		errorCount++;
	}
	if(fileSize>=10485760) // for check 10mb
	{
		$('#loadingDiv').hide();
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
		errorCount++;
	}
	if(errorCount==0)
	{
		if(jobdocument!="" && jobdocument!=null && jobdocument!=undefined)
		{
			$('#errordiv').empty();
			document.getElementById("frmDocumentUpload").submit();
		}
	}
	else
	{
		return false;
	}
	
}

function afterUpload()
{
	
}

function displayJobsDocsRecord()
{	
	var districtId = "";
	var schoolId = "";
	if(document.getElementById('districtHiddenId').value!='')
	{
		districtId  = document.getElementById('districtHiddenId').value;	
	}
	//alert(" districtId :: "+districtId);
	$('#loadingDiv').show();
	try
	{
		JobDocumentsByDistrictAjax.displayJobsDocumentsRecord(districtId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			if(data!='')
			{
				$('#loadingDiv').hide();			
				$('#documentsData').html(data);			
				applyScrollOnTbl();
			}
			else
			{
				//alert("No Data");
			}
			
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}	
	
}

function downloadJobDoc(uploadedBy,doucumentName,jobDocsId)
{
	if(doucumentName.indexOf("http") > -1){
		$("#docToolTip1").attr("href", doucumentName);
		$("#docToolTip1").attr("target", "_blank");
	}else{
		JobDocumentsByDistrictAjax.downloadDocument(uploadedBy,doucumentName,jobDocsId,
				{ 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
						if (data.indexOf(".doc") !=-1) {
						    document.getElementById('ifrmDocument').src = ""+data+"";
						}
						else
						{
							document.getElementById("hrefDoc").href = data;
							$("#hrefDoc").trigger("click");
							return false;
						}
						
						if(deviceType)
						{
							if (data.indexOf(".doc")!=-1)
							{
								$("#docfileNotOpen").css({"z-index":"3000"});
						    	$('#docfileNotOpen').show();
							}
							else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								$("#exelfileNotOpen").css({"z-index":"3000"});
						    	$('#exelfileNotOpen').show();
							}
							else
							{
								document.getElementById("hrefDoc").href = data;
							}
						}
						else if(deviceTypeAndroid)
						{
							if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								document.getElementById('ifrmDocument').src = ""+data+"";
							}
							else
							{
								document.getElementById("hrefDoc").href = data;	
							}	
						}
						else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
						{
							document.getElementById('ifrmDocument').src = ""+data+"";
						}
						else
						{
							document.getElementById("hrefDoc").href = data;	
						}	 
						return false;
						
					}
				});
	}
	
}

function downloadJobDocNew(uploadedBy,doucumentName,jobDocsId, counter)
{
    if(doucumentName.indexOf("http") > -1){
		$("#docToolTipNew"+counter).attr("href", doucumentName);
		$("#docToolTipNew"+counter).attr("target", "_blank");
	}else{
		JobDocumentsByDistrictAjax.downloadDocument(uploadedBy,doucumentName,jobDocsId,
				{ 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
						if (data.indexOf(".doc") !=-1) {
						    document.getElementById('ifrmDocument').src = ""+data+"";
						}
						else
						{
							document.getElementById("hrefDoc").href = data;
							$("#hrefDoc").trigger("click");
							return false;
						}
						
						if(deviceType)
						{
							if (data.indexOf(".doc")!=-1)
							{
								$("#docfileNotOpen").css({"z-index":"3000"});
						    	$('#docfileNotOpen').show();
							}
							else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								$("#exelfileNotOpen").css({"z-index":"3000"});
						    	$('#exelfileNotOpen').show();
							}
							else
							{
								document.getElementById("hrefDoc").href = data;
							}
						}
						else if(deviceTypeAndroid)
						{
							if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								document.getElementById('ifrmDocument').src = ""+data+"";
							}
							else
							{
								document.getElementById("hrefDoc").href = data;	
							}	
						}
						else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
						{
							document.getElementById('ifrmDocument').src = ""+data+"";
						}
						else
						{
							document.getElementById("hrefDoc").href = data;	
						}	 
						return false;
						
					}
				});
	}
	
}
function editJobDoc(jobDocsId,districtId,documentName,jobCategoryId)
{
	$('#erroreditdiv').empty();
	document.getElementById('jobcatIdOnEdit').value=jobCategoryId;
	document.getElementById('jobdocument').value="";
	document.getElementById('addEditFlag').value = 1;
	document.getElementById('jobDocId').value = jobDocsId;
	getSelectedJobCatsByIdForEdit();
	getRemJobCatForEdit();
	//getDetails();
	getUploadedDocument(documentName,jobCategoryId); 
	getDocumentCategory();
	openJobDocumentDiv();
	 document.getElementById('save').style.display="none";
	 document.getElementById('update').style.display="block";
	 $("#avlbList").prop('disabled', false);
	 $("#attachedList").prop('disabled', false);
}

function getDocumentCategory()
{
	var jbId = document.getElementById('jobDocId').value;
	//alert(" jbId :: "+jbId);
	try
	{
		JobDocumentsByDistrictAjax.showDocumentListById(jbId,{ 
			async: true,
			callback: function(data)
			{
				if(data!="")
				{
					var value = data.split("@@@");
					if(value[3]=="notfound")
					{
						//alert("id get");
						$('#jobDocsList').css("background-color", "white");
						document.getElementById('newDocName').disabled=true;
						document.getElementById('defaultDoc').checked=true;
						document.getElementById('jobDocsList').disabled = false;
						document.getElementById('jobDocsList').innerHTML = value[0];
						document.getElementById('newDocName').value="";
						document.getElementById('newDoc').checked=false;
						document.getElementById('docCaption').style.display="block";
						document.getElementById('newName').style.display="block";
						document.getElementById('newName').value=value[1];
						document.getElementById('docExpire').style.display="block";
						if(value[2]=="N/A")
						{
							$('input:radio[name=expDoc]')[1].checked = true;
							document.getElementById('expDateDiv').style.display="none";
							document.getElementById('expDate').value="";
						}
						else
						{
							document.getElementById('expDateDiv').style.display="block";
							document.getElementById('expDate').value=value[2];
							$('input:radio[name=expDoc]')[0].checked = true;
							
						}
						showDefaultDocumentName($("#jobDocsList option:selected").val());
					}
					else if(value[3]=="found")
					{
						$('#newDocName').css("background-color", "white");
						document.getElementById('newDocName').disabled=false;
						document.getElementById('newDoc').checked=true;
						document.getElementById('defaultDoc').checked=false;
						document.getElementById('jobDocsList').disabled = true;
						publishJobDocuments();
						document.getElementById('newDocName').value=value[1];
						document.getElementById('docCaption').style.display="none";
						document.getElementById('newName').value="";
						document.getElementById('docExpire').style.display="none";
						$('input:radio[name=expDoc]')[1].checked = true;
						document.getElementById('expDate').value="";
						document.getElementById('expDateDiv').style.display="none";
						document.getElementById('defDoc').style.display="none";
					}
				}
			},
			errorHandler:handleError 
		});
	}
	catch(e){alert(e);}
}

function getUploadedDocument(documentName,jobCategoryId)
{
	try
	{
		JobDocumentsByDistrictAjax.getDetailsByJobDocsId(documentName,jobCategoryId,{ 
			async: true,
			callback: function(data)
			{
				if(data!="")
				{
					for(var i=0;i<uploadedFiles.length;i++)
					{
						uploadedFiles.pop();
					}
					var check = data.indexOf("@@@");
					if(check==-1)
					{
						uploadedFiles.push(data);
						displayNewUploadedDocuments(uploadedFiles);
					}
					else
					{
						var getValue = data.split("@@@");					
						for(var f=0;f<getValue.length;f++)
						{
							uploadedFiles.push(getValue[f]);
						}
						displayNewUploadedDocuments(uploadedFiles);
					}
					document.getElementById("jobdocument").value ="";
				}
				else
				{
					document.getElementById('uploadedFiles').innerHTML="";							
				}		
			},
			errorHandler:handleError 
		});
	}
	catch(e){alert(e);}
}

/*function getDetails()
{
	//alert("add edit flag value :: "+document.getElementById('addEditFlag').value);
	var jbId = document.getElementById('jobDocId').value;
	try
	{
		JobDocumentsByDistrictAjax.getDetailsByJobDocsId(jbId,"","",{ 
			async: true,
			callback: function(data)
			{
				var getValue = data.split("@@@");
				//document.getElementById('editDocName').value = getValue[0];
				//document.getElementById('documentName').value = getValue[0];
				$('#uploadedDiv').fadeIn();
				document.getElementById('showPrevDoc').innerHTML = getValue[1];
				document.getElementById('prevDoc').value = getValue[1];
			},
			errorHandler:handleError 
		});
	}
	catch(e){alert(e);}
}*/
function showPreviousDocument()
{
	downloadJobDoc(document.getElementById('userId').value,document.getElementById('prevDoc').value,document.getElementById('jobDocId').value);
}

function editDocument()
{
	$('#erroreditdiv').empty();
	var errorCount = 0;
	var jobdocument		=	document.getElementById("jobdocument1").value;
	var editJobCategory		=	document.getElementById("editJobCategory").value;
	document.getElementById('edJbCateg').value = editJobCategory;
	var editDocName		=	document.getElementById("editDocName").value;
	var ext = jobdocument.substr(jobdocument.lastIndexOf('.') + 1).toLowerCase();
	var prevDocVal = document.getElementById("prevDoc").value;
	var fileSize = 0;
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{
		if(document.getElementById("jobdocument1").files[0]!=undefined)
		{
			fileSize = document.getElementById("jobdocument1").files[0].size;
		}
	}
	if(jobdocument!='' && prevDocVal!=jobdocument)
	{
	/*if(jobdocument==null && jobdocument!='')
	{
		$('#loadingDiv').hide();
		$('#erroreditdiv').show();
		$('#erroreditdiv').append("&#149; Please select Document.<br>");
		errorCount++;
	}*/
	/*else*/ 
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			//alert("extension");
			$('#loadingDiv').hide();
			$('#erroreditdiv').show();
			$('#erroreditdiv').append("&#149; "+resourceJSON.msgacceptabledocformate+"<br>");
			errorCount++;
		}
		else if(fileSize>=10485760) // for check 10mb
		{
			//alert("file size");
			$('#loadingDiv').hide();
			$('#erroreditdiv').show();
			$('#erroreditdiv').append("&#149; "+resourceJSON.msgScoreReportFilesize+"<br>");
			errorCount++;
		}
	}
	if(editDocName=="")
	{
		//alert("doc name error");
		$('#loadingDiv').hide();
		$('#erroreditdiv').show();
		$('#erroreditdiv').append("&#149; "+resourceJSON.msgdocumentname+"<br>");
		errorCount++;
	}
	if(errorCount == 0)
	{
		 if(jobdocument!='')
		 {
			 if(jobdocument!="" && jobdocument!=null && jobdocument!=undefined)
				{
					document.getElementById("frmEditDocumentUpload").submit();
				} 
		 }
		 else
		 {
			 //alert(" old update ");
			 getUploadedFileName(prevDocVal);
		 }
	}
	else
	{
		//alert("error");
		return false;
	}
}
function clearEditDocument()
{
	document.getElementById("jobdocument1").value = "";
	$('#erroreditdiv').empty();
}

function getJobCategoriesByJBID()
{
	var jbId = document.getElementById('jobDocId').value;
	try
	{
		JobDocumentsByDistrictAjax.getJobCategoryForEdit(jbId,{ 
		async: true,
		callback: function(data)
		{
			if(data!="")
			{
				document.getElementById('editJobCategory').innerHTML = data;	
			}
			else
			{
				//	alert("No Data");
			}
			
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}	
}
function openRemoveDocModal(jobDocId)
{
	$('#removeDocModal').modal('show');
	document.getElementById('removeConfirmMessage').innerHTML=resourceJSON.MsgRemoveJobDocument;
	document.getElementById('jobDocId').value = jobDocId;
}

function hideRemoveDocModal()
{
	$('#removeDocModal').modal('hide');
}

function removeJobDoc()
{
	
	var jbdocid = document.getElementById('jobDocId').value;
	$('#loadingDiv').show();	
	try
	{
		JobDocumentsByDistrictAjax.removeJobDocumentsRecord(jbdocid,{ 
		async: true,
		callback: function(data)
		{
			if(data=="success")
			{	
				$('#loadingDiv').hide();
				$('#removeDocModal').modal('hide');
				$('#successMessageModal').modal('show');
				document.getElementById('successTextMessage').innerHTML=resourceJSON.MsgSuccessfullyRemovedThatJob;
				cancelJobDocument();
			}
			else
			{
				//alert("No Data");
			}
			
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}	
}

function hideSuccessModal()
{
	$('#successMessageModal').modal('hide');
	resetJobCategories();
	resetForm();
	displayJobsDocsRecord();
}


var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	//alert(" hiddenId :: "+hiddenId);
	hiddenId=hiddenId;
	if(document.getElementById("userSessionEntityTypeId").value==2)
	{
	  document.getElementById("districtHiddenId").value = hiddenId;
	}
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtOrSchoolName){
	var searchArray = new Array();
	var entityID	=	document.getElementById("MU_EntityType").value;
	var districtIdForSchool	=document.getElementById("districtHiddenId").value;
	var userSessionEntityTypeId	=document.getElementById("userSessionEntityTypeId").value;
	if(userSessionEntityTypeId==1){
		//districtIdForSchool=0;
	}
	//alert(" userSessionEntityTypeId & districtOrSchoolName & districtIdForSchool :: "+userSessionEntityTypeId+"   :::::  "+districtOrSchoolName +"  :::  "+districtIdForSchool);
	
	
	if(entityID==2){
		UserAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	}else {
		UserAjax.getFieldOfSchoolList(districtIdForSchool,districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolId;
			}
		},
		errorHandler:handleError
		});	
	}
	return searchArray;
}

function getOnlyDistrictAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	if(document.getElementById("onlyDistrictName").value=="")
	{
		$("#avlbList").prop('disabled', true);
		$("#attachedList").prop('disabled', true);
	}
	document.getElementById("districtORSchoolName").value="";
	document.getElementById("districtHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyDistrictName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterOnlyArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterOnlyArray(districtOrSchoolName){
	
	var searchArray = new Array();
	//var entityID	=	document.getElementById("AEU_EntityType").value;
	//var districtId=	document.getElementById("districtSessionId").value;
	//alert("=========");

		UserAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function hideDistrictDiv(dis,hiddenId,divId)
{
	if(document.getElementById("districtHiddenId").value=="")
	{
		$("#avlbList").prop('disabled', true);
		$("#attachedList").prop('disabled', true);
	}
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				$('#districtORSchoolName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#districtORSchoolName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
			
			
			if(focus==0)
			$('#onlyDistrictName').focus();
			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
	resetJobCategories();
	$("#avlbList").prop('disabled', false);
	$("#attachedList").prop('disabled', false);
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function getSelectedJobCatsByIdForEdit()
{
	var jbdocid = document.getElementById('jobDocId').value;
	try
	{
		JobDocumentsByDistrictAjax.getSelectedJobCategory(jbdocid,{ 
			async: true,
			callback: function(data)
			{
				if(data!="")
				{
					document.getElementById('attachedList').innerHTML = data;
				}
				else
				{
					//alert("No Data");
				}
				
			},
			errorHandler:handleError 
		});
	}catch(e){alert(e);}
}

function getRemJobCatForEdit()
{
	var jbdocid = document.getElementById('jobDocId').value;
	try
	{
		JobDocumentsByDistrictAjax.getRemSelectedJobCategory(jbdocid,{ 
			async: true,
			callback: function(data)
			{
				if(data!="")
				{
					document.getElementById('avlbList').innerHTML = data;
				}
				else
				{
					//alert("No Data");
				}
				
			},
			errorHandler:handleError 
		});
	}catch(e){alert(e);}
}

function editJobDocs()
{
		validateAddEditDocument();
}

function showJobDocuments(teacherId)
{
	try
	{
		JobDocumentsByDistrictAjax.displayJobsDocuments(teacherId,noOfRows,page,sortOrderStr,sortOrderType,{ 
			async: true,
			callback: function(data)
			{
				if(data!="")
				{
					$('#loadingDiv').hide();					
					$('#documentsData').html(data);			
					applyScrollOnTbl();
				}
				else
				{
					//alert("No Data");
				}
				
			},
			errorHandler:handleError 
		});
	}catch(e){alert(e);}
}

function publishJobDocuments()
{
	try
	{
		JobDocumentsByDistrictAjax.showDocumentList({ 
			async: true,
			callback: function(data)
			{
				if(data!="")
				{
					$('#jobDocsList').html(data);						
				}
				else
				{
					//alert("No Data");
				}
				
			},
			errorHandler:handleError 
		});
	}catch(e){alert(e);}
}

function showOld()
{
	$('#newDocName').css("background-color","#eeeeee");
	$('#jobDocsList').css("background-color","white");
	document.getElementById('jobDocsList').disabled=false;
	document.getElementById('defaultDoc').checked=false;
	document.getElementById('newDocName').value="";
	document.getElementById('newDocName').disabled=true;
	document.getElementById('newDoc').checked=false;
	document.getElementById('defaultDoc').checked=true;

}
function hideOld()
{
	$('#newDocName').css("background-color","white");
	$('#jobDocsList').css("background-color","#eeeeee");
	document.getElementById('newDocName').value="";
	document.getElementById('newDocName').disabled=false;
	publishJobDocuments();
	document.getElementById('jobDocsList').disabled=true;
	document.getElementById('defaultJobDoc').innerHTML = "";
	document.getElementById('defaultDoc').checked=false;
	document.getElementById('docCaption').style.display="none";
	document.getElementById('docExpire').style.display="none";
	document.getElementById('expDateDiv').style.display="none";
	document.getElementById('defDoc').style.display="none";
	$('input:radio[name=expDoc]')[1].checked = true;
}
function showNewName()
{
	document.getElementById('docCaption').style.display="block";
	var selectVal = $("#jobDocsList option:selected").html();
	var id = $("#jobDocsList option:selected").val();
	if(id=="" || id==-1)
	{
		document.getElementById('newName').style.display = "none";
		document.getElementById('docExpire').style.display="none";
		document.getElementById('docExpFlag').style.display="none";
		document.getElementById('defDoc').style.display="none";
		document.getElementById('docExpFlag').value = "No";
		$('input:radio[name=expDoc]')[1].checked = true;
	}
	else
	{
		document.getElementById('newName').style.display = "block";
		document.getElementById('newName').value = selectVal;
		document.getElementById('docExpire').style.display="block";
		document.getElementById('docExpFlag').value = "No";
		showDefaultDocumentName(id);
	}
}
function showExpDate(expVal)
{
	if(expVal == "Yes")
	{
		document.getElementById('expDateDiv').style.display="block";
		document.getElementById('expDate').value = "";
		document.getElementById('docExpFlag').value = expVal;
	}
	else if(expVal="No")
	{
		document.getElementById('expDateDiv').style.display="none";
		document.getElementById('docExpFlag').value = expVal;
	}
}
function showDefaultDocumentName(selectVal)
{
	JobDocumentsByDistrictAjax.showDefaultDocumentName(selectVal,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!="")
			{
				document.getElementById('defDoc').style.display="block";
				document.getElementById('defaultJobDoc').innerHTML = data;
				document.getElementById('defaultDocument').value = data;
			}
		}
	});
}
function showDefaultDocument()
{
	var url=$('#defaultJobDoc').html();
	if(url.indexOf("www") > -1){
		if(!(url.indexOf("http") > -1)){
			alert("true")
			url="http://"+url;
		}
		$("#defdocLink").attr("href", url);
		$("#defdocLink").attr("target", "_blank");
		
	}else{
		$("#defdocLink").attr("href", "javascript:void(0)");
		//$("#defdocLink").attr("target", "");
		var selectVal = $("#jobDocsList option:selected").val();
		JobDocumentsByDistrictAjax.viewDefaultDocument(selectVal,{ 
					async: true,
					errorHandler:handleError,
					callback:function(data)
					{
						if (data.indexOf(".doc") !=-1) {
						    document.getElementById('ifrmDocument').src = ""+data+"";
						}
						else
						{
							document.getElementById("hrefDoc").href = data;
							$("#hrefDoc").trigger("click");
							return false;
						}
						
						if(deviceType)
						{
							if (data.indexOf(".doc")!=-1)
							{
								$("#docfileNotOpen").css({"z-index":"3000"});
						    	$('#docfileNotOpen').show();
							}
							else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								$("#exelfileNotOpen").css({"z-index":"3000"});
						    	$('#exelfileNotOpen').show();
							}
							else
							{
								document.getElementById("hrefDoc").href = data;
							}
						}
						else if(deviceTypeAndroid)
						{
							if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
							{
								document.getElementById('ifrmDocument').src = ""+data+"";
							}
							else
							{
								document.getElementById("hrefDoc").href = data;	
							}	
						}
						else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
						{
							document.getElementById('ifrmDocument').src = ""+data+"";
						}
						else
						{
							document.getElementById("hrefDoc").href = data;	
						} 
						return false;
						
					}
				});
	}
	
}

function checkDistrict(){
	if(document.getElementById("districtHiddenId").value=="")
	{
		$("#avlbList").prop('disabled', true);
		$("#attachedList").prop('disabled', true);
	}
}

function showDefDocs(defDocName)
{
	JobDocumentsByDistrictAjax.viewDefaultDocument(defDocName,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if (data.indexOf(".doc") !=-1) {
					    document.getElementById('ifrmDocument').src = ""+data+"";
					}
					else
					{
						document.getElementById("hrefDoc").href = data;
						$("#hrefDoc").trigger("click");
						return false;
					}
					
					if(deviceType)
					{
						if (data.indexOf(".doc")!=-1)
						{
							$("#docfileNotOpen").css({"z-index":"3000"});
					    	$('#docfileNotOpen').show();
						}
						else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
						{
							$("#exelfileNotOpen").css({"z-index":"3000"});
					    	$('#exelfileNotOpen').show();
						}
						else
						{
							document.getElementById("hrefDoc").href = data;
						}
					}
					else if(deviceTypeAndroid)
					{
						if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
						{
							document.getElementById('ifrmDocument').src = ""+data+"";
						}
						else
						{
							document.getElementById("hrefDoc").href = data;	
						}	
					}
					else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmDocument').src = ""+data+"";
					}
					else
					{
						document.getElementById("hrefDoc").href = data;	
					} 
					return false;
					
				}
			});
}

function viewMultipleJobDoc(documentName,jobCategoryId)
{
	JobDocumentsByDistrictAjax.showMultipleDocuments(documentName,jobCategoryId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!="")
			{
				$("#multipleJobDoc").modal('show');
				document.getElementById('multipleDocData').innerHTML = data;
				applyScrollOnTblMulti();
			}
		}
	});
}


//uploadedFiles
function uploadLink(){
	$('#errordiv').empty();
	$('#jobdocumentLink').css("background-color",'');
	//var message="";
	var uploadLink=trim(document.getElementById('jobdocumentLink').value);
	var myRegExp =/(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
	
	
	if(uploadLink.length==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrURL+"<br>");
		$('#jobdocumentLink').focus();
		$('#jobdocumentLink').css("background-color",txtBgColor);
	}else if (!myRegExp.test(uploadLink)){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrValidURL+"<br>");
		$('#jobdocumentLink').focus();
		$('#jobdocumentLink').css("background-color",txtBgColor);
	}else{
		uploadedFiles.push(uploadLink);
		displayNewUploadedDocuments(uploadedFiles);
	}
		
	
}

   function getDocsDiv(){
	   var districtHiddenId =	document.getElementById("districtHiddenId").value;
		try
		{
		JobDocumentsByDistrictAjax.getDocsDiv(districtHiddenId,flag,{
			async: false,
			callback: function(data)
			{
			  document.getElementById('tempdocDiv').innerHTML=data;
			  applyScrollOnTblDocs();
			},
		  errorHandler:handleError 
		 });
		 } 
		 catch(e){alert(e);}
	}
