var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

var txtBgColor="#F5E7E1";
var page1 = 1;
var noOfRows1 = 10;
var sortOrderStr1="";
var sortOrderType1="";


var page2 = 1;
var noOfRows2 = 10;
var sortOrderStr2="";
var sortOrderType2="";

var page3 = 1;
var noOfRows3 = 10;
var sortOrderStr3="";
var sortOrderType3="";

function getPaging(pageno,gridName)
{	
	if(gridName=="candidatesGrid"){

		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize1").value;
		//showGridSubjectAreasGrid();
		searchTeacher();
	}else if(gridName=="prosGrid"){
		if(pageno!='')
		{
			page1=pageno;	
		}
		else
		{
			page1=1;
		}
		noOfRows1 = document.getElementById("pageSize2").value;
		//showGridSubjectAreasGrid();
		showselgrid("selPros");
	}
	else if(gridName=="participantsGrid"){
		if(pageno!='')
		{
			page2=pageno;	
		}
		else
		{
			page2=1;
		}
		noOfRows2 = document.getElementById("pageSize3").value;
		participantsGrid();	
	}
	else if(gridName=="tempGrid"){
		if(pageno!='')
		{
			page3=pageno;	
		}
		else
		{
			page3=1;
		}
		noOfRows3 = document.getElementById("pageSize4").value;
		displaytempGrid();
	}
}
function getPagingAndSortingCommonMul(pageno,sortOrder,sortOrderTyp,gridName)
{
	if(gridName=="candidatesGrid")
	{

		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRows = document.getElementById("pageSize1").value;
		}else{
			noOfRows=10;
		}
		searchTeacher();	
	}else if(gridName=="prosGrid"){


		if(pageno!=''){
			page1=pageno;	
		}else{
			page1=1;
		}
		sortOrderStr1	=	sortOrder;
		sortOrderType1	=	sortOrderTyp;
		if(document.getElementById("pageSize2")!=null){
			noOfRows1 = document.getElementById("pageSize2").value;
		}else{
			noOfRows1=10;
		}
		showselgrid("selPros");	
	
	}
	else if(gridName=="participantsGrid"){


		if(pageno!=''){
			page2=pageno;	
		}else{
			page2=1;
		}
		sortOrderStr2	=	sortOrder;
		sortOrderType2	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRows2 = document.getElementById("pageSize3").value;
		}else{
			noOfRows2=10;
		}
		//participantsGrid();	
	}
	else if(gridName=="tempGrid"){
		if(pageno!=''){
			page3=pageno;	
		}else{
			page3=1;
		}
		sortOrderStr3	=	sortOrder;
		sortOrderType3	=	sortOrderTyp;
		if(document.getElementById("pageSize4")!=null){
			noOfRows3 = document.getElementById("pageSize4").value;
		}else{
			noOfRows3=10;
		}
		displaytempGrid();		
	}
}

function participantsGrid(){
	$('#loadingDiv').modal('show');
	var eventId=document.getElementById("eventId").value;	
	ParticipantsAjax.displayParticipantsGrid(noOfRows2,page2,sortOrderStr2,sortOrderType2,eventId,{ 
		async: true,
		callback: function(data){		
		document.getElementById("partgrid").innerHTML=data;
		applyScrollOnTbl();
		$('#loadingDiv').modal('hide');
		},
		errorHandler:handleError 
	});
	$('#loadingDiv').modal('hide');
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function showSelectionGrid(){
	$('#loadingDiv').modal('show');
	$("#partselectiongrid").show();	
	$("#selCand").focus();
	$('#loadingDiv').modal('hide');
}
function clearAll(){
	
	$("#selCandDis").hide();
	$("#selCandDisData").html("");
	$("#selCandDisData").html("");
	$("#selProsdDis").hide();
	$('#selImportDis').modal('hide');
	$("#addProsdDis").hide();
	$(".disButtons").hide();
	$("#partselectiongrid").hide();	
}
function showselgrid(result){
	$('#addProsError').empty();
	setDefColortoErrorMsgColor();
	document.getElementById("fName").value='';
	document.getElementById("lName").value='';
	document.getElementById("emailAddress1").value='';
	
	$('#errorDivCandidate').empty();
	
	document.getElementById("pritOfferReadyDataTableDiv").innerHTML="";	
	$("#tempIdopen").hide();
	
	$("#selCandDis").hide();
	$("#selCandDisData").html("");
	$("#selCandDisData").html("");
	$("#selProsdDis").hide();
	$('#selImportDis').modal('hide');
	//$("#addProsdDis").hide();
	$('#AddNewPros').modal('hide');
	$(".disButtons").hide();
	$("#gridVal").val("");
	if(result=="selCand"){
		$("#selCandDis").show();
		$(".disButtons").show();
		$("#gridVal").val(1);
	}
	/*if(result=="selPros"){
		var	firstName        =	trim(document.getElementById("firstNamePros").value);
		var	lastName         =	trim(document.getElementById("lastNamePros").value);
		var	emailAddress     =	trim(document.getElementById("emailAddressPros").value);
		
		$('#loadingDiv').modal('show');	
		ParticipantsAjax.displayCandidatesOnPros(noOfRows1,page1,sortOrderStr1,sortOrderType1,firstName,lastName,emailAddress,{ 
			async: true,
			callback: function(data){			
			document.getElementById("selProsDisData").innerHTML=data;
			applyScrollOnProsTbl();
			$("#selProsdDis").show();
			$('#loadingDiv').modal('hide');
			},
			errorHandler:handleError 
		});
		$(".disButtons").show();
	}*/
	if(result=="selImport"){
		$("#gridVal").val(3);
		$('#selImportDis').modal('show');
		$(".disButtons").show();
	}
	if(result=="addPros"){
		$("#gridVal").val(4);
		//$("#addProsdDis").show();
		$('#AddNewPros').modal('show');
		$("#fName").focus();
		$(".disButtons").show();
	}	
}

function prospectsTeacher(){
	$('#loadingDiv').modal('show');
	ParticipantsAjax.displayCandidatesOnPros(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){			
		document.getElementById("selProsDisData").innerHTML=data;
		applyScrollOnProsTbl();
		$("#selProsdDis").show();
		$('#loadingDiv').modal('hide');
		},
		errorHandler:handleError 
	});
	$('#loadingDiv').modal('hide');
}

function searchTeacher(){
	$('#errorDivCandidate').empty();
	var	firstName        =	trim(document.getElementById("firstName").value);
	var	lastName         =	trim(document.getElementById("lastName").value);
	var	emailAddress     =	trim(document.getElementById("emailAddress").value);

	var jobId = 0;
	$('#loadingDiv').modal('show');
	if($('#selJobCan').is(':checked')){
		if(document.getElementById("searchJob").value!="" && document.getElementById("searchJob").value!=0){
				jobId =document.getElementById("searchJob").value;
				document.getElementById('canError').style.display = "none";
				$('#loadingDiv').modal('hide');
		}else{
			document.getElementById('canError').innerHTML="&#149;"+resourceJSON.PlzEtrJobID+"";
			$('#loadingDiv').modal('hide');
			return false;
		}
	}
	
	ParticipantsAjax.displayCandidatesOnSearch(noOfRows,page,sortOrderStr,sortOrderType,jobId,firstName,lastName,emailAddress,{ 
		async: true,
		callback: function(data){
		document.getElementById("selCandDisData").innerHTML=data;
		applyScrollOnCandidateTbl();
		$('#loadingDiv').modal('hide');
		},
		errorHandler:handleError 
	});
}

function setDefColortoErrorMsgColor()
{
	$('#fName').css("background-color","");
	$('#lName').css("background-color","");
	$('#emailAddress1').css("background-color","");
	$('#errorDivCandidate').empty();
}

function addProsCand(){
	$('#addProsError').empty();
	setDefColortoErrorMsgColor();
	$('#emailAddress1').css("background-color", "##FFFFFF");
	
	var fName = trim(document.getElementById("fName").value);
	var lName = trim(document.getElementById("lName").value);
	var emailAddress = trim(document.getElementById("emailAddress1").value);
	
	var cnt=0;
	var focs=0;	
	
	if(fName=="")
	{
		$('#addProsError').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#fName').focus();
		$('#fName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(lName=="")
	{
		$('#addProsError').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lName').focus();

		$('#lName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(emailAddress=="")
	{
		$('#addProsError').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress1').focus();  
		
		$('#emailAddress1').css("background-color",txtBgColor);
		cnt++;focs++;
	}else if(!isEmailAddress(emailAddress))
	{	
		$('#addProsError').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress1').focus();
		
		$('#emailAddress1').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt>0)
	  return false;
	
	var eventId=$("#eventId").val();
	
	ParticipantsAjax.addTempProspectCand(fName,lName,emailAddress,eventId,{ 
		async: true,
		callback: function(data){
		//alert("@@@ :: "+data);
			if(data==1){
				document.getElementById('addProsError').innerHTML="&#149;  "+resourceJSON.MsgEmailAddressAlreadyExist+"";
				$('#loadingDiv').modal('hide');
			}else if(data==5){
				$('#addProsError').empty();
				$('#addProsError').append("&#149; "+resourceJSON.MsgParticipantIsAlreadyAttached+"<br>");
			}else if(data==6){
				$('#addProsError').empty();
				$('#addProsError').append("&#149;"+resourceJSON.MsgInviteForVideoInterview+"<br>");
			}else if(data==7){
				$('#addProsError').empty();
				$('#addProsError').append("&#149;"+resourceJSON.MsgSingleParticipantCanBeAdded+"<br>");
			}else {
				// participantsGrid();
				 participantsGridNew();
				// checkForRecord();
				 //clearAll();
				 document.getElementById("fName").value='';
				 document.getElementById("lName").value='';
				 document.getElementById("emailAddress").value='';
				 $('#errorDivCandidate').empty();
				$('#saveDataLoading').modal("show");
				$('#AddNewPros').modal('hide');
			}
		},
		errorHandler:handleError 
	});
}

function saveParticipant(fName,lName,email,nScore,tid){
	
	//$('#loadingDiv1').fadeIn();
	var eventId=$("#eventId").val();
	if(document.getElementById(tid).checked ==true){
	$('#loadingDiv').modal('show');
	ParticipantsAjax.saveParticipant(fName,lName,email,nScore,eventId,tid,{ 
		async: true,
		callback: function(data){
		//alert("data :: "+data)
			if(data==5){
				$('#errorDivCandidate').empty();
				$('#errorDivCandidate').append("&#149; "+resourceJSON.MsgParticipantIsAlreadyAttached+".<br>");
			}else if(data==6){
				$('#errorDivCandidate').empty();
				$('#errorDivCandidate').append("&#149; "+resourceJSON.MsgSingleParticipantCanBeAdded+"<br>");
			}
			//participantsGrid();
			participantsGridNew();
			//checkForRecord();
			//clearAll();
			//searchTeacher();
			$('#loadingDiv').modal('hide');
			//$('#loadingDiv1').hide();
		},
		errorHandler:handleError 
	});
	
 }
	//$('#loadingDiv').modal('hide');
	$('#loadingDiv1').hide();
}

function saveCandidate(chk){
	$('#errorDivCandidate').empty();
	//fName,lName,email,nScore,tid
	//$('#loadingDiv1').fadeIn();
	var eventId=$("#eventId").val();
//	if(document.getElementById(tid).checked ==true){
	//$('#loadingDiv').modal('show');
	var teacherId ;
	
	if(chk=='0')
		teacherId = trim(document.getElementById("searchTxtId").value); 
	else if(chk=='1')
		teacherId = trim(document.getElementById("participantId").value);
	
	ParticipantsAjax.saveCandidate(teacherId,eventId,{ 
		async: true,
		callback: function(data){
			if(data==5){
				$('#errorDivCandidate').append("&#149; "+resourceJSON.MsgParticipantIsAlreadyAttached+"<br>");
			}else if(data==6){
				$('#errorDivCandidate').append("&#149; "+resourceJSON.MsgSingleParticipantCanBeAdded+"<br>");
			}if(data==1)
			{
				$('#errorDivCandidate').append("&#149; "+resourceJSON.MsgEmailAddressAlreadyExist+"<br>");
			}
			else
			{
				if(chk=='0')
				{
					document.getElementById("searchTxt").value="";
					document.getElementById("searchTxtId").value="";
				}
				else if(chk=='1')
				{
					document.getElementById("participantTxt").value="";
					document.getElementById("participantId").value="";
				}
			}
			
			
			//participantsGrid();
			participantsGridNew();
			//checkForRecord();
			//clearAll();
			//searchTeacher();
//			$('#loadingDiv').modal('hide');
			//$('#loadingDiv1').hide();
		},
		errorHandler:handleError 
	});
	
 //}
	//$('#loadingDiv').modal('hide');
//	$('#loadingDiv1').hide();
}





function deleteParticipant(delPart){
	$('#errorDivCandidate').empty();
	$('#deletePartDiv').modal('show');
	$('#delteId').val(delPart);
}
function doneDelete(){
	$('#loadingDiv').modal('show');
	var participantDel = $('#delteId').val();
	ParticipantsAjax.delParticipant(participantDel,{ 
		async: true,
		callback: function(data){
		
			//participantsGrid();
			participantsGridNew();
			checkForRecord();
			$('#deletePartDiv').modal('hide');
			$('#loadingDiv').modal('hide');
		},
		errorHandler:handleError 
	});
	$('#deletePartDiv').modal('hide');
}

function validateFacilitatorFile()
{
	var facilitatorfile	=	document.getElementById("facilitatorfile").value;
	var errorCount=0;
	var aDoc="";
	$('#errorDivImport').empty();
	if(facilitatorfile==""){
		$('#errorDivImport').show();
		$('#errorDivImport').append("&#149; "+resourceJSON.msgXlsXlsxFile+"<br>");
		errorCount++;
		aDoc="1";
	}
	else if(facilitatorfile!="")
	{
		
		var ext = facilitatorfile.substr(facilitatorfile.lastIndexOf('.') + 1).toLowerCase();	
	
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("facilitatorfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("facilitatorfile").files[0].size;
			}
		}
		
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errorDivImport').show();
			$('#errorDivImport').append("&#149; "+resourceJSON.msgXlsXlsxFileOnly+"<br>");
			errorCount++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errorDivImport').show();
			$('#errorDivImport').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			errorCount++;
			aDoc="1";	
		}
	
	}
	
	if(aDoc==1){
		
		$('#facilitatorfile').focus();
		$('#facilitatorfile').css("background-color", "#F5E7E1");
	}
	
	if(errorCount==0){
		try{
			//$('#loadingDiv').show();
			if(facilitatorfile!=""){
				document.getElementById("facilitatorUploadServlet").submit();
				//return false;
			}
		}catch(err){alert(err)}
		
	}else{		
		$('#errorDivImport').show();
		return false;
	}
}
function uploadDataFacilitator(fname,sId){
	var eventId=$("#eventId").val();
	$('#loadingDiv').modal('show');
	ParticipantsAjax.saveImportList(fname,sId,eventId,{ 
		async: true,
		callback: function(data){
		//errorDivImport
		if(data[0]=="rowError"){
			//errorDivImport
			document.getElementById("errorDivImport").innerHTML="&#149; "+data[1];
		}else if(data[0]=="recError"){
			$("#dataImpotDiv").hide();
			$("#tempIdopen").show();
			displaytempGrid();
		}		
		$('#loadingDiv').modal('hide');
		},
		errorHandler:handleError 
	});
}

function displaytempGrid(){
	$('#loadingDiv').show();
	ParticipantsAjax.displayTempGrid(noOfRows3,page3,sortOrderStr3,sortOrderType3,{ 
		async: true,
		callback: function(data){
		$('#loadingDiv').hide();
		$('#dataImpotDiv').modal('hide');
		$('#selImportDis').modal('hide');
		$('#printOfferReadyDiv').modal('show');
		document.getElementById("pritOfferReadyDataTableDiv").innerHTML=data;
		
		applyScrollOnTempTbl();
		},
		errorHandler:handleError 
	});
}

function acceptImport(){
	$('#loadingDiv').show();
	var eventId=$("#eventId").val();
	ParticipantsAjax.acceptImport(eventId,{ 
		async: true,
		callback: function(data){
		if(data==2){
			$("#printOfferReadyDiv").modal('hide');
			$("#importProspectDiv").modal('hide');
			//$(".tempMsg").html("Import records successfully");
		    //$("#saveDataLoading").modal("show");
			//$("#mainDivOpen").show();
			//$("#tempIdopen").hide();
			//participantsGrid();
			participantsGridNew();
		}
		},
		errorHandler:handleError 
	});
}
function clearAllTempRec(){
	
	var eventId=$("#eventId").val();
	ParticipantsAjax.clearAllTempRec(eventId,{ 
		async: true,
		callback: function(data){
		if(data==2){
			$("#printOfferReadyDiv").modal('hide');
			/*$(".tempMsg").html("Import records successfully");
			$("#saveDataLoading").modal("show");*/
			$("#mainDivOpen").show();
			$("#tempIdopen").hide();
			//participantsGrid();
			participantsGridNew();
		}
		},
		errorHandler:handleError 
	});
}

function checkForRecord()
{
$("#addParticipantlink").show();
var eventId=$("#eventId").val();	
var formatId=$("#formatId").val();
if(formatId==1){
ParticipantsAjax.checkCandidates(eventId,{
async: true,
callback: function(data){	
if(data!=0){
$("#addParticipantlink").hide();	
}}});	
}}
//tempMsg


// mukesh code

function  checkPageRedirect()
{
	var formatId = document.getElementById('formatId').value;
	var eventId = document.getElementById('eventId').value;
	if(formatId==1)
	{
  	  window.location.href="eventschedule.do?eventId="+eventId;	
	}else
	{	
	  window.location.href="managefacilitators.do?eventId="+eventId;	
	}
	
}

function exitfrompage(){
	window.location.href="manageevents.do";	
}

function sendEmails()
{	
$('#loadingDiv').fadeIn();
var eventId=$("#eventId").val();
try{
		ParticipantsAjax.sendEmail(eventId,
	    {
			async: true,
			callback: function(data){ 
			  $('#loadingDiv').hide();
			  if(data==false){
				 $('#errSendInviteDiv').show();
				 $('#errSendInviteDiv').empty();
				 $('#errSendInviteDiv').append("&#149;"+resourceJSON.msgAddfacilators+"<br>");
			  }else{
				  $('#sendEmailtoParticipants').modal('show');
			  }			
		}		
	     });	
	}catch(e)
	{
	 alert(e);	
	}
}

/////////////// send Invite //////////////
function sendInvite()
{
	var participantId = $("#participantIds").val();
	
	$('#loadingDiv').show();
	CGInviteInterviewAjax.saveIIStatusForEvent(participantId,{
		async: true,
		callback: function(data)
		{	
			$('#loadingDiv').hide();
			if(data==1)
			{
				$("#errSendInviteDiv").show();
				$("#errSendInviteDiv").html(""+resourceJSON.PlzAddOneOrMoreParticipants+"");
			}else if(data==2){
				$("#errSendInviteDiv").show();
				$("#errSendInviteDiv").html(""+resourceJSON.cannotResendVVIInvitaion+"");
			}
			else
			{
				$("#errSendInviteDiv").hide();
				 $('#sendEmailtoParticipants').show();
				 document.getElementById("succesEmailMsg").innerHTML=""+resourceJSON.MsgGreatEventScheduled+""
			}
			
			//alert("Success");
		}
	});	
}

function checkInviteInterviewButton()
{
	var eventId = $("#eventId").val();
	
	ParticipantsAjax.checkInviteInterviewButton(eventId,{
		async: true,
		callback: function(data)
		{	
			if(data==true)
				$("#sendInviteBtn").hide();
			else if(data==false)
				$("#sendInviteBtn").show();
		}
	});
	
	
}
function hideForm()
{
	$('#addProsError').empty();
	document.getElementById("fName").value='';
	document.getElementById("lName").value='';
	document.getElementById("emailAddress1").value='';
	
	$('#fName').css("background-color", "");
	$('#lName').css("background-color", "");
	$('#emailAddress1').css("background-color", "");
	$("#AddNewPros").modal("hide");
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13));
}

function searchCandidateFromProspects()
{
	page1=1;
	showselgrid("selPros");
}



/********************************** district auto completer ***********************************/
/*
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;

function getRefreshDistrictName(){
	 count=0;
	 index=-1;
	 length=0;
	 divid='';
	 txtid='';
	 hiddenDataArray = new Array();
	 showDataArray = new Array();
	 degreeTypeArray = new Array();
	 hiddenId="";
	 districtNameFilter=0;
}

function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	getRefreshDistrictName();
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
			document.getElementById("searchTxt").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(searchTxt){
	var searchArray = new Array();
	//var jobId =document.getElementById("searchJob").value;
	
	ParticipantsAjax.getJobAppliedTeachers(searchTxt,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].firstName+" "+data[i].lastName+" "+data[i].emailAddress;
			showDataArray[i]=data[i].firstName+" "+data[i].lastName+" "+data[i].emailAddress;
			hiddenDataArray[i]=data[i].teacherId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			alert("01");
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			alert("02 hiddenId :: "+hiddenId);
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			alert("03");
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			alert("04 hiddenId :: "+hiddenId);
			dis.value=showDataArray[index];
			
		}
		
	}else{
		alert("05 :: hiddenId :: "+hiddenId);
			if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}*/

function clearData(chk)
{
	$('#errorDivCandidate').empty();
	if(chk=='0')
	{
		document.getElementById("searchTxt").value="";
		document.getElementById("searchTxtId").value="";
	}
	else if(chk=='1')
	{
		document.getElementById("participantTxt").value="";
		document.getElementById("participantId").value="";
	}
}

function chkVideo()
{
	var eventTypeId = document.getElementById("eventTypeId").value;

	if(eventTypeId=='1')
	{
		$("#impCan").hide();
		$("#impPros").hide();
	}
	else
	{
		$("#impCan").show();
		$("#impPros").show();
	}
		
}


/***********************************************Auto Suggest of Candidate****************************************************************/
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(searchTxt){
	var searchArray = new Array();
	var eventId = trim(document.getElementById("eventId").value);
	ParticipantsAjax.getJobAppliedTeachers(searchTxt,eventId,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].firstName+" "+data[i].lastName+" ("+data[i].emailAddress+")";
			showDataArray[i]=data[i].firstName+" "+data[i].lastName+" ("+data[i].emailAddress+")";
			hiddenDataArray[i]=data[i].teacherId;
		}
	},
		errorHandler:handleError 
	});	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

/************************************************************************************************************************************/

/*****************************************************Auto Suggest of Prospects******************************************************/
/*function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


function getDistrictMasterArray(searchTxt){
	var searchArray = new Array();
	ParticipantsAjax.getJobAppliedTeachers(searchTxt,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].firstName+" "+data[i].lastName+" ("+data[i].emailAddress+")";
			showDataArray[i]=data[i].firstName+" "+data[i].lastName+" ("+data[i].emailAddress+")";
			hiddenDataArray[i]=data[i].teacherId;
		}
	},
		errorHandler:handleError 
	});	

	return searchArray;
}*/

function getProspectAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("quesName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//searchArray = getDistrictMasterArray(txtSearch.value);
		searchArray = getPropectsArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


function getPropectsArray(districtOrSchoolName)
{
	var searchArray = new Array();
	var eventId = trim(document.getElementById("eventId").value);
		
	ParticipantsAjax.getPropectsArray(districtOrSchoolName,eventId,{ async: false,callback: function(data)
		{
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++)
			{
				searchArray[i]=data[i].firstName+" "+data[i].lastName+" ("+data[i].emailAddress+")";
				showDataArray[i]=data[i].firstName+" "+data[i].lastName+" ("+data[i].emailAddress+")";
				hiddenDataArray[i]=data[i].teacherId;
			}
		},
		});
	return searchArray;
}

function hideProspectDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


function showselgridImport(){
	$('#importProspectDiv').modal('show');
}





function participantsGridNew(){
	$('#loadingDiv').fadeIn();
	var eventId=document.getElementById("eventId").value;	
	ParticipantsAjax.displayParticipantsGridNew(eventId,{ 
		async: true,
		callback: function(data){		
		document.getElementById("participantsNewDiv").innerHTML=data.split("####")[0];
		var cnt = data.split("####")[1];
		for(i=1;i<cnt;i++)
		{
			$('#nsId'+i+'').tooltip();
			$('#rmId'+i+'').tooltip();
		}
		//applyScrollOnTbl();
		$('#loadingDiv').hide();
		},
		//errorHandler:handleError 
	});
	$('#loadingDiv').hide();
}
/************************************************************************************************************************************/