function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signincode.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function checkForInt(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

var txtBgColor="#F5E7E1";
function validateExamCode()
{	
	$('#examinationCode').css("background-color","");
	$('#examData').html("");
	$('#examData').hide();
	var cnt=0;
	var examinationCode=trim(document.getElementById("examinationCode").value);
	$('#divExternalErrorMsg').empty();
	if(examinationCode=="")
	{
		$('#divExternalErrorMsg').append("&#149; "+resourceJSON.PlzEtrExamCode+"<br>");
		$('#examinationCode').focus();
		$('#examinationCode').css("background-color",txtBgColor);
		cnt++;
	}
	
	if(cnt!=0)		
	{
		$('#divExternalErrorMsg').show();
	}
	else
	{
		ExamAjax.validateExamCode(examinationCode,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
			if(data=="1")
			{
				$('#divExternalErrorMsg').empty();
				$('#divExternalErrorMsg').append("&#149; "+resourceJSON.msgExamCodeNotMatched+"<br>");
				$('#divExternalErrorMsg').show();
				
				$('#examinationCode').focus();
				$('#examinationCode').css("background-color",txtBgColor);
			}
			else if(data=="2")
			{
				$('#divExternalErrorMsg').empty();
				$('#divExternalErrorMsg').append("&#149; "+resourceJSON.msgExamCodeIsNotActive+"<br>");
				$('#divExternalErrorMsg').show();
				
				$('#examinationCode').focus();
				$('#examinationCode').css("background-color",txtBgColor);
			}
			else
			{
				$('#examData').html(data);
				$('#examData').show();
			}
			}
		});	
	}
}


function startExam(teacherId,jobId)
{	
	alert(""+resourceJSON.msgStartExamForTeacher+" "+teacherId+" "+resourceJSON.msgAndJobID+" "+jobId);
}
