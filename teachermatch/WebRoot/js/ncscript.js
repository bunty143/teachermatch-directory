function getDistrictSettings(){
	
	//$('#loadingDiv').show();
	$('#divMain').html("");
	var districtId = document.getElementById("districtId").value;
	ScriptAjax.getDistrictSettings(districtId,{ 
		async: false,		
		callback: function(data){
		$('#divMain').html(data);
		///$('#loadingDiv').hide();
	}
	});	
}

function copyDistrictSettings(){
	var districtId = document.getElementById("districtId").value;
	var arr =[];
	var jobCat = document.getElementsByName("jobCat");
	var dspq = document.getElementsByName("dspq");
	var qq = document.getElementsByName("qq");
	var jsi = document.getElementsByName("jsi");
	var slc = document.getElementsByName("slc");
	var eReference = document.getElementById('eReference').checked;	
	var slcDeafult1 = document.getElementById('slcDefault').checked;	
	for (var i = 0; i < jobCat.length; i++) {
		if(jobCat[i].checked==true)
		{
			arr.push({ 
				"jobCategoryId"  : jobCat[i].value,
				"dspq"      : dspq[i].value,
				"qq"       : qq[i].value,
				"jsi" : jsi[i].value,
				"slc" : slc[i].value			
			});
		}
	}
	
	//alert(JSON.stringify(arr));
	var jsonStr = JSON.stringify(arr);
	ScriptAjax.copyDistrictSettings(districtId,jsonStr,eReference,slcDeafult1,{ 
		async: false,		
		callback: function(data){
		alert(data);
		getDistrictSettings();
	}
	});	

}