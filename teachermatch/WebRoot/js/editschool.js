/* @Author: Gagan 
 * @Discription: editschool js .
 */	
var keyContactDivVal=2;
var keyContactIdVal=null;
/*========= Sorting on Notes ==============*/
var page 			=	1;
var noOfRows 		= 	10;
var sortOrderStr	=	"";
var sortOrderType	=	"";

var pageDist 			= 	1;
var noOfRowsDist 		= 	10;
var sortOrderStrDist	=	"";
var sortOrderTypeDist	=	"";

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayNotes();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
/*======== Redirect to Manage User Page ===============*/
function cancelSchool()
{
	window.location.href="manageschool.do";
}

function addAdministrator(roleId)
{
	$('#errorschooldiv').empty();
	$('#firstName').css("background-color", "");
	$('#lastName').css("background-color", "");
	$('#emailAddress').css("background-color", "");
	dwr.util.setValues({firstName:null,lastName:null,title:null,emailAddress:null,phone:null,mobileNumber:null});
	$("#addAdministratorDiv").fadeIn();
	$('#roleId').val(roleId);
	$('#salutation').focus();
}

function uploadAssessment()
{
	$("#uploadAssessmentDiv").fadeIn();
	
	if(document.getElementById("cbxUploadAssessment").checked==false)
	{
		$("#uploadAssessmentDiv").hide();
	}
}

/*================== Notes Functionality =========================*/
function addNotes()
{
	$("#addNotesDiv").fadeIn();
	$('#errorschoolnotediv').empty();
	$('#sNote').find(".jqte_editor").css("background-color", "");
	$('#sNote').find(".jqte_editor").focus();
	$('#sNote').find(".jqte_editor").html("");
	
}
function displayNotes()
{
	dwr.util.setValues({note:null});
	var schoolId			=	document.getElementById("schoolId").value;
	SchoolAjax.displayNotesGrid(schoolId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#schoolNotesGrid').html(data);
			applyScrollOnTblNotes();
			//$('#schoolNotesTable').html(data);
			$('#addNotesDiv').hide();
		},
	errorHandler:handleError
	});
}

function saveNotes()
{
	var note								=	trim($('#sNote').find(".jqte_editor").html())
	var schoolId							=	trim(document.getElementById("schoolId").value);
	
	if($('#sNote').find(".jqte_editor").text().trim()==""){
		$('#errorschoolnotediv').show();	
		$('#errorschoolnotediv').empty();
		$('#errorschoolnotediv').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
		$('#sNote').find(".jqte_editor").focus();
		$('#sNote').find(".jqte_editor").css("background-color", "#F5E7E1");
		return false;
	}else if($('#sNote').find(".jqte_editor").text().trim()!=""){
		var charCount=trim($('#sNote').find(".jqte_editor").text());
		var count = charCount.length;
		if(count>500){
			$('#errorschoolnotediv').show();	
			$('#errorschoolnotediv').empty();
			$('#errorschoolnotediv').append("&#149; "+resourceJSON.msgNotesLen500+"<br>");
			$('#sNote').find(".jqte_editor").focus();
			$('#sNote').find(".jqte_editor").css("background-color", "#F5E7E1");
			return false;
		}
	}
	$('#errorschoolnotediv').empty();
	
	SchoolAjax.saveNotes(note,schoolId, { 
		async: true,
		callback: function(data)
		{
			displayNotes();
		},
		errorHandler:handleError
	});
}

function deleteNotes(notesId)
{
	$('#note').css("background-color", "");
	$('#errorschoolnotediv').empty();
	if (confirm( resourceJSON.msgdeleteNotes1 )) {
		SchoolAjax.deleteNotes(notesId, { 
			async: true,
			callback: function(data)
			{
				displayNotes();
			},
			errorHandler:handleError
		});
	}
}
function showFile(flagVal,districtORSchoool,districtORSchooolId,docFileName,linkId)
{
	if(docFileName!=null && docFileName!=''){
		SchoolAjax.showFile(districtORSchoool,districtORSchooolId,docFileName,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				if(data==""){			
					//alert("Job Specific Inventory is not uploaded.")
				}else{
					if(flagVal==1){
						document.getElementById("showLogo").innerHTML="<img src=\""+data+"\">";
					}else if(data.indexOf(".doc")!=-1)
					{
						document.getElementById("iframeJSI").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data; 
						return false;
					}
					
				}
			}
		});
	}
}
function clearFile(val){
	// line added by ashish ratan for IE 10-11
	// replace field with old html
	// comment by ashish ratan
		
	if(val==1){
		$('#logoPathFile')
		.replaceWith(
				"<input id='logoPathFile' name='logoPathFile' type='file' width='20px;' />");	 
		
		document.getElementById("logoPathFile").value="";
	}else{
		$('#assessmentUploadURLFile')
		.replaceWith(
				"<input id='assessmentUploadURLFile' name='assessmentUploadURLFile' type='file' width='20px;' />");	 
		
		
		document.getElementById("assessmentUploadURLFile").value="";
	}
}
function clearNotes()
{
	$("#addNotesDiv").hide();
	$('#note').css("background-color", "");
	$('#errorschoolnotediv').empty();
}
/*============================= School Administrator and Analyst Functionality   =================================================*/
/*============ Adding District User's Administrator and Analyst ========================*/
function validateSchoolAdministratorOrAnalyst()
{
	$('#loadingDiv').fadeIn();
	$('#errorschooldiv').empty();
	$('#firstName').css("background-color", "");
	$('#lastName').css("background-color", "");
	$('#emailAddress').css("background-color", "");
	var salutation			=	trim(document.getElementById("salutation").value);
	var firstName			=	trim(document.getElementById("firstName").value);
	var lastName			=	trim(document.getElementById("lastName").value);
	var title				=	trim(document.getElementById("title").value);
	var emailAddress		=	trim(document.getElementById("emailAddress").value);
//	var password			=	trim(document.getElementById("password").value);
	var phoneNumber			=	trim(document.getElementById("phone").value);
	var mobileNumber		=	trim(document.getElementById("mobileNumber").value);
	var roleId				=	document.getElementById("roleId").value;
	var entityType			=	"";
	var authenticationCode	=	"";
	var AEU_RoleId			=	"";
	
	var schoolId			=	document.getElementById("schoolId").value;
	var counter				=	0;
	var focusCount			=	0;
	
	if (firstName	==	"")
	{
		$('#errorschooldiv').show();
		$('#errorschooldiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focusCount	==	0)
			$('#firstName').focus();
		$('#firstName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (lastName	==	"")
	{
		$('#errorschooldiv').show();
		$('#errorschooldiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focusCount	==	0)
			$('#lastName').focus();
		$('#lastName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (emailAddress=="")
	{
		$('#errorschooldiv').show();
		$('#errorschooldiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focusCount	==	0)
			$('#emailAddress').focus();
		$('#emailAddress').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	else if(!isEmailAddress(emailAddress))
	{		
		$('#errorschooldiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focusCount==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	
	if(counter	==	0)
	{
		dwr.engine.beginBatch();			
		SchoolAjax.saveSchoolAdministratorOrAnalyst(schoolId,emailAddress,firstName,lastName,salutation,entityType,title,phoneNumber,mobileNumber,authenticationCode,roleId,{ 
			async: true,
			callback: function(data)
		{
			//alert("  data "+data);
			if((data	==	3) || (data	==	4))
			{
				$('#errorschooldiv').append("&#149; "+resourceJSON.msgaladyemail+"<br>");
				if(focusCount==0)
					$('#emailAddress').focus();
				$('#errorschooldiv').show();
				$('#emailAddress').css("background-color", "#F5E7E1");
				$('#loadingDiv').hide();
				return false;
			}
			else
			{
				if(data	==	2)
				{
					alert(" Server Error ");
				}
				else
				{
					if(roleId==3)
					{
						displaySchoolAdministrator(roleId);
					}
					if(roleId==6)
					{
						displaySchoolAnalyst(roleId);
					}
					clearUser();
					$('#loadingDiv').hide();
				}
			}
		},
		errorHandler:handleError
		});
		dwr.engine.endBatch();
	}
	else
	{
		$('#errorschooldiv').show();
		$('#loadingDiv').hide();
		return false;
	}
}

/*=========== Displaying Administrator and Analyst Grid =====================*/	
function displaySchoolAdministrator(roleId)
{
	tpJbIDisable();
	var schoolId			=	document.getElementById("schoolId").value;
	SchoolAjax.displaySchoolAdministratorGrid(schoolId,roleId,{ 
		async: true,
		callback: function(data)
		{
			$('#schoolAdministratorDiv').html(data);
			tpJbIEnable();
		},
		errorHandler:handleError
	});
}
function tpJbIEnable()
{
	var noOrRow = $('#schoolAdministratorDiv').children().size();
	for(var j=1;j<=noOrRow;j++)
	{
		$('#actDeactivateUserAdministrator'+j).tooltip();
	}
}
function tpJbIDisable()
{
	var noOrRow = $('#schoolAdministratorDiv').children().size();
	for(var j=1;j<=noOrRow;j++)
	{
		$('#actDeactivateUserAdministrator'+j).trigger('mouseout');
	}
}
function displaySchoolAnalyst(roleId)
{
	tpJbIAnalystDisable();
	var schoolId			=	document.getElementById("schoolId").value;
	SchoolAjax.displaySchoolAdministratorGrid(schoolId,roleId,{ 
		async: true,
		callback: function(data)
	{
		$('#schoolAnalystDiv').html(data);
		tpJbIAnalystEnable();
	},
	errorHandler:handleError
	});
}
/*========== For Analyst Enabling Tool Tip =============*/
function tpJbIAnalystEnable()
{
	var noOrRow = $('#schoolAnalystDiv').children().size();
	for(var j=1;j<=noOrRow;j++)
	{
		$('#actDeactivateUserAnalyst'+j).tooltip();
	}
}
function tpJbIAnalystDisable()
{
	var noOrRow = $('#schoolAnalystDiv').children().size();
	for(var j=1;j<=noOrRow;j++)
	{
		$('#actDeactivateUserAnalyst'+j).trigger('mouseout');
	}
}
/*========  activateDeactivateUser ===============*/
function activateDeactivateSchoolAdministratorOrAnalyst(roleId,userId,status)
{
	UserAjax.activateDeactivateUser(userId,status, { 
		async: true,
		callback: function(data)
		{
			if(roleId==3)
			{
				displaySchoolAdministrator(roleId);
			}
			if(roleId==6)
			{
				displaySchoolAnalyst(roleId);
			}
		},
		errorHandler:handleError
	});
}

function clearUser()
{
	$('#errorschooldiv').empty();
	$('#firstName').css("background-color", "");
	$('#lastName').css("background-color", "");
	$('#emailAddress').css("background-color", "");
	dwr.util.setValues({firstName:null,lastName:null,title:null,emailAddress:null,phone:null,mobileNumber:null});
	$("#addAdministratorDiv").hide();
}

/*============ Key Contact Functionality Start Here ======================================*/

function showSchoolKeyContact()
{ 
	keyContactIdVal=null;
	keyContactDivVal=2;
	dwr.util.setValues({ dropContactType:null});
	dwr.util.setValues({ keyContactId:null,keyContactFirstName:null,keyContactLastName:null,keyContactEmailAddress:null,keyContactPhoneNumber:null,keyContactTitle:null});
	$('#keyContactFirstName').css("background-color", "");
	$('#keyContactLastName').css("background-color", "");
	$('#keyContactEmailAddress').css("background-color", "");
	$('#errorkeydiv').empty();
	$("#addKeyContactSchoolDiv").fadeIn();
	$('#dropContactType').focus();
}

function displaySchoolKeyContact()
{
	var schoolId			=	document.getElementById("schoolId").value;
	SchoolAjax.displaySchoolKeyContactGrid(schoolId,{ 
		async: true,
		callback: function(data)
		{
			$('#keyContactTable').html(data);
			$('#addKeyContactSchoolDiv').hide();
		},
		errorHandler:handleError
	});
}

/*========  Check Valid Email address  ===============*/
function isEmailAddress(str) 
{
	str=str.trim();
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;	
	return emailPattern.test(str);
}

function addKeyContact()
{   	
	$('#errorschoolkeydiv').empty();
	$('#keyContactFirstName').css("background-color", "");
	$('#keyContactLastName').css("background-color", "");
	$('#keyContactEmailAddress').css("background-color", "");
	
	var keyContactId						=	trim(document.getElementById("keyContactId").value);
	var schoolId							=	trim(document.getElementById("schoolId").value);
	var keyContactTypeId					=	trim(document.getElementById("dropContactType").value);
	var keyContactFirstName						=	trim(document.getElementById("keyContactFirstName").value);
	var keyContactLastName						=	trim(document.getElementById("keyContactLastName").value);
	var keyContactEmailAddress				=	trim(document.getElementById("keyContactEmailAddress").value);
	var keyContactPhoneNumber				=	trim(document.getElementById("keyContactPhoneNumber").value);
	var keyContactTitle						=	trim(document.getElementById("keyContactTitle").value);
	var counter								=	0;
	var focusCount							=	0;
	
	if(keyContactFirstName=="")
	{
		$('#errorschoolkeydiv').show();	
		$('#errorschoolkeydiv').append("&#149; "+resourceJSON.msgContFName+"<br>");
		$('#keyContactFirstName').css("background-color", "#F5E7E1");
		$('#keyContactFirstName').focus();
		counter++;
		focusCount++;
	}
	if(keyContactLastName=="")
	{
		$('#errorschoolkeydiv').show();	
		$('#errorschoolkeydiv').append("&#149; "+resourceJSON.msgContLName+"<br>");
		$('#keyContactLastName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		//return false;
	}
	if(keyContactEmailAddress=="")
	{
		$('#errorschoolkeydiv').show();	
		$('#errorschoolkeydiv').append("&#149; "+resourceJSON.msgContEmail+"<br>");
		$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		//return false;
	}else if(keyContactEmailAddress!="")
	{
		if(!isEmailAddress(keyContactEmailAddress))
		{	$('#errorschoolkeydiv').show();	
			$('#errorschoolkeydiv').append("&#149; "+resourceJSON.msgValidContactEmail+"<br>");
			$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
	}
	
	if(counter	==	0)
	{
		$('#loadingDiv').fadeIn();
		SchoolAjax.saveKeyContact(keyContactDivVal,keyContactId,schoolId,keyContactTypeId,keyContactFirstName,keyContactLastName,keyContactEmailAddress,keyContactPhoneNumber,keyContactTitle,keyContactIdVal, { 
			async: true,
			callback: function(data)
			{
				$('#loadingDiv').hide();
				if(data==3){
					$('#errorschoolkeydiv').show();	
					$('#errorschoolkeydiv').append("&#149; "+resourceJSON.msgAlreadyRegisWithEmail+"<br>");
					$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
				}else if(data==4){
					$('#errorschoolkeydiv').show();	
					$('#errorschoolkeydiv').append("&#149; "+resourceJSON.msgKeyContWithSameEmail+"<br>");
					$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
				}else{
					displaySchoolKeyContact();
				}
			},
			errorHandler:handleError
		});
	}
	else
	{
		$('#errorschoolkeydiv').show();
		return false;
	}
	
}
function aKeyContactDiv(val){
	keyContactDivVal=val;
	if(val==1){
		editKeyContact(keyContactIdVal);
	}
}
function beforeEditKeyContact(keyContactId){
	keyContactIdVal=keyContactId;
	document.getElementById("Msg").innerHTML= resourceJSON.msgAnyChangeinDetails ;
	$('#myModalMsg').modal('show');
}
/*========  Edit Domain ===============*/
function editKeyContact(keyContactId)
{
	
	$('#keyContactName').css("background-color", "");
	$('#keyContactEmailAddress').css("background-color", "");
	$('#errorschoolkeydiv').empty();
	$('#addKeyContactSchoolDiv').fadeIn();
	SchoolAjax.getKeyContactsBykeyContactId(keyContactId,{ 
		async: true,
		callback: function(data)
	{
		//alert("data keyContactTypeId "+data.keyContactTypeId.contactTypeId+" Name"+data.keyContactTypeId.contactType);
		dwr.util.setValues(data);
		var optsKeyContact = document.getElementById('dropContactType').options;
		for(var i = 0, j = optsKeyContact.length; i < j; i++)
		{
			  if(data.keyContactTypeId.contactTypeId==optsKeyContact[i].value)
			  {
				  optsKeyContact[i].selected	=	true;
			  }
		}
	},
	errorHandler:handleError
	});
	return false;
}

function deleteKeyContact(keyContactId)
{
	if (confirm(resourceJSON.msgDeleteCont)) {
		SchoolAjax.deleteKeyContact(keyContactId, { 
			async: true,
			callback: function(data)
			{
				displaySchoolKeyContact();
			},
			errorHandler:handleError
		});
	}
}

function clearKeyContact()
{
	$('#errorschoolkeydiv').empty();
	$("#addKeyContactSchoolDiv").hide();
}

function validateEditSchool()
{
	$('#loadingDiv').fadeIn();
	$('#errormosaicinfodiv').empty();
	$('#errorlogoddiv').empty();
	$('#errorschoolcontactinfodiv').empty();
	$('#errorschoolaccountinfodiv').empty();
	$('#errordatediv').empty();
	$('#dmName').css("background-color", "");
	$('#dmPassword').css("background-color", "");
	$('#acName').css("background-color", "");
	$('#amName').css("background-color", "");
	$('#dmEmailAddress').css("background-color", "");
	$('#acEmailAddress').css("background-color", "");
	$('#amEmailAddress').css("background-color", "");
	$('#emailForTeacher').css("background-color", "");
	
	var generalInfoFlag=false;
	var contactInfoFlag=false;
	var userFlag=false;
	var accInfoFlag=false;
	
	/*==== Geting Dropdowns selected option id from objective.jsp in domainMaster and competencyMaster===*/
	var dmName					=	trim(document.getElementById("dmName").value);
	var dmPassword				=	document.getElementById("dmPassword").value;
	var acName					=	trim(document.getElementById("acName").value);
	var amName					=	trim(document.getElementById("amName").value);
	var dmEmailAddress			=	trim(document.getElementById("dmEmailAddress").value);
	var acEmailAddress			=	trim(document.getElementById("acEmailAddress").value);
	var amEmailAddress			=	trim(document.getElementById("amEmailAddress").value);
	var initiatedOnDate	=	trim(document.getElementById("initiatedOnDate").value);
	var contractStartDate	=	trim(document.getElementById("contractStartDate").value);
	var contractEndDate	=	trim(document.getElementById("contractEndDate").value);
	var emailForTeacher			=	trim(document.getElementById("emailForTeacher").value);
	var sDescription			=	trim($('#sDescription').find(".jqte_editor").text());
	
	//alert("dmName"+dmName+" acName "+acName+" amName "+amName);
	var counter				=	0;
	var focusCount			=	0;
	var dateErrorFlag=0;
	
	
	var logoPathFile	=	trim(document.getElementById("logoPathFile").value);
	if(logoPathFile!="" && logoPathFile!=null)
	{
		var ext = logoPathFile.substr(logoPathFile.lastIndexOf('.') + 1).toLowerCase();
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("logoPathFile").files[0]!=undefined)
			{
				fileSize = document.getElementById("logoPathFile").files[0].size;
			}
		}
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png'))
		{
			$('#errorlogoddiv').show();
			$('#errorlogoddiv').append("&#149; "+resourceJSON.msgLogoFormat+"<br>");
			if(focusCount	==	0)
			$('#logoPathFile').focus();
			$('#logoPathFile').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			generalInfoFlag=true;
		}
		else if(fileSize>=10485760)
		{
			$('#errorlogoddiv').show();
			$('#errorlogoddiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focusCount	==	0)
			$('#logoPathFile').focus();
			$('#logoPathFile').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			generalInfoFlag=true;
			
		}
	}
	
	$('#lblCriticialJobName').css("background-color", "");
	$('#lblAttentionJobName').css("background-color", "");
	var lblCriticialJobName	=	trim(document.getElementById("lblCriticialJobName").value);
	var lblAttentionJobName	=	trim(document.getElementById("lblAttentionJobName").value);
	//alert(" lblCriticialJobName "+lblCriticialJobName+" lblAttentionJobName "+lblAttentionJobName);
	
	if(lblCriticialJobName=="")
	{
		$('#errormosaicinfodiv').show();
		$('#errormosaicinfodiv').append("&#149;"+resourceJSON.msgLblForCrictJob+"<br>");
		if(focusCount	==	0)
		$('#lblCriticialJobName').focus();
		$('#lblCriticialJobName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		accInfoFlag=true;
	}
	if(lblAttentionJobName=="")
	{
		$('#errormosaicinfodiv').show();
		$('#errormosaicinfodiv').append("&#149; "+resourceJSON.msgLblAttentionJobs+"<br>");
		if(focusCount	==	0)
		$('#lblAttentionJobName').focus();
		$('#lblAttentionJobName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		accInfoFlag=true;
	}
	
	var assessmentUploadURLFile	=	trim(document.getElementById("assessmentUploadURLFile").value);
	var cbxUploadAssessment=document.getElementById("cbxUploadAssessment").checked;
	var assessmentUploadURLVal=document.getElementById("assessmentUploadURLVal").value;
	if(assessmentUploadURLVal==0 && cbxUploadAssessment==true && (assessmentUploadURLFile=="" || assessmentUploadURLFile==null))
	{
		$('#errordatediv').show();
		$('#errordatediv').append("&#149; "+resourceJSON.msgPleaseuploadInventory+"<br>");
		if(focusCount	==	0)
		$('#assessmentUploadURLFile').focus();
		$('#assessmentUploadURLFile').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		accInfoFlag=true;
	}else if(assessmentUploadURLFile!="" && assessmentUploadURLFile!=null)
	{
		var ext = assessmentUploadURLFile.substr(assessmentUploadURLFile.lastIndexOf('.') + 1).toLowerCase();
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("assessmentUploadURLFile").files[0]!=undefined)
			{
				fileSize = document.getElementById("assessmentUploadURLFile").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png'|| ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordatediv').show();
			$('#errordatediv').append("&#149; "+resourceJSON.msgSpecificInventoryfile+"<br>");
			if(focusCount	==	0)
			$('#assessmentUploadURLFile').focus();
			$('#assessmentUploadURLFile').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			accInfoFlag=true;
		}
		else if(fileSize>=10485760)
		{
			$('#errordatediv').show();
			$('#errordatediv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focusCount	==	0)
			$('#assessmentUploadURLFile').focus();
			$('#assessmentUploadURLFile').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			accInfoFlag=true;
		}
	}
	
	if(contractStartDate!="" && initiatedOnDate!=""){
		var initDate = new Date(initiatedOnDate);              
	    var startDate= new Date(contractStartDate);   
		if(startDate < initDate){
			$('#errordatediv').show();
			$('#errordatediv').append("&#149; "+resourceJSON.msgDateInitiatedlessequalStartDate+"<br>");
			if(focusCount	==	0)
				$('#initiatedOnDate').focus();
			$('#initiatedOnDate').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			dateErrorFlag=1;
			accInfoFlag=true;
		} 
	}
	if(initiatedOnDate!="" && contractEndDate!="" && dateErrorFlag==0){
		var endDate = new Date(contractEndDate);              
	    var initDate= new Date(initiatedOnDate);   
		if(initDate > endDate){
			$('#errordatediv').show();
			$('#errordatediv').append("&#149; "+resourceJSON.msgDateInitiatedlessequalEndDate+"<br>");
			if(focusCount	==	0)
				$('#contractStartDate').focus();
			$('#contractStartDate').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			dateErrorFlag=1;
			accInfoFlag=true;
		} 
	}
	if(contractStartDate!="" && contractEndDate!="" && dateErrorFlag==0){
		var endDate = new Date(contractEndDate);              
	    var startDate= new Date(contractStartDate);   
		if(startDate > endDate){
			$('#errordatediv').show();
			$('#errordatediv').append("&#149; "+resourceJSON.msgContractStDatelesseqEnddate+"<br>");
			if(focusCount	==	0)
				$('#contractStartDate').focus();
			$('#contractStartDate').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			dateErrorFlag=1;
			accInfoFlag=true;
		} 
	}
	if ($('#sDescription').find(".jqte_editor").text().trim()!=""){
		var charCount=$('#sDescription').find(".jqte_editor").text().trim();
		var count = charCount.length;
		if(count>1500)
		{
			$('#errorlogoddiv').show();
			$('#errorlogoddiv').append("&#149; "+resourceJSON.msgDesLengthExceed1500+"<br>");
			if(focusCount	==	0)
				$('#sDescription').find(".jqte_editor").focus();
				$('#sDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			generalInfoFlag=true;
		}
	}
	if (dmName	==	"")
	{
		$('#errorschoolcontactinfodiv').show();
		$('#errorschoolcontactinfodiv').append("&#149; "+resourceJSON.msgFinalDesMaker+"<br>");
		if(focusCount	==	0)
			$('#dmName').focus();
		$('#dmName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		contactInfoFlag=true;
	}
	if (dmPassword	==	"")
	{
		$('#errorschoolcontactinfodiv').show();
		$('#errorschoolcontactinfodiv').append("&#149; "+resourceJSON.msgPassword+"<br>");
		if(focusCount	==	0)
			$('#dmPassword').focus();
		$('#dmPassword').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		contactInfoFlag=true;
	}
	if (acName	==	"")
	{
		$('#errorschoolcontactinfodiv').show();
		$('#errorschoolcontactinfodiv').append("&#149; "+resourceJSON.msgStudentAssessmentCoordinator+" <br>");
		if(focusCount	==	0)
			$('#acName').focus();
		$('#acName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		contactInfoFlag=true;
	}
	if (amName	==	"")
	{
		$('#errorschoolcontactinfodiv').show();
		$('#errorschoolcontactinfodiv').append("&#149; "+resourceJSON.msgAccountManagerTM+"<br>");
		if(focusCount	==	0)
			$('#amName').focus();
		$('#amName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		contactInfoFlag=true;
	}
	if(dmEmailAddress=="")
	{
		$('#errorschoolcontactinfodiv').show();
		$('#errorschoolcontactinfodiv').append("&#149; "+resourceJSON.msgFinalDesMakerEmail+"<br>");
			if(focusCount==0)
				$('#dmEmailAddress').focus();
			
			$('#dmEmailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			contactInfoFlag=true;
	}
	if(dmEmailAddress!="")
	{
		if(!isEmailAddress(dmEmailAddress))
		{	
			$('#errorschoolcontactinfodiv').append("&#149; "+resourceJSON.msgValidDescMakerEmail+"<br>");
			if(focusCount==0)
				$('#dmEmailAddress').focus();
			
			$('#dmEmailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			contactInfoFlag=true;
		}
	}
	if(acEmailAddress!="")
	{
		if(!isEmailAddress(acEmailAddress))
		{	
			$('#errorschoolcontactinfodiv').append("&#149; "+resourceJSON.msgValidEmailStdAssecord+"<br>");
			if(focusCount==0)
				$('#acEmailAddress').focus();
			
			$('#acEmailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			contactInfoFlag=true;
		}
	}
	if(amEmailAddress!="")
	{
		if(!isEmailAddress(amEmailAddress))
		{	
			$('#errorschoolcontactinfodiv').append("&#149; "+resourceJSON.msgValidEmail4AM+"<br>");
			if(focusCount==0)
				$('#amEmailAddress').focus();
			
			$('#amEmailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			contactInfoFlag=true;
		}
	}
	var allowMessageTeacher=document.getElementById("allowMessageTeacher").checked;
	if(allowMessageTeacher==true && emailForTeacher==""){
		$('#errorschoolaccountinfodiv').append("&#149; "+resourceJSON.msgEmailAddToRecvMsg+"<br>");
		if(focusCount==0)
			$('#emailForTeacher').focus();
		
		$('#emailForTeacher').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		accInfoFlag=true;
	}else if(emailForTeacher!=""){
		if(!isEmailAddress(emailForTeacher)){	
			$('#errorschoolaccountinfodiv').append("&#149; "+resourceJSON.msgvalidEmailaddresstoreceive+"<br>");
			if(focusCount==0)
				$('#emailForTeacher').focus();
			
			$('#emailForTeacher').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
			accInfoFlag=true;
		}
	}

	var flagForURL	=	document.getElementsByName("flagForURL");
	for(i=0;i<flagForURL.length;i++)
	{
		if(flagForURL[i].checked	==	true)
		{
			var exitURL	=	document.getElementById("exitURL").value;
			//alert(" exitURL"+exitURL);
			if(exitURL=="")
			{
				$('#errorschoolaccountinfodiv').show();
				$('#errorschoolaccountinfodiv').append("&#149; "+resourceJSON.msgEnterURL+" <br>");
				if(focusCount	==	0)
					$('#exitURL').focus();
				$('#exitURL').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
				accInfoFlag=true;
			}
		}
	}
	var flagForMessage	=	document.getElementsByName("flagForMessage");
	for(i=0;i<flagForMessage.length;i++)
	{
		if(flagForMessage[i].checked	==	true)
		{
			if($('#eMessage').find(".jqte_editor").text().trim()=="")
			{
				$('#errorschoolaccountinfodiv').show();
				$('#errorschoolaccountinfodiv').append("&#149; "+resourceJSON.msgCompletionMessage+" <br>");
				if(focusCount	==	0)
					$('#eMessage').find(".jqte_editor").focus();
				$('#eMessage').find(".jqte_editor").css("background-color", "#F5E7E1");
	
				counter++;
				focusCount++;
				accInfoFlag=true;
			}else if ($('#eMessage').find(".jqte_editor").text().trim()!=""){
				var charCount=$('#eMessage').find(".jqte_editor").text().trim();
				var count = charCount.length;
				if(count>1000)
				{
					$('#errorschoolaccountinfodiv').show();
					$('#errorschoolaccountinfodiv').append("&#149; "+resourceJSON.msgCompletionMessagelength+"<br>");
					$('#eMessage').find(".jqte_editor").focus();
					$('#eMessage').find(".jqte_editor").css("background-color", "#F5E7E1");
		
					counter++;
					focusCount++;
					accInfoFlag=true;
				}
			}
		}
	}
	if(generalInfoFlag){
		alert(resourceJSON.msgGISection);
	}else if(contactInfoFlag){
		alert(resourceJSON.msgISection);
	}else if(userFlag){
		alert(resourceJSON.msgAISection);
	}else if(accInfoFlag){
		alert(resourceJSON.msgAISection);
	}
	
	/* ========== Mosaic Accordian Validation =================*/
	try{
		var iframeNorm = document.getElementById('ifrm1');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		if(innerNorm.getElementById('slider1').value!=''){
			$('#candidateFeedNormScore').val(innerNorm.getElementById('slider1').value);
		}
		}catch(e){} 

		if($("#mosaicRadiosFlag").val()==1)
		{
			try{
			var iframeNorm = document.getElementById('ifrm2');
			var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			if(innerNorm.getElementById('slider2').value!='')
				$('#candidateFeedDaysOfNoActivity').val(innerNorm.getElementById('slider2').value);
			}catch(e){}
		}
	
	try{
		var iframeNorm = document.getElementById('ifrm3');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		if(innerNorm.getElementById('slider3').value!='')
			$('#jobFeedCriticalJobActiveDays').val(innerNorm.getElementById('slider3').value);
		}catch(e){} 
	
	try{
		var iframeNorm = document.getElementById('ifrm4');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		if(innerNorm.getElementById('slider4').value!='')
			$('#jobFeedCriticalCandidateRatio').val(innerNorm.getElementById('slider4').value);
		}catch(e){} 
		
	try{
		var iframeNorm = document.getElementById('ifrm5');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		if(innerNorm.getElementById('slider5').value!='')
			$('#jobFeedCriticalNormScore').val(innerNorm.getElementById('slider5').value);
		}catch(e){} 
		
	try{
		var iframeNorm = document.getElementById('ifrm6');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		if(innerNorm.getElementById('slider6').value!='')
			$('#jobFeedAttentionJobActiveDays').val(innerNorm.getElementById('slider6').value);
		}catch(e){} 
		
	try{
		var iframeNorm = document.getElementById('ifrm7');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		if(innerNorm.getElementById('slider7').value!='')
			$('#jobFeedAttentionNormScore').val(innerNorm.getElementById('slider7').value);
		}catch(e){} 
		
	//counter=1;
	if(counter	==	0)
	{
		$('#loadingDiv').hide();
		document.getElementById("editSchoolForm").submit();
		return true;
	}
	else
	{
		$('#loadingDiv').hide();
		return false;
	}
}
function uncheckedMessageRadio()
{
	$('#errorschoolaccountinfodiv').empty();
	$('#exitURL').css("background-color", "");
	$('#exitMessage').css("background-color", "");
	//alert("Hi")
	var flagForMessage	=	document.getElementsByName("flagForMessage");
	for(i=0;i<flagForMessage.length;i++)
	{
		if(flagForMessage[i].checked	==	true)
		{
			flagForMessage[i].checked	=	false;
			break;
		}
	}
}
function uncheckedUrlRadio()
{
	$('#errorschoolaccountinfodiv').empty();
	$('#exitURL').css("background-color", "");
	$('#exitMessage').css("background-color", "");
	var flagForURL	=	document.getElementsByName("flagForURL");
	for(i=0;i<flagForURL.length;i++)
	{
		if(flagForURL[i].checked	==	true)
		{
			flagForURL[i].checked	=	false;
			break;
		}
	}
}

/*============ Make School Decision Maker Feild editable==================================*/
function editSchoolDmFields()
{
	var cnfmPassword					=	document.getElementById("cnfmPassword").value;
	var schoolId						=	trim(document.getElementById("schoolId").value);
	var entityTypeOfSession				=	trim(document.getElementById("entityTypeOfSession").value);
	if(entityTypeOfSession==1)
	{
		$('#errorschoolpwddiv').empty();
		if(cnfmPassword=='Te@cherMatch2006!')
		{
			document.getElementById("dmName").readOnly 				= 	false;
			document.getElementById("dmEmailAddress").readOnly 		= 	false;
			document.getElementById("dmPhoneNumber").readOnly 		= 	false;
			document.getElementById("btnDm").style.display	 		= 	"none";
			//document.getElementById("dmPassword").value				=	document.getElementById("cnfmPassword").value;
			document.getElementById("cnfmPassword").readOnly 		= 	true;
		}
		else
		{
			//$('#errorschoolpwddiv').append("&#149; Invalid Password <br>");
			SchoolAjax.matchPwdOfSchoolDm(schoolId,cnfmPassword,{ 
				async: true,
				callback: function(data)
				{
					$('#errorschoolpwddiv').empty();
					if(data!=null)
					{
						document.getElementById("dmName").readOnly 				= 	false;
						document.getElementById("dmEmailAddress").readOnly 		= 	false;
						document.getElementById("dmPhoneNumber").readOnly 		= 	false;
						document.getElementById("btnDm").style.display	 		= 	"none";
						document.getElementById("dmPassword").value				=	document.getElementById("cnfmPassword").value;
						document.getElementById("cnfmPassword").readOnly 		= 	true;
						//alert("dmPassword "+document.getElementById("dmPassword").value);
					}
					else
					{
						$('#errorschoolpwddiv').append("&#149; "+resourceJSON.msgInvalidPassword+" <br>");
					}
				},
				errorHandler:handleError
			});
		}
	}
	else
	{
		SchoolAjax.matchPwdOfSchoolDm(schoolId,cnfmPassword,{ 
			async: true,
			callback: function(data)
			{
				$('#errorschoolpwddiv').empty();
				if(data!=null)
				{
					document.getElementById("dmName").readOnly 				= 	false;
					document.getElementById("dmEmailAddress").readOnly 		= 	false;
					document.getElementById("dmPhoneNumber").readOnly 		= 	false;
					document.getElementById("btnDm").style.display	 		= 	"none";
					document.getElementById("dmPassword").value				=	document.getElementById("cnfmPassword").value;
					document.getElementById("cnfmPassword").readOnly 		= 	true;
					//alert("dmPassword "+document.getElementById("dmPassword").value);
				}
				else
				{
					$('#errorschoolpwddiv').append("&#149; "+resourceJSON.msgInvalidPassword+" <br>");
				}
			},
			errorHandler:handleError
		});
	}
}

/*
 * 	District Attachment
 *  Dated : 31 OCt 2014
 * 
 * */

function displayDistAttachment() {
	dwr.util.setValues({note:null});
	var schoolId	=	trim(document.getElementById("schoolId").value);
	var districtId	=	document.getElementById("districtId").value;
	//alert(schoolId+":::::::::::"+districtId);return false;
	SchoolAjax.displayDistAttachmentGrid(districtId,schoolId,noOfRowsDist,pageDist,sortOrderStrDist,sortOrderTypeDist,{
		async: true,
		callback: function(data)
		{
			$('#distAttachmentGrid').html(data);
			applyScrollOnTblDistAttachment();
			$('#distAttachmentDiv').hide();
		},
		errorHandler:handleError
	});

}

function addDistAttachment()
{
	$("#distAttachmentDiv").fadeIn();
	$("#distAttachmentEditDiv").hide();
	$('#errornotediv').empty();
}

function uploadDistFile(){
	$('#errornotediv').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var districtId	=	document.getElementById("districtId").value;
	var schoolId	=	trim(document.getElementById("schoolId").value);
	var fileName 	= 	document.getElementById("distAttachmentDocument").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;
	
	var filenameSS 	= 	fileName.replace(/.*(\/|\\)/, '');
	var baseFile 	= 	filenameSS.substr(0, filenameSS.lastIndexOf('.'));
	
	var d		=	new Date();
	var year	=	d.getFullYear();
	var month	=	d.getMonth();
	var date	=	d.getDate();
	var hour	=	d.getHours();
	var minute	=	d.getMinutes();
	var second	=	d.getSeconds();
	var dateString = year+"-"+month+"-"+date+"-"+hour+"-"+minute+"-"+second;
	var dateString = dateString.replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	
	var fullFilename = baseFile+dateString+"."+ext;
	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(distAttachmentDocument.files[0]!=undefined)
		fileSize = distAttachmentDocument.files[0].size;
	}
	
	if(fileName == null || fileName == ""){
		$('#errornotediv').append("&#149; "+resourceJSON.msgPlzSelectFile+"<br>");
		if(focs==0)
			$('#distAttachmentDocument').focus();
		
		$('#distAttachmentDocument').css("background-color",txtBgColor);
		counter++;
		focs++;	
		return false;
	}
	if(fileSize>=10485760)
	{
		$('#errornotediv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focs==0)
				$('#distAttachmentDocument').focus();
			
			$('#distAttachmentDocument').css("background-color",txtBgColor);
			counter++;
			focs++;	
			return false;
	}

	if(counter==0){
		if(fileName!=""){
			$("#loadingDiv").show();
			var file_data = $("#distAttachmentDocument").prop("files")[0];
			var form_data = new FormData();
			form_data.append("dateString", dateString)
			form_data.append("districtId", districtId)
			form_data.append("schoolId", schoolId)
			form_data.append("file", file_data)
			$.ajax({
		               url: "./schoolFileUploadServlet.do",
		               dataType: 'script',
		               cache: false,
		               contentType: false,
		               processData: false,
		               data: form_data,
		               type: 'post',
		               success : function(response) {
						   SchoolAjax.saveDistrictAttachment(districtId,schoolId,fullFilename,{
									async: true,
									callback: function(data)
									{
										$('#distAttachmentGrid').html(data);
										applyScrollOnTblDistAttachment();
										displayDistAttachment();
									},
									errorHandler:handleError
								});
									$("#loadingDiv").hide();
							}
		       })
		}
	}
}

function editDistrictAttachment(districtattachmentId) {
	$('#note').css("background-color", "");
	$("#distAttachmentEditDiv").fadeIn();
	$('#errornotediv').empty();
	$("#distAttachmentDiv").hide();
	document.getElementById("myModalViewDist").innerHTML 	= 	"<a href=\"javascript:void(0)\" onclick=\"vieDistrictFile("+districtattachmentId+")\">"+resourceJSON.ViewLink+"</a>";
	document.getElementById("myModalEditDist").innerHTML	=	"<a href=\"javascript:void(0)\" onclick=\"return uploadEditDistFile("+districtattachmentId+")\">"+resourceJSON.msgIamDone+"</a>";
}

function uploadEditDistFile(districtattachmentId) {
	$('#errornotediv').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var districtId	=	document.getElementById("districtId").value;
	var schoolId	=	trim(document.getElementById("schoolId").value);
	var fileName 	= 	document.getElementById("distAttachmentEditDocument").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;
	
	var filenameSS 	= 	fileName.replace(/.*(\/|\\)/, '');
	var baseFile 	= 	filenameSS.substr(0, filenameSS.lastIndexOf('.'));

	var d		=	new Date();
	var year	=	d.getFullYear();
	var month	=	d.getMonth();
	var date	=	d.getDate();
	var hour	=	d.getHours();
	var minute	=	d.getMinutes();
	var second	=	d.getSeconds();
	var dateString = year+"-"+month+"-"+date+"-"+hour+"-"+minute+"-"+second;
	var dateString = dateString.replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');

	var fullFilename = baseFile+dateString+"."+ext;
	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(distAttachmentEditDocument.files[0]!=undefined)
		fileSize = distAttachmentEditDocument.files[0].size;
	}
	if(fileName == null || fileName == ""){
		$('#errornotediv').append("&#149; "+resourceJSON.msgPleaseSelectFile+"<br>");
		if(focs==0)
			$('#distAttachmentEditDocument').focus();
		
		$('#distAttachmentEditDocument').css("background-color",txtBgColor);
		counter++;
		focs++;	
		return false;
	}
	if(fileSize>=10485760)
	{
		$('#errornotediv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focs==0)
				$('#distAttachmentEditDocument').focus();
			
			$('#distAttachmentEditDocument').css("background-color",txtBgColor);
			counter++;
			focs++;	
			return false;
	}
	
	
	if(counter==0){
		if(fileName!=""){
			$("#loadingDiv").show();
			var file_data = $("#distAttachmentEditDocument").prop("files")[0];
			var form_data = new FormData();
			form_data.append("dateString", dateString)
			form_data.append("districtId", districtId)
			form_data.append("schoolId", schoolId)
			//form_data.append("districtattachmentId", districtattachmentId)
			form_data.append("file", file_data)
			$.ajax({
				 	   url: "./schoolFileUploadServlet.do",
		               dataType: 'script',
		               cache: false,
		               contentType: false,
		               processData: false,
		               data: form_data,
		               type: 'post',
		               success : function(response) {
					   SchoolAjax.editDistrictAttachment(districtId,schoolId,districtattachmentId,fullFilename,{
								async: true,
								callback: function(data)
								{
									$('#distAttachmentGrid').html(data);
									applyScrollOnTblDistAttachment();
									displayDistAttachment();
								},
								errorHandler:handleError
							});
								$("#loadingDiv").hide();
						}
		       })
		}
	}
}

function vieDistrictFile(districtattachmentId) {
	SchoolAjax.vieDistrictFile(districtattachmentId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				 $('#distAttachmentDiv').modal('hide');
				 document.getElementById('ifrmAttachment').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#distAttachmentShowDiv').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		} else {
				$('.distAttachmentShowDiv').modal('show');
				document.getElementById('ifrmAttachment').src = ""+data+"";
		}
	}});

	}

function deleteDistrictAttachment(districtattachmentId) {
	$('#note').css("background-color", "");
	$('#errornotediv').empty();
	if (confirm(resourceJSON.msgdeleteNotes1)) {
		SchoolAjax.deleteDistrictAttachment(districtattachmentId, {
			async: true,
			callback: function(data)
			{
				$('#distAttachmentGrid').html(data);
				applyScrollOnTblDistAttachment();
				displayDistAttachment();
			},
			errorHandler:handleError
		});
	}
}
