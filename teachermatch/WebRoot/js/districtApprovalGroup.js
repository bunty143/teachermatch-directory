function addJobCategoryApprovalHideGroup()
{
	$("#groupApprovalDiv").hide();
	$("#ApprovalGroupFromEditFlag").val("false");
}

function addJobCategoryApproval()
{
	var districtId = $("#districtId").val();
	var jobcategoryId = $("#ApprovalJobCategoryId").val();
	var noOfApprovalNeeded = $("#ApprovalNoOfApprovalNeeded").val();
	var buildApprovalGroup = $("#ApprovalBuildApprovalGroup").val();
	var approvalBeforeGoLive = $("#ApprovalApprovalBeforeGoLive").val();
	var flag = $("#ApprovalGroupFromEditFlag").val();
	
	if(districtId!=null && districtId!="" && flag=="false")
	{
		$("#groupApprovalDiv").show();
		
		$("#ApprovalJobCategoryId").val("-1");
		jobcategoryId = $("#ApprovalJobCategoryId").val();
		$("#groupApprovalDiv").show();
		
		$("#ApprovalDistrictId").val(districtId);
		$("#ApprovalJobCategoryId").val(jobcategoryId);
		
		getApprovalGroupsInfo(districtId,jobcategoryId);
	}
}

function enableDisableApprovals()
{
	var districtId=$("#ApprovalDistrictId").val();
	var jobcategoryId= $("#ApprovalJobCategoryId").val();
	var isChecked = $("#approvalBeforeGoLive1").is(":checked");

	groupRadioButton();
	resetStackAndDiv();
	
	if(isChecked)
	{
		$("#noOfApprovalNeeded").removeAttr("disabled");
		$(".hideGroup").fadeIn(500);
		$("#addApprovalGroupAndMemberDiv").hide();
		getApprovalGroupsInfo(districtId,jobcategoryId);
	}
	else
	{
		$("#noOfApprovalNeeded").attr("disabled","disabled");
		$(".hideGroup").fadeOut(500);
	}
	
}

function getApprovalGroupsInfo(districtId,jobcategoryId)
{
	resetStackAndDiv();
	DistrictAjax.getApprovalGroupsInfo(districtId,jobcategoryId,
	{
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			$("#groupName").empty();
			$("#groupMembersList").empty();
			$("#groupMembers").empty();
			
			$("#groupName").append("<option value='-1'> Select Group </option>");
			$.each(data, function(key, value)
			{
				if(key=="groupAndMemberList")
				{
					var groupAndMemberList="";
					$.each(value, function(groupKey, groupvalue)
					{
						var groupInfo = groupKey;
						var groupMemberInfo = groupvalue.split(",");
						
						var groupId = groupInfo.split("_")[0];
						var groupName = groupInfo.split("_")[1];
						
						appendGroup(groupId,groupName);	//Append the group into Div
						addGroupAndMemberIntoStack(groupId, "");	//Append the group with no member
						
						for(i=0;i<groupMemberInfo.length;i++)
						{
							if(groupMemberInfo!=null && groupMemberInfo!="" && groupMemberInfo[i]!=null && groupMemberInfo[i]!="")
							{
								var memberId=groupMemberInfo[i].split("_")[0];
								var memberName=groupMemberInfo[i].split("_")[1];
														
								appendMemberIntoGroup(groupId,memberId,memberName);	//Append the member into group
								addGroupAndMemberIntoStack(groupId, memberId);
								
								//$("#groupMembers").append("<option value='"+memberId+"'>"+memberName+"</option>");
								//alert("memberId:- "+memberId+", memberName:- "+memberName);
							}
						}
					});
				}
				else
				if(key=="memberList")
				{
					var myarr = (""+value).split(",");
					for(i=0;i<myarr.length;i++)
					{
						if(myarr!=null && myarr!="" && myarr[i]!=null && myarr[i]!="")
						{
							var memberInfo=myarr[i].split("_");
						
							var memberId=memberInfo[0];;
							var memberName=memberInfo[1];
						
							$("#groupMembers").append("<option value='"+memberId+"'>"+memberName+"</option>");
						}
					}
				}
				else
				if(key=="noOfApprovalNeeded")
				{
					//if(value!=null && value=="true")
					$("#noOfApprovalNeeded option[value="+value+"]").attr("selected","selected");
					$("#ApprovalNoOfApprovalNeeded").val(value);
				}
				else
				if(key=="buildApprovalGroup")
				{
					if(value!=null && value=="true")
					{
						$("#buildApprovalGroups").attr("checked","checked");
						//$(".hideGroup").fadeIn(500);
					}
					else
					{
						$("#buildApprovalGroups").removeAttr("checked");
					}
					$("#ApprovalBuildApprovalGroup").val(value);
				}
				else
				if(key=="approvalBeforeGoLive")
				{
					if(value!=null && value=="true")
						$("#approvalBeforeGoLive1").attr("checked","checked");
					//else
					//	$("#approvalBeforeGoLive1").removeAttr("checked");
				}
			});
			
			var isChecked = $("#approvalBeforeGoLive1").is(":checked");
			if(isChecked==false)
				$("#noOfApprovalNeeded").attr("disabled","disabled");
			else
				$("#noOfApprovalNeeded").removeAttr("disabled");
			
			$("#buildApprovalGroupsDivId").show();
			$("#approvalBeforeGoLive1").show();
			$("#groupMembersList").show();
			$("#addApprovalGroupAndMemberDiv").hide();
				
		}
	});
}

function removeMember()
{
	var groupId = $("#removeMemberConfirmation").attr("removeMemberGroupId");
	var memberId = $("#removeMemberConfirmation").attr("removeMemberId");
	var memberName = $("#"+groupId+"_"+memberId).attr("name");
	
	$("#removeMemberConfirmation").modal("hide");
	//Shadab//$("#groupMembers").append("<option value='"+memberId+"'>"+memberName+"</option>");
	
	var d = document.getElementById(groupId+"_"+memberId);
	var olddiv = document.getElementById(groupId+"_"+memberId);
	d.remove(olddiv);
	
	removeMemberFromStack(groupId, memberId);
}

function removeMemberConfirmation(groupId, memberId)
{
	$("#removeMemberConfirmation").modal("show");
	$("#removeMemberConfirmation").attr("removeMemberGroupId",groupId);
	$("#removeMemberConfirmation").attr("removeMemberId",memberId);
}

function removeGroup()
{
	var groupId = $("#removeGroupConfirmation").attr("removeGroupId");
	$("#removeGroupConfirmation").modal("hide");
	
	$("#groupName option[value='"+groupId+"']").remove();	//remove group from select
	var memberIds = getMembersByGroupFromStack(groupId);	//append the group member into multiple select textbox
/*//Shadab//	for(var i=0;i<memberIds.length;i++)
	{
		if(memberIds[i]!="")
		{
			var memberId = memberIds[i];
			var memberName = $("#"+groupId+"_"+memberId).attr("name");
			$("#groupMembers").append("<option value='"+memberId+"'>"+memberName+"</option>");
		}
	}
*/	
	var d = document.getElementById(groupId);		//remove group and its all member
	var olddiv = document.getElementById(groupId);
	d.remove(olddiv);
	
	removeGroupFromStack(groupId);
}

function removeGroupConfirmation(groupId)
{
	//alert("groupId:- "+groupId);
	$("#removeGroupConfirmation").modal("show");
	$("#removeGroupConfirmation").attr("removeGroupId",groupId);
}

function addGroupORMember()
{
	var cnt = validateApprovalGroup();
	if(cnt==0)
	{
		var groupId;
		var memberIds=$("#groupMembers :selected").val();
		var checked = $('input[name=groupRadioButtonId]:checked').val();
		
		if(checked=="radioButton1")	//add new group
		{
			var groupName = $("#addGroupName").val();
			groupId = "";//"newGroup"+(new Date()).getMilliseconds();
			
			var groupNameArray = groupName.split(" ");
			for(var i=0;i<groupNameArray.length; i++ )
				if(i==0)
					groupId = groupId+groupNameArray[i];
				else
					groupId = groupId+"-"+groupNameArray[i];
			
			appendGroup(groupId,groupName);				//Append the group
			addGroupAndMemberIntoStack(groupId, "");	//Append the new group into stack;
			
			$("#groupMembers :selected").each(function(){
				var memberId = $(this).val();
				var memberName = $(this).text();
				
				if(!isMemberExistInGroup(memberId,groupId))
				{
					appendMemberIntoGroup(groupId,memberId,memberName);	//Append group in div
					addGroupAndMemberIntoStack(groupId, memberId);		//Append the member to group
				}
			});
		}
		else
		if(checked=="radioButton2")	//add member to group
		{
			groupId = $("#groupName option:selected").val();
		
			$("#groupMembers :selected").each(function(){
				var memberId = $(this).val();
				var memberName = $(this).text();
				
				if(!isMemberExistInGroup(memberId,groupId))
				{
					appendMemberIntoGroup(groupId,memberId,memberName);	//Append group in div
					addGroupAndMemberIntoStack(groupId, memberId);		//Append the member to group
				}
			});
		}
		//$("#groupMembers :selected").remove();	//remove group from select textbox
		$("#addApprovalGroupAndMemberDiv").fadeOut(500);
	}
}

function validateApprovalGroup()
{
	var count=0;
	
	var errorMessage="";
	$("#privilegeForDistrictErrorDiv").empty();
	$('#addGroupName').css("background-color", "#FFF");
	$('#groupName').css("background-color", "#FFF");
	$('#groupMembers').css("background-color", "#FFF");
	
	groupRadioButton();
	
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")	//add new group
	{
		var groupName = $("#addGroupName").val();
		if(groupName=="")
		{
			count++;
			errorMessage = errorMessage+"<label class=required>* Please enter Group Name </label><BR>";
			$('#addGroupName').css("background-color", "#F5E7E1");
			$('#addGroupName').focus();
		}
	}
	else
	if(checked=="radioButton2")	//add member to group
	{
		var groupId = $("#groupName option:selected").val();
		var memberIds=$("#groupMembers :selected").val();
		
		if(groupId=="-1")
		{
			count++;
			errorMessage = errorMessage+"<label class=required>* Please select Group Name </label><BR>";
			$('#groupName').css("background-color", "#F5E7E1");
		}

		if(memberIds==null || memberIds=="")
		{
			count++;
			errorMessage = errorMessage+"<label class=required>* Please select Member </label><BR>";
			$('#groupMembers').css("background-color", "#F5E7E1");
		}
		
		if(groupId=="-1")
			$('#groupName').focus();
		else
			$('#groupMembers').focus();
	}
	
	if(errorMessage!="")
	{
		$("#privilegeForDistrictErrorDiv").html(errorMessage);
		$("#privilegeForDistrictErrorDiv").fadeIn(500);
	}
	
	return count;
}

function groupRadioButton()
{
	var checked = $('input[name=groupRadioButtonId]:checked').val();
	if(checked=="radioButton1")
	{
		$("groupName").val("");
		$("#groupName").attr("disabled", "disabled");	//disable groupNameList
		$('#groupName').css("background-color", "#EEEEEE");
		$("#addGroupName").removeAttr("disabled");		//enable addGroup
		$('#addGroupName').css("background-color", "#FFF");
	}
	else
	if(checked=="radioButton2")
	{
		$("#addGroupName").attr("disabled", "disabled");	//disable groupNameList
		$('#addGroupName').css("background-color", "#EEEEEE");
		$("#groupName").removeAttr("disabled");				//enable addGroup
		$('#groupName').css("background-color", "#FFF");
	}
}

function showAddGroup()	
{
	groupRadioButton();
	$("#addGroupName").val("");
	$("#addApprovalGroupAndMemberDiv").fadeIn(500);
}

function hideAddApprovalGroupAndMemberDiv()
{
	$("#addApprovalGroupAndMemberDiv").fadeOut(500);
	$("#privilegeForDistrictErrorDiv").empty();
	$("#privilegeForDistrictErrorDiv").fadeOut(500);
}

function addOrUpdateDistrictApprovalGroupJobCategorywise(jobcategoryId)
{
	var districtId = $("#districtId").val();
	var noOfApprovals = $("#noOfApproval").val();
	var groupInfo = getStack();
	
	//alert("districtId:- "+districtId+"\njobcategoryId:- "+jobcategoryId+",\n noOfApprovals:- "+noOfApprovals+",\ngroupInfo:- "+groupInfo);
	$('#loadingDiv').show();
	DistrictAjax.addOrUpdateDistrictApprovalGroupJobCategorywise(districtId,jobcategoryId,noOfApprovals,groupInfo,{
		async: true,
		callback: function(data){
    		$('#loadingDiv').hide();
		},
	});
}

/**************************************************Start HTML Methods**************************************************/
function appendGroup(groupId,groupName)
{
	var htmlText="";
	htmlText = htmlText+("<div class='row mt5' id='"+groupId+"' name='"+groupName+"'>");
	htmlText = htmlText+("<label> "+groupName+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='Remove Group' onclick='removeGroupConfirmation(\""+groupId+"\")'><img width='15' height='15' class='can' src='images/can-icon.png'></a> </label>");
	htmlText = htmlText+"</div>";
	$("#groupMembersList").append(htmlText);
	$("#groupName").append("<option value='"+groupId+"'> "+groupName+" </option>");
	//return htmlText;
}

function appendMemberIntoGroup(groupId,memberId,memberName)
{
	var htmlText="";
	htmlText=htmlText+("<div style='margin-top: -6px;'>");
	htmlText = htmlText+("<div class='col-sm-3 col-md-3' id='"+groupId+"_"+memberId+"' name='"+memberName+"'>"+memberName+"&nbsp<a href='javascript:void(0);' data-toggle='tooltip' title='Remove Member' onclick='removeMemberConfirmation(\""+groupId+"\","+memberId+")' ><img width='15' height='15' class='can' src='images/can-icon.png'></a></div>");
	htmlText = htmlText+("</div>");
	$("#"+groupId).append(htmlText);
	
	//return htmlText;
}

/***************************************************End HTML Methods**************************************************/

/**************************************************Start Stack Methods*************************************************/
var groupStack="#";
function addGroupAndMemberIntoStack(groupId, memberId)
{
	var activeGroups = groupStack.split("#");
	var groupIdWithMemberId="";
	for(var i=0;i<activeGroups.length;i++)	//getting the group with member
	{
		var pos = activeGroups[i].indexOf(groupId+"_");
		if(pos==0)
		{
			groupIdWithMemberId = activeGroups[i];
			break;
		}
	}
	
	var group="";
	if(groupIdWithMemberId=="")											
		group = groupId+"_";								//add new group
	else
		group = groupIdWithMemberId;	//group exist so add the member into group
	
	if(memberId!="")												//add new member
		group = group+memberId+"_";		
	
	if(groupIdWithMemberId!="")						//This is a new Group
		groupStack = groupStack.replace("#"+groupIdWithMemberId+"#",("#"+group+"#") );		//remove the previous group
	else
	if(groupStack=="")												//add new upodated group and member into stack
		groupStack = "#"+groupStack+group+"#";
	else
		groupStack = groupStack+group+"#";
	
	groupStack = groupStack.replace("##","#");
	//alert("addGroupAndMemberIntoStack groupStack:- "+groupStack);
	return groupStack;
}

function removeGroupFromStack(groupId)
{
	var activeGroups = groupStack.split("#");
	for(var i=0;i<activeGroups.length;i++)	//getting the group with member
	{
		var pos = activeGroups[i].indexOf(groupId+"_");
		if(pos==0)
			groupStack = groupStack.replace("#"+activeGroups[i]+"#","#");		//remove the group
	}	
	
	groupStack = groupStack.replace("##","#");
	//alert("removeGroupFromStack groupStack:- "+groupStack);
	return groupStack;
}

function removeMemberFromStack(groupId, memberId)
{
	var activeGroups = groupStack.split("#");
	var groupIdWithMemberId="";
	for(var i=0;i<activeGroups.length;i++)	//getting the group with member
	{
		var pos = activeGroups[i].indexOf(groupId+"_");
		if(pos==0)
		{
			groupIdWithMemberId = activeGroups[i];
			break;
		}
	}
	
	var previousGroup = groupIdWithMemberId;
	if(groupIdWithMemberId!="")
	{
		groupIdWithMemberId = groupIdWithMemberId.replace("_"+memberId+"_","_");
		groupIdWithMemberId = groupIdWithMemberId.replace("__","_");
		
		groupStack = groupStack.replace("#"+previousGroup+"#",("#"+groupIdWithMemberId+"#") );	//remove the previous group
		//groupStack = groupStack+groupIdWithMemberId+"#";
	}
	
	groupStack = groupStack.replace("##","#");
	//alert("removeMemberFromStack groupStack:- "+groupStack);
	return groupStack;
}

function getMembersByGroupFromStack(groupId)
{
	var activeGroups = groupStack.split("#");
	var groupIdWithMemberId="";
	for(var i=0;i<activeGroups.length;i++)	//getting the group with member
	{
		var pos = activeGroups[i].indexOf(groupId+"_");
		if(pos==0)
		{
			groupIdWithMemberId = activeGroups[i];
			break;
		}
	}
	
	groupIdWithMemberId = groupIdWithMemberId.substring(groupIdWithMemberId.indexOf("_"), groupIdWithMemberId.length);
	
	//alert("getMembersByGroupFromStack Members:- "+groupIdWithMemberId.split("_"));
	return groupIdWithMemberId.split("_");
}

function isMemberExistInGroup(memberId, groupId)
{
	var groupMember = getMembersByGroupFromStack(groupId);
	var flag=false;
	for(var i=0;i<groupMember.length;i++)
	{
		if(memberId==groupMember[i])
		{
			flag=true;
			break;
		}
	}
	
	//alert("isMemberExistInGroup:- "+flag);
	return flag;
}

function resetStackAndDiv()
{
	groupStack="#";
	$("#groupMembersList").empty();
}

function getStack()
{
	return groupStack;
}

/**************************************************End Stack Methods*************************************************/