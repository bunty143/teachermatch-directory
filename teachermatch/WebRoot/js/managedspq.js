var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var secArray=[];
var sectionExternalChecked="";
var sectionInternalChecked="";
var sectionITransferChecked="";
var sortOrderType="";
var checkExistPortfolio="";
var manageButtonsActivity = [];
var orderingSection=[];
function pushToAryKeyVal(name, val) {
	   if(manageButtonsActivity[name]!= undefined){
		   var fieldStatus = manageButtonsActivity.name;
		   if(fieldStatus=="A")
			   manageButtonsActivity.push({name : val});		   
	   }else{		   
		   manageButtonsActivity.push({name : val});
	   }
}
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	 displayPortfolio();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	// ShowDistrict();
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

var required ="";
function displayOrHideSearchBox()
{
	districtNameFilter=1;
	var entityID=$('#entityType').val();
	$('#districtSearchId').val('');
	$('#districtSearchName').val('');
	if(entityID==1){
		$('#districtSearchLabel').hide();
		$('#districtSearchBox').hide();
		$('#searchPortfolio').hide();
		$('#districtClass').hide();	
		$('#districtClassMaster').hide();	
	}
	else
	{
		$('#districtClassMaster').show();
		$('#districtSearchLabel').show();
		$('#districtSearchBox').show();
		$('#searchPortfolio').show();
		$('#districtClass').show();	
		$('#districtSearchName').focus();
	}
}
function displayOrHideDistrict()
{
	var createPortfolio	=	document.getElementById("createPortfolio").value;
	$('#errordiv4DSPQ').empty();
	//alert("createPortfolio ::::: "+createPortfolio+" District Id ::::   "+$('#districtId').val());
	if(createPortfolio==1){
		//alert("mmmmm")
		$('#districtHideShow').show();
		$('#districtId').val($('#districtSearchId').val());
		//alert("MMM ::: District Id ::::::: "+$('#districtId').val()+" search Id ::::"+$('#districtSearchId').val());
	}
	else{
		//alert("nnnnn");
		$('#districtHideShow').hide();
		$('#districtSearchId').val($('#districtId').val());
		$('#districtId').val('');
		//alert("NNN ::: District Id ::::::: "+$('#districtId').val()+" search Id ::::"+$('#districtSearchId').val());
	}
}
function savePortfolioName()
{
	$('#errordiv4DSPQ').empty();
	var districtId		=	document.getElementById("districtId").value.trim();
	var portfolioName	=	document.getElementById("portfolioName").value.trim();
	var entityType	=	document.getElementById("entityTypeID").value.trim();
	var createPortfolio	=	document.getElementById("createPortfolio").value.trim();
	var txtBgColor = "#F30303";	
	var cnt=0;
	var focs=0;
	if(entityType==1 && createPortfolio==1){
		if(districtId=="")
		{
			$('#errordiv4DSPQ').append("&#149; Please select district name<BR>");
			if(focs==0){
				document.getElementById("districtName").focus();
				$('#errordiv4DSPQ').css("color",txtBgColor);
				cnt++;focs++;	
			}
		}
	}
	if(portfolioName==""){
		$('#errordiv4DSPQ').append("&#149; Please provide candidate portfolio name<BR>");
		if(focs==0){
			document.getElementById("portfolioName").focus();		
			$('#errordiv4DSPQ').css("color",txtBgColor);
			cnt++;focs++;		
		}
	}	
	if(cnt==0)
	{
		if(entityType==1 && createPortfolio==2){
			$('#defaultTMPortfolio').modal('show');
		}
		else
			savePortfolioValues();
	}
}
function savePortfolioValues()
{
	$('#defaultTMPortfolio').modal('hide');
	$('#errordiv4DSPQ').empty();
	var districtId		=	document.getElementById("districtId").value.trim();
	var portfolioName	=	document.getElementById("portfolioName").value.trim();
	var entityType	=	document.getElementById("entityTypeID").value.trim();
	var createPortfolio	=	document.getElementById("createPortfolio").value.trim();
	var txtBgColor = "#F30303";	
	var cnt=0;
	var focs=0;
	if(entityType==1 && createPortfolio==1){
		if(districtId=="")
		{
			$('#errordiv4DSPQ').append("&#149; Please select district name<BR>");
			if(focs==0){
				document.getElementById("districtName").focus();
				$('#errordiv4DSPQ').css("color",txtBgColor);
				cnt++;focs++;	
			}
		}
	}
	if(portfolioName==""){
		$('#errordiv4DSPQ').append("&#149; Please provide candidate portfolio name<BR>");
		if(focs==0){
			document.getElementById("portfolioName").focus();		
			$('#errordiv4DSPQ').css("color",txtBgColor);
			cnt++;focs++;		
		}
	}	
	if(cnt==0)
	{	
		ManageDspqAjax.savePortfolioName(districtId,portfolioName,{
			async: true,
			callback: function(data)
			{	
				var check=data.split("###")[0];
				var msg=data.split("###")[1]               
				if(check=="")
				{
					$('#duplicatePort').empty();
					$('#duplicatePort').append(msg);
					$('#duplicatePort').css("color",txtBgColor);
					$('#alertOnDuplicate').modal('show');
				}
				else
				{
					$('#portfolioID').val(msg);
					$('#editDspqID').val(msg);
					$('#showPortfolioDiv1').show();
					$('#continueDiv').hide();
					bindPortfolio();
				}
			},
			errorHandler:handleError 
		});
	}
}
function cancelDuplicatePort(){	
	$('#alertOnDuplicate').modal('hide');
}
function cancelDefaultTMPortfolio(){	
	$('#defaultTMPortfolio').modal('hide');
}
function duplicatePortfolio(){	
	$('#errordiv4DSPQ').empty();
	var districtId		=	document.getElementById("districtId").value;
	var portfolioName	=	document.getElementById("portfolioName").value.trim();
	var txtBgColor = "#F30303";	
	var cnt=0;
	var focs=0;
	var dspqPortfolioNameId=$('#editDspqID').val();
	ManageDspqAjax.updatePortfolioName(districtId,portfolioName,dspqPortfolioNameId,{
		async: true,
		callback: function(data)
		{	
			if(data.toString()!="")
			{
				displayPortfolio();
				$('#showPortfolioDiv1').show();
				$('#continueDiv').hide();
				$('#alertOnDuplicate').modal('hide');
				$('#portfolioID').val(data.toString());
			}
			$('#alertOnDuplicate').modal('hide');
		},
		errorHandler:handleError 
	});
	
}
function showPortfolioForm()
{
	$('#errordiv4DSPQ').empty();
	checkExistPortfolio="";
	$('#editDspqID').val("");
	callDiv('1');
	$('#portfolioName').val("");
	$('#cloneID').val();	
	
	$('#showPortfolioDiv').show();
	$('#showPortfolioDiv1').show();
	$('#dataGrid').hide();
	$('#searchBox').hide();
	$('#AddCandidate').hide();
	$('#EditCandidate').show();
	$('#continueDiv').hide();
	
}
function showPortfolio()
{	
	var groupId = 1;
	$('#checkPopup').val(1);
	$('#errordiv4DSPQ').empty();
	$('#createPortfolioHideShow').show();
	checkExistPortfolio="";
	$('#editDspqID').val("");
	$('#portfolioName').val("");
	$('#cloneID').val("");
	var entityTypeID=$("#entityTypeID").val();
	if(entityTypeID!=2){
	$('#districtName').val($("#districtSearchName").val());
	$('#districtId').val($("#districtSearchId").val());
	}
	if($("#districtSearchName").val()!=""){
		$('#districtName').prop('disabled', true);
	}else{
		$('#districtName').prop('disabled', false);
	}
	$('#showPortfolioDiv').show();
	$('#searchBox').hide();
	$('#dataGrid').hide();
	$('#AddCandidate').hide();
	$('#EditCandidate').show();
	$('#continueDiv').show();
	$('#sectionid').val(groupId);
	document.getElementById("sectionStart").value	=	1;
	document.getElementById("sectionEnds").value	=	4;
	document.getElementById("groupId").value		=	1; 
	$('#changebk2').attr('src', 'images/Academicsdspq1.png'); 
	$('#changebk3').attr('src', 'images/Credentialsdspq1.png'); 
	$('#changebk4').attr('src', 'images/Experiencedpsq1.png'); 
	$('#changebk1').attr('src', 'images/Personaldspq.png'); 
	$('#litab1').addClass("active");
	$('#litab2').removeClass("active");
	$('#litab3').removeClass("active");
	$('#litab4').removeClass("active");
	$('#back').hide();
	$('#Next').show();
	var portfolioId  = $('#PortfolioNameIDS').val();
	if($("#districtMasterId").val()!=0){
		
		$('#districtName').val($("#districtMasterName").val());
		$("#districtId").val($("#districtMasterId").val());
	}
}
function bindPortfolio()
{
	var groupId = 1;
	var districtId=$('#districtId').val();
	var portfolioId  = $('#PortfolioNameIDS').val();
	$('#loadingDiv').show();
	ManageDspqAjax.getDspqAccordian(groupId,portfolioId,districtId,{
		async: true,
		callback: function(data)
		{
			groupID=groupId;
			document.getElementById('my-tab-content').innerHTML=data.split("@@@")[0];
			$(".tempToolTip").tooltip();
			$('#loadingDiv').hide();
		},
		errorHandler:handleError 
});
}
function cancelPortfolioForm()
{	
	var showPopUp = $('#checkPopup').val();
	 $('#checkClickOnBackPort').val(1);
	var checkGlobal = document.getElementById("dspqGlobalLster").value;
	if(showPopUp==2 && checkGlobal>0)
		alertOnClickTab(1);
	else{
		$('#checkPopup').val(0);
		$('#dspqGlobalLster').val(0);
		$('#editDspqID').val("");
		$('#portfolioID').val("");
		$('#errordiv4DSPQ').empty();
		$('#showPortfolioDiv').hide();
		$('#showPortfolioDiv1').hide();
		$('#AddCandidate').show();
		$('#EditCandidate').hide();
		$('#dataGrid').show();
		$('#searchBox').show();
		$('#back').hide();
		$('#Next').show();
		$('#portfolioName').val("");
		$('#videoSource').val(resourceJSON.mainPortfolio);
		sectionExternalChecked="";
		sectionInternalChecked="";
		sectionITransferChecked="";	
		displayPortfolio();
	}
}
function blankAllField()
{
	$('#checkClickOnBackPort').val(0);
	$('#dspqGlobalLster').val(0);
	$('#checkPopup').val(0);
	$('#editDspqID').val("");
	$('#errordiv4DSPQ').empty();
	$('#showPortfolioDiv').hide();
	$('#showPortfolioDiv1').hide();
	$('#AddCandidate').show();
	$('#EditCandidate').hide();
	$('#dataGrid').show();
	$('#searchBox').show();
	$('#back').hide();
	$('#Next').show();
	$('#portfolioName').val("");
	$('#videoSource').val(resourceJSON.mainPortfolio);
	sectionExternalChecked="";
	sectionInternalChecked="";
	sectionITransferChecked="";
	displayPortfolio();
}
function displayPortfolioMaster()
{
/*	alert($("#districtMasterId").val()+"  ------  "+$('#districtSearchId').val());
	if($('#districtSearchId').val()==undefined && $("#districtMasterId").val()!=0){
		alert(11111111111+"       "+$("#districtMasterId").val())
		$('#districtSearchId').val($("#districtMasterId").val());
		alert($('#districtSearchId').val());
	}*/
	displayPortfolio();
}
var showCounter=0;
function displayPortfolio()
{
	$('#videoSource').val(resourceJSON.mainPortfolio);
	var entityType= $("#entityType").val();
	var districtId=0;
	if(entityType!=1){
	 districtId = $('#districtSearchId').val();
	 if((districtId==undefined || districtId=="" )&& $("#districtMasterId").val()!=0){
		  districtId=$("#districtMasterId").val();
		  $("#districtName").attr("disabled","disabled");
		 }else{
		  districtId=$("#districtId").val();
		  $("#districtName").attr("disabled","disabled");
		 }
	}else{
		$("#districtName").removeAttr("disabled");
	}
	var districtIdPage = $('#districtIdPage').val();
	
	if(districtIdPage!=districtId){
		$('#districtIdPage').val(districtId);
		page=1;
	}
	$("#districtId").val(districtId);
	$("#districtName").val($("#districtSearchName").val());
	ManageDspqAjax.displayPortfolioGrid(districtId,noOfRows,page,{
		async: false,
		callback: function(data)
		{
			$('#dspqGrid').html(data);
			$(".tempToolTip").tooltip();
			//applyScrollOnTbl();		
		},
		errorHandler:handleError 
});
	hideSearchAgainMaster();
}

function setAccordion(val){
    $(function ($) {
             var val2= $("'"+val+"'");
			// var val2= $('#collapse0,#collapse1,#collapse2,#collapse3');
              val2.on('show.bs.collapse hidden.bs.collapse', function () {
                  $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
              })
          });
                $('.accordion').collapse();
                $('.accordion').on('show', function (e) {
                $(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
                });
                $('.accordion').on('hide', function (e) {
                $(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
    });
}


var groupID;
function callDiv(groupId,temp)
{	
	//$('#loadingDiv').show();
	var editID= document.getElementById("editDspqID").value;
	$('#sectionid').val(groupId);
	if(groupId == 1){
		$('#videoSource').val(resourceJSON.personalInformationVideo);
		document.getElementById("sectionStart").value	=	1;
		document.getElementById("sectionEnds").value	=	4;
		document.getElementById("groupId").value		=	1; 
		$('#changebk2').attr('src', 'images/Academicsdspq1.png'); 
		$('#changebk3').attr('src', 'images/Credentialsdspq1.png'); 
		$('#changebk4').attr('src', 'images/Experiencedpsq1.png'); 
		$('#changebk1').attr('src', 'images/Personaldspq.png'); 
		$('#litab1').addClass("active");
		$('#litab2').removeClass("active");
		$('#litab3').removeClass("active");
		$('#litab4').removeClass("active");
		$('#back').hide();
		$('#Next').show();
	}else if(groupId == 2){
		$('#videoSource').val(resourceJSON.academicsVideo);
		document.getElementById("sectionStart").value	=	5;
		document.getElementById("sectionEnds").value	=	5;
		document.getElementById("groupId").value		=	2;
		$('#changebk1').attr('src', 'images/Personaldspq1.png'); 
		$('#changebk3').attr('src', 'images/Credentialsdspq1.png'); 
		$('#changebk4').attr('src', 'images/Experiencedpsq1.png'); 
		$('#changebk2').attr('src', 'images/Academicsdspq.png'); 
		$('#litab1').removeClass("active");
		$('#litab2').addClass("active");
		$('#litab3').removeClass("active");
		$('#litab4').removeClass("active");
		$('#back').show();
		$('#Next').show();
	}else if(groupId == 3){
		$('#videoSource').val(resourceJSON.credentialsVideo);
		document.getElementById("sectionStart").value	=	6;
		document.getElementById("sectionEnds").value	=	10;
		document.getElementById("groupId").value		=	3;
		$('#changebk1').attr('src', 'images/Personaldspq1.png'); 
		$('#changebk2').attr('src', 'images/Academicsdspq1.png');  
		$('#changebk4').attr('src', 'images/Experiencedpsq1.png'); 
		$('#changebk3').attr('src', 'images/Credentialsdspq.png'); 
		$('#litab1').removeClass("active");
		$('#litab2').removeClass("active");
		$('#litab3').addClass("active");
		$('#litab4').removeClass("active");
		$('#back').show();
		$('#Next').show();
	}else if(groupId == 4){
		$('#videoSource').val(resourceJSON.experiencesVideo);
		document.getElementById("sectionStart").value	=	11;
		document.getElementById("sectionEnds").value	=	14;
		document.getElementById("groupId").value		=	4;
		$('#changebk1').attr('src', 'images/Personaldspq1.png'); 
		$('#changebk2').attr('src', 'images/Academicsdspq1.png'); 
		$('#changebk3').attr('src', 'images/Credentialsdspq1.png'); 
		$('#changebk4').attr('src', 'images/Experiencedpsq.png'); 
		$('#litab1').removeClass("active");
		$('#litab2').removeClass("active");
		$('#litab3').removeClass("active");		
		$('#litab4').addClass("active");
		$('#back').show();
		$('#Next').hide();
	}
	if(temp==0)
		savePortfolioOnClickTab(groupId);
	var portfolioId  = $('#PortfolioNameIDS').val();
	var districtId=$('#districtId').val();
	$('#loadingDiv').show();
	ManageDspqAjax.getDspqAccordian(groupId,portfolioId,districtId,{
		async: true,
		callback: function(data)
		{
			groupID=groupId;
			document.getElementById('my-tab-content').innerHTML=data.split("@@@")[0];	
			if(sectionExternalChecked==1)
			{
				$('#sectionExternal').prop('checked', true);
				hideOrShowApplicant();
			}
			if(sectionInternalChecked==1)
			{
				$('#sectionInternal').prop('checked', true);
				hideOrShowApplicant();
			}
			if(sectionITransferChecked==1)
			{
				$('#sectionITransfer').prop('checked', true);
				hideOrShowApplicant();
			}
			if(editID!=null && editID!="")
			{
				//alert(editID);
				editDspqForSave(editID);
			}
			$(".tempToolTip").tooltip();
			
			
			// setAccordion(data.split("@@@")[1]);
			var showPopUp = $('#checkPopup').val();
			var checkClickOnBackPort = $('#checkClickOnBackPort').val();
			if(showPopUp==2 && checkClickOnBackPort==1)
				blankAllField();
			$('#loadingDiv').hide();
		},
		errorHandler:handleError 
});
	
}

function callDivs(groupId)
{
	$('#errordiv4DSPQ').empty();
	checkExistPortfolio="";
	$('#editDspqID').val("");
	var editID= document.getElementById("editDspqID").value;
	$('#sectionid').val(groupId);
	document.getElementById("sectionStart").value	=	1;
	document.getElementById("sectionEnds").value	=	4;
	document.getElementById("groupId").value		=	1; 
	$('#changebk2').attr('src', 'images/Academicsdspq1.png'); 
	$('#changebk3').attr('src', 'images/Credentialsdspq1.png'); 
	$('#changebk4').attr('src', 'images/Experiencedpsq1.png'); 
	$('#changebk1').attr('src', 'images/Personaldspq.png'); 
	$('#litab1').addClass("active");
	$('#litab2').removeClass("active");
	$('#litab3').removeClass("active");
	$('#litab4').removeClass("active");
	$('#back').hide();
	$('#Next').show();
	var portfolioId  = $('#PortfolioNameIDS').val();
	var districtId=$('#districtId').val();
	ManageDspqAjax.getDspqAccordian(groupId,portfolioId,districtId,{
		async: false,
		callback: function(data)
		{
			groupID=groupId;
			document.getElementById('my-tab-content').innerHTML=data.split("@@@")[0];
			$(".tempToolTip").tooltip();
		},
		errorHandler:handleError 
	});
	$('#portfolioName').val("");
	
	try
	{
	}catch(e){alert(e)}
	$('#showPortfolioDiv').show();
	$('#showPortfolioDiv1').show();
	$('#dataGrid').hide();
	$('#searchBox').hide();
	$('#AddCandidate').hide();
	$('#EditCandidate').show();
}
function manageDefaultRestore(secId)
{
	ManageDspqAjax.getFieldIdBySec(secId,{
		async: true,
		callback: function(data)
		{
		if(data.length!=0)
		{
			for(i=0;i<data.length;i++)
			{	
				var sectionId=document.getElementById("sectionIsRestore"+secId);	
				if(sectionId.checked)
					$('#restore'+data[i].dspqFieldId).prop('checked', true);
				else
					$('#restore'+data[i].dspqFieldId).prop('checked', false);
			}
		}
		},
		errorHandler:handleError 
});
}
function saveAndContinue()
{
	//alert(groupID);
	groupID++;
	callDiv(groupID);
	
}
function savePortfolio(tabId){
	
	$('#dspqGlobalLster').val(tabId);
	var dspqId=$("#dspqId").val();
	var districtId=$("#editDistrictId").val();	
	var cloneEditFlag =  $("#cloneEditFlag").val();	
	if(cloneEditFlag==1)
		savePortfolioNext();
	else{
		$('#loadingDiv').show();
		setTimeout(function() {jobCategoryAttachedNotification(3,dspqId,districtId,"");}, 10);
	}
	
}

function savePortfolioNext(){
	var tabId=$('#dspqGlobalLster').val();
	$('#dspqGlobalLster').val(0);
	$('#errordiv4DSPQ').empty();
	$('#errorValid').empty();
	var portfolioArray1  =	[];	
	secArray=[];
	var fieldName = "",req = "",instruction = "",tooltip="",status="",numberRequired=null,applicantTypeStatus="",othersAttribute="",sectionOrder="";
	var districtId		=	document.getElementById("districtId").value;
	var portfolioName	=	document.getElementById("portfolioName").value;
	var sectionExternal		=	document.getElementById("sectionExternal");
	var sectionInternal		=	document.getElementById("sectionInternal");
	var sectionITransfer	=	document.getElementById("sectionITransfer");
	var groupId 	 = document.getElementById("groupId").value;
	var sectionList	=	document.getElementById("sectionList").value;
	var fieldList		=	document.getElementById("fieldList").value;
	
	var txtBgColor = "#F30303";	
	var count = 0;
	var cnt=0;
	var focs=0;	
	var valid=false;
	var validSection=false;	
	var sectionRequired=false;
	var validAdditionalField=false;
	if(portfolioName==""){
		$('#errordiv4DSPQ').append("&#149; Please provide portfolio name<BR>");
		if(focs==0)
			document.getElementById("portfolioName").focus();
		
		$('#errordiv4DSPQ').css("color",txtBgColor);
		cnt++;focs++;
		return;
	}	
	if(count>0){
			$("#errordiv4DSPQ").html("&#149; Please fill all the text boxes<br>");
			$('#errordiv4DSPQ').css("color",txtBgColor);
			cnt++;focs++;
	}
	var k=1,L=1,M=1;	
	if(sectionInternal.checked==true)
	{
		M=2;
		sectionInternalChecked=1;
	}
	if(sectionITransfer.checked==true)
	{
		k=3;
		sectionITransferChecked=1;
	}
	var sectionArrayList=sectionList.split(",");
	var sectionOrderId=[];
	if(orderingSection!="")
			sectionOrderId= String(orderingSection).split(",");
	var fieldArrayList=fieldList.split("##");
	var secID="", fieldID="";
	try{
		for(var sec=1; sec<=sectionArrayList.length-1;sec++)
		{
			secID=sectionArrayList[sec];
			var fieldArray=fieldArrayList[sec].split(",");
			
			 var mainSection=document.getElementById("mainSection"+secID);
			 var allSection=document.getElementById("allSectionChecked_"+secID).value;			 
			  if(mainSection.checked==true || (mainSection.checked==false && allSection==1))
			   {
				  valid=false;
				  sectionRequired=false;
				  validSection=false;
				  
				for(var field=1; field<=fieldArray.length-1;field++)
				{
				fieldID=fieldArray[field];
				if(fieldID == 0)
				{
					for(var j=1;j<=3;j++)
					{	
						if(j==1)
						{
							L=1;
							applicantType="E";
							applicantTypeStatus="A";
						}
						if(j==2)
						{
							applicantType="I";
							if(M==2)
							{
								L=2;
								applicantTypeStatus="A";
							}
							else
							{
								L=1;
								applicantTypeStatus="I";
							}
						}
						if(j==3)
						{
							applicantType="T";
							if(k==3)
							{
								L=3;
								applicantTypeStatus="A";
							}
							else
							{
								if(M==2)
								{
									L=2;
									applicantTypeStatus="I";
								}
								else
									L=1
								applicantTypeStatus="I";
							}
						}
						if(sectionOrderId.length>0)
							sectionOrder=getIndex(orderingSection,secID);
						else
							sectionOrder	=	document.getElementById("sectionOrder_"+L+"_"+secID).value;
						instruction		=	document.getElementById("instruction_"+L+"_"+secID).value;
						//displayName		=	document.getElementById("sectionDisplay"+secID).value;	
						displayName		=	document.getElementById("sectionfield_"+L+"_"+secID).value;							
						
						var mainSection =	 document.getElementById("mainSection"+secID);								
						if(mainSection.checked==true)
						{
							status		=	document.getElementById("sectionstatus_"+L+"_"+secID).value;
						}									
						else
							status="I";
						req		=	document.getElementById("sectionreq_"+L+"_"+secID).value;
						if(req==0)
							req = false;
						else
							req = true;
						
						
						tooltip		=	document.getElementById("sectiontooltip_"+L+"_"+secID).value;
						var dspqSectionMaster = 	{sectionId:dwr.util.getValue("sectionId"+secID)};
						
						portfolioArray1.push({
							"dspqSectionMaster" : dspqSectionMaster,			
							"applicantType"  : applicantType,
							"instructions" : instruction,
							"displayName"  : displayName,
							"isRequired"  : req,
							"tooltip" :tooltip,
							//"numberRequired"  : numberRequired,
							"status" :status,
							"applicantTypeStatus" : applicantTypeStatus,
							"sectionOrder" : sectionOrder
						});
								
					}
					
				}
				else
				{
					////////////// work start here////////////////////////
						var TMAddTMDef=document.getElementById("TMAddTMDef"+fieldID).value;
						for(var j=1;j<=3;j++)
						{	
							if(j==1)
							{
								L=1;
								applicantType="E";
								applicantTypeStatus="A";
							}
							if(j==2)
							{
								applicantType="I";
								
								if(M==2)
								{
									L=2;
									applicantTypeStatus="A";
								}
								else
								{
									L=1;
									applicantTypeStatus="I";
								}
							}
							if(j==3)
							{
								applicantType="T";
								if(k==3)
								{
									L=3;
									applicantTypeStatus="A";
								}
								else
								{
									if(M==2)
									{
										L=2;
										applicantTypeStatus="I";
									}							
									else
										L=1;
									applicantTypeStatus="I";
								}
							}
							
							if(TMAddTMDef=="Default")
							{
								if(compareArrayValue(secID)=="1")
								{
									if(sectionOrderId.length>0)
										sectionOrder=getIndex(orderingSection,secID);
									else
										sectionOrder	=	document.getElementById("sectionOrder_"+L+"_"+secID).value;
									instruction		=	document.getElementById("instruction_"+L+"_"+secID).value;
									//displayName		=	document.getElementById("sectionDisplay"+secID).value;	
									displayName		=	document.getElementById("sectionfield_"+L+"_"+secID).value;
									numberRequired = null;
									if(secID=="9")
										numberRequired		=	document.getElementById("Refrences_"+L).value;
									if(secID=="6")
									{
										numberRequired		=	document.getElementById("degreeType_"+L).value;
										if(numberRequired==0)
											numberRequired=null;
										var degreeHierarchy		=	document.getElementById("degreeHierarchy_"+L);
										if(degreeHierarchy.checked==true)
											othersAttribute=0;
										else
											othersAttribute=1;
									}
									var mainSection =	 document.getElementById("mainSection"+secID);	
									if(mainSection.checked==true)
									{
										status		=	document.getElementById("sectionstatus_"+L+"_"+secID).value;
									}									
									else
										status="I";
									req		=	document.getElementById("sectionreq_"+L+"_"+secID).value;
									if(req==0)
										req = false;
									else
									{
										req = true;
										sectionRequired=true;
									}
									tooltip		=	document.getElementById("sectiontooltip_"+L+"_"+secID).value;
									var dspqSectionMaster = 	{sectionId:dwr.util.getValue("sectionId"+secID)};		
									portfolioArray1.push({
										"dspqSectionMaster" : dspqSectionMaster,			
										"applicantType"  : applicantType,
										"instructions" : instruction,
										"displayName"  : displayName,
										"isRequired"  : req,
										"tooltip" :tooltip,
										"numberRequired"  : numberRequired,
										"status" :status,
										"othersAttribute" :othersAttribute,
										"applicantTypeStatus" : applicantTypeStatus,
										"sectionOrder" : sectionOrder
									});
									
								}
								var req1		=	document.getElementById("req_1_"+fieldID).value;						
								var status1		=	document.getElementById("status_1_"+fieldID).value;
								var additionalField1=document.getElementById("additionalFieldID"+fieldID);
								
								if(status1=="A")
								{
									//alert(req1 +" "+status1 +" secID "+secID +" fieldID "+fieldID);
									if(valid==false)
									{									
										valid=true;										
									}
									if(sectionRequired==true)
									{
										if(req1==true)
											validSection=true;
									}
								}
								fieldName		=	document.getElementById("field_"+L+"_"+fieldID).value;						
								req				=	document.getElementById("req_"+L+"_"+fieldID).value;						
								tooltip		=	document.getElementById("tooltip_"+L+"_"+fieldID).value;
								status		=	document.getElementById("status_"+L+"_"+fieldID).value;								
								/*if(statusMain=="I")
									status		=	"I";
								else
									status		=	document.getElementById("status_"+j+"_"+fieldID).value;*/
								if(req==1)
									req=true;
								else
									false;
								
								if(tooltip=="")
									tooltip=null;
								if(instruction=="")
									instruction=null;
								
								var dspqFieldMaster = 	{dspqFieldId:dwr.util.getValue("dspqFieldId"+fieldID)};
								portfolioArray1.push({
									"dspqFieldMaster"  : dspqFieldMaster,
									"displayName"  : fieldName,
									//"numberRequired"  : numberRequired,
									"isRequired"  : req,
									"applicantType"  : applicantType,					
									"tooltip" :tooltip,
									"status" :status,
									"applicantTypeStatus" : applicantTypeStatus									
								});
								
							}
							else
							{
								
								var additionalField=document.getElementById("additionalFieldID"+fieldID);
								/*if(secID==18)
								{
									if(additionalField.checked)
									{			
										validAdditionalField=true;
										valid=true;
									}									
								}
								if(secID==16)
								{
									if(additionalField.checked)
									{			
										validAdditionalField=true;
										valid=true;
									}
								}*/
								if(additionalField.checked)
								{
									if(compareArrayValue(secID)=="1")
									{			
										if(sectionOrderId.length>0)
											sectionOrder=getIndex(orderingSection,secID);
										else
									 		sectionOrder	=	document.getElementById("sectionOrder_"+L+"_"+secID).value;
										instruction		=	document.getElementById("instruction_"+L+"_"+secID).value;
										//displayName		=	document.getElementById("sectionDisplay"+secID).value;	
										displayName		=	document.getElementById("sectionfield_"+L+"_"+secID).value;
										var mainSection =	 document.getElementById("mainSection"+secID);	
										if(mainSection.checked==true)
										{
											status		=	document.getElementById("sectionstatus_"+L+"_"+secID).value;
										}									
										else
											status="I";
										req		=	document.getElementById("sectionreq_"+L+"_"+secID).value;
										if(req==0)
											req = false;
										else
										{
											req = true;
											sectionRequired=true;
										}
										tooltip		=	document.getElementById("sectiontooltip_"+L+"_"+secID).value;
										var dspqSectionMaster = 	{sectionId:dwr.util.getValue("sectionId"+secID)};
										
		
										portfolioArray1.push({
											"dspqSectionMaster" : dspqSectionMaster,			
											"applicantType"  : applicantType,
											"instructions" : instruction,
											"displayName"  : displayName,
											"isRequired"  : req,
											"tooltip" :tooltip,
											//"numberRequired"  : numberRequired,
											"status" :status,
											"applicantTypeStatus" : applicantTypeStatus,
											"sectionOrder" : sectionOrder
										});
									}
									var req1		=	document.getElementById("req_1_"+fieldID).value;						
									var status1		=	document.getElementById("status_1_"+fieldID).value;
									var additionalField1=document.getElementById("additionalFieldID"+fieldID);
									
									if(status1=="A")
									{
										//alert(req1 +" "+status1 +" secID "+secID +" fieldID "+fieldID);
										if(valid==false)
										{									
											valid=true;
											valid1=1;
										}
										if(sectionRequired==true)
										{
											if(req1==true)
												validSection=true;
										}
									}
									fieldName		=	document.getElementById("field_"+L+"_"+fieldID).value;
									req				=	document.getElementById("req_"+L+"_"+fieldID).value;
									tooltip		=	document.getElementById("tooltip_"+L+"_"+fieldID).value;
									status		=	document.getElementById("status_"+L+"_"+fieldID).value;
									if(req==1)
										req=true;
									else
										false;
									if(tooltip=="")
										tooltip=null;
									if(instruction=="")
										instruction=null;
									var dspqFieldMaster = 	{dspqFieldId:dwr.util.getValue("dspqFieldId"+fieldID)};
									portfolioArray1.push({
										"dspqFieldMaster"  : dspqFieldMaster,
										"displayName"  : fieldName,	
										//"numberRequired"  : numberRequired,
										"isRequired"  : req,
										"applicantType"  : applicantType,					
										"tooltip" :tooltip,
										"status" :status,
										"applicantTypeStatus" : applicantTypeStatus										
									});
									
								}						
							}
						}
						if(TMAddTMDef!="Default")
						{
							var additionalField=document.getElementById("additionalFieldID"+fieldID);
							if(additionalField.checked)
								secArray.push(secID);
						}
						else
							secArray.push(secID);
				
						////////////// work end here////////////////////////
			 }
				
			}
				
				if(valid==false)
				{
					$('#alertOnError').modal('show');
					var fieldSectionName		=	document.getElementById("section_Field_"+secID).value;										
					$('#errorValid').append("&#149; Please select atleast one Additional active field in "+fieldSectionName+"<BR>");					
					$('#errorValid').css("color",txtBgColor);
					cnt++;
				}
				else
				{
					
					if(sectionRequired==true)
					{
						if(validSection==false)
						{
							$('#alertOnError').modal('show');
							var fieldSectionName		=	document.getElementById("section_Field_"+secID).value;										
							$('#errorValid').append("&#149; Please select atleast one required field in "+fieldSectionName+" Sub-Section.<BR>");					
							$('#errorValid').css("color",txtBgColor);
							cnt++;
						}
					}
					
				}
				
			}
		}
	}catch(e){
		alert(e);
	}	
	// return false;
	if(cnt==0){
	$('#loadingDiv').show();
		try{			
		 var checkExistPortfolio=$('#cloneID').val();		
		 var dspqPortfolioNameId=$('#portfolioID').val();
		 var editId=$('#editDspqID').val();
		 if(dspqPortfolioNameId=="" || dspqPortfolioNameId==0)
			 dspqPortfolioNameId=editId;
			ManageDspqAjax.saveDspqFieldsValues(districtId,portfolioArray1,portfolioName,groupId,dspqPortfolioNameId,checkExistPortfolio,{
			async: true,
			callback: function(data)
			{
				if(data=="Exists")
				{	
					$('#loadingDiv').hide();
					$('#duplicatePort').empty();
					$('#duplicatePort').append("Candidate portfolio name already exists!");
					$('#duplicatePort').css("color",txtBgColor);
					$('#alertOnDuplicate').modal('show');
					window.location.href="#";
				}
				else
				{			
					if(checkExistPortfolio=='clone')
					{
						$('#portfolioID').val(data);
						$('#editDspqID').val(data);
						$('#cloneID').val(data);
					}
					checkExistPortfolio="2";
					displayPortfolio();
					if(tabId==0)
					{
						$('#videoSource').val(resourceJSON.mainPortfolio);
						cancelPortfolioForm();						
					}
					else if(tabId==1)
					{
						groupID++;
						$('#Next').show();
						$('#back').show();
						if(groupID == 4)					
							$('#Next').hide();					
						else if(groupID == 1)				
							$('#back').hide();				
											
						callDiv(groupID);
					}
					else
					{
						groupID--;
						$('#Next').show();
						$('#back').show();
						if(groupID == 4)					
							$('#Next').hide();					
						else if(groupID == 1)				
							$('#back').hide();													
						callDiv(groupID);
					}
					$('#loadingDiv').hide();
			}
			},
		});
		}catch(e){alert(e)}
	}
}
function compareArrayValue(secID)
{
	var i = secArray.indexOf(secID);
    if(i == -1) {
    	return "1";
    	
    	
    }
    else{
    	return "0";
    	
    	
    }
}
function editDspq(dspqId,cloneEditFlag,districtId,districtName)
{
	$("#dspqId").val(dspqId);
	$("#cloneEditFlag").val(cloneEditFlag);
	$("#editDistrictId").val(districtId);
	$("#editDistrictName").val(districtName);
	$('#loadingDiv').show();
	if(cloneEditFlag==2)
		setTimeout(function() {jobCategoryAttachedNotification(1,dspqId,districtId,"");}, 10);
	else
		portfolioChangeWarningYesForEdit();
}
function editDspqForSave(dspqId)
{
	var oldId=$('#editDspqID').val();	
	if(dspqId!=oldId)
		callDivs('1');
	orderingSection=[];
	$('#showPortfolioDiv').show();
	$('#showPortfolioDiv1').show();
	var groupId 	 = document.getElementById("groupId").value;
	var sectionList	=	document.getElementById("sectionList").value;
	var fieldList		=	document.getElementById("fieldList").value;
	var TMAddFieldList		=	document.getElementById("TMAddFieldList").value;
	var r=false,r1=false;
	var sectionArrayList=sectionList.split(",");
	var fieldArrayList=fieldList.split("##");
	var TMAddList=TMAddFieldList.split(",");
	var field1="",field2="",field3="",field1Id="",field2Id="";
	var section1="",section2="",section3="",sectionId="",sectionId="";
	var secID="";
	var arrayField=[];
	var arrField  = [];
	for(var sec=1; sec<=sectionArrayList.length-1;sec++)
	{		
		var fieldArray=fieldArrayList[sec].split(",");			
		for(var field=1; field<=fieldArray.length-1;field++)
		{
			if(fieldArray[field]!=null && fieldArray[field]!="")
				arrayField.push(fieldArray[field]);
		}
	}
	orderingSection=[];
	var sectionOrderArray=$("#sectionOrderArray").val();
	if(sectionOrderArray!=null && sectionOrderArray!="")
		orderingSection=sectionOrderArray.split(",");
	document.getElementById("editDspqID").value=dspqId;
	var ss=dspqId;
	ManageDspqAjax.editDspq(dspqId,{
		async: false,
		callback: function(data)
		{
		sectionArrayList.splice(0, 1);
	try{
		for (var i=0 ;i<=data.length-1;i++ )
		{
			if((data[i].applicantType)=="E")			
				j=1;
			if((data[i].applicantType)=="I")
			{
				j=2;
				if((data[i].applicantTypeStatus)=="A")
				{
					if(r==false)
					{
						document.getElementById("sectionInternal").checked = true;				
						hideOrShowApplicant();
						r=true;
					}
				}
			}
			if((data[i].applicantType)=="T")
			{
				j=3;
				if((data[i].applicantTypeStatus)=="A")
				{
					if(r1==false)
					{
						document.getElementById("sectionITransfer").checked = true;
						hideOrShowApplicant();
						r1=true;
					}
				}
			}
			if((data[i].dspqSectionMaster)!=null)
			{
				secID=(data[i].dspqSectionMaster.sectionId).toString();
				try{
					if(data[i].status=="A")
						document.getElementById("allSectionChecked_"+secID).value =1;
					}catch (e) {
						
						// TODO: handle exception
					}
				var chk = sectionArrayList.indexOf(secID);
				if(chk!=-1)
				{					
					if(data[i].instructions!=null)
					{
						document.getElementById("instruction_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =data[i].instructions;
						$("#secInstruction_"+j+"_"+secID).attr('title', data[i].instructions);
					}
					if(data[i].displayName!=null)
					{
						document.getElementById("sectionfield_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =data[i].displayName;
												
						if(j==1)
						{
							sectionId = data[i].dspqSectionMaster.sectionId;
							section1 = data[i].displayName;
							$("#secLabel_"+j+"_"+secID).attr('title', section1);
						}
						if(j==2)
						{
							section2 = data[i].displayName;
							$("#secLabel_"+j+"_"+secID).attr('title', section2);
						}
						if(j==3)
						{
							sectionId = data[i].dspqSectionMaster.sectionId;
							section3 = data[i].displayName;
							$("#secLabel_"+j+"_"+secID).attr('title', section3);
						}
						if(sectionId ==sectionId)
						{
							if(section1==section2 && section2==section3 && section1==section3)
							{
								$('#sectionLabel_'+data[i].dspqSectionMaster.sectionId).html(section1);
							}
							else
							{								
								if(r==true && r1==true)
									$('#sectionLabel_'+data[i].dspqSectionMaster.sectionId).html(section1+"/"+section2+"/"+section3);
								else if(r==true)
									$('#sectionLabel_'+data[i].dspqSectionMaster.sectionId).html(section1+"/"+section2);
								else if(r1==true)
									$('#sectionLabel_'+data[i].dspqSectionMaster.sectionId).html(section1+"/"+section3);
							}
						}
					
					}
					if(data[i].isRequired==true)
					{
						document.getElementById("sectionreq_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =1;
						$('#secReqOpt_'+j+'_'+secID).removeClass("iconOptional");
						$('#secReqOpt_'+j+'_'+secID).addClass("iconRequired");			
						$('#secReqOpt_'+j+'_'+secID).attr('title', 'Required');
						$('#secReqOpt_'+j+'_'+secID).text('R');
					}
					if(data[i].tooltip!=null)
					{
						document.getElementById("sectiontooltip_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =data[i].tooltip;
						$("#secHelp_"+j+"_"+secID).attr('title', data[i].tooltip);
					}
					
					document.getElementById("sectionstatus_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =data[i].status;						
					if(data[i].status=="I")
					{
						var secID=data[i].dspqSectionMaster.sectionId;
						$('#sectionstatus_'+j+'_'+secID).val('I');			
						$('#sectionRequired'+j+'_'+secID).removeClass("activeIcon");
						$('#sectionRequired'+j+'_'+secID).addClass("inactiveIcon");
						$('#sectionRequired'+j+'_'+secID).attr('title', 'Inactive')
						$('#sectionRequired'+j+'_'+secID).text('X');
						//$('#instructionhide_'+j+'_'+secID).hide();						
					}
					else
					{
						$("#mainSection"+secID).prop('checked', true);
						if(secID!=25)
						{
							$('#panel'+secID).removeClass("hide");
							$('#panel'+secID).addClass("show");
						}							
						//$('#instructionHide_'+j+'_'+secID).show();
					}
					if(secID==9)
						document.getElementById("Refrences_"+j).value =data[i].numberRequired;
					if(secID==6)
					{
						if(data[i].numberRequired!=null)
							document.getElementById("degreeType_"+j).value =data[i].numberRequired;						
						if(data[i].othersAttribute==1)						
							$("#degreeHierarchy_"+j+j).prop('checked', true);
					}
					
					
				}
			}			
			
			if((data[i].dspqFieldMaster)!=null)
			{
				pushToAryKeyVal(data[i].dspqFieldMaster.dspqFieldId,data[i].status);
				
				var fieldIDS=(data[i].dspqFieldMaster.dspqFieldId).toString();
				var chk = arrayField.indexOf(fieldIDS);
				if(chk!=-1)
				{		
					var TMFieldID = TMAddList.indexOf(fieldIDS);
					if(TMFieldID!=-1)
					{
						document.getElementById("additionalFieldID"+data[i].dspqFieldMaster.dspqFieldId).checked = true;						
						if(j==1)
						hideShowAdditionalFields(data[i].dspqFieldMaster.dspqFieldId,secID,"array"+secID);						
					}
					if(data[i].displayName!=null)
					{
						if(j==1)
						{
							field1Id = data[i].dspqFieldMaster.dspqFieldId;
							field1 = data[i].displayName;
							$("#fieldLabel_"+j+"_"+fieldIDS).attr('title', field1);
						}
						if(j==2)
						{
							field2 = data[i].displayName;
							$("#fieldLabel_"+j+"_"+fieldIDS).attr('title', field2);
						}
						if(j==3)
						{
							field2Id = data[i].dspqFieldMaster.dspqFieldId;
							field3 = data[i].displayName;
							$("#fieldLabel_"+j+"_"+fieldIDS).attr('title', field3);
						}
						if(field1Id ==field2Id)
						{
							if(field1==field2 && field2==field3 && field1==field3)
							{
								$('#1displayField'+data[i].dspqFieldMaster.dspqFieldId).html(field1);
							}
							else
							{								
								if(r==true && r1==true)
									$('#1displayField'+data[i].dspqFieldMaster.dspqFieldId).html(field1+"/"+field2+"/"+field3);
								else if(r==true)
									$('#1displayField'+data[i].dspqFieldMaster.dspqFieldId).html(field1+"/"+field2);
								else if(r1==true)
									$('#1displayField'+data[i].dspqFieldMaster.dspqFieldId).html(field1+"/"+field3);
							}
						}
						document.getElementById("field_"+j+"_"+data[i].dspqFieldMaster.dspqFieldId).value =data[i].displayName;
					}
					var fieldRequired=data[i].dspqFieldMaster.dspqFieldId;
					if(data[i].isRequired==true)
					{
						document.getElementById("req_"+j+"_"+data[i].dspqFieldMaster.dspqFieldId).value = 1;						
						$('#reqOp_'+j+'_'+fieldRequired).removeClass("iconOptional");
						$('#reqOp_'+j+'_'+fieldRequired).addClass("iconRequired");	
						var IsFieldRequired = $("#isRequiredTest"+fieldRequired).val();
						if(IsFieldRequired !=1)
							$('#reqOp_'+j+'_'+fieldRequired).attr('title', 'Required');
						$('#reqOp_'+j+'_'+fieldRequired).text('R');
					}
					else						
					{
						$('#reqOp_'+j+'_'+fieldRequired).attr('title', 'Optional');
					}
					if(data[i].tooltip!=null)
					{
						document.getElementById("tooltip_"+j+"_"+data[i].dspqFieldMaster.dspqFieldId).value =data[i].tooltip;
						$("#fieldHelp_"+j+"_"+fieldIDS).attr('title', data[i].tooltip);
					}
					document.getElementById("status_"+j+"_"+data[i].dspqFieldMaster.dspqFieldId).value = data[i].status;
					var fieldid=data[i].dspqFieldMaster.dspqFieldId;
					if(data[i].status=="I")
					{			
						$('#'+j+'_'+fieldid).removeClass("activeIcon");
						$('#'+j+'_'+fieldid).addClass("inactiveIcon");
						$('#'+j+'_'+fieldid).attr('title', 'Inactive');
						$('#'+j+'_'+fieldid).text('X');
					}					
					
				}
			}			
			
		}		
		var cloneId = $('#cloneID').val();
		if(cloneId!="clone")
		{
			getDspqPortfolioName(dspqId);
			checkExistPortfolio="True"
		}
	}catch (e) {
		alert(e);
		// TODO: handle exception
	}
		},
		errorHandler:handleError 
});
	/*}
	else
	{
		$('#errordiv4DSPQ').empty();
		$('#errordiv4DSPQ').append("&#149; Please note that the "+arrayExists[0]+" Job Category already has active jobs and hence the portfolio can not be changed till all the active jobs are deactivated<BR>");		
		$('#errordiv4DSPQ').css("color","#F30303");
	}*/
}
function getDspqPortfolioName(dspqId){
	ManageDspqAjax.getDspqPortfolioName(dspqId,{
		async: false,
		callback: function(data)
		{		
			$('#portfolioName').val(data.portfolioName);		
		},
		errorHandler:handleError
});
}
function activateDeactivateDspq(districtId,dspqPortfolioNameId,status)
{
	$("#ADdistrictId").val(districtId);
	$("#ADdspqPortfolioNameId").val(dspqPortfolioNameId);
	$("#ADstatus").val(status);
	$('#loadingDiv').show();
	 setTimeout(function() {jobCategoryAttachedNotification(2,dspqPortfolioNameId,districtId,status);}, 10);
}
function activateDeactivateDspqAfterPrompt()
{
var dspqPortfolioNameId=$("#ADdspqPortfolioNameId").val();
var status=$("#ADstatus").val();
ManageDspqAjax.activateDeactivateDspq(dspqPortfolioNameId,status,{
	async: false,
	callback: function(data)
	{
		displayPortfolio();			
	},
	errorHandler:handleError
});
}
//////////////////Add Question////////////////////////
function openDivCustomField(){
	//document.getElementById("addDspqQuesListDiv").style.display="block";
	document.getElementById("addDspqQuesDiv").style.display="block";
	$("#errQuesdiv").empty();
	$( "span#i4QuesTxt" ).show();
}

function clearQues()
{
	try{
		$('#fieldName').val("");
		$('#fieldDisplayLabel').val("");
		$('#fieldHelperText').val("");
		$('#kendoEditorInstruction').val("");
		$('#kendoQuestionExplanation').val("");
		document.getElementById("addDspqQuesDiv").style.display="none";
		document.getElementById("fieldHelperText").value="";
		document.getElementById("questionTypeMaster").checked=false;
	}catch(e){}
}

function closeQuestionSection(){
	//document.getElementById("addDspqQuesListDiv").style.display="none";
	document.getElementById("addDspqQuesDiv").style.display="none";
}

function saveQuestionFromDspq(status) {

	var noOfOptionRows=document.getElementById("noOfOptionRows").value;
	var integer_noOfOptionRows;
	integer_noOfOptionRows = parseInt(noOfOptionRows);
	integer_noOfOptionRows=integer_noOfOptionRows*2;
	
	if(!validateDistrictQuestionsFromDspq())
		return;	
	var assQuestion 	= "";
	var assInstruction 	= "";
	var rowExplain 		= "";
	assQuestion		=	$("#fieldDisplayLabel").val();
	fieldDisplayLabel 	=$("#fieldName").val()	;
	assInstruction 	=	$("#kendoEditorInstruction").val();
	rowExplain 	=	$("#fieldHelperText").val();
	var selectedValue="";
	//var multiSelectVal=required.value();
	//var selectedValue=multiSelectVal.toString();	
	try {
		questionExplanation 	=	$("#kendoQuestionExplanation").val();
	} catch(e) {}
	var dspqGroupMaster={groupId:dwr.util.getValue("dspqGroupMaster")};
	var dspqSectionMaster={sectionId:dwr.util.getValue("dspqSectionMaster")};
	var questionType={questionTypeId:dwr.util.getValue("questionTypeMaster")};
	var qType=findSelected(questionType.questionTypeId);
	var isRequired={isRequired:dwr.util.getValue("isRequired")};
	var isRequiredVal = isRequired.isRequired;
	if(isRequiredVal == true){ isRequiredVal = 1;}else{isRequiredVal = 0;}
	var districtSpecificQuestions = {questionId:dwr.util.getValue("questionId"),dspqGroupMaster:dspqGroupMaster,dspqSectionMaster:dspqSectionMaster,question:assQuestion,fieldDisplayLabel:fieldDisplayLabel,questionExplanation:questionExplanation,tooltip:rowExplain,questionTypeMaster:questionType,questionInstructions:assInstruction};
	var districtMaster = {districtId:dwr.util.getValue("districtId")};
	var questionoptions={};
	var arr =[];
	if(qType=='tf' || qType=='et') {
		for(i=1;i<=2;i++) {
			var flag = $('#valid'+i).prop('checked');
			if(flag == true){ flag = 1;}else{flag = 0;}

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : flag
				});
			}
		}
	} else if(qType=='slsel'|| qType=='drsls'  || qType=='sswc') {
		for(i=1;i<=integer_noOfOptionRows;i++) {
			var flag = $('#valid'+i).prop('checked');
			if(flag == true){ flag = 1;}else{flag = 0;}
			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : flag
				});
			}
		}
		
	}  else if(qType=='mlsel') {

		for(i=1;i<=integer_noOfOptionRows;i++) {
			var flag = $('#valid'+i).prop('checked');
			if(flag == true){ flag = 1;}else{flag = 0;}

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : flag
				});
			}
		}
	} else if(qType=='mloet') {

		for(i=1;i<=integer_noOfOptionRows;i++) {
			var flag = $('#valid'+i).prop('checked');
			if(flag == true){ flag = 1;}else{flag = 0;}
			
			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : flag
				});
			}
		}
	} else if(qType=='sloet') {
		for(i=1;i<=integer_noOfOptionRows;i++) {
			var flag = $('#valid'+i).prop('checked');
			if(flag == true){ flag = 1;}else{flag = 0;}

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : flag
				});
			}
		}
	} 
	
	//dwr.util.getValues(districtSpecificQuestions);
	dwr.engine.beginBatch();
	//return false;
	var groupId = document.getElementById("groupId").value;
	var quesID=document.getElementById("questionId").value;
	var portfolioId  = $('#editDspqID').val();
	if(quesID==null || quesID=="")
	{
			ManageDspqAjax.saveDistrictQuestionFromDspq(districtSpecificQuestions,districtMaster,arr,isRequiredVal,selectedValue,portfolioId,{
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				var redirectURL = "";
				$('#dspqQuestionDiv').modal('hide');
				//callDiv(groupId);
				//displayAdditionalField();
				//$('#moreFields').show();
				//hideShowCustomAdditionalField(2);
			}
		});
			var sectionId={sectionId:dwr.util.getValue("dspqSectionMaster")};
			var secId=sectionId.sectionId;
			ManageDspqAjax.getLoadSectionDiv(secId,portfolioId,{
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					document.getElementById("New"+secId).innerHTML=data;
					hideOrShowApplicantForSection();
					$(".tempToolTip").tooltip();
				}
			});
	}
	else
	{
		var parentID = document.getElementById("parentQuestionId").value;
		var applicantType=document.getElementById("appTypes").value;
		ManageDspqAjax.saveEditDistrictQuestionFromDspq(districtSpecificQuestions,districtMaster,arr,isRequiredVal,selectedValue,applicantType,parentID,portfolioId,{
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				var redirectURL = "";
				$('#dspqQuestionDiv').modal('hide');
				//callDiv(groupId);
				//displayAdditionalField();
				//$('#moreFields').show();
				//hideShowCustomAdditionalField(2);
			}
		});	
		var sectionId={sectionId:dwr.util.getValue("dspqSectionMaster")};
		var secId=sectionId.sectionId;		
		ManageDspqAjax.getLoadSectionDiv(secId,portfolioId,{
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				document.getElementById("New"+secId).innerHTML=data;
				hideOrShowApplicantForSection();
				$(".tempToolTip").tooltip();
			}
		});
	}

	dwr.engine.endBatch();
}

function validateDistrictQuestionsFromDspq() {

	$('#errordiv').empty();
	$('#errordiv1').empty();
	$('#fieldName').css("background-color", "");
	$('#fieldDisplayLabel').css("background-color", "");
	$('#kendoEditorInstruction').css("background-color", "");
	$('#kendoQuestionExplanation').css("background-color", "");
	$('#questionTypeMaster').css("background-color", "");
	$('#dspqGroupMaster').css("background-color", "");
	$('#dspqSectionMaster').css("background-color", "");
	$('#opt1').css("background-color", "");
	var tooltip = document.getElementById("fieldHelperText").value;
	var cnt=0;
	var focs=0;
	var txtBgColor = "#F30303";
	
	if (trim(document.getElementById("dspqGroupMaster").value)==0){

		$('#errordiv').append("&#149; Please select Section<br>");
		if(focs==0)
			$('#dspqGroupMaster').focus();
		$('#dspqGroupMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if (trim(document.getElementById("dspqSectionMaster").value)==0){

		$('#errordiv').append("&#149; Please select sub section<br>");
		if(focs==0)
			$('#dspqSectionMaster').focus();
		$('#dspqSectionMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if($('#fieldName').val().trim()==""){
		$('#errordiv').append("&#149; Please enter Field Name<br>");
		if(focs==0)
			$('#fieldName').focus();
		$('#fieldName').css("background-color", "#F5E7E1");
		$('#errordiv').css("color",txtBgColor);
		cnt++;focs++;
	} else {
		var charCount=trim($('#fieldName').val());
		var count = charCount.length;
		if(count>500) {
			$('#errordiv').append("&#149; Field Name length cannot exceed 500 characters.<br>");
			if(focs==0)
				$('#fieldName').focus();
			$('#fieldName').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	
	if($('#fieldDisplayLabel').val().trim()==""){
		$('#errordiv').append("&#149; Please enter Field Display Label<br>");
		if(focs==0)
			$('#fieldDisplayLabel').focus();
		$('#fieldDisplayLabel').css("background-color", "#F5E7E1");
		$('#errordiv').css("color",txtBgColor);
		cnt++;focs++;
	} else {
		var charCount=trim($('#fieldDisplayLabel').val());
		var count = charCount.length;
		if(count>500) {
			$('#errordiv').append("&#149; Field Display Label length cannot exceed 500 characters.<br>");
			if(focs==0)
				$('#fieldDisplayLabel').focus();
			$('#fieldDisplayLabel').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	if (trim(document.getElementById("questionTypeMaster").value)==0){

		$('#errordiv').append("&#149; Please select Field Type<br>");
		if(focs==0)
			$('#questionTypeMaster').focus();
		$('#questionTypeMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	var requiredType = document.getElementById("isRequired").checked;
	var optionalType = document.getElementById("optional").checked;
	if (requiredType==false && optionalType==false){

		$('#errordiv').append("&#149; Please select Custom Field is Required/Optional<br>");
		if(focs==0)
			$('#isRequired').focus();
		$('#isRequired').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	var qType=findSelected(dwr.util.getValue("questionTypeMaster"));
	var c=0;
	var validCnt=0;
	
	if(qType == "et"){
		if($('#kendoQuestionExplanation').val().trim()==""){

			$('#errordiv').append("&#149; Please enter explanation<br>");
			if(focs==0)
				$('#kendoQuestionExplanation').focus();
			$('#kendoQuestionExplanation').css("background-color", "#F5E7E1");
			cnt++;focs++;
		} else {
			var charCount=trim($('#kendoQuestionExplanation').val());
			var count = charCount.length;
			if(count>5000) {
				$('#errordiv').append("&#149; Field explanation length cannot exceed 5000 characters.<br>");
				if(focs==0)
					$('#kendoQuestionExplanation').focus();
					$('#kendoQuestionExplanation').css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
	}
/* END Section */
	
	if(!(qType=='UAT') && !(qType=='UATEF') && !(qType=='ml') && !(qType=='SLD') && !(qType=='dt') && !(qType=='sl') && ! (trim(document.getElementById("questionTypeMaster").value)==0))
		if(document.getElementById("opt1")) {
			for(i=1;i<=6;i++) {
				if(trim(document.getElementById("opt"+i).value)=="")
					c++;
				if($('#valid'+i).prop('checked') && trim(document.getElementById("opt"+i).value)!="")
				validCnt++;
			}
			if(c==5 || c==6) {
				$('#errordiv').append("&#149; Please enter at least two Options<br>");
				if(focs==0)	{
					if(c==6)
						$('#opt1').focus();
					else
						$('#opt2').focus();
				}

				$('#opt1').css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}

	
	var arr =[];
	var charCount=trim($('#kendoEditorInstruction').val());
	var count = charCount.length;
	if(count>2500) {
		$('#errordiv').append("&#149; Field Instructions cannot exceed 2500 characters.<br>");
		if(focs==0)
			$('#kendoEditorInstruction').focus();
			$('#kendoEditorInstruction').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	if(cnt==0)
		return true;
	else {
		$('#errordiv').show();
		return false;
	}
}

function getQuestionOptions(questionTypeId) 
{
	var noOfOptionRows=document.getElementById("noOfOptionRows").value;
	var iTotalOpts=parseInt(noOfOptionRows);;
	
	showCustomeFieldColunns(0,iTotalOpts);
	
	$('#errordiv').empty();
	var qType=findSelected(questionTypeId);
	
	$('#rowExplain').hide();
	
	if(questionTypeId==0 || qType=='ml' || qType=='sl')
	{
		hideCustomeFieldColunns(0, iTotalOpts);
		return;
	} 
	else 
	{
		$('#row0').show();
		$('#addMoreOptionsDivId').show();
	}

	if(qType=='tf' || qType=='et') 
	{
		$('#addMoreOptionsDivId').hide();
		
		hideCustomeFieldColunns(2, iTotalOpts);
		if(qType=='et')
			$('#rowExplain').show();
		
		if($('#opt1').val()=="")
			$('#opt1').attr("value","True");
		if($('#opt2').val()=="")
			$('#opt2').attr("value","False");

	}
	
	if(qType=='SLD' || qType=='dt' || qType=='UAT' || qType=='UATEF') 
		hideCustomeFieldColunns(0, iTotalOpts);
	
}

function displayQuesList()
{
	var districtId		=	document.getElementById("districtId").value;	
	ManageDspqAjax.displayQuesList(districtId,{ 
		async: true,
		callback: function(data){
		document.getElementById("dspqQuestionList").innerHTML=data;
		applyScrollOnDspqTb();
	  },
	});
}

function getDistrictQuestions() {
	var districtId = document.getElementById("districtId").value;
	ReferenceChkQuestionsAjax.getDistrictQuestions(districtId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#tblGridQIns').show();
		$('#tblGrid').html(data);
	},
	errorHandler:handleError  
	});
}

function hideOrShowApplicant(){
	
	var sectionInternal = document.getElementById("sectionInternal");	
	var sectionITransfer = document.getElementById("sectionITransfer");
	
	var manageWidth=0;
	if(sectionInternal.checked){
		$('.sectionLabelInternal').removeClass("hide");
		$('.sectionLabelInternal').addClass("show");
		manageWidth=1			
	}
	else{
		$('.sectionLabelInternal').removeClass("show");
		$('.sectionLabelInternal').addClass("hide");
		manageWidth=0
		//$(".internal").removeClass("show");
		//$(".internal").addClass("hide");	
		
	}
	if(sectionITransfer.checked){
		$('.sectionLabelInternalTransfer').removeClass("hide");
		$('.sectionLabelInternalTransfer').addClass("show");
		if(manageWidth==1)
			manageWidth=2
		else
			manageWidth=1
		//$(".transfer").removeClass("hide");
		//$(".transfer").addClass("show");
	}
	else{
		$('.sectionLabelInternalTransfer').removeClass("show");
		$('.sectionLabelInternalTransfer').addClass("hide");
		if(manageWidth==1)
			manageWidth=1
		else
			manageWidth=0
		//$(".transfer").removeClass("show");
		//$(".transfer").addClass("hide");
	}
	if(manageWidth==2)
	{
		$(".manageWidth").removeClass("col-sm-7 col-md-7");
		$(".manageWidth").addClass("col-sm-5 col-md-5");
		$(".managePadding").removeClass("editLeftPadding1");
		$(".managePadding").addClass("editLeftPadding2");
		$(".manageExternal").removeClass("externalLeftPadding2");
		$(".manageExternal").addClass("externalLeftPadding3");
	}
	else if(manageWidth==1)
	{
		$(".manageWidth").removeClass("col-sm-9 col-md-9");
		$(".manageWidth").addClass("col-sm-7 col-md-7");
		$(".managePadding").removeClass("editLeftPadding2");
		$(".managePadding").addClass("editLeftPadding1");
		$(".manageExternal").removeClass("externalLeftPadding1");
		$(".manageExternal").removeClass("externalLeftPadding3");
		$(".manageExternal").addClass("externalLeftPadding2");
	}
	else
	{
		$(".manageWidth").removeClass("col-sm-5 col-md-5");
		$(".manageWidth").addClass("col-sm-9 col-md-9");
		$(".managePadding").removeClass("editLeftPadding1");
		$(".manageExternal").removeClass("externalLeftPadding2");
		$(".manageExternal").addClass("externalLeftPadding1");
	}
	
		//alert(sectionInternal.checked);
	return true;
	
}
function hideOrShowApplicantForSection(){
	
	var sectionInternal = document.getElementById("sectionInternal");	
	var sectionITransfer = document.getElementById("sectionITransfer");
	
	var manageWidth=0;
	if(sectionInternal.checked){
		$('.sectionLabelInternal').removeClass("hide");
		$('.sectionLabelInternal').addClass("show");
		manageWidth=1			
	}
	else{
		$('.sectionLabelInternal').removeClass("show");
		$('.sectionLabelInternal').addClass("hide");
		manageWidth=0
		//$(".internal").removeClass("show");
		//$(".internal").addClass("hide");	
		
	}
	if(sectionITransfer.checked){
		$('.sectionLabelInternalTransfer').removeClass("hide");
		$('.sectionLabelInternalTransfer').addClass("show");
		if(manageWidth==1)
			manageWidth=2
		else
			manageWidth=1
		//$(".transfer").removeClass("hide");
		//$(".transfer").addClass("show");
	}
	else{
		$('.sectionLabelInternalTransfer').removeClass("show");
		$('.sectionLabelInternalTransfer').addClass("hide");
		if(manageWidth==1)
			manageWidth=1
		else
			manageWidth=0
		//$(".transfer").removeClass("show");
		//$(".transfer").addClass("hide");
	}
	if(manageWidth==2)
	{
		$(".manageWidth").removeClass("col-sm-9 col-md-9");
		$(".manageWidth").addClass("col-sm-5 col-md-5");		
	}
	else
	{
		hideOrShowApplicant();
	}
		//alert(sectionInternal.checked);
	
	
}

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

var array1=[];var array2=[];var array4=[];var array5=[];var array6=[];var array7=[];
var array15=[];var array16=[];var array17=[];var array18=[];var array19=[];var array20=[];
var array21=[];var array22=[];var array23=[];var array24=[];var array25=[];
function hideShowAdditionalFields(additionalFieldId,secID,array){

	var i=0;
	var arrayDependant=[];
	var sectionExternal = document.getElementById("sectionExternal");
	var sectionInternal = document.getElementById("sectionInternal");	
	var sectionITransfer = document.getElementById("sectionITransfer");	
	$("#TMAdditionalSec"+secID).show();
	var additionalField = document.getElementById("additionalFieldID"+additionalFieldId);
	var panel = document.getElementById("panel"+secID);	
	var dependant = document.getElementById("dependantField_"+additionalFieldId).value;
	if(dependant!="null" && dependant!="")
	{
		arrayDependant=dependant.split(",");
		i=arrayDependant.length;
		i=i-1;
	}		
	for (var j=0; j<=i;j++)
	{
		if(dependant!="null" && dependant!="")
		{
			if(additionalField.checked)
			{
				$("#additionalFieldID"+arrayDependant[j]).prop('checked', true);
				additionalField = document.getElementById("additionalFieldID"+additionalFieldId);
				additionalFieldId=arrayDependant[j];
			}
			else
			{
				$("#additionalFieldID"+arrayDependant[j]).prop('checked', false);
				additionalField = document.getElementById("additionalFieldID"+additionalFieldId);
				additionalFieldId=arrayDependant[j];
			}
		}
	if(secID >=16)
	{
		if(additionalField.checked)
		{
			if(array==array16)
				array16.push(additionalFieldId);
			if(array==array17)
				array17.push(additionalFieldId);
			if(array==array18)
				array18.push(additionalFieldId);
			if(array==array19)
				array19.push(additionalFieldId);
			if(array==array20)
				array20.push(additionalFieldId);
			if(array==array21)
				array21.push(additionalFieldId);
			if(array==array22)
				array22.push(additionalFieldId);
			if(array==array23)
				array23.push(additionalFieldId);
			if(array==array24)
				array24.push(additionalFieldId);
			if(array==array25)
				array25.push(additionalFieldId);
		}
		else
		{
			if(array==array16)
				removeA(array16,additionalFieldId);
			if(array==array17)
				removeA(array17,additionalFieldId);
			if(array==array18)
				removeA(array18,additionalFieldId);
			if(array==array19)
				removeA(array19,additionalFieldId);
			if(array==array20)
				removeA(array20,additionalFieldId);
			if(array==array21)
				removeA(array21,additionalFieldId);
			if(array==array22)
				removeA(array22,additionalFieldId);
			if(array==array23)
				removeA(array23,additionalFieldId);
			if(array==array24)
				removeA(array24,additionalFieldId);
			if(array==array25)
				removeA(array25,additionalFieldId);
				
		}
				
		/*if(array.length!=0)
		{
			$("#panel"+secID).removeClass("hide");
			$("#panel"+secID).addClass("show");
		}
		else			
		{
			$("#panel"+secID).removeClass("show");
			$("#panel"+secID).addClass("hide");
		}	*/	
	}
	
	if(sectionExternal.checked)	{
		
		if(additionalField.checked)	{
			$(".externalAdditional"+additionalFieldId).removeClass("hide");
			$(".externalAdditional"+additionalFieldId).addClass("show");		
		}
		else{
			
			$(".externalAdditional"+additionalFieldId).removeClass("show");
			$(".externalAdditional"+additionalFieldId).addClass("hide");
		}
	}
	if(sectionInternal.checked)	{
			
			if(additionalField.checked)	{		
				$(".internalAdditional"+additionalFieldId).removeClass("hide");
				$(".internalAdditional"+additionalFieldId).addClass("show");		
			}
			else{
				
				$(".internalAdditional"+additionalFieldId).removeClass("show");
				$(".internalAdditional"+additionalFieldId).addClass("hide");
			}
		}
	if(sectionITransfer.checked){
		
		if(additionalField.checked)	{		
			$(".transferAdditional"+additionalFieldId).removeClass("hide");
			$(".transferAdditional"+additionalFieldId).addClass("show");		
		}
		else{
			
			$(".transferAdditional"+additionalFieldId).removeClass("show");
			$(".transferAdditional"+additionalFieldId).addClass("hide");
		}
	}
  }
}
function activeInactiveAdditionalFields(additionalFieldId,secID,array){
	
	var sectionExternal = document.getElementById("sectionExternal");
	var sectionInternal = document.getElementById("sectionInternal");	
	var sectionITransfer = document.getElementById("sectionITransfer");	
	$("#TMAdditionalSec"+secID).show();
	var additionalField = document.getElementById("additionalFieldAI"+additionalFieldId);
	var panel = document.getElementById("panel"+secID);
	
	if(secID >=16)
	{
		if(additionalField.checked)	
			array.push(additionalFieldId);
		else
			removeA(array,additionalFieldId);	
				
		if(array.length!=0)
		{
			$("#panel"+secID).removeClass("hide");
			$("#panel"+secID).addClass("show");
		}
		else			
		{
			$("#panel"+secID).removeClass("show");
			$("#panel"+secID).addClass("hide");
		}		
	}
	if(sectionExternal.checked)	{
		
		if(additionalField.checked)	{
			$(".externalAdditional"+additionalFieldId).removeClass("hide");
			$(".externalAdditional"+additionalFieldId).addClass("show");		
		}
		else{
			
			$(".externalAdditional"+additionalFieldId).removeClass("show");
			$(".externalAdditional"+additionalFieldId).addClass("hide");
		}
	}
	if(sectionInternal.checked)	{
			
			if(additionalField.checked)	{		
				$(".internalAdditional"+additionalFieldId).removeClass("hide");
				$(".internalAdditional"+additionalFieldId).addClass("show");		
			}
			else{
				
				$(".internalAdditional"+additionalFieldId).removeClass("show");
				$(".internalAdditional"+additionalFieldId).addClass("hide");
			}
		}
	if(sectionITransfer.checked){
		
		if(additionalField.checked)	{		
			$(".transferAdditional"+additionalFieldId).removeClass("hide");
			$(".transferAdditional"+additionalFieldId).addClass("show");		
		}
		else{
			
			$(".transferAdditional"+additionalFieldId).removeClass("show");
			$(".transferAdditional"+additionalFieldId).addClass("hide");
		}
	}
	$('#additionalFieldID'+additionalFieldId).attr('checked', false); 
	//'#additionalFieldID'+additionalFieldId).hide();	
}
function hideShowCustomAdditionalField(id)
{
	
	if(id==1)
	{
		$('#additionalField').show();
		$('#customFields').hide();
	}
	else
	{
		$('#additionalField').hide();
		$('#customFields').show();
	}
}
function hideShowAdditionalFields1(additionalFieldId,secID){
	var sectionExternal = document.getElementById("sectionExternal");
	var sectionInternal = document.getElementById("sectionInternal");	
	var sectionITransfer = document.getElementById("sectionITransfer");	
	$("#TMAdditionalSec"+secID).show();
	var additionalField = document.getElementById("additionalFieldID"+additionalFieldId);
	if(sectionExternal.checked)	{
		
		if(additionalField.checked)	{		
			$(".externalAdditional"+additionalFieldId).removeClass("hide");
			$(".externalAdditional"+additionalFieldId).addClass("show");		
		}
		else{
			
			$(".externalAdditional"+additionalFieldId).removeClass("show");
			$(".externalAdditional"+additionalFieldId).addClass("hide");
		}
	}
	if(sectionInternal.checked)	{
		
			if(additionalField.checked)	{		
				$(".internalAdditional"+additionalFieldId).removeClass("hide");
				$(".internalAdditional"+additionalFieldId).addClass("show");		
			}
			else{
				
				$(".internalAdditional"+additionalFieldId).removeClass("show");
				$(".internalAdditional"+additionalFieldId).addClass("hide");
			}
		}
	if(sectionITransfer.checked){
		
		if(additionalField.checked)	{		
			$(".transferAdditional"+additionalFieldId).removeClass("hide");
			$(".transferAdditional"+additionalFieldId).addClass("show");		
		}
		else{
			
			$(".transferAdditional"+additionalFieldId).removeClass("show");
			$(".transferAdditional"+additionalFieldId).addClass("hide");
		}
	}
}

function activateDeactivateQuestion(questionId,status,secId,action)
{
	var portfolioId  = $('#editDspqID').val();
	var groupId = document.getElementById("groupId").value;
	ManageDspqAjax.activateDeactivateQuestion(questionId,status,action,{
		async: false,
		callback: function(data)
		{
			
		},
		errorHandler:handleError
	});
	ManageDspqAjax.getLoadSectionDiv(secId,portfolioId,{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			document.getElementById("New"+secId).innerHTML=data;
			hideOrShowApplicantForSection();
			$(".tempToolTip").tooltip();
		}
	});
}
function hideDspqQuestion()
{	
	$('#dspqGridCancel').show();
	//$('#dspqGridCancel1').show();
	$('#dspqQuestionCancel').hide();
	$('#dspqCustomQuestion').hide();
	$('#saveQues').hide();	
	$('#customGrid').show();
}
function editCustomField(questionId)
{	
	$('#errordiv').empty();
	var fieldName=document.getElementById("customField"+questionId).value;
	document.getElementById("myModalLabelDspq").innerHTML=fieldName;
	$('#dspqCustomQuestion').hide();
	$('#dspqGridCancel').show();
	//$('#dspqGridCancel1').show();
	$('#dspqQuestionCancel').hide();
	$('#saveQues').hide();
	$('#loadingDiv').show();
	$('#customGrid').show();
	ManageDspqAjax.getCustomFieldGrid(questionId,{
		async: false,
		callback: function(data)
		{
		$('#dspqQuestionDiv').modal('show');
			document.getElementById("dspqCustomGrid").innerHTML=data;
		},
		errorHandler:handleError
	});
	$('#loadingDiv').hide();
}
function editDspqQuestion(questionId){
	$('#saveQues').show();
	$('#fieldName').prop('disabled', true);
	$('#dspqGroupMaster').prop('disabled', true);
	$('#dspqSectionMaster').prop('disabled', true);
	$('#dspqGridCancel').hide();
	//$('#dspqGridCancel1').hide();
	$('#dspqQuestionCancel').show();
	$('#customGrid').hide();
	document.getElementById("questionId").value = questionId;
	ManageDspqAjax.getDspqQuestionById(questionId,{
		async: false,
		callback: function(data)
		{
			//$('#dspqQuestionDiv').modal('show');
			document.getElementById("fieldName").value = data.fieldDisplayLabel;
			document.getElementById("fieldDisplayLabel").value = data.question;
			document.getElementById("fieldHelperText").value = data.tooltip;
			document.getElementById("parentQuestionId").value = data.parentQuestionId;
			document.getElementById("appTypes").value= data.applicantType;
			document.getElementById("kendoEditorInstruction").value= data.questionInstructions;
			document.getElementById("kendoQuestionExplanation").value= data.questionExplanation;
			/*var instruction = $("#kendoEditorInstruction").data("kendoEditor");
			instruction.value(data.questionInstructions);*/
			
			/*var explanation = $("#kendoQuestionExplanation").data("kendoEditor");
			explanation.value(data.questionExplanation);*/
			
			if(data.isRequired=="1"){
				document.getElementById("isRequired").checked = true;
			} else {
				document.getElementById("optional").checked = true;
			}
			
			getDspqGroup();
			getDspqSection();
		},
		errorHandler:handleError
	});
	
	ManageDspqAjax.getDspqOptionById(questionId,{
		async: false,
		callback: function(data)
		{
		
		var optionCount=0;
		jQuery.each(data, function (i, jsonSelectedOptions) 
		{
			optionCount++;
		});
		var remainderOpt=optionCount%2;
		if(remainderOpt==1)
			optionCount=optionCount+1;
		
		optionCount=optionCount/2;
		
		if(optionCount>3)
			document.getElementById("noOfOptionRows").value=optionCount;
		else
			document.getElementById("noOfOptionRows").value=3;
		generateCustomFieldOtionsOneTime();
		
		getDspqQuestionType();
		var questionTypeId = document.getElementById("questionTypeMaster").value;
		dwr.util.setValue("questionTypeMaster",questionTypeId);
		getQuestionOptions(questionTypeId);
		var cnnt=1;
		jQuery.each(data, function (i, jsonSelectedOptions) {	
		$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
		$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
		//$('#valid'+cnnt).attr('checked',jsonSelectedOptions.validOption);
		cnnt++;
		});
			
		},
		errorHandler:handleError
	});
	$('#dspqCustomQuestion').show();
}

function getDspqQuestionType() {
	try{
		var questionId 	= 	trim(document.getElementById("questionId").value);
	}catch(e){}
	if(questionId!="")
		ManageDspqAjax.getDspqQuestionTypeList(questionId,{ 
			async: false,
			callback: function(data)
			{
				if(data!=null){
					document.getElementById("questionTypeMaster").innerHTML=data;
				}
			},errorHandler:handleError
		});
}

function displayAdditionalField(){
	$('#moreFields').show();
	
}

function openDspqQuestionDiv(){
	try{
		$('#errordiv').empty();
		document.getElementById("myModalLabelDspq").innerHTML="Custom Fields";
		
		document.getElementById("fieldName").value					=	"";
		document.getElementById("fieldDisplayLabel").value			=	"";
		document.getElementById("fieldHelperText").value			=	"";
		document.getElementById("kendoEditorInstruction").value		=	"";
		document.getElementById("kendoQuestionExplanation").value	=	"";
		document.getElementById("questionTypeMaster").checked		=	false;
		document.getElementById("isRequired").checked		=	false;
		document.getElementById("optional").checked		=	false;
		document.getElementById("questionId").value					=	"";
		$('#fieldName').prop('disabled', false);
		$('#dspqGroupMaster').prop('disabled', false);
		$('#dspqSectionMaster').prop('disabled', false);
		
		$("#kendoEditorInstruction").val("");
		$("#kendoQuestionExplanation").val("");
		/*var instruction = $("#kendoEditorInstruction").data("kendoEditor");
		instruction.value("");
		
		var explanation = $("#kendoQuestionExplanation").data("kendoEditor");
		explanation.value("");*/
		elements = document.getElementsByTagName("select");
		for(i=0; i < elements.length ; i++){
		 elements[i].selectedIndex= 0;
		}
		var questionTypeId = document.getElementById("questionTypeMaster").value;
		
		var noOfOptionRows=document.getElementById("noOfOptionRows").value;
		var integer_noOfOptionRows;
		integer_noOfOptionRows = parseInt(noOfOptionRows);
		document.getElementById("noOfOptionRows").value=integer_noOfOptionRows;
		generateCustomFieldOtionsOneTime();
		for(i=0;i<=integer_noOfOptionRows;i++)
			$('#row'+i).hide();
		$('#rowExplain').hide();
		
	}catch(e){}
	$('#dspqGridCancel').show();
	//$('#dspqGridCancel1').hide();
	$('#dspqQuestionCancel').hide();
	$('#saveQues').show();
	$('#dspqQuestionDiv').modal('show');
	$('#customGrid').hide();
	$('#dspqCustomQuestion').show();
	$("#dspqGroupMaster").val($('[name="groupId"]').val());
	var groupid=$("#groupId").val();
	getSubSectionByGroupId($("#groupId").val());
}

function getSubSectionByGroupId(groupId) {
	
	if(groupId!="")
		ManageDspqAjax.getSectionListByGroupId(groupId,{ 
			async: false,
			callback: function(data)
			{
				if(data!=null){
					document.getElementById("dspqSectionMaster").innerHTML=data;
				}
			},errorHandler:handleError
		});
	$("#dspqSectionMaster").val($('[name="sectionIdOnMoreField"]').val());
}

function getDspqGroup() {
	try{
		var questionId 	= 	trim(document.getElementById("questionId").value);
	}catch(e){}
	if(questionId!="")
		ManageDspqAjax.getDspqGroup(questionId,{
			async: false,
			callback: function(data)
			{
				if(data!=null){
					document.getElementById("dspqGroupMaster").innerHTML=data;
				}
			},errorHandler:handleError
		});
}

function getDspqSection() {
	try{
		var questionId 	= 	trim(document.getElementById("questionId").value);
	}catch(e){}

	try{
		var groupId 	= 	trim(document.getElementById("dspqGroupMaster").value);
	}catch(e){}
	if(questionId!="")
		ManageDspqAjax.getDspqSection(questionId,groupId,{
			async: false,
			callback: function(data)
			{
				if(data!=null){
					document.getElementById("dspqSectionMaster").innerHTML=data;
				}
			},errorHandler:handleError
		});
}


function showFieldDiv(fieldID)
{
	$("#errordivfield").empty();
	//$("#sectionInternal1").prop('checked', false);
	//$("#sectionITransfer1").prop('checked', false);
	$('#internalType').hide();
	$('#internalTransferType').hide();
	if($('#sectionInternal').prop("checked") == true)
	{
		$('#internalType').show();		
	}
	if($('#sectionITransfer').prop("checked") == true)
	{
		$('#internalTransferType').show();		
	}	
	$("#fieldid").val(fieldID);
	var IsFieldRequired = $("#isRequiredTest"+fieldID).val();
	$("#fname").html($("#fieldIDD"+fieldID).val());
		
		for(var j=1; j<=3; j++)
		{
			//$('#mand'+j).removeClass("gray");
			var fieldName = "",req = "",tooltip="",status="",applicantType="";
			if(j==1)
				applicantType="E";
			if(j==2)
				applicantType="I";
			if(j==3)
				applicantType="IT";
				
			
			fieldName=$("#field_"+j+"_"+fieldID).val();
			req=$("#req_"+j+"_"+fieldID).val();
			tooltip=$("#tooltip_"+j+"_"+fieldID).val();
			status=$("#status_"+j+"_"+fieldID).val();
			$("#fieldname"+applicantType).val(fieldName);
			if(req==1)
			{
							
				/*$('#mand'+j).removeClass("blue");
				$('#mand'+j).addClass("green");			
				$('#mand'+j).attr('title', 'Mandatory');*/
				$("#mandetory"+applicantType).prop('checked', true);
				if(IsFieldRequired==1)
				{					
					//$('#mand'+j).attr('onclick', '');
					$("#mandetory"+applicantType).prop('disabled', true);
				}
				else
				{
					$("#mandetory"+applicantType).prop('disabled', false);
				}
			}
			else
			{
				/*$('#mand'+j).removeClass("green");
				$('#mand'+j).addClass("blue");			
				$('#mand'+j).attr('title', 'Optional');*/
				$("#mandetory"+applicantType).prop('checked', false);
				$("#mandetory"+applicantType).prop('disabled', false);
			}
			if(IsFieldRequired==1)
			{					
				/*$('#mand'+j).removeClass("green");
				$('#mand'+j).removeClass("blue");
				$('#mand'+j).addClass("gray");*/
				$("#active"+applicantType).prop('checked', true);
				$("#active"+applicantType).prop('disabled', true);
			}
			else
			{
				$("#active"+applicantType).prop('disabled', false);
				if(status=="A")
					
					$("#active"+applicantType).prop('checked', true);						
				else
				{
					$("#active"+applicantType).prop('checked', false);
					$("#active"+applicantType).prop('disabled', false);
				}
			}
			$("#tooltip"+applicantType).val(tooltip);			
		}		
		
	$("#myModalMsg1").show();
}
function showSectionDiv(sectionId)
{	
	$("#errordivSection").empty();
	$("#sectionInternal11").prop('checked', false);
	$("#sectionITransfer11").prop('checked', false);
	$('#internalTypeSection').hide();
	$('#internalTransferTypeSection').hide();	
	$("#sectionfieldid").val(sectionId);
	
	$("#sectionNameHeading").html($("#sectionfield1_"+sectionId).val());
	
		for(var j=1; j<=3; j++)
		{
			var fieldName = "",req = "",tooltip="",status="",applicantType="",instructions;
			if(j==1)
				applicantType="E";
			if(j==2)
				applicantType="I";
			if(j==3)
				applicantType="IT";
			$("#instruction_"+applicantType).val('');
			instructions=$("#instruction_"+j+"_"+sectionId).val();
			fieldName=$("#sectionfield_"+j+"_"+sectionId).val();
			req=$("#sectionreq_"+j+"_"+sectionId).val();
			tooltip=$("#sectiontooltip_"+j+"_"+sectionId).val();			
			$("#sectionName"+applicantType).val(fieldName);
			$("#instruction_"+applicantType).val(instructions);
			if(req==1)
			{	
				/*$('#sectionMand'+j).removeClass("blue");
				$('#sectionMand'+j).addClass("green");			
				$('#sectionMand'+j).attr('title', 'Mandatory');*/
				$("#sectionMandetory"+applicantType).prop('checked', true);
			}
			else
			{
				/*$('#sectionMand'+j).removeClass("green");
				$('#sectionMand'+j).addClass("blue");			
				$('#sectionMand'+j).attr('title', 'Optional');*/
				$("#sectionMandetory"+applicantType).prop('checked', false);
			}
			/*if(status=="A")
				$("#sectionActive"+applicantType).prop('checked', true);						
			else
				$("#sectionActive"+applicantType).prop('checked', false);*/
			
			$("#sectionTooltip"+applicantType).val(tooltip);			
		}
		
		var sectionInternal = document.getElementById("sectionInternal");	
		var sectionITransfer = document.getElementById("sectionITransfer");
		
		var sectionExternal1 = document.getElementById("sectionstatus_1_"+sectionId).value;
		var sectionInternal1 = document.getElementById("sectionstatus_2_"+sectionId).value;	
		var sectionITransfer1 = document.getElementById("sectionstatus_3_"+sectionId).value;
			if(sectionExternal1=="I")
				$('#externalTypeSection').hide();
			else
				$('#externalTypeSection').show();	
		
		if(sectionInternal.checked){
			if(sectionInternal1=="I")
			{
				$('#internalTypeSection').hide();
				$('#internalTypeSectionIns').hide();
			}
			else
			{
				$('#internalTypeSection').show();
				$('#internalTypeSectionIns').show();
			}
		}
		else
		{
			$('#internalTypeSection').hide();
			$('#internalTypeSectionIns').hide();
		}
		if(sectionITransfer.checked){
			if(sectionITransfer1=="I")
			{
				$('#internalTransferTypeSection').hide();
				$('#internalTransferTypeSectionIns').hide();
			}
			else
			{
				$('#internalTransferTypeSection').show();
				$('#internalTransferTypeSectionIns').show();
			}
		}
		else
		{
			$('#internalTransferTypeSection').hide();
			$('#internalTransferTypeSectionIns').hide();
		}
	$("#sectionModalMsg1").show();
}
function hideSectionPopup()
{
	$("#sectionModalMsg1").hide();	
}
function saveSectionRecord()
{
	$("#errordivSection").empty();
	var cnt=0,focs=0;
	var eDisplayName = $("#sectionNameE").val().trim();
	var iDisplayName = $("#sectionNameI").val().trim();
	var tDisplayName = $("#sectionNameIT").val().trim();
	if(eDisplayName=="")
	{
		$('#errordivSection').append("&#149; Please provide External Subsection name<BR>");
		if(focs==0)
			document.getElementById("sectionNameE").focus();		
		
		cnt++;focs++;
		
	}
	if(iDisplayName=="")
	{
		$('#errordivSection').append("&#149; Please provide Internal Subsection name<BR>");
		if(focs==0)
			document.getElementById("sectionNameI").focus();		
		
		cnt++;focs++;
		
	}
	if(tDisplayName=="")
	{
		$('#errordivSection').append("&#149; Please provide Internal Transfer Subsection name<BR>");
		if(focs==0)
			document.getElementById("sectionNameIT").focus();		
		
		cnt++;focs++;
		
	}
	
	if(cnt==0)	
	{
		var sectionId=$("#sectionfieldid").val();
		var applicantType="";
		for(var i=1; i<=3; i++)
		{
			if(i==1)
				applicantType="E" 
			if(i==2)
				applicantType="I"
			if(i==3)
				applicantType="IT"			
					
			$("#sectionapplicantType_"+i+"_"+sectionId).val(applicantType);
			$("#sectionfield_"+i+"_"+sectionId).val($("#sectionName"+applicantType).val());
			$("#instruction_"+i+"_"+sectionId).val($("#instruction_"+applicantType).val());
			/*var mand = document.getElementById("sectionMandetory"+applicantType);
			
			if(mand.checked==true)			
				$("#sectionreq_"+i+"_"+sectionId).val(1);
			else
				$("#sectionreq_"+i+"_"+sectionId).val(0);*/	
			
			var tooltips=$("#sectionTooltip"+applicantType).val();
			var secLabel=$("#sectionName"+applicantType).val();
			var secInstructions=$("#instruction_"+applicantType).val();
			
			$("#secHelp_"+i+"_"+sectionId).attr('title', tooltips);
			$("#secLabel_"+i+"_"+sectionId).attr('title', secLabel);
			$("#secInstruction_"+i+"_"+sectionId).attr('title', secInstructions);
			
			$("#sectiontooltip_"+i+"_"+sectionId).val($("#sectionTooltip"+applicantType).val());
			
			var status =document.getElementById("sectionActive"+applicantType);
			
			
			
		}
		var field1=$("#sectionNameE").val();
		var	field2=$("#sectionNameI").val();
		var	field3=$("#sectionNameIT").val();
		var applicant1 = document.getElementById("sectionExternal");
		var applicant2 = document.getElementById("sectionInternal");
		var applicant3 = document.getElementById("sectionITransfer");
		$("#sectionLabel_"+sectionId).empty();
		if(field1==field2 && field2==field3 && field1==field3)
		{
			$("#sectionLabel_"+sectionId).html(field1);
		}
		else
		{
			if(applicant1.checked==true)
				$("#sectionLabel_"+sectionId).append(field1);
			if(applicant2.checked==true)
				$("#sectionLabel_"+sectionId).append("/"+field2);
			if(applicant3.checked==true)
				$("#sectionLabel_"+sectionId).append("/"+field3);
		}
		hideSectionPopup();
	}
	
}
function hidepopup()
{
	$("#myModalMsg1").hide();	
}

function hideOrShowApplicantType()
{

	$('#internalType').hide();
	$('#internalTransferType').hide();
	if($('#sectionInternal1').prop("checked") == true)
	{
		$('#internalType').show();		
	}
	if($('#sectionITransfer1').prop("checked") == true)
	{
		$('#internalTransferType').show();		
	}
}
function hideOrShowSectionApplicantType()
{

	$('#internalTypeSection').hide();
	$('#internalTransferTypeSection').hide();
	if($('#sectionInternal11').prop("checked") == true)
	{
		$('#internalTypeSection').show();		
	}
	if($('#sectionITransfer11').prop("checked") == true)
	{
		$('#internalTransferTypeSection').show();		
	}
}

function activateDeactiveValue(fieldid)
{ 
	var i=$('#AD'+fieldid).val();
	if(i=="A")
	{
		$('#AD'+fieldid).val('I');
		$('#A'+fieldid).removeClass("fa fa-check-circle fa-lg");
		$('#A'+fieldid).addClass("fa fa-times-circle fa-lg");
		$('#A'+fieldid).attr('title', 'Inactive')
		$('#A'+fieldid).css('color', 'red')		
		
		
		$('#I'+fieldid).removeClass("fa fa-times-circle fa-lg");
		$('#I'+fieldid).addClass("fa fa-check-circle fa-lg");
		$('#I'+fieldid).attr('title', 'Active');
		$('#I'+fieldid).css('color', 'green');
		
		
	}
	else 
	{
		$('#AD'+fieldid).val('A');
		$('#A'+fieldid).removeClass("fa fa-times-circle fa-lg");
		$('#A'+fieldid).addClass("fa fa-check-circle fa-lg");
		$('#A'+fieldid).attr('title', 'Active');
		$('#A'+fieldid).css('color', 'green');
		
		$('#I'+fieldid).removeClass("fa fa-check-circle fa-lg");
		$('#I'+fieldid).addClass("fa fa-times-circle fa-lg");
		$('#I'+fieldid).attr('title', 'Inactive');
		$('#I'+fieldid).css('color', 'red');
	}
}
function actDeacApplicant(fieldid,applicantType)
{ 
	
	var i=$('#status_'+applicantType+'_'+fieldid).val();
	if(i=="A")
	{
		$('#status_'+applicantType+'_'+fieldid).val('I');
		$('#'+applicantType+'_'+fieldid).removeClass("activeIcon");
		$('#'+applicantType+'_'+fieldid).addClass("inactiveIcon");
		$('#'+applicantType+'_'+fieldid).attr('data-original-title', 'Inactive')
		$('#'+applicantType+'_'+fieldid).text('X');
	}
	else 
	{
		$('#status_'+applicantType+'_'+fieldid).val('A');
		$('#'+applicantType+'_'+fieldid).removeClass("inactiveIcon");
		$('#'+applicantType+'_'+fieldid).addClass("activeIcon");
		$('#'+applicantType+'_'+fieldid).attr('data-original-title', 'Active')
		$('#'+applicantType+'_'+fieldid).text('A');
	}
}
function requireOptionalField(fieldid,applicantType)
{ 
	
	var i=$('#req_'+applicantType+'_'+fieldid).val();	
	if(i==1)
	{
		$('#req_'+applicantType+'_'+fieldid).val(0);
		$('#reqOp_'+applicantType+'_'+fieldid).removeClass("iconRequired");
		$('#reqOp_'+applicantType+'_'+fieldid).addClass("iconOptional");
		$('#reqOp_'+applicantType+'_'+fieldid).attr('data-original-title', 'Optional')
		$('#reqOp_'+applicantType+'_'+fieldid).text('O');
	}
	else 
	{
		$('#req_'+applicantType+'_'+fieldid).val(1);
		$('#reqOp_'+applicantType+'_'+fieldid).removeClass("iconOptional");
		$('#reqOp_'+applicantType+'_'+fieldid).addClass("iconRequired");
		$('#reqOp_'+applicantType+'_'+fieldid).attr('data-original-title', 'Required')
		$('#reqOp_'+applicantType+'_'+fieldid).text('R');
	}
}

function chengeMandOptions(i)
{
	var fieldid=$("#fieldid").val();
	var reqOpt=$("#req_"+i+"_"+fieldid).val();
	var IsFieldRequired = $("#isRequiredTest"+fieldid).val();
	if(IsFieldRequired != 1)
	{
		if(reqOpt==1)
		{
			$("#req_"+i+"_"+fieldid).val(0);				
			/*$('#mand'+i).removeClass("green");
			$('#mand'+i).addClass("blue");			
			$('#mand'+i).attr('title', 'Optional');*/
			
			$('#reqOp_'+i+'_'+fieldid).removeClass("iconRequired");
			$('#reqOp_'+i+'_'+fieldid).addClass("iconOptional");
			$('#reqOp_'+i+'_'+fieldid).attr('title', 'Optional')
			$('#reqOp_'+i+'_'+fieldid).text('O');
		}
		else
		{
			$("#req_"+i+"_"+fieldid).val(1);
		/*	$('#mand'+i).removeClass("blue");
			$('#mand'+i).addClass("green");			
			$('#mand'+i).attr('title', 'Mandatory');*/
			
			$('#reqOp_'+i+'_'+fieldid).removeClass("iconOptional");
			$('#reqOp_'+i+'_'+fieldid).addClass("iconRequired");
			$('#reqOp_'+i+'_'+fieldid).attr('title', 'Required')
			$('#reqOp_'+i+'_'+fieldid).text('R');
			
		}
	}
		
}

function chengeSectiondMandOptions(i)
{
	var sectionId=$("#sectionfieldid").val();
	var reqOpt=$("#sectionreq_"+i+"_"+sectionId).val();
	if(reqOpt==1)
	{
		$("#sectionreq_"+i+"_"+sectionId).val(0);				
		/*$('#sectionMand'+i).removeClass("green");
		$('#sectionMand'+i).addClass("blue");			
		$('#sectionMand'+i).attr('title', 'Optional');*/
		
		$('#secReqOpt_'+i+'_'+sectionId).removeClass("iconRequired");
		$('#secReqOpt_'+i+'_'+sectionId).addClass("iconOptional");			
		$('#secReqOpt_'+i+'_'+sectionId).attr('title', 'Optional');
		$('#secReqOpt_'+i+'_'+sectionId).text('O');
	}
	else
	{
		$("#sectionreq_"+i+"_"+sectionId).val(1);
		/*$('#sectionMand'+i).removeClass("blue");
		$('#sectionMand'+i).addClass("green");			
		$('#sectionMand'+i).attr('title', 'Mandatory');*/
		
		$('#secReqOpt_'+i+'_'+sectionId).removeClass("iconOptional");
		$('#secReqOpt_'+i+'_'+sectionId).addClass("iconRequired");			
		$('#secReqOpt_'+i+'_'+sectionId).attr('title', 'Required');
		$('#secReqOpt_'+i+'_'+sectionId).text('R');
		
	}		
}
function changeSectiondOptions(SecId,applicantType)
{	
	var reqOpt=$("#sectionreq_"+applicantType+"_"+SecId).val();
	if(reqOpt==1)
	{
		$("#sectionreq_"+applicantType+"_"+SecId).val(0);
		$('#secReqOpt_'+applicantType+'_'+SecId).removeClass("iconRequired");
		$('#secReqOpt_'+applicantType+'_'+SecId).addClass("iconOptional");			
		$('#secReqOpt_'+applicantType+'_'+SecId).attr('data-original-title', 'Optional');
		$('#secReqOpt_'+applicantType+'_'+SecId).text('O');
	}
	else
	{
		$("#sectionreq_"+applicantType+"_"+SecId).val(1);
		$('#secReqOpt_'+applicantType+'_'+SecId).removeClass("iconOptional");
		$('#secReqOpt_'+applicantType+'_'+SecId).addClass("iconRequired");			
		$('#secReqOpt_'+applicantType+'_'+SecId).attr('data-original-title', 'Required');
		$('#secReqOpt_'+applicantType+'_'+SecId).text('R');
		
	}		
}
function activeFieldOptions(applicantType)
{	
	var fieldid=$("#fieldid").val();
	var applicant="";
	if(applicantType==1)
		applicant="E";
	if(applicantType==2)
		applicant="I";
	if(applicantType==3)
		applicant="IT";
	
	var status =document.getElementById("active"+applicant);
	if(status.checked==false)
	{		
		$('#status_'+applicantType+'_'+fieldid).val('I');
		$('#'+applicantType+'_'+fieldid).removeClass("activeIcon");
		$('#'+applicantType+'_'+fieldid).addClass("inactiveIcon");
		$('#'+applicantType+'_'+fieldid).attr('title', 'Inactive')
		$('#'+applicantType+'_'+fieldid).text('X');
	}
	else 
	{		
		$('#status_'+applicantType+'_'+fieldid).val('A');
		$('#'+applicantType+'_'+fieldid).removeClass("inactiveIcon");
		$('#'+applicantType+'_'+fieldid).addClass("activeIcon");
		$('#'+applicantType+'_'+fieldid).attr('title', 'Active')
		$('#'+applicantType+'_'+fieldid).text('A');
	}	
}



function saveRecord()
{
	$("#errordivfield").empty();
	var portfolioArray1  =	[];	
	$('#errordiv4DSPQ').empty();
	var portfolioArray1  =	[];	
	secArray=[];
	var fieldName = "",req = "",instruction = "",tooltip="",status="",numberRequired=null,applicantTypeStatus="";	
	var dspqPortfolioNameId	=	  editId=$('#editDspqID').val();	
	
	var cnt=0,focs=0;
	var eDisplayName = $("#fieldnameE").val().trim();
	var iDisplayName = $("#fieldnameI").val().trim();
	var tDisplayName = $("#fieldnameIT").val().trim();
	if(eDisplayName=="")
	{
		$('#errordivfield').append("&#149; Please provide External Field name<BR>");
		if(focs==0)
			document.getElementById("fieldnameE").focus();		
		
		cnt++;focs++;
		
	}
	if(iDisplayName=="")
	{
		$('#errordivfield').append("&#149; Please provide Internal Field name<BR>");
		if(focs==0)
			document.getElementById("fieldnameI").focus();		
		
		cnt++;focs++;
		
	}
	if(tDisplayName=="")
	{
		$('#errordivfield').append("&#149; Please provide Internal Transfer Field name<BR>");
		if(focs==0)
			document.getElementById("fieldnameIT").focus();		
		
		cnt++;focs++;
		
	}
	
	if(cnt==0)	
		{
		
		var fieldid=$("#fieldid").val();
		var applicantType="";
		for(var i=1; i<=3; i++)
		{
			if(i==1)
				applicantType="E" 
			if(i==2)
				applicantType="I"
			if(i==3)
				applicantType="IT"			
					
			$("#applicantType_"+i+"_"+fieldid).val(applicantType);
			$("#field_"+i+"_"+fieldid).val($("#fieldname"+applicantType).val());
			
			var fieldLabel=$("#fieldname"+applicantType).val();
			$("#fieldLabel_"+i+"_"+fieldid).attr('title', fieldLabel);
			var fieldHelp=$("#tooltip"+applicantType).val();
			$("#fieldHelp_"+i+"_"+fieldid).attr('title', fieldHelp);
			
			/*var mand = document.getElementById("mandetory"+applicantType);
			if(mand.checked==true)			
				$("#req_"+i+"_"+fieldid).val(1);
			else
				$("#req_"+i+"_"+fieldid).val(0);*/
			
			$("#tooltip_"+i+"_"+fieldid).val($("#tooltip"+applicantType).val());
			
			var status =document.getElementById("active"+applicantType);
			/*var statusMain =document.getElementById("AD"+fieldid).value;
			if(statusMain=="A")
			{*/
				if(status.checked==true)
				{
					$("#status_"+i+"_"+fieldid).val("A");
				}
				else
				{
					$("#status_"+i+"_"+fieldid).val("I");	
				}
			/*}
			else
			{			
				$("#status_"+i+"_"+fieldid).val("I");			
			}*/
				
				fieldName		=	$("#fieldname"+applicantType).val();
				req				=	$("#req_"+i+"_"+fieldid).val();
				tooltip			=	$("#tooltip"+applicantType).val()
				status			=	$("#status_"+i+"_"+fieldid).val();
				if(applicantType=="IT")
					applicantType="T";
				if(req==1)
					req=true;
				else
					false;
				if(tooltip=="")
					tooltip=null;
				if(instruction=="")
					instruction=null;
				var dspqFieldMaster = 	{dspqFieldId:dwr.util.getValue("dspqFieldId"+fieldid)};
				portfolioArray1.push({
					"dspqFieldMaster"  : dspqFieldMaster,
					"displayName"  : fieldName,			
					"isRequired"  : req,
					"applicantType"  : applicantType,					
					"tooltip" :tooltip,
					"status" :status				
				});
		}
		var field1=$("#fieldnameE").val();
		var	field2=$("#fieldnameI").val();
		var	field3=$("#fieldnameIT").val();
		var applicant1 = document.getElementById("sectionExternal");
		var applicant2 = document.getElementById("sectionInternal");
		var applicant3 = document.getElementById("sectionITransfer");
		$("#1displayField"+fieldid).empty();
		if(field1==field2 && field2==field3 && field1==field3)
		{
			$("#1displayField"+fieldid).html(field1);
		}
		else
		{
			
			if(applicant1.checked==true)
			{
				$("#1displayField"+fieldid).append(field1);
			}
			if(applicant2.checked==true)
				$("#1displayField"+fieldid).append("/"+field2);
			if(applicant3.checked==true)
				$("#1displayField"+fieldid).append("/"+field3);
		}		
		/*ManageDspqAjax.getFieldNameUpdate(portfolioArray1,dspqPortfolioNameId,{
			async: true,
			callback: function(data)
			{
				
			},
		});*/
		hidepopup();
	}
}
function showDivMoreFields(secID)
{
	$('.moreFileds1').removeClass("open");
	$('#moreSecId'+secID).addClass("open");
	$('#sectionIdOnMoreField').val(secID);
}

function cancelAdditionalFields(){	
	$('.moreFileds1').removeClass("open");
}
function showHideAdditionalFieldss(secID){	
	$('#TMadditionalFieldss'+secID).removeClass("hide");
	$('#TMadditionalFieldss'+secID).addClass("show");
	$('#TMCustomFieldss'+secID).removeClass("show");
	$('#TMCustomFieldss'+secID).addClass("hide");
	
	$('#additionalMore'+secID).addClass("moreSelectedColor");
	$('#customMore'+secID).removeClass("moreSelectedColor");
}
function showHideCustomFieldss(secID){	
	$('#TMadditionalFieldss'+secID).removeClass("show");
	$('#TMadditionalFieldss'+secID).addClass("hide");
	$('#TMCustomFieldss'+secID).removeClass("hide");
	$('#TMCustomFieldss'+secID).addClass("show");
	
	$('#additionalMore'+secID).removeClass("moreSelectedColor");
	$('#customMore'+secID).addClass("moreSelectedColor");
}
function hideShowSubsection(secID ,applicantType){	
	
		var status=$('#sectionstatus_'+applicantType+'_'+secID).val();
		if(status=="A")
		{
			$('#sectionstatus_'+applicantType+'_'+secID).val('I');			
			$('#sectionRequired'+applicantType+'_'+secID).removeClass("activeIcon");
			$('#sectionRequired'+applicantType+'_'+secID).addClass("inactiveIcon");
			$('#sectionRequired'+applicantType+'_'+secID).attr('data-original-title', 'Inactive');
			$('#sectionRequired'+applicantType+'_'+secID).text('X');
			
			/*$('#instructions_'+applicantType+'_'+secID).removeClass("show");
			$('#instructions_'+applicantType+'_'+secID).addClass("hide");
			$('#instructionHide_'+applicantType+'_'+secID).removeClass("show");
			$('#instructionHide_'+applicantType+'_'+secID).addClass("hide");*/	
			
		}
		else
		{
			$('#sectionstatus_'+applicantType+'_'+secID).val('A');			
			$('#sectionRequired'+applicantType+'_'+secID).removeClass("inactiveIcon");
			$('#sectionRequired'+applicantType+'_'+secID).addClass("activeIcon");			
			$('#sectionRequired'+applicantType+'_'+secID).attr('data-original-title', 'Active')
			$('#sectionRequired'+applicantType+'_'+secID).text('A');
			
			/*$('#instructions_'+applicantType+'_'+secID).removeClass("hide");
			$('#instructions_'+applicantType+'_'+secID).addClass("show");
			$('#instructionHide_'+applicantType+'_'+secID).removeClass("hide");
			$('#instructionHide_'+applicantType+'_'+secID).addClass("show");*/
		}
}
function hideOrShowMainSubsection(secID){
	var totalCheckedSection=$('input[name=topSection]:checked').length
	if($('#mainSection'+secID).prop("checked") == true)
	{
		$('#panel'+secID).removeClass("hide");
		$('#panel'+secID).addClass("show");
		for(var i=1; i<=3; i++)
		{
			var applicantType=i;
			$('#sectionstatus_'+applicantType+'_'+secID).val('A');			
			$('#sectionRequired'+applicantType+'_'+secID).removeClass("inactiveIcon");
			$('#sectionRequired'+applicantType+'_'+secID).addClass("activeIcon");			
			$('#sectionRequired'+applicantType+'_'+secID).attr('title', 'Active');
			$('#sectionRequired'+applicantType+'_'+secID).text('A');
			
		}
		
		var chkSec = getMatcheStatus(orderingSection, secID);
		if(!chkSec)
			orderingSection.push(secID);
		
	}
	else{
		//$('#panel'+secID).hide();
		$('#panel'+secID).removeClass("show");
		$('#panel'+secID).addClass("hide");		
		var chkSec = getIndex(orderingSection, secID);
		if(chkSec!="")
			orderingSection.splice(chkSec, secID);
	}
	
}
function checkIsEditable(dspqId) {
	var val="";
	if(dspqId!="")
		ManageDspqAjax.getPortfolioIsEditable(dspqId,{
			async: false,
			callback: function(data)
			{
				val= data;
			},errorHandler:handleError
		});
	return val;
}
function alertOnClickTab(nextDiv){	
	var currentTab = $('#sectionid').val();
	var checkGlobal = document.getElementById("dspqGlobalLster").value;	
	$('#nextTab').val(nextDiv);
	if(checkGlobal>0){
		if(currentTab==1)
			$('#sectionLblName').html("Personal");
		if(currentTab==2)
			$('#sectionLblName').html("Academics");
		if(currentTab==3)
			$('#sectionLblName').html("Credentials");
		if(currentTab==4)
			$('#sectionLblName').html("Experiences");
		$('#alertOnChangeTab').modal("show");
	}
	else
		cancelChange();
}
function cancelChange(){
	var showPopUp = $('#checkPopup').val();
	var checkClickOnBackPort = $('#checkClickOnBackPort').val();
	if(showPopUp==2 && checkClickOnBackPort==1){
		$('#alertOnChangeTab').modal("hide");
		blankAllField();		
	}
	else{
		$('#dspqGlobalLster').val(0);
		var nextDiv = $('#nextTab').val();
		$('#alertOnChangeTab').modal("hide");	
		callDiv(nextDiv);
	}
}
function tabChange(){
	var dspqId=$("#dspqId").val();
	var districtId=$("#editDistrictId").val();
	$('#loadingDiv').show();
	 setTimeout(function() {jobCategoryAttachedNotification(4,dspqId,districtId,"");}, 10);
}
function tabChangeFinal(){
	$('#dspqGlobalLster').val(0);
	var nextDiv=$('#nextTab').val();
	$('#alertOnChangeTab').modal("hide");
	callDiv(nextDiv,0);
}
function savePortfolioOnClickTab(tabId){
	
	$('#errordiv4DSPQ').empty();
	var portfolioArray1  =	[];	
	secArray=[];
	var fieldName = "",req = "",instruction = "",tooltip="",status="",numberRequired=null,applicantTypeStatus="",othersAttribute="";
	var districtId		=	document.getElementById("districtId").value;
	var portfolioName	=	document.getElementById("portfolioName").value.trim();
	var sectionExternal		=	document.getElementById("sectionExternal");
	var sectionInternal		=	document.getElementById("sectionInternal");
	var sectionITransfer	=	document.getElementById("sectionITransfer");
	var groupId 	 = document.getElementById("groupId").value;
	var sectionList	=	document.getElementById("sectionList").value;
	var fieldList		=	document.getElementById("fieldList").value;
	
	var txtBgColor = "#F30303";	
	var count = 0;
	var cnt=0;
	var focs=0;		
	if(portfolioName==""){
		$('#errordiv4DSPQ').append("&#149; Please provide portfolio name<BR>");
		if(focs==0)
			document.getElementById("portfolioName").focus();
		
		$('#errordiv4DSPQ').css("color",txtBgColor);
		cnt++;focs++;
		return;
	}	
	if(count>0){
			$("#errordiv4DSPQ").html("&#149; Please fill all the text boxes<br>");
			$('#errordiv4DSPQ').css("color",txtBgColor);
			cnt++;focs++;
	}
	var k=1,L=1,M=1;	
	if(sectionInternal.checked==true)
	{
		M=2;
		sectionInternalChecked=1;
	}
	if(sectionITransfer.checked==true)
	{
		k=3;
		sectionITransferChecked=1;
	}
	var sectionArrayList=sectionList.split(",");
	var fieldArrayList=fieldList.split("##");
	var secID="", fieldID="";
	try{
		for(var sec=1; sec<=sectionArrayList.length-1;sec++)
		{
			secID=sectionArrayList[sec];
			var fieldArray=fieldArrayList[sec].split(",");
			for(var field=1; field<=fieldArray.length-1;field++)
			{
				fieldID=fieldArray[field];
				if(fieldID == 0)
				{
					for(var j=1;j<=3;j++)
					{	
						if(j==1)
						{
							L=1;
							applicantType="E";
							applicantTypeStatus="A";
						}
						if(j==2)
						{
							applicantType="I";
							if(M==2)
							{
								L=2;
								applicantTypeStatus="A";
							}
							else
							{
								L=1;
								applicantTypeStatus="I";
							}
						}
						if(j==3)
						{
							applicantType="T";
							if(k==3)
							{
								L=3;
								applicantTypeStatus="A";
							}
							else
							{
								if(M==2)
								{
									L=2;
									applicantTypeStatus="I";
								}
								else
									L=1
								applicantTypeStatus="I";
							}
						}
						
								instruction		=	document.getElementById("instruction_"+L+"_"+secID).value;
								//displayName		=	document.getElementById("sectionDisplay"+secID).value;	
								displayName		=	document.getElementById("sectionfield_"+L+"_"+secID).value;							
								
								var mainSection =	 document.getElementById("mainSection"+secID);								
								if(mainSection.checked==true)
								{
									status		=	document.getElementById("sectionstatus_"+L+"_"+secID).value;
								}									
								else
									status="I";
								req		=	document.getElementById("sectionreq_"+L+"_"+secID).value;
								if(req==0)
									req = false;
								else
									req = true;
								tooltip		=	document.getElementById("sectiontooltip_"+L+"_"+secID).value;
								var dspqSectionMaster = 	{sectionId:dwr.util.getValue("sectionId"+secID)};
								
								portfolioArray1.push({
									"dspqSectionMaster" : dspqSectionMaster,			
									"applicantType"  : applicantType,
									"instructions" : instruction,
									"displayName"  : displayName,
									"isRequired"  : req,
									"tooltip" :tooltip,
									//"numberRequired"  : numberRequired,
									"status" :status,
									"applicantTypeStatus" : applicantTypeStatus
								});
								
					}
					
				}
				else
				{
				var TMAddTMDef=document.getElementById("TMAddTMDef"+fieldID).value;
				for(var j=1;j<=3;j++)
				{	
					if(j==1)
					{
						L=1;
						applicantType="E";
						applicantTypeStatus="A";
					}
					if(j==2)
					{
						applicantType="I";
						if(M==2)
						{
							L=2;
							applicantTypeStatus="A";
						}
						else
						{
							L=1;
							applicantTypeStatus="I";
						}
					}
					if(j==3)
					{
						applicantType="T";
						if(k==3)
						{
							L=3;
							applicantTypeStatus="A";
						}
						else
						{
							if(M==2)
							{
								L=2;
								applicantTypeStatus="I";
							}							
							else
								L=1;
							applicantTypeStatus="I";
						}
					}
					if(TMAddTMDef=="Default")
					{
						if(compareArrayValue(secID)=="1")
						{
							instruction		=	document.getElementById("instruction_"+L+"_"+secID).value;
							//displayName		=	document.getElementById("sectionDisplay"+secID).value;	
							displayName		=	document.getElementById("sectionfield_"+L+"_"+secID).value;	
							numberRequired = null;
							if(secID=="9")													
								numberRequired		=	document.getElementById("Refrences_"+L).value;															
							if(secID=="6")
							{
								numberRequired		=	document.getElementById("degreeType_"+L).value;
								if(numberRequired==0)
									numberRequired=null;
								var degreeHierarchy		=	document.getElementById("degreeHierarchy_"+L);
								if(degreeHierarchy.checked==true)
									othersAttribute=0;
								else
									othersAttribute=1;
							}
							var mainSection =	 document.getElementById("mainSection"+secID);	
							if(mainSection.checked==true)
							{
								status		=	document.getElementById("sectionstatus_"+L+"_"+secID).value;
							}									
							else
								status="I";
							req		=	document.getElementById("sectionreq_"+L+"_"+secID).value;
							if(req==0)
								req = false;
							else
								req = true;
							tooltip		=	document.getElementById("sectiontooltip_"+L+"_"+secID).value;
							var dspqSectionMaster = 	{sectionId:dwr.util.getValue("sectionId"+secID)};		
							portfolioArray1.push({
								"dspqSectionMaster" : dspqSectionMaster,			
								"applicantType"  : applicantType,
								"instructions" : instruction,
								"displayName"  : displayName,
								"isRequired"  : req,
								"tooltip" :tooltip,
								"numberRequired"  : numberRequired,
								"status" :status,
								"othersAttribute" :othersAttribute,
								"applicantTypeStatus" : applicantTypeStatus
							});
							
						}
						fieldName		=	document.getElementById("field_"+L+"_"+fieldID).value;						
						req				=	document.getElementById("req_"+L+"_"+fieldID).value;						
						tooltip		=	document.getElementById("tooltip_"+L+"_"+fieldID).value;
						status		=	document.getElementById("status_"+L+"_"+fieldID).value;
						/*if(statusMain=="I")
							status		=	"I";
						else
							status		=	document.getElementById("status_"+j+"_"+fieldID).value;*/
						if(req==1)
							req=true;
						else
							false;
						
						if(tooltip=="")
							tooltip=null;
						if(instruction=="")
							instruction=null;
						
						var dspqFieldMaster = 	{dspqFieldId:dwr.util.getValue("dspqFieldId"+fieldID)};
						portfolioArray1.push({
							"dspqFieldMaster"  : dspqFieldMaster,
							"displayName"  : fieldName,
							//"numberRequired"  : numberRequired,
							"isRequired"  : req,
							"applicantType"  : applicantType,					
							"tooltip" :tooltip,
							"status" :status,
							"applicantTypeStatus" : applicantTypeStatus
						});
						
					}
					else
					{

						var additionalField=document.getElementById("additionalFieldID"+fieldID);
						
						if(additionalField.checked)
						{
							if(compareArrayValue(secID)=="1")
							{								
								instruction		=	document.getElementById("instruction_"+L+"_"+secID).value;
								//displayName		=	document.getElementById("sectionDisplay"+secID).value;	
								displayName		=	document.getElementById("sectionfield_"+L+"_"+secID).value;
								var mainSection =	 document.getElementById("mainSection"+secID);	
								if(mainSection.checked==true)
								{
									status		=	document.getElementById("sectionstatus_"+L+"_"+secID).value;
								}									
								else
									status="I";
								req		=	document.getElementById("sectionreq_"+L+"_"+secID).value;
								if(req==0)
									req = false;
								else
									req = true;
								tooltip		=	document.getElementById("sectiontooltip_"+L+"_"+secID).value;
								var dspqSectionMaster = 	{sectionId:dwr.util.getValue("sectionId"+secID)};
								

								portfolioArray1.push({
									"dspqSectionMaster" : dspqSectionMaster,			
									"applicantType"  : applicantType,
									"instructions" : instruction,
									"displayName"  : displayName,
									"isRequired"  : req,
									"tooltip" :tooltip,
									//"numberRequired"  : numberRequired,
									"status" :status,
									"applicantTypeStatus" : applicantTypeStatus
								});
							}
							fieldName		=	document.getElementById("field_"+L+"_"+fieldID).value;
							req				=	document.getElementById("req_"+L+"_"+fieldID).value;
							tooltip		=	document.getElementById("tooltip_"+L+"_"+fieldID).value;
							status		=	document.getElementById("status_"+L+"_"+fieldID).value;
							if(req==1)
								req=true;
							else
								false;
							if(tooltip=="")
								tooltip=null;
							if(instruction=="")
								instruction=null;
							var dspqFieldMaster = 	{dspqFieldId:dwr.util.getValue("dspqFieldId"+fieldID)};
							portfolioArray1.push({
								"dspqFieldMaster"  : dspqFieldMaster,
								"displayName"  : fieldName,	
								//"numberRequired"  : numberRequired,
								"isRequired"  : req,
								"applicantType"  : applicantType,					
								"tooltip" :tooltip,
								"status" :status,
								"applicantTypeStatus" : applicantTypeStatus
							});
							
						}						
					}
				}
				if(TMAddTMDef!="Default")
				{
					var additionalField=document.getElementById("additionalFieldID"+fieldID);
					if(additionalField.checked)
						secArray.push(secID);
				}
				else
					secArray.push(secID);
				
				
			 }
				
			}
			
		}
	}catch(e){
		alert(e);
	}		
	$('#loadingDiv').show();// return false;
	if(cnt==0){
		try{
			
			var dspqPortfolioNameId=$('#portfolioID').val();
			 var editId=$('#editDspqID').val();
			 if(dspqPortfolioNameId=="" || dspqPortfolioNameId==0)
				 dspqPortfolioNameId=editId;
			ManageDspqAjax.saveDspqFieldsValues(districtId,portfolioArray1,portfolioName,groupId,dspqPortfolioNameId,checkExistPortfolio,{
			async: true,
			callback: function(data)
			{
				if(data!="")
				{
					$('#errordiv4DSPQ').empty();
					$('#errordiv4DSPQ').append(data);
					$('#errordiv4DSPQ').css("color",txtBgColor);
					$('#loadingDiv').hide();
					window.location.href="#";
				}	
				displayPortfolio();
				$('#loadingDiv').hide();
				
			},
		});
		}catch(e){alert(e)}
	}
}
function hideErrorButton(nextDiv){	
	$('#alertOnError').modal('hide');
}
function showVideo(){
	var videoSource=$('#videoSource').val();	
	var videofile="<iframe src="+videoSource+" width='485' height='315' frameborder='0' allowfullscreen></iframe>";
	$('#manageVideo').html(videofile);
	$('#showVideo').modal('show');
}
function hideVideo(){	
	$('#showVideo').modal('hide');
}



function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
    hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getDistrictArray(districtName){
	var searchArray = new Array();
	JobUploadTempAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{

	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


function mouseOverChk(txtdivid,txtboxId)
{	
	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
//////////////////////Start District Search Filter/////////////////////////

function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
    hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtSearchName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function hideDistrictSearchMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtSearchId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtSearchId").value=hiddenDataArray[index];
		}
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function createDefaultPortfolioConfirmation(portfolioID){
	$('#errordiv4DSPQ').empty();
	var districtId = $('#districtId').val();
	if(districtId!=null && districtId!=""){
		$('#defaultPortfolioId').val(portfolioID);
		$('#defaultPortfolio').modal('show');
	}
	else{
		$('#errordiv4DSPQ').empty();
		var txtBgColor = "#F30303";
		$("#errordiv4DSPQ").html("&#149; Please select the District Name!<br><br>");
		$('#errordiv4DSPQ').css("color",txtBgColor);
	}
		
	
}
function createDefaultPortfolio(){
	var portfolioId = $('#defaultPortfolioId').val();
	var districtId = $('#districtId').val();
	ManageDspqAjax.createDefaultPortfolio(portfolioId,districtId,{
		async: true,
		callback: function(data)
		{
			displayPortfolio();
			$('#defaultPortfolio').modal('hide');
		},
		errorHandler:handleError 
});

}
function hideDefaultPortfolioModal(){
	$('#defaultPortfolio').modal('hide');
}

//////////////////////End District Search Filter/////////////////////////

function globalLsterSetter() 
{
    $(".form-control").click(function() 
	{
		globalLsterSetterL2();
	});
	
	$(".radio").click(function() 
	{
		globalLsterSetterL2();
	});
	
	$(".checkbox").click(function() 
	{
		globalLsterSetterL2();
	});
	
	$(".custlstner").click(function() 
	{
		globalLsterSetterL2();
	});
}

function globalLsterSetterL2() 
{
	var dspqGlobalLster=0;
	try { dspqGlobalLster=document.getElementById("dspqGlobalLster").value; } catch (e) {}
	dspqGlobalLster++;
	try { document.getElementById("dspqGlobalLster").value=dspqGlobalLster; } catch (e) {}
}
function setEntityType()
{
	var entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="1" selected="selected">TM</option><option value="2">District</option>';
	var districtSection="<div class='col-sm-12 col-md-12 hide' id='districtSearchBox'><label>District name</label><span><input autocomplete='off' style='color:#555555; font-size: 14px;'  type='text' id='districtSearchName'  value='' maxlength='100'  name='districtSearchName' class='help-inline form-control' onfocus=\"getDistrictAuto(this, event, 'divTxtSearchShowData', 'districtSearchName','districtSearchId','');\" onkeyup=\"getDistrictAuto(this,event,'divTxtSearchShowData', 'districtSearchName','districtSearchId','');\" onblur=\"hideDistrictMasterDiv(this,'districtSearchId','divTxtSearchShowData');\"/></span><input type='hidden' id='districtSearchId' /><div id='divTxtSearchShowData'  onmouseover=\"mouseOverChk('divTxtSearchShowData','districtSearchName')\" style='display:none;position:absolute;z-index:5000;' class='result' ></div>";
	var btnRecord='<button onclick="displayPortfolio();" class="flatbtn" style="background: #9fcf68;width:60%" id="save" title="Search"><strong>Search <i class="fa fa-check-circle"></i></strong></button>';
	globalSearchSection(entityString,"","",districtSection,btnRecord);
	setTimeout(function(){ 
  		if($("#districtMasterId").val()!=0){
  			$("select#entityType").prop('selectedIndex', 1);
  			displayOrHideSearchBox();
  			$("#districtSearchName").val($("#districtMasterName").val());
  		}
  		}, 50);
	if($("#districtMasterId").val()==0){
		showSearchAgainMaster();
	}else{
		hideSearchAgainMaster();
	}
}

function jobCategoryAttachedNotification(showType,dspqId,districtId,status)
{
	//alert(showType+" dspqId ::::::::: "+dspqId+"  districtId  :::::: "+districtId);
	$("#showPromptType").val(showType);
	ManageDspqAjax.findJobcategoryListByPortfolioId(dspqId,districtId,{
		async: false,
		callback: function(data)
		{
			if(data!=""){
				$('#portfolioChangeWarningWithJobCategory').modal('show');
				document.getElementById("portfolioChangeWarningMsgWithJobCategory").innerHTML=data;
				/*if(showType==1){
					$("#promptMsg").html(resourceJSON.msgDoyouwanttoeditportfolio);
				}else if(showType==2){
					if(status=="A")
						$("#promptMsg").html(resourceJSON.msgtoactivateportfolio);
					else
						$("#promptMsg").html(resourceJSON.msgyouwanttodeactivateportfolio);
				}else if(showType==3){
					$("#promptMsg").html("Do you want to save portfolio?");
				}else if(showType==4){
					$("#promptMsg").html("Do you want to change section?");
				}*/
			}else{
				if(showType==1){
					portfolioChangeWarningYesForEdit();
				}else if(showType==2){
					activateDeactivateDspqAfterPrompt();
				}else if(showType==3){
					savePortfolioNext();
				}else if(showType==4){
					tabChangeFinal();
				}
			}
		},
		errorHandler:handleError 
});
	$('#loadingDiv').hide();
}
function portfolioChangeWarningCloseDiv()
{
	$("#portfolioChangeWarningWithJobCategory").modal("hide");
}
function portfolioWarningYesPrompt()
{
var promptType=$("#showPromptType").val();	
	if(promptType==1){
		portfolioChangeWarningYesForEdit();
	}else if(promptType==2){
		activateDeactivateDspqAfterPrompt();
	}else if(promptType==3){
		savePortfolioNext();
	}else if(promptType==4){
		tabChangeFinal();
	}
}

function portfolioChangeWarningYesForEdit()
{
	portfolioChangeWarningCloseDiv();
	var dspqId=$("#dspqId").val();
	var cloneEditFlag=$("#cloneEditFlag").val();
	var districtId=$("#editDistrictId").val();
	var districtName=$("#editDistrictName").val();
	$('#loadingDiv').show();
	$('#checkPopup').val(2)
	$('#checkClickOnBackPort').val(0);
	if(districtId==null || districtId==""){
		$('#districtHideShow').hide();
		$('#districtId').val("");
		$('#districtName').val("");
	}else{	
		$('#districtHideShow').show();
		$('#districtId').val(districtId);
		$('#districtName').val(districtName.replace("&#","'"));
		$('#districtName').prop('disabled', true);
	}
	$('#createPortfolioHideShow').hide();
	$('#videoSource').val(resourceJSON.personalInformationVideo);
	document.getElementById("PortfolioNameIDS").value=dspqId;
	if(cloneEditFlag==1)
		$('#cloneID').val('clone');
	else
		$('#cloneID').val('edit');
	var oldId=$('#editDspqID').val();	
	if(dspqId!=oldId)
		callDivs('1');
	
	$('#showPortfolioDiv').show();
	$('#showPortfolioDiv1').show();
	$('#continueDiv').hide();
	var groupId 	 = document.getElementById("groupId").value;
	var sectionList	=	document.getElementById("sectionList").value;
	var fieldList		=	document.getElementById("fieldList").value;
	var TMAddFieldList		=	document.getElementById("TMAddFieldList").value;
	var r=false,r1=false;
	var sectionArrayList=sectionList.split(",");
	var fieldArrayList=fieldList.split("##");
	var TMAddList=TMAddFieldList.split(",");	
	var secID="";
	var field1="",field2="",field3="",field1Id="",field2Id="";
	var section1="",section2="",section3="",sectionId="",sectionId="";
	
	var arrayField=[];
	var arrField  = [];
	for(var sec=1; sec<=sectionArrayList.length-1;sec++){		
		var fieldArray=fieldArrayList[sec].split(",");
		for(var field=1; field<=fieldArray.length-1;field++){
			if(fieldArray[field]!=null && fieldArray[field]!="")
				arrayField.push(fieldArray[field]);
		}
	}
	orderingSection=[];
	var sectionOrderArray=$("#sectionOrderArray").val();
	if(sectionOrderArray!=null && sectionOrderArray!="")
		orderingSection=sectionOrderArray.split(",");	
	document.getElementById("editDspqID").value=dspqId;
	var ss=dspqId;
	ManageDspqAjax.editDspq(dspqId,{
		async: false,
		callback: function(data)
		{		
		sectionArrayList.splice(0, 1);
	try{
		for (var i=0 ;i<=data.length-1;i++ ){
			if((data[i].applicantType)=="E")			
				j=1;
			if((data[i].applicantType)=="I"){
				j=2;
				if((data[i].applicantTypeStatus)=="A"){
					if(r==false){
						document.getElementById("sectionInternal").checked = true;				
						hideOrShowApplicant();
						r=true;
					}
				}
			}
			if((data[i].applicantType)=="T"){
				j=3;
				if((data[i].applicantTypeStatus)=="A"){
					if(r1==false){
						document.getElementById("sectionITransfer").checked = true;
						hideOrShowApplicant();
						r1=true;
					}
				}
			}
			if((data[i].dspqSectionMaster)!=null){
				secID=(data[i].dspqSectionMaster.sectionId).toString();
				try{
				if(data[i].status=="A")
					document.getElementById("allSectionChecked_"+secID).value =1;
				}catch (e) {
				}
				
				var chk = sectionArrayList.indexOf(secID);
				if(chk!=-1){					
					if(data[i].instructions!=null){
						document.getElementById("instruction_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =data[i].instructions;
						$("#secInstruction_"+j+"_"+secID).attr('title', data[i].instructions);
					}
					if(data[i].displayName!=null){
						document.getElementById("sectionfield_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =data[i].displayName;
												
						if(j==1){
							sectionId = data[i].dspqSectionMaster.sectionId;
							section1 = data[i].displayName;						
							$("#secLabel_"+j+"_"+secID).attr('title', section1);							
						}
						if(j==2){
							section2 = data[i].displayName;
							$("#secLabel_"+j+"_"+secID).attr('title', section2);
						}
						if(j==3){
							sectionId = data[i].dspqSectionMaster.sectionId;
							section3 = data[i].displayName;
							$("#secLabel_"+j+"_"+secID).attr('title', section3);
						}
						if(sectionId ==sectionId){
							if(section1==section2 && section2==section3 && section1==section3){
								$('#sectionLabel_'+data[i].dspqSectionMaster.sectionId).html(section1);
							}else{								
								if(r==true && r1==true)
									$('#sectionLabel_'+data[i].dspqSectionMaster.sectionId).html(section1+"/"+section2+"/"+section3);
								else if(r==true)
									$('#sectionLabel_'+data[i].dspqSectionMaster.sectionId).html(section1+"/"+section2);
								else if(r1==true)
									$('#sectionLabel_'+data[i].dspqSectionMaster.sectionId).html(section1+"/"+section3);
							}
						}
					
					}
					if(data[i].isRequired==true){
						document.getElementById("sectionreq_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =1;						
						$('#secReqOpt_'+j+'_'+secID).removeClass("iconOptional");
						$('#secReqOpt_'+j+'_'+secID).addClass("iconRequired");
						$('#secReqOpt_'+j+'_'+secID).attr('title', 'Required');
						$('#secReqOpt_'+j+'_'+secID).text('R');
						
					}
					if(data[i].tooltip!=null){
						document.getElementById("sectiontooltip_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =data[i].tooltip;
						$("#secHelp_"+j+"_"+secID).attr('title', data[i].tooltip);
					}
					
					document.getElementById("sectionstatus_"+j+"_"+data[i].dspqSectionMaster.sectionId).value =data[i].status;						
						if(data[i].status=="I"){
							var secIDS=data[i].dspqSectionMaster.sectionId;
							$('#sectionstatus_'+j+'_'+secIDS).val('I');			
							$('#sectionRequired'+j+'_'+secIDS).removeClass("activeIcon");
							$('#sectionRequired'+j+'_'+secIDS).addClass("inactiveIcon");
							$('#sectionRequired'+j+'_'+secIDS).attr('title', 'Inactive');
							$('#sectionRequired'+j+'_'+secID).text('X');
							//$('#instructionHide_'+j+'_'+secIDS).hide();
						}else{
							$("#mainSection"+secID).prop('checked', true);
							if(secID!=25)
							{
								$('#panel'+secID).removeClass("hide");
								$('#panel'+secID).addClass("show");
							}
							//$('#instructionHide_'+j+'_'+secID).show();
						}
						if(secID==9)
							document.getElementById("Refrences_"+j).value =data[i].numberRequired;
						if(secID==6){
							if(data[i].numberRequired!=null)
								document.getElementById("degreeType_"+j).value =data[i].numberRequired;						
							if(data[i].othersAttribute==1)						
								$("#degreeHierarchy_"+j+j).prop('checked', true);
						}
				}				
			}			
			
			if((data[i].dspqFieldMaster)!=null){
				pushToAryKeyVal(data[i].dspqFieldMaster.dspqFieldId,data[i].status);
				
				var fieldIDS=(data[i].dspqFieldMaster.dspqFieldId).toString();
				var chk = arrayField.indexOf(fieldIDS);
				if(chk!=-1){		
					var TMFieldID = TMAddList.indexOf(fieldIDS);
					if(TMFieldID!=-1){
						document.getElementById("additionalFieldID"+data[i].dspqFieldMaster.dspqFieldId).checked = true;						
						if(j==1)
						hideShowAdditionalFields(data[i].dspqFieldMaster.dspqFieldId,secID,"array"+secID);						
					}
					if(data[i].displayName!=null){
						document.getElementById("field_"+j+"_"+data[i].dspqFieldMaster.dspqFieldId).value =data[i].displayName;
												
						if(j==1){
							field1Id = data[i].dspqFieldMaster.dspqFieldId;
							field1 = data[i].displayName;
							$("#fieldLabel_"+j+"_"+fieldIDS).attr('title', field1);
						}
						if(j==2){
							field2 = data[i].displayName;
							$("#fieldLabel_"+j+"_"+fieldIDS).attr('title', field2);
						}
						if(j==3){
							field2Id = data[i].dspqFieldMaster.dspqFieldId;
							field3 = data[i].displayName;
							$("#fieldLabel_"+j+"_"+fieldIDS).attr('title', field3);
						}
						if(field1Id ==field2Id){
							if(field1==field2 && field2==field3 && field1==field3){
								$('#1displayField'+data[i].dspqFieldMaster.dspqFieldId).html(field1);
							}else{								
								if(r==true && r1==true)
									$('#1displayField'+data[i].dspqFieldMaster.dspqFieldId).html(field1+"/"+field2+"/"+field3);
								else if(r==true)
									$('#1displayField'+data[i].dspqFieldMaster.dspqFieldId).html(field1+"/"+field2);
								else if(r1==true)
									$('#1displayField'+data[i].dspqFieldMaster.dspqFieldId).html(field1+"/"+field3);
							}
						}	
					}
					var fieldRequired=data[i].dspqFieldMaster.dspqFieldId;
					if(data[i].isRequired==true){
						document.getElementById("req_"+j+"_"+data[i].dspqFieldMaster.dspqFieldId).value = 1;						
						$('#reqOp_'+j+'_'+fieldRequired).removeClass("iconOptional");
						$('#reqOp_'+j+'_'+fieldRequired).addClass("iconRequired");	
						var IsFieldRequired = $("#isRequiredTest"+fieldRequired).val();
						if(IsFieldRequired !=1)
							$('#reqOp_'+j+'_'+fieldRequired).attr('data-original-title', 'Required');
						$('#reqOp_'+j+'_'+fieldRequired).text('R');
					}else{
						$('#reqOp_'+j+'_'+fieldRequired).attr('data-original-title', 'Optional');
					}
					if(data[i].tooltip!=null){
						document.getElementById("tooltip_"+j+"_"+data[i].dspqFieldMaster.dspqFieldId).value =data[i].tooltip;
						$("#fieldHelp_"+j+"_"+fieldIDS).attr('title', data[i].tooltip);
					}
					document.getElementById("status_"+j+"_"+data[i].dspqFieldMaster.dspqFieldId).value = data[i].status;
					var fieldid=data[i].dspqFieldMaster.dspqFieldId;
					if(data[i].status=="I"){			
						$('#'+j+'_'+fieldid).removeClass("activeIcon");
						$('#'+j+'_'+fieldid).addClass("inactiveIcon");
						$('#'+j+'_'+fieldid).attr('title', 'Inactive');
						$('#'+j+'_'+fieldid).text('X');						
					}
				}
			}			
		}
		var cloneId = $('#cloneID').val();
		if(cloneId!="clone"){
			getDspqPortfolioName(dspqId);
			checkExistPortfolio="True"
		}
	}catch (e) {
		alert(e);
	}
		$('#loadingDiv').hide();
		},
		errorHandler:handleError 
});
}

function generateCustomFieldOtionsOneTime()
{
	var noOfOptionRows=document.getElementById("noOfOptionRows").value;
	var integer_noOfOptionRows;
	integer_noOfOptionRows = parseInt(noOfOptionRows);
	var allOptionsAreaText="";
	for(var i=1; i<=integer_noOfOptionRows; i++)
		allOptionsAreaText=allOptionsAreaText+getCustomeFieldMoreRow(i);
	
	$("#allOptionsArea").html("");
	document.getElementById("allOptionsArea").innerHTML=allOptionsAreaText;
}

function addCustomFieldMoreOtions()
{
	var noOfOptionRows=document.getElementById("noOfOptionRows").value;
	var integer_noOfOptionRows;
	integer_noOfOptionRows = parseInt(noOfOptionRows);
	integer_noOfOptionRows=integer_noOfOptionRows+1;
	document.getElementById("noOfOptionRows").value=integer_noOfOptionRows;
	var newMoreRow=getCustomeFieldMoreRow(integer_noOfOptionRows);
	$("#allOptionsArea").append(newMoreRow);
}
function getCustomeFieldMoreRow(iStartRow)
{
	var sReturnValues="";
	if(iStartRow >0)
	{
		sReturnValues="<div class='row top5' id='row"+iStartRow+"'>";
		var sRowInnerDataLeft=getCustomeFieldColunns(1, iStartRow);
		sReturnValues=sReturnValues+sRowInnerDataLeft;
		var sRowInnerDataRight=getCustomeFieldColunns(2, iStartRow);
		sReturnValues=sReturnValues+sRowInnerDataRight;
		sReturnValues=sReturnValues+"</div>";
	}
	return sReturnValues;
}

function getCustomeFieldColunns(iLetfOrRight,iStartRow)
{
	var sReturnValues="";
	var iColNo=0;
	if(iLetfOrRight==1)
		iColNo=((iStartRow-1)*2)+1;
	else if(iLetfOrRight==2)
		iColNo=((iStartRow-1)*2)+2;
	
	sReturnValues=sReturnValues+"<div class='col-sm-6 col-md-6'>";
	sReturnValues=sReturnValues+"<label style='width:10px;'>"+iColNo+"</label>&nbsp;<input type='hidden' name='hid"+iColNo+"' id='hid"+iColNo+"' /> <input type='text' name='opt"+iColNo+"' id='opt"+iColNo+"' class='form-control' style='display: inline-table;width: 90% !important;' maxlength='200' placeholder=''>";
	sReturnValues=sReturnValues+"</div>";
	
	return sReturnValues;
}

function hideCustomeFieldColunns(iFrom,iTotal)
{
	for(var i=iFrom; i<=iTotal; i=i+1)
		$('#row'+i).hide();
}

function showCustomeFieldColunns(iFrom,iTotal)
{
	for(var i=iFrom; i<=iTotal; i=i+1)
		$('#row'+i).show();
}

function upArrow(secId) {
	var currSib="",currId="",prevSibResult="",prevIdResult="",prevIdResultNotEx="";
	currId = document.getElementById('orderSec'+secId);
	prevIdResult=currId.previousSibling.innerHTML;
	currSib = document.getElementById('panel'+secId);
	prevSibResult=currSib.previousSibling.innerHTML;
	var isMatch=getMatcheStatus(orderingSection, prevIdResult);	
	if(!isMatch){
		prevIdResult=orderingSection[(orderingSection.length)-2];
		prevIdResultNotEx=document.getElementById('panel'+prevIdResult)
		prevSibResult =prevIdResultNotEx.innerHTML;		
	}
	var chkCurrentIndex = getIndex(orderingSection, secId);	
	if(chkCurrentIndex!=0){
		var resultDiv = document.getElementById('panel'+prevIdResult);
		resultDiv.innerHTML = currSib.innerHTML;
		var resultId = document.getElementById('panel'+secId);
		resultId.innerHTML = prevSibResult;
		
		var newResultDiv =  document.getElementById('orderSec'+prevIdResult);	
		newResultDiv.innerHTML = currId.innerHTML;
		var newResultId = document.getElementById('orderSec'+secId);
		newResultId.innerHTML = prevIdResult;
		
		document.getElementById("panel"+secId).id= "div_top_NOT";
		document.getElementById("panel"+prevIdResult).id= 'panel'+secId;
		document.getElementById("div_top_NOT").id= "panel"+prevIdResult;
		 
		document.getElementById("orderSec"+secId).id= "div_top_NOT";
		document.getElementById("orderSec"+prevIdResult).id= 'orderSec'+secId;
		document.getElementById("div_top_NOT").id= "orderSec"+prevIdResult;
		
		var chkSec = getIndex(orderingSection, prevIdResult);
		var chkCurrentSec = getIndex(orderingSection, secId);
		if(chkSec==0){
			var iSecId=parseInt(secId);
			var iPrevIdResult=parseInt(prevIdResult);
			$('#up'+iSecId).removeClass("enableArrow");
			$('#up'+iSecId).addClass("disableArrow");
			$('#up'+iPrevIdResult).removeClass("disableArrow");
			$('#up'+iPrevIdResult).addClass("enableArrow");
		}
		
		if(chkCurrentSec==(orderingSection.length-1)){
			var iSecId=parseInt(secId);
			var iPrevIdResult=parseInt(prevIdResult);
			$('#down'+iSecId).removeClass("disableArrow");
			$('#down'+iSecId).addClass("enableArrow");
			$('#down'+iPrevIdResult).removeClass("enableArrow");
			$('#down'+iPrevIdResult).addClass("disableArrow");
		}			
					
		orderingSection[chkCurrentSec]=prevIdResult;		
		orderingSection[chkSec]=secId;
	}	
		
}	
function downArrow(secId) {
	
	var currSib="",currId="",nextSibResult="",nextIdResult="",nextIdResultNotEx="";
	
	currId = document.getElementById('orderSec'+secId);
	nextIdResult=currId.nextSibling.innerHTML;
	currSib = document.getElementById('panel'+secId);
	nextSibResult=currSib.nextSibling.innerHTML;
	//alert("nextIdResult "+nextIdResult +"orderingSection "+orderingSection)
	var isMatch=getMatcheStatus(orderingSection, nextIdResult);	
	if(!isMatch){	
		nextIdResult=orderingSection[(orderingSection.length)-2];
		nextIdResultNotEx=document.getElementById('panel'+prevIdResult)
		nextSibResult =nextIdResultNotEx.innerHTML;		
	}
	var chkCurrentIndex = getIndex(orderingSection, secId);	
	if(chkCurrentIndex!=(orderingSection.length-1)){
		
		var resultDiv = document.getElementById('panel'+nextIdResult);
		resultDiv.innerHTML = currSib.innerHTML;
		var resultId = document.getElementById('panel'+secId);
		resultId.innerHTML = nextSibResult;
		
		var newResultDiv =  document.getElementById('orderSec'+nextIdResult);	
		newResultDiv.innerHTML = currId.innerHTML;
		var newResultId = document.getElementById('orderSec'+secId);
		newResultId.innerHTML = nextIdResult;
		
		document.getElementById("panel"+secId).id= "div_top_NOT";
		document.getElementById("panel"+nextIdResult).id= 'panel'+secId;
		document.getElementById("div_top_NOT").id= "panel"+nextIdResult;
		 
		document.getElementById("orderSec"+secId).id= "div_top_NOT";
		document.getElementById("orderSec"+nextIdResult).id= 'orderSec'+secId;
		document.getElementById("div_top_NOT").id= "orderSec"+nextIdResult;
		
		var chkSec = getIndex(orderingSection, nextIdResult);
		var chkCurrentSec = getIndex(orderingSection, secId);	
		if(chkSec==(orderingSection.length-1)){
			var iSecId=parseInt(secId);
			var iNextIdResult=parseInt(nextIdResult);
			$('#down'+iSecId).removeClass("enableArrow");
			$('#down'+iSecId).addClass("disableArrow");
			$('#down'+iNextIdResult).removeClass("disableArrow");
			$('#down'+iNextIdResult).addClass("enableArrow");
		}
		
		if(chkCurrentSec==0){
			var iSecId=parseInt(secId);
			var iNextIdResult=parseInt(nextIdResult);
			$('#up'+iSecId).removeClass("disableArrow");
			$('#up'+iSecId).addClass("enableArrow");
			$('#up'+iNextIdResult).removeClass("enableArrow");
			$('#up'+iNextIdResult).addClass("disableArrow");
		}
			
		orderingSection[chkCurrentSec]=nextIdResult;
		orderingSection[chkSec]=secId;
	}	
}


function getMatcheStatus(orderingSection,prevIdResult)
{
	try {
		var isMatche=false;
		if(orderingSection!=null && orderingSection.length >0)
		{
			var iPrevIdResult=parseInt(prevIdResult);
			for(var i=0; i < orderingSection.length; i++)
			{
				//alert(orderingSection +" :: "+orderingSection[i] +" == "+iPrevIdResult);
				var iTempValue = parseInt(orderingSection[i]);
				if(iTempValue!="NaN" && iTempValue==iPrevIdResult)
				{
					isMatche=true;
					break;
				}
			}
		}
	} catch (e) {
		alert(e);
	}
	
	return isMatche;
}
function getIndex(orderingSection,prevIdResult)
{
	try {
		var index="";
		if(orderingSection!=null && orderingSection.length >0)
		{
			var iPrevIdResult=parseInt(prevIdResult);
			for(var i=0; i < orderingSection.length; i++)
			{
				var iTempValue = parseInt(orderingSection[i]);
				if(iTempValue!="NaN" && iTempValue==iPrevIdResult)
				{
					index=i;
				}
			}
		}
	} catch (e) {
		alert(e);
	}
	
	return index;
}
