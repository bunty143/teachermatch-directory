//@author : ankit

var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr +exception.javaClassName);}
}
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayCandidates();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayCandidates();
}
/********************************** district & school auto completer ***********************************/
function showSchool()
{
	try {
		var districtId = $("#districtId").val();
		//if(districtId.length>0)
		//	document.getElementById("schoolName1").disabled=false;
		//else
		//	document.getElementById("schoolName1").disabled=true;
	} catch (e) {
	}
}

function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("schoolName1").focus();
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	
	var searchArray = new Array();
	JBServiceAjax.getDistrictMasterList(districtName,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
			showDataArray[i]=data[i].districtName;
		}
	}
	});	

	return searchArray;
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
			//	$('#universityName').focus();
			
		//	$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function __mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		document.getElementById('divResult'+txtdivid+i).className='normal';		
		if ($('#divResult'+txtdivid+i).is(':hover')) 
		{
			document.getElementById('divResult'+txtdivid+i).className='over';	       
	       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
	        index=i;
	    }
	}

}

var overText = function (div_value,txtdivid) 
{
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}


function getSchoolMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{		
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';	
		searchArray = getSchoolArray(txtSearch.value);		
		fatchData(txtSearch,searchArray,txtId,txtdivid);	
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}	
}

function getSchoolArray(schoolName){	
	var districtId = document.getElementById("districtId").value;	
	var searchArray = new Array();
	JBServiceAjax.getSchoolMasterList(schoolName,districtId,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			
			searchArray[i]=data[i].schoolMaster.schoolName;			
			hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			showDataArray[i]=data[i].schoolMaster.schoolName;
		}
	}
	});	
	return searchArray;
}
function hidedivTxtSchoolDataDiv()
{
	document.getElementById("divTxtSchoolData").style.display='none';	
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");		
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
function hideSchoolMasterDiv1(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");			
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var selectFirst="";

function displayCandidates()
{	
	var districtId = "";
	var schoolId = "";
	if(document.getElementById('districtId').value!='')
	{
		districtId  = document.getElementById('districtId').value;	
	}
	if(document.getElementById('schoolId1').value!='')
	{
		schoolId  = document.getElementById('schoolId1').value;	
	}
	$('#loadingDiv').show();
	try
	{
		CandidatesBySchoolSelectionAjax.displaySelectedSchoolCandidates(districtId,schoolId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			if(data!='')
			{
				$('#loadingDiv').hide();			
				$('#divData').html(data);			
				applyScrollOnTbl();
			}
			else
			{
				//alert("No Data");
			}
			
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}	
	
}

function generateSelectedCandidateReportExcel()
{
	var districtId = "";
	var schoolId = "";
	if(document.getElementById('districtId').value!='')
	{
		districtId  = document.getElementById('districtId').value;	
	}
	if(document.getElementById('schoolId1').value!='')
	{
		schoolId  = document.getElementById('schoolId1').value;	
	}
	//alert("districtId :: "+districtId+" schoolId :: "+schoolId );
	//alert(" sortOrderStr :: "+sortOrderStr+" sortOrderType :: "+sortOrderType);
	try
	{
		$('#loadingDiv').show();
		CandidatesBySchoolSelectionAjax.generateSelectedSchoolByCandidateExcel(districtId,schoolId,sortOrderStr,sortOrderType,{
		async: true,
		callback: function(data)
		{
		$('#loadingDiv').hide();
			if(deviceType)
			{					
					//$("#exelfileNotOpen").css({"z-index":"3000"});
			    	//$('#exelfileNotOpen').show();					
			}
			else
			{
				try
				{
					document.getElementById('ifrmTrans').src = "schoolselectedbycandidatereport/"+data+"";
				}
				catch(e)
				{alert(e);}
			}			
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}	
}

//

function generateSelectedSchoolByCandidatePdf()
{
	var districtId = "";
	var schoolId = "";
	if(document.getElementById('districtId').value!='')
	{
		districtId  = document.getElementById('districtId').value;	
	}
	if(document.getElementById('schoolId1').value!='')
	{
		schoolId  = document.getElementById('schoolId1').value;	
	}
	//alert(" sortOrderStr :: "+sortOrderStr+" sortOrderType :: "+sortOrderType);
	try
	{
		$('#loadingDiv').show();
		CandidatesBySchoolSelectionAjax.generateSelectedSchoolByCandidateReportPdf(districtId,schoolId,sortOrderStr,sortOrderType,{
		async: true,
		callback: function(data)
		{	$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
		}		
		else
		{
			$('#modalDownloadSelectedSchoolByCandidate').modal('hide');
			document.getElementById('ifrmSSBC').src = ""+data+"";
			try{
				$('#modalDownloadSelectedSchoolByCandidate').modal('show');
			}catch(err)
			{}		
	     }		
	     return false;
		}
	});
	}catch(e){alert(e)}	
}

function generateSelectedSchoolByCandidatePrintPre()
{
	var districtId = "";
	var schoolId = "";
	if(document.getElementById('districtId').value!='')
	{
		districtId  = document.getElementById('districtId').value;	
	}
	if(document.getElementById('schoolId1').value!='')
	{
		schoolId  = document.getElementById('schoolId1').value;	
	}
	try
	{
	$('#loadingDiv').fadeIn();
	CandidatesBySchoolSelectionAjax.generateSelectedSchoolByCandidatePrintPreview(districtId,schoolId,sortOrderStr,sortOrderType,{
		async: true,
		callback: function(data)
		{	$('#loadingDiv').hide();
			//alert("@@ :: "+data)
				$('#loadingDiv').hide();
				$('#printSelectedSchoolByCandidateDataTableDiv').html(data);
				$("#printSelectedSchoolByCandidateDiv").modal('show');
				applyScrollOnPrintTable();
		  
		},
		errorHandler:handleError
	});
	}
	catch(e){alert(e)}	
}

function printSelectedSchoolByCandidateData()
{
	var districtId = "";
	var schoolId = "";
	if(document.getElementById('districtId').value!='')
	{
		districtId  = document.getElementById('districtId').value;	
	}
	if(document.getElementById('schoolId1').value!='')
	{
		schoolId  = document.getElementById('schoolId1').value;	
	}
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	//alert("districtId :: "+districtId+" schoolId :: "+schoolId );
	try
	{
	$('#loadingDiv').fadeIn();
	CandidatesBySchoolSelectionAjax.generateSelectedSchoolByCandidatePrintPreview(districtId,schoolId,sortOrderStr,sortOrderType,{
		async: true,
		callback: function(data)
		{ 
		$('#loadingDiv').hide();
		 if (isSafari && !deviceType)
		    {
		    	window.document.write(data);
				window.print();
		    }else
		    {
		    	var newWindow = window.open();
		    	newWindow.document.write(data);	
		    	newWindow.print();
			 }
	    
		},
	errorHandler:handleError
	});
	}
	catch(e){alert(e);}
	
}
function cancelPrint()
{
	//alert("cancel print......");
	 $('#printSelectedSchoolByCandidateDiv').hide();
	 $('#loadingDiv').hide();
	 //applyScrollOnTbl();
		
}