$(document).ready(function(){
	//alert('document');
	allhide();
	$('#home').show();
	var parent=$('#toolkitDiv').parent();
	$(parent).css('margin-bottom','-23px');
	$('#toolkitdivs a').css('font-weight','bold');
	$('#toolkitDiv a').css('font-weight','normal');	
});
function allhide(){
	//alert('allhide');
	$('#home').hide();
	$('#videos').hide();
	$('#books').hide();
	$('#infographics').hide();
	$('#worksheets').hide();
	$('.commingsoon').hide();
}
function callInfoLink(val){
	//alert(val);
	if(val=='videos'){
		allhide();
		$('#videos').show();
	}
	else if(val=='books'){
		allhide();
		$('#books').show();
		$('.commingsoon').show();
	}
	else if(val=='infographics'){
		allhide();
		$('#infographics').show();
	}
	else if(val=='worksheets'){
		allhide();
		$('#worksheets').show();
	}
	else if(val=='home'){
		allhide();
		$('#home').show();
	}
}
function openNewWindow(val){
window.open(val);	
}
