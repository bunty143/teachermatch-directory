var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			//alert(" Hi ");
			$('#schoolName').attr('readonly', true);
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
			
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*--- mukesh ------------------------------School Auto Complete Js Starts ----------------------------------------------------------------*/

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	//alert(schoolName);
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
	//alert(districtId);
	CandidatejobstatusAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt1(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) || (charCode==44));
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr + exception.javaClassName);}
}



// for paging and sorting
function getPaging(pageno)
{
        //alert("Paging");
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		searchRecordsByEntityTypeEEC();
	
}


function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	  if(pageno!=''){
			page=pageno;
			//alert("page :: "+page);
		}else{
			page=1;
			//alert("default");
		}
		sortOrderStr=sortOrder;
		//alert("sortOrderStr :: "+sortOrderStr);
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			//alert("AA")
			noOfRows = document.getElementById("pageSize").value;
		}else{
		//	alert("BB");
			noOfRows=50;
		}
		searchRecordsByEntityTypeEEC();
		
	
}

function activecityType(){
	document.getElementById("certType").value='';
}


function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGridEEC").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#cg'+j).tooltip();
		$('#cgn'+j).tooltip();
	}
}

//**************** global defined variable for exporting *******************
var f_JobOrderType     
var f_searchTextId	
var f_schoolId		
var f_jobOrderId	    
var	f_firstName       
var	f_lastName        
var	f_emailAddress    
var f_stateId          
var f_certType		
var f_jobCategoryIds
var f_statusId

var f_sfromDate
var f_stoDate
var f_endfromDate
var f_endtoDate

var f_appsfromDate
var f_appstoDate

var pdateFlag=true
var normScoreSelectVal
var normScore
var internal
var jobstatus
var hiddenjob
var hiredfromDate
var hiredtoDate
//******************************************************************************

function searchRecordsByEntityTypeEEC()
{ 
	var searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	 var sfromDate	=	trim(document.getElementById("sfromDate").value);
	 var stoDate	=	trim(document.getElementById("stoDate").value);
	 	$('#sfromDate').css("background-color", "#FFFFFF");
	 var endfromDate	=	trim(document.getElementById("endfromDate").value);
     var endtoDate     =	trim(document.getElementById("endtoDate").value);
        $('#endfromDate').css("background-color", "#FFFFFF");
     
     var appfromDate = trim(document.getElementById("appsfromDate").value);
 	 var apptoDate = trim(document.getElementById("appstoDate").value);
 	  $('#appsfromDate').css("background-color", "#FFFFFF");
 	 
	 var	hirefromDate=trim(document.getElementById("hiredfromDate").value);
	 var	hiretoDate=trim(document.getElementById("hiredtoDate").value);
	     $('#hiredfromDate').css("background-color", "#FFFFFF");
	 
	if (searchTextId=='') 
	{
		if((sfromDate!="" && stoDate!="")||(sfromDate!="" && stoDate==""))
		{
		    var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		    var stoDate1;
			var sfromDate1;
			
			if(sfromDate!="" && stoDate!=""){
				var  firstDate	= sfromDate.split("-");
				var secondDate 	= stoDate.split("-");
				
				sfromDate1 = new Date(firstDate[2],firstDate[0],firstDate[1]);
				  stoDate1 = new Date(secondDate[2],secondDate[0],secondDate[1]);
			}else if(sfromDate!="" && stoDate==""){
				var  firstDate	= sfromDate.split("-");
				sfromDate1 = new Date(firstDate[2],firstDate[0],firstDate[1]);
				stoDate1 = new Date();
			}
			stoDate1.setHours(0);
			stoDate1.setMinutes(0);
			stoDate1.setSeconds(0);
		    stoDate1.setMilliseconds(0);
			sfromDate1.setHours(0);
			sfromDate1.setMinutes(0);
			sfromDate1.setSeconds(0);
			sfromDate1.setMilliseconds(0);
			
			var diffDays = Math.round((stoDate1.getTime() - sfromDate1.getTime())/(oneDay));
			/*var timeDiff = stoDate1.getTime() - sfromDate1.getTime();
			alert(timeDiff+"   timeDiff   "+stoDate1+"    "+sfromDate1);
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));*/
			
			//alert(diffDays)
			
			
			if(diffDays>30){
				$('#sfromDate').focus();
				$('#sfromDate').css("background-color", "#F5E7E1");
				/*$('#stoDate').focus();
				$('#stoDate').css("background-color", "#F5E7E1");
				*/$('#errDateCheckDiv').modal('show');
				 document.getElementById("errDateMsg").innerHTML= resourceJSON.msgJCDnotexceed1month;
				 return false;
			}
		}	
		if(sfromDate=="" && stoDate!=""){
			$('#sfromDate').focus();
			$('#sfromDate').css("background-color", "#F5E7E1");
			/*$('#stoDate').focus();
			$('#stoDate').css("background-color", "#F5E7E1")*/
			$('#errDateCheckDiv').modal('show');
			 document.getElementById("errDateMsg").innerHTML = resourceJSON.msgJCDnotexceed1month;
			 return false;
		}
		
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		if((endfromDate!="" && endtoDate!="")||(endfromDate!="" && endtoDate==""))
		{
			var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
			var endtoDate1;
			var endfromDate1;
			if(endfromDate!="" && endtoDate!=""){
				var  firstDate	= endfromDate.split("-");
				var secondDate 	= endtoDate.split("-");
				
				endfromDate1 = new Date(firstDate[2],firstDate[0],firstDate[1]);
				endtoDate1 = new Date(secondDate[2],secondDate[0],secondDate[1]);
			}else if(endfromDate!="" && endtoDate==""){
				
				endtoDate1 = new Date();
				var  firstDate	= endfromDate.split("-");
				endfromDate1 = new Date(firstDate[2],firstDate[0],firstDate[1]);
			}
			endtoDate1.setHours(0);
			endtoDate1.setMinutes(0);
			endtoDate1.setSeconds(0);
			endtoDate1.setMilliseconds(0);
			endfromDate1.setHours(0);
			endfromDate1.setMinutes(0);
			endfromDate1.setSeconds(0);
			endfromDate1.setMilliseconds(0);
			
			var diffDays = Math.round((endtoDate1.getTime() - endfromDate1.getTime())/(oneDay));
			
			
		//	alert(diffDays)
			
			if(diffDays>30){
				$('#endfromDate').focus();
				$('#endfromDate').css("background-color", "#F5E7E1");
			/*	$('#endtoDate').focus();
				$('#endtoDate').css("background-color", "#F5E7E1");*/
				$('#errDateCheckDiv').modal('show');
				 document.getElementById("errDateMsg").innerHTML=resourceJSON.msgJEDnotexceed1month;
				 return false;
			}
		}	
		if(endfromDate=="" && endtoDate!=""){
			$('#endfromDate').focus();
			$('#endfromDate').css("background-color", "#F5E7E1");
			/*$('#endtoDate').focus();
			$('#endtoDate').css("background-color", "#F5E7E1")*/
			$('#errDateCheckDiv').modal('show');
			 document.getElementById("errDateMsg").innerHTML=  resourceJSON.msgJEDnotexceed1month;
			 return false;
		}
//////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		if((appfromDate!="" && apptoDate!="")||(appfromDate!="" && apptoDate==""))
		{
			var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
			var apptoDate1;
			var appfromDate1;
			if(appfromDate!="" && apptoDate!=""){
				var  firstDate	= appfromDate.split("-");
				var secondDate 	= apptoDate.split("-");
				
				apptoDate1 = new Date(secondDate[2],secondDate[0],secondDate[1]);
				appfromDate1 = new Date(firstDate[2],firstDate[0],firstDate[1]);
			}else if(appfromDate!="" && apptoDate==""){
				var  firstDate	= appfromDate.split("-");
				apptoDate1 = new Date();
				appfromDate1 = new Date(firstDate[2],firstDate[0],firstDate[1]);
			}
			apptoDate1.setHours(0);
			apptoDate1.setMinutes(0);
			apptoDate1.setSeconds(0);
			apptoDate1.setMilliseconds(0);
			appfromDate1.setHours(0);
			appfromDate1.setMinutes(0);
			appfromDate1.setSeconds(0);
			appfromDate1.setMilliseconds(0);
			
			var diffDays = Math.round((apptoDate1.getTime() - appfromDate1.getTime())/(oneDay));
			/*var timeDiff = apptoDate1.getTime() - appfromDate1.getTime();
			var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));*/
			
		//	alert(diffDays)
			if(diffDays>30){
				$('#appsfromDate').focus();
				$('#appsfromDate').css("background-color", "#F5E7E1");
			/*	$('#appstoDate').focus();
				$('#appstoDate').css("background-color", "#F5E7E1");*/
				$('#errDateCheckDiv').modal('show');
				 document.getElementById("errDateMsg").innerHTML  = resourceJSON.msgADnotexceed1month;
				 return false;
			}
		}	
		if(appfromDate=="" && apptoDate!=""){
			$('#appsfromDate').focus();
			$('#appsfromDate').css("background-color", "#F5E7E1");
			/*$('#appstoDate').focus();
			$('#appstoDate').css("background-color", "#F5E7E1")*/
			$('#errDateCheckDiv').modal('show');
			 document.getElementById("errDateMsg").innerHTML=   resourceJSON.msgApplicationDtexccep1month;
			 return false;
		}
		//********************************************************************************************************		
		if((hirefromDate!="" && hiretoDate!="")||(hirefromDate!="" && hiretoDate==""))
		{
			var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
			var hiretoDate1;
			var hirefromDate1;
			if(hirefromDate!="" && hiretoDate!=""){
				var  firstDate	= hirefromDate.split("-");
				var secondDate 	= hiretoDate.split("-");
				
				  hiretoDate1 = new Date(secondDate[2],secondDate[0],secondDate[1]);
				hirefromDate1 = new Date(firstDate[2],firstDate[0],firstDate[1]);
			}else if(hirefromDate!="" && hiretoDate==""){
				var  firstDate	= hirefromDate.split("-");
				hiretoDate1 = new Date();
				hirefromDate1 = new Date(firstDate[2],firstDate[0],firstDate[1]);
			}
			hiretoDate1.setHours(0);
			hiretoDate1.setMinutes(0);
			hiretoDate1.setSeconds(0);
			hiretoDate1.setMilliseconds(0);
			hirefromDate1.setHours(0);
			hirefromDate1.setMinutes(0);
			hirefromDate1.setSeconds(0);
			hirefromDate1.setMilliseconds(0);
			
			
	      var diffDays = Math.round((hiretoDate1.getTime() - hirefromDate1.getTime())/(oneDay));
			
			
			//alert(diffDays)
			
			
			if(diffDays>30){
				$('#hiredfromDate').focus();
				$('#hiredfromDate').css("background-color", "#F5E7E1");
				/*$('#hiredtoDate').focus();
				$('#hiredtoDate').css("background-color", "#F5E7E1");*/
				$('#errDateCheckDiv').modal('show');
				 document.getElementById("errDateMsg").innerHTML=  resourceJSON.msgHiredDateHiredDate;
				 return false;
			}
		}	
		if(hirefromDate=="" && hiretoDate!=""){
			$('#hiredfromDate').focus();
			$('#hiredfromDate').css("background-color", "#F5E7E1");
			/*$('#hiredtoDate').focus();
			$('#hiredtoDate').css("background-color", "#F5E7E1")*/
			$('#errDateCheckDiv').modal('show');
			 document.getElementById("errDateMsg").innerHTML=   resourceJSON.msgHiredDateHiredDate  ;
			 return false;
		}
	}
	
	f_sfromDate     =  sfromDate;
	f_stoDate       =  stoDate;	
	
	f_endfromDate   =  endfromDate;
	f_endtoDate     =  endtoDate;
	
	f_appsfromDate  =  appfromDate;
	f_appstoDate    =  apptoDate;
	
	hiredfromDate   = hirefromDate;
	hiredtoDate     = hiretoDate;
	
	
	internal=document.getElementById("internalcandidate").value
	try{
		var iframeNorm = document.getElementById('ifrmNorm');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('normScoreFrm');
		document.getElementById("normScore").value=inputNormScore.value;
	}catch(e){}	
		 normScoreSelectVal		=	trim(document.getElementById("normScoreSelectVal").value);
		 normScore		=	trim(document.getElementById("normScore").value);
	
	
	$('#loadingDiv').fadeIn();

	
	
	  if(f_sfromDate=='' && f_stoDate=='' && f_endfromDate=='' && f_endtoDate=='' && f_appsfromDate=='' && f_appstoDate=='' &&(normScoreSelectVal=='' || normScoreSelectVal==0) && internal==2)
		{
		  pdateFlag=false
		}else{
			pdateFlag=true
		}
	        
	var JobOrderType     =   document.getElementById("JobOrderType").value;
	  f_JobOrderType     =   JobOrderType;
	var searchTextId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	  f_searchTextId     =  searchTextId
	var schoolId		 =	document.getElementById("schoolId").value;
	  f_schoolId         =  schoolId
	var jobOrderId	     =	trim(document.getElementById("jobOrderId").value);
	  f_jobOrderId		 =   jobOrderId
	var	firstName        =	trim(document.getElementById("firstName").value);
	  f_firstName        =  firstName
	var	lastName         =	trim(document.getElementById("lastName").value);
	  f_lastName 		 =  lastName
	var	emailAddress     =	trim(document.getElementById("emailAddress").value);
	  f_emailAddress 	 =  emailAddress
	var stateId          =  document.getElementById("stateId").value;
	  f_stateId  		 =  stateId
	if(document.getElementById("certType").value==''){
		document.getElementById("certificateTypeMaster").value=0;
	}
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	f_certType 		    =   certType
	var jobCategoryIds='';
    var 	jobCategoryId   = document.getElementById("jobCategoryId")
   
   for (var i = 0; i < jobCategoryId.options.length; i++) {
      if(jobCategoryId.options[i].selected ==true){
    	  if(jobCategoryIds==''){
    		  jobCategoryIds=  jobCategoryId.options[i].value;
    	  }
    	  else{
    		  jobCategoryIds=jobCategoryIds+ ","+jobCategoryId.options[i].value;
    	  }
      }
   } 
    f_jobCategoryIds   =  jobCategoryIds
    //@@@@@@@@@@@ status Names @@@@@@@@@@@@@@ 
 // var statusId=document.getElementById("statusId").value
    var statusId="";
    var 	statusNames   = document.getElementById("statusId")
    		jobstatus	 =	document.getElementById("jobStatus").value;
    		hiddenjob=	document.getElementById("hiddenjob").value;
    //alert("jobStatus-----"+jobstatus)
    for (var i = 0; i < statusNames.options.length; i++) {
       if(statusNames.options[i].selected ==true){
     	  if(statusId==''){
     		 statusId=  statusNames.options[i].value;
     	  }
     	  else{
     		 statusId=statusId+ "#@"+statusNames.options[i].value;
     	  }
       }
    } 
    
   
    f_statusId =statusId
	var status	=	0;
	var jobReqNoId=0;
	var resultFlag=true;		
	
	CandidatejobstatusAjax.displayRecordsByEntityType(resultFlag,JobOrderType,searchTextId,schoolId,"",
			"",jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,firstName,lastName,emailAddress,stateId,certType,jobCategoryIds,statusId,
			f_sfromDate,f_stoDate,f_endfromDate,f_endtoDate,f_appsfromDate,f_appstoDate,pdateFlag,normScoreSelectVal,normScore,internal,hiredfromDate,hiredtoDate,jobstatus,hiddenjob,{ 
		async: true,
		callback: function(data)
		{	
		
		document.getElementById("divMainEEC").innerHTML	=data;
		$('#loadingDiv').hide();
	    applyScrollOnTblEEC();
		},
	errorHandler:handleError
	});
}






function generateCandidateJobStatusxcel()
{
	jobstatus	 =	document.getElementById("jobStatus").value;
	hiddenjob=	document.getElementById("hiddenjob").value;
	$('#loadingDiv').fadeIn();
	
	var status	=	0;
	var jobReqNoId=0;
	var resultFlag=true;		
	
	CandidatejobstatusAjax.displayRecordsByEntityTypeEXL(resultFlag,f_JobOrderType,f_searchTextId,f_schoolId,"",
			"",f_jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,f_firstName,f_lastName,f_emailAddress,f_stateId,f_certType,f_jobCategoryIds,f_statusId,
			f_sfromDate,f_stoDate,f_endfromDate,f_endtoDate,f_appsfromDate,f_appstoDate,pdateFlag,normScoreSelectVal,normScore,internal,hiredfromDate,hiredtoDate,jobstatus,hiddenjob,{ 
		async: true,
		callback: function(data)
		{
		
		    $('#loadingDiv').hide();
			if(deviceType)
			{					
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
					
			}
			else
			{
				try
				{
					document.getElementById('ifrmTrans').src = "candidatestatusreport/"+data+"";
				}
				catch(e)
				{alert(e);}
			}
		},
	errorHandler:handleError
	});

}

function downloadCandidateJobStatusReport()
{
	jobstatus	 =	document.getElementById("jobStatus").value;
	hiddenjob=	document.getElementById("hiddenjob").value;
	$('#loadingDiv').fadeIn();
	
	var status	=	0;
	var jobReqNoId=0;
	var resultFlag=true;		
	
	CandidatejobstatusAjax.downloadCandidateJobStatusReportPDF(resultFlag,f_JobOrderType,f_searchTextId,f_schoolId,"",
			"",f_jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,f_firstName,f_lastName,f_emailAddress,f_stateId,f_certType,f_jobCategoryIds,f_statusId,
			f_sfromDate,f_stoDate,f_endfromDate,f_endtoDate,f_appsfromDate,f_appstoDate,pdateFlag,normScoreSelectVal,normScore,internal,hiredfromDate,hiredtoDate,jobstatus,hiddenjob,{ 
		async: true,
		callback: function(data)
		{		
		$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
		}		
		else
		{
			$('#modalDownloadCandidateJobStatus').modal('hide');
			document.getElementById('ifrmCJS').src = ""+data+"";
			try{
				$('#modalDownloadCandidateJobStatus').modal('show');
			}catch(err)
			{}		
	     }		
	     return false;
}});
	
}
 
function generateCandidateJobStatusPrintPre()
{
	jobstatus	 =	document.getElementById("jobStatus").value;
	hiddenjob=	document.getElementById("hiddenjob").value;
	$('#loadingDiv').fadeIn();
	
	var status	=	0;
	var jobReqNoId=0;
	var resultFlag=true;		
	
	CandidatejobstatusAjax.candidateJobStatusReportPrintPreview(resultFlag,f_JobOrderType,f_searchTextId,f_schoolId,"",
			"",f_jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,f_firstName,f_lastName,f_emailAddress,f_stateId,f_certType,f_jobCategoryIds,f_statusId,
			f_sfromDate,f_stoDate,f_endfromDate,f_endtoDate,f_appsfromDate,f_appstoDate,pdateFlag,normScoreSelectVal,normScore,internal,hiredfromDate,hiredtoDate,jobstatus,hiddenjob,{ 
		async: true,
		callback: function(data)
		{
		//alert("@@ :: "+data)
		$('#loadingDiv').hide();
		$('#pritCandidateJobSatatusDataTableDiv').html(data);
		$("#printCandidateJobSatatusDiv").modal('show');
		applyScrollOnPrintTable();
		  
		},
		errorHandler:handleError
	});
	
} 



function printCandidateJobStatusDATA()
{
	jobstatus	 =	document.getElementById("jobStatus").value;
	hiddenjob=	document.getElementById("hiddenjob").value;
	$('#loadingDiv').show();
	
	var status	=	0;
	var jobReqNoId=0;
	var resultFlag=true;		
	
	
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	CandidatejobstatusAjax.candidateJobStatusReportPrintPreview(resultFlag,f_JobOrderType,f_searchTextId,f_schoolId,"",
			"",f_jobOrderId,status,noOfRows,page,sortOrderStr,sortOrderType,jobReqNoId,f_firstName,f_lastName,f_emailAddress,f_stateId,f_certType,f_jobCategoryIds,f_statusId,
			f_sfromDate,f_stoDate,f_endfromDate,f_endtoDate,f_appsfromDate,f_appstoDate,pdateFlag,normScoreSelectVal,normScore,internal,hiredfromDate,hiredtoDate,jobstatus,hiddenjob,{ 
		async: true,
		callback: function(data)
		{ 
		$('#loadingDiv').hide();
		 if (isSafari && !deviceType)
		    {
		    	window.document.write(data);
				window.print();
		    }else
		    {
		    	var newWindow = window.open();
		    	newWindow.document.write(data);	
		    	newWindow.print();
			 }
	    
		},
	errorHandler:handleError
	});
	
}

 
 function canelPrint()
 {
	 $('#errDateCheckDiv').hide();
	 $('#printCandidateJobSatatusDiv').hide();
	 $('#loadingDiv').hide();
		
 }

function chkschoolBydistrict()
{
	var districtid = trim(document.getElementById('districtORSchoolName').value);
	if(trim(document.getElementById('entityType').value)==1)
	if(districtid=="")
	{
		document.getElementById('schoolName').disabled=true;
		$("#statusId").prop('disabled', true);
		$("#statusId").html("<option value='0'>"+resourceJSON.optAll+" </option>");
		
		$("#jobCategoryId").prop('disabled', true);
		$("#jobCategoryId").html("option value='0'>"+resourceJSON.optAll+" </option>");
	}
	else
	{
		document.getElementById('schoolName').disabled=false;
		$("#statusId").prop('disabled', false);
		$("#jobCategoryId").prop('disabled', false);
		
	}
}


function getCertificationList()
{
	var districtOrSchooHiddenlId=document.getElementById("districtOrSchooHiddenlId").value;
	
	CandidatejobstatusAjax.getCertificationList(districtOrSchooHiddenlId,{ 
		async: false,		
		callback: function(data)
		{
			if(data!=null && data!="")
			{	
				$("#jobCategoryId").prop('disabled', false);
				$("#jobCategoryId").html(data);
			}
			
		}
	});
}

function getStataussList()
{
	var districtOrSchooHiddenlId=document.getElementById("districtOrSchooHiddenlId").value;
	
	CandidatejobstatusAjax.getsecStatusList(districtOrSchooHiddenlId,{ 
		async: false,		
		callback: function(data)
		{
			if(data!=null && data!="")
			{			
				$("#statusId").html(data);
				$("#statusId").prop('disabled', false);
				
			}
			else{
				$("#statusId").html("");
				$("#statusId").prop('disabled', true);
			}
			
		}
	});
}

function displayAdvanceSearch()
 {
	$('#searchLinkDiv').hide();
	$('#hidesearchLinkDiv').show();
	$('#advanceSearchDiv').slideDown('slow');
 }

function hideAdvanceSearch()
{
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$('#advanceSearchDiv').slideUp('slow');
	//$('#advanceSearchDiv').toggle( "slide", { direction: "top" }, 1000);
}




