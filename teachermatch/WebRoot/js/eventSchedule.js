var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr="";
var domainsortOrderType="";

function getPaging(pageno)
{
	if(pageno!='') {
		page=pageno;	
	} else {
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	getCandidateSlotBySccheduleId(document.getElementById('scheduleIdForView').value);
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
  if(pageno!=''){
		page=pageno;
		//alert("page :: "+page);
	}else{
		page=1;
		//alert("default");
	}
	sortOrderStr=sortOrder;
	//alert("sortOrderStr :: "+sortOrderStr);
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		//alert("AA")
		noOfRows = document.getElementById("pageSize").value;
	}else{
	//	alert("BB");
		noOfRows=10;
	}
	getCandidateSlotBySccheduleId(document.getElementById('scheduleIdForView').value);
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function addSchedule()
{
	$('#errordiv').empty();
//	clrSchedule();
	$('#eventTypeWiseSchedule').show();
	
//	$('#addSchedulediv').show();	
//	checkType();
	
//	alert("01");
	
	var eventId = $("#eventId").val().trim();
	
//	alert(" event id :: "+eventId);
	
	var inputDivCount = $("#inputDivCount").val();
	
	var chkDATFlag = trim(document.getElementById("chkDATFlag").value);

	EventScheduleAjax.createAddScheduleDiv(eventId,inputDivCount,{
		async: true,
		callback: function(data)
		{
//			alert(data);
			$("#eventTypeWiseSchedule").append(data);
			if(chkDATFlag==1)
				$("#inputDivCount").val(1);
			else
				$("#inputDivCount").val(parseInt(inputDivCount)+1);
		},
		errorHandler:handleError	
	});
	
}

function cancelSchedule()
{
	$('#addSchedulediv').hide();	
}

function saveSchedule(cont)
{
	$('#errordiv').empty();	
	var counter=0;
	
	var inputDivCount = trim(document.getElementById("inputDivCount").value);

	var eventId = trim(document.getElementById("eventId").value);
	var chkDATFlag = trim(document.getElementById("chkDATFlag").value);
	var inttype	= trim(document.getElementById("inttype").value);
	var validfordays	= trim(document.getElementById("validfordays").value);
	var locationUrl	= trim(document.getElementById("locationUrl").value);
			
	var singlebookingonly=false;
	if(document.getElementById("singlebookingonly")!=null){
		if(document.getElementById("singlebookingonly").checked){
			singlebookingonly=true
		}
	}
	var noOfCandiadte="";
	try{noOfCandiadte=document.getElementById("noofcandidateperslot").value;}catch(e){}

	if(inttype==1)
	{
		var scheduledelId = trim(document.getElementById("scheduledelId").value);
		
		if (validfordays == null || validfordays == '') {

			$('#errordiv').append("&#149; "+resourceJSON.msgPleaseEnterValidDays+"<br>");
			$('#errordiv').show();
			if (counter == 0) {
				$('#validfordays').focus();
			}
			counter = counter + 1;
		} else {
			var a = isNaN(validfordays);
			if (a) {

				$('#errordiv').append(
						"&#149; "+resourceJSON.msgPleaseEnterOnlyNumericValues+"<br>");
				$('#errordiv').show();
				if (counter == 0) {
					$('#validfordays').focus();
				}
				counter = counter + 1;
			}
		}
		if(counter==0)
		{
			EventScheduleAjax.saveSchedule(scheduledelId,eventId,"","","","","",validfordays,
			{
				async: true,
				callback: function(data)
				{
					//displayEventSchedule();
				
					if(cont==0)
						window.location.href="manageevents.do";
					else if(cont==1)
						window.location.href="manageparticipant.do?eventId="+eventId;
				},
				errorHandler:handleError	
			});
			
		}
	}
	else
	{
		var timezone = trim(document.getElementById("timezoneforevent").value);
		var scheduleIdArr = "";
		var eventStartDateArr = "";
		var starthrArr = ""; 
		var starttimeformArr = "";
		var endhrArr = ""; 
		var endtimeformArr = "";
		var locationArr = "";
		var currentDate = new Date();
		var countRecord = 0;
		
		if(chkDATFlag=='1')
			inputDivCount=1;
		
		
			for(var i=0;i<inputDivCount;i++)
			{
				if(i==0){
					eventStartDateArr 	= eventStartDateArr+trim(document.getElementById("eventStartDate").value)+",";
					starthrArr 			= starthrArr+trim(document.getElementById("starthr").value)+",";
					starttimeformArr 	= starttimeformArr+trim(document.getElementById("starttimeform").value)+",";
					endhrArr	 		= endhrArr+trim(document.getElementById("endhr").value)+",";
					endtimeformArr 		= endtimeformArr+trim(document.getElementById("endtimeform").value)+",";
					
					if(trim(document.getElementById("locationfirst").value)=="")
						locationArr 		= locationArr+null+"##";
					else
						locationArr 		= locationArr+trim(document.getElementById("locationfirst").value)+"##";
				
					scheduleIdArr		= scheduleIdArr+trim(document.getElementById("eventScheduleId").value)+",";
				}
				else{
					eventStartDateArr = eventStartDateArr+trim(document.getElementById("eventStartDate"+i).value)+",";
					starthrArr 			= starthrArr+trim(document.getElementById("starthr"+i).value)+",";
					starttimeformArr 	= starttimeformArr+trim(document.getElementById("starttimeform"+i).value)+",";
					endhrArr	 		= endhrArr+trim(document.getElementById("endhr"+i).value)+",";
					endtimeformArr 		= endtimeformArr+trim(document.getElementById("endtimeform"+i).value)+",";
					
					if(trim(document.getElementById("location"+i).value)=="")
						locationArr 		= locationArr+null+"##";
					else
						locationArr 		= locationArr+trim(document.getElementById("location"+i).value)+"##";
				    
					scheduleIdArr		= scheduleIdArr+trim(document.getElementById("eventScheduleId"+i).value)+",";
				}
			}
			
			
			clearAllFieldsColor();
			
			//=============== Error Msg=========================		
				
				if (timezone == '') {
					if (counter == 0) {
						$('#timezoneforevent').focus();
					}
					$('#errordiv').append("&#149; "+resourceJSON.msgTimeZone1+"<br>");
					$('#timezoneforevent').css("background-color", "#F5E7E1");
					$('#errordiv').show();
					counter = counter + 1;
				}
			
				var eSdArrFlag = false;
				var comStartWithCurrentDateFlag = false;
				var comStartAndEndTimeFlag = false;
				var eShrArrFlag = false; 
				var eStimeformArrFlag = false;
				var eEhrArrFlag = false; 
				var eEtimeformArrFlag = false;
				
				for(var i=0;i<inputDivCount;i++)
				{
					var eSdArr = eventStartDateArr.split(",");
					var eShrArr = starthrArr.split(","); 
					var eStimeformArr = starttimeformArr.split(",");
					var eEhrArr = endhrArr.split(","); 
					var eEtimeformArr = endtimeformArr.split(",");
					var chkOneRow = false;
					
					if(i==0)
					{
						if(eSdArr[0]=='' && eShrArr[0]=='' && eEhrArr[0]=='')
			             {
							if(inputDivCount==1)
								chkOneRow=true;
			             }
						else
							chkOneRow=true;
						
						if(chkOneRow)
						{
							var startTimeDate = new Date();
							var endTimeDate = new Date();
							var startTimeHHMM = "";
							var endTimeHHMM = "";
							
							var retValue = true;
			                var stdate=eSdArr[0].split('-');
			                var dstart = new Date();
			                dstart.setFullYear(stdate[2]);
			                dstart.setMonth(stdate[0]-1);
			                dstart.setDate(stdate[1]);
			                var curdate=new Date();
							
							if (eSdArr[0] == '') {
								$('#eventStartDate').css("background-color", "#F5E7E1");
								$('#eventStartDate').focus();
								eSdArrFlag=true;
							}
							else if(curdate>dstart)
							{
								$('#eventStartDate').css("background-color", "#F5E7E1");
								$('#eventStartDate').focus();
								comStartWithCurrentDateFlag=true;
							}
								
							
							if (eShrArr[0] == '') {
								$('#starthr').focus();
								$('#starthr').css("background-color", "#F5E7E1");
								eShrArrFlag=true;
							}
							
							if (eStimeformArr[0] == '') {
								$('#starttimeform').focus();
								$('#starttimeform').css("background-color", "#F5E7E1");
								eStimeformArrFlag=true;
							}
							
							if (eEhrArr[0] == '') {
								$('#endhr').focus();
								$('#endhr').css("background-color", "#F5E7E1");
								eEhrArrFlag=true;
							}
							
							if (eEtimeformArr[0] == '') {
								$('#endtimeform').focus();
								$('#endtimeform').css("background-color", "#F5E7E1");
								eEtimeformArrFlag=true;
							}
							
							if(eShrArr[0]!='' && eStimeformArr[0]!='' && eEhrArr[0]!='' && eEtimeformArr[0]!='')
							{
								startTimeHHMM = eShrArr[0].split(":");
								endTimeHHMM = eEhrArr[0].split(":");
								if(eStimeformArr[0]=='PM')
								{
									startTimeHHMM[0] = parseInt(startTimeHHMM[0])+12;
								}
								if(eEtimeformArr[0]=='PM')
								{
									endTimeHHMM[0] = parseInt(endTimeHHMM[0])+12;
								}
								
								startTimeDate.setHours(startTimeHHMM[0], startTimeHHMM[1], '00');
								
								endTimeDate.setHours(endTimeHHMM[0], endTimeHHMM[1], '00');
								
								/*if(startTimeDate > endTimeDate)
								 {
									$('#starthr').focus();
									$('#starthr').css("background-color", "#F5E7E1");
									$('#starttimeform').focus();
									$('#starttimeform').css("background-color", "#F5E7E1");
									$('#endhr').focus();
									$('#endhr').css("background-color", "#F5E7E1");
									$('#endtimeform').focus();
									$('#endtimeform').css("background-color", "#F5E7E1");
									comStartAndEndTimeFlag=true;
								 }*/
							}
							
							countRecord = countRecord+1;
						}
						
					}
					else
					{
						/*if(i<2)
						{*/
						
						 if(eSdArr[i]=='' && eShrArr[i]=='' && eEhrArr[i]=='')
			             {
							 
			             }else
			             {
			            	 var startTimeDate = new Date();
								var endTimeDate = new Date();
								var startTimeHHMM = "";
								var endTimeHHMM = "";
								
								var retValue = true;
				                var stdate=eSdArr[i].split('-');
				                var dstart = new Date();
				                dstart.setFullYear(stdate[2]);
				                dstart.setMonth(stdate[0]-1);
				                dstart.setDate(stdate[1]);
				                var curdate=new Date();
								
								
								
								if (eSdArr[i] == '') {
									$('#eventStartDate'+i).css("background-color", "#F5E7E1");
									$('#eventStartDate'+i).focus();
									eSdArrFlag=true;
								}else if(curdate>dstart)
								{
									$('#eventStartDate'+i).css("background-color", "#F5E7E1");
									$('#eventStartDate'+i).focus();
									comStartWithCurrentDateFlag=true;
								}
								
								if (eShrArr[i] == '') {
									$('#starthr'+i).focus();
									$('#starthr'+i).css("background-color", "#F5E7E1");
									eShrArrFlag=true;
								}
								
								if (eStimeformArr[i] == '') {
									$('#starttimeform'+i).focus();
									$('#starttimeform'+i).css("background-color", "#F5E7E1");
									eStimeformArrFlag=true;
								}
								
								
								if (eEhrArr[i] == '') {
									$('#endhr'+i).focus();
									$('#endhr'+i).css("background-color", "#F5E7E1");
									eEhrArrFlag=true;
								}
								
								if (eEtimeformArr[i] == '') {
									$('#endtimeform'+i).focus();
									$('#endtimeform'+i).css("background-color", "#F5E7E1");
									eEtimeformArrFlag=true;
								}
								
								if(eShrArr[i]!='' && eStimeformArr[i]!='' && eEhrArr[i]!='' && eEtimeformArr[i]!='')
								{
									startTimeHHMM = eShrArr[i].split(":");
									endTimeHHMM = eEhrArr[i].split(":");
									if(eStimeformArr[i]=='PM')
									{
										startTimeHHMM[i] = parseInt(startTimeHHMM[i])+12;
									}
									if(eEtimeformArr[i]=='PM')
									{
										endTimeHHMM[i] = parseInt(endTimeHHMM[i])+12;
									}
									
									startTimeDate.setHours(startTimeHHMM[0], startTimeHHMM[1], '00');
									
									endTimeDate.setHours(endTimeHHMM[0], endTimeHHMM[1], '00');
									
									/*if(startTimeDate > endTimeDate)
									 {
										$('#starthr'+i).focus();
										$('#starthr'+i).css("background-color", "#F5E7E1");
										$('#starttimeform'+i).focus();
										$('#starttimeform'+i).css("background-color", "#F5E7E1");
										$('#endhr'+i).focus();
										$('#endhr'+i).css("background-color", "#F5E7E1");
										$('#endtimeform'+i).focus();
										$('#endtimeform'+i).css("background-color", "#F5E7E1");
										comStartAndEndTimeFlag=true;
									 }*/
								}
								
								countRecord =countRecord+1;
			             }
					}
				}
				
				
				if(chkDATFlag==3)
				{
					if(countRecord<2)
					{
						$('#errordiv').append("&#149; "+resourceJSON.msgschedulesAtLeast2+"<br>");
						$('#errordiv').show();
						counter = counter + 1;
					}
				}
				
				if(chkDATFlag==2)
				{
					if(countRecord<1)
					{
						$('#errordiv').append("&#149; "+resourceJSON.msgschedulesAtLeast1+"<br>");
						$('#errordiv').show();
						counter = counter + 1;
					}
				}
				
				
				
				if(eSdArrFlag)
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgPlzenterDate+"<br>");
					$('#errordiv').show();
					counter = counter + 1;
				}
				if(comStartWithCurrentDateFlag)
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgStDatelessCurrDate+"<br>");
					$('#errordiv').show();
					counter = counter + 1;
				}
				if(eShrArrFlag)
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgPlzenterStTime+"<br>");
					$('#errordiv').show();
					counter = counter + 1;
				}
						
				if(eStimeformArrFlag) {
					$('#errordiv').append("&#149; "+resourceJSON.msgStartTimeFormat+"<br>");
					$('#errordiv').show();
					counter = counter + 1;
				}
				
				if(eEhrArrFlag){
					$('#errordiv').append("&#149; "+resourceJSON.msgEndTime1+"<br>");
					$('#errordiv').show();
					counter = counter + 1;
				}
				
				if(eEtimeformArrFlag){
					$('#errordiv').append("&#149; "+resourceJSON.msgEndTimeFormat+"<br>");
					$('#errordiv').show();
					counter = counter + 1;
				}
				
				if(comStartAndEndTimeFlag)
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgEndTimeLessStTime+"<br>");
					$('#errordiv').show();
					counter = counter + 1;
				}
		//================================================
		
		
		try{
			if(counter==0)
			{
				EventScheduleAjax.saveSchedule(scheduleIdArr,eventId,timezone,eventStartDateArr,starthrArr,starttimeformArr,locationArr,null,endhrArr,endtimeformArr,singlebookingonly,noOfCandiadte,
				{
							async: true,
							callback: function(data)
							{
								$("#eventTypeWiseSchedule").append("");
									
								if(cont==0)
									window.location.href="manageevents.do";
								else if(cont==1)
									window.location.href="managefacilitators.do?eventId="+eventId;
							},
							errorHandler:handleError	
						});
			}
			}catch(e){alert(e)}
	}
}

function displayEventSchedule()
{
	//alert("displayEventSchedule");
	var eventId = document.getElementById('eventId').value;
	var inttype	= trim(document.getElementById("inttype").value);
	
//	alert(" inttype "+inttype);
	if(inttype==1)
	{
		$("#valdays").show();
		$("#addScheduleLink").hide();
		$("#addScheduleLinkForSlots").hide();
		EventScheduleAjax.getVVIDays(eventId,{
			async: true,
			callback: function(data)
			{
				$("#valdays").show();
				
				if(data!=null)
				{
					if(data[0]==0)
						document.getElementById("validfordays").value="";
					else
						document.getElementById("validfordays").value=data[0];
					
					document.getElementById("scheduledelId").value=data[1];
				}
				//$('#scheduleGrid').html(data);
				//applyScrollOnTblSchedule();
			//	$('#eventTypeWiseSchedule').show();
			//	$('#eventTypeWiseSchedule').html();
			//	$('#eventTypeWiseSchedule').html(data[0]);
			//	$('#inputDivCount').val(data[1]);
			},
			errorHandler:handleError
		});
		
	}
	else
	{
		$("#valdays").hide();
		var chkDATFlag = document.getElementById('chkDATFlag').value;
		if(chkDATFlag!=1)
		{
			if(chkDATFlag==2)
			{
				$("#addScheduleLink").show();
				$("#addScheduleLinkForSlots").hide();
			}
			else if(chkDATFlag==3)
			{
				$("#addScheduleLink").hide();
				$("#addScheduleLinkForSlots").show();
			}
		}
		else
		{
			$("#addScheduleLink").hide();
			$("#addScheduleLinkForSlots").hide();
		}
			
			
		$("#loadingDiv").show();
		
		
		try{
		
			EventScheduleAjax.geteventSchedule(eventId,noOfRows,page,sortOrderStr,sortOrderType,{
				async: true,
				callback: function(data)
				{
					//$('#scheduleGrid').html(data);
					//applyScrollOnTblSchedule();
					$('#eventTypeWiseSchedule').show();
				//	$('#eventTypeWiseSchedule').html();
					$('#eventTypeWiseSchedule').html(data[0]);
					$('#inputDivCount').val(data[1]);

					$("#loadingDiv").hide();
					$('#noofcandidate').tooltip();
				},
				errorHandler:handleError
			});
			
			/*EventScheduleAjax.geteventSchedule(eventId,noOfRows,page,sortOrderStr,sortOrderType,{
			async: true,
			callback: function(data)
			{
				//$('#scheduleGrid').html(data);
				//applyScrollOnTblSchedule();
				$("#loadingDiv").hide();
			},
			errorHandler:handleError
		});*/
		}catch(e){alert(e);}
		$("#loadingDiv").hide();	
	}
	
}
function deleteSchedule(scheduleId)
{
	document.getElementById("scheduledelId").value=scheduleId;
	$('#myModalactMsgShow').modal('show');
	
}

function removeScheduleById()
{var scheduleId=document.getElementById("scheduledelId").value;
	$("#loadingDiv").fadeIn();	
	EventScheduleAjax.deleteEventScheduleById(scheduleId,
	{
		async: true,
		callback: function(data)
		{
		hideMessageDiv();
		displayEventSchedule();
		$("#loadingDiv").hide();
			
		},
		errorHandler:handleError	
	}		
	);	
}

function hideMessageDiv()
{
	$('#myModalactMsgShow').modal('hide');	
}


function showEditSchedule(scheduleId)
{$("#loadingDiv").show();
var inttype = document.getElementById('inttype').value;
EventScheduleAjax.getScheduleById(scheduleId,
{
	async: true,
	callback: function(data)
	{$("#loadingDiv").hide();
	 var editdata=data.split('||||');
	 document.getElementById('scheduleId').value=scheduleId;
	 document.getElementById('eventStartDate').value=editdata[0];
	 document.getElementById('eventEndDate').value=editdata[0];
	 
		 if(inttype!=1)
		 {
			 document.getElementById("starthr1").value=editdata[1];	
			 document.getElementById("startmin1").value=editdata[2];
			 document.getElementById("starttimeform1").value=editdata[3];
			 document.getElementById("endhr1").value=editdata[4];
			 document.getElementById("endmin1").value=editdata[5];
			 document.getElementById("endtimeform1").value=editdata[6];
			 document.getElementById("location1").value=editdata[7];
			 document.getElementById("timezoneforevent").value=editdata[8];
		 }
		 else
		 {
		 	 document.getElementById("validfordays").value=editdata[1];		 
		 }
	 
	 $('#errordiv').empty();
	 $('#addSchedulediv').show();
	 checkType();
	$('#addSchedulediv').show();
	checkType();
	},
	errorHandler:handleError	
}		
);
}

function clrSchedule()
{
	 document.getElementById('scheduleId').value="";
	 document.getElementById('eventStartDate').value="";
	 document.getElementById('eventEndDate').value="";
	// document.getElementById('validfordays').value="";
	 document.getElementById('timezoneforevent').value="";
	 for(var i=1;i<=8;i++)
		{
	 document.getElementById("starthr"+i).value="";	
	document.getElementById("startmin"+i).value="";
	document.getElementById("starttimeform"+i).value="";
	 document.getElementById("endhr"+i).value="";
	// document.getElementById("endmin"+i).value="";
	 document.getElementById("endtimeform"+i).value="";
	 document.getElementById("location"+i).value="";	
		}
		}

function checkType()
{
var inttype = document.getElementById('inttype').value;
if(inttype=="1")
{
$("#evtimeslot").hide();	
$("#seldays").hide();
$("#scheduleStartEndDate").hide();
$("#valdays").show();	
}
else
{
$("#evtimeslot").show();	
$("#seldays").show();
$("#valdays").hide();	
}	
}
function exitSchedule()
{
	window.location.href="manageevents.do";			
}
function continueSchedule()
{/*
	
var eventId = document.getElementById('eventId').value;

var formatId = document.getElementById('format').value;
var location="";

if(formatId==1 ){
	var scheduleIdM= document.getElementById("scheduleIdM").value;
	var validfordays = document.getElementById('validfordays').value;
	
	var checkFlag = isNaN(validfordays);
     
     if (!checkFlag) {
	    if(((scheduleIdM!=0 || scheduleIdM!="") &&  validfordays>0) || validfordays>0 ){
	 			EventScheduleAjax.saveVirtualVideoSchedule(scheduleIdM,eventId,location,validfordays,{
	 			async: true,
	 			callback: function(data)
	 			{	},
	 			
	 		errorHandler:handleError
	 		});
	 	}else {
	 		//alert("else")
	 	}
     }
     else {
         	
 		 $('#errDaysDiv').append("Please enter integer value");
         $("#errDaysDiv").show();
         $('#validfordays').focus();
         document.getElementById("validfordays").value="";
         return false;
     }
	
	
	
}else{
	if((document.getElementById('eventStartDate').value!='') || document.getElementById('eventEndDate').value!=''){
		saveSchedule(0);
	}
}

if(formatId==1)
{
window.location.href="manageparticipant.do?eventId="+eventId;	
}else
{	
window.location.href="managefacilitators.do?eventId="+eventId;	
}
*/}

//mukesh fresh code start

function checkvideoSchedule(eventId)
 {
//	alert("0000000000000000444444444444");
	addSchedule();
	$('#loadingDiv').fadeIn();
	EventScheduleAjax.checkScheduleByEventId(eventId,{
		async: true,
		callback: function(data)
		{	
		if(data.split("@@@")[0]==1){
			$('#addScheduleLink').hide();
			document.getElementById("validfordays").value=data.split("@@@")[1];
			document.getElementById("scheduleIdM").value=data.split("@@@")[2];
			
		}else{
			$('#valdays').show();
			
		}
			$('#loadingDiv').hide();
			//$('#pritOfferReadyDataTableDiv').html(data);
		//	$("#printOfferReadyDiv").modal('show');
		},
	errorHandler:handleError
	});
	$('#addScheduleLink').hide();
	
 }

function checkForInt1(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
	return( (charCode >= 48 && charCode <= 57)||(charCode==13) || (charCode==44));
}
function viewCandidates(scheduleId)
{
	page=1;
	getCandidateSlotBySccheduleId(scheduleId);
}

function getCandidateSlotBySccheduleId(scheduleId)
{
	document.getElementById('scheduleIdForView').value='';
	document.getElementById('scheduleIdForView').value=scheduleId;
	$('#loadingDiv').fadeIn();
	EventScheduleAjax.getCandidateSlotBySccheduleId(scheduleId, noOfRows, page, sortOrderStr, sortOrderType,{
		async: true,
		callback: function(data)
		{	
		   $('#scheduleSelectedcandidate').modal('show');
		   $('#scheduleSelectedcandidateDiv').html(data);
		   applyScrollOnTblCanSchedele();
	       $('#loadingDiv').hide();
		},
	errorHandler:handleError
	});
}


function hidediv()
{
	$('#scheduleSelectedcandidate').hide();
}


function getTimeZone()
{
	$('#loadingDiv').fadeIn();
	EventScheduleAjax.getTimeZone({
		async: true,
		callback: function(data)
		{	
		   $('#timezone').html(data);
	       $('#loadingDiv').hide();
		},
	errorHandler:handleError
	});
}

function trim(s) {
	while ((s.substring(0, 1) == ' ') || (s.substring(0, 1) == '\n')
			|| (s.substring(0, 1) == '\r')) {
		s = s.substring(1, s.length);
	}
	while ((s.substring(s.length - 1, s.length) == ' ')
			|| (s.substring(s.length - 1, s.length) == '\n')
			|| (s.substring(s.length - 1, s.length) == '\r')) {
		s = s.substring(0, s.length - 1);
	}
	return s;
}

function clearAllFieldsColor()
{
	
	var inputDivCount = $('#inputDivCount').val();
	
	$('#timezoneforevent').css("background-color", "");
	
	for(var i=0;i<inputDivCount;i++)
	{
		if(i==0){
			$('#eventStartDate').css("background-color", "");
			$('#starthr').css("background-color", "");
	//		$('#startmin').css("background-color", "");
			$('#starttimeform').css("background-color", "");
		//	$('#eventEndDate').css("background-color", "");
			$('#endhr').css("background-color", "");
		//	$('#endtmin').css("background-color", "");
			$('#endtimeform').css("background-color", "");
		}
		else{
			$('#eventStartDate'+i).css("background-color", "");
			$('#starthr'+i).css("background-color", "");
		//	$('#startmin'+i).css("background-color", "");
			$('#starttimeform'+i).css("background-color", "");
		//	$('#eventEndDate'+i).css("background-color", "");
			$('#endhr'+i).css("background-color", "");
		//	$('#endtmin'+i).css("background-color", "");
			$('#endtimeform'+i).css("background-color", "");
		}
	}
}

function clearAllFieldsValues()
{
	var inputDivCount = $('#inputDivCount').val();
	
	for(var i=0;i<inputDivCount;i++)
	{
		if(i==0){
			$('#eventStartDate').val("");
			$('#starthr').val("");
		//	$('#startmin').val("");
			$('#starttimeform').val("");
		//	$('#eventEndDate').val("");
			$('#endhr').val("");
		//	$('#endtmin').val("");
			$('#endtimeform').val("");
		}
		else{
			$('#eventStartDate'+i).val("");
			$('#starthr'+i).val("");
		//	$('#startmin'+i).val("");
			$('#starttimeform'+i).val("");
		//	$('#eventEndDate'+i).val("");
			$('#endhr'+i).val("");
		//	$('#endtmin'+i).val("");
			$('#endtimeform'+i).val("");
		}
	}
}

function addScheduleSlot()
{
	/*$('#errordiv').empty();
	
	var eventId = $("#eventId").val().trim();
	var inputDivCount = $("#inputDivCount").val();
	var chkDATFlag = trim(document.getElementById("chkDATFlag").value);
	//alert("01");
	EventScheduleAjax.createScheduleSlots(eventId,inputDivCount,{
		async: true,
		callback: function(data)
		{
			$('#eventTypeWiseSchedule').show();
			$("#eventTypeWiseSchedule").append(data);
			$("#inputDivCount").val(parseInt(inputDivCount)+5);
		},
		errorHandler:handleError	
	});*/
}
function deleteSchedulePopUp(scheduleId)
{
	document.getElementById("deleteScheduleId").value=scheduleId;
	$('#message2showConfirm').html("Do you really want to remove this slot?");
	$('#footerbtn').html("<button class='btn btn-primary' onclick=\"deleteScheduleByIdOnConfirmed()\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#myModal3').modal('show');
	
}

function deleteScheduleByIdOnConfirmed()
{
	var scheduleId=document.getElementById("deleteScheduleId").value;
	$("#loadingDiv").fadeIn();	
	EventScheduleAjax.deleteEventScheduleById(scheduleId,{
		async: false,
		callback: function(data)
		{
			$("#loadingDiv").fadeOut();	
			displayEventSchedule();
			//html(""+resourceJSON.MsgMailSentSuccessfully+"");
			$('#message2showConfirm').html("Slot has removed successfully");
			$('#footerbtn').html("<button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#myModal3').modal('show');
			
		},
		errorHandler:handleError	
	});	
}

function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkforSingleBooking(){
	if(document.getElementById("singlebookingonly").checked==true){
		$("#noofcandidateDiv").hide();
		document.getElementById("noofcandidateperslot").value='';
	}else{
		$("#noofcandidateDiv").show();
		document.getElementById("noofcandidateperslot").value='';
	}
}

function addCandidateSchedulePopUp(scheduleId)
{
	$('#message2Error').empty();
	document.getElementById("deleteScheduleId").value=scheduleId;
	$("#loadingDiv").fadeIn();	
	EventScheduleAjax.getCandidateByEvent(scheduleId,{
		async: false,
		callback: function(data)
		{
			$("#loadingDiv").fadeOut();	
			$('#message21showConfirm').html(data);
			$('#footerbtn2').html("<button class='btn btn-primary' onclick=\"addCandidateToSlot()\" >Add</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
			$('#myModal4').modal('show');
		},
		errorHandler:handleError	
	});
}


function addCandidateToSlot()
{
	$('#message2Error').empty();
	var scheduleId=document.getElementById("deleteScheduleId").value;
	var participantId=document.getElementById("participantId").value;
	
		if(participantId==0){
			$('#message2Error').append("&#149; Please Select Participant<br>");
			$('#message2Error').show();
		}else{
			$("#loadingDiv").fadeIn();
			EventScheduleAjax.addCandidateToSlot(scheduleId,participantId,{
				async: false,
				callback: function(data)
				{
					$("#loadingDiv").fadeOut();
					var errmsg="";
					 if(data==0){
						$('#message2Error').append("&#149; This Participant already booked the slot of this event.<br>");
						$('#message2Error').show();
					 }else if(data==1){
						 $('#message2Error').append("&#149; Please choose another Slot, this Slot is already booked.<br>");
						 $('#message2Error').show();
					 }else if(data==2){
						 
					 }else{
						    $('#message2showConfirm').html("Participant added successfully");
						    $('#footerbtn').html("<button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
						    $('#myModal3').modal('show');
						    $('#myModal4').modal('hide');
						    displayEventSchedule();
					 }
				},
				errorHandler:handleError	
			});
		}
}
