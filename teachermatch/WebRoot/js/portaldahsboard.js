
function checkSession(responseText)
{
	if(responseText=='100001')
	{
		alert(resourceJSON.oops)
		window.location.href="signin.do";
	}	
}
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}

function getPortalMilestones()
{
	
	createXMLHttpRequest();  
	queryString = "getportalmilestones.do?dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				document.getElementById("divMilestones").innerHTML=xmlHttp.responseText.toString();
				portalTPEnable();
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	return false;
}
function getPortalJobsofIntrest()
{
	createXMLHttpRequest();  
	queryString = "getportaljobsofintrest.do?dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				document.getElementById("divJbInterest").innerHTML=xmlHttp.responseText.toString();
				portalTPEnable();
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	return false;
}

function portalTPEnable()
{
	$('#tpShare1').tooltip();
	$('#tpShare2').tooltip();
	$('#tpShare3').tooltip();
	$('#tpShare4').tooltip();

}
