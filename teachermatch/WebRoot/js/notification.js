var statusShortNames=new Array("hird","rem","widrw","soth","apl");
var demoNames=new Array("sch","can","doth");

function validatenotification()
{
		
	var checked_chkcstatus="";
	var inputs = document.getElementsByName("chkcstatus");
	for (var i = 0; i < inputs.length; i++) {
		if (inputs[i].type === 'checkbox') {
			if(inputs[i].checked){
				checked_chkcstatus+=inputs[i].value+",";
	        }
	    }
	} 
	
	var checked_chkdemonoti="";
	var inputs_demo = document.getElementsByName("chkdemonoti"); 
	for (var i = 0; i < inputs_demo.length; i++) {
		if (inputs_demo[i].type === 'checkbox') {
			if(inputs_demo[i].checked){
				checked_chkdemonoti+=inputs_demo[i].value+",";
	        }
	    }
	}
	UserEmailNotificationsAjax.saveOrUpdateNotification(statusShortNames,demoNames,checked_chkcstatus,checked_chkdemonoti,{ 
		async: true,
		callback: function(data)
		{
		$('#myModal_notofication').modal('show');
		if(data=='1')
			document.getElementById("blockMessage").innerHTML=resourceJSON.MsgNotificationAddedSuccessfully;
		else
			document.getElementById("blockMessage").innerHTML=resourceJSON.MsgNotificationNotAdded;
			
		},
	});
	
	return true;
	
}

function loadnotificationData()
{
	UserEmailNotificationsAjax.displayNotificationData(statusShortNames,{ 
		async: true,
		callback: function(data)
		{
			$('#notificatioData').html(data);
		},
	});
	return true;
}