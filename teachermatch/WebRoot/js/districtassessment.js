var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	getAssessmentGrid();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
		var pageIdNo	=	document.getElementById("pageIdNo").value;
	if(pageIdNo==1)
		getAssessmentGrid();
	else
		getAssessmentSectionGrid();
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function init() {
	  dwr.engine.setActiveReverseAjax(true);
}
/*======= Show AssessmentGrid on Press Enter Key ========= */
function chkForEnterGetAssessmentGrid(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		getAssessmentGrid();
	}	
}
/*======= Save Assessment on Press Enter Key ========= */
function chkForEnterSaveAssessment(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	//alert(evt.srcElement.id);
	/*if((charCode==13) && (evt.srcElement.id!='assessmentDescription'))
	{
		saveAssessment();
	}	*/
	if((charCode==13) && (evt.srcElement.className!='jqte_editor'))
	{
		saveAssessment();
	}
}
/*======= Save AssessmentSection on Press Enter Key ========= */
function chkForEnterSaveAssessmentSection(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;

	//alert(evt.srcElement.className);

	//alert(evt.srcElement.id);
	/*if((charCode==13) && (evt.srcElement.id!='sectionDescription' ))
	{
		if(evt.srcElement.id!='sectionInstructions')
		saveAssessmentSection();
	}	*/

	if((charCode==13))
	{
		if(evt.srcElement.className!='jqte_editor')
			saveAssessmentSection();
	}	
}

/*======= Save AssessmentSection Question on Press Enter Key ========= */
function chkForEnterSaveAssessmentQuestion(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	//alert(evt.srcElement.id);
	/*if((charCode==13) && (evt.srcElement.id!='question' ))
	{
		if(evt.srcElement.id!='questionInstruction')
			$('#saveQues').click();
	}	*/
	if((charCode==13))
	{
		if(evt.srcElement.className!='jqte_editor')
			$('#saveQues').click();
	}	
}

/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
function searchInventory()
{
	$('#loadingDiv').fadeIn();
	page = 1;
	getAssessmentGrid();
}
function getAssessmentGrid()
{

//	var assessmentType=document.getElementById("assessmentType1").value;
//	var assessmentStatus=document.getElementById("assessmentStatus1").value;
	var districtName=document.getElementById("districtName").value.trim();
	var districtId=document.getElementById("districtHiddenlId").value;
	if(districtName!='' && districtId=='')
		districtId=null;
	DistrictAssessmentAjax.getAssessment(/*assessmentType,assessmentStatus,*/districtId,noOfRows,page,sortOrderStr,sortOrderType, { 
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			$('#loadingDiv').hide();
			applyScrollOnTbl();
	},
	errorHandler:handleError  
	});
}

function addAssessment()
{
	clearAssessment();
	$('#divAssessmentRow').show();
	$('#districtAssessmentName').focus();
	$('#errordiv').hide();
	$('#divDone').show();
	
}
function deselectJobs()
{
	var tnl = document.getElementById("jobOrders");  

	for(i=0;i<tnl.length;i++){  
		if(tnl[i].selected == true){  
			tnl[i].selected= false;
		}
	}
}
function getJobOrders(assessmentId)
{

	var assessmentDetail = {assessmentId:assessmentId};
	var districtMaster = {districtId:dwr.util.getValue("districtMaster")};
	var schoolMaster = {schoolId:(dwr.util.getValue("schoolMaster")=='Select School'?0:dwr.util.getValue("schoolMaster"))};
	// alert(schoolMaster.schoolId);
	DistrictAssessmentAjax.getJobOrders(assessmentDetail,districtMaster,schoolMaster,{ 
		async: false,
		callback: function(data){
		//alert(data);
		dwr.util.removeAllOptions("jobOrders");
		for (var key in data) {
			  if (data.hasOwnProperty(key)) {
			    //alert(key + " -> " + data[key].jobId);
				  
				  var x = document.getElementById("jobOrders");
					var option = document.createElement("option");
					option.value = data[key].jobId;
					option.text = data[key].jobTitle+" ("+data[key].jobId+")";
					x.add(option);
			  }
			}
	},
	errorHandler:handleError  
	});	
}
function checkedUnchecked(dis)
{
	//alert(dis.id);
	if(dis.checked == true)
		dis.checked = false;
	else
		dis.checked = true;
}

function UncheckJobs()
{
	//alert('dd');
	var jobOrders = document.getElementById("jobOrders");
	for ( var i = 0; i < jobOrders.length; i++) {
		jobOrders[i].checked = false;
		//jobOrders[i].checked = true;
	}
}

function getJobOrderDistricts()
{
	var createdForEntity=2;
	createdForEntity=dwr.util.getValue("jobOrderType")=='2'?3:2;

	DistrictAssessmentAjax.getJobOrderDistricts(createdForEntity,{ 
		async: false,
		callback: function(data){
		dwr.util.removeAllOptions("districtMaster");
		dwr.util.addOptions("districtMaster",data,"districtId","districtName");
	},
	errorHandler:handleError  
	});	
}
function getJobOrderTypeData(districtId)
{
	
	if(document.getElementById("jobOrderType").value==2)
	{
		getJobOrderSchools(districtId);
		$('#schoolMaster').show();
		$('#schoolLbl').show();
	}
	else
	{
		$('#jobOrders').show();
		$('#infoData').show();
		getJobOrders(0);
		dwr.util.removeAllOptions("schoolMaster");
		dwr.util.addOptions("schoolMaster",["Select School"]);
	}
	if(districtId==0)
	{
		dwr.util.removeAllOptions("jobOrders");
	}else
	{
		displayJobCategoryByDistrict(districtId,"0");
	}
}
function displayJobCategoryByDistrict(districtId,jobCatId)
{
	DistrictAssessmentAjax.displayJobCategoryByDistrictForJSI(districtId,jobCatId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("jobCategoryMaster").innerHTML=data;
			$('#jobCategoryMaster').show();
			$('#jobCategoryLbl').show();
		}
	});
}

function getJobOrderTypeDataSOJ()
{
	$('#jobOrders').show();
	$('#infoData').show();
	//alert(dwr.util.getValue("schoolMaster"));
	if(document.getElementById("jobOrderType").value==2 && dwr.util.getValue("schoolMaster")>0)
	{
		getJobOrders(0);
	}else
		dwr.util.removeAllOptions("jobOrders");
	//getJobOrders(0);
}
function clearJobOrderTypeData()
{
	if(document.getElementById("jobOrderType").value==1)
	{
		dwr.util.removeAllOptions("schoolMaster");
		dwr.util.addOptions("schoolMaster",["Select School"]);

		if(dwr.util.getValue("districtMaster")>0)
			getJobOrderTypeData(dwr.util.getValue("districtMaster"))
			//$('#jobOrders').hide();
			//$('#infoData').hide();
			dwr.util.removeAllOptions("jobOrders");
		$('#schoolMaster').hide();
		$('#schoolLbl').hide();
	}else
	{
		dwr.util.removeAllOptions("jobOrders");
		if(dwr.util.getValue("districtMaster")>0)
		{
			$('#schoolMaster').show();
			$('#schoolLbl').show();
			getJobOrderSchools(dwr.util.getValue("districtMaster"));
		}
	}
}
function getJobOrderSchools(districtId)
{
	//alert("districtId: "+districtId);
	var districtMaster = {districtId:districtId};
	DistrictAssessmentAjax.getJobOrderSchools(districtMaster,{ 
		async: false,
		callback: function(data){
		//alert(data);
		dwr.util.removeAllOptions("schoolMaster");
		dwr.util.addOptions("schoolMaster",data,"schoolId","schoolName");
	},
	errorHandler:handleError  
	});	
}
function clearAssessment()
{
	dwr.util.setValues({ districtAssessmentId:null,districtAssessmentName:null,districtAssessmentDescription:null,status:null,assessmentSessionTime:null,questionSessionTime:null,questionRandomization:false,optionRandomization:false});
	$('#divAssessmentRow').hide();
	$('#divDone').hide();
	$('#divManage').hide();
	$('#districtAssessmentName').css("background-color", "");
	$('#assDescription').find(".jqte_editor").css("background-color", "");
	$('#assDescription').find(".jqte_editor").html("");
	document.getElementById('districtName').disabled=false;
	document.getElementById('districtName').value="";
	document.getElementById("jobCategoryMaster").innerHTML="<option value='0'>"+resourceJSON.msgJobCat+"</option>";

}
function clearData()
{

	dwr.util.setValues({ assessmentId:null,assessmentName:null,assessmentDescription:null,jobOrderType:null,isDefault:null,isResearchEPI:null,status:null,assessmentSessionTime:null,questionSessionTime:null,questionRandomization:false,optionRandomization:false});

	document.getElementsByName("assessmentType")[1].checked=true;
	$('#assessmentName').attr("disabled",false);
	var Atype=document.getElementsByName("assessmentType");
	for(i=0;i<Atype.length;i++)
		Atype[i].disabled=false;
	$('#questionSessionTime').attr("disabled",false);
	$('#assessmentSessionTime').attr("disabled",false);
	$('#questionRandomization').attr("disabled",false);
	$('#optionRandomization').attr("disabled",false);
	$('#jobOrderType').attr("disabled",false);
	//$('#isDefault').attr("disabled",false);

	$('#assessmentName').css("background-color", "");
	//$('#assessmentDescription').css("background-color", "");
	$('#assDescription').find(".jqte_editor").css("background-color", "");
	$('#assDescription').find(".jqte_editor").html("");
	$('#jobOrders').css("background-color", "");
	$('#jobs').html("");
	$('#jobs').hide();
	$('#isResearchEPI').attr("disabled",false);
}
function editAssessment(assessmentId,noAttempts)
{

	$('#errordiv').hide();
	$('#divDone').hide();
	$('#districtAssessmentName').css("background-color", "");
	$('#assDescription').find(".jqte_editor").css("background-color", "");
	$('#divAssessmentRow').show();
	$('#districtAssessmentName').focus();
	

	DistrictAssessmentAjax.getAssessmentById(assessmentId, { 
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{
		dwr.util.setValues(data);
		$('#districtAssessmentName').focus();
		$('#assDescription').find(".jqte_editor").html(data.districtAssessmentDescription);
		$('#districtHiddenlId').val(data.districtMaster.districtId);
		$('#districtName').val(data.districtMaster.districtName).attr("disabled","disabled");
		getJobCategoryByDistrict();
		
		if(data.jobCategoryMaster!=null){
			$('[name=jobCategoryMaster] option').filter(function() { 
			    return ($(this).val() == data.jobCategoryMaster.jobCategoryId);
			}).prop('selected', true);
		}
		}
	});

	document.getElementById("divManage").style.display="block";

	return false;
}
function validateAssessment()
{

	$('#errordiv').empty();
	$('#districtAssessmentName').css("background-color", "");
	//$('#assessmentDescription').css("background-color", "");
	$('#assDescription').find(".jqte_editor").css("background-color", "");
	$('#jobOrders').css("background-color", "");
	$('#districtName').css("background-color", "");
	
	var cnt=0;
	var focs=0;

	if (trim(document.getElementById("districtAssessmentName").value)==""){

		$('#errordiv').append("&#149; "+resourceJSON.msgEnterAssmntName+"<br>");
		if(focs==0)
			$('#districtAssessmentName').focus();
		$('#districtAssessmentName').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if ($('#assDescription').find(".jqte_editor").text().trim()==""){

		$('#errordiv').append("&#149; "+resourceJSON.msgAssmntDesc+"<br>");
		if(focs==0)
			$('#assDescription').find(".jqte_editor").focus();
		$('#assDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if ($('#districtHiddenlId').val()=='' && $('#districtHiddenlId').val()==0){

		$('#errordiv').append("&#149;"+resourceJSON.msgEnterDistName+".<br>");
		if(focs==0)
			$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
		
	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}

}

function saveAssessment()
{
	if(!validateAssessment())
		return;
	var districtAssessment = {districtAssessmentId:null,districtAssessmentName:null,districtAssessmentDescription:null,assessmentSessionTime:null,status:null,questionSessionTime:null,questionRandomization:false,optionRandomization:false};
	var districtMaster = {districtId:dwr.util.getValue("districtHiddenlId")};
//	var districtAssesmentID = {districtId:dwr.util.getValue("districtAssessmentId")};
	dwr.util.getValues(districtAssessment);
	dwr.engine.beginBatch();
	districtAssessment.districtMaster = districtMaster;
	
//	if($("#districtAssessmentId").val()!='')
//		districtAssessment.districtassessmentId =districtAssesmentID;
	var jobCategoryMaster = {jobCategoryId:(dwr.util.getValue("jobCategoryMaster")==resourceJSON.msgJobCat ?0:dwr.util.getValue("jobCategoryMaster"))};
	if(jobCategoryMaster.jobCategoryId!=0)
		districtAssessment.jobCategoryMaster=jobCategoryMaster;
	DistrictAssessmentAjax.saveAssessment(districtAssessment, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			getAssessmentGrid();
			clearAssessment();
			if(data==3)
			{
				document.getElementById('errordiv').innerHTML='&#149; '+resourceJSON.msgUniqAssmntName;
				$('#errordiv').show();
				$('#districtAssessmentName').css("background-color", "#F5E7E1");
				$('#districtAssessmentName').focus();
			}else if(data==1 || data==2)
			{
				getAssessmentGrid();
				clearAssessment();
			}
		}
	});
	dwr.engine.endBatch();

}
function deleteAssessment(districtAssessmentId)
{
	if (confirm(resourceJSON.msgDeletethisInventory)) {
		var districtAssessmentDetail = {districtAssessmentId:districtAssessmentId};
		alert(districtAssessmentDetail.districtAssessmentId);
		DistrictAssessmentAjax.deleteAssessment(districtAssessmentDetail,"",{ 
			async: true,
			errorHandler:handleError, 
			callback: function(data)
			{
				getAssessmentGrid();
				clearAssessment();
			}
		});

	}
}
function deleteJob(assessmentJobRelationId,assessmentId)
{
	if (confirm(resourceJSON.msgRemovethisJobInventory)) {
		var assessmentJobRelation = {assessmentJobRelationId:assessmentJobRelationId};

		DistrictAssessmentAjax.deleteJob(assessmentJobRelation,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			//alert(data);
			getSelectedJobs(assessmentId);
			getJobOrders(0);
		}
		});	

	}
}
function getSelectedJobs(assessmentId)
{

	var assessmentDetail = {assessmentId:assessmentId};

	DistrictAssessmentAjax.getSelectedJobs(assessmentDetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		//alert(data);
		$('#jobs').html(data);
		$('#jobs').show();
	}
	});	


}
function activateDeactivateAssessment(assessmentId,status)
{
	DistrictAssessmentAjax.activateDeactivateAssessment(assessmentId,status, { 
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{
		// no active base checking 
		/*if(data!=null)
		{
			alert("We already have active base inventory ("+data.assessmentName+"). Please deactivate it first. ");
			return;
		}*/
		getAssessmentGrid();
		clearAssessment();
		}
	});
}

function getJobs(flag)
{
	if(flag==1)
	{
		$('#jobOrders').hide();
		$('#infoData').hide();
		$('#jsjo1').hide();
		$('#jsjo2').hide();
		$('#jot1').hide();
		$('#jot2').hide();
		$('#def1').hide();
		$('#def2').hide();
		$('#def3').show();
		$('#def4').show();
	}
	else
	{
		$('#jobOrders').show();
		$('#infoData').show();
		$('#jsjo1').show();
		$('#jsjo2').show();
		$('#jot1').show();
		$('#jot2').show();
		$('#def1').show();
		$('#def2').show();
		$('#def3').hide();
		$('#def4').hide();
	}
}

/////////////////////////////////////Assessment Section ////////////////////////////////////////////////


function getAssessmentSectionGrid()
{
	//alert(dwr.util.getValue("districtassessmentId"));
	DistrictAssessmentAjax.getAssessmentSections(dwr.util.getValue("districtassessmentId"),noOfRows,page,sortOrderStr,sortOrderType,{  
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
		$('#divMain').html(data);
		applyScrollOnTbl();		
		}
	});

}

function addAssessmentSection()
{
	document.getElementById("divAssessmentRow").style.display="block";
	$('#sectionName').focus();
	$('#errordiv').hide();
	$('#sectionName').css("background-color", "");
	document.getElementById("divDone").style.display="block";
	document.getElementById("divManage").style.display="none";
	dwr.util.setValues({ sectionId:null,sectionName:null,sectionDescription:null, sectionInstructions:null});
	//clearData();
	$('#secDescription').find(".jqte_editor").html("");
	$('#secInstruction').find(".jqte_editor").html("");
	$('#secDescription').find(".jqte_editor").css("background-color", "");
	$('#secInstruction').find(".jqte_editor").css("background-color", "");
	return false;
}
function validateAssessmentSection()
{
	$('#errordiv').empty();
	$('#sectionName').css("background-color", "");
	$('#secDescription').find(".jqte_editor").css("background-color", "");
	$('#secInstruction').find(".jqte_editor").css("background-color", "");
	var cnt=0;
	var focs=0;

	if (trim(document.getElementById("sectionName").value)==""){

		$('#errordiv').append("&#149; "+resourceJSON.msgSectionName+"<br>");
		if(focs==0)
			$('#sectionName').focus();
		$('#sectionName').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	var charCount=trim($('#secDescription').find(".jqte_editor").text());
	var count = charCount.length;
	if(count>750)
	{
		$('#errordiv').append("&#149;"+resourceJSON.msgSectionDescriptionlength750char+"<br>");
		if(focs==0)
			$('#secDescription').find(".jqte_editor").focus();
		$('#secDescription').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	charCount=trim($('#secInstruction').find(".jqte_editor").text());
	count = charCount.length;
	if(count>2500)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgSectionInstructionslength2500char+"<br>");
		if(focs==0)
			$('#secInstruction').find(".jqte_editor").focus();
		$('#secInstruction').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}


	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}

}
function saveDistrictAssessmentSection()
{
	/*if(!validateAssessmentSection())
		return;*/

	var districtassessmentSection = {districtSectionId:null,sectionName:null,sectionDescription:null,sectionInstructions:null};
	dwr.util.getValues(districtassessmentSection);
	dwr.engine.beginBatch();
	DistrictAssessmentAjax.saveAssessmentSection(districtassessmentSection,dwr.util.getValue("districtassessmentId"), { 
		async: true,
		errorHandler:handleError,  
		callback:function(data)
		{
			getAssessmentSectionGrid();
			clearAssessmentSection();
		//alert("data :: "+data)
		/*if(data==3)
			{
				document.getElementById('errordiv').innerHTML='&#149; Please enter a unique Section Name';
				$('#errordiv').show();
				$('#sectionName').css("background-color", "#F5E7E1");
				$('#sectionName').focus();
			}else
			{
				getAssessmentSectionGrid();
				clearAssessmentSection();
			}*/
		}
	});

	dwr.engine.endBatch();

}
function editAssessmentSection(sectionId)
{
	dwr.util.setValues({ sectionId:null,sectionName:null,sectionDescription:null, sectionInstructions:null,sectionVideoUrl:null});
	$('#errordiv').hide();
	$('#sectionName').css("background-color", "");
	$('#secDescription').find(".jqte_editor").css("background-color", "");
	$('#secInstruction').find(".jqte_editor").css("background-color", "");

	document.getElementById("divDone").style.display="none";
	document.getElementById("divAssessmentRow").style.display="block";

	$('#sectionName').focus();

	DistrictAssessmentAjax.getAssessmentSectionById(sectionId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			dwr.util.setValues(data);
			//alert(data.sectionDescription);
			$('#secDescription').find(".jqte_editor").html(data.sectionDescription);
			$('#secInstruction').find(".jqte_editor").html(data.sectionInstructions);
		}
	});

	document.getElementById("divManage").style.display="block";

	return false;
}
function clearAssessmentSection()
{
	dwr.util.setValues({ districtSectionId:null,sectionName:null,sectionDescription:null, sectionInstructions:null});

	document.getElementById("divAssessmentRow").style.display="none";
	document.getElementById("divDone").style.display="none";
	document.getElementById("divManage").style.display="none";

	$('#sectionName').css("background-color", "");
	$('#secDescription').find(".jqte_editor").html("");
	$('#secInstruction').find(".jqte_editor").html("");
}
function deleteAssessmentSection(sectionId)
{
	if (confirm(resourceJSON.msgDeleteSection)) {

		var assessmentSection = {sectionId:sectionId};
		DistrictAssessmentAjax.deleteAssessmentSection(assessmentSection, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getAssessmentSectionGrid();
			clearAssessmentSection();
			}
		});

	}
}
function displayVideo(url)
{
	//alert("url "+url);
	if((url!=null && url.length>0))
	{
			DistrictAssessmentAjax.displayVideo(url, { 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
				//alert("data "+data);
				//getAssessmentSectionQuestionsGrid();
				$('#myModal2').modal('show');
				$('#message2show').html(data);
				//$('#dv').empty();
				//$('#dv').append(data);
				}
			});
	}
	else
	{
		$('#myModal2').modal('show');
		$('#message2show').html(resourceJSON.msgVideoCorrespondingQuestion);
	}
}

function getCompetencies(domainId)
{
	//alert(domainId);
	DistrictAssessmentAjax.getCompetencies(domainId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		dwr.util.removeAllOptions("competencyMaster");
		dwr.util.addOptions("competencyMaster",data,"competencyId","competencyName");
		dwr.util.removeAllOptions("objectiveMaster");
		dwr.util.addOptions("objectiveMaster",["Select Objective"]);
	}
	});	
}

function getObjectives(competencyId)
{
	//alert(competencyId);

	DistrictAssessmentAjax.getObjectives(competencyId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		dwr.util.removeAllOptions("objectiveMaster");
		dwr.util.addOptions("objectiveMaster",data,"objectiveId","objectiveName");
	}
	});	
}


function getQuestionOptions(questionTypeId)
{
	$('#imageType').hide();
	$('#imageuploadQuestion').hide();
	var qType=findSelected(questionTypeId);
//	alert("qType :: "+qType)
	if(questionTypeId==0 || qType=='sl' || qType=='ml')
	{
		for(i=0;i<=3;i++)
			$('#row'+i).hide();

		if(questionTypeId==0)
			$('#slmlScoreDiv').hide();
		else
			$('#slmlScoreDiv').show();
		$('#questionWeightage').attr("disabled","disabled");
		return;
	}
	else
	{
		for(i=0;i<=3;i++)
			$('#row'+i).show();
		$('#slmlScoreDiv').hide();
		$('#questionWeightage').attr("disabled",false);
	}
	
	
	if(qType=='tf')
	{
		$('#row2').hide();
		$('#row3').hide();
		for(i=1;i<=2;i++)
			$('#rank'+i).attr("disabled","disabled");

		//clearOptions();

		if($('#opt1').val()=="")
			$('#opt1').attr("value","True");
		if($('#opt2').val()=="")
			$('#opt2').attr("value","False");

	}else if(qType=='slsel')
	{
		$('#row2').show();
		$('#row3').show();
		for(i=1;i<=6;i++)
			$('#rank'+i).attr("disabled","disabled");

		//clearOptions();
	}else if(qType=='lkts')
	{
		$('#row2').show();
		$('#row3').show();
		for(i=1;i<=6;i++)
			$('#rank'+i).attr("disabled","disabled");

		//clearOptions();
	}
	else if(qType=='rt')
	{
		$('#row2').show();
		$('#row3').show();
		for(i=1;i<=6;i++)
			$('#rank'+i).attr("disabled",false);

		//clearOptions();
	}else if(qType=='it')
	{
		for(i=0;i<=3;i++)
			$('#row'+i).hide();

		$('#imageType').show();

		
	}else if(qType=='itq'){
		//alert(" slsel :: "+qType)
		$('#row2').show();
		$('#row3').show();
		imageuploadQuestion
		$('#imageuploadQuestion').show();
		for(i=1;i<=6;i++)
			$('#rank'+i).attr("disabled","disabled");
		
		return;
	}

}
function clearOptions()
{
	for(i=1;i<=6;i++)
	{
		$('#opt'+i).attr("value","");
		$('#score'+i).attr("value","");
		$('#rank'+i).attr("value","");
	}
}
function redirectTo(redirectURL)
{
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
}

function validateAssessmentSectionQuestions()
{
	$('#errordiv').empty();
	$('#errordiv1').empty();
	$('#domainMaster').css("background-color", "");
	$('#competencyMaster').css("background-color", "");
	$('#objectiveMaster').css("background-color", "");
	$('#stageOneStatus').css("background-color", "");
	$('#stageTwoStatus').css("background-color", "");
	$('#stageThreeStatus').css("background-color", "");
	//$('#question').css("background-color", "");
	$('#assQuestion').find(".jqte_editor").css("background-color", "");
	$('#assInstruction').find(".jqte_editor").css("background-color", "");
	$('#questionTypeMaster').css("background-color", "");
	$('#opt1').css("background-color", "");

	for(i=1;i<=6;i++)
	{
		$('#rank'+i).css("background-color", "");
		$('#score'+i).css("background-color", "");
	}

	var cnt=0;
	var focs=0;

	if (document.getElementById("domainMaster") && trim(document.getElementById("domainMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.msgDomainName+"<br>");
		if(focs==0)
			$('#domainMaster').focus();
		$('#domainMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if (document.getElementById("competencyMaster") && trim(document.getElementById("competencyMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.msgCompetency+"<br>");
		if(focs==0)
			$('#competencyMaster').focus();
		$('#competencyMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	//alert(document.getElementById("objectiveMaster").value);
	if(document.getElementById("objectiveMaster"))
		if (trim(document.getElementById("objectiveMaster").value)==resourceJSON.msgSelectObjective || trim(document.getElementById("objectiveMaster").value)==0){

			$('#errordiv').append("&#149; "+resourceJSON.msgObjective+"<br>");
			if(focs==0)
				$('#objectiveMaster').focus();
			$('#objectiveMaster').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	/* validation For Stage1,2,3*/
	if(document.getElementById("stageOneStatus"))
		if (trim(document.getElementById("stageOneStatus").value)==resourceJSON.msgSelectStageOne || trim(document.getElementById("stageOneStatus").value)==0){

			$('#errordiv').append("&#149; "+resourceJSON.msgStageOne+"<br>");
			if(focs==0)
				$('#stageOneStatus').focus();
			$('#stageOneStatus').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	
	if(document.getElementById("stageTwoStatus"))
		if (trim(document.getElementById("stageTwoStatus").value)==resourceJSON.msgSelectStageOne || trim(document.getElementById("stageTwoStatus").value)==0){

			$('#errordiv').append("&#149; "+resourceJSON.msgStageTwo+"<br>");
			if(focs==0)
				$('#stageTwoStatus').focus();
			$('#stageTwoStatus').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	
	if(document.getElementById("stageThreeStatus"))
		if (trim(document.getElementById("stageThreeStatus").value)==resourceJSON.msgSelectStageOne || trim(document.getElementById("stageThreeStatus").value)==0){

			$('#errordiv').append("&#149; "+resourceJSON.msgStageThree+"<br>");
			if(focs==0)
				$('#stageThreeStatus').focus();
			$('#stageThreeStatus').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	/* End Stage Validation */
	
	
	if($('#assQuestion').find(".jqte_editor").text().trim()==""){

		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrQuest+"<br>");
		if(focs==0)
			$('#assQuestion').find(".jqte_editor").focus();
		$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}else
	{
		var charCount=trim($('#assQuestion').find(".jqte_editor").text());
		var count = charCount.length;
		//alert(count);
		if(count>5000)
		{
			$('#errordiv').append("&#149; "+resourceJSON.MsgQuestionLength+"<br>");
			if(focs==0)
				$('#assQuestion').find(".jqte_editor").focus();
			$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	if (trim(document.getElementById("questionTypeMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.PlzSelectQuestionType+"<br>");
		if(focs==0)
			$('#questionTypeMaster').focus();
		$('#questionTypeMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	var qType=findSelected(dwr.util.getValue("questionTypeMaster"));
	var c=0;

	if(!(qType=='sl' || qType=='ml' || qType=='it'))
		if(document.getElementById("opt1"))
		{
			for(i=1;i<=6;i++)
			{
				if(trim(document.getElementById("opt"+i).value)=="")
					c++;
			}
			if(c==5 || c==6)
			{
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrAtLeastTwoOptions+"<br>");
				if(focs==0)
				{
					if(c==6)
						$('#opt1').focus();
					else
						$('#opt2').focus();
				}

				$('#opt1').css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}

	if(qType=='it' && document.getElementById("opt7"))
	{
		c=0;
		for(i=7;i<=12;i++)
		{
			if(trim(document.getElementById("opt"+i).value)=="")
				c++;
		}
		if(c==5 || c==6)
		{
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrAtLeastTwoOptions+"<br>");
			$('#errordiv').show();
			return;
		}
	}

	var arr =[];
/////////////////// for rank type question validation
	if(qType=='rt')
	{

		for(i=1;i<=6;i++)
		{
			if($('#rank'+i).val()!="")
				arr.push($('#rank'+i).val());
		}


		if(arr.length==0)
		{
			// rank validation
			/*$('#errordiv').append("&#149; Please enter Rank of corresponding option<br>");
			$('#rank1').css("background-color", "#F5E7E1");
			if(focs==0)
			{
				$('#rank1').focus();
			}
			cnt++;focs++;*/
		}else
		{

			var uniqueNames = [];
			var dup = [];
			var focArr = [];
			$.each(arr, function(j, el){

				if($.inArray(el, uniqueNames) === -1)
				{
					uniqueNames.push(el);
				}else
				{
					dup.push(el);
					focArr.push(j+1);
				}
			});

			if(dup.length>0)
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgUniqueRank+"<br>");
				$('#rank'+focArr[0]).css("background-color", "#F5E7E1");
				if(focs==0)
				{
					$('#rank'+focArr[0]).focus();
				}
				cnt++;focs++;
			}
		}


		// rank validation
		var arr2 = [1,2,3,4,5,6];
		for(i=1;i<=6;i++)
		{
			if($('#opt'+i).val()!="" && $('#rank'+i).val()=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgRankCorrespondingOption+"");
				$('#rank'+i).css("background-color", "#F5E7E1");
				if(focs==0)
				{
					$('#rank'+i).focus();
				}
				cnt++;focs++;
				break;
			}
		}

		if($('#questionWeightage').val()!=0)
		{
			for(i=1;i<=6;i++)
			{
				if($('#opt'+i).val()!="" && $('#score'+i).val()=="")
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgScoreCorrespondingOption+"");
					$('#score'+i).css("background-color", "#F5E7E1");
					if(focs==0)
					{
						$('#score'+i).focus();
					}
					cnt++;focs++;
					break;
				}
			}
		}
	}

///////////////////////	

	var charCount=trim($('#assInstruction').find(".jqte_editor").text());
	var count = charCount.length;
	//alert(count);
	if(count>2500)
	{
		$('#errordiv').append("&#149; "+resourceJSON.MsgInstructionsCannotExceed+"<br>");
		if(focs==0)
			$('#assInstruction').find(".jqte_editor").focus();
		$('#assInstruction').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}

}
function saveQuestion(districtAssessmentId,districtSectionId,status) {

	/*if(!validateAssessmentSectionQuestions())
		return;*/
	var questionType={questionTypeId:dwr.util.getValue("questionTypeMaster")};
    var assesmentquestionsId = {districtAssessmentQuestionId:dwr.util.getValue("districtAssessmentQuestionId"),question:null,questionTypeMaster:questionType,questionWeightage:null,questionInstruction:null,questionImage:dwr.util.getValue("questionImage")};
    
    var districtassessmentDetail 	= 	{districtAssessmentId:districtAssessmentId};
	var districtassessmentSections 	= 	{districtSectionId:districtSectionId};
	var qType				=	findSelected(dwr.util.getValue("questionTypeMaster"));
	var weightage 			= 	$('#questionWeightage').val()==""?0:$('#questionWeightage').val();
	var jjon				=	"{id:1}";
	var questionoptions		=	{};
	var arr 				=	[];
	var max 				= 	0;

	if(qType=='tf')	{
		var maxArr =[];

		for(i=1;i<=2;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;

	} else if(qType=='slsel') {
		var maxArr =[];

		for(i=1;i<=6;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;

	} else if(qType=='lkts') {
		var maxArr =[];

		for(i=1;i<=6;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}
		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;
	} else if(qType=='it') {
		var maxArr =[];
		for(i=7;i<=12;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="")
				{
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;
	} else if(qType=='rt') {
		var v;
		var total=0;
		for(i=1;i<=6;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : $('#rank'+i).val()==""?"0":$('#rank'+i).val()
				});

				if($('#score'+i).val()!="") {
					v = parseFloat($('#score'+i).val());
					if (!isNaN(v)) total += v;
				}
			}
		}

		max =  weightage*total;
		document.getElementById("maxMarks").value = max;
	}else if(qType=='itq') {
		var maxArr =[];

		for(i=1;i<=6;i++) {
			if($('#opt'+i).val()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"score"       : $('#score'+i).val(),
					"rank"       : 0
				});

				if($('#score'+i).val()!="") {
					maxArr.push($('#score'+i).val());
				}
			}
		}

		if(maxArr.length>0)
			max =  weightage*Math.max.apply(Math, maxArr);
			document.getElementById("maxMarks").value = max;

	} else if(qType=='sl' || qType=='ml'){
		arr.push({ 
			"optionId" : $('#hid'+0).val(),
			"questionOption"  : "",
			"score"       : $('#slmlScore').val(),
			"rank"       : 0
	  });
	}

	dwr.util.getValues(assesmentquestionsId);
	dwr.engine.beginBatch();
	
	if(qType=='sl' || qType=='ml'){
		assesmentquestionsId.questionWeightage=0;
		assesmentquestionsId.maxMarks=document.getElementById("slmlScore").value
	}else{
	   assesmentquestionsId.maxMarks = max;
	}
    DistrictAssessmentAjax.saveAssessmentSectionQuestion(districtassessmentDetail,districtassessmentSections,jjon,arr,assesmentquestionsId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
		 //alert("saved... ")
		var redirectURL = "";
		if(status=="save"){
			redirectURL = "districtassessmentquestions.do?assessmentId="+districtAssessmentId+"&sectionId="+districtSectionId;
		}
		else
			redirectURL = "districtassessmentsectionquestions.do?sectionId="+districtSectionId;

			redirectTo(redirectURL);

		}
	});

	dwr.engine.endBatch();

}
function cancelQuestion(assessmentId,sectionId)
{
	var redirectURL = "districtassessmentquestions.do?assessmentId="+assessmentId+"&sectionId="+sectionId;
	redirectTo(redirectURL);
}
function getAssessmentSectionQuestionsGrid()
{
	var districtassessmentDetail = {districtAssessmentId:dwr.util.getValue("districtAssessmentId")};
	var districtassessmentSections = {districtSectionId:dwr.util.getValue("districtSectionId")};
	DistrictAssessmentAjax.getAssessmentSectionQuestions(districtassessmentDetail,districtassessmentSections,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#tblGrid').html(data);
		}
	});
}
function deleteAssessmentQuestion(districtassessmentQuestionId)
{
	if (confirm("Are you sure you would like to remove this Question from the Assessment?")) {
		DistrictAssessmentAjax.deleteAssessmentQuestion(districtassessmentQuestionId, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getAssessmentSectionQuestionsGrid();
			}
		});

	}
}
function setAssessmentQuestion(assessmentQuestionId) {

	if(assessmentQuestionId>0) {
		var assessmentQuestions = {districtAssessmentQuestionId:assessmentQuestionId};
		DistrictAssessmentAjax.getAssessmentQuestionById(assessmentQuestions,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			dwr.util.setValues(data);

			$('#assQuestion').find(".jqte_editor").html(data.question);
			$('#assInstruction').find(".jqte_editor").html(data.questionInstruction);

			dwr.util.setValue("questionTypeMaster",data.questionTypeMaster.questionTypeId);
			getQuestionOptions(data.questionTypeMaster.questionTypeId);
			
			var cnnt=1;
			if(findSelected(data.questionTypeMaster.questionTypeId)=='it') {

				cnnt=7;

				jQuery.each(assessQuesOpts, function (i, jsonSelectedOptions) {								

					$('#fileDiv'+(cnnt-6)).hide();
					$('#imgTp'+(cnnt-6)).hide();
					$('#rm'+(cnnt-6)).show();
					$('#img'+(cnnt-6)).html("<a target='_blank' href='showImage?image=ques_images/"+jsonSelectedOptions.questionOption+"'><img src='showImage?image=ques_images/"+jsonSelectedOptions.questionOption+"' height='100' width='180'></a>");
					$('#img'+(cnnt-6)).show();

					$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
					$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
					$('#score'+cnnt).attr("value",jsonSelectedOptions.score);
					$('#rank'+cnnt).attr("value",jsonSelectedOptions.rank==0?"":jsonSelectedOptions.rank);
					cnnt++;
				});

			}else if(findSelected(data.questionTypeMaster.questionTypeId)=='itq') {
				//alert("itq");
				$('#fileDiv0').hide();
				$('#imgTp0').hide();
				$('#rm0').show();
				$('#img0').html("<a target='_blank' href='showImage?image=ques_images/"+data.questionImage+"'><img src='showImage?image=ques_images/"+data.questionImage+"' height='100' width='180'></a>");
				$('#img0').show();
				
				
				
				jQuery.each(assessQuesOpts, function (i, jsonSelectedOptions) {								
					$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
					$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
					$('#score'+cnnt).attr("value",jsonSelectedOptions.score);
					$('#rank'+cnnt).attr("value",jsonSelectedOptions.rank==0?"":jsonSelectedOptions.rank);
					cnnt++;
				});
				
			}else if(findSelected(data.questionTypeMaster.questionTypeId)=='sl' || findSelected(data.questionTypeMaster.questionTypeId)=='ml'){
				jQuery.each(assessQuesOpts, function (i, jsonSelectedOptions) {								
					$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
					$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
					$('#slmlScore').attr("value",jsonSelectedOptions.score);
					$('#rank'+cnnt).attr("value",jsonSelectedOptions.rank==0?"":jsonSelectedOptions.rank);
					cnnt++;
				});
			}else{
				jQuery.each(assessQuesOpts, function (i, jsonSelectedOptions) {								
					$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
					$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
					$('#score'+cnnt).attr("value",jsonSelectedOptions.score);
					$('#rank'+cnnt).attr("value",jsonSelectedOptions.rank==0?"":jsonSelectedOptions.rank);
					cnnt++;
				});
			}
		  }
		});	
	}
}
function getSectionQuestions(dis)
{
	var	redirectURL = "districtassessmentquestions.do?assessmentId="+$('#assessmentId').val()+"&sectionId="+dis.value;
	redirectTo(redirectURL);
}

function importQuestion(){
	
	$('.importQuestionDiv').modal('show');
}

function validateTeacherFile()
{
	var teacherfile	=	document.getElementById("teacherfile").value;
	var errorCount=0;
	var aDoc="";
	$('#errordivQuestionUpload').empty();
	
	if(teacherfile==""){
		$('#errordivQuestionUpload').show();
		$('#errordivQuestionUpload').append("&#149; "+resourceJSON.msgXlsXlsxFile+"<br>");
		errorCount++;
		aDoc="1";
	}
	else if(teacherfile!="")
	{
		var ext = teacherfile.substr(teacherfile.lastIndexOf('.') + 1).toLowerCase();	
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("teacherfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("teacherfile").files[0].size;
			}
		}
		
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errordivQuestionUpload').show();
			$('#errordivQuestionUpload').append("&#149; "+resourceJSON.msgXlsXlsxFileOnly+"<br>");
			errorCount++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordivQuestionUpload').show();
			$('#errordivQuestionUpload').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			errorCount++;
			aDoc="1";	
		}
	}

	if(aDoc==1){
		$('#teacherfile').focus();
		$('#teacherfile').css("background-color", "#F5E7E1");
	}
	
	if(errorCount==0){
		try{
			if(teacherfile!=""){
					document.getElementById("questionUploadServlet").submit();
			}
		}catch(err){}

	}else{ 
		$('#errordivQuestionUpload').show();
		return false;
	}
}


function uploadDataQuestion(fileName,sessionId){
	$('#loadingDiv').fadeIn();
	TeacherUploadTempAjax.saveTeacherTemp(fileName,sessionId,{
		async: true,
		callback: function(data){
			if(data=='1'){
				window.location.href="candidatetemplist.do";
			}else{
				$('#loadingDiv').hide();
				$('#errordivQuestionUpload').show();
				$('#errordivQuestionUpload').append("&#149; "+resourceJSON.msgField+" "+data+" "+resourceJSON.msgDoesNotMatch+"<br>");
			}
		},
		errorHandler:handleError 
	});
}

/*search with district Name*/
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	return searchArray;
}
var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="") {
			var onclick="onclick=\"$('#divTxtShowData').css('display','none');\"";
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;

				if(count==10)
					break;
			}
		} else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}

		scrolButtom();
	}catch (err){}
}
var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if($('#districtName').val().trim()!=''){
	document.getElementById("districtHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(showDataArray && showDataArray[index]) {
			dis.value=showDataArray[index];
			document.getElementById("districtHiddenlId").value=hiddenDataArray[index];
		}
		
	} else {
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}

	if(document.getElementById(divId)) {
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
	}else{
		document.getElementById("districtHiddenlId").value='';
	}
}
function inputOnEnter(e) {
    var key;

    if (window.event)
        key = window.event.keyCode; //IE
    else
        key = e.which; //Firefox & others

    if(key == 13)
        return false;
}
/*end*/

function getJobCategoryByDistrict()
{
	var districtId = document.getElementById("districtHiddenlId").value;

	if(districtId!=0)
	{
		//DWRAutoComplete
		DWRAutoComplete.getJobCategoryByDistrict(districtId,{ 
			async: false,		
			callback: function(data)
			{
			
			document.getElementById("jobCategoryMaster").innerHTML=data;
			$('#jobCategoryMaster').show();
			//	document.getElementById("jobCateSelect").innerHTML=data;
			}
		});
	}
	else
	{
		document.getElementById("jobCategoryMaster").innerHTML="<option value='0'>"+resourceJSON.msgDistrictJobsCategorylist+"</option>";
	}
	
}
