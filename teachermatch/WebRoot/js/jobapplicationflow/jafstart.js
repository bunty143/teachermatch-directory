function callNewSelfServiceApplicationFlow(jobId,candidateType)
{
	//alert("callNewSelfServiceApplicationFlow callNewSelfServiceApplicationFlow jafstart");
	DSPQServiceAjax.callNewSelfServiceApplicationFlow(jobId,candidateType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!='' && data!="DSPQNC" && data!="OldProcess" && data!="AlreadyCompleted")
			{
				window.location.href="./"+data;
			}
			else if(data!='' && data=="DSPQNC")
			{
				$('#DSPQNCDivNotification').modal('show');
			}
			else if(data!='' && data=="AlreadyCompleted")
			{
				$('#DSPQCompletedDivNotification').modal('show');
			}
			/*else
			{
				alert("JAFStart No 3");
				//getDistrictSpecificQuestionDefault(jobId)
			}*/
			
		}});
}

function DSPQNCNotificationClose()
{
	$('#DSPQNCDivNotification').modal('hide');
}

function DSPQCompletedDivNotificationClose()
{
	$('#DSPQCompletedDivNotification').modal('hide');
	window.location.href="./userdashboard.do";
}

/*var isFormSubmitted = false;
function getDistrictSpecificQuestionDefault(jobId)
{	
	var isAffilated=0;
	try{
		if(document.getElementById("ok_cancelflag").value==1)
		{	try{
				document.getElementById("isAffilated").checked=true;
			}catch(err){}
			isAffilated=1;
		}
	}catch(err){}
	
	try{
		if(document.getElementById("isAffilated").checked){
			isAffilated=1;
		}
	}catch(err){}
	DSPQServiceAjax.getDistrictSpecificQuestion(jobId,isAffilated,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
		
			if($("#isPrinciplePhiladelphia").val()=="1")
			{
				data=null;
			}
			
			if(data!=null)
			{
				
				try
				{
					var dataArray = data.split("@##@");
					totalQuestions = dataArray[1];
					
					var textmsg = dataArray[2];
					if(textmsg!=null && textmsg!="" && textmsg!='null')
						$('#textForDistrictSpecificQuestions').html(textmsg);
					
					if(isFormSubmitted==false)
					{
						
						isFormSubmitted = true;
						if(totalQuestions==0)
						{
							if(isJobAplied)
							{
								continueCompleteNow();
							}
							else
							{
								document.getElementById("frmApplyJob").submit();
								return;
							}
						}
						else if(totalQuestions!=undefined)
						{
							$('#tblGrid').html(dataArray[0]);
							$('#loadingDiv_dspq_ie').hide();
							$('#myModalDASpecificQuestions').modal('show');
						}
					}
					else if(totalQuestions>0)
					{	
						$('#myModalDASpecificQuestions').modal('show');
					}
					
				}catch(err)
				{}
			}
			else
			{
				document.getElementById("frmApplyJob").submit();
			}

		}
	});	
}*/