var rdURL = "userdashboard.do"; 
var totalAttempted=0;
var jOrder;
var vData;
var assessmentTakenCount=0;

function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert("AJAX NOT SUPPORTED BY YOUR BROWSER");
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
    
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function setFocus(inputControlId)
{
	if(inputControlId!="")
	{
		document.getElementById(inputControlId).focus();
	}
}

function startEPI(jobId)
{
	//alert("startEPI");
	//checkInventory(0);
	
	var epiJobId=""; 
	try { epiJobId=document.getElementById("dspqJobId").value; } catch (e) {}
	
	checkInventory(0,epiJobId);
}

function startJSI()
{
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}
	
	checkInventory(jobId,jobId);
}

function checkInventory(jobId,epiJobId)
{
	//alert("checkInventory jobId "+jobId);
	
	var jobOrder={jobId:jobId};
	DSPQServiceAjax.jafCheckInventory(jobOrder,epiJobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		//The Job Specific Inventory for this job has not been finalized. Please come back later to complete this application.
		
		//alert("checkInventory data.status "+data.status);
		
		var remodel=resourceJSON.msgBaseInventory;
		if(jobId>0)
			remodel=resourceJSON.msgJobSpecificInventory;

		if(data.assessmentName==null && data.status=='1')
		{
			//alert("1");
			$('#epiDataAreaIdL2').html(""+resourceJSON.msgThe+" "+remodel+" "+resourceJSON.msgFinalizedCompleteApplication);
			//$('#epiDataAreaIdL2').modal('show');
			$('#epiDataAreaIdL2').show();
			showtopRightBtnL2();
		}
		else if(data.assessmentName==null && data.status=='2')
		{
			//alert("2 "+""+resourceJSON.msgThe+" "+remodel+" "+resourceJSON.msgFinalizedCompleteApplication);
			$('#epiDataAreaIdL2').html(""+resourceJSON.msgThe+" "+remodel+" "+resourceJSON.msgFinalizedCompleteApplication);
			$('#epiDataAreaIdL2').show();
			
			showtopRightBtnL2();
		}
		else if(data.assessmentName==null && data.status=='3')
		{
			//alert("3");
			if(jobId>0)
				getTeacherJobDone(jobId);
			else
			{
				$('#epiDataAreaIdL2').html(resourceJSON.msgAlreadyCompletedInventory);
				//$('#epiDataAreaIdL2').modal('show');
				$('#epiDataAreaIdL2').show();
				
				showtopRightBtnL2();
			}
		}
		else if(data.assessmentName==null && data.status=='4')
		{
			//alert("4");
			$('#epiDataAreaIdL2').html(resourceJSON.msgExpiredTeacherMatchAdministrator);
			//$('#epiDataAreaIdL2').modal('show');
			$('#epiDataAreaIdL2').show();
			showtopRightBtnL2();
		}
		else if(data.assessmentName==null && data.status=='5')
		{
			//alert("5");
			if(jobId>0)
				getTeacherJobDone(jobId);
			else
			{
				$('#epiDataAreaIdL2').html(resourceJSON.msgTimedOutTeacherMatchAdministrator);
				//$('#epiDataAreaIdL2').modal('show');
				$('#epiDataAreaIdL2').show();
				showtopRightBtnL2();
			}
		}
		else if(data.assessmentName==null && data.status=='6')
		{
			//alert("6");
			$('#epiDataAreaIdL2').html(resourceJSON.msgActiveTeacherMatchAdministrator);
			//$('#epiDataAreaIdL2').modal('show');
			$('#epiDataAreaIdL2').show();
			showtopRightBtnL2();
		}
		else if(data.assessmentName==null && data.status=='7')
		{
			//alert("7");
			$('#epiDataAreaIdL2').html(resourceJSON.msgjobExpiredTeacherMatchAdministrator);
			//$('#epiDataAreaIdL2').modal('show');
			$('#epiDataAreaIdL2').show();
			showtopRightBtnL2();
		}
		else
		{
			//alert("Other");
			if(data.status.charAt(2)>=3)
			{
				//alert("Other 1");
				
				$('#jafwarningImg').html("<img src='images/stop.png' align='top'>");
				$('#jafmessage2showConfirm').html(""+resourceJSON.msgDidntAnswithin3Attempt+" clientservices@teachermatch.net "+resourceJSON.msgor+" (888) 312-7231 .");
				$('#jafnextMsgk').html("");
				$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' >"+resourceJSON.btnOk+"</button>");
				//$('#vcloseBtnk').html("<span>x</span>");
				//$('#myModalvk').modal('show');
				$('#epiDataAreaIdL2').show();
				
				showtopRightBtnL2();
				
				if(document.getElementById("bStatus"))
				{
					$('#bStatus').html(resourceJSON.msgtimeout1);
					$('#iconpophover3').trigger('mouseout');
					$('#bClick').html("");
				}
			}
			else
			{
				//alert("Other 2");
				
				var st_re = data.status.charAt(2)==''?0:data.status.charAt(2);
					
				$('#jafwarningImg').html("<img src='images/info.png' align='top'>");
				
				if(jobId>0)
				{
					//alert("Other 3");
					//alert(data.assessmentDescription);
					//alert(resourceJSON.msgAdditionalQuestions);
					$('#jafmessage2showConfirm').html(resourceJSON.msgAdditionalQuestions+"<BR>"+data.assessmentDescription+"<BR><BR>");
					$('#jafnextMsgk').html("");
					//$('#myModalLabelId').html(resourceJSON.msgAdditionalQuestions);
					$('#epiFooter').hide();
					
				}
				else
				{
					//alert("Other 4");
					//$('#myModalLabelId').html("TeacherMatch");
					
					if(data.ipaddress=='kelly' && jobId==0)
					{	
						//alert("Other 5");
						$('#jafmessage2showConfirm').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
						var infomsg = "resume";
						if(st_re==0 && jobId<=0)
							infomsg = "start";
						
						var mmms = "<p>Welcome valuable <b>Kelly Educational Staffing&reg; (KES&reg;)</b> talent. You are about to "+infomsg+" the <b>Educators Professional Inventory (EPI)</b>, a non-discriminatory, research-based teaching assessment developed by education experts. It evaluates full-time teaching candidates against four \"success indicators\" that education research has identified for highly effective teachers who will deliver academic growth: Qualifications, Teaching Skills, Cognitive Ability, and Attitudinal Factors.</p>"+
						"<p>KES is proud to partner with <b>TeacherMatch&trade;</b> to support and enhance our talent solutions to K-12 school districts. At the core, KES and TeacherMatch share a common philosophy and believe high quality substitute teachers are the most critical factor in fostering student achievement. Therefore, we're excited that we are able to offer you - a valued KES substitute teacher candidate - exclusive access to the EPI.</p>"+
						"<p><b>For more information about the exclusive benefits of EPI as a KES substitute teacher candidate, including how to receive your free Professional Development Plan, please contact Kelly Educational Staffing at 885-535-5915 or via email at <a href=\"mailto:KESTMSP@kellyservices.com\" target=\"_top\">KESTMSP@kellyservices.com</a>.</b></p>"+
						"<p><b>Important Note:</b> The TeacherMatch EPI is NOT used as a hiring assessment tool by KES, and your performance will NEVER be used as a qualifier or filter for you to be able to view or accept substitute-teaching positions with Kelly Educational Staffing school districts. Each item in the EPI has a stipulated time limit of 75 seconds. You must respond to each item within its stipulated time limit. You must answer all of the questions in one sitting.</p>"+
						"<p>We strive to maintain the integrity of the EPI and ensure that everyone has a fair and equal opportunity to complete the EPI. Therefore, before beginning, please note these IMPORTANT guidelines:</p>" +
						"<p>If you continue, please be prepared to follow these guidelines:<p><ul>" +
						"<li>While the average person takes approximately 45 minutes to complete, please reserve at least 90 minutes of uninterrupted time to complete it and avoid any 'timed out' issues Make sure that you have a stable and reliable internet connection </li>" +
						"<li> Do not close your browser or hit the \"back\" button on your browser</li>" +
						"<li> There are 75 questions - each one should take about 30 seconds to answer, but you have a maximum 75 seconds per question</li>" +
						"<li> A timer at corner of the screen will show you how much time you have left per question . . . and for the remainder of the test</li>" +
						"<li> You are not allowed to skip questions - it's to your advantage to try to answer each one within the allocated time, even if you must guess </li>" +
						"</ul>" +
						"<p>Accommodations</p>" +
						"<p><b>If you require accommodations for this assessment, we recommend that you contact your local KES branch</b>. Please click \"Ok\" if you are ready to take the EPI in its entirety otherwise, please click \"Cancel\" to exit.</p>" +
						"Best,<br><b>Kelly Educational Staffing&reg; (KES&reg;)</b>";
						
						$('#nextMsg').html(mmms);
						
					}
					else
					{
						//alert("Other 6");
						var msg = resourceJSON.msgEducatorsProfessionalInventory;
						if(jobId>0)
							msg = resourceJSON.msgJobSpecificInventory;
						var infomsg = "<p>"+resourceJSON.msgAboutResume+" "+msg+".";
						if(st_re==0 && jobId<=0)
							infomsg = "<p>"+resourceJSON.msgAboutStart+" "+msg+".";
						infomsg+=resourceJSON.msg100QuestionsInventory+"" +
								"<br><br>"+resourceJSON.msgEPIStipulatedTime75Seconds+"</p>";
						
						$('#jafmessage2showConfirm').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
						var nextMsg = infomsg+"<b>"+resourceJSON.msgPreparedTheseGuidelines+":</b><br><ul> "+
						"<li>"+resourceJSON.msg90MinutesUninterrupted+"</li> "+ 
						"<li>"+resourceJSON.msgStableReliableInternet+"</li> "+ 
						"<li>"+resourceJSON.msgBrowserButton+" </li>"+
						"<li>"+resourceJSON.msgAbleSkipQuestions+"</li> " +
						"<li>"+resourceJSON.msgComprehensiveOverviewTeacherMatch+" <a target='_blank' href='policiesandprocedures.do'>"+resourceJSON.msghere+"</a></li> " +
						"</ul>";
					
						nextMsg+="<p>"+resourceJSON.msgMaintainIntegrityEPI+"</p>";
						nextMsg+="<p>"+resourceJSON.msgUnableRetakeEPI12Months+"</p>";
						nextMsg+="<p><b>"+resourceJSON.msgAccommodations+"</b> </p>";
						nextMsg+="<p>"+resourceJSON.msgRequireAccommodationsAssessment+"</p>";
						
						$('#jafnextMsgk').html(nextMsg+" "+resourceJSON.msgEPIEntiretyPrepared+"");
					}
				}
					//alert("Other 7");
					if(data.assessmentType!=null && data.assessmentType==1)
					{
						$('#epiDataAreaId').hide();
					}
					$('#epiDataAreaIdL2').show();
					showtopRightBtnL2();
					//alert("Other 8");
					jOrder=jobOrder;
					vData=data;
					assessmentTakenCount=data.status.charAt(4);
			}
		}
	}
	});	
}

function startInventory()
{
	var data = vData;
	var jobOrder = jOrder;
	var loadingShow = true;
	if(data.status.indexOf("#")==-1)
	{
		$('#jafLoadingDiv').fadeIn();
		loadAssessmentQuestions(data,jobOrder);
		
	}
	else
	{
		var newJobId = jobOrder.jobId;
		var dd = data.status;
		var jId = dd.split("#")[3];
		
		var msg = "EPI";
		if(jId!=0)
		{
			jobOrder.jobId=jId;
			jOrder.jobId=jId;
			msg = "Job Specific Inventory";
		}
		
		if(data.status.charAt(2)==0 || data.status.charAt(2)==1)
		{
			data.status=data.status.charAt(0);
			$("#jafwarningImg").addClass("mt10");
			$('#jafwarningImg').html("<img src='images/warn.png' align='top'>");
			$('#jafmessage2showConfirm').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
			$('#jafnextMsgk').html(resourceJSON.msgExceededTimeLimit+" <font color='red'>"+resourceJSON.msgSECOND+"</font> "+resourceJSON.msgReminderRegardingImportance+" "+msg+". "+resourceJSON.msgStipulatedLimit);
			$('#bottombtn').html("<button class='flatbtn' style='width:100px;' id='btnConti' type='button' onclick='return runInventory(1,"+newJobId+");'>"+resourceJSON.BtnContinue+" <i class='icon'></i></button>");
			$('#epiDataAreaIdL2').show();
			
			// Top Right button according to bottom
			$('#topRightInvDivId').show();
			$('#topRightInvDivIdL2').html("<button class='flatbtn' style='width:100px;' id='btnConti' type='button' onclick='return runInventory(1,"+newJobId+");'>"+resourceJSON.BtnContinue+" <i class='icon'></i></button>");

		}
		else if(data.status.charAt(2)==2)
		{
			data.status=data.status.charAt(0);
			$("#jafwarningImg").addClass("mt10");
			$('#jafwarningImg').html("<img src='images/warn.png' align='top'>");
			$('#jafmessage2showConfirm').html("<b>"+resourceJSON.msgPleaseReadCarefully+"</b>");
			var nextMsg="<p>"+resourceJSON.msgexceededtimelimitforsec1 +msg+". "+resourceJSON.msgexceededtimelimitforsec2 +msg+" "+resourceJSON.msgitems+".</p>";
			
			if(jId==0)
			nextMsg += "<p>"+resourceJSON.msgTMStatRemain1+"</p><p>"+resourceJSON.msgTMStatRemain2+"</p>";
			
			//nextMsg += "<p>"+resourceJSON.msgTMPolicy1+" <a href='javascript:void(0);'>here</a>.</p><p> "+resourceJSON.msgTMPolicy2+" (888) 312-7231 "+resourceJSON.msgTMPolicy3+" <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</p> <p>"+resourceJSON.msgTMPolicy4+"</p>";
			nextMsg += "<p>If you would like to review TeacherMatch's policy regarding this issue in greater depth please click <a href='javascript:void(0);'>here</a>.</p><p> If you have any further questions regarding this matter, please call us at (888) 312-7231 or email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</p> <p>Thank you and best of luck in your job search.</p>";
			$('#jafnextMsgk').html(nextMsg);
			//$('#bottombtn').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"runInventory(1,"+newJobId+");\">"+resourceJSON.BtnContinue+" <i class='icon'></i></button>");
			$('#bottombtn').html("<button class='flatbtn' style='width:100px;' id='btnConti' type='button' onclick='return runInventory(1,"+newJobId+");'>"+resourceJSON.BtnContinue+" <i class='icon'></i></button>");
			$('#epiDataAreaIdL2').show();
			
			// Top Right button according to bottom
			$('#topRightInvDivId').show();
			$('#topRightInvDivIdL2').html("<button class='flatbtn' style='width:100px;' id='btnConti' type='button' onclick='return runInventory(1,"+newJobId+");'>"+resourceJSON.BtnContinue+" <i class='icon'></i></button>");
			
		}
	}
}

function loadAssessmentQuestions(assessmentDetail,jobOrder)
{
	var epiJobId=""; 
	try { epiJobId=document.getElementById("dspqJobId").value; } catch (e) {}
	DSPQServiceAjax.jafLoadAssessmentQuestions(assessmentDetail,jobOrder,epiJobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1)
		{
			runAssessment(assessmentDetail,jobOrder,0,0);
		}
		else if(data=='vlt')
		{
			$('#jafLoadingDiv').hide();
			$('#jafwarningImg').html("<img src='images/warn.png' align='top'>");
			$('#jafmessage2showConfirm').html( resourceJSON.msgTimeutInvt );
			$('#jafnextMsgk').html("");
			$('#bottombtn').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#epiDataAreaIdL2').show();
			
			showtopRightBtnL2();
			
		}
		else if(data=='comp')
		{
			try{
				$('#loadingDivInventory').hide();
				$('#jafwarningImg').html("<img src='images/info.png' align='top'>");
				$('#jafmessage2showConfirm').html("You have already completed this inventory.");
				$('#jafnextMsgk').html("");
				$('#bottombtn').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
				$('#epiDataAreaIdL2').show();
				
				showtopRightBtnL2();
				
				}
				catch(err)
				{}
		}
		else if(data=='icomp')
		{
			$('#jafLoadingDiv').hide();
		}
	}
	});	
	
}

function runAssessment(assessmentDetail,jobOrder,doInsert,newJobId)
{
	//alert("runAssessment");
	$('#jafLoadingDiv').fadeIn();
	//alert("doInsert "+doInsert);
	if(doInsert==1)
	{
		//alert("Yes 1");
		insertTeacherAssessmentAttempt(assessmentDetail,jobOrder);
		// last Attempt insertion
		//insertLastAttempt(jobOrder);
		//alert("Yes 2");
		insertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder);
	}
	//alert("3");
	
	var dspqJobId=""; 
	try { dspqJobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	var dspqjobflowdistrictId="";
	try { dspqjobflowdistrictId=document.getElementById("dspqjobflowdistrictId").value; } catch (e) {}
	
	var dspqPortfolioNameId="";
	try { dspqPortfolioNameId=document.getElementById("dspqPortfolioNameId").value; } catch (e) {}
	
	var redirectURL = "jobapplicationflowrunassessmentstep2.do?assessmentId="+assessmentDetail.assessmentId+"&jobId="+jobOrder.jobId+"&newJobId="+newJobId+"&dspqJobId="+dspqJobId+"&ct="+dspqct;
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
	
	//$('#jafLoadingDiv').fadeOut();
	//alert("We are working on show EPI Question and Instruction");
	
}

function insertTeacherAssessmentAttempt(assessmentDetail,jobOrder)
{
	DSPQServiceAjax.jafinsertTeacherAssessmentAttempt(assessmentDetail,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}

function insertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder)
{
	DSPQServiceAjax.jafinsertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}

function runInventory(flg,newJobId)
{
	var data = vData;
	var jobOrder = jOrder;
	runAssessment(data,jobOrder,1,newJobId);
}


function getAssessmentSectionCampaignQuestionsGrid(whoClicked)
{
	try
	{
		var attemptId = dwr.util.getValue("attemptId");
		totalAttempted = dwr.util.getValue("totalAttempted");
		var assessmentDetail = {assessmentId:dwr.util.getValue("assessmentId")};
		
		//alert("dwr.util.getValue('teacherAssessmentType') "+dwr.util.getValue("teacherAssessmentType"));
		
		var teacherAssessmentdetail = {teacherAssessmentId:dwr.util.getValue("teacherAssessmentId"),assessmentType:dwr.util.getValue("teacherAssessmentType")};
		var teacherAssessmentQuestion // = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
		var teacherSectionDetail //= {teacherSectionId:dwr.util.getValue("teacherSectionId")};
		var arr =[];
		if(document.getElementById("teacherAssessmentQuestionId"))
		{
			teacherAssessmentQuestion = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
			if(document.getElementById("teacherAssessmentQuestionId").value!="")
			{
				var opts=document.getElementsByName("opt");
				var questionTypeShortName = dwr.util.getValue("questionTypeShortName");
				var questionTypeMaster = {questionTypeId:dwr.util.getValue("questionTypeId")};
				var qType = dwr.util.getValue("questionTypeShortName");
				var questionWeightage = dwr.util.getValue("questionWeightage");
				var questionSessionTime = dwr.util.getValue("questionSessionTime");
	
				var o_maxMarks = dwr.util.getValue("o_maxMarks");
				if(qType=='tf' || qType=='slsel' || qType=='lkts' || qType=='it')
				{
	
					var optId="";
					var score=0;
					if($('input[name=opt]:radio:checked').length > 0 )
					{
						optId=$('input[name=opt]:radio:checked').val();
						var opts = document.getElementsByName("opt");
						var scores = document.getElementsByName("score");
						for (var i = 0; i < opts.length; i++) {
							if(optId==opts[i].value)
							{
								score = scores[i].value;
								break;
							}
						}
					}
					var totalScore = questionWeightage*score;
					if(optId=='')
					{
						totalSkippedQuestions++;
						//strike checking
						checkStrikes(questionSessionTime,whoClicked);
	
						return;
					}
	
					arr.push({ 
						"selectedOptions"  : optId,
						"optionScore"      : score,
						"totalScore"       : totalScore,
						"questionWeightage" : questionWeightage,
						"questionTypeMaster" : questionTypeMaster,
						"jobOrder" : jobOrder,
						"maxMarks" :o_maxMarks
					});
	
				}else if(qType=='rt')
				{
					var optId="";
					var score="";
					var rank="";
					var scoreRank=0;
					var opts = document.getElementsByName("opt");
					var scores = document.getElementsByName("score");
					var ranks = document.getElementsByName("rank");
					var o_ranks = document.getElementsByName("o_rank");
					var tt=0;
					var uniqueflag=false;
					for(var i = 0; i < opts.length; i++) {
						optId += opts[i].value+"|";
						score += scores[i].value+"|";
						rank += ranks[i].value+"|";
	
						if(checkUniqueRank(ranks[i]))
						{
							uniqueflag=true;
							break;
						}
	
						if(ranks[i].value==o_ranks[i].value)
						{
							scoreRank+=parseInt(scores[i].value);
							//alert(scoreRank);
						}
						if(ranks[i].value=="")
						{
							tt++;
						}
					}
					/////////////////////////////////////
					if(uniqueflag)
						return;
	
					if(tt!=0)
						optId=""; 
	
					if(optId=="")
					{
						totalSkippedQuestions++;
						//strike checking
						checkStrikes(questionSessionTime,whoClicked);
						return;
					}
	
					var totalScore = questionWeightage*scoreRank;
					arr.push({ 
						"selectedOptions"  : optId,
						"optionScore"      : score,
						"totalScore"       : totalScore,
						"insertedRanks"    : rank,
						"questionWeightage" : questionWeightage,
						"questionTypeMaster" : questionTypeMaster,
						"jobOrder" : jobOrder,
						"maxMarks" :o_maxMarks
					});
	
				}else if(qType=='sl' || qType=='ml')
				{
					if(dwr.util.getValue("opt")=='')
					{
						totalSkippedQuestions++;
						//strike checking
						checkStrikes(questionSessionTime,whoClicked);
	
						return;
					}
	
					arr.push({ 
						"insertedText"    : dwr.util.getValue("opt"),
						"questionTypeMaster" : questionTypeMaster,
						"totalScore"       : 0,
						"questionWeightage" : 0,
						"jobOrder" : jobOrder,
						"maxMarks" :o_maxMarks
					});
				}
				else if(qType=='mlsel')
				{
					var optId="";
					var score=0;
					if($('input[name=opt]:checked').length > 0 )
					{
						optId=$('input[name=opt]:checked').val();
						var opts = document.getElementsByName("opt");
						var scores = document.getElementsByName("score");
						var optsValues="";
						for (var i = 0; i < opts.length; i++) {
							if(opts[i].checked==true)
							{
								if(optsValues=="")
								{
									optsValues=opts[i].value+"|";
								}
								else
								{
									optsValues=optsValues+opts[i].value+"|";
								}
							}
						}
					}
					var totalScore = questionWeightage*score;
					if(optId=='')
					{
						totalSkippedQuestions++;
						//strike checking
						checkStrikes(questionSessionTime,whoClicked);
	
						return;
					}
	
					arr.push({ 
						"selectedOptions"  : optsValues,
						"optionScore"      : score,
						"totalScore"       : totalScore,
						"questionWeightage" : questionWeightage,
						"questionTypeMaster" : questionTypeMaster,
						"jobOrder" : jobOrder,
						"maxMarks" :o_maxMarks
					});
	
				}
				else if(qType=='mloet')
				{
					var insertedText = dwr.util.getValue("insertedText");
					var optId="";
					var score=0;
					if($('input[name=opt]:checked').length > 0 )
					{
						optId=$('input[name=opt]:checked').val();
						var opts = document.getElementsByName("opt");
						var scores = document.getElementsByName("score");
						var optsValues="";
						for (var i = 0; i < opts.length; i++) {
							if(opts[i].checked==true)
							{
								if(optsValues=="")
								{
									optsValues=opts[i].value+"|";
								}
								else
								{
									optsValues=optsValues+opts[i].value+"|";
								}
							}
						}
					}
					var totalScore = questionWeightage*score;
					if(optId=='')
					{
						totalSkippedQuestions++;
						//strike checking
						checkStrikes(questionSessionTime,whoClicked);
	
						return;
					}
	
					arr.push({ 
						"selectedOptions"  : optsValues,
						"optionScore"      : score,
						"totalScore"       : totalScore,
						"questionWeightage" : questionWeightage,
						"questionTypeMaster" : questionTypeMaster,
						"jobOrder" : jobOrder,
						"maxMarks" :o_maxMarks,
						"insertedText"    : insertedText
					});
	
				}
				else if(qType=='sloet')
				{
					var insertedText = dwr.util.getValue("insertedText");
					var optId="";
					var score=0;
					if($('input[name=opt]:radio:checked').length > 0 )
					{
						optId=$('input[name=opt]:radio:checked').val();
						var opts = document.getElementsByName("opt");
						var scores = document.getElementsByName("score");
						for (var i = 0; i < opts.length; i++) {
							if(optId==opts[i].value)
							{
								score = scores[i].value;
								break;
							}
						}
					}
					var totalScore = questionWeightage*score;
					if(optId=='')
					{
						totalSkippedQuestions++;
						//strike checking
						checkStrikes(questionSessionTime,whoClicked);
	
						return;
					}
	
					arr.push({ 
						"selectedOptions"  : optId,
						"optionScore"      : score,
						"totalScore"       : totalScore,
						"questionWeightage" : questionWeightage,
						"questionTypeMaster" : questionTypeMaster,
						"jobOrder" : jobOrder,
						"maxMarks" :o_maxMarks,
						"insertedText"    : insertedText
					});
	
				}
				else
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);
					return;
	
					arr.push({ 
						"questionWeightage" : questionWeightage,
						"questionTypeMaster" : questionTypeMaster,
						"totalScore" : 0,
						"jobOrder" : jobOrder,
						"maxMarks" :o_maxMarks
					});
				}
	
			}
	
		}
	
		if(document.getElementById("teacherSectionId"))
			teacherSectionDetail = {teacherSectionId:dwr.util.getValue("teacherSectionId")};
	
		var newJobId = document.getElementById("newJobId").value;
		$('#sectionName').show();
		$("#sbmtbtn").attr("disabled", "disabled");
		DSPQServiceAjax.jafgetAssessmentSectionCampaignQuestions(assessmentDetail,teacherAssessmentdetail,teacherAssessmentQuestion,teacherSectionDetail,arr,attemptId,newJobId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			//alert("data "+data);
			window.clearInterval(intervalID);
	
			arr =[];
			$("#sbmtbtn").attr("disabled", false);
			
			try{$('#tblGrid').html(data);
			}catch(e)
			{}
		}
		});	
	}
	catch(e)
	{alert("e====="+e)}
}

function checkStrikes(questionSessionTime,whoClicked)
{	
	//alert("questionSessionTime "+questionSessionTime+" whoClicked "+whoClicked);
	if(questionSessionTime==0)
	{
		$("#jafinventoryQuestion").addClass("DSPQRequired12");
		$("#lbljafInvenWarning").addClass("icon-warning DSPQRequired12");
		$("#lbljafInvenWarningAfterSpace").html("&nbsp;");
		$("#lbljafInvenWarningBeforeSpace").html("&nbsp;");
	}
	else
	{
		if(whoClicked==1)
		{
			$("#jafinventoryQuestion").addClass("DSPQRequired12");
			$("#lbljafInvenWarning").addClass("icon-warning DSPQRequired12");
			$("#lbljafInvenWarningAfterSpace").html("&nbsp;");
			$("#lbljafInvenWarningBeforeSpace").html("&nbsp;");
						
			var remSec = parseInt($('.min').html()==null?0:$('.min').html())*60 +parseInt($('.sec').html());
			if(remSec>0)
			{
				return;
			}
		}
		else
		{
			strikeCheck(document.getElementById("teacherAssessmentQuestionId").value,whoClicked);
		}
	}
}

function strikeCheck(teacherAssessmentQuestionId,whoClicked)
{
	var teacherAssessmentType = dwr.util.getValue("teacherAssessmentType");
	var moreMsg = ".";
	if(teacherAssessmentType==1)
		moreMsg = ".";
		
	$('#tm-root').html("");
	var questionSessionTime = 0;
	if(document.getElementById("questionSessionTime"))
		questionSessionTime = dwr.util.getValue("questionSessionTime")==null?0:dwr.util.getValue("questionSessionTime");

	var totalChances = parseInt(totalAttempted) + totalStrikes;
	if(totalChances==1  && (whoClicked!=2))
	{
		var onclick = "";
		onclick = "onclick=\"stopTimer(),tmTimer("+questionSessionTime+",'1')\"";
		
		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='images/warn.png' align='top'>");
		$('#message2showConfirm1').html(resourceJSON.msgTimedOutthestipulatedtime);
		$('#nextMsg').html( resourceJSON.msgreadytoStrt1+" <font color='red'>"+resourceJSON.msgSECOND+"</font> "+resourceJSON.msgreadytoStrt2 + moreMsg);
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' "+onclick+" aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
		$('#vcloseBtn').html("<span "+onclick+" >x</span>");
		$('#myModalv').modal('show');

		if(questionSessionTime>0 && whoClicked==0)
		{
			window.clearInterval(intervalID);
			saveStrike(teacherAssessmentQuestionId);
			timeout =  setTimeout('redirectToDashboard('+teacherAssessmentType+')', 10000);
		}
		return;
	}
	if(totalChances==2  && (whoClicked!=2))
	{
		var onclick = "";
		onclick = "onclick=\"stopTimer(),tmTimer("+questionSessionTime+",'1')\"";
		
		$('#warningImg1').html("<img src='images/warn.png' align='top'>");
		$('#message2showConfirm1').html(  resourceJSON.msgTimedOutthestipulatedtime2 );
		$('#nextMsg').html( resourceJSON.msgreadytoStrt1+" <font color='red'>"+resourceJSON.msgFINAL+"</font> "+resourceJSON.msgreadytoStrt2 +moreMsg);
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' "+onclick+" aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
		$('#vcloseBtn').html("<span "+onclick+" >x</span>");
		$('#myModalv').modal('show');

		if(questionSessionTime>0 && whoClicked==0)
		{
			window.clearInterval(intervalID);
			saveStrike(teacherAssessmentQuestionId);
			timeout = setTimeout('redirectToDashboard('+teacherAssessmentType+')', 10000);

		}
		return;
	}else if((totalAttempted>=1 && totalStrikes>=1) || (totalAttempted>=2 && totalStrikes==0)) //if(totalChances>=3)
	{
	
		if(whoClicked==0)
		{
			window.clearInterval(intervalID);
		}

		finishAssessment(0);
		return;
	}

}

function finishAssessment(sessionCheck)
{
	$('#myModalvk').modal('hide');
  	$('#myModalv').modal('hide');
	var newJobId = document.getElementById("newJobId").value;
	
	DSPQServiceAjax.jafFinishAssessment(teacherAssessmentdetail,jobOrder,newJobId,sessionCheck,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#tm-root').html(data);
	}
	});	
}

function checkAssessmentDone(teacherAssessmentId)
{
	var teacherAssessmentdetail = {teacherAssessmentId:teacherAssessmentId}
	DSPQServiceAjax.jafCheckAssessmentDone(teacherAssessmentdetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==0)
		{
			getAssessmentSectionCampaignQuestionsGrid(0);
		}
		else
		{
			finishAssessment(0)
		}
	}
	});	
}


function stopTimer()
{
	clearTimeout(timeout);
}

function saveStrike(teacherAssessmentQuestionId)
{
	if(teacherAssessmentQuestionId==0)
		return;

	var teacherAssessmentQuestion = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
	var teacherAssessmentdetail = {teacherAssessmentId:dwr.util.getValue("teacherAssessmentId"),assessmentType:dwr.util.getValue("teacherAssessmentType")};
	DSPQServiceAjax.jafSaveStrike(teacherAssessmentdetail,teacherAssessmentQuestion,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		totalStrikes++;
	}
	});	
}

function jafRedirectInventory(sts,redirectURL,assessmentType)
{
	var jobId=""; 
	try { jobId=document.getElementById("dspqJobId").value; } catch (e) {}

	var dspqct=""; 
	try { dspqct=document.getElementById("dspqct").value; } catch (e) {}
	
	window.location.href=redirectURL+"?jobId="+jobId+"&ct="+dspqct+"&sts="+sts+"&at="+assessmentType;
	
}

function jafEPISuccessMsg()
{
	var sts=""; 
	try { sts=document.getElementById("sts").value; } catch (e) {}

	var at=""; 
	try { at=document.getElementById("at").value; } catch (e) {}
	
	if(sts=="0")
	{
		var txtEPIStatus="";
		try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
		if(txtEPIStatus=="comp")
		{
			$('#epiSuccessMsg').show();
		}
	}
	else
	{
		$('#epiSuccessMsg').hide();
	}
}


function jafJSISuccessMsg()
{
	var sts=""; 
	try { sts=document.getElementById("sts").value; } catch (e) {}

	var at=""; 
	try { at=document.getElementById("at").value; } catch (e) {}
	
	if(sts=="0")
	{
		$('#jsiSuccessMsg').show();	
	}
	else
	{
		$('#jsiSuccessMsg').hide();
	}
}


function showtopRightBtnL2()
{
	try { $('#topRightInvDivId').show();	} catch (e) {}
	try { $('#toprightbtnInvCompVlt').hide();	} catch (e) {}
	try { $('#toprightbtnInvL2').hide();	} catch (e) {}
	try { $('#toprightbtnInvL3').show();	} catch (e) {}
}
