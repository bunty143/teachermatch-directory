function cameraCheck()
{
	if(document.getElementById("browsepic").style.display=='inline'){
		document.getElementById("camera").innerHTML='Detected';
	}else{
		document.getElementById("camera").innerHTML='Not Detected';
	}
}

function cmpVersion(a, b) {
	//alert("a=="+a+" and b =="+b);
    var i, cmp, len, re = /(\.0)+[^\.]*$/;
    a = (a + '').replace(re, '').split('.');
    b = (b + '').replace(re, '').split('.');
    len = Math.min(a.length, b.length);
    for( i = 0; i < len; i++ ) {
        cmp = parseInt(b[i], 10) - parseInt(a[i], 10);
        if( cmp !== 0 ) {
            return cmp;
        }
    }
    return b.length - a.length;
}

function DetectBrowser()
{
	//cameraCheck();
	var recomVersion=0;
	var nVer = navigator.appVersion;
	var nAgt = navigator.userAgent;
	var browserName  = navigator.appName;
	var fullVersion  = ''+parseFloat(navigator.appVersion); 
	var majorVersion = parseInt(navigator.appVersion,10);
	var nameOffset,verOffset,ix;

	// In Opera, the true version is after "Opera" or after "Version"
	if ((verOffset=nAgt.indexOf("Opera"))!=-1) {
	 browserName = "Opera";
	 fullVersion = nAgt.substring(verOffset+6);
	 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
	   fullVersion = nAgt.substring(verOffset+8);
	}
	// In MSIE, the true version is after "MSIE" in userAgent
	else if ((verOffset=nAgt.indexOf("MSIE"))!=-1) {
	 browserName = "Microsoft Internet Explorer";
	 fullVersion = nAgt.substring(verOffset+5);
	 recomVersion='7';
	}
	// In Chrome, the true version is after "Chrome" 
	else if ((verOffset=nAgt.indexOf("Chrome"))!=-1) {
	 browserName = "Chrome";
	 fullVersion = nAgt.substring(verOffset+7);
	 recomVersion='26.0.1410.64';
	}
	// In Safari, the true version is after "Safari" or after "Version" 
	else if ((verOffset=nAgt.indexOf("Safari"))!=-1) {
	 browserName = "Safari";
	 fullVersion = nAgt.substring(verOffset+7);
	 if ((verOffset=nAgt.indexOf("Version"))!=-1) 
	   fullVersion = nAgt.substring(verOffset+8);
	 recomVersion='5.1.5';
	}
	// In Firefox, the true version is after "Firefox" 
	else if ((verOffset=nAgt.indexOf("Firefox"))!=-1) {
	 browserName = "Firefox";
	 fullVersion = nAgt.substring(verOffset+8);
	 recomVersion='20.0';
	}
	// In most other browsers, "name/version" is at the end of userAgent 
	else if ( (nameOffset=nAgt.lastIndexOf(' ')+1) < 
			  (verOffset=nAgt.lastIndexOf('/')) ) 
	{
	 browserName = nAgt.substring(nameOffset,verOffset);
	 fullVersion = nAgt.substring(verOffset+1);
	 if (browserName.toLowerCase()==browserName.toUpperCase()) {
	  browserName = navigator.appName;
	 }
	}
	// trim the fullVersion string at semicolon/space if present
	if ((ix=fullVersion.indexOf(";"))!=-1)
	   fullVersion=fullVersion.substring(0,ix);
	if ((ix=fullVersion.indexOf(" "))!=-1)
	   fullVersion=fullVersion.substring(0,ix);

	majorVersion = parseInt(''+fullVersion,10);
	if (isNaN(majorVersion)) {
	 fullVersion  = ''+parseFloat(navigator.appVersion); 
	 majorVersion = parseInt(navigator.appVersion,10);
	}

	if(recomVersion!=0)
	{
		//alert(cmpVersion(recomVersion, fullVersion));
		if(cmpVersion(recomVersion, fullVersion)>=0)
		{
			document.getElementById("browserVersion").innerHTML = ''
				+''+browserName+' '
				 +''+fullVersion+' <img src=\'images/green_ok.jpg\'/>';
		}
		else
		{
			document.getElementById("browserVersion").innerHTML = ''
				+''+browserName+' '
				 +''+fullVersion+' <img src=\'images/red_cancel.png\'/>';
		}
	}
     
	/*
	document.getElementById("browserVersion").innerHTML = ''
	+''+browserName+' '
	 +''+fullVersion+'<br>';
	*/
    //pop_up
   /* var win = window.open(null,
                          'ReportViewer',
                          'width=0px,height=0px,left=50px,top=20px,location=no,directories=no,status=no,menubar=no,toolbar=no,resizable=1,maximize:yes,scrollbars=0');

   if (win == null || typeof(win) == "undefined" || (win == null && win.outerWidth == 0) || (win != null && win.outerHeight == 0) || win.test == "undefined") 
    {

        document.getElementById("pop").innerHTML='Disable'
		//alert("The popup was blocked. You must allow popups to use this site.");  
    }
    else if (win)
    {
        win.onload = function()
        {          
            if (win.screenX === 0) {
				        document.getElementById("pop").innerHTML='Disable';
                //alert("The popup was blocked. You must allow popups to use this site.");
                win.close();
            } 
			else{
				win.close();
				    document.getElementById("pop").innerHTML='Enable';
			}
        };
    }else{
 		win.close();
		    document.getElementById("pop").innerHTML='Enable';
	}
*/


var getAcrobatInfo = function() {
 
  var getBrowserName = function() {
    return this.name = this.name || function() {
      var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";
 
      if(userAgent.indexOf("chrome") > -1)        return "chrome";
      else if(userAgent.indexOf("safari") > -1)   return "safari";
      else if(userAgent.indexOf("msie") > -1)     return "ie";
      else if(userAgent.indexOf("firefox") > -1)  return "firefox";
      return userAgent;
    }();
  };
 
  var getActiveXObject = function(name) {
    try { return new ActiveXObject(name); } catch(e) {}
  };
 
  var getNavigatorPlugin = function(name) {
    for(key in navigator.plugins) {
      var plugin = navigator.plugins[key];
      if(plugin.name == name) return plugin;
    }
  };
 
  var getPDFPlugin = function() {
    return this.plugin = this.plugin || function() {
      if(getBrowserName() == 'ie') {
        //
        // load the activeX control
        // AcroPDF.PDF is used by version 7 and later
        // PDF.PdfCtrl is used by version 6 and earlier
        return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
      }
      else {
        return getNavigatorPlugin('Adobe Acrobat') || getNavigatorPlugin('Chrome PDF Viewer') || getNavigatorPlugin('WebKit built-in PDF');
      }
    }();
  };
 
  var isAcrobatInstalled = function() {
    return !!getPDFPlugin();
  };
 
  var getAcrobatVersion = function() {
    try {
      var plugin = getPDFPlugin();
 
      if(getBrowserName() == 'ie') {
        var versions = plugin.GetVersions().split(',');
        var latest   = versions[0].split('=');
        return parseFloat(latest[1]);
      }
 
      if(plugin.version) return parseInt(plugin.version);
      return plugin.name
      
    }
    catch(e) {
      return null;
    }
  }
 
  //
  // The returned object
  // 
  return {
    browser:        getBrowserName(),
    acrobat:        isAcrobatInstalled() ? 'Available <img src=\'images/green_ok.jpg\'/>' : 'Unavailable <img src=\'images/red_cancel.png\'/>',
    acrobatVersion: getAcrobatVersion()
  };
};
var info = getAcrobatInfo();
//alert(info);
//alert(info.browser+ " " + info.acrobat + " " + info.acrobatVersion);
//alert(info.acrobat);
 document.getElementById("plugin").innerHTML=info.acrobat;





//CHECK COOKIE
 var cookieEnabled = (navigator.cookieEnabled) ? 'Enabled <img src=\'images/green_ok.jpg\'/>' : 'Disabled <img src=\'images/red_cancel.png\'/>';

	if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled)
	{ 
		document.cookie="testcookie";
		cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
	}
	 document.getElementById("cook").innerHTML=cookieEnabled;

//check_flash
	/*if(!FlashDetect.installed){
		document.getElementById("flash").innerHTML="Flash is required. <img src='images/red_cancel.png'/>";
	}else{
	    document.getElementById("flash").innerHTML="Installed <img src='images/green_ok.jpg'/>";
	}*/

}
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}

window.onload = DetectBrowser;

var myQueryString=document.location.search;

function detectPopupBlocker()
{
	$('#loadingDiv').fadeIn();
	var myTest = window.open("about:blank","","height=100,width=100,status=no,titlebar=no,top=0,location=no");
	if (!myTest)
	{
		 document.getElementById("pop").innerHTML='Disable';
	}
	else
	{
		myTest.close();
		
		 document.getElementById("pop").innerHTML='Enable';
	}
	$('#loadingDiv').hide();
	
}