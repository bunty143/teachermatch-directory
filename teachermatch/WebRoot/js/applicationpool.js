/* @Author: Gagan 
 * @Discription: view of application pool js.
*/
/*========  For Sorting  ===============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

var pageHired = 1;
var noOfRowsHired = 10;
var sortOrderStrHired="";
var sortOrderTypeHired="";

var pageNotes = 1;
var noOfRowsNotes = 10;
var sortOrderStrNotes="";
var sortOrderTypeNotes="";
function getPaging(pageno)
{
	var gridNo	=	document.getElementById("gridNo").value;
	//alert('gridNo'+gridNo);
	if(gridNo==3)
	{
		if(pageno!='')
		{
			pageNotes=pageno;	
		}
		else
		{
			pageNotes=1;
		}
		
		noOfRowsNotes 	=	document.getElementById("pageSize3").value;
		teacherId = document.getElementById("teacherId").value;
		getNotesDiv(teacherId);
	}
	if(gridNo==1)
	{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		
		noOfRows 	=	document.getElementById("pageSize").value;
		displayApplicantGrid();
	}
	else
	{
		if(gridNo==2)
		{
			if(pageno!='')
			{
				pageHired=pageno;	
			}
			else
			{
				pageHired=1;
			}
			
			noOfRowsHired 	=	document.getElementById("pageSize1").value;
			displayHiredApplicantGrid();
		}
	}
}

function cancelNotes()
{
	$('#divTxtNode').find(".jqte_editor").html("");		
	$('#myModalNotes').modal('show');
	$('#errordivNotes').empty();
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	document.getElementById("spnBtnSave").style.display="inline";
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var gridNo	=	document.getElementById("gridNo").value;
	if(gridNo==3)
	{
		if(pageno!=''){
			pageNotes=pageno;	
		}else{
			pageNotes=1;
		}
		
		sortOrderStrNotes	=	sortOrder;
		sortOrderTypeNotes	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsNotes = document.getElementById("pageSize3").value;
		}else{
			noOfRowsNotes=10;
		}
		teacherId = document.getElementById("teacherId").value;
		getNotesDiv(teacherId);
	}
	if(gridNo==1)
	{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=10;
		}
	
		displayApplicantGrid();
	}
	else
	{
		if(gridNo==2)
		{
			if(pageno!='')
			{
				pageHired=pageno;	
			}
			else
			{
				pageHired=1;
			}
			sortOrderStrHired	=	sortOrder;
			sortOrderTypeHired	=	sortOrderTyp;
			if(document.getElementById("pageSize1")!=null)
			{
				noOfRowsHired = document.getElementById("pageSize1").value;
			}
			else
			{
				noOfRows=10;
			}
			displayHiredApplicantGrid();
		}
	}
	
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

function displayApplicantGrid()
{
	var jobId 	=	document.getElementById("jobId").value;
	//alert(" sortOrderStr Value  "+sortOrderStr); 
	ApplicantPoolAjax.displayApplicantGrid(jobId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		//alert(" data "+data);
		$('#applicantGrid').html(data);
		applyScrollOnTbl();
		tpAppEnable();
		
	},
	errorHandler:handleError  
	});
}
function tpAppEnable()
{
	var noOrRow = document.getElementById("applicantTable").rows.length;
	//var noOrRow = 10;
	for(var j=1;j<=noOrRow;j++)
	{
		
		$('#tpResume'+j).tooltip();
		$('#tpPortfolio'+j).tooltip();
		$('#tpMsg'+j).tooltip();
		$('#tpNotes'+j).tooltip();
		$('#tpPDReport'+j).tooltip();
		
	}
}
function displayHiredApplicantGrid()
{
	var jobId 	=	document.getElementById("jobId").value;
	//alert(" sortOrderStr Value  "+sortOrderStr); 
	ApplicantPoolAjax.displayHiredApplicantGrid(jobId,noOfRowsHired,pageHired,sortOrderStrHired,sortOrderTypeHired,{ 
		async: true,
		callback: function(data){
		//alert(" data "+data);
		$('#applicantHiredGrid').html(data);
		applyScrollOnTblHired();
	},
	errorHandler:handleError  
	});
}
function getSortFirstGrid()
{
	$('#gridNo').val("1");
}

function getSortSecondGrid()
{
	$('#gridNo').val("2");
}
function getSortThirdNotesGrid()
{
	$('#gridNo').val("3");
}
function getNotesDiv(teacherId,jobId)
{
	currentPageFlag='notes';
	document.getElementById("teacherId").value=teacherId;	
	$('#divTxtNode').find(".jqte_editor").html("");		
	$('#myModalNotes').modal('show');	
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	$('#errordivNotes').empty();
	//alert(noOfRows+" "+sortOrderStr+" "+sortOrderType+" page "+page);
	CandidateReportAjax.getNotesDetail(teacherId,pageNotes,noOfRowsNotes,sortOrderStrNotes,sortOrderTypeNotes,
	{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert("data "+data);
			document.getElementById("divNotes").innerHTML=data;
			applyScrollOnNotesTbl();
		}
	});
}////////// Note scroll 
function applyScrollOnNotesTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblNotes').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,150,475,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function saveNotes()
{	
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var noteId = document.getElementById("noteId").value;
	
	var cnt=0;
	var focs=0;	
	$('#errordivNotes').empty();
	//if ($('#divTxtNode').find(".jqte_editor").text().trim()=="")
	//if (trim($('#divTxtNode').find(".jqte_editor").text())=="")
	if ($('#divTxtNode').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
		if(focs==0)
			$('#divTxtNode').find(".jqte_editor").focus();
		$('#divTxtNode').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;	
	}
	else
	{	
		CandidateReportAjax.saveNotes(teacherId,notes,noteId,
		{
			async:false,
			errorHandler:handleError,
			callback:function(data)
			{
				getNotesDiv(teacherId)					
			}
		});	
	}
}
function editNoted(notesId, viewOnly)
{	
	$('#errordivAct').empty();
	CandidateReportAjax.showEditNotes(notesId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("noteId").value=data.noteId;
			$('#divTxtNode').find(".jqte_editor").html(data.note);
			
			$('#divTxtNode').find(".jqte_editor").focus();
			if(viewOnly!=undefined)
				document.getElementById("spnBtnSave").style.display="none";
			else
				document.getElementById("spnBtnSave").style.display="inline";
		}
	});
}
function downloadResume(teacherId,linkId)
{
	$('#loadingDiv').show();
	CandidateReportAjax.downloadResume(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			
			$('#loadingDiv').hide();
			if(data=="")			
				alert(resourceJSON.ResumeNotUploaded)
			else				
			{
				if (data.indexOf(".doc") !=-1) {
				    document.getElementById('ifrmResume').src = ""+data+"";
				}
				else
				{
					//window.open(data);
					document.getElementById(linkId).href = data; 
					return false;
				}
			}
			
			
		}
	});
}

function downloadPortfolioReport(teacherId, linkId)
{	
	$('#loadingDiv').show();
	CandidateReportAjax.downloadPortfolioReport(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data=="")
			{
					document.getElementById(linkId).href = "javascript:void(0)";
					alert(resourceJSON.MsgPortfolioNotCompleted)
					return true;
			}
			else	
			{
				
				//window.location.href=data;
  				//window.focus();
				document.getElementById(linkId).href = data; 
				return false;
			}
				
		}
	});
}
function getMessageDiv(teacherId,emailId,jobId)
{
	document.getElementById("teacherDetailId").value=teacherId;	
	document.getElementById("emailId").value=emailId;
	document.getElementById("jobId").value=jobId;
	document.getElementById("emailDiv").innerHTML=emailId;
	$('#messageSend').find(".jqte_editor").html("");
	document.getElementById("messageSubject").value="";
	$('#myModalMessage').modal('show');	
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	$('#messageSubject').focus();
	$('#errordivMessage').empty();
}
function validateMessage()
{
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	$('#errordivMessage').empty();
	
	var cnt=0;
	var focs=0;
		if(trim(messageSubject)=="")
		{
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
			if(focs==0)
				$('#messageSubject').focus();
			$('#messageSubject').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if ($('#messageSend').find(".jqte_editor").text().trim()==""){

			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
			if(focs==0)
				$('#messageSend').find(".jqte_editor").focus();
			$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else{
			var charCount=$('#messageSend').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			if(count>1000)
 			{
				$('#errordivMessage').append("&#149; "+resourceJSON.MsgLgthNotExceed+"<br>");
				if(focs==0)
					$('#messageSend').find(".jqte_editor").focus();
				$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		
		
	if(cnt==0){
		$('#lodingImage').append("<img src=\"images/loadingAnimation.gif\" /> "+resourceJSON.msgSending);
		CandidateReportAjax.saveMessage(teacherId,messageSubject,messageSend,jobId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				hideMessage();
			}
		});
		return true;
	}else
	{
		$('#errordivMessage').show();
		return false;
	}
}

function hideMessage()
{	
	$('#lodingImage').empty();
	$('#myModalMessage').modal('hide');
	
	$('#message2show').html(resourceJSON.MsgSentToCandidate);
	$('#myModal2').modal('show');
		
	document.getElementById('messageSubject').value="";
	document.getElementById('messageSend').value="";
}
