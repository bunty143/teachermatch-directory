var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

var disableIds = [];


var txtBgColor = "#F5E7E1";
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";


function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayAssessmentSlotDetails();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayAssessmentSlotDetails();
}
function scheduler_save(e) {
	//alert('scheduler_save::::');
	// saveAssessmentInvite(e);
}
function scheduler_add (e) {
   // kendoConsole.log("save");
}
/*function getDayByDate(date){
    var d = new Date(date);
    var weekday = new Array(7);
    weekday[0]=  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    var n = weekday[d.getDay()];
    return n;
}*/
var tmpCnt=0;
var array=null;
var enableDays=[];
var colorDays=[];
function displayExamCalendar() 
{
	
	
	$("#displayExamCalendarDiv").show();
	$('#loadingDiv').show();
    var districtAssessmentId=document.getElementById("districtAssessmentId").value;
    var centreScheduleId=document.getElementById("centreScheduleId").value;
    var teacherId=document.getElementById("teacherId").value;
    var jobId=document.getElementById("jobId").value;
    var startDate = "";
    if($("#examDate").val()!=null)
    	startDate = $("#examDate").val();
    
    $("#scheduler").empty();
	var scheduler = $("#scheduler").data("kendoScheduler");
	if(scheduler!=null)
		scheduler.destroy();
    
    CGServiceAjax.displayExamCalendar(districtAssessmentId,centreScheduleId,teacherId,jobId,startDate,{
    	async : false,
    	callback : function(data) {
    	//alert(JSON.stringify(data[0]));
    	 var bookings = [{field: "id",dataSource: data[1]}];
    	 array = data[2];
    	 for (i = 0; i < array.length; i++) { 
    	   var entry=array[i];
    	   var name;
    	      for (name in entry) {
    	         if(name=="day"){
    	        	 	enableDays.push(entry[name]);
    	         }
    	   }
    	 }
    	 
    	 var current = new Date();     // get current date
    	 if( !(startDate==null || startDate=="") )
    	 {
    		 current = new Date();     // get current date
    		 var dateInArray = startDate.split("-")
    		 current.setDate(dateInArray[1]);
    		 current.setMonth(dateInArray[0]-1);
    		 current.setFullYear(dateInArray[2]);
    	 }
    	 
    	 var weekstart = current.getDate() - current.getDay() +1;
    	 var monday = new Date(current.setDate(weekstart));
    	 
    	 $("#scheduler").kendoScheduler({    
         height: 490,
         allDaySlot: false,
         showWorkHours:true,
         editable: false,
         date:monday,
    	 views: [{type: "workWeek", selected: true }],
    	        selectable: true,
    	        save: scheduler_save,
    	        add: scheduler_add,
    	        change:scheduler_change,
    	        dataSource: data[0],
    	      	resources: bookings,
    	      	navigate:scheduler_navigate,    	      	
    	      	
    	      	  edit: function (e) {
    	        	    	  if (e.event.id==0) { 
    	        	             e.preventDefault();
    	        	          }
    	        	    	  else
    	        	    	  {
	    	    	        	  disableSlotIds();
	    	        	    	  for(var id = 0 ;id<disableIds.length;id++)
	    	        	    	  {
	    	        	    		  if (e.event.id==disableIds[id]) {
	    	        	      	            e.preventDefault();
	    	    	          	        }
	    	        	    	  }
	    	        	    	  for(var i=0;i<disableIds.length;i++)
	    	      					{
	    	      						disableIds.pop();
	    	      					}
    	        	    	  }    	       
    	    },
    	    dataBound: function(e) {
                var scheduler = this;
                var view = scheduler.view();
                var count=0;
                view.content.find("td").each(function() {
                var slot = scheduler.slotByElement($(this));
                 //check if current slot should be colored based on your custom logic                 
                /*  var n = getDayByDate(slot.startDate);
                  if (enableDays.indexOf(n)> -1) {                    	 
                              var pos = enableDays.indexOf(n);
                              var color = colorDays[pos];
                              $(this).css("background", "#ebebeb"));
                  }else{
                         $(this).css("background", "#ebebeb"));
                  }*/
                })
                }
    	});
    	
    	$('#loadingDiv').hide();
    	}
    });
}
var event = "";
function scheduler_navigate(e)
{
	if(e.action=="next" || e.action=="previous" || e.action=="changeDate" || e.action=="today")
	{
		var mondayDate = getMonday(e.date);
		var mydate = (mondayDate.getMonth()+1)+"-"+mondayDate.getDate()+"-"+mondayDate.getFullYear();
		$("#examDate").val(mydate);
		displayExamCalendar();
	}
	
	if(e.action="workView")
	{
		event = "";
	}
}

function getMonday(d)
{
	d = new Date(d);
	var day = d.getDay(),
	diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
	return new Date(d.setDate(diff));
}

function scheduler_change(e) {
	  var events = e.events; //list of selected Scheduler events
      var centreScheduleId =  events[events.length - 1].id;
      document.getElementById("centreScheduleId").value=centreScheduleId;
      var	districtAssessmentId=document.getElementById("districtAssessmentId").value;
      var	centreScheduleId=document.getElementById("centreScheduleId").value;
      var	teacherId=document.getElementById("teacherId").value;
      var dt = events[events.length - 1].start;
      if( event!="workView")
      {
    	  var n = ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + ("0" + (dt.getDate())).slice(-2) + '/' +  dt.getFullYear();
          //alert("date select in kendo :::: "+n);
          if (enableDays.indexOf(n)> -1) {
        		$('#loadingDiv').show();
	               CGServiceAjax.getStartEndDateByAssessment(centreScheduleId,districtAssessmentId,teacherId,{ 
	  		  		async: false,
	  		  		callback: function(data){
	  		    	  if(data.length>0)
	  		    	  {	  		    		
	  			    	  $("#dataWindow").html(data);
	  			    	  var win = $("#showpopupKendoWindow").data("kendoWindow");
	  			    	  win.center();
	  			    	  win.open();
	  			    	$('#loadingDiv').hide();
	  		    	  }
	  		    	  else
	  		    	  {
	  		    		 // alert(" Available ");
	  		    		$('#loadingDiv').hide();
	  		    	  }
	  		  		},
	  		  		errorHandler:handleError,
	  		  	});
          }else{
    	       		e.preventDefault();
    	       		$('#loadingDiv').hide();
    	       }
      }
}

function closePopUp()
{
	 var win = $("#showpopupKendoWindow").data("kendoWindow");
	 win.close();
}
function saveSlot()
{
	saveAssessmentInvite();
}

function objToString (obj) {
	var str = '';
	for (var p in obj) {
		  if (obj.hasOwnProperty(p)) {
				str += p + '::' + obj[p] + '\n';
		  }
	}
	return str;
}
function saveAssessmentInvite(){
	
	var teacherId=document.getElementById("teacherId").value;
	var jobId=document.getElementById("jobId").value;
	var	districtAssessmentId=document.getElementById("districtAssessmentId").value;
	var	centreScheduleId=document.getElementById("centreScheduleId").value;
	CGServiceAjax.saveAssessmentInvite(teacherId,jobId,districtAssessmentId,centreScheduleId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		closePopUp();
			displayExamCalendar();
			
		}
	});
}
function displayAssessmentSlotDetails(){
	var teacherId = document.getElementById('teacherId').value;
	$('#loadingDiv').show();
	try
	{
		CGServiceAjax.showAssessmentDetails(teacherId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: false,
		callback: function(data)
		{
			if(data!='')
			{
				$('#loadingDiv').hide();
				try{$('#examSlotData').html(data);	}catch(e){alert(e);}		
				try{applyScrollOnTbl();}catch(e){alert(e);}
			}
			else
			{
				//alert("No Data");
			}
			
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}
}

function disableSlotIds()
{
	try
	{
		CGServiceAjax.getDisabledSlotIds({ 
		async: false,
		callback: function(data)
		{
			if(data!='')
			{
				for(var i=0;i<disableIds.length;i++)
				{
					disableIds.pop();
				}
				var result = data.split(",");
				for(var i=0;i<result.length;i++)
				{
					disableIds.push(result[i]);
				}
			}
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}
}