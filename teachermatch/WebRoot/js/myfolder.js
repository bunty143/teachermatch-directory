function validateDelete()
{
	var savecandiadetidarray="";
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	
	//alert(" errortreediv "+savecandiadetidarray);
	if(savecandiadetidarray=="" || savecandiadetidarray.length<0)
	{
		$("#errortreediv").show();
		document.getElementById("errortreediv").innerHTML="&#149; "+resourceJSON.msgSelectCandidate;
		return false;
	}
	else
	{
		//$("#deleteShareCandidate").modal("show");
		try{
			$("#deleteShareCandidate").modal("show");
		}catch(err)
		  {}
		$("#savecandiadetidarray").val(savecandiadetidarray);
	}
}

function deleteCandidate()
{
	var rootNode =$("#tree").dynatree("getActiveNode");
	var folderId=rootNode.data.key;
	var checkboxshowHideFlag=1; // For displaying Check box
	var savecandiadetidarray=$("#savecandiadetidarray").val();
	//alert(" folderId "+folderId+" savecandiadetidarray "+savecandiadetidarray);
	CandidateGridAjax.deleteCandidate(savecandiadetidarray,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" ==== data === "+data);
		if(data==1)
		{
			displaySavedCGgridByFolderId(1,folderId,checkboxshowHideFlag)
			//$('#deleteShareCandidate').modal('hide');
			try{
				$('#deleteShareCandidate').modal('hide');
			}catch(err)
			  {}
			
			
		}
	}
	});
}

function displayShareUsergrid()
{
	$("#shareFlag").val(0);
	var savecandiadetidarray="";
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 }
	 if(savecandiadetidarray!=""){
		 $("#savecandiadetidarray").val(savecandiadetidarray);
		 displayUsergrid();
	 }else{
		 $("#errortreediv").show();
		$('#errortreediv').html("&#149; "+resourceJSON.msgSelectCandidate);
	 }
}
function displayShareUsergridForShare()
{
   $("#shareFlag").val(1);
	var savecandiadetidarray="";
	var inputs = document.getElementsByName("mycbx"); 
	//alert("length=>"+inputs.length);
	 for (var i = 0; i < inputs.length; i++) {
		    //if (inputs[i].type === 'checkbox') {
	        //	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	         //   }
	       // }
	 }
	 if(savecandiadetidarray!=""){
		 $("#savecandiadetidarray").val(savecandiadetidarray);
		 displayUsergrid();
	 }/*else{
		 $("#errortreediv").show();
		$('#errortreediv').html("&#149; Please select Candidate");
	 }*/
}
function displayUsergrid(teacherId,flagpopover)
{
	if(teacherId!="")
	{
		$("#teacherIdFromSharePoPUp").val(teacherId);
		$("#txtteacherIdShareflagpopover").val(flagpopover);
	}	
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=1;
	document.getElementById("userCPS").value=0;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";
	
	$('#errorinvalidschooldiv').hide();
	if($('#entityType').val()==2)
	{
		//alert("---"+$('#schoolId').val())
		$('#schoolId').val("0");
		$('#schoolName').val("");
	}
	else
	{
		if($('#entityType').val()==3)
		{
			//alert("-School Id--"+$('#schoolId').val())
			$('#schoolId').val($('#loggedInschoolId').val());
			$('#schoolName').val($('#loggedInschoolName').val());
			//alert("-School Id--"+$('#schoolId').val())
		}
	}
	var savecandiadetidarray="";
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 }
	 $("#savecandiadetidarray").val(savecandiadetidarray);
	 
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
		}catch(err)
		  {}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		if(schoolId=="")
		{
			$('#errorinvalidschooldiv').show();
			$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.SelectValidSchool);
			return false;
		}
		if($('#entityType').val()!="")
		{
			/* ========= Display School List ============ */
			CandidateGridAjax.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
					//alert(" ==== data === "+data);
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}
		/*else
		{
			if(entityType==3)
			{
				alert("School Login");
			}
		}*/
	
	
}

function searchUser(searchFlag)
{
	//alert(" ---- searchFlag "+searchFlag+"  "+$('#schoolId').val()+" School Name "+$('#schoolName').val());
	if(searchFlag==1)
	{
		defaultShareDiv();
	}
	document.getElementById("userFPS").value=1;
	document.getElementById("userFPD").value=0;
	document.getElementById("userCPS").value=0;
	document.getElementById("userCPD").value=0;
	
	currentPageFlag="us";
	$('#errorinvalidschooldiv').hide();
	var savecandiadetidarray="";
	 savecandiadetidarray = $("#savecandiadetidarray").val();
	if(savecandiadetidarray!="" || $("#txtteacherIdShareflagpopover").val()==1)
	{
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
		}catch(err)
		  {}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		var schoolName	=	trim($('#schoolName').val());
			if(schoolId=="")
			{
				if( schoolName!="")
				{
					$('#errorinvalidschooldiv').show();
					$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.SelectValidSchool);
					return false;
				}
				else
				{
					schoolId=0;
				}
				
			}
		if($('#entityType').val()!="")
		{
			CandidateGridAjax.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}
	}
	
}

function shareCandidate()
{
	defaultSaveDiv();
	$('#errorinvalidschooldiv').hide();
	$('#errortreediv').hide();
	$('#errorinvalidschooldiv').empty();
	var userIdarray =	"";
	var myUsercbx	=	document.getElementsByName("myUsercbx");
	var folderId = document.getElementById('frame_folderId').value;
	
	var shareFlag = $("#shareFlag").val();
	for (e=0;e<myUsercbx.length;e++) 
	 {
	  if (myUsercbx[e].checked==true) 
	  {
		  userIdarray+=myUsercbx[e].value+",";
	   }
	 }
	 	if(userIdarray=="")
	 	{
	 		$('#errorinvalidschooldiv').show();
	 		$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.PlzSelectUsr);
	 	}
	 	savecandiadetidarray = $("#savecandiadetidarray").val();
	 	//alert(" userIdarray "+userIdarray+" savecandiadetidarray "+savecandiadetidarray);
	 	
	 	if(shareFlag==1)
	 	{
	 
	 		//alert(" userIdarray "+userIdarray+" savecandiadetidarray "+savecandiadetidarray+" folderId "+folderId);
	 		savecandiadetidarray	=	$("#teacherIdFromSharePoPUp").val();
	 	
	 		CandidateGridAjax.shareFolderToUsers(userIdarray,savecandiadetidarray,folderId,
				{ 
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
					if(data==1)
					{
						try{
							$('#shareDiv').modal("hide");
							$('#saveAndShareConfirmDiv').html("You have successfully shared the Candidates to the selected User(s).");
							$('#shareConfirm').modal("show");
						}catch(err)
						  {}
					}
				}
				});
	 	}
	 	else if(shareFlag==0)
	 	{
	 		
			if($("#txtteacherIdShareflagpopover").val()==1 && $("#teacherIdFromSharePoPUp").val()!="")
			{
				
				savecandiadetidarray	=	$("#teacherIdFromSharePoPUp").val();
				CandidateGridAjax.shareCandidatesToUserForTeacher(userIdarray,savecandiadetidarray,folderId,
				{ 
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						//alert(" ==== data === "+data);
					if(data==1)
					{
						//$('#shareDiv').modal("hide");
						//$('#saveAndShareConfirmDiv').html("You have successfully shared the Candidates to the selected Users.");
						//$('#shareConfirm').modal("show");
						
						try{
							$('#shareDiv').modal("hide");
							$('#saveAndShareConfirmDiv').html(resourceJSON.MsgSuccessfullySharedTheCandidates);
							$('#shareConfirm').modal("show");
						}catch(err)
						  {}
					}
				}
				});
			}	
			else
			{
				
				CandidateGridAjax.shareCandidatesToUser(userIdarray,savecandiadetidarray,folderId,
				{ 
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						//alert(" ==== data === "+data);
					if(data==1)
					{
						//$('#shareDiv').modal("hide");
						//$('#saveAndShareConfirmDiv').html("You have successfully shared the Candidates to the selected Users.");
						//$('#shareConfirm').modal("show");
						
						try{
							$('#shareDiv').modal("hide");
							$('#saveAndShareConfirmDiv').html(resourceJSON.MsgSuccessfullySharedTheCandidates);
							$('#shareConfirm').modal("show");
						}catch(err)
						  {}
						
						uncheckAllCbx();
					}
				}
				});
			}
	 	}
	 
}
function uncheckAllCbx()
{
	
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	//savecandiadetidarray+=	inputs[i].value+",";
	        		inputs[i].checked=false;
	            }
	        }
	 } 
	
}
function displayCandidatesOfHomeFolder()
{
	$('#tree').dynatree({
		persist: true,
		onActivate: function(node) {
			$('#errortreediv').hide();
			$('#errordeletetreediv').hide();
			$("#echoActivated").text(node.data.title + ", key=" + node.data.key);
			$("#frame_folderId").val(node.data.key);
			window.parent.displaySavedCGgridByFolderId(1,node.data.key,1);
		/*	var rootNode =$("#tree").dynatree("getActiveNode");
			if(rootNode==null)
			{
				//document.getElementById('errortreediv').innerHTML		=	'&#149; Please enter Domain Name';
				$('#errortreediv').html("&#149; Please select any folder");
				$('#errortreediv').show();
				return false;
			}
		*/
			//$("#frame_folderId").val($("#tree").dynatree(rootNode.data.key);
			//alert(" ----- frame_folderId ----- "+$("#frame_folderId").val());
		},
		onClick: function(node, event) {
			// Close menu on click
			if( $(".contextMenu:visible").length > 0 ){
				$(".contextMenu").hide();
//				return false;
			}
		},
		onKeydown: function(node, event) {
			// Eat keyboard events, when a menu is open
			if( $(".contextMenu:visible").length > 0 )
				return false;

			switch( event.which ) {

			// Open context menu on [Space] key (simulate right click)
			case 32: // [Space]
				$(node.span).trigger("mousedown", {
					preventDefault: true,
					button: 2
					})
				.trigger("mouseup", {
					preventDefault: true,
					pageX: node.span.offsetLeft,
					pageY: node.span.offsetTop,
					button: 2
					});
				return false;

			// Handle Ctrl-C, -X and -V
			case 67:
				if( event.ctrlKey ) { // Ctrl-C
					copyPaste("copy", node);
					return false;
				}
				break;
			case 86:
				if( event.ctrlKey ) { // Ctrl-V
					copyPaste("paste", node);
					return false;
				}
				break;
			case 88:
				if( event.ctrlKey ) { // Ctrl-X
					copyPaste("cut", node);
					return false;
				}
				break;
			}
		},
		/*Bind context menu for every node when it's DOM element is created.
		  We do it here, so we can also bind to lazy nodes, which do not
		  exist at load-time. (abeautifulsite.net menu control does not
		  support event delegation)*/
		onCreate: function(node, span){
		//alert(" onCreate Method is calling "); 
		 	node.expand(false);
		 	//$("#"+node.data.key).append("<li>Gagan</li>");
		 	//alert("Hi");
			bindContextMenu(span);
		},
		onFocus: function(node) {
			$("#echoFocused").text(node.data.title);
		},
		onBlur: function(node) {
			$("#echoFocused").text("-");
		},
		/*Load lazy content (to show that context menu will work for new items too)*/
		onLazyRead: function(node){
			node.appendAjax({
				url: "contextmenu/sample-data2.json"
			});
		},
	/* D'n'd, just to show it's compatible with a context menu.
	   See http://code.google.com/p/dynatree/issues/detail?id=174 */
	dnd: {
		preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
		onDragStart: function(node) {
			return true;
		},
		onDragEnter: function(node, sourceNode) {
			if(node.parent !== sourceNode.parent)
				return false;
			return ["before", "after"];
		},
		onDrop: function(node, sourceNode, hitMode, ui, draggable) {
			sourceNode.move(node, hitMode);
		}
	}
	});
	var tree = $('#tree').dynatree("getTree");
	var root = tree.getRoot();
	var nodeList = root.getChildren();
	try{
		nodeList[0].activate(true);
	}catch(err){}
	
	var folderId 				=	"";
	try{
		folderId = nodeList[0].data.key;
	}catch(err){}
	$("#frame_folderId").val(folderId);
	$("#home_folderId").val(folderId);
		
	var checkboxshowHideFlag= 1;
	displaySavedCGgridByFolderId(1,folderId,checkboxshowHideFlag)
}

function showCommunicationsDivForSaveMyFolder()
{
	//$('#myModalCommunications').modal('hide');	
	try{
		$('#myModalCommunications').modal('hide');	
	}catch(err)
	  {}
}

//8--------------------------------------------------------------------------------------------------------------
function closeSaveDiv(){
	document.getElementById("pageFlag").value=1;
	defaultSaveDiv();
}

function saveToFolderJFTNULL(teachersaveId,teacherId,flagpopover)
{
	defaultSaveDiv();
	document.getElementById("pageFlag").value=0;
	if(teacherId!="")
	{
		$("#teachetIdFromPoPUp").val(teacherId);
		$("#txtflagpopover").val(flagpopover);
	}	
	//------------- Here I am creating only Home and Shared Folder For User	
	try{
		$('#saveToFolderDiv').modal('show');
	}catch(err)
	  {}
	//$('#saveToFolderDiv').modal('show');
	$('#errordiv').hide();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
	innerDoc.getElementById('errortreediv').style.display="none";
	innerDoc.getElementById('errordeletetreediv').style.display="none";

	var checkboxshowHideFlag =0; 
	var folderId = innerDoc.getElementById('frame_folderId').value;
	if(innerDoc.getElementById('frame_folderId').value!="" || innerDoc.getElementById('frame_folderId').value.length>0)
	{
		//alert(" if block ");
		// For hide checkboxes on Save to Folder Div
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
		// return false;
	}
	else
	{
		//alert(" else block ");
		folderId = $("#home_folderId").val();
		//alert(" folderId "+folderId);
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
	}
	
}

function saveCandidateToFolderByUserFromMyFolder()
{
	document.getElementById('iframeSaveCandidate').contentWindow.checkFolderId();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	var folderId = innerDoc.getElementById('frame_folderId').value;
	var frameHeight=iframe.contentWindow.document.body.scrollHeight;
	$('iframe').css('height',frameHeight);
	var fId="";
	if(innerDoc.getElementById('frame_folderId').value=="" || innerDoc.getElementById('frame_folderId').value.length==0)
	{
		//alert("Yes")
		innerDoc.getElementById('errortreediv').style.display="block";
		innerDoc.getElementById('errortreediv').innerHTML="&#149; "+resourceJSON.PlzSelectAnyFolder;
		 return false;
	}
	else
	{
		//alert(" Else No ");
		//fId=folderId;
	}
	//alert(" sgfsdgb "+innerDoc.getElementById('frame_folderId').value);
	var savecandidatearray="";
	 var inputs = document.getElementsByName("cgCBX"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 	
	 	if($("#txtflagpopover").val()==1 && $("#teachetIdFromPoPUp").val()!="")
		{
	 		savecandidatearray	=	$("#teachetIdFromPoPUp").val();
		}	
	
	 
	CandidateGridAjax.saveCandidateToFolderByUserFromTeacherId(savecandidatearray,folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
			if(data==1)
			{
				
				try{
					$("#saveToFolderDiv").modal("hide");
					$("#saveToFolderDiv").modal("hide");
				}catch(err)
				  {}
				
				$('#saveAndShareConfirmDiv').html(resourceJSON.SavedCandidate);
				//$('#shareConfirm').modal("show");
				window.location.href="myfolder.do";
			}
			if(data==3)
			{
				//alert("Duplicate Candaidate found ");
				$("#savecandidatearray").val(savecandidatearray);
				$("#txtoverrideFolderId").val(folderId);
				try{
					$("#duplicatCandidate").modal("show");
				}catch(err)
				  {}
			}
		}
	});
	 
	 
}
function saveWithDuplicateRecordFromMyFolder()
{
	var folderId= $("#txtoverrideFolderId").val();
	var savecandidatearray= $("#savecandidatearray").val();
	//alert(" savecandidatearray "+savecandidatearray+" folderId :  "+folderId);
	CandidateGridAjax.saveWithDuplicateRecord(savecandidatearray,folderId,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
					if(data==1)
					{
						//$("#saveToFolderDiv").modal("hide");
						//$("#duplicatCandidate").modal("hide");
						try{
							$("#saveToFolderDiv").modal("hide");
							$("#duplicatCandidate").modal("hide");
						}catch(err)
						  {}
						uncheckedAllCBX();
					}
				}
			});
	
}

//changed: by rajendra
/*
function downloadReferenceForCandidate(referenceId,teacherId,linkId)
{		
	    $("#myModalLabelText").html("Reference");	
		PFCertifications.downloadReferenceForCandidate(referenceId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
		if(data=="")
		{
			data="javascript:void(0)";
		}
		else
		{
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById("ifrmRef").src=data;
			}
			else
			{
				try{
					document.getElementById('ifrmTransCommon').src = ""+data+"";
					$('#modalDownloadsCommon').modal('show');								
				}catch(err)
				  {
					alert(err)
				  }
			}			
		}	
		return false;
		}});
}*/
function downloadTranscript(academicId,linkid)
{	
	$("#myModalLabelText").html(resourceJSON.MsgTranscript);	
	CandidateReportAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
		if(deviceType)
		{
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
			    if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 $('#modalDownloadsTranscript').modal('hide');
					 document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkid).href = data;	
				}	
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#modalDownloadsCommon').modal('hide');
			 document.getElementById('ifrmTransCommon').src = ""+data+"";
		}
		else
		{
			try{
				document.getElementById('ifrmTransCommon').src = ""+data+"";
				$('#modalDownloadsCommon').modal('show');
			}catch(err)
			  {}	
		}		
	   }
	});
}


/*========================== TFA  Edit By Specific users ==========================================*/
function editTFAbyUser(teacherId)
{
	$("#draggableDivMaster").show();
	$('#edittfa').hide();
	$('#savetfa').show();
	$('#canceltfa').show();
	document.getElementById("tfaList").disabled=false;
}

function saveTFAbyUser()
{
	$("#tfaMsgShow").hide();
	
	var tfa = document.getElementById("tfaList").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	
	TeacherInfotAjax.updateTFAByUser(teacherId,tfa,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			var tfalist = document.getElementById("tfaList");
			if(data!=null)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
			else
			{
				tfalist.selectedIndex = 1;
			}
			
			$("#draggableDivMaster").show();
			$('#edittfa').show();
			$('#savetfa').hide();
			$('#canceltfa').hide();
			document.getElementById("tfaList").disabled=true;
		}
	});
}

function cancelTFAbyUser(teacherId)
{
		$("#draggableDivMaster").show();
		$('#edittfa').show();
		$('#savetfa').hide();
		$('#canceltfa').hide();
		document.getElementById("tfaList").disabled=true;
		var tfalist = document.getElementById("tfaList");
		TeacherInfotAjax.displayselectedTFA(teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
		});
		
}

function displayTFAMessageBox(teacherId)
{
	document.getElementById("teacherDetailId").value=teacherId;	
	$("#tfaMsgShow").show();
}

function closeTFAMsgBox()
{
	$("#tfaMsgShow").hide();
}


/*====================================================================*/
//---------------school search----------------
function getSchoolMasterAutoCompQuestion(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArrayQuestion(txtSearch.value);
		fatchData2Question(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArrayQuestion(schoolName){
	var searchArray = new Array();
	var districtId	= $("#questionDistrictId").val();
	BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2Question= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDivQuestion(dis,hiddenId,divId)
{
	document.getElementById(hiddenId).value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
function downloadOctUpload(octId)
{
		//alert("downloadAnswer0");
	TeacherProfileViewInDivAjax.downloadOctUpload(octId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			if(data!="")
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameReferencesID').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefOctUpload").href = data; 
					return false;
				}
			}
		});
	}

function getEmployeeNumberInfo(flag,table){
	var height="height:500px;";
	if(flag)
	height="height:auto;";
	var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:850px;'><div class='modal-content'><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";
		try{
			$('#employeeInfo').modal('hide'); 
		}catch(e){}
		try{
			$('#employeeInfo').remove(); 
		}catch(e){}
		$('#draggableDivMaster').after(div);
		$('#draggableDivMaster').modal('hide');
		$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});
		$('#employeeInfo #tableData').html(table); 
		$('.confirmFalse').click(function(){  	
			$('#employeeInfo').modal('hide');   
			$('#employeeInfo').remove(); 
		 	$('#draggableDivMaster').modal('show');  
		});
	$('#loadingDiv').hide();
 }
function openEmployeeNumberInfo(empId){
$('#loadingDiv').show();
  try{
	  TeacherProfileViewInDivAjax.openEmployeeNumberInfo(empId,{
		  async: false,
		  cache: false,
		  errorHandler:handleError,
		  callback:function(data){
		  $('#draggableDivMaster').modal('hide');
		  $('#draggableDivMaster').hide();
		  //alert(data);
		  var divData=data.split('####');
		  if(divData[0].trim()=='1')
			  	getEmployeeNumberInfo(true,divData[1]);
		  		else
		  		getEmployeeNumberInfo(false,data);		  
		  }
	   });
  }catch(e){alert(e)}
  }



function getTeacherLicenceGrid_DivProfile(teacherId){
	
	TeacherProfileViewInDivAjax.displayRecordsBySSNForDate(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLicneseGridCertificationsGridDate').html(data);			
			//    $('#loadingDiv').hide();				
			   applyScrollOnTblLicenseDate();
		}
		
		},
	errorHandler:handleError
	});
}


function getLEACandidatePortfolio_DivProfile(teacherId){
	
	TeacherProfileViewInDivAjax.getLEACandidatePortfolio(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLEACandidatePotfolioDiv').html(data);			
			//    $('#loadingDiv').hide();				
				applyScrollOnLEACandidatePorfolio();
		}
		
		},
	errorHandler:handleError
	});
}