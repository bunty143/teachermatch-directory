var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

function getPaging(pageno)
{

	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	noOfRows = document.getElementById("pageSize").value;
	getQuestionListByDistrictAndJobCategory();
		
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	getQuestionListByDistrictAndJobCategory();
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

function displayStatusDashboard(){
	
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	
	var jobCategory=document.getElementById("txtjobCategory").value;
	
	var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;
	
	var jobSubCategory=document.getElementById("txtjobSubCategory").value;
	
	if(jobSubCategory!=null && jobSubCategory!=""){
		jobCategory = jobSubCategory;
	}
	
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	
	//alert('districtId::::'+districtId +" jobCategory:::"+jobCategory+" branchId:::"+branchId+" headQuarterId:::"+headQuarterId);
	
	 
	ManageStatusAjax.displayStatusDashboardOp(headQuarterId,branchId,districtId,jobCategory,false,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("statusdashboard").innerHTML=data.toString();	
		}
	});
  }
 
	 

function displayTreeStructure(){
	ManageStatusAjax.displayTreeStructure(
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("statustree").innerHTML=data.toString();	
		}
	});
}

function checkFolderIsDeleted(rootNode)
{
	$('#errortreediv').empty();
	var folderId="";
	if(rootNode==null)
	{
		rootNode =$("#tree").dynatree("getActiveNode");
		folderId=rootNode.data.key;
	}
	else
	{
		// When root Node is not null
		folderId=rootNode.data.key;
	}
	
	ManageStatusAjax.checkFolderIsDeleted(folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" ==== data === "+data);
			if(data==3)
			{
				$('#errortreediv').html("&#149; "+resourceJSON.msgThisStatusDeleted);
				$('#errortreediv').show();
			}
			else 
			{
				if(data==4 || data==5)
				{
					try{
						deleteStatusfolder(rootNode,data);
					}catch(err)
					  {}
				}
			}
			
		}
	});
}

function deleteStatusfolder(rootNode,datastatus)
{
	$('#currentObject').val(rootNode.data.key);
	
	$('#deleteFolder').modal('show');
	
	if(datastatus==4)
	{
			$('#deleteFolder_body').html(resourceJSON.msgReallyDeleteStatus);
		
	}
	else if(datastatus==5)
	{
		$('#deleteFolder_body').html(resourceJSON.msgStatusAttachedJobCategories);
	}
}

function deleteStatusConfirm()
{
	$('#deleteFolder').modal('hide');
	var rootNodeKey =$('#currentObject').val();
	deleteFolder(rootNodeKey);
}



function checkFolderId()
{
	var rootNode =$("#tree").dynatree("getActiveNode");
	if(rootNode==null)
	{
		$("#frame_folderId").val("");
	}
	else
	{
		$("#frame_folderId").val(rootNode.data.key);
	}
}


function deleteFolder(rootNodeKey)
{
	/* ---------- Deleting Node here without executing query  because query will be already execute for removing Node inside iframe --------------*/
	/*var pageFlag= document.getElementById("pageFlag").value;
	//alert(" deleteFolder "+rootNodeKey+" pageFlag "+pageFlag);
	if(pageFlag	==0 && rootNodeKey!=0)  
	{
		rootNode =$("#tree").dynatree("getTree").getNodeByKey(rootNodeKey);
		rootNode.remove();
		return false;
	}*/
	if(rootNodeKey!=0)
	{
		rootNode =$("#tree").dynatree("getTree").getNodeByKey(rootNodeKey);
		rootNode.activate(true);
	}
	
	
	$('#errortreediv').empty();
	var folderId="";
	if(rootNode==null)
	{
		rootNode =$("#tree").dynatree("getActiveNode");
		folderId=rootNode.data.key;
	}
	else
	{
		// When root Node is not null
		folderId=rootNode.data.key;
	}
	
	
	HeadQuarterBranchServiceAjax.deleteStatusFolder(folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			rootNode.remove();
			$('#deleteFolder').modal('hide');
			$("#frame_folderId").val("");
			var tree = $('#tree').dynatree("getTree");
			var root = tree.getRoot();
			var nodeList = root.getChildren();
			nodeList[0].activate(true);
			var folderId 				=	nodeList[0].data.key;
			displayStatusDashboard();
		}
	});
}

function createStatusFolder(rootNode)
{
	$('#errortreediv').empty();
	if(rootNode==null)
	{
		$('#errortreediv').html("&#149; "+resourceJSON.msgSelectAnyStatus);
		$('#errortreediv').show();
		return false;
	}
	
	var districtId=document.getElementById("districtId").value;
	var jobCategory=document.getElementById("txtjobCategory").value;
	var jobSubCategory=document.getElementById("txtjobSubCategory").value;
	
	if(jobSubCategory!=null && jobSubCategory!=""){
		jobCategory = jobSubCategory;
	}
	
	if(districtId==null || districtId=="")
		districtId=0;
	if(jobCategory==null || jobCategory=="")
		jobCategory=0;
	
	var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	
	var parentId=rootNode.data.key;
	HeadQuarterBranchServiceAjax.createStatusFolder(parentId,headQuarterId,branchId,districtId,jobCategory,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data.secondaryStatusId!=null){
				var children=rootNode .getChildren() ;
				var nextSiblingNode=null;
				if(children.length>1){
					nextSiblingNode = children[children.length-2].getNextSibling();
				}else{
					nextSiblingNode = children[children.length-1];
				}
				var nodeId=data.secondaryStatusId;
				var newNode=rootNode.addChild({
					title: data.secondaryStatusName,
					tooltip: "",
					isFolder: true,
					expand:true,
					key: data.secondaryStatusId
				},nextSiblingNode);
				rootNode.expand(true);
				editNode(newNode);
				displayStatusDashboard();
			}else{
				$('#errortreediv').html("&#149; "+resourceJSON.msgCreateChild);
				$('#errortreediv').show();
			}
		}
	});
}
function editNode(node){

if(node==null)
{
	return false;
}

var prevTitle = node.data.title,
	tree = node.tree;
// Disable dynatree mouse- and key handling
tree.$widget.unbind();
// Replace node with <input>
$(".dynatree-title", node.span).html("<input id='editNode' style='width:100px;'  maxlength='25' value='" + prevTitle + "'>");
// Focus <input> and bind keyboard handler
$("input#editNode")
	.focus()
	.keydown(function(event){
		document.getElementById("renameEnterFlag").value=1; 
		switch( event.which ) {
		case 27: // [esc]
			$("input#editNode").val(prevTitle);
			$(this).blur();
			break;
		case 13: // [enter]
			// simulate blur to accept new value
			document.getElementById("renameEnterFlag").value=1; 
			$(this).blur();
			break;
		}
	}).blur(function(event){
		// Accept new value, when user leaves <input>
		var title = $("input#editNode").val();
		if(title=="" || title.length<0 || title==prevTitle)
		{
			node.setTitle(prevTitle);
		}else{
			node.setTitle(title);
		//---------------------- Edit Or Rename Folder Name --------------------------
			HeadQuarterBranchServiceAjax.renameStatusFolder(node.data.key,title,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
					if (data == 5){
						$('#errortreediv')
						.html(
						"&#149; "+resourceJSON.msgAttributesExistingStatus);
						$('#errortreediv').show();
						node.setTitle(prevTitle);
					}else if (data == 3){
						$('#errortreediv')
						.html(
						"&#149; "+resourceJSON.msgAttributesExistingStatus);
						$('#errortreediv').show();
						node.setTitle(prevTitle);
					}else if(data==4){
						$('#errortreediv').html("&#149; "+resourceJSON.msgCreateStatusNameJSI);
						$('#errortreediv').show();
						node.setTitle(prevTitle);
					}else if(data==0){
						$('#errortreediv').html("&#149; "+resourceJSON.msgNotRenameStatus);
						$('#errortreediv').show();
						node.setTitle(prevTitle);
					}else if(data==1 && document.getElementById("renameEnterFlag").value!=2){
						$('#errortreediv').html("&#149; "+resourceJSON.msgAlreadyExistStatus);
						$('#errortreediv').show();
						node.setTitle(prevTitle);
						editNode(node);
						tree.$widget.unbind();
						return false;
					}else{
						document.getElementById("renameEnterFlag").value=2;
						$('#errortreediv').hide();
						displayStatusDashboard();
					}
				}
			});
		}
		// Re-enable mouse and keyboard handlling
		tree.$widget.bind();
		node.focus();
	});
}


// --- Implement Cut/Copy/Paste --------------------------------------------
var clipboardNode = null;
var pasteMode = null;

function copyPaste(action, node) {
	
	var districtId=document.getElementById("districtId").value;
	var jobCategory=document.getElementById("txtjobCategory").value;
	if(districtId==null || districtId=="")
		districtId=0;
	if(jobCategory==null || jobCategory=="")
		jobCategory=0;
	
	var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	
	switch( action ) {
	case "cut":
		var cutCheck=createStatusCheck(node);
		if(cutCheck==1){
			$('#errortreediv').html("&#149; "+resourceJSON.msgNotCutSelectStatus);
			$('#errortreediv').show();
			clipboardNode.remove();
		}
	case "copy":
		var cutCheck=createStatusCheck(node);
		if(cutCheck==1){
			$('#errortreediv').html("&#149; "+resourceJSON.msgNotCopySelectStatus);
			$('#errortreediv').show();
			clipboardNode.remove();
		}else{
			clipboardNode = node;
			pasteMode = action;
			break;
		}
	case "paste":
		if( !clipboardNode ) {
			break;
		}
		if( pasteMode == "cut" ) {
			
			// Cut mode: check for recursion and remove source
			var isRecursive = false;
			var dictKey=null;
			var i="";
			var cb = clipboardNode.toDict(true, function(dict){
				// If one of the source nodes is the target, we must not move
				//alert(createStatusCheck(dict));
				if( dict.key == node.data.key )
					isRecursive = true;
				dictKey	=	dict.key;
				i+=dict.key+",";
			});
			if( isRecursive ) {
				return;
			}
			var n=i.split(",");
			var cutFolderKey=n[0];
			HeadQuarterBranchServiceAjax.cutPasteFolder(node.data.key,cutFolderKey,headQuarterId,branchId,districtId,jobCategory,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data){	
					displayStatusDashboard();
				}
			});
			var children=node.getChildren() ;
			var nextSiblingNode=null;
			if(children.length>1){
				nextSiblingNode = children[children.length-2].getNextSibling();
			}else{
				nextSiblingNode = children[children.length-1];
			}
			node.addChild(cb,nextSiblingNode);
			clipboardNode.remove();
		} else {
		var dictKey=null;
		var dictTitle=null;
			// Copy mode: prevent duplicate keys:
			var cb = clipboardNode.toDict(true, function(dict){
				dictTitle=dict.title;
				dict.title = resourceJSON.msgCopyOf+" " + dict.title;
				dictKey	=	dict.key;
				delete dict.key; // Remove key, so a new one will be created
			});
			
			HeadQuarterBranchServiceAjax.copyPasteFolder(node.data.key,dictTitle,headQuarterId,branchId,districtId,jobCategory,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					var children=node.getChildren() ;
					var nextSiblingNode=null;
					if(children.length>1){
						nextSiblingNode = children[children.length-2].getNextSibling();
					}else{
						nextSiblingNode = children[children.length-1];
					}
					var nodeId=data.secondaryStatusId;
					var newNode=node.addChild({
						title: data.secondaryStatusName,
						tooltip: "",
						isFolder: true,
						expand:true,
						key: data.secondaryStatusId
					},nextSiblingNode);
					displayStatusDashboard();
				}
			});
			
		}
		clipboardNode = pasteMode = null;
		break;
	default:
		alert(resourceJSON.msgUnhandledClipboardAction+" '" + action + "'");
	}
};




function createStatusCheck(rootNode)
{
	var returnVal=0;
	try{
		var parentId=rootNode.data.key;
		ManageStatusAjax.statusCheck(parentId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
			returnVal=data;
			}
		});
	}catch(err){}
	return returnVal;
}
function statusCheck(rootNode)
{
	var returnVal=0;
	try{
		var parentId=rootNode.data.key;
		ManageStatusAjax.statusCheck(parentId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
			returnVal=data;
			}
		});
	}catch(err){}
	return returnVal;
}


//Start ... for Question

function getQuestionDiv(rootNode){
	
	//cancelQuestion();
	var folderId="";
	if(rootNode==null)
	{
		rootNode =$("#tree").dynatree("getActiveNode");
		folderId=rootNode.data.key;
	}
	else
	{
		// When root Node is not null
		folderId=rootNode.data.key;
	}
	
	//alert("folderId "+folderId);
	//var nodeTitleText=rootNode.data.title;
	var headQuarterId = "";
	try{
		headQuarterId = $("#headQuarterId").val();
	}catch(e){}
	if(folderId!="")
	{
		ManageStatusAjax.isQuestionAdd(folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				if(!data)
				{
					$("#myModalMsgShow_NodeInfo").modal('show');
				}
				else
				{
					if(folderId!="")
					{
						$('#errorOption').empty(); 
						$('#errorOption2').empty(); 
						$("#myFolderId").val(folderId);
						$("#myModalQuestionList").modal('show');
						$("#myModalQuestionList" ).draggable({
							handle:'#myModalQuestionList_header'
						});
						if(rootNode.data.title!=null){
							var statusTitle=rootNode.data.title;
							if(statusTitle.toLowerCase()=="jsi"){
								document.getElementById("myModalLabelForJSIQuestion").innerHTML=resourceJSON.msgManageAttributesFor+" "+rootNode.data.title;
								$("#myModalQuestionList").modal("hide");
								$("#myModalMaxQuestion").modal("show");
							}else{
								$('#errorOption').empty(); 
								$('#errorOption2').empty(); 
								document.getElementById("myModalLabelForQuestion").innerHTML=resourceJSON.msgManageAttributesFor+" "+rootNode.data.title;
								$("#myModalQuestionList").modal("show");
								$("#myModalMaxQuestion").modal("hide");
							}
						}
						try{
							document.getElementById("divQuestionList").style.display="none";
						}catch(e){}
					}
					if(headQuarterId!="" && headQuarterId>0){
						resetAttributesForHQ();
						displayAllHqAdmins();
						displayAllBrAdmins($("#jobCategory").val());
					}else{
						resetQuestionForm();
					}
					getQuestionListByDistrictAndJobCategory();
					getJobCategoryWiseStatusPrivilege();
					displayAllAndSelecedDist();
				}
			}
		});
	}
}


function showQuestionForm()
{
	
	document.getElementById('addAQuestion').value=1;
	document.getElementById("divQuestionList").style.display="block";
	document.getElementById("question").value="";	
	document.getElementById("question").focus();
	document.getElementById("requiredtofinalizenode").checked=false; 
	$('#errorQuestion').empty();
	resetQuestionForm();
	
	setDefColortoErrorMsgQuestion();
	
	return false;
}

function setDefColortoErrorMsgQuestion()
{
	$('#question').css("background-color","");
}

function cancelQuestion()
{
	document.getElementById("divQuestionList").style.display="none";
	document.getElementById("question").value="";
	document.getElementById("scoringCaption").value="";
	document.getElementById("previousScore").value="0";
	document.getElementById("questionId").value="";
	
	document.getElementById('addAQuestion').value=0;
}

//End ... for Question

function resetJobCategory()
{
	document.getElementById("txtjobCategory").value="";
	$("#tree").hide();
	$("#folderIcons").hide();
	$("#questionIcon").hide();
	$("#questionIcon_popup").hide();
	$("#statusdashboard").hide();
	getJobSubCateList();
}

function resetJobSubCategory()
{
	document.getElementById("txtjobSubCategory").value="";
	$("#tree").hide();
	$("#folderIcons").hide();
	$("#questionIcon").hide();
	$("#questionIcon_popup").hide();
	$("#statusdashboard").hide();
}

function resetDistrictAndJobCategory()
{
	document.getElementById("txtjobCategory").value="";
	document.getElementById("districtId").value=0;
	document.getElementById("districtName").value="";
	$("#tree").hide();
	$("#folderIcons").hide();
	$("#questionIcon").hide();
	$("#questionIcon_popup").hide();
	$("#statusdashboard").hide();
}

function displayJobCategoryByDistrict()
{
	var txtjobCategory=document.getElementById("txtjobCategory").value;
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	
	var branchId = 0;
	/**
	 * comment for branchId (jobCatgeories always populates only of headquarter master)
	 * 
	 **/	
	/*var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;*/
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	
	if(headQuarterId!=0){
	AutoSearchFilterAjax.displayJobCategoryByHeadQuarter(headQuarterId,branchId,districtId,txtjobCategory,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("jobCategory_div").innerHTML=data;
			getJobSubCateList();
		}
	});
	}
	else{
		AutoSearchFilterAjax.displayJobCategoryByDistrictBranchAndHead(headQuarterId,branchId,districtId,txtjobCategory,
				{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				document.getElementById("jobCategory_div").innerHTML=data;
				getJobSubCateList();
			}
		});
	}
}


function displayStatusBody()
{
	var displayTypeId=0;
	displayTypeId=$("#displayTypeId").val();
	if(displayTypeId==1){
		hideSearchAgainMaster();
	}
	$("#folderIcons").hide();
	$("#questionIcon").hide();
	//$("#questionIcon_popup").hide();
	var url = 'managestatus.do';
	
 
	
	var entityType = $("#entityType").val();
	var districtId = document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	
	/*var branchId = document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;*/
	var branchId=0;
	
	var jobCategory=0;
	var jobSubCategoryId = 0;
	
	$('#errordiv').empty();
	$('#districtName').css("background-color", "");
	if(entityType!=5 && (districtId==0 || districtId==null || districtId==""))
	{
		$('#errordiv').html("&#149; "+resourceJSON.msgNameActiveDistrict+"<br>");
		$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
		return;
	}
	/*else if(entityType==5 && (branchId=="" || branchId==0))
	{
		$('#errordiv').html("&#149; Please enter the name of an branch<br>");
		$('#branchName').focus();
		$('#branchName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
		return;
	}*/
	else
	{
		jobCategory=document.getElementById("jobCategory").value;
		jobSubCategoryId=document.getElementById("jobSubCategoryId").value;
		
		
		
		if(entityType==5)
		{
			$("#folderIcons").show();
			$("#questionIcon").hide();
			$("#questionIcon_popup").hide();
			
			var branchId=document.getElementById("branchId").value;
			if(branchId==null || branchId=="")
				branchId=0;
			
			var headQuarterId=document.getElementById("headQuarterId").value;
			if(headQuarterId==null || headQuarterId=="")
				headQuarterId=0;
			if(jobCategory!="" && jobCategory > 0)
			{
				$("#folderIcons").show();
				$("#questionIcon").show();
				$("#questionIcon_popup").show();
				window.location.href = "./"+url+"?districtId="+districtId+"&jobCategory="+jobCategory+"&headQuarterId="+headQuarterId+"&branchId="+branchId;
			}else{
				$("#folderIcons").show();
				$("#questionIcon").show();
				$("#questionIcon_popup").show();
				window.location.href = "./"+url+"?districtId="+districtId+"&headQuarterId="+headQuarterId+"&branchId="+branchId;	
			}
			
		}
		else
		{
			if(jobSubCategoryId!="" && jobSubCategoryId > 0)
			{
				$("#folderIcons").show();
				$("#questionIcon").show();
				$("#questionIcon_popup").show();
				if(displayTypeId==1)
					window.location.href = "./managestatusdspq.do?districtId="+districtId+"&jobCategory="+jobCategory+"&jobSubCategoryId="+jobSubCategoryId;
				else
					window.location.href = "./"+url+"?districtId="+districtId+"&jobCategory="+jobCategory+"&jobSubCategoryId="+jobSubCategoryId;
				
				
			}else if(jobCategory!="" && jobCategory > 0)
			{
				$("#folderIcons").show();
				$("#questionIcon").show();
				$("#questionIcon_popup").show();
				if(displayTypeId==1)
					window.location.href = "./managestatusdspq.do?districtId="+districtId+"&jobCategory="+jobCategory;
				else
					window.location.href = "./"+url+"?districtId="+districtId+"&jobCategory="+jobCategory;
			}
			else
			{
				$("#folderIcons").show();
				$("#questionIcon").hide();
				$("#questionIcon_popup").hide();
				if(displayTypeId==1)
					window.location.href = "./managestatusdspq.do?districtId="+districtId;
				else
					window.location.href = "./"+url+"?districtId="+districtId;
			}
		}
		
		//displayStatusDashboard();
		//displaySecondaryStatusByDistrict(districtId);
		
		//copyDataFromDistrictToJobCategory();
	}
}

function displayStatusBodyOnPageLoad()
{
	$("#folderIcons").hide();
	$("#questionIcon").hide();
	$("#questionIcon_popup").hide();
	
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	
	/*var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;*/
	var branchId=0;
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	
	var jobCategory=document.getElementById("txtjobCategory").value;
	var jobSubCategoryId=document.getElementById("txtjobSubCategory").value;
	
	if(( jobCategory!="" && jobCategory > 0 ) && (districtId!="" && districtId > 0) &&(jobSubCategoryId!="" && jobSubCategoryId > 0))
	{
		$("#folderIcons").show();
		$("#questionIcon").show();
		$("#questionIcon_popup").show();
		displayStatusDashboard();
		displaySecondaryStatusByDistrict();
		
	}else if(( jobCategory!="" && jobCategory > 0 ) && (districtId!="" && districtId > 0))
	{
		$("#folderIcons").show();
		$("#questionIcon").show();
		$("#questionIcon_popup").show();
		displayStatusDashboard();
		displaySecondaryStatusByDistrict();
	}
	else if(districtId!="" && districtId > 0)
	{
		$("#folderIcons").show();
		$("#questionIcon").hide();
		$("#questionIcon_popup").hide();
		displayStatusDashboard();
		displaySecondaryStatusByDistrict();
	}else  if(headQuarterId!="" && headQuarterId > 0 && ( jobCategory!="" && jobCategory > 0 )){
		$("#folderIcons").show();
		$("#questionIcon").show();
		$("#questionIcon_popup").show();
		displayStatusDashboard();
		displaySecondaryStatusByDistrict();
	}
	else  if(headQuarterId!="" && headQuarterId > 0){
		$("#folderIcons").show();
		$("#questionIcon").hide();
		$("#questionIcon_popup").hide();
		displayStatusDashboard();
		displaySecondaryStatusByDistrict();
	}
}


function displaySecondaryStatusByDistrict()
{
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	
	/*var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;*/
	
	var branchId=0;
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;

	var jobCategory=document.getElementById("txtjobCategory").value;
			
	var jobSubCategory=document.getElementById("txtjobSubCategory").value;
	
	if(jobSubCategory!=null && jobSubCategory!=""){
		jobCategory = jobSubCategory;
	}
	
	 
	ManageStatusAjax.displaySecondaryStatusByDistrictOrJobCategoryOp(headQuarterId,branchId,districtId,jobCategory,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("tree").innerHTML=data;
			treeSetup();
		}
	});
	
	}
	 
 


function copyDataFromDistrictToJobCategory()
{
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	
	/*var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;*/
	var branchId=0;
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	
	var jobCategory=document.getElementById("txtjobCategory").value;
	
	var jobSubCategory=document.getElementById("txtjobSubCategory").value;
	
	if(jobSubCategory!=null && jobSubCategory!=""){
		jobCategory = jobSubCategory;
	}
	 
	
 
		ManageStatusAjax.copyDataFromDistrictToJobCategoryOp(headQuarterId,branchId,districtId,jobCategory,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert("data "+data);
			}
		});	
	}
	 

function resetQuestionForm()
{
	document.getElementById("question").value="";
	document.getElementById("scoringCaption").value="";
	document.getElementById("previousScore").value="0";
	document.getElementById("questionId").value="";
	$('#ifrmScore').attr('src', "slideract.do?name=sliderScore&tickInterval=10&max=100&swidth=360&svalue=0&step=1");
	$('#ifrmJSIScore').attr('src', "slideract.do?name=sliderJSIScore&tickInterval=10&max=100&swidth=360&svalue=0&step=1");

}
function displayslider()
{
	var scoringCaption=document.getElementById("scoringCaption").value;
	if(scoringCaption!=null && scoringCaption!='')
	{
		$("#score_slider_div").show();
	}
	else
	{
		$("#score_slider_div").hide();
		$('#ifrmScore').attr('src', "slideract.do?name=sliderScore&tickInterval=10&max=100&swidth=360&svalue=0&step=1");
	}
}

function getJobCategoryWiseStatusPrivilege()
{
	var elements = document.getElementsByName("updatejobRadio");
	try{
	  for (i=0;i<elements.length;i++) 
	  {
		elements[i].checked = false;
	  }
	 }catch(e){}
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	var jobCategoryId=document.getElementById("txtjobCategory").value;
	
	var txtjobSubCategory="0";
	try{
		txtjobSubCategory=document.getElementById("jobSubCategoryId").value;
	}catch(e){}
	if(txtjobSubCategory!="" && txtjobSubCategory!="0"){
		jobCategoryId=txtjobSubCategory;
	}
	
	var secondaryStatusId=document.getElementById("myFolderId").value;
	try{
	document.getElementById("secondaryStatusId_INS").value=secondaryStatusId;
	}catch(e){}	
	if(districtId!="" && districtId>0){
	ManageStatusAjax.getJobCategoryWiseStatusPrivilege(districtId,jobCategoryId,secondaryStatusId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	//displayDistLevel
		
			if(data.finalizeForEvent!=null){
				document.getElementById("finalizeforevnt").checked=data.finalizeForEvent;
			}
			if(data.internalExternalOrBothCandidate!=0){
				if(data.internalExternalOrBothCandidate==1){
					$('#bothIntAndExtCand').prop('checked', true);
					$('#internalCand').prop('checked', true);
					$('#externalCand').prop('checked', true);
				}else if(data.internalExternalOrBothCandidate==2){
					$('#internalCand').prop('checked', true);
				}
				else if(data.internalExternalOrBothCandidate==3){
					$('#externalCand').prop('checked', true);
				}
			}
			document.getElementById("panel").checked=data.panel;			
			document.getElementById("autoUpdateStatus").checked=data.autoUpdateStatus;
			document.getElementById("autoUpdateStatus2").checked=data.autoUpdateStatus;
			document.getElementById("panelJSI").checked=data.panel;
			document.getElementById("jobcategorywisestatusprivilege").checked=data.statusPrivilegeForSchools;
			document.getElementById("jobcategorywisestatusprivilegeJSI").checked=data.statusPrivilegeForSchools;
			
			if(data.statusUpdateExpiryDate!="" && data.statusUpdateExpiryDate!=null){
				try{
					var date = new Date(data.statusUpdateExpiryDate);
					document.getElementById("autoUpdateStatusTill").value=(date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear();
				}catch(e){}
				try{
					var date = new Date(data.statusUpdateExpiryDate);
					document.getElementById("autoUpdateStatusTill2").value=(date.getMonth() + 1) + '-' + date.getDate() + '-' +  date.getFullYear();
				}catch(e){}
			}else{
				document.getElementById("autoUpdateStatusTill").value="";
				document.getElementById("autoUpdateStatusTill2").value="";
			}
			
			if(data.districtMaster.isZoneRequired==1){
				$(".displayDistLevel").show();
			}
			
			if(data.autoUpdateStatus){
				$('.updatejobcategoryDiv').show();
				
				if(data.districtMaster.isZoneRequired==1){
					$(".displayDistLevel").show();
				
					document.getElementById("updatejobRadio3").checked=false;
				}else{
					$(".displayDistLevel").hide();
					document.getElementById("updatejobRadio3").checked=false;				
				}
				if(data.updateStatusOption==0){
					document.getElementById("updatejobRadio1").checked=true;
					//document.getElementById("updatejobRadio21").checked=true;
				}
				if(data.updateStatusOption==1){
					document.getElementById("updatejobRadio2").checked=true;
					//document.getElementById("updatejobRadio22").checked=true;
				}
				if(data.updateStatusOption==2 && data.districtMaster.isZoneRequired==1){
					document.getElementById("updatejobRadio3").checked=true;
					//document.getElementById("updatejobRadio22").checked=true;
				}
			}else{ 
				$('.updatejobcategoryDiv').hide();
				//$('.updatejobcategoryDiv2').hide();
			}
			
			if(data.maxValuePerJSIQuestion!=null && data.autoUpdateStatus){
				$('.updatejobcategoryDiv2').show();
				if(data.districtMaster.isZoneRequired==1){
					$(".displayDistLevel2").show();
					document.getElementById("updatejobRadio23").checked=false;
				}else{
					$(".displayDistLevel2").hide();
					document.getElementById("updatejobRadio23").checked=false;
				}
				
				if(data.updateStatusOption==0){				
					document.getElementById("updatejobRadio21").checked=true;
				}
				if(data.updateStatusOption==1){					
					document.getElementById("updatejobRadio22").checked=true;
				}
				if(data.updateStatusOption==2 && data.districtMaster.isZoneRequired==1){
					document.getElementById("updatejobRadio23").checked=true;
					//document.getElementById("updatejobRadio22").checked=true;
				}
			}else{
				$('.updatejobcategoryDiv2').hide();
			}
			
			if(data.maxValuePerJSIQuestion!=null)
				$('#ifrmJSIScore').attr('src', "slideract.do?name=sliderJSIScore&tickInterval=10&max=100&swidth=360&svalue="+data.maxValuePerJSIQuestion+"&step=1");
			
			$('#iconInstructionFN').tooltip();
			document.getElementById("instructionFileId").value=data.jobCategoryStatusPrivilegeId;
			if(data.instructionAttachFileName!=null && data.instructionAttachFileName!="")
			{	
				document.getElementById("removeInstructionFileNameSpan").style.display="block";
				document.getElementById("divInstructionFileName").style.display="block";
				
				document.getElementById("divInstructionFileName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadAttachInstructionFileName('"+data.secondaryStatus.secondaryStatusId+"','"+data.instructionAttachFileName+"','"+data.secondaryStatus.secondaryStatusName+"');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.instructionAttachFileName+"</a>";
			}
			else
			{
				document.getElementById("removeInstructionFileNameSpan").style.display="none";
				document.getElementById("divInstructionFileName").style.display="none";
			}
			
		}
	});
	}
	
}


function insertOrUpdateQuestion(source)
{
	$('#errMultiDisDiv').empty();
	var isNotError=true;
	
	var isPanelChecked=document.getElementById("panel").checked;	
	var isPanelCheckedJSI=document.getElementById("panelJSI").checked;
	
	var isChecked=document.getElementById("jobcategorywisestatusprivilege").checked;
	var isJSIChecked=document.getElementById("jobcategorywisestatusprivilegeJSI").checked;
	var addAQuestion=document.getElementById('addAQuestion').value;
	var questionId = document.getElementById('questionId').value;
	var question = trim(document.getElementById('question').value);
	var skillAttributeId = document.getElementById('scoringCaption').value;
	var requiredtofinalizenode=document.getElementById('requiredtofinalizenode').checked;
	var jobCategoryId=0;//document.getElementById("txtjobCategory").value;
	var secondaryStatusId=document.getElementById("myFolderId").value;
	var previousScore=document.getElementById("previousScore").value;
	var chkNotifyAll=document.getElementById("chkNotifyAll").checked;
	//added by 05-06-2015
	var txtjobSubCategory =  $("#txtjobSubCategory").val();
	
	if(txtjobSubCategory!=null && txtjobSubCategory!='' && txtjobSubCategory>0)
		jobCategoryId = $("#txtjobSubCategory").val();
	else
		jobCategoryId = document.getElementById("txtjobCategory").value;
	
	var internalExternalOrBothCandidate="0";
	try{
		if($('#bothIntAndExtCand').is(':checked')){
			internalExternalOrBothCandidate=$('#bothIntAndExtCand').val();
		}else if($('#internalCand').is(':checked')){
			internalExternalOrBothCandidate=$('#internalCand').val();
		}else if($('#externalCand').is(':checked')){
			internalExternalOrBothCandidate=$('#externalCand').val();
		}
		//alert("internalExternalOrBothCandidate======="+internalExternalOrBothCandidate);
	}catch(e){}
	
	// 14 September 2015
	var statusForFirstTimeJobApplyVal = "";
	try{
		if($('#statusForFirstTimeJobApply').is(':checked')){
			statusForFirstTimeJobApplyVal = 1;
		}
	}catch(e){}
	//statusForFirstTimeJobApplyVal
	var finalizeforevntVal = "";
	try{
		if($('#finalizeforevnt').is(':checked')){
			finalizeforevntVal = 1;
		}
	}catch(e){}
	
	//ended by 05-06-2015
	if(source=='2'){
		//add by Ram Nath
		if(question=="" && skillAttributeId=="" && addAQuestion=='1')
		{
			$('#errorQuestion').html("&#149; "+resourceJSON.msgQuestionAttributeScore);
			$('#errorQuestion').show();
			$('#question').focus();
			isNotError=false;
			return false;
		}
		//end by Ram Nath
		var isAutoUpdateStatusChecked=document.getElementById("autoUpdateStatus2").checked;
		var statusUpdateExpiryDate=document.getElementById("autoUpdateStatusTill2").value;
		var checked_site_radio = $('input:radio[name=updatejobRadio2]:checked').val();
	}else{
		
		var isAutoUpdateStatusChecked=document.getElementById("autoUpdateStatus").checked;
		var statusUpdateExpiryDate=document.getElementById("autoUpdateStatusTill").value;
		var checked_site_radio = $('input:radio[name=updatejobRadio]:checked').val();
	}
		
	///////////////////////////////////////////////////////////////////////////////////////////////////
	var arrDAIds	=	"";
	var opts = document.getElementById('attachedDAList').options;
	
	var alldisAdm=document.getElementById("alldisAdm").checked;
	var selecteddisAdm=document.getElementById("selecteddisAdm").checked;
	var allSchoolAdm=document.getElementById("allSchoolAdm").checked;
	
	var overrideAdm=document.getElementById("overrideAdm").checked;
	
	if(selecteddisAdm)
	{
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrDAIds+=opts[i].value+"#";
		}
		
		if(document.getElementById("attachedDAList").options.length==0)
		{
				$('#errMultiDisDiv').html("&#149; "+resourceJSON.msgAtLeastOneDistrictAdmin);
				$('#errMultiDisDiv').show();
				$('#attachedDAList').focus();
				isNotError=false;
				return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	var districtId=document.getElementById("districtId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	var maxScore=0;
	var maxJSIScore=0;
	try{
		var ifrmScore = document.getElementById('ifrmScore');
		var innerNorm = ifrmScore.contentDocument || ifrmScore.contentWindow.document;
		if(innerNorm.getElementById('sliderScore').value!=''){
			maxScore=innerNorm.getElementById('sliderScore').value;
		}
	}catch(e){}
	
	try{
		var ifrmJSIScore = document.getElementById('ifrmJSIScore');
		var innerJSINorm = ifrmJSIScore.contentDocument || ifrmJSIScore.contentWindow.document;
		if(innerJSINorm.getElementById('sliderJSIScore').value!=''){
			maxJSIScore=innerJSINorm.getElementById('sliderJSIScore').value;
		}
	}catch(e){}
	
	if(question=="" && skillAttributeId=="" && addAQuestion=='1')
	{
		$('#errorQuestion').html("&#149; "+resourceJSON.msgQuestionAttributeScore);
		$('#errorQuestion').show();
		$('#question').focus();
		isNotError=false;
		return false;
	}

	if(skillAttributeId!="" && maxScore==0 && addAQuestion=='1')
	{
		$('#errorQuestion').html("&#149; "+resourceJSON.msgScoreGreaterthan0);
		$('#errorQuestion').show();
		isNotError=false;
		return false;
	}
	
	if(isAutoUpdateStatusChecked)
	{
	     if(checked_site_radio==undefined)
	     {
	    	 if(source=='2'){
	    		 $('#errorOption2').html("&#149; "+resourceJSON.msgSelectAnyOption);			
			return false;
	    	 }else{
	    		 
	    		 $('#errorOption').html("&#149; "+resourceJSON.msgSelectAnyOption);
	    		 $('#errorOption').show();
	 			return false;
	    	 }
		 }else{
			 $('#errorOption').hide();
		 }
	    
	}
	else
		checked_site_radio=null;
	
	
	if(isNotError){
		var jobCategoryStatusPrivilegeId=document.getElementById("instructionFileId").value;
		var instructionFN = document.getElementById("instructionFN");
		var cnt=0;
		var focs=0;	
		var instructionFileName = instructionFN.value;
		if(source=='1' && instructionFileName!="")
		{
			var instructionFileNameExtension = instructionFileName.substr(instructionFileName.lastIndexOf('.') + 1).toLowerCase();
			var instructionFileSize=0;		
			if ($.browser.msie==true){	
				instructionFileSize = 0;	   
			}else{		
				if(instructionFN.files[0]!=undefined)
					instructionFileSize = instructionFN.files[0].size;
			}	
				
			$('#errordivInstruction').empty();
			
			if(instructionFileNameExtension!="")
			{
				if(!(instructionFileNameExtension=='jpg' || instructionFileNameExtension=='jpeg' || instructionFileNameExtension=='gif' || instructionFileNameExtension=='png' || instructionFileNameExtension=='pdf' || instructionFileNameExtension=='doc' || instructionFileNameExtension=='docx' || instructionFileNameExtension=='txt'))
				{
					$('#errordivInstruction').append("&#149; "+resourceJSON.msgaacptfileformate+"<br>");
						if(focs==0)
							$('#instructionFN').focus();
						
						$('#instructionFN').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}	
				else if(instructionFileSize>=10485760)
				{
					$('#errordivInstruction').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
						if(focs==0)
							$('#instructionFN').focus();
						
						$('#instructionFN').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}
			}
			
			if(cnt!=0)		
			{
				$('#errordivInstruction').show();
				return false;
			}
			else
			{
				if(instructionFileName!="")
				{	
					$('#loadingDiv').show();
					document.getElementById("frmStatusInstruction").submit();
				}
			}
		}
		else
		{	
			ManageStatusAjax.saveOrUpdateQuestion(questionId,requiredtofinalizenode,districtId,jobCategoryId,secondaryStatusId,question,skillAttributeId,maxScore,maxJSIScore,isChecked,isJSIChecked,source,'',isPanelChecked,isPanelCheckedJSI,isAutoUpdateStatusChecked,checked_site_radio,statusUpdateExpiryDate,arrDAIds,allSchoolAdm,overrideAdm,chkNotifyAll,internalExternalOrBothCandidate,statusForFirstTimeJobApplyVal,finalizeforevntVal,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data=='0'){
						resetQuestionForm();
						getQuestionListByDistrictAndJobCategory();
						cancelQuestion();
					}else if(data=='1'){
						resetQuestionForm();
						getQuestionListByDistrictAndJobCategory();
						cancelQuestion();
						$("#myModalQuestionList").modal("hide");
						$("#myModalSaveConfirm").modal("show");
					}else if(data=='2'){
						//add by ram nath
						if(addAQuestion=='1' && question!="")
							insertOrUpdateQuestion(0);
						try{
							$('#jsiQuestion').remove();
						}catch(e){}
						
						//end by Ram Nath
						resetQuestionForm();
						getQuestionListByDistrictAndJobCategory();
						cancelQuestion();
						$("#myModalMaxQuestion").modal("hide");
						$("#myModalSaveConfirm").modal("show");
					}/*else if(data=='4'){
						$('#errorQuestion').html("&#149; You have already set a score against this attribute. Please select some other attribute to set the score.");
						$('#errorQuestion').show();
					}*/
				}
			});
		}
		
	}
}


function getQuestionListByDistrictAndJobCategory()
{
	var districtId=document.getElementById("districtId").value;
	var headQuarterId = "";
	try{
		headQuarterId = document.getElementById("headQuarterId").value;
	}catch(e){}
	if(districtId==null || districtId=="")
		districtId=0;
	
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	var jobCategoryId=0;//document.getElementById("txtjobCategory").value;
	var secondaryStatusId=document.getElementById("myFolderId").value;
	var txtjobSubCategory = $("#txtjobSubCategory").val();
	if(txtjobSubCategory!=null && txtjobSubCategory!='' && txtjobSubCategory>0)
		jobCategoryId = $("#txtjobSubCategory").val();
	else
		jobCategoryId = document.getElementById("txtjobCategory").value;
	
	
	ManageStatusAjax.getQuestionListByDistrictAndJobCategory(headQuarterId,districtId,jobCategoryId,secondaryStatusId,noOfRows,page,sortOrderStr,sortOrderType,false,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
		try{
			document.getElementById("divGridQuestionList").innerHTML=data;
		}catch(e){}
		}
	});
}


function editQuestion(questionId)
{
	$('#errorQuestion').empty();
	$('#errMultiDisDiv').empty();
	resetQuestionForm();
	
	ManageStatusAjax.editQuestion(questionId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null)
			{
				showQuestionForm();
				document.getElementById("question").value=data.question;
				document.getElementById("previousScore").value=data.maxScore;
				document.getElementById("questionId").value=data.questionId;
				
				document.getElementById("requiredtofinalizenode").checked=data.isQuestionRequired;
				if(data.skillAttributesMaster!=null)
					document.getElementById("scoringCaption").value=data.skillAttributesMaster.skillAttributeId;
				else
					document.getElementById("scoringCaption").value="";
				//displayslider();
				
				$('#ifrmScore').attr('src', "slideract.do?name=sliderScore&tickInterval=10&max=100&swidth=360&svalue="+data.maxScore+"&step=1");
				
			}
		}
	});
	//getQuestionListByDistrictAndJobCategory
	
}

function activateDeactivateQuestion(questionId,status)
{
	ManageStatusAjax.activateDeactivateQuestion(questionId,status,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			getQuestionListByDistrictAndJobCategory();
		}
	});
	
}

//*************************************

function bindContextMenu(span) {
	$(span).contextMenu({menu: "myMenu"}, function(action, el, pos) {
		var node = $.ui.dynatree.getNode(el);
		switch( action ) {
		case "cut":
		case "copy":
		case "paste":
			copyPaste(action, node);
			break;
		case "edit":
		editNode(node);
		break;
		case "create":
		node.activate(true);
		createStatusFolder(node);
		break;
		case "delete":
		checkFolderIsDeleted(node);
		break;
		
		case "question":
		node.activate(true);
		getQuestionDiv(node);
		break;
		
		default:
		}
	});
}


function treeSetup() {
	
	 //var $ = jQuery.noConflict();
	 
	 $("#tree").dynatree({
			persist: true,
			onActivate: function(node) {
				$('#errortreediv').hide();
				$("#echoActivated").text(node.data.title + ", key=" + node.data.key);
			},
			onClick: function(node, event) {
				// Close menu on click
				if( $(".contextMenu:visible").length > 0 ){
					$(".contextMenu").hide();
				}
			},
			onKeydown: function(node, event) {
				// Eat keyboard events, when a menu is open
				if( $(".contextMenu:visible").length > 0 )
					return false;

				switch( event.which ) {

				// Open context menu on [Space] key (simulate right click)
				case 32: // [Space]
					$(node.span).trigger("mousedown", {
						preventDefault: true,
						button: 2
						})
					.trigger("mouseup", {
						preventDefault: true,
						pageX: node.span.offsetLeft,
						pageY: node.span.offsetTop,
						button: 2
						});
					return false;

				// Handle Ctrl-C, -X and -V
				case 67:
					if( event.ctrlKey ) { // Ctrl-C
						copyPaste("copy", node);
						return false;
					}
					break;
				case 86:
					if( event.ctrlKey ) { // Ctrl-V
						copyPaste("paste", node);
						return false;
					}
					break;
				case 88:
					if( event.ctrlKey ) { // Ctrl-X
						copyPaste("cut", node);
						return false;
					}
					break;
				}
			},
			/*Bind context menu for every node when it's DOM element is created.
			  We do it here, so we can also bind to lazy nodes, which do not
			  exist at load-time. (abeautifulsite.net menu control does not
			  support event delegation)*/
			onCreate: function(node, span){
			 	node.expand(false);
				bindContextMenu(span);
			},
			onFocus: function(node) {
				$("#echoFocused").text(node.data.title);
			},
			onBlur: function(node) {
				$("#echoFocused").text("-");
			},
			/*Load lazy content (to show that context menu will work for new items too)*/
			onLazyRead: function(node){
				node.appendAjax({
					url: "contextmenu/sample-data2.json"
				});
			},
		/* D'n'd, just to show it's compatible with a context menu.
		   See http://code.google.com/p/dynatree/issues/detail?id=174 */
		dnd: {
			preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
			onDragStart: function(node) {
				return true;
			},
			onDragEnter: function(node, sourceNode) {
				if(node.parent !== sourceNode.parent)
					return false;
				return ["before", "after"];
			},
			onDrop: function(node, sourceNode, hitMode, ui, draggable) {
				sourceNode.move(node, hitMode);
			}
		}
		});
	 
	 
	 try{
			var tree = $('#tree').dynatree("getTree");
			var root = tree.getRoot();
			var nodeList = root.getChildren();
			nodeList[0].activate(true);
		}catch(err){}
		
		
		/* ---------------------- Create Folder Method Start here --------------- */
		$("#btnAddCode").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			createStatusFolder(activeNode);
		});
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
	$("#renameFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			editNode(activeNode);
		});

/*-----------------  Cut Copy Paste Folder ------------------------*/
	$("#cutFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			copyPaste('cut', activeNode);
		});

	$("#copyFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			copyPaste('copy', activeNode);
		});

	$("#pasteFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			copyPaste('paste', activeNode);
		});
		
	$("#deletFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			 checkFolderIsDeleted(activeNode);
		});	
	
	$("#addQuestion").click(function(){
			clearCheckBoxIntExtRequied();
			var activeNode = $("#tree").dynatree("getActiveNode");
			//add by Ram Nath
			var str=""+activeNode;
			var activeStatus=str.split(':')[1].trim().toLowerCase().substring(1,(str.split(':')[1].trim().length-1));
			try{
			if(activeStatus=='jsi'){
				$('#myModalMaxQuestion .modal-body .control-group').after(getAddQuestionHtml());
			}
			}catch(e){}
			//end by Ram Nath
			 getQuestionDiv(activeNode);
		});	
	
	
	var isMac = /Mac/.test(navigator.platform);
	
	$("#tree").dynatree({
		title: "Event samples",
		onClick: function(node, event) {
			if( event.shiftKey ){
				editNode(node);
				return false;
			}
		},
		onDblClick: function(node, event) {
			editNode(node);
			return false;
		},
		onKeydown: function(node, event) {//-----------------
			if( $(".contextMenu:visible").length > 0 )
					return false;

				switch( event.which ) {
				// Open context menu on [Space] key (simulate right click)
				case 32: // [Space]
					$(node.span).trigger("mousedown", {
						preventDefault: true,
						button: 2
						})
					.trigger("mouseup", {
						preventDefault: true,
						pageX: node.span.offsetLeft,
						pageY: node.span.offsetTop,
						button: 2
						});
					return false;

				// Handle Ctrl-C, -X and -V
				case 67:
					if( event.ctrlKey ) { // Ctrl-C
						copyPaste("copy", node);
						return false;
					}
					break;
				case 86:
					if( event.ctrlKey ) { // Ctrl-V
						copyPaste("paste", node);
						return false;
					}
					break;
				case 88:
					if( event.ctrlKey ) { // Ctrl-X
						copyPaste("cut", node);
						return false;
					}
					break;
				
				case 113: // [F2]
				editNode(node);
				return false;
				case 13: // [enter]
					if( isMac ){
						editNode(node);
						return false;
					}
				}
			 //======== Function End ============
		}
		//---------------
		
	});

}


// ************************** Start Auto Suggest for District **************
/*
///////////////////////////////////////////////////////////////////////////////////////////////
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
	var branchId=document.getElementById("branchId").value;
	if(branchId==null || branchId=="")
		branchId=0;
	
	var headQuarterId=document.getElementById("headQuarterId").value;
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId=0;
	AutoSearchFilterAjax.getFieldOfDistrictListByBranchAndHead(districtName,branchId,headQuarterId,{ 
		//BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
*/
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(document.getElementById("districtName").value==""){
		//document.getElementById('schoolName').readOnly=true;
		//document.getElementById('schoolName').value="";
		//document.getElementById('schoolId').value="0";
	}
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
			//document.getElementById('schoolName').readOnly=false;
			//document.getElementById('schoolName').value="";
			//document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


//************** End Auto Suggest ****************************


// Start ... Upload instruction file

function clearInstructionFile(){
	// line added by ashish ratan for IE 10-11
	// replace field with old html
	// comment by ashish ratan
		$('#instructionFN')
		.replaceWith(
				"<input id='instructionFN' name='instructionFN' type='file' width='20px;' />");	 
		
	document.getElementById("instructionFN").value=null;
	document.getElementById("instructionFN").reset();
	$('#instructionFN').css("background-color","");
}

function removeInstructionConfirmMsg()
{
	$('#modalDownloadIAFNdeleteFile').modal('show');
}
function removeInstructionFile()
{
	$('#modalDownloadIAFNdeleteFile').modal('hide');
	var instructionFileId=document.getElementById("instructionFileId").value;
	if(instructionFileId > 0)
	{
			$('#loadingDiv').show();
			ManageStatusAjax.removeInstructionFile(instructionFileId,{ 
				async: false,
				callback: function(data)
				{
					$('#loadingDiv').hide();
					if(data!=null)
					{
						document.getElementById("removeInstructionFileNameSpan").style.display="none";
						document.getElementById("divInstructionFileName").style.display="none";
					}
				},errorHandler:handleError
			});
	}
}

function downloadAttachInstructionFileName(filePath,fileName,secondaryStatusName)
{
	$('#loadingDiv').show();
	ManageStatusAjax.downloadAttachInstructionFile(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(data!='')
			{
				if (data.indexOf(".doc") !=-1) 
				{
					try{$('#modalDownloadIAFN').modal('hide');}catch(err){}
				    document.getElementById('ifrmAttachInstructionFileName').src = ""+data+"";
				}
				else
				{
					document.getElementById('ifrmAttachInstructionFileName').src = ""+data+"";
					try{
						$( "#modalDownloadIAFN" ).draggable({
							handle:'#modalDownloadIAFNMove'
						});
						$('#modalDownloadIAFN').modal('hide');
						$('#myModalLabelInstructionHeader').html(resourceJSON.msgInstructionsFor+""+secondaryStatusName);
						$('#modalDownloadIAFN').modal('show');
					}catch(err){}
				}
			}
		}
	});
}

function insertOrUpdateQuestionCallFromServlet(strFileName)
{
	$('#loadingDiv').hide();
	document.getElementById("instructionFN").value=null;
	$('#instructionFN').css("background-color","");
	var isPanelChecked=document.getElementById("panel").checked;
	var isPanelCheckedJSI=document.getElementById("panelJSI").checked;
	var isAutoUpdateStatusChecked=document.getElementById("autoUpdateStatus").checked;
	var isChecked=document.getElementById("jobcategorywisestatusprivilege").checked;
	var isJSIChecked=document.getElementById("jobcategorywisestatusprivilegeJSI").checked;
	var addAQuestion=document.getElementById('addAQuestion').value;
	var questionId = document.getElementById('questionId').value;
	var question = document.getElementById('question').value;
	var skillAttributeId = document.getElementById('scoringCaption').value;
	var jobCategoryId=0;//document.getElementById("txtjobCategory").value;
	var secondaryStatusId=document.getElementById("myFolderId").value;
	var previousScore=document.getElementById("previousScore").value;
	var districtId=document.getElementById("districtId").value;
	//added by 05-06-2015
	var internalExternalOrBothCandidate="0";
	try{
		if($('#bothIntAndExtCand').is(':checked')){
			internalExternalOrBothCandidate=$('#bothIntAndExtCand').val();
		}else if($('#internalCand').is(':checked')){
			internalExternalOrBothCandidate=$('#internalCand').val();
		}else if($('#externalCand').is(':checked')){
			internalExternalOrBothCandidate=$('#externalCand').val();
		}
		//alert("internalExternalOrBothCandidate======="+internalExternalOrBothCandidate);
	}catch(e){}
	//ended by 05-06-2015
	
	var statusForFirstTimeJobApplyVal = "";
	try{
		if($('#statusForFirstTimeJobApply').is(':checked')){
			statusForFirstTimeJobApplyVal = 1;
		}
	}catch(e){}
	
	var finalizeforevntVal = "";
	try{
		if($('#finalizeforevnt').is(':checked')){
			finalizeforevntVal = 1;
		}
	}catch(e){}
	
	if(districtId==null || districtId=="")
		districtId=0;
	var maxScore=0;
	var maxJSIScore=0;
	var txtjobSubCategory = $("#txtjobSubCategory").val();
	
	
	if(txtjobSubCategory!=null && txtjobSubCategory!='' && txtjobSubCategory>0)
		jobCategoryId = $("#txtjobSubCategory").val();
	else
		jobCategoryId = document.getElementById("txtjobCategory").value;
	
///////////////////////////////////////////////////////////////////////////////////////////////////
	var arrDAIds	=	"";
	var opts = document.getElementById('attachedDAList').options;
	
	var alldisAdm=document.getElementById("alldisAdm").checked;
	var selecteddisAdm=document.getElementById("selecteddisAdm").checked;
	var allSchoolAdm=document.getElementById("allSchoolAdm").checked;
	
	var overrideAdm=document.getElementById("overrideAdm").checked;
	
	var chkNotifyAll=document.getElementById("chkNotifyAll").checked;
	
	if(selecteddisAdm)
	{
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrDAIds+=opts[i].value+"#";
		}
		
		if(document.getElementById("attachedDAList").options.length==0)
		{
				$('#errMultiDisDiv').html("&#149; "+resourceJSON.msgAtLeastOneDistrictAdmin);
				$('#errMultiDisDiv').show();
				$('#attachedDAList').focus();
				isNotError=false;
				return false;
		}
	}
	//////////////////////////////////////////////////////////////////////////////////////////////////
	
	try{
		var ifrmScore = document.getElementById('ifrmScore');
		var innerNorm = ifrmScore.contentDocument || ifrmScore.contentWindow.document;
		if(innerNorm.getElementById('sliderScore').value!=''){
			maxScore=innerNorm.getElementById('sliderScore').value;
		}
	}catch(e){}
	
	try{
		var ifrmJSIScore = document.getElementById('ifrmJSIScore');
		var innerJSINorm = ifrmJSIScore.contentDocument || ifrmJSIScore.contentWindow.document;
		if(innerJSINorm.getElementById('sliderJSIScore').value!=''){
			maxJSIScore=innerJSINorm.getElementById('sliderJSIScore').value;
		}
	}catch(e){}
	
	var isAutoUpdateStatusChecked=document.getElementById("autoUpdateStatus").checked;
	var statusUpdateExpiryDate=document.getElementById("autoUpdateStatusTill").value;
	var checked_site_radio = $('input:radio[name=updatejobRadio]:checked').val();
	var requiredtofinalizenode=document.getElementById('requiredtofinalizenode').checked;

	if(isAutoUpdateStatusChecked)
	{}else
		checked_site_radio=2;

	ManageStatusAjax.saveOrUpdateQuestion(questionId,requiredtofinalizenode,districtId,jobCategoryId,secondaryStatusId,question,skillAttributeId,maxScore,maxJSIScore,isChecked,isJSIChecked,1,strFileName,isPanelChecked,isPanelCheckedJSI,isAutoUpdateStatusChecked,checked_site_radio,statusUpdateExpiryDate,arrDAIds,allSchoolAdm,overrideAdm,chkNotifyAll,internalExternalOrBothCandidate,statusForFirstTimeJobApplyVal,finalizeforevntVal,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data=='0'){
				resetQuestionForm();
				getQuestionListByDistrictAndJobCategory();
				cancelQuestion();
			}else if(data=='1'){
				resetQuestionForm();
				getQuestionListByDistrictAndJobCategory();
				cancelQuestion();
				$("#myModalQuestionList").modal("hide");
				$("#myModalSaveConfirm").modal("show");
			}else if(data=='2'){
				resetQuestionForm();
				getQuestionListByDistrictAndJobCategory();
				cancelQuestion();
				$("#myModalMaxQuestion").modal("hide");
				$("#myModalSaveConfirm").modal("show");
			}/*else if(data=='4'){
				$('#errorQuestion').html("&#149; You have already set a score against this attribute. Please select some other attribute to set the score.");
				$('#errorQuestion').show();
			}*/
		}
	});

		

}

// End ... Upload instruction file

function unableOrDisablexheckBoxes()
{
	
	
	 var isAutoUpdateStatusChecked=document.getElementById("autoUpdateStatus").checked;
	 if(isAutoUpdateStatusChecked)
	 {
	   $('.updatejobcategoryDiv').show();
	   //$('#"errorOption"').empty();
	 }
	 else
	 {
		 $('.updatejobcategoryDiv').hide();
		 var elements = document.getElementsByName("updatejobRadio");						 
		 for (i=0;i<elements.length;i++) 
		 {
			elements[i].checked = false;
		 }
		 $('#errorOption').empty();
	 }
	 
}

function hideshowHqCheckBoxes(){
	var isAutoUpdateStatusChecked=document.getElementById("autoUpdateStatus").checked;
	 if(isAutoUpdateStatusChecked)
	 {
	   $('.autoStatusDivForkelly').show();
	 }
	 else
	 {
		 $('.autoStatusDivForkelly').hide();
	 }
}
function unableOrDisablexheckBoxes2()
{
	
	
	 var isAutoUpdateStatusChecked=document.getElementById("autoUpdateStatus2").checked;
	 if(isAutoUpdateStatusChecked)
	 {
	   $('.updatejobcategoryDiv2').show();
	   //$('#"errorOption"').empty();
	 }
	 else
	 {
		 $('.updatejobcategoryDiv2').hide();
		 var elements = document.getElementsByName("updatejobRadio2");

		 document.getElementsByName("autoUpdateStatusTill2").value="";
		 for (i=0;i<elements.length;i++) 
		 {
			elements[i].checked = false;
		 }
		 $('#errorOption2').empty();
	 }
}

function chkDistrictAdmins(val)
{
	if(val==0)
	{
		$('#selecteddisAdm').checked=false;
		$('#alldisAdm').checked=true;
		$('#multiDisAdminDiv').hide();
		
	}
	else if(val==1)
	{
		$('#alldisAdm').checked=false;
		$('#selecteddisAdm').checked=true;
		$('#multiDisAdminDiv').show();
	}
}



function getJobSubCateList()
{
	
	var districtId = $("#districtId").val();
	var jobCategoryId = $("#jobCategory").val();
	var txtjobSubCategory =  $("#txtjobSubCategory").val();
	
	if(districtId=='')
		districtId=0;
	
	
	if(jobCategoryId=='')
		jobCategoryId=0;
	
		ManageStatusAjax.getJobSubCateForSeach(jobCategoryId,districtId,txtjobSubCategory,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=null && data!="")
			{
				$("#jobSubCateDiv").html(data);
			}
		},
	});
}











function displayAllAndSelecedDist()
{
	var districtId=document.getElementById("districtId").value;
	var headQuarterId = "";
	try{
		headQuarterId = document.getElementById("headQuarterId").value;
	}catch(e){}
	if(headQuarterId==null || headQuarterId=="")
		headQuarterId = 0;
	
	if(districtId==null || districtId=="")
		districtId=0;
	var jobCategoryId=document.getElementById("txtjobCategory").value;
	var secondaryStatusId=document.getElementById("myFolderId").value;
	ManageStatusAjax.displayAllAndSelecedDist(headQuarterId,districtId,jobCategoryId,secondaryStatusId,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			var dataArr=data.split('||');
			if(districtId!=null && districtId>0){
				document.getElementById("1stSchoolDiv").innerHTML=dataArr[0];
				document.getElementById("2ndSchoolDiv").innerHTML=dataArr[1];
				if(dataArr[2]=='true')
				{
					$('#multiDisAdminDiv').show();
					document.getElementById("alldisAdm").checked=false;
					document.getElementById("selecteddisAdm").checked=true;
				}
				else if(dataArr[2]=='false')
				{
					document.getElementById("alldisAdm").checked=true;
					document.getElementById("selecteddisAdm").checked=false;
					$('#multiDisAdminDiv').hide();
				}

				if(dataArr[3]=='true')
					document.getElementById("allSchoolAdm").checked=true;
				else
					document.getElementById("allSchoolAdm").checked=false;


				if(dataArr[4]=='true')
					document.getElementById("overrideAdm").checked=true;
				else
					document.getElementById("overrideAdm").checked=false;

				if(dataArr[5]=='true')
					document.getElementById("chkNotifyAll").checked=true;
				else
					document.getElementById("chkNotifyAll").checked=false;
				
				try{
					if(dataArr[6]=='true')
						document.getElementById("statusForFirstTimeJobApply").checked=true;
					else
						document.getElementById("statusForFirstTimeJobApply").checked=false;
				}catch(e){}
				/*try{
					if(dataArr[7]=='true')
						document.getElementById("finalizeforevnt").checked=true;
				}catch(e){}*/
				
				
			}
			else if(headQuarterId!=null && headQuarterId>0){
				resetAttributesForHQ();
				var dataArr=data.split('||');
				if(dataArr[5]!=null && dataArr[5]!="" && dataArr[5]!="false"){
					$('#statusnotify').prop('checked', true);
					$("#statusNotifyDiv").show();
					if(dataArr[5]=="0"){
						$('input:checkbox[name=selectedhqbradmin]')[0].checked = true;
						$('input:checkbox[name=selectedhqbradmin]')[1].checked = false;
						$('input:checkbox[name=allorselectedbranchadmins]')[1].checked = false;
						$('input:checkbox[name=allorselectedbranchadmins]')[0].checked = false;
						$("#hqAdminsDiv").show();
						$("#branchAdminsDiv").hide();
						$("#selAllBranches").hide();
						$("#lstHQAdmins").html(dataArr[0]);
						$("#attachedHQList").html(dataArr[1]);
					}
					else if(dataArr[5]=="1"){
						$('input:checkbox[name=selectedhqbradmin]')[0].checked = false;
						$('input:checkbox[name=selectedhqbradmin]')[1].checked = true;
						$('input:checkbox[name=allorselectedbranchadmins]')[1].checked = false;
						$('input:checkbox[name=allorselectedbranchadmins]')[0].checked = true;
						$("#selAllBranches").show();
						$("#hqAdminsDiv").hide();
						$("#branchAdminsDiv").hide();
					}
					else if(dataArr[5]=="2"){
						$('input:checkbox[name=selectedhqbradmin]')[0].checked = false;
						$('input:checkbox[name=selectedhqbradmin]')[1].checked = true;
						$('input:checkbox[name=allorselectedbranchadmins]')[0].checked = false;
						$('input:checkbox[name=allorselectedbranchadmins]')[1].checked = true;
						$("#hqAdminsDiv").hide();
						$("#branchAdminsDiv").show();
						$("#selAllBranches").show();
						$("#lstBranchAdmins").html(dataArr[0]);
						$("#attachedBAList").html(dataArr[1]);
					}
				}
				if(dataArr[2]=="1"){
					$('input:checkbox[name=isBranchHQSet]')[0].checked = false;
					$('input:checkbox[name=isBranchHQSet]')[1].checked = true;
				}							
				else if(dataArr[2]=="0"){
					$('input:checkbox[name=isBranchHQSet]')[0].checked = true;
					$('input:checkbox[name=isBranchHQSet]')[1].checked = false;
				}
				if(dataArr[3]!=""){
					if(dataArr[3]=="1"){
						$('#autoUpdateStatus').prop('checked', true);
						$('.autoStatusDivForkelly').show();
						$('input:checkbox[name=autoStatusForAllAny]')[0].checked = true;
						$('input:checkbox[name=autoStatusForAllAny]')[1].checked = false;
					}							
					else if(dataArr[3]=="0"){
						$('#autoUpdateStatus').prop('checked', true);
						$('.autoStatusDivForkelly').show();
						$('input:checkbox[name=autoStatusForAllAny]')[0].checked = false;
						$('input:checkbox[name=autoStatusForAllAny]')[1].checked = true;
					}

					if(dataArr[4]!="" && dataArr[4].length>0 && dataArr[4]!="false"){
						$('#autoStatusForDate').prop('checked', true);
						$('.autoStatusDivForkelly').show();
						$('#autoUpdateStatusTill').val(dataArr[4]);
					}
				}
			}
		}
	});
}

//add by Ram Nath for jsi attribute
function showQuestionForm1(){
	var ifrmJSIScore = document.getElementById('ifrmJSIScore');
	var ifrmJSIScoreForm = ifrmJSIScore.contentDocument || ifrmJSIScore.contentWindow.document;
	var ifrmJSIScoreValue = ifrmJSIScoreForm.getElementById('sliderJSIScore').value;	
	//var src1=$('#ifrmJSIScore').attr('src');
	//alert(ifrmJSIScoreValue);
	var src=$('#ifrmJSIScore').attr('src').split('=');
	showQuestionForm();
	for(var r=0;r<src.length;r++)
	{	
		if(src[r].indexOf('&svalue') >= 0){
			var value= src[r+1].split('&');			
			src[r+1]=ifrmJSIScoreValue+"&"+value[1];
		}
	}
	var srcChangeValue='';
	for(var r=0;r<src.length;r++)
		if(r!=(src.length-1))
		srcChangeValue+=src[r]+"=";
		else
			srcChangeValue+=src[r];		
	//alert("src 1=="+src1+" \n src 2=="+srcChangeValue);
	$('#ifrmJSIScore').attr('src',srcChangeValue);
	$('#sliderJSIScore').val(ifrmJSIScoreValue);
	//ifrmJSIScoreForm.getElementById('sliderJSIScore').value=ifrmJSIScoreValue;
}


function getAddQuestionHtml(){
	var attribute=$('#scoringCaption').html();
	var allDiv="<div id='jsiQuestion'><div class='row'>"+
				 "<div class='col-sm-12 col-md-12'>"+
					"<div class='add-Question  pull-right'>"+
							"<a href='javascript:void(0);' onclick='showQuestionForm1();'>+ "+resourceJSON.msgAddQuestion+"</a>"+
					"</div>	"+
				"</div>"+
				"</div>"+				
				"<div class='row'><div class='col-sm-4 col-md-4 ' id='divGridQuestionList'></div></div>"+				
				"<div id='divQuestionList' class='mt10 ' style='display: none;'>"+
						"<div class='row'>"+
					       "<div class='col-sm-8 col-md-8'>"+
								"<div class='divErrorMsg' id='errorQuestion'></div>"+
							"</div>"+
				      	"</div>"+
			    		"<div class='row'>"+
					       "<div class='col-sm-8 col-md-8'>"+
					       "<span class=''><label>"+resourceJSON.msgQuestion2+"</label>"+
					       	"<input type='text' id='question' name='question'  maxlength='2500' class='form-control'/>"+
					       "</span>"+
					       "</div>"+
				      	"</div>"+				      	
				    	"<div class='row'>"+
				    		"<div class='col-sm-8 col-md-8'>"+
						       	"<span><label>"+resourceJSON.msgAttributetoScore+"&nbsp;<a href='#' id='scorecaptionInfo' rel='tooltip' data-original-title='Select an attribute to score'><img src='images/qua-icon.png' width='15' height='15' alt=''></a></label>"+
						       	"<select class='form-control' id='scoringCaption' name='scoringCaption'>"+
						       	attribute+
								"</select>"+
						       "</span>"+
						   "</div>"+
				    	"</div>"+			    		
			    		"<div class='row top15' style='height: 50px;'>"+
				    			"<div class='col-sm-8 col-md-8'>"+
						       	"<span><label id='scoreLabel'>"+resourceJSON.MsgScore+"</label></br>"+
						       	"<span>"+
						       		"<iframe id='ifrmScore' src='slideract.do?name=sliderScore&tickInterval=10&max=100&swidth=360&svalue=0&step=1' scrolling='no' frameBorder='0' style='padding: 0px; margin-left: -2px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:410px;margin-top:-10px;'></iframe>"+
						       	"</span>"+					       			
						       "</span>"+
						   "</div>"+
				       "</div>"+				       
				       "<div class='row top10'>"+
							"<div class='col-sm-5 col-md-5' id='divDone'>"+
							"<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;' onclick='insertOrUpdateQuestion(0)'>"+
								""+resourceJSON.msgIamDone+""+
							"</a>&nbsp;&nbsp;"+
							"<a class='idone' style='cursor: pointer; margin-left: 0px; text-decoration:none;' onclick='cancelQuestion()'>"+
								""+resourceJSON.btnCancel+""+
							"</a>"+
						"</div>"+
					   "</div>"+
				 "</div></div>";
	return allDiv;
	
}
$(function(){	
$('#myModalMaxQuestion .modal-header .close').click(function(){$('#jsiQuestion').remove(); });
$('#myModalMaxQuestion .modal-footer #cancel').click(function(){$('#jsiQuestion').remove(); });
});
function clearCheckBoxIntExtRequied(){
	$('#bothIntAndExtCand').prop('checked', false);
	$('#internalCand').prop('checked', false);
	$('#externalCand').prop('checked', false);
}

$(function(){
	$('#bothIntAndExtCand').click(function(){
		if($('#bothIntAndExtCand').is(':checked')){
			$('#internalCand').prop('checked', true);
			$('#externalCand').prop('checked', true);
		}else{
			$('#internalCand').prop('checked', false);
			$('#externalCand').prop('checked', false);		
		}
	});
	$('#internalCand').click(function(){
		if($('#internalCand').is(':checked')){
			if($('#externalCand').is(':checked')){
				$('#bothIntAndExtCand').prop('checked', true);
			}
		}else{
			$('#bothIntAndExtCand').prop('checked', false);		
		}
	});
	$('#externalCand').click(function(){
		if($('#externalCand').is(':checked')){
			if($('#internalCand').is(':checked')){
				$('#bothIntAndExtCand').prop('checked', true);
			}
		}else{
			$('#bothIntAndExtCand').prop('checked', false);		
		}
	});
});
//end By 



// added by ankit
function toggleFlag(id1,id2){
	if($('#'+id1+'').is(':checked')){
		$('#'+id2+'').prop('checked', false);
		$('#'+id1+'').prop('checked', true);
	}
}

function resetAttributesForHQ(){
	$("#errMultiDisDiv").empty();
	$('input:checkbox[name=isBranchHQSet]')[0].checked = false;
	$('input:checkbox[name=isBranchHQSet]')[1].checked = false;
	$('#autoUpdateStatus').prop('checked', false);
	$('input:checkbox[name=autoStatusForAllAny]')[0].checked = false;
	$('input:checkbox[name=autoStatusForAllAny]')[1].checked = false;
	$('#autoStatusForDate').prop('checked', false);
	$('.autoStatusDivForkelly').hide();
	$('#autoUpdateStatusTill').val("");
	$('#statusnotify').prop('checked', false);
	$('input:checkbox[name=selectedhqbradmin]')[0].checked = false;
	$('input:checkbox[name=selectedhqbradmin]')[1].checked = false;
	$('input:checkbox[name=allorselectedbranchadmins]')[1].checked = false;
	$('input:checkbox[name=allorselectedbranchadmins]')[0].checked = false;
	$("#hqAdminsDiv").hide();
	$("#branchAdminsDiv").hide();
	$("#selAllBranches").hide();
	$("#statusNotifyDiv").hide();
}

function cancelAttribute(){
	resetAttributesForHQ();
	$("#myModalQuestionList").modal("hide");
}

function displayAllHqAdmins(){
	ManageStatusAjax.displayAllActiveHQAdmins({ 
		async: true,
		callback: function(data)
		{
			if(data!=null && data!="")
			{
				$("#lstHQAdmins").html(data);
			}
		},
	});
}

function displayAllBrAdmins(jobCategoryId){
	ManageStatusAjax.displayAllActiveBranchAdmins(jobCategoryId,{ 
		async: true,
		callback: function(data)
		{
			if(data!=null && data!="")
			{
				$("#lstBranchAdmins").html(data);
			}
		},
	});
}

function hideShowUsersDiv(id1,id2,flag){
	if(flag=="1"){
		if($('#'+id1+'').is(':checked')){
			$('#'+id2+'').show();
		}else{
			$('#'+id2+'').hide();
		}
	}
	else{
		if($('#'+id1+'').is(':checked')){
			$('#'+id2+'').hide();
		}
	}
}

/**
 *  add and update  attributes for hq
 * @return
 */

function addUpdateAttributeForHQ(){
	
	var isBranchHqSet = "";
	if($('input:checkbox[name=isBranchHQSet]')[0].checked == true){
		isBranchHqSet = "0"; // branch can set
	}
	else if($('input:checkbox[name=isBranchHQSet]')[1].checked == true){
		isBranchHqSet = "1"; // hq can set
	}
	
	var autoUpdateStatus = "";
	autoUpdateStatus = $("#autoUpdateStatus").val();
	
	var autoUpdateAllAny = "";
	if($('input:checkbox[name=autoStatusForAllAny]')[0].checked == true){
		autoUpdateAllAny = "0"; // for all jobs
	}
	else if($('input:checkbox[name=autoStatusForAllAny]')[1].checked == true){
		autoUpdateAllAny = "1"; // for any jobs
	}
	
	var autoStatusForDate = $("#autoStatusForDate").val();
	var autoUpdateStatusTill = "";
	if(autoStatusForDate){
		autoUpdateStatusTill = $("#autoUpdateStatusTill").val();
	}
	
	var statusnotify = "";
	statusnotify = $("#statusnotify").val();
	
	var selectedhqbradmin = "";
	if($('input:checkbox[name=selectedhqbradmin]')[0].checked == true){
		selectedhqbradmin = "0"; // selected hq's
	}
	else if($('input:checkbox[name=selectedhqbradmin]')[1].checked == true){
		selectedhqbradmin = "1"; // selected branches
	}
	
	var allorselectedbranchadmins = "";
	
	if($('input:checkbox[name=allorselectedbranchadmins]')[0].checked == true){
		allorselectedbranchadmins = "0"; // for all branch admins
	}
	else if($('input:checkbox[name=allorselectedbranchadmins]')[1].checked == true){
		allorselectedbranchadmins = "1"; // for selected branch admins
	}
	
	var arrUserIds	=	"";
	
	if(selectedhqbradmin=="0"){
		var opts = document.getElementById('attachedHQList').options;
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrUserIds+=opts[i].value+"#";
		}
	}
	else if(allorselectedbranchadmins=="1"){
		var opts = document.getElementById('attachedBAList').options;
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrUserIds+=opts[i].value+"#";
		}
	}
	
	var secondaryStatusId=document.getElementById("myFolderId").value;
	var jobCategory = document.getElementById("jobCategory").value;
	var headQuarterId = document.getElementById("headQuarterId").value;

	ManageStatusAjax.saveHQAttributes(headQuarterId,secondaryStatusId,jobCategory,isBranchHqSet,autoUpdateStatus,autoUpdateAllAny,autoStatusForDate,autoUpdateStatusTill,statusnotify,selectedhqbradmin,allorselectedbranchadmins,arrUserIds,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		callback: function(data)
		{
			if(data!=null && data!="")
			{
				$("#myModalQuestionList").modal("hide");
				resetAttributesForHQ();
				$("#myModalSaveConfirm").modal("show");
				arrUserIds	=	"";
			}
		},
	});
}

function validateAttributesHQ(){
	var errorCount = 0;
	$("#errMultiDisDiv").show();
	$("#errMultiDisDiv").empty();
	var isBranchHqSet = "";
	if($('input:checkbox[name=isBranchHQSet]')[0].checked == true){
		isBranchHqSet = "0"; // branch can set
	}
	else if($('input:checkbox[name=isBranchHQSet]')[1].checked == true){
		isBranchHqSet = "1"; // hq can set
	}
	
	if(isBranchHqSet==""){
		$('#errMultiDisDiv').append("&#149; "+resourceJSON.PlzSlctNodePermsn+"<br>");
		errorCount++;
	}
	
	if(document.getElementById("autoUpdateStatus").checked == true){
		var autoUpdateAllAny = "";
		if($('input:checkbox[name=autoStatusForAllAny]')[0].checked == true){
			autoUpdateAllAny = "0"; // for all jobs
		}
		else if($('input:checkbox[name=autoStatusForAllAny]')[1].checked == true){
			autoUpdateAllAny = "1"; // for any jobs
		}
		
		if(autoUpdateAllAny==""){
			$('#errMultiDisDiv').append("&#149; "+resourceJSON.PlzSlctAutoUpdateOpt+"<br>");
			errorCount++;
		}
		
		if(document.getElementById("autoStatusForDate").checked == true){
			if($("#autoUpdateStatusTill").val()==""){
				$('#errMultiDisDiv').append("&#149; "+resourceJSON.PlzSlctAutoUpdateDate+"<br>");
				if(errorCount==0)
				$('#autoUpdateStatusTill').focus();
				errorCount++;
			}
		}
	}
	
	if(document.getElementById("statusnotify").checked == true){
		var selectedhqbradmin = "";
		if($('input:checkbox[name=selectedhqbradmin]')[0].checked == true){
			selectedhqbradmin = "0"; // selected hq's
		}
		else if($('input:checkbox[name=selectedhqbradmin]')[1].checked == true){
			selectedhqbradmin = "1"; // selected branches
		}
		
		if(selectedhqbradmin==""){
			$('#errMultiDisDiv').append("&#149; "+resourceJSON.PlzSlctHQBRAdmins+"<br>");
			errorCount++;
		}
		else if(selectedhqbradmin=="0"){
			if(document.getElementById("attachedHQList").options.length==0)
			{
				$('#errMultiDisDiv').append("&#149; "+resourceJSON.PlzSlctHQAdmins+"<br>");
				if(errorCount==0)
				$('#attachedHQList').focus();
				errorCount++;
			}
			
		}
		else if(selectedhqbradmin=="1"){
			var allorselectedbranchadmins = "";
			
			if($('input:checkbox[name=allorselectedbranchadmins]')[0].checked == true){
				allorselectedbranchadmins = "0"; // for all branch admins
			}
			else if($('input:checkbox[name=allorselectedbranchadmins]')[1].checked == true){
				allorselectedbranchadmins = "1"; // for selected branch admins
			}
			if(allorselectedbranchadmins==""){
				$('#errMultiDisDiv').append("&#149; "+resourceJSON.PlzSlctBAAdmins+"<br>");
				errorCount++;
			}
			else if((allorselectedbranchadmins=="1") && (document.getElementById("attachedBAList").options.length==0))
			{
				$('#errMultiDisDiv').append("&#149; "+resourceJSON.PlzSlctBAAdmins+"<br>");
				if(errorCount==0)
				$('#attachedBAList').focus();
				errorCount++;
			}
		}		
	}
	
	if(errorCount==0){
			 addUpdateAttributeForHQ();
	}
}
function setEntityType()
{
	var entityString='';	
	var districtSection="<div class='col-sm-12 col-md-12' style='padding-left:65px;'>"+
	        "<label>District Name</label>"+        
		       " <span>"+
		        	"<input type='text' id='districtName'  value='' maxlength='100'  name='districtName' class='help-inline form-control'"+
		          	"onfocus=\"getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');\""+
					"onkeyup=\"getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');\""+
					"onblur=\"hideDistrictMasterDiv(this,'districtId','divTxtShowData'); resetJobCategory();displayJobCategoryByDistrict();\"/>"+
		      	"</span>"+
			"<c:if test='${entityType==1 || entityType==5}'>"+
		      		"<input type='hidden' id='districtId' value='' />"+
		      	"</c:if>"+
		        "<div id='divTxtShowData'  onmouseover='mouseOverChk('divTxtShowData','districtName')' style=' display:none;position:absolute;z-index:5000;' class='result' ></div>"+
      	 "</div>";
	var btnRecord='<button type="button" class="btn btn-primary"  onclick="return displayStatusBody();"><strong>Go <i class="icon" ></i></strong></button>';
	globalSearchSection(entityString,"","",districtSection,btnRecord);
	$("#entityTypeDivMaster").hide();
	$("#districtClassMaster").show();
	
	if($("#districtMasterId").val()==0){
		showSearchAgainMaster();
	}else{
		hideSearchAgainMaster();
	}	
	if($("#distID").val()!=0){
		$("#districtId").val($("#distID").val());
		$("#districtName").val($("#distName").val());
	}
}
