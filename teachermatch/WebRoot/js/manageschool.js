/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/

var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	noOfRows 	= 	document.getElementById("pageSize").value;
	entityID	=	document.getElementById("loggedInEntityType").value;
	searchRecordsByEntityType();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=50;
	}
	entityID	=	document.getElementById("loggedInEntityType").value;
	searchRecordsByEntityType();
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
/*======= Show District on Press Enter Key ========= */
function chkForEnterRecordsByEntityType(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		searchRecordsByEntityType();
	}	
}        
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}
/*========  SearchDistrictOrSchool ===============*/
function displayOrHideSearchBox(loggedEntityType)
{
	document.getElementById("districtORSchoolName").value="";
	document.getElementById("districtOrSchooHiddenlId").value="";
	var entity_type	=	document.getElementById("MU_EntityType").value; 
	$(document).ready(function()
	{
		/*==== entity_type == 0 Means Select Entity Type 1 Means TM 2-> Districts and 3-> Schools */
		if(loggedEntityType ==	1)
		{
			if(entity_type ==	2)
			{
				document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrict;
			}
			if(entity_type ==	3)
			{
				document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;
			}
			$("#Searchbox").fadeIn();
			document.getElementById('districtORSchoolName').focus();
			if(entity_type ==	loggedEntityType)
			{
				$("#Searchbox").hide();
			}
		}
		if(loggedEntityType ==	2)
		{
			if(entity_type ==	2)
			{
				//document.getElementById("captionDistrictOrSchool").innerHTML	=	"District"
				$("#Searchbox").hide();
			}
			if(entity_type ==	3)
			{
				document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;
				$("#Searchbox").fadeIn();
				$('#districtORSchoolName').focus();
			}
		}
	});
}

function searchSchool()
{
	page = 1;
	searchRecordsByEntityType();
}

function updateMsg(authKeyVal,entityType)
{
	if(authKeyVal!="0"){
		if(authKeyVal!="2" && entityType==1){
			document.getElementById("updateMsg").innerHTML=resourceJSON.msgSchoolDetailsUpdatedSuccessfully+" <br/><br/>  "+resourceJSON.msgAuthenticationKey+": "+authKeyVal;
		}else{
			document.getElementById("updateMsg").innerHTML=resourceJSON.msgSchoolDetailsUpdatedSuccessfully+"";
		}
		$('#myModalUpdateMsg').modal('show');
	}
}

/*========  SearchDistrictOrSchool ===============*/
function searchRecordsByEntityType()
{
	var entityID	=	document.getElementById("MU_EntityType").value;
	var schoolId	=	document.getElementById("schoolHiddenId").value;	
	var districtId	=	document.getElementById("districtHiddenId").value;
	var resultFlag=true;
	
	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;
	if(document.getElementById("districtORSchoolName").value!="" && searchTextId==0){		
		resultFlag=false;
	}	
	delay(1000);
	SchoolAjax.displayRecordsByEntityType(resultFlag,entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType, { 
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			applyScrollOnTbl();		
		},
		errorHandler:handleError 
	});
	
}
/*========  displaySchoolMasterRecords ===============*/
function displaySchoolMasterRecords(entityID)
{
	$('#loadingDiv').fadeIn();
	//return false;
	var schoolHiddenId	=null;	
	var resultFlag=true;
	try{
		schoolHiddenId=document.getElementById("schoolHiddenId").value;
	}catch(err){
		schoolHiddenId=0;
	}
	if(schoolHiddenId	==	"" || schoolHiddenId	==	null)
	{
		schoolHiddenId	=0;
	}
	if(document.getElementById("districtORSchoolName").value!="" && schoolHiddenId==0){
			resultFlag=false;
	}
	if(entityID==1){
		document.getElementById("MU_EntityType").value=3;
		$("#Searchbox").fadeIn();
		document.getElementById('districtORSchoolName').focus();
		document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msglblSchoolDep;
	}
	delay(1000);
	SchoolAjax.displayRecordsByEntityType(true,entityID,schoolHiddenId,noOfRows,page,sortOrderStr,sortOrderType, { 
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			applyScrollOnTbl();	
			$('#loadingDiv').hide();
		},
		errorHandler:handleError 
	});
}
/*========  activateDeactivateUser ===============*/
function activateDeactivateSchool(entityID,schoolId,status)
{
document.getElementById("entitystat").value=entityID;
document.getElementById("actschool").value=schoolId;
document.getElementById("diststat").value=status;
	if(status=="I")
	{
		$('#myModalactMsgShow').modal('show');
	}
	else
	{
	toggleStatus();	
	}
}

function toggleStatus()
{
var entityID=document.getElementById("entitystat").value;
var schoolId=document.getElementById("actschool").value;
var status=document.getElementById("diststat").value;

SchoolAjax.activateDeactivateSchool(schoolId,status, { 
async: true,
callback: function(data)
{
	displaySchoolMasterRecords(entityID)
},
errorHandler:handleError 
});
}


var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtOrSchoolName){

	var searchArray = new Array();
	var entityID	=	document.getElementById("MU_EntityType").value;
	var districtIdForSchool	=document.getElementById("districtHiddenId").value;
	var userSessionEntityTypeId	=document.getElementById("userSessionEntityTypeId").value;
	if(userSessionEntityTypeId==1){
		districtIdForSchool=0;
	}
	
	if(entityID==2){
		SchoolAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	}else {
		SchoolAjax.getFieldOfSchoolList(districtIdForSchool,districtOrSchoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	}
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}