function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function setBasicAdvanceColor(resultType)
{  
  $('.mybtn').css({backgroundColor: '#fff'});
	if(resultType=='basic'){
	   $('#weekly').css({backgroundColor: '#9BBB59'});
	   $( "#resulyType" ).text("Week");
				//kesdashboardResultjs('week');
				
	 }
	 if(resultType=='advance'){
	   $('#monthly').css({backgroundColor: '#9BBB59'});    
	   $( "#resulyType" ).text("Month");
	    //kesdashboardResultjs('month');
	}
}


function getTotalJobsAndCandidateDetail(){
	
	NcDashBoardAjax.getTotalJobsAndCandidateDetail(2,{
		async: true,
		callback: function(data)
		{	
			//alert("calBack :: "+data);
			 $( "#activeJobs" ).text(data.split("#")[0]);
			 $( "#totalPosition" ).text(data.split("#")[1]);
			 $( "#totalApplication" ).text(data.split("#")[2]);
			 $( "#uniqueCandidate" ).text(data.split("#")[3]);
			 
			 
			 
		},
	});
}

//positionFilledAndHiredChart
function positionFilledAndHiredChart()
{
	$('#container1').html("<div style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>");

	NcDashBoardAjax.positionFilledAndHiredChart(2,{ 
		async: true,
		callback: function(data)
		{
		//alert("data :: "+data[0])
		$( "#unFilledId" ).text(data[1]);
		$( "#underFilledId" ).text(data[2]);
		
		var totalhire=0;
		var hiredList=0;
		var noofHire=0;
		if(data[0]!=null && data[0]!='')
			totalhire=data[0];
		if(data[3]!=null)
			hiredList=data[3];
		if(data[4]!=null)
			noofHire=data[4];
		
		
		 	$('#container1').highcharts({
		 		chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: 0,
		            plotShadow: true
		 		},
		 		colors: ['#017BC9','#DFDFDF'],
		        title: {
		            text: ' <b style="font-size:2em;">'+totalhire+'%</b><br><b>'+hiredList+' of '+noofHire+'<br>Positions Filled</b>',
		            align: 'center', 
		            verticalAlign: 'middle',
		            y: 0
		        },
		        tooltip: {
		            enabled: false
		        },
		        exporting: {
		         enabled: false
		        },
		        plotOptions: {
		            pie: {
		                dataLabels: {
		                    enabled: false,
		                    distance:10,
		                    style: {
		                        fontWeight: 'normal',
		                        color: 'black' ,
		                        fontSize:'12px'
		                    }
		                },
		                startAngle:90,
		                endAngle:  450,
		                center: ['50%', '50%']
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Hired/UnHired',
		            innerSize: '95%',
		            data: [["Hires",totalhire],["Unhired",(100-totalhire)]]
		        }]
	    });
	 
	 }, 
	errorHandler:handleError
	}); 
}

function positionCompetitiveChart()
{
	$('#container2').html("<div style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>");

	NcDashBoardAjax.positionCompetitiveChart(2,{ 
		async: true,
		callback: function(data)
		{
	//	alert("data :: "+data)
		$( "#unCompetitaveId" ).text(data[1]);
		$( "#underCompetitaveId" ).text(data[2]);
		
		var totalhire=0;
		var totalapply=0;
		var totalCandFilled=0;
		if(data[0]!=null && data[0]!='')
			totalhire=data[0];
		
		if(data[3]!=null)
			totalapply=data[3];
		
		if(data[4]!=null)
			totalCandFilled=data[4];
		
		 	$('#container2').highcharts({
		 		chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: 0,
		            plotShadow: true
		 		},
		 		colors: ['#5BE055','#DFDFDF'],
		        title: {
		            text: ' <b style="font-size:2em;">'+totalhire+'%</b><br><b>'+totalapply+' of '+totalCandFilled+'<br>Positions Competitive</b>',
		            align: 'center', 
		            verticalAlign: 'middle',
		            y: 0
		        },
		        tooltip: {
		            enabled: false
		        },
		        exporting: {
		         enabled: false
		        },
		        plotOptions: {
		            pie: {
		                dataLabels: {
		                    enabled: false,
		                    distance:10,
		                    style: {
		                        fontWeight: 'normal',
		                        color: 'black' ,
		                        fontSize:'12px'
		                    }
		                },
		                startAngle:90,
		                endAngle:  450,
		                center: ['50%', '50%']
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Hired/UnHired',
		            innerSize: '95%',
		            data: [["Hires",totalhire],["Unhired",(100-totalhire)]]
		        }]
	    });
	 
	 }, 
	errorHandler:handleError
	}); 
}

function leasActiveChart()
{
	$('#container3').html("<div style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>	Loading...</div>");

	NcDashBoardAjax.leasActiveChart(2,{ 
		async: true,
		callback: function(data)
		{
	//	alert("data :: "+data)
		$( "#inActiveLEId" ).text(data[1]);
		$( "#underinActiveLEId" ).text(data[2]);
		
		var totalhire=0;
		var noODistrictLoginWeek=0;
		var noODistrictLoginAll=0;
		if(data[0]!=null && data[0]!='')
			totalhire=data[0];
		
		if(data[3]!=null)
			noODistrictLoginWeek=data[3];
		
		if(data[4]!=null)
			noODistrictLoginAll=data[4];
		
		 	$('#container3').highcharts({
		 		chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: 0,
		            plotShadow: true
		 		},
		 		colors: ['#FEBF00','#DFDFDF'],
		        title: {
		            text: ' <b style="font-size:2em;">'+totalhire+'%</b><br><b>'+noODistrictLoginWeek+' of '+noODistrictLoginAll+'<br>Schools Active</b>',
		            align: 'center', 
		            verticalAlign: 'middle',
		            y: 0
		        },
		        tooltip: {
		            enabled: false
		        },
		        exporting: {
		         enabled: false
		        },
		        plotOptions: {
		            pie: {
		                dataLabels: {
		                    enabled: false,
		                    distance:10,
		                    style: {
		                        fontWeight: 'normal',
		                        color: 'black' ,
		                        fontSize:'12px'
		                    }
		                },
		                startAngle:90,
		                endAngle:  450,
		                center: ['50%', '50%']
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: 'Hired/UnHired',
		            innerSize: '95%',
		            data: [["Hires",totalhire],["Unhired",(100-totalhire)]]
		        }]
	    });
	 
	 }, 
	errorHandler:handleError
	}); 
}


//listUnFilledDistrict
function listUnFilledDistrict()
{
	$('#listUnFilledDistrictDiv').html("<div style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>");

	NcDashBoardAjax.listUnFilledDistrict(2,{ 
		async: true,
		callback: function(data)
		{
		//	alert("data  "+data[0]);
			$('#listUnFilledDistrictDiv').html(data[0]);
			
		}, 
	errorHandler:handleError
	}); 
}

