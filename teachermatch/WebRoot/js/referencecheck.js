var certificationDivCount=0;
var schoolDivCount=0;
var schoolDivCountCheck=0;
var schoolDivVal="";

var arrNewRequisition=[];

/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/* ======= Save AddEditJobOrder on Press Enter Key ========= */
function chkForEnterAddEditJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if((charCode==13) && (evt.srcElement.className!='jqte_editor'))
	{
		validateAddEditJobOrder();
	}	
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function checkJobStartDate()
{
	if($("#jobId").val()==""){
		var month = new Array();
		month[0] = resourceJSON.January;
		month[1] = resourceJSON.February;
		month[2] = resourceJSON.March;
		month[3] = resourceJSON.April;
		month[4] = resourceJSON.May;
		month[5] = resourceJSON.June;
		month[6] = resourceJSON.July;
		month[7] = resourceJSON.August;
		month[8] = resourceJSON.September;
		month[9] = resourceJSON.October;
		month[10] = resourceJSON.November;
		month[11] = resourceJSON.December;
		$('#errordiv').empty();
		$('#jobStartDate').css("background-color", "");
		var jobStartDate	=	new Date(trim(document.getElementById("jobStartDate").value));
		if(trim(document.getElementById("jobStartDate").value)==""){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPostingStartDate+"");
			$('#jobStartDate').focus();
			$('#jobStartDate').css("background-color", "#F5E7E1");
			$("#jsiDiv").hide();
		}else{
			$("#jsiDiv").fadeIn();
			var twoDaysAgoDate  =	new Date();
			twoDaysAgoDate = new Date(twoDaysAgoDate.setDate(twoDaysAgoDate.getDate()+2));
			var diffDays =  twoDaysAgoDate.getTime()-jobStartDate.getTime();
			diffDays=parseInt(diffDays / (1000 * 60 * 60 * 24));
			if(!(diffDays<=0)){
				$("#jsiDiv").hide();
				document.getElementById("textJsiDateMsg").innerHTML=""+resourceJSON.msgJobStratDateless+" "+month[twoDaysAgoDate.getMonth()]+" "+twoDaysAgoDate.getDate()+", "+twoDaysAgoDate.getFullYear()+""
				$("#textJsiDate").modal("show");
				$('#jobStartDate').focus();
				$('#jobStartDate').css("background-color", "#F5E7E1");
			}else{
				$("#jsiDiv").fadeIn();
			}
		}	
	}
}

function showHideScoreDiv(id){
	var maxScore = document.getElementById("MaxScore"+id).checked;
	if(maxScore == true){
		document.getElementById("MaxScoreDiv"+id).style.display="none";
	} else {
		document.getElementById("MaxScoreDiv"+id).style.display="block";
	}
}

var isFormSubmitted = false;
function showReferenceRecord() {
	//$('#loadingDiv').show();
	var referenceId	=	$("#elerefAutoId").val();
	var headQuarterId = $("#headQuarterId").val();
	var districtId	=	$("#districtId").val();
	var jobId		=	$("#jobId").val();
	var teacherId	=	$("#teacherId").val();
	var isAffilated	=	0;

	ReferenceCheckAjax.getDistrictSpecificQuestion(referenceId,headQuarterId, districtId, jobId, teacherId, isAffilated,{
		async: true,
		errorHandler:handleError,
		callback: function(data) {

			if(data!=null) {
				try {
					var dataArray 	= data.split("@##@");
					totalQuestions 	= dataArray[1];
					var textmsg = dataArray[2];

					if(textmsg!=null && textmsg!="" && textmsg!='null')
						$('#textForDistrictSpecificQuestions').html(textmsg);

					if(isFormSubmitted==false) {
						isFormSubmitted = true;
						if(totalQuestions==0) {
							document.getElementById("frmApplyJob").submit();
							return;
						}else if(totalQuestions!=undefined) {
							$('#tblGrid').html(dataArray[0]);
							//$('#loadingDiv_dspq_ie').hide();
							//$('#myModalDASpecificQuestions').modal('show');
						}
					}
				}catch(err){}
			} else {
				document.getElementById("frmApplyJob").submit();
			}
		}
	});	
}

function setDefColorForErrorMsg(){
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#email').css("background-color","");
}


function setDistrictQuestions(fromWhere) {

	$('#errordiv4question').empty();
	setDefColorForErrorMsg();
	var referenceId	=	$("#elerefAutoId").val();
	var districtId	=	$("#districtId").val();
	var headQuarterId = $("#headQuarterId").val();
	var jobId		=	$("#jobId").val();
	var teacherId	=	$("#teacherId").val();
	var iframeNorm 		= 	"";
	var innerNorm 		= 	"";
	var dsliderchkVal	= 	"";
	var inputNormScore 	= 	"";
	//var MaxScore		=	"";
	var count = 0;
	var cnt=0;
	var focs=0;
	
	var firstName	=	$("#firstName").val();
	var lastName	=	$("#lastName").val();
	var email		=	$("#email").val();
	var designation	=	$("#designation").val();
	var organization=	$("#organization").val();
	var contactnumber=	$("#contactnumber").val();
	var secondaryPhone=	$("#secondaryPhone").val();
	var relationship=	$("#relationship").val();
	var length		=	$("#length").val();
	var reference	=	$("#reference").val();
	var manager		=	$("#manager").val();

	var arr 		=	[];
	var arrUserInfo =	[];
	
	if(firstName=="")
	{
		$('#errordiv4question').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#firstName').focus();

		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(lastName=="")
	{
		$('#errordiv4question').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lastName').focus();

		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(email=="")
	{
		$('#errordiv4question').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#email').focus();

		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	} else if(!isEmailAddress(email)) {		
		$('#errordiv4question').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	var arrUserInfo = [firstName,lastName,email,designation,organization,contactnumber,secondaryPhone,relationship,length,reference,manager];
	
	for(i=1;i<=totalQuestions;i++) {
		
		var districtSpecificRefChkQuestion 	= 	{questionId:dwr.util.getValue("Q"+i+"questionId")};
		var questionTypeShortName 			= 	dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionTypeMaster 				= 	{questionTypeId:dwr.util.getValue("Q"+i+"questionTypeId")};
		var qType 							= 	dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionMaxScore				=	"";
		try{
			 iframeNorm 		= 	document.getElementById("Q"+i+"ifrmStatusNote");
			 innerNorm 			= 	iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			 inputNormScore 	= 	innerNorm.getElementById('statusNoteFrm');
			 questionMaxScore	=	inputNormScore.value;
			 dsliderchkVal 		= 	document.getElementById("Q"+i+"dsliderchk").value;
			 var MaxScore		=	document.getElementById("MaxScore"+i);
			
		}catch(err){}
		try{
			if(MaxScore.checked == false){
				if(dsliderchkVal == 1){
					if(questionMaxScore == "0"){
						count = count+1;
					}
				}
			} else { questionMaxScore = 999; }
		}catch(errr){}
		if(qType=='tf' || qType=='slsel') {
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {

				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} else {
				$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				return;
			}

			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
			});

		} else if(qType=='ml') {
			var insertedText = dwr.util.getValue("Q"+i+"opt");
			//if(insertedText!=null && insertedText!="") {
				arr.push({
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionMaxScore" : questionMaxScore,
					"questionType" : questionTypeShortName,
					"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				});
			/*} else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}*/
		} else if(qType=='et') {
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} else {
				$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				return;
			}

			var insertedText = dwr.util.getValue("Q"+i+"optet");
			var isValidAnswer = dwr.util.getValue("Q"+optId+"validQuestion");

			if(isValidAnswer=="false" && insertedText.trim()=="") {
				$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				return;
			}

			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : isValidAnswer,
			});
		} else if(qType=='mlsel'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				}
				 
				if(multiSelectArray!=""){

					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("Q"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionMaxScore" : questionMaxScore,
						"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
						"isValidAnswer" : isValidAnswer,
					});
				} else {
						$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						return;
				}
			}catch(err){}
		} if(qType=='mloet'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("Q"+i+"optmloet");
				if(multiSelectArray!=""){

					arr.push({
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("Q"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionMaxScore" : questionMaxScore,
						"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
						"isValidAnswer" : isValidAnswer,
					});
				}else{
						$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
						return;
				}
			}catch(err){}
		} else if(qType=='SLD') {
			var insertedText = dwr.util.getValue("Q"+i+"opt");
				arr.push({
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionMaxScore" : questionMaxScore,
					"questionType" : questionTypeShortName,
					"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				});
		} else if(qType=='sloet') {
			var optId="";
			var insertedText = dwr.util.getValue("Q"+i+"optet");
			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {

				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} else {
				$("#errordiv4question").append("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
				return;
			}
			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
			});

		}
	}
	//alert(arr[0].insertedText);return false;
	if(fromWhere == "dashboard"){
		if(count > 0 ){
			$('#myModalReferenceCheckConfirm').modal('show');
			return false;
		}
	}
	if(arr.length == totalQuestions) {
		if(headQuarterId=="") headQuarterId=0;
		if(districtId=="")   districtId  =  0;
	//	 console.log ( 'Hiiiiiii       headQuarterId=='+headQuarterId +'***********  district Id =='+districtId );
		ReferenceCheckAjax.setDistrictQuestions(referenceId,headQuarterId, districtId, jobId, teacherId,arr,arrUserInfo,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				
				// Answer Save Successfully referencecheckspecificquestions.do
				window.location="referencecheckfinal.do";
				arr =[];
			 }
		  }
		});	
	} else {
		$("#errordiv4question").html("&#149; "+resourceJSON.PlzProvideResponseToAllQues+".<br>");
		return;
	}
}

function saveDistrictSpecificQuestion() {

	var isaffilatedstatus=document.getElementById("isaffilatedstatushiddenId").value;
	var jobId = document.getElementById("jobId").value;
	if(isaffilatedstatus==1) {
		try {
			try{$('#myModalv').modal('hide');}catch(err){}
			$('#myModalDASpecificQuestions').modal('hide');
			//$('#epiIncompAlert').modal('show');
		}catch(err) {}
	}
	ReferenceCheckAjax.saveJobForTeacher(jobId,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1) {
			try {
				$('#myModalDASpecificQuestions').modal('hide');
			}catch(err) {}
				// Start ... Dynamic portfolio for external job
				var completeNow = document.getElementById("completeNow").value;
				
				if(completeNow==1) {
					continueCompleteNow();
				} else {
					getTeacherJobDone(jobId);
					$('#myModalCL').modal('hide');
				}
				// End ... Dynamic portfolio for external job
			}
		}
	});	
}

function closeReferenceCheck(id){
	$('#myModalReferenceCheckConfirm').modal('hide');
}


/**********************Add BY Ram Nath***************************************/
$(function(){	
	$('.confirmFalse').click(function(){
		$('#eRef').hide();		
		$('#jWTeacherStatusNotesDiv').show();
		try{
			if($('#cg').val()==1){
				$('#jWTeacherStatusNotesDiv').hide();	
				$('#myModalReferenceCheck').show();
			}
		}catch(e){}
	});
});


function openQuestionEReference(val){
//alert('Enter'+val);
$('#tblGrid_eref').html('');
$('#eRef #errordiv4question').html('');
$('#eRef #elerefAutoId').val('');
$('#eRef #districtId').val('');
$('#eRef #jobId').val('');
$('#eRef #teacherId').val('');
var referenceJobId		=	document.getElementById("referenceJobId").value
var districtId  		=	"";
  try{districtId=document.getElementById("districtId").value;}catch (e) {}
var headQuarerId = "";
try{headQuarerId 	=	document.getElementById("headQuarerId").value;}catch (e) {}
//eId1+","+tId1+","+uId;//referenceJobId,districtId
//alert(val+","+referenceJobId+","+districtId);
var allInfo=val.split(",");
var referenceId=allInfo[0];
var teacherId=allInfo[1];
var userId=allInfo[2];
$('#eRef #elerefAutoId').val(referenceId);
$('#eRef #districtId').val(districtId);
$('#eRef #jobId').val(referenceJobId);
$('#eRef #teacherId').val(teacherId);
$('#eRef #userId').val(userId);
				
$('#myModalLabel').html($('#jWTeacherStatusNotesDiv .modal-dialog #myModalLabel #status_title').html());
$('#eRef').show();
$('#jWTeacherStatusNotesDiv').hide();
try{
$('#myModalReferenceCheck').hide();
}catch(e){}
var isAffilated	=	0;

	ReferenceCheckAjax.getDistrictSpecificQuestion(referenceId,headQuarerId, districtId, referenceJobId, teacherId, isAffilated,{
		async: true,
		errorHandler:handleError,
		callback: function(data) {
		//alert(data);
		$('#tblGrid_eref').html(data);
		////setTimeout(function(){
		//$("html, body").delay(2000).animate({scrollTop: $('#eRef #firstName').offset().top }, 2000);
		//$('#eRef #firstName').offset()},10);
		//$('#eRef #tblGrid').(data);			
		}
	});	
}

function setDistrictQuestions1(fromWhere) {
	$('#errordiv4question').empty();
	var fitScore			=	"";
	var statusId			=	"";
	var secondaryStatusId	= "";
	var cg=0;
	var teacherDetails="";
	var teacherId="";
	try{
	fitScore			=	document.getElementById("fitScoreForStatusNote").value;
	statusId			=	document.getElementById("statusId").value;
	secondaryStatusId	=	document.getElementById("secondaryStatusId").value;
	}catch(e){}
	
	try{
		if($('#cg').val()==1){
			cg=1;
			teacherDetails=$('#teacherDetails').val();
			teacherId=$('#teacherId').val();
		}
	}catch(e){}
	
	//alert(fitScore+"     "+statusId+"     "+secondaryStatusId);
	var referenceId	=	$("#eRef #elerefAutoId").val();
	var headQuarterId = $("#headQuarterId").val();
	var districtId	=	$("#eRef #districtId").val();
	var jobId		=	$("#eRef #jobId").val();
	var teacherId	=	$("#eRef #teacherId").val();
	var userId		=	$("#eRef #userId").val();
	var iframeNorm 		= 	"";
	var innerNorm 		= 	"";
	var dsliderchkVal	= 	"";
	var inputNormScore 	= 	"";
	//var MaxScore		=	"";
	var count = 0;
	var cnt=0;
	var focs=0;
	
	var firstName	=	$("#eRef #firstName").val();
	var lastName	=	$("#eRef #lastName").val();
	var email		=	$("#eRef #email").val();
	var designation	=	$("#eRef #designation").val();
	var organization=	$("#eRef #organization").val();
	var contactnumber=	$("#eRef #contactnumber").val();
	var secondaryPhone=	$("#eRef #secondaryPhone").val();
	var relationship=	$("#eRef #relationship").val();
	var length		=	$("#eRef #length").val();
	var reference	=	$("#eRef #reference").val();
	var manager		=	$("#eRef #manager").val();
	var totalQuestions =parseInt($("#eRef #totalQuestions").val());
	//alert('totalQuestions===='+totalQuestions+"  referenceId=="+referenceId+" districtId==="+districtId+" jobId==="+jobId+" teacherId=="+teacherId); 

	var arr 		=	[];
	var arrUserInfo =	[];
	
	if(firstName=="")
	{
		$('#errordiv4question').append("&#149; Please enter First Name<br>");
		if(focs==0)
			$('#firstName').focus();

		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(lastName=="")
	{
		$('#errordiv4question').append("&#149; Please enter Last Name<br>");
		if(focs==0)
			$('#lastName').focus();

		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(email=="")
	{
		$('#errordiv4question').append("&#149; Please enter Email<br>");
		if(focs==0)
			$('#email').focus();

		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	} else if(!isEmailAddress(email)) {		
		$('#errordiv4question').append("&#149; Please enter valid Email<br>");
		if(focs==0)
			$('#email').focus();
		
		$('#email').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	var arrUserInfo = [firstName,lastName,email,designation,organization,contactnumber,secondaryPhone,relationship,length,reference,manager,userId];
	
	//alert("firstName===="+firstName+"  lastName====="+lastName+"  email==="+email+"  designation====="+designation+"  organization====="+organization+"  contactnumber====="+contactnumber+"  secondaryPhone====="+secondaryPhone+"  relationship====="+relationship+"  length====="+length+"  reference====="+reference+"  manager====="+manager);
	
	//alert('totalQuestions==1=='+totalQuestions);
	for(i=1;i<=totalQuestions;i++) {
		
		var districtSpecificRefChkQuestion 	= 	{questionId:dwr.util.getValue("Q"+i+"questionId")};
		var questionTypeShortName 			= 	dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionTypeMaster 				= 	{questionTypeId:dwr.util.getValue("Q"+i+"questionTypeId")};
		var qType 							= 	dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionMaxScore				=	"";
		try{
			 iframeNorm 		= 	document.getElementById("Q"+i+"ifrmStatusNote");
			 innerNorm 			= 	iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			 inputNormScore 	= 	innerNorm.getElementById('statusNoteFrm');
			 questionMaxScore	=	inputNormScore.value;
			 dsliderchkVal 		= 	document.getElementById("Q"+i+"dsliderchk").value;
			 var MaxScore		=	document.getElementById("MaxScore"+i);
			
		}catch(err){}
		try{
			if(MaxScore.checked == false){
				if(dsliderchkVal == 1){
					if(questionMaxScore == "0"){
						count = count+1;
					}
				}
			} else { questionMaxScore = 999; }
		}catch(errr){}
		if(qType=='tf' || qType=='slsel') {
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {

				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} 
			else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}
		//alert('totalQuestions==2=='+totalQuestions);
			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
			});

		} else if(qType=='ml') {
			var insertedText = dwr.util.getValue("Q"+i+"opt");
			//if(insertedText!=null && insertedText!="") {
				arr.push({
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionMaxScore" : questionMaxScore,
					"questionType" : questionTypeShortName,
					"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				});
			/*} else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}*/
		} else if(qType=='et') {
			var optId="";
		//alert('totalQuestions==3=='+totalQuestions);
			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} 
			else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}

			var insertedText = dwr.util.getValue("Q"+i+"optet");
			var isValidAnswer = dwr.util.getValue("Q"+optId+"validQuestion");

			if(isValidAnswer=="false" && insertedText.trim()=="") {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}

			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : isValidAnswer,
			});
		} else if(qType=='mlsel'){
		//alert('totalQuestions==4=='+totalQuestions);
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				}
				 
				if(multiSelectArray!=""){

					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("Q"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionMaxScore" : questionMaxScore,
						"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
						"isValidAnswer" : isValidAnswer,
					});
				} 
				else {
						$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
						return;
				}
			}catch(err){}
		} if(qType=='mloet'){
		//alert('totalQuestions==6=='+totalQuestions);
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("Q"+i+"optmloet");
				if(multiSelectArray!=""){

					arr.push({
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("Q"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionMaxScore" : questionMaxScore,
						"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
						"isValidAnswer" : isValidAnswer,
					});
				}
				else{
						$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
						return;
				}
			}catch(err){}
		} else if(qType=='SLD') {
		//alert('totalQuestions=7==='+totalQuestions);
			var insertedText = dwr.util.getValue("Q"+i+"opt");
				arr.push({
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionMaxScore" : questionMaxScore,
					"questionType" : questionTypeShortName,
					"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				});
		} else if(qType=='sloet') {
			var optId="";
			var insertedText = dwr.util.getValue("Q"+i+"optet");
			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 ) {

				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			} 
			else {
				$("#errordiv4question").append("&#149; Please provide responses to all the questions since they are required.<br>");
				return;
			}
			arr.push({
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionMaxScore" : questionMaxScore,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificRefChkQuestions" : districtSpecificRefChkQuestion,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
			});

		}
	}
	//alert(arr[0].insertedText);return false;
	//alert(fromWhere);
	if(fromWhere == "dashboard"){
		if(count > 0 ){
			$('#eRef').hide();
			$('#myModalReferenceCheckConfirm1').show();
			return false;
		}
	}
	
	//alert("arr.length=============="+arr+"      ------    "+totalQuestions);
	//alert(referenceId+"==="+districtId+"==="+jobId+"==="+teacherId+"==="+arr+"==="+arrUserInfo);
	if(arr.length == totalQuestions) {
		ReferenceCheckAjax.setDistrictQuestions(referenceId,headQuarterId , districtId, jobId, teacherId,arr,arrUserInfo,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				$('#eRef').hide();				
				if(cg==1){
					$('#myModalReferenceCheckConfirm1').hide();
					$('#myModalReferenceCheck').show();
					getReferenceCheck(teacherDetails,teacherId);
				}else{	
					$('#jWTeacherStatusNotesDiv').show();
					jWTeacherStatusNotesOnOpen(fitScore,statusId,secondaryStatusId);
				}
				// Answer Save Successfully referencecheckspecificquestions.do
				//alert('enter');
				//window.location="referencecheckfinal.do";
				arr =[];
			 }
		  }
		});	
	} 
	else {
		$("#errordiv4question").html("&#149; Please provide responses to all the questions since they are required.<br>");
		return;
	}
}
$(function(){

$('.cancelERef').click(function(){
	$('#myModalReferenceCheckConfirm1').hide();
	$('#eRef').show();
});
});
/*************************************end ****************************************/