/* @Author: Hanzala Subhani 
 * @Discription: view of edit domain js.
*/
var page 			= 	1;
var noOfRows 		= 	50;
var sortOrderStr	=	"";
var sortOrderType	=	"";
var pageZS 			= 	1;
var noOfRowsZS 		= 	10;
var sortOrderStrZS	=	"";
var sortOrderTypeZS	=	"";

var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

function getPaging(pageno)
{
	if(pageno!=''){
		page = pageno;	
	} else {
		page = 1;
	}
	noOfRows = document.getElementById("pageSize").value;
	displayApplicatRecords();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr=sortOrder;
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRows = document.getElementById("pageSize").value;
		}else{
			noOfRows=20;
		}
		displayApplicatRecords();
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

/*========  displayJobRecords ===============*/
function displayApplicatRecords()
{
	$('#loadingDiv').show();
	
	var districtId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	var sfromDate = $("#sfromDate").val();
	var stoDate = $("#stoDate").val();
	var jobOrderId	=	trim(document.getElementById("jobOrderId").value);
	var HNS	    	=	document.getElementById("HighNeedschool").checked;
	var jobStatus	=	document.getElementById("Status").value;
	var empStatus	=	document.getElementById("empstatus").value;
		 
		//alert(HNS);
	ManageApplicantReportAjax.displayApplicatRecords(districtId,noOfRows,page,sortOrderStr,sortOrderType,sfromDate,stoDate,jobStatus,jobOrderId,HNS,empStatus,{ 
		async: true,
		callback: function(data)
		{
			if(data!=""){
				$('#iconsDiv').show();
			}
			$('#divApplicantRecord').html(data);
			applyScrollOnReportTbl();
			$('#loadingDiv').hide();
			
		},
		errorHandler:handleError
	});
}

// Export In Excell
function exportRecordIntoExcel()
{
	exportcheck=true;
	$('#loadingDiv').fadeIn();
	var districtId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	 var sfromDate = $("#sfromDate").val();
		var stoDate = $("#stoDate").val();
		var jobOrderId	     =	trim(document.getElementById("jobOrderId").value);
		 var HNS	     =	document.getElementById("HighNeedschool").checked;
		 var jobStatus		 =	document.getElementById("Status").value;
		 var empStatus		 =	document.getElementById("empstatus").value;
	ManageApplicantReportAjax.exportRecordIntoExcel(districtId,noOfRows,page,sortOrderStr,sortOrderType,sfromDate,stoDate,jobStatus,jobOrderId,HNS,empStatus,{ 
		async: true,
		callback: function(data)
		{
		//alert(data);
			$('#loadingDiv').hide();
			if(deviceType){
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			} else {
				
				try
				{
					document.getElementById('ifrmTrans').src = "applicantsNotContactedList/"+data+"";
				}
				catch(e)
				{
					//alert(e);
					}
			}
		},
		errorHandler:handleError
	});
}

// PDF 
function getRecordListPDF()
{
	exportcheck=true;
	$('#loadingDiv').fadeIn();
	var districtId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	 var sfromDate = $("#sfromDate").val();
		var stoDate = $("#stoDate").val();
		var jobOrderId	     =	trim(document.getElementById("jobOrderId").value);
		 var HNS	     =	document.getElementById("HighNeedschool").checked;
		 var jobStatus		 =	document.getElementById("Status").value;
		 var empStatus		 =	document.getElementById("empstatus").value;
	ManageApplicantReportAjax.getRecordListPDF(districtId,noOfRows,page,sortOrderStr,sortOrderType,exportcheck,sfromDate,stoDate,jobStatus,jobOrderId,HNS,empStatus,{ 
		async: true,
		callback: function(data)
		{
			$('#loadingDiv').hide();
			if(deviceType || deviceTypeAndroid){
				window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
			} else {
				
				$('#modalDownloadApplicantList').modal('hide');
				document.getElementById('ifrmCJS').src = ""+data+"";
				try{
					$('#modalDownloadApplicantList').modal('show');
				}catch(err)
				{}
			}
		},
		errorHandler:handleError
	});
}

var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
function printApplicantRecord(){
	
	$('#loadingDiv').fadeIn();
	var districtId	 =	document.getElementById("districtOrSchooHiddenlId").value;
	 var sfromDate = $("#sfromDate").val();
		var stoDate = $("#stoDate").val();
		var jobOrderId	     =	document.getElementById("jobOrderId").value;
		 var HNS	     =	document.getElementById("HighNeedschool").checked;
		 var jobStatus		 =	document.getElementById("Status").value;
		 var empStatus		 =	document.getElementById("empstatus").value;
	ManageApplicantReportAjax.printApplicantRecord(districtId,noOfRows,page,sortOrderStr,sortOrderType,sfromDate,stoDate,jobStatus,jobOrderId,HNS,empStatus,{ 
		async: true,
		callback: function(data)
		{
			$('#loadingDiv').hide();
			try{
				   if (isSafari && !deviceType)
				    {
				    	window.document.write(data);
						window.print();
				    }else
				    {
				    	var newWindow = window.open();
				    	newWindow.document.write(data);	
				    	newWindow.print();			    	
					 } 
				}catch (e) 
				{
					$('#printmessage1').modal('show');							 
				}
		},
		errorHandler:handleError
	});
	
}


function getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId)
{
	$('#draggableDivMaster').hide();
	hideProfilePopover();
	document.getElementById("commDivFlag").value=commDivFlag;
	document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	var commDivFlagPnrCheck = document.getElementById("commDivFlagPnrCheck").value;
	CandidateGridAjaxNew.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,false,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		$("#divCommTxt").html(data);
		
		try{
			$("#saveToFolderDiv").modal("hide");
			$('#myModalCommunications').modal('show');
			if(commDivFlagPnrCheck == 1){
				/* 
					  $('#commPhone').hide();
					  $('#commMsg').hide();
					  $('#commNotes').hide();
				 */
			}
		}catch(e){}
		$('#commPhone').tooltip();
		$('#commMsg').tooltip();
		$('#commNotes').tooltip();
	}
	});	
}

function showProfileContentClose(){
		$('#draggableDivMaster').hide(); 
		$('#myModalJobList').modal('hide');
		$('#loadingDiv').hide();
		document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";
}

function saveToFolderJFTNULL(teachersaveId,teacherId,flagpopover) {
	currentPageFlag="stt";
	document.getElementById('teacherIdForHover').value=teacherId;
	$('#saveToFolderDiv').modal('show');
	$('#errordiv').hide();
	$('#draggableDivMaster').hide();

	var iframe 		= 	document.getElementById('iframeSaveCandidate');
	var innerDoc 	= 	iframe.contentDocument || iframe.contentWindow.document;

	innerDoc.getElementById('errortreediv').style.display		=	"none";
	innerDoc.getElementById('errordeletetreediv').style.display	=	"none";

	var folderId = innerDoc.getElementById('frame_folderId').value;
	if(innerDoc.getElementById('frame_folderId').value!="" || innerDoc.getElementById('frame_folderId').value.length > 0)
	{
		var checkboxshowHideFlag = 0; // For hide checkboxes on Save to Folder Div
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
	}
}

function showCommunicationsDivForSave() {
	$('#draggableDivMaster').show();
}

function defaultTeacherGrid(){
	$('#draggableDivMaster').show();
}

function trim(s) {
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function mouseOverChk(txtdivid,txtboxId) {	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function displayAdvanceSearch()
{
	$('#searchLinkDiv').hide();
	$('#hidesearchLinkDiv').show();
	$('#advanceSearchDiv').slideDown('slow');
}

function hideAdvanceSearch()
{
	$('#searchLinkDiv').show();
	$('#hidesearchLinkDiv').hide();
	$('#advanceSearchDiv').slideUp('slow');
}

function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function hideDistrictMasterDiv(dis,hiddenId,divId){

	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			//alert(" Hi ");
			$('#schoolName').attr('readonly', true);
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
			
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function chkschoolBydistrict()
{
	var districtid = trim(document.getElementById('districtORSchoolName').value);
	if(trim(document.getElementById('entityType').value)==1)
	if(districtid=="")
	{
		document.getElementById('schoolName').disabled=true;
	}
	else
	{
		document.getElementById('schoolName').disabled=false;
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}