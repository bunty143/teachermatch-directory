function globalSearchSection(entityDiv,headQuarterDiv,branchDiv,districtDiv,btnDiv)
{
$(".modalTM").show();
$("#entityTypeDivMaster").html(entityDiv);
$("#headQuarterClassMaster").html(headQuarterDiv);
$("#branchClassMaster").html(branchDiv);
$("#districtClassMaster").html(districtDiv);
$("#searchBtnMaster").html(btnDiv);
}
function showSearchAgainMaster()
{
	$("#searchMasterDiv").show();
	$("#searchIdMaster").hide();
}
function hideSearchAgainMaster()
{
	$("#searchIdMaster").show();
	$("#searchMasterDiv").hide();
}

//******************** For Grid shadow **********************
function onHoverHighLight(divId){
	$("#"+divId.id).addClass("gridDivHighLight");
}
function onOutHighLight(divId){
	$("#"+divId.id).removeClass("gridDivHighLight");
}