/* @Author: Amit Chaudhary */
//for template
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
//for message
var pageM = 1;
var noOfRowsM = 10;
var sortOrderStrM="";
var sortOrderTypeM="";
//for assessment message
var pageAm = 1;
var noOfRowsAm = 10;
var sortOrderStrAm="";
var sortOrderTypeAm="";
//
var gridNameFlag = "";

function setGridNameFlag(gridName){
	document.getElementById("gridNameFlag").value=gridName;
	gridNameFlag = document.getElementById("gridNameFlag").value;
}

function getPaging(pageno)
{
	if(gridNameFlag=="" || gridNameFlag=="templateGrid"){
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize1").value;
		getTemplateGrid();
	}
	else if(gridNameFlag=="messageGrid"){
		if(pageno!='')
		{
			pageM=pageno;	
		}
		else
		{
			pageM=1;
		}
		noOfRowsM = document.getElementById("pageSize2").value;
		getMessageGrid();
	}
	else if(gridNameFlag=="assessmentMessageGrid"){
		if(pageno!='')
		{
			pageAm=pageno;	
		}
		else
		{
			pageAm=1;
		}
		noOfRowsAm = document.getElementById("pageSize3").value;
		getAssessmentMessageGrid();
	}
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	gridNameFlag = document.getElementById("gridNameFlag").value;
	if(gridNameFlag=="templateGrid"){
		if(pageno!=''){
			page=pageno;	
		}else{
			page=1;
		}
		sortOrderStr	=	sortOrder;
		sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			noOfRows = document.getElementById("pageSize1").value;
		}else{
			noOfRows=10;
		}
		getTemplateGrid();
	}else if(gridNameFlag=="messageGrid"){
		if(pageno!=''){
			pageM=pageno;	
		}else{
			pageM=1;
		}
		sortOrderStrM	=	sortOrder;
		sortOrderTypeM	=	sortOrderTyp;
		if(document.getElementById("pageSize2")!=null){
			noOfRowsM = document.getElementById("pageSize2").value;
		}else{
			noOfRowsM=10;
		}
		getMessageGrid();
	}else if(gridNameFlag=="assessmentMessageGrid"){
		if(pageno!=''){
			pageAm=pageno;	
		}else{
			pageAm=1;
		}
		sortOrderStrAm	=	sortOrder;
		sortOrderTypeAm	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsAm = document.getElementById("pageSize3").value;
		}else{
			noOfRowsAm=10;
		}
		getAssessmentMessageGrid();
	}
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function addTemplate()
{
	$('#templateDiv').show();
	clearTemplate();
	return false;
}

function clearTemplate()
{
	dwr.util.setValues({assessmentTemplateId:null,templateName:null,assessmentType:null,isDefault:null,status:null});
	$('#errordivTemplate').hide();
	$('#templateName').css("background-color", "");
	$('#templateName').val('');
	$('#assessmentType').css("background-color", "");
	$("select[name='assessmentType']").prop("disabled",false);
	$("select[name='assessmentType']").val(0);
	$('#assessmentType').focus();
}

function closeTemplate()
{
	$('#templateDiv').hide();
	return false;
}
function validateTemplate()
{
	$('#errordivTemplate').empty();
	$('#templateName').css("background-color", "");
	$('#assessmentType').css("background-color", "");
	var cnt=0;

	if($('#assessmentType').val() == '0') {
    	$('#errordivTemplate').append("&#149; Please Select Inventory Type<br>");
    	if(cnt==0)
    		$('#assessmentType').focus();
    	$('#assessmentType').css("background-color", "#F5E7E1");
		cnt++;
    }
	if (trim(document.getElementById("templateName").value)==""){
		$('#errordivTemplate').append("&#149; Please enter Template Name<br>");
		if(cnt==0)
			$('#templateName').focus();
		$('#templateName').css("background-color", "#F5E7E1");
		cnt++;
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#errordivTemplate').show();
		return false;
	}
}

function saveTemplate()
{
	if(!validateTemplate())
		return;
	
	var template = { assessmentTemplateId:null,templateName:null,assessmentType:null,isDefault:null,status:null };
	dwr.util.getValues(template);
	dwr.engine.beginBatch();
	template.assessmentType=1;
	template.assessmentType = $("select[name='assessmentType']").val();
	
	AssessmentMessageAjax.saveTemplate(template,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data==3)
			{
				document.getElementById('errordivTemplate').innerHTML="&#149; Please enter a unique Template Name";
				$('#errordivTemplate').show();
				$('#templateName').css("background-color", "#F5E7E1");
				$('#templateName').focus();
			}
			else if(data==1 || data==2)
			{
				getTemplateGrid();
				closeTemplate();
			}
			else
			{
				var type = "Base";
				if($("select[name='assessmentType']").val()==3){
					type = "Smart Practices";
				}
				else if($("select[name='assessmentType']").val()==4){
					type = "IPI";
				}
				document.getElementById('errordivTemplate').innerHTML="&#149; We already have a default Template '"+data+"' for "+type+". Please make sure that we have only one default Temlate in an Inventory.";
				$('#errordivTemplate').show();
				$('#isDefault').css("background-color", "#F5E7E1");
				$('#isDefault').focus();
			}
		}
	});
	dwr.engine.endBatch();
}

function editTemplate(assessmentTemplateId)
{
//	$('#loadingDiv').show();
//	$('#errordivTemplate').hide();
//	$('#templateName').css("background-color", "");
	clearTemplate();
	$("select[name='assessmentType']").prop("disabled",true);
	
	AssessmentMessageAjax.getTemplateById(assessmentTemplateId,{
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{
			dwr.util.setValues(data);
			if(data.isDefault==null || data.isDefault==0)
				$("#isDefault").prop('checked',false);
			else if(data.isDefault==1)
				$("#isDefault").prop('checked',true);
			$('#templateDiv').show();
			$('#templateName').focus();
		}
	});
}

function activateDeactivateTemplate(assessmentTemplateId,status)
{
	AssessmentMessageAjax.activateDeactivateTemplate(assessmentTemplateId,status,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
			getTemplateGrid();
		}
	});
}

function getTemplateGrid()
{
	$('#loadingDiv').show();
	AssessmentMessageAjax.getTemplate(noOfRows,page,sortOrderStr,sortOrderType, { 
		async: true,
		errorHandler:handleError,
		callback: function(data){
			$('#templateGrid').html(data);
			$('#loadingDiv').hide();
			applyScrollOnTblTemplate();
		} 
	});
}

function addMessage()
{
	$('#messageDiv').show();
	clearMessage();
//	getSteps('assessmentStepMaster','0');
	return false;
}

function clearMessage()
{
	dwr.util.setValues({templateWiseStepMessageId:null,assessmentTemplateMaster:null,assessmentStepMaster:null,stepMessage:null,status:null});
	$('#errordivMessage').hide();
	$('#assessmentTypeM').css("background-color", "");
	$("select[name='assessmentTypeM']").prop("disabled",false);
	$("select[name='assessmentTypeM']").val(0);
	$('#assessmentTemplateMaster').css("background-color", "");
	dwr.util.removeAllOptions('assessmentTemplateMaster');
	dwr.util.addOptions('assessmentTemplateMaster',["Select Template"]);
	$("select[name='assessmentTemplateMaster']").prop("disabled",false);
	$("select[name='assessmentTemplateMaster']").val(0);
	$('#assessmentStepMaster').css("background-color", "");
	dwr.util.removeAllOptions('assessmentStepMaster');
	dwr.util.addOptions('assessmentStepMaster',["Select Step"]);
	$("select[name='assessmentStepMaster']").prop("disabled",false);
	$("select[name='assessmentStepMaster']").val(0);
	$('#stpMessage').find(".jqte_editor").css("background-color", "");
	$('#stpMessage').find(".jqte_editor").html("");
	$('#assessmentTypeM').focus();
}

function closeMessage()
{
	$('#messageDiv').hide();
	return false;
}

function getTemplatesByInventoryType(assessmentType, from)
{
//	getTemplatesByInventoryType($("select[name='assessmentType']").val(), 'assessmentTemplateMasterAm');
//	getSteps('assessmentStepMasterAm');
//	dwr.util.removeAllOptions("assessmentTemplateMaster");
//	dwr.util.addOptions("assessmentTemplateMaster",["Select Template"]);
	dwr.util.removeAllOptions(from);
//	dwr.util.addOptions(from,["Select Template"]);
//	$("select[name="+from+"]").val();
	$("select[name="+from+"]").append($("<option></option>").attr("value",0).text('Select Template')); 
	AssessmentMessageAjax.getTemplatesByInventoryType(assessmentType,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
			dwr.util.addOptions(from,data,"assessmentTemplateId","templateName");
	    }
	});
}

function getSteps(from,templateID)
{
//	dwr.util.removeAllOptions("assessmentStepMaster");
//	dwr.util.addOptions("assessmentStepMaster",["Select Step"]);
	dwr.util.removeAllOptions(from);
	dwr.util.addOptions(from,["Select Step"]);
	AssessmentMessageAjax.getSteps(templateID,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
			//dwr.util.addOptions(from,data,"assessmentStepId","stepName");
			document.getElementById("assessmentStepMaster").innerHTML=data;
	    }
	});
}

function validateMessage()
{
	$('#errordivMessage').empty();
	$('#assessmentTypeM').css("background-color", "");
	$('#assessmentTemplateMaster').css("background-color", "");
	$('#assessmentStepMaster').css("background-color", "");
	$('#stpMessage').find(".jqte_editor").css("background-color", "");
	var cnt=0;

	if($('#assessmentTypeM').val() == '0') {
    	$('#errordivMessage').append("&#149; Please Select Inventory Type<br>");
    	if(cnt==0)
    		$('#assessmentTypeM').focus();
    	$('#assessmentTypeM').css("background-color", "#F5E7E1");
		cnt++;
    }
	if($('#assessmentTemplateMaster').val() == '0' || $('#assessmentTemplateMaster').val() == 'Select Template') {
    	$('#errordivMessage').append("&#149; Please Select Template<br>");
    	if(cnt==0)
    		$('#assessmentTemplateMaster').focus();
    	$('#assessmentTemplateMaster').css("background-color", "#F5E7E1");
		cnt++;
    }
	if($('#assessmentStepMaster').val() == '0' || $('#assessmentStepMaster').val() == 'Select Step') {
    	$('#errordivMessage').append("&#149; Please Select Step<br>");
    	if(cnt==0)
    		$('#assessmentStepMaster').focus();
    	$('#assessmentStepMaster').css("background-color", "#F5E7E1");
		cnt++;
    }
	if ($('#stpMessage').find(".jqte_editor").text().trim()==""){
		$('#errordivMessage').append("&#149; Please enter Default Message<br>");
		if(cnt==0)
			$('#stpMessage').find(".jqte_editor").focus();
		$('#stpMessage').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#errordivMessage').show();
		return false;
	}
}

function saveMessage()
{
	if(!validateMessage())
		return;
	
	var templateWiseStepMessage = { templateWiseStepMessageId:null,assessmentTemplateMaster:null,assessmentStepMaster:null,stepMessage:null,status:null };
	dwr.util.getValues(templateWiseStepMessage);
	dwr.engine.beginBatch();
	var assessmentTemplateMaster = {assessmentTemplateId:dwr.util.getValue("assessmentTemplateMaster")};
	var assessmentStepMaster = {assessmentStepId:dwr.util.getValue("assessmentStepMaster")};
	
	if(assessmentTemplateMaster.assessmentTemplateId>0)
		templateWiseStepMessage.assessmentTemplateMaster=assessmentTemplateMaster;
	if(assessmentStepMaster.assessmentStepId>0)
		templateWiseStepMessage.assessmentStepMaster=assessmentStepMaster;
	
	AssessmentMessageAjax.saveMessage(templateWiseStepMessage,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data==1 || data==2)
			{
				getMessageGrid();
				closeMessage();
			}
		}
	});
	dwr.engine.endBatch();
}

function editMessage(templateWiseStepMessageId)
{
//	$('#loadingDiv').show();
	clearMessage();
	AssessmentMessageAjax.getMessageById(templateWiseStepMessageId,{
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{
			dwr.util.setValues(data);
			
			$("select[name='assessmentTypeM']").val(data.assessmentTemplateMaster.assessmentType);
			$("select[name='assessmentTypeM']").prop("disabled",true);
			
			dwr.util.removeAllOptions("assessmentTemplateMaster");
			dwr.util.addOptions("assessmentTemplateMaster",data,"assessmentTemplateId","templateName");
			$("select[name='assessmentTemplateMaster']").val(data.assessmentTemplateMaster.assessmentTemplateId);
			$("select[name='assessmentTemplateMaster']").prop("disabled",true);
			
			dwr.util.removeAllOptions("assessmentStepMaster");
			dwr.util.addOptions("assessmentStepMaster",data,"assessmentStepId","stepName");
			$("select[name='assessmentStepMaster']").val(data.assessmentStepMaster.assessmentStepId);
			$("select[name='assessmentStepMaster']").prop("disabled",true);
			
			$('#stpMessage').find(".jqte_editor").html(data.stepMessage);
			$('#messageDiv').show();
			$('#stpMessage').find(".jqte_editor").focus();
		}
	});
}

function activateDeactivateMessage(assessmentTemplateId,status)
{
	AssessmentMessageAjax.activateDeactivateMessage(assessmentTemplateId,status,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
			getMessageGrid();
		}  
	});
}

function getMessageGrid()
{
	$('#loadingDiv').show();
	AssessmentMessageAjax.getMessage(noOfRowsM,pageM,sortOrderStrM,sortOrderTypeM, { 
		async: true,
		errorHandler:handleError,
		callback: function(data){
			$('#messageGrid').html(data);
			$('#loadingDiv').hide();
			applyScrollOnTblMessage();
		}
	});
}

function addAssessmentMessage()
{
	$('#assessmentMessageDiv').show();
	clearAssessmentMessage();
//	getSteps();
	return false;
}

function clearAssessmentMessage()
{
	dwr.util.setValues({templateWiseStepMessageId:null,assessmentTemplateMaster:null,assessmentStepMaster:null,stepMessage:null,status:null});
	$('#errordivAssessmentMessage').hide();
	$('#assessmentTypeAm').css("background-color", "");
	$("select[name='assessmentTypeAm']").prop("disabled",false);
	$("select[name='assessmentTypeAm']").val(0);
	
	$('#assessmentGroupDetails').css("background-color", "");
	dwr.util.removeAllOptions('assessmentGroupDetails');
	//dwr.util.addOptions('assessmentGroupDetails',["Select Group"]);
	$("select[name=assessmentGroupDetails]").append($("<option></option>").attr("value",0).text('Select Group'));
	$("select[name='assessmentGroupDetails']").prop("disabled",false);
	$("select[name='assessmentGroupDetails']").val(0);
	
	$('#assessmentDetail').css("background-color", "");
	dwr.util.removeAllOptions('assessmentDetail');
	//dwr.util.addOptions('assessmentDetail',["Select Assessment"]);
	$("select[name=assessmentDetail]").append($("<option></option>").attr("value",0).text('Select Assessment'));
	$("select[name='assessmentDetail']").prop("disabled",false);
	$("select[name='assessmentDetail']").val(0);
	
	$('input[name=useDefault]').attr("disabled",false);
	$("#useDefault1").prop('checked',true);
    $('#useDefaultDiv').hide();
	
	$('#assessmentTemplateMasterAm').css("background-color", "");
	dwr.util.removeAllOptions('assessmentTemplateMasterAm');
	//dwr.util.addOptions('assessmentTemplateMasterAm',["Select Template"]);
	$("select[name=assessmentTemplateMasterAm]").append($("<option></option>").attr("value",0).text('Select Template'));
	$("select[name='assessmentTemplateMasterAm']").prop("disabled",false);
	$("select[name='assessmentTemplateMasterAm']").val(0);
	
	$('#assessmentStepMasterAm').css("background-color", "");
	dwr.util.removeAllOptions('assessmentStepMasterAm');
	//dwr.util.addOptions('assessmentStepMasterAm',["Select Step"]);
	$("select[name=assessmentStepMasterAm]").append($("<option></option>").attr("value",0).text('Select Step'));
	$("select[name='assessmentStepMasterAm']").prop("disabled",false);
	$("select[name='assessmentStepMasterAm']").val(0);
	
	$('#stpMessageAm').find(".jqte_editor").css("background-color", "");
	$('#stpMessageAm').find(".jqte_editor").html("");
	$("#assessmentWiseStepMessageId").val('0');
	$('#assessmentTypeAm').focus();
}

function closeAssessmentMessage()
{
	$('#assessmentMessageDiv').hide();
	return false;
}

function getGroupsByAssessmentType(assessmentType, from)
{
	dwr.util.removeAllOptions("assessmentGroupDetails");
	//dwr.util.addOptions("assessmentGroupDetails",["Select Group"]);
	$("select[name=assessmentGroupDetails]").append($("<option></option>").attr("value",0).text('Select Group'));
	AssessmentAjax.getAllAssessmentGroupDetailsList(assessmentType, from,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
			dwr.util.addOptions("assessmentGroupDetails",data,"assessmentGroupId","assessmentGroupName");
	    }
	});
}

function getAssessmentByGroup(assessmentGroupId)
{
	dwr.util.removeAllOptions("assessmentDetail");
	//dwr.util.addOptions("assessmentDetail",["Select Assessment"]);
	$("select[name=assessmentDetail]").append($("<option></option>").attr("value",0).text('Select Assessment'));
	AssessmentMessageAjax.getAssessmentByGroup(assessmentGroupId,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
			dwr.util.addOptions("assessmentDetail",data,"assessmentId","assessmentName");
	    }
	});
}

function showAndHideUseDefaultDiv(flag){
	if(flag=="1"){
		$('#useDefaultDiv').hide();
	}
	else if(flag=="2"){
		if(checkIsUseDefaultTemplate($('#assessmentTypeAm').val(), $("#assessmentWiseStepMessageId").val())){
//			var assessmentWiseStepMessageId = $("#assessmentWiseStepMessageId").val();
//			return;
		}
		else{
			$('#useDefaultDiv').show();
		}
		getTemplatesByInventoryType($("select[name='assessmentTypeAm']").val(), 'assessmentTemplateMasterAm');
	}
}

function checkIsUseDefaultTemplate(assessmentType,assessmentWiseStepMessageId){
	if(assessmentType!=0){
		AssessmentMessageAjax.checkIsUseDefaultTemplate(assessmentType,assessmentWiseStepMessageId,{
			async: false,
			errorHandler:handleError,
			callback: function(data){
				if(data==true){
					msg = "We already have a default template for assessment '"+$("select[name='assessmentDetail']").text()+"'. So, firstly you have to deactivate this, then you can add the custom messages for assessment '"+$("select[name='assessmentDetail']").text()+"'.";
					$('#modalMsg').html(msg);
					$('#modalUseDefault').modal('show');
				}
		    }
		});
	}
}

function getStepByAssessmentId(assessmentTemplateId, assessmentId){
	if(assessmentId==0){
		assessmentId = $('#assessmentDetail').val();
	}
	dwr.util.removeAllOptions("assessmentStepMasterAm");
	dwr.util.addOptions("assessmentStepMasterAm",["Select Step"]);
	AssessmentMessageAjax.getStepByAssessmentId(assessmentTemplateId, assessmentId,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
			document.getElementById("assessmentStepMasterAm").innerHTML=data;
	    }
	});
}

function getMessageByAssessmentAndStep(assessmentStepId){
	$('#stpMessageAm').find(".jqte_editor").html($("#assessmentStepMasterAm option:selected").attr("msg"));
}

function validateAssessmentMessage()
{
	$('#errordivAssessmentMessage').empty();
	$('#assessmentTypeAm').css("background-color", "");
	$('#assessmentGroupDetails').css("background-color", "");
	$('#assessmentDetail').css("background-color", "");
	$('#assessmentTemplateMasterAm').css("background-color", "");
	$('#assessmentStepMasterAm').css("background-color", "");
	$('#stpMessageAm').find(".jqte_editor").css("background-color", "");
	var cnt=0;

	if($('#assessmentTypeAm').val() == '0') {
    	$('#errordivAssessmentMessage').append("&#149; Please Select Inventory Type<br>");
    	if(cnt==0)
    		$('#assessmentTypeAm').focus();
    	$('#assessmentTypeAm').css("background-color", "#F5E7E1");
		cnt++;
    }
	if($('#assessmentGroupDetails').val() == '0' || $('#assessmentGroupDetails').val() == 'Select Group') {
    	$('#errordivAssessmentMessage').append("&#149; Please Select Group<br>");
    	if(cnt==0)
    		$('#assessmentGroupDetails').focus();
    	$('#assessmentGroupDetails').css("background-color", "#F5E7E1");
		cnt++;
    }
	if($('#assessmentDetail').val() == '0' || $('#assessmentDetail').val() == 'Select Assessment') {
    	$('#errordivAssessmentMessage').append("&#149; Please Select Assessment<br>");
    	if(cnt==0)
    		$('#assessmentDetail').focus();
    	$('#assessmentDetail').css("background-color", "#F5E7E1");
		cnt++;
    }
	if($('#useDefault2').prop("checked")) {
		if($('#assessmentTemplateMasterAm').val() == '0' || $('#assessmentTemplateMasterAm').val() == 'Select Template') {
	    	$('#errordivAssessmentMessage').append("&#149; Please Select Template<br>");
	    	if(cnt==0)
	    		$('#assessmentTemplateMasterAm').focus();
	    	$('#assessmentTemplateMasterAm').css("background-color", "#F5E7E1");
			cnt++;
	    }
		if($('#assessmentStepMasterAm').val() == '0' || $('#assessmentStepMasterAm').val() == 'Select Step') {
	    	$('#errordivAssessmentMessage').append("&#149; Please Select Step<br>");
	    	if(cnt==0)
	    		$('#assessmentStepMasterAm').focus();
	    	$('#assessmentStepMasterAm').css("background-color", "#F5E7E1");
			cnt++;
	    }
		if($('#stpMessageAm').find(".jqte_editor").text().trim()==""){
			$('#errordivAssessmentMessage').append("&#149; Please enter Default Message<br>");
			if(cnt==0)
				$('#stpMessageAm').find(".jqte_editor").focus();
			$('#stpMessageAm').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++
		}
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#errordivAssessmentMessage').show();
		return false;
	}
}

function saveAssessmentMessage()
{
	if(!validateAssessmentMessage())
		return;
	var defaultt=0;
	var assessmentTypeAm=document.getElementById("assessmentTypeAm").value;  
	var assessmentGroupDetails=document.getElementById("assessmentGroupDetails").value;
	var assessmentDetail=document.getElementById("assessmentDetail").value; 
	var useDefault1=document.getElementById("useDefault1").value; 
	var useDefault2=document.getElementById("useDefault2").value;  
	if(document.getElementById('useDefault2').checked){
		defaultt=useDefault2;
	}else if(document.getElementById('useDefault1').checked){
		defaultt=useDefault1;
	}
	var assessmentTemplateMasterAm=document.getElementById("assessmentTemplateMasterAm").value;  
	var assessmentStepMasterAm=document.getElementById("assessmentStepMasterAm").value;
	var stepMessageAm=$('[name="stepMessageAm"]').parents().parents('.jqte').find(".jqte_editor").html();
	
	var assessmentWiseStepMessageId = $("#assessmentWiseStepMessageId").val();
  
	AssessmentMessageAjax.saveAssessmentMessage(assessmentWiseStepMessageId,assessmentTypeAm,assessmentGroupDetails,assessmentDetail,defaultt,assessmentTemplateMasterAm,assessmentStepMasterAm,stepMessageAm,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
			if(data==1 || data==2)
			{
				getAssessmentMessageGrid();
		  		closeAssessmentMessage();
			}
			else
			{
				var assessmentName = data.split("#")[0];
				var isUseDefault = data.split("#")[1];
				var msg = "We already have a custom template message for assessment '"+assessmentName+"'";
				if(isUseDefault=='true1'){
					msg = "We already have a default template for assessment '"+assessmentName+"'";
					$('#errordivAssessmentMessage').html(msg);
					$('#errordivAssessmentMessage').show();
					$('#assessmentDetail').css("background-color", "#F5E7E1");
					$('#assessmentDetail').focus();
				}
				else if(isUseDefault=='true2'){
					msg = "We already have a default template for assessment '"+assessmentName+"'. So, firstly you have to deactivate this, then you can add the custom messages for assessment '"+assessmentName+"'.";
					$('#modalMsg').html(msg);
					$('#modalUseDefault').modal('show');
				}
				else if(isUseDefault=='false1'){
					msg = "We already have a custom messages for assessment '"+assessmentName+"'. So, firstly you have to deactivate all the respective custom messages, then you can add the default template for assessment '"+assessmentName+"'.";
					$('#modalMsg').html(msg);
					$('#modalUseDefault').modal('show');
				}
				else{
					$('#errordivAssessmentMessage').html(msg);
					$('#errordivAssessmentMessage').show();
					$('#assessmentDetail').css("background-color", "#F5E7E1");
					$('#assessmentDetail').focus();
				}
			}
	  		
	    }
	});
}

function getAssessmentMessageGrid()
{
	$('#loadingDiv').show();
	AssessmentMessageAjax.getAssessmentMessage(noOfRowsM,pageM,sortOrderStrM,sortOrderTypeM, { 
		async: true,
		errorHandler:handleError,
		callback: function(data){
			$('#assessmentMessageGrid').html(data);
			$('#loadingDiv').hide();
			applyScrollOnTblAssessment();
		}
	});
}

function editAssessmentMessage(assessmentWiseStepMessageId)
{
//	$('#loadingDiv').show();
	clearAssessmentMessage();
	$("#assessmentWiseStepMessageId").val(assessmentWiseStepMessageId);
	AssessmentMessageAjax.getAssessmentWisesTepmMessageByID(assessmentWiseStepMessageId,{
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{
			dwr.util.setValues(data);
//			$('input[name=useDefault]').attr("disabled",true);
			$("select[name='assessmentTypeAm']").val(data.assessmentDetail.assessmentType);
			$("select[name='assessmentTypeAm']").prop("disabled",true);			
			
			try{
				dwr.util.removeAllOptions("assessmentGroupDetails");
				$("select[name='assessmentGroupDetails']").prop("disabled",true);
				dwr.util.addOptions("assessmentGroupDetails",data.assessmentDetail,"assessmentGroupId","assessmentGroupName");
				$("select[name='assessmentGroupDetails']").val(data.assessmentDetail.assessmentGroupDetails.assessmentGroupId);
			}
			catch(err){}
			
			dwr.util.removeAllOptions("assessmentDetail");
			dwr.util.addOptions("assessmentDetail",data,"assessmentId","assessmentName");
			$("select[name='assessmentDetail']").val(data.assessmentDetail.assessmentId);
			$("select[name='assessmentDetail']").prop("disabled",true);
			
			$('#assessmentMessageDiv').show();
			
			if(data.useDefault==true){
				$("#useDefault2").prop('checked',false);
			    $("#useDefault1").prop('checked',true);
			    $('#useDefaultDiv').hide();
			}else{
				$("#useDefault2").prop('checked',true);
			    $("#useDefault1").prop('checked',false);
			    $('#useDefaultDiv').show();
			    dwr.util.removeAllOptions("assessmentTemplateMasterAm");
				dwr.util.addOptions("assessmentTemplateMasterAm",data,"assessmentTemplateId","templateName");
				$("select[name='assessmentTemplateMasterAm']").val(data.assessmentTemplateMaster.templateName);
				$("select[name='assessmentTemplateMasterAm']").prop("disabled",true);
				
				dwr.util.removeAllOptions("assessmentStepMasterAm");
				dwr.util.addOptions("assessmentStepMasterAm",data,"assessmentStepId","stepName");
				$("select[name='assessmentStepMasterAm']").val(data.assessmentStepMaster.stepName);
				$("select[name='assessmentStepMasterAm']").prop("disabled",true);
				
				$('#stpMessageAm').find(".jqte_editor").html(data.stepMessage);
				$('#stpMessageAm').find(".jqte_editor").focus();
			}
		},
		errorHandler:handleError
	});
}

function activateDeactivateAssessmentMessage(assessmentWiseStepMessageId,status)
{
	AssessmentMessageAjax.activateDeactivateAssessmentMessage(assessmentWiseStepMessageId,status,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
			getAssessmentMessageGrid();
		}
	});
}
