var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var jstatus = "";
function getPagingForJobboard(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayJobRecords();
}
var txtBgColor="#F5E7E1";

function getPagingAndSorting_not_in_use(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayJobRecords();
}

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var hiddenId="";

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function displayState(){
	JBServiceAjax.displayStateData({ 
		async: false,		
		callback: function(data){
			document.getElementById("stateDivId").innerHTML=data;
		}
	});
}

function selectCityByState()
{
	var stateId = $("#stateId").val();
	JBServiceAjax.selectedCityByStateId(stateId,{ 
		async: false,		
		callback: function(data){
			document.getElementById("cityDivId").innerHTML=data;
		}
	});
}

function getSlider()
{
	var iFrmId="ifrmPostedWithin";
	var name="PostedWithinFrm";
	var tickInterval="1";
	var max="12";
	var swidth="530";
	var svalue="0";
	var iSlWidth=570;
	var style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:45px;width:"+iSlWidth+"px;margin-top:-7px;";
	JBServiceAjax.getTickSlider(iFrmId,name,tickInterval,max,swidth,svalue,style,{ 
	async: false,
	callback: function(data)
	{
		$('#JBPostedWithin').html(data.toString())	
		
	}
	});
}

function showSchool()
{
	try {
		var districtId = $("#districtId").val();
		if(districtId.length>0)
			document.getElementById("schoolName1").disabled=false;
		else
			document.getElementById("schoolName1").disabled=true;
	} catch (e) {
		// TODO: handle exception
	}
}

function hidestate()
{
	try
	{
		var districtId = $("#districtId").val();
		if(districtId!=null && districtId!="")
			document.getElementById("stateId").disabled=true;
		else
			document.getElementById("stateId").disabled=false;
	}
	catch (e) {
		// TODO: handle exception
	}
}

function displayJBAdvanceSearch()
{
	//$('#moreFilterBtn').hide();
	//$('#moreFilterDiv').slideDown(1500);
	
	$('#moreFilterBtn').fadeOut();
	$('#moreFilterDiv').slideDown('slow');
	$('#divJBData').hide();
	$('#ShowListingsId').show();	
}
function displayRecordsInit()
{
	$('#loadingDiv').show();
	JBServiceAjax.displayJobsByTM(0,10,1,sortOrderStr,sortOrderType,"","",0,0,"","","",{ 
		
		async: true,
		callback: function(data)
		{			
		var getValue = data.split("@@@");
			document.getElementById("divJBData").innerHTML=getValue[0];
			$('#loadingDiv').hide();
			//showMapLocations(getValue[1]);
					
			//showDisplay();
			// ******************** Map Start Code ******************************	
			//alert(getValue[1]);
			if(getValue[1].length==1)
			{
				var locations = getValue[1];
			}
			else if(getValue[1].length>1)
			{	
				var locations = getValue[1].split("||");
			}
              var map = new google.maps.Map(document.getElementById('jbMap'), {
                 zoom: 3,
                 center: new google.maps.LatLng(37.09024, -95.712891),
                 mapTypeId: google.maps.MapTypeId.ROADMAP
               });

               var infowindow = new google.maps.InfoWindow();
               var marker, i;
               var latitude = "";
        	   var longitude = "";
               //var locations = getValue[1];
                 var storeLoc = [];
                 /*if(locations.length>10)
                 {*/
                	 //alert(" Locations more than 10  :: "+locations.length);
                	 
                 	var init = 0;
                 	var end = 10; 
                 	var count = 0;
                   for (i = 0; i < locations.length; i++)
                   {                	
               	        $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+locations[i]+'&sensor=false', null, function (data) {
               	            var p = data.results[0].geometry.location
               	           // alert("address :: "+locations[i]+" lat :: "+p.lat+" long :: "+p.lng );
               	            var latlng = new google.maps.LatLng(p.lat, p.lng);
               	            marker = new google.maps.Marker({
      	                            position: latlng,
      	                            map: map,
      	                            animation: google.maps.Animation.DROP,
      	                            icon: "images/rsz_red_pin.png",
      	                            title:data.results[0].formatted_address      	                            
      	                        });
               	            
               	        
               	         
               	         var contentString = "<div style='border-radius:4px;:8px 8px 16px #222;color:#000;text-align:center;overflow:hidden;'>"+data.results[0].formatted_address+"</div>";            	                       
               	    //  var contentString = data.results[0].formatted_address;
	                        var infoWindow = new google.maps.InfoWindow();                	                        
	                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
	                            return function() {
	                                infoWindow.setContent(contentString);
	                                infoWindow.open(map, marker);
	                            }
	                        })(marker, i)); 

               	        });       	    
            		
            	            /*count = count + 1;
            	            if(count==10)
                 			{            	            	
                 				init = end;
                 				var rem = locations.length - end;
                 				//alert("rem :: "+rem);
                 				if(rem < 10)
                 				{
                 					end = end + rem;
                 				}
                 				else
                 				{
                 					end = end + 10;
                 				}                 				
                			}   */
                  }   
                  
			
          	
			
		},
		errorHandler:handleError
	});
}

function searchJob()
{
	$('#divJBData').show();
	page=1;
	noOfRows=10;
	displayJobRecords();
}

function displayJobRecords()
{
	try
	{
	$('#loadingDiv').fadeIn();
	var iframeNorm = document.getElementById('ifrmPostedWithin');
	var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
	var inputNormScore = innerNorm.getElementById('PostedWithinFrm');
	document.getElementById("postedWithin").value=inputNormScore.value;	
	var monthWiseSearch	=	document.getElementById('postedWithin').value;
	//alert("PostedWithinFrm :: "+dayWiseSearch)
	var districtId = $('#districtId').val();
	var schoolId1 = $('#schoolId1').val();
	var cityId = $('#cityId').val();
	var stateId = $('#stateId').val();
	var zipCode = $('#zipCode').val();
	var subjectId 		= 	document.getElementById('subjectId');;
	var certificateTypeMaster	= 	document.getElementById('certificateTypeMaster').value;
	
	var subjectIdList = "";
	for(var i=0; i<subjectId.options.length;i++)
	{	
		if(subjectId.options[i].selected == true){
			subjectIdList = subjectIdList+subjectId.options[i].value+",";
		}
	}
//	delay(1000);
	JBServiceAjax.displayJobsByTM(monthWiseSearch,noOfRows,page,sortOrderStr,sortOrderType,districtId,schoolId1,cityId,stateId,zipCode,subjectIdList,certificateTypeMaster,{ 
		async: true,
		callback: function(data)
		{			
			var getValue = data.split("@@@");
			document.getElementById("divJBData").innerHTML=getValue[0];
			$('#loadingDiv').hide();			
		},
		errorHandler:handleError
	});
	}
	catch (e) {
		// TODO: handle exception
	}
}
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}
function showDisplay()
{
	var noOrRow = document.getElementById("rowsappended").value;
	for(i=1;i<=noOrRow;i++)
	{	
		var sh = $('#des'+i).text();
		if(sh.length>=250)
			$('#desShow'+i).html(sh.substring(0,250)+"...");
		else
			$('#desShow'+i).html(sh);
	}
}




function showMiamiPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}


function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("schoolName").focus();
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	
	var searchArray = new Array();
	JBServiceAjax.getDistrictMasterList(districtName,true,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
			showDataArray[i]=data[i].districtName;
		}
	}
	});	

	return searchArray;
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	getDistrictWiseSubject(document.getElementById(hiddenId).value);
	index = -1;
	length = 0;
}

function getSchoolMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("jobCategoryId").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';	
		searchArray = getSchoolArray(txtSearch.value);		
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
	
}

function getSchoolArray(schoolName){

	var districtId = document.getElementById("districtId").value;
	
	var searchArray = new Array();
	JBServiceAjax.getSchoolMasterList(schoolName,districtId,true,{ 
		async: false,		
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			
			searchArray[i]=data[i].schoolMaster.schoolName;			
			hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			showDataArray[i]=data[i].schoolMaster.schoolName;
		}
	}
	});	

	return searchArray;
}
function hidedivTxtSchoolDataDiv()
{
	document.getElementById("divTxtSchoolData").style.display='none';	
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
function hideSchoolMasterDiv1(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		document.getElementById(hiddenId).value=hiddenDataArray[index];		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.msgvalidschool+"<br>");
			if(focs==0)
				$('#universityName').focus();
			
			$('#universityName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i] + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function __mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		document.getElementById('divResult'+txtdivid+i).className='normal';		
		if ($('#divResult'+txtdivid+i).is(':hover')) 
		{
			document.getElementById('divResult'+txtdivid+i).className='over';	       
	       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
	        index=i;
	    }
	}

}

var overText = function (div_value,txtdivid) 
{
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function getZoneDAForEJob(dsId)
{
	var districtOrSchooHiddenlId=dsId;
	JBServiceAjax.getZoneListAllForEJob(districtOrSchooHiddenlId,{ 
		async: true,
		callback: function(data)
		{	
			if(data!=null && data!="")
			{
				$("#isZoneAvailable").val(1);			
				$("#displayZone").show();			
				$("#zone").html(data);
			}
			else{
				$("#displayZone").hide();
				$("#zone").html(data);				
				$("#isZoneAvailable").val(0);
			}	
		
		},
	});
	
	
}
//****************************************************
//*************** Certification **********************
//****************************************************


function getAllCertificateTypeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getAllCertificateTypeArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getAllCertificateTypeArray(certType){
	
	var searchArray = new Array();	
	JBServiceAjax.getAllCertificateTypeList(certType,true,{ 
		async: false,
		callback: function(data){
		//alert(data);
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
		}
	}
	});	

	return searchArray;
}
function hideAllCertificateTypeDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.MsgEtrValidCertificate+"<br>");
			if(focs==0)
				$('#certName').focus();
			
			$('#certName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		else
		{
			$('#errordiv').empty();	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function getFilterCertificateTypeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getFilterCertificateTypeArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFilterCertificateTypeArray(certType){
	
	var stateId	=0;
	try{
		stateId=document.getElementById("stateId").value;
	}catch(e){}
	var searchArray = new Array();	
	JBServiceAjax.getFilterCertificateTypeList(certType,stateId,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
		}
	}
	});	

	return searchArray;
}

var xmlHttp=null;
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}

function applyteacherjobfrommain(jobId)
{	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();

	createXMLHttpRequest();  
	queryString = "applyteacherjobfrommain.do?jobId="+jobId+"&dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			//alert(xmlHttp.status);
			if(xmlHttp.status==200)	
			{
				var msgStatus = xmlHttp.responseText; 
				//alert(msgStatus);
				if(msgStatus=="")
				{
					//alert("coverletter");
				setCoverLetter();
				}
				else
				{
					//alert("url redirect");
				 //window.location.href=msgStatus;
					window.open(
							msgStatus,
							  '_blank' // <- This is what makes it open in a new window.
							);
				}
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);
}
function applyteacherjobfromquest(jobId)
{	
	//alert(" apply job...");
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	var msgStatus ="";
	createXMLHttpRequest();  
	queryString = "applyteacherjobfromquest.do?jobId="+jobId+"&dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		//alert(xmlHttp.readyState);
		//alert(xmlHttp.status);
		if(xmlHttp.readyState == 4)
		{
			//alert(xmlHttp.status);
			if(xmlHttp.status==200)	
			{
				msgStatus = xmlHttp.responseText; 				
			}			
		}
	}	
	xmlHttp.send(null);
	return msgStatus;
}

function setCoverLetter(){
	document.getElementById("divCoverLetter").style.display="block";
	$('#myModalCL').modal('show');
	$('#divCoverLetter').find(".jqte_editor").focus();
	document.getElementById("rdoCL1").checked=true;
	
	$('#iconpophover101').tooltip();
	
	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}
function setCLEnable(){

	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="block";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}
function setClBlank(){
	
	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="none";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}


function setCLEnableForMartinAdmin(){
	
	$('#coverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetterAdmin").style.display="block";

	$('#errordivCL').empty();
	$('#coverLetter').find(".jqte_editor").css("background-color", "");
}
function setClBlankForMartinAdmin(){
	
	$('#coverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetterAdmin").style.display="none";

	$('#errordivCL').empty();
	$('#coverLetter').find(".jqte_editor").css("background-color", "");
}

function jobApplicationHistory(jobID,districtID,exitURL,schoolID)
{
	//alert(" jobApplicationHistory ");
	QuestAjax.jobApplicationHistory(jobID,districtID,exitURL,schoolID,{ 
		async: true,
		callback: function(data){								
		if(data=="")
		{	
			$('#loadingDiv').hide();
			hideDiv();			
		}
		else
		{
			$("#signUpServerError").html(data);
			$("#divServerError").show();
			return false;
		}				
		},errorHandler:handleError 
	});	
}

function jobApplicationHistoryFromJobApply(jobID,districtID,exitURL,schoolID)
{
	//alert(" jobApplicationHistory ");
	QuestAjax.jobApplicationHistory(jobID,districtID,exitURL,schoolID,{ 
		async: true,
		callback: function(data){								
		if(data=="")
		{	
			$('#loadingDiv').hide();		
		}
		else
		{			
			return false;
		}				
		},errorHandler:handleError 
	});	
}

function chkApplyJobNonClient(jobId,appCriteria,exitUrl,districtId,schoolId)
{	
	$('#loadingDiv').show();	
	document.getElementById('tempjobid').value=jobId;
	document.getElementById('district').value=districtId;
	document.getElementById('school').value=schoolId;
	document.getElementById('criteria').value=appCriteria;
	var urlstatus = exitUrl.indexOf("http://");
	var urlstatus1 = exitUrl.indexOf("https://");
	if(urlstatus==-1 && urlstatus1==-1)
	{
		document.getElementById('exitUrl').value="http://"+exitUrl;
	}
	else
	{
		document.getElementById('exitUrl').value=exitUrl;
	}
		
	if(appCriteria==1)
	{
		var cnt=0;		
		var focs=0;	
		$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149;"+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;		
						$('#loadingDiv').hide();						
						$("#registerModal").modal("show");	
						$('#fname').focus();
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){				
			var jstatus ="";
			jstatus = applyteacherjobfromquest(jobId);
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");	
			}
			else
			{				
				$('#loadingDiv').hide();
				jobApplicationHistory(jobId,districtId,exitUrl,schoolId);
				//checkPopup(document.getElementById('exitUrl').value);
				$('#applyJobComplete').modal('show');
			}
		}else{
			$('#errordiv').show();
			return false;
		}
	}
	else if(appCriteria==3)
	{		
		var cnt=0;		
		var focs=0;	
		$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;
						try
						{						
							$('#loadingDiv').hide();
							jobApplicationHistory(jobId,districtId,exitUrl,schoolId);
							checkPopup(document.getElementById('exitUrl').value);
						}catch(e){alert(e)}													
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){
			var jstatus="";
			jstatus = applyteacherjobfromquest(jobId);			
			if(jstatus==1)
			{				
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);				
				$("#appliedJob").modal("show");	
				checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{				
				$('#loadingDiv').hide();
			jobApplicationHistory(jobId,districtId,exitUrl,schoolId);	
			//checkPopup(document.getElementById('exitUrl').value);
			$('#applyJobComplete').modal('show');
			}	
		}else{
			$('#errordiv').show();
			return false;
		}
		
	}
	else if(appCriteria==0)
	{
		var cnt=0;
		var focs=0;	
		$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}									
				}			
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){					
			//applyteacherjobfrommain(jobId);			
			jobApplicationHistory(jobId,districtId,exitUrl,schoolId);
			$('#loadingDiv').hide();
			window.open(
					"applyteacherjob.do?jobId="+jobId,
					  '_blank' 
					);
		}else{
			$('#errordiv').show();
			return false;
		}
	}
	else if(appCriteria==2)
	{
		var cnt=0;		
		var focs=0;	
		$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149;"+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;		
						$('#loadingDiv').hide();
						$("#registerModal").modal("show");						
						$('#fname').focus();
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){				
			checkInventoryStatusComplete();
			jstatus = applyteacherjobfromquest(jobId);
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");
				checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{				
				$('#loadingDiv').hide();
				jobApplicationHistory(jobId,districtId,exitUrl,schoolId);			
				if(document.getElementById('epistatus').value=="complete")
				{
					//checkPopup(document.getElementById('exitUrl').value);
					$('#applyJobComplete').modal('show');
					//checkInventory(0);
				}
				else
				{
					checkInventory(0,jobId);
				}
			}	
			
		}else{
			$('#errordiv').show();
			return false;
		}
	}
	

	
}

function hideDiv()
{
	$('#signUpErrordiv').empty();
	document.getElementById("fname").value = "";
	document.getElementById("lname").value = "";
	document.getElementById("signupEmail").value = "";
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#signupEmail').css("background-color","");
	$("#registerModal").modal("hide");
}
function signUpTempUser()
{ 
	//checkInventoryStatusComplete();
	var jstatus="";
	var districtId = "";		
	var other =""
	var firstName = document.getElementById("fname");
	var lastName = document.getElementById("lname");
	var emailAddress = document.getElementById("signupEmail");				
try
{
	var duplicate = 0;
	var cnt=0;
	var focs=0;	
	var checkvalue=0;
	$('#signUpErrordiv').empty();
	$('#signUpServerError').css("background-color","");		
	//$('#districtName').css("background-color","");
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#signupEmail').css("background-color","");	
	var emailStatus = checkQuestEmail(emailAddress.value);
	//alert(emailStatus);
	if(trim(firstName.value)=="")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#fname').focus();
		
		$('#fname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lname').focus();
		
		$('#lname').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(emailAddress.value))
	{		
		$('#signUpErrordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(emailStatus=="teacher")
	{
		if(document.getElementById('criteria').value!=2)
		{			
			jstatus = applyteacherjobfromquest(document.getElementById('tempjobid').value);			
			if(jstatus==1)
			{
				$('#loadingDiv').hide();				
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");
				checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				//checkPopup(document.getElementById('exitUrl').value);
				$('#applyJobComplete').modal('show');
			}
		}
		else if(document.getElementById('criteria').value == 2)
		{		
			jstatus = applyteacherjobfromquest(document.getElementById('tempjobid').value);			
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");
				checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				if(document.getElementById('epistatus').value=="complete")
				{					
					$('#applyJobComplete').modal('show');
				}
				else
				{
					checkInventory(0,document.getElementById('tempjobid').value);
				}
			}
		}
		cnt++;focs++;
		duplicate = 1;
	}
	else if(emailStatus=="usermaster")
	{
		$('#signUpErrordiv').append("&#149; "+resourceJSON.msgaladyemail+"<br>");
		if(focs==0)
			$('#signupEmail').focus();
		
		$('#signupEmail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(cnt==0 && duplicate==0)
	{			
		try
		{			
			QuestAjax.questSignUpTemp("",trim(firstName.value),trim(lastName.value),trim(emailAddress.value),"",0,0,{ 
				async: true,
				callback: function(data){				
				if(data=="")
				{						
					hideDiv();	
					if(document.getElementById('criteria').value!=2)
					{						
						jstatus = applyteacherjobfromquest(document.getElementById('tempjobid').value);			
						if(jstatus==1)
						{
							$('#loadingDiv').hide();
							jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
							$("#appliedJob").modal("show");							
						}
						else
						{
							$('#loadingDiv').hide();
							jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
							$('#applyJobComplete').modal('show');
						}
					}
					else if(document.getElementById('criteria').value == 2)
					{				
						checkInventoryStatusComplete();
						jstatus = applyteacherjobfromquest(document.getElementById('tempjobid').value);			
						if(jstatus==1)
						{
							$('#loadingDiv').hide();
							jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
							$("#appliedJob").modal("show");	
							checkPopup(document.getElementById('exitUrl').value);
						}
						else
						{
							$('#loadingDiv').hide();
							jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
							if(document.getElementById('epistatus').value=="complete")
							{
								$('#applyJobComplete').modal('show');
							}
							else
							{
								checkInventory(0,document.getElementById('tempjobid').value);
							}		
						}
					}					
				}
				else
				{
					$("#signUpServerError").html(data);
					$("#divServerError").show();
					return false;
				}				
				},errorHandler:handleError 
			});	
		}catch(e){alert(e)}	
	}
	else if(cnt!=0)
	{
		$('#signUpErrordiv').show();
		return false;
	}
}
catch(e){alert(e);}
}

function checkQuestEmail(emailAddress)
{
	//alert(emailAddress);
	var res;
	createXMLHttpRequest();  
	queryString = "checkemail.do?emailAddress="+emailAddress+"&dt="+new Date().getTime();
	//alert(queryString);
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				res = xmlHttp.responseText;				
			}			
			/*else
			{
				alert("server Error");
			}*/
		}
	}	
	xmlHttp.send(null);
	//alert("result :: "+res);
	if(res==1)
	{
		return "teacher";
	}
	else if(res==2)
	{
		return "usermaster";
	}
	else if(res==0)
		return "other";
	
}

function isEmailAddress(str) 
{	

	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	
	return emailPattern.test(str);	
}

function checkInventoryStatusComplete()
{
	try{
		DashboardAjax.getInventoryStatus(
		{
			async: false,		
			callback: function(data){		
			if(data)
			 {			
			 	document.getElementById("epistatus").value = "complete";
													
			 }
			else
			 {
				document.getElementById("epistatus").value = "";							
		      	}		
		}
		}		
	);	}catch(e){alert(e)}
}
function chkApplyJobNonClientDashBoard(jobId,appCriteria,exitUrl,districtId,schoolId)
{
	checkInventoryStatusComplete();
	$('#loadingDiv').show();	
	document.getElementById('tempjobid').value=jobId;
	document.getElementById('district').value=districtId;
	document.getElementById('school').value=schoolId;
	document.getElementById('criteria').value=appCriteria;
	var urlstatus = exitUrl.indexOf("http://");
	var urlstatus1 = exitUrl.indexOf("https://");
	if(urlstatus==-1 && urlstatus1==-1)
	{
		document.getElementById('exitUrl').value="http://"+exitUrl;
	}
	else
	{
		document.getElementById('exitUrl').value=exitUrl;
	}
		
	if(appCriteria==1)
	{
		var cnt=0;		
		var focs=0;	
		//$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;		
						$('#loadingDiv').hide();						
						$("#registerModal").modal("show");	
						$('#fname').focus();
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){				
			var jstatus ="";
			jstatus = applyteacherjobfromquest(jobId);
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistoryFromJobApply(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");	
				//checkPopup(document.getElementById('exitUrl').value);
			}
			else
			{				
				$('#loadingDiv').hide();
				jobApplicationHistoryFromJobApply(jobId,districtId,exitUrl,schoolId);			
				}
			$('#applyJobComplete').modal('show');
			//getDashboardJobsofIntrestAvailable();
		}else{
			//$('#errordiv').show();
			return false;
		}
	}
	else if(appCriteria==3)
	{		
		var cnt=0;		
		var focs=0;	
		//$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;
						try
						{						
							$('#loadingDiv').hide();
							jobApplicationHistoryFromJobApply(jobId,districtId,exitUrl,schoolId);
							checkPopup(document.getElementById('exitUrl').value);
						}catch(e){alert(e)}													
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){
			
			jstatus = applyteacherjobfromquest(jobId);			
			if(jstatus==1)
			{				
				$('#loadingDiv').hide();
				jobApplicationHistoryFromJobApply(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);			
				//checkPopup(document.getElementById('exitUrl').value);
				$("#appliedJob").modal("show");	
			}
			else
			{				
				$('#loadingDiv').hide();
				jobApplicationHistoryFromJobApply(jobId,districtId,exitUrl,schoolId);		
			//checkPopup(document.getElementById('exitUrl').value);
			}
			$('#applyJobComplete').modal('show');
			//getDashboardJobsofIntrestAvailable();
		}else{
			//$('#errordiv').show();
			return false;
		}
		
	}
	else if(appCriteria==0)
	{
		var cnt=0;
		var focs=0;	
		//$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}									
				}			
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){					
			applyteacherjobfrommain(jobId);			
			jobApplicationHistoryFromJobApply(jobId,districtId,exitUrl,schoolId);			
		}else{
			$('#errordiv').show();
			return false;
		}
	}
	else if(appCriteria==2)
	{
		//alert(" Criteria 2")
		var cnt=0;		
		var focs=0;	
		//$('#errordiv').empty();
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;		
						$('#loadingDiv').hide();
						$("#registerModal").modal("show");						
						$('#fname').focus();
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){				
			
			jstatus = applyteacherjobfromquest(jobId);
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistoryFromJobApply(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				//checkPopup(document.getElementById('exitUrl').value);
				$("#appliedJob").modal("show");
			}
			else
			{				
				$('#loadingDiv').hide();
				jobApplicationHistoryFromJobApply(jobId,districtId,exitUrl,schoolId);			
				if(document.getElementById('epistatus').value=="complete")
				{
							
				}
				else
				{
					checkInventory(0,jobId);
				}
			}	
			$('#applyJobComplete').modal('show');
			//getDashboardJobsofIntrestAvailable();
		}else{
			//$('#errordiv').show();
			return false;
		}
	}
}

function hideInfoBlog()
{
	checkPopup(document.getElementById('exitUrl').value);
	$('#applyJobComplete').modal('hide');	
}
function hideInfo()
{
	checkPopup(document.getElementById('exitUrl').value);
	window.location.href="userdashboard.do";
	$('#applyJobComplete').modal('hide');		
	
}

function hideUserInfoBlog()
{
	checkPopup(document.getElementById('exitUrl').value);	
	window.location.href="userdashboard.do";
	$('#applyJobComplete').modal('hide');		
	
}
function checkPopup(url) {
	  var openWin = window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	  if (!openWin) {
		  	window.location.href=url;
	    } else {
	    window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	   }
}
function hidePopDiv()
{
	$('#signUpErrordiv').empty();
	document.getElementById("fname").value = "";
	document.getElementById("lname").value = "";
	document.getElementById("signupEmail").value = "";
	$('#fname').css("background-color","");
	$('#lname').css("background-color","");
	$('#signupEmail').css("background-color","");
	$("#registerModal").modal("hide");
}

function getDistrictWiseSubject(districtId)
{
	var distId = "";
	distrId = districtId;
	if(distrId.length==0 || distrId=="")
	{
		distrId = "0";
	}
	//alert(" districtId get :: "+districtId);
	try{
		QuestAjax.getSubjects(distrId,true,
		{
			async: true,		
			callback: function(data){		
			if(data)
			 {		
				document.getElementById("subjectId").innerHTML = ""
			 	document.getElementById("subjectId").innerHTML = data;
			 }
			else
			 {
				document.getElementById("subjectId").innerHTML = "";							
		     }		
		}
		}		
	);	}catch(e){alert(e)}
}


//shadab start
function hideJBAdvanceSearch()
{
	$('#moreFilterBtn').fadeIn();
	$('#moreFilterDiv').slideUp(1000);
	$('#divJBData').show();
	$('#ShowListingsId').hide();
}

function searcESJob()
{
	$('#divJBData').show();
	$('#ShowListingsId').hide();
	page=1;
	noOfRows=10;
	searchDocument(0);
}

var locations = "";
function searchDocument(from)
{
	//$('#loadingDiv').show();
	//alert(document.getElementById("loadingDiv").style.display);
	//$('#divJBData').show();
	$('#loadingDiv').fadeIn();
	$('#moreFilterBtn').fadeIn();
	$('#moreFilterDiv').slideUp('slow');
	//alert(document.getElementById("loadingDiv").style.display);
	
	
	//document.getElementById("loadingDiv").style.display="block";
	//alert(document.getElementById("loadingDiv").style.display);
	//alert(from);
		
	
	try
	{
		var iframeNorm = document.getElementById('ifrmPostedWithin');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
		var inputNormScore = innerNorm.getElementById('PostedWithinFrm');
		document.getElementById("postedWithin").value=inputNormScore.value;	
	}
	catch(err)
	{
		
	}
		
	var monthWiseSearch	=	document.getElementById('postedWithin').value;
	var districtName = $('#districtName').val();
	
	//alert(document.getElementById("stateId").disabled);
	var stateName = $("#stateId option:selected").text();
	//alert("stateId="+stateId);
	var cityName = $('#cityId').val();
	//alert("cityId="+cityId);
	var schoolName = $('#schoolName1').val();
	//alert("schoolName1="+schoolName1);
	var certificateTypeMaster	= 	document.getElementById('certType').value;
	//alert("certificateTypeMaster="+certificateTypeMaster);
	var zipCode = $('#zipCode').val();
	
	var subjectIdList = "";
	for(var i=0; i<subjectId.options.length;i++)
	{	
		if(subjectId.options[i].selected == true){
			//subjectIdList = subjectIdList+subjectId.options[i].text+",";
			subjectIdList = subjectIdList+subjectId.options[i].value+" ";
		}
	}
	//alert(subjectIdList);
	//return false;
	
	
	//return false;
//	alert(monthWiseSearch);
	//var searchArray = new Array();
	
	//alert($("#searchTerm").val());
	//alert(document.getElementById('districtId').value);
	//alert(page);
	var districtid=document.getElementById('districtId').value;
	//if(document.getElementById("stateId").disabled)
		//stateName="";
	//if(document.getElementById("cityId").disabled)
		//cityName="";
	//if(document.getElementById("schoolName1").disabled)
		//schoolName="";
	//if(document.getElementById("districtName").disabled)
		//districtName="";
	//if(document.getElementById("districtId").disabled)
		//districtid="";
		
	JBServiceAjax.searchByDocument($("#searchTerm").val(),monthWiseSearch,districtName,districtid,stateName,cityName,schoolName,certificateTypeMaster,zipCode,from,subjectIdList,noOfRows,page,{
		
		async: true,		
		callback: function(data){
		try
		{
		
		
		
			//alert(data);
			//alert(data.length);
			var getValue = data.split("@@@");
	//		alert(from>0);
			if(from>0)
			{
		//		alert(from>0);
				document.getElementById("divJBData").innerHTML=document.getElementById("divJBData").innerHTML+getValue[0];
	//			 alert(from>0);
				
			}
			else
			{
				document.getElementById("divJBData").innerHTML=getValue[0];
			}
			
			$('#loadingDiv').hide();
			
			document.getElementById("from").innerHTML=getValue[1];
			
		
			if(page==1)
			//if(getValue[2].length>0)
			{	
				
					locations=getValue[2];
				
				var val = locations.split("||");
				var adrs = locations.split("||");
				
				

			//}
			//alert(locations);
				var latlang="";
			var map = new google.maps.Map(document.getElementById('jbMap'), {
                zoom: 3,
                center: new google.maps.LatLng(37.09024, -95.712891),
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });

              var infowindow = new google.maps.InfoWindow();
              var marker, i;
              var latitude = "";
       	   var longitude = "";
       	   
              //var locations = getValue[1];
                var storeLoc = [];
               
                	var init = 0;
                	var end = 10; 
                	var count = 0;
                	var m=0;
                	
                	var pp=[];
                	var locMap = [];
                	
                	
                	//new adter adding latitude and longitude
                	//alert(val);
                	 for (i = 0; i < val.length-1; i++)
                     {
                		 
                		// alert(val[i].split("---")[1]);
                		 //alert(val[i].split("---")[1].split(",")[0]+"=="+val[i].split("---")[1].split(",")[1]);
                		 
                         marker = new google.maps.Marker({
                             position: new google.maps.LatLng(val[i].split("---")[1].split(",")[0],val[i].split("---")[1].split(",")[1]),
                             map: map,
                             animation: google.maps.Animation.DROP,
	                          icon: "images/rsz_red_pin.png",
	                          title:val[i].split("---")[0]   
                           });

                           google.maps.event.addListener(marker, 'click', (function(marker, i) {
                             return function() {
                            	//alert(val[i]);
                               infowindow.setContent(val[i].split("---")[0]);
                               infowindow.open(map, marker);
                             }
                           })(marker, i));

                     } 
                	
                	

		}
			//map end
		}
		catch(err){}
		
		                  
	}
	});	

	
	
	
}


function getESPagingForJobboard(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	searchDocument(0);
}

function runScript(characterCode){
	//alert(characterCode.keyCode);
	if(characterCode.keyCode == 13)
	{
	
		//searchDocument(0);
		searcESJob();
	}
	
}


//shadab end
