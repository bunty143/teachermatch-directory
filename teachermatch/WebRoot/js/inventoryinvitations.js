var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	noOfRows = document.getElementById("pageSize").value;
	displayTempTeacher();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayTempTeacher();
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

/*search with district Name*/
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	return searchArray;
}

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="") {
			var onclick="onclick=\"$('#divTxtShowData').css('display','none');\"";
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;

				if(count==10)
					break;
			}
		} else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}

		scrolButtom();
	}catch (err){}
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if($('#districtName').val().trim()!=''){
	document.getElementById("districtHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(showDataArray && showDataArray[index]) {
			dis.value=showDataArray[index];
			document.getElementById("districtHiddenlId").value=hiddenDataArray[index];
		}
		
	} else {
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}

	if(document.getElementById(divId)) {
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
	}else{
		document.getElementById("districtHiddenlId").value='';
	}
}

function getGroups(assessmentType)
{
	getGroupsByAssessmentType(assessmentType, 1);
}

function getGroupsByAssessmentType(assessmentType, from)
{
	dwr.util.removeAllOptions("assessmentGroupDetails");
	dwr.util.addOptions("assessmentGroupDetails",["Select Group"]);
	AssessmentAjax.getAllAssessmentGroupDetailsList(assessmentType, from,{
		async: false,
		callback: function(data){
		console.log(data);
		dwr.util.addOptions("assessmentGroupDetails",data,"assessmentGroupId","assessmentGroupName");
	    },errorHandler:handleError
	});
}

function validateInvitations()
{
	$('#errordiv').empty();
	$('#orgType').css("background-color", "");
	$('#assessmentType').css("background-color", "");
	$('#assessmentGroupDetails').css("background-color", "");
	$('#districtName').css("background-color", "");
	$('#teacherfile').css("background-color", "");
	var cnt=0;
	var focs=0;
	
	var orgType			=	document.getElementById("orgType").value;
	var assessmentType	=	document.getElementById("assessmentType").value;
	var grpName			=	document.getElementById("assessmentGroupDetails").value;
	var districtId		=	document.getElementById("districtHiddenlId").value;
	var teacherfile		=	document.getElementById("teacherfile").value;
	var aDoc="";
	
	if (orgType=="" || orgType==0){
		$('#errordiv').append("&#149; Please select OrgType<br>");
		if(focs==0)
			$('#orgType').focus();
		$('#orgType').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(assessmentType=="" || assessmentType==0){
		$('#errordiv').append("&#149; Please select Inventory Type<br>");
		if(focs==0)
			$('#assessmentType').focus();
		$('#assessmentType').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(grpName=="" || grpName=="Select Group" || grpName==0){
		$('#errordiv').append("&#149; Please select Group Name<br>");
		if(focs==0)
			$('#assessmentGroupDetails').focus();
		$('#assessmentGroupDetails').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(districtId=="" || districtId==0){
		$('#errordiv').append("&#149; Please enter District Name<br>");
		if(focs==0)
			$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	else if(districtId!="" && districtId!=0){
		InventoryInvitationsAjax.getDistrictWithAuthKey(districtId,{
			async: false,
			callback: function(data){
				console.log(data);
				if(data=="0"){
					$('#errordiv').append("&#149; Please enter another District Name which have authKey<br>");
					if(focs==0)
						$('#districtName').focus();
					$('#districtName').css("background-color", "#F5E7E1");
					cnt++;focs++;
				}
			},errorHandler:handleError
		});
	} 
	if(teacherfile==""){
		$('#errordiv').append("&#149; Please select xls or xlsx file to import<br>");
		if(focs==0)
			$('#teacherfile').focus();
		$('#teacherfile').css("background-color", "#F5E7E1");
		cnt++;focs++;
		aDoc="1";
	}
	else if(teacherfile!="")
	{
		var ext = teacherfile.substr(teacherfile.lastIndexOf('.') + 1).toLowerCase();	
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("teacherfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("teacherfile").files[0].size;
			}
		}
		
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errordiv').append("&#149; Please select xls or xlsx file only.<br>");
			if(focs==0)
				$('#teacherfile').focus();
			$('#teacherfile').css("background-color", "#F5E7E1");
			cnt++;focs++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordiv').append("&#149; File size must be less than 10mb.<br>");
			if(focs==0)
				$('#teacherfile').focus();
			$('#teacherfile').css("background-color", "#F5E7E1");
			cnt++;focs++;
			aDoc="1";
		}
	}
	
	if(cnt==0){
		try{
			//$('#loadingDiv').show();
			if(teacherfile!=""){
					document.getElementById("teacherUploadServlet").submit();
			}
		}catch(err){}
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function uploadDataTeacher(fileName,sessionId)
{
	var orgType			=	document.getElementById("orgType").value;
	var assessmentType	=	document.getElementById("assessmentType").value;
	//var grpName			=	document.getElementById("assessmentGroupDetails").value;
	var grpName			=	$("#assessmentGroupDetails option:selected").text();
	var districtId		=	document.getElementById("districtHiddenlId").value;
	
	$('#loadingDiv').fadeIn();
	InventoryInvitationsAjax.saveTeacherTemp(fileName,sessionId,orgType,assessmentType,grpName,districtId,{ 
		async: true,
		callback: function(data){
			if(data=='1'){
				window.location.href="invitationstemplist.do";
			}else{
				$('#loadingDiv').hide();
				$('#errordiv').show();
				$('#errordiv').append("&#149; Field(s) "+data+" does not match.<br>");
			}
		},
		errorHandler:handleError 
	});
}

function displayTempTeacher()
{
	InventoryInvitationsAjax.displayTempTeacherRecords(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#tempTeacherGrid').html(data);
			applyScrollOnTbl();
		},
		errorHandler:handleError  
	});
}

function tempTeacherReject(sessionIdTxt)
{
	$('#loadingDiv').fadeIn();
	InventoryInvitationsAjax.deleteTempTeacher(sessionIdTxt,{ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			//window.location.href="inventoryinvitations.do";
			tempTeacher();
		},
		errorHandler:handleError 
	});
}

function tempTeacher()
{
	window.location.href="inventoryinvitations.do";
}

function saveTeacher()
{
	$('#loadingDiv').fadeIn();
	InventoryInvitationsAjax.saveTeacher({ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			//document.getElementById("Msg").innerHTML="Candidate details have been imported successfully.";
			$('#myModalMsg').modal('show');
		},
		errorHandler:handleError 
	});
}