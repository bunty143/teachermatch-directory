
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else if(exception.javaClassName=='undefined'){}
	else{alert(""+resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

function getDashboardSnapshot(){
	
	$('#snapshot').html("<br><br><br><div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	DAorSADashboardAjax.getDashboardSnapshot({ 
		async: true,
		callback: function(data){
		$('#snapshot').html(data);
	},
	errorHandler:handleError  
	});
}
function getDashboardCharts(){
	
	$('#chart1').html("<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	$('#chart2').html("<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	$('#chart3').html("<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	DAorSADashboardAjax.getDashboardCharts({ 
		async: true,
		callback: function(data){
		//$('#snapshot').html(data);
		var ddtta = data.split("###");
		$('#chart1').html("<img src='"+ddtta[0]+"' width='300' alt='circle graph'>");
		$('#chart2').html("<img src='"+ddtta[1]+"' width='300' alt='circle graph'>");
		$('#chart3').html("<img src='"+ddtta[2]+"' width='300' alt='circle graph'>");
	},
	errorHandler:handleError  
	});
}