/* @Author: Ankit Sharma 
 * @Discription: editbranch js .
 */	
var keyContactDivVal=2;
var keyContactIdVal=null;
/*=========  Paging and Sorting ==============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

var pageNotes = 1;
var noOfRowsNotes = 10;
var sortOrderStrNotes="";
var sortOrderTypeNotes="";

var pageDist 			= 	1;
var noOfRowsDist 		= 	10;
var sortOrderStrDist	=	"";
var sortOrderTypeDist	=	"";

var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr="";
var domainsortOrderType="";
function getPaging(pageno)
{	
	var gridNo	=	document.getElementById("gridNo").value;
	if(gridNo==3){
		if(pageno!='')
		{	
			domainpage=pageno;	
		}
		else
		{	
			domainpage=1;
		}
		domainnoOfRows = document.getElementById("pageSize3").value;
		getDistrictDomains();
		
	}else{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		displayHqBranchDistricts();
	}
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
		var gridNo	=	document.getElementById("gridNo").value;
		if(gridNo==3)
		{
			if(pageno!='')
			{
				domainpage=pageno;	
			}
			else
			{
				domainpage=1;
			}
			domainsortOrderStr	=	sortOrder;
			domainsortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize3")!=null){
				domainnoOfRows = document.getElementById("pageSize3").value;
			}else{
				domainnoOfRows=10;
			}
			getDistrictDomains();
		}else if(gridNo==1){
			if(pageno!=''){
				page=pageno;	
			}else{
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
		
			displayHqBranchDistricts();
		}
		else
		{	
			if(pageno!=''){
				pageNotes=pageno;	
			}else{
				pageNotes=1;
			}
			sortOrderStrNotes	=	sortOrder;
			sortOrderTypeNotes	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRowsNotes = document.getElementById("pageSize").value;
			}else{
				noOfRowsNotes=10;
			}
			displayNotes();
		}
}

function getSortNotesGrid()
{
	$('#gridNo').val("2")
}
function getSortDomainGrid()
{
	$('#gridNo').val("3")
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

/*======== Redirect to Manage User Page ===============*/
	function cancelBranch()
	{
		window.location.href="managebranches.do";
	}

//  Get city List according to state 
	function getCityListByState()
	{
		var stateId = document.getElementById("stateId").value;
		DistrictAjax.getCityListByState(stateId, {async: false, callback: function(data)
			{
				document.getElementById("cityId").innerHTML	=	data;
			},
			errorHandler:handleError 
		});
		return true;
	}

	function addAdministrator(roleId)
	{
		$('#errordistrictdiv').empty();
		$('#firstName').css("background-color", "");
		$('#lastName').css("background-color", "");
		$('#emailAddress').css("background-color", "");
		//dwr.util.setValues({firstName:null,lastName:null,title:null,emailAddress:null,phone:null,mobileNumber:null});
		$("#addAdministratorDiv").fadeIn();
		$('#roleId').val(roleId);
		$('#salutation').focus();
	}
	
	function clearUser()
	{
		$('#errorusersdiv').empty();
		$('#firstName').css("background-color", "");
		$('#lastName').css("background-color", "");
		$('#emailAddress').css("background-color", "");
		//dwr.util.setValues({firstName:null,lastName:null,title:null,emailAddress:null,phone:null,mobileNumber:null});
		//$('#phoneNumber').val("");
		$("#addAdministratorDiv").hide();
	}
	
	function addNotes()
	{
		$("#addNotesDiv").fadeIn();
		$('#errornotediv').empty();
		$('#dNote').find(".jqte_editor").css("background-color", "");
		$('#dNote').find(".jqte_editor").focus();
		$('#dNote').find(".jqte_editor").html("");
	}
	function showFile(flagVal,branch,branchId,docFileName,linkId)
	{
		if(docFileName!=null && docFileName!=''){
			BranchesAjax.showFile(branch,branchId,docFileName,{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					if(data==""){
						//alert("blank............");
						//alert("Job Specific Inventory is not uploaded.")
					}else{
						if(flagVal==1){
							document.getElementById("showLogo").innerHTML="<img src=\""+data+"\">";
						}else if(data.indexOf(".doc")!=-1)
						{
							document.getElementById("iframeJSI").src=data;
						}
						else
						{
							document.getElementById(linkId).href = data; 
							return false;
						}
						
					}
				}
				
			});
		}
		else
		{
			
		}
	}
	function uploadAssessment(val){
		
		if(!(val==null || val=='undefind' || val==0)){
			$("#uploadAssessmentDiv").fadeIn();
			
			if(document.getElementById("cbxUploadAssessment").checked==false)
			{
				$("#uploadAssessmentDiv").hide();
			}
		}
	}
	
	function showKeyContact()
	{
		keyContactIdVal=null;
		keyContactDivVal=2;
		dwr.util.setValues({ dropContactType:null});
		dwr.util.setValues({ keyContactId:null,keyContactFirstName:null,keyContactLastName:null,keyContactEmailAddress:null,keyContactPhoneNumber:null,keyContactTitle:null});
		$('#keyContactFirstName').css("background-color", "");
		$('#keyContactLastName').css("background-color", "");
		$('#keyContactEmailAddress').css("background-color", "");
		$('#errorkeydiv').empty();
		$("#addKeyContactDiv").fadeIn();
		$('#dropContactType').focus();
	}

	function displayKeyContact()
	{
		var branchId	=	document.getElementById("bId").value;
		var headQuarterId = document.getElementById("hqId").value;
		BranchesAjax.displayKeyContactGrid(branchId,headQuarterId,{ 
			async: false,
			callback: function(data)
			{				
					$('#keyContactTable').html(data);
					$('#addKeyContactDiv').hide();
				
			},
			errorHandler:handleError 
		});
	}
	
	/*========  Check Valid Email address  ===============*/
	function isEmailAddress(str) 
	{	
		var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
		return emailPattern.test(str);
	}

	function addKeyContact()
	{
		$('#errorkeydiv').empty();
		$('#keyContactFirstName').css("background-color", "");
		$('#keyContactLastName').css("background-color", "");
		$('#keyContactEmailAddress').css("background-color", "");
		var keyContactId			=	trim(document.getElementById("keyContactId").value);
		var headQuarterId			=	trim(document.getElementById("hqId").value);
		var branchId				=	trim(document.getElementById("bId").value);
		var keyContactTypeId		=	trim(document.getElementById("dropContactType").value);
		var keyContactFirstName		=	trim(document.getElementById("keyContactFirstName").value);
		var keyContactLastName		=	trim(document.getElementById("keyContactLastName").value);
		var keyContactEmailAddress	=	trim(document.getElementById("keyContactEmailAddress").value);
		var keyContactPhoneNumber	=	trim(document.getElementById("keyContactPhoneNumber").value);
		var keyContactTitle			=	trim(document.getElementById("keyContactTitle").value);
		var counter					=	0;
		var focusCount				=	0;
		if(keyContactFirstName == "")
		{
			$('#errorkeydiv').show();	
			$('#errorkeydiv').append("&#149; Please enter Contact First Name<br>");
			$('#keyContactFirstName').css("background-color", "#F5E7E1");
			$('#keyContactFirstName').focus();
			counter++;
			focusCount++;
			//return false;
		}
		if(keyContactLastName=="")
		{
			$('#errorkeydiv').show();	
			$('#errorkeydiv').append("&#149; Please enter Contact Last Name<br>");
			$('#keyContactLastName').css("background-color", "#F5E7E1");
			//$('#keyContactLastName').focus();
			counter++;
			focusCount++;
			//return false;
		}
		if(keyContactEmailAddress=="")
		{
			$('#errorkeydiv').show();	
			$('#errorkeydiv').append("&#149; Please enter Contact Email<br>");
			$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
			//$('#keyContactEmailAddress').focus();
			counter++;
			focusCount++;
			//return false;
		}else 
		if(keyContactEmailAddress!="")
		{
			if(!isEmailAddress(keyContactEmailAddress))
			{	$('#errorkeydiv').show();	
				//$('#errorkeydiv').empty();
				$('#errorkeydiv').append("&#149; Please enter valid Email<br>");
				 //alert("Please enter valid email id");
				$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
				//return false;
				counter++;
				focusCount++;
			}
		}
		
		if(counter	==	0)
		{
			$('#loadingDiv').fadeIn();
			BranchesAjax.saveKeyContact(keyContactDivVal,keyContactId,headQuarterId,branchId,keyContactTypeId,keyContactFirstName,keyContactLastName,keyContactEmailAddress,keyContactPhoneNumber,keyContactTitle,keyContactIdVal, { 
				async: false,
				callback: function(data)
				{
					$('#loadingDiv').hide();
					if(data==3){
						$('#errorkeydiv').show();	
						$('#errorkeydiv').append("&#149; A teacher has already registered with the email address, you provided. A teacher cannot be a Key Contact.<br>");
						$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
					}else if(data==4){
						$('#errorkeydiv').show();	
						$('#errorkeydiv').append("&#149; The Key Contact with same email address is already defined in this District/School. Please provide another email address to define a new Key Contact.<br>");
						$('#keyContactEmailAddress').css("background-color", "#F5E7E1");
					}else{
						displayKeyContact();
					}
				},
				errorHandler:handleError
			});
		}
		else
		{
			$('#errorkeydiv').show();
			return false;
		}
		//$('#errorkeydiv').empty();
	}
	function aKeyContactDiv(val){
		keyContactDivVal=val;
		if(val==1){
			editKeyContact(keyContactIdVal);
		}
	}
	function beforeEditKeyContact(keyContactId){
		keyContactIdVal=keyContactId;
		document.getElementById("Msg").innerHTML="Any change in the details of this Key Contact will reflect in the associated User details. Do you really want to continue?";
		$('#myModalMsg').modal('show');
	}
	
	/*========  Edit Domain ===============*/
	function editKeyContact(keyContactId)
	{
		$('#keyContactFirstName').css("background-color", "");
		$('#keyContactLaststName').css("background-color", "");
		$('#keyContactEmailAddress').css("background-color", "");
		$('#errorkeydiv').empty();
		$('#addKeyContactDiv').fadeIn();
		
		DistrictAjax.getKeyContactsBykeyContactId(keyContactId, { 
			async: false,
			callback: function(data)
			{
				//alert("data keyContactTypeId "+data.keyContactTypeId.contactTypeId+" Name"+data.keyContactTypeId.contactType);
				dwr.util.setValues(data);
				var optsKeyContact = document.getElementById('dropContactType').options;
				for(var i = 0, j = optsKeyContact.length; i < j; i++)
				{
					  if(data.keyContactTypeId.contactTypeId==optsKeyContact[i].value)
					  {
						  optsKeyContact[i].selected	=	true;
					  }
				}
			},
			errorHandler:handleError
		});
		return false;
	}

	function deleteKeyContact(keyContactId)
	{
		if (confirm("Are you sure, you would like to delete this Contact?")) {
			BranchesAjax.deleteKeyContact(keyContactId, { 
				async: false,
				callback: function(data)
				{
					displayKeyContact();
				},
				errorHandler:handleError
				});
		}
	}
	
	function clearKeyContact()
	{
		$("#addKeyContactDiv").hide();
	}
	
	
	
	function validateBranchAdministratorOrAnalyst()
	{
		$('#loadingDiv').fadeIn();
		$('#errorusersdiv').empty();
		$('#firstName').css("background-color", "");
		$('#lastName').css("background-color", "");
		$('#emailAddress').css("background-color", "");

		var salutation			=	trim(document.getElementById("salutation").value);
		var firstName			=	trim(document.getElementById("firstName").value);
		var lastName			=	trim(document.getElementById("lastName").value);
		var title				=	trim(document.getElementById("title").value);
		var emailAddress		=	trim(document.getElementById("emailAddress").value);
		var phoneNumber			=	trim(document.getElementById("phone").value);
		var mobileNumber		=	trim(document.getElementById("mobileNumber").value);
		var roleId				=	document.getElementById("roleId").value;
		var entityType			=	"";
		var authenticationCode	=	"";
		var AEU_RoleId			=	"";
		
		var branchId			=	document.getElementById("bId").value;
		var headQuarterId		=	document.getElementById("hqId").value;
		var counter				=	0;
		var focusCount			=	0;
		
		
		if (firstName	==	"")
		{
			$('#errorusersdiv').show();
			$('#errorusersdiv').append("&#149; Please enter First Name<br>");
			if(focusCount	==	0)
				$('#firstName').focus();
			$('#firstName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		if (lastName	==	"")
		{
			$('#errorusersdiv').show();
			$('#errorusersdiv').append("&#149; Please enter Last Name<br>");
			if(focusCount	==	0)
				$('#lastName').focus();
			$('#lastName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		if (emailAddress=="")
		{
			$('#errorusersdiv').show();
			$('#errorusersdiv').append("&#149; Please enter Email<br>");
			if(focusCount	==	0)
				$('#emailAddress').focus();
			$('#emailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		else if(!isEmailAddress(emailAddress))
		{		
			$('#errorusersdiv').append("&#149; Please enter valid Email<br>");
			if(focusCount==0)
				$('#emailAddress').focus();
			
			$('#emailAddress').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
		
		if(counter	==	0)
		{
			dwr.engine.beginBatch();			
			BranchesAjax.saveBranchAdministratorOrAnalyst(headQuarterId,branchId,emailAddress,firstName,lastName,salutation,entityType,title,phoneNumber,mobileNumber,authenticationCode,roleId,{ 
				async: false,
				callback: function(data)
				{
					if((data	==	3) || (data	==	4))
					{
						$('#errorusersdiv').append("&#149; A Member has already registered with the email, you provided. Please provide another email address to register<br>");
						if(focusCount==0)
							$('#emailAddress').focus();
						$('#errorusersdiv').show();
						$('#emailAddress').css("background-color", "#F5E7E1");
						$('#loadingDiv').hide();
						return false;
					}
					else
					{
						if(data	==	2)
						{
							alert(" Server Error ");
						}
						else
						{
							if(roleId==11)
							{
								displayBranchAdministrator(roleId);
							}
							if(roleId==13)
							{
								displayBranchAnalyst(roleId);
							}
							clearUser();
							$('#loadingDiv').hide();
						}
					}
				},
				errorHandler:handleError 
				});
				dwr.engine.endBatch();					
		}
		else
		{
			$('#errorusersdiv').show();
			$('#loadingDiv').hide();
			return false;
		}
	}

/*=========== Displaying Administrator and Analyst Grid =====================*/	
	function displayBranchAdministrator(roleId)
	{
		tpJbIDisable();
		var branchId			=	document.getElementById("bId").value;
		var headQuarterId		=	document.getElementById("hqId").value;
		BranchesAjax.displayBranchAdministratorGrid(headQuarterId,branchId,roleId,{ 
			async: false,
			callback: function(data)
			{
				$('#branchAdministratorDiv').html(data);
				/*========== for showing Tool tip on Images ============*/
				tpJbIEnable();
			},
			errorHandler:handleError 
		});
	}

	function tpJbIEnable()
	{
		var noOrRow = $('#branchAdministratorDiv').children().size();
		for(var j=1;j<=noOrRow;j++)
		{
			$('#actDeactivateUserAdministrator'+j).tooltip();
		}
	}
	
	function tpJbIDisable()
	{
		var noOrRow = $('#branchAdministratorDiv').children().size();
		for(var j=1;j<=noOrRow;j++)
		{
			$('#actDeactivateUserAdministrator'+j).trigger('mouseout');
		}
	}
	
	function displayBranchAnalyst(roleId)
	{
		tpJbIAnalystDisable();
		var branchId			=	document.getElementById("bId").value;
		var headQuarterId		=	document.getElementById("hqId").value;
		BranchesAjax.displayBranchAdministratorGrid(headQuarterId,branchId,roleId,{ 
			async: false,
			callback: function(data)
			{
				$('#branchAnalystDiv').html(data);
				tpJbIAnalystEnable();
			},
			errorHandler:handleError 
		});
	}
	
	/*========== For Analyst Enabling Tool Tip =============*/
	function tpJbIAnalystEnable()
	{
		var noOrRow = $('#branchAnalystDiv').children().size();
		for(var j=1;j<=noOrRow;j++)
		{
			$('#actDeactivateUserAnalyst'+j).tooltip();
		}
	}
	
	function tpJbIAnalystDisable()
	{
		var noOrRow = $('#branchAnalystDiv').children().size();
		for(var j=1;j<=noOrRow;j++)
		{
			$('#actDeactivateUserAnalyst'+j).trigger('mouseout');
		}
	}
	
	/*========  activateDeactivateUser ===============*/
	function activateDeactivateDistrictAdministratorOrAnalyst(roleId,userId,status)
	{
		UserAjax.activateDeactivateUser(userId,status, { 
			async: false,
			callback: function(data)
			{
				//alert("data "+data);
				if(roleId==2)
				{
					displayBranchAdministrator(roleId);
				}
				if(roleId==5)
				{
					displayBranchAnalyst(roleId);
				}
			},
			errorHandler:handleError 
		});
	}
	
	
	
	
	/*============ Add HeadQuarterBranches Functionality ===========================*/
	function addDistict()
	{
		//dwr.util.setValues({districtORSchoolName:null,districtOrSchooHiddenlId:null});
		$("#addDistrictDiv").fadeIn();
		$("#districtName").focus();
		$('#districtName').val("");
	}
	
	function displayHqBranchDistricts()
	{
		//dwr.util.setValues({districtORSchoolName:null,districtOrSchooHiddenlId:null});
		var branchId	=	trim(document.getElementById("bId").value);
		BranchesAjax.displayHqBranchesDistrictGrid(branchId,noOfRows,page,sortOrderStr,sortOrderType,{ 
			async: false,
			callback: function(data)
			{
				$('#hqBranchesDistrict').html(data);
				applyScrollOnTbl();
				$('#districtName').val("");
				$('#addDistrict').hide();				
			},
			errorHandler:handleError 
		});
	}
	
	function saveHqBranchDistrict()
	{
		var headQuarterId  =	 trim(document.getElementById("hqId").value);
		var branchId  =	0;
		try{
			branchId = trim(document.getElementById("bId").value);
		}catch(e){}
		var districtId = trim(document.getElementById("districtId").value);
		
		if(districtName=="")
		{
				$('#errorhqbranchdiv').show();	
				$('#errorhqbranchdiv').empty();
				$('#errorhqbranchdiv').append("&#149; Please enter District Name<br>");
				$('#districtName').css("background-color", "#F5E7E1");
				$("#districtName").focus();
				return false;
		}
		if(districtId=="")
		{
				$('#errorhqbranchdiv').show();	
				$('#errorhqbranchdiv').empty();
				$('#errorhqbranchdiv').append("&#149; Please enter valid District Name<br>");
				$('#districtName').css("background-color", "#F5E7E1");
				$("#districtName").focus();
				return false;
		}
		//$('#errornotediv').empty();
		
		HeadQuarterAjax.saveHeadQuarterBranchesDistricts(headQuarterId,branchId,districtId, { 
			async: false,
			callback: function(data)
			{
				//alert(" data "+data);
				if(data	==	3)
				{
					$('#errorhqbranchdiv').empty();
					$('#errorhqbranchdiv').append("&#149; The District you enterd is already added. Please enter another District Name<br>");
					$('#districtName').focus();
					$('#errorhqbranchdiv').show();
					$('#districtName').css("background-color", "#F5E7E1");
					return false;
				}
				else
				{
					if(data	==	2)
					{
						alert(" Server Error ");
					}
					else
					{
						displayHqBranchDistricts();
						$('#districtName').val("");
						$('#districtId').val("");
					}
				}
			},
			errorHandler:handleError 
		});
	}

	function deleteHqBranchDistricts(hqbdId,distId)
	{
		$('#districtName').css("background-color", "");
		$('#errorhqbranchdiv').empty();
		if (confirm("Are you sure, you would like to delete this District?")) {
			BranchesAjax.deleteHqBranchesDistrict(hqbdId,distId, { 
				async: false,
				callback: function(data)
				{
					displayHqBranchDistricts();
					$('#districtName').val("");
				},
				errorHandler:handleError 
			});
		}
	}

	function clearHqBranchDistricts()
	{
		//dwr.util.setValues({districtORSchoolName:null,districtOrSchooHiddenlId:null});
		$("#addDistrictDiv").hide();
		$('#districtName').val("");
		$('#errorhqbranchdiv').empty();
	}
	
	/*============ Radio buttons ==========================*/
	function uncheckedOtherRadio(val)
	{  
		var rd1					=	document.getElementsByName("noDistrictUnderContract");
		var rd2					=	document.getElementsByName("allDistrictsUnderContract");
		var rd4					=	document.getElementsByName("selectedDistrictUnderContract");

		if(val	==	1)
		{
			for(i=0;i<rd2.length;i++)
			{
				rd2[i].checked	=	false;
			}
			for(i=0;i<rd4.length;i++)
			{
				rd4[i].checked	=	false;
			}
			$("#addSchoolLink").hide();
			$("#hqBranchesDistrict").hide();
			$("#addDistrictDiv").hide();
		}
		if(val	==	2)
		{
			for(i=0;i<rd1.length;i++)
			{
				rd1[i].checked	=	false;
			}
			for(i=0;i<rd4.length;i++)
			{
				rd4[i].checked	=	false;
			}
			$("#addSchoolLink").hide();
			$("#hqBranchesDistrict").hide();
			$("#addDistrictDiv").hide();
		}
		if(val	==	3)
		{
			for(i=0;i<rd1.length;i++)
			{
				rd1[i].checked	=	false;
			}
			for(i=0;i<rd2.length;i++)
			{
				rd2[i].checked	=	false;
			}
			for(i=0;i<rd4.length;i++)
			{
				rd4[i].checked	=	false;
			}
			$("#addSchoolLink").hide();
			$("#hqBranchesDistrict").hide();
			$("#addDistrictDiv").hide();
		}
		if(val	==	4)
		{
			for(i=0;i<rd1.length;i++)
			{
				rd1[i].checked	=	false;
			}
			for(i=0;i<rd2.length;i++)
			{
				rd2[i].checked	=	false;
			}
			$("#addSchoolLink").fadeIn();
			$("#hqBranchesDistrict").fadeIn();
		}
		
	}
	
	function getHQBDGrid()
	{
		$('#gridNo').val("1");
	}
	
	
	/*=====================================================================*/
	var count=0;
	var index=-1;
	var length=0;
	var divid='';
	var txtid='';
	var hiddenDataArray = new Array();
	var showDataArray = new Array();
	var degreeTypeArray = new Array();
	var hiddenId="";
	function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38) //up key
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13) // RETURN
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			
			document.getElementById("districtName").focus();
			
		} 
		else if(event.keyCode==9) // Tab
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			searchArray = getDistrictMasterArray(txtSearch.value);
			fatchData(txtSearch,searchArray,txtId,txtdivid);
			
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}

	function getDistrictMasterArray(districtName){
		var searchArray = new Array();
		DistrictAjax.getActiveFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
			errorHandler:handleError 
		});	

		return searchArray;
	}


	var selectFirst="";

	var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			if(document.getElementById(txtId).value!="")
			{
				
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i].toUpperCase() + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
					
				}
				
			}
			else {
				
			}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}
			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
			{
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
	}
	function hideDistrictMasterDiv(dis,hiddenId,divId)
	{
		if(parseInt(length)>0){
			if(index==-1){
				index=0;
			}
			if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
				document.getElementById(hiddenId).value=hiddenDataArray[index];
			}
			
			if(dis.value==""){
				document.getElementById(hiddenId).value="";
			}
			else if(showDataArray && showDataArray[index]){
				dis.value=showDataArray[index];
				
			}
			
		}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
		}
		
		if(document.getElementById(divId))
		{
			document.getElementById(divId).style.display="none";
		}
		index = -1;
		length = 0;
	}

	var downArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index<length-1){
				for(var i=0;i<10;i++){
					{
						if(document.getElementById('divResult'+txtdivid+i))
							var div_id=document.getElementById('divResult'+txtdivid+i);
						if(div_id)
						{
							if(div_id.className=='over')
								index=div_id.id.split('divResult'+txtdivid)[1];
							div_id.className='normal';
						}
					}
				}
				index++;
				if(document.getElementById('divResult'+txtdivid+index))
				{
					var div_id=document.getElementById('divResult'+txtdivid+index);
					div_id.className='over';
					document.getElementById(txtid).value = div_id.innerHTML;
					selectFirst=div_id.innerHTML;
				}
			}
		}
	}

	var upArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index>0){
				for(var i=0;i<length;i++){

					var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
				index--;
				if(document.getElementById('divResult'+txtdivid+index))
					document.getElementById('divResult'+txtdivid+index).className='over';
				if(txtid && document.getElementById('divResult'+txtdivid+index))
				{
					document.getElementById(txtid).value=
						document.getElementById('divResult'+txtdivid+index).innerHTML;
					selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
				}
			}
		}
	}

	var overText=function (div_value,txtdivid) {

		for(var i=0;i<length;i++)
		{
			if(document.getElementById('divResult'+i))
			{
				var div_id=document.getElementById('divResult'+i);

				if(div_id.className=='over')
					index=div_id.id.split(txtdivid)[1];
				div_id.className='normal';
			}
		}
		div_value.className = 'over';
		document.getElementById(txtid).value= div_value.innerHTML;
	}
	function trim(s)
	{
		while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
			s = s.substring(1,s.length);
		}
		while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
			s = s.substring(0,s.length-1);
		}
		return s;
	}
	function mouseOverChk(txtdivid,txtboxId)
	{	
		for(var i=0;i<length;i++)
		{
			$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
		}
	}


	function fireMouseOverEvent(event)
	{
		for(var i=0;i<length;i++)
		{	
			document.getElementById('divResult'+event.data.param2+i).className='normal';
		}
	    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
	   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
	    index=event.data.param1;
	}




	//Search Records By EntityType

	function DisplayHideSearchBox(){
		var entity_type	=	document.getElementById("MU_EntityType").value; 
		if(entity_type ==2)
		{
			$("#Searchbox").fadeIn();
			$('#SearchTextboxDiv').fadeIn();
			$('#districtName').focus();
		}else{
			document.getElementById("districtName").value="";
			$("#Searchbox").fadeIn();
			$("#SearchTextboxDiv").hide();
		}
	}

	function onLoadDisplayHideSearchBox(entity_type){
		
		if(entity_type ==2){
			$("#Searchbox").hide();
			$('#SearchTextboxDiv').hide();
			document.getElementById("MU_EntityType").value=2;
		}
		else if(entity_type ==5) // for headquarter
		{
			$("#Searchbox").hide();
			$('#SearchTextboxDiv').hide();
			document.getElementById("MU_EntityType").value=5;
		}
		else if(entity_type ==1){
			$("#Searchbox").fadeIn();
			$('#SearchTextboxDiv').fadeIn();
			document.getElementById("districtName").focus();
			document.getElementById("MU_EntityType").value=2;
		}else{
			$("#Searchbox").fadeIn();
			$('#SearchTextboxDiv').hide();
		}
		ShowDistrict();
	}

	/*========  Show District ===============*/
	function searchDistrict()
	{
		page = 1;
		ShowDistrict();
	}
	function ShowDistrict()
	{
		$('#loadingDiv').fadeIn();
		var branchExist = $('#branchExist').val();
		var branchId = "";
		var status = "";
		var entityID	=	document.getElementById("MU_EntityType").value;
		var searchText	="";
		try{
			searchText=document.getElementById("districtName").value;
		}catch(err){
			searchText="";
		}

			branchId = $('#branchId').val();
			
			status = "All";
				delay(1000);
				DistrictAjax.SearchDistrictRecords(entityID,searchText,noOfRows,page,sortOrderStr,sortOrderType,branchId,status,{ 
					async: true,
					callback: function(data)
					{
					$('#divMain').html(data);
					applyScrollOnTbl();
					$('#loadingDiv').hide();
					},
					errorHandler:handleError 
				});
	}
	function delay(sec){
		var starttime = new Date().getTime();
		starttime = starttime+sec;
		while(true){
			if(starttime< new Date().getTime()){
				break;
			}
		}
	}

	//========  Rahul Tyagi: Changes  19/11/2014===============
	function activateDeactivateDistrict(district,status)
	{
		document.getElementById("actdist").value=district;
		document.getElementById("diststat").value=status;
		if(status=="I")
		{
			$('#myModalactMsgShow').modal('show');
		}
		
		else
		{
		toggleStatus();	
		}
	}

	function toggleStatus()
	{
		var district=document.getElementById("actdist").value;
		var status=document.getElementById("diststat").value;
		DistrictAjax.activateDeactivateDistrict(district,status, { 
			async: true,
			callback: function(data)
			{
			
				
			
				ShowDistrict();
			},
			errorHandler:handleError });	
	}




	//===========================================================
	 // for branch auto search

	function getBranchMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
	{	
		//alert("01");
		hiddenId=hiddenId;
		divid=txtdivid;
		txtid=txtSearch.id;
		if(event.keyCode==40){
			downArrowKey(txtdivid);
		} 
		else if(event.keyCode==38)
		{
			upArrowKey(txtdivid);
		} 
		else if(event.keyCode==13)
		{
			if(document.getElementById(divid))
				document.getElementById(divid).style.display='block';
			document.getElementById("branchName").focus();
		} 
		else if(event.keyCode==9)
		{
			
		}
		else if(txtSearch.value!='')
		{
			index=-1;
			length=0;
			document.getElementById(divid).style.display='block';
			//alert("02");
			searchArray = getBranchArray(txtSearch.value);
			fatchData(txtSearch,searchArray,txtId,txtdivid);
		}
		else if(txtSearch.value==""){
			document.getElementById(divid).style.display='none';
		}
	}

	function getBranchArray(branchName){
		
		//alert("03");
		var searchArray = new Array();
		BranchesAjax.getFieldOfBranchList(branchName,{  
			async: false,		
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].branchName;
				hiddenDataArray[i]=data[i].branchId;
				showDataArray[i]=data[i].branchName;
			}
		}
		});	

		return searchArray;
	}

	function hideBranchMasterDiv(dis,hiddenId,divId)
	{
		if(parseInt(length)>0){
			if(index==-1){
				index=0;
			}
				document.getElementById(hiddenId).value=hiddenDataArray[index];
			
			if(dis.value==""){
				document.getElementById(hiddenId).value="";
			}
			else if(showDataArray && showDataArray[index]){
				dis.value=showDataArray[index];
				
			}
			
		}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
			if(dis.value!="")
			{
				var cnt=0;
				var focs=0;	
				$('#errordiv').empty();	
				$('#errordiv').append("&#149; Please enter valid Branch<br>");
			}
		}
		if(document.getElementById(divId))
		{
			document.getElementById(divId).style.display="none";
		}
		//getDistrictWiseSubject(document.getElementById(hiddenId).value);
		index = -1;
		length = 0;
	}

	var selectFirst="";

	var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			//alert(" length :: "+len);
			if(document.getElementById(txtId).value!="")
			{			
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i] + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
					
				}
				
			}
			else {
				
			}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}
			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
			{
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
	}

	var downArrowKey = function(txtdivid){
		if(txtdivid)
		{
			if(index<length-1){
				for(var i=0;i<10;i++){
					{
						if(document.getElementById('divResult'+txtdivid+i))
							var div_id=document.getElementById('divResult'+txtdivid+i);
						if(div_id)
						{
							if(div_id.className=='over')
								index=div_id.id.split('divResult'+txtdivid)[1];
							div_id.className='normal';
						}
					}
				}
				index++;
				if(document.getElementById('divResult'+txtdivid+index))
				{
					var div_id=document.getElementById('divResult'+txtdivid+index);
					div_id.className='over';
					document.getElementById(txtid).value = div_id.innerHTML;
					selectFirst=div_id.innerHTML;
				}
			}
		}
	}

	var upArrowKey = function(txtdivid){
		
		if(txtdivid)
		{
			if(index>0){
				for(var i=0;i<length;i++){

					var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
				index--;
				if(document.getElementById('divResult'+txtdivid+index))
					document.getElementById('divResult'+txtdivid+index).className='over';
				if(txtid && document.getElementById('divResult'+txtdivid+index))
				{
					document.getElementById(txtid).value=
						document.getElementById('divResult'+txtdivid+index).innerHTML;
					selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
				}
			}
		}
	}

	function mouseOverChk(txtdivid,txtboxId)
	{	
		for(var i=0;i<length;i++)
		{
			$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
		}
	}


	function fireMouseOverEvent(event)
	{
		for(var i=0;i<length;i++)
		{	
			document.getElementById('divResult'+event.data.param2+i).className='normal';
		}
	    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
	   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
	    index=event.data.param1;
	}

	function __mouseOverChk(txtdivid,txtboxId)
	{	
		for(var i=0;i<length;i++)
		{
			document.getElementById('divResult'+txtdivid+i).className='normal';		
			if ($('#divResult'+txtdivid+i).is(':hover')) 
			{
				document.getElementById('divResult'+txtdivid+i).className='over';	       
		       	document.getElementById(txtboxId).value= $('#divResult'+txtdivid+i).text();
		        index=i;
		    }
		}

	}

	var overText = function (div_value,txtdivid) 
	{
		for(var i=0;i<length;i++)
		{
			if(document.getElementById('divResult'+i))
			{
				var div_id=document.getElementById('divResult'+i);

				if(div_id.className=='over')
					index=div_id.id.split(txtdivid)[1];
				div_id.className='normal';
			}
		}
		div_value.className = 'over';
		document.getElementById(txtid).value= div_value.innerHTML;
	}
	function trim(s)
	{
		while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
			s = s.substring(1,s.length);
		}
		while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
			s = s.substring(0,s.length-1);
		}
		return s;
	}
