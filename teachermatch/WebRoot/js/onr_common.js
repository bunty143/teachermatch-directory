/*========  For Sorting  ===============*/
var pagePNR 		= 	1;
var noOfRowsPNR 	= 	10;
var sortOrderStrPNR	=	"";
var sortOrderTypePNR=	"";

var pageMsg 		= 	1;
var sortOrderStrMsg	=	"";
var sortOrderTypeMsg=	"";
var noOfRowsMsg 	= 	10;	

//For Assessment
var pageASMT 			= 	1;
var noOfRowsASMT		=	10;
var sortOrderStrASMT	=	"";
var sortOrderTypeASMT 	= 	10;

var pageForI9 			= 	1;
var noOfRowsForI9 		= 	10;
var sortOrderStrForI9	=	"";
var sortOrderTypeForI9	=	"";

var pageForFP 			= 	1;
var noOfRowsForFP 		= 	10;
var sortOrderStrForFP	=	"";
var sortOrderTypeForFP	=	"";

var pageForDT 			= 	1;
var noOfRowsForDT 		= 	10;
var sortOrderStrForDT	=	"";
var sortOrderTypeForDT	=	"";

var pageForTS 			= 	1;
var noOfRowsForTS 		= 	10;
var sortOrderStrForTS	=	"";
var sortOrderTypeForTS	=	"";

var pageForEEOC 		= 	1;
var noOfRowsForEEOC 	= 	10;
var sortOrderStrForEEOC	=	"";
var sortOrderTypeForEEOC=	"";

function divClose(){
	 pageForI9 			= 	1;
	 noOfRowsForI9 		= 	10;
	 sortOrderStrForI9	=	"";
	 sortOrderTypeForI9	=	"";

	 pageForFP 			= 	1;
	 noOfRowsForFP 		= 	10;
	 sortOrderStrForFP	=	"";
	 sortOrderTypeForFP	=	"";

	 pageForDT 			= 	1;
	 noOfRowsForDT 		= 	10;
	 sortOrderStrForDT	=	"";
	 sortOrderTypeForDT	=	"";

	 pageForTS 			= 	1;
	 noOfRowsForTS 		= 	10;
	 sortOrderStrForTS	=	"";
	 sortOrderTypeForTS	=	"";

	 pageForEEOC 		= 	1;
	 noOfRowsForEEOC 	= 	10;
	 sortOrderStrForEEOC	=	"";
	 sortOrderTypeForEEOC=	"";	
}

function getPagingPnr(pageno)
{
	var name	=	document.getElementById("myModalLabelI9Hidden").value;
	var name1	=	document.getElementById("myModalLabelFpHidden").value;
	var name2	=	document.getElementById("myModalLabelDtHidden").value;
	var name3	=	document.getElementById("myModalLabelTransHidden").value;
	var name4	=	document.getElementById("myModalLabelEEOCHidden").value;

	var sapStatus1	=	document.getElementById("addStatusI9Hidden").value;
	var sapStatus2	=	document.getElementById("addStatusFPHidden").value;
	var sapStatus3	=	document.getElementById("addStatusDTHidden").value;
	var sapStatus4	=	document.getElementById("addStatusTransHidden").value;
	var sapStatus5	=	document.getElementById("addStatusEEOCHidden").value;
	if(currentPageFlag == "job"){
		var t_id 	= 	document.getElementById("teacherId").value;
		var e_id	=	document.getElementById("eligibilityId").value;
		var j_id	=	document.getElementById("jobId").value;
		
		if(pageno!='') {
			pageForI9=pageno;	
		} else {
			pageForI9=1;
		}
		noOfRowsForI9 = document.getElementById("pageSize").value;
		open_popup(t_id,j_id,e_id,name,sapStatus1);
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!='')
		{
			pageASMT=pageno;	
		}
		else
		{
			pageASMT=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
		noOfRowsASMT 	= document.getElementById("pageSize3").value;
		getdistrictAssessmetGrid_DivProfile(teacherId);
	}  else if(currentPageFlag == "fp"){
		
		var t_id 	= 	document.getElementById("teacherId1").value;
		var e_id	=	document.getElementById("eligibilityId1").value;
		var j_id	=	document.getElementById("jobId1").value;
		
		if(pageno!='') {
			pageForFP=pageno;
		} else {
			pageForFP=1;
		}
		noOfRowsForFP = document.getElementById("pageSize").value;
		open_popup_fp(t_id,j_id,e_id,name1,sapStatus2);
	}else if(currentPageFlag == "dt"){
		var t_id 	= 	document.getElementById("teacherId2").value;
		var e_id	=	document.getElementById("eligibilityId2").value;
		var j_id	=	document.getElementById("jobId2").value;
		if(pageno!='') {
			pageForDT=pageno;
		} else {
			pageForDT=1;
		}
		noOfRowsForDT = document.getElementById("pageSize").value;
		open_popup_dt(t_id,j_id,e_id,name2,sapStatus3);
	} else if(currentPageFlag == "ts"){
		var t_id 	= 	document.getElementById("teacherIdTrans").value;
		var e_id	=	document.getElementById("eligibilityIdTrans").value;
		var j_id	=	document.getElementById("jobIdTrans").value;
		if(pageno!='') {
			pageForTS=pageno;
		} else {
			pageForTS=1;
		}
		noOfRowsForTS = document.getElementById("pageSize").value;
		open_popup_trans(t_id,j_id,e_id,name3,sapStatus4);
	}else if(currentPageFlag == "frs"){
		var t_id 	= 	document.getElementById("teacherIdEEOC").value;
		var e_id	=	document.getElementById("eligibilityIdEEOC").value;
		var j_id	=	document.getElementById("jobIdEEOC").value;
		if(pageno!='') {
			pageForEEOC=pageno;
		} else {
			pageForEEOC=1;
		}
		noOfRowsForEEOC = document.getElementById("pageSize").value;
		openPopupFrsCode(t_id,j_id,e_id,name4,sapStatus5);
	}else {
		if(pageno!='') {
			pagePNR=pageno;
			document.getElementById("pageNumberSet").value	=	pageno;
		} else {
			pagePNR=1;
			document.getElementById("pageNumberSet").value	=	1;
		}
		noOfRowsPNR = document.getElementById("pageSize1").value;
		
		searchPanel();
	}
}

function getPagingAndSortingPnr(pageno,sortOrder,sortOrderType)
{
	if(pageno!=''){
		pagePNR=pageno;	
	}else{
		pagePNR=1;
	}
	sortOrderStrPNR	=	sortOrder;
	sortOrderTypePNR	=	sortOrderType;
	if(document.getElementById("pageSize1")!=null){
		noOfRowsPNR = document.getElementById("pageSize1").value;
	}else{
		noOfRowsPNR=10;
	}
	searchPanel();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*======= Save Panel on Press Enter Key ========= */

function chkForEnterDisplayPanel(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		searchPanel();
	}	
}

//For Additional Document
function getdistrictAssessmetGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getdistrictAssessmetGrid(noOfRowsASMT,pageASMT,sortOrderStrASMT,sortOrderTypeASMT,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAssessmentDetails').html(data);
			applyScrollOnTbl_AssessmentDetails();

		}});
}

function searchPanelButton() {
	document.getElementById("pageNumberSet").value=1;	
	defaultSet();
	divClose();
	searchPanel();
	
}
/* filter onr panel */
var ssn="";
var fname="";
var lname="";
var position="";
function searchPanel() {
	ssn		=	$("#ssn").val();
	fname	=	$("#firstName").val();
	lname	=	$("#lastName").val();
	position=	$("#position").val();	 // position is refer to jobrequition no

	var statusType=document.getElementsByName("statusType").value;
	if(statusType == null){
		$('#loadingDivHZS').show();
	} else {
		$('#loadingDiv').show();
	}
 
		ONRAjax.displayPNRGridOp(ssn,fname,lname,position,noOfRowsPNR,pagePNR,sortOrderStrPNR,sortOrderTypePNR,statusType,{
			async: true,
			callback: function(data){
			$('#panelGrid').html(data);
			applyScrollOnTblOnr($('#userRoleId').val());
			$('#loadingDiv').hide();
			$('#loadingDivHZS').hide();
		},
			errorHandler:handleError  
	});
	
}

/*========  displayPNRGrid ===============*/
function displayPNRGrid() {
	try{
		var statusType = document.getElementsByName("statusType").value;
	}catch(e){}
	ssn		=	$("#ssn").val();
	fname	=	$("#firstName").val();
	lname	=	$("#lastName").val();
	position=	$("#position").val();

	defaultSet();
	divClose();

	if(statusType == null){
		$('#loadingDivHZS').show();
	} else {
		$('#loadingDiv').show();
	}
	try{
		pagePNR	 =	document.getElementById("pageNumberSet").value;
	}catch(e){}
	 
		ONRAjax.displayPNRGridOp(ssn,fname,lname,position,noOfRowsPNR,pagePNR,sortOrderStrPNR,sortOrderTypePNR,statusType,{
			async: true,
			callback: function(data){
			$('#panelGrid').html(data);
			applyScrollOnTblOnr($('#userRoleId').val());
			$('#loadingDiv').hide();
			$('#loadingDivHZS').hide();
		},
			errorHandler:handleError  
	});
	  
}

/*========  Add New Panel===============*/

function defaultSet(){
	pagePNR = 1;
	sortOrderStrPNR="";
	sortOrderTypePNR="";
	currentPageFlag		=	"";
}

function open_popup(t_id,j_id,e_id,name,sapStatus){
	if(($('#sadistrictId').val()==1200390 || $('#sadistrictId').val()==806900) && $('#entityType').val()==3){
	}else{
	defaultSet();
	getOnrGrid(t_id,j_id,e_id,name,sapStatus);
	}
}

function getOnrGrid(t_id,j_id,e_id,name,sapStatus)
{
	$('#addStatusI9').empty();
	currentPageFlag='job';
	var eligibilityName = "";
	
	if(e_id == 1){
			eligibilityName = resourceJSON.msgI9 
	}else if(e_id == 2){
			eligibilityName = resourceJSON.msgW4 
	}else if(e_id == 6){
			eligibilityName = resourceJSON.MsgFRSForm 
	}else if(e_id == 7){
			eligibilityName = resourceJSON.msgDirectDeposit 
	}else if(e_id == 8){
			eligibilityName = resourceJSON.msgOathOfLoyalty 
	}else if(e_id == 17){
			eligibilityName = resourceJSON.msgNC4 
	}else if(e_id == 18){
			eligibilityName = resourceJSON.msgNCHC 
	}else if(e_id == 19){
			eligibilityName = resourceJSON.msgLEA 
	}else if(e_id == 20){
			eligibilityName = resourceJSON.msgNEF 
	}else if(e_id == 21){
			eligibilityName = resourceJSON.msgPERA 
	}else if(e_id == 22){
			eligibilityName = resourceJSON.msgSSF 
	}else if(e_id == 23){
			eligibilityName = resourceJSON.msgCAF 
	}else if(e_id == 24){
			eligibilityName = resourceJSON.msgMF 
	}else if(e_id == 25){
			eligibilityName = resourceJSON.msgCDE 
	}
	

	ONRAjax.PopUpGrid(t_id,j_id,e_id,noOfRowsForI9,pageForI9,sortOrderStr,sortOrderTypeForI9,currentPageFlag,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("teacherId").value		=	t_id;
		document.getElementById("eligibilityId").value	=	e_id;
		document.getElementById("jobId").value			=	j_id;
		
		document.getElementById("myModalLabelI9").innerHTML 	= 	eligibilityName+" "+resourceJSON.msgfor+" "+name;
		document.getElementById("myModalLabelI9Hidden").value	=	name;
		
		var sAddStatus=document.getElementById("sAddStatus").value;
		if(sAddStatus=="1")
		{
			if(sapStatus == 'N' || sapStatus == 'null'){
				document.getElementById("addStatusI9").innerHTML 	= resourceJSON.MsgAddStatus;
			}
		}
		
		
		document.getElementById("addStatusI9Hidden").value	=  sapStatus;
		
		$('#errordiv').hide();
		$('#notesmsg').find(".jqte_editor").css("background-color", "#FFFFFF");
		$('.onr_popup').modal('show');
		document.getElementById("divTranscriptI9").innerHTML=data;
		applyScrollOnTblI9();
	}});
}
function open_div(){
	document.getElementById("add_action").style.display="block";
	document.getElementById("status1").checked = false;
	document.getElementById("status2").checked = false;
	document.getElementById("status3").checked = false;
	$('#notesmsg').find(".jqte_editor").html("");
	$("#eligibilyFile").val('');
}

function open_popup_fp(t_id,j_id,e_id,name,sapStatus){
	if(($('#sadistrictId').val()==1200390 || $('#sadistrictId').val()==806900) && $('#entityType').val()==3){
	}else{
	defaultSet();
	getOnrGridFP(t_id,j_id,e_id,name,sapStatus);
	}
}

function getOnrGridFP(t_id,j_id,e_id,name,sapStatus) {
	$('#addStatusFP').empty();
	currentPageFlag='fp';
	
	ONRAjax.PopUpGridFP(t_id,j_id,e_id,noOfRowsForFP,pageForFP,sortOrderStr,sortOrderTypeForFP,currentPageFlag,{	
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("teacherId1").value		=	t_id;
		document.getElementById("eligibilityId1").value	=	e_id;
		document.getElementById("jobId1").value			=	j_id;
		document.getElementById("myModalLabelFp").innerHTML = ""+resourceJSON.MsgFingerPrint+" "+name;
		document.getElementById("myModalLabelFpHidden").value	=	name;
		
		var sAddStatus=document.getElementById("sAddStatus").value;
		if(sAddStatus=="1")
		{
			if(sapStatus == 'N' || sapStatus == 'null'){
				document.getElementById("addStatusFP").innerHTML 	= ""+resourceJSON.MsgAddStatus+"";
			}
		}
		document.getElementById("addStatusFPHidden").value	=  sapStatus;
		$('.onr_popup_fp').modal('show');
		document.getElementById("divFP").innerHTML=data;
		applyScrollOnTblFP();
	}});
}

function open_content(e_id,eligibilityId,userName){
	$('.onr_popup').modal('hide');
	defaultSet();
	currentPageFlag='job';
	getOnrGridNotes(e_id,eligibilityId,userName);
}

function getOnrGridNotes(e_id,eligibilityId,userName)
{
	
	var eligibilityName = "";
	if(e_id == 1){ 
			eligibilityName = resourceJSON.msgI9 
	}else if(e_id == 2){ 
			eligibilityName = resourceJSON.msgW4 
	}else if(e_id == 6){ 
			eligibilityName = resourceJSON.MsgFRSForm 
	}else if(e_id == 7){ 
			eligibilityName = resourceJSON.msgDirectDeposit 
	}else if(e_id == 8){ 
			eligibilityName = resourceJSON.msgOathOfLoyalty 
	}
	
	ONRAjax.openNotesById(eligibilityId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("UserProfileNameI9").innerHTML = eligibilityName+" "+resourceJSON.msgfor+" "+ userName;
		$('.onrNotesShow').modal('show');
		document.getElementById("divOnrNotes").innerHTML=data;
	}});
}
/* FP Content DIV Open */
function open_content_fp(eligibilityId,userName){
	$('.onr_popup_fp').modal('hide');
	defaultSet();
	currentPageFlag='fp';
	getOnrGridNotesFP(eligibilityId,userName);
}

function getOnrGridNotesFP(eligibilityId,userName)
{
	ONRAjax.openNotesByIdFP(eligibilityId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("UserProfileNameFP").innerHTML = ""+resourceJSON.MsgFingerPrint+" "+ userName;
		$('.onrNotesShowFP').modal('show');
		document.getElementById("divOnrNotesFP").innerHTML=data;
	}});
}

/* DT Content DIV Open */
function open_content_dt(eligibilityId,userName){
	$('.onr_popup_dt').modal('hide');
	defaultSet();
	currentPageFlag='dt';
	getOnrGridNotesDT(eligibilityId,userName);
}

function getOnrGridNotesDT(eligibilityId,userName)
{
	ONRAjax.openNotesByIdDT(eligibilityId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("UserProfileNameDT").innerHTML = ""+resourceJSON.MsgDrugTestfor+" "+ userName;
		$('.onrNotesShowDT').modal('show');
		document.getElementById("divOnrNotesDT").innerHTML=data;
	}});
}

/* Transcript Content DIV Open */
function open_content_trans(eligibilityId,userName){
	$('.onr_popup_trans').modal('hide');
	defaultSet();
	currentPageFlag='tr';
	getOnrGridNotesTrans(eligibilityId,userName);
}

function getOnrGridNotesTrans(eligibilityId,userName)
{	
	ONRAjax.openNotesByIdTrans(eligibilityId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("UserProfileNameTrans").innerHTML = ""+resourceJSON.MsgTranscriptfor+" "+ userName;
		$('.onrNotesShowTrans').modal('show');
		document.getElementById("divOnrNotesTrans").innerHTML=data;
	}});
}

function open_div_fp(){
	document.getElementById("add_action_fp").style.display="block";
	document.getElementById("status4").checked = false;
	document.getElementById("status5").checked = false;
	document.getElementById("status6").checked = false;
	document.getElementById("status7").checked = false;
	document.getElementById("examStartDateFp").value = "";
	document.getElementById("examClearDateFp").value = "";
	document.getElementById("examStartDateFp1").value = "";
	document.getElementById("failedDateFp").value = "";
	document.getElementById("examStartDateFp2").value = "";
	document.getElementById("reprintDateFp").value = "";
	$('#notesmsgFp').find(".jqte_editor").html("");
	$("#eligibilyFileFp").val('');
}

function fpClearDate(){
	$('#FailedDateDiv').hide();
	$('#ReprintDateDiv').hide();
	document.getElementById("ExaminationDateDiv").style.display="block";
}

function dtClearDate(){
	$('#FailedDateDivDT').hide();
	document.getElementById("ExaminationDateDivDT").style.display="block";
}

function dtFailedDate(){
	document.getElementById("FailedDateDivDT").style.display="block";
	
}

function fpReprintDate(){
	$('#FailedDateDiv').hide();
	$('#ExaminationDateDiv').hide();
	document.getElementById("ReprintDateDiv").style.display="block";
}

function fpPrintedDate(){
	$('#FailedDateDiv').hide();
	$('#ExaminationDateDiv').hide();
	document.getElementById("ReprintDateDiv").style.display="none";
}

function fpFailedDate(){
	$('#ReprintDateDiv').hide();
	$('#ExaminationDateDiv').hide();
	document.getElementById("FailedDateDiv").style.display="block";
}

/*	Drug Test (DT) Functions */
function open_popup_dt(t_id,j_id,e_id,name,sapStatus){
	if(($('#sadistrictId').val()==1200390 || $('#sadistrictId').val()==806900) && $('#entityType').val()==3){
	}else{
	defaultSet();
	getOnrGridDT(t_id,j_id,e_id,name,sapStatus);
	}
}

function getOnrGridDT(t_id,j_id,e_id,name,sapStatus)
{	
	$('#addStatusDT').empty();
	currentPageFlag='dt';
	ONRAjax.PopUpGridDT(t_id,j_id,e_id,noOfRowsForDT,pageForDT,sortOrderStrForDT,sortOrderTypeForDT,currentPageFlag,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		document.getElementById("teacherId2").value		=	t_id;
		document.getElementById("eligibilityId2").value	=	e_id;
		document.getElementById("jobId2").value			=	j_id;
		if($("#sadistrictId").val()==806900 || $("#districtId").val()==806900)
			document.getElementById("myModalLabelDt").innerHTML = ""+resourceJSON.msgBackGroundCheck+" "+name;
		else
			document.getElementById("myModalLabelDt").innerHTML = ""+resourceJSON.MsgDrugTestfor+" "+name;
		document.getElementById("myModalLabelDtHidden").value	=	name;
		
		var sAddStatus=document.getElementById("sAddStatus").value;
		if(sAddStatus=="1")
		{
			if(sapStatus == 'N' || sapStatus == 'null'){
				document.getElementById("addStatusDT").innerHTML 	= resourceJSON.MsgAddStatus;
			}
		}
		
		
		document.getElementById("addStatusDTHidden").value	=  sapStatus;
		$('.onr_popup_dt').modal('show');
		document.getElementById("divDT").innerHTML=data;
		applyScrollOnTblDT();
	}});
}

function open_div_dt(){
	document.getElementById("add_action_dt").style.display="block";
	document.getElementById("status8").checked = false;
	document.getElementById("status9").checked = false;
	document.getElementById("examStartDateDt").value = "";
	$('#notesmsgDt').find(".jqte_editor").html("");
	$("#eligibilyFileDt").val('');
}

function saveNotesPnr(){
	$('#errordiv').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var charCount	=	$('#notesmsg').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var status 		= 	$("input[name=status]:radio:checked").val();
	var notes		=	$('#notesmsg').find(".jqte_editor").html();
	var fileName 	= 	document.getElementById("eligibilyFile").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;

	var status1 	= 	document.getElementById("status1");
	var status2 	= 	document.getElementById("status2");
	var status3 	= 	document.getElementById("status3");
	var teacherId	=	document.getElementById("teacherId").value;
	var eligibilityId	=	document.getElementById("eligibilityId").value;
	var jobId			=	document.getElementById("jobId").value;
	
	
	var StatusMaster	=	document.getElementById("StatusMaster").value;
	var split			=	StatusMaster.split("#$#");
	
	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(eligibilyFile.files[0]!=undefined)
		fileSize = eligibilyFile.files[0].size;
	}
	
	if(status1.checked == false && status2.checked == false && status3.checked == false){
		if(focus == 0){
			$('#errordiv').append("&#149; "+resourceJSON.PlzSelectStatus+"<br>");
			$('#errordiv').show();
			counter++;
			focus++;
		}
	}
	if((status1.checked == true && split[1] == 1) || (status2.checked == true && split[2] == 1) || (status3.checked == true && split[3] == 1)){
		if ($('#notesmsg').find(".jqte_editor").text().trim()==""){
			if(focus == 0){
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
				$('#notesmsg').find(".jqte_editor").focus();
				$('#notesmsg').find(".jqte_editor").css("background-color", "#F5E7E1");
				$('#errordiv').show();
				counter++;
				focus++;
			}
		}
  }
		if(fileSize>=10485760)
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
					if(focus==0)
						$('#eligibilyFile').focus();
					
					$('#eligibilyFile').css("background-color","#F5E7E1");
					counter++;
					focs++;	
					return false;
			}
			if(fileName==""){
				fileName= document.getElementById("eligibilyFile").value;
			}	
			
			if(counter == 0){
				if(fileName=="" && jsiFileName!=""){
					fileName = jsiFileName;
				}
				if(fileName!="" && fileName!=jsiFileName){
					document.getElementById("frmonrI9Upload").submit();
				} else {
					
						ONRAjax.saveNotes(status,notes,teacherId,eligibilityId,jobId,"",{
						async: true,
						errorHandler:handleError,
						callback:function(data)
						{
							displayPNRGrid();
							$('#onr_popup').modal('hide');
							$('#add_action').hide();
							
						}});
						
		  }
	}
}
function saveNotesUpload(fileName){
	$('#errordiv').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var charCount	=	$('#notesmsg').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var status 		= 	$("input[name=status]:radio:checked").val();
	var notes		=	$('#notesmsg').find(".jqte_editor").html();
	var teacherId	=	document.getElementById("teacherId").value
	var eligibilityId	=	document.getElementById("eligibilityId").value
	var jobId			=	document.getElementById("jobId").value
	
	ONRAjax.saveNotes(status,notes,teacherId,eligibilityId,jobId,fileName,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			displayPNRGrid();
			$('#onr_popup').modal('hide');
			$('#add_action').hide();
		}});
}

function saveNotesFP(){

	$('#errordiv1').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var charCount	=	$('#notesmsg').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var status 		= 	$("input[name=statusFP]:radio:checked").val();
	var notes		=	$('#notesmsgFp').find(".jqte_editor").html();
	var fileName 	= 	document.getElementById("eligibilyFileFp").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;
	var status4 	= 	document.getElementById("status4");
	var status5 	= 	document.getElementById("status5");
	var status6 	= 	document.getElementById("status6");
	var status7 	= 	document.getElementById("status7");
	var statusP 	= 	document.getElementById("statusP");
	
	var teacherId1		=	document.getElementById("teacherId1").value
	var eligibilityId1	=	document.getElementById("eligibilityId1").value
	var jobId1			=	document.getElementById("jobId1").value
	
	var examStartDateFp	=	document.getElementById("examStartDateFp").value
	var examStartDateFp1=	document.getElementById("examStartDateFp1").value
	var examStartDateFp2=	document.getElementById("examStartDateFp2").value
	var examClearDateFp	=	document.getElementById("examClearDateFp").value
	var failedDateFp	=	document.getElementById("failedDateFp").value
	var reprintDateFp	=	document.getElementById("reprintDateFp").value
	
	var StatusMaster1	=	document.getElementById("StatusMaster1").value
	var split			=	StatusMaster1.split("#$#");
	
	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(eligibilyFileFp.files[0]!=undefined)
		fileSize = eligibilyFileFp.files[0].size;
	}
	
	if(status4.checked == false && status5.checked == false && status6.checked == false && status7.checked == false && statusP.checked == false){
		if(focus == 0){
			$('#errordiv1').append("&#149; "+resourceJSON.PlzSelectStatus+"<br>");
			$('#errordiv1').show();
			counter++;
			focus++;
		}
	}
	if(status4.checked == true){
		if(examStartDateFp == ""){
			if(focus == 0){
				$('#errordiv1').append("&#149; "+resourceJSON.PlzSlctExamDate+"<br>");
				$('#errordiv1').show();
				counter++;
				focus++;
			}
		}
		
		if(examClearDateFp == ""){
			if(focus == 0){
				$('#errordiv1').append("&#149; "+resourceJSON.PlzSlctClearDate+"<br>");
				$('#errordiv1').show();
				counter++;
				focus++;
			}
		}
		
	}

	if(status5.checked == true){
		if(examStartDateFp1 == ""){
			if(focus == 0){
				$('#errordiv1').append("&#149; "+resourceJSON.PlzSlctExamDate+"..<br>");
				$('#errordiv1').show();
				counter++;
				focus++;
			}
		}
		
		if(failedDateFp == ""){
			if(focus == 0){
				$('#errordiv1').append("&#149; "+resourceJSON.PlzSlctFaileddate+"<br>");
				$('#errordiv1').show();
				counter++;
				focus++;
			}
		}
	}

	if(status7.checked == true){
		if(examStartDateFp2 == ""){
			if(focus == 0){
				$('#errordiv1').append("&#149; "+resourceJSON.PlzSlctExamDate+".<br>");
				$('#errordiv1').show();
				counter++;
				focus++;
			}
		}
		
		if(reprintDateFp == ""){
			if(focus == 0){
				$('#errordiv1').append("&#149; "+resourceJSON.PlzSlctReprintDate+"<br>");
				$('#errordiv1').show();
				counter++;
				focus++;
			}
		}
		
	}
if((status4.checked == true && split[1] == 1) || (status5.checked == true && split[2] == 1) || (status6.checked == true && split[3] == 1) || (status7.checked == true && split[4] == 1) || (statusP.checked == true && split[5] == 1)){
	if ($('#notesmsgFp').find(".jqte_editor").text().trim()==""){
		if(focus == 0){
			$('#errordiv1').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
			$('#notesmsgFp').find(".jqte_editor").focus();
			$('#notesmsgFp').find(".jqte_editor").css("background-color", "#F5E7E1");
			$('#errordiv1').show();
			counter++;
			focus++;
		}
	}
}

	if(fileSize>=10485760)
			{
				$('#errordiv1').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
					if(focs==0)
						$('#eligibilyFileFp').focus();
					
					$('#eligibilyFileFp').css("background-color",txtBgColor);
					counter++;
					focs++;	
					return false;
			}
			if(fileName==""){
				fileName= document.getElementById("eligibilyFileFp").value;
			}	
			
			if(counter == 0){
				if(fileName=="" && jsiFileNameFp!=""){
					fileName = jsiFileNameFp;
				}
				if(fileName!="" && fileName!=jsiFileNameFp){
					document.getElementById("frmoFpUpload").submit();
				} else {
			    ONRAjax.saveNotesFP(status,notes,teacherId1,eligibilityId1,jobId1,examStartDateFp,examClearDateFp,failedDateFp,reprintDateFp,examStartDateFp1,examStartDateFp2,"",{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					displayPNRGrid();
					$('#onr_popup1').modal('hide');
					$('#add_action_fp').hide();
				}});
					
		}
	}
}

function saveNotesFPUpload(fileName){
	
	$('#errordiv1').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var charCount	=	$('#notesmsg').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var status 		= 	$("input[name=statusFP]:radio:checked").val();
	var notes		=	$('#notesmsgFp').find(".jqte_editor").html();
	
	var teacherId1		=	document.getElementById("teacherId1").value
	var eligibilityId1	=	document.getElementById("eligibilityId1").value
	var jobId1			=	document.getElementById("jobId1").value
	
	var examStartDateFp	=	document.getElementById("examStartDateFp").value
	var examStartDateFp1=	document.getElementById("examStartDateFp1").value
	var examStartDateFp2=	document.getElementById("examStartDateFp2").value
	var examClearDateFp	=	document.getElementById("examClearDateFp").value
	var failedDateFp	=	document.getElementById("failedDateFp").value
	var reprintDateFp	=	document.getElementById("reprintDateFp").value
	
	ONRAjax.saveNotesFP(status,notes,teacherId1,eligibilityId1,jobId1,examStartDateFp,examClearDateFp,failedDateFp,reprintDateFp,examStartDateFp1,examStartDateFp2,fileName,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			displayPNRGrid();
			$('#onr_popup1').modal('hide');
			$('#add_action_fp').hide();
		}});
}

function saveNotesDT(){
	$('#errordiv3').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var charCount	=	$('#notesmsgDt').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var status 		= 	$("input[name=statusDT]:radio:checked").val();
	var notes		=	$('#notesmsgDt').find(".jqte_editor").html();
	var fileName 	= 	document.getElementById("eligibilyFileDt").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;

	var status8 		= 	document.getElementById("status8");
	var status9 		= 	document.getElementById("status9");
	var status10 		= 	document.getElementById("status10");
	var status11        =  	document.getElementById("status11");
	var teacherId2		=	document.getElementById("teacherId2").value
	var eligibilityId2	=	document.getElementById("eligibilityId2").value
	var jobId2			=	document.getElementById("jobId2").value
	var examStartDateDt	=	document.getElementById("examStartDateDt").value
	
	var StatusMaster2	=	document.getElementById("StatusMaster2").value;
	var split			=	StatusMaster2.split("#$#");
	
	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(eligibilyFileDt.files[0]!=undefined)
		fileSize = eligibilyFileDt.files[0].size;
	}
	if($("#districtId").val()==806900){
		if(status8.checked == false && status9.checked == false && status10.checked == false && status11.checked == false){
			if(focus == 0){
				$('#errordiv3').append("&#149; "+resourceJSON.PlzSelectStatus+"<br>");
				$('#errordiv3').show();
				counter++;
				focus++;
			}
		}
	}else{
		if(status8.checked == false && status9.checked == false && status10.checked == false){
			if(focus == 0){
				$('#errordiv3').append("&#149; "+resourceJSON.PlzSelectStatus+"<br>");
				$('#errordiv3').show();
				counter++;
				focus++;
			}
		}
	}
	if(status8.checked == true || status9.checked == true || status10.checked == true){
		if(examStartDateDt == ""){
			if(focus == 0){
				$('#errordiv3').append("&#149; "+resourceJSON.PlzSlctExamDate+"<br>");
				$('#errordiv3').show();
				counter++;
				focus++;
			}
		}
	}
if((status8.checked == true && split[1] == 1) || (status9.checked == true && split[2] == 1) || (status10.checked == true && split[3] == 1)){	
	if ($('#notesmsgDt').find(".jqte_editor").text().trim()==""){
		if(focus == 0){
			$('#errordiv3').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
			$('#notesmsgDt').find(".jqte_editor").focus();
			$('#notesmsgDt').find(".jqte_editor").css("background-color", "#F5E7E1");
			$('#errordiv3').show();
			counter++;
			focus++;
		}
	 }
  }
		if(fileSize >= 10485760) {
				$('#errordiv3').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
					if(focs==0)
						$('#eligibilyFile').focus();

					$('#eligibilyFileDt').css("background-color",txtBgColor);
					counter++;
					focs++;
					return false;
			}
			if(fileName==""){
				fileName= document.getElementById("eligibilyFileDt").value;
			}

	if(counter == 0){
		
		if(fileName=="" && jsiFileNameDt!=""){
			fileName = jsiFileNameDt;
		}
		if(fileName!="" && fileName!=jsiFileNameDt){
			document.getElementById("frmDtUpload").submit();
		} else {
		
			ONRAjax.saveNotesDT(status,notes,teacherId2,eligibilityId2,jobId2,examStartDateDt,"",{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					displayPNRGrid();
					 $('#onr_popup2').modal('hide');
					 $('#add_action_dt').hide();
				}});
	     }
	  }

}

function saveNotesDTUpload(fileName){
	$('#errordiv3').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var charCount	=	$('#notesmsgDt').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var status 		= 	$("input[name=statusDT]:radio:checked").val();
	var notes		=	$('#notesmsgDt').find(".jqte_editor").html();
	var teacherId2		=	document.getElementById("teacherId2").value
	var eligibilityId2	=	document.getElementById("eligibilityId2").value
	var jobId2			=	document.getElementById("jobId2").value
	var examStartDateDt	=	document.getElementById("examStartDateDt").value
	
	ONRAjax.saveNotesDT(status,notes,teacherId2,eligibilityId2,jobId2,examStartDateDt,fileName,{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				displayPNRGrid();
				$('#onr_popup2').modal('hide');
				$('#add_action_dt').hide();
			}});
 	}


function fpCloseDate() {
	$('#FailedDateDiv').hide();
	$('#ExaminationDateDiv').hide();
	$('#ReprintDateDiv').hide();
}

function dtCloseDate() {
	$('#ExaminationDateDivDT').hide();
}

function cancelDiv() { 
	$('#notesmsg').find(".jqte_editor").text() 		== 	""
	$('#notesmsgDt').find(".jqte_editor").text()	==	""
	$('#notesmsgFp').find(".jqte_editor").text()	==	""
	$('#notesmsgEEOC').find(".jqte_editor").text()	==	""
	$('#add_action').hide();
	$('#add_action_fp').hide();
	$('#add_action_dt').hide();
	$('#errordiv').hide();
	$('#add_action_academic').hide();
	$('#EEOCDivOpen').hide();
	
}

function hideDiv(){
	defaultSet();
	divClose();
	$('#add_action_academic').hide();
	$('#add_action').hide();
	$('#add_action_fp').hide();
	$('#add_action_dt').hide();
	return false;
}
function hideDivTop(){
	$('.onr_popup').modal('show');
}

function hideDivFP(){
	$('.onr_popup_fp').modal('show');
}

function hideDivDT(){
	$('.onr_popup_dt').modal('show');
}
function hideDivTrans(){
	$('.onr_popup_trans').modal('show');
}
function hideDivEEOC(){
	$('.onr_popup_frsCode').modal('show');
}
/*	Transcript Functions */
function open_popup_trans(t_id,j_id,e_id,name,sapStatus){
	if(($('#sadistrictId').val()==1200390 || $('#sadistrictId').val()==806900) && $('#entityType').val()==3){
	}else{
	defaultSet();
	getOnrGridTrans(t_id,j_id,e_id,name,sapStatus);
	}
}

function getOnrGridTrans(t_id,j_id,e_id,name,sapStatus)
{

	$('#addStatusTrans').empty();
	currentPageFlag='ts';
	ONRAjax.PopUpGridTrans(t_id,j_id,e_id,noOfRowsForTS,pageForTS,sortOrderStr,sortOrderTypeForTS,currentPageFlag,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		document.getElementById("teacherIdTrans").value		=	t_id;
		document.getElementById("eligibilityIdTrans").value	=	e_id;
		document.getElementById("jobIdTrans").value			=	j_id;
		document.getElementById("myModalLabelTrans").innerHTML = ""+resourceJSON.MsgTranscriptfor+" "+name;
		document.getElementById("myModalLabelTransHidden").value	=	name;
		
		var sAddStatus=document.getElementById("sAddStatus").value;
		if(sAddStatus=="1")
		{
			if(sapStatus == 'N' || sapStatus == 'null'){
			}				document.getElementById("addStatusTrans").innerHTML 	= resourceJSON.MsgAddStatus;

		}
		
		document.getElementById("addStatusTransHidden").value	=  sapStatus;
		$('.onr_popup_trans').modal('show');
		document.getElementById("divTrans").innerHTML=data;
		applyScrollOnTblTrans();
	}});
}

function open_div_trans() {
	var t_id 	= 	document.getElementById("teacherIdTrans").value;
	var e_id	=	document.getElementById("eligibilityIdTrans").value;
	var j_id	=	document.getElementById("jobIdTrans").value;
	document.getElementById("add_action_academic").style.display="block";
	$('#errordivTrans').empty();
	defaultSet();
	TransDivOpen(t_id,j_id,e_id);
}

function TransDivOpen(t_id,j_id,e_id)
{	
	currentPageFlag='ts';
	ONRAjax.TransDivOpen(t_id,j_id,e_id,noOfRowsForTS,pageForTS,sortOrderStrForTS,sortOrderTypeForTS,currentPageFlag,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		document.getElementById("divTransAcademic").innerHTML=data;
		applyScrollOnTblTransAcademic();
	}});
}

var dtCh= "-";
var minYear=1951;
var d = new Date();
var maxYear=d.getFullYear();

function saveTransAcademic(id){
	
	$('#errordivTrans').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var AcademicIDs	=	"";
	var charCount	=	$('#notesTrans').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var status 		= 	id;
	var notes		=	$('#notesTrans').find(".jqte_editor").html();
	var fileName 	= 	document.getElementById("eligibilyFileTrans").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;
	document.getElementById("formId").value = id;
	var teacherIdTrans		=	document.getElementById("teacherIdTrans").value
	var eligibilityIdTrans	=	document.getElementById("eligibilityIdTrans").value
	var jobIdTrans			=	document.getElementById("jobIdTrans").value
	
	var StatusMaster3		=	document.getElementById("StatusMaster3").value
	var split				=	StatusMaster3.split("#$#");
	var graduationDate		=	"";
	
	var transcript_data	=	document.getElementsByName("transcript_data[]");
	var counter;
	for(var i=0; i < transcript_data.length; i++){
		
		if(transcript_data[i].checked == true){
			graduationDate	=	document.getElementById("graduationDate_"+transcript_data[i].value).value;
			AcademicIDs	 =	AcademicIDs+"##"+transcript_data[i].value;
			if(id == 22 && (graduationDate == "" || graduationDate == null)){
				if(counter == 0){
					$('#errordivTrans').append("&#149; "+resourceJSON.msgDegreeConferredDate+"<br>");
					$("#graduationDate_"+transcript_data[i].value).focus();
					$('#errordiv3').show();
				}
				
				counter++;
				focus++;
			}else if(id == 22 && (graduationDate != "" || graduationDate != null)){
				if(counter == 0){
					saveGraduationDate(transcript_data[i].value);
				}
			}
		}
	}

	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(eligibilyFileTrans.files[0]!=undefined)
		fileSize = eligibilyFileTrans.files[0].size;
	}
	if((id == 22 && split[1] == 1) || (id == 23 && split[2] == 1) || (id == 24 && split[3] == 1)){
		if ($('#notesTrans').find(".jqte_editor").text().trim()==""){
			if(focus == 0){
				$('#errordivTrans').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
				$('#notesTrans').find(".jqte_editor").focus();
				$('#notesTrans').find(".jqte_editor").css("background-color", "#F5E7E1");
				$('#errordiv3').show();
				counter++;
				focus++;
			}
		 }
	 }
		if(fileSize >= 10485760) {
			$('#errordivTrans').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
				if(focs==0)
					$('#eligibilyFile').focus();

				$('#eligibilyFileTrans').css("background-color",txtBgColor);
				counter++;
				focs++;
				return false;
		}
		if(fileName==""){
			fileName= document.getElementById("eligibilyFileTrans").value;
		}

	if(counter == 0){
		if(fileName=="" && jsiFileNameTrans!=""){
			fileName = jsiFileNameTrans;
		}
		if(fileName!="" && fileName!=jsiFileNameTrans){ 
			document.getElementById("frmTransUpload").submit();
		} else {
		
				ONRAjax.saveNotesTrans(status,notes,teacherIdTrans,eligibilityIdTrans,jobIdTrans,AcademicIDs,"",{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					displayPNRGrid();
					$('#onr_popup_trans').modal('hide');
					$('#add_action_academic').hide();
				}});
	    }
	}
}

function saveTransAcademicUpload(id,fileName){
	$('#errordivTrans').empty();
	var focus 		= 	0;
	var counter		= 	0;
	var AcademicIDs	=	"";
	var charCount	=	$('#notesTrans').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var status 		= 	id;
	var notes		=	$('#notesTrans').find(".jqte_editor").html();
	
	var teacherIdTrans		=	document.getElementById("teacherIdTrans").value
	var eligibilityIdTrans	=	document.getElementById("eligibilityIdTrans").value
	var jobIdTrans			=	document.getElementById("jobIdTrans").value
	
	var transcript_data	=	document.getElementsByName("transcript_data[]");
	for(var i=0; i < transcript_data.length; i++){
		if(transcript_data[i].checked == true){
			AcademicIDs	 =	AcademicIDs+"##"+transcript_data[i].value;
		}
	}
	
	if(counter == 0){
		ONRAjax.saveNotesTrans(status,notes,teacherIdTrans,eligibilityIdTrans,jobIdTrans,AcademicIDs,fileName,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					displayPNRGrid();
					$('#onr_popup_trans').modal('hide');
					$('#add_action_academic').hide();
				}});
	}
}

function showAcademicQues(){
	var counter = 0;
	var transcript_data	=	document.getElementsByName("transcript_data[]");
	for(var i=0; i < transcript_data.length; i++){
		if(transcript_data[i].checked == true){			
			counter++;
		}
	}

	if(counter > 0){ 
		$('textarea').jqte();
		document.getElementById("academicGridAcademicNotes").style.display="block";
	} else {
		$('#academicGridAcademicNotes').hide();
	}
}


function sendToSAPdb()
{
	var savecandidatearray="";
	var inputs = document.getElementsByName("onr_sheet"); 
	var splitArray;
	var inputValue;
	var checkCnt		=	0;
	var checkCntSap		=	0;
	var checkSendToSap 	=	0;
	var totalOs		=	document.getElementsByName("totalos");

	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		inputValue 	= 	inputs[i].value;
	        		splitArray	=	inputValue.split(",");
	        		if($("#districtId").val()==806900){
	        			splitArray[2] = 9;
	        		}
	        		
	        		if(splitArray[2] < 9 ){
	        			checkSendToSap = checkSendToSap+1;
	        		} else if(splitArray[1] == "N" || splitArray[1] == "null"){
	        			savecandidatearray+=	splitArray[0]+",";
	        		} else {
	        			checkCntSap++;
	        		}
	        		checkCnt++;
	            }
	        }
	 }

	 if(checkCnt==0) {
			$('#message2showConfirm').html(""+resourceJSON.PlzSlctAtleastOneCandidate+"");
			$('#myModal3').modal('show');
			return;
	 }
	 
	 if(checkSendToSap > 0) {
			$('#message2showConfirm').html(""+resourceJSON.MsgOverAllStatus+"");
			$('#myModal3').modal('show');
			return;
	 }
	 
	 if(checkCntSap > 0) {
			$('#message2showConfirm').html(resourceJSON.MsgplzSlctNewRecord);
			$('#myModal3').modal('show');
			return;
	 }
	 
	 var splitArray;
	 var splitArrayError;
	 var splitArray1;
	 var erroMessage = resourceJSON.MsgOneOrMoreSelectedCandidate;
	 var errorMessage1="";
	 var errorMessageMain="";
	 ONRAjax.sendToSAPdb(savecandidatearray,{
		async: true,
		callback: function(data){
		 splitArray			=	data.split("$%#%$");
		 if(splitArray[1]>1){
			 splitArrayError	=	splitArray[0].split("@!@");
			 for(i=0; i<splitArrayError.length; i++){
				 splitArray1 	= 	splitArrayError[i].split("$#$");
				 errorMessage1	=	errorMessage1+(i+1)+". "+splitArray1[0]+"<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+splitArray1[1]+"<BR>";
			 }
		 } else {
			 splitArray1 	= 	splitArray[0].split("$#$");
			 errorMessage1	=	errorMessage1+(1)+". "+splitArray1[0]+"<BR>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"+splitArray1[1]+"<BR>";
		 }

		 errorMessageMain = erroMessage+"<BR><BR>"+errorMessage1;
		if(splitArray[1]=="0") {
			 if($("#districtId").val()==806900 || $("#sadistrictId").val()==806900){
				 $('#message2showConfirm').html(resourceJSON.MsgInformationSentToPeopleSoft);
			 }else{
				 $('#message2showConfirm').html(resourceJSON.MsgInformationSentToSAP);
			 }
			$('#myModal3').modal('show');
			displayPNRGrid();
		} else {
			$('#message2showConfirm').html(errorMessageMain);
			$('#myModal3').modal('show');
		}
	},
	errorHandler:handleError  
	});
}
function rejectJobForTeacher(){

	var removeArray="";
	var inputs = document.getElementsByName("onr_sheet"); 
	var splitArray;
	var inputValue;
	var checkCntSap = 	0;
	var checkCnt	=	0;

	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		inputValue 	= 	inputs[i].value;
	        		splitArray	=	inputValue.split(",");
	        		if(splitArray[1] == "N" || splitArray[1] == "null"){
	        			removeArray+=	splitArray[0]+",";
	        		} else {
	        			checkCntSap++;
	        		}
		        		checkCnt++;
	        	 }
	        }
	 	}
	 
	 if(checkCnt==0)
	 {
			$('#message2showConfirm').html(resourceJSON.PlzSlctCandidateToReject);
			$('#myModal3').modal('show');
			return;
	 }
	 
	 if(checkCntSap > 0) {
			$('#message2showConfirm').html(resourceJSON.PlzSlctNewRecordOnly);
			$('#myModal3').modal('show');
			return;
	 }
	 
	 ONRAjax.rejectJobForTeacher(removeArray,{ 
		async: true,
		callback: function(data){
		
		if(data=="0")
		{
			$('#message2showConfirm').html(resourceJSON.MsgRejectSuccessfully);
			$('#myModal3').modal('show');
			displayPNRGrid();
		}else{
			$('#message2showConfirm').html(resourceJSON.MsgInformationMissing);
			$('#myModal3').modal('show');
			displayPNRGrid();
		}
		
	},
	errorHandler:handleError  
	});
}

/* Unhired Funcyionality Implemented */

/*function unhireSap(){

	var unhireArray="";
	var inputs = document.getElementsByName("onr_sheet");
	var splitArray;
	var inputValue;
	var checkCnt=0;
	var checkCntSap=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		inputValue 	= 	inputs[i].value;
	        		splitArray	=	inputValue.split(",");
	        		if(splitArray[1] == "N" || splitArray[1] == "null"){
	        			checkCntSap++;
	        		} else {
	        			unhireArray+=	splitArray[0]+",";
	        		}
	        		checkCnt++;
	            }
	        }
	    }
	 
	 if(checkCnt==0) {
			$('#message2showConfirm').html("Please select atleast one candidate to unhire.");
			$('#myModal3').modal('show');
			return;
	 }
	 
	 if(checkCntSap > 0) {
			$('#message2showConfirm').html("We can not unhire new candidate.");
			$('#myModal3').modal('show');
			return;
	 }
	 ONRAjax.unhireJobForTeacher(unhireArray,{ 
		async: true,
		callback: function(data){
		
		if(data=="0")
		{
			$('#message2showConfirm').html("candidate(s) unhired successfully.");
			$('#myModal3').modal('show');
			displayPNRGrid();
		}
		
	},
	errorHandler:handleError  
	});
}*/

function getStatusType(status){
	var ssn		=	"";
	var fname	=	"";
	var lname	=	"";
	var position=	"";
	$("#ssn").val("");
	$("#firstName").val("");
	$("#lastName").val("");
	$("#position").val("");
	document.getElementById("pageNumberSet").value=1;
	document.getElementsByName("statusType").value=status;
	displayPNRGrid();
}

function saveGraduationDate(id){
	var graduationDate	=	document.getElementById("graduationDate_"+id).value;
	
	if (isDate(graduationDate)==false){
		dt.focus()
		return false
	}
	
	ONRAjax.saveGraduationDate(id,graduationDate,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('.onrNotesShowTrans').modal('hide');
		}});
}

function canleGraduationDate(id){
	var graduationDate	=	document.getElementById("graduationDate_"+id).value;
	document.getElementById("graduationDate_"+id).value = "";
	if(graduationDate != null || graduationDate != ""){
	ONRAjax.canleGraduationDate(id,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('.onrNotesShowTrans').modal('hide');
		}});
	}
}

/*	Save FRS Code Implemented By Hanzala Subhani Dated : 13-06-2014 */
function saveFrsCode(teacherId){
	var frsCode	= document.getElementById("frsCode_"+teacherId).value;
	ONRAjax.saveFrsCode(teacherId,frsCode,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			displayPNRGrid();
			$('#onr_popup').modal('hide');
			$('#add_action').hide();
		}});
}

/*	
 * Purpose		:	For FRS Code( Retirement Code.)
 * By			:	Hanzala Subhani
 * Dated		:	18 July 2014
 */

/*	FRS Code Functions */
function openPopupFrsCode(t_id, j_id, e_id, name, sapStatus) {
	if(($('#sadistrictId').val()==1200390 || $('#sadistrictId').val()==806900) && $('#entityType').val()==3){
	}else{
	$('#errordivFrsCode').empty();
	document.getElementById("sapFrsCode").focus();
	currentPageFlag='frs';
	document.getElementById("EEOCDivOpen").style.display="none";
	$('#notesmsgEEOC').find(".jqte_editor").css("background-color", "#FFFFFF");
	$('#notesmsgEEOC').find(".jqte_editor").html("");
	resetDivOrFormElements("frsCodeTableHistory");
	defaultSet();
	getOnrGridFrsCode(t_id, j_id, e_id, name, sapStatus);
	}
}

function getOnrGridFrsCode(t_id, j_id, e_id, name, sapStatus) {
	currentPageFlag='frs';
	$('#addStatusEEOC').empty();
	ONRAjax
			.PopUpGridFrsCode(
					t_id,
					j_id,
					e_id,
					noOfRowsForEEOC,
					pageForEEOC,
					sortOrderStr,
					sortOrderTypeForEEOC,
					currentPageFlag,
					{
						async : true,
						errorHandler : handleError,
						callback : function(data) {
						
							document.getElementById("teacherIdEEOC").value		=	t_id;
							document.getElementById("eligibilityIdEEOC").value	=	e_id;
							document.getElementById("jobIdEEOC").value			=	j_id;
						
							document.getElementById("myModalLabelFrsCode").innerHTML = ""+resourceJSON.MsgEqualEmploymentOpportunity+" "+ name;
							document.getElementById("myModalLabelEEOCHidden").value = name;
							var sAddStatus=document.getElementById("sAddStatus").value;
							if(sAddStatus=="1")
							{
								if(sapStatus == 'N' || sapStatus == 'null'){
									document.getElementById("addStatusEEOC").innerHTML 	= ""+resourceJSON.MsgAddStatus+"";
								}
							}
							
							
							document.getElementById("addStatusEEOCHidden").value	=  sapStatus;
							$('.onr_popup_frsCode').modal('show');
							document.getElementById("divFrsCode").innerHTML = data;
							if ($('#ethnicityId1').val() != -1) {
								setOnrGridFrsCode();
							}

							applyScrollOnTblEEOC();
						}
					});
}

function resetDivOrFormElements(class_name) {
	jQuery("#" + class_name).find(':input').each(function() {
		switch (this.type) {
		case 'password':
		case 'text':
		case 'textarea':
		case 'file':
		case 'select-one':
		case 'select-multiple':
			jQuery(this).val('');
			break;
		case 'checkbox':
		case 'radio':
			this.checked = false;
		}
	});
}

function setOnrGridFrsCode() {
	var frsCode 		= 	$('#frsCode1').val();
	var ethnicityId 	= 	$('#ethnicityId1').val();
	var ethonicOriginId = 	$('#ethonicOriginId1').val();
	var raceIds 		= 	($('#raceId1').val()).split(",");
	var genderId 		= 	$('#genderId1').val();

	document.getElementById('sapFrsCode').value = frsCode;
	$('#sapEthnicOriginId_' + ethonicOriginId).prop("checked", true);
	$('#sapEthnicityId_' + ethnicityId).prop("checked", true);
	$('#sapGenderId_' + genderId).prop("checked", true);
	$('#notesmsgEEOC').find(".jqte_editor").html();

	for ( var i = 0; i < raceIds.length - 1; i++) {
		$('#sapRaceId_' + raceIds[i]).prop("checked", true);
	}
}

/*	Save EEOC Data */
function saveEEOCData(){
	$('#errordivFrsCode').empty();
	var sapEthnicOriginId	=	document.getElementsByName("sapEthnicOriginId[]");
	var sapEthnicityId		=	document.getElementsByName("sapEthnicityId[]");
	var sapGenderId			=	document.getElementsByName("sapGenderId[]");
	var sapRaceId			=	document.getElementsByName("sapRaceId[]");
	var sapTeacherId		=	document.getElementById("sapTeacherId").value;
	var sapJobId			=	document.getElementById("sapJobId").value;
	var sapFrsCode			=	document.getElementById("sapFrsCode").value;
	var eligibilityIdEEOC	=	document.getElementById("eligibilityIdEEOC").value;
	
	var charCount	=	$('#notesmsgEEOC').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var notes		=	$('#notesmsgEEOC').find(".jqte_editor").html();
	var fileName 	= 	document.getElementById("eligibilyFileEEOC").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;
	
	var checkCnt1			=	0;
	var checkCnt2			=	0;
	var checkCnt3			=	0;
	var checkCnt4			=	0;
	var counter				=	0;
	var sapEthnicOriginIdVal;
	var sapEthnicityIdVal;
	var raceArray = "";
	
	
	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(eligibilyFileEEOC.files[0]!=undefined)
		fileSize = eligibilyFileEEOC.files[0].size;
	}

	for(var i=0; i < sapEthnicOriginId.length; i++){
		if(sapEthnicOriginId[i].checked == true){
			sapEthnicOriginIdVal = sapEthnicOriginId[i].value;
			checkCnt1++;
		}
	}
	for(var i=0; i < sapEthnicityId.length; i++){
		if(sapEthnicityId[i].checked == true){
			sapEthnicityIdVal = sapEthnicityId[i].value;
			checkCnt2++;
		}
	}
	
	for(var i=0; i < sapGenderId.length; i++){
		if(sapGenderId[i].checked == true){
			sapGenderIdVal = sapGenderId[i].value;
			checkCnt3++;
		}
	}
	
	for(var i=0; i < sapRaceId.length; i++){
		if(sapRaceId[i].checked == true){
			sapRaceIdVal = sapRaceId[i].value;
			raceArray+=	sapRaceIdVal+",";
			checkCnt4++;
		}
	}
	
	if(checkCnt1 == 0){
		$('#errordivFrsCode').append("&#149; "+resourceJSON.PlzSelectEhnicOrigin+"<br>");
		counter++;
	}

	if(checkCnt2 == 0){
		$('#errordivFrsCode').append("&#149;"+resourceJSON.msgSelectEthnicity1+"<br>");
		counter++;
	}

	if(checkCnt3 == 0){
		$('#errordivFrsCode').append("&#149; "+resourceJSON.PlzSlctGender+"<br>");
		counter++;
	}
	
	if(checkCnt4 == 0){
		$('#errordivFrsCode').append("&#149; "+resourceJSON.MsgAtleastOneRace+"<br>");
		counter++;
	}
	
	if ($('#notesmsgEEOC').find(".jqte_editor").text().trim()==""){
		if(focus == 0){
			$('#errordivFrsCode').append("&#149; "+resourceJSON.PlzEtrNotes+"<br>");
			$('#notesmsgEEOC').find(".jqte_editor").focus();
			$('#notesmsgEEOC').find(".jqte_editor").css("background-color", "#F5E7E1");
			$('#errordiv1').show();
			counter++;
			focus++;
		}
	}
	if(fileSize>=10485760)
	{
		$('#errordivFrsCode').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focs==0)
				$('#eligibilyFileFp').focus();
			
			$('#eligibilyFileFp').css("background-color",txtBgColor);
			counter++;
			focs++;	
			return false;
	}
	
	if(fileName==""){
		fileName= document.getElementById("eligibilyFileEEOC").value;
	}	

	if(counter == 0){

			if(fileName=="" && jsiFileNameFp!=""){
				fileName = jsiFileNameFp;
			}
			if(fileName!="" && fileName!=jsiFileNameFp){
				document.getElementById("frmoEEOCUpload").submit();
			} else {
			ONRAjax.saveEEOCData(notes,sapFrsCode,sapTeacherId,sapJobId,eligibilityIdEEOC,sapEthnicOriginIdVal,sapEthnicityIdVal,sapGenderIdVal,raceArray,"",{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				displayPNRGrid();
				$('#onr_popup_frsCode').modal('hide');
			}});
		  }
	   }
	}

function saveEEOCDataUpload(fileName){
	$('#errordivFrsCode').empty();
	var sapEthnicOriginId	=	document.getElementsByName("sapEthnicOriginId[]");
	var sapEthnicityId		=	document.getElementsByName("sapEthnicityId[]");
	var sapGenderId			=	document.getElementsByName("sapGenderId[]");
	var sapRaceId			=	document.getElementsByName("sapRaceId[]");
	
	var sapTeacherId		=	document.getElementById("sapTeacherId").value;
	var sapJobId			=	document.getElementById("sapJobId").value;
	var sapFrsCode			=	document.getElementById("sapFrsCode").value;
	var eligibilityIdEEOC	=	document.getElementById("eligibilityIdEEOC").value;
	
	var charCount	=	$('#notesmsgEEOC').find(".jqte_editor").text().trim();
	var count 		= 	charCount.length;
	var notes		=	$('#notesmsgEEOC').find(".jqte_editor").html();
	
	var sapEthnicOriginIdVal;
	var sapEthnicityIdVal;
	var raceArray = "";
	for(var i=0; i < sapEthnicOriginId.length; i++){
		if(sapEthnicOriginId[i].checked == true){
			sapEthnicOriginIdVal = sapEthnicOriginId[i].value;
		}
	}
	
	for(var i=0; i < sapEthnicityId.length; i++){
		if(sapEthnicityId[i].checked == true){
			sapEthnicityIdVal = sapEthnicityId[i].value;
		}
	}
	
	for(var i=0; i < sapGenderId.length; i++){
		if(sapGenderId[i].checked == true){
			sapGenderIdVal = sapGenderId[i].value;
		}
	}

	for(var i=0; i < sapRaceId.length; i++){
		if(sapRaceId[i].checked == true){
			sapRaceIdVal = sapRaceId[i].value;
			raceArray+=	sapRaceIdVal+",";
		}
	}
	ONRAjax.saveEEOCData(notes,sapFrsCode,sapTeacherId,sapJobId,eligibilityIdEEOC,sapEthnicOriginIdVal,sapEthnicityIdVal,sapGenderIdVal,raceArray,fileName,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	
		displayPNRGrid();
		$('#onr_popup_frsCode').modal('hide');
	}});
}

/*	Display EEOC DIV */
function openDivEEOC(){
	document.getElementById("sapFrsCode").focus();
	document.getElementById("EEOCDivOpen").style.display="block";
}

/* EEOC Content DIV Open */
function openContentEEOC(eligibilityId,userName){
	$('.onr_popup_frsCode').modal('hide');
	defaultSet();
	currentPageFlag='frs';
	getOnrGridNotesEEOC(eligibilityId,userName);
}

function getOnrGridNotesEEOC(eligibilityId,userName)
{ 	
	ONRAjax.openNotesByIdDT(eligibilityId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{	document.getElementById("UserProfileNameEEOC").innerHTML = ""+resourceJSON.MsgEqualEmploymentOpportunity+" "+ userName;
		$('.onrNotesShowEEOC').modal('show');
		document.getElementById("divOnrNotesEEOC").innerHTML=data;
	}});
}


function getMessageDivTest(teacherId,emailId,jobId,msgType){
	$('#shareDivDemoTest').modal('show');
}

function getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId)
{
	$('#draggableDivMaster').hide();
	hideProfilePopover();
	document.getElementById("commDivFlag").value=commDivFlag;
	document.getElementById("jobForTeacherGId").value=jobForTeacherGId;
	var commDivFlagPnrCheck = document.getElementById("commDivFlagPnrCheck").value;
	CandidateGridAjaxNew.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,false,{
		async: false,
		errorHandler:handleError,
		callback: function(data){
		
		$("#divCommTxt").html(data);
		
		try{
			$("#saveToFolderDiv").modal("hide");
			$('#myModalCommunications').modal('show');
			
			$("#myModalCommunications").attr("commDivFlag",commDivFlag);
			$("#myModalCommunications").attr("jobForTeacherGId",jobForTeacherGId);
			$("#myModalCommunications").attr("teacherId",teacherId);
			$("#myModalCommunications").attr("jobId",jobId);
			
			if(commDivFlagPnrCheck == 1){
				/*
					$('#commPhone').hide();
					$('#commMsg').hide();
					$('#commNotes').hide();
				*/
			}
		}catch(e){}
		$('#commPhone').tooltip();
		$('#commMsg').tooltip();
		$('#commNotes').tooltip();
	}
	});	
}

/*	Open Attachment in DIV  */

function downloadAttachment(filePath,fileName)
{
	
	ONRAjax.downloadAttachment(filePath,fileName,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		
		if(deviceType)
		{
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				 $('#modalDownloadsAttachment').modal('hide');
				 document.getElementById('ifrmAttachment').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#modalDownloadsAttachment').modal('hide');
			 document.getElementById('ifrmAttachment').src = ""+data+"";
		} else {
				$('.pnrAttachment').modal('show');
				document.getElementById('ifrmAttachment').src = ""+data+"";
		}
	}});
}

function hideAttachmentDiv(){
	
}

function showProfileContent(dis,jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,event)
{
	document.getElementById("jobId").value=jobId;
	var scrollTopPosition = $(window).scrollTop();
	var x=event.clientX;
	var y=event.clientY;
	var z=y-180+scrollTopPosition;
	z=z+80;	
	var style = $('<style>.popover { position: absolute;top: '+z+'px !important;left: 0px;width: 770px; }</style>')
	$('html > head').append(style);	
	showProfileContentClose();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;		
	TeacherProfileViewInDivAjax.showProfileContent(jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,0,{ 
	async: true,
	callback: function(data)
	{  
		
			$("#cgTeacherDivMaster").modal('show');
			$("#cgTeacherDivBody").html(data);
	         
			TeacherProfileViewInDivAjax.showProfileContent(jobforteacherId,teacherId,normscore,jobId,colorName,noOfRecordCheck,1,{ 
				async: true,
				callback: function(redata){
				setTimeout(function () {
					$("#profile_id").after(redata);
					$('#removeWait').remove();
		        }, 100);
			}
			});

		$('#loadingDiv').hide();
		$('#tagaction').css('pointer-events', 'none');
		$('#tagaction').css('font-size', '13px');
		$('#tagaction').addClass("icon-tag icon-large iconcolorhover");
		$('#tagaction span').hide();
		$('#tpResumeprofile').tooltip();
		$('#tpPhoneprofile').tooltip();
		$('#tpPDReportprofile').tooltip();
		$('#tpScoreReportGE').tooltip();
		$('#tpScoreReportSA').tooltip();
		$('#tpJSAProfile').tooltip();
		$('#tpTeacherProfileVisitHistory').tooltip();
	},
	errorHandler:handleError  
	});
} 

function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function isDate(dtStr){
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.indexOf(dtCh)
	var pos2=dtStr.indexOf(dtCh,pos1+1)
	var strMonth=dtStr.substring(0,pos1)
	var strDay=dtStr.substring(pos1+1,pos2)
	var strYear=dtStr.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
	for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (pos1==-1 || pos2==-1){
		$('#errordivTrans').append("&#149; "+resourceJSON.MsgDateFormat+".<br>");
		return false
	}
	if (strMonth.length<1 || month<1 || month>12){
		$('#errordivTrans').append("&#149; "+resourceJSON.PlzEtrValidMonth+".<br>");
		return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		$('#errordivTrans').append("&#149; "+resourceJSON.PlzValidDate+".<br>");
		return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		$('#errordivTrans').append('&#149; '+resourceJSON.PlzEtrValid4DigitYear+' '+minYear+' '+resourceJSON.MsgAnd+' '+maxYear+'.<br>');
		return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		$('#errordivTrans').append("&#149; "+resourceJSON.PlzValidDate+".<br>");
		return false
	}
	return true
}

function defaultTeacherGrid(){
	page = 1;
	noOfRows = 100;
	sortOrderStr="";
	sortOrderType="";
	currentPageFlag="";
	$('#draggableDivMaster').show();
}

function saveToFolderJFTNULL(teachersaveId,teacherId,flagpopover) {
	currentPageFlag="stt";
	document.getElementById('teacherIdForHover').value=teacherId;
	if(teacherId!="")
	{
		$("#teachetIdFromPoPUp").val(teacherId);
		$("#txtflagpopover").val(flagpopover);
	}
	$('#saveToFolderDiv').modal('show');
	$('#errordiv').hide();
	$('#draggableDivMaster').hide();

	var iframe 		= 	document.getElementById('iframeSaveCandidate');
	var innerDoc 	= 	iframe.contentDocument || iframe.contentWindow.document;

	innerDoc.getElementById('errortreediv').style.display		=	"none";
	innerDoc.getElementById('errordeletetreediv').style.display	=	"none";

	var folderId = innerDoc.getElementById('frame_folderId').value;
	if(innerDoc.getElementById('frame_folderId').value!="" || innerDoc.getElementById('frame_folderId').value.length > 0)
	{
		var checkboxshowHideFlag = 0; // For hide checkboxes on Save to Folder Div
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
	}
}

function displaySavedCGgridByFolderId(hoverDisplay,folderId,checkboxshowHideFlag)
{
	document.getElementById("checkboxshowHideFlag").value=checkboxshowHideFlag;
	document.getElementById("folderId").value=folderId;
	var pageFlag= document.getElementById("pageFlag").value;
	CandidateGridAjax.displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag,pageSTT,noOfRowsSTT,sortOrderStrSTT,sortOrderTypeSTT,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				document.getElementById("savedCandidateGrid").innerHTML=data;
				displaySaveCandidatePopUpDiv();
			}
		}
	});
}

/*function defaultSaveDiv(){
	pageSTT = 1;
	sortOrderStrSTT ="";
	sortOrderTypeSTT ="";
	noOfRowsSTT = 10;
	currentPageFlag="stt";
	//$('#draggableDivMaster').show();
}*/

function showCommunicationsDivForSave()
{
	page = 1;
	noOfRows = 100;
	sortOrderStr="";
	sortOrderType="";
	currentPageFlag="";
	$('#draggableDivMaster').show();
}

function showProfileContentClose(){
	if(deviceType || deviceTypeAndroid) {
		$('#cgTeacherDivMaster').modal('hide'); 
		$('#cgTeacherDivMaster').hide();
		document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";
	} else {
		$('#draggableDivMaster').hide(); 
		$('#myModalJobList').modal('hide');
		document.getElementById("teacherIdForprofileGrid").value="";
		currentPageFlag="";
	}
}

function displayUsergrid(teachersavedId,flagpopover) {

	document.getElementById("teachersharedId").value=teachersavedId;
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=1;
	document.getElementById("userCPS").value=0;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";

	$('#errorinvalidschooldiv').hide();
	if($('#entityType').val()==2)
	{
		$('#schoolId').val("0");
		$('#shareSchoolName').val("");
	}
	else
	{
		if($('#entityType').val()==3)
		{
			$('#schoolId').val($('#loggedInschoolId').val());
			$('#shareSchoolName').val($('#loggedInschoolName').val());
		}
	}
	var savecandiadetidarray="";
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 $("#savecandiadetidarray").val(savecandiadetidarray);
	if(savecandiadetidarray!="" || flagpopover==1)
	{
		try{
				$('#shareDiv').modal('show');
				$('#draggableDivMaster').hide();
			}catch(err){}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		if(districtId=="")
			districtId="0";
		var schoolId	=	trim($('#schoolId').val());
		if(schoolId=="")
		{
			$('#errorinvalidschooldiv').show();
			$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.SelectValidSchool);
			return false;
		}
		
		if($('#entityType').val()!="")
		{
			/* ========= Display School List ============ */
			CandidateGridAjax.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}
	}
	else
	{
		$("#errortreediv").show();
		$('#errortreediv').html("&#149; "+resourceJSON.PlzSlctCandidates);
	}
}
function defaultSaveDiv(){
	pageSTT = 1;
	sortOrderStrSTT ="";
	sortOrderTypeSTT ="";
	noOfRowsSTT = 10;
	currentPageFlag="stt";
}
function showProfileContentClose_ShareFolder(){
	defaultSaveDiv();
    //$('.profile').popover('destroy');
	document.getElementById("teacherIdForprofileGrid").value="";
    
}
function showProfileContentForTeacher(dis,teacherId,noOfRecordCheck,sVisitLocation,event)
{
	   try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);

	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   }      
	   }
	   catch(e){alert(e)}

	var districtMaster = null;
	showProfileContentClose_ShareFolder();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
	async: true,
	callback: function(data)
	{
	  $('#loadingDiv').hide();
	 
		  $("#draggableDivMaster").modal('show');
			$("#draggableDiv").html(data);
	       
			TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
				async: true,
				callback: function(resdata){
				setTimeout(function () {
					$("#profile_id").after(resdata);
					$('#removeWait').remove();
		        }, 100);
				
			}
			});
		
		$('#tpResumeprofile').tooltip();
		$('#tpPhoneprofile').tooltip();
		$('#tpPDReportprofile').tooltip();
		$('#tpTeacherProfileVisitHistory').tooltip();
		$('#teacherProfileTooltip').tooltip();
		$('#tagaction').css('pointer-events', 'none');
		$('#tagaction').css('font-size', '13px');
		$('#tagaction').css('font-weight', 'bold');
		$('#tagaction').addClass("icon-tag icon-large iconcolorhover");
		$('#tagaction span').hide();
		
		$('#shareaction1').css('pointer-events', 'none');
		$('#shareaction1').css('font-size', '13px');
		$('#shareaction1').css('font-weight', 'bold');
		$('#shareaction1').addClass("icon-tag icon-large iconcolorhover");
		$('#shareaction1 span').hide();
		
	},
	errorHandler:handleError  
	});
}

function generatePNRExel()
{
	var statusType = document.getElementsByName("statusType").value;
	$('#loadingDiv').fadeIn();
   ONRAjax.generatePNRExel(ssn,fname,lname,position,noOfRowsPNR,pagePNR,sortOrderStrPNR,sortOrderTypePNR,statusType,{
		async: true,
		callback: function(data){
   	    $('#loadingDiv').hide();
		if(deviceType)
		{					
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
				
		}
		else
		{
			try
			{
				document.getElementById('ifrmTrans').src = "pnrreport/"+data+"";
			}
			catch(e)
			{alert(e);}
		}
	},
errorHandler:handleError
});

}

function generatePNRPDF()
{
	var statusType = document.getElementsByName("statusType").value;
	$('#loadingDiv').fadeIn();
   ONRAjax.generatePNRPDF(ssn,fname,lname,position,noOfRowsPNR,pagePNR,sortOrderStrPNR,sortOrderTypePNR,statusType,{
		async: true,
		callback: function(data){		
		$('#loadingDiv').hide();	
		if(deviceType || deviceTypeAndroid)
		{
			window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
		}		
		else
		{
			$('#modalDownloadOfferReady').modal('hide');
			document.getElementById('ifrmCJS').src = ""+data+"";
			try{
				$('#modalDownloadOfferReady').modal('show');
			}catch(err)
			{}		
	     }		
	     return false;
},
errorHandler:handleError
});

}


function printPNRDATA()
{
	$('#loadingDiv').show();
	var statusType = document.getElementsByName("statusType").value;
	var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
	 ONRAjax.generatePNRPrint(ssn,fname,lname,position,noOfRowsPNR,pagePNR,sortOrderStrPNR,sortOrderTypePNR,statusType,{
		async: true,
		callback: function(data){		
		$('#loadingDiv').hide();
		 try{
			 if (isSafari && !deviceType)
			    {
			    	window.document.write(data);
					window.print();
			    }else
			    {
			    	var newWindow = window.open();
			    	newWindow.document.write(data);	
			    	newWindow.print();
				}
				//$('#printDataTableDiv').html(data);
				//$("#printmessage1").modal('show');
		 }catch(e){
			 $('#printmessage1').modal('show');
		 }
		},
	errorHandler:handleError
	});
}

function rejectFromAllJobs(){
	var removeArray="";
	var inputs 			= 	document.getElementsByName("onr_sheet");
	var splitArray;
	var splitArray1;
	var inputValue;
	var checkCntSap  = 	0;
	var checkCnt	 =	0;
	var checkCntFpDt = 	0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		inputValue 	= 	inputs[i].value;
	        		splitArray	=	inputValue.split(",");
	        		if(splitArray[1] == "N" || splitArray[1] == "null"){
	        			if(splitArray[3] == "Yes" || splitArray[4] == "Yes"){
	        				checkCntFpDt++; 
	        			}
	        			removeArray+=	splitArray[0]+",";
	        		} else {
	        			checkCntSap++;
	        		}
		        		checkCnt++;
	        	 }
	        }
	 	}
	 
	 if(checkCnt==0) {
			$('#message2showConfirm').html(resourceJSON.PlzSlctCandidateToReject);
			$('#myModal3').modal('show');
			return;
	 }
	 
	 if(checkCntSap > 0) {
			$('#message2showConfirm').html(resourceJSON.PlzSlctNewRecordOnly);
			$('#myModal3').modal('show');
			return;
	 }
	 
	 if(checkCntFpDt > 0) {
			$('#message2showConfirm').html(resourceJSON.MsgNotSetToRED);
			$('#myModal3').modal('show');
			return;
	 }
	 $('#loadingDiv').show();
	 ONRAjax.rejectFromAllJobs(removeArray,{
		async: true,
		callback: function(data){
		if(data=="0") {
			 $('#loadingDiv').hide();
			$('#message2showConfirm').html(resourceJSON.MsgRejectSuccessfully);
			$('#myModal3').modal('show');
			displayPNRGrid();
		} else {
			$('#loadingDiv').hide();
			$('#message2showConfirm').html(resourceJSON.MsgInformationMissing);
			$('#myModal3').modal('show');
			displayPNRGrid();
		}
	},
		errorHandler:handleError  
	});
}

function undoStatusConfirm(eId,eligibilityId,fullName,fromWhere,onrStatusType) {
	document.getElementById("eligibilityIdSet").value 	= 	eligibilityId;
	document.getElementById("onrStatusType").value 		= 	onrStatusType;
	var eligibilityName = "";
	if(eId == 1){
			eligibilityName = resourceJSON.msgI9
	} else if(eId == 2){
			eligibilityName = resourceJSON.msgW4
	} else if(eId == 3){
		eligibilityName = resourceJSON.MsgForFingerPrint
	} else if(eId == 4){
		eligibilityName = resourceJSON.MsgForDrugTest
	} else if(eId == 5){
		eligibilityName = resourceJSON.MsgTranscript
	} else if(eId == 6){
			eligibilityName = resourceJSON.MsgFRSForm 
	} else if(eId == 7){
			eligibilityName = resourceJSON.msgDirectDeposit 
	} else if(eId == 8){
			eligibilityName = resourceJSON.msgOathOfLoyalty; 
	} else if(eId == 10){
		eligibilityName = resourceJSON.MsgOpportunityCompliance;
	}
	
	document.getElementById("myModalStatusUndoLabel").innerHTML = eligibilityName+" "+resourceJSON.MsgUndoStatusFor+" "+fullName;
	
	if(fromWhere == "dashboard"){
			$('#onr_popup').modal('hide');
			$('#onr_popup1').modal('hide');
			$('#onr_popup2').modal('hide');
			$('#onr_popup_trans').modal('hide');
			$('#onr_popup_frsCode').modal('hide');
			$('#myModalUndoStatusConfirm').modal('show');
			return false;
	}
}

function undoStatus() {
	eligibilityId = document.getElementById("eligibilityIdSet").value;;
	ONRAjax.undoStatus(eligibilityId,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		displayPNRGrid();
		$('#myModalUndoStatusConfirm').modal('hide');
		$('#add_action').hide();
	}});
}

function hideDivUndoStatus(){
	var onrStatusType = $("#onrStatusType").val();
	try{
		$('#myModalUndoStatusConfirm').modal('hide');
		if(onrStatusType == "I9"){
			$('.onr_popup').modal('show');
		} else if(onrStatusType == "FP"){
			$('.onr_popup_fp').modal('show');
		} else if(onrStatusType == "DT"){
			$('.onr_popup_dt').modal('show');
		} else if(onrStatusType == "TRANS"){
			$('.onr_popup_trans').modal('show');
		} else if(onrStatusType == "FRS"){
			$('.onr_popup_frsCode').modal('show');
		}
	}catch(e){}
}
function getVideoPlay(videoLinkId)
{
	var teacherId =  document.getElementById("teacherIdForprofileGrid").value
	$('#videovDiv').modal('show');
	$('#loadingDivWaitVideo').show();
	$('#interVideoDiv').empty();
	var videoPath='';		
	PFCertifications.getVideoPlayDivForProfile(videoLinkId,teacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#loadingDivWaitVideo').hide();
		if(data!=""&& data!=null)
		{			
			var videoPath="";				
			videoPath="<video poster='images/video_back.png' src="+data+" width='480' height='280' controls autoplay><source src="+data+" type='video/ogg'>"+resourceJSON.msgBrowservideotag+"<br>"+resourceJSON.msgPlsDownload+" <a target='_blank' href='https://www.apple.com/in/quicktime/download/'>"+resourceJSON.msgQuickTimePlayer+"</a><br>"+resourceJSON.msgRestartSafaribrowser+" </video>";
			$('#interVideoDiv').append(videoPath);			
		}
		else
		{
			$('#interVideoDiv').css("color","Red");
			$('#interVideoDiv').append(resourceJSON.msgProblemoccured);
		}
		}});
}
function downloadOctUpload(octId)
{
		//alert("downloadAnswer0");
	TeacherProfileViewInDivAjax.downloadOctUpload(octId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			if(data!="")
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameReferencesID').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefOctUpload").href = data; 
					return false;
				}
			}
		});
	}



function getTeacherLicenceGrid_DivProfile(teacherId){
	
	TeacherProfileViewInDivAjax.displayRecordsBySSNForDate(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLicneseGridCertificationsGridDate').html(data);			
			//    $('#loadingDiv').hide();				
			   applyScrollOnTblLicenseDate();
		}
		
		},
	errorHandler:handleError
	});
}

function getLEACandidatePortfolio_DivProfile(teacherId){
	
	TeacherProfileViewInDivAjax.getLEACandidatePortfolio(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLEACandidatePotfolioDiv').html(data);			
			//    $('#loadingDiv').hide();				
				applyScrollOnLEACandidatePorfolio();
		}
		
		},
	errorHandler:handleError
	});
}

/*========  displayPNRGrid Op===============*/
function displayPNRGridOp() {
	try{
		var statusType = document.getElementsByName("statusType").value;
	}catch(e){}
	ssn		=	$("#ssn").val();
	fname	=	$("#firstName").val();
	lname	=	$("#lastName").val();
	position=	$("#position").val();

	defaultSet();
	divClose();

	if(statusType == null){
		$('#loadingDivHZS').show();
	} else {
		$('#loadingDiv').show();
	}
	try{
		pagePNR	 =	document.getElementById("pageNumberSet").value;
	}catch(e){}
	ONRAjax.displayPNRGridOp(ssn,fname,lname,position,noOfRowsPNR,pagePNR,sortOrderStrPNR,sortOrderTypePNR,statusType,{
		async: true,
		callback: function(data){
		$('#panelGrid').html(data);
		applyScrollOnTblOnr($('#userRoleId').val());
		$('#loadingDiv').hide();
		$('#loadingDivHZS').hide();
	},
		errorHandler:handleError  
	});
}

function showProfileContentForTeacherForONB(dis,teacherId,jobId,noOfRecordCheck,sVisitLocation,event)
{	
	var jftId = new Object();jftId['teacherId']=teacherId;jftId['jobId']=jobId;
	ONBServiceAjax.getJFTIdByJobIdAndTeacherId(jftId,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			$("[name='jobforteacherId']").val(data);
		},
	errorHandler:handleError
	});
	   try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);
	      
	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";  
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   }      
	   }
	   catch(e){alert(e)}
	   
	var districtMaster = null;
	showProfileContentClose_ShareFolder();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
	async: true,
	callback: function(data)
	{
	  $('#loadingDiv').hide();
	  $("#draggableDivMaster").modal('show');
	  //var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
		$("#draggableDiv").html(data);
       
		TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
			async: true,
			callback: function(resdata){
			setTimeout(function () {
				$("#profile_id").after(resdata);
				$('#removeWait').remove();
	        }, 100);
			
			
		}
		});
	  
	  $('#tpResumeprofile').tooltip();
	  $('#tpPhoneprofile').tooltip();
	  $('#tpPDReportprofile').tooltip();
	  $('#tpTeacherProfileVisitHistory').tooltip();
	  $('#teacherProfileTooltip').tooltip();
	  $('textarea').jqte();
	},
	errorHandler:handleError  
	});
} 

function showProfileContentForTeacherForONB(dis,teacherId,jobId,noOfRecordCheck,sVisitLocation,event)
{	
	var jftId = new Object();jftId['teacherId']=teacherId;jftId['jobId']=jobId;
	ONBServiceAjax.getJFTIdByJobIdAndTeacherId(jftId,{
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			$("[name='jobforteacherId']").val(data);
		},
	errorHandler:handleError
	});
	   try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);
	      
	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";  
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   }      
	   }
	   catch(e){alert(e)}
	   
	var districtMaster = null;
	showProfileContentClose_ShareFolder();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
	async: true,
	callback: function(data)
	{
	  $('#loadingDiv').hide();
	  $("#draggableDivMaster").modal('show');
	  //var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
		$("#draggableDiv").html(data);
       
		TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
			async: true,
			callback: function(resdata){
			setTimeout(function () {
				$("#profile_id").after(resdata);
				$('#removeWait').remove();
	        }, 100);
			
			
		}
		});
	  
	  $('#tpResumeprofile').tooltip();
	  $('#tpPhoneprofile').tooltip();
	  $('#tpPDReportprofile').tooltip();
	  $('#tpTeacherProfileVisitHistory').tooltip();
	  $('#teacherProfileTooltip').tooltip();
	  $('textarea').jqte();
	},
	errorHandler:handleError  
	});
} 

function showQualificationDivForONB(jobId,teacherId,districtId,headQuarterId,branchId)
{
	     document.getElementById("teacherIdforPrint").value='';
	     document.getElementById("districtIdforPrint").value='';
	     document.getElementById("headQuarterIdforPrint").value='';
	     document.getElementById("branchIdforPrint").value='';
         document.getElementById("teacherIdforPrint").value=teacherId;
         document.getElementById("districtIdforPrint").value=districtId;
         document.getElementById("headQuarterIdforPrint").value=headQuarterId;
         document.getElementById("branchIdforPrint").value=branchId;
        CandidateGridAjaxNew.getQualificationDetailsForONB(jobId,teacherId,districtId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
	    	$('#qualificationDiv').modal('show');
	    	getDistrictSpecificQuestionDataForONB(districtId,teacherId,jobId);
			
			try{
				$('#qualificationDivBody').html(data);
				if(document.getElementById("userType").value==2 || document.getElementById("userType").value==5)
				{
					if(document.getElementById("flagForDistrictSpecificQuestions").value=="null" || document.getElementById("flagForDistrictSpecificQuestions").value=="false")
					{
						if((document.getElementById("flagForFinalize").value=='null' || document.getElementById("flagForFinalize").value=="false"))
						{	
							$("#qStatusSave").show();
							$("#qStatusFinalize").show();
						}
						else
						{
							$("#qStatusSave").hide();
							$("#qStatusFinalize").hide();
						}
						
					}else if(document.getElementById("flagForDistrictSpecificQuestions").value=="true"){
						$("#qStatusSave").hide();
						$("#qStatusFinalize").hide();
					}
				}
				else
				{
					$("#qStatusSave").hide();
					$("#qStatusFinalize").hide();
				}
			}catch(err){}
		}
	});
}

function saveQualificationNoteForONB(saveStatus)
{
	var teacherId = document.getElementById("teacherIdforPrint").value;
	var jobId = document.getElementById("jobIdonb").value;
	
	var note = $("#statusNoteID").val();
	$('#errorQStatus').empty();
	if(note==''){
		$('#errorQStatus').show();
		$('#errorQStatus').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
	}else{
		CandidateGridAjaxNew.saveQualificationNoteForONB(teacherId,note,saveStatus,jobId,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#qualificationDiv').modal('hide');
				displayPNRGrid();
			}
		});
	}
}
function printQualificationDivForONB()
{	
	var teacherId = document.getElementById("teacherIdforPrint").value;
	var districtId = document.getElementById("districtIdforPrint").value;
	var headQuarterId = document.getElementById("headQuarterIdforPrint").value;
	var branchId = document.getElementById("branchIdforPrint").value;
	 $('#loadingDiv').fadeIn();
	    CandidateGridAjaxNew.getQualificationDetailsForONBPrint(teacherId,districtId,headQuarterId,branchId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
	     	$('#loadingDiv').hide();
			try
			{
			    	var newWindow = window.open();
			    	newWindow.document.write(data);			
					newWindow.print();							   
			}
			catch (e) 
			{							
				$('#printmessage1').modal('show');
			}
		}
	});
}
function getDistrictSpecificQuestionDataForONB(districtId,teacherId,jobId)
{	
	dId=districtId;
	tId=teacherId;
	CandidateGridAjaxNew.getDistrictSpecificQuestionDataForONB(districtId,teacherId,jobId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#getDistrictSpecificQuestion').html(data);
		}
	});
}
$(document).ready(function(){
	$("input[name='statusDT']").change(function(){
		if(!$("#status11").is(":checked")){	
			$("#requiredExam").html('<span class="required">*</span>');
		}else{
			$("#requiredExam").html('');
		}
	});
	
});