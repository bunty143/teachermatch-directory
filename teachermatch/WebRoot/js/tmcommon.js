var xmlHttp=null;
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function showDiv(divId)
{
	var tempDivId;
	for(var j=0;j<divArray.length;j++)
	{
		tempDivId=document.getElementById(divArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}
function showDivMsg(divId,msgArray)
{

	var tempDivId;
	for(var j=0;j<msgArray.length;j++)
	{
		tempDivId=document.getElementById(msgArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}

function deleteData(delUrl)
{
	if(window.confirm(resourceJSON.cnfMsgDelete))
	{
		var url = delUrl+"&td="+new Date().getTime();
		window.location.href=url;
	}
}


function showHideBankDiv(stBank,stChequeDD,stButton)
{
	document.getElementById("divBankList").style.display=stBank;
	document.getElementById("divChequeDD").style.display=stChequeDD;
	document.getElementById("divPayButton").style.display=stButton;
}

function isNumber(field) {
	var str=field.value;
	if (str.indexOf('.') == -1) str += ".";
	var decNum = str.substring(str.indexOf('.')+1, str.length);
	if (decNum.length > 2)//here is the key u can just change from 2 to 3,45 etc to restict no of digits aftre decimal
	{
		alert(resourceJSON.msgInvalid2DigitsDecimal)
	}
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	if (decNum.length > 1)//here is the key u can just change from 2 to 3,45 etc to restict no of digits aftre decimal
	{
		//alert("Invalid more than 2 digits after decimal")
		//count=false;
	}
	//alert(count);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
	if(parts.length > 1 && charCode==46)
		return false;

	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function checkForIntUpto6(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 49 && charCode <= 54)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	if(str.indexOf('teachermatch.org')>-1 || str.indexOf('teachermatch.com')>-1){
		emailPattern = /^[a-zA-Z0-9._+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	}
	return emailPattern.test(str);
}


function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}
function checkForEnter(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		//chkPassword();
		return true;
	}
	return false;
}
function setCoverLetter(){
	document.getElementById("divCoverLetter").style.display="block";
	$('#myModalCL').modal('show');
	$('#divCoverLetter').find(".jqte_editor").focus();
	document.getElementById("rdoCL1").checked=true;

	$('#iconpophover101').tooltip();
	
	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}

function setCLEnable(){
	
	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="block";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}
function setClBlank(){
	
	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="none";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}

function setLatestCoverLetter(){

	AssessmentCampaignAjax.getLatestCoverLetter({ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
		$('#divCoverLetter').find(".jqte_editor").html(data.coverLetter);
		}
	});	

	document.getElementById("divCoverLetter").style.display="block";
	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}


function setLatestCoverLetterForMartinAdmin(){

	AssessmentCampaignAjax.getLatestCoverLetter({ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
		$('#divCoverLetterAdmin').find(".jqte_editor").html(data.coverLetter);
		}
	});	

	document.getElementById("divCoverLetterAdmin").style.display="block";
	$('#errordivCL').empty();
	$('#divCoverLetterAdmin').find(".jqte_editor").css("background-color", "");
}

/*function redirectToPortfolioUrl(portfolioURL)
{
	alert("Hi"+portfolioURL);
	window.location.href=portfolioURL;
}*/

var totalQuestions = 0;
function checkCL(){
	
	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
	var jobId=document.getElementById("jobId").value;
	var isAffilated=0;
	if(document.getElementById("isAffilated").checked)
	{
		isAffilated=1;
	}
	if($("#districtIdForDSPQ").length > 0)
	{
		var districtId = document.getElementById("districtIdForDSPQ").value;
		if(districtId==806810 || districtId==7800294){
			isAffilated=$('input[name=isAffilated]:checked').val();
		}
	}
	
	var cnt=0;
	var focs=0;
	
	
	if(document.getElementById("rdoCL1").checked==true || document.getElementById("rdoCL2").checked==true || document.getElementById("rdoCL3").checked==true)
	{
		if(document.getElementById("rdoCL2").checked==false && ($("#districtIdForDSPQ").length > 0 && $("#districtIdForDSPQ").val()!=806900) && $("#districtIdForDSPQ").val().trim()!='1201290')
		{
			if ($('#divCoverLetter').find(".jqte_editor").text().trim()==""){
				$('#errordivCL').append("&#149; "+resourceJSON.msgTypeCoverLtr+"<br>");
				if(focs==0)
					$('#divCoverLetter').find(".jqte_editor").focus();
				$('#divCoverLetter').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;			
			}
		}
	}
	
	////////////////////////
	if(document.getElementById("districtIdForDSPQ").value==806810){
		
		try{
			var	isAffilated=$('input[name=isAffilated]:checked').val();
			if(isAffilated==2){
				//$('#f_namee').css("background-color", "#FFF");
				//$('#l_namee').css("background-color", "#FFF");
				$('#nameWhileEmployed').css("background-color", "#FFF");
				if( $("#nameWhileEmployed").val().trim()=="" ){
					$('#errordivCL').append("&#149; Please Enter Name While Employed<br>");
//					if(focs==0){
						$('#nameWhileEmployed').css("background-color", "#F5E7E1");
//						$('#l_namee').focus();
						cnt++;focs++;
//					}
				}
				
			/*	if( $("#l_namee").val().trim()=="" ){
					$('#errordivCL').append("&#149; Please Enter Last Name<br>");
//					if(focs==0){
						$('#l_namee').css("background-color", "#F5E7E1");
//						$('#l_namee').focus();
						cnt++;focs++;
//					}
				}
				
				if($("#f_namee").val().trim()=="" ){
					$('#errordivCL').append("&#149; Please Enter First Name<br>");
//						if(focs==0){
							$('#f_namee').css("background-color", "#F5E7E1");
//							$('#f_namee').focus();
							cnt++;focs++;
//						}
				}*/
				
			}
			
			if(isAffilated==1){
				$('#empfe2location').css("background-color", "#FFF");
				$('#empfe2position').css("background-color", "#FFF");
				
				if($("#empfe2position").val().trim()==""){
					$('#errordivCL').append("&#149; Please Enter Position<br>");
//					if(focs==0){
						$('#empfe2position').css("background-color", "#F5E7E1");
//						$('#empfe2position').focus();
						cnt++;focs++;
//					}
				}
				if($("#empfe2location").val().trim()==""){
					$('#errordivCL').append("&#149; Please Enter Location<br>");
//						if(focs==0){
							$('#empfe2location').css("background-color", "#F5E7E1");
//							$('#empfe2location').focus();
							cnt++;focs++;
//						}
				}
				
			}

			if(isAffilated==3){
				$('#empaproretdatefe6').css("background-color", "#FFF");
				if($("#empaproretdatefe6").val().trim()==""){
					$('#errordivCL').append("&#149; Please Enter Retirement Date<br>");
//						if(focs==0){
							$('#empaproretdatefe6').css("background-color", "#F5E7E1");
//							$('#empaproretdatefe6').focus();
							cnt++;focs++;
//						}
				}
				
			}
		}catch(err){}
	}
	/////////////////////////
	
	
	//Dhananjay Verma 23-02-2016
	
	if(document.getElementById("districtIdForDSPQ").value==1201290){
		
		try{
			var	isAffilated=$('input[name=isAffilated]:checked').val();
			
			
			var	coverLetterRadioButton=$('input[id=rdoCL2ForMartinAdmin]:checked').val();
			
			
			if(coverLetterRadioButton == 'NCL')
			{
				
			}
			if(coverLetterRadioButton == 'YCL')
			{
				var coverLetter=$('[name="coverLetter"]').parents().parents('.jqte').find(".jqte_editor").text();
				
				$('#coverLetter').css("background-color", "#FFF");
				
				if( coverLetter.trim() == "" ){
					
					$('#errordivMartin').append("&#149; Please type in your cover letter<br>");
						
						$('#coverLetter').css("background-color", "#F5E7E1");
			
						$('#coverLetter').css("background-color", "#F5E7E1");
						isNext=false;
						if(focs==0)
						{
							$('#coverLetter').focus();
							focs++;
						}
						cnt++;
					}
				
			}
			
			
			if(isAffilated==2){//Formal Employee
				
				$('#fromalempjobtitle').css("background-color", "#FFF");
				
				if( $("#fromalempjobtitle").val().trim()=="" ){
					
					$('#errordivMartin').append("&#149; Please Enter Job Title<br>");
						
						$('#fromalempjobtitle').css("background-color", "#F5E7E1");
			
						cnt++;focs++;
					}
			
				
				if( $("#formalempdatesofemployee").val().trim()=="" ){
					
					$('#errordivMartin').append("&#149; Please Enter Dates Of Employment<br>");
		
						$('#formalempdatesofemployee').css("background-color", "#F5E7E1");
			
						isNext=false;
						if(focs==0)
						{
							$('#formalempdatesofemployee').focus();
							focs++;
						}
						cnt++;
					}
		
		
				if( $("#formalempnamewhileemployee").val().trim()=="" ){
			
					$('#errordivMartin').append("&#149; Please Enter Name While Employed<br>");

					$('#formalempnamewhileemployee').css("background-color", "#F5E7E1");
	
					isNext=false;
					if(focs==0)
					{
						$('#formalempnamewhileemployee').focus();
						focs++;
					}
					cnt++;
			}
		}
			
			
				if(isAffilated==3){//Retired Employee
				
					$('#retiredempdateofretirement').css("background-color", "#FFF");
					
					if( $("#retiredempdateofretirement").val().trim()=="" ){
					
					$('#errordivMartin').append("&#149; Please Enter Date of Retirement <br>");
		
						$('#retiredempdateofretirement').css("background-color", "#F5E7E1");
			
						isNext=false;
						if(focs==0)
						{
							$('#retiredempdateofretirement').focus();
							focs++;
						}
						cnt++;
					}
			}
			

				if(isAffilated==1){//Current Employee
	
						$('#currentempjobtitle').css("background-color", "#FFF");
	
						if( $("#currentempjobtitle").val().trim()=="" ){
		
							$('#errordivMartin').append("&#149; Please Enter Job Title<br>");

							$('#currentempjobtitle').css("background-color", "#F5E7E1");

							cnt++;focs++;
						}

	
						if( $("#currentemplocation").val().trim()=="" ){
		
							$('#errordivMartin').append("&#149; Please Enter Location<br>");

							$('#currentemplocation').css("background-color", "#F5E7E1");

							cnt++;focs++;
						}


						if( $("#currentempyearinlocation").val().trim()=="" ){

							$('#errordivMartin').append("&#149; Please Enter Year In Location<br>");

							$('#currentempyearinlocation').css("background-color", "#F5E7E1");

							cnt++;focs++;
						}


						if( $("#currentempsupervisor").val().trim()=="" ){

							$('#errordivMartin').append("&#149; Please Enter Supervisor<br>");

							$('#currentempsupervisor').css("background-color", "#F5E7E1");

							cnt++;focs++;

						}
				}


			if(isAffilated==5){//Current Substitute Employee
	
					$('#currentsubstitutejobtitle').css("background-color", "#FFF");
	
					if( $("#currentsubstitutejobtitle").val().trim()=="" ){
		
						$('#errordivMartin').append("&#149; Please Enter Job Title<br>");

						$('#currentsubstitutejobtitle').css("background-color", "#F5E7E1");

						cnt++;focs++;
					}
	
			}
			
			if(isAffilated == undefined)
			{
				$('#errordivMartin').append("&#149; Please Check At Least One Employee Status<br>");
				
				isNext=false;
				if(focs==0)
				{
					
					focs++;
				}
				cnt++;
				
			}
			
		}catch(err){
			
			alert(err.message);
		}
	}
	
	
	if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==7800294){
		try{
			$('#emppos').css("background-color", "");
			$('#districtEmail').css("background-color", "");
			var	isAffilated=$('input[name=isAffilated]:checked').val();
			if(isAffilated==2 && $("#emppos").val().trim()=="" ){
					$('#errordivCL').append("&#149; "+resourceJSON.msgEnterEmployeePos+"</BR>");
					$('#emppos').css("background-color", "#F5E7E1");
					if(focs==0)
					{
						$('#emppos').focus();
						cnt++;focs++;
					}
			}
			if(isAffilated==1 && $('#districtEmail').val().trim()==""){
				$('#errordivCL').append("&#149; "+"Please enter District Email Address"+"<br>");
				$('#districtEmail').css("background-color", "#F5E7E1");
				if(focs==0)
				{
					$('#districtEmail').focus();
					cnt++;focs++;
				}
			}
			if(cnt==0 && (isAffilated==1 || isAffilated==2)){
				var empPosition = $("#emppos").val().trim();
				var districtEmail = $('#districtEmail').val().trim();
				PFExperiences.updateTeacherFormerEmpInfo(0,empPosition,districtEmail,isAffilated,
					{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
							//alert("call 1");
						}
					});
			}
		}catch(err){}
		var	isaffiliateds=$('input[name=isAffilated]:checked').val();
		$("#isaffiliateds").val(isaffiliateds);
	}

	//alert("cnt: "+cnt);
	if(cnt==0)
	{
		
		//Dhananjay Verma 
		
		if(document.getElementById("districtIdForDSPQ").value==1201290){
				
			var districtId = document.getElementById("districtIdForDSPQ").value
			//var teacherIDForMartin = $("#tIDD").val().trim();
			var	isAffilated=$('input[name=isAffilated]:checked').val();
			
if(isAffilated==2){//Formal Employee
				
				var FormalEmployeeJobTitle = $("#fromalempjobtitle").val().trim();
				var FormalEmployeeDateOfEmployement= $("#formalempdatesofemployee").val().trim();
				var formalempnamewhileemployee = $("#formalempnamewhileemployee").val().trim();
				
				var DateOfEmployement	=	new Date(trim(FormalEmployeeDateOfEmployement));

				PFExperiences.updateTeacherFormalEmployee(districtId,teacherid,FormalEmployeeJobTitle,DateOfEmployement,formalempnamewhileemployee,isAffilated,{ 
							async: false,
							errorHandler:handleError,
							callback: function(data)
							{
								//alert(data);
					
							}
						});
				
			}
			
			
				if(isAffilated==3){//Retired Employee
				
					
					var RetiredEmployee = $("#retiredempdateofretirement").val().trim();
					var retiredempdateofretirement	=	new Date(trim(RetiredEmployee));
					
					PFExperiences.updateTeacherRetiredEmployee(districtId,teacherid,retiredempdateofretirement,isAffilated,{ 
								async: false,
								errorHandler:handleError,
								callback: function(data)
								{
									//alert(data);
						
								}
							});
					
			}
			
			

				if(isAffilated==1){//Current Employee
					
					var FormalEmployeeJobTitle = $("#currentempjobtitle").val().trim();
					var FormalEmployeeLocation = $("#currentemplocation").val().trim();
					var FormalEmployeeYearLocation = $("#currentempyearinlocation").val().trim();
					var FormalEmployeeSupervisor = $("#currentempsupervisor").val().trim();
						

					PFExperiences.updateTeacherCurrentEmployee(districtId,teacherid,FormalEmployeeJobTitle,FormalEmployeeLocation,FormalEmployeeYearLocation,FormalEmployeeSupervisor,isAffilated,{ 
								async: false,
								errorHandler:handleError,
								callback: function(data)
								{
									//alert('success data is save');
						
								}
							});
					
				}	


			if(isAffilated==5){//Current Substitute Employee
	
				
				var FormalEmployeeJobTitle = $("#currentsubstitutejobtitle").val().trim();
				

				PFExperiences.updateTeacherCurrentSubstituteEmployee(districtId,teacherid,FormalEmployeeJobTitle,isAffilated,{ 
							async: false,
							errorHandler:handleError,
							callback: function(data)
							{
							}
						});
					}
				}
		
		
		
	if(document.getElementById("districtIdForDSPQ").value==806810){
			
		
			var nameWhileEmployed="";
			var f_namee="";
			var l_namee="";
			var empfe2location="";
			var empfe2position="";
			var empaproretdatefe6="";
			var districtid  = document.getElementById("districtIdForDSPQ").value;
			
			var teacherid=$("#tIDD").val().trim();
			var	isAffilated=$('input[name=isAffilated]:checked').val();
			
			if(isAffilated==2){
				var f_namee=$("#f_namee").val().trim();
				var l_namee=$("#l_namee").val().trim();
				var nameWhileEmployed=$("#nameWhileEmployed").val().trim();
				}
					
			if(isAffilated==1){
				var empfe2location=$("#empfe2location").val().trim();
				var empfe2position=$("#empfe2position").val().trim();
				
				
			}
			if(isAffilated==3){
				var empaproretdatefe6=$("#empaproretdatefe6").val();
				var startDate	=	new Date(trim(empaproretdatefe6));
			}
			PFExperiences.updateTeacherInfo(teacherid,isAffilated,f_namee,l_namee,empfe2location,empfe2position,startDate,nameWhileEmployed,districtid,{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
							//alert(data);
				
						}
					});
		
		}
		
		
		AssessmentCampaignAjax.checkIsAffilated(jobId,isAffilated,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			
			
			
			
			/*=== data=0 ==== means no previous job applied for this district ======*/
			//if(data!=0 && data!=isAffilated) /* === data and isAffilated both are same then we will display this Alert only:changed on 2 Sep : */
			if(data!=0 && ( (data==1 && isAffilated==0) ||(data==1 && isAffilated==1)) )
			{
				$('#myModalCL').modal('hide');
				if(isAffilated==1) // Teacher has checked isAffilated check curent job and latest previous job's isAffilated status 0 means NOT checked 
				{
					document.getElementById("divAlertText").innerHTML=resourceJSON.msgAlreadyAppliedjobssameDistrict;			
				}
				else
				{
					if(isAffilated==0)// Teacher has NOT checked isAffilated check curent job and latest previous job's isAffilated status 1 means Checked.
						document.getElementById("divAlertText").innerHTML=resourceJSON.msgAlreadyAppliedjobssameDistrictNot;
				}
				//alert("[ tm common js method ] checkIsAffilated : data : "+data+"  From Interface isAffilated : "+isAffilated);
				$('#divAlert').modal('show');
				document.getElementById("ok_cancelflag").value=isAffilated;
				
				$('#isresetStatus').val(1);
				$('#isAffilatedValue').val(isAffilated);
			}
			else
			{
				//alert("else count value "+cnt+" data  "+data)
				if(cnt==0)
				{
					AssessmentCampaignAjax.getDistrictSpecificQuestion(jobId,isAffilated,{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
						if(data!=null)
						{
							try
							{
								var dataArray = data.split("@##@");
								totalQuestions = dataArray[1];
								
								var textmsg = dataArray[2];
								if(textmsg!=null && textmsg!="" && textmsg!='null')
									$('#textForDistrictSpecificQuestions').html(textmsg);
								
								if(totalQuestions==0)
								{
									document.getElementById("frmApplyJob").submit();
									return;
								}
								
								$('#tblGrid').html(dataArray[0]);
								$('#myModalDASpecificQuestions').modal('show');
							}catch(err)
							{}
						}
						else
						{
							document.getElementById("frmApplyJob").submit();
						}

						}
					});	

				}
			}
		}
		});	
	}
	return false;
}

function resetjftIsAffilated()
{
	var isAffilated=0;
	try{
		if(document.getElementById("ok_cancelflag").value==1)
		{
			document.getElementById("isAffilated").checked=true;
			isAffilated=1;
		}
		else
		{
			document.getElementById("isAffilated").checked=false;
			isAffilated=0;
		}
	}catch(err){}
	var jobId=document.getElementById("jobId").value;

	AssessmentCampaignAjax.getDistrictSpecificQuestion(jobId,isAffilated,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			$('#isresetStatus').val(1);
			$('#isAffilatedValue').val(isAffilated);
			//alert(" data "+data);
			if(data!=null)
			{
				
				
				var dataArray = data.split("@##@");
				totalQuestions = dataArray[1];
				var textmsg = dataArray[2];
				if(textmsg!=null && textmsg!="" && textmsg!='null')
					$('#textForDistrictSpecificQuestions').html(textmsg);
				
				if(totalQuestions==0)
				{
					document.getElementById("frmApplyJob").submit();
					resetjftIsAffilatedAfterDSQ();
					return;
				}
												
				try
				{
					$('#tblGrid').html(dataArray[0]);
					$('#divAlert').modal('hide');
					/*$('#isresetStatus').val(1);
					$('#isAffilatedValue').val(isAffilated);*/
					//$('#myModalDASpecificQuestions').modal('show');
					validatePortfolioErrorMessageAndGridData('level1');
				}catch(err)
				{}
			}
			else
			{
				document.getElementById("frmApplyJob").submit();
				resetjftIsAffilatedAfterDSQ();
				return;
			}

		}
	});	
	return true;
}

function canceljftIsAffilated()
{
	var isAffilated=0;
	try{
		if(document.getElementById("ok_cancelflag").value==1)
		{
			document.getElementById("isAffilated").checked=true;
			isAffilated=1;
		}
		else
		{
			document.getElementById("isAffilated").checked=false;
			isAffilated=0;
		}
	}catch(err){}
	$('#isresetStatus').val(0);
	var jobId=document.getElementById("jobId").value;
	AssessmentCampaignAjax.getDistrictSpecificQuestion(jobId,isAffilated,{ 
 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
		if(data!=null)
		{

			var dataArray = data.split("@##@");
			totalQuestions = dataArray[1];
			var textmsg = dataArray[2];
			if(textmsg!=null && textmsg!="" && textmsg!='null')
				$('#textForDistrictSpecificQuestions').html(textmsg);

			if(totalQuestions==0)
			{
				document.getElementById("frmApplyJob").submit();
				return;
			}
			try
			{
				$('#tblGrid').html(dataArray[0]);
				//$('#myModalDASpecificQuestions').modal('show');
				$('#divAlert').modal('hide');
				validatePortfolioErrorMessageAndGridData('level1');
			}catch(err)
			{}
		}
		else
		{
			document.getElementById("frmApplyJob").submit();
		}
		}
	});	


	//document.getElementById("frmApplyJob").submit();
	return true;
}

/*====== GAgan : It will change the status in jobforteacher table after job applied by teacher =====*/
function resetjftIsAffilatedAfterDSQ()
{
	//alert("[ resetjftIsAffilatedAfterDSQ Method ] jobId :"+$('#jobId').val()+" isAffilatedValue : "+$('#isAffilatedValue').val())
	AssessmentCampaignAjax.resetjftIsAffilated($('#jobId').val(),$('#isAffilatedValue').val(),{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			//alert("resetjftIsAffilatedAfterDSQ : data :"+data)
			if(data==1)
			{
				//getDashboardJobsofIntrest();
			}
		}
	});
}


function getDashboardJobsofIntrest()
{
	DashboardAjax.getJbofIntresGrid({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#divJbInterest').html(data);
		tpJbIntrestDashboardEnable();
	}
	});	

}

//vishwanath (Used to limit charachers)
function LimitCharacter(stringVal,limit,append_charecter)
{
	var stringVal_Count=stringVal.length;
	var temp_count=0;
	var newString='';
	var stringArray=stringVal.split(" ");

	if(stringArray.length>1)
	{
		for(i=0; i<stringArray.length;i++)
		{	
			if(i!=0)  /////this is for space character
			{
				temp_count++;
				newString+=" ";
			}

			temp_count+=stringArray[i].length;
			if(temp_count<=limit)
			{	
				newString+=stringArray[i];

			}
			else
			{	newString+=append_charecter;
			break;
			}
		}

	}
	else
	{
		if(stringVal.length>limit)
		{	
			for(i=0; i<limit;i++)
			{
				newString+=stringVal[i];
			}
			newString+=append_charecter;
		}
		else
		{
			newString=stringVal;
		}

	}
	return  newString;
}

function showEmpNoDiv(radioFlag)
{
	
	var nextProcess=true;	
	/*if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990 && $("input:checkbox[name=isAffilated]").is(":checked")){
		if($('input:radio[name=staffType]').is(":checked") && radioFlag!=2){
			//callDynamicPortfolio();
			$("#myModalDymanicPortfolio").hide();
			
			$("#affDivPHiL").hide();
			$("#dynamicInfoMsg").html("You have declared yourself as an current Internal Candidate while filling the Cover Letter details but now you are changing your declaration. Please first change the declaration in Cover Letter screen.");
			$("#wrongDivPHiL").show();
			nextProcess=false;
		}else if(!$('input:radio[name=staffType]').is(":checked") && radioFlag==2){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").hide();
			$("#dynamicInfoMsg").html("You have declared yourself as an External Candidate while filling the Cover Letter details but now you are changing your declaration. Please first change the declaration in Cover Letter screen.");
			$("#wrongDivPHiL").show();
			nextProcess=false;
		}
		
	}*/
	if(nextProcess==true)
	if(radioFlag==1)
	{
		$("#empfewDiv").hide();
		$("#fe1Div").show();
		$("#fe2Div").hide();
		$("#position6Div").hide();
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==1302010){
			$("#empfewDiv").show();
		}
	}
	else if(radioFlag==2)
	{
		$("#fe1Div").hide();
		$("#fe2Div").show();
		$("#position6Div").hide();
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").show();
			$("#rdCEmp1").hide();
			$("#rdCEmp2").hide();
		}		
	}
	else if(radioFlag==6)
	{
		$("#fe1Div").hide();
		$("#fe2Div").hide();
		$("#position6Div").show();
		if($("#districtIdForDSPQ").length > 0 && document.getElementById("districtIdForDSPQ").value==4218990){
			$("#myModalDymanicPortfolio").hide();
			$("#affDivPHiL").hide();
			$("#rdCEmp1").hide();
			$("#rdCEmp2").hide();
			
			
		}		
	}
	
	else if(radioFlag.trim() == 'CEmpOfMartin')
	{
		
		$("#currentSubstituteEmployee").hide();
		$("#retiredEmployeeStateofFlorida").hide();
		$("#formalEmployeeOfMartin").hide();
		
		$("#currentEmployeeOfMartin").show();
		
		
	}
	else if(radioFlag.trim() == 'FormalEmpOfMartin')
	{
		
		$("#currentSubstituteEmployee").hide();
		$("#retiredEmployeeStateofFlorida").hide();
		$("#currentEmployeeOfMartin").hide();
		
		$("#formalEmployeeOfMartin").show();
				
		
	}
	else if(radioFlag.trim() == 'RetiredEmpOfMartin')
	{
		
		$("#currentSubstituteEmployee").hide();
		$("#formalEmployeeOfMartin").hide();
		$("#currentEmployeeOfMartin").hide();
		
		$("#retiredEmployeeStateofFlorida").show();
		
		
	}
	else if(radioFlag.trim() == 'CurrentSubstituteEmpOfMartin')
	{
		
		$("#currentSubstituteEmployee").show();
		$("#retiredEmployeeStateofFlorida").hide();
		$("#formalEmployeeOfMartin").hide();
		$("#currentEmployeeOfMartin").hide();
		
	}
	else if(radioFlag.trim() ==  'never_emp')
	{
		$("#currentSubstituteEmployee").hide();
		$("#retiredEmployeeStateofFlorida").hide();
		$("#formalEmployeeOfMartin").hide();
		$("#currentEmployeeOfMartin").hide();
	}
	
	else
	{
		$("#fe1Div").hide();
		$("#fe2Div").hide();
		$("#position6Div").hide();
	}
	if($("#dID").length > 0 && document.getElementById("dID").value==7800294)
	{
		if(radioFlag==1)
		{
			$("#districtEmailDiv").hide();
			$("#fePosDiv").show();
		}
		else if(radioFlag==2)
		{
			$("#fePosDiv").hide();
			$("#districtEmailDiv").show();
		}
		$('#districtEmail').css("background-color","");
		$('#emppos').css("background-color","");
		validateCandidateTypeForExternalJobApply();		
	}
}


