/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/

var pageFor = 1;
var noOfRowsFor = 10;
var sortOrderStrFor="";
var sortOrderTypeFor="";


function getPaging(pageno)
{
	if(pageno!='')
	{
		pageFor=pageno;	
	}
	else
	{
		pageFor=1;
	}
	noOfRowsFor 	= 	document.getElementById("pageSize").value;
	//entityID	=	document.getElementById("loggedInEntityType").value;
	displaySchoolMasterRecords(2);
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		pageFor=pageno;	
	}else{
		pageFor=1;
	}
	sortOrderStrFor=sortOrder;
	sortOrderTypeFor=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRowsFor = document.getElementById("pageSize").value;
	}else{
		noOfRowsFor=10;
	}
	//entityID	=	document.getElementById("loggedInEntityType").value;
	displaySchoolMasterRecords(2);
}

/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
/*======= Show District on Press Enter Key ========= */
        
function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}


/*========  displaySchoolMasterRecords ===============*/

var txtBgColor="#F5E7E1";
function displaySchoolMasterRecords(entityID)
{   
	$('#loadingDiv').show();
	var schoolnametext=document.getElementById("schoolName").value;
	var schoolId		=	document.getElementById("schoolId").value;
	var status	=	document.getElementById("status").value;		
	try
	{	
	    ManageSchoolAjax.displayRecordsByEntityType(true,entityID,schoolId,noOfRowsFor,pageFor,sortOrderStrFor,sortOrderTypeFor,status,schoolnametext,{ 
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			applyScrollOnTbl();	
			$('#loadingDiv').hide();
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}
}
/*========  activateDeactivateUser ===============*/
var entityID1="";
var schoolId1="";
var status1="";
function activateDeactivateSchool(entityID,schoolId,status)
{
	entityID1=entityID;
	schoolId1=schoolId;
	status1=status;
	$("#myModalactMsgShow").modal("show");	
}
function activateDeactivateSchoolstart() 
{
	try
	{	
		ManageSchoolAjax.activateDeactivateSchool(schoolId1,status1, { 
		async: true,
		callback: function(data)
		{			
			document.getElementById("updateMsg").innerHTML=resourceJSON.msgSuccessfullyUpdateSchool;
	    	$("#myModalUpdateMsg").modal("show");
	    	
			displaySchoolMasterRecords(entityID1)
		},
		errorHandler:handleError 
		});
	}
	catch(e)
	{alert(e);}
}


function showschoolDiv()
{
	$("#addschoolDiv").show();
	document.getElementById("enterSchoolName").value="";
	$('#enterSchoolName').focus();
	$('#saveSchool').show();
	$('#updateSchool').hide();
}
function saveSchool(entityType)
{
	  var cnt=0;
	  $('#errordiv').empty();	 
	  $('#enterSchoolName').css("background-color","");
	  var enterSchoolName=document.getElementById("enterSchoolName").value;
	  if(trim(enterSchoolName)=='')
	  {
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");
			if(cnt==0)
				$('#enterSchoolName').focus();
			
			$('#enterSchoolName').css("background-color",txtBgColor);
			cnt++;
	  }
	  
	  if(trim(enterSchoolName)!='')
	  {
		 if(checkschool(trim(enterSchoolName)))
		{
		    $('#errordiv').append("&#149; "+resourceJSON.msgSchoolAlreadyExist+"<br>");
			if(cnt==0)
				$('#enterSchoolName').focus();
			
			$('#enterSchoolName').css("background-color",txtBgColor);
			cnt++;
		}
	  }
	
	  if(cnt!=0)
	  {
				$('#errordiv').show();
				return false;
			
	  }
	  else
	  {
		  try
			{
			    ManageSchoolAjax.saveSchoolByUser(enterSchoolName,{ 
				async: true,
				callback: function(data)
				{		
			    	$('#loadingDiv').hide();
			    	document.getElementById("enterSchoolName").value="";
			    	$("#addschoolDiv").hide();
			    	
			    	document.getElementById("updateMsg").innerHTML=resourceJSON.msgsuccessfullySaveSchool;
			    	$("#myModalUpdateMsg").modal("show");
			    	displaySchoolMasterRecords(entityType);
				},
				errorHandler:handleError 
			});
			}catch(e){alert(e)}
	  }
}
var backupschoolid;
var backupschoolname;
function editSchool(schoolid,schoolName)
{
	backupschoolname=schoolName;
	 backupschoolid=schoolid;
	 $("#addschoolDiv").show();
	 $('#saveSchool').hide();
	 $('#updateSchool').show();
	 document.getElementById("enterSchoolName").value=schoolName;
	 $('#enterSchoolName').focus();	
}
function updateSchool(entityType)
{
	  var cnt=0;
	  $('#errordiv').empty();	 
	  $('#enterSchoolName').css("background-color","");
	  var enterSchoolName=document.getElementById("enterSchoolName").value;	
	  if(trim(enterSchoolName)=='')
	  {
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");
			if(cnt==0)
				$('#enterSchoolName').focus();
			
			$('#enterSchoolName').css("background-color",txtBgColor);
			cnt++;
	  }
	  if(trim(enterSchoolName)!=trim(backupschoolname))
	  {
		 if(checkschool(trim(enterSchoolName)))
		 {
		    $('#errordiv').append("&#149; "+resourceJSON.msgSchoolAlreadyExist+"<br>");
			if(cnt==0)
				$('#enterSchoolName').focus();
			
			$('#enterSchoolName').css("background-color",txtBgColor);
			cnt++;
		}
	  }
	  if(cnt!=0)
	  {
				$('#errordiv').show();
				return false;
			
	  }
	  else
	  {
		  try
			{
			    ManageSchoolAjax.updateSchoolByUser(backupschoolid,enterSchoolName,{ 
				async: true,
				callback: function(data)
				{	
			    	if(data==true)
			    	{
				    	document.getElementById("enterSchoolName").value="";
				    	$("#addschoolDiv").hide();
				    	document.getElementById("updateMsg").innerHTML=resourceJSON.msgSuccessfullyUpdateSchool;
				    	$("#myModalUpdateMsg").modal("show");
				    	displaySchoolMasterRecords(entityType);
			    	}
			    	else
			    	{
			    		alert(resourceJSON.msgServerErr);
			    	}	    	
				},
				errorHandler:handleError 
			});
			}catch(e){alert(e)}
	  }
}
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function checkschool(schoolname)
{
	var res;
	createXMLHttpRequest();  
	queryString = "checkschoolName.do?schoolname="+schoolname+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{	
				checkSession(xmlHttp.responseText)
				res = xmlHttp.responseText;					
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}
function checkSession(responseText)
{
	if(responseText=='100001')
	{
		alert(resourceJSON.oops)
		window.location.href="quest.do";
	}	
}
function cancelsaveuser()
{
	document.getElementById("enterSchoolName").value="";
	$("#addschoolDiv").hide();
}
function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);		
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	try
	{
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;		
		BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	}
	catch(e){alert(e)}
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*--------------------------------- End  School Auto Complete Js  ----------------------------------------------------------------*/
var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}
