var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));

var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId,districtOrSchooHiddenlId)
{
	
	document.getElementById(districtOrSchooHiddenlId).value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			
			$('#schoolName').attr('readonly', true);
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
			
		}
		else if(showDataArray && showDataArray[index]){
			
			dis.value=showDataArray[index];
			document.getElementById(districtOrSchooHiddenlId).value=hiddenDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

/*--- mukesh ------------------------------School Auto Complete Js Starts ----------------------------------------------------------------*/

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}



selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt1(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) || (charCode==44));
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}



// for paging and sorting
function getPaging(pageno)
{
        //alert("Paging");
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		searchRecordJobDescriptionLibrary();
	
}


function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	
	  if(pageno!=''){
			page=pageno;
			//alert("page :: "+page);
		}else{
			page=1;
			//alert("default");
		}
		sortOrderStr=sortOrder;
		//alert("sortOrderStr :: "+sortOrderStr);
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			//alert("AA")
			noOfRows = document.getElementById("pageSize").value;
		}else{
		//	alert("BB");
			noOfRows=50;
		}
		searchRecordJobDescriptionLibrary();
		
	
}

function activecityType(){
	document.getElementById("certType").value='';
}


function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGridEEC").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#cg'+j).tooltip();
		$('#cgn'+j).tooltip();
	}
}

//**************** global defined variable for exporting *******************
var f_JobOrderType     
var f_searchTextId	
var f_schoolId		
var f_jobOrderId	    
var	f_firstName       
var	f_lastName        
var	f_emailAddress    

//******************************************************************************

function searchRecordJobDescriptionLibrary()
{
//	alert("QQQQQQQQQQQ");
	$('#loadingDiv').fadeIn();
	
	
	var searchTextId =	document.getElementById("districtOrSchooHiddenlId").value;
	//var testvalueId =	document.getElementById("testvalueId").value;
	
	var jobcategoryId=0;
	//alert("searchTextId  "+searchTextId);
	
	//setTimeout(function(){
	if(searchTextId!=''){
		try{
			jobcategoryId=	document.getElementById("jobcategoryId").value;
		}
		catch(err) {}
		//	alert("jobcategoryId 55555 "+jobcategoryId);
		
	}
	
	var statusId =	document.getElementById("statusId").value;
	
	if(searchTextId=='')
		searchTextId=0;
	 
	  var status 		= 	"";
	  var endfromDate 	= 	"";
	  var endtoDate 	= 	"";
	 
	 /* try{
		  status	    =	trim(document.getElementById("status").value);  
	  }catch(e){}*/
	 /* try{
		  endfromDate	=	trim(document.getElementById("endfromDate").value);  
	  }catch(e){}
	  try{
		  endtoDate     =	trim(document.getElementById("endtoDate").value);
	  }catch(e){}*/
	  
	  DistrictJobDescriptionLibraryAjax.displayRecordsByEntityType(searchTextId,noOfRows,page,sortOrderStr,sortOrderType,jobcategoryId,statusId,{ 
		async: true,
		callback: function(data)
		{	
			document.getElementById("divMainOfferReady").innerHTML	=data;
			$('#loadingDiv').hide();
		    applyScrollOnTblOfferReady();
		},
	errorHandler:handleError
	});
	//}, 100);
}

function jobcategorybaseondistrict()

{
	//alert("0000");
	$('#jobCategoryDiv').show();
	
	//alert("5555");
	var districtid = trim(document.getElementById('districtOrSchooHiddenlId').value);
	if(districtid=='')
		districtid=0;
	$('#jobcategoryIdid').empty();
	$('#loadingDiv').show();
	DistrictJobDescriptionLibraryAjax.jobcategoryaList(districtid,{ 
		async: true,
		callback: function(data)
		{	
					$("#jobcategoryIdid").html(data);
					$("#jobCategoryDiv").show();
					$('#loadingDiv').hide();
		},
	errorHandler:handleError
	});
	}


function cleanformField()
{
	var disvalue=document.getElementById("districtrelatedId").value;
	if(disvalue==''){
		document.getElementById("districtSchoolName").value='';
		document.getElementById("districtOrSchooHiddlId").value='';
	}
	//document.getElementById("jobcategoryfordescriptionId").value='0';
	document.getElementById("districtjobdescriptionnameId").value='';
	document.getElementById("addeditstatusId").value='0';
	
		$(".jqte_editor").html('');
}


function validationMethod()
{
	
	var counter=0;
	$('#errordiv').empty();
	$('#districtSchoolName').css("background-color", "#ffffff");
	$('#jobcategoryfordescriptionId').css("background-color", "#ffffff");
	$('#districtjobdescriptionnameId').css("background-color", "#ffffff");
	$('#addeditstatusId').css("background-color", "#ffffff");
	
	var districtId=document.getElementById("districtOrSchooHiddlId").value;
	
	
	var districtjobdescriptionname=document.getElementById("districtjobdescriptionnameId").value;
	var addeditstatusId=document.getElementById("addeditstatusId").value;
	
	
	
	
			if(districtId==''){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
				//alert("TTTT");
				$('#districtSchoolName').focus();
				$('#districtSchoolName').css("background-color", "#F5E7E1");
				counter++;
			}
			//alert("districtId "+districtId);
			if(districtId!=''){
				//alert("1111");
				var jobcategoryId=document.getElementById("jobcategoryfordescriptionId").value;
				if(jobcategoryId=='0'){
					//alert("222");
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgJobCategoryName+"<br>");
					$('#jobcategoryfordescriptionId').focus();
					$('#jobcategoryfordescriptionId').css("background-color", "#F5E7E1");
					counter++;
				}
			}
			
	if(districtjobdescriptionname==''){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseEnterDescription+"<br>");
		$('#districtjobdescriptionnameId').focus();
		$('#districtjobdescriptionnameId').css("background-color", "#F5E7E1");
		counter++;
	}
	
	if(addeditstatusId=='0'){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzsetStatus+"<br>");
		$('#addeditstatusId').focus();
		$('#addeditstatusId').css("background-color", "#F5E7E1");
		counter++;
	}
	if(counter!=0)
		return false;
	else
		return true;
}
function divclosemethod()
{
	cleanformField();
	$('#errordiv').empty();
	$('#errordiv').hide();
	
	$('#divAddEditRecord').hide();
}

function jobcategorybaseondistrictAddEdit()
{
	
	var districtid = trim(document.getElementById('districtOrSchooHiddlId').value);
	if(districtid=='')
		districtid=0;
	else
		$('#jobcategoryDivId123').show();
	
	$('#jobcategoryeditId').empty();
	//$('#loadingDiv').show();
	DistrictJobDescriptionLibraryAjax.jobcategoryaEditList(districtid,{ 
		async: false,
		callback: function(data)
		{	
					$("#jobcategoryeditId").html(data);
					if(districtid!='')
						$('#jobcategoryDivId123').show();
					else
						$('#jobcategoryDivId123').hide();
					//	$('#loadingDiv').hide();
					
		},
	errorHandler:handleError
	});
	return true;
	}


////////////////////////////////////////////////////////////////////////////////////////////


function addEditdistrictjobdescriptionLibrary(){
	
	var validStatus=validationMethod();
	//alert("validStatus  "+validStatus);
	var districtId='';
	var disvalue=document.getElementById("districtrelatedId").value;
	
	if(disvalue!='')
		districtId=document.getElementById("districtOrSchooHiddlId").value;
	else
		districtId=document.getElementById("districtOrSchooHiddlId").value;
	if(validStatus==true){
	var districtjobdescriptionId=document.getElementById("districtjobcateLibraryID").value
	
		
	//alert("11111");
	var jobcategoryId=document.getElementById("jobcategoryfordescriptionId").value;
//	alert("jobcategoryId  "+jobcategoryId);
	//alert("22222");
	var districtjobdescriptionname=document.getElementById("districtjobdescriptionnameId").value;
	var addeditstatusId=document.getElementById("addeditstatusId").value;
	var jobdescriptionvalue=trim($('#jobdescriptionNote').find(".jqte_editor").html());
	//alert("jobdescription   "+jobdescriptionvalue);
	$('#loadingDiv').show();
	DistrictJobDescriptionLibraryAjax.addEditdescriptionLibrary(districtjobdescriptionId,districtId,jobcategoryId,districtjobdescriptionname,jobdescriptionvalue,addeditstatusId,{ 
		async: true,
		callback: function(data)
		{	
		cleanformField();
		$('#divAddEditRecord').hide();
		searchRecordJobDescriptionLibrary();
				$('#loadingDiv').hide();
		},
	errorHandler:handleError
	});
	
	}
	else
	{
			//alert("districtId 5555 "+districtId);
			if(districtId==''){
				//alert("hiiiiiiiiiiiiiii");
				//document.getElementById("jobcategoryDivId123").style.visibility = "hidden";
			//	document.getElementById("jobcategoryDivId123").style.display = 'none';
			$('#jobcategoryDivId123').hide();
			}
		
			
	}
	
}

function addnewJobDescriprion(actionType,recordID)
{
	
	
	$('#errordiv').empty();
	$('#districtSchoolName').css("background-color", "#ffffff");
	$('#jobcategoryfordescriptionId').css("background-color", "#ffffff");
	$('#districtjobdescriptionnameId').css("background-color", "#ffffff");
	$('#addeditstatusId').css("background-color", "#ffffff");
	$('#jobcategoryDivId123').hide();
	if(actionType=='0'){
		 $('#divAddEditRecord').show();
		cleanformField();
		document.getElementById("districtjobcateLibraryID").value=0;
	
	var	districtId=document.getElementById("districtOrSchooHiddlId").value;
	if(districtId!=''){
		//alert("WWW");
		jobcategorybaseondistrictAddEdit();
	}
		/*setTimeout(function(){
		  }, 100);*/
	}else{
	
		document.getElementById("districtjobcateLibraryID").value=recordID;
		$('#loadingDiv').show();
		DistrictJobDescriptionLibraryAjax.editDistrictJobDescriptionLibrary(recordID,{ 
			async: true,
			callback: function(data)
			{	
						cleanformField();
						var obj = JSON.parse(data);
						
						var disvalue=document.getElementById("districtrelatedId").value;
						
						//if(disvalue==''){
							document.getElementById("districtSchoolName").value=obj.districtname;
							document.getElementById("districtOrSchooHiddlId").value=obj.districtId;
					//	}
							//alert("33333");
							//jobcategorybaseondistrictAddEdit();
							//alert("jobcategorybaseondistrictAddEdit()  "+jobcategorybaseondistrictAddEdit());
							if(jobcategorybaseondistrictAddEdit()){
							//alert("11111");
						//setTimeout(function(){
						document.getElementById("addeditstatusId").value=obj.status;
						document.getElementById("districtjobdescriptionnameId").value=obj.jobdescriptionname;
						$(".jqte_editor").html(obj.jobdescription);
						 document.getElementById("jobcategoryfordescriptionId").value=obj.jobcategoryId;
						 $('#divAddEditRecord').show();
						 $('#loadingDiv').hide();
						 
						// }, 400);
							}
							
			},
		errorHandler:handleError
		});
	}
}
function cleandistrictfield(){	
	 jQuery("#districtORSchoolName").change(function(){
		 if(jQuery(this).val()==""){
			 $('#jobcategoryIdid').empty();
			 $('#jobCategoryDiv').hide();
		}
		});
}


function activateDeactivateDescription(descriptionid,action){
	//alert("alert "+descriptionid+"  ssds  "+action);
	
	DistrictJobDescriptionLibraryAjax.activeANDDeactivate(descriptionid,action,{ 
		async: true,
		callback: function(data)
		{	
		searchRecordJobDescriptionLibrary();
					
		},
	errorHandler:handleError
	});
	
}
	


