var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var previousDistrictId=0;
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
	noOfRows = document.getElementById("pageSize").value;
	var entityID	=	document.getElementById("entityID").value;
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}
	displayJobCategories();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}
		displayJobCategories();
}
//***************** Adding by deepak *****************************

function addNewJobCategory(flag)
{
	$(".selectJobCategory").hide();
	$('#subjobcategory').val("");
	$("select#parentJobCategoryId")[0].selectedIndex = 0;
	$("#jobcategoryId").val("");
	$("#jobcategory").val("");
	$('#jobCatDiv').show();
	$(".scButton").show();
	$('#maingrid').hide();
	$(".actionDiv").hide();
	$('.typeDiv').addClass('col-sm-11 col-md-11 widthDiv');
	$('.typeDiv').removeClass('col-sm-10 col-md-10 divWidth');
	$("#section0").attr("checked",true);
	$("#section1").attr("checked",true);
	$("#section2").attr("checked",true);
	hideOrShowCondidateType(0);
    if(flag==1)
    {
    	$('#externalSection').addClass('disabled');
    	$('#internalSection').addClass('disabled');
    	$('#internalTransferSection').addClass('disabled'); 
    	
    	$('#createJobCategory').prop("checked",true);
    	$('.newJobCategory').show();
    	$("#Searchbox").hide();
    	//************** if district already selected than copy same ******************
    	var districtIdFilter=$("#districtIdFilter").val();
    	$('#createJobCategory').prop("checked",true);
    	//$(".golive").show();
    	var userType=$("#userType").val();
    //************* End ***********************************
    //var manageclass2=$(".managewidth").attr("class").substring(30,48);
    //$(".managewidth").removeClass(manageclass2);
    if($("#districtMasterId").val()!=0){
		$("#districtName").val($("#districtMasterName").val());
		$("#districtId").val($("#districtMasterId").val());
		//$(".golive").show();
		$('#jobcategory').focus();
		$('#loadingDiv_jobCatGrid').show();
		 setTimeout(function() {multipleAjaxMethods();}, 50);
	}else{
		$("#districtName").val("");
		$("#districtId").val(0);
	    $('#districtName').focus();
	}
    enableDisableApprovalsFirst();
    } 
    $("#addJobCategory").hide();
    $("#backJobCategory").show();
    defaultInactiveLayout();
}
//*************************** End *****************************
function cancelJobCateDiv(id)
{
	var userType=$("#userType").val();
	$('#jobCatDiv').hide();
	$('#maingrid').show();
	$(".selectJobCategory").hide();
	$('#subjobcategory').val("");
	$("select#parentJobCategoryId")[0].selectedIndex = 0;
	if(id==1){
			if($("#districtIdFilter").val()==null || $("#districtIdFilter").val()==""){
			$("#districtNameFilter").val("");
			$("#districtIdFilter").val("");
			}else{
				$('#districtName').val($("#districtNameFilter").val());
				$("#districtId").val($("#districtIdFilter").val());
				$("#districtMasterName").val($("#districtNameFilter").val());
				$("#districtMasterId").val($("#districtIdFilter").val());
				$("#jobcategory").focus();
			}
	}else{
		if(userType!=2){
			$("select#entityType")[0].selectedIndex = 0;
			$("#districtId").val("");
		}
	    $('#districtName').val("");
	}
	$('#districtName').css("background-color", "");
	$('#quesName').val("");
	$('#timePerQues').val("");
	$('#vviExpDays').val("");
	$('#maxScoreForVVI').val("");
	$("#previousDistrictId").val("");
	$("#jobcategoryId").val("");
	$("#jobcategory").val("");
	$('#errordiv').hide();
	//$(".golive").hide();
	$("#Searchbox").hide();
	$('#jobcategory').css("background-color", "");
	//************** For the Approval Process reset *********
	$("#groupApprovalDiv").hide();
	$("#groupApprovalDiv1").hide();
	$(".hideGroup").fadeOut(500);
	//************** For the VVI reset *********
	$("#offerVVIDiv").hide();
	$("#quesName").val("");
	$("#timePerQues").val("");
	$("#vviExpDays").val("");
	$("#marksDiv").hide();
	$("#autolinkDiv").hide();
	//************ For the Online Assessment **************************
	$('.onlineAssessment').hide();
	$("#multiDisAdminDiv").hide();
	//************ For the E-Reference Question set **************************
	$('.EeferenceQuestionSetMain').hide();
	$(".scButton").hide();
	$(".jobSpecificInventoryMain").hide();
	$('.QualificationQuestion').hide();
	$('.QualificationQuestionOnBording').hide();
	$("#section0").attr("checked",true);
	$("#section1").attr("checked",true);
	$("#section2").attr("checked",true);
	hideOrShowCondidateType(0);
	$(".externalDiv").show();
	$(".internalDiv").show();
	$(".internalTDiv").show();
	$("#approvalBeforeGoLive1").prop("checked",false);
	$("#buildApprovalGroups").prop("checked",false);
	enableDisableApprovalsFirst();
	$("select#noOfApprovalNeeded")[0].selectedIndex = 0;
	//************************* Reset checkboxes ***************************
	resetLayout();
	$("#addJobCategory").show();
    $("#backJobCategory").hide();
}
/*========  Search District ===============*/
function displayOrHideSearchBox()
{ 
	districtNameFilter=1;
	var entityID	=	document.getElementById("entityID").value;
	if(entityID==1){
		document.getElementById("districtNameFilter").value="";
		document.getElementById("districtIdFilter").value="";
		var searchEntityType=$('#entityType').val();
		if(searchEntityType == 2)
		{
			document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrict;
			$("#districtClass").show();
			$("#districtSearchBox").show();
			$("#Searchbox").show();
			$("#districtClassMaster").show();
			$('#districtNameFilter').focus();
		}else{
			$("#Searchbox").hide();
			$("#districtClass").hide();
			$("#districtSearchBox").hide();
			$("#districtClassMaster").hide();
		}
	}
}
function chkForEnterSearchJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		page = 1;
		displayJobCategories();
	}	
}
function searchJobCat()
{
	page = 1;
	var op=document.getElementById("entityType");
	searchEntityType=op.options[op.selectedIndex].value;
	if(searchEntityType == 1)
	{
		$("#districtMasterId").val(0);
	}else{
		$("#districtNameFilter").css("background-color", "");
		 if($("#districtIdFilter").val()==""){
			 $("#districtNameFilter").css("background-color", "#F5E7E1");
			 $("#districtNameFilter").focus();
			 return false; 
		 }else{
			if($('#districtId').val()==undefined){
				$('#districtId').val($("#districtMasterId").val());
			}else{
				$("#districtMasterId").val($("#districtId").val());
				$("#districtMasterName").val($("#districtNameFilter").val());
			}
			if($('#districtIdFilter').val()==undefined && $('#districtIdFilter').val()!=null){
				$('#districtIdFilter').val($("#districtMasterId").val());
			}else{
				$("#districtMasterId").val($("#districtIdFilter").val());
				$("#districtMasterName").val($("#districtNameFilter").val());
			}
		 }
	}
	displayJobCategories();
	hideSearchAgainMaster();
	
}
function displayJobCategories()
{	
	var resultFlag=false;
	var entityID	=	document.getElementById("entityID").value;
	var searchTextId=0;
	var searchEntityType
	if(entityID==1)
	{
		var op=document.getElementById("entityType");
		searchEntityType=op.options[op.selectedIndex].value;
		if(searchEntityType == 2)
		{
			if($("#districtIdFilter").val()!=undefined || $("#districtIdFilter").val()!=""){
			searchTextId	=	document.getElementById("districtIdFilter").value;
			$('#districtId').val($("#districtIdFilter").val());
			}else{
				searchTextId	=	$("#districtMasterId").val();
				$('#districtId').val($("#districtMasterId").val());
			}
		}else{
			if($("#districtMasterId").val()==0){
			searchTextId=0;
			resultFlag=false;	
			}else{
				searchTextId	=	$("#districtMasterId").val();
				$('#districtId').val($("#districtMasterId").val());
				resultFlag=true;	
			}
		}
	}
	else
	{
		searchTextId=0;
		resultFlag=true;
	}
	var districtId = $('#districtId').val();
	  $('#loadingDiv_jobCatGrid').show();
	 JobCategoryAjaxNew.displayJobCategories(resultFlag,searchEntityType,entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: false,
		callback: function(data){
		$('#jobCategoryGrid').html(data);
		applyScrollOnTbl();
		$(".tempToolTip").tooltip();
		$('#loadingDiv_jobCatGrid').hide();
	},
	}); 
		if($("#districtMasterId").val()!=0){
			hideSearchAgainMaster();
		}else{
			showSearchAgainMaster();
			}
}

function chkForEnterSaveJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveJobCategory();
	}	
}

//********************** Adding By Deepak saveJobCategory************************
function saveJobCategory()
{ 
var flag=false; 
flag=mainGridActiveInactiveValidation();
	if(flag)
	{
		return flag;
	}
	else
	{
		$('#loadingDiv').show();
		$('#errordiv').empty();
		var jobcategoryName="";
		var candidatetypestatus=[];
		
		if($("#section0").is(":checked"))
			candidatetypestatus[0]=1;
		else
			candidatetypestatus[0]=0;
		if($("#section1").is(":checked"))
			candidatetypestatus[1]=1;
		else
			candidatetypestatus[1]=0;
		
		if($("#section2").is(":checked"))
			candidatetypestatus[2]=1;
		else
			candidatetypestatus[2]=0;
		
		var parentJobCategoryId=0;
		var jobCategoryId=0;
		var editClone=$("#editClone").val();
		if(editClone==1)
			jobCategoryId=trim(document.getElementById('jobcategoryId').value);
				
		if($("#createJobCategory").is(":checked"))
		{
			jobcategoryName=trim(document.getElementById('jobcategory').value);
		}
		else
		{
			jobcategoryName=trim(document.getElementById('subjobcategory').value);
			parentJobCategoryId=$('#parentJobCategoryId').val();
		}
		
		var districtId = $('#districtId').val();
		var record0=record1=record2="";
		var arr=['E','I','T'];
		var temp,midValue;
		for(var i=0;i<3;i++)
		{ 
			temp="";
			
			for(var j=0;j<8;j++)
			{
				midValue="";
				if(j==3){
					midValue=$("#"+(j+1)+arr[i]+arr[i]+"id").val()+";"+$("#etypeValue"+(i+1)).val();
				}else{
					midValue=$("#"+(j+1)+arr[i]+arr[i]+"id").val();
				}
					
				temp+=(j+1)+"-"+midValue+":"+$("#"+(j+1)+"_"+(j+1)+"_"+(i+1)).val()+"|";
			}
			
			if(i==0)
				record0=temp;
			if(i==1)
				record1=temp;
			if(i==2)
				record2=temp;
		}
		
		//************* VVI additional field value **********************************
		var maxScoreForVVI;
		var sendAutoVVILink;
		var vviExpDays;
		var timePerQues;
		var approvalByPredefinedGroups;
		var schoolSelection=false;
		var vFlag=false;
		vFlag=checkSectionActiveOrInactive(6);
		if(vFlag)
		{
		if($("#maxScoreForVVIHidden").val()!=null){
			maxScoreForVVI=$("#maxScoreForVVIHidden").val();
		}
		
		if($("#sendAutoVVILinkHidden").val()!=null){
			sendAutoVVILink=$("#sendAutoVVILinkHidden").val();
		}
		var statusDiv=$("#statusDivHidden").val();
		
		if($("#timePerQuesHidden").val()!=null){
			timePerQues=$("#timePerQuesHidden").val();
		}
		
		if($("#vviExpDaysHidden").val()!=null){
			vviExpDays=$("#vviExpDaysHidden").val();
		}
		approvalByPredefinedGroups=$("#approvalByPredefinedGroupsHidden").val();
		}
		
		var assessmentflag=checkSectionActiveOrInactive(7);
			if(assessmentflag){
				if($("#schoolSelectionHidden").is(":checked"))
					schoolSelection=1;
				else
					schoolSelection=0;
			}else{
				schoolSelection=-1;
			}
		//************************ Approval Process  **********************************************
		var approvalBeforeGoLive;
		if($("#approvalBeforeGoLive1").is(":checked")){
			approvalBeforeGoLive=1;
		}else{
			approvalBeforeGoLive=0;
		}
		var noOfApprovalNeeded=$("#noOfApprovalNeeded").val();
		var buildApprovalGroup=0;
		if($("#buildApprovalGroups").is(":checked")){
			buildApprovalGroup=1;
		}
          JobCategoryAjaxNew.saveJobCategory(districtId,candidatetypestatus,jobCategoryId,jobcategoryName,parentJobCategoryId,record0,record1,record2,maxScoreForVVI,sendAutoVVILink,statusDiv,timePerQues,vviExpDays,approvalByPredefinedGroups,schoolSelection,approvalBeforeGoLive,noOfApprovalNeeded,buildApprovalGroup,editClone,{			
            async: false,
			callback: function(data){
	    		if(data=="true"){
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgDuplicateJobCategory+"<br>");
					if($("#createJobCategory").is(":checked"))
					{
						$('#jobcategory').focus();
						$('#jobcategory').css("background-color", "#F5E7E1");	
					}
					else
					{
						$('#subjobcategory').focus();
						$('#subjobcategory').css("background-color", "#F5E7E1");	
					}
						
				}else{
					if($("#buildApprovalGroups").is(":checked")){
						addOrUpdateDistrictApprovalGroupJobCategorywise(data);
					}
					cancelJobCateDiv();
				}	
				$('#loadingDiv').hide();
				displayJobCategories();
			},errorHandler:handleError
		});
		
	}
}

function customQuestionMO(flag)
{
	if(flag==1)
	{
		if($("#ExtofferJSIRadioOptionalE").prop("checked")==true)
			return "0"+"#";
		else if( $("#ExtofferJSIRadioMaindatoryE").prop("checked")==true)
			return "1"+"#";
		else
			return "false";	
	}
	if(flag==2)
	{
		if($("#ExtofferJSIRadioOptionalI").prop("checked")==true)
			return "0"+"#";
		else if( $("#ExtofferJSIRadioMaindatoryI").prop("checked")==true)
			return "1"+"#";
		else
			return "false";	
	}
	if(flag==3)
	{
		if($("#ExtofferJSIRadioOptionalT").prop("checked")==true)
			return "0"+"#";
		else if( $("#ExtofferJSIRadioMaindatoryT").prop("checked")==true)
			return "1"+"#";
		else
			return "false";	
	}
	
}

//********************** Adding by deepak *********************************
function editJobCategory(type,jobCategoryId)
{
	var entityID= $("#entityID").val();
	$('#errordiv').empty();
	$(".actionDiv").show();
	$('.typeDiv').removeClass('col-sm-11 col-md-11 widthDiv');
	$('.typeDiv').addClass('col-sm-10 col-md-10 divWidth');
	$('#jobcategory').css("background-color", "");
	$('#districtName').css("background-color", "");
	$('#quesName').css("background-color", "");
	$('#maxScoreForVVI').css("background-color", "");
	$('#jobCatDiv').show();
	$(".scButton").show();
	$('#maingrid').hide();
	$(".actionDiv").hide();
	$('.typeDiv').addClass('col-sm-11 col-md-11 widthDiv');
	$('.typeDiv').removeClass('col-sm-10 col-md-10 divWidth');
	$("#editMode").val(1);
	$("#editClone").val(type);
	$('#loadingDiv_jobCatGrid').show();
	JobCategoryAjaxNew.editJobCategoryId(jobCategoryId,{
		async: false,
		callback: function(data){
			if(document.getElementById('entityID').value==1 || document.getElementById('entityID').value==2){
				if(entityID!=2){
				document.getElementById('districtId').value=data['districtid'];
				document.getElementById('districtName').value=data['districtname'];
				}
				$('#ApprovalJobCategoryId').val(data['jobCategoryId']);
				$('#jobcategoryId').val(data['jobCategoryId']);
				if(type==1){
					if(data['parentJobCategoryId']==null || data['parentJobCategoryId']=="")
					{
						document.getElementById('createJobCategory').checked=true;
						hideOrShowCreateJob(1);
						$("#jobcategory").val(data['jobCategoryName']);
					}
					else{
						document.getElementById('selectJobCategory').checked=true;
						hideOrShowCreateJob(2);
						$("#subjobcategory").val(data['jobCategoryName']);
						$('#parentJobCategoryId option:selected').val(data['parentJobCategoryId'])
					}
				}
				else
				{
					document.getElementById('createJobCategory').checked=true;
					$("#jobcategory").val("");
					hideOrShowCreateJob(1);
				}
				if(data['schoolSelection']==1)
					$("#schoolSelection").prop("checked",true);
				else
					$("#schoolSelection").prop("checked",false);
				
				if(data['approvalByPredefinedGroups']==1){
					$("#approvalByPredefinedGroups").prop("checked",true);
					$('#approvalBPGroupsDiv').show();
				}
				else{
					$("#approvalByPredefinedGroups").prop("checked",false);
					$('#approvalBPGroupsDiv').hide();
				}
				
				//*********** Approval Process ****************************
				//$(".golive").show();
				if(data['approvalBeforeGoLive']==1){		
					$("#approvalBeforeGoLive1").attr("checked","checked");
					enableDisableApprovals();
					$('#noOfApprovalNeeded option[value="'+data['noOfApprovalNeeded']+'"]').attr("selected", "selected");
					if(data['buildApprovalGroup']==1){
						$("#buildApprovalGroups").attr("checked","checked");
						enableDisableApprovalsFirst();
					}
					}
				//********************************* All Category Status fields *******************************
				setTimeout(function(){
				if(editmultipleAjaxMethods()){     
				setTimeout(function(){ 
				var candType=[resourceJSON.lblExternal,resourceJSON.lblInternal,resourceJSON.lblInternalTransfer];
				var assessmentArr=['asses0','asses1','asses2']
				var editEmptyMsg=[resourceJSON.msgNoPortfolioConfigured,resourceJSON.msgNoPreScreenConfigured,resourceJSON.msgNoEPIConfigured,resourceJSON.msgNoCustomQuestionConfigured,resourceJSON.msgNoOnBoardingConfigured,resourceJSON.msgNoVVIConfigured,resourceJSON.msgNoAssessmentConfigured,resourceJSON.msgNoEReferenceConfigured];
				//*********** VVI Master Column  ****************************
				if(data['sendAutoVVILink']){
					$("#sendAutoVVILinkHidden").val(1);
					$("#sendAutoVVILink").prop("checked",true);
					showLinkDivNew();
					var statusIdForAutoVVILink=null;
					var secondaryStatusIdForAutoVVILink=null;
					if(data['statusIdForAutoVVILink']!=null){
					statusIdForAutoVVILink=data['statusIdForAutoVVILink'];
					$('#statusDiv option[value="'+statusIdForAutoVVILink+'"]').attr("selected", "selected");
					}else if(data['secondaryStatusIdForAutoVVILink']!=null){
					 secondaryStatusIdForAutoVVILink=data['secondaryStatusIdForAutoVVILink'];
					 $('#statusDiv option[value="SSID_'+statusIdForAutoVVILink+'"]').attr("selected", "selected");
					}
				}
				if(data['maxScoreForVVI']!=null){
					$("#wantScore").prop("checked",true);
					showMarksDiv();
				   $("#maxScoreForVVI").val(data['maxScoreForVVI']);
				   $("#maxScoreForVVIHidden").val(data['maxScoreForVVI']);
				}
				$("#timePerQues").val(data['timePerQues']);
				$("#timePerQuesHidden").val(data['timePerQues']);
				$("#vviExpDays").val(data['vviExpDays']);
				$("#vviExpDaysHidden").val(data['vviExpDays']);
				//***********************************************************************
				var arr=['E','I','T'];
				for(var i=0;i<arr.length;i+=1)
				{
					
					if(data[i+"status"+arr[i]]=="A"){
						$("#section"+i).prop("checked",true);
						hideOrShowCondidateType(Number(i)+1);
					}else{
						$("#section"+i).prop("checked",false);
					}
					$('#colId'+i+' option[value="'+data[i+'portfolio'+arr[i]]+'"]').attr("selected", "selected");
					//alert("portfolio ::::::: "+data[i+'portfolio'+arr[i]]);
					if(data[i+'portfolio'+arr[i]]==null){
						$("#1"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
						$("#1"+arr[i]).css("background-color","#73D2EF");
						$("#1"+arr[i]+arr[i]).val("");
						$("#1"+arr[i]+arr[i]+"id").val(0);
						$("#1"+arr[i]).html(resourceJSON.instN);
						$("#1_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						deactivateSection(1,(i+1));
					}else if(data[i+'portfolio'+arr[i]]!=null){
						$("#1"+arr[i]).attr("data-original-title",$("#colId"+i+" option:selected").text());
						$("#1"+arr[i]+arr[i]).val($("#colId"+i+" option:selected").text());
						$("#1"+arr[i]+arr[i]+"id").val(data[i+'portfolio'+arr[i]]);
						$("#1"+arr[i]).css("background-color","rgb(0, 147, 294)");
						$("#1"+arr[i]).html(resourceJSON.instC);
						if(data[i+'portfolioStatus'+arr[i]]=="A" || data[i+"status"+arr[i]]==0)
							$("#1_"+(i+1)).attr("data-original-title",resourceJSON.msgInactive);
						else
							$("#1_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						
						deactivateSection(1,(i+1));
						$("#1_1_"+(i+1)).val(data[i+'portfolioStatus'+arr[i]]);
					}
					$('#qqAvlbList'+i+' option[value="'+data[i+'prescreen'+arr[i]]+'"]').attr("selected", "selected");
					if(data[i+'prescreen'+arr[i]]==null){
						$("#2"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
						$("#2"+arr[i]).css("background-color","#73D2EF");
						$("#2"+arr[i]+arr[i]).val("");
						$("#2"+arr[i]+arr[i]+"id").val(0);
						$("#2"+arr[i]).html(resourceJSON.instN);
						$("#2_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						deactivateSection(2,(i+1));
					}else if(data[i+'prescreen'+arr[i]]!=null){
						$("#2"+arr[i]).attr("data-original-title",$("#qqAvlbList"+i+" option:selected").text());
						$("#2"+arr[i]+arr[i]).val($("#qqAvlbList"+i+" option:selected").text());
						$("#2"+arr[i]+arr[i]+"id").val(data[i+'prescreen'+arr[i]]);
						$("#2"+arr[i]).css("background-color","rgb(0, 147, 294)");
						$("#2"+arr[i]).html(resourceJSON.instC);
						if(data[i+'prescreenStatus'+arr[i]]=="A" || data[i+"status"+arr[i]]==0)
							$("#2_"+(i+1)).attr("data-original-title",resourceJSON.msgInactive);
						else
							$("#2_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						
						deactivateSection(2,(i+1));
						$("#2_2_"+(i+1)).val(data[i+'prescreenStatus'+arr[i]]);
					}
					$('#EPIId'+i+' option[value="'+data[i+'epi'+arr[i]]+'"]').attr("selected", "selected");
					if(data[i+'epi'+arr[i]]==null){
						$("#3"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
						$("#3"+arr[i]).css("background-color","#73D2EF");
						$("#3"+arr[i]+arr[i]).val("");
						$("#3"+arr[i]+arr[i]+"id").val(0);
						$("#3"+arr[i]).html(resourceJSON.instN);
						$("#3_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						deactivateSection(3,(i+1));
					}else if(data[i+'epi'+arr[i]]!=null){
						$("#3"+arr[i]).attr("data-original-title",$("#EPIId"+i+" option:selected").text());
						$("#3"+arr[i]+arr[i]).val($("#EPIId"+i+" option:selected").text());
						$("#3"+arr[i]+arr[i]+"id").val(data[i+'epi'+arr[i]]);
						$("#3"+arr[i]).css("background-color","rgb(0, 147, 294)");
						$("#3"+arr[i]).html(resourceJSON.instC);
						
						if(data[i+'epiStatus'+arr[i]]=="A" || data[i+"status"+arr[i]]==0)
							$("#3_"+(i+1)).attr("data-original-title",resourceJSON.msgInactive);
						else
							$("#3_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						
						deactivateSection(3,(i+1));
						$("#3_3_"+(i+1)).val(data[i+'epiStatus'+arr[i]]);
					}
					$('#JSIId'+i+' option[value="'+data[i+'custom'+arr[i]]+'"]').attr("selected", "selected");
					if(data[i+'custom'+arr[i]]==null){
						$("#4"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
						$("#4"+arr[i]).css("background-color","#73D2EF");
						$("#4"+arr[i]+arr[i]).val("");
						$("#4"+arr[i]+arr[i]+"id").val(0);
						$("#4"+arr[i]).html(resourceJSON.instN);
						$("#4_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						deactivateSection(4,(i+1));
					}else if(data[i+'custom'+arr[i]]!=null){
						$("#4"+arr[i]).attr("data-original-title",$("#JSIId"+i+" option:selected").text());
						$("#4"+arr[i]+arr[i]).val($("#JSIId"+i+" option:selected").text());
						$("#4"+arr[i]+arr[i]+"id").val(data[i+'custom'+arr[i]]);
						$("#4"+arr[i]).css("background-color","rgb(0, 147, 294)");
						$("#4"+arr[i]).html(resourceJSON.instC);
						
						if(data[i+'customStatus'+arr[i]]=="A" || data[i+"status"+arr[i]]==0)
							$("#4_"+(i+1)).attr("data-original-title",resourceJSON.msgInactive);
						else
							$("#4_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						
						deactivateSection(4,(i+1));
						$("#4_4_"+(i+1)).val(data[i+'customStatus'+arr[i]]);
					}
					$("#etypeValue"+(i+1)).val(data[i+'customMandOpt'+arr[i]]);
					resetOptMend((i+1),2);
					$('#qqAvlbListForOnboard'+i+' option[value="'+data[i+'onboarding'+arr[i]]+'"]').attr("selected", "selected");
					if(data[i+'onboarding'+arr[i]]==null){
						$("#5"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
						$("#5"+arr[i]).css("background-color","#73D2EF");
						$("#5"+arr[i]+arr[i]).val("");
						$("#5"+arr[i]+arr[i]+"id").val(0);
						$("#5"+arr[i]).html(resourceJSON.instN);
						$("#5_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						deactivateSection(5,(i+1));
					}else if(data[i+'onboarding'+arr[i]]!=null){
						$("#5"+arr[i]).attr("data-original-title",$("#qqAvlbListForOnboard"+i+" option:selected").text());
						$("#5"+arr[i]+arr[i]).val($("#qqAvlbListForOnboard"+i+" option:selected").text());
						$("#5"+arr[i]+arr[i]+"id").val(data[i+'onboarding'+arr[i]]);
						$("#5"+arr[i]).css("background-color","rgb(0, 147, 294)");
						$("#5"+arr[i]).html(resourceJSON.instC);
						
						if(data[i+'onboardingStatus'+arr[i]]=="A" || data[i+"status"+arr[i]]==0)
							$("#5_"+(i+1)).attr("data-original-title",resourceJSON.msgInactive);
						else
							$("#5_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						
						deactivateSection(5,(i+1));
						$("#5_5_"+(i+1)).val(data[i+'onboardingStatus'+arr[i]]);
					}
					if(data[i+'vvi'+arr[i]]==null){
						$("#6"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
						$("#6"+arr[i]).css("background-color","#73D2EF");
						$("#6"+arr[i]+arr[i]).val("");
						$("#6"+arr[i]+arr[i]+"id").val(0);
						$("#6"+arr[i]).html(resourceJSON.instN);
						$("#6_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						deactivateSection(6,(i+1));
					}else if(data[i+'vvi'+arr[i]]!=null){
						$("#6"+arr[i]).attr("data-original-title",data['vviQuesName'+i]);
						$("#6"+arr[i]+arr[i]).val(data['vviQuesName'+i]);
						$("#6"+arr[i]+arr[i]+"id").val(data[i+'vvi'+arr[i]]);
						$("#6"+arr[i]).css("background-color","rgb(0, 147, 294)");
						$("#6"+arr[i]).html(resourceJSON.instC);
						
						if(data[i+'vviStatus'+arr[i]]=="A" || data[i+"status"+arr[i]]==0)
							$("#6_"+(i+1)).attr("data-original-title",resourceJSON.msgInactive);
						else
							$("#6_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						
						deactivateSection(6,(i+1));
						$("#quesName"+i).val(data['vviQuesName'+i]);
						$("#quesId"+i).val(data[i+'vvi'+arr[i]]);
					}
					if(data[i+'onlineassessment'+arr[i]]!=null){
						 if(data[i+'onlineassessment'+arr[i]].length<1 && data[i+'onlineassessment'+arr[i]]==0){
							$("#7"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
							$("#7"+arr[i]).css("background-color","#73D2EF");
							$("#7"+arr[i]+arr[i]).val("");
							$("#7"+arr[i]+arr[i]+"id").val(0);
							$("#7"+arr[i]).html(resourceJSON.instN);
							$("#7_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
							deactivateSection(7,(i+1));
						}else if(data[i+'onlineassessment'+arr[i]].length>=1){
							$("#7"+arr[i]+arr[i]+"id").val(data[i+'onlineassessment'+arr[i]]);
							var checklist0=document.getElementsByName(assessmentArr[i]); 
							var dataArr=[];
							if(data[i+'onlineassessment'+arr[i]].length==1){
								dataArr[0]=data[i+'onlineassessment'+arr[i]];
							}else{
							  dataArr=data[i+'onlineassessment'+arr[i]].split(",");
							}
								for(var k=0;k<dataArr.length;k++){
									$('input[name="'+assessmentArr[i]+'"]').each(function() {
										if($(this).val()==dataArr[k]){	
									    $(this).attr('checked', 'checked');
										}
									  });
								}
							if(i==0){selunselect0();}else if(i==1){selunselect1();}else if(i==2){selunselect2();}
							$("#7"+arr[i]).css("background-color","rgb(0, 147, 294)");
							$("#7"+arr[i]).html(resourceJSON.instC);
							if(data[i+'onlineassessmentStatus'+arr[i]]=="A" || data[i+"status"+arr[i]]==0)
								$("#7_"+(i+1)).attr("data-original-title",resourceJSON.msgInactive);
							else
								$("#7_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
							
							deactivateSection(7,(i+1));
						}
					}else{
						$("#7"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
						$("#7"+arr[i]).css("background-color","#73D2EF");
						$("#7"+arr[i]+arr[i]).val("");
						$("#7"+arr[i]+arr[i]+"id").val(0);
						$("#7"+arr[i]).html(resourceJSON.instN);
						$("#7_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						deactivateSection(7,(i+1));
					}
					$('#avlbList'+i+' option[value="'+data[i+'ereference'+arr[i]]+'"]').attr("selected", "selected");
					if(data[i+'ereference'+arr[i]]==null){
						$("#8"+arr[i]).attr("data-original-title",editEmptyMsg[0]);
						$("#8"+arr[i]).css("background-color","#73D2EF");
						$("#8"+arr[i]+arr[i]).val("");
						$("#8"+arr[i]+arr[i]+"id").val(0);
						$("#8"+arr[i]).html(resourceJSON.instN);
						$("#8_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						deactivateSection(8,(i+1));
					}else if(data[i+'ereference'+arr[i]]!=null){
						$("#8"+arr[i]).attr("data-original-title",$("#avlbList"+i+" option:selected").text());
						$("#8"+arr[i]+arr[i]).val($("#avlbList"+i+" option:selected").text());
						$("#8"+arr[i]+arr[i]+"id").val(data[i+'ereference'+arr[i]]);
						$("#8"+arr[i]).css("background-color","rgb(0, 147, 294)");
						$("#8"+arr[i]).html(resourceJSON.instC);
						if(data[i+'ereferenceStatus'+arr[i]]=="A" || data[i+"status"+arr[i]]==0)
							$("#8_"+(i+1)).attr("data-original-title",resourceJSON.msgInactive);
						else
							$("#8_"+(i+1)).attr("data-original-title",resourceJSON.msgActive);
						
						deactivateSection(8,(i+1));
						$("#8_8_"+(i+1)).val(data[i+'ereferenceStatus'+arr[i]]);
					}
				}
				hideOrShowCondidateType(0);
				$('#loadingDiv_jobCatGrid').hide();
					}, 100);
				}
				}, 50);
		}
	 },errorHandler:handleError
	});
}



//***********************************************************
function activateDeactivateJobCategory(jobcatId,status)
{	
	JobCategoryAjaxNew.activateDeactivateJobCategory(jobcatId,status,{ 
		async: true,
		callback: function(data){
			displayJobCategories();
		},
	});
}

function removeJsi()
{
	var jobcategoryId = document.getElementById("jobcategoryId").value;
	
	$('#message2showConfirm').html(resourceJSON.msgWouldLikeRemoveJSI);
	$('#footerbtn').html("<button class='btn btn-primary' onclick=\"removeJSIAttachment('"+jobcategoryId+"')\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#myModal3').modal('show');
	
}

function removeJSIAttachment(jobcategoryId)
{
		$('#myModal3').modal('hide');
		JobCategoryAjaxNew.removeJsi(jobcategoryId,{ 
			async: false,
			callback: function(data){
				
				if(data==1)
				{
					document.getElementById("removeJsi").style.display="none";
					document.getElementById('assessmentDocument').value="";
					document.getElementById('jsiFileName').value="";
					displayJobCategories();
					
				}else if(data==3)
				{
					$('#myModal2').modal('show');
					$('#message2show').html(resourceJSON.msgStatusAttachedJSI);
				}
			},errorHandler:handleError
		});
	
}

/********************************** district auto completer ***********************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;

function getRefreshDistrictName(){
	 count=0;
	 index=-1;
	 length=0;
	 divid='';
	 txtid='';
	 hiddenDataArray = new Array();
	 showDataArray = new Array();
	 degreeTypeArray = new Array();
	 hiddenId="";
	 districtNameFilter=0;
}

function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	getRefreshDistrictName();
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		}
		else {
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

/* ========= Start :: Virtual Video Interview ============*/
function showOfferVVI()
{
	var districtId = trim(document.getElementById("districtId").value);
	var offerVVIFlag=0;
	if(districtId!="")
	{	
	for(var i=1;i<=3;i++)
	{
		if($("#"+i+"_7").attr('title')==resourceJSON.msgActive)
			offerVVIFlag=1;			
	}
	if(offerVVIFlag)
		$("#offerVVIDiv").show();
	else
		$("#offerVVIDiv").hide();
	}else{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
	 }	
}

function showMarksDiv()
{
	var wantScore 		= 	document.getElementById("wantScore").checked;
	var maxScoreForVVI 	= 	document.getElementById("maxScoreForVVI").value;
		
	if(wantScore==true)
	{
		$("#marksDiv").show();
		$("#maxScoreForVVI").val($("#districtMaxScore").val());
	}
	else
	{
		$("#marksDiv").hide();
		$("#maxScoreForVVI").val("");
	}	
}

var showLinkCounter=0;

function showLinkDivNew()
{
	var sendAutoVVILink 	= 	document.getElementById("sendAutoVVILink").checked;
	if(sendAutoVVILink==true)
	{
		 $("#autolinkDiv").show();
	}
	else
	{
		$("#autolinkDiv").hide();
		
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function getStatusList()
{
	var districtId = trim(document.getElementById("districtId").value);
	if(districtId!="")
	 JobCategoryAjaxNew.getStatusList(districtId,{ 
		async: false,
		callback: function(data)
		{ 
			if(data!=null)
				document.getElementById("statusDiv").innerHTML=data;	
		},errorHandler:handleError
	});
}

function getVVIDefaultFields(jobcategoryId)
{
	var districtId = trim(document.getElementById("districtId").value);
	
	if(districtId!="" && jobcategoryId=="")
	{
		JobCategoryAjaxNew.getVVIDefaultFields(districtId,{ 
			async: false,
			callback: function(data)
			{
				if(data!=null)
				{
					if(data.approvalByPredefinedGroups==true)
						$('#approvalByPredefinedGroupsHidden').val(1);
					else
						$('#approvalByPredefinedGroupsHidden').val(0);
											
					//For VVI
					if(data.offerVirtualVideoInterview==true)
					{
						var arr=['E','I','T'];
						for(var i=0;i<=2;i+=1)
						{
							$("#6"+arr[i]+arr[i]+"id").val(data.i4QuestionSets.ID);
							$("#6"+arr[i]+arr[i]).val(data.i4QuestionSets.questionSetText);
							$("#6"+arr[i]).attr("data-original-title",data.i4QuestionSets.questionSetText);
							$("#6"+arr[i]).css("background-color","rgb(226, 177, 148)");
							$("#6"+arr[i]).html(resourceJSON.instD);
						}

						if(data.maxScoreForVVI!=null && data.maxScoreForVVI!="")
						{
							$("#districtMaxScore").val(data.maxScoreForVVI);
							$("#maxScoreForVVIHidden").val(data.maxScoreForVVI);
						}

						if(data.sendAutoVVILink==true)
						{
						var itemList = document.getElementById('slctStatusID');
							
								if(data.statusMasterForVVI!=null)
								{
									for (var i = 0; i < itemList.length; i++) 
									{
										if (itemList.options[i].value== data.statusMasterForVVI.statusId) {
											$("#sendAutoVVILinkHidden").val(1);
											$("#statusDivHidden").val(data.statusMasterForVVI.statusId);
											break;
										}
									}
								}else if(data.secondaryStatusForVVI!=null)
								{
									for (var j = 0; j < itemList.length; j++) 
									{
										if (itemList.options[j].value== "SSID_"+data.secondaryStatusForVVI.secondaryStatusId) {
											//itemList.options[j].selected = true;
											$("#sendAutoVVILinkHidden").val(1);
											$("#statusDivHidden").val(data.statusMasterForVVI.statusId);
											break;
										}
									}
								}
						}else
						{
							//$("#autolinkDiv").hide();
						}
						document.getElementById('timePerQuesHidden').value=data.timeAllowedPerQuestion;
						document.getElementById('vviExpDaysHidden').value=data.VVIExpiresInDays;
					}else
					{
						//$("#offerVVIDiv").hide();
					}
				}
			},errorHandler:handleError
		});
	}
}



function clearVVIFields()
{
	document.getElementById('wantScore').checked=false;
	document.getElementById('maxScoreForVVI').value="";
	document.getElementById('sendAutoVVILink').checked=false;
	document.getElementById('timePerQues').value="";
	document.getElementById('vviExpDays').value="";
}

/* ========= End :: Virtual Video Interview ============*/


/************************************* Question Set auto complete**********************************************/
function getQuestionSetAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(type==0)
			document.getElementById("quesName0").focus();
		if(type==1)
			document.getElementById("quesName1").focus();
		if(type==2)
			document.getElementById("quesName2").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//searchArray = getDistrictMasterArray(txtSearch.value);
		searchArray = getQuesSetArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


function getQuesSetArray(quesText)
{
	var searchArray = new Array();
	
	var districtId = trim(document.getElementById("districtId").value);
	
	if(districtId!="")
	{
		I4QuestionSetAjax.getFieldOfQuesList(quesText,districtId,{ async: false,callback: function(data)
			{
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++)
				{
					searchArray[i]=data[i].questionSetText;
					showDataArray[i]=data[i].questionSetText;
					hiddenDataArray[i]=data[i].ID;
				}
			},
		});	
	}

	return searchArray;
}
/************************************* District Assessment auto complete**********************************************/
function getDistrictAssessmentAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("assessmentName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//searchArray = getDistrictMasterArray(txtSearch.value);
		searchArray = getDistrictAssessmentArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


function getDistrictAssessmentArray(distASMTName)
{
	var searchArray = new Array();
	
	var districtId = trim(document.getElementById("districtId").value);
	if(districtId!="")
	{
		DistrictAssessmentAjax.getFieldOfDistASMT(distASMTName,districtId,{ async: false,callback: function(data)
			{
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++)
				{
					searchArray[i]=data[i].districtAssessmentName;
					showDataArray[i]=data[i].districtAssessmentName;
					hiddenDataArray[i]=data[i].districtAssessmentId;
				}
			},
		});
	}

	return searchArray;
}



/*************************************************************************************************/

function displayAllDistrictAssessment()
{
 var objectValue="";
	var districtId=document.getElementById("districtId").value;
	var jobcategoryId=document.getElementById("jobcategoryId").value;
	if(districtId==null || districtId=="")
		districtId=0;
	if(districtId>0)
	{
		JobCategoryAjaxNew.displayAllDistrictAssessment(districtId,jobcategoryId,
				{ 
			        async: false,
					errorHandler:handleError,
					callback: function(data)
					{
			        	$(".mutliSelect0").html(data);
			        	objectValue=String(data);
			        	$(".mutliSelect1").html(objectValue.replace(/selunselect0/g, "selunselect1").replace(/asses0/g, "asses1"));
			        	$(".mutliSelect2").html(objectValue.replace(/selunselect0/g, "selunselect2").replace(/asses0/g, "asses2"));
					}
				});
	}
	
}

var showOfferCounter=0;
function showOfferAMT()
{
	var districtId= $("#districtId").val();
	var previousDistrictId=$("#previousDistrictId").val();
	var offerVVI 		= 	document.getElementById("offerAssessmentInviteOnly").checked;
	if(offerVVI==true)
	{
		 if(previousDistrictId!=districtId)
		 {
		  displayAllDistrictAssessment();
		  $("#previousDistrictId").val(districtId);
		  showOfferCounter=1;
		 }
		 else
		 {
			 if(showOfferCounter==0)
			 {
				  displayAllDistrictAssessment();
				  $("#previousDistrictId").val(districtId); 
				  showOfferCounter=1;
			 }
		 }
		$("#multiDisAdminDiv").show()
		//*********** Check Offer Assessment to candidates on invite only ********************
		if($("#lstDistrictAdmins option").length==0 && $("#attachedDAList option").length==0)
		{
			$("#districtAssessment").show();
		}
	//****************************************************************************************	
	}
	else
	{
		$("#multiDisAdminDiv").hide();
	}
}

function getReferenceByDistrictList(jobcategoryId) {
	try{
		var districtId 		= 	trim(document.getElementById("districtId").value);

	if(districtId!="")
	JobCategoryAjaxNew.getReferenceByDistrictList(districtId,jobcategoryId,{ 
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		callback: function(data)
		{
			if(data!=null)
			{
				document.getElementById("avlbList0").innerHTML=data;	
				document.getElementById("avlbList1").innerHTML=data;
				document.getElementById("avlbList2").innerHTML=data;
			}
		},errorHandler:handleError
	});
	}catch(e){alert(e);}
}

function getReferenceByDistrictAndJobCategoryList() {
	try{
		var districtId 		= 	trim(document.getElementById("districtId").value);
		var jobcategoryId	=	document.getElementById("jobcategoryId").value;
	}catch(e){}
	if(districtId!="")
	JobCategoryAjaxNew.getReferenceByDistrictAndJobCategoryList(districtId,jobcategoryId,{ 
		async: false,
		callback: function(data)
		{
			if(data!=null)
				document.getElementById("attachedList").innerHTML=data;	
		},errorHandler:handleError
	});
}

function getQQuestionSetByDistrictAndJobCategoryList(){
	try{
		var districtId = trim(document.getElementById("districtId").value);
		var jobCategoryId = trim(document.getElementById("jobcategoryId").value);
		
		JobCategoryAjaxNew.getQQuestionSetByDistrictAndJobCategoryList(districtId, jobCategoryId,{
			preHook:function(){$('#loadingDiv').show()},
			postHook:function(){$('#loadingDiv').hide()},
			async:true,
			callback:function(data){
			if(data!=null)
			{
				document.getElementById("qqAvlbList").innerHTML=data;
	
			}
		},errorHandler:handleError
		});
	}catch(e){e}
    
}

$(function(){
	$("#jobCatRadio1").click(function() {
		$("#parentJobCategoryId").prop("disabled",true);
		$("#parentJobCategoryId").val("0");
		$("#jobcategory").prop("disabled",false);
		$("#subJobCatDiv").hide();
	});
	$("#jobCatRadio2").click(function() {
		$('#errordiv').empty();
		var districtId=trim(document.getElementById('districtId').value);
		if(districtId!=""){
			$("#parentJobCategoryId").prop("disabled",false);
			$("#jobcategory").prop("disabled",true);
			$("#jobcategory").val("");
		}else{
			$("#jobCatRadio2").prop("checked",false);
			$("#jobCatRadio1").prop("checked",true);			
			$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
		}		
	});
	$("#parentJobCategoryId").change(function(){
		var str = "";
	    $( "#parentJobCategoryId option:selected" ).each(function() {
	      str= $( this ).val();
	    });
	    $("#subJobCatDiv").hide();
	    if(str!="0"){
	    	$("#subJobCatDiv").show();
	    }
	});
});
function getJobCategoryByDistrictId(){
	try{
		var districtId 	= 	trim(document.getElementById("districtId").value);
		var jobSubCate = 	$("#jobcategoryId").val();
		if($("#jobcategoryId").val()=="")
			jobSubCate = 0 ;
	}catch(e){}
	  JobCategoryAjaxNew.getJobCategoryByDistrictId(districtId,jobSubCate,{ 
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: false,
		callback: function(data)
		{
			if(data!=null)
				$("#parentJobCategoryId").html(data);
		},errorHandler:handleError
	});
}
function getQQSetForOnboardingByDistrictAndJobCategoryList(jobCategoryId){
	try{ 
		var districtId = trim(document.getElementById("districtId").value);
		JobCategoryAjaxNew.getQQSetForOnboardingByDistrictAndJobCategoryList(districtId, jobCategoryId,{
			async:false,
			callback:function(data){
			if(data!=null)
			{
				for(var i=0;i<3;i+=1)
				{
					document.getElementById("qqAvlbListForOnboard"+i).innerHTML=data;
					document.getElementById("qqAvlbList"+i).innerHTML=data;
				}
				pre_ScreenSelectedIndex();
			}
		},errorHandler:handleError
		});
	}catch(e){e}
    
}

function pre_ScreenSelectedIndex()
{
	var arr=['E','I','T'];
	if($("#qqAvlbListForOnboard0").val()!=null && $("#qqAvlbListForOnboard0").val()!="")
	{
		var value=$("#qqAvlbListForOnboard0 option:selected").text();
		
		for(var i=0;i<=2;i+=1)
		{
			$("#2"+arr[i]+arr[i]+"id").val($("#qqAvlbListForOnboard0").val());
			$("#2"+arr[i]+arr[i]).val(value);
			$("#2"+arr[i]).attr("data-original-title",value);
			$("#2"+arr[i]).css("background-color","rgb(226, 177, 148)");
			$("#2"+arr[i]).html(resourceJSON.instD);
			
			$("#5"+arr[i]+arr[i]+"id").val($("#qqAvlbListForOnboard0").val());
			$("#5"+arr[i]+arr[i]).val(value);
			$("#5"+arr[i]).attr("data-original-title",value);
			$("#5"+arr[i]).css("background-color","rgb(226, 177, 148)");
			$("#5"+arr[i]).html(resourceJSON.instD);
		}
	}
}

// ////////////////////////////////////           Adding by Deepak           ////////////////////////////////////////
function hideOrShowCreateJob(type)
{
$('#errordiv').empty();
$('.newJobCategory').hide();
$('.selectJobCategory').hide();
$("#jobCatType").val(type);
var categoryFlag=0;
if(type==1)
{
	var districtId=trim(document.getElementById('districtId').value);
	$('.newJobCategory').show();
	if($("#subjobcategory").val()!="")
		$("#jobcategory").val($("#subjobcategory").val());
}
if(type==2)
{
	var districtId=trim(document.getElementById('districtId').value);
	if(districtId!=""){		
		$('#errordiv').hide();
		$('.selectJobCategory').show();
		if($("#jobcategory").val()!="")
			$("#subjobcategory").val($("#jobcategory").val());
		
		var previousDistrictId=$("#previousDistrictId").val();
		if(previousDistrictId!=districtId)
		 {
			       getJobCategoryByDistrictId();
				   $("#previousDistrictId").val(districtId);
				   categoryFlag=1;
		}
		else
		{
				  if(categoryFlag==0)
				  {
					  getJobCategoryByDistrictId();
					  $("#previousDistrictId").val(districtId);
					  categoryFlag=1;
				  }
		}
		
	}else{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
		$('#createJobCategory').prop("checked",true);
		$('#selectJobCategory').prop("checked",false);
		$('.newJobCategory').show();
	 }	
}

}

function showOnlineAssessment()
{
	var showOnlineFlag=0;
	var districtId = trim(document.getElementById("districtId").value);
	if(districtId!="")
	{
      //var onlineAssessmentSection = document.getElementById("onlineAssessmentSection");
		for(var i=1;i<=3;i++)
		{
			if($("#"+i+"_8").attr('title')==resourceJSON.msgActive)
				showOnlineFlag=1;			
		}
	if(showOnlineFlag)
	{
		$('.onlineAssessment').show();
	}
	else
	{
		$('.onlineAssessment').hide();	
	}
	}else{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
	 }	
}
var eRefCounter=0;
function EReferenceQuestionSet()
{
	var districtId = trim(document.getElementById("districtId").value);
	var previousDistrictId=$("#previousDistrictId").val();
	var e_ReFlag=0;
	if(districtId!="")
	{
		for(var i=1;i<=3;i++)
		{
			if($("#8_8_"+i).val()=="A")
				e_ReFlag=1;			
		}
	if(e_ReFlag)
	{
		 if(previousDistrictId!=districtId)
		 {
		   getReferenceByDistrictList(0);
		   $("#previousDistrictId").val(districtId);
		   eRefCounter=1;
		}
		else
		{
		  if(eRefCounter==0)
		  {
			  getReferenceByDistrictList(0);
			  $("#previousDistrictId").val(districtId);
			  eRefCounter=1;
		  }
		}
	}
	}else{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
	 }
}
var QQCounter=0;
function QualificationQuestionSet()
{
	var districtId = trim(document.getElementById("districtId").value);
	var previousDistrictId=$("#previousDistrictId").val();
	if(districtId!="" )
	{
		var preScreenFlag=0;
		var preScreenOnFlag=0;
		for(var i=1;i<=3;i+=1)
		{
			if($("#2_2_"+i).val()=="A")
				preScreenFlag=1;
			
		}
		if(preScreenFlag)
		{
			getQQSetForOnboardingByDistrictAndJobCategoryList(0);
			
		}
	}else{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
		//$('#QualificationQuestionSection').prop("checked",false);
	 }	
}

//var QQOnBordingCounter=0;
function QualificationQuestionSetOnBording()
{
	var districtId =document.getElementById("districtId").value;
	var previousDistrictId=$("#previousDistrictId").val();
	var preScreenFlag=0;
	var preScreenOnFlagOnBording=0;
	if(districtId!=0)
	{
	for(var i=1;i<=3;i++)
	{
		if($("#"+i+"_5").attr('title')==resourceJSON.msgActive)
			preScreenOnFlagOnBording=1;			
	}
	if(preScreenOnFlagOnBording)
	{
		$('.QualificationQuestionOnBording').show();
		 if(previousDistrictId!=districtId)
		 {
		   getQQSetForOnboardingByDistrictAndJobCategoryList(0);
		   $("#previousDistrictId").val(districtId);
		   QQCounter=1;
		 }
		 else{
			 if(QQCounter==0)
			 {
				 getQQSetForOnboardingByDistrictAndJobCategoryList(0);
				   $("#previousDistrictId").val(districtId);
				   QQCounter=1;
			 }
		 }
	}
	else
	{
		$('.QualificationQuestionOnBording').hide();
		$("select#qqAvlbListForOnboard").prop('selectedIndex', 0);
	}
	}else{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
		//$('#QualificationQuestionSection').prop("checked",false);
	 }	
}
function jobSpecificInventory()
{
 var jsiFlag=0;	
	for(var i=1;i<=3;i++)
	{
		if($("#"+i+"_4").attr('title')==resourceJSON.msgActive)
		{
			jsiFlag=1;
			$("#"+i+"_4_4").show();
		}
		else
		{
			$("#"+i+"_4_4").hide();	
		}
	}
	if(jsiFlag)
	{
		
		$('#jsisubsection').show();
		$('.customQ').removeClass("col-sm-3 col-md-3");
		$('.customQ').addClass("col-sm-2 col-md-2");
		$('.customQQ').removeClass("col-sm-3 col-md-3");
		$('.customQQ').addClass("col-sm-2 col-md-2");
		$(".custQ").css('margin-left', '-15px');
		$("#customQId").css('margin-left', '-26px');
		$("#onQQ").css('margin-left', '-27px');
		$("#preSpan").css('margin-left', '-15px');
		$("#assSpan").css('margin-left', '-20px');
		$('.sectionFirst').removeClass("col-sm-8 col-md-8");
		$('.sectionFirst').addClass("col-sm-9 col-md-9");
		$('.sectionSecond').removeClass("col-sm-4 col-md-4");
		$('.sectionSecond').addClass("col-sm-3 col-md-3");
		$(".dataSection").show();
		$('.jobSpecificInventoryMain').show();
	}
	else
	{
		$('#jsisubsection').hide();
		$('.customQ').removeClass("col-sm-2 col-md-2");
		$('.customQ').addClass("col-sm-3 col-md-3");
		$('.customQQ').removeClass("col-sm-2 col-md-2");
		$('.customQQ').addClass("col-sm-3 col-md-3");
		$("#customQId").css('margin-left', '-15px');
		$(".custQ").css('margin-left', '20px');
		$("#onQQ").css('margin-left', '-5px');
		$("#assSpan").css('margin-left', '0px');
		$("#preSpan").css('margin-left', '-8px');
		$('.sectionFirst').removeClass("col-sm-9 col-md-9");
		$('.sectionFirst').addClass("col-sm-8 col-md-8");
		$('.sectionSecond').removeClass("col-sm-3 col-md-3");
		$('.sectionSecond').addClass("col-sm-4 col-md-4");
		$(".dataSection").hide();
		$('.jobSpecificInventoryMain').hide();	
	}
}

function deactivateSection(canType,sectionid)
{
	$('#errordiv').empty();
	var districtId = trim(document.getElementById("districtId").value);
	if(districtId!="")
	{
	var status=$("#"+canType+"_"+sectionid).attr('data-original-title');
	if(status==resourceJSON.msgActive)
	{
		$('#'+canType+'_'+canType+'_'+sectionid).val("I");
		$('#'+canType+'_'+sectionid).attr('data-original-title', resourceJSON.msgInactive);
		$('#'+canType+'_'+sectionid).html(resourceJSON.instX);
		$('#'+canType+'_'+sectionid).css('background-color', 'grey');
		if(sectionid==1)
		{
		   $("#"+canType+"E").hide();
		   if(canType==4)
			   $("#etype1").hide();
		}
		if(sectionid==2)
		{
		   $("#"+canType+"I").hide();
		   if(canType==4)
			   $("#etype2").hide();
		}
		if(sectionid==3)
		{
		   $("#"+canType+"T").hide();
		   if(canType==4)
			   $("#etype3").hide();
		}
	}
	else 
	{
		$('#'+canType+'_'+canType+'_'+sectionid).val("A");
		$('#'+canType+'_'+sectionid).attr('data-original-title', resourceJSON.msgActive);
		$('#'+canType+'_'+sectionid).html(resourceJSON.instA);
		$('#'+canType+'_'+sectionid).css('background-color', 'green');
		if(sectionid==1)
		{
		   $("#"+canType+"E").show();
		   if(canType==4)
			   $("#etype1").show();
		}
		if(sectionid==2)
		{
		   $("#"+canType+"I").show();
		   if(canType==4)
			   $("#etype2").show();
		}
		if(sectionid==3)
		{
		   $("#"+canType+"T").show();
		   if(canType==4)
			    $("#etype3").show();
		}
	}
	}else{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
	 }	
}

function resetSectionCheckBoxByDistrict()
{
	var districtId= $("#districtId").val();
	var previousDistrictId=$("#previousDistrictId").val();
	 if(previousDistrictId!=districtId)
	 { 
		    if(districtId)
		    {
		    $('#errordiv').empty();
		    $('#errordiv').hide();
		   // $(".golive").show();
		    document.getElementById("approvalBeforeGoLive1").checked=false;
		    }else{
		    	$(".golive").hide();
		    	document.getElementById("approvalBeforeGoLive1").checked=false;
		    }

		    $('#saveGroup').hide();
		    //$("#approvalBeforeGoLive1").removeAttr("disabled");
			$('#jobcategory').css("background-color", "");
			$('#districtName').css("background-color", "");
			//************** For the Approval Process reset *********
			$("#groupApprovalDiv").hide();
			$("#groupApprovalDiv1").hide();
			$(".hideGroup").fadeOut(500);
		    $("#previousDistrictId").val(districtId);
		    //*************** Reset checkboxes ***************************************
		    resetLayout();
		    defaultInactiveLayout();
		    hideOrShowCondidateType(0);
		    $('#loadingDiv_jobCatGrid').show();
			 setTimeout(function() {multipleAjaxMethods();}, 50);
	}
}

function resetLayout()
{
	var msgArr=[resourceJSON.msgNoPortfolioConfigured,resourceJSON.msgNoPreScreenConfigured,resourceJSON.msgNoEPIConfigured,resourceJSON.msgNoCustomQuestionConfigured,resourceJSON.msgNoOnBoardingConfigured,resourceJSON.msgNoVVIConfigured,resourceJSON.msgNoAssessmentConfigured,resourceJSON.msgNoEReferenceConfigured];
	 var arr=['E','I','T'];
		for(var i=1;i<=8;i++)
		{
		     for(var j=1;j<=3;j++)
		     {
				if($("#"+i+"_"+i+"_"+j).val()=="I")
				{
					$('#'+i+'_'+i+'_'+j).val("A");
					$('#'+i+'_'+j).html("A");
					$('#'+i+'_'+j).attr('data-original-title', resourceJSON.msgActive)
					$('#'+i+'_'+j).css('background-color', 'green');
				}
				 $("#"+i+arr[j-1]).show();
				 $("#"+i+arr[j-1]).attr('data-original-title', msgArr[i-1]);
				 $("#"+i+arr[j-1]).html(resourceJSON.instN);
			     $("#"+i+arr[j-1]+arr[j-1]).val("");
				 $("#"+i+arr[j-1]+arr[j-1]+"id").val(0);
				 $("#"+i+arr[j-1]).css('background-color', '#73D2EF');	
				 if(i==4){
					 $("#etype"+j).attr('data-original-title',resourceJSON.msgRequired);
					 $("#etype"+j).css('background-color', 'rgb(255, 0, 0)');	
					 $("#etype"+j).css('display', 'inline-block');
					 $("#etypeValue"+i).val(1);
					 $("#etype"+j).html(resourceJSON.instR);
				 }
			}
		     if(i==6){
		    	$("#wantScore").prop("checked",false); 
		    	showMarksDiv();
		    	$("#maxScoreForVVI").val("");
		    	$("#maxScoreForVVIHidden").val("");
		    	$("#sendAutoVVILinkHidden").val("");
		    	$("#sendAutoVVILink").prop("checked",false); 
		    	$("select#statusDiv")[0].selectedIndex = 0;
		    	showLinkDivNew();
		    	$("#timePerQues").val("");
		    	$("#timePerQuesHidden").val("");
		    	$("#vviExpDays").val("");
		    	$("#vviExpDaysHidden").val("");
		    	
		     }
		}
		jobSpecificInventory(); 
}
function confirmDistrictAssesment()
{
	$("#districtAssessment").hide();
	document.getElementById("offerAssessmentInviteOnly").checked=false;
	$("#multiDisAdminDiv").hide();
}

var EPICounter=portfolioCounter=CQCounter=AssCounter=vviCounter=0,duplicateCounter=0;
function clickView(id,type,showType)
{
	$('#errordiv').empty();
	var districtId=$("#districtId").val();
	var editMode=$("#editMode").val();
	$("#showType").val(showType);
	if(districtId =="" ||districtId==null)
	{
		$('#errordiv').show();
		$('#districtName').focus();
		$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);	
	}
	else
	{
	$("#activeId").val(id);
	$("#activeType").val(type);
	if(id==1)
	{
		$("#errordivportfolio").empty();
		$("#errordivportfolio").hide();
	    var districtId=$("#districtId").val();
	    $("#allPortfolio").removeAttr("disabled");
	    var previousDistrictId=$("#previousDistrictId").val();
		  if(showType==1){
	    	getPortfolioNameByDistrict(districtId,"colId0",type);
	    	if(duplicateCounter==0){
	    	getPortfolioNameByDistrict(districtId,"colId1","I");
			getPortfolioNameByDistrict(districtId,"colId2","T");
			duplicateCounter=1;
	    	}
		   }else{
			   if(showType==2){
				   getPortfolioNameByDistrict(districtId,"colId0","E");
				   getPortfolioNameByDistrict(districtId,"colId1","I");
				   getPortfolioNameByDistrict(districtId,"colId2","T");
			   }
		   }
	   $(".porfolioHide0").hide();
	   $(".porfolioHide1").hide();
	   $(".porfolioHide2").hide();
	   $("#allPortfolio").attr("checked",false);
		if(showType==1)
		{
			var portfolioCounterFlag=0;
			$(".porfolioHide0").show();
			showSingleType(id,type,"headingDiv",resourceJSON.lblPortfolio,"colId0","colTitel0","tableoption");
			for(var i=0;i<3;i+=1){
				if($("#section"+i).is(":checked")){
					portfolioCounterFlag+=1;
				}
			}
			if(portfolioCounterFlag==1)
				$("#allPortfolioCheckBox").hide();
			else
				$("#allPortfolioCheckBox").show();
	  }else{
		  $("#allPortfolioCheckBox").show();
		  showMultitype(id,"porfolioHide0","porfolioHide1","porfolioHide2",resourceJSON.lblPortfolio,"headingDiv","colId0","colId1","colId2","colTitel0","colTitel1","colTitel2","tableoption","allPortfolio");
		  findAllSelectedValue(0,"colId0","colId1","colId2","allPortfolio");
	  }
	}
	if(id==2)
	{
		$("#errordivpre").empty();
		$("#errordivpre").hide();
	    var districtId=$("#districtId").val();
	    var previousDistrictId=$("#previousDistrictId").val();
	   $(".preSecreenModel0").hide();
	   $(".preSecreenModel1").hide();
	   $(".preSecreenModel2").hide();
	   $("#allPre").attr("checked",false);
		 //  QualificationQuestionSet();
	   //$("#preScreen").show();
		if(showType==1)
		{
		$(".preSecreenModel0").show();
		showSingleType(id,type,"headingDivPre",resourceJSON.lblPreScreen,"qqAvlbList0","colname0","preScreen");
	  }else{
		  showMultitype(id,"preSecreenModel0","preSecreenModel1","preSecreenModel2",resourceJSON.lblPreScreen,"headingDivPre","qqAvlbList0","qqAvlbList1","qqAvlbList2","colname0","colname1","colname2","preScreen","allPre")
		  findAllSelectedValue(1,"qqAvlbList0","qqAvlbList1","qqAvlbList2","allPre");
	  }
    }
	if(id==3)
	{
		$("#errordivepi").empty();
		$("#errordivepi").hide();
	    var districtId=$("#districtId").val();
	    var previousDistrictId=$("#previousDistrictId").val();
	   $(".EPIHide0").hide();
	   $(".EPIHide1").hide();
	   $(".EPIHide2").hide();
	   $("#allEPI").attr("checked",false);
	   if(previousDistrictId!=districtId && editMode==0)
	   {
		getAssessmentNameByAssessmentType(1,"EPIId");
	    $("#previousDistrictId").val(districtId);
	    EPICounter=1;
	   }
	   else
	   {
		  if(EPICounter==0 && editMode==0)
			 {
			       getAssessmentNameByAssessmentType(1,"EPIId");
				   $("#previousDistrictId").val(districtId);
				   EPICounter=1;
			 }

	   }
		if(showType==1)
		{
		$(".EPIHide0").show();
		showSingleType(id,type,"headingDivEPI",resourceJSON.lblEPIGroup,"EPIId0","epiLabel0","EPIDiv");
	  }else{
		  showMultitype(id,"EPIHide0","EPIHide1","EPIHide2",resourceJSON.lblEPIGroup,"headingDivEPI","EPIId0","EPIId1","EPIId2","epiLabel0","epiLabel1","epiLabel2","EPIDiv","allEPI");
		  findAllSelectedValue(2,"EPIId0","EPIId1","EPIId2","allEPI");
	  }
	  }
	if(id==4)
	{
	    var districtId=$("#districtId").val();
		var previousDistrictId=$("#previousDistrictId").val();
		$("#errordivjsi").empty();
		$("#errordivjsi").hide();
	   $(".JSIHide0").hide();
	   $(".JSIHide1").hide();
	   $(".JSIHide2").hide();
	   $("#allJSI").attr("checked",false);
	   if(previousDistrictId!=districtId && editMode==0)
	   {
	   getAssessmentNameByDistrictId(2,districtId);
	   $("#previousDistrictId").val(districtId);
	   CQCounter=1;
	   }
	   else
	   {
		  if(CQCounter==0 && editMode==0)
			 {
			     getAssessmentNameByDistrictId(2,districtId);
				   $("#previousDistrictId").val(districtId);
				   CQCounter=1;
			 }

  }
		if(showType==1)
		{
		$(".JSIHide0").show();
		showSingleType(id,type,"headingDivJSI",resourceJSON.lblCustomQuestions,"JSIId0","jsiLabel0","JSIDiv");
	  }else{
		  showMultitype(id,"JSIHide0","JSIHide1","JSIHide2",resourceJSON.lblCustomQuestions,"headingDivJSI","JSIId0","JSIId1","JSIId2","jsiLabel0","jsiLabel1","jsiLabel2","JSIDiv","allJSI");
		  findAllSelectedValue(3,"JSIId0","JSIId1","JSIId2","allJSI");
	  }
	  }
	if(id==5)
	{
		$("#errordivpreon").empty();
		$("#errordivpreon").hide();
	    var districtId=$("#districtId").val();
	   $(".preSecreenOnModel0").hide();
	   $(".preSecreenOnModel1").hide();
	   $(".preSecreenOnModel2").hide();
	   $("#allPreOn").attr("checked",false);
		if(showType==1)
		{
		$(".preSecreenOnModel0").show();
		showSingleType(id,type,"headingDivPreOn",resourceJSON.lblOnBoarding,"qqAvlbListForOnboard0","colnameOn0","preScreenOnBoarding");
	  }else{
		  showMultitype(id,"preSecreenOnModel0","preSecreenOnModel1","preSecreenOnModel2",resourceJSON.lblOnBoarding,"headingDivPreOn","qqAvlbListForOnboard0","qqAvlbListForOnboard1","qqAvlbListForOnboard2","colnameOn0","colnameOn1","colnameOn2","preScreenOnBoarding","allPreOn");
		  findAllSelectedValue(4,"qqAvlbListForOnboard0","qqAvlbListForOnboard1","qqAvlbListForOnboard2","allPreOn");
	  }
	  }
	if(id==6)
	{
		$("#errordivvvi").empty();
		$("#errordivvvi").hide();
		$("#headingDivVVI").html(resourceJSON.msgVirtualVideoInterview);
	    var districtId=$("#districtId").val();
	    $("#allVVI").attr("checked",false);
	    $(".VVIMultiType").hide();
	    $(".VVI0").hide();
	    $(".VVI1").hide();
	    $(".VVI2").hide();
	   
	    //************* Additional text fields ****************************
	    var maxScoreValue=$("#maxScoreForVVIHidden").val();
	    if(maxScoreValue.length>0)
	    {
	    	$("#wantScore").prop("checked",true);
	         showMarksDiv();
	    	$("#maxScoreForVVI").val(maxScoreValue);
	    }
	    if($("#sendAutoVVILinkHidden").val()==1)
	    {
	    	$("#sendAutoVVILink").prop("checked",true);
	    	showLinkDivNew();
	    	$('#statusDiv').val($("#statusDivHidden").val());
	    }
	    else
	    {
	    	$("#sendAutoVVILink").prop("checked",false);
	    	showLinkDivNew();
	    }
	    if($("#approvalByPredefinedGroupsHidden").val()==1)
	    {
	    	$("#approvalByPredefinedGroups").prop("checked",true);
	    }
	    else
	    {
	    	$("#approvalByPredefinedGroups").prop("checked",false);
	    }
	    $("#timePerQues").val($("#timePerQuesHidden").val());
	    $("#vviExpDays").val($("#vviExpDaysHidden").val());
	    //*****************************************************************
	    if(showType==1)
		{
	    	if(type=='E')
	    		$(".VVI0").show();
	 		else if(type=='I')
	 			$(".VVI1").show();
	 		else if(type=='T')
	 			$(".VVI2").show();
		
		showSingleTypeVVI(id,type,"headingDivVVI",resourceJSON.msgVirtualVideoInterview);
	  }else{
		  showMultitypeForVVI(id,"VVI0","VVI1","VVI2",resourceJSON.lblSelectaquestionset,"headingDivVVI","captionForExternal","captionForInternal","captionForInternalTransfer");
	  }
	  }
	if(id==7)
	{
		$("#assessmentDiv").show();
		$("#errordivassessment").empty();
		$("#errordivassessment").hide();
	    var districtId=$("#districtId").val();
	    $("#allAss").attr("checked",false);
	    var districtId=$("#districtId").val();
	    $(".onAss0").hide();
		$(".onAss1").hide();
		$(".onAss2").hide();
	   if(showType==1)
		{
			if(type=='E')
			{
				//$("#headingDivAssessment").html(resourceJSON.msgOnlineAssessments+" "+resourceJSON.msgforExternal);
				$("#headingDivAssessment").html(resourceJSON.msgforExternal+" "+resourceJSON.msgOnlineAssessments);
				$(".onAss0").show();
				$("#colnameOnAss0").html(resourceJSON.msgSelectDistrictAssessment);
				checkedMultiList(0);
				$(".assLbl0").attr("style","width:30%;");
				$(".assTxt0").attr("style","width:40%;");
				$(".multi0").attr("style","width:420px");
			}
			if(type=='I')
			{
				//$("#headingDivAssessment").html(resourceJSON.msgOnlineAssessments+" "+resourceJSON.msgforInternal);
				$("#headingDivAssessment").html(resourceJSON.msgforInternal+" "+resourceJSON.msgOnlineAssessments);
				$(".onAss1").show();
				$("#colnameOnAss1").html(resourceJSON.msgSelectDistrictAssessment);
				checkedMultiList(1);
				$(".assLbl1").attr("style","width:30%;");
				$(".assTxt1").attr("style","width:40%;");
				$(".multi1").attr("style","width:420px");
			}
			if(type=='T')
			{
				//$("#headingDivAssessment").html(resourceJSON.msgOnlineAssessments+" "+resourceJSON.msgforInternalTransfer);
				$("#headingDivAssessment").html(resourceJSON.msgforInternalTransfer+" "+resourceJSON.msgOnlineAssessments);
				$(".onAss2").show();
				$("#colnameOnAss2").html(resourceJSON.msgSelectDistrictAssessment);
				checkedMultiList(2);
				$(".assLbl2").attr("style","width:30%;");
				$(".assTxt2").attr("style","width:40%;");
				$(".multi2").attr("style","width:420px");
			}
	  }else
	  {
		 if($("#"+id+"_"+id+"_1").val()=="A" && $("#section0").is(":checked")){
			 $(".onAss0").show();
			 $(".assLbl0").attr("style","width:44%;");
			$(".assTxt0").attr("style","width:40%;");
			$(".multi0").attr("style","width:330px");
		 }
		 if($("#"+id+"_"+id+"_2").val()=="A" && $("#section1").is(":checked")){
			 $(".onAss1").show();
			 $(".assLbl1").attr("style","width:44%;");
			$(".assTxt1").attr("style","width:40%;");
			$(".multi1").attr("style","width:330px;");
		 }
		 if($("#"+id+"_"+id+"_3").val()=="A" && $("#section2").is(":checked")){
			 $(".onAss2").show();
			 $(".assLbl2").attr("style","width:44%;");
			 $(".assTxt2").attr("style","width:40%;");
			 $(".multi2").attr("style","width:330px;");
		 }
		for(var i=0;i<=2;i++)
		{
			checkedMultiList(i);	
		}
		/*$("#colnameOnAss0").html(resourceJSON.msgSelectDistrictAssessment+" "+resourceJSON.msgforExternal);
		$("#colnameOnAss1").html(resourceJSON.msgSelectDistrictAssessment+" "+resourceJSON.msgforInternal);
		$("#colnameOnAss2").html(resourceJSON.msgSelectDistrictAssessment+" "+resourceJSON.msgforInternalTransfer);*/
		$("#colnameOnAss0").html(resourceJSON.msgforExternal+" "+resourceJSON.msgSelectDistrictAssessment);
		$("#colnameOnAss1").html(resourceJSON.msgforInternal+" "+resourceJSON.msgSelectDistrictAssessment);
		$("#colnameOnAss2").html(resourceJSON.msgforInternalTransfer+" "+resourceJSON.msgSelectDistrictAssessment);
		$("#headingDivAssessment").html(resourceJSON.msgOnlineAssessments);
		
		if($("#"+id+"_"+id+"_1").val()=="A" && $("#section0").is(":checked")){
			//alert("1 . "+$("#7EEid").val()+"  2. "+$("#7IIid").val()+"  3. "+$("#7TTid").val());
			if($("#7EEid").val()==0 && $("#7IIid").val()==0 && $("#7TTid").val()==0)
			{
				$("#allAss").attr("checked",true);
				showdisabled(6);
			}else{
				//alert("  1. "+$("#7IIid").val()+"  2. "+$("#7TTid").val());
				if($("#7IIid").val()==0 && $("#7TTid").val()==0){
					$("#allAss").attr("checked",true);
					showdisabled(6);
				}else{
					$("#allAss").attr("checked",false);
					showdisabled(6);
				}
			}
		}
	  }
	}
	if(id==8)
	{
		$("#allERef").attr("checked",false);
		$(".ereference1").hide();
		$(".ereference2").hide();
		$(".ereference3").hide();
		if(editMode==0)
			EReferenceQuestionSet();
		
		$('#avlbList0 option[value=""]').attr("selected", "selected");
		var arr=['E','I','T'];
		if(showType==1)
		{
			$(".ereference1").show();
			showSingleType(id,type,"headingDiv1",resourceJSON.lblEReference,"avlbList0","erefLabel0","eref");
		}
		else{
			showMultitype(id,"ereference1","ereference2","ereference3",resourceJSON.lblEReference,"headingDiv1","avlbList0","avlbList1","avlbList2","erefLabel0","erefLabel1","erefLabel2","eref","allERef");
			  findAllSelectedValue(7,"avlbList0","avlbList1","avlbList2","allERef");
		  }
	}
  }
}
//************* Master function for show Div ***********************************
function showSingleType(id,type,headingId,headingText,selectId,labelId,mainDivId)
{
	$("#"+mainDivId).show();
	if(type=='E')
	{
		//$("#"+headingId).text(headingText+" "+resourceJSON.msgforExternal+" "+resourceJSON.msgcandidate);
		$("#"+headingId).text(resourceJSON.msgforExternal+" "+headingText);
		$('#'+selectId+' option[value="'+$("#"+id+"EEid").val()+'"]').attr("selected", "selected");
		$("#"+labelId).text(headingText);
		if(id==1)
			$('#'+selectId).attr("onchange","portfolioChangeNotificationInJobCategory(this.value,'E');");
	}
	else if(type=='I')
	{
		//$("#"+headingId).text(headingText+" "+resourceJSON.msgforInternal+" "+resourceJSON.msgcandidate);
		$("#"+headingId).text(resourceJSON.msgforInternal+" "+headingText);
		$('#'+selectId+' option[value="'+$("#"+id+"IIid").val()+'"]').attr("selected", "selected");
		$("#"+labelId).text(headingText);
		if(id==1)
			$('#'+selectId).attr("onchange","portfolioChangeNotificationInJobCategory(this.value,'I');");
	}
	else if(type=='T')
	{
		//$("#"+headingId).text(headingText+" "+resourceJSON.msgforInternalTransfer+" "+resourceJSON.msgcandidate);
		$("#"+headingId).text(resourceJSON.msgforInternalTransfer+" "+headingText);
		$('#'+selectId+' option[value="'+$("#"+id+"TTid").val()+'"]').attr("selected", "selected");
		$("#"+labelId).text(headingText);
		if(id==1)
			$('#'+selectId).attr("onchange","portfolioChangeNotificationInJobCategory(this.value,'T');");
	}
	if(id==1){
		$(".portfolioLbl").attr("style","width:11%;");
		$(".portfolioText").attr("style","width:87%;");
	}else if(id==2){
		$(".preLbl").attr("style","width:22%;");
		$(".preText").attr("style","width:76%;");
	}
	else if(id==3){
		$(".epiLbl").attr("style","width:13%;");
		$(".epiText").attr("style","width:85%;");
	}else if(id==4){
		$(".customLbl").attr("style","width:20%;");
		$(".customTxt").attr("style","width:77%;");
	}else if(id==5){
		$(".onBoardLbl").attr("style","width:15%;");
		$(".onBoardTxt").attr("style","width:83%;");
	}else if(id==8){
		$(".eRefLbl").attr("style","width:20%;");
		$(".eRefTxt").attr("style","width:77%;");
	}
}

function showMultitype(id,showSelect,showSelect1,showSelect2,HeadingText,HeadingDivId,selectId,selectId1,selectId2,headingId,headingId1,headingId2,mainDivId,checkAllId)
{
	    var counterFlag=0;
	    if($("#"+id+"_"+id+"_2").val()=="A" && $("#section1").is(":checked")){
	    	$("."+showSelect1).show();
	    	counterFlag=1;
	    }
	    if($("#"+id+"_"+id+"_3").val()=="A" && $("#section2").is(":checked")){
	    	$("."+showSelect2).show();
	    	counterFlag=1;
	    }
	    if($("#"+id+"_"+id+"_1").val()=="A" && $("#section0").is(":checked"))
	    {
		   $("."+showSelect).show();
		   if(id==1){
			   $('#'+selectId).attr("onchange","portfolioChangeNotificationInJobCategory(this.value,'E');");
			   if(counterFlag==0)
				   $("#allPortfolioCheckBox").hide();
		   }
	    }
	    
	    
	    if($("#"+id+"_"+id+"_1").val()=="I" && $("#"+id+"_"+id+"_2").val()=="I" && $("#"+id+"_"+id+"_3").val()=="I")
	    {
	    	$("#"+mainDivId).hide();
	    	 $("#errordiv").show();
	    	 $("#errordiv").append(resourceJSON.msgselectatleastoneCandidatetype);
	    }
	    else
	    {
		$("#"+HeadingDivId).text(HeadingText);
		$("#"+mainDivId).show();
		if($("#"+id+"EEid").val()!=0)
		{
			$('#'+selectId+' option[value="'+$("#"+id+"EEid").val()+'"]').attr("selected", "selected");
		};
		if($("#"+id+"IIid").val()!=0)
		{
			$('#'+selectId1+' option[value="'+$("#"+id+"IIid").val()+'"]').attr("selected", "selected");
		}
		if($("#"+id+"TTid").val()!=0)
		{
			$('#'+selectId2+' option[value="'+$("#"+id+"TTid").val()+'"]').attr("selected", "selected");
		}
		/*$("#"+headingId).html(HeadingText+" "+resourceJSON.msgforExternal);
		$("#"+headingId1).html(HeadingText+" "+resourceJSON.msgforInternal);
		$("#"+headingId2).html(HeadingText+" "+resourceJSON.msgforInternalTransfer);*/
		$("#"+headingId).html(resourceJSON.msgforExternal+" "+HeadingText);
		$("#"+headingId1).html(resourceJSON.msgforInternal+" "+HeadingText);
		$("#"+headingId2).html(resourceJSON.msgforInternalTransfer+" "+HeadingText);
	    }
	    if($("#"+id+"_"+id+"_1").val()=="A" && $("#section0").is(":checked")){
	    $("#"+checkAllId).attr("checked",true);
	    }
	    showdisabled(Number(id)-1);
	    if(id==1){
			$(".portfolioLbl").attr("style","width:25%;");
			$(".portfolioText").attr("style","width:73%;");
		}else if(id==2){
			$(".preLbl").attr("style","width:36%;");
			$(".preText").attr("style","width:64%;");
		}else if(id==3){
			$(".epiLbl").attr("style","width:25%;");
			$(".epiText").attr("style","width:73%;");
		}else if(id==4){
			$(".customLbl").attr("style","width:32%;");
			$(".customTxt").attr("style","width:65%;");
		}else if(id==5){
			$(".onBoardLbl").attr("style","width:29%;");
			$(".onBoardTxt").attr("style","width:69%;");
		}else if(id==8){
			$(".eRefLbl").attr("style","width:34%;");
			$(".eRefTxt").attr("style","width:63%;");
		}
}

function showSingleTypeVVI(id,type,headingId,headingText)
{
	$("#VVIDiv").show();
	if(type=='E')
	{
		//$("#"+headingId).text(headingText+" "+resourceJSON.msgforExternal+" "+resourceJSON.msgcandidate);
		$("#"+headingId).text(resourceJSON.msgforExternal+" "+headingText);
		if($("#6"+type+type+"id").val()>0)
		{
		$("#quesName0").val($("#6"+type+type).val());
		$("#quesId0").val($("#6"+type+type+"id"));
		}
	}
	else if(type=='I')
	{
		//$("#"+headingId).text(headingText+" "+resourceJSON.msgforInternal+" "+resourceJSON.msgcandidate);
		$("#"+headingId).text(resourceJSON.msgforInternal+" "+headingText);
		if($("#6"+type+type+"id").val()>0)
		{
		$("#quesName1").val($("#6"+type+type).val());
		$("#quesId1").val($("#6"+type+type+"id"));
		}
	}
	else if(type=='T')
	{
		//$("#"+headingId).text(headingText+" "+resourceJSON.msgforInternalTransfer+" "+resourceJSON.msgcandidate);
		$("#"+headingId).text(resourceJSON.msgforInternalTransfer+" "+headingText);
		if($("#6"+type+type+"id").val()>0)
		{
		$("#quesName2").val($("#6"+type+type).val());
		$("#quesId2").val($("#6"+type+type+"id"));
		}
	}	
}
function showMultitypeForVVI(id,showSelect,showSelect1,showSelect2,HeadingText,HeadingDivId,headingId,headingId1,headingId2)
{
	   $(".VVIMultiType").show();
	   var valueCounter=0;
	   var typeArr=['E','I','T'];
	   for(var i=0;i<3;i+=1)
	   {
		   if($("#6"+typeArr[i]+typeArr[i]+"id").val()!=0){
			   valueCounter=1;
		   }
	   }
	   if(valueCounter==1)
		   $("#diffQuestionSet").prop("checked",true);
	   else
		   $("#sameQuestionSet").prop("checked",true);
	   
	    showHideSingleMultiple();	    
	    if($("#"+id+"_"+id+"_1").val()=="I" && $("#"+id+"_"+id+"_2").val()=="I" && $("#"+id+"_"+id+"_3").val()=="I"){
	    	$("#VVIDiv").hide();
	    	 $("#errordiv").show();
	    	 $("#errordiv").append(resourceJSON.msgselectatleastoneCandidatetype);
	    }else{
		$("#"+HeadingDivId).text(resourceJSON.msgVirtualVideoInterview);
		$("#VVIDiv").show(); 
		if($("#"+id+"EEid").val()!=0){
			$('#quesId0').val($("#"+id+"EEid").val());
			$('#quesName0').val($("#"+id+"EE").val());
		};
		if($("#"+id+"IIid").val()!=0){
			$('#quesId1').val($("#"+id+"IIid").val());
			$('#quesName1').val($("#"+id+"II").val());
		}
		if($("#"+id+"TTid").val()!=0){
			$('#quesId2').val($("#"+id+"TTid").val());
			$('#quesName2').val($("#"+id+"TT").val());
		}
		/*$("#"+headingId).html(HeadingText+" "+resourceJSON.msgforExternal);
		$("#"+headingId1).html(HeadingText+" "+resourceJSON.msgforInternal);
		$("#"+headingId2).html(HeadingText+" "+resourceJSON.msgforInternalTransfer);*/
		$("#"+headingId).html(resourceJSON.msgforExternal+" "+HeadingText);
		$("#"+headingId1).html(resourceJSON.msgforInternal+" "+HeadingText);
		$("#"+headingId2).html(resourceJSON.msgforInternalTransfer+" "+HeadingText);
	    }	
}
//********************* End *****************************************************************8
function saveDivRecord()
{
	var id=$("#activeId").val();
 	var type=$("#activeType").val();
 	var textValue=$("#colId0").val();
 	var selectValue=$('#colId0 option:selected').text();
 	var saveType=$("#showType").val();
 	var arr=['E','I','T'];
 	if(saveType==1){
 	if(textValue==0){
 		if($("#allPortfolio").attr("checked")){
 			saveForAllInSingle(id,resourceJSON.msgNoPortfolioConfigured,0);
 			hidePortfolioDiv();
 		}else{
 	 		saveForSingle(id,type,resourceJSON.msgNoPortfolioConfigured,0);
	 	 	hidePortfolioDiv();
 	 	 }
 	}else{
 		var seletedText=$("#colId0 option:selected").text();
 		var selectid=$("#colId0 option:selected").val();
 	 	if($("#allPortfolio").attr("checked")){
 	 		if(findAvailableId(id,"colId0",selectid,seletedText)){	
 	 		//saveForAllInSingle(id,seletedText,selectid);
 			hidePortfolioDiv();
 	 		}else{
 				//alert("Data not matched");
 			}
 		}else{
 	 		saveForSingle(id,type,seletedText,selectid);
		 	hidePortfolioDiv();
 	 	}
 	  }
    }else{
 		var counter=0;
 		if($("#allPortfolio").attr("checked"))
 		{
 	    	counter=verifyListAndInsertrecod(id,"colId0","colId1","colId2","errordivportfolio");
 			if(counter==1)
 				hidePortfolioDiv();
 		}else{
	 		insertWithoutAllSelection(id,"colId0","colId1","colId2");
	 		hidePortfolioDiv();
 		}
 	}
}

function savePreScreenRecord()
{
	var id=$("#activeId").val();
 	var type=$("#activeType").val();
 	var textValue=$("#qqAvlbList0").val();
 	var showstatus=$("#showType").val();
 	$("#errordivpre").empty();
 	$("#errordivpre").hide();
 	if(showstatus==1)
 	{
	 	if(textValue==0)
	 	{
	 		if($("#allPre").attr("checked"))
	 		{
	 			saveForAllInSingle(id,resourceJSON.msgNoPreScreenConfigured,0);
	 			hidePrePopup();
	 		}else{
	 			saveForSingle(id,type,resourceJSON.msgNoPreScreenConfigured,0);
			 	 	hidePrePopup();
	 		}
		}
	 	else
	 	{
	 		var seletedText=$("#qqAvlbList0 option:selected").text();
	 		var selectid=$("#qqAvlbList0 option:selected").val();
	 		if($("#allPre").attr("checked"))
	 		{
	 			saveForAllInSingle(id,seletedText,selectid);
	 			hidePrePopup();
	 		}else{
	 			saveForSingle(id,type,seletedText,selectid);
			 	 	hidePrePopup();
	 		}
	 	  }
 	}
 	else{
 		var counter=1;
 		if($("#allPre").attr("checked"))
 		{
 			counter=verifyListAndInsertrecod(id,"qqAvlbList0","qqAvlbList1","qqAvlbList2","errordivpre");
 			if(counter==1)
 				hidePrePopup();
 		}else{
	 		insertWithoutAllSelection(id,"qqAvlbList0","qqAvlbList1","qqAvlbList2");
	 		hidePrePopup();
 		}
 	}
}


function saveERefRecord()
{
	var id=$("#activeId").val();
 	var type=$("#activeType").val();
 	var textValue=$("#avlbList0").val();
 	var showstatus=$("#showType").val();
 	$("#errordiveref").empty();
 	$("#errordiveref").hide();
 	if(showstatus==1)
 	{
	 	if(textValue==0)
	 	{
	 		if($("#allERef").attr("checked")){
	 			saveForAllInSingle(id,resourceJSON.msgNoEReferenceConfigured,0);
	 			hideE_RefPopup();
	 		}else{
	 			saveForSingle(id,type,resourceJSON.msgNoEReferenceConfigured,0);
			 	 	hideE_RefPopup();
	 		}
		}else{
	 		var seletedText=$("#avlbList0 option:selected").text();
	 		var selectid=$("#avlbList0 option:selected").val();
	 		if($("#allERef").attr("checked"))
	 		{
	 			saveForAllInSingle(id,seletedText,selectid);
	 			hideE_RefPopup();
	 		}else{
	 			saveForSingle(id,type,seletedText,selectid);
			 	 	hideE_RefPopup();
	 		}
	 	  }
 	}else{
 		var counter=0;
 		if($("#allERef").attr("checked"))
 		{	
 			counter=verifyListAndInsertrecod(id,"avlbList0","avlbList1","avlbList2","errordiveref");
 			if(counter==1)
 				hideE_RefPopup();
 		}else{
	 		insertWithoutAllSelection(id,"avlbList0","avlbList1","avlbList2");
	 		hideE_RefPopup();
 		}
 	}
}


function saveEPIRecord()
{
	var id=$("#activeId").val();
 	var type=$("#activeType").val();
 	var textValue=$("#EPIId0").val();
 	var showstatus=$("#showType").val();
 	$("#errordivepi").empty();
 	$("#errordivepi").hide();
 	if(showstatus==1)
 	{
	 	if(textValue==0)
	 	{
	 		if($("#allEPI").attr("checked"))
	 		{
	 			saveForAllInSingle(id,resourceJSON.msgNoEPIConfigured,0);
	 			hideEPIDiv();
	 		}else{
	 			saveForSingle(id,type,resourceJSON.msgNoEPIConfigured,0);
			 	 	hideEPIDiv();
	 		}
		}else{
	 		var seletedText=$("#EPIId0 option:selected").text();
	 		var selectid=$("#EPIId0 option:selected").val();
	 		if($("#allEPI").attr("checked"))
	 		{
	 			saveForAllInSingle(id,seletedText,selectid);
	 		    hideEPIDiv();
	 		}else{
	 			saveForSingle(id,type,seletedText,selectid);
			 	 	hideEPIDiv();
	 		}
	 	  }
 	}
 	else
 	{
 		var counter=0;
 		if($("#allEPI").attr("checked"))
 		{
 			counter=verifyListAndInsertrecod(id,"EPIId0","EPIId1","EPIId2","errordivepi");
 			if(counter==1)
 				hideEPIDiv();
 		}else{
	 		insertWithoutAllSelection(id,"EPIId0","EPIId1","EPIId2");	
	 		hideEPIDiv();
 		}
 	}
}


//****************** For the Custom Question ***************************
function saveJSIRecord()
{
	var id=$("#activeId").val();
 	var type=$("#activeType").val();
 	var textValue=$("#JSIId0").val();
 	var showstatus=$("#showType").val();
 	$("#errordivepi").empty();
 	$("#errordivepi").hide();
 	if(showstatus==1)
 	{
	 	if(textValue==0)
	 	{
	 		if($("#allJSI").attr("checked"))
	 		{
	 			saveForAllInSingle(id,resourceJSON.msgNoCustomQuestionConfigured,0);
	 			hideJSIDiv();
	 		}else{
	 			saveForSingle(id,type,resourceJSON.msgNoCustomQuestionConfigured,0);
			 	 	hideJSIDiv();
	 		}
		}else{
	 		var seletedText=$("#JSIId0 option:selected").text();
	 		var selectid=$("#JSIId0 option:selected").val();
	 		if($("#allJSI").attr("checked"))
	 		{
	 			saveForAllInSingle(id,seletedText,selectid);
	 		    hideJSIDiv();
	 		}else{
	 			saveForSingle(id,type,seletedText,selectid);
			 	 	hideJSIDiv();
	 		}
	 	  }
 	}
 	else
 	{
 		var counter=1;
 		if($("#allJSI").attr("checked"))
 		{
 			counter=verifyListAndInsertrecod(id,"JSIId0","JSIId1","JSIId2","errordivjsi");
 			if(counter==1)
 				hideJSIDiv();
 		}else{
	 		insertWithoutAllSelection(id,"JSIId0","JSIId1","JSIId2");
	 		hideJSIDiv();
 		}
 	}
}

function saveOnBoardRecord()
{
	var id=$("#activeId").val();
 	var type=$("#activeType").val();
 	var textValue=$("#qqAvlbListForOnboard0").val();
 	var showstatus=$("#showType").val();
 	$("#errordivpreon").empty();
 	$("#errordivpreon").hide();
 	if(showstatus==1)
 	{
	 	if(textValue==0)
	 	{
	 		if($("#allPreOn").attr("checked"))
	 		{
	 			saveForAllInSingle(id,resourceJSON.msgNoOnBoardingConfigured,0);
	 			hidePreOnPopup();
	 		}else{
	 			saveForSingle(id,type,resourceJSON.msgNoOnBoardingConfigured,0)
			 	hidePreOnPopup();
	 		}
		}
	 	else
	 	{
	 		var seletedText=$("#qqAvlbListForOnboard0 option:selected").text();
	 		var selectid=$("#qqAvlbListForOnboard0 option:selected").val();
	 		if($("#allPreOn").attr("checked"))
	 		{
	 			saveForAllInSingle(id,seletedText,selectid);
	 			hidePreOnPopup();
	 		}else{
	 			saveForSingle(id,type,seletedText,selectid);
	 			hidePreOnPopup();
	 		}
	 	  }
 	}
 	else
 	{
 		var counter=0;
 		if($("#allPreOn").attr("checked"))
 		{
 			counter=verifyListAndInsertrecod(id,"qqAvlbListForOnboard0","qqAvlbListForOnboard1","qqAvlbListForOnboard2","errordivpreon");			
 			if(counter==1)
 				hidePreOnPopup();		
 		}else{
	 		insertWithoutAllSelection(id,"qqAvlbListForOnboard0","qqAvlbListForOnboard1","qqAvlbListForOnboard2");
	 		hidePreOnPopup();
 		}
 	}
}

function saveAssessmentRecord()
{
	var id=$("#activeId").val();
 	var type=$("#activeType").val();
 	var textValue=0
 	var showstatus=$("#showType").val();
 	$("#errordivpreon").empty();
 	$("#errordivpreon").hide();
 	var schoolSelection = $("#schoolSelection").is(":checked");
 	if(schoolSelection)
 		$("#schoolSelectionHidden").val(1);
 	else
 		$("#schoolSelectionHidden").val(0);
 	if(showstatus==1)
 	{
 		if(type=='E')
 			textValue=$("#selectId0").val();
 		else if(type=='I')
 			textValue=$("#selectId1").val();
 		else if(type=='T')
 			textValue=$("#selectId2").val();
 		
	 	if(textValue==0)
	 	{
	 		if($("#allAss").attr("checked"))
	 		{
	 			saveForAllInSingle(id,resourceJSON.msgNoAssessmentConfigured,0);
	 			resetAllAss();
	 			hideAssPopup();
	 		}else{
	 			saveForSingle(id,type,resourceJSON.msgNoAssessmentConfigured,0)
	 			hideAssPopup();
	 		}
		}else{
	 		var seletedText="";
	 		var selectid=0;
	 		if(type=='E'){
 				seletedText=$("#selectValue0").val();
 				selectid=$("#selectId0").val();
 			}else if(type=='I'){
	 			seletedText=$("#selectValue1").val();
	 			selectid=$("#selectId1").val();
	 		}else if(type=='T'){
 				seletedText=$("#selectValue2").val();
 				selectid=$("#selectId2").val();
 			}
	 		if($("#allAss").attr("checked"))
	 		{
	 			var arr=['E','I','T'];
	 		    for(var i=0;i<3;i+=1)
	 			{
	 				if($("#"+id+"_"+id+"_"+(i+1)).val()=="A")
	 				{
	 					$("#"+id+arr[i]+arr[i]).val(seletedText);
	 	 				$("#"+id+arr[i]+arr[i]+"id").val(selectid);
	 	 				if(selectid==0)
	 	 				{
	 	 					$("#"+id+arr[i]).css("background-color","#73D2EF");
	 	 					$("#"+id+arr[i]).html(resourceJSON.instN);
	 	 				}
	 	 				else
	 	 				{
	 	 					$("#"+id+arr[i]).css("background-color","rgb(0, 147, 294)");
	 	 					$("#selectValue"+i).val(seletedText);
		 	 				$("#selectId"+i).val(selectid);
		 	 				$("#"+id+arr[i]).html(resourceJSON.instC);
	 	 				}
	 			 	 	$("#"+id+arr[i]).attr('data-original-title',seletedText);
	 				}
	 			}
	 			hideAssPopup();
	 		}
	 		else
	 		{
	 			var seletedText1="";
		 		var selectid1="";
	 			if(type=='E')
	 			{
	 				seletedText1=$("#selectValue0").val();
	 				selectid1=$("#selectId0").val();
	 			}
	 			else if(type=='I')
		 			 {
		 				seletedText1=$("#selectValue1").val();
		 				selectid1=$("#selectId1").val();
		 			 }
	 			else if(type=='T')
	 			 {
	 				seletedText1=$("#selectValue2").val();
	 				selectid1=$("#selectId2").val();
	 			 }
	 			$("#"+id+type+type+"id").val(selectid);
	 		    if(selectid1==0)
	 		    {
	 				$("#"+id+type).css("background-color","#73D2EF");
	 				 $("#"+id+type+type).val("");
	 				$("#"+id+type).html(resourceJSON.instN);
	 		    }
	 			else
	 			{
	 			    $("#"+id+type).css("background-color","rgb(0, 147, 294)");
	 			    $("#"+id+type+type).val(seletedText1);
	 			   $("#"+id+type).html(resourceJSON.instC);
	 			}
	 		 	$("#"+id+type).attr('data-original-title',seletedText1);
	 			hideAssPopup();
	 		}
	 	  }
 	}
 	else
 	{
 		var counter=0;
 		if($("#allAss").attr("checked"))
 		{
 			counter=verifyListAndInsertrecodForAssessment(id,"errordivassessment");			
 			if(counter==1)
 				hideAssPopup();		
 		}
 		else
 		{
 			insertWithoutAllSelectionForAssessment(id);
 			hideAssPopup();
 		}
 	}
}
function saveVVIRecord()
{

	var id=$("#activeId").val();
 	var type=$("#activeType").val();
 	var textValue=0
 	var showstatus=$("#showType").val();
 	$("#errordivvvi").empty();
 	$("#errordivvvi").hide();
 	if(document.getElementById("wantScore").checked==true)
 	{
 		if($("#maxScoreForVVI").val().length<=0)
 		{
 			$("#errordivvvi").show();
 			$("#errordivvvi").append(resourceJSON.msgEnterMaxMarks);
 			$("#maxScoreForVVI").focus();
 			return false;
 		}
 		else
 		{
 			$("#maxScoreForVVIHidden").val($("#maxScoreForVVI").val());
 		}
 	}
 	if(document.getElementById("sendAutoVVILink").checked==true)
 	{
 		$("#statusDivHidden").val($("#statusDiv").val());
 		$("#sendAutoVVILinkHidden").val(1);
 	}
 	else
 	{
 		$("#statusDivHidden").val("");
 		$("#sendAutoVVILinkHidden").val(0);
 	}
 	if(document.getElementById("approvalByPredefinedGroups").checked==true)
 	{
 		$("#approvalByPredefinedGroupsHidden").val(1);
 	}
 	else
 	{
 		$("#approvalByPredefinedGroupsHidden").val(0);
 	}
 	$("#timePerQuesHidden").val($("#timePerQues").val());
 	$("#vviExpDaysHidden").val($("#vviExpDays").val());
 	
 	if(showstatus==1)
 	{
 		if(type=='E')
 			textValue=$("#quesId0").val();
 		else if(type=='I')
 			textValue=$("#quesId1").val();
 		else if(type=='T')
 			textValue=$("#quesId2").val();
 		
	 	if(textValue==0)
	 	{
	 		if($("#allVVI").attr("checked"))
	 		{
	 			saveForAllInSingle(id,resourceJSON.msgNoVVIConfigured,0);
	 			hideVVIPopup();
	 		}
	 		else
	 		{
	 			saveForSingle(id,type,resourceJSON.msgNoVVIConfigured,0);
	 			hideVVIPopup();
	 		}
		}
	 	else
	 	{
	 		var seletedText="";
	 		var selectid=0;
	 		
	 		if(type=='E')
 			{
 				seletedText=$("#quesName0").val();
 				selectid=$("#quesId0").val();
 			}
 			if(type=='I')
	 		{
	 				seletedText=$("#quesName1").val();
	 				selectid=$("#quesId1").val();
	 		}
 			if(type=='T')
 			{
 				seletedText=$("#quesName2").val();
 				selectid=$("#quesId2").val();
 			}
	 		if($("#allVVI").attr("checked"))
	 		{
	 			var arr=['E','I','T'];
	 		    for(var i=0;i<3;i+=1)
	 			{
	 				if($("#"+id+"_"+id+"_"+(i+1)).val()=="A")
	 				{
	 					$("#"+id+arr[i]+arr[i]).val(seletedText);
	 	 				$("#"+id+arr[i]+arr[i]+"id").val(selectid);
	 	 				if(selectid==0)
	 	 				{
	 	 					$("#"+id+arr[i]).css("background-color","#73D2EF");
	 	 					$("#"+id+arr[i]).html(resourceJSON.instN);
	 	 				}
	 	 				else
	 	 				{
	 	 					$("#"+id+arr[i]).css("background-color","rgb(0, 147, 294)");
	 	 					$("#quesName"+i).val(seletedText);
		 	 				$("#quesId"+i).val(selectid);
		 	 				$("#"+id+arr[i]).html(resourceJSON.instC);
	 	 				}
	 			 	 	$("#"+id+arr[i]).attr('data-original-title',seletedText);
	 				}
	 			}
	 		   hideVVIPopup();
	 		}
	 		else
	 		{
	 			var seletedText1="";
		 		var selectid1="";
	 			if(type=='E')
	 			{
	 				seletedText1=$("#quesName0").val();
	 				selectid1=$("#quesId0").val();
	 			}
	 			if(type=='I')
	 			{
	 				seletedText1=$("#quesName1").val();
	 				selectid1=$("#quesId1").val();
	 			}
	 			if(type=='T')
	 			{
	 				seletedText1=$("#quesName2").val();
	 				selectid1=$("#quesId2").val();
	 			}
	 			$("#"+id+type+type+"id").val(selectid);
	 				 			
	 		    if(selectid1==0)
	 		    {
	 				$("#"+id+type).css("background-color","#73D2EF");
	 				 $("#"+id+type+type).val("");
	 				$("#"+id+type).html(resourceJSON.instN);
	 		    }
	 			else
	 			{
	 			    $("#"+id+type).css("background-color","rgb(0, 147, 294)");
	 			    $("#"+id+type+type).val(seletedText1);
	 			   $("#"+id+type).html(resourceJSON.instC);
	 			}
	 		 	$("#"+id+type).attr('data-original-title',seletedText1);
	 		 	hideVVIPopup();
	 		}
	 	  }
 	}
 	else
 	{
 		var counter=0;
 		if($("#allVVI").attr("checked"))
 		{
 			counter=verifyListAndInsertrecodVVI(id,"","","","errordivvvi");
 			if(counter==1)
 				hideVVIPopup();		
 		}
 		else
 		{
 			insertWithoutAllSelection(id,"","","");
 			hideVVIPopup();
 		}
 	}

}
//********************* Master function *****************************************
function checkAllPortfolio(id,selectId)
{
	    var availableMsg="";
		var arr=['E','I','T'];
		var colId=['colId0','colId1','colId2'];
		var candidateType=['External','Internal','Internal Transfer'];
		var typeArr=['E','I','T'];
		var counter=0;
		var nextCount;
		var msgCount=0;
		var seletedText=$("#"+selectId+" option:selected").text();
 		var selectid=$("#"+selectId+" option:selected").val();
 		//alert("seletedText :::::::::::: "+seletedText+"     selectid  ::::::::   "+selectid)
	    for(var i=0;i<3;i+=1)
		{
	    	nextCount=0;
	    	      if(selectId!=colId[i]){
	    	        $("#"+colId[i]+" option").each(function()
	    			{
	    	        	if($(this).val()==selectid){
	    	        	counter+=1;
	    	        	nextCount=1;
	    	        	//singleValueConfigure(id,typeArr[i],seletedText);
	    	        	}
	    			});
	    	        if(nextCount==0){
	    	        	//resetPortfolioConfig(typeArr[i]);
	    	        	if($("#section"+i).is(":checked")){
		    	        	if(msgCount==0){
		    	    		availableMsg=candidateType[i];
		    	    		msgCount+=1;
		    	        	}else{
		    	        		availableMsg+=" and "+candidateType[i];
		    	        	}
	    	        	}
	    	    	}
	    	      }else{
	    	    	  //singleValueConfigure(id,typeArr[i],seletedText);
	    	      }
		}
	    $("#allPortfolio").prop("checked",false);
        showdisabled(0);
	    if(availableMsg!=""){
	              $("#allPortfolio").attr("disabled","disabled");
	    }else{
	    	if(selectId=="colId0")
	    		$("#allPortfolio").removeAttr("disabled");
	    }
}
function findAvailableId(id,selectId,seletedId,seletedText)
{
	var availableMsg="";
	var arr=['E','I','T'];
	var colId=['colId0','colId1','colId2'];
	var candidateType=['External','Internal','Internal Transfer'];
	var typeArr=['E','I','T'];
	var counter=0;
	var nextCount;
	var msgCount=0;
    for(var i=0;i<3;i+=1)
	{
    	nextCount=0;
    	      if(selectId!=colId[i]){
    	        $("#"+colId[i]+" option").each(function()
    			{
    	        	if($(this).val()==seletedId){
    	        	counter+=1;
    	        	nextCount=1;
    	        	singleValueConfigure(id,typeArr[i],seletedText);
    	        	}
    			});
    	        if(nextCount==0){
    	        	resetPortfolioConfig(typeArr[i]);
    	        	if($("#section"+i).is(":checked")){
	    	        	if(msgCount==0){
	    	    		availableMsg=candidateType[i];
	    	    		msgCount+=1;
	    	        	}else{
	    	        		availableMsg+=" and "+candidateType[i];
	    	        	}
    	        	}
    	    	}
    	      }else{
    	    	  singleValueConfigure(id,typeArr[i],seletedText);
    	      }
	}
    if(availableMsg!=""){
    	$("#warningmsg").html("&#149; "+"Portfolio is not configured for "+availableMsg+" Candidate Type.</br>");
    	 $("#portfoliowarning").modal("show");
    }else{
    	saveForAllInSingle(id,seletedText,seletedId);
    }
 return true;
}

function singleValueConfigure(id,type,seletedText)
{
	     $("#"+id+type).css("background-color","rgb(0, 147, 294)");
	     $("#"+id+type+type).val(seletedText);
	     $("#"+id+type).html(resourceJSON.instC);
	     $("#"+id+type).attr('data-original-title',seletedText);
}
function saveForAllInSingle(id,seletedText,seletedId)
{
	var arr=['E','I','T'];
	    for(var i=0;i<3;i+=1)
		{
			if($("#"+id+"_"+id+"_"+(i+1)).val()=="A")
			{
				$("#"+id+arr[i]+arr[i]).val(seletedText);
 				$("#"+id+arr[i]+arr[i]+"id").val(seletedId);
 				if(seletedId==0)
 				{
 					$("#"+id+arr[i]).css("background-color","#73D2EF");
 					$("#"+id+arr[i]).html(resourceJSON.instN);
 				}
 				else
 				{
 					$("#"+id+arr[i]).css("background-color","rgb(0, 147, 294)");
 					$("#"+id+arr[i]).html(resourceJSON.instC);
 				}
		 	 	$("#"+id+arr[i]).attr('data-original-title',seletedText);
			}
		}
}

function saveForSingle(id,type,seletedText,seletedId)
{
	   
	    $("#"+id+type+type+"id").val(seletedId);
	    if(seletedId==0)
	    {
			$("#"+id+type).css("background-color","#73D2EF");
			 $("#"+id+type+type).val("");
			 $("#"+id+type).html(resourceJSON.instN);
	    }
		else
		{
		    $("#"+id+type).css("background-color","rgb(0, 147, 294)");
		    $("#"+id+type+type).val(seletedText);
		    $("#"+id+type).html(resourceJSON.instC);
		}
	 	$("#"+id+type).attr('data-original-title',seletedText);
}

function insertWithoutAllSelection(id,selectId,selectId1,selectId2)
{
	var emptyMsg=[resourceJSON.msgNoPortfolioConfigured,resourceJSON.msgNoPreScreenConfigured,resourceJSON.msgNoEPIConfigured,resourceJSON.msgNoCustomQuestionConfigured,resourceJSON.msgNoOnBoardingConfigured,resourceJSON.msgNoVVIConfigured,resourceJSON.msgNoAssessmentConfigured,resourceJSON.msgNoEReferenceConfigured];
	var colId=colId1=colId2=0;
    var selectValue=selectValue1=selectValue2="";
        if(id==6)
        {
        	if($("#"+id+"_"+id+"_1").val()=="A")
			{
				colId=$("#quesId0").val();
				selectValue=$('#quesName0').val();
			}
			if($("#"+id+"_"+id+"_2").val()=="A")
			{
				colId1=$("#quesId1").val();
				selectValue1=$('#quesName1').val();
			}
			if($("#"+id+"_"+id+"_3").val()=="A")
			{
				colId2=$("#quesId2").val();
				selectValue2=$('#quesName2').val();
			}
        }
        else
        {
			if($("#"+selectId).val())
			{
				colId=$("#"+selectId).val();
				selectValue=$('#'+selectId+' option:selected').text();
			}
			if($("#"+selectId1).val())
			{
				colId1=$("#"+selectId1).val();
				selectValue1=$('#'+selectId1+' option:selected').text();
			}
			if($("#"+selectId2).val())
			{
				colId2=$("#"+selectId2).val();
				selectValue2=$('#'+selectId2+' option:selected').text();
			}
        }
		if(colId==0)
 		{
 			    $("#"+id+"EE").val("");
 			    $("#"+id+"EEid").val(colId);
		 		$("#"+id+"E").css("background-color","#73D2EF");
		 	 	$("#"+id+"E").attr('data-original-title',emptyMsg[id-1]);
		 	 	$("#"+id+"E").html(resourceJSON.instN);
 		}
 		else
 		{
 			$("#"+id+"EE").val(selectValue);
 			$("#"+id+"EEid").val(colId);
			$("#"+id+"E").css("background-color","rgb(0, 147, 294)");
	 	 	$("#"+id+"E").attr('data-original-title',selectValue);
	 	 	$("#"+id+"E").html(resourceJSON.instC);
 		}
 		if(colId1==0)
 		{
 			    $("#"+id+"II").val("");
 			    $("#"+id+"IIid").val(colId1);
		 		$("#"+id+"I").css("background-color","#73D2EF");
		 	 	$("#"+id+"I").attr('data-original-title',emptyMsg[id-1]);
		 	 	$("#"+id+"I").html(resourceJSON.instN);
 		}
 		else
 		{
 			$("#"+id+"II").val(selectValue1);
 			$("#"+id+"IIid").val(colId1);
			$("#"+id+"I").css("background-color","rgb(0, 147, 294)");
	 	 	$("#"+id+"I").attr('data-original-title',selectValue1);
	 	 	$("#"+id+"I").html(resourceJSON.instC);
 		}
 		if(colId2==0)
 		{
 			    $("#"+id+"TT").val("");
 			    $("#"+id+"TTid").val(colId2);
		 		$("#"+id+"T").css("background-color","#73D2EF");
		 	 	$("#"+id+"T").attr('data-original-title',emptyMsg[id-1]);
		 	 	$("#"+id+"T").html(resourceJSON.instN);
 		}
 		else
 		{
 			$("#"+id+"TT").val(selectValue2);
 			$("#"+id+"TTid").val(colId2);
			$("#"+id+"T").css("background-color","rgb(0, 147, 294)");
	 	 	$("#"+id+"T").attr('data-original-title',selectValue2);
	 	 	$("#"+id+"T").html(resourceJSON.instC);
 		}	
}

function insertWithoutAllSelectionForAssessment(id)
{
	var emptyMsg=[resourceJSON.msgNoPortfolioConfigured,resourceJSON.msgNoPreScreenConfigured,resourceJSON.msgNoEPIConfigured,resourceJSON.msgNoCustomQuestionConfigured,resourceJSON.msgNoOnBoardingConfigured,resourceJSON.msgNoVVIConfigured,resourceJSON.msgNoAssessmentConfigured,resourceJSON.msgNoEReferenceConfigured];
	var colId=colId1=colId2=0;
    var selectValue=selectValue1=selectValue2="";
		if($("#selectId0").val())
		{
			colId=$("#selectId0").val();
			selectValue=$("#selectValue0").val();
		}
		if($("#selectId1").val())
		{
			colId1=$("#selectId1").val();
			selectValue1=$("#selectValue1").val();
		}
		if($("#selectId2").val())
		{
			colId2=$("#selectId2").val();
			selectValue2=$("#selectValue2").val();
		}
		if(colId==0)
 		{
 			    $("#"+id+"EE").val("");
 			    $("#"+id+"EEid").val(colId);
		 		$("#"+id+"E").css("background-color","#73D2EF");
		 	 	$("#"+id+"E").attr('data-original-title',emptyMsg[id-1]);
		 	 	$("#"+id+"E").html(resourceJSON.instN);
 		}
 		else
 		{
 			$("#"+id+"EE").val(selectValue);
 			$("#"+id+"EEid").val(colId);
			$("#"+id+"E").css("background-color","rgb(0, 147, 294)");
	 	 	$("#"+id+"E").attr('data-original-title',selectValue);
	 	 	$("#"+id+"E").html(resourceJSON.instC);
 		}
 		if(colId1==0)
 		{
 			    $("#"+id+"II").val("");
 			    $("#"+id+"IIid").val(colId1);
		 		$("#"+id+"I").css("background-color","#73D2EF");
		 	 	$("#"+id+"I").attr('data-original-title',emptyMsg[id-1]);
		 	 	$("#"+id+"I").html(resourceJSON.instN);
 		}
 		else
 		{
 			$("#"+id+"II").val(selectValue1);
 			$("#"+id+"IIid").val(colId1);
			$("#"+id+"I").css("background-color","rgb(0, 147, 294)");
	 	 	$("#"+id+"I").attr('data-original-title',selectValue1);
	 	 	$("#"+id+"I").html(resourceJSON.instC);
 		}
 		if(colId2==0)
 		{
 			    $("#"+id+"TT").val("");
 			    $("#"+id+"TTid").val(colId2);
		 		$("#"+id+"T").css("background-color","#73D2EF");
		 	 	$("#"+id+"T").attr('data-original-title',emptyMsg[id-1]);
		 	 	$("#"+id+"T").html(resourceJSON.instN);
 		}
 		else
 		{
 			$("#"+id+"TT").val(selectValue2);
 			$("#"+id+"TTid").val(colId2);
			$("#"+id+"T").css("background-color","rgb(0, 147, 294)");
	 	 	$("#"+id+"T").attr('data-original-title',selectValue2);
	 	 	$("#"+id+"T").html(resourceJSON.instC);
 		}	
}
function verifyListAndInsertrecod(id,selectId,selectId1,selectId2,errorDivId)
{
	var counter=0;
	var emptyMsg=[resourceJSON.msgNoPortfolioConfigured,resourceJSON.msgNoPreScreenConfigured,resourceJSON.msgNoEPIConfigured,resourceJSON.msgNoCustomQuestionConfigured,resourceJSON.msgNoOnBoardingConfigured,resourceJSON.msgNoVVIConfigured,resourceJSON.msgNoAssessmentConfigured,resourceJSON.msgNoEReferenceConfigured];
	var precolId=precolId1=precolId2=0;
	var preselectValue=preselectValue1=preselectValue2="";
	$("#"+errorDivId).empty();
		if(id==6)
		{
			if($("#"+id+"_"+id+"_1").val()=="A")
			{
				precolId=$("#quesId0").val();
				preselectValue=$('#quesName0').val();
			}
//			if($("#"+id+"_"+id+"_2").val()=="A")
//			{
//				precolId1=$("#quesId1").val();
//				preselectValue1=$('#quesName1').val();
//			}
//			if($("#"+id+"_"+id+"_3").val()=="A")
//			{
//				precolId2=$("#quesId2").val();
//				preselectValue2=$('#quesName2').val();
//			}
		}
		else
		{
			if($("#"+id+"_"+id+"_1").val()=="A")
			{
				precolId=$("#"+selectId).val();
				preselectValue=$('#'+selectId+' option:selected').text();
			}
//			if($("#"+id+"_"+id+"_2").val()=="A")
//			{
//				precolId1=$("#"+selectId1).val();
//				preselectValue1=$('#'+selectId1+' option:selected').text();
//			}
//			if($("#"+id+"_"+id+"_3").val()=="A")
//			{
//				precolId2=$("#"+selectId2).val();
//				preselectValue2=$('#'+selectId2+' option:selected').text();
//			}
		}
		if(precolId!=0)
			{
				if(precolId1!=0)
				{
					if(precolId2!=0)
	 				{
	 					if(precolId==precolId1 && precolId==precolId2 && precolId1==precolId2)
	 					{
	 						insertRecordOnSelection(id,preselectValue,preselectValue,precolId);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						if(precolId==precolId1)
						{
							insertRecordOnSelection(id,preselectValue,preselectValue,precolId);	
							counter=1;
						}
						else
							counter=2;
					}
				}
				else
				{
					if(precolId2!=0)
	 				{
	 					if(precolId==precolId2)
	 					{
	 						insertRecordOnSelection(id,preselectValue,preselectValue,precolId);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						insertRecordOnSelection(id,preselectValue,preselectValue,precolId);
						counter=1;
					}
				}
			}
			else
			{
				if(precolId1!=0)
				{
					if(precolId2!=0)
	 				{
	 					if(precolId1==precolId2)
	 					{
	 						insertRecordOnSelection(id,preselectValue1,preselectValue1,precolId1);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						insertRecordOnSelection(id,preselectValue1,preselectValue1,precolId1);
						counter=1;
					}
				}
				else
				{
					if(precolId2!=0)
					{
						insertRecordOnSelection(id,preselectValue2,preselectValue2,precolId2);
						hideEPIDiv();
					}
					else
					{
						insertRecordOnSelection(id,"",emptyMsg[(id-1)],0);
						counter=1;
					}
				}	
			}
		//alert("Id ::::::: "+id+" Counter ::::::: "+counter);
		if(id==1 && counter!=2)
		{
			hidePortfolioDiv();
			if(precolId!=0){
				findAvailableId(id,"colId0",precolId,preselectValue);
			}else if(precolId1!=0){
				findAvailableId(id,"colId1",precolId1,preselectValue1);
			}else if(precolId2!=0){
				findAvailableId(id,"colId2",precolId2,preselectValue2);
			}
		  }
		    if(counter==2)
			{
				$("#"+errorDivId).show();
				if(id==6)
				{
					if($("#"+id+"_"+id+"_1").val()=="A")
						$("#quesName0").focus();
					else if($("#"+id+"_"+id+"_2").val()=="A")
						$("#quesName1").focus();
					else if($("#"+id+"_"+id+"_3").val()=="A")
						$("#quesName2").focus();	
				}
				else
				{
					if($("#"+id+"_"+id+"_1").val()=="A")
						$("#"+selectId).focus();
					else if($("#"+id+"_"+id+"_2").val()=="A")
						$("#"+selectId1).focus();
					else if($("#"+id+"_"+id+"_3").val()=="A")
						$("#"+selectId2).focus();
				}
			  $("#"+errorDivId).append(resourceJSON.msgMatchErrorCheck);
			}
		 else
			 return counter;
}
function verifyListAndInsertrecodVVI(id,selectId,selectId1,selectId2,errorDivId)
{
	var counter=0;
	var emptyMsg=[resourceJSON.msgNoPortfolioConfigured,resourceJSON.msgNoPreScreenConfigured,resourceJSON.msgNoEPIConfigured,resourceJSON.msgNoCustomQuestionConfigured,resourceJSON.msgNoOnBoardingConfigured,resourceJSON.msgNoVVIConfigured,resourceJSON.msgNoAssessmentConfigured,resourceJSON.msgNoEReferenceConfigured];
	var precolId=precolId1=precolId2=0;
	var preselectValue=preselectValue1=preselectValue2="";
         if($("#sameQuestionSet").prop("checked")==true)
         {
        	 if($("#"+id+"_"+id+"_1").val()=="A")
 			{
 				precolId=$("#quesId0").val();
 				preselectValue=$('#quesName0').val();
 			}
        	 else if($("#"+id+"_"+id+"_2").val()=="A")
 			{
 				precolId=$("#quesId1").val();
 				preselectValue=$('#quesName1').val();
 			}
        	 else if($("#"+id+"_"+id+"_3").val()=="A")
 			{
 				precolId=$("#quesId2").val();
 				preselectValue=$('#quesName2').val();
 			}
        	 	if(precolId!=0)
				{
					insertRecordOnSelection(id,preselectValue,preselectValue,precolId);
					hideVVIPopup();
				}
				else
				{
					insertRecordOnSelection(id,"",emptyMsg[(id-1)],0);
					counter=1;
				}
         }
         else
         {
			if($("#"+id+"_"+id+"_1").val()=="A")
			{
				precolId=$("#quesId0").val();
				preselectValue=$('#quesName0').val();
			}
			if($("#"+id+"_"+id+"_2").val()=="A")
			{
				precolId1=$("#quesId1").val();
				preselectValue1=$('#quesName1').val();
			}
			if($("#"+id+"_"+id+"_3").val()=="A")
			{
				precolId2=$("#quesId2").val();
				preselectValue2=$('#quesName2').val();
			}
		
		    if(precolId!=0)
			{
				if(precolId1!=0)
				{
					if(precolId2!=0)
	 				{
	 					if(precolId==precolId1 && precolId==precolId2 && precolId1==precolId2)
	 					{
	 						insertRecordOnSelection(id,preselectValue,preselectValue,precolId);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						if(precolId==precolId1)
						{
							insertRecordOnSelection(id,preselectValue,preselectValue,precolId);	
							counter=1;
						}
						else
							counter=2;
					}
				}
				else
				{
					if(precolId2!=0)
	 				{
	 					if(precolId==precolId2)
	 					{
	 						insertRecordOnSelection(id,preselectValue,preselectValue,precolId);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						insertRecordOnSelection(id,preselectValue,preselectValue,precolId);
						counter=1;
					}
				}
			}
			else
			{
				if(precolId1!=0)
				{
					if(precolId2!=0)
	 				{
	 					if(precolId1==refcolId2)
	 					{
	 						insertRecordOnSelection(id,preselectValue1,preselectValue1,precolId1);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						insertRecordOnSelection(id,preselectValue1,preselectValue1,precolId1);
						counter=1;
					}
				}
				else
				{
					if(precolId2!=0)
					{
						insertRecordOnSelection(id,preselectValue2,preselectValue2,precolId2);
						hideEPIDiv();
					}
					else
					{
						insertRecordOnSelection(id,"",emptyMsg[(id-1)],0);
						counter=1;
					}
				}	
			}
         }
		    if(counter==2)
			{
				$("#"+errorDivId).show();
				
					if($("#"+id+"_"+id+"_1").val()=="A")
						$("#quesName0").focus();
					else if($("#"+id+"_"+id+"_2").val()=="A")
						$("#quesName1").focus();
					else if($("#"+id+"_"+id+"_3").val()=="A")
						$("#quesName2").focus();	
			
			  $("#"+errorDivId).append(resourceJSON.msgMatchErrorCheck);
			}
		   else
			 return counter;
}


function verifyListAndInsertrecodForAssessment(id,errorDivId)
{
	var counter=0;
	var emptyMsg=[resourceJSON.msgNoPortfolioConfigured,resourceJSON.msgNoPreScreenConfigured,resourceJSON.msgNoEPIConfigured,resourceJSON.msgNoCustomQuestionConfigured,resourceJSON.msgNoOnBoardingConfigured,resourceJSON.msgNoVVIConfigured,resourceJSON.msgNoAssessmentConfigured,resourceJSON.msgNoEReferenceConfigured];
	var precolId=precolId1=precolId2=0;
	var preselectValue=preselectValue1=preselectValue2="";
	$("#"+errorDivId).empty();
		if($("#"+id+"_"+id+"_1").val()=="A")
		{
			precolId=$("#selectId0").val();
			preselectValue=$('#selectValue0').val();
		}
		/*if($("#"+id+"_"+id+"_2").val()=="A")
		{
			precolId1=$("#selectId1").val();
			preselectValue1=$('#selectValue1').val();
		}
		if($("#"+id+"_"+id+"_3").val()=="A")
		{
			precolId2=$("#selectId2").val();
			preselectValue2=$('#selectValue2').val();
		}	*/
		if(precolId.length>1)
			{
				if(precolId1.length>1)
				{
					if(precolId2.length>1)
	 				{
	 					if(matchString(precolId,precolId1) && matchString(precolId,precolId2) && matchString(precolId1,precolId2))
	 					{
	 						insertRecordOnSelectionForAssessment(id,preselectValue,preselectValue,precolId);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						if(matchString(precolId,precolId1))
						{
							insertRecordOnSelectionForAssessment(id,preselectValue,preselectValue,precolId);	
							counter=1;
						}
						else
							counter=2;
					}
				}
				else
				{
					if(precolId2.length>1)
	 				{
	 					if(matchString(precolId,precolId2))
	 					{
	 						insertRecordOnSelectionForAssessment(id,preselectValue,preselectValue,precolId);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						insertRecordOnSelectionForAssessment(id,preselectValue,preselectValue,precolId);
						counter=1;
					}
				}
			}
			else
			{
				if(precolId1.length>1)
				{
					if(precolId2.length>1)
	 				{
	 					if(matchString(precolId1,precolId2))
	 					{
	 						insertRecordOnSelectionForAssessment(id,preselectValue1,preselectValue1,precolId1);
	 						counter=1;
	 					}
	 					else
	 						counter=2;
	 				}
					else
					{
						insertRecordOnSelectionForAssessment(id,preselectValue1,preselectValue1,precolId1);
						counter=1;
					}
				}
				else
				{
					if(precolId2.length>1)
					{
						insertRecordOnSelectionForAssessment(id,preselectValue2,preselectValue2,precolId2);
						hideEPIDiv();
					}
					else
					{
						insertRecordOnSelectionForAssessment(id,"",emptyMsg[(id-1)],0);
						counter=1;
					}
				}	
			}
		    if(counter==2)
			{
				$("#"+errorDivId).show();
				if($("#"+id+"_"+id+"_1").val()=="A")
					$(".hida0").focus();
				else if($("#"+id+"_"+id+"_2").val()=="A")
					$(".hida1").focus();
				else if($("#"+id+"_"+id+"_3").val()=="A")
					$(".hida2").focus();
				
			  $("#"+errorDivId).append(resourceJSON.msgMatchErrorCheck);
			}
		 else
			 return counter;
}

function resetAllAss()
{
	for(var i=0;i<3;i++)
	{
		$("#selectId"+i).val(0);
		$("#selectValue"+i).val(resourceJSON.msgSelectDistrictAssessment);
		$("#hida"+i).html(resourceJSON.msgSelectDistrictAssessment);
		$('input[name="asses'+i+'"]').each(function() {	
		    $(this).attr('checked', false);
		  });
	}
}
function insertRecordOnSelection(id,selectValue,selectValue1,colId)
{
	var arr=['E','I','T'];
	var typeArr=['section0','section1','section2']
	for(var i=0;i<3;i+=1)
	{
	  if($("#"+id+"_"+id+"_"+(i+1)).val()=="A" && $("#"+typeArr[i]).is(":checked"))
		{
			$("#"+id+arr[i]+arr[i]).val(selectValue);
			$("#"+id+arr[i]+arr[i]+"id").val(colId);
			if(colId==0)
			{
				$("#"+id+arr[i]).css("background-color","#73D2EF");
				$("#"+id+arr[i]).html(resourceJSON.instN);
			}
			else
			{
				$("#"+id+arr[i]).css("background-color","rgb(0, 147, 294)");
				$("#"+id+arr[i]).html(resourceJSON.instC);
			}
	 	 	$("#"+id+arr[i]).attr('data-original-title',selectValue1);
	 	 	
		}
	}
}
function insertRecordOnSelectionForAssessment(id,selectValue,selectValue1,colId)
{
	var arr=['E','I','T'];
	var typeArr=['section0','section1','section2']
	for(var i=0;i<3;i+=1)
	{
	  if($("#"+id+"_"+id+"_"+(i+1)).val()=="A" && $("#"+typeArr[i]).is(":checked"))
		{
			$("#"+id+arr[i]+arr[i]).val(selectValue);
			$("#"+id+arr[i]+arr[i]+"id").val(colId);
			if(colId==0)
			{
				$("#"+id+arr[i]).css("background-color","#73D2EF");
				$("#"+id+arr[i]).html(resourceJSON.instN);
			}
			else
			{
				$("#"+id+arr[i]).css("background-color","rgb(0, 147, 294)");
				$("#"+id+arr[i]).html(resourceJSON.instC);
			}
	 	 	$("#"+id+arr[i]).attr('data-original-title',selectValue1);
		}
	}
}
//************************* End **************************************************8
function hidePortfolioDiv()
{
	$('#colId option[value=""]').attr("selected", "selected");
	$('#colId1 option[value=""]').attr("selected", "selected");
	$('#colId2 option[value=""]').attr("selected", "selected");
	$("#tableoption").hide();	
}
function hidePrePopup()
{
	$('#qqAvlbList0 option[value=""]').attr("selected", "selected");
	$('#qqAvlbList1 option[value=""]').attr("selected", "selected");
	$('#qqAvlbList2 option[value=""]').attr("selected", "selected");
	$("#preScreen").hide();	
}
function hideEPIDiv()
{
	$('#EPIId0 option[value=""]').attr("selected", "selected");
	$('#EPIId1 option[value=""]').attr("selected", "selected");
	$('#EPIId2 option[value=""]').attr("selected", "selected");
	$("#EPIDiv").hide();	
}
function hideJSIDiv()
{
	$('#JSIId0 option[value=""]').attr("selected", "selected");
	$('#JSIId1 option[value=""]').attr("selected", "selected");
	$('#JSIId2 option[value=""]').attr("selected", "selected");
	$("#JSIDiv").hide();	
}
function hidePreOnPopup()
{
	$('#qqAvlbListForOnboard0 option[value=""]').attr("selected", "selected");
	$('#qqAvlbListForOnboard1 option[value=""]').attr("selected", "selected");
	$('#qqAvlbListForOnboard2 option[value=""]').attr("selected", "selected");
	$("#preScreenOnBoarding").hide();	
}
function hideE_RefPopup()
{
	$('#avlbList0 option[value=""]').attr("selected", "selected");
	$('#avlbList1 option[value=""]').attr("selected", "selected");
	$('#avlbList2 option[value=""]').attr("selected", "selected");
	$("#eref").hide();
}
function hideVVIPopup()
{
	$("#VVIDiv").hide();
	$("#sameQuestionSet").prop("checked",true);
	$("#maxScoreForVVI").val("");
	$("#wantScore").prop("checked",false);
	showMarksDiv();
	$("#sendAutoVVILink").prop("checked",false);
	showLinkDivNew();
	$("#timePerQues").val("");
	$("#vviExpDays").val("");
	for(var i=0;i<3;i+=1)
	{
		$("#quesId"+i).val("");
		$("#quesName"+i).val("");
	}
}
function hideAssPopup()
{
	$("#assessmentDiv").hide();
}

function hideOrShowCondidateType(type)
{
var sectionExternal=document.getElementById("section0");
var sectionInternal=document.getElementById("section1");
var sectionITransfer=document.getElementById("section2");
var counterFlag=0;
if(sectionExternal.checked){
	$(".externalDiv").show();
	counterFlag+=1;
}else
	$(".externalDiv").hide();

if(sectionInternal.checked){
	$(".internalDiv").show();
	counterFlag+=1;
}else
	$(".internalDiv").hide();

if(sectionITransfer.checked){
	$(".internalTDiv").show();
	counterFlag+=1;
}else
	$(".internalTDiv").hide();

if(counterFlag==0)
	$(".showhideedit").hide();
else
	$(".showhideedit").show();

$("#extraWidth").removeAttr("class");
for(var i=0;i<8;i+=1){
	$("#extraWidth"+i).removeAttr("class");
}
//alert("counterFlag :::::::: "+counterFlag);
if(counterFlag==1){
	$("#extraWidth").attr("class","col-sm-6 col-md-6 managewidth");
	for(var i=0;i<8;i+=1){
		$("#extraWidth"+i).attr("class","col-sm-6 col-md-6 managewidth");
	}
}
if(counterFlag==2){
	$("#extraWidth").attr("class","col-sm-4 col-md-4 managewidth");
	for(var i=0;i<8;i+=1){
		$("#extraWidth"+i).attr("class","col-sm-4 col-md-4 managewidth");
	}
}
if(counterFlag==3){
	$("#extraWidth").attr("class","col-sm-2 col-md-2 managewidth");
	for(var i=0;i<8;i+=1){
		$("#extraWidth"+i).attr("class","col-sm-2 col-md-2 managewidth");
	}
  }
}

function resetOptMend(type,sType)
{
	if(sType==1)
	{
		if($("#etype"+type).text()=="R")
		{
			$("#etype"+type).html(resourceJSON.instO);
			$("#etype"+type).attr('data-original-title',resourceJSON.msgOptional);
			$(".etype"+type).css("background-color","#ffc0cb");
			$("#etypeValue"+type).val("0");
		}
		else
		{
			$("#etype"+type).html(resourceJSON.instR);
			$("#etype"+type).attr('data-original-title',resourceJSON.msgRequired);
			$(".etype"+type).css("background-color","red");
			$("#etypeValue"+type).val("1");
		}
	}
	else
	{
		if($("#etypeValue"+type).val()==0)
		{
			$("#etype"+type).html(resourceJSON.instO);
			$("#etype"+type).attr('data-original-title',resourceJSON.msgOptional);
			$(".etype"+type).css("background-color","#ffc0cb");
			
		}
		else
		{
			$("#etype"+type).html(resourceJSON.instR);
			$("#etype"+type).attr('data-original-title',resourceJSON.msgRequired);
			$(".etype"+type).css("background-color","red");
		}
	}
}

/*function activeDeativeEPI(type)
{
	if($("#3"+type).text()=="R")
	{
		$("#3"+type).html(resourceJSON.instN);
		$("#3"+type).attr('data-original-title','Not Required');
		$("#3"+type).css("background-color","#73D2EF");
		$("#3"+type+type).val("0");
	}
	else
	{
		$("#3"+type).html(resourceJSON.instR);
		$("#3"+type).attr('data-original-title','Required');
		$("#3"+type).css("background-color","red");
		$("#3"+type+type).val("1");
	}
}*/
function getPortfolioNameByDistrict(districtId,colId,candidateType)
{
	$("#loadingDiv_jobEdit").show();
	 JobCategoryAjaxNew.getPortfolioNameByDistrictId(districtId,candidateType,{ 
			async: false,
			callback: function(data){
		       if(data!=null)
		       {
		    	   $("#"+colId).html(data);
		       }
		},
		});
	 $("#loadingDiv_jobEdit").hide();
}


function getAssessmentNameByAssessmentType(type,divId)
{
	var active=1;
	 JobCategoryAjaxNew.getAssessmentNameByAssessmentType(type,active,{ 
		    preHook:function(){$('#loadingDiv').show()},
			postHook:function(){$('#loadingDiv').hide()},
			async: false,
			callback: function(data){
		       if(data.length>0)
		       {
		    	   for(var i=0;i<=2;i+=1)
		    		   $("#"+divId+i).html(data);
		       }
		},
		});
}
/*function getAssessmentNameByAssessmentType(type,divId)
{
	 JobCategoryAjaxNew.getAssessmentNameByAssessmentType(type,{ 
		    preHook:function(){$('#loadingDiv').show()},
			postHook:function(){$('#loadingDiv').hide()},
			async: false,
			callback: function(data){
		       if(data.length<0)
		       {
		    	   for(var i=0;i<=2;i+=1)
		    		   $("#"+divId+i).html(data);
		       }
		},
		});
}*/

function getAssessmentNameByDistrictId(type,districtId)
{
           JobCategoryAjaxNew.getAssessmentNameByDistrictAndType(type,districtId,{ 
			async: false,
			callback: function(data){
		       if(data.length>0)
		       {
		    	   for(var i=0;i<=2;i+=1)
		    	   {
		    		   $("#JSIId"+i).html(data);
		    	   }
		       }
		       else
		       {
		    	   var candType=['E','I','T'];
		    	   var id=4;
		    	   for(var i=0;i<3;i++)
					{
		    		    $("#JSIId"+i).html("<option>"+resourceJSON.msgPleaseaSet+"</option>"); 
				        $("#"+id+candType[i]+candType[i]).val("");
						$("#"+id+candType[i]+candType[i]+"id").val(0);
					    $("#"+id+candType[i]).css("background-color","rgb(115, 210, 239)");
					 	$("#"+id+candType[i]).attr('data-original-title',resourceJSON.msgNoCustomQuestionConfigured);
					 	$("#"+id+candType[i]).html(resourceJSON.instN);
					}	
		       }
		},
		});
}

//*********************** Function for multi select *************************************     
      function selunselect0()
      {
         var checkboxes = document.getElementsByName('asses0');
         var vals = "";
         var valsArr =[];
         var p=0;
         var checkCounter=0;
         for (var i=0, n=checkboxes.length;i<n;i++) {
		  if (checkboxes[i].checked) 
		  {
			  checkCounter+=1;
			  valsArr[p]=checkboxes[i].value
			  if(p==0) 
			  {
			  vals +=checkboxes[i].title;
			  }
			  else
			  {
			  vals +=", "+checkboxes[i].title;
			  }
			  p+=1;
		  }
        }
        if(checkCounter==0)
        {
        $(".hida0").html(resourceJSON.msgSelectDistrictAssessment);
        $("#selectValue0").val(resourceJSON.msgSelectDistrictAssessment);
        $("#selectId0").val(0);
        }else
        {
        $(".hida0").html(vals);
        $("#selectValue0").val(vals);

        if(p==0)
        	$("#selectId0").val(0);
        else
          {
        	$("#selectId0").val(valsArr);
        	$("#7EE").val(vals);
        	$("#7EEid").val(valsArr);
        	$('#7E').attr('data-original-title', vals);
    		$('#7E').css('background-color', '#e2b194');
    		$('#7E').html(resourceJSON.instD);
          }
        }
      }
      
      function selunselect1()
      {
         var checkboxes = document.getElementsByName('asses1');
         var vals1 = "";
         var valsArr1 =[];
         var p=0;
         var checkCounter1=0;
         for (var i=0, n=checkboxes.length;i<n;i++) {
		  if (checkboxes[i].checked) 
		  {
			  checkCounter1+=1;
			  valsArr1[p]=checkboxes[i].value
			  if(p==0)
			  {
			  vals1 +=checkboxes[i].title;
			  }
			  else
			  {
			  vals1 +=", "+checkboxes[i].title;
			  }
			  p+=1;
		  }
        }
        if(checkCounter1==0)
        {
        $(".hida1").html(resourceJSON.msgSelectDistrictAssessment);
        $("#selectValue1").val(resourceJSON.msgSelectDistrictAssessment);
        $("#selectId1").val(0);
        }else
        {
        $(".hida1").html(vals1);
        $("#selectValue1").val(vals1);
        if(p==0)
        	$("#selectId1").val(0);
        else
        {
        	$("#selectId1").val(valsArr1);
        	$("#7II").val(vals1);
        	$("#7IIid").val(valsArr1);
        	$('#7I').attr('data-original-title', vals1);
    		$('#7I').css('background-color', '#e2b194');
    		$('#7I').html(resourceJSON.instD);
        }
        }
      }
      
      function selunselect2()
      {
         var checkboxes = document.getElementsByName('asses2');
         var vals2 = "";
         var valsArr2 =[];
         var p=0;
         var checkCounter2=0;
         for (var i=0, n=checkboxes.length;i<n;i++) {
		  if (checkboxes[i].checked) 
		  {
			  checkCounter2+=1;
			  valsArr2[p]=checkboxes[i].value
			  if(p==0){
			  vals2 +=checkboxes[i].title;
			  }else{
			  vals2 +=", "+checkboxes[i].title;
			  }
			  p+=1;
		  }
        }
        if(checkCounter2==0)
        {
        $(".hida2").html(resourceJSON.msgSelectDistrictAssessment);
        $("#selectValue2").val(resourceJSON.msgSelectDistrictAssessment);
        $("#selectId2").val(0);
        }
        else
        {
        $(".hida2").html(vals2);
        $("#selectValue2").val(vals2);
        if(p==0)
        	$("#selectId2").val(0);
        else
        {
        	$("#selectId2").val(valsArr2);
        	$("#7TT").val(vals2);
        	$("#7TTid").val(valsArr2);
        	$('#7T').attr('data-original-title', vals2);
    		$('#7T').css('background-color', '#e2b194');
    		$('#7T').html(resourceJSON.instD);
        }
        }
      }
  // ************ multi List checked based on Ids *********************************    
      function checkedMultiList(type)
      {  var nameArr=['0','1','2'];
    	  var selectIds=($("#selectId"+type).val()).split(",");
    	  for(var i=0;i<selectIds.length;i++)
    	  {
    		  $("input[name='asses"+nameArr[type]+"'][value='"+selectIds[i]+"']").attr("checked", true); 
    	  }
    	  if(type==0)
    		  $(".hida"+nameArr[type]).html($("#selectValue"+type).val());
    	  if(type==1)
    		  $(".hida"+nameArr[type]).html($("#selectValue"+type).val());
    	  if(type==2)
    		  $(".hida"+nameArr[type]).html($("#selectValue"+type).val());
      }
     
//************************* Match string one by one *********************************************
      Array.prototype.equals = function (array, strict) {
  	    if (!array)
  	        return false;

  	    if (arguments.length == 1)
  	        strict = true;

  	    if (this.length != array.length)
  	        return false;

  	    for (var i = 0; i < this.length; i++) {
  	        if (this[i] instanceof Array && array[i] instanceof Array) {
  	            if (!this[i].equals(array[i], strict))
  	                return false;
  	        }
  	        else if (strict && this[i] != array[i]) {
  	            return false;
  	        }
  	        else if (!strict) {
  	            return this.sort().equals(array.sort(), true);
  	        }
  	    }
  	    return true;
  	}

  	function matchString(p,q)
  	{
  	var x=p.split(',');
  	var y=q.split(',');
  	return(x.equals(y, false));
  	} 
  	//****************** Selected list of JSI *********************************
  	function autoSelectJSI()
  	{
  		var districtId=$("#districtId").val();
  		var candType=['E','I','T'];
  		var id=4;
  		getAssessmentNameByDistrictId(2,districtId);
  		var selectedIndex=$('select#JSIId0 option').length;
  		if(selectedIndex>1)
  		{
  			$("select#JSIId0").prop('selectedIndex', 1);
  			var list = document.getElementById("JSIId0");
  			var optionValue = list.options[1].text;
  			var optionId = list.options[1].value;
  			for(var i=0;i<3;i++)
  			{
  			$("#"+id+candType[i]+candType[i]).val(optionValue);
 			$("#"+id+candType[i]+candType[i]+"id").val(optionId);
			$("#"+id+candType[i]).css("background-color","rgb(226, 177, 148)");
	 	 	$("#"+id+candType[i]).attr('data-original-title',optionValue);
	 	 	$("#"+id+candType[i]).html(resourceJSON.instD);
  			}
  		}
  	}
  	
  	
  	//*********************************************************************************
  	function selectAssList()
  	{
           var checkboxes = document.getElementsByName('assData');
           var vals = "";
           var valsArr ="";
           var p=0;
           var checkCounter=0;
           for (var i=0, n=checkboxes.length;i<n;i++) {
  		  if (checkboxes[i].checked) 
  		  {
  			  checkCounter++;
  			  if(p==0)
  			  {
  			  vals +=checkboxes[i].title;
  			  valsArr+=checkboxes[i].value
  			  }
  			  else
  			  {
  			  vals +=", "+checkboxes[i].title;
  			  valsArr+="#"+checkboxes[i].value
  			  }
  			  p++;
  		  }
          }
          if(checkCounter==0)
          {
          $(".hidaAss").html(resourceJSON.msgSelectMembers);
          $("#selectAssId").val(0);
          }else
          {
          $(".hidaAss").html(vals);
          $("#selectAssId").val(valsArr);
          }
  	}
  	function resetSelectAssList()
  	{
           var checkboxes = document.getElementsByName('assData');
           var vals = "";
           var valsArr ="";
           var p=0;
           var checkCounter=0;
           for (var i=0, n=checkboxes.length;i<n;i+=1) {
	  		  if (checkboxes[i].checked) 
	  			  checkboxes[i].checked=false;	  			
          }
          if(checkCounter==0)
          {
          $(".hidaAss").html(resourceJSON.msgSelectMembers);
          $("#selectAssId").val(0);
          }
  	}
  	
  	function showHideSingleMultiple()
  	{
  		if($("#sameQuestionSet").prop("checked")==true)
  		{
	    		$(".VVI0").hide();
	 			$(".VVI1").hide();
	 			$(".VVI2").hide();
	 			if($("#6_6_1").val()=="A" && $("#section0").is(":checked"))
	  		    {
	  				$(".VVI0").show();
	  			   $("#captionForExternal").show();
	  		    }
	 			else if($("#6_6_2").val()=="A" && $("#section1").is(":checked"))
	  		    {
	  		    	$(".VVI1").show();
	  		    	$("#captionForInternal").show();
	  		    }
	 			else if($("#6_6_3").val()=="A" && $("#section2").is(":checked"))
	  		    {
	  		    	$(".VVI2").show();
	  		    	$("#captionForInternalTransfer").show();
	  		    	
	  		    }	
  		}
  		else if($("#diffQuestionSet").prop("checked")==true)
  		{
  			if($("#6_6_1").val()=="A" && $("#section0").is(":checked"))
  		    {
  				$(".VVI0").show();
  			   $("#captionForExternal").show();
  		    }
  		    if($("#6_6_2").val()=="A" && $("#section1").is(":checked"))
  		    {
  		    	$(".VVI1").show();
  		    	$("#captionForInternal").show();
  		    }
  		    if($("#6_6_3").val()=="A" && $("#section2").is(":checked"))
  		    {
  		    	$(".VVI2").show();
  		    	$("#captionForInternalTransfer").show();
  		    	
  		    }	
  		}
  	}

  	function mainGridActiveInactiveValidation()
  	{
  		var flag=false;
  		$("#errordiv").empty();
  		$("#errordiv").hide();
  		var ctype=['E','I','T'];
  		var typeArr=['section0','section1','section2'];
  		var categorytype=[resourceJSON.lblPortfolio,resourceJSON.lblPreScreen,resourceJSON.lblEPIGroup,resourceJSON.lblCustomQuestions,resourceJSON.lblOnBoarding,resourceJSON.lblVirtualVideointerview,resourceJSON.lblOnlineAssessment,resourceJSON.lblEReference];
  		var candidateType=[resourceJSON.lblExternal,resourceJSON.lblInternal,resourceJSON.lblInternalTransfer];
  		var arrValid=[];
  		var typeString="";
  		var focusCounter=0;
  		$('#jobcategory').css("background-color", "#FFFFFF");
		$('#parentJobCategoryId').css("background-color", "#FFFFFF");
		$('#subjobcategory').css("background-color", "#FFFFFF");
  		if($("#createJobCategory").is(":checked")==true)
  		{
  			
  			var jobcategory=trim($("#jobcategory").val());
  			if(jobcategory==null || jobcategory=="")
  			{
  				flag=true;
  				$('#errordiv').append("&#149; "+resourceJSON.msgPlsEnterJobCategory+"<br>");
  				$('#jobcategory').css("background-color", "#F5E7E1");
  				$("#jobcategory").focus();
  				focusCounter=1;
  			}
  		}
  		else
  		{
  			var subjobcategoryName=trim(document.getElementById('subjobcategory').value);
  			if($('#parentJobCategoryId').val()=="0"){
  				flag=true;
  				$('#errordiv').append("&#149; "+resourceJSON.msgJobCategoryName+"<br>");
  				if(focusCounter	==	0)
  					$('#parentJobCategoryId').focus();
  					$('#parentJobCategoryId').css("background-color", "#F5E7E1");
  					focusCounter=1;
  			}else if (subjobcategoryName=="")
  			{	
  				$('#errordiv').append("&#149; "+resourceJSON.msgSubJobCategory+"<br>");
  				if(focusCounter	==	0)
  				$('#subjobcategory').focus();
  				$('#subjobcategory').css("background-color", "#F5E7E1");
  				focusCounter=1;
  				flag=true;
  			}
  		}
  		for(var i=1;i<=8;i+=1)
  		{
  		 var arrCounter=0;
  		 for(var j=1;j<=3;j+=1)
  		 {
  			 if($("#"+i+"_"+i+"_"+j).val()=='A' && $("#"+typeArr[j-1]).is(":checked"))
  			 {
  				 if($("#"+i+ctype[j-1]+ctype[j-1]+"id").val()==0)
  				 {
  					arrValid[arrCounter]=candidateType[j-1];
  					arrCounter+=1;
  					flag=true;
  				 }
  				 
  			 }
  		 }
		if(arrValid.length==1)
			typeString=arrValid[0];
		else if(arrValid.length==2)
			typeString=arrValid[0]+" and "+arrValid[1];
		else if(arrValid.length==3)
			typeString=arrValid[0]+", "+arrValid[1]+" and "+arrValid[2];
		if(arrCounter>0)
		{
		$("#errordiv").append("&#149; "+categorytype[i-1]+" "+resourceJSON.msgisnotconfiguredforthe+" "+typeString+" "+resourceJSON.msgcandidate+"<br>");
		}
  	  }
/*  		if($("#approvalBeforeGoLive1").attr("checked")=="checked")
  		{
  			if($("#noOfApprovalNeeded").val()==0)
  			{
  				$("#errordiv").append("&#149; "+resourceJSON.msgPleaseofApprovalsNeeded+"<br>");
  				flag=true;
  			}
  		}*/
  		if(flag==true)
  		{
  			$("#errordiv").show();
  			 window.scrollTo(0, 0);
  		}
  		return flag;
  	}
  	
  	var showOfferCounterAdd=0;
  	function multipleAjaxMethods()
  	{
  		getOnBOardinByDistrict(districtId);
  		 // displayAllDistrictAssessment();
  		 // autoSelectJSI();
		 // getVVIDefaultFields(0);
		 // getStatusList();
		  //QualificationQuestionSet();
  		  $("#previousDistrictId").val(districtId);
  		 
  		var selectedId=$("#selectedId0").val();
  		if(selectedId!=0)
  		{
  			   selunselect0();
  			   selunselect1();
  			   selunselect2();
  		}
  		$('#loadingDiv_jobCatGrid').hide();
  	}
  	
  	
  	var editOfferCounterAdd=0;
  	function editmultipleAjaxMethods()
  	{
  		$('#loadingDiv').show();
  		var districtId = trim(document.getElementById("districtId").value);
  		var jobCategoryId=$('#jobcategoryId').val();
  		getPortfolioNameByDistrict(districtId,"colId0","E");
		   getPortfolioNameByDistrict(districtId,"colId1","I");
		   getPortfolioNameByDistrict(districtId,"colId2","T");
		   getOnBOardinByDistrict(districtId);
  			//getAssessmentNameByAssessmentType(1,"EPIId");
  			//getReferenceByDistrictList(jobCategoryId);
  			//displayAllDistrictAssessment();
  			//getAssessmentNameByDistrictId(2,districtId);
  			//getVVIDefaultFields(jobCategoryId);
  			//getStatusList();
  			//getQQSetForOnboardingByDistrictAndJobCategoryList(jobCategoryId);
  			$('#loadingDiv').hide();  		
  			return true;
  	}
  	function resetPortfolioConfig(type)
  	{
  	 
	  		$("#1"+type+type).val(resourceJSON.msgNoPortfolioConfigured);
	  		$("#1"+type+type+"id").val(0);
	  		$("#1"+type).css("background-color","rgb(115, 210, 239)");
	 	 	$("#1"+type).attr('data-original-title',resourceJSON.msgNoPortfolioConfigured);
	 	 	$("#1"+type).html(resourceJSON.instN);
  	}
  	function closeDiv()
  	{
      $("#portfoliowarning").modal("hide");
  	}
  	
  	function showdisabled(sectionId)
  	{
  		var divArr=['allPortfolio','allPre','allEPI','allJSI','allPreOn','allVVI','allAss','allERef'];
  		var divSectionId=['colId','qqAvlbList','EPIId','JSIId','qqAvlbListForOnboard','quesName','hida','avlbList'];
  		if(sectionId==6){
  			if($("#allAss").is(":checked")){
  				$(".multi1").css({"background-color":"#D3D3D3","text-decoration":"underline","cursor":"not-allowed"});
  				$(".multi2").css({"background-color":"#D3D3D3","text-decoration":"underline","cursor":"not-allowed"});
  			}else{
  				$(".multi1").css({"background-color":"","text-decoration":"","cursor":"pointer"});
  				$(".multi2").css({"background-color":"","text-decoration":"","cursor":"pointer"});
  			}
  			
  		}else{
  		if($("#"+divArr[sectionId]).is(":checked")){
  			$("#"+divSectionId[sectionId]+"1").attr('disabled','disabled');
  			$("#"+divSectionId[sectionId]+"2").attr('disabled','disabled');
  		}else{
  			$("#"+divSectionId[sectionId]+"1").removeAttr('disabled');
  			$("#"+divSectionId[sectionId]+"2").removeAttr('disabled');
  		 }
  		}
  	}
function findAllSelectedValue(section,select0,select1,select2,checkBoxId)
{
	var val1=0;
	var val2=0;
	var val3=0;
	if($("#"+select0).is(":visible")){
		val1=$("#"+select0).val();
	}
	if($("#"+select1).is(":visible")){
		val2=$("#"+select1).val();
	}
	if($("#"+select2).is(":visible")){
		val3=$("#"+select2).val();
	}
	//alert("1 . "+val1+" 2. "+val2+" 3. "+val3);
	if((val1=="" || val1==null) && (val2=="" || val2==null) && (val3=="" || val1==null)){
		 $("#"+checkBoxId).attr("checked",true);
		 showdisabled(section);
	}else{
		if(val2=="" && val3==""){
			$("#"+checkBoxId).attr("checked",true);
			showdisabled(section);
		}else{
		$("#"+checkBoxId).attr("checked",false);
		 showdisabled(section);
		}
	}
}

function setEntityType()
{
  		var entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="1" selected="selected">TM</option><option value="2">District</option>';
  		var district="<div class='col-sm-12 col-md-12 hide' id='districtSearchBox'><label id='captionDistrictOrSchool'>District name</label><span><input autocomplete='off' style='color:#555555; font-size: 14px;'  type='text' id='districtNameFilter'  value='' maxlength='100'  name='districtNameFilter' class='help-inline form-control' onfocus=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\" onkeyup=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\" onblur=\"hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');\"/></span><input type='hidden' id='districtIdFilter' value='${userMaster.districtId.districtId}' /><div id='divTxtShowDataFilter'  onmouseover=\"mouseOverChk('divTxtShowDataFilter','districtNameFilter')\" style='display:none;position:absolute;z-index:5000;' class='result' ></div>";
  		var btnRecord='<button onclick="searchJobCat();" class="flatbtn" style="background: #9fcf68;width:60%" id="save" title="Search"><strong>Search <i class="fa fa-check-circle"></i></strong></button>';
  		globalSearchSection(entityString,"","",district,btnRecord);
  		setTimeout(function(){ 
  		if($("#districtMasterId").val()!=0){
  			$("select#entityType").prop('selectedIndex', 1);
  			displayOrHideSearchBox();
  			$("#districtNameFilter").val($("#districtMasterName").val());
  			$("#districtIdFilter").val($("#districtMasterId").val());
  		}
  		}, 50);
}


function portfolioChangeNotificationInJobCategory(portfolioId,candidateType)
{
	//****************** Adding by deepak ***************************************
	if(candidateType=="E"){
		checkAllPortfolio(1,"colId0");
	}else if(candidateType=="I"){
		checkAllPortfolio(1,"colId1");
	}else if(candidateType=="T"){
		checkAllPortfolio(1,"colId2");
	}
	//***************************************************************************
	
	//alert("portfolioId "+portfolioId +" candidateType "+candidateType);
	$("#candidateType_AllEdit").val(candidateType);
	$("#applyToAllCandidateTypes").val("");
	var jobcategoryId = trim(document.getElementById("jobcategoryId").value);
	var applyToAllCandidateTypes=$("#allPortfolio").attr("checked");
	
	$("#applyToAllCandidateTypes").val(applyToAllCandidateTypes);
	
	var districtId=$("#districtIdFilter").val();
	if(districtId=="" ||districtId==undefined){
		districtId=$("#districtId").val();
	}
	if(jobcategoryId >0 && portfolioId!="" && portfolioId > 0)
	{
		JobCategoryAjaxNew.portfolioChangeNotification(portfolioId,jobcategoryId,candidateType,applyToAllCandidateTypes,districtId,{ 
		async: false,
		callback: function(data)
		{
			if(data!="")
			{
				document.getElementById("portfolioChangeWarningMsg").innerHTML=data;
				$("#tableoption").hide();
				$('#portfolioChangeWarning').modal('show');
				applyScrollOnTbl_PortfolioChngTblJob();
			}
		},
		});
	}
}

function portfolioChangeWarningCloseDiv()
{
	$("#portfoliowarning").modal("hide");
	
	var applyToAllCandidateTypes=$("#applyToAllCandidateTypes").val();
	//alert("applyToAllCandidateTypes "+applyToAllCandidateTypes);
	if(applyToAllCandidateTypes!="")
	{
		var candidateTypePortChange=$("#candidateTypePortChange").val();
		if(candidateTypePortChange!="")
			clickView(1,candidateTypePortChange,1);
		else
			clickView(1,'',2);
	}
	else
	{
		var candidateType_AllEdit=$("#candidateType_AllEdit").val();
		
		$("#portfoliowarning").modal("hide");
		$("#tableoption").show();
		
		if(candidateType_AllEdit=="E")
		{
			var EEid=$("#1EEid").val();
			$("#colId0").val(EEid);
		}
		else if(candidateType_AllEdit=="I")
		{
			var IIid=$("#1IIid").val();
			$("#colId1").val(IIid);
		}
		else if(candidateType_AllEdit=="T")
		{
			var TTid=$("#1TTid").val();
			$("#colId2").val(TTid);
		}
	}
	
}

function portfolioChangeWarningYes()
{
  $("#portfoliowarning").modal("hide");
  $("#tableoption").show();
}


function applyScrollOnTbl_PortfolioChngTblJob()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#portfolioChngTblJob').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 720,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,500,100], //144,130,136,185,136,146,97
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function defaultInactiveLayout()
{
		for(var i=1;i<=8;i++){
		     for(var j=1;j<=3;j++){
		    	 $('#'+i+'_'+j).attr('data-original-title', resourceJSON.msgActive);
		    	 deactivateSection(i,j);
		     }
		}
}

function checkSectionActiveOrInactive(i)
{
	var vflag=false;
	var arrValid=[];
	var ctype=['E','I','T'];
	var typeArr=['section0','section1','section2'];
		var categorytype=[resourceJSON.lblPortfolio,resourceJSON.lblPreScreen,resourceJSON.lblEPIGroup,resourceJSON.lblCustomQuestions,resourceJSON.lblOnBoarding,resourceJSON.lblVirtualVideointerview,resourceJSON.lblOnlineAssessment,resourceJSON.lblEReference];
		var candidateType=[resourceJSON.lblExternal,resourceJSON.lblInternal,resourceJSON.lblInternalTransfer];
		//var i=6;
		 var arrCounter=0;
		 for(var j=1;j<=3;j+=1)
		 {
			 if($("#"+i+"_"+i+"_"+j).val()=='A' && $("#"+typeArr[j-1]).is(":checked"))
			 {
				 if($("#"+i+ctype[j-1]+ctype[j-1]+"id").val()==0)
				 {
					arrValid[arrCounter]=candidateType[j-1];
					arrCounter+=1;
					vflag=true;
				 }
				 
			 }
		 }
	if(arrValid.length==1)
		typeString=arrValid[0];
	else if(arrValid.length==2)
		typeString=arrValid[0]+" and "+arrValid[1];
	else if(arrValid.length==3)
		typeString=arrValid[0]+", "+arrValid[1]+" and "+arrValid[2];
	if(arrCounter>0)
	{
	$("#errordiv").append("&#149; "+categorytype[i-1]+" "+resourceJSON.msgisnotconfiguredforthe+" "+typeString+" "+resourceJSON.msgcandidate+"<br>");
	}
	return vflag;
}
function hideOnBoardingMaster(){
	var host = window.location.hostname;
	 if(!((host=="titan.teachermatch.org") || (host=="localhost"))){
		 $("#onBoardingMaster").hide();
	 }else{
		 $("#onBoardingMaster").show();
	 }
}

function getOnBOardinByDistrict(districtId)
{
	JobCategoryAjaxNew.getOnBoardingByDistrictId(districtId,{ 
		async: false,
		callback: function(data){
	    if(data!=null)
	    {
	    	$("#qqAvlbListForOnboard0").html(data);
	    	$("#qqAvlbListForOnboard1").html(data);
	    	$("#qqAvlbListForOnboard2").html(data);
	    }
	},
	});
}