var txtBgColor="#F5E7E1";
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);

var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
function getDistrictORSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtORSchoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictORSchoolArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictORSchoolArray(districtOrSchoolName){
	var searchArray = new Array();
	/*==== Gagan : District Auto Complete will show for each case ========*/
	ManageJobOrdersAjax.getFieldOfDistrictList(districtOrSchoolName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	
	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("districtOrSchooHiddenlId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			//alert(" Hi ");
			$('#schoolName').attr('readonly', true);
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
			document.getElementById(hiddenId).value="";
			
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}


function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	//alert(schoolName);
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtOrSchooHiddenlId").value;
	//alert(districtId);
	OfferReadyByStafferAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {
	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function checkForCGInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8 || charCode==46)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

// for paging and sorting
function getPaging(pageno)
{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		showCertGrid();
}


function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	
	  if(pageno!=''){
			page=pageno;
			//alert("page :: "+page);
		}else{
			page=1;
			//alert("default");
		}
		sortOrderStr=sortOrder;
		//alert("sortOrderStr :: "+sortOrderStr);
		sortOrderType=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			//alert("AA")
			noOfRows = document.getElementById("pageSize").value;
		}else{
		//	alert("BB");
			noOfRows=50;
		}
		showCertGrid();
		
	
}
function activecityType(){
	document.getElementById("certType").value='';
}
function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGridEEC").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#cg'+j).tooltip();
		$('#cgn'+j).tooltip();
	}
}

function getDivNo(divNo)
{
	$('#divNo').val(divNo);
}
function downloadResume()
{
	PFExperiences.downloadResume(
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
					if (data.indexOf(".doc") !=-1) {
					   // $('#modalDownloadsTranscript').modal('hide');
					    document.getElementById('ifrmResume').src = ""+data+"";
					}
					else
					{
						//window.open(data);
						//openWindow(data,'Support',650,490);
						document.getElementById("hrefResume").href = data; 
						return false;
					}
				}
			});
}
//CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
var finalCallStatus = 1;
function showCertGrid()
{
	PFCertifications.getPFCertificationsGrid(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#divDataGrid').html(data);
		applyScrollOnTbl();
	}});
}
function setDefColortoErrorMsg()
{
	$('#certificationtypeMaster').css("background-color","");
	$('#stateMaster').css("background-color","");
	$('#yearReceived').css("background-color","");
	$('#certType').css("background-color","");
	$('#certName').css("background-color","");
	$('#certUrl').css("background-color","");
	$('#certificationStatusMaster').css("background-color","");
	$('#yearexpires').css("background-color","");
}
function showForm()
{
	document.getElementById("frmCertificate").reset();
	document.getElementById("certId").value="";
	document.getElementById("divMainForm").style.display="block";
	document.getElementById("removeCert").style.display="none";
	document.getElementById("stateMaster").focus();
	
	$('#errordiv').empty();
	setDefColortoErrorMsg();
	return false;
}
function hideForm()
{

	document.getElementById("frmCertificate").reset();
	document.getElementById("certId").value="";	
	document.getElementById("divMainForm").style.display="none";
	$('#errordiv').empty();
	setDefColortoErrorMsg();
	return false;
}
function clearForm()
{
	$("#praxisArea").hide();
	$("#reading").html("");
	$("#writing").html("");
	$("#maths").html("");
}
function clearCertType()
{
	$("#certificateTypeMaster").val("");
	$("#certType").val("");
}
function fieldsDisable(value){
	if(value==5){
		$('#errordiv').empty();
		$( "#divMainForm" ).find( "span" ).css( "color", "white" );
		setDefColortoErrorMsg();
		document.getElementById("certificationtypeMaster").value=0;
		document.getElementById("stateMaster").value="";
		document.getElementById("yearReceived").value="";
		document.getElementById("yearexpires").value="";
		document.getElementById("certType").value="";
		document.getElementById("doenumber").value="";
		document.getElementById("certUrl").value="";
		document.getElementById("pathOfCertificationFile").value="";
		document.getElementById("pkOffered").checked=false;
		document.getElementById("kgOffered").checked=false;
		document.getElementById("g01Offered").checked=false;
		document.getElementById("g02Offered").checked=false;
		document.getElementById("g03Offered").checked=false;
		document.getElementById("g04Offered").checked=false;
		document.getElementById("g05Offered").checked=false;
		document.getElementById("g06Offered").checked=false;
		document.getElementById("g07Offered").checked=false;
		document.getElementById("g08Offered").checked=false;
		document.getElementById("g09Offered").checked=false;
		document.getElementById("g10Offered").checked=false;
		document.getElementById("g11Offered").checked=false;
		document.getElementById("g12Offered").checked=false;
		document.getElementById("clearLink").style.display="none";
		getPraxis();
		
		document.getElementById("certificationtypeMaster").disabled=true;
		document.getElementById("stateMaster").disabled=true;		
		document.getElementById("yearReceived").disabled=true;
		document.getElementById("yearexpires").disabled=true;
		document.getElementById("certType").disabled=true;
		document.getElementById("doenumber").disabled=true;
		document.getElementById("certUrl").disabled=true;
		document.getElementById("pathOfCertificationFile").disabled=true;
		document.getElementById("pkOffered").disabled=true;
		document.getElementById("kgOffered").disabled=true;
		document.getElementById("g01Offered").disabled=true;
		document.getElementById("g02Offered").disabled=true;
		document.getElementById("g03Offered").disabled=true;
		document.getElementById("g04Offered").disabled=true;
		document.getElementById("g05Offered").disabled=true;
		document.getElementById("g06Offered").disabled=true;
		document.getElementById("g07Offered").disabled=true;
		document.getElementById("g08Offered").disabled=true;
		document.getElementById("g09Offered").disabled=true;
		document.getElementById("g10Offered").disabled=true;
		document.getElementById("g11Offered").disabled=true;
		document.getElementById("g12Offered").disabled=true;
		document.getElementById("clearLink").style.display="none";
		
	}else{
		$( "#divMainForm" ).find( "span" ).css( "color", "red" );
		document.getElementById("clearLink").style.display="block";
		document.getElementById("certificationtypeMaster").disabled=false;
		document.getElementById("stateMaster").disabled=false;		
		document.getElementById("yearReceived").disabled=false;
		document.getElementById("yearexpires").disabled=false;
		document.getElementById("certType").disabled=false;
		document.getElementById("doenumber").disabled=false;
		document.getElementById("certUrl").disabled=false;
		document.getElementById("pathOfCertificationFile").disabled=false;
		document.getElementById("pkOffered").disabled=false;
		document.getElementById("kgOffered").disabled=false;
		document.getElementById("g01Offered").disabled=false;
		document.getElementById("g02Offered").disabled=false;
		document.getElementById("g03Offered").disabled=false;
		document.getElementById("g04Offered").disabled=false;
		document.getElementById("g05Offered").disabled=false;
		document.getElementById("g06Offered").disabled=false;
		document.getElementById("g07Offered").disabled=false;
		document.getElementById("g08Offered").disabled=false;
		document.getElementById("g09Offered").disabled=false;
		document.getElementById("g10Offered").disabled=false;
		document.getElementById("g11Offered").disabled=false;
		document.getElementById("g12Offered").disabled=false;
	}
}

function getPraxis()
{
	
	var statemaster = {stateId:dwr.util.getValue("stateMaster")};
	PFCertifications.getPraxis(statemaster,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!=null)
			{	
				$("#praxisArea").show();
				$("#reading").html(data.praxisIReading);
				$("#writing").html(data.praxisIWriting);
				$("#maths").html(data.praxisIMathematics);
			}else
			{
				$("#praxisArea").hide();
				$("#reading").html("");
				$("#writing").html("");
				$("#maths").html("");
			}
	}});
}

function insertOrUpdate(callStatus)
{
	finalCallStatus = callStatus;
	var certId = document.getElementById("certId");

	var stateMaster = document.getElementById("stateMaster");
	var yearReceived = document.getElementById("yearReceived");
	var certificationStatusMaster = document.getElementById("certificationStatusMaster").value;
	var certType = document.getElementById("certType");
	var certUrl = trim(document.getElementById("certUrl").value);
	
	var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
	var certificationTypeMaster = document.getElementById("certificationtypeMaster").value;
	var yearExpires = document.getElementById("yearexpires").value;
	var doenumber = trim(document.getElementById("doenumber").value);
	var certText = trim(document.getElementById("certText").value);
	
	var pathOfCertificationFile = document.getElementById("pathOfCertificationFile");
	var cnt=0;
	var focs=0;	
	
	var fileName = pathOfCertificationFile.value;
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize=0;		
	if ($.browser.msie==true){	
	    fileSize = 0;	   
	}else{		
		if(pathOfCertificationFile.files[0]!=undefined)
		fileSize = pathOfCertificationFile.files[0].size;
	}	
	if(doenumber=="")
	{
		doenumber=0;
	}
	if(yearExpires=="")
	{
		yearExpires=0;
	}
	$('#errordiv').empty();
	setDefColortoErrorMsg();
	
	if(certificationStatusMaster=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgcertificate+"<br>");
			if(focs==0)
				$('#certificationStatusMaster').focus();
			
		$('#certificationStatusMaster').css("background-color",txtBgColor);
			cnt++;focs++;	
	}
	
	if(certificationStatusMaster!=5){
		var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
		if(trim(certificationtypeMasterObj.certificationTypeMasterId)==0)
		{
			$('#errordiv').append("&#149;"+resourceJSON.msgceritificationtype+"<br>");
			if(focs==0)
				$('#certificationtypeMaster').focus();
			
			$('#certificationtypeMaster').css("background-color",txtBgColor);
				cnt++;focs++;	
		}
		
		
		
		if(trim(stateMaster.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgState+"<br>");
			if(focs==0)
				$('#stateMaster').focus();
			
			$('#stateMaster').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(certificationStatusMaster!=3)
		{
			if(trim(yearReceived.value)=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgyearreciverd+"<br>");
				if(focs==0)
					$('#yearReceived').focus();
				
				$('#yearReceived').css("background-color",txtBgColor);
				cnt++;focs++;
			}
		}
		
		
		var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
		if(trim(certTypeObj.certTypeId)=="" && trim(certType.value)!="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgnotexistselecstate+"<br>");
			if(focs==0)
				$('#certType').focus();
			
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(certType.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgentercertificatename+"<br>");
			if(focs==0)
				$('#certType').focus();
			
			$('#certType').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(certificationStatusMaster==1 || certificationStatusMaster==2)
		{
			var pathOfCertification = document.getElementById("pathOfCertification").value;
			if(certUrl=="" && pathOfCertification=="" && pathOfCertificationFile.value=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgentervalidlicence+"<br>");
					if(focs==0)
					$('#certUrl').focus();
					$('#certUrl').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
		if(ext!=""){
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgaacptfileformate+"<br>");
					if(focs==0)
						$('#pathOfCertification').focus();
					
					$('#pathOfCertification').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}	
			else if(fileSize>=10485760)
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
					if(focs==0)
						$('#pathOfCertification').focus();
					
					$('#pathOfCertification').css("background-color",txtBgColor);
					cnt++;focs++;	
					return false;
			}
		}
	}
	if(cnt!=0)		
	{
		$('#errordiv').show();
		return false;
	}
	else
	{
		var stateObj = {stateId:dwr.util.getValue("stateMaster")};
		var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
		var certificate =null;
		
		if(dwr.util.getValue("certificationStatusMaster")==""){
			certificate = {certId:null, stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:null,certUrl:null,
					certificateTypeMaster:certTypeObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
					g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
					g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
					writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
					yearExpired:trim(yearexpires.value),doeNumber:null,certText:null};
		}else{
			certificate = {certId:null,certificationStatusMaster:certificationStatusMasterObj,certUrl:null,
					stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:trim(certType.value),certificateTypeMaster:certTypeObj,
					pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,
					g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
					g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
					writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:certificationtypeMasterObj,
					yearExpired:yearExpires,doeNumber:doenumber,certText:certText};
		}
		dwr.engine.beginBatch();
		dwr.util.getValues(certificate);
		
		var certiFile = document.getElementById("pathOfCertification").value;
		if(certiFile!=""){
			certificate.pathOfCertification=certiFile;
		}
		if(fileName!="")
		{	
			PFCertifications.findDuplicateCertificate(certificate,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
					{
						if(data)
						{
							$('#errordiv').append("&#149; "+resourceJSON.msgduplicateceritificate+"<br>");
							$('#certType').focus();
							$('#certType').css("background-color",txtBgColor);
							$('#errordiv').show();
							return false;
						}else{
							$('#loadingDiv').show();
							chkForALLDiv=1;
							document.getElementById("frmCertificate").submit();
						}
					}
				});
				dwr.engine.endBatch();
				return true;
		}else{
			if(certificationStatusMaster==5){
				certificate = {certId:certId.value, stateMaster:null,yearReceived:null,certType:null,certUrl:null,
						certificationStatusMaster:certificationStatusMasterObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
						g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
						g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
						writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
						yearExpired:null,doeNumber:null,certText:null};
			}
			PFCertifications.saveOrUpdate(certificate,{
				async: true,
				errorHandler:handleError,
				callback:function(data)
					{
						if(data==1)
						{
							$('#errordiv').append("&#149; "+resourceJSON.msgduplicateceritificate+"<br>");
							$('#certType').focus();
							$('#certType').css("background-color",txtBgColor);
							$('#errordiv').show();
							return false;
						}else if(data==0){
							errorHandler:handleError
						}
						$('#loadingDiv').hide();
						hideForm();
						showCertGrid();
					}
				});
				dwr.engine.endBatch();
				return true;
			}
		}	
		
		//sendToOtherPager();
		return true;
	//}	
}
function saveCertification(fileName){
	var certId = document.getElementById("certId");
	var certText = trim(document.getElementById("certText").value);
	var stateMaster = document.getElementById("stateMaster");
	var yearReceived = document.getElementById("yearReceived");
	
	var certType = document.getElementById("certType");
	var certUrl = trim(document.getElementById("certUrl").value);
	
	var stateObj = {stateId:dwr.util.getValue("stateMaster")};
	var certificationStatusMasterObj= {certificationStatusId:dwr.util.getValue("certificationStatusMaster")};
	var certTypeObj = {certTypeId:dwr.util.getValue("certificateTypeMaster")};
	var certificate =null;
	
	var certificationtypeMasterObj = {certificationTypeMasterId:dwr.util.getValue("certificationtypeMaster")};
	var yearExpires = document.getElementById("yearexpires").value;
	var doenumber = trim(document.getElementById("doenumber").value);
	
	if(doenumber=="")
	{
		doenumber=0;
	}
	if(yearExpires=="")
	{
		yearExpires=0;
	}
	
	if(dwr.util.getValue("certificationStatusMaster")==""){
		certificate = {certId:null, stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:null,certUrl:null,
				certificateTypeMaster:certTypeObj,pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,
				g03Offered:null,g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
				g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
				writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:null,
				yearExpired:trim(yearexpires.value),doeNumber:null,certText:null};
	}else{
		certificate = {certId:null,certificationStatusMaster:certificationStatusMasterObj,certUrl:null,
				stateMaster:stateObj,yearReceived:trim(yearReceived.value),certType:trim(certType.value),certificateTypeMaster:certTypeObj,
				pkOffered:null,kgOffered:null,g01Offered:null,g02Offered:null,g03Offered:null,
				g04Offered:null,g05Offered:null,g06Offered:null,g07Offered:null,g08Offered:null,
				g09Offered:null,g10Offered:null,g11Offered:null,g12Offered:null,readingQualifyingScore:null,
				writingQualifyingScore:null,mathematicsQualifyingScore:null,certificationTypeMaster:certificationtypeMasterObj,
				yearExpired:yearExpires,doeNumber:doenumber,certText:certText};
	}
	
	dwr.engine.beginBatch();
	dwr.util.getValues(certificate);

	if(fileName!=""){
		certificate.pathOfCertification=fileName;
	}
	//alert("fileName :: "+fileName+"\n>>>> "+certificate.pathOfCertification);
	PFCertifications.saveOrUpdate(certificate,{
		async: false,
		errorHandler:handleError,
		callback:function(data)
			{
				if(data==1)
				{
					$('#errordiv').append("&#149; "+resourceJSON.msgduplicateceritificate+"<br>");
					$('#certType').focus();
					$('#certType').css("background-color",txtBgColor);
					$('#errordiv').show();
					return false;
				}else if(data==0){
					errorHandler:handleError
				}
				$('#loadingDiv').hide();
				hideForm();
				showCertGrid();
				/*if(finalCallStatus==1)
				{
					window.location.href="experiences.do";
				}*/
			}
		});
	dwr.engine.endBatch();
	//sendToOtherPager();
}

function downloadCertification(certId, linkId)
{		
		PFCertifications.downloadCertification(certId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmCert").src=data;
			}else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function showEditForm(id)
{
	document.getElementById('certText').value="";
	PFCertifications.showEditForm(id,{
		async: true,
		callback: function(data)
		{	
			showForm();
			try{
				dwr.util.setValues(data);
				
				document.getElementById("certId").value=data.certId;
				/*alert(""+data.stateMaster.stateId);*/
				if(data.stateMaster!=null && data.stateMaster.stateId!=null){
					document.getElementById("stateMaster").value=data.stateMaster.stateId;
				}
				
				document.getElementById('certText').value=data.certText;
				if(data.doeNumber==0)
				{
					document.getElementById("doenumber").value="";
				}	
				else
				{
					document.getElementById("doenumber").value=data.doeNumber;
				}
					
				if(data.certificationStatusMaster!=null){
					document.getElementById("certificationStatusMaster").value=data.certificationStatusMaster.certificationStatusId;
					if(data.certificationStatusMaster.certificationStatusId==3){
						showHideFileUpload(data.certificationStatusMaster.certificationStatusId);
					}else{
						showHideFileUpload(data.certificationStatusMaster.certificationStatusId);
					}
						
				}	
				else{
					document.getElementById("certificationStatusMaster").value="";
				}	
				
				if(data.certificationTypeMaster!=null){
					document.getElementById("certificationtypeMaster").value=data.certificationTypeMaster.certificationTypeMasterId;
					document.getElementById("certificationtypeMaster").text=data.certificationTypeMaster.certificationType;
				}
				
				if(data.yearReceived!=null && data.yearReceived!="")
					document.getElementById("yearReceived").value=data.yearReceived;
				else
					document.getElementById("yearReceived").text="Select";
					
				if(data.yearExpired==null || data.yearExpired==0)
				{
					document.getElementById("yearexpires").text="Select Year";
				}
				else
				{
					document.getElementById("yearexpires").value=data.yearExpired;
				}	
				document.getElementById("certUrl").value=data.certUrl;
				
				
				if(data.certificateTypeMaster!=null){
						document.getElementById("certType").value=data.certificateTypeMaster.certType;
						document.getElementById("certificateTypeMaster").value=data.certificateTypeMaster.certTypeId;
				}else{
						document.getElementById("certType").value="";
						document.getElementById("certificateTypeMaster").value="";
				}
				
				if(data.pathOfCertification!=null && data.pathOfCertification!="")
				{	
					document.getElementById("removeCert").style.display="block";
					document.getElementById("divCertName").style.display="block";
					document.getElementById("pathOfCertification").value=data.pathOfCertification;
					
					document.getElementById("divCertName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadCertification('"+data.certId+"','hrefEditRef');" +
					"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
					"return false;\">"+data.pathOfCertification+"</a>";
				}else{
					document.getElementById("divCertName").style.display="none";
					document.getElementById("removeCert").style.display="none";
					document.getElementById("pathOfCertificationFile").value="";
				}
				fieldsDisable(data.certificationStatusMaster.certificationStatusId);
				getPraxis();
				return false;
			}catch(e){
				alert(e);
			}
		},errorHandler:handleError
	});
	return false;
}

function removeCertificationFile()
{
	var certId = document.getElementById("certId").value;
	if(window.confirm(resourceJSON.msgdeletecerticicate))
	{
		PFCertifications.removeCertification(certId,{ 
			async: false,
			callback: function(data){	
				document.getElementById("removeCert").style.display="none";
				showCertGrid();
				hideForm();
			},errorHandler:handleError
		});
	}
}
function clearCertification(){	
			
  $('#pathOfCertificationFile').replaceWith("<input id='pathOfCertificationFile' name='pathOfCertificationFile' type='file' width='20px;' />");
  document.getElementById("pathOfCertificationFile").value="";
  document.getElementById("pathOfCertificationFile").reset();
  $('#pathOfCertificationFile').css("background-color","");
}
function showHideFileUpload(option)
{
	if(option==3)
	{
		$("#fileuploadDiv").hide();
	}else{
		$("#fileuploadDiv").show();
	}
}


function deleteRecord_Certificate(id)
{	
	document.getElementById("certificateTypeID").value=id;
	$('#deleteCertRec').modal('show');
}


function deleteCertificateRecord(){
	var id=document.getElementById("certificateTypeID").value;
	
	PFCertifications.removeCertification(id,{ 
		async: false,
		callback: function(data){
		},errorHandler:handleError
	});
	
	PFCertifications.deleteRecord(id,{ 
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(data){
			$('#deleteCertRec').modal('hide');
			showCertGrid();
			hideForm();
		}
		return false;
	}});
}



function saveAndContinueCertificationsAndResume()
{
	var fileName=document.getElementById("resume").value;
	var hdnResume = document.getElementById("hdnResume").value;
	var cnts=0;
	var focss=0;	
	$('#resume').css("background-color","");
	
	$('#errordiv1').empty();
	if(fileName=="")
	{
		if(hdnResume=="")
		{
			$('#errordiv1').append("&#149; "+resourceJSON.msgresumr+"<br>");
			if(focss==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnts++;focss++;	
		}else{
			window.location.href="userdashboard.do";
		}
	}
	else
	{
		var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();

		var fileSize=0;		
		if ($.browser.msie==true)
		{	
			fileSize = 0;	   
		}
		else
		{	
			if(document.getElementById("resume").files[0]!=undefined)
				fileSize = document.getElementById("resume").files[0].size;
		}

		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordiv1').append("&#149; "+resourceJSON.msgacceptableresume+"<br>");
			if(focss==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnts++;focss++;	

		}
		else if(fileSize>=10485760)
		{		
			$('#errordiv1').append("&#149; "+resourceJSON.msgfilesize+"<br>");
			if(focss==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnts++;focss++;	

		}
		else
		{
			$('#loadingDiv').show();
			document.getElementById("frmExpResumeUpload").submit();
		}
	}
	if(cnts!=0)		
	{
		$('#errordiv1').show();
		var opened=true;
		$(".accordion-body").each(function(){
	        if($(this).hasClass("in")) {
	        	if($(this).attr('id')=="collapseTwo")
	        	opened=false;
	        }
	    });
		if(opened)
			$("#resumeDiv").click();
		
		return false;
	}
	//return true;
}


function saveResume(fileName)
{
	MyDashboardAjax.addOrEditResume(fileName, { 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		  {
			$('#loadingDiv').hide();
			document.getElementById("frmExpResumeUpload").reset();
			document.getElementById("divResumeTxt").innerHTML="<label id='lblResume'>"+resourceJSON.MsgRecentResumeFile+"<a href='javascript:void(0)' id='hrefResume' onclick='downloadResume();if(this.href!='javascript:void(0)')window.open(this.href, mywin,left=200,top=50,width=700,height=600,toolbar=1,resizable=0); return false;>"+fileName+"</a></label>";
			document.getElementById("hdnResume").value=fileName;
			window.location.href="userdashboard.do";
		  }
	});
}

function canclePortfolio(){
	window.location.href="userdashboard.do";
}
function saveDoumentSatisfactoryAnswer()
{
	$('#errordiv0').empty();
	var cnts0,focss=0;
	document.getElementById("isdocumentonfileId").value='';
	var isdocumentonfile="";
    var isdocumentonfile_Id = document.getElementsByName('isdocumentonfile');
	
	for(var i = 0; i < isdocumentonfile_Id.length; i++){
	    if(isdocumentonfile_Id[i].checked){
	    	isdocumentonfile = isdocumentonfile_Id[i].value;
	    }
	}
	document.getElementById("isdocumentonfileId").value=isdocumentonfile;
	if (isdocumentonfile == null || isdocumentonfile == '') {
		$('#errordiv0').append("&#149; "+resourceJSON.PlzSlctAnyOptionBelow+"<br>");
		if(focss==0){
	       $('#isdocumentonfile').focus();
		}

		$('#isdocumentonfile').css("background-color",txtBgColor);
		cnts++;focss++;	
		return false;
	}
	else{
		MyDashboardAjax.saveDocumentonStatus(isdocumentonfile, { 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			  {
				document.getElementById("isdocumentonfileId").value=data;
				
			  }
		});
	}
 	saveAndContinueCertificationsAndResume();
}


function checkdocumentonfile()
{
   var isdocumentonfile=document.getElementById("isdocumentonfileId").value;
	if(isdocumentonfile!=null && isdocumentonfile!=''){
		if(isdocumentonfile=='true'){
		   $("#isdocumentonfile1").prop("checked", true)
		}
		else{
		   $("#isdocumentonfile0").prop("checked", true)
		}
	}
	
	
}