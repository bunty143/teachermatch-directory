/* @Author: Gagan 
 * @Discription: view of edit Objective js.
*/

var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	DisplayObjective();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	DisplayObjective();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*======= Save Domian on Press Enter Key ========= */
function chkForEnterSaveObjective(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveObjective();
	}	
}
/*========  Add New Objective===============*/
function addObjective()
{
	$('#errordiv').empty();
	$('#domainMaster').css("background-color", "");
	$('#competencyMaster').css("background-color", "");
	$('#objectiveName').css("background-color", "");
	$('#objectiveUId').css("background-color", "");
	$('#objectiveUId').val('');
	$('#objectiveUId').prop('disabled', false);
	dwr.util.setValues({ domainMaster:null,objectiveId:null,objectiveName:null,objectiveUId:null});
	document.getElementById("divObjectiveForm").style.display	=	"block";
	$('#domainMaster').focus();
	$('#errordiv').hide();
	var cmOptions	=	"<select class='form-control' name='competencyMaster' id='competencyMaster'><option value='0'>"+resourceJSON.MsgSelectCompetency+"</option></select>";
	//document.getElementById("competencyMaster").innerHTML		=	cmOptions;
	$('#competencyListByDomainId').html(cmOptions);
	document.getElementById("divDone").style.display			=	"block";
	document.getElementById("divManage").style.display			=	"none";
	return false;
}
/*========  DisplayObjective ===============*/
function DisplayObjective()
{
	ObjectiveAjax.DisplayObjectiveRecords(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			//document.getElementById("objectiveTable").innerHTML		=	data;
			$('#objectiveGrid').html(data);
			applyScrollOnTbl();
		},
		errorHandler:handleError  
	});
}

/*========  Clear Objective Fields ===============*/
function clearObjective()
{
	document.getElementById("divObjectiveForm").style.display	=	"none";
	document.getElementById("divDone").style.display			=	"none";
	document.getElementById("divManage").style.display			=	"none";
	document.getElementById("objectiveName").value				=	"";
	document.getElementsByName("objectiveStatus")[0].checked	=	true;
	$('#objectiveUId').val('');
	$('#objectiveUId').prop('disabled', false);
	$('#objectiveUId').css("background-color", "");
}
/*========  Save Objective  ===============*/
function saveObjective()
{
	$('#errordiv').empty();
	$('#domainMaster').css("background-color", "");
	$('#competencyMaster').css("background-color", "");
	$('#objectiveName').css("background-color", "");
	$('#objectiveUId').css("background-color", "");
	/*==== Geting Dropdowns selected option id from objective.jsp in domainMaster and competencyMaster===*/
	var domainMaster		=	trim(document.getElementById("domainMaster").value);
	var competencyMaster	=	trim(document.getElementById("competencyMaster").value);
	var objectiveId			=	trim(document.getElementById("objectiveId").value);
	var objectiveName		=	trim(document.getElementById("objectiveName").value);
	var objective			=	document.getElementsByName("objectiveStatus");
	var objectiveUId		=	trim(document.getElementById("objectiveUId").value);
	var counter				=	0;
	var focusCount			=	0;
	
	for(i=0;i<objective.length;i++)
	{
		if(objective[i].checked	==	true)
		{
			objectiveStatus		=	objective[i].value;
			break;
		}
	}
	if (domainMaster	==	0)
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgDomainName+"<br>");
		if(focusCount	==	0)
			$('#domainMaster').focus();
		$('#domainMaster').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (competencyMaster	==	0)
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlsSlctCompetencyName+"<br>");
		if(focusCount	==	0)
			$('#competencyMaster').focus();
		$('#competencyMaster').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (objectiveName=="")
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrObjectiveName+"<br>");
		if(focusCount	==	0)
			$('#objectiveName').focus();
		$('#objectiveName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}
	if (objectiveUId=="")
	{
		$('#errordiv').append('&#149; '+resourceJSON.msgEnterObjectiveUId);
		$('#objectiveUId').css("background-color", "#F5E7E1");
		if(focusCount	==	0)
			document.getElementById("objectiveUId").focus();
		counter++;
		focusCount++;
	}
	if(counter	==	0)
	{
		dwr.engine.beginBatch();			
		ObjectiveAjax.saveObjective(domainMaster,competencyMaster,objectiveId,objectiveName,objectiveStatus,objectiveUId, { 
			async: true,
			callback: function(data)
			{
				if(data==3)
				{
					document.getElementById('errordiv').innerHTML	=	'&#149; '+resourceJSON.PlzEtrUniqueObjectiveName;
					$('#errordiv').show();
					$('#objectiveName').css("background-color", "#F5E7E1");
					$('#objectiveName').focus();
				}else if(data==4){
					document.getElementById('errordiv').innerHTML	=	'&#149; '+resourceJSON.msgUniqueObjectiveUId;
					$('#errordiv').show();
					$('#objectiveUId').css("background-color", "#F5E7E1");
					$('#objectiveUId').focus();
				}else{
					DisplayObjective();
					clearObjective();
				}
			},
			errorHandler:handleError 
			});
		dwr.engine.endBatch();
		return true;
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}
/*========  activateDeactivateObjective ===============*/
function activateDeactivateObjective(objectiveId,status)
{
	ObjectiveAjax.activateDeactivateObjective(objectiveId,status, { 
		async: true,
		callback: function(data)
		{
			DisplayObjective();
			clearObjective();
		},
		errorHandler:handleError 
	});
				
}
/*========  Edit Objective ===============*/
function editObjective(objectiveId)
{
	$('#errordiv').empty();
	$('#domainMaster').css("background-color", "");
	$('#competencyMaster').css("background-color", "");
	$('#objectiveName').css("background-color", "");
	$('#objectiveUId').css("background-color", "");
	document.getElementById("divDone").style.display			=	"none";
	document.getElementById("divObjectiveForm").style.display	=	"block";
	var objective	=	document.getElementsByName("objectiveStatus");
	
	$('#domainMaster').focus();
	/* ======= async: false is used for execute statement line by line not Random ============*/
		ObjectiveAjax.getObjectiveById(objectiveId, {async: false, callback: function(data)
		{
			//alert(data.competencyMaster.domainMaster.domainId);
			dwr.util.setValues(data);
			var optsDomain	= document.getElementById('domainMaster').options;
			for(var i = 0, j = optsDomain.length; i < j; i++)
			{
				  if(data.competencyMaster.domainMaster.domainId	==	optsDomain[i].value)
				  {
					  optsDomain[i].selected	=	true;
				  }
			}
			getCompetenciesByDomainId(data.competencyMaster.domainMaster.domainId,data.competencyMaster.competencyId);
			if(data.status=='A')
				objective[0].checked	=	true;
			else
				objective[1].checked	=	true;
			$('#objectiveUId').val(data.objectiveUId);
			$('#objectiveUId').prop('disabled', true);
		},
		errorHandler:handleError	
		});
	
		document.getElementById("divManage").style.display="block";
		
	return false;
}

function getCompetenciesByDomainId(domainId,competencyId)
{
	ObjectiveAjax.getCompetenciesByDomainId(domainId,competencyId, {async: false, callback: function(data)
	{
		$('#competencyListByDomainId').html(data);
		return;
	},
	errorHandler:handleError
	});
}

function isAlphabetsKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
		return true;
	else
		return false;
}

/* @Author: Gagan 
 * @Discription: view of edit Objective js.
*/