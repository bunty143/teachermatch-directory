/* @Author: Rahul 
 * @Discription: managedocuments js .
 * 
 * modify for headquarter and branch
 * 27-06-2015 --  Ankit
 * 
 */	
var keyContactDivVal=2;
var keyContactIdVal=null;
/*=========  Paging and Sorting ==============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

var domainpage = 1;
var domainnoOfRows = 10;
var domainsortOrderStr="";
var domainsortOrderType="";

var txtBgColor="#F5E7E1";

var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType;//=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.tgetHonorsGridoLowerCase()));

function getPaging(pageno)
{
	if(pageno!='') {
		page=pageno;	
	} else {
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	displayDistAttachment();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp) {

	var gridNo	=	document.getElementById("gridNo").value;
		if(gridNo==3) {	
			if(pageno!='') {
				domainpage=pageno;	
			} else {
				domainpage=1;
			}
			domainsortOrderStr	=	sortOrder;
			domainsortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize3")!=null){
				domainnoOfRows = document.getElementById("pageSize3").value;
			}else{
				domainnoOfRows=10;
			}
			 displayDistAttachment();
		}else if(gridNo==1){
			if(pageno!=''){
				page=pageno;	
			} else {
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
			 displayDistAttachment();
		} else {
			if(pageno!=''){
				page=pageno;	
			}else{
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null || document.getElementById("pageSize")!=""){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
			 displayDistAttachment();
		}
}
function getSortSchoolGrid() {
	$('#gridNo').val("1");
}

function getSortNotesGrid() {
	$('#gridNo').val("2")
}

function getSortDomainGrid() {
	$('#gridNo').val("3")
}

function clearDocFile() {
	
	document.getElementById("distAttachmentDocument").value=null;	

}

function addNewDoc() {
	clearDocFile();
	$('#districtName').val("");
	$('#schoolName').val("");
	$('#errornotediv').hide();
	$("#update").hide();
	$("#save").show();
	$('#distAttachmentDiv').show();
	$("#removeTagsIcon").hide();
	$('#districtName').css("background-color", "");
	if($('#entityID').val()==1){
		$('#districtName').focus();
		document.getElementById('districtId').value="";
		document.getElementById('schoolId').value="";
	}
	
	if($('#entityID').val()==2){
		$('#districtName').focus();
		document.getElementById('schoolId').value="";
	}
	
	if($('#entityID').val()==5){
		$("#branchDiv").show();
		$('#branchName').val("");
		$('#branchName').focus();
		document.getElementById('bnchId').value="";
		document.getElementById('districtId').value="";
		document.getElementById('schoolId').value="";
	}
	if($('#entityID').val()==6){
		document.getElementById('districtId').value="";
		document.getElementById('schoolId').value="";
	}	
	if(resourceJSON.kellyupdatecodelive=='yes' || ($('#entityID').val()!=5 && $('#entityID').val()!=6))
	{		
		$("#documentDiv").show();
		$("#docName").val("");
	}	
}

function cancelDocDiv() {
	
	$('#distAttachmentDiv').hide();
	clearDocFile();
	$('#districtName').val("");
	$('#schoolName').val("");
	$('#errornotediv').hide();
$('#tags').css("background-color", "");
	$('#districtName').css("background-color", "");
}

function cancelEditDiv() {
	$('#distAttachmentDiv').hide();
		
	$('#distAttachmentDocument')
	.replaceWith(
			"<input id='distAttachmentEditDocument' type='file' />");	 
document.getElementById("distAttachmentEditDocument").value=null;
document.getElementById("distAttachmentEditDocument").reset();
$('#distAttachmentEditDocument').css("background-color","");	

}

/********************************** school auto completer ***********************************/
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray 	= new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;

function getSchoolMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type) {
	
	var districtId	=	document.getElementById("districtId").value;
	if(districtId=="")
	{
		document.getElementById('schoolName').readOnly=true;	
	}else
	{
		document.getElementById('schoolName').readOnly=false;
	}
	
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		//fetchSchoolData(txtSearch,searchArray,txtId,txtdivid);
		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			if(document.getElementById(txtId).value!="") {
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i].toUpperCase() + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
				}
			} else {
		}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}

			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0)) {
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="") {
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		} else {
	}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}

		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0)) {
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;

	DistrictAjax.getFieldOfSchoolList(districtId,schoolName,
	{
		
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].schoolName;
			showDataArray[i]=data[i].schoolName;
			hiddenDataArray[i]=data[i].schoolId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


function hideSchoolMasterDiv(dis,hiddenId,divId) {
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
		}
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}

	if(document.getElementById(divId)) {	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid) {
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {
	for(var i=0;i<length;i++) {
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s) {
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId) {	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}

function fireMouseOverEvent(event) {
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


function displayDistAttachment() {
	dwr.util.setValues({note:null});
	$("#loadingDiv").show();
	var schoolId				=	document.getElementById("schoolHiddenId").value;
	var districtId				=	document.getElementById("districtHiddenId").value;
	var docname                 =   document.getElementById("docname").value;
	var headQuarterId = document.getElementById("headQuarterId").value;
	var branchId = document.getElementById("branchHiddenId").value;
	//alert(" schoolId :: "+schoolId+" districtId :: "+districtId+" docname :: "+docname+" headQuarterId :: "+headQuarterId+" branchId :: "+branchId);
	DistrictAjax.displayDistAttachmentGrid(headQuarterId,branchId,districtId,schoolId,docname,noOfRows,page,sortOrderStr,sortOrderType,{
		async: true,
		callback: function(data)
		{
			$('#distAttachmentGrid').html(data);
			applyScrollOnTblDistAttachment();
			clearDocFile();
			$('#districtName').val("");
			$('#schoolName').val("");
			$('#distAttachmentDiv').hide();
		},
		errorHandler:handleError
	});
	$("#loadingDiv").hide();
}

function addDistAttachment() {
	$("#distAttachmentDiv").fadeIn();
	$("#distAttachmentEditDiv").hide();
	$('#errornotediv').empty();
}

function uploadDistFile(){
	$('#errornotediv').empty();
	$('#districtId').css("background-color", "");
	$('#distAttachmentDocument').css("background-color", "");
	var counter				=	0;
	var focusCount			=	0;
	var branchId ="";
	var headQuarterId  = "";	
	try{
		headQuarterId = document.getElementById("headQuarterId").value;
		branchId    =   document.getElementById("bnchId").value;		
	}catch(e){}
	var schoolId    =   document.getElementById("schoolId").value;
	var districtId	=	document.getElementById("districtId").value;
	var docName		=	document.getElementById("docName").value;
	var fileName 	= 	document.getElementById("distAttachmentDocument").value;
	var roleId 		= 	document.getElementById("hiddenRoleId").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;

if(roleId==1){
	var districtName    =   trim(document.getElementById("districtName").value);
}
var filenameSS 	= 	fileName.replace(/.*(\/|\\)/, '');
var baseFile 	= 	filenameSS.substr(0, filenameSS.lastIndexOf('.'));
var d		=	new Date();
var year	=	d.getFullYear();
var month	=	d.getMonth();
var date	=	d.getDate();
var hour	=	d.getHours();
var minute	=	d.getMinutes();
var second	=	d.getSeconds();
var dateString = year+"-"+month+"-"+date+"-"+hour+"-"+minute+"-"+second;
var dateString = dateString.replace('-', '');
var dateString = dateString .replace('-', '');
var dateString = dateString .replace('-', '');
var dateString = dateString .replace('-', '');
var dateString = dateString .replace('-', '');

var fullFilename = baseFile+dateString+"."+ext;
if ($.browser.msie==true) {
    fileSize = 0;	   
} else {		
	if(distAttachmentDocument.files[0]!=undefined)
	fileSize = distAttachmentDocument.files[0].size;
}
if(districtName=="" && roleId==1) {
	$('#errornotediv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
	$('#errornotediv').show();
	
	if(focusCount	==	0)
		$('#districtName').focus();
	$('#districtName').css("background-color", "#F5E7E1");
	counter++;
	focusCount++;
}

if(branchId=="" && roleId==5){
	$('#errornotediv').append("&#149; Please enter Branch.<br>");
	$('#errornotediv').show();
	
	if(focusCount	==	0)
		$('#branchName').focus();
	$('#districtName').css("background-color", "#F5E7E1");
	counter++;
	focusCount++;
}

if(fileName == null || fileName == ""){
	$('#errornotediv').append("&#149; "+resourceJSON.msgPleaseSelectFile+"<br>");
	$('#errornotediv').show();
	$('#distAttachmentDocument').css("background-color","#F5E7E1");
	if(focusCount==0)
		$('#distAttachmentDocument').focus();	
		
	counter++;
	focusCount++;	
	return false;
}

if(fileSize>=10485760) {
	$('#errornotediv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
		if(focusCount==0)
			$('#distAttachmentDocument').focus();
		
		$('#distAttachmentDocument').css("background-color","#F5E7E1");
		counter++;
		focusCount++;	
		return false;
}

if(counter==0) {
	if(fileName!=""){
		$("#loadingDiv").show();
		url="./distFileUploadServlet.do";
		var file_data = $("#distAttachmentDocument").prop("files")[0];
		var form_data = new FormData();
		if(headQuarterId!=""){
			form_data.append("hqId", headQuarterId)
		}
		if(branchId!="" || branchId!=0){
			form_data.append("branchId", branchId)
		}
		form_data.append("dateString", dateString)
		form_data.append("districtId", districtId)
		form_data.append("file", file_data)
		$.ajax({
	               url: url,
	               dataType: 'script',
	               cache: false,
	               dataType: 'text',
	               contentType: false,
	               processData: false,
	               data: form_data,
	               type: 'post',
	               success : function(response, data123, xhr) {
					
						if(response=="")
						{
							
							DistrictAjax.saveDistrictAttachment(headQuarterId,branchId,docName,districtId,fullFilename,schoolId,{
								async: true,
								callback: function(data)
								{
									
									if(roleId=="10"){
									document.getElementById("bnchId").value="";
									document.getElementById("branchHiddenId").value="";
									}
									$("#successModal").modal('show');
									$("#loadingDiv").hide();
								},
								errorHandler:handleError
							});
						}
						else
							fileContainsVirusDiv(response);
					}
				})
			}
		}
}

function deleteDistrictAttachment(districtattachmentId) {

	$('#note').css("background-color", "");
$('#errornotediv').empty();
if (confirm(resourceJSON.msgWouldLikeDeleteAttachment)) {
	$("#loadingDiv").show();
	DistrictAjax.deleteDistrictAttachment(districtattachmentId, {
		async: true,
		callback: function(data)
		{
			$('#distAttachmentGrid').html(data);
			applyScrollOnTblDistAttachment();
			displayDistAttachment();
			cancelDocDiv();
			$("#loadingDiv").hide();
		},
		errorHandler:handleError
	});
}
}

function editDistrictAttachment(attachmentId,entity) {
$('#note').css("background-color", "");

DistrictAjax.getAttachmentBYId(attachmentId,{
	async: true,
	callback: function(data)
	{
		var datas=data.split('::::');
		if(datas[5]=="0"){
			$('#districtId').val(datas[1]);
			$('#districtName').val(datas[2]);
			$('#schoolId').val(datas[3]);
			$('#schoolName').val(datas[4]);
			
		}
		if(datas[5]=="1"){
			$("#branchDiv").show();
			$("#bnchId").val("");
			$("#branchName").val("");			
		}
		if(datas[5]=="2"){
			$("#branchDiv").show();
			$("#bnchId").val(datas[1]);
			$("#branchName").val(datas[2]);			
		}
		$('#editFileName').val(datas[0]);
		$('#docID').val(attachmentId);
		$('#crtby').val(entity);			
	},
	errorHandler:handleError
});

$('#errornotediv').empty();
$("#distAttachmentDiv").show();
$("#update").show();
$("#updateschoolfile").hide();
$("#save").hide();

try {
	$('#crtby').val(entitytype);	
} catch (e) {
	// TODO: handle exception
}

}

function uploadEditDistFile() {
	$('#errornotediv').empty();
	try {
		
	
	var focus 		= 	0;
	var counter		= 	0;
	var branchId = "";
	var headQuarterId = "";
	try{
		headQuarterId = document.getElementById("headQuarterId").value;
		branchId =   document.getElementById("bnchId").value;
	}catch(e){}
	var districtId	=	document.getElementById("districtId").value;
	var schoolId	=	trim(document.getElementById("schoolId").value);
	var fileName 	= 	document.getElementById("distAttachmentDocument").value;
	var attachmentId=   document.getElementById("docID").value;
	var ext 		= 	fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize	=	0;
	var crtby       =document.getElementById("hiddenRoleId").value;
	var editFileName = document.getElementById("editFileName").value;
	var filenameSS 	= 	fileName.replace(/.*(\/|\\)/, '');
	var baseFile 	= 	filenameSS.substr(0, filenameSS.lastIndexOf('.'));

	var d		=	new Date();
	var year	=	d.getFullYear();
	var month	=	d.getMonth();
	var date	=	d.getDate();
	var hour	=	d.getHours();
	var minute	=	d.getMinutes();
	var second	=	d.getSeconds();
	var dateString = year+"-"+month+"-"+date+"-"+hour+"-"+minute+"-"+second;
	var dateString = dateString.replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var dateString = dateString .replace('-', '');
	var fullFilename = baseFile+dateString+"."+ext;

	if ($.browser.msie==true) {
	    fileSize = 0;	   
	} else {		
		if(distAttachmentDocument.files[0]!=undefined)
		fileSize = distAttachmentDocument.files[0].size;
	}


	
	
	

	
	var roleId 		= 	document.getElementById("hiddenRoleId").value;
	

if(roleId==1){
	
	var districtName    =   trim(document.getElementById("districtName").value);
	
if(districtName.length<1)
{

	$('#errornotediv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
	
	$('#errornotediv').show();
	
	$('#districtName').focus();
	$('#districtName').css("background-color", "#F5E7E1");
	
	counter++;
		
	return false;


}
}
	


if(fileName == null || fileName == ""){
	$('#errornotediv').append("&#149; "+resourceJSON.msgPleaseSelectFile+"<br>");
	$('#errornotediv').show();
	$('#distAttachmentDocument').css("background-color","#F5E7E1");

		$('#distAttachmentDocument').focus();	
		
	counter++;
	
	return false;
}



 if(fileSize>=10485760) {
		$('#errornotediv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focus==0)
				$('#distAttachmentDocument').focus();

			$('#distAttachmentDocument').css("background-color",txtBgColor);
			counter++;
			focus++;	
			return false;
	}
	
	if(counter==0){
		if(fileName!="" || editFileName!=""){
			$("#loadingDiv").show();
			var file_data = $("#distAttachmentDocument").prop("files")[0];
			var form_data = new FormData();
			if(headQuarterId!=""){
				form_data.append("hqId", headQuarterId)
			}
			if(branchId!="" || branchId!=0){
				form_data.append("branchId", branchId)
			}
			form_data.append("dateString", dateString)
			form_data.append("districtId", districtId)
			form_data.append("schoolId", schoolId)
			url="./distFileUploadServlet.do";
			form_data.append("file", file_data)
			
			if(editFileName!="" && fileName=="")
			{
				fullFilename = editFileName;
			}
			$.ajax({
				url: url,
				dataType: 'text',
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success : function(response, data123, xhr)
				{
					if(response=="")
					{
						DistrictAjax.editDistrictAttachment(headQuarterId,branchId,districtId,schoolId,attachmentId,fullFilename,{
							async: true,
							callback: function(data)
							{
								
								if(roleId=="10"){
									document.getElementById("bnchId").value="";
									document.getElementById("branchHiddenId").value="";
								}
								$('#distAttachmentGrid').html(data);
								$("#successEditModal").modal('show');
								$("#loadingDiv").hide();								
							},
							errorHandler:handleError
						});
						
					}
					else
						fileContainsVirusDiv(response);
				}
			})
		}
	}
	} catch (e) {
		// TODO: handle exception
	}
}

function vieDistrictFile(districtattachmentId) {
	document.getElementById("ifrmAttachment").src="";
DistrictAjax.vieDistrictFile(districtattachmentId,{
async: true,
errorHandler:handleError,
callback:function(data)
{
	if(deviceType)
	{
		if (data.indexOf(".doc")!=-1)
		{
			$("#docfileNotOpen").css({"z-index":"3000"});
	    	$('#docfileNotOpen').show();
		}
		else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			$("#exelfileNotOpen").css({"z-index":"3000"});
	    	$('#exelfileNotOpen').show();
		}
		else
		{
			document.getElementById(linkid).href = data;
		}
	}
	else if(deviceTypeAndroid)
	{
		if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{document.getElementById('ifrmAttachment').src = ""+data+"";
			 $('#distAttachmentDiv').modal('hide');
			 
		}
		else
		{
			document.getElementById(linkid).href = data;
		}
	}
	else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
	{
		 document.getElementById('ifrmAttachment').src = ""+data+"";
		 $('#distAttachmentShowDiv').modal('hide');
		 
	
	} else {	
			document.getElementById('ifrmAttachment').src = ""+data+"";
			$('#distAttachmentShowDiv').modal('show');
		
	}
}});

}

function upDateStatus(districtattachmentId)
{	
	$('#note').css("background-color", "");
	$('#errornotediv').empty();
	
		DistrictAjax.updateDistrictAttachmentStatus(districtattachmentId, {
			async: true,
			callback: function(data)
			{
				$('#distAttachmentGrid').html(data);
				applyScrollOnTblDistAttachment();
				displayDistAttachment();
			},
			errorHandler:handleError
		});
	
}

/********************************** district auto completer ***********************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray 	= new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;

//--------------------Searchbox---------------------------------------

function getSchoolSearchAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type) {
	
	
var districtId	=	document.getElementById("districtHiddenId").value;
	
	if(districtId=="")
	{
		document.getElementById('districtORSchoolName').readOnly=true;	
	}else
	{
		document.getElementById('districtORSchoolName').readOnly=false;
	}
	
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("onlyDistrictName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArrays(txtSearch.value);
	
		fatchsearchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}



function getSchoolArrays(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtHiddenId").value;
	
	
	
	DistrictAjax.getFieldOfSchoolList(districtId,schoolName,
	{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].schoolName;
			showDataArray[i]=data[i].schoolName;
			hiddenDataArray[i]=data[i].schoolId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}

var selectFirst="";

function fatchsearchData(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		} else {
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

//--------------------Searchbox---------------------------------------
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{

	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	DistrictAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
				if(count==10)
					break;
			}
		}
		else {

		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s) {
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function mouseOverChk(txtdivid,txtboxId) {	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event) {
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


/*========  For Handling session Time out Error ===============*/
function handleError(message, exception) {
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/

/*=========== For Searching Documents ================*/
function chkForEnterRecordsByEntityType(evt) {
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		searchUser();
	}	
}

function displayOrHideSearchBox(loggedEntityType) {
	$('#errordiv').empty();
	$('#onlyDistrictName').css("background-color", "");
	$('#districtORSchoolName').css("background-color", "");
	var entity_type	=	document.getElementById("MU_EntityType").value;
	//alert("entity_type ::::: "+entity_type);
	$(document).ready(function() {
		/*==== entity_type == 0 Means Select Entity Type 1 Means TM 2-> Districts and 3-> Schools */
		//alert("loggedEntityType  :::::::::::  "+loggedEntityType);
		if(loggedEntityType ==	1) {
			//alert(11111111111)
			document.getElementById("onlyDistrictName").value		=	"";
			document.getElementById("districtHiddenId").value		=	"";
			document.getElementById("districtORSchoolName").value	=	"";
			document.getElementById("schoolHiddenId").value			=	"";
			document.getElementById("docname").value				=	"";
			document.getElementById("onlyHeadQuarterName").value	=	"";
			document.getElementById("headQuarterHiddenId").value	=	"";
			document.getElementById("onlyBranchName").value	=	"";
			document.getElementById("branchHiddenId").value			=	"";
			
			$(".schoolClass").hide();
			if(entity_type ==	5) {	
			//	alert(55555555)
				$("#districtClass").hide();
				document.getElementById("onlyHeadQuarterName").value	=	"";
				document.getElementById("headQuarterHiddenId").value	=	"";
				document.getElementById("branchHiddenId").value			=	"";
				$("#onlyHeadQuarterName").focus();
				$("#headQuarterClass").show();
				$("#Searchbox").hide();
			}
			if(entity_type ==	6) {
			//	alert(66666666)
				document.getElementById("onlyHeadQuarterName").value	=	"";
				document.getElementById("headQuarterHiddenId").value	=	"";
				document.getElementById("onlyBranchName").value	=	"";
				document.getElementById("branchHiddenId").value			=	"";
				$("#onlyHeadQuarterName").focus();
				$("#headQuarterClass").show();
				$("#districtClass").hide();
				$("#Searchbox").hide();
			}
			if(entity_type ==	2) {	
			//	alert(22222222)
				$("#districtClass").show();
				$("#onlyDistrictName").focus();
				//$('#districtName').css("background-color", "");
				$("#Searchbox").hide();
			}

			if(entity_type ==	3) {
				//alert(33333333)
				$("#districtClass").show();
				$("#onlyDistrictName").focus();
				$("#Searchbox").show();
			}

			if(entity_type==0) {
				//alert(0000000)
				$("#districtClass").hide();
				$("#Searchbox").hide();
			}
		}
		if(loggedEntityType ==	2) {
			//alert("222222222   :::  "+loggedEntityType)
			document.getElementById("districtORSchoolName").value	=	"";
			document.getElementById("schoolHiddenId").value			=	"";
			document.getElementById("docname").value				=	"";
			//alert("222222222 Entity  :::  "+entity_type)
			if(entity_type ==	2 || entity_type==0) {
				$("#Searchbox").hide();
			}
			if(entity_type ==	3) {
				$("#Searchbox").show();
			}
		}
		if(loggedEntityType ==	5) {
			//alert("5555555555   :::  "+loggedEntityType)
			//document.getElementById("onlyHeadQuarterName").value	=	"";
			document.getElementById("branchHiddenId").value			=	"";
			document.getElementById("docname").value				=	"";
			//alert("666666 Entity  :::  "+entity_type)
			if(entity_type ==	5 || entity_type==0) {
				$("#branchClass").hide();
				$("#Searchbox").hide();
				document.getElementById("onlyBranchName").value	=	"";
				document.getElementById("branchHiddenId").value			=	"";
			}
			if(entity_type ==	6) {
				$("#branchClass").show();
				$("#Searchbox").hide();
			}
		}
		if(loggedEntityType ==	6) {
		//	alert("6666666   :::  "+loggedEntityType)
			document.getElementById("onlyBranchName").value	=	"";
			document.getElementById("branchHiddenId").value			=	"";
			document.getElementById("docname").value				=	"";
		//	alert("6666666 Entity  :::  "+entity_type)
			if(entity_type ==	6) {
				$("#Searchbox").show();
			}
		}
		
	});
}

/*=========== For Searching Attachments ================*/

function searchAttachment() {
	page = 1;
	displayDistAttachment();
}

function searchAttachmentByEntityType() {
	
	var entityID				=	document.getElementById("MU_EntityType").value;
	var schoolId				=	document.getElementById("schoolHiddenId").value;
	var districtId				=	document.getElementById("districtHiddenId").value;
	var docname                 =   document.getElementById("docname").value;
	var districtORSchoolName    =   trim(document.getElementById("districtORSchoolName").value);
	var onlyDistrictName   		=   trim(document.getElementById("onlyDistrictName").value);
	var userEntityType	        =	document.getElementById("userEntityType").value;
	var counter					=	0;
	var focusCount				=	0;
	$('#errordiv').empty();
	$('#onlyDistrictName').css("background-color", "");
	$('#districtORSchoolName').css("background-color", "");
	
	if(userEntityType==1){
		if(entityID==2 && districtORSchoolName==""){
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		
			if(focusCount	==	0)
				$('#districtORSchoolName').focus();
			$('#districtORSchoolName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}else if(entityID==3){
			if(onlyDistrictName==""){
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");	
				if(focusCount	==	0)
					$('#onlyDistrictName').focus();
				$('#onlyDistrictName').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
			if(districtORSchoolName==""){
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");	
				if(focusCount	==	0)
					$('#districtORSchoolName').focus();
				$('#districtORSchoolName').css("background-color", "#F5E7E1");
				counter++;
				focusCount++;
			}
		}
	}

	if(userEntityType==2 && entityID==3){
		if(districtORSchoolName==""){
			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrSchool+"<br>");	
			if(focusCount	==	0)
				$('#districtORSchoolName').focus();
			$('#districtORSchoolName').css("background-color", "#F5E7E1");
			counter++;
			focusCount++;
		}
	}

	var resultFlag=true;
	if(document.getElementById("districtORSchoolName").value=="" && entityID==2){
		document.getElementById("districtOrSchooHiddenlId").value=districtId;
	}
	if(document.getElementById("districtORSchoolName").value=="" && entityID==3){
		document.getElementById("districtOrSchooHiddenlId").value=schoolId;
	}
	var searchTextId	=	document.getElementById("districtOrSchooHiddenlId").value;

	if(document.getElementById("districtORSchoolName").value!="" && searchTextId==0){
		resultFlag=false;
	}
	
	if(counter	==	0) {
		DistrictAjax.displayRecordsByEntityType(resultFlag,entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType,docname,{ 
			async: true,
			callback: function(data)
			{
				$('#divMain').html(data);
				applyScrollOnTbl();
			},
			errorHandler:handleError
		});
	}
}
/*=========== For Searching Attachments ================*/

//----------------Rahul:15/11/2014

function clearFrame()
{
	document.getElementById("ifrmAttachment").src=""
	
}



/***************************************************************auto complete ******************************************************/
var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictDiv(dis,hiddenId,divId)
{
	document.getElementById("districtSessionId").value="";
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				
				$('#districtORSchoolName').attr('readonly', true);
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#districtORSchoolName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("districtSessionId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; Please enter valid District<br>");
			
			
			if(focus==0)
			$('#onlyDistrictName').focus();
			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

//commented by hafeez

//function hideDistrictMasterDiv(dis,hiddenId,divId)
//{
//	document.getElementById("districtOrSchooHiddenlId").value="";
//	$('#errordiv').empty();
//	if(parseInt(length)>0){
//		if(index==-1){
//			index=0;
//		}
//		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
//			document.getElementById(hiddenId).value=hiddenDataArray[index];
//		}
//	
//		if(dis.value==""){
//				document.getElementById(hiddenId).value="";
//		}
//		else if(showDataArray && showDataArray[index]){
//			dis.value=showDataArray[index];
//			document.getElementById("districtOrSchooHiddenlId").value=hiddenDataArray[index];
//		}
//		
//	}else{
//		if(document.getElementById(hiddenId))
//		{
//			document.getElementById(hiddenId).value="";
//		}
//		if(dis.value!=""){
//			var focuschk=0;
//			districtORSchoolNameCount=2;
//			$('#errordiv').empty();	
//			$('#errordiv').show();
//			if(document.getElementById("AEU_EntityType").value==2){
//				$('#errordiv').append("&#149; Please enter valid District<br>");
//			}else if(document.getElementById("AEU_EntityType").value==3){
//				$('#errordiv').append("&#149; Please enter valid School<br>");
//			}
//			
//			if(focus==0)
//			$('#districtORSchoolName').focus();
//			
//			focus++;
//		}
//	}
//	
//	if(document.getElementById(divId))
//	{
//		document.getElementById(divId).style.display="none";
//	}
//	index = -1;
//	length = 0;
//}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}
var isBranchExistsArray = new Array();
function getOnlyHeadQuarterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("headQuarterHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyHeadQuarterName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		//alert(searchArray);
		//fatchData2(txtSearch,searchArray,txtId,txtdivid);
		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			if(document.getElementById(txtId).value!="")
			{
				
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i].toUpperCase() + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
					
				}
				
			}
			else {
				
			}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}
			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
			{
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getOnlyHeadQuarterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("headQuarterHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyDistrictName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getHeadQuarterOnlyArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getHeadQuarterOnlyArray(headQuarterName){
	
			var searchArray = new Array();
			AutoSearchFilterAjax.getFieldOfHeadQuarterList(headQuarterName,{ 
			async: false,
			callback: function(data){
			isBranchExistsArray=new Array();
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){				
				searchArray[i]=data[i].headQuarterName;
				showDataArray[i]=data[i].headQuarterName;
				hiddenDataArray[i]=data[i].headQuarterId;
				isBranchExistsArray[i]=data[i].isBranchExist;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}
function hideHeadQuarterDiv(dis,hiddenId,divId)
{
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlyHeadQuarterName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
			document.getElementById("headQuarterHiddenId").value=hiddenDataArray[index];
			document.getElementById("isBranchExistsHiddenId").value=isBranchExistsArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; Please enter valid Head Quarter<br>");
			
			
			if(focus==0)
			$('#onlyHeadQuarterName').focus();			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
		if($('#onlyHeadQuarterName').val().trim()!=''){			
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyBranchName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}
		if($('#AEU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()=='' && $('#onlyBranchName').val()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',false);
		}
		if($('#AEU_EntityType').val()!='5' && $('#isBranchExistsHiddenId').val()!='' && $('#isBranchExistsHiddenId').val().trim()=='true'){
			$('#branchClass').fadeIn();
			$('#onlyBranchName').attr('readonly',false);
			$('#onlyBranchName').show();
		}else if($('#isBranchExistsHiddenId').val()!=''){
			$('.branchClass').hide();
			if($('#AEU_EntityType').val()=='3'){
				try{
					$('#onlyDistrictName').attr('readonly',false);
					$('#onlyDistrictName').focus();
					$('#onlyDistrictName').val('');
					$('#districtORSchoolName').val('');
					}catch(e){}			
			}else{
				try{
					$('#districtORSchoolName').attr('readonly',false);
					$('#onlyDistrictName').attr('readonly',false);
					$('#districtORSchoolName').focus();
					$('#onlyBranchName').val('');
					$('#districtORSchoolName').val('');
					}catch(e){}
			}
		}
	}
	index = -1;
	length = 0;
}

function getOnlyBranchAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("branchHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyBranchName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getBranchOnlyArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getOnlyBranchAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	document.getElementById("branchHiddenId").value="";
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("onlyBranchName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getBranchOnlyArray(txtSearch.value);
		//fatchData2(txtSearch,searchArray,txtId,txtdivid);
		var result = document.getElementById(txtdivid);
		try{
			result.style.display='block';
			result.innerHTML = '';
			var items='';
			count=0;
			var len=searchArray.length;
			if(document.getElementById(txtId).value!="")
			{
				
				for(var i=0;i<len;i++){
						items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
						searchArray[i].toUpperCase() + "</div>";
						count++;
						length++;
						
					if(count==10)
						break;
					
				}
				
			}
			else {
				
			}
			if(count!=0)
				result.innerHTML = items;
			else{
				result.style.display='none';
				selectFirst="";
			}
			if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
			{
				document.getElementById('divResult'+txtdivid+0).className='over';
				selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
			}
			
			scrolButtom();
		}catch (err){}
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}
function getBranchOnlyArray(branchName){
	
	var searchArray = new Array();
	var headQuarterId=$('#headQuarterHiddenId').val();
	if($('#userSessionEntityTypeId').val()==5){
		headQuarterId=$('#headQuarterId').val();
	}
	
	AutoSearchFilterAjax.getBranchListByHQ(headQuarterId,branchName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){				
				searchArray[i]=data[i].branchName;
				showDataArray[i]=data[i].branchName;
				hiddenDataArray[i]=data[i].branchId;
			}
		},
		errorHandler:handleError
		});	
	
	return searchArray;
}
function hideBranchDiv(dis,hiddenId,divId)
{
	$('#errordiv').empty();
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
		}
	
		if(dis.value==""){
				document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			$('#onlyBranchName').attr('readonly', false);
			dis.value=showDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
			document.getElementById("branchHiddenId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
		{
			document.getElementById(hiddenId).value="";
		}
		if(dis.value!=""){
			var focuschk=0;
			onlyDistrictName=2;
			$('#errordiv').empty();	
			$('#errordiv').show();
			
				$('#errordiv').append("&#149; Please enter valid Branch<br>");
			
			
			if(focus==0)
			$('#onlyBranchName').focus();			
			focus++;
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
		if($('#onlyBranchName').val().trim()!=''){			
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}else if($('#AEU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()!='' && $('#isBranchExistsHiddenId').val()!='' && $('#isBranchExistsHiddenId').val().trim()=='true' && $('#onlyBranchName').val().trim()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',true);
		}else if($('#AEU_EntityType').val()=='3' && $('#onlyHeadQuarterName').val()=='' && $('#onlyBranchName').val()==''){
			$('#districtORSchoolName').attr('readonly',true);
			$('#onlyDistrictName').attr('readonly',false);
		}
		if($('#onlyBranchName').val()!=''){			
			if($('#AEU_EntityType').val()=='3'){
				try{
					$('#onlyDistrictName').attr('readonly',false);
					$('#onlyDistrictName').focus();
					$('#onlyDistrictName').val('');
					$('#districtORSchoolName').val('');
					$('#districtHiddenId').val();
					$('#schoolHiddenId').val();
					}catch(e){}			
			}else{
				try{
					$('#districtORSchoolName').attr('readonly',false);
					$('#onlyDistrictName').attr('readonly',false);
					$('#districtORSchoolName').focus();
					$('#districtORSchoolName').val('');
					$('#schoolHiddenId').val();
					$('#districtHiddenId').val();
					}catch(e){}
			}
		}
	}
	index = -1;
	length = 0;
}

$(function(){
	function clearBackground(){
		$('#onlyDistrictName').css("background-color", "");
		$('#onlyBranchName').css("background-color", "");
		$('#onlyHeadQuarterName').css("background-color", "");
		$('#districtORSchoolName').css("background-color", "");
		}
	$('.clearback').click(function(){clearBackground();});
	
	$('#AEU_EntityType').change(function(){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		$('#districtOrSchooHiddenlId').val('');
		$('#schoolHiddenId').val('');
		$('#onlyBranchName').val('');
		$('#branchHiddenId').val('');
		$('#onlyHeadQuarterName').val('');
		$('#headQuarterHiddenId').val('');
	});

	$('#onlyHeadQuarterName').blur(function(){		
		if($('#onlyHeadQuarterName').val()!=null && $('#onlyHeadQuarterName').val()!=""){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		$('#schoolHiddenId').val('');
		$('#onlyBranchName').val('');
		$('#branchHiddenId').val('');
		}else{
			$('#onlyBranchName').val('');
			$('#branchHiddenId').val('');
		}
		});
	$('#onlyBranchName').blur(function(){
		if($('#onlyBranchName').val()!=null && $('#onlyBranchName').val()!=""){
		$('#onlyDistrictName').val('');
		$('#districtHiddenId').val('');
		$('#districtORSchoolName').val('');
		$('#schoolHiddenId').val('');
		}
		});
	$('#onlyDistrictName').blur(function(){
		if($('#onlyDistrictName').val()!=null && $('#onlyDistrictName').val()!=""){		
		$('#districtORSchoolName').val('');
		$('#schoolHiddenId').val('');
		}
		});
});
/***************************************************************End********************************************************************/
function clrlPage(){
	$("#successModal").modal('hide');
	applyScrollOnTblDistAttachment();
	displayDistAttachment();
}

//*************************** Only for self service layout *****************************************************************
	function setEntityType()
  	{
		var entityType=$("#userEntityType").val();
		var entityString="";
		var headQuarter="";
		var branch="";
		if(entityType==1){
			entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="0" selected="selected"><spring:message code="optAll"/></option><option value="5">HeadQuarter</option><option value="6">Branch</option><option value="2"><spring:message code="optDistrict"/></option><option value="3"><spring:message code="optSchool"/></option>';
			headQuarter="<label>Head Quarter</label><input class='form-control clearback' type='text' id='onlyHeadQuarterName' name='onlyHeadQuarterName' value='${headQuarterName}' onfocus=\"getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');\" onkeyup=\"getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');\" onblur=\"hideHeadQuarterDiv(this,'headQuarterHiddenId','divTxtHeadQuarterData');\"	/><div id='divTxtHeadQuarterData' style=' display:none;position:absolute;'  onmouseover=\"mouseOverChk('divTxtHeadQuarterData','onlyHeadQuarterName')\"  class='result' ></div> <input type='hidden' name='headQuarterHiddenId' id='headQuarterHiddenId' value='${headQuarterId}'/><input type='hidden' id='isBranchExistsHiddenId' value='${isBranchExistsHiddenId}'/>";
		}else if(entityType==2){
			entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="0" ><spring:message code="optAll"/></option><option value="2"  selected="selected" ><spring:message code="optDistrict"/></option><option value="3"><spring:message code="optSchool"/></option>';
		}else if(entityType==3){
			entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="3" selected="selected"><spring:message code="optSchool"/></option>';
			branch="<label>Branch</label><input class='form-control clearback' type='text' id='onlyBranchName' name='onlyBranchName'   value='${branchName}'	onfocus=\"getOnlyBranchAutoComp(this, event, 'divTxtBranchData', 'onlyBranchName','branchHiddenId','');\" onkeyup=\"getOnlyBranchAutoComp(this, event, 'divTxtBranchData', 'onlyBranchName','branchHiddenId','');\" onblur=\"hideBranchDiv(this,'branchHiddenId','divTxtBranchData');\"	/>";
			}else if(entityType==5){
			entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="5" selected="selected">HeadQuarter</option><option value="6">Branch</option>';
		    headQuarter="<label>Head Quarter</label><input class='form-control clearback' type='text' id='onlyHeadQuarterName' name='onlyHeadQuarterName' value='${headQuarterName}'/><input type='hidden' name='headQuarterHiddenId' id='headQuarterHiddenId' value='${headQuarterId}'/><input type='hidden' id='isBranchExistsHiddenId' value='${isBranchExistsHiddenId}'/>";
		}else if(entityType==6){
			entityString='<label>Entity Type</label><select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();"><option value="6">Branch</option>';
			beanch="<label>Branch</label><input class='form-control clearback' type='text' id='onlyBranchName' name='onlyBranchName'   value='${branchName}'/>";
		}
  		var district="<div class='col-sm-12 col-md-12 hide' id='districtSearchBox'><label id='captionDistrictOrSchool'>District name</label><span><input autocomplete='off' style='color:#555555; font-size: 14px;'  type='text' id='districtNameFilter'  value='' maxlength='100'  name='districtNameFilter' class='help-inline form-control' onfocus=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\" onkeyup=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\" onblur=\"hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');\"/></span><input type='hidden' id='districtIdFilter' value='${userMaster.districtId.districtId}' /><div id='divTxtShowDataFilter'  onmouseover=\"mouseOverChk('divTxtShowDataFilter','districtNameFilter')\" style='display:none;position:absolute;z-index:5000;' class='result' ></div>";
  		var btnRecord='<button onclick="searchJobCat();" class="flatbtn" style="background: #9fcf68;width:60%" id="save" title="Search"><strong>Search <i class="fa fa-check-circle"></i></strong></button>';
  		globalSearchSection(entityString,"","",district,btnRecord);
  	}