function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}
var rdURL = "userdashboard.do"; 
var totalAttempted=0;
var jOrder;
var vData;
var assessmentTakenCount=0;
var iJobId=null;
var jJobId=null;
function checkInventory(jobId,epiJobId)
{
	//alert("checkInventory callNewSelfServiceApplicationFlow assessment-campaign");
	DSPQServiceAjax.callNewSelfServiceApplicationFlow(jobId,'E',{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!='' && data!="DSPQNC" && data!="OldProcess" && data!="AlreadyCompleted")
			{
				window.location.href="./"+data;
			}
			else if(data!='' && data=="DSPQNC")
			{
				$('#DSPQNCDivNotification').modal('show');
			}
			else if(data!='' && data=="AlreadyCompleted")
			{
				$('#DSPQCompletedDivNotification').modal('show');
			}
			else
			{
				//alert("AC Call 3");
				var jobOrder={jobId:jobId};
				jJobId = jobId;
				AssessmentCampaignAjax.checkInventory(jobOrder,epiJobId,{ 
					async: false,
					errorHandler:handleError,
					callback: function(data){
					//The Job Specific Inventory for this job has not been finalized. Please come back later to complete this application.
					var remodel=resourceJSON.msgBaseInventory;
					if(jobId>0)
						remodel=resourceJSON.msgJobSpecificInventory;

					if(data.assessmentName==null && data.status=='1')
					{
						$('#message2show').html(""+resourceJSON.msgThe+" "+remodel+" "+resourceJSON.msgFinalizedCompleteApplication);
						$('#myModal2').modal('show');
					}else if(data.assessmentName==null && data.status=='2')
					{
						$('#message2show').html(""+resourceJSON.msgThe+" "+remodel+" "+resourceJSON.msgFinalizedCompleteApplication);
						$('#myModal2').modal('show');
					}
					else if(data.assessmentName==null && data.status=='3')
					{
						if(jobId>0)
							getTeacherJobDone(jobId);
						else
						{
							$('#message2show').html(resourceJSON.msgAlreadyCompletedInventory);
							$('#myModal2').modal('show');
						}
					}else if(data.assessmentName==null && data.status=='4')
					{
						$('#message2show').html(resourceJSON.msgExpiredTeacherMatchAdministrator);
						$('#myModal2').modal('show');
					}
					else if(data.assessmentName==null && data.status=='5')
					{
						if(jobId>0)
							getTeacherJobDone(jobId);
						else
						{
							$('#message2show').html(resourceJSON.msgTimedOutTeacherMatchAdministrator);
							$('#myModal2').modal('show');
						}
					}
					else if(data.assessmentName==null && data.status=='6')
					{
						$('#message2show').html(resourceJSON.msgActiveTeacherMatchAdministrator);
						$('#myModal2').modal('show');
					}
					else if(data.assessmentName==null && data.status=='7')
					{
						$('#message2show').html(resourceJSON.msgjobExpiredTeacherMatchAdministrator);
						$('#myModal2').modal('show');
					}
					else
					{
						if(data.status.charAt(2)>=3)
						{
							//$("#myModalvk").css({ top: '60%' });
							$('#warningImg1k').html("<img src='images/stop.png' align='top'>");
							$('#message2showConfirm1k').html(""+resourceJSON.msgDidntAnswithin3Attempt+" clientservices@teachermatch.net "+resourceJSON.msgor+" (888) 312-7231 .");
							$('#nextMsgk').html("");
							$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' >"+resourceJSON.btnOk+"</button>");
							$('#vcloseBtnk').html("<span>x</span>");
							$('#myModalvk').modal('show');
							if(document.getElementById("bStatus"))
							{
								$('#bStatus').html(resourceJSON.msgtimeout1);
								$('#iconpophover3').trigger('mouseout');
								$('#bClick').html("");
							}
						}else
						{
							
							var st_re = data.status.charAt(2)==''?0:data.status.charAt(2);
								
							$('#warningImg1').html("<img src='images/info.png' align='top'>");
							
							if(jobId>0)
							{
								$('#message2showConfirm1').html(data.assessmentDescription);
								$('#myModalLabelId').html(resourceJSON.msgAdditionalQuestions);
							}
							else
							{
								$('#myModalLabelId').html("TeacherMatch");
								
								if(data.ipaddress=='kelly' && jobId==0)
								{	
									$('#message2showConfirm1').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
									var infomsg = "resume";
									if(st_re==0 && jobId<=0)
										infomsg = "start";
									
									/*var mmms = "<p>Welcome valuable <b>Kelly Educational Staffing&reg; (KES&reg;)</b> talent. You are about to "+infomsg+" the <b>Educators Professional Inventory (EPI)</b>, a non-discriminatory, research-based teaching assessment developed by education experts. It evaluates full-time teaching candidates against four \"success indicators\" that education research has identified for highly effective teachers who will deliver academic growth: Qualifications, Teaching Skills, Cognitive Ability, and Attitudinal Factors.</p>" +
											"<p>KES is proud to partner with TeacherMatch&trade; to support and enhance our talent solutions to K-12 school districts. At the core, KES and TeacherMatch share a common philosophy and believe high quality substitute teachers are the most critical factor in fostering student achievement. Therefore, we're excited that we are able to offer you - a valued KES substitute teacher candidate - exclusive access to the EPI.</p>" +
											"<p>For more information about the exclusive benefits of EPI as KES substitute teacher candidate, including how to receive your free Professional Development Plan, please click the following link: xxxxxx <br>Important Note: The TeacherMatch EPI is NOT used as a hiring assessment tool by KES, and your score will NEVER be used as a qualifier or filter for you to be able to view or accept substitute-teaching positions with Kelly Educational Staffing school districts. Each item in the EPI has a stipulated time limit of 75 seconds. You must respond to each item within its stipulated time limit. You must answer all of the questions in one sitting.</p>" +
											"<p>We strive to maintain the integrity of the EPI and ensure that everyone has a fair and equal opportunity to complete the EPI. Therefore, before beginning, please note these IMPORTANT guidelines:</p>" +
											"<p>If you continue, please be prepared to follow these guidelines:<p><ul>" +
											"<li>While the average person takes approximately 45 minutes to complete, please reserve at least 90 minutes of uninterrupted time to complete it and avoid any 'timed out' issues Make sure that you have a stable and reliable internet connection </li>" +
											"<li> Do not close your browser or hit the \"back\" button on your browser</li>" +
											"<li> There are 75 questions - each one should take about 30 seconds to answer, but you have a maximum 75 seconds per question</li>" +
											"<li> A timer at corner of the screen will show you how much time you have left per question . . . and for the remainder of the test</li>" +
											"<li> You are not allowed to skip questions - it's to your advantage to try to answer each one within the allocated time, even if you must guess </li>" +
											"</ul>" +
											"<p>Accommodations</p>" +
											"<p>If you require accommodations for this assessment, we recommend that you contact your local KES branch.Please click \"Ok\" if you are ready to take the EPI in its entirety otherwise, please click \"Cancel\" to exit.</p>" +
											"Best,<br>Kelly Educational Staffing&reg; (KES&reg;)";*/
									
									var mmms = "<p>Welcome valuable <b>Kelly Educational Staffing&reg; (KES&reg;)</b> talent. You are about to "+infomsg+" the <b>Educators Professional Inventory (EPI)</b>, a non-discriminatory, research-based teaching assessment developed by education experts. It evaluates full-time teaching candidates against four \"success indicators\" that education research has identified for highly effective teachers who will deliver academic growth: Qualifications, Teaching Skills, Cognitive Ability, and Attitudinal Factors.</p>"+
									"<p>KES is proud to partner with <b>TeacherMatch&trade;</b> to support and enhance our talent solutions to K-12 school districts. At the core, KES and TeacherMatch share a common philosophy and believe high quality substitute teachers are the most critical factor in fostering student achievement. Therefore, we're excited that we are able to offer you - a valued KES substitute teacher candidate - exclusive access to the EPI.</p>"+
									"<p><b>For more information about the exclusive benefits of EPI as a KES substitute teacher candidate, including how to receive your free Professional Development Plan, please contact Kelly Educational Staffing at 885-535-5915 or via email at <a href=\"mailto:KESTMSP@kellyservices.com\" target=\"_top\">KESTMSP@kellyservices.com</a>.</b></p>"+
									"<p><b>Important Note:</b> The TeacherMatch EPI is NOT used as a hiring assessment tool by KES, and your performance will NEVER be used as a qualifier or filter for you to be able to view or accept substitute-teaching positions with Kelly Educational Staffing school districts. Each item in the EPI has a stipulated time limit of 75 seconds. You must respond to each item within its stipulated time limit. You must answer all of the questions in one sitting.</p>"+
									"<p>We strive to maintain the integrity of the EPI and ensure that everyone has a fair and equal opportunity to complete the EPI. Therefore, before beginning, please note these IMPORTANT guidelines:</p>" +
									"<p>If you continue, please be prepared to follow these guidelines:<p><ul>" +
									"<li>While the average person takes approximately 45 minutes to complete, please reserve at least 90 minutes of uninterrupted time to complete it and avoid any 'timed out' issues Make sure that you have a stable and reliable internet connection </li>" +
									"<li> Do not close your browser or hit the \"back\" button on your browser</li>" +
									"<li> There are 75 questions - each one should take about 30 seconds to answer, but you have a maximum 75 seconds per question</li>" +
									"<li> A timer at corner of the screen will show you how much time you have left per question . . . and for the remainder of the test</li>" +
									"<li> You are not allowed to skip questions - it's to your advantage to try to answer each one within the allocated time, even if you must guess </li>" +
									"</ul>" +
									"<p>Accommodations</p>" +
									"<p><b>If you require accommodations for this assessment, we recommend that you contact your local KES branch</b>. Please click \"Ok\" if you are ready to take the EPI in its entirety otherwise, please click \"Cancel\" to exit.</p>" +
									"Best,<br><b>Kelly Educational Staffing&reg; (KES&reg;)</b>";
									
									$('#nextMsg').html(mmms);
									
								}else
								{
									var msg = resourceJSON.msgEducatorsProfessionalInventory;
									if(jobId>0)
										msg = resourceJSON.msgJobSpecificInventory;
									var infomsg = "<p>"+resourceJSON.msgAboutResume+" "+msg+".";
									if(st_re==0 && jobId<=0)
										infomsg = "<p>"+resourceJSON.msgAboutStart+" "+msg+".";
									
									$('#message2showConfirm1').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
									$('#nextMsg').html(infomsg+data.splashInstruction2);
									
									if(jobId<=0 && infomsg.indexOf('resume')!=-1){
										$('#message2showConfirm1').html("<b>Welcome back!</b>");
										$('#nextMsg').html("You are about to resume the <b>TeacherMatch Educators Professional Inventory (EPI)</b>. There are 100 questions in the inventory. They are divided into three sections: Attitudinal Factors, Cognitive Ability, and Teaching Skills, each a key success indicator for highly effective teachers.<p></p><b>BEFORE YOU BEGIN:</b> Be prepared to follow these important guidelines:<ul><li><b><font color='red'>Each item or question in the EPI has a stipulated time limit of 75 seconds</font></b> for you to submit your answer. There is a timer in the upper right corner of the screen to help you keep track of time.</li><li>A pop-up reminder will appear on your screen when you have <b>30 seconds</b> left to submit your answer to any question. It is important to respond within this time period to avoid being <b><font color='red'>&#8220;Timed Out&#8221;</font></b>.</li><li>You are <b><font color='red'>not able to skip questions</font></b> &#8212; and you <b><font color='red'>must answer all of the questions</font></b> in one session.</li><li>Once you click the <b>&#8220;Next&#8221;</b> button, <b><font color='red'>your answer is recorded as final</font></b> and cannot be changed. This means you cannot hit the <b>Back</b> arrow on your browser to get back to the previous question to change your answer.</li></ul><p></p>Please click <b>&#8220;Ok&#8221;</b> if you are ready to take the <b>EPI</b> in its entirety and abide by the guidelines presented above. Or, click <b>&#8220;Cancel&#8221;</b> to exit.");
									}
									
									/*infomsg+=resourceJSON.msg100QuestionsInventory+"" +
											"<br><br>"+resourceJSON.msgEPIStipulatedTime75Seconds+"</p>";
									
									$('#message2showConfirm1').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
									var nextMsg = infomsg+"<b>"+resourceJSON.msgPreparedTheseGuidelines+":</b><br><ul> "+
									"<li>"+resourceJSON.msg90MinutesUninterrupted+"</li> "+ 
									"<li>"+resourceJSON.msgStableReliableInternet+"</li> "+ 
									"<li>"+resourceJSON.msgBrowserButton+" </li>"+
									"<li>"+resourceJSON.msgAbleSkipQuestions+"</li> " +
									"<li>"+resourceJSON.msgComprehensiveOverviewTeacherMatch+" <a target='_blank' href='policiesandprocedures.do'>"+resourceJSON.msghere+"</a></li> " +
									"</ul>";
								
									nextMsg+="<p>"+resourceJSON.msgMaintainIntegrityEPI+"</p>";
									nextMsg+="<p>"+resourceJSON.msgUnableRetakeEPI12Months+"</p>";
									nextMsg+="<p><b>"+resourceJSON.msgAccommodations+"</b> </p>";
									nextMsg+="<p>"+resourceJSON.msgRequireAccommodationsAssessment+"</p>";
									
									$('#nextMsg').html(nextMsg+" "+resourceJSON.msgEPIEntiretyPrepared+"");*/
								}
							}
								$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"startInventory()\">"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
								$('#vcloseBtn').html("<span>x</span>");
								$('#myModalv').modal('show');
								//$("#myModalv").css({ top: '45%' });
								
								jOrder=jobOrder;
								vData=data;
								assessmentTakenCount=data.status.charAt(4);
								iJobId=epiJobId;
						}
					}
				}
				});	
				
			}
		}
	});
	
	
}

function startInventory()
{
	var data = vData;
	var jobOrder = jOrder;
	var loadingShow = true;
	var epiJobId = iJobId;
	if(data.status.indexOf("#")==-1)
	{
		$('#loadingDivInventory').fadeIn();
		loadAssessmentQuestions(data,jobOrder,epiJobId);
		
	}else
	{
		var newJobId = jobOrder.jobId;
		var dd = data.status;
		//var jId = dd.split("#")[2];
		var jId = dd.split("#")[3];
		
		var msg = "EPI";
		if(jId!=0)
		{
			jobOrder.jobId=jId;
			jOrder.jobId=jId;
			msg = "Job Specific Inventory";
		}
		
		if(data.status.charAt(2)==0 || data.status.charAt(2)==1)
		{
			data.status=data.status.charAt(0);
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/warn.png' align='top'>");
			$('#message2showConfirm1k').html("<b>"+resourceJSON.msgReadCarefully+"</b>");
			$('#nextMsgk').html(resourceJSON.msgExceededTimeLimit+" <font color='red'>"+resourceJSON.msgSECOND+"</font> "+resourceJSON.msgReminderRegardingImportance+" "+msg+". "+resourceJSON.msgStipulatedLimit);
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"runInventory(1,"+newJobId+");\">"+resourceJSON.btnOk+"</button>");
			$('#vcloseBtnk').html("<span onclick=\"runInventory(1,"+newJobId+");\">x</span>");
			$('#myModalvk').modal('show');

		}else if(data.status.charAt(2)==2)
		{
			data.status=data.status.charAt(0);
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/warn.png' align='top'>");
			$('#message2showConfirm1k').html("<b>"+resourceJSON.msgPleaseReadCarefully+"</b>");
			var nextMsg="<p>"+resourceJSON.msgexceededtimelimitforsec1 +msg+". "+resourceJSON.msgexceededtimelimitforsec2 +msg+" "+resourceJSON.msgitems+".</p>";
			
			if(jId==0)
			nextMsg += "<p>"+resourceJSON.msgTMStatRemain1+"</p><p>"+resourceJSON.msgTMStatRemain2+"</p>";
			
			//nextMsg += "<p>"+resourceJSON.msgTMPolicy1+" <a href='javascript:void(0);'>here</a>.</p><p> "+resourceJSON.msgTMPolicy2+" (888) 312-7231 "+resourceJSON.msgTMPolicy3+" <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</p> <p>"+resourceJSON.msgTMPolicy4+"</p>";
			nextMsg += "<p>If you would like to review TeacherMatch's policy regarding this issue in greater depth please click <a href='javascript:void(0);'>here</a>.</p><p> If you have any further questions regarding this matter, please call us at (888) 312-7231 or email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</p> <p>Thank you and best of luck in your job search.</p>";
			

			$('#nextMsgk').html(nextMsg);
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick=\"runInventory(1,"+newJobId+");\">"+resourceJSON.btnOk+"</button>");
			$('#vcloseBtnk').html("<span onclick=\"runInventory(1,"+newJobId+");\">x</span>");
			$('#myModalvk').modal('show');
			
		}
		/*else if(data.status.charAt(2)>=3)
		{
			//alert("You did not answer the question within three attempts. You have been \"disqualified\" and cannot continue the inventory at this time. Please contact technical support at (888) 312-7231 .");
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/stop.png' align='top'>");
			$('#message2showConfirm1k').html("You did not answer the question within three attempts. You have been \"disqualified\" and cannot continue the inventory at this time. If you have questions, please contact support at clientservices@teachermatch.net or (888) 312-7231 .");
			$('#nextMsgk').html("");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' >Ok</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');
			if(document.getElementById("bStatus"))
			{
				$('#bStatus').html("Violated");
				$('#bClick').html("");
			}
		}*/
	}
}
function runInventory(flg,newJobId)
{
	var data = vData;
	var jobOrder = jOrder;
	var epiJobId = iJobId;
	runAssessment(data,jobOrder,1,newJobId,epiJobId);
}
function loadAssessmentQuestions(assessmentDetail,jobOrder,epiJobId)
{
	AssessmentCampaignAjax.loadAssessmentQuestions(assessmentDetail,jobOrder,epiJobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==1)
			runAssessment(assessmentDetail,jobOrder,0,0,epiJobId);
		else if(data=='vlt')
		{
			$('#loadingDivInventory').hide();
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/warn.png' align='top'>");
			$('#message2showConfirm1k').html( resourceJSON.msgTimeutInvt );
			$('#nextMsgk').html("");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');
		}else if(data=='comp')
		{
			try{$('#loadingDivInventory').hide();
			//$("#myModalvk").css({ top: '60%' });
			$('#warningImg1k').html("<img src='images/info.png' align='top'>");
			$('#message2showConfirm1k').html("You have already completed this inventory.");
			$('#nextMsgk').html("");
			$('#footerbtn1k').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#vcloseBtnk').html("<span>x</span>");
			$('#myModalvk').modal('show');
			}catch(err)
			{
			}
		}else if(data=='icomp')
		{
			$('#loadingDivInventory').hide();
		}
	}
	});	
	
}

function runAssessment(assessmentDetail,jobOrder,doInsert,newJobId,epiJobId)
{
	$('#loadingDivInventory').fadeIn();
	if(doInsert==1)
	{
		insertTeacherAssessmentAttempt(assessmentDetail,jobOrder);
		// last Attempt insertion
		//insertLastAttempt(jobOrder);
		insertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder);
	}

	if(epiJobId==null)
		epiJobId=0;
	
	var redirectURL = "runAssessment.do?assessmentId="+assessmentDetail.assessmentId+"&jobId="+jobOrder.jobId+"&newJobId="+newJobId+"&epiJobId="+epiJobId;
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
}
function insertTeacherAssessmentAttempt(assessmentDetail,jobOrder)
{
	AssessmentCampaignAjax.insertTeacherAssessmentAttempt(assessmentDetail,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}
function insertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder)
{
	AssessmentCampaignAjax.insertLastAttempt(assessmentDetail,assessmentTakenCount,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
	}
	});	
}
function getAssessmentSectionCampaignQuestionsGrid(whoClicked)
{
try
{
	var attemptId = dwr.util.getValue("attemptId");
	totalAttempted = dwr.util.getValue("totalAttempted");
	var assessmentDetail = {assessmentId:dwr.util.getValue("assessmentId")};
	var teacherAssessmentdetail = {teacherAssessmentId:dwr.util.getValue("teacherAssessmentId"),assessmentType:dwr.util.getValue("teacherAssessmentType")};
	var teacherAssessmentQuestion // = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
	var teacherSectionDetail //= {teacherSectionId:dwr.util.getValue("teacherSectionId")};
	var arr =[];
	if(document.getElementById("teacherAssessmentQuestionId"))
	{
		teacherAssessmentQuestion = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
		if(document.getElementById("teacherAssessmentQuestionId").value!="")
		{
			var opts=document.getElementsByName("opt");
			var questionTypeShortName = dwr.util.getValue("questionTypeShortName");
			var questionTypeMaster = {questionTypeId:dwr.util.getValue("questionTypeId")};
			var qType = dwr.util.getValue("questionTypeShortName");
			var questionWeightage = dwr.util.getValue("questionWeightage");
			var questionSessionTime = dwr.util.getValue("questionSessionTime");

			var o_maxMarks = dwr.util.getValue("o_maxMarks");
			if(qType=='tf' || qType=='slsel' || qType=='lkts' || qType=='it')
			{
				var optId="";
				var score=0;
				var questionOptionId = null;
				var questionOptionTag = null;
				if($('input[name=opt]:radio:checked').length > 0 )
				{
					optId=$('input[name=opt]:radio:checked').val();
					var opts = document.getElementsByName("opt");
					var scores = document.getElementsByName("score");
					var questionOptionIds = document.getElementsByName("questionOptionId");
					var questionOptionTags = document.getElementsByName("questionOptionTag");
					for (var i = 0; i < opts.length; i++) {
						if(optId==opts[i].value)
						{
							score = scores[i].value;
							questionOptionId = questionOptionIds[i].value;
							questionOptionTag = questionOptionTags[i].value;
							break;
						}
					}
				}
				var totalScore = questionWeightage*score;
				if(optId=='')
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);

					return;
				}

				arr.push({ 
					"selectedOptions"  : optId,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"jobOrder" : jobOrder,
					"maxMarks" : o_maxMarks,
					"questionOptionId" : (questionOptionId!='null'?questionOptionId:""),
					"questionOptionTag" : (questionOptionTag!='null'?questionOptionTag:"")
				});

			}else if(qType=='rt')
			{
				var optId="";
				var score="";
				var rank="";
				var scoreRank=0;
				var questionOptionId = "";
				var questionOptionTag = "";
				var opts = document.getElementsByName("opt");
				var scores = document.getElementsByName("score");
				var ranks = document.getElementsByName("rank");
				var o_ranks = document.getElementsByName("o_rank");
				var questionOptionIds = document.getElementsByName("questionOptionId");
				var questionOptionTags = document.getElementsByName("questionOptionTag");
				var tt=0;
				var uniqueflag=false;
				for(var i = 0; i < opts.length; i++) {
					optId += opts[i].value+"|";
					score += scores[i].value+"|";
					rank += ranks[i].value+"|";
					questionOptionId = questionOptionIds[i].value+"|";
					questionOptionTag = questionOptionTags[i].value+"|";

					if(checkUniqueRank(ranks[i]))
					{
						uniqueflag=true;
						break;
					}

					if(ranks[i].value==o_ranks[i].value)
					{
						scoreRank+=parseInt(scores[i].value);
						//alert(scoreRank);
					}
					if(ranks[i].value=="")
					{
						tt++;
					}
				}
				/////////////////////////////////////
				if(uniqueflag)
					return;

				if(tt!=0)
					optId=""; 

				if(optId=="")
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);
					return;
				}

				var totalScore = questionWeightage*scoreRank;
				arr.push({ 
					"selectedOptions"  : optId,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"insertedRanks"    : rank,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"jobOrder" : jobOrder,
					"maxMarks" : o_maxMarks,
					"questionOptionId" : (questionOptionId!=""?questionOptionId:""),
					"questionOptionTag" : (questionOptionTag!=""?questionOptionTag:"")
				});

			}else if(qType=='sl' || qType=='ml')
			{
				if(dwr.util.getValue("opt")=='')
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);

					return;
				}

				arr.push({ 
					"insertedText"    : dwr.util.getValue("opt"),
					"questionTypeMaster" : questionTypeMaster,
					"totalScore"       : 0,
					"questionWeightage" : 0,
					"jobOrder" : jobOrder,
					"maxMarks" :o_maxMarks
				});
			}
			else if(qType=='mlsel')
			{
				var optId="";
				var score=0;
				var questionOptionId = null;
				var questionOptionTag = null;
				if($('input[name=opt]:checked').length > 0 )
				{
					optId=$('input[name=opt]:checked').val();
					var opts = document.getElementsByName("opt");
					var scores = document.getElementsByName("score");
					var questionOptionIds = document.getElementsByName("questionOptionId");
					var questionOptionTags = document.getElementsByName("questionOptionTag");
					var optsValues="";
					for (var i = 0; i < opts.length; i++) {
						if(opts[i].checked==true)
						{
							if(optsValues=="")
							{
								optsValues=opts[i].value+"|";
								questionOptionId = questionOptionIds[i].value+"|";
								questionOptionTag = questionOptionTags[i].value+"|";
							}
							else
							{
								optsValues=optsValues+opts[i].value+"|";
								questionOptionId = questionOptionId+questionOptionIds[i].value+"|";
								questionOptionTag = questionOptionTag+questionOptionTags[i].value+"|";
							}
						}
					}
				}
				var totalScore = questionWeightage*score;
				if(optId=='')
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);

					return;
				}

				arr.push({ 
					"selectedOptions"  : optsValues,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"jobOrder" : jobOrder,
					"maxMarks" :o_maxMarks,
					"questionOptionId" : (questionOptionId!='null'?questionOptionId:""),
					"questionOptionTag" : (questionOptionTag!='null'?questionOptionTag:"")
				});

			}
			else if(qType=='mloet')
			{
				var insertedText = dwr.util.getValue("insertedText");
				var optId="";
				var score=0;
				var questionOptionId = null;
				var questionOptionTag = null;
				if($('input[name=opt]:checked').length > 0 )
				{
					optId=$('input[name=opt]:checked').val();
					var opts = document.getElementsByName("opt");
					var scores = document.getElementsByName("score");
					var questionOptionIds = document.getElementsByName("questionOptionId");
					var questionOptionTags = document.getElementsByName("questionOptionTag");
					var optsValues="";
					for (var i = 0; i < opts.length; i++) {
						if(opts[i].checked==true)
						{
							if(optsValues=="")
							{
								optsValues=opts[i].value+"|";
								questionOptionId = questionOptionIds[i].value+"|";
								questionOptionTag = questionOptionTags[i].value+"|";
							}
							else
							{
								optsValues=optsValues+opts[i].value+"|";
								questionOptionId = questionOptionId+questionOptionIds[i].value+"|";
								questionOptionTag = questionOptionTag+questionOptionTags[i].value+"|";
							}
						}
					}
				}
				var totalScore = questionWeightage*score;
				if(optId=='')
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);

					return;
				}

				arr.push({ 
					"selectedOptions"  : optsValues,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"jobOrder" : jobOrder,
					"maxMarks" :o_maxMarks,
					"insertedText"    : insertedText,
					"questionOptionId" : (questionOptionId!='null'?questionOptionId:""),
					"questionOptionTag" : (questionOptionTag!='null'?questionOptionTag:"")
				});

			}
			else if(qType=='sloet')
			{
				var insertedText = dwr.util.getValue("insertedText");
				var optId="";
				var score=0;
				var questionOptionId = null;
				var questionOptionTag = null;
				if($('input[name=opt]:radio:checked').length > 0 )
				{
					optId=$('input[name=opt]:radio:checked').val();
					var opts = document.getElementsByName("opt");
					var scores = document.getElementsByName("score");
					var questionOptionIds = document.getElementsByName("questionOptionId");
					var questionOptionTags = document.getElementsByName("questionOptionTag");
					for (var i = 0; i < opts.length; i++) {
						if(optId==opts[i].value)
						{
							score = scores[i].value;
							questionOptionId = questionOptionIds[i].value;
							questionOptionTag = questionOptionTags[i].value;
							break;
						}
					}
				}
				var totalScore = questionWeightage*score;
				if(optId=='')
				{
					totalSkippedQuestions++;
					//strike checking
					checkStrikes(questionSessionTime,whoClicked);

					return;
				}

				arr.push({ 
					"selectedOptions"  : optId,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"jobOrder" : jobOrder,
					"maxMarks" :o_maxMarks,
					"insertedText"    : insertedText,
					"questionOptionId" : (questionOptionId!='null'?questionOptionId:""),
					"questionOptionTag" : (questionOptionTag!='null'?questionOptionTag:"")
				});

			}
			else
			{
				totalSkippedQuestions++;
				//strike checking
				checkStrikes(questionSessionTime,whoClicked);
				return;

				arr.push({ 
					"questionWeightage" : questionWeightage,
					"questionTypeMaster" : questionTypeMaster,
					"totalScore" : 0,
					"jobOrder" : jobOrder,
					"maxMarks" :o_maxMarks
				});
			}

		}

	}

	if(document.getElementById("teacherSectionId"))
		teacherSectionDetail = {teacherSectionId:dwr.util.getValue("teacherSectionId")};

	var newJobId = document.getElementById("newJobId").value;
	var epiJobId = document.getElementById("epiJobId").value;
	$('#sectionName').show();
	$("#sbmtbtn").attr("disabled", "disabled");
	AssessmentCampaignAjax.getAssessmentSectionCampaignQuestions(assessmentDetail,teacherAssessmentdetail,teacherAssessmentQuestion,teacherSectionDetail,arr,attemptId,newJobId,epiJobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){

		window.clearInterval(intervalID);

		arr =[];
		$("#sbmtbtn").attr("disabled", false);
		$("body").scrollTop(0);
		try{$('#tblGrid').html(data);
		}catch(e)
		{}
	}
	});	
}
catch(e)
{alert("e====="+e)}
}

function checkStrikes(questionSessionTime,whoClicked)
{	
	if(questionSessionTime==0)
	{
		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='images/info.png' align='top'>");
		$('#message2showConfirm1').html(resourceJSON.msgAllQMand);
		$('#nextMsg').html("");
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
		$('#vcloseBtn').html("<span>x</span>");
		$('#myModalv').modal('show');
	}else
	{
		if(whoClicked==1)
		{
			
			//$("#myModalv").css({ top: '60%' });@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		
			$('#warningImg1').html("<img src='images/info.png' align='top'>");
		
			$('#message2showConfirm1').html(resourceJSON.msgEveryAnsb4zero);		
			$('#nextMsg').html("");			
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");	
			$('#vcloseBtn').html("<span>x</span>");					
			$('#myModalv').modal('show');
						
				
					
			var remSec = parseInt($('.min').html()==null?0:$('.min').html())*60 +parseInt($('.sec').html());
			if(remSec>0)
			{
				return;
			}
			

		}else
		{
			strikeCheck(document.getElementById("teacherAssessmentQuestionId").value,whoClicked);
		}
	}
}
function checkUniqueRank(dis)
{
	if(isNaN(dis.value))
	{
		dis.value="";
		dis.focus();
		return;
	}
	var arr =[];
	var ranks = document.getElementsByName("rank");
	for(i=0;i<ranks.length;i++)
	{
		if(ranks[i].value!="" && ranks[i]!=dis)
		{
			arr.push(ranks[i].value);
		}
	}
	var o_rank = document.getElementsByName("o_rank");

	var arr2 =[];
	for(i=0;i<o_rank.length;i++)
		arr2.push(o_rank[i].value);


	if(dis.value!="")
		if($.inArray(dis.value, arr2)==-1)
		{
			dis.value="";
			$('#warningImg1').html("<img src='images/info.png' align='top'>");
			$('#message2showConfirm1').html(resourceJSON.msgValidRank);
			$('#nextMsg').html("");
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#myModalv').modal('show');
			//$("#myModalv").css({ top: '45%' });
			$('#vcloseBtn').html("<span>x</span>");
			
			dis.focus();
			return false;
		}


	$.each(arr, function(j, el){
		if(parseInt(el)==parseInt(dis.value))
		{
			dis.value="";
			$('#warningImg1').html("<img src='images/info.png' align='top'>");
			$('#message2showConfirm1').html(resourceJSON.msgUniqueRank);
			$('#nextMsg').html("");
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#myModalv').modal('show');
			//$("#myModalv").css({ top: '45%' });
			$('#vcloseBtn').html("<span>x</span>");
			dis.focus();

			return false;
		}
	});
}
function redirectToDashboard(teacherAssessmentType,attempt)
{
	window.onbeforeunload = function() {};	
	var attemptId = dwr.util.getValue("attemptId");
	
	AssessmentCampaignAjax.setAssessmentForced(attemptId,{ 
		async: false,
		callback: function(data){
		if(epiStandalone)
			window.location.href="epiboard.do?w="+teacherAssessmentType+"&attempt="+attempt;
		else
			window.location.href=rdURL+"?w="+teacherAssessmentType+"&attempt="+attempt;
	},
	errorHandler:handleError  
	});	
}
function stopTimer()
{
	clearTimeout(timeout);
}
function strikeCheck(teacherAssessmentQuestionId,whoClicked)
{
	var teacherAssessmentType = dwr.util.getValue("teacherAssessmentType");
	var moreMsg = ".";
	var inventory = "";
	if(teacherAssessmentType==1){
		moreMsg = ".";
		inventory = "EPI";
	}
	else if(teacherAssessmentType==2){
		inventory = "JSI"
	}
		
	$('#tm-root').html("");
	var questionSessionTime = 0;
	if(document.getElementById("questionSessionTime"))
		questionSessionTime = dwr.util.getValue("questionSessionTime")==null?0:dwr.util.getValue("questionSessionTime");

	var totalChances = parseInt(totalAttempted) + totalStrikes;
	if(totalChances==1  && (whoClicked!=2))
	{
		var onclick = "";
		onclick = "onclick=\"stopTimer(),tmTimer("+questionSessionTime+",'1')\"";
		
		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='images/firstVlt.png' align='top'>");
		$('#message2showConfirm1').html(resourceJSON.msgTimedOutthestipulatedtime);
		$('#nextMsg').html( resourceJSON.msgreadytoStrt1+" <font color='red'>"+resourceJSON.msgSECOND+"</font> "+resourceJSON.msgreadytoStrt2+" "+inventory+resourceJSON.msgreadytoStrt3 + moreMsg);
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' "+onclick+" aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
		$('#vcloseBtn').html("<span "+onclick+" >x</span>");
		$('#myModalv').modal('show');
		if(teacherAssessmentType==1 || teacherAssessmentType==2){
			sendMailToCandidate(teacherAssessmentType,'vlt1');
		}
		if(questionSessionTime>0 && whoClicked==0)
		{
			window.clearInterval(intervalID);
			saveStrike(teacherAssessmentQuestionId);
			timeout =  setTimeout('redirectToDashboard('+teacherAssessmentType+',1)', 30000);
		}
		return;
	}
	if(totalChances==2  && (whoClicked!=2))
	{
		var onclick = "";
		onclick = "onclick=\"stopTimer(),tmTimer("+questionSessionTime+",'1')\"";
		
		//$("#myModalv").css({ top: '60%' });
		$('#warningImg1').html("<img src='images/secondVlt.png' align='top'>");
		$('#message2showConfirm1').html(resourceJSON.msgTimedOutthestipulatedtime2);
		$('#nextMsg').html( resourceJSON.msgreadytoStrt1+" <font color='red'>"+resourceJSON.msgTHIRD+"</font> "+resourceJSON.msgreadytoStrt4+resourceJSON.msgreadytoStrt3 +moreMsg);
		$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' "+onclick+" aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
		$('#vcloseBtn').html("<span "+onclick+" >x</span>");
		$('#myModalv').modal('show');
		if(teacherAssessmentType==1 || teacherAssessmentType==2){
			sendMailToCandidate(teacherAssessmentType,'vlt2');
		}
		if(questionSessionTime>0 && whoClicked==0)
		{
			window.clearInterval(intervalID);
			saveStrike(teacherAssessmentQuestionId);
			timeout = setTimeout('redirectToDashboard('+teacherAssessmentType+',2)', 30000);

		}
		return;
	}else if((totalAttempted>=1 && totalStrikes>=1) || (totalAttempted>=2 && totalStrikes==0)) //if(totalChances>=3)
	{
	
		if(whoClicked==0)
		{
			window.clearInterval(intervalID);
		}

		finishAssessment(0);
		return;
	}

}
function redirectToURL(redirectURL)
{
	var url = redirectURL;
	window.location.href=url;
}
function redirectTo()
{
	if(document.getElementById('epiStandAlone') && $('#epiStandAlone').val()==false || $('#epiStandAlone').val()=="false")
	{
		//$(location).attr('href','epiboard.do');
		//window.location.href='epiboard.do';
		
		if(document.getElementById('rURL'))
		{
			var url = $('#rURL').val();
			window.location.href=url;
		}else
		{
			window.location.href='portalboard.do';
		}
		
		//window.open(url,"_tab","TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
	}else
	{
		//window.location.href='userdashboard.do';
		if(document.getElementById('rURL'))
		{
			var url = $('#rURL').val();
			window.location.href=url;
		}else
		{
			window.location.href=rdURL;
		}
		
		//window.open(url,"_tab","TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
	}
}
var isPortal = false;
function showInfoAndRedirect(sts,msg,nextmsg,url,epiFlag)
{
	//$("#myModalv").css({ top: '60%' });
	var img="info";
	if(sts==1)
		img = "warn";
	else if(sts==2)
		img = "stop";

	if(epiFlag==1 && msg=="thirdVlt")
	{
		msg = "<b>Timed Out &#8212; FINAL ALERT!</b><p></p>Unfortunately you have not responded to questions within the stipulated <b>75 seconds</b> for the <b><font color='red'>THIRD</font></b> time. As explained previously, after the third alert, you are now <b><font color='red'>&#8220;Timed Out&#8221;</font></b> from the <b>EPI</b> &#8212; and are not allowed to continue with the EPI.<p></p>Your TeacherMatch EPI status will remain <b><font color='red'>&#8220;Timed Out&#8221;</font></b> for 12 months.<p></p>Please note that you will still be able to apply to any and all positions you are interested in, and schools are free to continue considering you for employment. <b>EPI</b> results are merely one of the many data points schools use to guide their hiring process.<p></p>If you would like to review the TeacherMatch policy regarding this issue in greater depth, please click <a href='termsofuse.do'>here</a>. And if you have any further questions regarding this matter, please call us at 855.980.0511 or email us at <a href=\"mailto:applicants@teachermatch.org\">applicants@teachermatch.org</a>.<p></p>Thank you for your time &#8212; and best of luck in your job search!";
		img = "thirdVlt";
	}
	$('#warningImg1').html("<img src='images/"+img+".png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html(nextmsg);
	
	var onclick = "";
	if(url!=""){
		if(epiFlag==1){
			onclick = "onclick='redirectTo()'";
		}else{
			//onclick = "onclick=checkPopup('"+url+"')";
			onclick = "onclick=redirectToPage('"+url+"')";
		}
	}
	
	
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">"+resourceJSON.btnOk+"</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
}
function redirectToPage(url)
{
	//alert(url);
	checkPopup(url);
	if(isPortal)
		window.location.href="portaldashboard.do";
	else
		window.location.href=rdURL;
}
function clearDialog(divId)
{
	$("#"+divId ).dialog( "close" );
	$("#"+divId ).hide();
}
function checkPopup(url) {
	  var openWin = window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	  if (!openWin) {
		  	window.location.href=url;
	    } else {
	    window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	   }
}
function saveStrike(teacherAssessmentQuestionId)
{
	if(teacherAssessmentQuestionId==0)
		return;

	var teacherAssessmentQuestion = {teacherAssessmentQuestionId:dwr.util.getValue("teacherAssessmentQuestionId")};
	var teacherAssessmentdetail = {teacherAssessmentId:dwr.util.getValue("teacherAssessmentId"),assessmentType:dwr.util.getValue("teacherAssessmentType")};
	AssessmentCampaignAjax.saveStrike(teacherAssessmentdetail,teacherAssessmentQuestion,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		totalStrikes++;
	}
	});	
}


function finishAssessment(sessionCheck)
{

	$('#myModalvk').modal('hide');
  	$('#myModalv').modal('hide');
  	
	var newJobId = document.getElementById("newJobId").value;
	var epiJobId = document.getElementById("epiJobId").value;

	AssessmentCampaignAjax.finishAssessment(teacherAssessmentdetail,jobOrder,newJobId,sessionCheck,epiJobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#tm-root').html(data);
	}
	});	
}

function checkAssessmentDone(teacherAssessmentId)
{
	var teacherAssessmentdetail = {teacherAssessmentId:teacherAssessmentId}
	AssessmentCampaignAjax.checkAssessmentDone(teacherAssessmentdetail,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data==0)
			getAssessmentSectionCampaignQuestionsGrid(0);
		else
			finishAssessment(0)
	}
	});	
}
function portfolioURL(){
	var portfolioAction= document.getElementById("portfolioAction").value; 
	window.location.href=portfolioAction;
}

function checkJSI(jobId){		
	
	var jobOrder={jobId:jobId};
	AssessmentCampaignAjax.CheckJSI(jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){		
		var remodel="Base Inventory";
		if(jobId>0)
			remodel=resourceJSON.msgJobSpecificInventory;

		if(data.assessmentName==null && data.status=='3')
		{
			jOrder=jobOrder;
			vData=data;		
			getJSIDistrictQuestion(jobId);
			/*$('#message2show').html("The "+remodel+" has not been finalized. Please come back later to complete this application.");
			$('#myModal2').modal('show');	*/		
			
		}	
		
	}
	});	
}

var totalJSIQuestions=0;
function getJSIDistrictQuestion(jobId)
{
	$('#errordiv4JSIquestion').empty();	
	AssessmentCampaignAjax.getJSIDistrictSpecificQuestion(jobId,{
	async: true,
	errorHandler:handleError,
	callback: function(data)
	{		
		document.getElementById("JSIjobId").value="";
		var dataArray = data.split("@##@");	
		totalJSIQuestions = dataArray[1];
		if(dataArray[0]!=null && dataArray[0]!='' && totalJSIQuestions!=0)
		{
			try
			{								
				$('#tblJSIGrid').html(dataArray[0]);
				$('#myModalDASpecificQuestionsJSI').modal('show');
				document.getElementById("JSIjobId").value=jobId;
				
			}catch(err)
			{}
		}
		else
		{	
		}

	}});	
	
}

function setJSIDistrictQuestions()
{
	
	$('#errordiv4JSIquestion').empty();
	var arr =[];
	var jobOrder = {jobId:document.getElementById("JSIjobId").value};
	for(i=1;i<=totalJSIQuestions;i++)
	{	
		var teacherassessmentquestions = {teacherAssessmentQuestionId:dwr.util.getValue("Q"+i+"questionId")};		
		var questionTypeMaster = {questionTypeId:dwr.util.getValue("Q"+i+"questionTypeId"),questionTypeShortName:dwr.util.getValue("Q"+i+"questionTypeShortName")};
		var qType = dwr.util.getValue("Q"+i+"questionTypeShortName");		
		if(qType=='tf' || qType=='slsel')
		{			
			var optId="";
			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 )
			{
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			}else
			{
				$("#errordiv4QQquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				return;
			}
			
			arr.push({ 
				"selectedOptions"  : optId,				
				"questionTypeMaster" : questionTypeMaster,				
				"teacherAssessmentQuestion" : teacherassessmentquestions, 	
				"jobOrder" : jobOrder,
			});

		}else if(qType=='ml')
		{
			var insertedText = dwr.util.getValue("Q"+i+"opt");
			if(insertedText!=null && insertedText!="")
			{				
				arr.push({ 
					"insertedText"    : insertedText,					
					"questionTypeMaster" : questionTypeMaster,					
					"teacherAssessmentQuestion" : teacherassessmentquestions,
					"jobOrder" : jobOrder
				});
			}else
			{
				$("#errordiv4JSIquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				return;
			}
		}else if(qType=='et')
		{
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 )
			{
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			}else
			{
				$("#errordiv4JSIquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				return;
			}
			
			var insertedText = dwr.util.getValue("Q"+i+"optet");			
				
			arr.push({ 
				"selectedOptions"  : optId,			
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,				
				"teacherAssessmentQuestion" : teacherassessmentquestions,				
				"jobOrder" : jobOrder,
			});
		}
	}
	if(arr.length==totalJSIQuestions)
	{
		AssessmentCampaignAjax.setJSIDistrictQuestions(arr,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{				
				arr =[];
				$('#myModalDASpecificQuestionsJSI').modal('hide');				
				$('#SuccessMessageforJSI').modal('show');
			}

		}
		});	
	}else
	{
		$("#errordiv4JSIquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
		return;
	}

}

function sendMailToCandidate(assessmentType,status)
{
	var epiJobId = document.getElementById("epiJobId").value;
	var jobId = jJobId;
//	alert('assessmentType : '+assessmentType+' : status : '+status+' jobId : '+jobId+' epiJobId : '+epiJobId);
	AssessmentCampaignAjax.sendMailToCandidate(assessmentType,status,jobId,epiJobId,{
		async: true,
		errorHandler:handleError,
		callback: function(data){
			
		}
	});
}