var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{	
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
	noOfRows = document.getElementById("pageSize").value;
	displayGeoZoneGrid();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayGeoZoneGrid();
}

function addnew(){
	$("#geozonediv").show();
	$("#districtName").val("");
	$("#attachedListSchool option").remove();
	$('#ZNDescription').find(".jqte_editor").html("");
	$("#geozonename").val("");
	$("#geozonecode").val("");
	$("#geozoneId").val("");
	$("#districtName").focus();
	
	$("#geozonename").css("background-color", "");
	$("#geozonecode").css("background-color", "");
	$("#districtName").css("background-color", "");
	$("#attachedListSchool").css("background-color", "");
	if($("#entityType").val()==2){
		getSchoolByDistrict();
		$("#geozonename").focus();
	}
}

function cancel(){
	$('#errordiv').empty();
	$('#errordiv').hide();
	$("#geozonediv").hide();
}

function getSchoolByDistrict()
{
		var districtId=$("#districtId").val();	
		GeoZoneAjax.getSchoolByDistrict(districtId,{ 
			async: false,
			callback: function(data){
			$("#avlbListSchool options").remove();
			$("#attachedListSchool options").remove();
			$("#avlbListSchool").html(data);
		},
		errorHandler:handleError 
		});
}

function chkForEnterSearchZone(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		displayGeoZoneGrid()
	}	
}
function searchZoneList()
{
	if($("#districtNameFilter").val()==""){
		$("#districtMasterId").val(0);
	}
	displayGeoZoneGrid();
}
function displayGeoZoneGrid()
{
	var searchTextId=0;
	var entityType	=	document.getElementById("entityType").value;
		searchTextId=document.getElementById('districtIdFilter').value;
if(searchTextId=="" || searchTextId==undefined){
	if($("#districtMasterId").val()!=undefined){
		if($("#districtMasterId").val()!=0){
			searchTextId=$("#districtMasterId").val();
		}else{
			searchTextId=0;
		}
	}else{
		searchTextId=0;
	}
}
	GeoZoneAjax.displayGeoZoneGrid(searchTextId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: false,
		callback: function(data){
		cancel();
		$("#geozoneGrid").html(data);
		applyScrollOnTbl();
	},
	errorHandler:handleError 
	});	
	hideSearchAgainMaster();	
}

function saveZoneAndSchool()
{	
	var geozonename=$.trim($("#geozonename").val());
	var geozonecode=$.trim($("#geozonecode").val());
	var description	=$.trim($('#ZNDescription').find(".jqte_editor").text());
	var attachedSchoolOpts=document.getElementById('attachedListSchool').options;
	var noOfSchoolsInGeoZone=attachedSchoolOpts.length;
	var districtId=$("#districtId").val();
	var schoolIdStr="";
	for(var x=0;x<attachedSchoolOpts.length;x++)
	{
		schoolIdStr+=attachedSchoolOpts[x].value+"#";
	}
	schoolIdStr=schoolIdStr.substring(0, schoolIdStr.length-1);
	var geozoneId=$("#geozoneId").val();
	if(geozoneId=="")
	{
		geozoneId=0;
	}
	
	var counter=0;
	var focuscount=0;
	
	$('#districtName').css("background-color", "");
	$("#geozonename").css("background-color", "");
	$("#geozonecode").css("background-color", "");
	$("#districtName").css("background-color", "");
	$("#attachedListSchool").css("background-color", "");
	
	$('#errordiv').empty();
	if(districtId==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br/>");
		$('#districtName').css("background-color", "#F5E7E1");
		if(focuscount	==	0)
			$('#districtName').focus();
		counter++;
		focuscount++;
	}
	if(geozonename==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrGeoZoneDisplayName+"<br/>");
		$("#geozonename").css("background-color", "#F5E7E1");
		if(focuscount	==	0)
			$("#geozonename").focus();
		counter++;
		focuscount++;
	}
	if(geozonecode==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrGeoCode+"<br/>");
		$("#geozonecode").css("background-color", "#F5E7E1");
		if(focuscount	==	0)
			$("#geozonecode").focus();
		counter++;
		focuscount++;
	}
	if(noOfSchoolsInGeoZone==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.PlzSlctSchools+"<br/>");
		$("#attachedListSchool").css("background-color", "#F5E7E1");
		if(focuscount	==	0)
			$("#attachedListSchool").focus();
		counter++;
		focuscount++;
	}
	if(counter==0)
	{	$('#loadingDiv').show();
		GeoZoneAjax.saveZoneAndSchool(geozoneId,geozonename,geozonecode,description,noOfSchoolsInGeoZone,districtId,schoolIdStr,{ 
			async: true,
			callback: function(data){
			$('#loadingDiv').hide();
				if(data==1){
					$('#errordiv').show();	
					$('#errordiv').append("&#149; "+resourceJSON.PlzEtrUniqueGeoZone+"<br/>");
					$("#geozonename").css("background-color", "#F5E7E1");
					$("#geozonename").focus();
				}else if(data==2){
					$('#errordiv').show();	
					$('#errordiv').append("&#149; "+resourceJSON.PlzEtrUniqueGeoZoneCode+"<br/>");
					$("#geozonecode").css("background-color", "#F5E7E1");
					$("#geozonecode").focus();
				}else
				{	
					$('#errordiv').empty();
					$('#errordiv').show();
					cancel();
					displayGeoZoneGrid();
					getSchoolByDistrict();
				}
		},
		errorHandler:handleError 
		});	
	}
}
function editGeoZone(geozoneId)
{
	GeoZoneAjax.getZoneById(geozoneId,{ 
		async: false,
		callback: function(data){
		addnew();
		$("#geozoneId").val(geozoneId);
		$("#districtName").val(data.districtMaster.districtName);
		$("#districtId").val(data.districtMaster.districtId);
		$("#districtName").focus();
		getSchoolByDistrict();
		$("#geozonename").val(data.geoZoneName);
		$("#geozonecode").val(data.geoZoneCode);
		$('#ZNDescription').find(".jqte_editor").html(data.geoZoneDescription);
		for(var i=0;i<data.schoolIds.length;i++)
		{
			$("#attachedListSchool").append("<option value='"+data.schoolIds[i]+"'>"+data.schoolNames[i]+"</option>");
		}
	},
	errorHandler:handleError 
	});	 	
}
function showDeleteModel(geozoneId)
{
	$("#geozoneId").val(geozoneId);
	$('#deleteGeoZone').modal('show');
}
function deleteGeoZone()
{	
	var geozoneId=$("#geozoneId").val();
	GeoZoneAjax.deleteZoneById(geozoneId,{ 
		async: false,
		callback: function(data){
			$('#deleteGeoZone').modal('hide');
			displayGeoZoneGrid();
	},
	errorHandler:handleError 
	});	 	
}
/**************************************  District auyo list ****************************************************/
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	GeoZoneAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
			
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
			if(document.getElementById(hiddenId))
				document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}

function setEntityType()
{
	var entityString="";
	$("#entityTypeDivMaster").hide();
	$("#districtClassMaster").show();
	var district="<div style='margin-left:65px;'><label>District</label>"+
					    	"<input autocomplete='off' type='text' id='districtNameFilter' name='districtNameFilter' class='form-control'"+ 
		             		"onfocus=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\""+
							"onkeyup=\"getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');\""+
							"onblur=\"hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');\"/>"+
							"<div id='divTxtShowDataFilter'  onmouseover=\"mouseOverChk('divTxtShowDataFilter','districtNameFilter');\""+
							 "style='display:none;position:absolute;z-index: 999999' class='result' ></div></div>";
	var btnRecord="<button class='btn btn-primary' type='button' onclick='searchZoneList()'>Search <i class='icon'></i></button>";
	globalSearchSection(entityString,"","",district,btnRecord);
	setTimeout(function(){ 
		if($("#districtMasterId").val()!=0){
			$("#districtIdFilter").val($("#districtMasterId").val());
			$("#districtNameFilter").val($("#districtMasterName").val());
			hideSearchAgainMaster();
		}else{
			showSearchAgainMaster();
		}
	}, 100);
}