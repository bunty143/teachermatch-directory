/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
/*========  For Sorting  ===============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayDomain();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert( resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*======= Save Domain on Press Enter Key ========= */
function chkForEnterSaveDomain(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveDomain();
	}	
}
/*========  displayDomain ===============*/
function displayDomain()
{
	DomainAjax.displayDomainRecords(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		$('#domainGrid').html(data);
		applyScrollOnTbl();
	},
	errorHandler:handleError  
	});
}
/*========  Add New Domain===============*/
function addNewDomain()
{
	dwr.util.setValues({ domainId:null,domainName:null,multiplier:null,domainUId:null});
	document.getElementById("divDomanForm").style.display	=	"block";
	$('#domainName').focus();
	$('#errordiv').hide();
	$('#domainName').css("background-color", "");
	$('#domainUId').css("background-color", "");
	$('#domainUId').val('');
	$('#domainUId').prop('disabled', false);
	$('#displayInPDReport').attr('checked', false); // will uncheck the checkbox with id displayInPDReport
	document.getElementById("divDone").style.display		=	"block";
	document.getElementById("divManage").style.display		=	"none";
	return false;
}
/*========  Clear Domain Fields ===============*/
function clearDomain()
{
	document.getElementById("divDomanForm").style.display	=	"none";
	document.getElementById("divDone").style.display		=	"none";
	document.getElementById("divManage").style.display		=	"none";
	document.getElementById("domainName").value				=	"";
	document.getElementsByName("domainStatus")[0].checked	=	true;
	$('#domainUId').val('');
	$('#domainUId').prop('disabled', false);
	$('#domainUId').css("background-color", "");
}
/*========  Save Domain  ===============*/
function saveDomain()
{
	$('#errordiv').empty();
	$('#domainName').css("background-color", "");
	$('#domainUId').css("background-color", "");
	var domainId			=	trim(document.getElementById("domainId").value);
	var domainUId			=	trim(document.getElementById("domainUId").value);
	var multiplier			=	trim(document.getElementById("multiplier").value);
	var domainName			=	trim(document.getElementById("domainName").value);
	var displayInPDReport	=	document.getElementById("displayInPDReport");
	var domainStatus		=	document.getElementsByName("domainStatus");
	var cnt=0;	
	
	for(i=0;i<domainStatus.length;i++)
	{
		if(domainStatus[i].checked	==	true)
		{
			domainValue	=	domainStatus[i].value;
			break;
		}
	}
	if (domainName=="")
	{
		$('#errordiv').append('&#149; '+resourceJSON.msgEnterDomainName+"<br>");
		$('#domainName').css("background-color", "#F5E7E1");
		if(cnt==0)
			document.getElementById("domainName").focus();
		cnt++;
	}
	if (domainUId=="")
	{
		$('#errordiv').append('&#149; '+resourceJSON.msgEnterDomainUId);
		$('#domainUId').css("background-color", "#F5E7E1");
		if(cnt==0)
			document.getElementById("domainUId").focus();
		cnt++;
	}
	if(displayInPDReport.checked	==	true)
		PDReportValue	=	displayInPDReport.value;
	else
		PDReportValue	=	0;
	
	if(multiplier=="")
	{
		multiplier=0;
	}
	if(cnt==0){
		
	}else{
		$('#errordiv').show();
		return false;
	}

	dwr.engine.beginBatch();		
	DomainAjax.saveDomain(domainId,domainName,PDReportValue,domainValue,multiplier,domainUId,{ 
		async: true,
		callback: function(data)
		{
			if(data==3){
				document.getElementById('errordiv').innerHTML	=	'&#149; '+resourceJSON.msgUniqueDomainName;
				$('#errordiv').show();
				$('#domainName').css("background-color", "#F5E7E1");
				$('#domainName').focus();
			}
			else if(data==4){
				document.getElementById('errordiv').innerHTML	=	'&#149; '+resourceJSON.msgUniqueDomainUId;
				$('#errordiv').show();
				$('#domainUId').css("background-color", "#F5E7E1");
				$('#domainUId').focus();
			}
			else{
				displayDomain();
				clearDomain();
			}
		},
		errorHandler:handleError 
	});
	dwr.engine.endBatch();
}
/*========  activateDeactivateDomain ===============*/
function activateDeactivateDomain(domainId,status)
{
	DomainAjax.activateDeactivateDomain(domainId,status, { 
		async: true,
		callback: function(data)
		{
			displayDomain();
			clearDomain();
		},
		errorHandler:handleError 
	});
				
}
/*========  Edit Domain ===============*/
function editDomain(domainId)
{
	$('#errordiv').hide();
	$('#domainName').css("background-color", "");
	$('#domainUId').css("background-color", "");
	var displayInPDReport									=	document.getElementById("displayInPDReport");
	document.getElementById("divDone").style.display		=	"none";
	document.getElementById("divDomanForm").style.display	=	"block";
	var domainStatus	=	document.getElementsByName("domainStatus");
	
	$('#domainName').focus();
		DomainAjax.getDomainById(domainId, { 
			async: true,
			callback: function(data)
			{
				dwr.util.setValues(data);
				if(data.displayInPDReport	==	1)
					$('#displayInPDReport').attr('checked', true); // will check the checkbox with id displayInPDReport
				else
					$('#displayInPDReport').attr('checked', false); // will uncheck the checkbox with id displayInPDReport
				if(data.status=='A')
					domainStatus[0].checked	=	true;
				else
					domainStatus[1].checked	=	true;
				$('#domainUId').val(data.domainUId);
				$('#domainUId').prop('disabled', true);
			},
		errorHandler:handleError 
		});
	
	document.getElementById("divManage").style.display="block";
	return false;
}
function isAlphabetsKey(evt)
{
	var charCode = (evt.which) ? evt.which : event.keyCode
	if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
		return true;
	else
		return false;
}

/* @Author: Gagan 
 * @Discription: view of edit domain js.
*/
