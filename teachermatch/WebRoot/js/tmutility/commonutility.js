function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function getJobs()
{
	//$('#loadingDiv').show();
	CommonUtilityAjax.getJobs(null,null,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			try{
			//$('#loadingDiv').hide();	
			$('#myModalJobListCommon').modal('show');
			document.getElementById("divJobCommon").innerHTML=data;
			}catch(err){}
		} 
	});
}