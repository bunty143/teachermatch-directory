var pageONB 		= 	1;
var noOfRowsONB 	= 	500;
var sortOrderStrONB	=	"tLastName";
var sortOrderTypeONB=	"";

var pageForTC = 1;
var sortOrderStrForTC="";
var sortOrderTypeForTC="";
var noOfRowsForJob = 50;


var pageSNote = 1;
var noOfRowsSNote=10;
var sortOrderStrSNote="";
var sortOrderTypeSNote = 10;

var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);


function displayKellyONBGrid(actionFlag,pagenationFlag) {
	//alert("actionFlag:"+actionFlag);	
	if(pagenationFlag==1){
		pageONB 		= 	1;		
	}
	document.getElementById("tpMenuActionDiv").style.display="none";
	var ksnId=false;
	var onb=false;
	var hired=false;
	var tTIE=false;
	var Withdrawn=false;
	var hidden=false;
	var actionSearch=0;
	var TeacherIds = new Array();
	try{
		ksn		=	$("#ksn").val().trim();
		fname	=	$("#firstName").val();
		lname	=	$("#lastName").val();
		email   =	$("#email").val();
		jobCategoryId = $("#jobCategoryId").val();
		jobId   =	    $("#jobId").val().trim();
		branchSearchId =$("#branchSearchId").val();
		if(document.getElementById('ksnId').checked==true){
			ksnId=true;
		}
		if(document.getElementById('hired').checked==true){
			hired=true;
		}
		if(document.getElementById('tTIE').checked==true){
			tTIE=true;		
		}
		if(document.getElementById('Withdrawn').checked==true){
			Withdrawn=true;
		}
		if(document.getElementById('hidden').checked==true){
			hidden=true;
		}
		
		
		if($("input[name=actionSearchId]:radio:checked").length > 0 && (ksnId || hired || tTIE || Withdrawn || hidden))
		{
			actionSearch = $("input[name=actionSearchId]:radio:checked").val();
		}
		
		if(ksnId)
		{
			showSubFIlterDiv();
		}
		$('#loadingDivHZS').show();
		TeacherIds	=	getTeacherIdAndJobId();
	}
	catch(e){
		
	}
	try{
		KellyONRAjax.displayKellyONBGridNEW(ksn,fname,lname,email,jobCategoryId,noOfRowsONB,pageONB,sortOrderStrONB,sortOrderTypeONB,ksnId,onb,hired,tTIE,Withdrawn,hidden,actionFlag,TeacherIds,actionSearch,jobId,branchSearchId,{
		async: true,
		callback: function(data)
		{
			if(actionFlag==0)
			{
		      $('#kellyONBGrid').html(data);
		      $('#exportDiv').show();	
		      applyScrollOnTblKellyOnb();
		      showTooltip();
		    }else if(actionFlag==1 || actionFlag==2){
				try
					{
				    document.getElementById('ifrmTrans').src = "Onboarding/"+data+"";		
				    //uncheckedAllCHKMassTP();
					}
					catch(e)
					{alert(e);}
			}else if(actionFlag==3){
				document.getElementById('ifrmCJS').src = ""+data+"";
				try{
					$('#modalDownloadJobOrder').modal('show');
				}catch(err){}
			}else if(actionFlag==4){
				try{
					if(isSafari && !deviceType)
				    {
				    	window.document.write(data);
						window.print();
				    }else{
						var newWindow = window.open();
				    	newWindow.document.write(data);	
				    	newWindow.print();
				    }
				}catch(err){}
			}
			
		  $('#loadingDivHZS').hide();
	},
		errorHandler:handleError 		
	});
		
	}catch(e){alert(e);}
}
function actionMenuTP(){
	var inputs = document.getElementsByTagName("input"); 
	var chkCount=0;
	var chkCheckedCount=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	chkCount++;
	            if(inputs[i].checked){
	            	chkCheckedCount++;
	            }
	        }
	 }
	 
	 if(chkCheckedCount>0){
		 document.getElementById("tpMenuActionDiv").style.display="inline";
	 }else{
		 document.getElementById("tpMenuActionDiv").style.display="none";
	 }
}
function uncheckedAllCHKMassTP()
{
	 var inputs = document.getElementsByName("chkMassTP"); 
	 for (var i = 0; i < inputs.length; i++) 
	 {
	        if (inputs[i].type === 'checkbox') 
	        {
	        	inputs[i].checked=false;
	        }
	 }
	 document.getElementById("select_all_candidate").checked=false;
	 document.getElementById("tpMenuActionDiv").style.display="none";
}
function getTeacherIdAndJobId()
{
	var TeacherIds = new Array();
	var inputs = document.getElementsByName("chkMassTP"); 	
	var countTID=0;
	for (var i = 0; i < inputs.length; i++) 
	{
		if (inputs[i].type === 'checkbox') 
		{
			if(inputs[i].checked==true)
			{
				TeacherIds[countTID]=inputs[i].value;
				countTID=countTID+1;
	        }
	    }
	} 
	return TeacherIds;
}
function selectAllCandidate(){
	 var flag = document.getElementById("select_all_candidate").checked;
	 if(flag==true)
	 { 
		$('input[name="chkMassTP"]').each(function() {
		$(this).prop('checked', true);
		 document.getElementById("tpMenuActionDiv").style.display="inline";
		});
	 }
	 else{
		 
		 $('input[name="chkMassTP"]').each(function() {
			 $(this).prop('checked', false);
			 document.getElementById("tpMenuActionDiv").style.display="none";
				});
	 }
}
function getJobByTeacherAndJob(teacherId,jobId,teacherFullName){
	defaultSet();	
	currentPageFlag='job';
	noOfRowsForJob=50;	
	$('#loadingDiv').show();
	//hideProfilePopover();
	//alert(teacherId+"---"+jobCategoryId+"---"+teacherFullName);
	var zoneIdd=$("#zoneMM").val();
	KellyONRAjax.getJobListByTeacherAndJobCategory(teacherId,jobId,noOfRowsForJob,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,true,{ 
		async: true,
		callback: function(data)
		{
			try
			{
				$('#loadingDiv').hide();			
				//$('#myModalJobList').show(); 
				$('#myModalJobList').modal('show');
				document.getElementById("divJob").innerHTML=data;							
				if(teacherFullName!='')
				{
					try
					{
						document.getElementById("jobDetailHeaderText").innerHTML="Jobs "+teacherFullName+" Has Applied For";
					}
					catch(err)
					{}
				}
				applyScrollOnJob();
				//jobEnable();
				
			}
			catch(err)
			{}
		},
		errorHandler:handleError 
	});
}
function setPageFlag(){
	//$('#myModalJobList').hide();
	$('#myModalJobList').modal('hide');
}
function setGridProfileVariable(currentPageFlagValue){
	currentPageFlag=currentPageFlagValue;
}
function defaultSet(){
	pageForTC = 1;
	noOfRowsForTC =100;
	noOfRowsForJob=50;
	sortOrderStrForTC="";
	sortOrderTypeForTC="";
	currentPageFlag="";
}
/*function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(currentPageFlag=="job"){
		if(pageno!=''){
			pageForTC=pageno;	
		}else{
			pageForTC=1;
		}
		sortOrderStrForTC	=	sortOrder;
		sortOrderTypeForTC	=	sortOrderTyp;
		
		var teacherId=document.getElementById("tId").value;
		var categId=document.getElementById("jcategId").value;
		var name=document.getElementById("fullName").value;
		//alert(teacherId+"--"+categId+"--"+name);
		getJobByTeacherAndJob(teacherId,categId,name);
	}
}*/

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
		if(pageno!=''){
			pageONB=pageno;	
		}else{
			pageONB=1;
		}
		sortOrderStrONB	=	sortOrder;
		sortOrderTypeONB	=	sortOrderTyp;
		displayKellyONBGrid(0,0);
}

function getPaging(page)
{
	if(page!='')
	{
		pageONB=page;	
	}
	else
	{
		pageONB=1;
	}
	noOfRowsONB = document.getElementById("pageSize").value;
	
	//if(noOfRowsONB>500)
		displayKellyONBGrid(0,0);
	
}
function sendMessageMassTalent()
{
	removeMessagesFileMassTP();
	var TeacherIds = new Array();
	TeacherIds	=	getTeacherIdAndJobId();
	try{
		$('#myModalMessageMassTalent').modal('show');
	}catch(err){}
	document.getElementById("teacherIdForMessageMassTP").value=TeacherIds;
	
	$('#messageSendMassTP').find(".jqte_editor").html("");
	document.getElementById("messageSubjectMassTP").value="";
	$('#messageSubjectMassTP').css("background-color", "");
	$('#messageSendMassTP').find(".jqte_editor").css("background-color", "");
	$('#messageSubjectMassTP').focus();
	$('#errordivMessageMassTP').empty();
	
	KellyONRAjax.getEmailAddressMassTalent(TeacherIds,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("emailDivMassTP").innerHTML=data;
		}
	});
}
function addMessageFileTypeMassTP()
{
	$('#fileMessagesMassTP').empty();
	$('#fileMessagesMassTP').html("<a href='javascript:void(0);' onclick='removeMessagesFileMassTP();'><img src='images/can-icon.png' title='remove'/></a> <input id='AttachFileMessagesMassTP' name='AttachFileMessagesMassTP' size='20' title='Choose a File' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+"");
}
function removeMessagesFileMassTP()
{
	$('#fileMessagesMassTP').empty();
	$('#fileMessagesMassTP').html("<a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'>"+resourceJSON.AttachFile+"</a>");
}
function callByServletMassTalent(sourcefileName,sourcefilePath)
{
	document.getElementById("sourcefileNameMassTP").value=sourcefileName;
	document.getElementById("sourcefilePathMassTP").value=sourcefilePath;
	
	validateMessageMassTalent(99);
}

function validateMessageMassTalent(source)
{
	var sourcefileNameMassTP="";
	var sourcefilePathMassTP="";
	if(source=="1")
	{
		document.getElementById("sourcefileNameMassTP").value="";
		document.getElementById("sourcefilePathMassTP").value="";
		$('#loadingDiv').show();
	}
	else if(source=="99")
	{
		sourcefileNameMassTP=document.getElementById("sourcefileNameMassTP").value;
		sourcefilePathMassTP=document.getElementById("sourcefilePathMassTP").value;
	}
	
	$('#messageSubjectMassTP').css("background-color", "");
	$('#messageSendMassTP').find(".jqte_editor").css("background-color", "");
	
	var messageSendMassTP = $('#messageSendMassTP').find(".jqte_editor").html();
	
	var messageSubject = document.getElementById("messageSubjectMassTP").value;
	$('#errordivMessageMassTP').empty();
	
	var messageFileName="";
	var fileMessage=null;
	try{
		fileMessage=document.getElementById("AttachFileMessagesMassTP").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
		if(trim(messageSubject)=="")
		{
			$('#errordivMessageMassTP').append("&#149; "+resourceJSON.PlzEtrSub+"<br>");
			if(focs==0)
				$('#messageSubjectMassTP').focus();
			$('#messageSubjectMassTP').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		if ($('#messageSendMassTP').find(".jqte_editor").text().trim()=="")
		{
			$('#errordivMessageMassTP').append("&#149; "+resourceJSON.PlzEtrMsg+"<br>");
			if(focs==0)
				$('#messageSendMassTP').find(".jqte_editor").focus();
			$('#messageSendMassTP').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		
		if(fileMessage=="" && cnt==0){
			$('#errordivMessageMassTP').show();
			$('#errordivMessageMassTP').append("&#149; "+resourceJSON.PlzUplMsg+"<br>");
			cnt++;
		}
		else if(fileMessage!="" && fileMessage!=null)
		{
			var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("AttachFileMessagesMassTP").files[0]!=undefined)
				{
					fileSize = document.getElementById("AttachFileMessagesMassTP").files[0].size;
				}
			}
			
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
			{
				$('#errordivMessageMassTP').show();
				$('#errordivMessageMassTP').append("&#149; "+resourceJSON.PlzSelectAcceptMsg+"<br>");
				cnt++;
			}
			else if(fileSize>=10485760)
			{
				$('#errordivMessageMassTP').show();
				$('#errordivMessageMassTP').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
				cnt++;
			}
		}
		
		if(cnt==0)
		{	
			try{
				//alert("fileMessage::::"+fileMessage+"---source:"+source)
				if(fileMessage!="" && fileMessage!=null && source=="1")
				{
					
					document.getElementById("frmMessageUploadMassTP").submit();
					
				}
				else
				{
					var filePath="";
					if(source=="99")
					{
						fileMessage=sourcefileNameMassTP;
						filePath=sourcefilePathMassTP;
					}
					
					var teacherIds = new Array();
					teacherIds=getTeacherIdAndJobId();
					KellyONRAjax.SendMessageToAllTalent(teacherIds,messageSubject,messageSendMassTP,fileMessage,filePath,
					{
						async: false,
						errorHandler:handleError,
						callback:function(data)
						{	
							sendMessageConfirmationMassTalent();
							uncheckedAllCHKMassTP();
							$('#loadingDiv').hide();
						}
					});
					
					return true;
				}
				
			}catch(err){}
		}
		else
		{
			$('#loadingDiv').hide();
		}
}
function sendMessageConfirmationMassTalent()
{	
	try{
		$('#myModalMessageMassTalent').modal('hide');
		$('#myModalMsgShow_SendMessageMassTalent').modal('show');
	}catch(err)
	  {}
}
function printResume()
{
	var teacherIds="";
	$('input[name="chkMassTP"]:checked').each(function() {
		
		if($(this).attr("value") !=null && ($(this).attr("value")!="") )
		{
			if(teacherIds=="")
				teacherIds = teacherIds + $(this).attr("value");
			else
				teacherIds = teacherIds + "," + $(this).attr("value");
		}
	});
	if(teacherIds!="")
	{
		$('#loadingDiv').show();
		KellyONRAjax.printResume(teacherIds,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#loadingDiv').hide();
				if(data=="")	
				{
					alert("Resume is not uploaded by the candidates.")
				}
				else
				{
					var messageAndPath = data.split("##");
					var message = messageAndPath[0];
					var path = messageAndPath[1];
										
					if(message=="")
						window.open(path, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
					else
						showConfirmationMessageForPrintResume(message, path);
				}
				uncheckedAllCHKMassTP();
			}
		});
	}
}

function showConfirmationMessageForPrintResume(message, path)
{
	$('#confirmationMessageForPrintResume').attr("path",path);
	$('#confirmationMessageForPrintResume .modal-body').html(message);
	$('#confirmationMessageForPrintResume').modal('show');
}

function showConfirmationMessageForPrintResumeOk()
{
	var path = $('#confirmationMessageForPrintResume').attr("path");
	if(path!="")
		window.open(path, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
}

function callMessageQueueAPI22(callerId,countTID)
{
	document.getElementById("msgCancelBtn").style.display="inline";
	var msg = "";
	var title = "";
	if(callerId==1){
		msg = "linked to KSN";
	}
	if(callerId==2 || callerId==3){
		msg = "Invited to eRegistration";
	}
	
	
	
	if(countTID>0){
		$('#myModalJobList786').modal('show');
		$('#divJob786').show();
		$('#divJob786').html(""+countTID+" record(s) will be "+msg+".  Would you like proceed?");
		if(callerId==2 || callerId==3){
			$('#sendKellyBtn').attr("onClick","callMessageQueueAPIINF('"+callerId+"')");
		}else{
			$('#sendKellyBtn').attr("onClick","callMessageQueueAPI('"+callerId+"')");
		}
	}
}
function callMessageQueueAPIINF(callerId){
	$('#myModalJobList786').modal('show');
	$('#divJob786').show();
	$('#divJob786').html("The Invitation to eRegistration has been initiated. Depending on the number of records, this request may take several minutes to process. Click the Search button to refresh and view the status updates.");
	document.getElementById("msgCancelBtn").style.display="none";
	$('#sendKellyBtn').attr("onClick","callMessageQueueAPI('"+callerId+"')");
}


function callMessageQueueAPI(callerId){	
	try{
		$('#myModalJobList786').modal('hide');
	}catch(e){}
	var TeacherIds = new Array();
	TeacherIds	=	getTeacherIdAndJobId();
	var msg='';
	var duplicateCheck = true;
	if($('#duplicateCheck').val()==0)
		duplicateCheck = false;
	try{
	KellyONRAjax.callMessageQueueAPI(TeacherIds,callerId,duplicateCheck,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
		            //alert("callerId:"+callerId+"---data:"+data);
		if(data!=null && data!=''){
			var eReg=0;
			if(data==1){
				if(TeacherIds.length==1)
					msg="This record is in the process of or has already been linked to KSN";
				else
					msg="One or more records is linked or is in the process of being linked to KSN";
			}else if(data==2){
				msg="One or more talent is already completed WF1 Registration";
			}else if(data==3){
				msg="One or more talent is not linked to KSN";
			}else if(data==4){
				msg="One or more talent is already completed WF2 Registration";
			}else if(data==5){
				msg="One or more talent is not completed WF1 Registration";
			}else if(data==6){
				msg="One or more talent is already completed WF3 Registration";
			}else if(data==7){
				msg="One or more talent is not completed WF2 Registration";
			}else if(data==8){
				msg="The Link to KSN has been initiated. Depending on the number of records, this request may take several minutes to process. Click the Search button to refresh and view the status updates.";
			}else if(data==11){
				msg="Talent(s) Linked to KSN Successfully";
			}else if(data==22){
				msg="Talent(s) Invite to eReg WF1 Successfully";
				eReg=1;
			}else if(data==33){
				msg="Talent(s) Invite to eReg WF2 Successfully";
				eReg=1;
			}else if(data==44){
				msg="Talent(s) Invite to eReg WF3 Successfully";
				eReg=1;
			}else{
				msg="Request Failed";
			}
			if(data!=null && data!=''){
			    if(eReg==0){
				     document.getElementById("messageshow").innerHTML=msg;
				     $('#myModalMsgShowAPIvalidation').modal('show');
			    }
			    $('#duplicateCheck').val(1);
			    uncheckedAllCHKMassTP();
			}
		}
		else if(duplicateCheck){
			$('#duplicateCheck').val(0);
			callMessageQueueAPI22(callerId,TeacherIds.length);
		}else{
			$('#duplicateCheck').val(1);
		}
	}
	});
	}catch(e){}
	
}

function closeLinkToKSNModal(){
	$("#modalLinkToKSN").modal("hide");
}

function callLinkToKSN(teacherId,jobForTeacherId,currentNode,statusNotes,secStatusId){
	try{
		$("#currentNode").val(currentNode);
		$("#teacherId").val(teacherId);
	}catch(e){}
	$('#loadingDiv').show();
	KellyONRAjax.callLinkToKSNFromOnboarding(teacherId,jobForTeacherId,currentNode,statusNotes,secStatusId,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					var hideFlag = data.split("####");					
					if(hideFlag[1]=='0'){						
						declinedKSNID(teacherId,'C');
						reflectLinkToKsnNodeColor($("#teacherId").val(),$("#currentNode").val());
					}else{
						$("#ksndetailsnew").show();
						$("#modalStatusNotes").modal("show");
						$("#ksndetailsnew").html(hideFlag[0]);
						applyScrollOnTableGrid("tblKSNdetails");
						getStatusNotes("notesGenDiv");
						$("#ksndetailsnew").show();
						$("#notesRow").hide();
						reflectLinkToKsnNodeColor($("#teacherId").val(),$("#currentNode").val());
						$('#loadingDiv').hide();
					}
				}
			});
}

var countKSN=0;
function varifyTchIdANDTmtlntID(teacherid , tmtalentID)
{
	if(tmtalentID!=null && tmtalentID!=0){
		if(teacherid!=tmtalentID){
			$('#modalStatusNotes').modal('hide');
			$('#confrmteacheridTmTlntId').modal('show');
			jQuery("#btnKSNAPIButtonId").click(function(){
				/*if(countKSN==0){
				countKSN=1;
				$('#loadingDiv').show();
				ManageStatusAjax.callAPIMethod(teacherid,{ 
			 		async: true,	
			 		errorHandler:handleError,
			 		callback:function(data)
			 		{

			 		}
			     });
			}*/
				$('#loadingDiv').hide();
				$('#confrmteacheridTmTlntId').hide();
				$('#modalStatusNotes').modal('show');

			});
			/*jQuery("#clnKSNId").click(function(){
			$('#loadingDiv').hide();
			$('#confrmteacheridTmTlntId').hide();	
			$('#modalLinkToKSN').modal('show');
		});*/
		}
	}
}		
function closeAssessmentResetDiv(){
	
	$("#confrmteacheridTmTlntId").modal('hide');
	$('#modalStatusNotes').modal('show');
}

function acceptedmethod(teacherId){
	var fitScore		=	"";
	var statusId		=	"";
	var secondaryStatusId=	"";
	try{
		$("#currentNode").val(resourceJSON.lblLinkToKsn);
		$("#teacherId").val(teacherId);
	}catch(e){}
	var talentKSNDatailID = document.getElementsByName('teacherStatus');
	var ksnID = "";
	$('#loadingDiv').show();
	for (var i=0; i<talentKSNDatailID.length;i++) {
	  if (talentKSNDatailID[i].checked) 
	  {
		  ksnID =talentKSNDatailID[i].value;
	  }
	}
	$('#modalStatusNotes').modal("hide");
	if(ksnID==''){
		$('#modalSelectKSNId').modal('show');
		$('#loadingDiv').hide();
		jQuery("#oktochoseKSNNO").click(function(){
			$('#modalSelectKSNId').modal('hide');
			$('#modalStatusNotes').modal("show");
			});
	}else{
		var flag = 0;
		ManageStatusAjax.changeKSNNumberOfTeacher(fitScore,statusId,secondaryStatusId,ksnID,teacherId,{ 
		 		async: true,	
		 		errorHandler:handleError,
		 		callback:function(data)
		 		{
				$('#loadingDiv').hide();
				$('#modalsuccessKSNId').modal('show');
				if(data!="SUCCESS"){
					$("#internalTxt1").html(data);
					flag =1;
					//$('#modalLinkToKSN').modal('show');
				}
				else{
					flag =0;
					$("#internalTxt1").html("Successfully Changed");
				}
				reflectLinkToKsnNodeColor($("#teacherId").val(),$("#currentNode").val());
				jQuery("#oktochoseKSNNOo").click(function(){
					$('#loadingDiv').hide();
					//if(flag==0)
						$('#modalStatusNotes').modal('hide');
					//else
						//$('#modalStatusNotes').modal('show');
					
				});
				}
		     });
	}
}

function declinedKSNID(tchID,flag){
	var fitScore		=	"";
	var statusId		=	"";
	var secondaryStatusId=	"";
	
	try{
		$("#currentNode").val(resourceJSON.lblLinkToKsn);
		$("#teacherId").val(tchID);
	}catch(e){}
	
	$('#modalLinkToKSN').modal('hide');
	$('#loadingDiv').show();
	if(tchID!=''){
		if(flag=='D'){
			$('#loadingDiv').hide();
			$('#modalStatusNotes').modal('hide');
			$('#confrmmodalKSNId').modal('show');
			jQuery("#btnKSNButtonId").click(function(){
				$('#loadingDiv').show();
				ManageStatusAjax.inactiveKSNNumberOfTalentKSMDetail(tchID,flag,{ 
			 		async: true,	
			 		errorHandler:handleError,
			 		callback:function(data)
			 		{
						$('#loadingDiv').hide();
						reflectLinkToKsnNodeColor(tchID,$("#currentNode").val());
			 		}
			     });
			});
			jQuery("#clnId").click(function(){
				$('#loadingDiv').hide();
				$('#modalStatusNotes').modal('show');
				});
		}else{
			ManageStatusAjax.inactiveKSNNumberOfTalentKSMDetail(tchID,flag,{ 
		 		async: true,	
		 		errorHandler:handleError,
		 		callback:function(data)
		 		{
				$('#loadingDiv').hide();
				$('#modalStatusNotes').modal('hide');
				reflectLinkToKsnNodeColor(tchID,$("#currentNode").val());
				$('#errContinuemsg').html(data);
				$('#modalErrContinue').modal('show');
		 		}
		     });
		}
	}
}

function closeContinue(){
	$('#modalErrContinue').modal('hide');
	$('#modalStatusNotes').modal('hide');
}
function hideUnhideAppBtn(hideUnhideflag){
	if(hideUnhideflag=="0"){
		document.getElementById("onlyDec").style.display="block";
		document.getElementById("appDec").style.display="none";
	}
	else if(hideUnhideflag=="1"){
		document.getElementById("onlyDec").style.display="none";
		document.getElementById("appDec").style.display="block";
		//document.getElementById("applyDecline").style.display="block";
	}
}
function applyCSS(className){
	$("."+className+"").css({"padding": "6px 10px 6px 10px","-webkit-border-top-left-radius": "30px","border-top-left-radius": "30px","-moz-border-radius-topleft": "30px","-webkit-border-top-right-radius": "30px","border-top-right-radius": "30px","-moz-border-radius-topright": "30px","-webkit-border-bottom-right-radius": "30px","border-bottom-right-radius": "30px","-moz-border-radius-bottomright": "30px","-webkit-border-bottom-left-radius": "30px","border-bottom-left-radius": "30px","-moz-border-radius-bottomright": "30px"});
	}

function reflectLinkToKsnNodeColor(teacherId,nodeVal){
	
	KellyONRAjax.getLatestNodeColor(teacherId,nodeVal,{ 
 		async: true,	
 		errorHandler:handleError,
 		callback:function(data)
 		{
		if(nodeVal==resourceJSON.lblLinkToKsn)
			$("."+teacherId+"").css({"background-color":data});
		else
			$("."+teacherId+""+nodeVal.split(" ")[1]).css({"background-color":data});
 		}
     });
}
function showSelectedDataForPrint()
{
	$('#errForPrint').empty();
	$('#errForPrint').hide();
	$('#selectedprintData').modal('show');
      var elements = document.getElementsByName("chkportfolioItem");
		 for (i=0;i<elements.length;i++) 
		 {
			elements[i].checked = true;
			elements[i].disabled=true;
		 }
}
function printMultipleData()
{
	$('#errForPrint').empty();
	var savecandidatearray="";
	var chkForPrint = 0 ;
	var inputs = document.getElementsByName("chkMassTP"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	        		savecandidatearray+=	inputs[i].value.split("-")[1]+",";
	            }
	        }
	 } 
	 document.getElementById("jftIds").value=savecandidatearray;
	//alert("array ="+savecandidatearray);
	 var chkcl=false;
	 var chkqq=false;
	 var chkjsi=false;
	 var chkportfolio=false;
	 var chkforPortfolios=0;
	 
	 var chkacademics=false;
	 var chkcertification=false;
	 var chkreferences=false;
	 var chkexperience=false;
	 var chkinvolvement=false;
	 
	 var pointscheck = new Array();
	
	 var cqpinputs = document.getElementsByName("cqp"); 	
	 for (var i = 0; i < cqpinputs.length; i++) {
	        if (cqpinputs[i].type === 'checkbox') {
	        	if(cqpinputs[i].checked){		        		
	        		
	        		 if(cqpinputs[i].value=="chkqq")
	        			 chkqq=true;
	        		 
	        		 if(cqpinputs[i].value=="chkjsi")
	        			 chkjsi=true;
	        		
	        		 if(cqpinputs[i].value=="chkportfolio")
	        		 {
	        			chkportfolio=true;
        				if (document.getElementById("pinfo").checked==true) 
        				{
	        				chkforPortfolios=chkforPortfolios+1
	        				pointscheck.push("pinfo");
        				}
        				if (document.getElementById("resume").checked==true) 
        				{
	        				chkforPortfolios=chkforPortfolios+1
	        				pointscheck.push("resume");
        				}
        				if (document.getElementById("dspqQues").checked==true) 
        				{
	        				chkforPortfolios=chkforPortfolios+1
	        				pointscheck.push("dspqQues");
        				}
	        		 }
	        		
	            }
	        }
	 } 	
	 if(chkforPortfolios==0 && chkportfolio==true)
	 {
		 $('#errForPrint').show();
		 $('#errForPrint').append("&#149; "+resourceJSON.PlzSelectPortfolio+"<br>");
		 return false;
	 }
	 if(chkcl==false && chkqq==false && chkportfolio==false && chkjsi==false)
	 {
		 $('#errForPrint').show();
		 $('#errForPrint').append("&#149; "+resourceJSON.PlzSelectOneOption+"<br>");
	 }
	 else
	 {
		 $('#loadingDiv').fadeIn();
		 CandidateGridAjaxNew.getPrintMultipleData(savecandidatearray,chkForPrint,chkcl,chkqq,chkportfolio, chkjsi, pointscheck,false,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
			     $('#loadingDiv').hide();				 
					try{
						$('#selectedprintData').modal('hide');
						$('#printDataProfile').modal('show');
						
						for (var i = 0; i < cqpinputs.length; i++) {						      
					        	if(cqpinputs[i].checked==true){		        		
					        		cqpinputs[i].checked=false; 
					            }						      
					    } 							
						$('#printDataProfileDiv').html(data);
						 if($("#districtId").val()==1201470){
					    	 $("#printDataProfile .modal-dialog").width(1040);
					    	 $("#tblTeacherAcademics_Profile").width(950);
					    	 $("#tblTeacherCertificates_Profile").width(950);
					    	 $("#tbleleReferences_Profile").width(950);
					    	 $("#tblWorkExp_Profile").width(950);
					    	 $("#involvementGrid").width(950);
					     }
					}catch(err){}
					
				}
			});
		 
		 
	 }
}
function chkportfolioOPtions()
{
	 var elements = document.getElementsByName("chkportfolioItem");
	 if(document.getElementsByName("cqp")[2].checked==true)
	 {
		 $("#portfolioItmDiv").show();
		 for (i=0;i<elements.length;i++) 
		 {
			elements[i].checked = true;
			elements[i].disabled=false;
		 }
	 }
	 else{
		 $("#portfolioItmDiv").hide();
		 for (i=0;i<elements.length;i++) 
		 {
			elements[i].checked = true;
			elements[i].disabled=true;
		 }
	 }
	 	
}
function cancelMultiPrint()
{
	$('#errForPrint').empty();
	$('#errForPrint').hide();
	$("#portfolioItmDiv").hide();
	 var cqpinputs = document.getElementsByName("cqp"); 	
	for (var i = 0; i < cqpinputs.length; i++) {						      
    	if(cqpinputs[i].checked==true){		        		
    		cqpinputs[i].checked=false; 
        }						      
    } 	
	$('#selectedprintData').modal('hide');
	
	var elements = document.getElementsByName("chkportfolioItem");
		 for (i=0;i<elements.length;i++) 
		   elements[i].checked = false;
	
}
function printAllData()
{	
	$('#loadingDiv').fadeIn();
	var inputs = document.getElementById("jftIds").value;
	var chkqq = document.getElementById("chkqq").value;
	var chkportfolio=document.getElementById("chkportfolio").value;	
	
	var chkjsi=document.getElementById("chkjsi").value;	
	
	
	var pointscheck = new Array();
		if (document.getElementById("pinfo").checked==true) 
		{
			pointscheck.push("pinfo");
		}
		if (document.getElementById("resume").checked==true) 
		{
			pointscheck.push("resume");
		}
		if (document.getElementById("dspqQues").checked==true) 
		{
			pointscheck.push("dspqQues");
		}
	
	
	
	var chkForPrint = 1;		
		if(inputs!="")
		{
			    CandidateGridAjaxNew.getPrintMultipleData(inputs,chkForPrint,false,chkqq,chkportfolio, chkjsi,pointscheck,true,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
			    	$('#loadingDiv').hide();
					try
					{
							window.open(data, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
					}
					catch (e) 
					{							
						$('#printmessage1').modal('show');							 
					}	
					
			    	/*$('#loadingDiv').hide();
					try
					{
							//window.open(data, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');
							var newWindow = window.open();
					    	newWindow.document.write(data);			
							newWindow.print();	
					}
					catch (e) 
					{							
						$('#printmessage1').modal('show');							 
					}*/	
				}
			});
		}
}	
function closeModalsuccessKSNId(){
	$('#modalsuccessKSNId').modal('hide');
	$('#modalLinkToKSN').modal('show');
}
// statusLogModal
// statusLog

function getNodeStatusLog(nodeName,teacherId,jobForTeacherId,secStatusId,statusId){
	KellyONRAjax.getNodeStatusLog(nodeName,teacherId,jobForTeacherId,secStatusId,statusId,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					$("#modalStatusNotes").modal("show");
					$("#statusLog").html(data);
					var showHideResetButtonONB=$('#showHideResetButtonONB').val();
					if(showHideResetButtonONB==0){
						$('#resetINFbtn').hide();
					}else
						$('#resetINFbtn').show();
				}
			});
}
function doSPSWaived(teacherId,jobForTeacherId,fullName,statusId,nodeName,nodeColorNameW){
	try{
		document.getElementById("waived1").style.display="none";
		document.getElementById("undo").style.display="none";
		document.getElementById("nodeColor").value=nodeColorNameW;
	}catch(err){}
		
	$('#errorStatusNote').empty();
    $("#statusNotes").val("");
	try{
		$("#teacherId").val(teacherId);
	}catch(e){}
	
//	$("#secStatusId").val(secStatusId);
	$("#currentNode").val(fullName);
	$("#statusId").val(statusId);
	var showBtns = false;
	
	if(($("#roleId").val()!='12' && $("#roleId").val()!='13')){
		showBtns = true;
	}
	document.getElementById("jobForTeacherId").value=jobForTeacherId;
	$('#loadingDiv').show();
	KellyONRAjax.getSPStatus(teacherId,jobForTeacherId,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					getTeacherAssessmentGridSPSNode(teacherId);
					getStatusNotes("notesDiv");
					$("#statusNotes").hide();
					$("#modalSPStatus").modal("show");
		            getSPNodeStatusLog(nodeName,teacherId,jobForTeacherId,statusId);
		            if(data=='0'){
		            	if(showBtns){
		            		document.getElementById("waived1").style.display="inline";
		            		document.getElementById("undo").style.display="none";
		            	}
		            	$("#statusNoteDiv").show();
		            }else if(data=='1'){
		            	if(showBtns){
		            		document.getElementById("undo").style.display="inline";
		            		document.getElementById("waived1").style.display="none";
		            	}
		            	$("#statusNoteDiv").hide();
		            }else if(data=='2'){
		            	if(showBtns){
		            		document.getElementById("waived1").style.display="inline";
		            		document.getElementById("undo").style.display="none";
		            	}
		            	$("#statusNoteDiv").show();
		            }else if(data=='4'){
		            	if(showBtns){
		            		document.getElementById("waived1").style.display="none";
		            		document.getElementById("undo").style.display="none";
		            	}
		            	$("#statusNoteDiv").hide();		            	
		            }else if(data=='3'){
		            	if(showBtns){
		            		document.getElementById("waived1").style.display="inline";
		            		document.getElementById("undo").style.display="none";
		            	}
		            	$("#statusNoteDiv").show();
		            }
		            document.getElementById("SPSName").innerHTML="SP Status for "+fullName;
					$('#loadingDiv').hide();
				}
			});
}
function doSPSWaivedForMultiple(){	
	$("#modalSPStatusMultiple").modal("show");
}
function closeSPSWaivedModal(){
	$("#modalSPStatusMultiple").modal("hide");
	$("#modalSPStatus").modal("hide");
}
function saveSPSWaived(){
	$("#modalSPStatus").scrollTop(0);
	$('#errorStatusNote').empty();
	var nodeColor = document.getElementById("nodeColor").value;	
	var status=$("#statusNote").val();
	if(status < 1 || status==""){
		$('#errorStatusNote').show();
		$('#errorStatusNote').append("&#149; Please Select A Reason.<br>");
		}
	else
	{
	var jobForTeacherId = document.getElementById("jobForTeacherId").value;
	var secStatusId = $("#statusId").val();//$("#secStatusId").val();	
	$('#loadingDiv').show();
	KellyONRAjax.saveSPStatusWaived(jobForTeacherId,status,secStatusId,nodeColor,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
		            $("#modalSPStatus").modal("hide");
		            $("#statusNotes").hide();
					$('#loadingDiv').hide();
					displayKellyONBGrid(0,0);
					$('select').prop('selectedIndex', 0);
				}
			});
	}
}
function saveSPSWaivedMultiple(){

	if($('#statusNotesMultiple').find(".jqte_editor").text().trim()==""){
		$('#errorStatusNoteMultiple').show();
		$('#errorStatusNoteMultiple').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		}
	else
	{
	var statusNotes = $('#statusNotesMultiple').find(".jqte_editor").html();
	var TeacherIds = new Array();
	TeacherIds	=	getTeacherIdAndJobId();
	$('#loadingDiv').show();
	KellyONRAjax.saveSPStatusWaivedMultiple(TeacherIds,statusNotes,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
		            $("#modalSPStatusMultiple").modal("hide");
		            $("#statusNotes").hide();
					$('#loadingDiv').hide();
					displayKellyONBGrid(0,0);
				}
			});
	}
}
function saveSPSUndo(){
	 $("#modalSPStatus").scrollTop(0);
	 $('#errorStatusNote').empty();
	 var status=$("#statusNote").val();
	/*if(status < 1 || status==""){
		$('#errorStatusNote').show();
		$('#errorStatusNote').append("&#149; Please Select A Reason.<br>");
		}*/
	//else
	//{
	var jobForTeacherId = document.getElementById("jobForTeacherId").value;
	var secStatusId = $("#statusId").val(); //$("#secStatusId").val();
	$('#loadingDiv').show();
	KellyONRAjax.saveSPStatusUndo(jobForTeacherId,status,secStatusId,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
		            $("#modalSPStatus").modal("hide");
		            $("#statusNotes").hide();
					$('#loadingDiv').hide();
					displayKellyONBGrid(0,0);
					$('select').prop('selectedIndex', 0);
				}
			});
	//}
}
function showSubFIlterDiv(){
	var inputs = document.getElementsByName('CGFilter');
	var count=0;
	var chkCount=0;
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	count++;
	            if(inputs[i].checked){
	            	chkCount++;
	            }
	        }
	 }
	 if(chkCount>0){
		 document.getElementById("subFilterDiv").style.display="inline";
	 }else{
		 document.getElementById("subFilterDiv").style.display="none";
		 var inputs = document.getElementsByName("actionSearchId");
		 for (var i = 0; i < inputs.length; i++) {
		        if (inputs[i].checked == true)
		        	inputs[i].checked = false;
		 }
	 }
}


function showNotesDiv(teacherId,jobForTeacherId,nodeName,secStatusId,linkedToKSN,statusId)
{
	var showBtns = false;
	if(($("#roleId").val()!='12' && $("#roleId").val()!='13')){
		showBtns = true;
	}
	
	$("#modalStatusNotes").modal("show");
	$("#modalTitle").html(nodeName);
	
	if(showBtns)
		document.getElementById("resetINFbtn").style.display="none";
	if(nodeName==resourceJSON.lblLinkToKsn){
		$(".btnFinalizeid").text(resourceJSON.btnfinalizeLinktoKSN);
	}else if(nodeName==resourceJSON.lblINVWF1){
		$(".btnFinalizeid").text(resourceJSON.btnfinalizeINVtoWF1);
		document.getElementById("resetINFbtn").style.display="inline";
	}else if(nodeName==resourceJSON.lblINVWF2){
		$(".btnFinalizeid").text(resourceJSON.btnfinalizeINVtoWF2);
		document.getElementById("resetINFbtn").style.display="inline";
	}else if(nodeName==resourceJSON.lblINVWF3){
		$(".btnFinalizeid").text(resourceJSON.btnfinalizeINVtoWF3);
		document.getElementById("resetINFbtn").style.display="inline";
	}else{
		$(".btnFinalizeid").text("Finalize");
	}
	
	$('#stNotesDiv').find(".jqte_editor").empty();
	$("#teacherId").val(teacherId);
	$("#jobForTeacherId").val(jobForTeacherId);
	$("#currentNode").val(nodeName);
	$("#secStatusId").val(secStatusId);
	$("#statusId").val(statusId);
	$("#linkedToKSN").val(linkedToKSN);
	getNodeStatusLog(nodeName,teacherId,jobForTeacherId,secStatusId,statusId);
	getStatusNotes("notesGenDiv");
	$("#notesRow").hide();
	if(nodeName==resourceJSON.lblLinkToKsn){
			getTalentKSNDetail();
	}
}





function genericNotesSaveDiv(){
	var jobForTeacherId = "";
	var teacherId = "";
	var nodeName = "";
	var secStatusId = "";
	var statusId = "";
	teacherId = $("#teacherId").val();
	jobForTeacherId = $("#jobForTeacherId").val();
	nodeName = $("#currentNode").val();	
	secStatusId = $("#secStatusId").val();
	statusId = $("#statusId").val();
	var statusNotes = "";
		statusNotes = $('#stNotesDiv').find(".jqte_editor").html();
		$('#loadingDiv').show();
			if((nodeName==resourceJSON.lblINVWF1) || (nodeName==resourceJSON.lblINVWF2) || (nodeName==resourceJSON.lblINVWF3)){
				$("#modalStatusNotes").modal("hide");
				callWorkFlowsAPI(teacherId,jobForTeacherId,nodeName,statusNotes,secStatusId);
			}
			else if(nodeName==resourceJSON.lblLinkToKsn)
			{ 
					var linkedToKSN = $("#linkedToKSN").val();
					if(linkedToKSN!=null && linkedToKSN!="" && linkedToKSN==1&& statusNotes.length==0){		
							$('#loadingDiv').hide();
							$('#emptynotespopup').show();							
							//addStatusNotes('notesRow');
							//$('#stNotesDiv').find(".jqte_editor").focus();
						}
						else{
							$("#modalStatusNotes").modal("hide");
							callLinkToKSN(teacherId,jobForTeacherId,nodeName,statusNotes,secStatusId);
						}
			}
			else{
				KellyONRAjax.saveNotesForOnBoardingNodes(teacherId,jobForTeacherId,nodeName,statusNotes,secStatusId,statusId,
						{ 
							async: true,
							errorHandler:handleError,
							callback:function(data)
							{
								getStatusNotes("notesGenDiv");
								$("#notesRow").hide();
								//applyScrollOnStatusNote();
								$('#loadingDiv').hide();
							}
						});
			}
      }


function getStatusNotes(divId)
{
	$('#loadingDiv').show();
	var jobForTeacherId = "";
	var teacherId = "";
	var nodeName = "";
	var secStatusId = "";
	var statusId = "";
	teacherId = $("#teacherId").val();
	jobForTeacherId = $("#jobForTeacherId").val();
	nodeName = $("#currentNode").val();
	secStatusId = $("#secStatusId").val();
	statusId = $("#statusId").val();
	$("#"+divId+"").html("");
	try{
		$("#ksndetailsnew").hide();
	}catch(e){}
		KellyONRAjax.getStatusNotesDiv(teacherId,jobForTeacherId,nodeName,secStatusId,pageSNote,noOfRowsSNote,sortOrderStrSNote,sortOrderTypeSNote,statusId,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					$("#"+divId+"").html(data);
					applyScrollOnStatusNote();
					$('#loadingDiv').hide();
					
				}
			});
}
function addStatusNotes(id){
	$("#"+id+"").show();
}

function getTalentKSNDetail()
{
	var teacherId = $("#teacherId").val();;
	var secondaryStatusId=$("#secStatusId").val();
	try{
		ManageStatusAjax.getTalentKSNDetail(teacherId,secondaryStatusId,
		{
			async:true,
			errorHandler:handleError,
			callback:function(data)
			{
				$('#ksndetailsnew').show();
				$('#ksndetailsnew').html(data);
				applyScrollOnTableGrid("tblStatusNote_2");
			}
		});
	}catch(err){}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function downloadSLCReport()
{
	$('#loadingDiv').show();
	
	var statusName = $("#currentNode").val();
	var jobForTeacherId =$("#jobForTeacherId").val();
	var statusId="0";
	var secondaryStatusId=$("#secStatusId").val();
	
	CandidateGridAjaxNew.downloadSLCReport(jobForTeacherId,statusId,secondaryStatusId,{
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			//$('#loadingDiv').hide();
			
			document.getElementById("myModalLabelSLCNotes").innerHTML="SLC "+statusName+" Notes";
			
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					$('#modalDownloadCGSLCReport').modal('hide');
					document.getElementById(hrefId).href=data;				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  try{
					    $('#modalDownloadCGSLCReport').modal('hide');
						}catch(err)
						  {}
					    document.getElementById('ifrmCGSLCR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadCGSLCReport').modal('hide');
					document.getElementById(hrefId).href=data;					
				}				
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				  try{
				    	$('#modalDownloadCGSLCReport').modal('hide');
					}catch(err)
					  {}
				    document.getElementById('ifrmCGSLCR').src = ""+data+"";
			}
			else
			{
				$('#modalDownloadCGSLCReport').modal('hide');
				document.getElementById('ifrmCGSLCR').src = ""+data+"";
				try{
					$('#modalDownloadCGSLCReport').modal('show');
				}catch(err)
				  {}
				
			}		
			
			return false;
		}});
}

function callWorkFlowsAPI(teacherId,jobForTeacherId,currentNode,statusNotes,secStatusId){
	try{
		$("#currentNode").val(currentNode);
		$("#teacherId").val(teacherId);
	}catch(e){}
	$('#loadingDiv').show();
	KellyONRAjax.callWorkFlowFromOnboarding(teacherId,jobForTeacherId,currentNode,statusNotes,secStatusId,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data!=null && data!=""){
						$("#modalStatusNotes").modal("hide");
						//$("#ksndetailsnew").html(data);
						$('#modalsuccessKSNId').modal('show');
						$("#internalTxt1").html(data);
						getStatusNotes("notesGenDiv");
						$("#notesRow").hide();
						reflectLinkToKsnNodeColor($("#teacherId").val(),$("#currentNode").val());
					}
					$('#loadingDiv').hide();
				}
			});
}
function hidepopup(){
	$('#emptynotespopup').hide();
	addStatusNotes('notesRow');	
	$('#stNotesDiv').find(".jqte_editor").focus();
}


// For profile view
function printCommunicationslogs()
{
	var commDivFlag = $("#myModalCommunications").attr("commDivFlag");
	var jobForTeacherGId = $("#myModalCommunications").attr("jobForTeacherGId");
	var teacherId = $("#myModalCommunications").attr("teacherId");
	var jobId = $("#myModalCommunications").attr("jobId");
	
	//alert("commDivFlag:- "+commDivFlag+"\njobForTeacherGId:- "+jobForTeacherGId+"\nteacherId:- "+teacherId+"\njobId:- "+jobId);
	CandidateGridAjaxNew.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,true,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
			
			var newWindow = window.open();
			newWindow.document.write(data);	
			newWindow.print();
		}
	});
}
function saveCandidateToFolderByUser()
{
	defaultTeacherGrid();
	document.getElementById('iframeSaveCandidate').contentWindow.checkFolderId();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	var folderId = innerDoc.getElementById('frame_folderId').value;
	//alert(" frame_folderId sdfsdfsdf "+folderId+" value "+folderId.value+" :::::: "+folderId.length);
	//alert(" folderId "+folderId);
	var frameHeight=iframe.contentWindow.document.body.scrollHeight;
	$('iframe').css('height',frameHeight);
	var fId="";
	if(innerDoc.getElementById('frame_folderId').value=="" || innerDoc.getElementById('frame_folderId').value.length==0)
	{
		//alert("Yes")
		innerDoc.getElementById('errortreediv').style.display="block";
		innerDoc.getElementById('errortreediv').innerHTML="&#149; "+resourceJSON.PlzSelectAnyFolder+"";
		 return false;
	}
	else
	{
		//alert(" Else No ");
		//fId=folderId;
	}
	//alert(" sgfsdgb "+innerDoc.getElementById('frame_folderId').value);

	
	
	var teacherId=	document.getElementById('teacherIdForHover').value
	
	CandidateGridAjaxNew.saveCandidateToFolderByUserFromTeacherId(teacherId,folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
			if(data==1)
			{
				$("#saveToFolderDiv").modal("hide");
				$('#saveAndShareConfirmDiv').html(resourceJSON.SavedCandidate);
				$('#shareConfirm').modal("show");
				//document.getElementById("savedCandidateGrid").innerHTML=data;
				//displaySaveCandidatePopUpDiv();
				//uncheckedAllCBX();
			}
			if(data==3)
			{
				//alert("Duplicate Candaidate found ");
				$("#txtoverrideFolderId").val(folderId);
				try{
					$("#duplicatCandidate").modal("show");
				}catch(err)
				  {}
			}
		}
	});
	 
	 
}
function saveWithDuplicateRecord()
{
	$("#saveToFolderDiv").modal("hide");
	$("#duplicatCandidate").modal("hide");
	
}

function displaySavedCGgridByFolderId(hoverDisplay,folderId,checkboxshowHideFlag)
{
	document.getElementById("checkboxshowHideFlag").value=checkboxshowHideFlag;
	document.getElementById("folderId").value=folderId;
	var pageFlag= document.getElementById("pageFlag").value;
	CandidateGridAjaxNew.displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag,pageSTT,noOfRowsSTT,sortOrderStrSTT,sortOrderTypeSTT,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				document.getElementById("savedCandidateGrid").innerHTML=data;
				displaySaveCandidatePopUpDiv();
			}
		}
	});
}



/* ===== Gagan : displaySaveCandidatePopUpDiv() Method =========== */
function displaySaveCandidatePopUpDiv()
{
	
	var noOrRow = document.getElementById("saveCandidateTable").rows.length;
	for(var j=5000;j<=5000+noOrRow;j++)
	{
		
		$('.profile'+j).popover({ 
		 	trigger: "manual",
		    html : true,
		    placement: 'right',
		    content: $('#profileDetails'+j).html(),
		  }).on("mouseenter", function () {
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });
	}
}
count=0;
index=-1;
length=0;
divid='';
txtid='';
hiddenDataArray = new Array();
showDataArray = new Array();
degreeTypeArray = new Array();
hiddenId="";

function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;
	//BatchJobOrdersAjax
	CGServiceAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function searchUserthroughPopUp(searchFlag)
{
	if(searchFlag==1)
	{
		defaultShareDiv();
	}
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=0;
	document.getElementById("userCPS").value=1;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";
	$('#errorinvalidschooldiv').hide();
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
			}catch(err){}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		var schoolName	=	trim($('#shareSchoolName').val());
		if(schoolId=="")
		{
			if( schoolName!="")
			{
				$('#errorinvalidschooldiv').show();
				$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.SelectValidSchool+"");
				return false;
			}
			else
			{
				schoolId=0;
			}
			
		}
		if($('#entityType').val()!="")
		{
			CandidateGridAjaxNew.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}	
}
function shareCandidatethroughPopUp()
{
	defaultTeacherGrid();
	$('#errorinvalidschooldiv').hide();
	$('#errorinvalidschooldiv').empty();
	var userIdarray =	"";
	var myUsercbx	=	document.getElementsByName("myUsercbx");
	var folderId = document.getElementById('folderId').value;
	
	 for (e=0;e<myUsercbx.length;e++) 
	 {
	  if (myUsercbx[e].checked==true) 
	  {
		  userIdarray+=myUsercbx[e].value+",";
	   }
	 }
	 	if(userIdarray=="")
	 	{
	 		$('#errorinvalidschooldiv').show();
	 		$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.PlzSelectUsr+"");
	 	}
	 	var teachersharedId=document.getElementById("teachersharedId").value;
		CandidateGridAjaxNew.shareCandidatesToUserForTeacher(userIdarray,teachersharedId,folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
			if(data==1)
			{
				$('#shareDiv').modal("hide");
				$('#saveAndShareConfirmDiv').html(resourceJSON.msgsuccessSharedCand);
				$('#shareConfirm').modal("show");
				//uncheckedAllCBX();
			}
		}
		});
	 
	 
}
function closeProfileOpenTags(teacherId,id)
{
	/*
	
	try { $("#draggableDivMaster").modal('hide');	} catch (e) {}
	$('#profile'+j).popover('hide');
	
	showTagDetailsTP(teacherId,j);
	
	$('#tagPreview'+j).popover('show');
	
	/*if($.inArray(parseInt(j), arrTagCall)==-1)
	{
		arrTagCall.push(parseInt(j));
		$('#tagPreview'+j).popover('show');
	}*/
	
	// Gourav code
	
	$("#draggableDivMaster").modal('hide');
	$("#draggableDivMaster").hide();
	$('.allPopOver').popover('hide');
	$(".popover" ).remove();
	$("#tagPreview"+id).click();
	//document.getElementById('tagPreview'+id).click();
		
	/*$('.popup').css('left',event.pageX);      // <<< use pageX and pageY
    $('.popup').css('top',event.pageY);*/
   $('.popup').css('display','inline');     
   $(".popover-content").css("margin-top", "10px");
    
    //input[type=radio]
   if($('.popover').find('input[type=checkbox]').length>15){   
	$('.popover').css('width','70%');
   }else{
	   $('.popover').css('width','40%');
   }
	$('#currentPopUpId').val(id);
	var popH = $('.popover').css('top').replace('px','');
	var diff =$("#tagPreview"+id).offset().top-$(".arrow").offset().top;
	var k = Number(popH) + Number(diff); 
	$('.popover').css('top',k+'px');
	
	
	
}
function saveTags(teacherId){
	/*var checkValues = $('input[name=tagsValue]:checked').map(function()
            {
                return $(this).val();
            }).get();*/
var districtId = $("#districtId").val();
if(districtId=="")
	districtId="0";
var checkboxes = document.getElementsByName('tagsValue'+teacherId);
var vals = "";
for (var i=0, n=checkboxes.length;i<n;i++) {
  if (checkboxes[i].checked) 
  {
  vals += ","+checkboxes[i].value;
  }
}
if (vals) vals = vals.substring(1);
	TeacherInfotAjax.saveTags(districtId,teacherId,vals,null,{ 
		async: false,
		callback: function(data)
		{
			var currentPopUpId =  $('#currentPopUpId').val();
			$('#tagPreview'+currentPopUpId).popover('hide');
		},
		errorHandler:handleError 
	});
	
}

function showTooltip(){
	var noOrRow=0;
	try {
	 noOrRow = document.getElementById("onbGridTable").rows.length;
	
	for(var j=1;j<=noOrRow;j++){
		try{$('#cndNotReviewed'+j).tooltip(); }catch(e){}
	}
	}catch(err) {}
}

function closeActActionForKelly(counter)
{
	$(".popover" ).remove();
	$('#tagPreview'+counter).popover('hide');
}
function showProfileContentForKelly(dis,teacherId,noOfRecordCheck,sVisitLocation,event)
{
	   try
	   {
		   var mydiv = document.getElementById("mydiv");
	       var curr_width = parseInt(mydiv.style.width);

	       if(deviceType)
	       {
	    	   mydiv.style.width = 630+"px";
	       }
	       else
		   {
	    	   mydiv.style.width = 647+"px";  
		   }      
	   }
	   catch(e){alert(e)}

	var districtMaster = null;
	showProfileContentClose_ShareFolder();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
	async: true,
	callback: function(data)
	{
	  $('#loadingDiv').hide();
	 
		  $("#draggableDivMaster").modal('show');
			$("#draggableDiv").html(data);
	       
			TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
				async: true,
				callback: function(resdata){
				setTimeout(function () {
					$("#profile_id").after(resdata);
					$('#removeWait').remove();
		        }, 100);
				
			}
			});
		
		$('#tpResumeprofile').tooltip();
		$('#tpPhoneprofile').tooltip();
		$('#tpPDReportprofile').tooltip();
		$('#tpTeacherProfileVisitHistory').tooltip();
		$('#teacherProfileTooltip').tooltip();		
	},
	errorHandler:handleError  
	});
}

function disconnectTalent(){
	$("#errorDiv").empty();
	$("#errorDiv").hide();
	var teacherId="";
	var newksnId ="";
	newksnId = trim(document.getElementById("inputKSN").value);
	//alert("newksnId :: "+newksnId)
	teacherId = document.getElementById("disconnectTalentId").value;
	var errorCount = 0;
	if(newksnId.length==0){
		$("#inputKSN").focus();
		$("#errorDiv").show();
		$('#errorDiv').append("&#149; Please enter KSN ID<br>");
		errorCount++;
	}

	if(errorCount==0)
		//CandidateGridSubAjax.disconnectQueue(teacherId,newksnId, {
		ManageStatusAjax.disconnectQueue(teacherId,newksnId, {
		preHook:function(){$('#loadingDiv').show()},
		postHook:function(){$('#loadingDiv').hide()},
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//alert(data);
			var result = data.split("###");
			if(result.length>0){
				if(result[0]=="error"){
					$("#inputKSN").focus();
					$("#errorDiv").show();
					$('#errorDiv').append(result[1]);
				}
				else{
					if(data.length>0){
						$("#draggableDivMaster").modal("hide");
						$("#disconnectResponse").modal("show");
						if(result[0]=="Success")
							$("#disconnectRespMsg").text(result[0]+": "+result[1]);
						else
							$("#disconnectRespMsg").text(data);
					}
				}
			}
		}
	});	
}

function hideDisconnectResponse()
{
	$("#disconnectResponse").modal("hide");
	//$("#draggableDivMaster").modal("show");
	//$("#tpDisconnect").hide();
	//$("#inputKSN").val("");
	//showLinkToKSNResponse(document.getElementById("disconnectTalentId").value);
}

function showLinkToKSNResponse(teacherId){
	ManageStatusAjax.getLinktoKSNResponse(teacherId,{ 
		async: true,
		callback: function(data){
			$("#ksndetails").empty();
			$("#ksndetails").html(data);
		},
		errorHandler:handleError  
	});
}

function saveStatusResetINF(){
	var jobForTeacherId = document.getElementById("jobForTeacherId").value;
	var secStatusId = $("#secStatusId").val();
	$('#loadingDiv').show();
	KellyONRAjax.saveStatusResetINF(jobForTeacherId,status,secStatusId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
	        $("#modalStatusNotes").modal("hide");
	        $("#statusNotes").hide();
			$('#loadingDiv').hide();
			displayKellyONBGrid(0,0);
		}
	});
}

function getSPNodeStatusLog(nodeName,teacherId,jobForTeacherId,statusId){
	KellyONRAjax.getNodeStatusLogForSPS(nodeName,teacherId,jobForTeacherId,statusId,
			{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					$("#statusSPLog").html(data);
				}
			});
}
function resetDuplicateCheck()
{
	$('#duplicateCheck').val(1);
}

function getTeacherAssessmentGridSPSNode(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherAssessmentGridSP(teacherId,noOfRowsEJ,pageEJ,sortOrderStrEJ,sortOrderTypeEJ, { 
	  async: true,
	  errorHandler:handleError,
	  callback:function(data)
	  {
			$('#gridTeacherAssessmentSP').html(data);
			applyScrollOnAssessmentsSP();
	  }});
}
