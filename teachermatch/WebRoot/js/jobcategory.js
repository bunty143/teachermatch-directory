var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	var entityID	=	document.getElementById("entityID").value;
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}
	displayJobCategories();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(entityID==1){
		var searchTextId	=	document.getElementById("districtIdFilter").value;
	}
		displayJobCategories();
}
function addNewJobCategory()
{	
	$('#jobCatDiv').show();
	$('#jobcategory').val("");
	$('#jobcategoryId').val("");
	$('#districtName').val("");
	document.getElementById("divJsiName").style.display="none";
	document.getElementById("removeJsi").style.display="none";
	document.getElementById("jsiFileName").value="";
	document.getElementById("assessmentDocument").value="";
	$('#attachedDAList').css("background-color", "");
	document.getElementById("attachedDAList").value="";	
	
	$("#offerPortfolioNeededjs").show(); 
	$("#imofferPortfolioNeededimjs").show();
	
	$('#offerDistrictSpecificItems').prop("checked",true);
	$('#offerQualificationItems').prop("checked",true);
	$('#offerJSI').prop("checked",true);
	$('#offerPortfolioNeeded').prop("checked",true);
	
	$('#JSIRadionDiv').show();
	$('#offerJSIRadioMaindatory').prop("checked",true);
	$('#jobCatRadio1').prop("checked",true);
	$("#parentJobCategoryId").prop("disabled",true);
	$("#parentJobCategoryId").val(0);
	$("#subJobCatDiv").hide();
	$("#subjobcategory").val("");
	$("#jobcategory").prop("disabled",false);
	$( "#offerJSI" ).on( "click", function( event ) {
	  	if(document.getElementById("offerJSI").checked == true){
	  		$('#JSIRadionDiv').show();
	  		$('#offerJSIRadioMaindatory').prop("checked",true);
	  	} else {
	  		$('#JSIRadionDiv').hide();
	  	}
	 });
	//add by ankit******/
		
	$('#imofferDistrictSpecificItems').prop("checked",true);
	$('#imofferQualificationItems').prop("checked",true);
	$('#imofferJSI').prop("checked",true);
	$('#imofferPortfolioNeededim').prop("checked",true);
	$('#imJSIRadionDiv').show();
	$('#imofferJSIRadioMaindatory').prop("checked",true);
	$( "#imofferJSI" ).on( "click", function( event ) {
	  	if(document.getElementById("imofferJSI").checked == true){
	  		$('#imJSIRadionDiv').show();
	  		$('#imofferJSIRadioMaindatory').prop("checked",true);
	  	} else {
	  		$('#imJSIRadionDiv').hide();
	  	}
	 });
	//swadesh
	$( "#autoRejectCheck" ).on( "click", function( event ) {
		//alert("93");
	  	if(document.getElementById("autoRejectCheck").checked == true){
	  		$('#autoRejectDiv').show();
	  	} else {
	  		$('#autoRejectDiv').hide();
	  	}
	 });
	//add end by ankit
	$('#epicheck').prop("checked",false);
	$('#epicheck2').prop("checked",false);
	$('#jobcategory').css("background-color", "");
	$('#districtName').css("background-color", "");
	if($('#entityID').val()==1){
		$('#districtName').focus();
		document.getElementById('districtId').value="";
		$('#jsiDiv').show();
	}else{
		$('#jobcategory').focus();
	}
	try
	{
		//getQQuestionSetByDistrictAndJobCategoryList();
		//getQQSetForOnboardingByDistrictAndJobCategoryList();
			document.getElementById('offerVirtualVideoInterview').checked=false;
			document.getElementById('wantScore').checked=false;
			document.getElementById('sendAutoVVILink').checked=false;
			document.getElementById('timePerQues').value="";
			document.getElementById('vviExpDays').value="";
			document.getElementById('quesId').value="";
			document.getElementById('quesName').value="";
			document.getElementById('maxScoreForVVI').value="";
			document.getElementById('slctStatusID').value="";
			$("#offerVVIDiv").hide();
			$("#marksDiv").hide();
			$("#autolinkDiv").hide();
	}catch(err){}
	
	showSubEpi();
	getEpiGroups();
}

function cancelJobCateDiv()
{
	$('#jobCatDiv').hide();
	$('#jobcategory').val("");
	$('#districtName').val("");
	$('#errordiv').hide();
	$('#epicheck').prop("checked",false);
	$('#epicheck2').prop("checked",false);
	$('#jobcategory').css("background-color", "");
	$('#districtName').css("background-color", "");
}
/*========  Search District ===============*/
function displayOrHideSearchBox()
{ 
	districtNameFilter=1;
	var entityID	=	document.getElementById("entityID").value;
	if(entityID==1){
		document.getElementById("districtNameFilter").value="";
		document.getElementById("districtIdFilter").value="";
		var op=document.getElementById("entityType");
		var searchEntityType=op.options[op.selectedIndex].value;
		if(searchEntityType == 2)
		{
			document.getElementById("captionDistrictOrSchool").innerHTML	=	resourceJSON.msgDistrict;
			$("#Searchbox").show();
			$('#districtNameFilter').focus();
		}else{
			$("#Searchbox").hide();
		}
	}
}
function chkForEnterSearchJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		page = 1;
		displayJobCategories();
	}	
}
function searchJobCat()
{
	page = 1;
	displayJobCategories();
}
function displayJobCategories()
{	
	var resultFlag=false;
	var entityID	=	document.getElementById("entityID").value;
	var searchTextId=0;
	var searchEntityType
	if(entityID==1){
		var op=document.getElementById("entityType");
		searchEntityType=op.options[op.selectedIndex].value;
		if(searchEntityType == 2){
			searchTextId	=	document.getElementById("districtIdFilter").value;
			if(document.getElementById("districtNameFilter").value=="" && searchTextId==""){
				searchTextId=0;
				resultFlag=true;
			}
		}
		if(document.getElementById("districtNameFilter").value!="" && searchTextId==""){
			searchTextId=0;
			resultFlag=false;
		}
	}else{
		searchTextId=0;
		resultFlag=true;
	}
	
	try{
		
		var districtId = $('#districtId').val();
		
		if(districtId==804800)
			$('#approvalBPGroupsDiv').show()
		else
			$('#approvalBPGroupsDiv').hide()
	}catch(e){}
	
	
	
	
	//alert("resultFlag :"+resultFlag+"\n searchTextId "+searchTextId);
	JobCategoryAjax.displayJobCategories(resultFlag,searchEntityType,entityID,searchTextId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		$('#jobCategoryGrid').html(data);
		applyScrollOnTbl();
	},
	});
}

function chkForEnterSaveJobOrder(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveJobCategory();
	}	
}

function saveJobCategory()
{
	var arrSecStatusSliderValue='';
	//swadesh
	var autoRejectCheck=document.getElementById('autoRejectCheck').checked;
	
	try{
		var iframeNorm = document.getElementById('autoCheckSlider');
		var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
			if(innerNorm.getElementById('autoReject').value!='' && autoRejectCheck==true)
			{
				arrSecStatusSliderValue+=innerNorm.getElementById('autoReject').value;
			}else
				arrSecStatusSliderValue='';
		}catch(e){} 
	
	$('#attachedDAList').css("background-color", "");
		
	var fileSize = 0;
	
	var jobcategoryName=trim(document.getElementById('jobcategory').value);
	var subjobcategoryName=trim(document.getElementById('subjobcategory').value);
	var jobCategoryId=trim(document.getElementById('jobcategoryId').value);
	var districtId=trim(document.getElementById('districtId').value);
	var entityID=trim(document.getElementById('entityID').value);
	var jsiFileName=trim(document.getElementById('jsiFileName').value);
		
	var offerDistrictSpecificItems=document.getElementById('offerDistrictSpecificItems').checked;
	var offerQualificationItems=document.getElementById('offerQualificationItems').checked;
	var offerJSI=document.getElementById('offerJSI').checked;
	var offerPortfolioNeeded=document.getElementById('offerPortfolioNeeded').checked;
	var parentJobCategoryId="";
	
	/* For VVI */
	var offerVVIForInternal=document.getElementById('offerVVIForInternal').checked;
	var offerVirtualVideoInterview="";
	var quesId="";
	var quesName="";
	var maxScoreForVVI="";
	var sendAutoVVILink="";
	var slctStatusID="";
	var timePerQues="";
	var vviExpDays="";
	var jobCompletedVVILink="";
	var jobCompletedVVILink1="";
	var jobCompletedVVILink2="";
	/*add by ankit*/
	var imepicheck2=document.getElementById('imepicheck2').checked;
	var imofferPortfolioNeeded=document.getElementById('imofferPortfolioNeededim').checked;
	var imofferDistrictSpecificItems=document.getElementById('imofferDistrictSpecificItems').checked
	var imofferQualificationItems=document.getElementById('imofferQualificationItems').checked;
	var imofferJSI=document.getElementById('imofferJSI').checked;
	var attachDSPQFromJC=document.getElementById('attachDSPQFromJC').checked;
	var attachSLCFromJC=document.getElementById('attachSLCFromJC').checked;
	/*add end by ankit*/
	
	var approvalByPredefinedGroups=document.getElementById('approvalByPredefinedGroups').checked;
	
	
	
	// Reference Check
	try{
		var questionSetVal = document.getElementById('avlbList').value;
	} catch(e){}
	
	if(offerJSI == true){
		var offerJSIRadioMaindatory	=	document.getElementById('offerJSIRadioMaindatory').checked;
		var offerJSIRadioOptional	=	document.getElementById('offerJSIRadioOptional').checked;
		if(offerJSIRadioMaindatory == true){
			offerJSI = 1;
		} else if(offerJSIRadioOptional == true){
			offerJSI = 2;
		}
		
	} else if(offerJSI == false){
		offerJSI = 0;
	}
	
/**********add by ankit********/
	if(imofferJSI == true){
		var imofferJSIRadioMaindatory	=	document.getElementById('imofferJSIRadioMaindatory').checked;
		var imofferJSIRadioOptional	=	document.getElementById('imofferJSIRadioOptional').checked;
		if(imofferJSIRadioMaindatory == true){
			imofferJSI = 1;
		} else if(imofferJSIRadioOptional == true){
			imofferJSI = 2;
		}
		
	} else if(imofferJSI == false){
		imofferJSI = 0;
	}
	/*******add end by ankit****/
	
	
	var baseStatus;
	var epiForFullTimeTeachers;
	var assessmentGroupId = null;
	
	if(document.getElementById('epicheck').checked)
		baseStatus=true;
	else
		baseStatus=false;
	
	if(document.getElementById('epicheck2').checked)
		epiForFullTimeTeachers=true;
	else
		epiForFullTimeTeachers=false;
	
	if($('#assessmentGroupId').is(":visible") && $('#assessmentGroupId').val()!=0)
		assessmentGroupId = $('#assessmentGroupId').val();
	else
		assessmentGroupId = null;
	
	var assessmentDocument	=	document.getElementById("assessmentDocument").value;
	if(document.getElementById("assessmentDocument").files[0]!=undefined)
	{
		fileSize = document.getElementById("assessmentDocument").files[0].size;
	}
	
	var counter				=	0;
	var focuscount			=	0;
	$('#errordiv').empty();
	$('#jobcategory').css("background-color", "");
	$('#districtName').css("background-color", "");
	$('#parentJobCategoryId').css("background-color", "");
	$('#subjobcategory').css("background-color", "");
	$('#assessmentGroupId').css("background-color", "");
	if(entityID==1){
		var districtName=trim(document.getElementById('districtName').value);
		//if(districtName=""){
				if(districtId==""){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
				$('#districtName').focus();
				$('#districtName').css("background-color", "#F5E7E1");
				counter++;
				focuscount++;
			}
		//}
	}
	
	if (jobcategoryName	==	"" && !$('#jobcategory').prop('disabled'))
	{	
		$('#errordiv').show();
		$('#errordiv').append("&#149; "+resourceJSON.msgPlsEnterJobCategory+"<br>");
		if(focuscount	==	0)
			$('#jobcategory').focus();
			$('#jobcategory').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	if(!$('#parentJobCategoryId').prop('disabled')){
		if($('#parentJobCategoryId').val()=="0"){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPlsEnterJobCategory+"<br>");
			if(focuscount	==	0)
				$('#parentJobCategoryId').focus();
				$('#parentJobCategoryId').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}else if (subjobcategoryName=="")
		{	
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgSubJobCategory+"<br>");
			if(focuscount	==	0)
				$('#subjobcategory').focus();
				$('#subjobcategory').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}
		if (subjobcategoryName!=""){
			jobcategoryName=subjobcategoryName;
			parentJobCategoryId=$('#parentJobCategoryId').val();
		}
	}
	
	if($('#assessmentGroupId').is(":visible") && $('#assessmentGroupId').val()==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please Select EPI Group<br>");
		if(focuscount==0)
			$('#assessmentGroupId').focus();
			$('#assessmentGroupId').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	if(fileSize>=10485760)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
		counter++;
		focuscount++;
	}

	/* For VVI*/
		offerVirtualVideoInterview=document.getElementById('offerVirtualVideoInterview').checked;
		
		if(offerVirtualVideoInterview==true)
		{
			wantScore=document.getElementById('wantScore').checked;
			sendAutoVVILink=document.getElementById('sendAutoVVILink').checked;
			timePerQues=trim(document.getElementById('timePerQues').value);
			vviExpDays=trim(document.getElementById('vviExpDays').value);
			quesId=trim(document.getElementById('quesId').value);
			quesName=trim(document.getElementById('quesName').value);
			
			if(quesName=="")
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgPleaseQuestionSet+"<br>");
				$('#quesName').css("background-color", "#F5E7E1");
				counter++;
				focuscount++;
			}
			
			if(wantScore==true)
			{
				maxScoreForVVI=trim(document.getElementById('maxScoreForVVI').value);
				if(maxScoreForVVI=="")
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgPleaseMaxScore+"<br>");
					$('#maxScoreForVVI').css("background-color", "#F5E7E1");
					counter++;
					focuscount++;
				}
			}
			
			if(sendAutoVVILink==true){
				jobCompletedVVILink1=document.getElementById('jobCompletedVVILink1').checked;
				jobCompletedVVILink2=document.getElementById('jobCompletedVVILink2').checked;
				
				if(jobCompletedVVILink1==true)
					jobCompletedVVILink = false;
				else if(jobCompletedVVILink2==true)
					jobCompletedVVILink = true;
				
				if(jobCompletedVVILink==false)
					slctStatusID=trim(document.getElementById('slctStatusID').value);
			}
			
			if(timePerQues!="")
			{
				if(timePerQues<30)
				{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgquestionatleast30seconds+"<br>");
					
					if(focusCount==0)
						$('#timePerQues').focus();
					
					$('#timePerQues').css("background-color", "#F5E7E1");
					counter++;
					focusCount++;
				}
			}
		}
		
		
		var offerAssessmentInviteOnly=document.getElementById('offerAssessmentInviteOnly').checked;
		var opts = "";
		var arrDAIds = "";
		if(offerAssessmentInviteOnly)
		{
			opts = document.getElementById('attachedDAList').options;
			
			for(var i = 0, j = opts.length; i < j; i++)
			{
				arrDAIds+=opts[i].value+"#";
			}
			
			if(document.getElementById("attachedDAList").options.length==0)
			{
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgleastoneDistrictAssessment+"<br>");
				$('#attachedDAList').css("background-color", "#F5E7E1");
				counter++;
				focuscount++;
			}
		}
		
		var qquestionSet = document.getElementById("qqAvlbList").value;
		var qqSetForOnbord = document.getElementById("qqAvlbListForOnboard").value;
		
	if(counter==0){
		if(assessmentDocument=="" && jsiFileName!=""){
			assessmentDocument=jsiFileName;
		}
		
		
		if(assessmentDocument!="" && assessmentDocument!=jsiFileName)
		{ /// if assessmentDocument==jsiFileName are equals mean file is already uploaded. Then there is no need to upload this file again.
			if(entityID==1){
				$("#districtHiddenId").val(districtId);
				if(districtId!=""){
					document.getElementById("frmJsiUpload").submit();
				}else{
					$('#errordiv').show();
					$('#errordiv').append("&#149; "+resourceJSON.msgvaliddistrict+"<br>");
					$('#districtName').focus();
					$('#districtName').css("background-color", "#F5E7E1");
				}	
			}else{
				document.getElementById("frmJsiUpload").submit();
			}
		
			
		}else
		{	
			$('#loadingDiv').show();
			var schoolSelection = $("#schoolSelection").is(":checked");
			var approvalBeforeGoLive=$("#approvalBeforeGoLive1").is(":checked");
			var noOfApprovalNeeded=$("#noOfApprovalNeeded").val();
			var buildApprovalGroup=$("#buildApprovalGroups").is(":checked");
			JobCategoryAjax.saveJobCategory(jobCategoryId,jobcategoryName,districtId,baseStatus,epiForFullTimeTeachers,assessmentDocument,offerDistrictSpecificItems,offerQualificationItems,offerJSI,offerPortfolioNeeded,offerVVIForInternal,offerVirtualVideoInterview,quesId,maxScoreForVVI,sendAutoVVILink,slctStatusID,timePerQues,vviExpDays,offerAssessmentInviteOnly,arrDAIds,questionSetVal,imepicheck2,imofferPortfolioNeeded,imofferDistrictSpecificItems,imofferQualificationItems,imofferJSI,schoolSelection,qquestionSet,parentJobCategoryId,qqSetForOnbord,approvalBeforeGoLive,noOfApprovalNeeded,buildApprovalGroup,attachDSPQFromJC,attachSLCFromJC,approvalByPredefinedGroups,assessmentGroupId,jobCompletedVVILink,arrSecStatusSliderValue,{
				async: true,
				callback: function(data){
		    		if(data=="true"){
						$('#errordiv').show();
						$('#errordiv').append("&#149; "+resourceJSON.msgDuplicateJobCategory+"<br>");
						if(counter	==	0)
							$('#jobcategory').focus();
							$('#jobcategory').css("background-color", "#F5E7E1");
						counter++;
					}else{
						cancelJobCateDiv();
						addOrUpdateDistrictApprovalGroupJobCategorywise(data);
					}	
					$('#loadingDiv').hide();
					displayJobCategories();
				},
			});
		
			
		}
			
	}
}
function downloadJsi(jobCategoryId,linkId)
{
	JobCategoryAjax.downloadJsi(jobCategoryId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}		
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data;			
				}
			}
			else if(deviceTypeAndroid)
			{
			    if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					  
					document.getElementById("ifrmJsi").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;				
				}				
			}		
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById("ifrmJsi").src=data;
			}
			else
			{
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function saveRecord(fileName)
{
	
	$('#errordiv').empty();
	$('#attachedDAList').css("background-color", "");
	
	
	var counter				=	0;
	var focuscount			=	0;
	var assessmentDocument	=	fileName;
		
	var jobcategoryName=trim(document.getElementById('jobcategory').value);
	var jobCategoryId=trim(document.getElementById('jobcategoryId').value);
	var districtId=trim(document.getElementById('districtId').value);
	var entityID=trim(document.getElementById('entityID').value);
	var offerDistrictSpecificItems=document.getElementById('offerDistrictSpecificItems').checked;
	var offerQualificationItems=document.getElementById('offerQualificationItems').checked;
	var offerJSI=document.getElementById('offerJSI').checked;
	var offerPortfolioNeeded=document.getElementById('offerPortfolioNeeded').checked;
	/***add by ankit*****/
	var imepicheck2=document.getElementById('imepicheck2').checked;
	var imofferPortfolioNeeded=document.getElementById('imofferPortfolioNeededim').checked;
	var imofferDistrictSpecificItems=document.getElementById('imofferDistrictSpecificItems').checked
	var imofferQualificationItems=document.getElementById('imofferQualificationItems').checked;
	var imofferJSI=document.getElementById('imofferJSI').checked;
	//SWADESH
	var autoRejectCheck=document.getElementById('autoRejectCheck').checked;
	var subjobcategoryName=trim(document.getElementById('subjobcategory').value);
	var parentJobCategoryId="";
	var attachDSPQFromJC=document.getElementById('attachDSPQFromJC').checked;
	var attachSLCFromJC=document.getElementById('attachSLCFromJC').checked;
	var approvalByPredefinedGroups=document.getElementById('approvalByPredefinedGroups').checked;
	var assessmentGroupId = null;
	
	if (subjobcategoryName!=""){
		jobcategoryName=subjobcategoryName;
		parentJobCategoryId=$('#parentJobCategoryId').val();
	}

	
	/****add end by ankit*****/
	
	var baseStatus;
	var epiForFullTimeTeachers;
	
	// Start Reference Check
	try{
		var questionSetVal = document.getElementById('avlbList').value;
	} catch(e){}
	// End Reference Check
	
	if(document.getElementById('epicheck').checked)
		baseStatus=true;
	else
		baseStatus=false;
	
	if(document.getElementById('epicheck2').checked)
		epiForFullTimeTeachers=true;
	else
		epiForFullTimeTeachers=false;
	
	if(offerJSI == true){
		var offerJSIRadioMaindatory	=	document.getElementById('offerJSIRadioMaindatory').checked;
		var offerJSIRadioOptional	=	document.getElementById('offerJSIRadioOptional').checked;
		if(offerJSIRadioMaindatory == true){
			offerJSI = 1;
		} else if(offerJSIRadioOptional == true){
			offerJSI = 2;
		}
		
	} else if(offerJSI == false){
		offerJSI = 0;
	}
	/******add by ankit*******/
	if(imofferJSI == true){
		var imofferJSIRadioMaindatory	=	document.getElementById('imofferJSIRadioMaindatory').checked;
		var imofferJSIRadioOptional	=	document.getElementById('imofferJSIRadioOptional').checked;
		if(imofferJSIRadioMaindatory == true){
			imofferJSI = 1;
		} else if(imofferJSIRadioOptional == true){
			imofferJSI = 2;
		}
		
	} else if(imofferJSI == false){
		imofferJSI = 0;
	}
	/**********/
	
	if($('#assessmentGroupId').is(":visible") && $('#assessmentGroupId').val()!=0)
		assessmentGroupId = $('#assessmentGroupId').val();
	else
		assessmentGroupId = null;
	
	
	/* For VVI*/
	
	/* For VVI */
	var offerVVIForInternal=document.getElementById('offerVVIForInternal').checked;
	var offerVirtualVideoInterview=document.getElementById('offerVirtualVideoInterview').checked;
	var quesId="";
	var quesName="";
	var maxScoreForVVI="";
	var sendAutoVVILink="";
	var slctStatusID="";
	var timePerQues="";
	var vviExpDays="";
	var jobCompletedVVILink="";
	var jobCompletedVVILink1="";
	var jobCompletedVVILink2="";
	
	var slctStatusID="";
	
	if(offerVirtualVideoInterview==true)
	{
		wantScore=document.getElementById('wantScore').checked;
		sendAutoVVILink=document.getElementById('sendAutoVVILink').checked;
		timePerQues=trim(document.getElementById('timePerQues').value);
		vviExpDays=trim(document.getElementById('vviExpDays').value);
		quesId=trim(document.getElementById('quesId').value);
		quesName=trim(document.getElementById('quesName').value);
		jobCompletedVVILink1=document.getElementById('jobCompletedVVILink1').checked;
		jobCompletedVVILink2=document.getElementById('jobCompletedVVILink2').checked;
		
		
		if(jobCompletedVVILink1==true)
			jobCompletedVVILink = false;
		else if(jobCompletedVVILink2==true)
			jobCompletedVVILink = true;
		
		
		/*if(quesName=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgPleaseQuestionSet+"<br>");
			$('#quesName').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}*/
		
		if(wantScore==true)
		{
			maxScoreForVVI=trim(document.getElementById('maxScoreForVVI').value);
			/*if(maxScoreForVVI=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msgPleaseMaxScore+"<br>");
				$('#maxScoreForVVI').css("background-color", "#F5E7E1");
				counter++;
				focuscount++;
			}*/
		}
		
		if(sendAutoVVILink==true)
			slctStatusID=trim(document.getElementById('slctStatusID').value);
	}
	
	
	var offerAssessmentInviteOnly=document.getElementById('offerAssessmentInviteOnly').checked;
	var opts = "";
	var arrDAIds = "";
	if(offerAssessmentInviteOnly)
	{
		opts = document.getElementById('attachedDAList').options;
		
		for(var i = 0, j = opts.length; i < j; i++)
		{
			arrDAIds+=opts[i].value+"#";
		}
		
		if(document.getElementById("attachedDAList").options.length==0)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgleastoneDistrictAssessment+"<br>");
			$('#attachedDAList').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}
	}
	
	
	if(!$('#parentJobCategoryId').prop('disabled')){
		if($('#parentJobCategoryId').val()=="0"){
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgPlsEnterJobCategory+"<br>");
			if(focuscount	==	0)
				$('#parentJobCategoryId').focus();
				$('#parentJobCategoryId').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}else if (subjobcategoryName=="")
		{	
			$('#errordiv').show();
			$('#errordiv').append("&#149; "+resourceJSON.msgSubJobCategory+"<br>");
			if(focuscount	==	0)
				$('#subjobcategory').focus();
				$('#subjobcategory').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}
		if (subjobcategoryName!=""){
			jobcategoryName=subjobcategoryName;
			parentJobCategoryId=$('#parentJobCategoryId').val();
		}
	}
	
	if($('#assessmentGroupId').is(":visible") && $('#assessmentGroupId').val()==0){
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please Select EPI Group<br>");
		if(focuscount==0)
			$('#assessmentGroupId').focus();
			$('#assessmentGroupId').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	var qquestionSet = document.getElementById("qqAvlbList").value;		

	var approvalBeforeGoLive=$("#approvalBeforeGoLive1").is(":checked");
	var noOfApprovalNeeded=$("#noOfApprovalNeeded").val();
	var buildApprovalGroup=$("#buildApprovalGroups").is(":checked");
	var schoolSelection = $("#schoolSelection").is(":checked");
	var qqSetForOnbord = document.getElementById("qqAvlbListForOnboard").value;

	JobCategoryAjax.saveJobCategory(jobCategoryId,jobcategoryName,districtId,baseStatus,epiForFullTimeTeachers,assessmentDocument,offerDistrictSpecificItems,offerQualificationItems,offerJSI,offerPortfolioNeeded,offerVVIForInternal,offerVirtualVideoInterview,quesId,maxScoreForVVI,sendAutoVVILink,slctStatusID,timePerQues,vviExpDays,offerAssessmentInviteOnly,arrDAIds,questionSetVal,imepicheck2,imofferPortfolioNeeded,imofferDistrictSpecificItems,imofferQualificationItems,imofferJSI,schoolSelection,qquestionSet,parentJobCategoryId,qqSetForOnbord,approvalBeforeGoLive,noOfApprovalNeeded,buildApprovalGroup,attachDSPQFromJC,attachSLCFromJC,approvalByPredefinedGroups,assessmentGroupId,jobCompletedVVILink,{ 
		async: true,
		callback: function(data){
		
			if(data=="true"){
				$('#errordiv').show();
				$('#errordiv').append("&#149; "+resourceJSON.msgDuplicateJobCategory+"<br>");
				if(counter	==	0)
					$('#jobcategory').focus();
					$('#jobcategory').css("background-color", "#F5E7E1");
				counter++;
			}else{
				cancelJobCateDiv();
				addOrUpdateDistrictApprovalGroupJobCategorywise(data);
			}	
			$('#loadingDiv').hide();
			displayJobCategories();
		},
	});
}

var qqAddFlag = true;
function editJobCategory(jobCategoryId)
{
	$('#errordiv').empty();
	$('#jobcategory').css("background-color", "");
	$('#districtName').css("background-color", "");
	$('#quesName').css("background-color", "");
	$('#maxScoreForVVI').css("background-color", "");
	
	addNewJobCategory();
	//alert("qqAddFlag : "+qqAddFlag);
	$('#loadingDiv').show();
	JobCategoryAjax.getJobCategoryId(jobCategoryId,{
		async: true,
		callback: function(data){
		document.getElementById('jobcategoryId').value=data[1].jobCategoryId;
		document.getElementById("schoolSelection").checked = data[1].schoolSelection;
		if(data[1].districtMaster!=null){
			if(document.getElementById('entityID').value==1){
				document.getElementById('districtName').value=data[1].districtMaster.districtName;
			}
			document.getElementById('districtId').value=data[1].districtMaster.districtId;
			var districtId	= data[1].districtMaster.districtId;
			
			if(data[1].jobCategoryId!=null && data[1].districtMaster.districtId!=null)
				$('#jsiDiv').fadeIn();
			/* ====== Gagan : jsiDiv only in case of when districtId is available  =========*/ 
			if(data[1].assessmentDocument!=null && data[1].assessmentDocument!="") {	
				document.getElementById("removeJsi").style.display="block";
				document.getElementById("divJsiName").style.display="block";
				document.getElementById("jsiFileName").value=data[1].assessmentDocument;
				document.getElementById("divJsiName").innerHTML="<a href='javascript:void(0)' id='hrefEditRef' onclick=\"downloadJsi('"+data[1].jobCategoryId+"','hrefEditRef');" +
				"if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');"+
				"return false;\">view</a>";
				
				}else{
				document.getElementById("divJsiName").style.display="none";
				document.getElementById("removeJsi").style.display="none";
				document.getElementById("jsiFileName").value="";
			}
									
			length=1;
		}
		if(districtId!=null && districtId!=""){
			getQQuestionSetByDistrictAndJobCategoryList();
			getQQSetForOnboardingByDistrictAndJobCategoryList();
		}
		if(data[1].parentJobCategoryId!=null && data[1].parentJobCategoryId.jobCategoryId!=0){
			$("#jobCatRadio2").prop("checked",true);
			getJobCategoryByDistrictId();
			document.getElementById('subjobcategory').value=data[1].jobCategoryName;
			
			if(data[1].attachDSPQFromJC==true){
				document.getElementById('attachDSPQFromJC').checked=true;;
			}
			if(data[1].attachSLCFromJC==true){
				document.getElementById('attachSLCFromJC').checked=true;
			}
			
			
			$("#jobcategory").prop("disabled",true);
			$("#parentJobCategoryId").prop("disabled",false);
			$("#subJobCatDiv").show();
		//	$("#parentJobCategoryId").val(data.parentJobCategoryId);
		}else{
			document.getElementById('jobcategory').value=data[1].jobCategoryName;
			$("#subjobcategory").prop("disabled",false);
		}
		document.getElementById("epicheck").checked = data[1].baseStatus;
		if(document.getElementById("epicheck").checked || document.getElementById("epicheck2").checked || document.getElementById("imepicheck2").checked){
			getEpiGroups();
			if(data[1].assessmentGroupId!=null){
				document.getElementById('assessmentGroupId').value=data[1].assessmentGroupId;
			}
			$('.epiGroupDiv').show();
		}
		document.getElementById("epicheck2").checked = data[1].epiForFullTimeTeachers;
		document.getElementById("offerDistrictSpecificItems").checked = data[1].offerDistrictSpecificItems;
		document.getElementById("offerQualificationItems").checked = data[1].offerQualificationItems;
		//document.getElementById("offerJSI").checked = data.offerJSI;
		document.getElementById("offerPortfolioNeeded").checked = data[1].offerPortfolioNeeded;
		$('#JSIRadionDiv').hide();
		/*********add by ankit***************/
		document.getElementById("imepicheck2").checked = data[1].epiForIMCandidates;
		document.getElementById("imofferDistrictSpecificItems").checked = data[1].districtSpecificItemsForIMCandidates;
		document.getElementById("imofferQualificationItems").checked = data[1].qualificationItemsForIMCandidates;
		document.getElementById("imofferPortfolioNeededim").checked = data[1].portfolioForIMCandidates;
		
			
		/*********add end by ankit***************/

			if(data[1].offerJSI == 0){
				document.getElementById("offerJSI").checked = false; 
				$('#JSIRadionDiv').hide();
			} else {
				$('#JSIRadionDiv').show();
				document.getElementById("offerJSI").checked = true;
				if(data[1].offerJSI == 1){
					document.getElementById("offerJSIRadioMaindatory").checked = true;
				} else if(data[1].offerJSI == 2){
					document.getElementById("offerJSIRadioOptional").checked = true;
				}
			}
			
			/**********add by ankit*****/
			if(data[1].jsiForIMCandidates == 0){
				document.getElementById("imofferJSI").checked = false; 
				$('#imJSIRadionDiv').hide();
			} else {
				$('#imJSIRadionDiv').show();
				document.getElementById("imofferJSI").checked = true;
				if(data[1].jsiForIMCandidates == 1){
					document.getElementById("imofferJSIRadioMaindatory").checked = true;
				} else if(data[1].jsiForIMCandidates == 2){
					document.getElementById("imofferJSIRadioOptional").checked = true;
				}
			}
			/***********add end by ankit************/
		
			$( "#offerJSI" ).on( "click", function( event ) {
			  	if(document.getElementById("offerJSI").checked == true){
			  		$('#JSIRadionDiv').show();
			  		if(data[1].offerJSI == 1){
						document.getElementById("offerJSIRadioMaindatory").checked = true;
					} else if(data[1].offerJSI == 2){
						document.getElementById("offerJSIRadioOptional").checked = true;
					}
			  	} else {
			  		$('#JSIRadionDiv').hide();
			  	}
			 });
			
			/********add by ankit***********/
			$( "#imofferJSI" ).on( "click", function( event ) {
			  	if(document.getElementById("imofferJSI").checked == true){
			  		$('#imJSIRadionDiv').show();
			  		if(data[1].jsiForIMCandidates== 1){
						document.getElementById("imofferJSIRadioMaindatory").checked = true;
					} else if(data[1].jsiForIMCandidates== 2){
						document.getElementById("imofferJSIRadioOptional").checked = true;
					}
			  	} else {
			  		$('#imJSIRadionDiv').hide();
			  	}
			 });
			/********add end by ankit***********/
			
			//SWADESH
			try{
			if(data[1].autoRejectScore!=null && data[1].autoRejectScore!=''){
			$("#lblautoCheckSlider").html("<iframe id='autoCheckSlider' src='slideract.do?name=autoReject&tickInterval=10&max=100&swidth=250&svalue="+data[1].autoRejectScore+"' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:290px;'></iframe>");
			document.getElementById("autoRejectCheck").checked = true;
			$('#autoRejectDiv').show();
			}
			else{
			$("#lblautoCheckSlider").html("<iframe id='autoCheckSlider' src='slideract.do?name=autoReject&tickInterval=10&max=100&swidth=250&svalue=0' scrolling='no' frameBorder='0' style='border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:290px;'></iframe>");
			document.getElementById("autoRejectCheck").checked = false;
			$('#autoRejectDiv').hide();
			}
			}catch(e){}
				
			if(document.getElementById("epicheck").checked == true) {
				$("#epicheck2Span").show();
			} else {
				//$("#epicheck2Span").hide();
			}
			
			//For District Assessment
			try
			{
				if(data[1].offerAssessmentInviteOnly==true)
				{
					$("#offerDistASMTDiv").show();
					document.getElementById('offerAssessmentInviteOnly').checked=true;
			
					document.getElementById('assessmentId').value=data[1].offerAssessmentInviteOnly;
					document.getElementById('assessmentName').value=data[1].districtAssessmentDetail.districtAssessmentName;
				}
			}catch(err){}
			
			
			/* For VVI*/
			try
			{
				// get All Status List
				getStatusList();
				if(data[1].offerVirtualVideoInterview==true)
				{
					$("#offerVVIDiv").show();
					document.getElementById('offerVirtualVideoInterview').checked=true;
					document.getElementById('quesId').value=data[1].i4QuestionSets.ID;
					document.getElementById('quesName').value=data[1].i4QuestionSets.questionSetText;
					
					if(data[1].maxScoreForVVI!=null && data[1].maxScoreForVVI!="")
					{
						$("#marksDiv").show();
						document.getElementById('wantScore').checked=true;
						document.getElementById('maxScoreForVVI').value=data[1].maxScoreForVVI;
					}
					else
					{
						$("#marksDiv").hide();
						document.getElementById('wantScore').checked=false;
					}
					
					if(data[1].sendAutoVVILink==true)
					{
						$("#autolinkDiv").show();
						document.getElementById('sendAutoVVILink').checked=true;
						
						if(data[1].jobCompletedVVILink==null){
							document.getElementById('jobCompletedVVILink1').checked=false;
							document.getElementById('jobCompletedVVILink2').checked=false;
						}else if(data[1].jobCompletedVVILink==false){
							document.getElementById('jobCompletedVVILink1').checked=true;
							document.getElementById('jobCompletedVVILink2').checked=false;
							var itemList = document.getElementById('slctStatusID');
							
							if(data[1].statusIdForAutoVVILink!=null)
							{
								for (var i = 0; i < itemList.length; i++) 
								{
									if (itemList.options[i].value== data[1].statusIdForAutoVVILink) {
										itemList.options[i].selected = true;
										break;
									}
								}
							}else if(data[1].secondaryStatusIdForAutoVVILink!=null)
							{
								for (var j = 0; j < itemList.length; j++) 
								{
									if (itemList.options[j].value== "SSID_"+data[1].secondaryStatusIdForAutoVVILink) {
										itemList.options[j].selected = true;
										break;
									}
								}
							}
						}else if(data[1].jobCompletedVVILink==true){
							document.getElementById('jobCompletedVVILink1').checked=false;
							document.getElementById('jobCompletedVVILink2').checked=true;
							document.getElementById('slctStatusID').value="";
						}
							
					}else
					{
						document.getElementById('offerVirtualVideoInterview').checked=false;
						$("#autolinkDiv").hide();
					}
					
					document.getElementById('timePerQues').value=data[1].timeAllowedPerQuestion;
					document.getElementById('vviExpDays').value=data[1].VVIExpiresInDays;
					
				}else
				{
					document.getElementById('offerVirtualVideoInterview').checked=false;
					$("#offerVVIDiv").hide();
				}
			}catch(err){}
			
			//Checked and uncked Approval Process
			try
			{
				var approvalBeforeGoLive322 =  data[1].approvalBeforeGoLive;
				var noOfApprovalNeeded322 = data[1].noOfApprovalNeeded;
				var buildApprovalGroup322 = data[1].buildApprovalGroup;
				var districtId= data[1].districtMaster.districtId;
				var jobcategoryId = data[1].jobCategoryId;
				
				$("#ApprovalDistrictId").val(districtId);
				$("#ApprovalJobCategoryId").val(jobcategoryId);
				$("#ApprovalNoOfApprovalNeeded").val(noOfApprovalNeeded322);
				$("#ApprovalBuildApprovalGroup").val(buildApprovalGroup322);
				$("#ApprovalApprovalBeforeGoLive").val(data[1].approvalBeforeGoLive322);
				
				if(data[1].approvalByPredefinedGroups==true)
					$("#approvalByPredefinedGroups").prop('checked',true);
				else
					$("#approvalByPredefinedGroups").prop('checked',false);
				
				$("#ApprovalGroupFromEditFlag").val("true");
				
				if(approvalBeforeGoLive322==true)
				{
					$("#approvalBeforeGoLive1").attr("checked","checked");
				}
				else
				{
					$("#approvalBeforeGoLive1").removeAttr("checked");
						setTimeout(function(){enableDisableApprovals();},1000);	
				}
				
				if(buildApprovalGroup322==true)
				{
					$("#buildApprovalGroups").attr("checked","checked");
					//alert("buildApprovalGroup322 is checked");
				}
				else
				{
					$("#buildApprovalGroup").removeAttr("checked");
				}
				
				if(noOfApprovalNeeded322!=null)
				{
					$("#noOfApprovalNeeded option[value="+noOfApprovalNeeded+"]").attr("selected","selected");
					if(approvalBeforeGoLive322==true)
						$("#noOfApprovalNeeded").attr("disabled","disabled");
					else
						$("#noOfApprovalNeeded").removeAttr("disabled");
				}
				
				//alert("jobcategoryId:- "+jobcategoryId+"\ndistrictId:- "+districtId+"\napprovalBeforeGoLive322:- "+approvalBeforeGoLive322+"\nnoOfApprovalNeeded322:- "+noOfApprovalNeeded322+"\nbuildApprovalGroup322:- "+buildApprovalGroup322);
				$("#groupApprovalDiv").show();
				getApprovalGroupsInfo(districtId,jobcategoryId);
				$("#noOfApprovalNeeded").val(noOfApprovalNeeded322);
			}
			catch(err){}
			
			//For District Assessment
			try
			{
				getReferenceByDistrictList();
				displayAllDistrictAssessment();
				
			}catch(err){}
			if(data[0]=="A")
			{
				$("#offerPortfolioNeededjs").css("display", "none"); 
				$("#imofferPortfolioNeededimjs").css("display", "none"); 
			}
			else
			{
				$("#offerPortfolioNeededjs").css("display", "block"); 
				$("#imofferPortfolioNeededimjs").css("display", "block"); 
			}
			
			$('#loadingDiv').hide();
		},
	});
}

function activateDeactivateJobCategory(jobcatId,status)
{	
	JobCategoryAjax.activateDeactivateJobCategory(jobcatId,status,{ 
		async: true,
		callback: function(data){
			displayJobCategories();
		},
	});
}

function removeJsi()
{
	var jobcategoryId = document.getElementById("jobcategoryId").value;
	
	$('#message2showConfirm').html(resourceJSON.msgWouldLikeRemoveJSI);
	$('#footerbtn').html("<button class='btn btn-primary' onclick=\"removeJSIAttachment('"+jobcategoryId+"')\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#myModal3').modal('show');
	
}

function removeJSIAttachment(jobcategoryId)
{
		$('#myModal3').modal('hide');
		JobCategoryAjax.removeJsi(jobcategoryId,{ 
			async: false,
			callback: function(data){
				
				if(data==1)
				{
					document.getElementById("removeJsi").style.display="none";
					//hideForm_Certification();
					//showGridCertifications();
					//validatePortfolioErrorMessageAndGridData('level2');
					document.getElementById('assessmentDocument').value="";
					document.getElementById('jsiFileName').value="";
					displayJobCategories();
					
				}else if(data==3)
				{
					$('#myModal2').modal('show');
					$('#message2show').html(resourceJSON.msgStatusAttachedJSI);
				}
			},errorHandler:handleError
		});
	
}

function showSubEpi()
{
	document.getElementById("epicheck2").checked = false;
	
	if(document.getElementById("epicheck").checked == true)
	{
		$("#epicheck2Span").show();
	}
	else
	{
		//$("#epicheck2Span").hide();
	}
}






/********************************** district auto completer ***********************************/

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
var districtNameFilter=0;

function getRefreshDistrictName(){
	 count=0;
	 index=-1;
	 length=0;
	 divid='';
	 txtid='';
	 hiddenDataArray = new Array();
	 showDataArray = new Array();
	 degreeTypeArray = new Array();
	 hiddenId="";
	 districtNameFilter=0;
}

function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	getRefreshDistrictName();
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		if(districtNameFilter==1)
			document.getElementById("districtNameFilter").focus();
		else
			document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	ManageJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
	errorHandler:handleError 
	});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}


/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgSomeerror+": "+exception.javaClassName);}
}

/* ========= Start :: Virtual Video Interview ============*/
function showOfferVVI()
{
	var offerVVI 		= 	document.getElementById("offerVirtualVideoInterview").checked;
	
	if(offerVVI==true)
		$("#offerVVIDiv").show();
	else
		$("#offerVVIDiv").hide();
}

function showMarksDiv()
{
	var wantScore 		= 	document.getElementById("wantScore").checked;
	var maxScoreForVVI 	= 	document.getElementById("maxScoreForVVI").value;
		
	if(wantScore==true)
		$("#marksDiv").show();
	else
		$("#marksDiv").hide();
	
	if(maxScoreForVVI!="")
	{
		document.getElementById("wantScore").checked=true;
		$("#marksDiv").show();
	}
	
}

function showLinkDiv()
{
	var sendAutoVVILink 	= 	document.getElementById("sendAutoVVILink").checked;
	
	if(sendAutoVVILink==true)
		$("#autolinkDiv").show();
	else
		$("#autolinkDiv").hide();
	
}


function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function getStatusList()
{
	var districtId = trim(document.getElementById("districtId").value);
	if(districtId!="")
	JobCategoryAjax.getStatusList(districtId,{ 
		async: false,
		callback: function(data)
		{
			if(data!=null)
				document.getElementById("statusDiv").innerHTML=data;	
		},errorHandler:handleError
	});
}

function getVVIDefaultFields()
{
	var districtId = trim(document.getElementById("districtId").value);
	var jobcategoryId = trim(document.getElementById("jobcategoryId").value);
	
	if(districtId!="" && jobcategoryId=="")
	{
		//clearVVIFields();
		$('#loadingDiv').show();
		
		JobCategoryAjax.getVVIDefaultFields(districtId,{ 
			async: true,
			callback: function(data)
			{
				if(data!=null)
				{
					if(data.approvalByPredefinedGroups==true)
						document.getElementById('approvalByPredefinedGroups').checked=true;
					else
						document.getElementById('approvalByPredefinedGroups').checked=false;
					
					//For VVI
					if(data.offerVirtualVideoInterview==true)
					{
						// get All Status List
						getStatusList();
						$("#offerVVIDiv").show();
						document.getElementById('offerVirtualVideoInterview').checked=true;
						
						document.getElementById('quesId').value=data.i4QuestionSets.ID;
						document.getElementById('quesName').value=data.i4QuestionSets.questionSetText;
						
						if(data.maxScoreForVVI!=null && data.maxScoreForVVI!="")
						{
							$("#marksDiv").show();
							document.getElementById('wantScore').checked=true;
							
							document.getElementById('maxScoreForVVI').value=data.maxScoreForVVI;
						}
						else
						{
							$("#marksDiv").hide();
							document.getElementById('wantScore').checked=false;
						}
						
						if(data.sendAutoVVILink==true)
						{
							$("#autolinkDiv").show();
							document.getElementById('sendAutoVVILink').checked=true;
							
							if(data.jobCompletedVVILink==null){
								document.getElementById('jobCompletedVVILink1').checked=false;
								document.getElementById('jobCompletedVVILink2').checked=false;
							}else if(data.jobCompletedVVILink==false){
								document.getElementById('jobCompletedVVILink1').checked=true;
								document.getElementById('jobCompletedVVILink2').checked=false;
								var itemList = document.getElementById('slctStatusID');
								
								if(data.statusMasterForVVI!=null)
								{
									for (var i = 0; i < itemList.length; i++) 
									{
										if (itemList.options[i].value== data.statusMasterForVVI.statusId) {
											itemList.options[i].selected = true;
											break;
										}
									}
								}else if(data.secondaryStatusForVVI!=null)
								{
									for (var j = 0; j < itemList.length; j++) 
									{
										if (itemList.options[j].value== "SSID_"+data.secondaryStatusForVVI.secondaryStatusId) {
											itemList.options[j].selected = true;
											break;
										}
									}
								}
							}else if(data.jobCompletedVVILink==true){
								document.getElementById('jobCompletedVVILink1').checked=false;
								document.getElementById('jobCompletedVVILink2').checked=true;
								
							}
								
						}else
						{
							$("#autolinkDiv").hide();
						}
						
						document.getElementById('timePerQues').value=data.timeAllowedPerQuestion;
						document.getElementById('vviExpDays').value=data.VVIExpiresInDays;
						
					}else
					{
						$("#offerVVIDiv").hide();
					}
				}
				$('#loadingDiv').hide();
			},errorHandler:handleError
		});
	}
}



function clearVVIFields()
{
	document.getElementById('offerVirtualVideoInterview').checked=false;
	document.getElementById('quesId').value="";
	document.getElementById('quesName').value="";
	
	document.getElementById('wantScore').checked=false;
	document.getElementById('maxScoreForVVI').value="";
	
	document.getElementById('sendAutoVVILink').checked=false;
	document.getElementById('jobCompletedVVILink1').checked=false;
	document.getElementById('jobCompletedVVILink2').checked=false;
	document.getElementById('slctStatusID').value = "";
	
	document.getElementById('timePerQues').value="";
	document.getElementById('vviExpDays').value="";
}

/* ========= End :: Virtual Video Interview ============*/


/************************************* Question Set auto complete**********************************************/
function getQuestionSetAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("quesName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//searchArray = getDistrictMasterArray(txtSearch.value);
		searchArray = getQuesSetArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


function getQuesSetArray(quesText)
{
	var searchArray = new Array();
	
	var districtId = trim(document.getElementById("districtId").value);
	
	if(districtId!="")
	{
		I4QuestionSetAjax.getFieldOfQuesList(quesText,districtId,{ async: false,callback: function(data)
			{
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++)
				{
					searchArray[i]=data[i].questionSetText;
					showDataArray[i]=data[i].questionSetText;
					hiddenDataArray[i]=data[i].ID;
				}
			},
		});	
	}

	return searchArray;
}
/*************************************************************************************************/
/************************************* District Assessment auto complete**********************************************/
function getDistrictAssessmentAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("assessmentName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		//searchArray = getDistrictMasterArray(txtSearch.value);
		searchArray = getDistrictAssessmentArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}


function getDistrictAssessmentArray(distASMTName)
{
	var searchArray = new Array();
	
	var districtId = trim(document.getElementById("districtId").value);
	if(districtId!="")
	{
		DistrictAssessmentAjax.getFieldOfDistASMT(distASMTName,districtId,{ async: false,callback: function(data)
			{
				hiddenDataArray = new Array();
				showDataArray = new Array();
				for(i=0;i<data.length;i++)
				{
					searchArray[i]=data[i].districtAssessmentName;
					showDataArray[i]=data[i].districtAssessmentName;
					hiddenDataArray[i]=data[i].districtAssessmentId;
				}
			},
		});
	}

	return searchArray;
}

/*
function hideDistrictASMTDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{	
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}*/

/*************************************************************************************************/

function displayAllDistrictAssessment()
{
	var districtId=document.getElementById("districtId").value;
	var jobcategoryId=document.getElementById("jobcategoryId").value;
	
	if(districtId==null || districtId=="")
		districtId=0;
	if(districtId>0)
	{
		JobCategoryAjax.displayAllDistrictAssessment(districtId,jobcategoryId,
				{ 
					async: true,
					errorHandler:handleError,
					callback: function(data)
					{
						var dataArr=data.split('||');
						document.getElementById("1stSchoolDiv").innerHTML=dataArr[0];
						document.getElementById("2ndSchoolDiv").innerHTML=dataArr[1];
						
						if(document.getElementById("districtId").value!='')
						{
							if(dataArr[2]=='true')
							{
								document.getElementById("offerAssessmentInviteOnly").checked=true;
								$('#multiDisAdminDiv').show();
							}
							else
							{
								document.getElementById("offerAssessmentInviteOnly").checked=false;
								$('#multiDisAdminDiv').hide();
							}
						}
						else
						{
							document.getElementById("offerAssessmentInviteOnly").checked=false;
							$('#multiDisAdminDiv').hide();
						}
							
					}
				});
	}
	
}


function showOfferAMT()
{
	var offerVVI 		= 	document.getElementById("offerAssessmentInviteOnly").checked;
	
	if(offerVVI==true)
		$("#multiDisAdminDiv").show();
	else
		$("#multiDisAdminDiv").hide();
}
// Reference Check
/*function getReferenceByDistrictList() {
	var districtId = trim(document.getElementById("districtId").value);
	if(districtId!="")
	JobCategoryAjax.getReferenceByDistrictList(districtId,{ 
		async: false,
		callback: function(data)
		{
			if(data!=null)
				document.getElementById("avlbList").innerHTML=data;	
		},errorHandler:handleError
	});
}*/
function getReferenceByDistrictList() {
	try{
		var districtId 		= 	trim(document.getElementById("districtId").value);
		var jobcategoryId	=	document.getElementById("jobcategoryId").value;
	}catch(e){}
	
	if(districtId!="")
	JobCategoryAjax.getReferenceByDistrictList(districtId,jobcategoryId,{ 
		async: false,
		callback: function(data)
		{
			if(data!=null)
				document.getElementById("avlbList").innerHTML=data;	
		},errorHandler:handleError
	});
}

function getReferenceByDistrictAndJobCategoryList() {
	try{
		var districtId 		= 	trim(document.getElementById("districtId").value);
		var jobcategoryId	=	document.getElementById("jobcategoryId").value;
	}catch(e){}
	if(districtId!="")
	JobCategoryAjax.getReferenceByDistrictAndJobCategoryList(districtId,jobcategoryId,{ 
		async: false,
		callback: function(data)
		{
			if(data!=null)
				document.getElementById("attachedList").innerHTML=data;	
		},errorHandler:handleError
	});
}

function getQQuestionSetByDistrictAndJobCategoryList(){
	try{
		var districtId = trim(document.getElementById("districtId").value);
		var jobCategoryId = trim(document.getElementById("jobcategoryId").value);
		
		JobCategoryAjax.getQQuestionSetByDistrictAndJobCategoryList(districtId, jobCategoryId,{
			async:true,
			callback:function(data){

			if(data!=null)
				document.getElementById("qqAvlbList").innerHTML=data;	

		},errorHandler:handleError
		});
	}catch(e){e}
    
}

$(function(){
	$("#jobCatRadio1").click(function() {
		$("#parentJobCategoryId").prop("disabled",true);
		$("#parentJobCategoryId").val("0");
		$("#jobcategory").prop("disabled",false);
		$("#subJobCatDiv").hide();
	});
	$("#jobCatRadio2").click(function() {
		$('#errordiv').empty();
		var districtId=trim(document.getElementById('districtId').value);
		if(districtId!=""){
			$("#parentJobCategoryId").prop("disabled",false);
			$("#jobcategory").prop("disabled",true);
			$("#jobcategory").val("");
			getJobCategoryByDistrictId();
		}else{
			$("#jobCatRadio2").prop("checked",false);
			$("#jobCatRadio1").prop("checked",true);			
			$('#errordiv').append(resourceJSON.msgDistrictNamejobcategory);
		}
		
		
	});
	$("#parentJobCategoryId").change(function(){
		var str = "";
		$("#subjobcategory").val("");
	    $( "#parentJobCategoryId option:selected" ).each(function() {
	      str= $( this ).val();
	    });
	    $("#subJobCatDiv").hide();
	    if(str!="0"){
	    	$("#subJobCatDiv").show();
	    }
	});
});
function getJobCategoryByDistrictId(){
	try{
		var districtId 	= 	trim(document.getElementById("districtId").value);
		var jobSubCate = 	$("#jobcategoryId").val();
	
		try{
			if(districtId==804800)
				$('#approvalBPGroupsDiv').show()
			else
				$('#approvalBPGroupsDiv').hide()
		}catch(e){}
		
		
		if($("#jobcategoryId").val()=="")
			jobSubCate = 0 ;
	}catch(e){}
	if(districtId!="")
	JobCategoryAjax.getJobCategoryByDistrictId(districtId,jobSubCate,{ 
		async: false,
		callback: function(data)
		{
			if(data!=null)
				$("#parentJobCategoryId").html(data);	
		},errorHandler:handleError
	});
}
function getQQSetForOnboardingByDistrictAndJobCategoryList(){
	try{ 
		var districtId = trim(document.getElementById("districtId").value);
		var jobCategoryId = trim(document.getElementById("jobcategoryId").value);
		JobCategoryAjax.getQQSetForOnboardingByDistrictAndJobCategoryList(districtId, jobCategoryId,{
			async:true,
			callback:function(data){

			if(data!=null)
				document.getElementById("qqAvlbListForOnboard").innerHTML=data;	

		},errorHandler:handleError
		});
	}catch(e){e}
    
}





function hideById(itemId){
	$("#"+itemId).hide();
	
}

function showHistory(jobCatId){
	$("#jobCategoryHistoryModalId").show();
	//alert("111111");
	document.getElementById("divJobHisTxt").innerHTML= '<img src="images/loadingAnimation.gif" style="margin-left:45%;">';
	
	try{
	HqJobCategoryAjax.getDataFromJobcategoryMasterHistory(jobCatId,{
		async:false,
		callback:function(data){
		if(data!=null){
			document.getElementById("divJobHisTxt").innerHTML=data;	

		}

	},errorHandler:handleError
	});
	}catch(e){}
}

function showHideEpiGroup()
{
	var epicheck = document.getElementById("epicheck").checked;
	var epicheck2 = document.getElementById("epicheck2").checked;
	var imepicheck2 = document.getElementById("imepicheck2").checked;
	if(epicheck)
	{
		$('.epiGroupDiv').show();
	}
	else if(epicheck2)
	{
		$('.epiGroupDiv').show();
	}
	else if(imepicheck2)
	{
		$('.epiGroupDiv').show();
	}
	else
	{
		$('.epiGroupDiv').hide();
	}
}

function getEpiGroups(){
	JobCategoryAjax.getEpiGroups({
		async: false,
		callback: function(data)
		{
			if(data!=null)
				$("#assessmentGroupId").html(data);	
		},errorHandler:handleError
	});
}


function showHideVVILink(txt){
	
	if(txt==false){
		 document.getElementById("jobCompletedVVILink1").disabled=false;
		 document.getElementById("slctStatusID").disabled=false;
	}else if(txt==true){
		 document.getElementById("jobCompletedVVILink1").checked=false;
		 document.getElementById("slctStatusID").value="";
		 document.getElementById("slctStatusID").disabled=true;
	}
}