function addReferenceInstruction()
{
	$('#eRefInstructions').find(".jqte_editor").css("background-color", "");
	$('#eRefInstructions').find(".jqte_editor").text("");
	$('#QIerrordiv').empty();
	$('#eRefInstructionModel').modal('show');
		
}
function deleteInstruction()
{
	$('#deleteInstructionModel').modal('show');
}  
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else if(exception.javaClassName=='undefined'){}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function searchData()
{	
	$("#tblGrid").show();
	$("#qdetails").show();
	$("#tblGridQInsID").show();
	$("#managedqCLine").show();
	$("#addQuestionInstrunction").show();
	$("#managedqIcon").show();
	$("#managedqText").show();
	
	$('#districtName').css("background-color", "");
	if($('#districtId').val()==0 && $("#entityType").val()!=5)			   // change @ Anurag
	{	$("#tblGrid").hide();
		$("#qdetails").hide();
		$("#tblGridQInsID").hide();
		$("#managedqCLine").hide();
		$("#addQuestionInstrunction").hide();
		$("#managedqIcon").hide();
		
		$('#errordiv').html("&#149; "+resourceJSON.PlzEtrAnyDistrictName+"<br>");
		$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
	 	 
	}
	
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
	
	$('#closePan').html("<a style='padding-right: 6px;' onclick='hideSearchPan();' href='javascript:void(0);' ><i class='icon-remove icon-large' ></i></a>");
	$('#addQuestion').html("<a href='referencecheckspecificquestions.do?districtId="+$('#districtId').val()+"&headQuarterMasterId="+$('#headQuarterId').val()+"' >+ "+resourceJSON.MszAddQuestion+"</a>");
	getReferenceInstruction();
	getDistrictQuestions();
	 
}

function saveReferenceInstruction() {
	var qins;
	var flag=0;
	$('#QIerrordiv').empty();
	$('#eRefInstructions').find(".jqte_editor").css("background-color", "");
	if($('#eRefInstructions').find(".jqte_editor").text().trim().length>0){
		qins=$('#eRefInstructions').find(".jqte_editor").html();
		flag=1;
	}else{
		$('#eRefInstructions').find(".jqte_editor").css("background-color", "#F5E7E1");
		$('#QIerrordiv').html("&#149; "+resourceJSON.PlzEtrInstructions+"<br>");
		$('#QIerrordiv').show();
		flag=0;
		return;
	}

	if(flag==1){
		var districtId = document.getElementById("districtId").value;
		var headQuarterId = $("#headQuarterId").val();
		ReferenceChkQuestionsAjax.saveReferenceInstruction(qins,headQuarterId,districtId,{
			async: true,
			errorHandler:handleError,
			callback: function(data){
			getReferenceInstruction();
			getDistrictQuestions();
			$('#eRefInstructionModel').modal('hide');
			$('#eRefInstructions').find(".jqte_editor").text("");
		},
		errorHandler:handleError  
		});
	}
}

function editReferenceInstruction() {

	var districtId = document.getElementById("districtId").value;
	var headQuarterId = $("#headQuarterId").val();
	ReferenceChkQuestionsAjax.loadReferenceInstruction(headQuarterId,districtId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#eRefInstructionModel').modal('show');
		$('#eRefInstructions').find(".jqte_editor").height(225);
		$('#eRefInstructions').find(".jqte_editor").html(data);
	},
	errorHandler:handleError  
	});
}

function deleteReferenceInstruction() {

	var districtId = document.getElementById("districtId").value;
	ReferenceChkQuestionsAjax.deleteReferenceInstruction(districtId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
			$('#deleteInstructionModel').modal('hide')	
			getReferenceInstruction();
			getDistrictQuestions();
	},
	errorHandler:handleError
	});
}

function getReferenceInstruction() {
	var districtId = document.getElementById("districtId").value;
 	var headQuarterId = ""; 
 	 if(document.getElementById("headQuarterId"))
 	headQuarterId =document.getElementById("headQuarterId").value;	// change @ Anurag
 	 
	ReferenceChkQuestionsAjax.getReferenceInstruction(headQuarterId,districtId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		if(data!=""){
			$('#addQuestionInstrunction').hide();
			$('#tblGridQIns').html(data);
			document.getElementById("addQuestion").setAttribute("style","margin-top:0px");
			document.getElementById("tblGridQInsID").style.display='inline';
		}else{
			document.getElementById("addQuestion").setAttribute("style","margin-top:-10px");
			$('#addQuestionInstrunction').show();
			document.getElementById("tblGridQInsID").style.display='none';
		}
	},
	errorHandler:handleError  
	});
}

function getDistrictQuestions() {       // change @ Anurag
	var districtId = document.getElementById("districtId").value;
	var headQuarterId = null;
	
	if(document.getElementById("headQuarterId"))
		headQuarterId = document.getElementById("headQuarterId").value;
	 
	ReferenceChkQuestionsAjax.getDistrictQuestions(districtId,headQuarterId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#tblGridQIns').show();
		$('#tblGrid').html(data);
	},
	errorHandler:handleError  
	});
}

function getQuestionOptions(questionTypeId) {

	var qType=findSelected(questionTypeId);
	if(questionTypeId==0 || qType=='ml') {
		for(i=0;i<=3;i++)
			$('#row'+i).hide();
			$('#rowExplain').hide();

		return;
	} else {
		for(i=0;i<=3;i++)
			$('#row'+i).show();
		$('#rowExplain').hide();
	}

	if(qType=='tf' || qType=='et') {
		$('#row2').hide();
		$('#row3').hide();
		if(qType=='et'){
			$('#rowExplain').show();
		}
		if(qType=='tf'){
			$('#rowExplain').hide();
		}
		if($('#opt1').val()=="")
			$('#opt1').attr("value","True");
		if($('#opt2').val()=="")
			$('#opt2').attr("value","False");

	} else if(qType=='slsel') {
		$('#row2').show();
		$('#row3').show();
		$('#rowExplain').hide();
	} if(qType=='SLD') {
		for(i=0;i<=3;i++)
			$('#row'+i).hide();
			$('#rowExplain').hide();
	}
}

function clearOptions() {
	
	for(i=1;i<=6;i++) {
		$('#opt'+i).attr("value","");
		$('#valid'+i).attr("checked",false);
	}
}

function redirectTo(redirectURL) {	
	var url = redirectURL+"&dt="+new Date().getTime();
	window.location.href=url;
}

function validateDistrictQuestions() {
	$('#errordiv').empty();
	$('#errordiv1').empty();
	$('#assQuestion').find(".jqte_editor").css("background-color", "");
	$('#assInstruction').find(".jqte_editor").css("background-color", "");
	$('#assExplanation').find(".jqte_editor").css("background-color", "");
	$('#questionTypeMaster').css("background-color", "");
	$('#opt1').css("background-color", "");
	var questionMaxScore = document.getElementById("questionMaxScore").value;
	var cnt=0;
	var focs=0;

	if($('#assQuestion').find(".jqte_editor").text().trim()==""){

		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrQuest+"<br>");
		if(focs==0)
			$('#assQuestion').find(".jqte_editor").focus();
		$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	} else {
		var charCount=trim($('#assQuestion').find(".jqte_editor").text());
		var count = charCount.length;
		if(count>5000) {
			$('#errordiv').append("&#149; "+resourceJSON.MsgQuestionLength+"<br>");
			if(focs==0)
				$('#assQuestion').find(".jqte_editor").focus();
			$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	
	if (trim(document.getElementById("questionTypeMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.PlzSelectQuestionType+"<br>");
		if(focs==0)
			$('#questionTypeMaster').focus();
		$('#questionTypeMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	var qType=findSelected(dwr.util.getValue("questionTypeMaster"));
	var c=0;
	var validCnt=0;
	
	if(qType == "et"){
		if($('#assExplanation').find(".jqte_editor").text().trim()==""){

			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrQuestionExplanation+"<br>");
			if(focs==0)
				$('#assExplanation').find(".jqte_editor").focus();
			$('#assExplanation').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		} else {
			var charCount=trim($('#assExplanation').find(".jqte_editor").text());
			var count = charCount.length;
			if(count>5000) {
				$('#errordiv').append("&#149; "+resourceJSON.MsgQuestionExplanationLength+"<br>");
				if(focs==0)
					$('#assExplanation').find(".jqte_editor").focus();
				$('#assExplanation').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
	}
/* END Section */
	
	if(!(qType=='ml') && !(qType=='SLD'))
		if(document.getElementById("opt1")) {
			for(i=1;i<=6;i++) {
				if(trim(document.getElementById("opt"+i).value)=="")
					c++;
				if($('#valid'+i).prop('checked') && trim(document.getElementById("opt"+i).value)!="")
				validCnt++;
			}
			if(c==5 || c==6) {
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrAtLeastTwoOptions+"<br>");
				if(focs==0)	{
					if(c==6)
						$('#opt1').focus();
					else
						$('#opt2').focus();
				}

				$('#opt1').css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}

	
	var arr =[];
	var charCount=trim($('#assInstruction').find(".jqte_editor").text());
	var count = charCount.length;
	if(count>2500) {
		$('#errordiv').append("&#149; "+resourceJSON.MsgInstructionsCannotExceed+"<br>");
		if(focs==0)
			$('#assInstruction').find(".jqte_editor").focus();
		$('#assInstruction').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	if(cnt==0)
		return true;
	else {
		$('#errordiv').show();
		return false;
	}

}
function saveQuestion(status) {
	if(!validateDistrictQuestions())
		return;

	var questionType={questionTypeId:dwr.util.getValue("questionTypeMaster")};
	var qType=findSelected(questionType.questionTypeId);
	var districtSpecificQuestions = {questionId:dwr.util.getValue("questionId"),question:null,questionExplanation:null,questionTypeMaster:questionType,questionMaxScore:null,questionInstructions:null};
	 
	 var districtMaster =null;
	if($("#districtId").val()!="")
     districtMaster = {districtId:dwr.util.getValue("districtId")};
	 
	var headQuarterMaster =null;
	if($("#headQuarterId").val()!="")
	 headQuarterMaster = {headQuarterId:dwr.util.getValue("headQuarterId")};
	 
	var questionoptions={};
	var arr =[];
	if(qType=='tf' || qType=='et') {
		
		for(i=1;i<=2;i++) {
			
			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='slsel') {

		for(i=1;i<=6;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	}  else if(qType=='mlsel') {

		for(i=1;i<=6;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='mloet') {

		for(i=1;i<=6;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='sloet') {
		for(i=1;i<=6;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	}
	var qSetId = document.getElementById("questionSetId").value;
	dwr.util.getValues(districtSpecificQuestions);
	dwr.engine.beginBatch();
	 
	if(districtMaster==null)
		districtMaster={districtId:0};
	ReferenceChkQuestionsAjax.saveDistrictQuestion(districtSpecificQuestions,districtMaster,headQuarterMaster,arr,{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
		
		   // change @ Anurag
		
		var distid="";
		var hqid="";
		if(districtMaster!=null)
			distid=districtMaster.districtId;
		if(headQuarterMaster!=null)
			hqid=headQuarterMaster.headQuarterId;
			
	 
		var hqid="";
		
			var redirectURL = "";
			if(status=="save")
				redirectURL = "referencechkquestions.do?districtId="+distid+"&headQuartertId="+hqid;
			else if(status=="continue")
				redirectURL = "referencecheckspecificquestions.do?districtId="+distid+"&headQuartertId="+hqid;	
			else if(status=="saveQues")
				redirectURL = "refChkquestionSetQuestion.do?quesSetId="+qSetId+"&districtId="+distid+"&headQuartertId="+hqid;
			
			redirectTo(redirectURL);
		}
	});

	dwr.engine.endBatch();
}

// @ Anurag
function cancelQuestion(districtId , headQuarterId) {
	var qSetId = document.getElementById("questionSetId").value
	if(qSetId==0){
	var redirectURL = "referencechkquestions.do?districtId="+districtId+"&headQuarterId="+headQuarterId;
	}else{
		redirectURL = "refChkquestionSetQuestion.do?quesSetId="+qSetId+"&districtId="+districtId+"&headQuarterId="+headQuarterId;;
	}
	redirectTo(redirectURL);
}

function activateDeactivateDistrictQuestion(questionId,status) {

	ReferenceChkQuestionsAjax.activateDeactivateDistrictQuestion(questionId,status, {
		async: true,
		errorHandler:handleError,
		callback:  function(data)
		{
			if(data!=null)
			{
				if(data.status=="A")
					$('#Q'+questionId).html("<a href='javascript:void(0)' onclick=\"activateDeactivateDistrictQuestion("+questionId+",'I')\">"+resourceJSON.MsgDeactivate+"</a>");
				else
					$('#Q'+questionId).html("<a href='javascript:void(0)' onclick=\"activateDeactivateDistrictQuestion("+questionId+",'A')\">"+resourceJSON.MsgActivate+"</a>");
			}
		}
	});
}

function setDistrictQuestion(questionId) {

	if(questionId>0) {
		var districtSpecificQuestions = {questionId:questionId};
		ReferenceChkQuestionsAjax.getDistrictQuestionById(districtSpecificQuestions,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			$('#assQuestion').find(".jqte_editor").html(data.question)
			//document.getElementById('question').innerHTML = data.question;
			$('#assInstruction').find(".jqte_editor").html(data.questionInstructions)
			$('#assExplanation').find(".jqte_editor").html(data.questionExplanation)
			dwr.util.setValue("questionTypeMaster",data.questionTypeMaster.questionTypeId);
			dwr.util.setValue("questionMaxScore",data.questionMaxScore);
			//$('#questionMaxScore').html(data.questionMaxScore)
			getQuestionOptions(data.questionTypeMaster.questionTypeId);
			var cnnt=1;
			jQuery.each(assessQuesOpts, function (i, jsonSelectedOptions) {	
				$('#hid'+cnnt).attr("value",jsonSelectedOptions.optionId);
				$('#opt'+cnnt).attr("value",jsonSelectedOptions.questionOption);
				$('#valid'+cnnt).attr('checked',jsonSelectedOptions.validOption);
				cnnt++;
			});
		  }
		});	
	}
}

function hideSearchPan() {
	$('#districtName').css("background-color", "");
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
}

function getSearchPan() {
	$('#errordiv').hide();
	$('#districtName').css("background-color", "");
	$('#searchItem').fadeIn(1000);
	$('#sa').fadeOut(1200);
}
//////////////////////////////////////////////////////////////////
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray 	= new Array();
var showDataArray 		= new Array();
var degreeTypeArray 	= new Array();
var hiddenId="";

function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type) {

	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	}
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();

	}
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
		BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="") {

			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
		} else {

	}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}

function hideDistrictMasterDiv(dis,hiddenId,divId) {

	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid) {

		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}
function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}
