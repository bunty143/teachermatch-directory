var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var currentPageFlag="";
var pageSTT = 1;
var sortOrderStrSTT ="";
var sortOrderTypeSTT ="";
var noOfRowsSTT = 10;

var pageUS = 1;
var sortOrderStrUS="";
var sortOrderTypeUS ="";
var noOfRowsUS = 10;

var pageJob = 1;
var sortOrderStrJob="";
var sortOrderTypeJob ="";
var noOfRowsJob= 50;

var teacherIdForjob;
var txtBgColor="#F5E7E1";

//Start for Profilr Div Grid

//videoLink
var pagevideoLink = 1;
var sortOrderStrvideoLink="";
var sortOrderTypevideoLink ="";
var noOfRowsvideoLink= 10;

//References
var pageReferences = 1;
var sortOrderStrReferences="";
var sortOrderTypeReferences ="";
var noOfRowsReferences= 10;

//workExp
var pageworkExp = 1;
var sortOrderStrworkExp="";
var sortOrderTypeworkExp ="";
var noOfRowsworkExp= 10;

//tpvh - teacherprofilevisithistory
var pagetpvh = 1;
var sortOrderStrtpvh="";
var sortOrderTypetpvh ="";
var noOfRowstpvh= 10;

//teacherAce
var pageteacherAce = 1;
var sortOrderStrteacherAce="";
var sortOrderTypeteacherAce ="";
var noOfRowsteacherAce= 10;

//teacherCerti
var pageteacherCerti = 1;
var sortOrderStrteacherCerti="";
var sortOrderTypeteacherCerti ="";
var noOfRowsteacherCerti= 10;

//for addtional documents
var pageaddDoc= 1;
var noOfRowsaddDoc= 10;
var sortOrderStraddDoc="";
var sortOrderTypeaddDoc="";

//for language profiency
var pageLang= 1;
var noOfRowsLang= 10;
var sortOrderStrLang="";
var sortOrderTypeLang="";

//For Assessment
var pageASMT 				= 	1;
var noOfRowsASMT			=	10;
var sortOrderStrASMT		=	"";
var sortOrderTypeASMT 		= 	10;

//teacher assessemnt
var pageEJ= 1;
var noOfRowsEJ= 10;
var sortOrderStrEJ="";
var sortOrderTypeEJ="";

//End for Profilr Div Grid
function downloadCertificationforCG(certId, linkId,teaId)
{		
	TeacherProfileViewInDivAjax.downloadCertificationforCG(certId,teaId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmTrans").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmTrans").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;
				}			
			}	
			return false;		
		
		}});
}


function checkForInt1(evt) 
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;
    return( (charCode >= 48 && charCode <= 57)||(charCode==13));
}


function defaultSaveDiv(){
	pageSTT = 1;
	sortOrderStrSTT ="";
	sortOrderTypeSTT ="";
	noOfRowsSTT = 10;
	currentPageFlag="stt";
}
function defaultShareDiv(){
	pageUS = 1;
	sortOrderStrUS="";
	sortOrderTypeUS ="";
	noOfRowsUS = 10;
	currentPageFlag="us";
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else if(exception.javaClassName=='undefined'){}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}


function getPaging(pageno)
{
	 if(currentPageFlag=="stt"){
			if(pageno!='')
			{
				pageSTT=pageno;	
			}
			else
			{
				pageSTT=1;
			}
			noOfRowsSTT = document.getElementById("pageSize3").value;
			var folderId = document.getElementById("folderId").value;
			var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
			displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
	}else if(currentPageFlag=="us"){
		if(pageno!='')
		{
			pageUS=pageno;	
		}
		else
		{
			pageUS=1;
		}
		noOfRowsUS = document.getElementById("pageSize3").value;
		
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			var teachersharedId=document.getElementById("teachersharedId").value;
			displayUsergrid(teachersharedId,1);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			displayShareFolder();
		}
	}
	else if(currentPageFlag=="job"){

		if(pageno!='')
		{
			pageJob=pageno;	
		}
		else
		{
			pageJob=1;
		}
		
		noOfRowsJob = document.getElementById("pageSize1").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		teacherIdForjob=teacherIdForprofileGrid;
		getJobOrderList();
	}
	else if(currentPageFlag=="workExp"){
		if(pageno!='')
		{
			pageworkExp=pageno;	
		}
		else
		{
			pageworkExp=1;
		}
		noOfRowsworkExp = document.getElementById("pageSize11").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="References"){
		if(pageno!='')
		{
			pageReferences=pageno;	
		}
		else
		{
			pageReferences=1;
		}
		noOfRowsReferences = document.getElementById("pageSize12").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid)
		
	}
	else if(currentPageFlag=="videoLink"){
		if(pageno!='')
		{
			pagevideoLink=pageno;
		}
		else
		{
			pagevideoLink=1;
		}
		noOfRowsvideoLink = document.getElementById("pageSize13").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		if(pageno!='')
		{
			pageLang=pageno;
		}
		else
		{
			pageLang=1;
		}
		noOfRowsLang = document.getElementById("pageLangProf").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="document"){
		if(pageno!='')
		{
			pageaddDoc=pageno;
		}
		else
		{
			pageaddDoc=1;
		}
		noOfRowsaddDoc = document.getElementById("pageSize15").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="assessmentDetails"){
		if(pageno!='')
		{
			pageASMT=pageno;	
		}
		else
		{
			pageASMT=1;
		}
		teacherId 		= document.getElementById("teacherIdForReference").value;
	//	teacherDetails	= document.getElementById("teacherDetailsForReference").value;
		noOfRowsASMT 	= document.getElementById("pageSize3").value;
		getdistrictAssessmetGrid_DivProfile(teacherId);
	}  
	else if(currentPageFlag=="tpvh"){
		if(pageno!='')
		{
			pagetpvh=pageno;
		}
		else
		{
			pagetpvh=1;
		}
		noOfRowstpvh = document.getElementById("pageSize14").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherAce"){
		if(pageno!='')
		{
			pageteacherAce=pageno;
		}
		else
		{
			pageteacherAce=1;
		}
		noOfRowsteacherAce = document.getElementById("pageSize10").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherCerti"){
		if(pageno!='')
		{
			pageteacherCerti=pageno;
		}
		else
		{
			pageteacherCerti=1;
		}
		noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	} 
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	 if(currentPageFlag=="stt"){
		if(pageno!=''){
			pageSTT=pageno;	
		}else{
			pageSTT=1;
		}
		sortOrderStrSTT	=	sortOrder;
		sortOrderTypeSTT	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsSTT = document.getElementById("pageSize3").value;
		}else{
			noOfRowsSTT=10;
		}
		var folderId = document.getElementById("folderId").value;
		var checkboxshowHideFlag= document.getElementById("checkboxshowHideFlag").value;
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
	}else if(currentPageFlag=="us"){
		if(pageno!=''){
			pageUS=pageno;	
		}else{
			pageUS=1;
		}
		sortOrderStrUS	=	sortOrder;
		sortOrderTypeUS	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsUS = document.getElementById("pageSize3").value;
		}else{
			noOfRowsUS=10;
		}
		var userFPS=document.getElementById("userFPS").value;
		var userFPD=document.getElementById("userFPD").value;
		var userCPS=document.getElementById("userCPS").value;
		var userCPD=document.getElementById("userCPD").value;
		if(userFPS==1){
			searchUser(0);
		}else if(userFPD==1){
			var teachersharedId=document.getElementById("teachersharedId").value;
			displayUsergrid(teachersharedId,1);
		}else if(userCPS==1){
			searchUserthroughPopUp(0);
		}else if(userCPD==1){
			displayShareFolder();
		}
	}
	else if(currentPageFlag=="job"){
		if(pageno!=''){
			pageJob=pageno;	
		}else{
			pageJob=1;
		}
		sortOrderStrJob	=	sortOrder;
		sortOrderTypeJob	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			noOfRowsJob = document.getElementById("pageSize1").value;
		}else{
			noOfRowsJob=50;
		}
		
		//noOfRowsJob = document.getElementById("pageSize1").value;
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		teacherIdForjob=teacherIdForprofileGrid;
		getJobOrderList();
	}
	else if(currentPageFlag=="workExp"){
		if(pageno!=''){
			pageworkExp=pageno;	
		}else{
			pageworkExp=1;
		}
		sortOrderStrworkExp	=	sortOrder;
		sortOrderTypeworkExp	=	sortOrderTyp;
		if(document.getElementById("pageSize11")!=null){
			noOfRowsworkExp = document.getElementById("pageSize11").value;
		}else{
			noOfRowsworkExp=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getPFEmploymentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="References"){
		if(pageno!=''){
			pageReferences=pageno;	
		}else{
			pageReferences=1;
		}
		sortOrderStrReferences	=	sortOrder;
		sortOrderTypeReferences	=	sortOrderTyp;
		if(document.getElementById("pageSize12")!=null){
			noOfRowsReferences = document.getElementById("pageSize12").value;
		}else{
			noOfRowsReferences=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="videoLink"){
		if(pageno!=''){
			pagevideoLink=pageno;	
		}else{
			pagevideoLink=1;
		}
		sortOrderStrvideoLink	=	sortOrder;
		sortOrderTypevideoLink	=	sortOrderTyp;
		if(document.getElementById("pageSize13")!=null){
			noOfRowsvideoLink = document.getElementById("pageSize13").value;
		}else{
			noOfRowsvideoLink=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getVideoLinksGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="document"){
		if(pageno!=''){
			pageaddDoc=pageno;	
		}else{
			pageaddDoc=1;
		}
		sortOrderStraddDoc	=	sortOrder;
		sortOrderTypeaddDoc	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsaddDoc = document.getElementById("pageSize15").value;
		}else{
			noOfRowsaddDoc=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getadddocumentGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="languageProfiency"){
		if(pageno!=''){
			pageLang=pageno;	
		}else{
			pageLang=1;
		}
		sortOrderStrLang	=	sortOrder;
		sortOrderTypeLang	=	sortOrderTyp;
		if(document.getElementById("pageLangProf")!=null){
			noOfRowsLang = document.getElementById("pageLangProf").value;
		}else{
			noOfRowsLang=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;		
		getlanguage_DivProfile(teacherIdForprofileGrid);
	}
else if(currentPageFlag=="assessmentDetails"){
		if(pageno!=''){
			pageASMT=pageno;	
		}else{
			pageASMT=1;
		}
		sortOrderStrASMT	=	sortOrder;
		sortOrderTypeASMT	=	sortOrderTyp;
		if(document.getElementById("pageSize15")!=null){
			noOfRowsASMT = document.getElementById("pageSize15").value;
		}else{
			noOfRowsASMT=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getdistrictAssessmetGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="tpvh"){
		if(pageno!=''){
			pagetpvh=pageno;	
		}else{
			pagetpvh=1;
		}
		sortOrderStrtpvh	=	sortOrder;
		sortOrderTypetpvh	=	sortOrderTyp;
		if(document.getElementById("pageSize14")!=null){
			noOfRowstpvh = document.getElementById("pageSize14").value;
		}else{
			noOfRowstpvh=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherProfileVisitHistoryShow(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherAce"){
		if(pageno!=''){
			pageteacherAce=pageno;	
		}else{
			pageteacherAce=1;
		}
		sortOrderStrteacherAce	=	sortOrder;
		sortOrderTypeteacherAce	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherAce = document.getElementById("pageSize10").value;
		}else{
			noOfRowsteacherAce=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAcademicsGrid_DivProfile(teacherIdForprofileGrid);
	}else if(currentPageFlag=="teacherCerti"){
		if(pageno!=''){
			pageteacherCerti=pageno;	
		}else{
			pageteacherCerti=1;
		}
		sortOrderStrteacherCerti	=	sortOrder;
		sortOrderTypeteacherCerti	=	sortOrderTyp;
		if(document.getElementById("pageSize10")!=null){
			noOfRowsteacherCerti = document.getElementById("pageSize9").value;
		}else{
			noOfRowsteacherCerti=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherCertificationsGrid_DivProfile(teacherIdForprofileGrid);
	}
	else if(currentPageFlag=="teacherAssessment"){
		if(pageno!=''){
			pageEJ=pageno;	
		}else{
			pageEJ=1;
		}
		sortOrderStrEJ=sortOrder;
		sortOrderTypeEJ=sortOrderTyp;
		if(document.getElementById("pageSize")!=null){
			noOfRowsEJ = document.getElementById("pageSize").value;
		}else{
			noOfRowsEJ=10;
		}
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getTeacherAssessmentGrid_DivProfile(teacherIdForprofileGrid);
	}
}
var districtMaster = null;
var schoolMaster = null;
var headQuarterMaster = null;
var branchMaster = null;
function getDefaultData()
{
	if($("#loadingFlag").val()==1)
	{

		$('div#lastPostsLoader').html('<img src="images/loadingAnimation.gif">');
		CommonDashboardAjax.getEndlessData(0,15,0,0,districtMaster,schoolMaster,headQuarterMaster,branchMaster,{  
				async: true,
				callback: function(data){
				var dataarray = data.split("@###@");
				var ddtta=""+dataarray[0];
						
				$(".items").append(ddtta);
				$("#loadingCounter").val(dataarray[1]);
				$("#jobLoadingCounter").val(dataarray[2]);
				$("#yellowJobLoadingCounter").val(dataarray[3]);
				$("#loadingFlag").val(dataarray[4]);
				$('div#lastPostsLoader').empty();
				divhover();
				$(".tool").tooltip();
				//namehover();
				//window.location.href='#feed';
				$("html, body").animate({ scrollTop: 0 }, "slow");
				//window.scrollTo(0, 0);
				
			},
			errorHandler:handleError  
			});
	}
}

function updateRecords()
{
	CommonDashboardAjax.UpdateRecords({ 
			async: true,
			callback: function(data){
			alert(data);
		},
	errorHandler:handleError  
	});
}

function getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId)
{
	hideProfilePopover();
	document.getElementById("commDivFlag").value=commDivFlag;
	CandidateGridAjax.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,false,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		document.getElementById("divCommTxt").innerHTML=data.toString();
		try{
		$('#myModalCommunications').modal('show');
		
		$("#myModalCommunications").attr("commDivFlag",commDivFlag);
		$("#myModalCommunications").attr("jobForTeacherGId",jobForTeacherGId);
		$("#myModalCommunications").attr("teacherId",teacherId);
		$("#myModalCommunications").attr("jobId",jobId);
		
		
		$('#commMsg').tooltip();
		$('#commNotes').tooltip();
		}catch(err)
		  {
		  }
	}
	});	
}

function printCommunicationslogs()
{
	var commDivFlag = $("#myModalCommunications").attr("commDivFlag");
	var jobForTeacherGId = $("#myModalCommunications").attr("jobForTeacherGId");
	var teacherId = $("#myModalCommunications").attr("teacherId");
	var jobId = $("#myModalCommunications").attr("jobId");
	
	//alert("commDivFlag:- "+commDivFlag+"\njobForTeacherGId:- "+jobForTeacherGId+"\nteacherId:- "+teacherId+"\njobId:- "+jobId);
	CandidateGridAjax.getCommunications(commDivFlag,jobForTeacherGId,teacherId,jobId,true,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
			
			//alert("Before CommunicationLogData:- "+data);
			var newWindow = window.open();
			newWindow.document.write(data);	
			newWindow.print();
		}
	});
}
function downloadCommunication(filePath,fileName,linkId)
{
	$('#loadingDiv').show();
	CandidateGridAjax.downloadCommunication(filePath,fileName,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					$('#loadingDiv').hide();
					if (data.indexOf(".doc") !=-1 || data.indexOf(".xls") !=-1) {
					    document.getElementById('ifrmTrans').src = ""+data+"";
					}else{
						document.getElementById(linkId).href = data; 
						return false;
					}
				}
			});
}
function getCandidateStats()
{
	$('#container4').html("<div style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>");
	
	CommonDashboardAjax.getCandidateStats(districtMaster,schoolMaster,headQuarterMaster,branchMaster,{ 
			async: true,
			callback: function(data){
			//alert(data);
		//var jsondata={"chart":{"type":"bar"},"title":{"text":"Hired Candidate Distribution By States","align":"left","style":{"color":"#007AB4","fontSize":"10px"}},"xAxis":{"categories":["IL","NY"],"labels":{"color":"#007AB4","fontSize":"10px"}},"yAxis":{"min":0,"title":{"text":"","align":"left","style":{"color":"#007AB4","fontSize":"10px"}}},"legend":{"backgroundColor":"#FFFFFF","reversed":true},"plotOptions":{"series":{"stacking":"normal"}},"series":[{"name":"Other Candidates","color":"#A52A2A","data":[1,1]},{"name":"Top Candidates","color":"#8bbc21","data":[0,0]}]};
		//$('#container4').highcharts(data);
		createGraph(data);
		   
		},
	errorHandler:handleError  
	});
}
var tmstyle={
	color: '#007AB4',
	fontSize: '10px',
	fontfamily:'Century Gothic'
}

function getJobOrderStatus()
{
	$('#container1').html("<div class='img-responsive' style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	
	CommonDashboardAjax.getJobOrderStatus(districtMaster,schoolMaster,{ 
		async: true,
		callback: function(data){
		//alert(data);
		var normRed=data[0];
		var normYellow=data[1];
		var normGreen=data[2];
		var youRed=data[3];
		var youYellow=data[4];
		var youGreen=data[5];
		var lblCriticialJobName=data[6];
		var lblAttentionJobName=data[7];
		

		var  categories = [lblCriticialJobName, lblAttentionJobName, resourceJSON.toolOnPace],
        name = resourceJSON.msgJobStatYouvsNorm ,
        data = [{
                y: youRed,
                color: '#ff0000',
                drilldown: {
                    name: lblCriticialJobName,
                    categories: [lblCriticialJobName],
                    data: [normRed],
                    color: '#ff0000'
                }
            }, {
                y: youYellow,
                color: '#ffd700',
                drilldown: {
                    name: lblAttentionJobName,
                    categories: [lblAttentionJobName],
                    data: [normYellow],
                    color: '#ffd700'
                }
            }, {
                y: youGreen,
                color: "#8bbc21",
                drilldown: {
                    name: resourceJSON.toolOnPace,
                    categories: [resourceJSON.toolOnPace],
                    data: [normGreen],
                    color: "#8bbc21"
                }
            
            }];

    // Build the data arrays
    var browserData = [];
    var versionsData = [];
    for (var i = 0; i < data.length; i++) {
        // add browser data
        browserData.push({
            name: categories[i],
            y: data[i].y,
            color: data[i].color
        });

        // add version data
        for (var j = 0; j < data[i].drilldown.data.length; j++) {
            var brightness = 0.2 - (j / data[i].drilldown.data.length) / 5 ;
            versionsData.push({
                name: data[i].drilldown.categories[j],
                y: data[i].drilldown.data[j],
               // color: Highcharts.Color(data[i].color).brighten(brightness).get()
			    color: data[i].color
            });
        }
    }
		
	// Create the chart
    var chartOptions = {
	    	chart: {
	        type: 'pie',
	        renderTo: 'container1',
	        events: {click:function () {
	            hs.htmlExpand(document.getElementById(chartOptions.chart.renderTo), {
	                width: 9999,
	                height: 9999,
	                allowWidthReduction: true,
	                preserveContent: false
	            }, {
	                chartOptions: chartOptions
	            });
	        }}
	    },
	    exporting: {
            enabled: false
        },
        title: {
            text: resourceJSON.msgJobStatYouvsNorm,
            align: "left",
            style:tmstyle
        },
        plotOptions: {
            pie: {
                shadow: false,
                center: ['53%', '47%']
            }
        },
        tooltip: {
            valueSuffix: resourceJSON.jobsdashbord,
            style: tmstyle
        },
        credits: {
            enabled: false
        },
        series: [{
            name: resourceJSON.Youdashbord,  
            data: browserData,
            size: '60%',
            style:tmstyle,
            dataLabels: {
                formatter: function() {
                    return this.y > 5 ? this.point.name : null;
                },
                color: 'white',
                distance: -30
            }
        }, {
            name: 'National Norm',
            data: versionsData,
            style:tmstyle,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                formatter: function() {
                    // display only if larger than 1
                    return this.y > 1 ? ''+ this.point.name +': '+ this.y +'%'  : null;
                },style:tmstyle
            }
        }]
    };
    
    var chart = new Highcharts.Chart(chartOptions);
		
		},
	errorHandler:handleError  
	});
}
function getPoolQuality()
{
	$('#container3').html("<div style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	
	CommonDashboardAjax.getPoolQuality(districtMaster,schoolMaster,headQuarterMaster,branchMaster,{  
			async: true,
			callback: function(data){
			//alert(data);
		createGraph({
	            chart: {
	        		renderTo: 'container3',
	        		events: {},
	                type: 'column'
	            },
	            exporting: {
	                enabled: false
	            },
	            title: {
	                text: resourceJSON.msgAppPoolQtyconv,
	                align: "left",
	                style:tmstyle
	            },
	            xAxis: {
	                categories: [  resourceJSON.lblApplicants , resourceJSON.lblHired],
	                labels: {
	                    style: tmstyle
	                }
	            },
	            yAxis: {
	            	gridLineWidth: 0,
	            	min: 0,
	            	max: 100,
	            	tickInterval: 20,
	                title: {
	                    text: '',
	                    style: tmstyle
	                },
	                /*stackLabels: {
	                    enabled: true,
	                    style: tmstyle
	                },*/labels: {
	                    overflow: 'justify',
	                    style: tmstyle
	                }
	            },
	            legend: {
	                align: 'right',
	                x: -5,
	                verticalAlign: 'top',
	                y: 20,
	                floating: true,
	                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',
	                borderColor: '#CCC',
	                borderWidth: 1,
	                shadow: false,
	                enabled: false
	            },credits: {
	                enabled: false
	            },
	            tooltip: {
	                formatter: function() {
	                    return '<b>'+ this.x +'</b><br/>'+
	                        this.series.name +': '+ this.y +'%'+'<br/>'
	                         /*+ 'Total: '+ this.point.stackTotal+'%'*/;
	                },
	                style:tmstyle
	            },
	            plotOptions: {
	                column: {
	                    stacking: 'normal',
	                   /*dataLabels: {
	                        enabled: true,
	                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
	                        format: '{point.y:.2f}%'
	                    }*/
	                }
	            },
	            series: data

	        });
			
		},
	errorHandler:handleError  
	});
}

function getHiringVelocity()
{
	createGraph({
	    chart: {
	        renderTo: 'container2',
	        events: {},
	        type: 'bar'
	    },
	    exporting: {
	            enabled: false
	        }
        ,
          title: {
                text: resourceJSON.msgElapsedHired,
                align: "left",
                style:tmstyle
            },
           
            xAxis: {
                categories: [  resourceJSON.subMath , resourceJSON.subScience , resourceJSON.subGeography, resourceJSON.subSocialStudies, resourceJSON.subArt,  resourceJSON.subHistory],
                title: {
                    text: null,
                }
                ,labels: {
	                    style: tmstyle
	                }
            },
            yAxis: {
            	gridLineWidth: 0,
                min: 0,
                title: {
                    text: '',
                    align: 'high'
                },
                labels: {
	                    overflow: 'justify',
	                    style: tmstyle
	                }
            },
            tooltip: {
                //valueSuffix: ' candidates',
            	valueSuffix: ' '+resourceJSON.lbldays,
                style: tmstyle
            },
            plotOptions: {
                bar: {
                    dataLabels: {
                        enabled: true
                    }
                }
            },
            legend: {
            //layout: 'vertical',
                layout: 'horizontal',
                align: 'right',
                verticalAlign: 'top',
                x: -40,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: '#FFFFFF',
                shadow: true,
                enabled: false
            },
            credits: {
                enabled: false
            },
            series: [{
                name: resourceJSON.lblNationalNorm,
                //color: '#0066FF',
                data: [29, 39, 50, 24, 37,48]
            }, {
                name: resourceJSON.lblYou ,
                //color: '#8bbc21',
                color: '#800080',
                data: [33, 41, 54, 28, 42,54]
            }]
        });
}

function searchData()
{
	//alert($('#schoolId').val());
	$('#districtName').css("background-color", "");
	if($('#districtId').val()==0 && $('#schoolId').val()==0)
	{
		$('#errordiv').html("&#149; "+resourceJSON.msgAnyDistOrSchool+"<br>");
		$('#districtName').focus();
		$('#districtName').css("background-color", "#F5E7E1");
		$('#errordiv').show();
		return;
	}
	
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
	$('#closePan').html("<a style='padding-right: 6px;'   onclick='hideSearchPan();' href='javascript:void(0);' ><i class='icon-remove icon-large' ></i></a>");
	showTMDefaultView();
	
	$('#loadingCounter').val();
	$('#jobLoadingCounter').val();
	$('#yellowJobLoadingCounter').val();
	$('#loadingFlag').val(1);
	
	$(".items").empty();
	
	if(dwr.util.getValue("districtId")!=0)
		districtMaster={districtId:dwr.util.getValue("districtId")};
	
	if(dwr.util.getValue("schoolId")!="" && dwr.util.getValue("schoolId")!=0)
		schoolMaster={schoolId:dwr.util.getValue("schoolId")};
	else 
		schoolMaster=null;
	getDefaultData();
	getCandidateStats();
	getJobOrderStatus();
	//getHiringVelocity();
	getPoolQuality();
	
	
}
function hideSearchPan()
{
	$('#searchIdjob').show();
	$('#districtName').css("background-color", "");
	$('#searchItem').fadeOut(1000);
	$('#searchItems').fadeOut(1000);
	$('#sa').fadeIn(1200);
}
function getSearchPan()
{
	$('#searchIdjob').hide();
	$('#errordiv').hide();
	$('#districtName').css("background-color", "");
	//$('#districtName').focus();
	$('#searchItem').fadeIn(1000);
	$('#sa').fadeOut(1200);
	//$('#searchIdjob').hide();
	//setTMDefaultView();
}
function getSearchPan1()
{
	$('#searchItems').fadeIn(1000);
	//$('#districtName').css("background-color", "");
	//$('#sa').fadeOut(1200);
	$('#searchIdjob').hide();
	
}
function setTMDefaultView()
{
	/*var dddta="<div style='text-align:center;padding-top:100px;'><br>No data available</div>";
	$('#container1').html(dddta);
	$('#container2').html(dddta);
	$('#container3').html(dddta);
	$('#container4').html(dddta);*/
	
		/*$('#container1').hide();
		$('#container2').hide();
		$('#container3').hide();
		$('#container4').hide();*/
		$('#feed').hide();
		$('#sticker').hide();
		
}
function showTMDefaultView()
{
		/*$('#container1').show();
		$('#container2').show();
		$('#container3').show();
		$('#container4').show();*/
	
	$('#feed').show();
	$('#sticker').show();
}
///////////////////////////////////////////////////////////////////////////////////////////////
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictArray(districtName){
	var searchArray = new Array();
		BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].districtName;
				showDataArray[i]=data[i].districtName;
				hiddenDataArray[i]=data[i].districtId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(document.getElementById("districtName").value==""){
		document.getElementById('schoolName').readOnly=true;
		document.getElementById('schoolName').value="";
		document.getElementById('schoolId').value="0";
	}
	document.getElementById("districtId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("districtId").value=hiddenDataArray[index];
			document.getElementById('schoolName').readOnly=false;
			document.getElementById('schoolName').value="";
			document.getElementById('schoolId').value="0";
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArray(txtSearch.value);
		fatchData2(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArray(schoolName){
	var searchArray = new Array();
	var districtId	=	document.getElementById("districtId").value;
	    BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById("schoolId").value=hiddenDataArray[index];
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}




var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function showCommunicationsDiv()
{
	//$('#myModalMessage').modal('hide');	
	//$('#myModalNotes').modal('hide');	
	//$('#myModalCommunications').modal('show');
	try{
		$('#myModalMessage').modal('hide');	
		$('#myModalNotes').modal('hide');	
		$('#myModalCommunications').modal('show');
		}catch(err){}
}
function showCommunicationsDivForMsg()
{
	//$('#myModalMessage').modal('hide');	
	try{
		$('#myModalMessage').modal('hide');
		}catch(err){}
	var msgType=document.getElementById("msgType").value;
	if(msgType==1){
		//$('#myModalCommunications').modal('show');
		try{
			$('#myModalCommunications').modal('show');
			}catch(err){}
	}
}
function showCommunicationsForPhone()
{
	var phoneType=document.getElementById("phoneType").value;
	if(phoneType==1){
		//$('#myModalPhone').modal('hide');	
		//$('#myModalCommunications').modal('show');
		try{
			$('#myModalPhone').modal('hide');	
			$('#myModalCommunications').modal('show');
			}catch(err){}
	}
}
function addNoteFileType()
{
	$('#fileNotes').empty();
	$('#fileNotes').html("<a href='javascript:void(0);' onclick='removeNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileNote' name='fileNote' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+"");
}
function removeNotesFile()
{
	$('#fileNotes').empty();
	$('#fileNotes').html("<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function addPhoneFileType()
{
	$('#filePhones').empty();
	$('#filePhones').html("<a href='javascript:void(0);' onclick='removePhoneFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='filePhone' name='filePhone' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removePhoneFile()
{
	$('#filePhones').empty();
	$('#filePhones').html("<a href='javascript:void(0);' onclick='addPhoneFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addPhoneFileType();'>"+resourceJSON.AttachFile+"</a>");
}
function addMessageFileType()
{
	$('#fileMessages').empty();
	$('#fileMessages').html("<a href='javascript:void(0);' onclick='removeMessagesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileMessage' name='fileNote' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}
function removeMessagesFile()
{
	$('#fileMessages').empty();
	$('#fileMessages').html("<a href='javascript:void(0);' onclick='addMessageFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addMessageFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function getNotesDiv(teacherId,jobId)
{
	removeNotesFile();
	hideCommunicationsDiv();
	currentPageFlag='notes';
	document.getElementById("teacherId").value=teacherId;
	document.getElementById("teacherIdForNote").value=teacherId;
	$('#divTxtNode').find(".jqte_editor").html("");		
	//$('#myModalNotes').modal('show');	
	try{
		$('#myModalNotes').modal('show');
		}catch(err){}
	$('#divTxtNode').find(".jqte_editor").css("background-color", "");
	document.getElementById("noteId").value="";
	$('#errordivNotes').empty();
}
function getMessageDiv(teacherId,emailId,jobId,msgType)
{
	removeMessagesFile();
	hideCommunicationsDiv();
	currentPageFlag='msg';
	document.getElementById("teacherDetailId").value=teacherId;	
	document.getElementById("teacherIdForMessage").value=teacherId;	
	document.getElementById("emailId").value=emailId;
	document.getElementById("jobId").value=jobId;
	document.getElementById("emailDiv").innerHTML=emailId;
	$('#messageSend').find(".jqte_editor").html("");
	document.getElementById("messageSubject").value="";
	//$('#myModalMessage').modal('show');	
	try{
		$('#myModalMessage').modal('show');	
		}catch(err){}
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	$('#messageSubject').focus();
	$('#errordivMessage').empty();
	document.getElementById("msgType").value=msgType;
	if(msgType==0){
		CandidateGridAjax.getMessages(teacherId,jobId,pageMsg,noOfRowsMsg,sortOrderStrMsg,sortOrderTypeMsg,
		{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				document.getElementById("divMessages").innerHTML=data;
				applyScrollOnMessageTbl();
			}
		});
	}else{
		document.getElementById("divMessages").innerHTML="";	
	}
	if( ($('#entityType').val()==2) || ($('#entityType').val()==3) )
	{
		//alert(" jobId : "+jobId+" teacherId "+teacherId);
		getTemplatesByDistrictId(jobId);
		getDocuments();
	}
}


function getTemplatesByDistrictId(jobId)
{
	//alert(" jobId : "+jobId);
	CandidateGridAjax.getTemplatesByDistrictId(jobId,
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" data : "+data+" Length : "+data.length);
					if(data!="" || data.length>0)
					{
						$("#templateDiv").fadeIn();
						$("#btnChangetemplatelink").hide();
						$("#districttemplate").html(data);
					}
				}
			});
}

function setTemplate()
{
	var templateId	=	$("#districttemplate").val();
	if(templateId==0)
		$("#btnChangetemplatelink").fadeOut();
	else
		$("#btnChangetemplatelink").fadeIn();
}

function getTemplate()
{
	var templateId	=	$("#districttemplate").val();
	var teacherId	=	$("#teacherDetailId").val();
	var jobId		=	$("#jobId").val();
	var confirmFlagforChangeTemplate = $("#confirmFlagforChangeTemplate").val(); // Gagan : confirmFlagforChangeTemplate 1 means confirm change template 
	//alert(" confirmFlagforChangeTemplate "+confirmFlagforChangeTemplate);
	
	if(templateId!=0)
	{
		CandidateGridAjax.getTemplatesByTemplateId(templateId,jobId,teacherId,
		{
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" data : "+data+" Length : "+data.length);
				if(data!=null)
				{
					if($('#messageSubject').val() && $('#messageSend').find(".jqte_editor").text().trim()!="" && confirmFlagforChangeTemplate==0)
					{
						try{
							$('#confirmChangeTemplate').modal('show');
						}	
						catch (err){}
					}
					else
					{
						$('#messageSubject').val(data.subjectLine);
						$('#messageSend').find(".jqte_editor").html(data.templateBody);
						$("#confirmFlagforChangeTemplate").val(0);
					}
				}
			}
		});
	}
	return false;
}

function confirmChangeTemplate()
{
	$("#confirmFlagforChangeTemplate").val(1);
	getTemplate();
	try{
		$('#confirmChangeTemplate').modal('hide');
	}	
	catch (err){}
}



function showMessage(messageId)
{	
	CandidateGridAjax.showMessage(messageId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			document.getElementById("messageSubject").value=data.messageSubject;
			$('#messageSend').find(".jqte_editor").html(data.messageSend);
			$('#messageSend').find(".jqte_editor").focus();
		}
	});
}

function validateMessage()
{
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	var teacherId = document.getElementById("teacherDetailId").value;
    var documentname=document.getElementById("districtDocument").value;
	var jobId = document.getElementById("jobId").value;
	var messageDateTime = document.getElementById("messageDateTime").value;
	$('#errordivMessage').empty();
	var messageFileName="";
	var fileMessage=null;
	try{
		fileMessage=document.getElementById("fileMessage").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
		if(trim(messageSubject)=="")
		{
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrSub+".<br>");
			if(focs==0)
				$('#messageSubject').focus();
			$('#messageSubject').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else if ($('#messageSend').find(".jqte_editor").text().trim()==""){

			$('#errordivMessage').append("&#149; "+resourceJSON.PlzEtrMsg+".<br>");
			if(focs==0)
				$('#messageSend').find(".jqte_editor").focus();
			$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}/*else{
			var charCount=$('#messageSend').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			if(count>1000)
 			{
				$('#errordivMessage').append("&#149; Message length cannot exceed 1000 characters.<br>");
				if(focs==0)
					$('#messageSend').find(".jqte_editor").focus();
				$('#messageSend').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}*/
		
		if(fileMessage=="" && cnt==0){
			$('#errordivMessage').show();
			$('#errordivMessage').append("&#149; "+resourceJSON.PlzUplMsg+".<br>");
			cnt++;
		}else if(fileMessage!="" && fileMessage!=null)
		{
			var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
			messageFileName="message"+messageDateTime+"."+ext;
			var fileSize = 0;
			if ($.browser.msie==true)
		 	{	
			    fileSize = 0;	   
			}
			else
			{
				if(document.getElementById("fileMessage").files[0]!=undefined)
				{
					fileSize = document.getElementById("fileMessage").files[0].size;
				}
			}
			
			if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'|| ext=='xlsx' || ext=='xls'))
			{
				$('#errordivMessage').show();
				$('#errordivMessage').append("&#149;"+resourceJSON.PlzSelectAcceptMsg+".<br>");
				cnt++;
			}
			else if(fileSize>=10485760)
			{
				$('#errordivMessage').show();
				$('#errordivMessage').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
				cnt++;
			}
		}
		if(cnt==0){	
			try{
				$('#loadingDiv').show();
				if(fileMessage!="" && fileMessage!=null){
					document.getElementById("frmMessageUpload").submit();
				}else{
					$('#lodingImage').append("<img src=\"images/loadingAnimation.gif\" /> "+resourceJSON.msgSending);
					CandidateGridAjax.saveMessage(teacherId,messageSubject,messageSend,jobId,messageFileName,documentname,{ 
						async: true,
						errorHandler:handleError,
						callback:function(data)
						{	
							$('#loadingDiv').hide();
							var senderEmail = document.getElementById("emailDiv").innerHTML;
							confMessage(teacherId,senderEmail,jobId);
						}
					});
					return true;
				}
			}catch(err){}
		}else{
			$('#errordivMessage').show();
			return false;
		}
}
function saveMessageFile(messageDateTime){
	$('#messageSubject').css("background-color", "");
	$('#messageSend').find(".jqte_editor").css("background-color", "");
	var messageSend = $('#messageSend').find(".jqte_editor").html();
	var messageSubject = document.getElementById("messageSubject").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	$('#errordivMessage').empty();
	var messageFileName="";
	var fileMessage="";
	try{
		fileMessage=document.getElementById("fileMessage").value;
	}catch(e){}
	if(fileMessage!="" && fileMessage!=null)
	{
		var ext = fileMessage.substr(fileMessage.lastIndexOf('.') + 1).toLowerCase();	
		messageFileName="message"+messageDateTime+"."+ext;
	}
	CandidateGridAjax.saveMessage(teacherId,messageSubject,messageSend,jobId,messageFileName,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			var senderEmail = document.getElementById("emailDiv").innerHTML;
			confMessage(teacherId,senderEmail,jobId);
		}
	});
	return true;
	
}
function confMessage(teacherId,senderEmail,jobId)
{	
	$('#lodingImage').empty();
	//$('#myModalMessage').modal('hide');
	try{
		$('#myModalMessage').modal('hide');
		$('#myMsgShow').modal('show');
		}catch(err){}
	//$('#myMsgShow').modal('show');
	document.getElementById("message2show").innerHTML=resourceJSON.MsgSentToCandidate;
}

function showMessageDiv()
{	
	var commDivFlag=document.getElementById("commDivFlag").value;
	var msgType=document.getElementById("msgType").value;
	//$('#myMsgShow').modal('hide');
	//$('#myModalMessage').modal('show');
	try{
		$('#myMsgShow').modal('hide');
		$('#myModalMessage').modal('show');
		}catch(err){}
	
	var teacherId = document.getElementById("teacherDetailId").value;
	var jobId = document.getElementById("jobId").value;
	var senderEmail = document.getElementById("emailDiv").innerHTML;
	if(msgType==1){
		//$('#myMsgShow').modal('hide');
		//$('#myModalMessage').modal('hide');
		try{
			$('#myMsgShow').modal('hide');
			$('#myModalMessage').modal('hide');
			}catch(err){}
		
		getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);
	}else{
		document.getElementById('messageSubject').value="";
		document.getElementById('messageSend').value="";
		getMessageDiv(teacherId,senderEmail,jobId,0);
	}
}
function hideCommunicationsDiv()
{
	//$('#myModalCommunications').modal('hide');
	try{
		$('#myModalCommunications').modal('hide');
		}catch(err){}
}
function saveNotes()
{	
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var jobId = document.getElementById("jobId").value;
	var noteId = document.getElementById("noteId").value;
	var noteDateTime=document.getElementById("noteDateTime").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var fileNote=null;
	var noteFileName="";
	try{
		fileNote=document.getElementById("fileNote").value;
	}catch(e){}
	var cnt=0;
	var focs=0;	
	$('#errordivNotes').empty();
	if ($('#divTxtNode').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#divTxtNode').find(".jqte_editor").focus();
		$('#divTxtNode').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#errordivNotes').show();
		$('#errordivNotes').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#errordivNotes').show();
			$('#errordivNotes').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivNotes').show();
			$('#errordivNotes').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null){
				document.getElementById("frmNoteUpload").submit();
			}else{
				CandidateGridAjax.saveNotes(teacherId,notes,noteId,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						$('#loadingDiv').hide();	
						showCommunicationsDiv();
						getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);				
					}
				});	
			}
		}catch(err){}
	}
}
function saveNoteFile(noteDateTime)
{	
	var notes = $('#divTxtNode').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherId").value;
	var noteId = document.getElementById("noteId").value;
	var jobId = document.getElementById("jobId").value;
	var commDivFlag=document.getElementById("commDivFlag").value;
	var fileNote=null;
	try{
		fileNote=document.getElementById("fileNote").value;
	}catch(e){}
	var noteFileName="";
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="note"+noteDateTime+"."+ext;
	}
	CandidateGridAjax.saveNotes(teacherId,notes,noteId,noteFileName,
	{
		async:false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			showCommunicationsDiv();
			getCommunicationsDiv(commDivFlag,jobForTeacherGId,teacherId,jobId);				
		}
	});	
}
function deletesavefolder(rootNode)
{
	//alert("rootNode "+rootNode);
	$('#currentObject').val(rootNode.data.key);
	//$('#deleteFolder').modal('show');
	try{
		$('#deleteFolder').modal('show');
		}catch(err){}
	
}
function deleteconfirm()
{
	//alert("delexcv          m");
	//$('#deleteFolder').modal('hide');
	try{
		$('#deleteFolder').modal('hide');
		}catch(err){}
	var rootNodeKey =$('#currentObject').val();
	//parent.deleteFolder();
	document.getElementById('iframeSaveCandidate').contentWindow.deleteFolder(rootNodeKey);
	//document.getElementById('target_Frame').contentWindow.callingtargetFunction();
	//window.frames["iframeSaveCandidate"].deleteFolder();
	//window.frames["original_preview_iframe"].exportAndView(img_id);//
}

function saveToFolderJFTNULL(teachersaveId,teacherId,flagpopover)
{
	document.getElementById('teacherIdForHover').value=teacherId;
/*------------- Here I am creating only Home and Shared Folder For User*/	
	//$('#saveToFolderDiv').modal('show');
	try{
		$('#saveToFolderDiv').modal('show');
		}catch(err){}
	$('#errordiv').hide();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	
	innerDoc.getElementById('errortreediv').style.display="none";
	innerDoc.getElementById('errordeletetreediv').style.display="none";
	

	
	var folderId = innerDoc.getElementById('frame_folderId').value;
	if(innerDoc.getElementById('frame_folderId').value!="" || innerDoc.getElementById('frame_folderId').value.length>0)
	{
		var checkboxshowHideFlag =0; // For hide checkboxes on Save to Folder Div
		displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag);
		// return false;
	}
}

function saveCandidateToFolderByUser()
{
	document.getElementById('iframeSaveCandidate').contentWindow.checkFolderId();
	var iframe = document.getElementById('iframeSaveCandidate');
	var innerDoc = iframe.contentDocument || iframe.contentWindow.document;
	var folderId = innerDoc.getElementById('frame_folderId').value;
	//alert(" frame_folderId sdfsdfsdf "+folderId+" value "+folderId.value+" :::::: "+folderId.length);
	//alert(" folderId "+folderId);
	var frameHeight=iframe.contentWindow.document.body.scrollHeight;
	$('iframe').css('height',frameHeight);
	var fId="";
	if(innerDoc.getElementById('frame_folderId').value=="" || innerDoc.getElementById('frame_folderId').value.length==0)
	{
		//alert("Yes")
		innerDoc.getElementById('errortreediv').style.display="block";
		innerDoc.getElementById('errortreediv').innerHTML="&#149; "+resourceJSON.PlzSelectAnyFolder+"";
		 return false;
	}
	else
	{
		//alert(" Else No ");
		//fId=folderId;
	}
	//alert(" sgfsdgb "+innerDoc.getElementById('frame_folderId').value);

	
	
	var teacherId=	document.getElementById('teacherIdForHover').value
	
	
	CandidateGridAjax.saveCandidateToFolderByUserFromTeacherId(teacherId,folderId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" data "+data);// data will be 2: when first time Home and shared folder will be created
			if(data==1)
			{
				$("#saveToFolderDiv").modal("hide");
				$('#saveAndShareConfirmDiv').html(resourceJSON.SavedCandidate+".");
				$('#shareConfirm').modal("show");
				//document.getElementById("savedCandidateGrid").innerHTML=data;
				//displaySaveCandidatePopUpDiv();
				//uncheckedAllCBX();
			}
			if(data==3)
			{
				//alert("Duplicate Candaidate found ");
				$("#txtoverrideFolderId").val(folderId);
				try{
					$("#duplicatCandidate").modal("show");
				}catch(err)
				  {}
			}
		}
	});
	 
	 
}
function saveWithDuplicateRecord()
{
	$("#saveToFolderDiv").modal("hide");
	$("#duplicatCandidate").modal("hide");
}

function displaySavedCGgridByFolderId(hoverDisplay,folderId,checkboxshowHideFlag)
{
	currentPageFlag="stt";
	if(document.getElementById("checkboxshowHideFlag"))
		document.getElementById("checkboxshowHideFlag").value=checkboxshowHideFlag;
	if(document.getElementById("folderId"))
		document.getElementById("folderId").value=folderId;
	var pageFlag= document.getElementById("pageFlag").value;
	CandidateGridAjax.displaySavedCGgridByFolderId(pageFlag,folderId,pageFlag,pageSTT,noOfRowsSTT,sortOrderStrSTT,sortOrderTypeSTT,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data!=null)
			{
				document.getElementById("savedCandidateGrid").innerHTML=data;
				displaySaveCandidatePopUpDiv();
			}
		}
	});
}



/* ===== Gagan : displaySaveCandidatePopUpDiv() Method =========== */
function displaySaveCandidatePopUpDiv()
{
	
	var noOrRow = document.getElementById("saveCandidateTable").rows.length;
	for(var j=5000;j<=5000+noOrRow;j++)
	{
		
		$('.profile'+j).popover({ 
		 	trigger: "manual",
		    html : true,
		    placement: 'right',
		    content: $('#profileDetails'+j).html(),
		  }).on("mouseenter", function () {
		        var _this = this;
		        $(this).popover("show");
		        $(this).siblings(".popover").on("mouseleave", function () {
		            $(_this).popover('hide');
		        });
		    }).on("mouseleave", function () {
		        var _this = this;
		        setTimeout(function () {
		            if (!$(".popover:hover").length) {
		                $(_this).popover("hide")
		            }
		        }, 100);
		    });
	}
}



/*==================Gagan: Share Div Function Start Here ======================*/
function validateDelete()
{
	//var noOrRow = document.getElementById("saveCandidateTable").rows.length;
	//alert(" noOrRow "+noOrRow);
/*	for(var j=1;j<=noOrRow;j++)
	{
		
	}*/
	var savecandiadetidarray="";
	//var inputs = document.getElementsByTagName("input"); 
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	
	//alert(" errortreediv "+savecandiadetidarray);
	if(savecandiadetidarray=="" || savecandiadetidarray.length<0)
	{
		$("#errortreediv").show();
		document.getElementById("errortreediv").innerHTML="&#149; "+resourceJSON.msgSelectCandidate ;
		return false;
	}
	else
	{
		$("#deleteShareCandidate").modal("show");
		$("#savecandiadetidarray").val(savecandiadetidarray);
	}
	//var rootNode =$("#tree").dynatree("getActiveNode");
	//alert(" rootNode "+rootNode);
}

function deleteCandidate()
{
	var rootNode =$("#tree").dynatree("getActiveNode");
	var folderId=rootNode.data.key;
	var checkboxshowHideFlag=1; // For displaying Check box
	var savecandiadetidarray=$("#savecandiadetidarray").val();
	//alert(" folderId "+folderId+" savecandiadetidarray "+savecandiadetidarray);
	CandidateGridAjax.deleteCandidate(savecandiadetidarray,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			//alert(" ==== data === "+data);
		if(data==1)
		{
			displaySavedCGgridByFolderId(0,folderId,checkboxshowHideFlag)
			//$('#deleteShareCandidate').modal('hide');
			try{
				$('#deleteShareCandidate').modal('hide');
				}catch(err){}
			
		}
	}
	});
}


function displayUsergrid(teachersavedId,flagpopover)
{
	// flagpopover=1 that means User has clicked on pop up
	//alert(" teachersavedId,flagpopover "+teachersavedId+" -------------"+flagpopover);
	/*if(teachersavedId!="")
	{
		$("#teachersavedIdFromSharePoPUp").val(teachersavedId);
		$("#txtteachersavedIdShareflagpopover").val(flagpopover);
	}	*/
	
	document.getElementById("teachersharedId").value=teachersavedId;
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=1;
	document.getElementById("userCPS").value=0;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";
	
	$('#errorinvalidschooldiv').hide();
	if($('#entityType').val()==2)
	{
		//alert("---"+$('#schoolId').val())
		$('#schoolId').val("0");
		$('#schoolName').val("");
	}
	else
	{
		if($('#entityType').val()==3)
		{
			//alert("-School Id--"+$('#schoolId').val())
			$('#schoolId').val($('#loggedInschoolId').val());
			$('#schoolName').val($('#loggedInschoolName').val());
			//alert("-School Id--"+$('#schoolId').val())
		}
	}
	var savecandiadetidarray="";
	//var inputs = document.getElementsByTagName("input"); 
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	savecandiadetidarray+=	inputs[i].value+",";
	            }
	        }
	 } 
	 $("#savecandiadetidarray").val(savecandiadetidarray);
	if(savecandiadetidarray!="" || flagpopover==1)
	{
		//$('#schoolId').val("0");
		//$('#schoolName').val("");
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
			}catch(err){}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		//alert(" schoolId "+schoolId); 
		if(schoolId=="")
		{
			$('#errorinvalidschooldiv').show();
			$('#errorinvalidschooldiv').html("&#149;"+resourceJSON.SelectValidSchool+"");
			return false;
		}
		
		if($('#entityType').val()!="")
		{
			//alert(entityType+" "+districtId+" "+schoolId);
			/* ========= Display School List ============ */
			CandidateGridAjax.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
					//alert(" ==== data === "+data);
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}
		/*else
		{
			if(entityType==3)
			{
				alert("School Login");
			}
		}*/
	}
	else
	{
		$("#errortreediv").show();
		$('#errortreediv').html("&#149; "+resourceJSON.PlzSelectCandidates);
	}
	
}
function uncheckAllCbx()
{
	
	var inputs = document.getElementsByName("mycbx"); 
	 for (var i = 0; i < inputs.length; i++) {
	        if (inputs[i].type === 'checkbox') {
	        	if(inputs[i].checked){
	            	//savecandiadetidarray+=	inputs[i].value+",";
	        		inputs[i].checked=false;
	            }
	        }
	 } 
	
}
function searchUserthroughPopUp(searchFlag)
{
	if(searchFlag==1)
	{
		defaultShareDiv();
	}
	document.getElementById("userFPS").value=0;
	document.getElementById("userFPD").value=0;
	document.getElementById("userCPS").value=1;
	document.getElementById("userCPD").value=0;
	currentPageFlag="us";
	$('#errorinvalidschooldiv').hide();
		//$('#shareDiv').modal('show');
		try{
			$('#shareDiv').modal('show');
			}catch(err){}
		var entityType	=	$('#entityType').val();
		var districtId	=	$('#districtId').val();
		var schoolId	=	trim($('#schoolId').val());
		var schoolName	=	trim($('#schoolName').val());
		if(schoolId=="")
		{
			if( schoolName!="")
			{
				$('#errorinvalidschooldiv').show();
				$('#errorinvalidschooldiv').html("&#149;"+resourceJSON.SelectValidSchool+"");
				return false;
			}
			else
			{
				schoolId=0;
			}
			
		}
		if($('#entityType').val()!="")
		{
			CandidateGridAjax.getDistictSchoolUserList(entityType,districtId,schoolId,pageUS,noOfRowsUS,sortOrderStrUS,sortOrderTypeUS,
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{	
				if(data!="")
				{
					$('#divShareCandidateToUserGrid').html(data);
				}
			}
			});
		}
	
}




function shareCandidatethroughPopUp()
{
	defaultSaveDiv();
	$('#errorinvalidschooldiv').hide();
	$('#errorinvalidschooldiv').empty();
	var userIdarray =	"";
	var myUsercbx	=	document.getElementsByName("myUsercbx");
	 for (e=0;e<myUsercbx.length;e++) 
	 {
	  if (myUsercbx[e].checked==true) 
	  {
		  userIdarray+=myUsercbx[e].value+",";
	   }
	 }
	 	if(userIdarray=="")
	 	{
	 		$('#errorinvalidschooldiv').show();
	 		$('#errorinvalidschooldiv').html("&#149; "+resourceJSON.PlzSelectUsr+"");
	 	}
	 	var teachersharedId=document.getElementById("teachersharedId").value;
	 	var folderId = document.getElementById('folderId').value;
	 	
	 	CandidateGridAjax.shareCandidatesToUserForTeacher(userIdarray,teachersharedId,folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
			if(data==1)
			{
				$('#shareDiv').modal("hide");
				$('#saveAndShareConfirmDiv').html(resourceJSON.msgsuccessSharedCand);
				$('#shareConfirm').modal("show");
				//uncheckedAllCBX();
			}
		}
		});
}
/*
function defaultSet(){
	pageForTC = 1;
	noOfRowsForTC =100;
	noOfRowsForJob=10;
	sortOrderStrForTC="";
	sortOrderTypeForTC="";
	currentPageFlag="";
}
function getJobList(tId){
	defaultSet();
	currentPageFlag='job';
	teacherId=tId;
	getJobOrderList();
}

function getJobOrderList(){
	TeacherInfotAjax.getJobListByTeacher(teacherId,noOfRowsForJob,pageForTC,sortOrderStrForTC,sortOrderTypeForTC,true, { 
		async: true,
		callback: function(data)
		{
			$('#myModalJobList').modal('show');
			document.getElementById("divJob").innerHTML=data;
			applyScrollOnJob();
			//jobEnable();
		},
		errorHandler:handleError 
	});
}
*/

function setPageFlag(){
	defaultSet();
	currentPageFlag='';
}



//var noOfRowsForJob=10;
function defaultSet(){
	pageJob = 1;
	noOfRowsJob=50;
	sortOrderStrJob="";
	sortOrderTypeJob="";
	currentPageFlag="";
	//alert(noOfRowsForJob);
}

function getJobList(tId){
	defaultSet();
	currentPageFlag='job';
	teacherIdForjob=tId;
	getJobOrderList();
}

function getJobOrderList(){
	hideProfilePopover();
	currentPageFlag='job';
	$('#loadingDiv').show();
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherInfotAjax.getJobListByTeacherByProfile(teacherIdForjob,noOfRowsJob,pageJob,sortOrderStrJob,sortOrderTypeJob,false,visitLocation,0,0, { 
		async: true,
		callback: function(data)
		{
		try{
		$('#loadingDiv').hide();	
		//$('#myModalJobList').show(); 
		$('#myModalJobList').modal('show');
		
		document.getElementById("divJob").innerHTML=data;
		
		}catch(e){}			
		var noOrRow = document.getElementById("jobTable").rows.length;
		for(var j=1;j<=noOrRow;j++)
		{
			$('#tpJSA'+j).tooltip();
		}
		},
		errorHandler:handleError 
	});
}


function applyScrollOnJob()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 950,
        minWidth: null,
        minWidthAuto: false,
        colratio:[50,50,260,210,97,90,90,103], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}



//Teacher Profile

//start from by ramesh

/*
function showProfileContentClose(){
 $('.profile').popover('destroy');
 currentPageFlag="";
}
*/



function showProfileContentForTeacher(dis,teacherId,noOfRecordCheck,sVisitLocation,event)
{
	var scrollTopPosition = $(window).scrollTop();
	//alert("scrollTopPosition "+scrollTopPosition);
	var x=event.clientX;
	var y=event.clientY;
	var z=y-100+scrollTopPosition;
	var style = $('<style>.popover { position: absolute;top: '+z+'px !important;left: 0px;width: 770px; }</style>')
	$('html > head').append(style);
	
	showProfileContentClose();
	$('#loadingDiv').show();
	document.getElementById("teacherIdForprofileGrid").value=teacherId;
	TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,0,{ 
			async: true,
			callback: function(data){
			/*$(dis).closest('.profile').popover({ 
				    html : true,
				    placement: 'right',
				    content: data,
				  })*/
				  
			
				  $('#loadingDiv').hide();
			
				  
				  /*@Start 

				  @Ashish 

				  @Description :: for Mosaic Teacher Profile(cgTeacherDivMaster)*/

				  //$("#draggableDivMaster").modal('show');
				  
					  $("#draggableDivMaster").modal('show');
					  
					  //var wait="<div id='removeWait' style='text-align:center;padding-top:0px;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div>";
						$("#draggableDiv").html(data);
				       
						TeacherProfileViewInDivAjax.showProfileContentForTeacher(teacherId,noOfRecordCheck,sVisitLocation,districtMaster,1,{ 
							async: true,
							callback: function(resdata){
							setTimeout(function () {
								$("#profile_id").after(resdata);
								$('#removeWait').remove();
					        }, 100);
							
							//$("#draggableDiv").html(resdata);
						}
						});

				  /*@End 

				  @Ashish 

				  @Description :: for Mosaic Teacher Profile(cgTeacherDivMaster)*/
				  
				 // $('.profile').popover('show')
				  
				  $('#tpResumeprofile').tooltip();
				  $('#tpPhoneprofile').tooltip();
				  $('#tpPDReportprofile').tooltip();
				  $('#tpTeacherProfileVisitHistory').tooltip();
				  $('#teacherProfileTooltip').tooltip();
				  $('textarea').jqte();
				  /*currentPageFlag="workExp";
				  getPagingAndSorting('1',"CreatedDateTime",1);
				  
				  currentPageFlag="videoLink";
				  getPagingAndSorting('1',"createdDate",1);
				  
				  currentPageFlag="teacherAce";
				  getPagingAndSorting('1',"createdDateTime",1);
				  
				  currentPageFlag="teacherCerti";
				  getPagingAndSorting('1',"createdDateTime",1);*/
				  
				  //currentPageFlag="References";
				  //getPagingAndSorting('1',"lastName",1);
				  
	},
	errorHandler:handleError  
	});
} 

function getPhoneDetailShowPro(teacherId){
	hideProfilePopover();
	TeacherProfileViewInDivAjax.getPhoneDetailByDiv(teacherId,
	{
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			if(data==""){
				document.getElementById("divAlertText").innerHTML=resourceJSON.MsgPhNoIsNotAvailable;
				try{$('#divAlert').modal('show');
								}catch(err){}
			}
			else{
				document.getElementById("divPhoneByPro").innerHTML=data;			
				try{$('#myModalPhoneShowPro').modal('show');
				}catch(err){}
			}
			
		}
	});
}


function setGridProfileVariable(currentPageFlagValue){
	currentPageFlag=currentPageFlagValue;
}

function getTeacherProfileVisitHistoryShow_FTime(teacherId){
	var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
	currentPageFlag="tpvh";
	getPagingAndSorting('1',"visitedDateTime",1);
}

function getTeacherProfileVisitHistoryShow(teacherId){
	
	hideProfilePopover();	
	$('#loadingDiv').show();	
	currentPageFlag="tpvh";
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getGridDataForTeacherProfileVisitHistoryByTeacher(teacherId,noOfRowstpvh,pagetpvh,sortOrderStrtpvh,sortOrderTypetpvh,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();			
			try{
				$('#divteacherprofilevisithistory').html(data);				
				//applyScrollOnTblProfileVisitHistory();			
				$('#myModalProfileVisitHistoryShow').modal('show');
			}catch(e){alert(e)}
			/*currentPageFlag="tpvh";
			getPagingAndSorting('1',"visitedDateTime",1);*/
			
		}});
}


function getDistrictSpecificQuestion_DivProfile(districtId,teacherId,sVisitLocation)
{	
	TeacherProfileViewInDivAjax.getDistrictSpecificQuestionGrid_ByDistrict(districtId,teacherId,sVisitLocation,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#getDistrictSpecificQuestion').html(data);
		}});
}

function getQuestionExplain(questionID)
{
	TeacherProfileViewInDivAjax.getQAExplaination(questionID,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$("#divExplain").html(data);
		try{
				$('#myModalQAEXEditor').show();
				
				$("#draggableDivMaster").hide();	
			}catch(e){
				//alert(e);
			}
		}
		});
}
function showPreviousModel()
{
	$("#draggableDivMaster").show();
	$('#myModalQAEXEditor').hide();
}

// Level 01 :: Certifications Grid
function getTeacherCertificationsGrid_DivProfile(teacherId)
{
	//alert(" 01 Certifications Grid");
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherCertificationsGrid(teacherId,noOfRowsteacherCerti,pageteacherCerti,sortOrderStrteacherCerti,sortOrderTypeteacherCerti,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherCertifications').html(data);
		}});
}

//Level 02 :: Academics Grid

function getTeacherAcademicsGrid_DivProfile(teacherId)
{
	//alert(" 02 :: Academics Grid");
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getTeacherAcademicsGrid(teacherId,noOfRowsteacherAce,pageteacherAce,sortOrderStrteacherAce,sortOrderTypeteacherAce,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataTeacherAcademics').html(data);
		}});
}

function getHonorsGrid(teacherId)
{
	alert("getHonorsGrid:-");
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getHonorsGridWithoutAction(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			document.getElementById("gridDataTeacherHonors").innerHTML=data;
			applyScrollOnHonors();
			
		}});
}

/*function applyScrollOnHonors()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#honorsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[962], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
*/

var dp_Involvement_Rows=10;
var dp_Involvement_page=1;
var dp_Involvement_sortOrderStr="";
var dp_Involvement_sortOrderType="";

function getInvolvementGrid(teacherId)
{
	TeacherProfileViewInDivAjax.getInvolvementGrid(teacherId,dp_Involvement_Rows,dp_Involvement_page,dp_Involvement_sortOrderStr,dp_Involvement_sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			//document.getElementById("divDataInvolvement").innerHTML=data;
			$('#gridDataTeacherInvolvementOrVolunteerWork').html(data);
			//applyScrollOnInvl();
		}});
}

function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#involvementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[250,250,175,176,111], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

//Level 03 :: Work Experience
function getPFEmploymentGrid_DivProfile(teacherId)
{
	//alert(" 03 :: Work Experience");
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getGridDataWorkExpEmployment(teacherId,noOfRowsworkExp,pageworkExp,sortOrderStrworkExp,sortOrderTypeworkExp,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataWorkExpEmployment').html(data);
		}});
}

//Level 04 :: Reference Grid
function getElectronicReferencesGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	var jobId=document.getElementById("jobId").value;
	
	TeacherProfileViewInDivAjax.getElectronicReferencesGrid(teacherId,noOfRowsReferences,pageReferences,sortOrderStrReferences,sortOrderTypeReferences,visitLocation,jobId,districtMaster,schoolMaster, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataReference').html(data);
		}});
}

//Level 05 :: Video Grid
function getVideoLinksGrid_DivProfile(teacherId)
{
	//alert(" 05 :: Video Grid");
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getVideoLinksGrid(teacherId,noOfRowsvideoLink,pagevideoLink,sortOrderStrvideoLink,sortOrderTypevideoLink,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataVideoLink').html(data);
		}});
}



//For additional doc
function getadddocumentGrid_DivProfile(teacherId)
{
	//alert(noOfRowsvideoLink+":::::::::::::"+pagevideoLink+"::"+sortOrderStrvideoLink+"::"+sortOrderTypevideoLink+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getAdditionalDocumentsGrid(noOfRowsaddDoc,pageaddDoc,sortOrderStraddDoc,sortOrderTypeaddDoc,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAdditionalDocuments').html(data);
		}});
}

function updateAScore(teacherId,districtId){
	
	var slider_aca_ach=0;
	var slider_lea_res=0;
	try{
		var ifrm_aca_ach = document.getElementById('ifrm_aca_ach');
		var innerNorm = ifrm_aca_ach.contentDocument || ifrm_aca_ach.contentWindow.document;
		if(innerNorm.getElementById('slider_aca_ach').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_aca_ach=innerNorm.getElementById('slider_aca_ach').value;
		}
		}catch(e){}
		
		
	try{
		var ifrm_lea_res = document.getElementById('ifrm_lea_res');
		var innerNorm1 = ifrm_lea_res.contentDocument || ifrm_lea_res.contentWindow.document;
		if(innerNorm1.getElementById('slider_lea_res').value!=''){
			//$('#candidateFeedNormScore').val(innerNorm.getElementById('slider_aca_ach').value);
			slider_lea_res=innerNorm1.getElementById('slider_lea_res').value;
		}
		}catch(e){}
	
		var txt_sl_slider_aca_ach = document.getElementById('txt_sl_slider_aca_ach').value;
		var txt_sl_slider_lea_res = document.getElementById('txt_sl_slider_lea_res').value;
		
		//if((slider_aca_ach!=0 || slider_lea_res!=0) && ( txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res)){
		if(txt_sl_slider_aca_ach!=slider_aca_ach || txt_sl_slider_lea_res!=slider_lea_res){
			TeacherProfileViewInDivAjax.getUpdateAndSaveAScore(teacherId,districtId,slider_aca_ach,slider_lea_res,{ 
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{
					if(data!="")
					{
						document.getElementById('txt_sl_slider_aca_ach').value=slider_aca_ach;
						document.getElementById('txt_sl_slider_lea_res').value=slider_lea_res;
						$('#ascore_profile').html(data);	
					}
				
				}});
		}
}
// ADD PDP Configuration.
function openDistrictlist(){
	var modalDownloadPflag=true;
	var errorflag=true;
	$('#errorDivDistrictlist').empty();
	$('#modalDistrictDetails').css("background-color", "");
	var districtDetail = document.getElementById("modalDistrictDetails");
	var districtId = districtDetail.options[districtDetail.selectedIndex].value;
	var districtName = districtDetail.options[districtDetail.selectedIndex].text;
	if(districtDetail.options[districtDetail.selectedIndex].text=="Select District"){
		$('#errorDivDistrictlist').show();
		$('#errorDivDistrictlist').append("&#149; "+"Select District"+"<br>");
    	$('#modalDistrictDetails').focus();
    	$('#modalDistrictDetails').css("background-color", "#F5E7E1");
    	errorflag=false;
    	return false;
	}
	$("#selectDistrictDiv").modal("hide");
	var teacherId=document.getElementById("districtlistdivteacherid").value;
	CGServiceAjax.generatePDPReportForMultipleJobApplyDistrict(districtId,teacherId,{		
		async: false,
		errorHandler:handleError,
		callback: function(data){
			if(deviceType)
			{	
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{	
					$('#modalDownloadPDR').modal('hide');
					$("#draggableDivMaster").modal('show');	
					document.getElementById(hrefId).href = data;
				
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					    $('#modalDownloadPDR').modal('hide');
					    document.getElementById('ifrmPDR').src = ""+data+"";
				}
				else
				{
					$('#modalDownloadPDR').modal('hide');
					$("#draggableDivMaster").modal('show');	
					document.getElementById(hrefId).href = data;
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				    $('#modalDownloadPDR').modal('hide');
				    document.getElementById('ifrmPDR').src = ""+data+"";
			}
			else
			{
				    $('#modalDownloadPDR').modal('hide');
					  document.getElementById('ifrmPDR').src = ""+data+"";
				try{
					$('#modalDownloadPDR').modal('show');
				}catch(err)
				  {}
			}		
		}
	});
}

function generatePDReport(teacherId,hrefId)
{
	
	var moredisflag=true;
	var modalDownloadPflag=true;
	hideProfilePopover();
	$('#loadingDiv').show();
	CandidateReportAjax.generatePDReport(teacherId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){

		$('#loadingDiv').hide();	
		
		$('#loadingDiv').hide();
		document.getElementById("districtlistdivteacherid").value = teacherId;
		var districtselect=data.split("##");
		if(districtselect[1]=="Case3DistrictList"){
			var districts = districtselect[0].split("!!!!");
			document.getElementById('modalDistrictDetails').innerHTML = "";
			$('#modalDistrictDetails').append("<option value="+"0"+"selected="+"selected"+">"+"Select District"+"</option>");
			for(var i=0; i<districts.length; i++){
				var district = districts[i].split("!!!");
				$('#modalDistrictDetails').append("<option value="+district[0]+">"+district[1]+"</option>");
			}
			 moredisflag=false;
			$("#draggableDivMaster").modal('show');
			$("#selectDistrictDiv").modal("show");
				
		}
		if(moredisflag){
		if(deviceType)
		{	
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				$("#draggableDivMaster").modal('show');		
				window.open(data, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;
				
			
			}
		}
		else if(deviceTypeAndroid)
		{
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				    try{
			    	 $('#modalDownloadPDR').modal('hide');
			    	}catch(err){}
			        document.getElementById('ifrmPDR').src = ""+data+"";
			}
			else
			{
				$("#draggableDivMaster").modal('show');		
				window.open(data, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;
			}	
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			    try{
		    	 $('#modalDownloadPDR').modal('hide');
		    	}catch(err){}
		        document.getElementById('ifrmPDR').src = ""+data+"";
		}
		else
		{			
			$('#modalDownloadPDR').modal('hide');
			document.getElementById('ifrmPDR').src = ""+data+"";
			try{
				$('#modalDownloadPDR').modal('show');
			}catch(err)
			  {}
			
		
		}		
	}
	}
	});	
}


function downloadResume(teacherId,linkId)
{
	try{
		$('#loadingDiv').show();
	}catch (e) {
		var $j=jQuery.noConflict();
		$j('#loadingDiv').show();
	}
	CandidateReportAjax.downloadResume(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
		try{
			$('#loadingDiv').show();
		}catch (e) {
			var $k=jQuery.noConflict();
			$k('#loadingDiv').show();
		}
			if(data==""){			
				alert(resourceJSON.ResumeNotUploaded)
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						try{
							$("#docfileNotOpen").css({"z-index":"3000"});
					    	$('#docfileNotOpen').show();
						}catch (e) {
							var $k1=jQuery.noConflict();
							$k1("#docfileNotOpen").css({"z-index":"3000"});
					    	$k1('#docfileNotOpen').show();
						}
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						try{
							$("#exelfileNotOpen").css({"z-index":"3000"});
					    	$('#exelfileNotOpen').show();
						}catch (e) {
							var $k2=jQuery.noConflict();
							$k2("#exelfileNotOpen").css({"z-index":"3000"});
					    	$k2('#exelfileNotOpen').show();
						}
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						 document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 	
				}			
			}				
			return false;		
		}
	});
}


//Start for reference notes

function closeRefNotesDivEditor()
{
	var eleRefId=document.getElementById("eleRefId").value;
	getRefNotesDiv(eleRefId);
}

function getRefNotesDiv(eleRefId)
{	
	document.getElementById("eleRefId").value=eleRefId;
	hideProfilePopover();
	$('#myModalReferenceNotesEditor').modal('hide');
	var jobId = document.getElementById("jobId").value;

	TeacherProfileViewInDivAjax.getRefeNotes(eleRefId,jobId,districtMaster,schoolMaster,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){	
		$("#divRefNotesInner").html(data);	
		$('#myModalReferenceNoteView').modal('show');
		//$('#myModalReferenceNoteView').show()			
		$('#refeNotesId').tooltip();
	}
	});
}

function getNotesEditorDiv()
{
	$('#myModalReferenceNoteView').modal('hide');
	
	removeRefeNotesFile();
	$('#errordivNotes_ref').empty();
	$('#divTxtNode_ref').find(".jqte_editor").html("");	
	$('#divTxtNode_ref').find(".jqte_editor").css("background-color", "");
	$('#myModalReferenceNotesEditor').modal('show');
	
}


function saveReferenceNotes(){
	var noteFileName="";
	var notes = $('#divTxtNode_ref').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	
	var cnt=0;
	var focs=0;
	
	$('#errordivNotes_ref').empty();
	
	if ($('#divTxtNode_ref').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzEtrNotes+".<br>");
		if(focs==0)
			$('#divTxtNode_ref').find(".jqte_editor").focus();
		$('#divTxtNode_ref').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;
	}else if(fileNote==""){
		$('#errordivNotes_ref').show();
		$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzUpldNotes+".<br>");
		cnt++;
	}else if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("fileNote_ref").files[0]!=undefined)
			{
				fileSize = document.getElementById("fileNote_ref").files[0].size;
			}
		}
		
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt' || ext=='xlsx' || ext=='xls'))
		{
			$('#errordivNotes_ref').show();
			$('#errordivNotes_ref').append("&#149; "+resourceJSON.PlzSelectNoteFormat+".<br>");
			cnt++;
		}
		else if(fileSize>=10485760)
		{
			$('#errordivNotes_ref').show();
			$('#errordivNotes_ref').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+".<br>");
			cnt++;
		}
	}
	if(cnt==0){	
		try{
			$('#loadingDiv').show();
			if(fileNote!="" && fileNote!=null)
			{
				document.getElementById("frmNoteUpload_ref").submit();
			}
			else
			{
				TeacherProfileViewInDivAjax.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
				{
					async:false,
					errorHandler:handleError,
					callback:function(data)
					{
						$('#loadingDiv').hide();
						getRefNotesDiv(eleRefId);
					}
				});	
			}
		}catch(err){}
	}

	
}

function saveRefNoteFile(noteDateTime)
{	
	var notes = $('#divTxtNode_ref').find(".jqte_editor").html();
	var teacherId = document.getElementById("teacherIdForprofileGrid").value;
	var eleRefId = document.getElementById("eleRefId").value;
	var jobId = document.getElementById("jobId").value;
	
	var fileNote=null;
	try{
		fileNote=document.getElementById("fileNote_ref").value;
	}catch(e){}
	
	var noteFileName="";
	
	if(fileNote!="" && fileNote!=null)
	{
		var ext = fileNote.substr(fileNote.lastIndexOf('.') + 1).toLowerCase();	
		noteFileName="ref_note"+noteDateTime+"."+ext;
	}
	
	TeacherProfileViewInDivAjax.saveReferenceNotesByAjax(eleRefId,teacherId,jobId,notes,noteFileName,
			{
				async:false,
				errorHandler:handleError,
				callback:function(data)
				{
					$('#loadingDiv').hide();
					getRefNotesDiv(eleRefId);
				}
			});	
}

function addRefeNoteFileType()
{
	$('#fileRefNotes').empty();
	$('#fileRefNotes').html("<a href='javascript:void(0);' onclick='removeRefeNotesFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='fileNote_ref' name='fileNote_ref' size='20' title='"+resourceJSON.ChooseFile+"' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;"+resourceJSON.MaxSize10Mb+".");
}

function removeRefeNotesFile()
{
	$('#fileRefNotes').empty();
	$('#fileRefNotes').html("<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>"+resourceJSON.AttachFile+"</a>");
}

function downloadReferenceNotes(filePath,fileName,linkId)
{
	$('#loadingDiv').show();
	TeacherProfileViewInDivAjax.downloadReferenceNotes(filePath,fileName,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	
			$('#loadingDiv').hide();
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkId).href = data; 
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 	
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById(linkId).href = data; 	
			}			 
			return false;	
		}
	});
}


function showProfilePopover()
{
		//$('.profile').popover('show');
		$("#draggableDivMaster").modal('show');
		$('#myModalJobList').hide(); // @AShish :: Hide for Job Details(myModalJobList) Div
		$('#myModalReferenceNoteView').hide();
		
		//Refresh for Reference Notes
		var teacherIdForprofileGrid=document.getElementById("teacherIdForprofileGrid").value;
		getElectronicReferencesGrid_DivProfile(teacherIdForprofileGrid);
		
		$('#tpResumeprofile').tooltip();
		$('#tpPhoneprofile').tooltip();
		$('#tpPDReportprofile').tooltip();
		$('#tpTeacherProfileVisitHistory').tooltip();
		$('#teacherProfileTooltip').tooltip();
}
function hideProfilePopover()
{
	//$('.profile').popover('hide');
	$("#draggableDivMaster").modal('hide');	
}

function generatePilarReport(teacherId,jobId,hrefId)
{
	$("#myModalJobList").css({"z-index":"1"});
	jobOrder={jobId:jobId};
	CandidateReportAjax.generatePilarReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#loadingDiv').hide();
		if(data==0)
		{
			$('#myModalMsgShow').html(resourceJSON.NoJobSpecificInventory);
			$('#myModalJobList').modal('hide');
			$('#myModalMsg').modal('show');
			return false;
		}else if(data==1)
		{
			$('#myModalMsgShow').html(resourceJSON.CandidateHasNotCompleteThisJobOrder);
			$('#myModalJobList').modal('hide');
			$('#myModalMsg').modal('show');
			return false;
		}else if(data==2)
		{
			$('#myModalMsgShow').html(resourceJSON.CandidateHasTimeOut);
			$('#myModalJobList').modal('hide');
			$('#myModalMsg').modal('show');
			return false;
		}else if(data==3){
			$("#myModalJobList").css({"z-index":"3000"});
			generateJSAReport(teacherId,jobOrder,hrefId);
		}
	}
	});	
}

function generateJSAReport(teacherId,jobOrder,hrefId)
{
	$('#loadingDiv').show();
	CandidateReportAjax.generateJSAReport(teacherId,jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#loadingDiv').hide();

		document.getElementById(hrefId).href=data;
	}
	});	
}

function downloadTranscriptNew(academicId,linkid)
{	
	CandidateGridAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkid).href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 $('#modalDownloadsTranscript').modal('hide');
					    document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{				
					document.getElementById(linkid).href = data;
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				 $('#modalDownloadsTranscript').modal('hide');
				    document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{				
				document.getElementById('ifrmTrans').src = ""+data+"";
				$("#myModalTranscript").css({"z-index":"1000"});
				try{
					$('#modalDownloadsTranscript').modal('show');
				}catch(err)
				  {}
			}			
		}
	});
}

function downloadTranscript(academicId,linkid)
{	
	CandidateGridAjax.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{		
			if(deviceType)
			{
				if (data.indexOf(".doc")!=-1)
				{
					$("#docfileNotOpen").css({"z-index":"3000"});
			    	$('#docfileNotOpen').show();
				}
				else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();
				}
				else
				{
					document.getElementById(linkid).href = data;
				}
			}
			else if(deviceTypeAndroid)
			{
				if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					 $('#modalDownloadsTranscript').modal('hide');
					    document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkid).href = data;
				}	
			}
			else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				 $('#modalDownloadsTranscript').modal('hide');
				    document.getElementById('ifrmTrans').src = ""+data+"";
			}
			else
			{
				document.getElementById('ifrmTrans').src = ""+data+"";
				$("#myModalTranscript").css({"z-index":"1000"});
				try{
					$('#modalDownloadsTranscript').modal('show');
				}catch(err)
				  {}
			}	
		}
	});
}
//changed: by rajendra
function downloadReferenceForCandidate(referenceId,teacherId,linkId)
{		
		PFCertifications.downloadReferenceForCandidate(referenceId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;
					}
				}
				else if(deviceTypeAndroid)
				{
					    if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
						{
							document.getElementById("ifrmRef").src=data;
						}
						else
						{
							document.getElementById(linkId).href = data;	
						}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					document.getElementById(linkId).href = data;	
				}			
			}
			return false;			
		}});
}
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}

var isCertification  = false,
	isSchool         = false,
	isAllJobOrder    = false,
	isNumberofopening=false

function chknoOfOpeningValue()
{  
 if(document.getElementById("numOpeningSelectVal").value!=0)
  {
	 $("#numberofopening").prop('disabled', false);
  }
 else
 {
	 $("#numberofopening").prop('disabled', true); 
	 document.getElementById('numberofopening').value='';
 }
}
	
function getjoborderDetails()
{
  $('#joborderchartDiv').modal('show');
  var schoolId = document.getElementById("schoolId").value;
  var stateId=0,
      certType=0;
	$('#joborderbySchoolscontainer').html("<div class='img-responsive' id='loadingDivJobStatus' style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	CommonDashboardAjax.getJobOrderStatusBySchools(districtMaster,schoolMaster,schoolId,stateId, certType,false,false,0,0,{
		async: true,
		callback: function(data){
      
       $('#loadingDivJobStatus').hide();
       
       $('#legendforchartDiv').show();
       var div = d3.select("#joborderbySchoolscontainer").append("div").attr("class", "toolTip");
		 
		var color = d3.scale.category20();
		
		 var width = 700,
		    height = 380,
		    cwidth = 85;

		var svg = d3.select("#joborderbySchoolscontainer").append("svg")
		    .attr("width", width)
		    .attr("height", height)
		    .append("g")
		    .attr("transform", "translate(420,170)");

		var color = d3.scale.ordinal().range(["#FF1919", "#FFF019", "#A4D53A"]);

		    var arc = d3.svg.arc();
		   
		    var pie = d3.layout.pie()
		    .value(function(d) { return d; });
		     
		    var gs = svg.selectAll("g").data(d3.values(data)).enter().append("g");
		 
		    var path = gs.selectAll("path")
		    .data(function(d) { return pie(d); })
		    .enter().append("path")
		    .attr("fill", function(d, i) { return color(i); })
		    .attr("data-legend", function(d){ return d;})
		    .attr("d", function(d, i, j) { return arc.innerRadius(20+cwidth*j).outerRadius(cwidth*(j+1))(d);
		    });
		
		    path.append("text")
		      .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
		      .attr("dy", ".35em")
		      .style("text-anchor", "middle")
		      .text(function(d) { return d.data; });

		  
		 //adding tooltip
		 path.on("mousemove", function(d){
		    div.style("left", (d3.event.pageX+12)+"px");
		    div.style("top", (d3.event.pageY-10)+"px");
			div.style("display", "inline-block");
			div.html(d.value+"%");
		});

		path.on("mouseout", function(d){
		    div.style("display", "none");
		});

		path.on("click",function(d){
        	if(d3.select(this).attr("transform") == null){
        	d3.select(this).attr("transform","translate(40,0)");
        	}else{
        		d3.select(this).attr("transform",null);
        	}
	        });
		
	},
	errorHandler:handleError  
	});

}


// mukesh 
function getjoborderDetailsBySchools()
{
	$('#cerErrorDiv').empty();
	var schoolId = document.getElementById("schoolId").value;
	
	var stateId =  document.getElementById("stateId").value;
	//alert("stateId :: "+stateId);
	
	if(document.getElementById("certType").value==''){
		document.getElementById("certificateTypeMaster").value=0;
	}
	var certType		=	trim(document.getElementById("certificateTypeMaster").value);
	//alert("certType :: "+certType)
	
	if(isCertification)
	if(stateId==0 && certType==0){
		$('#cerErrorDiv').css("color","red");
		$('#cerErrorDiv').append("&#149; "+resourceJSON.msgCertLicStateorName+"<br>");
		return false;
	}
	
	
	 var numOpeningSelectVal		=	trim(document.getElementById("numOpeningSelectVal").value);
	 var numberofopening		=	trim(document.getElementById("numberofopening").value);
	 
	 if(numOpeningSelectVal!=0 && isNumberofopening==true && numberofopening=='')
	 {
		$('#cerErrorDiv').css("color","red");
		$('#cerErrorDiv').append("&#149; "+resourceJSON.msgPlzPvdnumberopening+"<br>");
		return false;
	 }
	
	$('#joborderbySchoolscontainer').html("<div class='img-responsive' id='loadingDivJobStatus' style='text-align:center;padding-top:100px;'><img src='images/loadingAnimation.gif' /> <br>"+resourceJSON.MsgLoad+"</div>");
	
	CommonDashboardAjax.getJobOrderStatusBySchools(districtMaster,schoolMaster,schoolId,stateId,certType,isCertification,isNumberofopening,numOpeningSelectVal,numberofopening,{ 
		async: true,
		callback: function(data){
         
          $('#loadingDivJobStatus').hide();
          $('#legendforchartDiv').show();
          $('#searchItems').hide();
          $('#searchIdjob').show();
          
		var div = d3.select("#joborderbySchoolscontainer").append("div").attr("class", "toolTip");
		 
		var color = d3.scale.category20();
		 
	
		 var width = 700,
		    height = 380,
		    cwidth = 85;

		var svg = d3.select("#joborderbySchoolscontainer").append("svg")
		    .attr("width", width)
		    .attr("height", height)
		    .append("g")
		    .attr("transform", "translate(420,170)");

		var color = d3.scale.ordinal().range(["#FF1919", "#FFF019", "#A4D53A"]);

		    var arc = d3.svg.arc();
		   
		    var pie = d3.layout.pie()
		    .value(function(d) { return d; });
		     
		    var gs = svg.selectAll("g").data(d3.values(data)).enter().append("g");
		 
		    var path = gs.selectAll("path")
		    .data(function(d) { return pie(d); })
		    .enter().append("path")
		    .attr("fill", function(d, i) { return color(i); })
		    .attr("data-legend", function(d){ return d;})
		    .attr("d", function(d, i, j) { return arc.innerRadius(20+cwidth*j).outerRadius(cwidth*(j+1))(d);
		    });
		
		  
		 //adding tooltip
		 path.on("mousemove", function(d){
			    div.style("left", (d3.event.pageX+12)+"px");
			    div.style("top", (d3.event.pageY-10)+"px");
				div.style("display", "inline-block");
		    div.html(d.value+"%");
		});

		path.on("mouseout", function(d){
		    div.style("display", "none");
		});
		
		path.on("click",function(d){
        	if(d3.select(this).attr("transform") == null){
        	d3.select(this).attr("transform","translate(40,0)");
        	}else{
        		d3.select(this).attr("transform",null);
        	}
	      });
	
		
	
	},
	errorHandler:handleError  
	});

 
}



function activecityType(){
	document.getElementById("certType").value='';
}

function clearText()
{
	document.getElementById("schoolId").value='';
	document.getElementById("schoolName1").value='';
}



function chartjobOrderByOpening()
{
	    isCertification = false
	    isSchool        = false
	    isAllJobOrder   = false
	    isNumberofopening=true
	   
	$('#cerErrorDiv').empty();
	$('#jobCertDiv').hide();
	$('#searchIdjob').show();
	$('#searchItems').hide();
	$('#districtSchoolDiv').show();
	$('#numberofopeningDiv').show();
	getjoborderDetails();
	$('#districtSchoolDiv').show();
}

    
function chartjobOrderByCertification()
{
	    isCertification = true
	    isSchool        = false
	    isAllJobOrder   = false
	    isNumberofopening=false
	$('#cerErrorDiv').empty();
	$('#numberofopeningDiv').hide();
	$('#jobCertDiv').show();
	$('#searchIdjob').show();
	$('#searchItems').hide();
	$('#districtSchoolDiv').show();
	getjoborderDetails();
	$('#districtSchoolDiv').show();
}
function getjoborderDetailsInAllJob()
{
	 isCertification    = false
	    isSchool        = false
	    isAllJobOrder   = true
	    isNumberofopening=false
	$('#cerErrorDiv').empty();
	$('#searchIdjob').hide();
	$('#searchItems').hide();
	$('#numberofopeningDiv').hide();
	getjoborderDetails();
}

function getjoborderDetailsBySchoolsDefault()
{
 document.getElementById("stateId").value=0;
 document.getElementById("certificateTypeMaster").value=0;
 isCertification    = false
    isSchool        = true
    isAllJobOrder   = false
    isNumberofopening=false
	$('#cerErrorDiv').empty();
	$('#searchIdjob').show();
	$('#jobCertDiv').hide();
	$('#searchItems').hide();
	$('#districtSchoolDiv').show();
	$('#numberofopeningDiv').hide(); 
	 getjoborderDetails();
}

function downloadUploadedDocument(additionDocumentId,linkId,teacherid)
{		
	    $("#myModalLabelText").html("Document");	
		PFCertifications.downloadUploadedDocumentForCg(additionDocumentId,teacherid,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data=="")
			{
				data="javascript:void(0)";
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data;
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById("ifrmRef").src=data;
					}
					else
					{
						document.getElementById(linkId).href = data;
					}	
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById("ifrmRef").src=data;
				}
				else
				{
					try{
						document.getElementById('ifrmTransCommon').src = ""+data+"";
						$('#modalDownloadsCommon').modal('show');								
					}catch(err)
					  {
						alert(err)
					  }
				}			
			}	
			return false;			
		}});
}


/*========================== TFA  Edit By Specific users ==========================================*/
function editTFAbyUser(teacherId)
{
	$("#draggableDivMaster").show();
	$('#edittfa').hide();
	$('#savetfa').show();
	$('#canceltfa').show();
	document.getElementById("tfaList").disabled=false;
}

function saveTFAbyUser()
{
	$("#tfaMsgShow").hide();
	
	var tfa = document.getElementById("tfaList").value;
	var teacherId = document.getElementById("teacherDetailId").value;
	
	TeacherInfotAjax.updateTFAByUser(teacherId,tfa,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			var tfalist = document.getElementById("tfaList");
			if(data!=null)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
			else
			{
				tfalist.selectedIndex = 1;
			}
			
			$("#draggableDivMaster").show();
			$('#edittfa').show();
			$('#savetfa').hide();
			$('#canceltfa').hide();
			document.getElementById("tfaList").disabled=true;
		}
	});
}

function cancelTFAbyUser(teacherId)
{
		$("#draggableDivMaster").show();
		$('#edittfa').show();
		$('#savetfa').hide();
		$('#canceltfa').hide();
		document.getElementById("tfaList").disabled=true;
		var tfalist = document.getElementById("tfaList");
		TeacherInfotAjax.displayselectedTFA(teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				for(var i=0; i<tfalist.options.length;i++)
				{	
					if(tfalist.options[i].value==data)
					{
						tfalist.options[i].selected = true;
					}
				}
			}
		});
		
}

function displayTFAMessageBox(teacherId)
{
	document.getElementById("teacherDetailId").value=teacherId;	
	$("#tfaMsgShow").show();
}

function closeTFAMsgBox()
{
	$("#tfaMsgShow").hide();
}
function saveDistrictSpecificQues(teacherId){	
	$('#errordivspecificquestion').empty();
	var arr =[];
	var jobOrder = {jobId:document.getElementById("jobId").value};
	var dspqType=document.getElementById("dspqType").value;
	var isRequiredCount=0;
	var ansRequiredCount=0;

	var totalQuestionsList=document.getElementById("totalQuestionsList").value;

	for(i=1;i<=totalQuestionsList;i++)
	{   
		var isRequiredAns=0;
		var isRequired = dwr.util.getValue("QS"+i+"isRequired");
		if(isRequired==1){
			isRequiredCount++;
		}
		var districtSpecificQuestion = {questionId:dwr.util.getValue("QS"+i+"questionId")};
		var questionTypeShortName = dwr.util.getValue("QS"+i+"questionTypeShortName");
		var questionTypeMaster = {questionTypeId:dwr.util.getValue("QS"+i+"questionTypeId")};
		var qType = dwr.util.getValue("QS"+i+"questionTypeShortName");
		var o_maxMarks = dwr.util.getValue("o_maxMarksS");
		
		var scroreVal=$('#dstQuesSlid'+i).val();		
			if ($("#ifrmQ"+i).length > 0) {
				var iframeNorm = document.getElementById("ifrmQ"+i);
				var innerNorm = iframeNorm.contentDocument || iframeNorm.contentWindow.document;
				if(innerNorm.getElementById('sliderQ'+i).value!='')
				$('#dstQuesSlid'+i).val(innerNorm.getElementById('sliderQ'+i).value);
				scroreVal=innerNorm.getElementById('sliderQ'+i).value;		
			}
		
		if(qType=='tf' || qType=='slsel' ||  qType=='slsel')
		{
			var optId="";
			var errorFlag=1;
			if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
			{
				 errorFlag=0;
				optId=$("input[name=QS"+i+"opt]:radio:checked").val();
			}else if(isRequired==1){
				$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				dSPQuestionsErrorCount=1;
				errorFlag=1;
			}
			if(errorFlag==0){
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : dwr.util.getValue("QS"+optId+"validQuestion"),
					"jobOrder" : jobOrder,
					"sliderScore" : scroreVal
				});
			}

		}else if(qType=='ml' || qType=='sl')
		{
			var insertedText = dwr.util.getValue("QS"+i+"opt");
			if((insertedText!=null && insertedText!="") || isRequired==0)
			{
				if(isRequired==1){
					isRequiredAns=1;
				}
			var schoolMaster="";	
				if($(".school"+i).length>0){
					schoolMaster=$(".school"+i).val();
				}
				
				arr.push({ 
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"jobOrder" : jobOrder,
					"schoolIdTemp" : schoolMaster
				});
			}else
			{
				if(isRequired==1){
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
					dSPQuestionsErrorCount=1;
				}
			}
		}else if(qType=='et'){
			var optId="";
			var errorFlag=1;
			if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
			{
				errorFlag=0;
				optId=$("input[name=QS"+i+"opt]:radio:checked").val();
			}else
			{
				if(isRequired==1){
					errorFlag=1;
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
					dSPQuestionsErrorCount=1;
				}
			}
			
			var insertedText = dwr.util.getValue("QS"+i+"optet");
			var isValidAnswer = dwr.util.getValue("QS"+optId+"validQuestion");
			if(isValidAnswer=="false" && insertedText.trim()==""){
				if(isRequired==1){
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
					dSPQuestionsErrorCount=1;
					errorFlag=1;
				}
			}
			if(errorFlag==0){
				if(isRequired==1){
					isRequiredAns=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"insertedText"    : insertedText,
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : isValidAnswer,
					"jobOrder" : jobOrder,
					"sliderScore" : scroreVal
				});
			}
		}else if(qType=='mlsel'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				if(multiSelectArray!=""  || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder,
						"sliderScore" : scroreVal
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}if(qType=='mloet'){
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("QS"+i+"optmloet"); 
				if(multiSelectArray!="" || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder,
						"sliderScore" : scroreVal
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}else if(qType=='rt'){
			var optId="";
			var score="";
			var rank="";
			var scoreRank=0;
			var opts = document.getElementsByName("optS");
			var scores = document.getElementsByName("scoreS");
			var ranks = document.getElementsByName("rankS");
			var o_ranks = document.getElementsByName("o_rankS");
			
			var tt=0;
			var uniqueflag=false;
			for(var i = 0; i < opts.length; i++) {
				optId += opts[i].value+"|";
				score += scores[i].value+"|";
				rank += ranks[i].value+"|";
				if(checkUniqueRankForPortfolio(ranks[i]))
				{
					uniqueflag=true;
					break;
				}

				if(ranks[i].value==o_ranks[i].value)
				{
					scoreRank+=parseInt(scores[i].value);
				}
				if(ranks[i].value=="")
				{
					tt++;
				}
			}
			if(uniqueflag)
				return;

			if(tt!=0)
				optId=""; 

			if(optId=="")
			{
				//totalSkippedQuestions++;
				//strike checking
			}
			var totalScore = scoreRank;
			if(isRequired==1){
				isRequiredAns=1;
			} 
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("QS"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOptS"+optId),
				"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
				"isValidAnswer" : isValidAnswer,
				"jobOrder" : jobOrder,
				"optionScore"      : score,
				"totalScore"       : totalScore,
				"insertedRanks"    : rank,
				"maxMarks" :o_maxMarks,
				"sliderScore" : scroreVal
			});
		}else if(qType=='OSONP'){
			//	alert("hello "+qType);
			try{
				 var multiSelectArray="";
				 var inputs = document.getElementsByClassName("OSONP"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'radio') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				var insertedText = dwr.util.getValue("QS"+i+"OSONP");
				
				if(multiSelectArray!="" || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}else if(qType=='DD'){
			try{
				
				 var multiSelectArray=$("#dropdown"+i+" :selected").val() ;				
			
				if(multiSelectArray!=""  || isRequired==0){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						//"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}
		if(qType=='sscb'){
			try{
				
				var insertedText = dwr.util.getValue("QS"+i+"multiselectText");
				 var multiSelectArray="";
				 var inputs = document.getElementsByName("multiSelect"+i); 
				 for (var j = 0; j < inputs.length; j++) {
				        if (inputs[j].type === 'checkbox') {
				        	if(inputs[j].checked){
				        		multiSelectArray+=	inputs[j].value+"|";
				            }
				        }
				} 
				 
				 if($(".school"+i).length>0){
						schoolMaster=$(".school"+i).val();
					}
				if(multiSelectArray!=""){
					if(isRequired==1){
						isRequiredAns=1;
					} 
					arr.push({ 
						"selectedOptions"  : multiSelectArray,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"schoolIdTemp" : schoolMaster,
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}else{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}catch(err){}
		}
		
		if(isRequiredAns==1){
			ansRequiredCount++;
		}
	}
	//alert('isRequiredCount:::'+isRequiredCount);
	//alert('ansRequiredCount:::'+ansRequiredCount);
	if(isRequiredCount==ansRequiredCount) 
	{
		CandidateGridSubAjax.setDistrictQPortfoliouestions(arr,dspqType,teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				arr =[];
				$("#saveDataLoading").modal("show");
			}
		}
		});	
	}else
	{
		$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
		dSPQuestionsErrorCount=1;
	}
}

/*====================================================================*/


function showReferencesForm()
{
	 $("#divElectronicReferences").show();
	 resetReferenceform();
	 document.getElementById("salutation").focus();
}
function resetReferenceform()
{
	    document.getElementById("rdcontacted1").checked = true
	 	document.getElementById("salutation").value=0;
		document.getElementById("firstName1").value='';
		document.getElementById("lastName1").value='';
		document.getElementById("designation").value='';
		document.getElementById("organization").value='';
		document.getElementById("email").value='';
		document.getElementById("contactnumber").value='';
		$('#referenceDetailstext').find(".jqte_editor").html("");
		document.getElementById("pathOfReferenceFile").value="";
		document.getElementById("pathOfReferenceFile").value=null;
}


//======================= Mukesh References ===============================
function insertOrUpdateElectronicReferencesM(sbtsource)
{
	 		var referenceDetails	= trim($('#referenceDetailstext').find(".jqte_editor").html());
			var teacherId		=   trim(document.getElementById("teacherId").value);
			var elerefAutoId	=	document.getElementById("elerefAutoId");
			var salutation		=	document.getElementById("salutation");
			
			var firstName		=	trim(document.getElementById("firstName1").value);
			var lastName		= 	trim(document.getElementById("lastName1").value);
			var designation		= 	trim(document.getElementById("designation").value);
			
			var organization	=	trim(document.getElementById("organization").value);
			var email			=	trim(document.getElementById("email").value);
			var longHaveYouKnow	=	trim(document.getElementById("longHaveYouKnow").value);
			var contactnumber	=	trim(document.getElementById("contactnumber").value);
			var rdcontacted0	=   document.getElementById("rdcontacted0");
			var rdcontacted1	=   document.getElementById("rdcontacted1");
			var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
			
			var cnt=0;
			var focs=0;	
			
			var fileName = pathOfReferencesFile.value;
			var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
			var fileSize=0;		
			if ($.browser.msie==true){	
			    fileSize = 0;	   
			}else{		
				if(pathOfReferencesFile.files[0]!=undefined)
				fileSize = pathOfReferencesFile.files[0].size;
			}
			
			$('#errordivElectronicReferences').empty();
			setDefColortoErrorMsgToElectronicReferences();
			
			if(firstName=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
				if(focs==0)
					$('#firstName1').focus();

				$('#firstName1').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(lastName=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
				if(focs==0)
					$('#lastName1').focus();

				$('#lastName1').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(organization=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
				if(focs==0)
					$('#organization').focus();
				
				$('#organization').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(email=="")
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			else if(!isEmailAddress(email))
			{		
				$('#errordivElectronicReferences').append("&#149;"+resourceJSON.PlzEtrVldEmail+"<br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}if(contactnumber=="")
			{
				$('#errordivElectronicReferences').append("&#149;"+resourceJSON.PlzEtrCtctNum+"<br>");
				if(focs==0)
					$('#contactnumber').focus();
				
				$('#contactnumber').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(longHaveYouKnow=="" && $("#districtId").val()==4218990)
			{
				$('#errordivElectronicReferences').append("&#149; "+resourceJSON.msgHowLongYouKnwPers+"<br>");
				if(focs==0)
					$('#longHaveYouKnow').focus();
				
				$('#longHaveYouKnow').css("background-color",txtBgColor);
				cnt++;focs++;
			}

			
			var rdcontacted_value;
			
			if (rdcontacted0.checked) {
				rdcontacted_value = false;
			}
			else if (rdcontacted1.checked) {
				rdcontacted_value = true;
			}
			
			if(ext!=""){
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errordivElectronicReferences').append("&#149; Acceptable file formats include PDF, MS-Word, GIF, PNG, and JPEG files<br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}	
				else if(fileSize>=10485760)
				{
					$('#errordivElectronicReferences').append("&#149; "+resourceJSON.FileSizeLessthan10Mb+"<br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}
			}
			

			if(cnt!=0)		
			{
				$('#errordivElectronicReferences').show();
				return false;
			}
			else
			{
				var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null};
				dwr.engine.beginBatch();
				
				dwr.util.getValues(teacherElectronicReferences);
				teacherElectronicReferences.rdcontacted=rdcontacted_value;
				teacherElectronicReferences.salutation=salutation.value;
				teacherElectronicReferences.firstName=firstName;
				teacherElectronicReferences.lastName=lastName;;
				teacherElectronicReferences.designation=designation;
				teacherElectronicReferences.organization=organization;
				teacherElectronicReferences.email=email;
				teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
				teacherElectronicReferences.referenceDetails=referenceDetails;
				
				var refFile = document.getElementById("pathOfReference").value;
				if(refFile!=""){
					teacherElectronicReferences.pathOfReference=refFile;
				}
				if(fileName!="")
				{	
					
					PFCertifications.findDuplicateReferences(teacherElectronicReferences,teacherId,{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
						if(data=="isDuplicate")
						{
							$('#errordivElectronicReferences').append("&#149; An Electronic Reference has already registered with this Email<br>");
							if(focs==0)
								$('#email').focus();
							$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
							$('#errordivElectronicReferences').show();
							return false;
						}else{
							document.getElementById("frmElectronicReferences").submit();
						}
						}
					});
					
				}else{
					PFCertifications.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
						async: true,
						errorHandler:handleError,
						callback: function(data)
						{
							if(data=="isDuplicate")
							{
								$('#errordivElectronicReferences').append("&#149; An Electronic Reference has already registered with the provided Email<br>");
								if(focs==0)
									$('#email').focus();
								$('#email').css("background-color",txtBgColor);
									cnt++;focs++;
								$('#errordivElectronicReferences').show();
								return false;
							}
							hideElectronicReferencesForm();
							getElectronicReferencesGrid_DivProfile(teacherId);
						}
					});
				}
				
				dwr.engine.endBatch();
				return true;
			}
			
}

function setDefColortoErrorMsgToElectronicReferences()
{
	$('#salutation').css("background-color","");
	$('#firstName1').css("background-color","");
	$('#lastName1').css("background-color","");
	$('#designation').css("background-color","");
	$('#organization').css("background-color","");
	$('#email').css("background-color","");
	$('#contactnumber').css("background-color","");
}

function hideElectronicReferencesForm()
{
	resetReferenceform();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="none";
	//resetReferenceform();
	return false;
}

function saveNobleReference(fileName)
{
   var referenceDetailstext	= trim($('#referenceDetailstext').find(".jqte_editor").text());
   
		var teacherId		=   trim(document.getElementById("teacherId").value);	
	    var elerefAutoId	=	document.getElementById("elerefAutoId");
		var salutation		=	document.getElementById("salutation");
		
		var firstName		=	trim(document.getElementById("firstName1").value);
		var lastName		= 	trim(document.getElementById("lastName1").value);;
		var designation		= 	trim(document.getElementById("designation").value);
		
		var organization	=	trim(document.getElementById("organization").value);
		var longHaveYouKnow	=	trim(document.getElementById("longHaveYouKnow").value);
		var email			=	trim(document.getElementById("email").value);
		
		var contactnumber	=	trim(document.getElementById("contactnumber").value);
		var rdcontacted0	=   document.getElementById("rdcontacted0");
		var rdcontacted1	=   document.getElementById("rdcontacted1");
		
		var cnt=0;
		var focs=0;	
		
		var rdcontacted_value;
		if (rdcontacted0.checked) {
			rdcontacted_value = false;
		}
		else if (rdcontacted1.checked) {
			rdcontacted_value = true;
		}
		
		var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,pathOfReference:null,longHaveYouKnow:null};
		dwr.engine.beginBatch();
	
		dwr.util.getValues(teacherElectronicReferences);
		teacherElectronicReferences.rdcontacted=rdcontacted_value;
		teacherElectronicReferences.salutation=salutation.value;
		teacherElectronicReferences.firstName=firstName;
		teacherElectronicReferences.lastName=lastName;;
		teacherElectronicReferences.designation=designation;
		teacherElectronicReferences.organization=organization;
		teacherElectronicReferences.email=email;
		teacherElectronicReferences.pathOfReference=fileName;
		teacherElectronicReferences.referenceDetails=referenceDetailstext;
		teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;

		PFCertifications.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data=="isDuplicate")
				{
					///updateReturnThreadCount("ref");
					$('#errordivElectronicReferences').append("&#149; An Electronic Reference has already registered with the provided Email<br>");
					if(focs==0)
						$('#email').focus();
					$('#email').css("background-color",txtBgColor);
						cnt++;focs++;
					$('#errordivElectronicReferences').show();
					return false;
				}
				hideElectronicReferencesForm();
				getElectronicReferencesGrid_DivProfile(teacherId);
				
			}
		});
	
		dwr.engine.endBatch();
		return true;
	}
function editFormElectronicReferences(id)
{
	$('#errordivElectronicReferences').empty();
	setDefColortoErrorMsgToElectronicReferences();
	PFCertifications.editElectronicReferences(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
		       showReferencesForm();
				
	        dwr.util.setValues(data);
	       // document.getElementById("firstName").value	 = "";
			//document.getElementById("lastName").value   = "";
		    document.getElementById("salutation").value	   =	data.salutation;
		    document.getElementById("firstName1").value	   =	data.firstName;
			document.getElementById("lastName1").value	   =	data.lastName;
			
				$('#referenceDetailstext').find(".jqte_editor").html(data.referenceDetails);
				return false;
			}
		});
	return false;
}

function removeReferences()
{
	var teacherId =  document.getElementById("teacherId").value
	var referenceId = document.getElementById("elerefAutoId").value;
	if(window.confirm("Are you sure, you would like to delete the Recommendation Letter."))
	{
		PFCertifications.removeReferencesForNoble(referenceId,teacherId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				getElectronicReferencesGrid_DivProfile(teacherId);
				hideElectronicReferencesForm();
			}
		});
	}
}
var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	if(status==1)
	$('#chgstatusRef1').modal('show');
	else if(status==0)
	$('#chgstatusRef2').modal('show');	
}
function changeStatusElectronicReferencesConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');
	
		PFCertifications.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			    getElectronicReferencesGrid_DivProfile(teacherId);
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}


function setDefColortoErrorMsgToVideoLinksForm()
{
	$('#videourl').css("background-color","");
}
function showVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="block";
	document.getElementById("videourl").focus();
	setDefColortoErrorMsgToVideoLinksForm();
	$('#errordivvideoLinks').empty();
	return false;
}

function hideVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="none";
	return false;
}

function insertOrUpdatevideoLinks()
{
	 
	 var teacherId =  document.getElementById("teacherId").value
	var elerefAutoId = document.getElementById("videolinkAutoId");
	var videourl = trim(document.getElementById("videourl").value);
	var cnt=0;
	var focs=0;	
	
	$('#errordivvideoLinks').empty();
	setDefColortoErrorMsgToVideoLinksForm();
	
	if(videourl=="")
	{
		$('#errordivvideoLinks').append("&#149; "+resourceJSON.PlzEtrVideoLink+"<br>");
		if(videourl==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt!=0 )		
	{
		$('#errordivvideoLinks').show();
		return false;
	}
	else
	{	
		var teacherVideoLink = {videolinkAutoId:null,videourl:null, createdDate:null};
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherVideoLink);
		teacherVideoLink.videourl=videourl;
		PFCertifications.saveOrUpdateVideoLink(teacherVideoLink,teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				hideVideoLinksForm();
				getVideoLinksGrid_DivProfile(teacherId);
			}
		});
		dwr.engine.endBatch();
		return true;
	}
}


function editFormVideoLink(id)
{
	PFCertifications.editVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

function deleteFormVideoLink(id)
{
	PFCertifications.deleteVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}
function delVideoLnkConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value	
	 $('#delVideoLnk').modal('hide');
		PFCertifications.deleteVIdeoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getVideoLinksGrid_DivProfile(teacherId);	
			//getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;

}
//------------------Rahul:GetDocument---18/11/2014---------------------

function getDocuments()
{
	CandidateGridAjax.getDocumentsByDistrictOrSchoolId(
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
				
					if(data!="" || data.length>0)
					{
						//$("#templateDiv").fadeIn();
						//$("#btnChangetemplatelink").hide();
						$("#districtDocument").html(data);
					}else
					{
						$("#documentdiv").hide();
					}
				}
			});	

}

function showDocumentLink()
{
var docname=document.getElementById("districtDocument").value;	

	if(docname.length>1)
	{
		$('#viewDoc').show();
		
	}
	else
	{
		$('#viewDoc').hide();	
	}

}

function vieDistrictFile(ids) {

	document.getElementById(ids).src = "";
	var districtattachment=document.getElementById("districtDocument").value;
		
	CandidateGridAjax.vieDistrictFile(districtattachment,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
		
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				// $('#distAttachmentDiv').modal('hide');
				
				 document.getElementById(ids).src = ""+data+"";
			
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#distAttachmentShowDiv').modal('hide');
			 document.getElementById(ids).src = ""+data+"";
		} else {
			//alert(data)
			$('#myModalMessage').modal('hide');
			document.getElementById(ids).src = ""+data+"";
		
			$('.distAttachmentShowDiv').modal('show');
				
				return false;
		}
	}});

 }
//------------------Rahul:GetDocument---14/11/2014---------------------

function getDocuments()
{
	CandidateGridAjax.getDocumentsByDistrictOrSchoolId(
			{
				async: true,
				errorHandler:handleError,
				callback:function(data)
				{	
				
					if(data!="" || data.length>0)
					{
						//$("#templateDiv").fadeIn();
						//$("#btnChangetemplatelink").hide();
						$("#districtDocument").html(data);
					}else
					{
						$("#documentdiv").hide();
					}
				}
			});	
}

function showDocumentLink()
{
var docname=document.getElementById("districtDocument").value;	

	if(docname.length>1)
	{
		$('#viewDoc').show();
		
	}
	else
	{
		$('#viewDoc').hide();	
	}
}

function vieDistrictFile(ids) {

	document.getElementById(ids).src = "";
	var districtattachment=document.getElementById("districtDocument").value;
		
	CandidateGridAjax.vieDistrictFile(districtattachment,{
	async: true,
	errorHandler:handleError,
	callback:function(data)
	{
		if(deviceType)
		{
			
			if (data.indexOf(".doc")!=-1)
			{
				$("#docfileNotOpen").css({"z-index":"3000"});
		    	$('#docfileNotOpen').show();
			}
			else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				$("#exelfileNotOpen").css({"z-index":"3000"});
		    	$('#exelfileNotOpen').show();
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if(deviceTypeAndroid)
		{
		
			if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
			{
				// $('#distAttachmentDiv').modal('hide');
				
				 document.getElementById(ids).src = ""+data+"";
			
			}
			else
			{
				document.getElementById(linkid).href = data;
			}
		}
		else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
		{
			 $('#distAttachmentShowDiv').modal('hide');
			 document.getElementById(ids).src = ""+data+"";
		} else {
			//alert(data)
			$('#myModalMessage').modal('hide');
			document.getElementById(ids).src = ""+data+"";
		
			$('.distAttachmentShowDiv').modal('show');
				
				return false;
		}
	}});
 }


function showMessageDiv1()
{
	$('#myModalMessage').modal('show');
	$('.distAttachmentShowDiv').modal('hide');
}

function pnqSave(teacherId){
	var pnqOption = $("#pnqList option:selected").val();
	TeacherProfileViewInDivAjax.updatePNQByUser(teacherId,pnqOption,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			
		}
	});
}

function certTextDiv(id){
	
	$("#draggableDivMaster").modal('hide');
	
	$(".certTextContent").html($("#cerTTextContent"+id).val());
	$("#certTextDivDSPQ").modal("show");
}
function certTextDivClose(){
	 
	 $("#draggableDivMaster").modal('show');
	 ("#certTextDivDSPQ").modal("show");
	 $(".certTextContent").html("");
} 

//---------------school search----------------
function getSchoolMasterAutoCompQuestion(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArrayQuestion(txtSearch.value);
		fatchData2Question(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArrayQuestion(schoolName){
	var searchArray = new Array();
	var districtId	= $("#questionDistrictId").val();
	BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2Question= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDivQuestion(dis,hiddenId,divId)
{
	document.getElementById(hiddenId).value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}
//For language profiency
function getlanguage_DivProfile(teacherId)
{
	//alert(noOfRowsaddDoc+":::::::::::::"+pageaddDoc+"::"+sortOrderStraddDoc+"::"+sortOrderTypeaddDoc+"::"+teacherId);
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.displayTeacherLanguageByTeacherId(noOfRowsLang,pageLang,sortOrderStrLang,sortOrderTypeLang,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridlanguageProfiency').html(data);
			applyScrollOnTblLanguage();

		}});
}
//For Additional Document
function getdistrictAssessmetGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	TeacherProfileViewInDivAjax.getdistrictAssessmetGrid(noOfRowsASMT,pageASMT,sortOrderStrASMT,sortOrderTypeASMT,teacherId,visitLocation, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divGridAssessmentDetails').html(data);
//			applyScrollOnTbl_AssessmentDetails();
		}});
}
function downloadOctUpload(octId)
{
		//alert("downloadAnswer0");
	TeacherProfileViewInDivAjax.downloadOctUpload(octId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
			if(data!="")
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameReferencesID').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefOctUpload").href = data; 
					return false;
				}
			}
		});
	}

function getEmployeeNumberInfo(flag,table){
	var height="height:500px;";
	if(flag)
	height="height:auto;";
	var div="<div class='modal hide in' id='employeeInfo' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'><div class='modal-dialog' style='width:850px;'><div class='modal-content'><div class='modal-header'><!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span class='confirmFalse' aria-hidden='true'>&times;</span></button>--><h4 class='modal-title' id='myModalLabel' style='color:white;'>Employee Information</h4></div><div class='modal-body' style='"+height+"overflow:auto;' id='tableData'></div><div class='modal-footer'><button type='button' class='btn btn-default confirmFalse' >Ok</button></div></div></div></div>";
		try{
			$('#employeeInfo').modal('hide'); 
		}catch(e){}
		try{
			$('#employeeInfo').remove(); 
		}catch(e){}
		$('#draggableDivMaster').after(div);
		$('#draggableDivMaster').modal('hide');
		$('#employeeInfo').modal({show:true,backdrop: 'static',keyboard: false,});
		$('#employeeInfo #tableData').html(table); 
		$('.confirmFalse').click(function(){  	
			$('#employeeInfo').modal('hide');   
			$('#employeeInfo').remove(); 
		 	$('#draggableDivMaster').modal('show');  
		});
	$('#loadingDiv').hide();
 }
function openEmployeeNumberInfo(empId){
$('#loadingDiv').show();
  try{
	  TeacherProfileViewInDivAjax.openEmployeeNumberInfo(empId,{
		  async: false,
		  cache: false,
		  errorHandler:handleError,
		  callback:function(data){
		  $('#draggableDivMaster').modal('hide');
		  $('#draggableDivMaster').hide();
		  //alert(data);
		  var divData=data.split('####');
		  if(divData[0].trim()=='1')
			  	getEmployeeNumberInfo(true,divData[1]);
		  		else
		  		getEmployeeNumberInfo(false,data);
		  }
	   });
  }catch(e){alert(e)}
  }
function getTeacherAssessmentGrid_DivProfile(teacherId)
{
	var visitLocation=document.getElementById("teacherIdForprofileGridVisitLocation").value;
	
	TeacherProfileViewInDivAjax.getTeacherAssessmentGrid(teacherId,noOfRowsEJ,pageEJ,sortOrderStrEJ,sortOrderTypeEJ, { 
  async: true,
  errorHandler:handleError,
  callback:function(data)
  {
   $('#gridTeacherAssessment').html(data);  
	   applyScrollOnAssessments();
  }});
}

function getTeacherLicenceGrid_DivProfile(teacherId){
	
	TeacherProfileViewInDivAjax.displayRecordsBySSNForDate(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		//alert(data);
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLicneseGridCertificationsGridDate').html(data);			
			//    $('#loadingDiv').hide();				
			   applyScrollOnTblLicenseDate();
		}
		
		},
	errorHandler:handleError
	});
}

function getLEACandidatePortfolio_DivProfile(teacherId){
	TeacherProfileViewInDivAjax.getLEACandidatePortfolio(teacherId,{ 
		async: true,
		callback: function(data)
		{	
		if(data==null||data=="")
		{				
		ssnNotFound("No License data found for your Social Security");
		return false;
		}
		else
		{
			//	$('#licenseDivDate').show();
				$('#divLEACandidatePotfolioDiv').html(data);			
				//    $('#loadingDiv').hide();				
				applyScrollOnLEACandidatePorfolio();
		}
		
		},
	errorHandler:handleError
	});
}
function getBackgroundCheckGrid_DivProfile(teacherId)
{
	TeacherProfileViewInDivAjax.getBackgroundCheckGrid_DivProfile(teacherId, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#gridDataBackgroundCheck').html(data);			
			applyScrollOnTblBackgroundCheck_profile();
		}});
}
function downloadBackgroundCheck(teacherId,linkId)
{	
	$('#loadingDiv').show();
	CandidateGridAjaxNew.downloadBackgroundCheck(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{			
			$('#loadingDiv').hide();
			if(data=="")	
			{
				alert("Background Check is not uploaded by the candidate.")
			}
			else
			{
				if(deviceType)
				{
					if (data.indexOf(".doc")!=-1)
					{
						$("#docfileNotOpen").css({"z-index":"3000"});
				    	$('#docfileNotOpen').show();
					}
					else if(data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						$("#exelfileNotOpen").css({"z-index":"3000"});
				    	$('#exelfileNotOpen').show();
					}
					else
					{
						document.getElementById(linkId).href = data; 	
					}
				}
				else if(deviceTypeAndroid)
				{
					if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
					{
						document.getElementById('ifrmTrans').src = ""+data+"";
					}
					else
					{
						document.getElementById(linkId).href = data; 		
					}
				}
				else if((data.indexOf(".doc")!=-1) || data.indexOf(".xls")!=-1 || data.indexOf(".xlsx")!=-1)
				{
					document.getElementById('ifrmTrans').src = ""+data+"";
				}
				else
				{
					document.getElementById(linkId).href = data; 		
				}			
			}	
			return false;			
							
		}
	});
}
