var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
		noOfRows = document.getElementById("pageSize").value;
		displayQues();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayQues();
}

function searchi4Ques()
{
	page = 1;
	displayQues();
}


function displayQues()
{
	var txtQuestion		=	document.getElementById("quesSearchText").value;
	var quesSetId		=	document.getElementById("quesSetId").value;
	var districtId= "";
	var headQuarterId = "";
	try{
		districtId		=	document.getElementById("districtId").value;
	}catch(e){}
	try{
		headQuarterId		=	document.getElementById("headQuarterId").value;
	}catch(e){}
	DistrictSpecificQuestionsSetQuesAjax.displayQuestions(noOfRows,page,sortOrderStr,sortOrderType,txtQuestion,quesSetId,districtId,headQuarterId,{ 
		async: true,
		callback: function(data){
		document.getElementById("refChkQuesGrid").innerHTML=data;
		applyScrollOnTbl();
		document.getElementById("addQuesDiv").style.display="none";
	},
	});
}

function displayQuesSetQues()
{
	var quesSetId		=	document.getElementById("quesSetId").value;
	var districtId= "";
	var headQuarterId = "";
	try{
		districtId		=	document.getElementById("districtId").value;
	}catch(e){}
	try{
		headQuarterId		=	document.getElementById("headQuarterId").value;
	}catch(e){}
	DistrictSpecificQuestionsSetQuesAjax.displayQuesSetQues(quesSetId,districtId,headQuarterId,{ 
		async: true,
		callback: function(data){
		document.getElementById("i4QuesSetQuesGrid").innerHTML=data;
		applyScrollOnTbl_1();
		document.getElementById("addQuesDiv").style.display="none";
	},
	});
}

/* Add New Question to Question Set*/
function saveQuestion123()
{
	var quesSetId		=	trim(document.getElementById("quesSetId").value);
	var txtQuestion		=	trim(document.getElementById("txtQuestion").value);
	var districtId		=	trim(document.getElementById("districtId").value);
	
	if (txtQuestion=="")
	{
		document.getElementById('errQuesdiv').innerHTML		=	"&#149; "+resourceJSON.PlzEtrQuest;
		$('#errQuesdiv').show();
		$('#txtQuestion').css("background-color", "#F5E7E1");
		document.getElementById("txtQuestion").focus();
		return false;
	}else
	{
		if(txtQuestion.length>256)
		{
			document.getElementById('errQuesdiv').innerHTML		=	"&#149; "+resourceJSON.PlzEtrQuestLength;
			$('#errQuesdiv').show();
			$('#txtQuestion').css("background-color", "#F5E7E1");
			document.getElementById("txtQuestion").focus();
			return false;
		}
	}
	
	I4QuestionsSetQuesAjax.saveI4QuesInQuesSet(quesSetId,txtQuestion,districtId,{ 
		async: true,
		callback: function(data)
		{
			displayQuesSetQues();
			displayQues()
			clearQues();
		},
	});
}

function clearQues()
{
	try{
		$('#assQuestion').find(".jqte_editor").html("");
		$('#assInstruction').find(".jqte_editor").html("");
		document.getElementById("addQuesDiv").style.display="none";
		//document.getElementById("quesId").value="";
		document.getElementById("questionMaxScore").value="";
		document.getElementById("questionTypeMaster").checked=false;
	}catch(e){}
}

/* Display Question Grid */
function addNewQues()
{
	document.getElementById("addQuesDiv").style.display="block";
	$("#errQuesdiv").empty();
	$( "span#i4QuesTxt" ).show();
}

//save Question from Question Pool to Question Set
function addQuesFromQPtoQS(quesID)
{
	var quesSetId			=	trim(document.getElementById("quesSetId").value);
	var districtId= "";
	var headQuarterId = "";
	try{
		districtId		=	document.getElementById("districtId").value;
	}catch(e){}
	try{
		headQuarterId		=	document.getElementById("headQuarterId").value;
	}catch(e){}
	 
	DistrictSpecificQuestionsSetQuesAjax.addQuesFromQPtoQS(quesID,quesSetId,districtId,headQuarterId,{ 
			async: false,
			callback:function(data)
			{
		 		if(data==1)
		 		{
		 			displayQuesSetQues();
					displayQues()
		 		}
			}
		});
}

//Delete Question from Question Set And display in Question Pool
function deleteQuesFromQuesSet(quesSetQuesID)
{
	DistrictSpecificQuestionsSetQuesAjax.deleteQuesFromQuesSet(quesSetQuesID,{ 
		async: false,
		callback:function(data)
		{
			if(data==1)
	 		{
	 			displayQuesSetQues();
				displayQues()
	 		}
		}
	});
}

// Move Up Question in Question set
function moveUpQues(quesSetQuesID)
{
	var districtId= "";
	var headQuarterId = "";
	try{
		districtId = $("#districtId").val();
	}catch(e){}
	try{
		headQuarterId		=	document.getElementById("headQuarterId").value;
	}catch(e){}	

	DistrictSpecificQuestionsSetQuesAjax.moveUpQues(quesSetQuesID,districtId,headQuarterId,{ 
		async: false,
		callback:function(data)
		{
			if(data==1)
	 		{
	 			displayQuesSetQues();
			}
		}
	});
}

//Move Down Question in Question set
function moveDownQues(quesSetQuesID)
{
	var districtId= "";
	var headQuarterId = "";
	try{
		districtId = $("#districtId").val();
	}catch(e){}
	try{
		headQuarterId		=	document.getElementById("headQuarterId").value;
	}catch(e){}	

	DistrictSpecificQuestionsSetQuesAjax.moveDownQues(quesSetQuesID,districtId,headQuarterId,{ 
		async: false,
		callback:function(data)
		{
			if(data==1)
	 		{
	 			displayQuesSetQues();
			}
		}
	});
}

function validateDistrictQuestionsFromQuestionSet() {
	$('#errordiv').empty();
	$('#errordiv1').empty();
	$('#assQuestion').find(".jqte_editor").css("background-color", "");
	$('#assInstruction').find(".jqte_editor").css("background-color", "");
	$('#assExplanation').find(".jqte_editor").css("background-color", "");
	$('#questionTypeMaster').css("background-color", "");
	$('#opt1').css("background-color", "");
	//var questionMaxScore = document.getElementById("questionMaxScore").value;
	var cnt=0;
	var focs=0;

	if($('#assQuestion').find(".jqte_editor").text().trim()==""){

		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrQuest+"<br>");
		if(focs==0)
			$('#assQuestion').find(".jqte_editor").focus();
		$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	} else {
		var charCount=trim($('#assQuestion').find(".jqte_editor").text());
		var count = charCount.length;
		if(count>5000) {
			$('#errordiv').append("&#149; "+resourceJSON.MsgQuestionLength+"<br>");
			if(focs==0)
				$('#assQuestion').find(".jqte_editor").focus();
			$('#assQuestion').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	
	if (trim(document.getElementById("questionTypeMaster").value)==0){

		$('#errordiv').append("&#149; "+resourceJSON.PlzSelectQuestionType+"<br>");
		if(focs==0)
			$('#questionTypeMaster').focus();
		$('#questionTypeMaster').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	var qType=findSelected(dwr.util.getValue("questionTypeMaster"));
	var c=0;
	var validCnt=0;
	
	if(qType == "et"){
		if($('#assExplanation').find(".jqte_editor").text().trim()==""){

			$('#errordiv').append("&#149; "+resourceJSON.PlzEtrQuestionExplanation+"<br>");
			if(focs==0)
				$('#assExplanation').find(".jqte_editor").focus();
			$('#assExplanation').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		} else {
			var charCount=trim($('#assExplanation').find(".jqte_editor").text());
			var count = charCount.length;
			if(count>5000) {
				$('#errordiv').append("&#149; "+resourceJSON.MsgQuestionExplanationLength+"<br>");
				if(focs==0)
					$('#assExplanation').find(".jqte_editor").focus();
				$('#assExplanation').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		if(validateExplanationRequired()==false)
		{
			cnt++;
			$('#errordiv').append("&#149; "+resourceJSON.msgSelectExplanationRequired+"<br>");
		}
	}
/* END Section */
	
	if(!(qType=='ml') && !(qType=='SLD') && !(qType=='sl'))
		if(document.getElementById("opt1")) {
			for(i=1;i<=10;i++) {
				if(trim(document.getElementById("opt"+i).value)=="")
					c++;
				if($('#valid'+i).prop('checked') && trim(document.getElementById("opt"+i).value)!="")
				validCnt++;
			}
			if(c==9 || c==10) {
				$('#errordiv').append("&#149; "+resourceJSON.PlzEtrAtLeastTwoOptions+"<br>");
				if(focs==0)	{
					if(c==9)
						$('#opt1').focus();
					else
						$('#opt2').focus();
				}

				$('#opt1').css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}

	
	var arr =[];
	var charCount=trim($('#assInstruction').find(".jqte_editor").text());
	var count = charCount.length;
	if(count>2500) {
		$('#errordiv').append("&#149; "+resourceJSON.MsgInstructionsCannotExceed+"<br>");
		if(focs==0)
			$('#assInstruction').find(".jqte_editor").focus();
		$('#assInstruction').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}

	if(cnt==0)
		return true;
	else {
		$('#errordiv').show();
		return false;
	}

}

function saveQuestionFromQuestionSet(status) {
	if(!validateDistrictQuestionsFromQuestionSet())
		return;
	
	var quesSetId		=	trim(document.getElementById("quesSetId").value);
	var questionType={questionTypeId:dwr.util.getValue("questionTypeMaster")};
	var qType=findSelected(questionType.questionTypeId);
	var districtSpecificQuestions = {questionId:dwr.util.getValue("questionId"),question:null,questionExplanation:null,questionTypeMaster:questionType,questionInstructions:null};
	var districtMaster= "";
	var HeadQuarterMaster = "";
	try{
		districtMaster = {districtId:dwr.util.getValue("districtId")};
	}catch(e){}
	try{
		HeadQuarterMaster = {headQuarterId:dwr.util.getValue("headQuarterId")};
	}catch(e){}	
	
	var questionoptions={};
	var arr =[];
	
	if(qType=='tf') {
		
		for(i=1;i<=2;i++) {
			
			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='et'){
		for(i=1;i<=2;i++)
		{
			if($('#opt'+i).val().trim()!="")
			{
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked'),
					"requiredExplanation": $('#explanationRequired'+i).prop('checked')
				});
			}
		}
	}
	else if(qType=='slsel') {

		for(i=1;i<=10;i++) {
			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	}  else if(qType=='mlsel') {

		for(i=1;i<=6;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='mloet') {

		for(i=1;i<=6;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='sloet') {
		for(i=1;i<=6;i++) {

			if($('#opt'+i).val().trim()!="") {
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	}
	dwr.util.getValues(districtSpecificQuestions);
	dwr.engine.beginBatch();
	// ReferenceChkQuestionsAjax.saveDistrictQuestionFromQueationSet(districtSpecificQuestions,districtMaster,arr,quesSetId,{
	DistrictQuestionsAjax.saveDistrictQuestionFromQueationSet(districtSpecificQuestions,HeadQuarterMaster,districtMaster,arr,quesSetId,{
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			var redirectURL = "";
			if(status=="save"){
				displayQuesSetQues();
				displayQues();
				clearQues();
			} else {
				if(HeadQuarterMaster!=null && HeadQuarterMaster !="")
					redirectURL = "districtspecificquestions.do?headQuarterId="+HeadQuarterMaster.headQuarterId;
				else
					redirectURL = "districtspecificquestions.do?districtId="+districtMaster.districtId;
				displayQuesSetQues();
				displayQues()
				clearQues();
			}
			
			//redirectTo(redirectURL);
		}
	});

	dwr.engine.endBatch();
}


function saveQuestion(status)
{
	if(!validateDistrictQuestions())
		return;

	var questionType={questionTypeId:dwr.util.getValue("questionTypeMaster")};
	var qType=findSelected(questionType.questionTypeId);
	var districtSpecificQuestions = {questionId:dwr.util.getValue("questionId"),question:null,questionExplanation:null,questionTypeMaster:questionType,questionInstructions:null};
	var districtMaster = {districtId:dwr.util.getValue("districtId")};
	var questionoptions={};
	var arr =[];
	if(qType=='tf' || qType=='et')
	{
		for(i=1;i<=2;i++)
		{
			if($('#opt'+i).val().trim()!="")
			{
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});
			}
		}
	} else if(qType=='slsel') {
		for(i=1;i<=6;i++)
		{
			if($('#opt'+i).val().trim()!="")
			{ 
				arr.push({ 
					"optionId" : $('#hid'+i).val(),
					"questionOption"  : $('#opt'+i).val(),
					"validOption"  : $('#valid'+i).prop('checked')
				});

			}
		}

	}
	dwr.util.getValues(districtSpecificQuestions);
	dwr.engine.beginBatch();
	DistrictQuestionsAjax.saveDistrictQuestion(districtSpecificQuestions,districtMaster,arr,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			var redirectURL = "";
			if(status=="save")
				redirectURL = "districtquestions.do?districtId="+districtMaster.districtId;
			else
				redirectURL = "districtspecificquestions.do?districtId="+districtMaster.districtId;
			
			redirectTo(redirectURL);
		}
	});

	dwr.engine.endBatch();
}


function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function validateExplanationRequired()
{	var check=false;
	var count = 0;
	for(i=1;i<=2;i++)
	{
		if($('#explanationRequired'+i).prop('checked') && trim(document.getElementById("opt"+i).value)!="")
			count++;
	}
	if(count>0) check=true;
		return check;	
}