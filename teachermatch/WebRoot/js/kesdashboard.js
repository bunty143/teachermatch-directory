function top3jobcategory(mydata){

	$(function () {
	    $('#container3').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: 0,
	            plotShadow: false
	        },
	        title: {
	            text: '',
	            align: 'center',
	            verticalAlign: 'middle',
	            y: 0
	        }, 
	            colors: ['#b7dd70', '#85aff0', '#e26a68', '#24CBE5', '#64E572', '#FF9655', '#FFF263','#6AF9C4']
	            ,
	        
	        exporting: {
	         enabled: false
	},
	        plotOptions: {
	            pie: {
	                dataLabels: {
	                    enabled: true,
	                    formatter: function() {
							var s = this.point.name.split(' '); 
							var editData='';
								if(s.length>0){   var ii=1;
									s.forEach(function(entry)
									{  if(ii%3==0)
										editData+=entry+'<br/>';
									else editData+=entry+" ";
									ii=ii+1;})
		                        return editData;
								}
								else{
									return "";
								}
		                    },
	                    distance: 0,
	                    style: {
	                        fontWeight: 'bold',
	                        color: 'black',
	                        textShadow: '0px 1px 1px grey'
	                    }
	                },
	                startAngle: 90,
	                endAngle: 450,
	                center: ['50%', '53%']
	            }
	        },
	        series: [{
	            type: 'pie',
	            name: '',
	            innerSize: '50%',
	            data: mydata
	        }]
	    });
	});

	}












	function hires(totalhire){

	$(function () {
	    $('#container1').highcharts({
	        chart: {
	            plotBackgroundColor: null,
	            plotBorderWidth: 0,
	            plotShadow: false
	        },
	        title: {
	            text: ' <b style="font-size:2em;">'+totalhire+'</b><br>Hires',
	            align: 'center', 
	            verticalAlign: 'middle',
	            y: 0
	        },
	        tooltip: {
	            enabled: false
	        },
	        
	        exporting: {
	         enabled: false
	},
	        plotOptions: {
	            pie: {
	                dataLabels: {
	                    enabled: false,
	                    distance:10,
	                    style: {
	                        fontWeight: 'normal',
	                        color: 'black' ,
	                        fontSize:'12px'
	                    }
	                },
	                startAngle:90,
	                endAngle:  450,
	                center: ['50%', '50%']
	            }
	        },
	        series: [{
	            type: 'pie',
	            name: '',
	            innerSize: '65%',
	            data: [["Hires",1000]]
	        }]
	    });
	});

	}

	
	
	

	// anurag start
	 function reportByTime(resultType){  
	 $('.mybtn').css({backgroundColor: '#fff'});
	 
	  if(resultType=='week'){
	 		$('#weekly').css({backgroundColor: '#9BBB59'});
	 		$( "#resulyType" ).text("Week");
				kesdashboardResultjs('week');
				
	 		}
	 		
	 		
	 if(resultType=='month'){
			 $('#monthly').css({backgroundColor: '#9BBB59'});    
			 $( "#resulyType" ).text("Month");
			    kesdashboardResultjs('month');
	  
	 }
	}
	 
	 
	 
	function top3jobcatnew(top1Name,top2Name,top3Name,top1val,top2Val,top3Val){
		
		var name1=top1Name;
		var name2=top2Name;
		var name3=top3Name;
		if(top1val==0) top1Name= '';
	     else top1Name= top1Name.substring(0,15)+'.... ('+top1val+')';   
	    
	     if(top2Val==0)  top2Name= '';	
	     else
	    	 top2Name= top2Name.substring(0,15)+'.... ('+top2Val+')';		
	     
	     if(top3Val==0) top3Name = '';  
	     else
	    	 top3Name = top3Name.substring(0,15)+'.... ('+top3Val+')';  
		
		var chart = new CanvasJS.Chart("container3",
			    {
 
				  interactivityEnabled: true,
				  animationEnabled: true,
				  theme: "theme2",
			      legend:{
			        enable:true,
				    verticalAlign: "bottom",
			        horizontalAlign: "top"
			      },
			      data: [
			      {
			       toolTipContent: "{legendText} - ({y})", 
			       startAngle: 30,
			       indexLabelFontSize: 11,
			       indexLabelFontFamily:  'Century Gothic',
			       indexLabelFontColor: "black",
			       indexLabelLineColor: "#99BCE2",
			       indexLabelPlacement: "outside",
			       type: "doughnut",
			       showInLegend: false,
			       dataPoints: [
			       {  y: top1val,   indexLabel:  top1Name  , legendText:name1},
			       {  y: top2Val,   indexLabel: top2Name  , legendText:name2},
			       {  y: top3Val,   indexLabel: top3Name  , legendText:name3} 
			        
			       ]
			     }
			     ]
			   });

			    chart.render();
			   
			  }
		
		
	
	function top3jobcatnewForEmpty(){
		
		var chart = new CanvasJS.Chart("container3",
			    {
 
				  interactivityEnabled: false,
				  animationEnabled: true,
				  theme: "theme2",
			      legend:{
			        enable:false,
				    verticalAlign: "bottom",
			        horizontalAlign: "top"
			      },
			      data: [
			      {
			       
			       startAngle: 30,
			       indexLabelFontSize: 11,
			       indexLabelFontFamily:  'Century Gothic',
			       indexLabelFontColor: "black",
			       indexLabelLineColor: "#99BCE2",
			       indexLabelPlacement: "outside",
			       type: "doughnut",
			       showInLegend: false,
			       dataPoints: [
			      {  y: 1,  indexLabel: ''  } 
			      
			        
			       ]
			     }
			     ]
			   });

			    chart.render();
			   
			  }
	 







function kesdashboardResultjs(resultType)
{  

	
	var activeJobs , activeBranches, totalApplicants,totalhires;
	var top1Name,top2Name,top3Name , top1val, top2Val , top3Val;
	var branchId = 0;
	if($("#branchSearchId").val()!=null && $("#branchSearchId").val()!="")
		branchId = $("#branchSearchId").val();
//	 alert(result);
	 try{
	HeadQuarterAjax.kesdashboardResult(resultType, 1 ,branchId,{ 
		async: false,
		callback: function(data)
		
			{
		     activeJobs   =   data['activeJobs'];
		     activeBranches = data['activeBranches'];
		     totalApplicants =  data['totalApplicants'];
		     totalhires= data['hires'];
		    // alert((data['top1CatName']).substring(0,15));
		     top1val=parseInt(data['top1CatVal']);
		     top2Val= parseInt(data['top2CatVal']);
		     top3Val = parseInt(data['top3CatVal']);
		      
		     top1Name= data['top1CatName'];   
		     
		     top2Name= data['top2CatName'];		
		     
		     top3Name = data['top3CatName'];  	
		     
		     
		     $( "#activeJobs" ).text(activeJobs);
			 $( "#activeBranches" ).text(activeBranches);
			 $( "#totalApplicants" ).text(totalApplicants);
			 
			 
			 
			 //graph for hires
//			 Middle School Teaching - TM Default=1, Elementary Teaching - TM Default=4, Test job Cat with Dist=2
			 
			  hires(totalhires);
//			  var mydata=  [['Middle School Teaching - TM Default',1],['Elementary Teaching - TM Default',4],['Test job Cat with Dist',2]];
//			  var mydata=  [[top1Name,top1val],[top2Name,top2Val],[top3Name,top3Val]];
//				alert(mydata);
// 		 		top3jobcategory(mydata);
	
			  if(top1val==0 && top2Val==0 && top3Val==0){
				  top3jobcatnewForEmpty();
				 
			  }else{
 	    	  top3jobcatnew(top1Name,top2Name,top3Name,top1val,top2Val,top3Val); 
			  }
			 
			},
		});
	 }catch(ee){alert(ee);}
	
	 
	 
	 
	 
//	 alert(result);

}
//Ajay Jain
function showDivData()
{
	var branchdashboard=$("#branchdashboard").val();
	if(branchdashboard!=null && branchdashboard!="")
		showBranchSearchDiv();
	else	
		showWrapperDiv();	
}
function showBranchSearchDiv()
{	 
	 $("#wrapper").hide();
	 $("#searchItem").show();
}
function showWrapperDiv()
{	
	$("#searchItem").hide();
	$("#wrapper").show();
	reportByTime('week');
}
function searchData()
{
	$('#branchName').css("background-color", "");
	$('#errordiv1').empty();
	if($("#branchSearchId").val()=="")
	{		
		$('#errordiv1').html("&#149; "+resourceJSON.enaterBranchName+"<br>");
		$('#branchName').focus();
		$('#branchName').css("background-color", "#F5E7E1");
		$('#errordiv1').show();
		return false;
	}
	$('#searchItem').fadeOut(1000);
	$('#sa').fadeIn(1200);
	$('#closePan').html("<a style='padding-right: 6px;'   onclick='hideSearchPan();' href='javascript:void(0);' ><i class='icon-remove icon-large' ></i></a>");
	showWrapperDiv();
}
function getSearchPan()
{	
	$('#errordiv').hide();
	$('#branchName').css("background-color", "");	
	$('#searchItem').fadeIn(1000);
	$('#sa').fadeOut(1200);	
}
function hideSearchPan()
{
	$('#errordiv').hide();
	$('#branchName').css("background-color", "");
	$('#searchItem').fadeOut(1000);	
	$('#sa').fadeIn(1200);
} 


