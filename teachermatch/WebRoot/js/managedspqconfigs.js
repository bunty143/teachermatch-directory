/*========  For Handling session Time out Error ===============*/
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
var fielfId;
function displayDSPQConfigs()
{
	//alert("Manage DSPQ Configs");
	cancelDSPQConfigs();
	var districtId = document.getElementById("districtId").value;
	var jobCategoryId = document.getElementById("jobCategoryId").value;	
	ManageDSPQConfigAjax.getDSPQConfigss(districtId,jobCategoryId,{ 
		async: true,
		callback: function(data)
		{		
			//var obj = eval ("(" + data + ")");
		/*
			private Integer reference;
			private Boolean referenceLetter;
			private Integer referenceLettersOfRecommendation;
		 */
			//alert(data.length);	
		if(data.length!=0)
		{
			for(i=0;i<data.length;i++)
			{
				var ct="";
				var internal  = data[i];
				if(data[i].candidateType=="I")
					ct="int";
				if(data[i].candidateType=="E")
					ct="ext";
				if(data[i].candidateType=="M")
					ct="intMov";
				$('#'+ct+'ernalApplicants').attr('checked', internal.candidateType);
				if(internal.coverLetter==1)
				$('#intDistCL').attr('checked', internal.coverLetter);
				
				$('input[name='+ct+'DistCL_r][value='+(internal.coverLetter==true?1:0)+']').attr('checked', true); 
				if(internal.address==1)
				$('#intDistAdd').attr('checked', internal.address);
				$('input[name='+ct+'DistAdd_r][value='+(internal.address==true?1:0)+']').attr('checked', true);
				if(internal.phoneNumber==1)
				$('#intDistMob').attr('checked', internal.phoneNumber);
				$('input[name='+ct+'DistMob_r][value='+(internal.phoneNumber==true?1:0)+']').attr('checked', true);
				if(internal.ssn==1)
				$('#intDistSSN').attr('checked', internal.ssn);
				$('input[name='+ct+'DistSSN_r][value='+(internal.ssn==true?1:0)+']').attr('checked', true); 
				$('input[name='+ct+'DistSSNFull_r][value='+(internal.ssnFull==true?1:0)+']').attr('checked', true);
				
				/*if(internal.academic>=1) // consider as boolean only 0 and 1
					$('#'+ct+'DistEdu').attr('checked', true);
				else
					$('#'+ct+'DistEdu').attr('checked', false);*/
				
				//$('#'+ct+'DistAca').attr('checked', internal.academic);
				if(internal.academic==1)
				$('input[name=intDistAca][value='+(internal.academic==true?1:0)+']').attr('checked', true); 
				$('input[name='+ct+'DistAca_r][value='+(internal.academic==true?1:0)+']').attr('checked', true); 
				if(internal.willingAsSubstituteTeacher==1)
				$('#intDistWill').attr('checked', internal.willingAsSubstituteTeacher);
				$('input[name='+ct+'DistWill_r][value='+(internal.willingAsSubstituteTeacher==true?1:0)+']').attr('checked', true);
				if(internal.tfaAffiliate==1)
				$('#intDistTFA').attr('checked', internal.tfaAffiliate);
				$('input[name='+ct+'DistTFA_r][value='+(internal.tfaAffiliate==true?1:0)+']').attr('checked', true);
				if(internal.resume==1)
				$('#intDistResume').attr('checked', internal.resume);
				$('input[name='+ct+'DistResume_r][value='+(internal.resume==true?1:0)+']').attr('checked', true);
				if(internal.expCertTeacherTraining==1)
				$('#intDistWorkExp').attr('checked', internal.expCertTeacherTraining);
				$('input[name='+ct+'DistWorkExp_r][value='+(internal.expCertTeacherTraining==true?1:0)+']').attr('checked', true);
				
				if(internal.reference>=1)
					$('#'+ct+'DistRef').attr('checked', true);
				else
					$('#'+ct+'DistRef').attr('checked', false);
				
				$('input[name='+ct+'DistRef_r][value='+(internal.reference==true?1:0)+']').attr('checked', true);
				
				$('#'+ct+'DistNoOfRef option[value="' + internal.reference + '"]').prop('selected', true);
				
				if(internal.certification>=1)
					$('#'+ct+'DistCert').attr('checked', true);
				else
					$('#'+ct+'DistCert').attr('checked', false);
				
				$('input[name='+ct+'DistCert_r][value='+(internal.certification==true?1:0)+']').attr('checked', true);
				
				if(internal.proofOfCertification>=1)
					$('#'+ct+'DistProffOfCert').attr('checked', true);
				else
					$('#'+ct+'DistProffOfCert').attr('checked', false);
				
				$('input[name='+ct+'DistProffOfCert_r][value='+(internal.proofOfCertification==true?1:0)+']').attr('checked', true);
				
				//$('#'+ct+'DistNoOfRec option[value="' + internal.referenceLettersOfRecommendation + '"]').prop('selected', true);
				if(internal.referenceLettersOfRecommendation==1)
				$('#intDistRefLetrRec').attr('checked', internal.referenceLettersOfRecommendation);
				$('input[name='+ct+'DistRefLetrRec_r][value='+(internal.referenceLettersOfRecommendation==true?1:0)+']').attr('checked', true);
				
				if(internal.nationalBoardCert==1)
				$('#intDistNationalBoardCert').attr('checked', internal.nationalBoardCert);
				$('input[name='+ct+'DistNationalBoardCert_r][value='+(internal.nationalBoardCert==true?1:0)+']').attr('checked', true);
				
				if(internal.academicTranscript==1)
					$('#intDistTrans').attr('checked', internal.academicTranscript);
				
				$('input[name='+ct+'DistTrans_r][value='+(internal.academicTranscript==true?1:0)+']').attr('checked', true);
				//$('#'+ct+'DistNoOfTrans option[value="' + internal.academicTranscript + '"]').prop('selected', true);
				
				if(internal.personalinfo==1)
				$('#intDistPI').attr('checked', internal.personalinfo);
				$('input[name='+ct+'DistPI_r][value='+(internal.personalinfo==true?1:0)+']').attr('checked', true);				
				
				if(internal.dateOfBirth==1)
				$('#intDistDob').attr('checked', internal.dateOfBirth);
				$('input[name='+ct+'DistDob_r][value='+(internal.dateOfBirth==true?1:0)+']').attr('checked', true);
				
				if(internal.ethnicorigin==1)
				$('#intDistEO').attr('checked', internal.ethnicorigin);				
				$('input[name='+ct+'DistEO_r][value='+(internal.ethnicorigin==true?1:0)+']').attr('checked', true);
				
				if(internal.ethinicity==1)
				$('#intDistET').attr('checked', internal.ethinicity);
				$('input[name='+ct+'DistET_r][value='+(internal.ethinicity==true?1:0)+']').attr('checked', true);
				if(internal.race==1)
				$('#intDistRace').attr('checked', internal.race);
				$('input[name='+ct+'DistRace_r][value='+(internal.race==true?1:0)+']').attr('checked', true);
				if(internal.genderId==1)
				$('#intDistSex').attr('checked', internal.genderId);
				$('input[name='+ct+'DistSex_r][value='+(internal.genderId==true?1:0)+']').attr('checked', true);
				
				var eeoc = false;
				if(internal.personalinfo || internal.ethnicorigin || internal.ethinicity || internal.race || internal.genderId)
					eeoc = true;
				
				$('#'+ct+'DistEEOC').attr('checked', eeoc);
				
				if(internal.veteran==1)
				$('#intDistVetrn').attr('checked', internal.veteran);
				$('input[name='+ct+'DistVetrn_r][value='+(internal.veteran==true?1:0)+']').attr('checked', true);
				if(internal.employment==1)
				$('#intDistEmp').attr('checked', internal.employment);
				$('input[name='+ct+'DistEmp_r][value='+(internal.employment==true?1:0)+']').attr('checked', true);
				if(internal.formeremployee==1)
				$('#intDistFEmp').attr('checked', internal.formeremployee);
				$('input[name='+ct+'DistFEmp_r][value='+(internal.formeremployee==true?1:0)+']').attr('checked', true);
				if(internal.retirementnumber==1)
				$('#intDistRetNo').attr('checked', internal.retirementnumber);
				$('input[name='+ct+'DistRetNo_r][value='+(internal.retirementnumber==true?1:0)+']').attr('checked', true);
				if(internal.generalKnowledgeExam==1)
				$('#intDistGKE').attr('checked', internal.generalKnowledgeExam);
				$('input[name='+ct+'DistGKE_r][value='+(internal.generalKnowledgeExam==true?1:0)+']').attr('checked', true);
				if(internal.subjectAreaExam==1)
				$('#intDistSAE').attr('checked', internal.subjectAreaExam);
				$('input[name='+ct+'DistSAE_r][value='+(internal.subjectAreaExam==true?1:0)+']').attr('checked', true);
				if(internal.additionalDocuments==1)
				$('#intDistAddDoc').attr('checked', internal.additionalDocuments);
				$('input[name='+ct+'DistAddDoc_r][value='+(internal.additionalDocuments==true?1:0)+']').attr('checked', true);
				if(internal.affidavit==1)
				$('#intDistAffidavit').attr('checked', internal.affidavit);
				$('input[name='+ct+'DistAffidavit_r][value='+(internal.affidavit==true?1:0)+']').attr('checked', true);
				if(internal.videoLink==1)
				$('#intDistVideoLink').attr('checked', internal.videoLink);
				$('input[name='+ct+'DistVideoLink_r][value='+(internal.videoLink==true?1:0)+']').attr('checked', true);
				if(internal.involvement==1)
				$('#intDistInv').attr('checked', internal.involvement);
				$('input[name='+ct+'DistInv_r][value='+(internal.involvement==true?1:0)+']').attr('checked', true);
				if(internal.honors==1)
				$('#intDistHon').attr('checked', internal.honors);
				$('input[name='+ct+'DistHon_r][value='+(internal.honors==true?1:0)+']').attr('checked', true);
		  }
		}
		else{
			cancelDSPQConfigs();
		}
		},
		errorHandler:handleError 
	});
}
function cancelDSPQConfigs()
{
	for(i=0;i<3;i++)
	{				
	var ct="";			
	if(i==0)
		ct="int";
	if(i==1)
		ct="ext";
	if(i==2)
		ct="intMov";
	$('#'+ct+'ernalApplicants').attr('checked', false);
	$('#intDistCL').attr('checked', false);
	$('input[name='+ct+'DistCL_r][value=1]').attr('checked', false);
	$('input[name='+ct+'DistCL_r][value=0]').attr('checked', false);
	$('#intDistAdd').attr('checked', false);
	$('input[name='+ct+'DistAdd_r][value=0]').attr('checked', false); 
	$('input[name='+ct+'DistAdd_r][value=1]').attr('checked', false); 
	$('#intDistMob').attr('checked', false);
	$('input[name='+ct+'DistMob_r][value=0]').attr('checked', false); 
	$('input[name='+ct+'DistMob_r][value=1]').attr('checked', false);
	$('#intDistSSN').attr('checked', false);
	$('input[name='+ct+'DistSSN_r][value=0]').attr('checked', false); 
	$('input[name='+ct+'DistSSN_r][value=1]').attr('checked', false);
	$('input[name='+ct+'DistSSNFull_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistSSNFull_r][value=1]').attr('checked', false);
	/*if(internal.academic>=1) // consider as boolean only 0 and 1
		$('#'+ct+'DistEdu').attr('checked', true);
	else
		$('#'+ct+'DistEdu').attr('checked', false);*/
	
	$('#intDistAca').attr('checked', false);

	$('input[name='+ct+'DistAca_r][value=0]').attr('checked', false); 
	$('input[name='+ct+'DistAca_r][value=1]').attr('checked', false); 
	
	$('#intDistWill').attr('checked', false);
	$('input[name='+ct+'DistWill_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistWill_r][value=1]').attr('checked', false);
	$('#intDistTFA').attr('checked', false);
	$('input[name='+ct+'DistTFA_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistTFA_r][value=1]').attr('checked', false);
	$('#intDistResume').attr('checked', false);
	$('input[name='+ct+'DistResume_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistResume_r][value=1]').attr('checked', false);
	$('#intDistWorkExp').attr('checked',false);
	$('input[name='+ct+'DistWorkExp_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistWorkExp_r][value=1]').attr('checked', false);
	
	
	$('#intDistRef').attr('checked', false);
	
	$('input[name='+ct+'DistRef_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistRef_r][value=1]').attr('checked', false);
	
	$('#'+ct+'DistNoOfRef option[value=0').prop('selected', false);
	

	
	$('#intDistCert').attr('checked', false);			
	$('input[name='+ct+'DistCert_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistCert_r][value=1]').attr('checked', false);
	

	$('#intDistProffOfCert').attr('checked', false);			
	$('input[name='+ct+'DistProffOfCert_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistProffOfCert_r][value=1]').attr('checked', false);
	
	$('#intDistRefLetrRec').attr('checked', false);
	$('input[name='+ct+'DistRefLetrRec_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistRefLetrRec_r][value=1]').attr('checked', false);
	
	$('#intDistNationalBoardCert').attr('checked', false);
	$('input[name='+ct+'DistNationalBoardCert_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistNationalBoardCert_r][value=1]').attr('checked', false);
	
	$('#intDistTrans').attr('checked', false);
	$('input[name='+ct+'DistTrans_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistTrans_r][value=1]').attr('checked', false);
	
	$('#intDistPI').attr('checked', false);
	$('input[name='+ct+'DistPI_r][value=0]').attr('checked', false);				
	$('input[name='+ct+'DistPI_r][value=1]').attr('checked', false);
	
	$('#intDistDob').attr('checked', false);
	$('input[name='+ct+'DistDob_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistDob_r][value=1]').attr('checked', false);
	
	$('#intDistEO').attr('checked', false);				
	$('input[name='+ct+'DistEO_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistEO_r][value=1]').attr('checked', false);
	
	$('#intDistET').attr('checked', false);
	$('input[name='+ct+'DistET_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistET_r][value=1]').attr('checked', false);
	
	$('#intDistRace').attr('checked',false);
	$('input[name='+ct+'DistRace_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistRace_r][value=1]').attr('checked', false);
	
	$('#intDistSex').attr('checked',false);
	$('input[name='+ct+'DistSex_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistSex_r][value=1]').attr('checked', false);
	
	var eeoc = false;
	
	
	$('#intDistEEOC').attr('checked', false);
	
	$('#intDistVetrn').attr('checked', false);
	$('input[name='+ct+'DistVetrn_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistVetrn_r][value=1]').attr('checked', false);
	
	$('#intDistEmp').attr('checked', false);
	$('input[name='+ct+'DistEmp_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistEmp_r][value=1]').attr('checked', false);
	
	$('#intDistFEmp').attr('checked', false);
	$('input[name='+ct+'DistFEmp_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistFEmp_r][value=1]').attr('checked', false);
	
	$('#intDistRetNo').attr('checked', false);
	$('input[name='+ct+'DistRetNo_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistRetNo_r][value=1]').attr('checked', false);
	
	$('#intDistGKE').attr('checked', false);
	$('input[name='+ct+'DistGKE_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistGKE_r][value=1]').attr('checked', false);
	
	$('#intDistSAE').attr('checked', false);
	$('input[name='+ct+'DistSAE_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistSAE_r][value=1]').attr('checked', false);
	
	$('#intDistAddDoc').attr('checked', false);
	$('input[name='+ct+'DistAddDoc_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistAddDoc_r][value=1]').attr('checked', false);
	
	$('#intDistAffidavit').attr('checked', false);
	$('input[name='+ct+'DistAffidavit_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistAffidavit_r][value=1]').attr('checked', false);
	
	$('#intDistVideoLink').attr('checked', false);
	$('input[name='+ct+'DistVideoLink_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistVideoLink_r][value=1]').attr('checked', false);
	
	$('#intDistInv').attr('checked', false);
	$('input[name='+ct+'DistInv_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistInv_r][value=1]').attr('checked', false);
	
	$('#intDistHon').attr('checked', false);
	$('input[name='+ct+'DistHon_r][value=0]').attr('checked', false);
	$('input[name='+ct+'DistHon_r][value=1]').attr('checked', false);
	
	}
}
function saveDSPQConfigs(cType)
{
	//alert("Manage DSPQ Configs");	
	var districtId = document.getElementById("districtId").value;
	var jobCategoryId = document.getElementById("jobCategoryId").value;
	var districtPortfolioConfig = {};
	
	for(i=1;i<=3;i++)
	{
	var ct = "";
	if(i==1)
	{
		ct = "int";
		districtPortfolioConfig.candidateType="I";
	}
	else if(i==2)
	{
		ct = "ext";
		districtPortfolioConfig.candidateType="E";
	}
	else if(i==3)
	{
		ct = "intMov";
		districtPortfolioConfig.candidateType="M"
	}
	districtPortfolioConfig.jobCategoryMaster={jobCategoryId:jobCategoryId};
	districtPortfolioConfig.districtMaster={districtId:districtId};	
	districtPortfolioConfig.coverLetter=$("input[name="+ct+"DistCL_r]:checked").val()==1?true:false;	
	districtPortfolioConfig.academic=$("input[name="+ct+"DistAca_r]:checked").val()==1?1:0;
	districtPortfolioConfig.academicTranscript=$("input[name="+ct+"DistTrans_r]:checked").val()==1?1:0;
	districtPortfolioConfig.certification=$("input[name="+ct+"DistCert_r]:checked").val()==1?1:0;	
	districtPortfolioConfig.proofOfCertification=$("input[name="+ct+"DistProffOfCert_r]:checked").val()==1?1:0;
	districtPortfolioConfig.reference=$("input[name="+ct+"DistRef_r]:checked").val()==1?1:0;
	districtPortfolioConfig.referenceLettersOfRecommendation=$("input[name="+ct+"DistRefLetrRec_r]:checked").val()==1?1:0;
	districtPortfolioConfig.resume=$("input[name="+ct+"DistResume_r]:checked").val()==1?true:false;
	districtPortfolioConfig.tfaAffiliate=$("input[name="+ct+"DistTFA_r]:checked").val()==1?true:false;	
	districtPortfolioConfig.willingAsSubstituteTeacher=$("input[name="+ct+"DistWill_r]:checked").val()==1?true:false;	
	districtPortfolioConfig.address=$("input[name="+ct+"DistAdd_r]:checked").val()==1?true:false;
	districtPortfolioConfig.expCertTeacherTraining=$("input[name="+ct+"DistWorkExp_r]:checked").val()==1?true:false;
	districtPortfolioConfig.nationalBoardCert=$("input[name="+ct+"DistNationalBoardCert_r]:checked").val()==1?true:false;
	districtPortfolioConfig.affidavit=$("input[name="+ct+"DistAffidavit_r]:checked").val()==1?true:false;
	districtPortfolioConfig.phoneNumber=$("input[name="+ct+"DistMob_r]:checked").val()==1?true:false;
	districtPortfolioConfig.personalinfo=$("input[name="+ct+"DistPI_r]:checked").val()==1?true:false;
	districtPortfolioConfig.dateOfBirth=$("input[name="+ct+"DistDob_r]:checked").val()==1?true:false;
	districtPortfolioConfig.ssn=$("input[name="+ct+"DistSSN_r]:checked").val()==1?true:false;	
	districtPortfolioConfig.race=$("input[name="+ct+"DistRace_r]:checked").val()==1?true:false;
	districtPortfolioConfig.genderId=$("input[name="+ct+"DistSex_r]:checked").val()==1?true:false;
	districtPortfolioConfig.ethnicorigin=$("input[name="+ct+"DistET_r]:checked").val()==1?true:false;	
	districtPortfolioConfig.ethinicity=$("input[name="+ct+"DistEO_r]:checked").val()==1?true:false;
	districtPortfolioConfig.employment=$("input[name="+ct+"DistEmp_r]:checked").val()==1?true:false;	
	districtPortfolioConfig.formeremployee=$("input[name="+ct+"DistFEmp_r]:checked").val()==1?true:false;
	districtPortfolioConfig.generalKnowledgeExam=$("input[name="+ct+"DistGKE_r]:checked").val()==1?true:false;	
	districtPortfolioConfig.subjectAreaExam=$("input[name="+ct+"DistSAE_r]:checked").val()==1?true:false;
	districtPortfolioConfig.additionalDocuments=$("input[name="+ct+"DistAddDoc_r]:checked").val()==1?true:false;
	districtPortfolioConfig.retirementnumber=$("input[name="+ct+"DistRetNo_r]:checked").val()==1?true:false;
	districtPortfolioConfig.veteran=$("input[name="+ct+"DistVetrn_r]:checked").val()==1?true:false;
	districtPortfolioConfig.videoLink=$("input[name="+ct+"DistVideoLink_r]:checked").val()==1?true:false;
	districtPortfolioConfig.involvement=$("input[name="+ct+"DistInv_r]:checked").val()==1?true:false;
	districtPortfolioConfig.honors=$("input[name="+ct+"DistHon_r]:checked").val()==1?true:false;
	
	ManageDSPQConfigAjax.saveDSPQConfigs(districtPortfolioConfig,{ 
		async: true,
		callback: function(data)
		{
			cancelHideShowDiv();
			displayDSPQConfigs();
			$('#myModal2').modal('show');
			$('#message2show').html("District settings updated successfully");
		},
		errorHandler:handleError 
	});
	}
}

function getShowFields(fName,reqiredField)
{		
	$('#myFieldName').html(fName);
	fielfId=reqiredField;
	$('#'+reqiredField).show();
	$('#'+reqiredField+'ext').show();
	$('#'+reqiredField+'IMov').show();
	$('#showFields').modal('show'); 
}
function cancelHideShowDiv()
{
	$('#'+fielfId).hide();
	$('#'+fielfId+'ext').hide();
	$('#'+fielfId+'IMov').hide();
}
function getHideShowCheck(ID)
{		
	if(ID=1)
	{
		$('#disableCheck').show();
		$('#checkBoxShow').hide();
	}
	else
	{
		$('#disableCheck').hide();
		$('#checkBoxShow').show();	
	}
}
function displayDSPQ()
{
	//$('#loadingDiv').show();
	var districtId = document.getElementById("districtId").value;
	var jobCategoryId = document.getElementById("jobCategoryId").value;	
	ManageDSPQConfigAjax.displayDspq(districtId,jobCategoryId,noOfRows,page,{ 
		async: true,
		callback: function(data)
		{	
			$('#dspqGrid').html(data);
				applyScrollOnTbl();
			//$('#loadingDiv').hide();
		},
		errorHandler:handleError 
	});
	
}
function displayDSPQPersonal()
{
	ManageDSPQConfigAjax.displayDspqPersonal(1,{ 
		async: true,
		callback: function(data)
		{	
			$('#dspqGrid').html(data);
				applyScrollOnTbl();
			//$('#loadingDiv').hide();
		},
		errorHandler:handleError 
	});
	
}