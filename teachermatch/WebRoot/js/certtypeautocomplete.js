function getAllCertificateTypeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getAllCertificateTypeArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		//alert(divid);
		document.getElementById(divid).style.display='none';
	}
}

function getAllCertificateTypeArray(certType){
	
	var searchArray = new Array();	
	DWRAutoComplete.getAllCertificateTypeList(certType,{ 
		async: false,
		callback: function(data){
		//alert(data);
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
		}
	}
	});	

	return searchArray;
}
function hideAllCertificateTypeDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		//alert(hiddenId);
		//alert(hiddenDataArray[index]);
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		//dis.value="";
		
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			//setDefColortoErrorMsg();
			$('#errordiv').append("&#149; "+resourceJSON.MsgEtrValidCertificate+"<br>");
			if(focs==0)
				$('#certName').focus();
			
			$('#certName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		else
		{
			$('#errordiv').empty();	
			//setDefColortoErrorMsg();
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function getFilterCertificateTypeAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getFilterCertificateTypeArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFilterCertificateTypeArray(certType){
	
	var stateId	=0;
	try{
		stateId=document.getElementById("stateId").value;
	}catch(e){}
	var searchArray = new Array();	
	CGServiceAjax.getFilterCertificateTypeList(certType,stateId,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
		}
	}
	});	

	return searchArray;
}


/**************** Start :: For candidate List **************/
function getFilterCertificateTypeAutoCompForCan(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38)
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13)
	{
		
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		document.getElementById("hrefDone").focus();
		
	} 
	else if(event.keyCode==9)
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getFilterCertificateTypeArrayForCan(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getFilterCertificateTypeArrayForCan(certType){
	
	var stateId	=0;
	try{
		stateId=document.getElementById("stateIdForcandidate").value;
	}catch(e){}
	var searchArray = new Array();	
	CGServiceAjax.getFilterCertificateTypeList(certType,stateId,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
			hiddenDataArray[i]=data[i].certTypeId;
			showDataArray[i]=data[i].certType+" ["+data[i].stateId.stateName+"]";
		}
	}
	});	

	return searchArray;
}
/**************** End :: For candidate List ****************/






function hideFilterCertificateTypeDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
		if(dis.value!="")
		{
			var cnt=0;
			var focs=0;	
			$('#errordiv').empty();	
			$('#errordiv').append("&#149; "+resourceJSON.MsgEtrValidCertificate+"<br>");
			if(focs==0)
				$('#certName').focus();
			
			$('#certName').css("background-color",txtBgColor);
			cnt++;focs++;	
		}
		else
		{
			$('#errordiv').empty();	
		}
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}