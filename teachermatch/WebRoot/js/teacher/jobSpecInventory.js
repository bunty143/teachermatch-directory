
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayJobSpecInventoryGrid();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayJobSpecInventoryGrid();
}
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*========  JobSpecInventoryGrid ===============*/
function displayJobSpecInventoryGrid()
{
	$("#loadingDivJSI").show();
	JobSpecInventoryAjax.displayJobSpecInventoryGrid(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		//alert(" data "+data);
		document.getElementById("jobInventoryGrid").innerHTML=data;
		$("#loadingDivJSI").hide();
		//$('#jobInventoryGrid').html(data);
		//tpJbIEnable();
		applyScrollOnTbl();
		/*========== for showing Tool tip on Images ============*/
		tpJbIDisable();
		tpJbIEnable();
	},
	errorHandler:handleError  
	});
}
function tpJbIDisable()
{
	var noOrRow = document.getElementById("tblGridJSI").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{
		$('#iconpophover'+j).tooltip();
	}
}
function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGridJSI").rows.length;
	for(var j=1;j<=noOrRow;j++)
	{	
		$('#iconpophover'+j).tooltip();
	}
}