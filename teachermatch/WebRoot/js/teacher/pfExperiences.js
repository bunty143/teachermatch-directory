/*========= Paging and Sorting ===========*/
var page = 1;
var noOfRows = 100;
var sortOrderStr="";
var sortOrderType="";
/*function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}

	noOfRows = document.getElementById("pageSize").value;
	//getPFEmploymentGrid();
	getInvolvementGrid();
}*/
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=100;
	}
	var divNo	=	document.getElementById("divNo").value;
	if(divNo!=null && divNo!="" && divNo==2)
	{
		getPFEmploymentGrid();
	}
	else
	{
		getInvolvementGrid();
	}
}
function getDivNo(divNo)
{
	$('#divNo').val(divNo);
}


var txtBgColor="#F5E7E1";
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function removeResume()
{	
	if(window.confirm(resourceJSON.msgsureremoveresum))
	{
		$('#resume').css("background-color","");
		$('#errordiv').empty();
		PFExperiences.deleteResume({ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
			document.getElementById("hrefExperiences").className = "";
			document.getElementById("divResumeTxt").innerHTML="";
			document.getElementById("hdnResume").value="";
			document.getElementById("frmExpResumeUpload").reset();
			}});
	}
	return false;
}

function downloadResume()
{
	PFExperiences.downloadResume(
			{ 
				async: false,
				errorHandler:handleError,
				callback:function(data)
				{
					if (data.indexOf(".doc") !=-1) {
					   // $('#modalDownloadsTranscript').modal('hide');
					    document.getElementById('ifrmResume').src = ""+data+"";
					}
					else
					{
						//window.open(data);
						//openWindow(data,'Support',650,490);
						document.getElementById("hrefResume").href = data; 
						return false;
					}
				}
			});
}

function saveAndContinueExperiences()
{
	
	
	
	var divHonor=0;
	var divInvolvement=0;
	var divEmployment=0;
	var msg=[];
	var block=0;
	
	if($("#role").is(':visible'))
		if(!insertOrUpdateEmployment())
		{
			divEmployment=1;
			msg.push("Employment");

			var opened=true;
			$(".accordion-body").each(function(){
		        if($(this).hasClass("in")) {
		        	if($(this).attr('id')=="collapseTwo")
		        	opened=false;
		        }
		    });
			if(block==0 && opened)
			$("#employmentDiv").click();
			
			block++;
		}

	if($("#organization").is(':visible'))
		if(!saveOrUpdateInvolvement())
		{
			divInvolvement=1;
			msg.push(" Involvement");
			
			var opened=true;
			$(".accordion-body").each(function(){
		        if($(this).hasClass("in")) {
		        	if($(this).attr('id')=="collapseThree")
		        	opened=false;
		        }
		    });
			if(block==0 && opened)
			$("#involvementDiv").click();
			
			block++;
		}

	if($("#honor").is(':visible'))
		if(!saveOrUpdateHonors())
		{
			divHonor=1;
			msg.push(" Honors");
			var opened=true;
			$(".accordion-body").each(function(){
		        if($(this).hasClass("in")) {
		        	if($(this).attr('id')=="collapseFour")
		        	opened=false;
		        }
		    });
			if(block==0 && opened)
			$("#honorDiv").click();
			
			block++;
		}


	if((divHonor+divInvolvement+divEmployment)>0)
	{
		$('#message2show').html(resourceJSON.msgpleasecomplete+msg+resourceJSON.msgsavecontinue+".");
		$('#myModal2').modal('show');
		return;
	}

	var fileName=document.getElementById("resume").value;
	var hdnResume = document.getElementById("hdnResume").value;
	var cnt=0;
	var focs=0;	

	$('#resume').css("background-color","");
	$('#errordiv').empty();


	if(fileName=="")
	{
		if(hdnResume=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgresumr+"<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt++;focs++;	


		}
		else
		{
			
			PFExperiences.updatePortfolioStatus({ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
				if(data=='ok')
					window.location.href="affidavit.do";
			}});
			
			//window.location.href="affidavit.do";
		}
	}
	else
	{
		var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();


		var fileSize=0;		
		if ($.browser.msie==true)
		{	
			fileSize = 0;	   
		}
		else
		{	
			if(document.getElementById("resume").files[0]!=undefined)
				fileSize = document.getElementById("resume").files[0].size;
		}

		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgacceptableresume+"<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt++;focs++;	

		}
		else if(fileSize>=10485760)
		{		
			$('#errordiv').append("&#149; "+resourceJSON.msgfilesize+"<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt++;focs++;	

		}
		else
		{
			$('#loadingDiv').show();
			document.getElementById("frmExpResumeUpload").submit();
		}
	}

	if(cnt!=0)		
	{
		$('#errordiv').show();
		var opened=true;
		$(".accordion-body").each(function(){
	        if($(this).hasClass("in")) {
	        	if($(this).attr('id')=="collapseOne")
	        	opened=false;
	        }
	    });
		if(opened)
			$("#resumeDiv").click();
		
		$('#message2show').html(""+resourceJSON.msgresumesectionacceptable+" ");
		$('#myModal2').modal('show');
		return false;
	}
	return true;
}

function saveResume(fileName)
{
		PFExperiences.addOrEditResume(fileName, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			document.getElementById("frmExpResumeUpload").reset();
			document.getElementById("divResumeTxt").innerHTML="<label>"+resourceJSON.msgresume+fileName+"</label>";
			window.location.href="affidavit.do";
		}});
}

function getPFEmploymentGrid()
{
	PFExperiences.getPFEmploymentGrid(noOfRows,page,sortOrderStr,sortOrderType, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		$('#divDataEmployment').html(data);
		applyScrollOnTbl();
		}});
}

function showEmploymentForm()
{
	document.getElementById("frmEmployment").reset();
	document.getElementById("roleId").value="";	
	document.getElementById("divEmployment").style.display="block";
	document.getElementById("role").focus();
	document.getElementById("currentlyWorking").checked = false;
	document.getElementById("divToMonth").style.display="block";
	document.getElementById("divToYear").style.display="block";
	setDefColortoErrorMsgToEmployment();
	$('#primaryRespdiv').find(".jqte_editor").html("");
	$('#mostSignContdiv').find(".jqte_editor").html("");
	$('#errordivEmployment').empty();
	return false;
}

function hideEmploymentForm()
{
	document.getElementById("frmEmployment").reset();
	document.getElementById("roleId").value="";	
	document.getElementById("divEmployment").style.display="none";
	return false;
}



function insertOrUpdateEmployment()
{	
	var position = document.getElementById("empPosition");
	var role = document.getElementById("role");
	var empOrg = document.getElementById("empOrg");
	var fieldId = document.getElementById("fieldId");
	var currentlyWorking = document.getElementById("currentlyWorking");
	var roleStartMonth = document.getElementById("roleStartMonth");
	var roleStartYear = document.getElementById("roleStartYear");	
	var roleEndMonth = document.getElementById("roleEndMonth");
	var roleEndYear = document.getElementById("roleEndYear");
	//shadab
	
	var cityEmp = document.getElementById("cityEmp");
	var stateOfOrg = document.getElementById("stateOfOrg");
	
	var empRoleTypeId=document.getElementsByName("empRoleTypeId");
	var amount=document.getElementById("amount");
	var cnt=0;
	var focs=0;	
	var amountFlag=false;
	if(amount!=""){
		amountFlag=isNaN(amount.value);
	}
	var reasonForLea = trim($('#reasonForLeadiv').find(".jqte_editor").text());
	var charCount=trim($('#primaryRespdiv').find(".jqte_editor").text());
	var countPrimaryResp = charCount.length;
	
	charCount=trim($('#mostSignContdiv').find(".jqte_editor").text());
	var countmostSignCont = charCount.length;
	
	$('#errordivEmployment').empty();
	setDefColortoErrorMsgToEmployment();
	if(amountFlag)
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgnumericamount+"<br>");
		if(focs==0)
			$('#amount').focus();

		$('#amount').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(trim(position.value)=="0")
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgposition+"<br>");
		if(focs==0)
			$('#empPosition').focus();

		$('#empPosition').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(trim(role.value)=="")
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgrole+"<br>");
		if(focs==0)
			$('#role').focus();

		$('#role').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(trim(empOrg.value)=="")
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgorganization+"<br>");
		if(focs==0)
			$('#empOrg').focus();

		$('#empOrg').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(trim(cityEmp.value)=="")
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgNameofCity+"<br>");
		if(focs==0)
			$('#cityEmp').focus();

		$('#cityEmp').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(stateOfOrg.value)=="")
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgstateorganization+"<br>");
		if(focs==0)
			$('#stateOfOrg').focus();

		$('#stateOfOrg').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(trim(fieldId.value)=="")
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectField+"<br>");
		if(focs==0)
			$('#fieldId').focus();

		$('#fieldId').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(roleStartMonth.value)=="0")
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectFromMonth+"<br>");
		if(focs==0)
			$('#roleStartMonth').focus();

		$('#roleStartMonth').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(roleStartYear.value)=="0")
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectFromYear+"<br>");
		if(focs==0)
			$('#roleStartYear').focus();

		$('#roleStartYear').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(currentlyWorking.checked==false)
	{
		if(trim(roleEndMonth.value)=="0")
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectToMonth+"<br>");
			if(focs==0)
				$('#roleEndMonth').focus();

			$('#roleEndMonth').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(roleEndYear.value)=="0")
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgPleaseselectToYear+"<br>");
			if(focs==0)
				$('#roleEndYear').focus();

			$('#roleEndYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}

		if(trim(roleStartMonth.value)!="0" && trim(roleStartYear.value)!="0" && trim(roleEndMonth.value)!="0" && trim(roleEndYear.value)!="0")
		{
			if(trim(roleStartYear.value)>trim(roleEndYear.value))
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgFromgreaterthan+"<br>");
				if(focs==0)
					$('#roleStartMonth').focus();

				$('#roleStartMonth').css("background-color",txtBgColor);
				$('#roleStartYear').css("background-color",txtBgColor);
				$('#roleEndMonth').css("background-color",txtBgColor);
				$('#roleEndYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			else if(parseInt(trim(roleStartMonth.value))>parseInt(trim(roleEndMonth.value)) && (trim(roleStartYear.value)==trim(roleEndYear.value)))
			{
				$('#errordivEmployment').append("&#149; "+resourceJSON.msgFromgreaterthan+"<br>");
				if(focs==0)
					$('#roleStartMonth').focus();

				$('#roleStartMonth').css("background-color",txtBgColor);
				$('#roleStartYear').css("background-color",txtBgColor);
				$('#roleEndMonth').css("background-color",txtBgColor);
				$('#roleEndYear').css("background-color",txtBgColor);
				cnt++;focs++;
			}
		}
	}
	var chkRole=true;
	for(i=0;i<empRoleTypeId.length;i++)
	{
		if(empRoleTypeId[i].checked==true)
		{
			chkRole=false;
			break;
		}
	}
	if(chkRole)
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgtypeRole+"<br>");
		if(focs==0)
			$('#empRoleTypeId').focus();		
		cnt++;focs++;
	}
	
	if(countPrimaryResp>5000)
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgmaximumlength+"<br>");
		if(focs==0)
			$('#primaryRespdiv').find(".jqte_editor").focus();
		$('#primaryRespdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if(countmostSignCont>5000)
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgsufficientdistribution+"<br>");
		if(focs==0)
			$('#mostSignContdiv').find(".jqte_editor").focus();
		$('#mostSignContdiv').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(roleEndYear.value!=0 && roleEndMonth.value!=0){
		if(reasonForLea.trim().length==0)
		{
			$('#errordivEmployment').append("&#149; "+resourceJSON.msgReasonforleaving+"<br>");
			if(focs==0)
				$('#reasonForLeadiv').find(".jqte_editor").focus();
				$('#reasonForLeadiv').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	if(reasonForLea.length>5000)
	{
		$('#errordivEmployment').append("&#149; "+resourceJSON.msgmaximumlengthfiled+"<br>");
		if(focs==0)
			$('#reasonForLeadiv').find(".jqte_editor").focus();
		$('#reasonForLeadiv').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	if(cnt!=0)		
	{
		$('#errordivEmployment').show();
		return false;
	}
	else
	{
		var roleType;
		for(i=0;i<empRoleTypeId.length;i++)
		{
			if(empRoleTypeId[i].checked==true)
			{
				roleType=empRoleTypeId[i].value;
				break;
			}
		}	

		var roleTypeObj = {empRoleTypeId:roleType};	

		var fieldObj = {fieldId:dwr.util.getValue("fieldId")};
		var employment = {position:null,roleId:null,fieldMaster:fieldObj,empRoleTypeMaster:roleTypeObj,role:null, roleStartMonth:null, roleStartYear:null, roleEndMonth:null, roleEndYear:null,amount:null,primaryResp:null,mostSignCont:null,reasonForLeaving:null};
		dwr.engine.beginBatch();

		dwr.util.getValues(employment);
		employment.currentlyWorking=document.getElementById("currentlyWorking").checked;
		employment.reasonForLeaving=reasonForLea;
		employment.position=position.value;
		employment.organization=empOrg.value;
		employment.city=cityEmp.value;
		employment.state=stateOfOrg.value;
		
		PFExperiences.saveOrUpdateEmployment(employment,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
			hideEmploymentForm();
			getPFEmploymentGrid();
			//$("#hrefCertifications").addClass("stepactive");
			//hideForm();
			}
		});
		dwr.engine.endBatch();
		return true;
	}	
}


function hideToExp()
{
	var chk = document.getElementById("currentlyWorking").checked;
	if(chk)
	{
		document.getElementById("divToMonth").style.display="none";
		document.getElementById("divToYear").style.display="none";
	}
	else
	{
		document.getElementById("divToMonth").style.display="block";
		document.getElementById("divToYear").style.display="block";
	}
}
function showEditFormEmployment(id)
{
	PFExperiences.showEditFormEmployment(id,
			{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
		showEmploymentForm();
		if(data.noWorkExp==null || data.noWorkExp==0)
		{
			document.getElementById("roleId").value=id;
			
			document.getElementById("empPosition").value=data.position;
		  		
			document.getElementById("role").value=data.role;
			document.getElementById("fieldId").value=data.fieldMaster.fieldId;

			document.getElementById("amount").value=data.amount;
			document.getElementById("empOrg").value=data.organization;
			document.getElementById("cityEmp").value=data.city;
			document.getElementById("stateOfOrg").value=data.state;

			if(data.currentlyWorking)
			{
				document.getElementById("currentlyWorking").click();
				document.getElementById("roleStartMonth").value=data.roleStartMonth;
				document.getElementById("roleStartYear").value=data.roleStartYear;
			}
			else
			{
				document.getElementById("currentlyWorking").checked=false;
				document.getElementById("divToMonth").style.display="block";
				document.getElementById("divToYear").style.display="block";
				document.getElementById("roleStartMonth").value=data.roleStartMonth;
				document.getElementById("roleStartYear").value=data.roleStartYear;
				

				document.getElementById("roleEndMonth").value=data.roleEndMonth;
				document.getElementById("roleEndYear").value=data.roleEndYear;
			}

			var empRoleTypeId=document.getElementsByName("empRoleTypeId");

			for(i=0;i<empRoleTypeId.length;i++)
			{
				if(empRoleTypeId[i].value==data.empRoleTypeMaster.empRoleTypeId)
					empRoleTypeId[i].checked=true

			}

			//document.getElementById("primaryResp").value=data.primaryResp;
			//document.getElementById("mostSignCont").value=data.mostSignCont;
			$('#primaryRespdiv').find(".jqte_editor").html(data.primaryResp);
			$('#mostSignContdiv').find(".jqte_editor").html(data.mostSignCont);
			$('#reasonForLeadiv').find(".jqte_editor").html(data.reasonForLeaving);
			/*document.getElementById("certificateNameMaster").value=data.certificateNameMaster.certNameId;
				document.getElementById("certName").value=data.certificateNameMaster.certName;
			 */
		}

		
		return false;

		//document.getElementById("stateMaster").value=data.stateMaster.stateId;
		//document.getElementById("").value="";
		//document.getElementById("").value="";
		//document.getElementById("").value="";
		//document.getElementById("").value="";
		//document.getElementById("").value="";

		}
			});

	return false;

}

function deleteEmployment(id)
{	
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFExperiences.deleteEmployment(id, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getPFEmploymentGrid();
			hideEmploymentForm();

			return false;
			}});
	}
	return false;
}

function setDefColortoErrorMsgToEmployment()
{

	$('#role').css("background-color","");
	$('#empPosition').css("background-color", "")
	$('#fieldId').css("background-color","");
	$('#empOrg').css("background-color","");
	$('#roleStartMonth').css("background-color","");
	$('#roleStartYear').css("background-color","");
	$('#roleEndMonth').css("background-color","");
	$('#roleEndYear').css("background-color","");
	$('#currencyId').css("background-color","");
	$('#amount').css("background-color","");
	
	$('#primaryRespdiv').find(".jqte_editor").css("background-color","");
	$('#mostSignContdiv').find(".jqte_editor").css("background-color","");
	$('#reasonForLeadiv').find(".jqte_editor").css("background-color","");
	
	$('#cityEmp').css("background-color","");
	$('#stateOfOrg').css("background-color","");

}
function getInvolvementGrid()
{
	PFExperiences.getInvolvementGrid(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		//document.getElementById("divDataInvolvement").innerHTML=data;
			$('#divDataInvolvement').html(data);
			applyScrollOnInvl();
		}});
}
function saveOrUpdateInvolvement()
{

	var organization = document.getElementById("organization");
	var orgTypeId = document.getElementById("orgTypeId");
	var rangeId = document.getElementById("rangeId");
	var leadNoOfPeople =  document.getElementById("leadNoOfPeople");

	var rdo1 =  document.getElementById("rdo1");

	var cnt=0;
	var focs=0;	

	$('#errordivInvolvement').empty();
	setDefColortoErrorMsgToInvolvement();


	if(trim(organization.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.PlzEtrOrg+"<br>");
		if(focs==0)
			$('#organization').focus();

		$('#organization').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(orgTypeId.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgorgtype+"<br>");
		if(focs==0)
			$('#orgTypeId').focus();

		$('#orgTypeId').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(rangeId.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgnumberofpeople+"<br>");
		if(focs==0)
			$('#rangeId').focus();

		$('#rangeId').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(rdo1.checked && trim(leadNoOfPeople.value)=="")
	{
		$('#errordivInvolvement').append("&#149; "+resourceJSON.msgmanypeoplelead+"<br>");
		if(focs==0)
			$('#leadNoOfPeople').focus();

		$('#leadNoOfPeople').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(cnt!=0)		
	{
		$('#errordivInvolvement').show();
		return false;
	}
	else
	{
		var orgObj = {orgTypeId:dwr.util.getValue("orgTypeId")};
		var peopleRangeObj = {rangeId:dwr.util.getValue("rangeId")};
		var involvement = {involvementId:null, organization:null, orgTypeMaster:orgObj, peopleRangeMaster:peopleRangeObj, leadNoOfPeople:null};

		dwr.engine.beginBatch();	
		dwr.util.getValues(involvement);	
		PFExperiences.saveOrUpdateInvolvement(involvement,
				{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{

			hideInvolvement();
			getInvolvementGrid();

			return false;
			}
				});
		dwr.engine.endBatch();
	}
	return true;


}

function showInvolvementForm()
{
	document.getElementById("frmInvolvement").reset();
	document.getElementById("involvementId").value="";	
	document.getElementById("divInvolvement").style.display="block";
	document.getElementById("organization").focus();
	document.getElementById("divLeadNoOfPeople").style.display="none";
	setDefColortoErrorMsgToInvolvement();
	$('#errordivInvolvement').empty();
	return false;
}
function hideInvolvement()
{
	document.getElementById("frmInvolvement").reset();
	document.getElementById("involvementId").value="";	
	document.getElementById("divInvolvement").style.display="none";
	return false;
}

function showEditFormInvolvement(id)
{
	PFExperiences.showEditFormInvolvement(id,
			{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
		showInvolvementForm();
		//alert(data.leadNoOfPeople)

		if(data.leadNoOfPeople!=null)
		{
			document.getElementById("rdo1").click();
		}
		else
		{
			document.getElementById("divLeadNoOfPeople").style.display="none";
			document.getElementById("leadNoOfPeople").value="";		
		}
		document.getElementById("involvementId").value=id;
		document.getElementById("organization").value=data.organization;
		document.getElementById("orgTypeId").value=data.orgTypeMaster.orgTypeId;
		document.getElementById("rangeId").value=data.peopleRangeMaster.rangeId;
		document.getElementById("leadNoOfPeople").value=data.leadNoOfPeople;


		}
			});

	return false;
}

function deleteRecordInvolvment(id)
{
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFExperiences.deleteRecordInvolvment(id, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			hideInvolvement();
			getInvolvementGrid();

			return false;
			}});
	}
	return false;
}

function setDefColortoErrorMsgToInvolvement()
{
	$('#organization').css("background-color","");
	$('#orgTypeId').css("background-color","");
	$('#rangeId').css("background-color","");
	$('#leadNoOfPeople').css("background-color","");

}

function showAndHideLeadNoOfPeople(opt)
{
	if(opt==1)
	{
		document.getElementById("divLeadNoOfPeople").style.display="block";
	}
	else
	{
		document.getElementById("divLeadNoOfPeople").style.display="none";
		document.getElementById("leadNoOfPeople").value="";		
	}

}

function getHonorsGrid()
{
	PFExperiences.getHonorsGrid({ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
		document.getElementById("divDataHoner").innerHTML=data;
		applyScrollOnHonors();
		}});
}

function saveOrUpdateHonors()
{

	var honor = document.getElementById("honor");
	var honorYear = document.getElementById("honorYear");


	var cnt=0;
	var focs=0;	

	$('#errordivHonor').empty();
	setDefColortoErrorMsgToHoner();


	if(trim(honor.value)=="")
	{
		$('#errordivHonor').append("&#149; "+resourceJSON.msgaward+"<br>");
		if(focs==0)
			$('#honor').focus();

		$('#honor').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(trim(honorYear.value)=="0")
	{
		$('#errordivHonor').append("&#149; "+resourceJSON.msgawardyear+"<br>");
		if(focs==0)
			$('#honorYear').focus();

		$('#honorYear').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(cnt!=0)		
	{
		$('#errordivHonor').show();
		return false;
	}
	else
	{
		var honer = {honorId:null, honor:null, honorYear:null};

		dwr.engine.beginBatch();	
		dwr.util.getValues(honer);	
		PFExperiences.saveOrUpdateHonors(honer,
				{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{

			hideHonor();
			getHonorsGrid();

			return false;
			}
				});
		dwr.engine.endBatch();
	}
	return true;

}

function showEditHonors(id)
{
	PFExperiences.showEditHonors(id,
			{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{		
		showHonorForm();


		document.getElementById("honorId").value=id;
		document.getElementById("honor").value=data.honor;
		document.getElementById("honorYear").value=data.honorYear;

		}
			});

	return false;
}

function deleteRecordHonors(id)
{
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFExperiences.deleteRecordHonors(id,{ 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			hideHonor();
			getHonorsGrid();

			return false;
			}});
	}
	return false;
}
function setDefColortoErrorMsgToHoner()
{
	$('#honor').css("background-color","");
	$('#honorYear').css("background-color","");

}
function hideHonor()
{
	document.getElementById("frmHonor").reset();
	document.getElementById("honorId").value="";	
	document.getElementById("divHonor").style.display="none";
	return false;
}
function showHonorForm()
{
	document.getElementById("frmHonor").reset();
	document.getElementById("honorId").value="";	
	document.getElementById("divHonor").style.display="block";
	document.getElementById("honor").focus();
	setDefColortoErrorMsgToHoner();
	$('#errordivHonor').empty();
	return false;
}

function chkForEnterEmployment(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	//alert(">?>"+evt.srcElement.id)
	if(charCode==13 && evt.srcElement.id!="")
	{
		insertOrUpdateEmployment();
	}	
}
function chkForEnterInvolvement(evt)
{	

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveOrUpdateInvolvement();
	}	
}
function chkForEnterAward(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		saveOrUpdateHonors();
	}	
}
