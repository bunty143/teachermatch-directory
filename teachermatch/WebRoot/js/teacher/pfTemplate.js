var txtBgColor="#F5E7E1";
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}
function showGrid()
{
	//alert("")
	PFAcademics.getPFAcademinGrid( function(data)
	{
		document.getElementById("divDataGrid").innerHTML=data;
	});
}
function showForm()
{
	document.getElementById("divFormGrid").style.display="block";
}

function delRow(id)
{
	//alert(id)
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFAcademics.deletePFAcademinGrid(id, function(data)
		{
			showGrid();
			return false;
		});
	}
}
