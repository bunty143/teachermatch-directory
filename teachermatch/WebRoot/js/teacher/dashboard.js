var divFavTeacher = new Array("divServerError","divFavTeacherName","divFavTeacherStory");
var txtBgColor="#F5E7E1";
var selJBIStatus = "all";
var xmlHttp=null;

var page = 1;
var noOfRows = 50;
var sortOrderStr	=	"";
var sortOrderType	=	"";

var schoolIdsStore="";

var pageforSchool		=	1;
var noOfRowsSchool 		=	50;
var sortOrderStrSchool	=	"";
var sortOrderTypeSchool	=	"";


function defaultSet(){
	
	  page = 1;
	 noOfRows = 50;
	 sortOrderStr="";
	 sortOrderType="";
}


var pageZS = 1;
var noOfRowsZS = 10;
var sortOrderStrZS="";
var sortOrderTypeZS="";

function getPaging(pageno)
{	
	var gridNo	=	document.getElementById("gridNo").value;	
	if(gridNo==1)
	{
		if(pageno!='')
		{
			page=pageno;	
		}
		else
		{
			page=1;
		}
		noOfRows = document.getElementById("pageSize").value;
		//shadab commented below line and added new method to enable elasticsearch
		getJosOfIntrestGrid();
		//getJosOfIntrestGridES();
	}
	else if(gridNo==2)
	{
		if(pageno!='')
		{
			pageforSchool=pageno;	
		}
		else
		{
			pageforSchool=1;
		}		
		noOfRowsSchool = document.getElementById("pageSize1").value;
		showSchoolList(schoolIdsStore);
	}	
	else if(gridNo==3)
	{
		if(pageno!='')
		{
			pageZS=pageno;	
		}
		else
		{
			pageZS=1;
		}		
		noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
		displayZoneSchoolList();
	}	
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{	
	if(document.getElementById("gridNo") && document.getElementById("gridNo").value!=null)
	{
		var gridNo	=	document.getElementById("gridNo").value;
		if(gridNo==1)
		{
			if(pageno!=''){
				page=pageno;	
			}else{
				page=1;
			}
			sortOrderStr	=	sortOrder;
			sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize")!=null){
				noOfRows = document.getElementById("pageSize").value;
			}else{
				noOfRows=10;
			}
			//shadab commented below line and added new method to enable elasticsearch
			getJosOfIntrestGrid();
			//getJosOfIntrestGridES();
		}
		else if(gridNo==2)
		{
			if(pageno!=''){
				pageforSchool=pageno;	
			}else{
				pageforSchool=1;
			}
			sortOrderStrSchool	=	sortOrder;
			sortOrderTypeSchool	=	sortOrderTyp;
			if(document.getElementById("pageSize1")!=null){
				noOfRowsSchool = document.getElementById("pageSize1").value;
			}else{
				noOfRowsSchool=50;
			}
			showSchoolList(schoolIdsStore);
		}
		else if(gridNo==3)
		{
			if(pageno!=''){
				pageforSchool=pageno;	
			}else{
				pageforSchool=1;
			}
			sortOrderStrZS	=	sortOrder;
			sortOrderTypeZS	=	sortOrderTyp;
			if(document.getElementById("pageSizeZoneSchool")!=null){
				noOfRowsZS = document.getElementById("pageSizeZoneSchool").value;
			}else{
				noOfRowsZS=100;
			}
			displayZoneSchoolList();
		}
	}
		
	
	
	if(document.getElementById("gridNameFlag") && document.getElementById("gridNameFlag").value!=null)
	{
		var gridNameFlag = document.getElementById("gridNameFlag").value;
		if(gridNameFlag=="subjectAreas")
		{
			if(pageno!=''){
				dp_SubjectArea_page=pageno;	
			}else{
				dp_SubjectArea_page=1;
			}
			dp_SubjectArea_sortOrderStr	=	sortOrder;
			dp_SubjectArea_sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize1")!=null){
				dp_SubjectArea_noOfRows = document.getElementById("pageSize1").value;
			}else{
				dp_SubjectArea_noOfRows=10;
			}
			showGridSubjectAreasGrid();
		}
		else if(gridNameFlag=="additionalDocuments"){
			if(pageno!=''){
				dp_AdditionalDocuments_page=pageno;	
			}else{
				dp_AdditionalDocuments_page=1;
			}
			dp_AdditionalDocuments_sortOrderStr	=	sortOrder;
			dp_AdditionalDocuments_sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize1")!=null){
				dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize1").value;
			}else{
				dp_AdditionalDocuments_noOfRows=10;
			}
			showGridAdditionalDocuments();
		}else if(gridNameFlag=="academics"){
			if(pageno!=''){
				dp_Academics_page=pageno;	
			}else{
				dp_Academics_page=1;
			}
			dp_Academics_sortOrderStr	=	sortOrder;
			dp_Academics_sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize1")!=null){
				dp_Academics_noOfRows = document.getElementById("pageSize1").value;
			}else{
				dp_Academics_noOfRows=10;
			}
			showGridAcademics();
		}else if(gridNameFlag=="certification"){
			if(pageno!=''){
				dp_Certification_page=pageno;	
			}else{
				dp_Certification_page=1;
			}
			dp_Certification_sortOrderStr	=	sortOrder;
			dp_Certification_sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize3")!=null){
				dp_Certification_noOfRows = document.getElementById("pageSize3").value;
			}else{
				dp_Certification_noOfRows=10;
			}
			showGridCertifications();
		}else if(gridNameFlag=="reference"){
			if(pageno!=''){
				dp_Reference_page=pageno;	
			}else{
				dp_Reference_page=1;
			}
			dp_Reference_sortOrderStr	=	sortOrder;
			dp_Reference_sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize1")!=null){
				dp_Reference_noOfRows = document.getElementById("pageSize1").value;
			}else{
				dp_Reference_noOfRows=10;
			}
			getElectronicReferencesGrid();
		}else if(gridNameFlag=="workExperience"){
			if(pageno!=''){
				dp_Employment_page=pageno;	
			}else{
				dp_Employment_page=1;
			}
			dp_Employment_sortOrderStr	=	sortOrder;
			dp_Employment_sortOrderType	=	sortOrderTyp;
			if(document.getElementById("pageSize1")!=null){
				dp_Employment_noOfRows = document.getElementById("pageSize1").value;
			}else{
				dp_Employment_noOfRows=10;
			}
			getPFEmploymentDataGrid();
		}
	}
	
	
}
function showMiamiPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf", "Miami Geo Zone", "width=600, height=600,top="+top+",left="+left);
}
function getSortFirstGrid()
{
	$('#gridNo').val("1");
}

function getSortSecondGrid()
{
	$('#gridNo').val("2");
}


function checkSession(responseText)
{
	if(responseText=='100001')
	{
		alert(resourceJSON.oops)
		window.location.href="signin.do";
	}	
}
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
	else
		alert(resourceJSON.msgAjaxNotSupported);
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
/*=========== For Resolving IE TextArea Maxlength Problem ================*/
function chkLenOfTextArea(textValue,maxlength)
{
	var tlen=textValue.value.length;
	if(tlen>maxlength)
	{
		document.getElementById(textValue.id).value=document.getElementById(textValue.id).value.substr(0,maxlength)
	}
}

function showDiv(divId)
{
	var tempDivId;
	for(var j=0;j<divArray.length;j++)
	{
		tempDivId=document.getElementById(divArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}
function showDivMsg(divId,msgArray)
{

	var tempDivId;
	for(var j=0;j<msgArray.length;j++)
	{
		tempDivId=document.getElementById(msgArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}
function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}
function deleteData(delUrl)
{
	if(window.confirm("Are you sure to delete?"))
	{
		var url = delUrl+"&td="+new Date().getTime();
		window.location.href=url;
	}
}


function showHideBankDiv(stBank,stChequeDD,stButton)
{
	document.getElementById("divBankList").style.display=stBank;
	document.getElementById("divChequeDD").style.display=stChequeDD;
	document.getElementById("divPayButton").style.display=stButton;
}

function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert(resourceJSON.msgValueAllNumeric);
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}



function chkFavTeacher()
{
	var favTeacherName = document.getElementById("favTeacherName");
	var favTeacherStory = document.getElementById("favTeacherStory");

	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");

	$('#favTeacherName').css("background-color","");
	$('#favTeacherStory').css("background-color","");

	if(trim(favTeacherName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgEnterFavTeacher+"<br>");
		if(focs==0)
			$('#favTeacherName').focus();

		$('#favTeacherName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(favTeacherStory.value)=="")
	{
		$('#errordiv').append("&#149; "+msgEnterFavTeacherStory+"<br>");
		if(focs==0)
			$('#favTeacherStory').focus();

		$('#favTeacherStory').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function getDashboardJobsofIntrest()
{
	var url =window.location.href;
	if(url.indexOf("userdashboardnew.do") > -1){
	DashboardAjax.getJbofIntresGrid_Op({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#divJbInterest').html(data);
	    tpJbIntrestDashboardEnable();
		getDashboardJobsofIntrestCount();
	}
	});	
	}
	else{
		DashboardAjax.getJbofIntresGrid({ 
			async: true,
			errorHandler:handleError,
			callback: function(data){
			$('#divJbInterest').html(data);
			tpJbIntrestDashboardEnable();
			
		}
		});	
	}
	
}

function getDashboardJobsofIntrestCount()
{
  var url =window.location.href;
	if(url.indexOf("userdashboardnew.do") > -1){
	DashboardAjax.getJbofIntresGrid_OpCount({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#jobStatusCount').html(data);
		tpJbIntrestDashboardEnable();
	}
	});
	
  }
	}
function getDashboardJobsofIntrestAvailable()
{
 //  Optimized Job Of Intrest function
	var url =window.location.href;
	if(url.indexOf("userdashboardnew.do") > -1){
	DashboardAjax.getJbofIntresGridAvailable_Op({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		try
		{
		$('#divFavTeacher').html(data);
		tpJbIntrestDashboardEnable();
		}
		catch(e){alert(e)}
	}
	});	
	}
	else{
		DashboardAjax.getJbofIntresGridAvailable({ 
			async: true,
			errorHandler:handleError,
			callback: function(data){
			try
			{
			$('#divFavTeacher').html(data);
			tpJbIntrestDashboardEnable();
			}
			catch(e){alert(e)}
		}
		});	
		}
	
}
function getDashboardMilestones()
{ 
	var url =window.location.href;
	if(url.indexOf("userdashboardnew.do") > -1){
		DashboardAjax.getMilestonesGrid_Op({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#divMilestones').html(data);		
		powerProfile(document.getElementById('teacherID').value);
		tpJbIntrestDashboardEnable();
	}
	});
	}else
	{
		DashboardAjax.getMilestonesGrid({ 
			async: true,
			errorHandler:handleError,
			callback: function(data){
			$('#divMilestones').html(data);		
			powerProfile(document.getElementById('teacherID').value);
			tpJbIntrestDashboardEnable();
		}
		});
		
	}
}
function powerProfile(teacherId)
{
	 //alert(teacherId);
	var id=""; 
	id = teacherId;
	
	var url =window.location.href;
	if(url.indexOf("userdashboardnew.do") > -1){
	QuestAjax.powerTracker_Op(id,{
		async: true,
		callback: function(data){	
		try
		{
			document.getElementById('score').innerHTML = data;
		}
		catch(err)
		{
		}
		
		
	}
	});
	}else{
		QuestAjax.powerTracker(id,{
			async: true,
			callback: function(data){	
			try
			{
				document.getElementById('score').innerHTML = data;
			}
			catch(err)
			{
			}
			
			
		}
		});
	}
}

function getDashboardReport()
{
	
	var url =window.location.href;
	if(url.indexOf("userdashboardnew.do") > -1){
		DashboardAjax.getReportGrid_Op({ 
			async: true,
			errorHandler:handleError,
			callback: function(data){
			$('#divReport').html(data);
			tpJbIntrestDashboardEnable();
		}
		});	
	}
	else{
	DashboardAjax.getReportGrid({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#divReport').html(data);
		tpJbIntrestDashboardEnable();
	}
	});	
	}
}

function jbWithdraw(jobForTeacherId)
{
	createXMLHttpRequest();  
	//queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime();
	queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime()+"&mailSentFlag="+"No";
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				checkSession(xmlHttp.responseText)
				if(xmlHttp.responseText == 'hird'){
					$('#message2show').html(resourceJSON.msgYouHaveHired);
					$('#myModal2').modal('show');
				} else {
					$('#message2showConfirm').html(resourceJSON.cnfmsgWidrawApplicationNewMsg);
					$('#footerbtn').html("<button class='btn btn-primary' onclick=\"withdrawJob('"+jobForTeacherId+"')\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
					$("#withdrawnReasonDiv").show();
					$('#myModal3').modal('show');
				}
			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);
	return false;
	
}

function withdrawJob(jobForTeacherId)
{
	$('#myModal3').modal('hide');
	var withdrawnReasonId = $('#withdrawnReasonId').val();
	tpJbIntrestDashboardDisable();	
	createXMLHttpRequest();  
	//queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime();
	queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime()+"&mailSentFlag="+"Yes"+"&withdrawnReasonId="+withdrawnReasonId;
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				checkSession(xmlHttp.responseText)
				if(xmlHttp.responseText == 'hird'){
					$('#message2show').html(resourceJSON.msgYouHaveHired);
					$('#myModal2').modal('show');
				}
				getDashboardJobsofIntrest();
				getDashboardMilestones();
				getDashboardJobsofIntrestAvailable();
				tpJbIntrestDashboardEnable();

			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);		
	return false;
}

function jbHide(jobId)
{
	$('#message2showConfirm').html(resourceJSON.cnfMsgHideApp);
	$('#footerbtn').html("<button class='btn btn-primary' onclick=\"hideJob('"+jobId+"')\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#myModal3').modal('show');
}
var totalQQQuestions=0;
function getQQDistrictQuestion(jobId)
{	
	$('#errordiv4QQquestion').empty();
	AssessmentCampaignAjax.getQQDistrictSpecificQuestion(jobId,{
	async: true,
	errorHandler:handleError,
	callback: function(data)
	{
		document.getElementById("QQjobId").value="";
		var dataArray = data.split("@##@");	
		totalQQQuestions = dataArray[1];
		if(dataArray[0]!=null && dataArray[0]!='' && totalQQQuestions!=0)
		{
			try
			{								
				$('#tblQQGrid').html(dataArray[0]);
				$('#myModalDASpecificQuestionsQQ').modal('show');
				document.getElementById("QQjobId").value=jobId;
				
			}catch(err)
			{}
		}
		else
		{		
			$('#SuccessMesageforQQ').hide();
			$('#QuestionMesageforQQ').show();
			$('#QuestionNotAvailable').modal('show');
			
		}

	}});	
	
}
function setQQDistrictQuestions()
{
	$('#errordiv4QQquestion').empty();
	var arr =[];
	var jobOrder = {jobId:document.getElementById("QQjobId").value};
	for(i=1;i<=totalQQQuestions;i++)
	{	
		var districtSpecificQuestion = {questionId:dwr.util.getValue("Q"+i+"questionId")};
		var questionTypeShortName = dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionTypeMaster = {questionTypeId:dwr.util.getValue("Q"+i+"questionTypeId")};
		var qType = dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionSets = {ID:dwr.util.getValue("qqQuestionSetID")};
		if(qType=='tf' || qType=='slsel')
		{
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 )
			{
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			}else
			{
				$("#errordiv4QQquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				return;
			}
			
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificQuestions" : districtSpecificQuestion,
				"questionSets"	: questionSets,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
				"jobOrder" : jobOrder,
			});

		}else if(qType=='ml')
		{
			var insertedText = dwr.util.getValue("Q"+i+"opt");
			if(insertedText!=null && insertedText!="")
			{
				arr.push({ 
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"districtSpecificQuestions" : districtSpecificQuestion,
					"questionSets"	: questionSets,
					"jobOrder" : jobOrder
				});
			}else
			{
				$("#errordiv4QQquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				return;
			}
		}else if(qType=='et')
		{
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 )
			{
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			}else
			{
				$("#errordiv4QQquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				return;
			}
			
			var insertedText = dwr.util.getValue("Q"+i+"optet");
			var isValidAnswer = dwr.util.getValue("Q"+optId+"validQuestion");

			if(isValidAnswer=="false" && insertedText.trim()=="")
			{
				$("#errordiv4QQquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				return;
			}
				
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificQuestions" : districtSpecificQuestion,
				"questionSets"	: questionSets,
				"isValidAnswer" : isValidAnswer,
				"jobOrder" : jobOrder,
			});
		}
	}
	if(arr.length==totalQQQuestions)
	{
		AssessmentCampaignAjax.setQQDistrictQuestions(arr,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{				
				arr =[];
				$('#myModalDASpecificQuestionsQQ').modal('hide');
				$('#SuccessMesageforQQ').show();
				$('#QuestionMesageforQQ').hide();
				$('#QuestionNotAvailable').modal('show');
			}

		}
		});	
	}else
	{
		$("#errordiv4QQquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
		return;
	}

}
function hideJob(jobId)
{
		$('#myModal3').modal('hide');
		tpJbIntrestDashboardDisable();
		createXMLHttpRequest();  
		queryString = "jobhide.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{	
					checkSession(xmlHttp.responseText)
					getDashboardJobsofIntrest();
					getDashboardMilestones();
					getDashboardJobsofIntrestAvailable();
					tpJbIntrestDashboardEnable();

				}			
				else
				{
					alert(resourceJSON.msgServerErr);
				}
			}
		}
		xmlHttp.send(null);		
		return false;
}
function tpJbIntrestDashboardDisable()
{
	$('#tpJbStatus1').trigger('mouseout');
	$('#tpJbStatus2').trigger('mouseout');
	$('#tpJbStatus3').trigger('mouseout');
	$('#tpJbStatus4').trigger('mouseout');

	$('#tpWithdraw1').trigger('mouseout');
	$('#tpWithdraw2').trigger('mouseout');
	$('#tpWithdraw3').trigger('mouseout');
	$('#tpWithdraw4').trigger('mouseout');	

	$('#tpCoverLetter1').trigger('mouseout');
	$('#tpCoverLetter2').trigger('mouseout');
	$('#tpCoverLetter3').trigger('mouseout');
	$('#tpCoverLetter4').trigger('mouseout');
	
	$('#tpShare1').trigger('mouseout');
	$('#tpShare2').trigger('mouseout');
	$('#tpShare3').trigger('mouseout');
	$('#tpShare4').trigger('mouseout');

	$('#tpHide1').trigger('mouseout');
	$('#tpHide2').trigger('mouseout');
	$('#tpHide3').trigger('mouseout');
	$('#tpHide4').trigger('mouseout');

}

function tpJbIntrestDashboardEnable()
{
	$('#iconpophoverRe').tooltip();
	$('#iconpophoverBase').tooltip({
	        toggle: 'toolip',
	        placement: 'right'
	    });

	$('#iconpophover1').tooltip();
	$('#iconpophover2').tooltip();
	$('#iconpophover3').tooltip();
	$('#iconpophover4').tooltip();
	$('#iconpophover5').tooltip();
	$('#iconpophover6').tooltip();

	$('#tpApply1').tooltip();
	$('#tpApply2').tooltip();
	$('#tpApply3').tooltip();
	$('#tpApply4').tooltip();

	$('#tpWithdraw1').tooltip();
	$('#tpWithdraw2').tooltip();
	$('#tpWithdraw3').tooltip();
	$('#tpWithdraw4').tooltip();

	$('#tpCoverLetter1').tooltip();
	$('#tpCoverLetter2').tooltip();
	$('#tpCoverLetter3').tooltip();
	$('#tpCoverLetter4').tooltip();
	
	
	$('#tpJbStatus1').tooltip();
	$('#tpJbStatus2').tooltip();
	$('#tpJbStatus3').tooltip();
	$('#tpJbStatus4').tooltip();


	$('#tpShare1').tooltip();
	$('#tpShare2').tooltip();
	$('#tpShare3').tooltip();
	$('#tpShare4').tooltip();
	$('#tpShare5').tooltip();
	$('#tpShare6').tooltip();
	$('#tpShare7').tooltip();
	$('#tpShare8').tooltip();

	$('#tpHide1').tooltip();
	$('#tpHide2').tooltip();
	$('#tpHide3').tooltip();
	$('#tpHide4').tooltip();
	
	$('#QQ1').tooltip();
	$('#QQ2').tooltip();
	$('#QQ3').tooltip();
	$('#QQ4').tooltip();
	$('#QQ5').tooltip();
	$('#QQ6').tooltip();
	$('#QQ7').tooltip();
	$('#QQ8').tooltip();
	
	$('#JSI1').tooltip();
	$('#JSI2').tooltip();
	$('#JSI3').tooltip();
	$('#JSI4').tooltip();
	$('#JSI5').tooltip();
	$('#JSI6').tooltip();
	$('#JSI7').tooltip();
	$('#JSI8').tooltip();

}


function tpJbIEnable()
{
	var noOrRow = document.getElementById("tblGridJoF").rows.length;
try
{
	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpShare'+j).tooltip();
		$('#tpJbStatus'+j).tooltip();

		$('#iconpophover'+j).tooltip();
		$('#tpApply'+j).tooltip();
		$('#tpWithdraw'+j).tooltip();
		
		$('#tpCoverLetter'+j).tooltip();
		
		$('#tpHide'+j).tooltip();
		$('#tpCoverLetter'+j).tooltip();
		$('#QQ'+j).tooltip();
		$('#JSI'+j).tooltip();
	}
}
catch(e){}
}
function tpJbIDisable()
{
	var noOrRow = document.getElementById("tblGridJoF").rows.length;

	for(var j=1;j<=noOrRow;j++)
	{
		$('#tpJbStatus'+j).trigger('mouseout');

		$('#iconpophover'+j).trigger('mouseout');
		$('#tpApply'+j).trigger('mouseout');
		$('#tpCoverLetter'+j).trigger('mouseout');
		$('#tpWithdraw'+j).trigger('mouseout');
		$('#tpShare'+j).trigger('mouseout');
		$('#tpHide'+j).trigger('mouseout');
		$('#tpCoverLetter'+j).trigger('mouseout');
	}
}



function chkShareJob()
{	
	var fname1 = document.getElementById("yrname")
	var fname2 = document.getElementById("yremail")

	var fname1 = document.getElementById("fname1")
	var fname2 = document.getElementById("fname2")
	var fname3 = document.getElementById("fname3")
	var fname4 = document.getElementById("fname4")
	var fname5 = document.getElementById("fname5")

	var fremail1 = document.getElementById("fremail1")
	var fremail2 = document.getElementById("fremail2")
	var fremail3 = document.getElementById("fremail3")
	var fremail4 = document.getElementById("fremail4")
	var fremail5 = document.getElementById("fremail5")


	var name="";
	var email="";
	var noOfUser=0;

	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	$('#yrname').css("background-color","");
	$('#yremail').css("background-color","");
	$('#fname1').css("background-color","");
	$('#fname2').css("background-color","");
	$('#fname3').css("background-color","");
	$('#fname4').css("background-color","");
	$('#fname5').css("background-color","");
	$('#fremail1').css("background-color","");
	$('#fremail2').css("background-color","");
	$('#fremail3').css("background-color","");
	$('#fremail4').css("background-color","");
	$('#fremail5').css("background-color","");

	for(var j=1;j<=5;j++)
	{	
		name=document.getElementById("fname"+j).value;
		email=document.getElementById("fremail"+j).value;
		if(trim(name)!="" ||  trim(email)!="")
		{
			noOfUser++;	
		}
	}

	if(trim(yrname.value)=="")
	{

		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseEnterName+"<br>");
		if(focs==0)
			$('#yrname').focus();

		$('#yrname').css("background-color",txtBgColor);
		cnt++;focs++;
	}


	if(trim(yremail.value)=="")
	{

		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseEnterEmailAdd
+"<br>");
		if(focs==0)
			$('#yremail').focus();

		$('#yremail').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(trim(yremail.value)))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.msgValidEmail+"<br>");
		if(focs==0)
			$('#yremail').focus();

		$('#yremail').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if(noOfUser==0)
	{
		$('#errordiv').append("&#149; "+ resourceJSON.msgFrendsNameEmail+" <br>");
		if(focs==0)
			$('#fname1').focus();

		$('#fname1').css("background-color",txtBgColor);
		$('#fremail1').css("background-color",txtBgColor);
		cnt++;focs++;
	}

	if((trim(fname1.value)!=="")||(trim(fremail1.value)!==""))
	{
		if(trim(fname1.value)=="")
		{

			$('#errordiv').append("&#149; "+resourceJSON.msgFrendsname+"<br>");
			if(focs==0)
				$('#fname1').focus();

			$('#fname1').css("background-color",txtBgColor);
			cnt++;focs++;
		}

		if(trim(fremail1.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgFrendEmail+"<br>");
			if(focs==0)
				$('#fremail1').focus();

			$('#fremail1').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(!isEmailAddress(trim(fremail1.value)))
		{		
			$('#errordiv').append("&#149; "+resourceJSON.msgFrendFirstEmail+"<br>");
			if(focs==0)
				$('#fremail1').focus();

			$('#fremail1').css("background-color",txtBgColor);
			cnt++;focs++;
		}		
	}

	if((trim(fname2.value)!=="")||(trim(fremail2.value)!==""))
	{
		if(trim(fname2.value)=="")
		{			
			$('#errordiv').append("&#149; "+resourceJSON.msgFrendSecondName+"<br>");
			if(focs==0)
				$('#fname2').focus();

			$('#fname2').css("background-color",txtBgColor);
			cnt++;focs++;
		}

		if(trim(fremail2.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgFrendSecongEmail+"<br>");
			if(focs==0)
				$('#fremail2').focus();

			$('#fremail2').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(!isEmailAddress(trim(fremail2.value)))
		{		
			$('#errordiv').append("&#149; "+resourceJSON.msgValidSeconfFriendEmail+"<br>");
			if(focs==0)
				$('#fremail2').focus();

			$('#fremail2').css("background-color",txtBgColor);
			cnt++;focs++;
		}	
	}

	if((trim(fname3.value)!=="")||(trim(fremail3.value)!==""))
	{
		if(trim(fname3.value)=="")
		{			
			$('#errordiv').append("&#149; "+resourceJSON.msgThirdFrendName+"<br>");
			if(focs==0)
				$('#fname3').focus();

			$('#fname3').css("background-color",txtBgColor);
			cnt++;focs++;
		}

		if(trim(fremail3.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgThirdFrendEmail+"<br>");
			if(focs==0)
				$('#fremail3').focus();

			$('#fremail3').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(!isEmailAddress(trim(fremail3.value)))
		{		
			$('#errordiv').append("&#149; "+resourceJSON.msgValidThirdFrendEmail+"<br>");
			if(focs==0)
				$('#fremail3').focus();

			$('#fremail3').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	}

	if((trim(fname4.value)!=="")||(trim(fremail4.value)!==""))
	{
		if(trim(fname4.value)=="")
		{			
			$('#errordiv').append("&#149;"+resourceJSON.msg4FriendName+"<br>");
			if(focs==0)
				$('#fname4').focus();

			$('#fname4').css("background-color",txtBgColor);
			cnt++;focs++;
		}

		if(trim(fremail4.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msg4FriendEmail+"<br>");
			if(focs==0)
				$('#fremail4').focus();

			$('#fremail4').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(!isEmailAddress(trim(fremail4.value)))
		{		
			$('#errordiv').append("&#149; "+resourceJSON.msg4FrendValidEmail+"<br>");
			if(focs==0)
				$('#fremail4').focus();

			$('#fremail4').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	}

	if((trim(fname5.value)!=="")||(trim(fremail5.value)!==""))
	{
		if(trim(fname5.value)=="")
		{			
			$('#errordiv').append("&#149; "+resourceJSON.msg5FrendName+"<br>");
			if(focs==0)
				$('#fname5').focus();

			$('#fname5').css("background-color",txtBgColor);
			cnt++;focs++;
		}

		if(trim(fremail5.value)=="")
		{
			$('#errordiv').append("&#149; "+resourceJSON.msg5FrendEmail+"<br>");
			if(focs==0)
				$('#fremail5').focus();

			$('#fremail5').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		else if(!isEmailAddress(trim(fremail5.value)))
		{		
			$('#errordiv').append("&#149; "+resourceJSON.msg5ValidFrendEmail+"<br>");
			if(focs==0)
				$('#fremail5').focus();

			$('#fremail5').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	}

	if(cnt==0)
	{	
		$('#loadingDiv').show();
		return true;
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}



function testTP()
{
	document.getElementById("tpWithdraw1").focus();
}

function chkBaseInventoryStatus(prefStatus, portfolioStatus)
{
	if(!prefStatus)
	{
		alert(resourceJSON.msgCompleteJobPref)
	}
	else if(!portfolioStatus)
	{
		alert(resourceJSON.msgCompletePortfolio)
	}else
		openEpiInfo();
}

function chkJobSpecificInventory(prefStatus, portfolioStatus,jobId)
{
	if(!prefStatus)
	{
		alert(resourceJSON.msgCompleteJobPref
)
	}
	else 
	if(!portfolioStatus)
	{
		alert(resourceJSON.msgCompletePortfolio)
	}
	/*alert("prefStatus:: "+prefStatus);
	alert("portfolioStatus:: "+portfolioStatus);
	callDynamicPortfolio_externalJobApply(1);
	//getDistrictQuestionsSet(jobId);
	
	document.getElementById("jobId").value=jobId;
	document.getElementById("savejobFlag").value=1;
	
	checkTeacherCriteria(jobId);*/
	
}

function getJosOfIntrestGrid()
{
	$('#loadingDiv').show();
	
	
	/*var searchBy = document.getElementById("selJBIStatus").value;
	var geoZoneId = document.getElementById("zone").value;
	var districtId = document.getElementById("districtId").value;
	var schoolId = document.getElementById("schoolId1").value;
	var stateId = document.getElementById("stateId").value;
	var cityId = document.getElementById("cityId").value;
	var subjectId = document.getElementById("subjectId").value;	*/
	var dt=new Date().getTime();
	
	
	var searchBy="";
	try {searchBy=document.getElementById("selJBIStatus").value;} catch (e) {	}
	var geoZoneId="";
	try {geoZoneId=document.getElementById("zone").value;} catch (e) {	}
	var districtId="";
	try {districtId=document.getElementById("districtId").value;} catch (e) {	}
	var schoolId="";
	try {schoolId=document.getElementById("schoolId1").value;} catch (e) {	}
	var stateId="";
	try {stateId=document.getElementById("stateId").value;} catch (e) {	}
	var cityId="";
	try {cityId=document.getElementById("cityId").value;} catch (e) {	}
	var subjectId="";
	try {subjectId=document.getElementById("subjectId").value;} catch (e) {	}
	
	var subjectId = document.getElementById("subjectId");
    var selected1 = [];
    for (var i = 0; i < subjectId.length; i++) {
    	if(subjectId.options[i].value!=0){
    		if (subjectId.options[i].selected) selected1.push(subjectId.options[i].value);
    	}
    }
    var url =window.location.href;
	if(url.indexOf("jobsofinterestnew.do") > -1){
		DashboardAjax.getJobsOfInterestAjaxOp(searchBy,noOfRows,page,sortOrderStr,sortOrderType,dt,districtId,schoolId,stateId,cityId,selected1,geoZoneId,
		{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data!=null && data!="")
				{
					document.getElementById("divJobsOfIntrest").innerHTML=data;
					applyScrollOnTbl();				
					tpJbIEnable();			
					$('#loadingDiv').hide();
				}
				else
				{
					$('#loadingDiv').hide();
				}
			}
		});	
	}else{
		DashboardAjax.getJobsOfInterestAjax(searchBy,noOfRows,page,sortOrderStr,sortOrderType,dt,districtId,schoolId,stateId,cityId,selected1,geoZoneId,
				{ 
					async: true,
					errorHandler:handleError,
					callback: function(data)
					{
						if(data!=null && data!="")
						{
							document.getElementById("divJobsOfIntrest").innerHTML=data;
							applyScrollOnTbl();				
							tpJbIEnable();			
							$('#loadingDiv').hide();
						}
						else
						{
							$('#loadingDiv').hide();
						}
					}
				});	
	}
}

function getJbsIWithdraw(jobForTeacherId)
{
	//alert("aaaa"+jobForTeacherId);
	createXMLHttpRequest();  
	//queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime();
	queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime()+"&mailSentFlag="+"No";
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				checkSession(xmlHttp.responseText)
				if(xmlHttp.responseText == 'hird'){
					$('#message2show').html(resourceJSON.msgYouHaveHired);
					$('#myModal2').modal('show');
				} else {
					$('#message2showConfirm').html(resourceJSON.cnfmsgWidrawApplicationNewMsg);
					$('#footerbtn').html("<button class='btn btn-primary' onclick=\"withdrawJobJOI('"+jobForTeacherId+"')\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
					$("#withdrawnReasonDiv").show();
					$('#myModal3').modal('show');
				}
			}
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);
	return false;

}

function withdrawJobJOI(jobForTeacherId)
{
		$('#myModal3').modal('hide');
		var withdrawnReasonId = $('#withdrawnReasonId').val();
		tpJbIDisable();
		createXMLHttpRequest();  
		 //queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime();
	    //queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime()+"&mailSentFlag="+"Yes";
		queryString = "jobwithdraw.do?jobForTeacherId="+jobForTeacherId+"&dt="+new Date().getTime()+"&mailSentFlag="+"Yes"+"&withdrawnReasonId="+withdrawnReasonId;
		xmlHttp.open("POST", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					checkSession(xmlHttp.responseText)	
					getJosOfIntrestGrid();
				}			
				else
				{
					alert(resourceJSON.msgServerErr);
				}
			}
		}
		xmlHttp.send(null);		
		return false;
}

function getJbsIHide(jobId)
{	
	$('#message2showConfirm').html(resourceJSON.cnfMsgHideApp);
	$('#footerbtn').html("<button class='btn btn-primary' onclick=\"hideJobJOI('"+jobId+"')\" >"+resourceJSON.btnOk+"</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnCancel+"</button>");
	$('#myModal3').modal('show');
}
function hideJobJOI(jobId)
{
		$('#myModal3').modal('hide');
		tpJbIDisable();
		createXMLHttpRequest();  
		queryString = "jobhide.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					checkSession(xmlHttp.responseText)
					getJosOfIntrestGrid();				
				}			
				else
				{
					alert(resourceJSON.msgServerErr);
				}
			}
		}
		xmlHttp.send(null);		
		return false;
}

function serachByBtn(){
	defaultSet();
	//selJBIStatus = document.getElementById("selJBIStatus").value;
	getJosOfIntrestGrid()
}

function getJBIbyStatus()
{
	selJBIStatus = document.getElementById("selJBIStatus").value;
//shadab commented below line and added new method to enable elasticsearch
	
	getJosOfIntrestGrid();
	//getJosOfIntrestGridES();
}

function chkApplyJob(jobId)
{
	
	
//For Martin District Hide Div Id Before Click Apply Job
	
	var districtId = document.getElementById("dID").value;
	
	if(districtId == 1201290)
	{
		$('#currentEmployeeOfMartin').hide();
		$('#formalEmployeeOfMartin').hide();
		$('#retiredEmployeeStateofFlorida').hide();
		$('#currentSubstituteEmployee').hide();
		//Remove Error Message  Before Click Apply Job
		$('#errordivMartin').html('');
		$('#errordivMartin').empty();
		
		 $("#fromalempjobtitle").css("background-color", "");
		 $("#formalempdatesofemployee").css("background-color", "");
		 $("#formalempnamewhileemployee").css("background-color", "");
		

			 $("#retiredempdateofretirement").css("background-color", "");
			

			 $("#currentempjobtitle").css("background-color", "");
			 $("#currentemplocation").css("background-color", "");
			 $("#currentempyearinlocation").css("background-color", "");
			 $("#currentempsupervisor").css("background-color", "");

			 $("#currentsubstitutejobtitle").css("background-color", "");
			 
			 
			 
			 		$("#fromalempjobtitle").val("");
			 		$("#formalempdatesofemployee").val("");
			 		$("#formalempnamewhileemployee").val("");
			 		

					$("#retiredempdateofretirement").val("");
				
					$("#currentempjobtitle").val("");
					$("#currentemplocation").val("");
					$("#currentempyearinlocation").val("");
					$("#currentempsupervisor").val("");

					$("#currentsubstitutejobtitle").val("");
				
		 
		 
		$('input:radio[name=isAffilated]:checked').attr('checked', false);
	}
	
	//for Jeffco
	callSetDayAndMonth($('[name="dID"]').val());
	
	if($('[name="dID"]').val()!=null && $('[name="dID"]').val()==3702970)
	{
		window.location.href="https://ats4.searchsoft.net/ats/job_board?softsort=NAME&APPLICANT_TYPE_ID=00000001&COMPANY_ID=00014342";
		return false;
	}
	var districtId = document.getElementById("dID").value;	
	if(districtId == 7800294)
	{
		$("#fe5Div").hide();
		$("#fe3Div").hide();
		$("#nameDiv").hide();
		$("#positionfe2Div").hide();
		$("#locationfe2Div").hide();
		$("#fePosDiv").hide();
		$("#districtEmailDiv").hide();
		$('#districtEmail').css("background-color","");
		$('#emppos').css("background-color","");
		$('input:radio[name=isAffilated]:checked').attr('checked', false);
	}
	document.getElementById("isAffilated").checked=false;
	$("#inst").hide();
	$("#noninst").hide();
	$("#partinst").hide();
	
	var radioChecked = $('input:radio[name=staffType]').is(':checked');
	if(radioChecked)
		$('input:radio[name=staffType]:checked').attr('checked', false);
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();

	createXMLHttpRequest();
	
	queryString = "chkJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
	
	xmlHttp.open("GET", queryString, false);
	
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{
				var msgStatus = xmlHttp.responseText; 
				if(msgStatus==4){					
					$('#divAlert').modal('show');
					cnt++;focs++;
				}
				else if(msgStatus==1){
					$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
					if(focs==0)
						$('#fremail5').focus();

					$('#fremail5').css("background-color",txtBgColor);
					cnt++;focs++;
				}
				else if(msgStatus==2){
					$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
					if(focs==0)
						$('#fremail5').focus();

					$('#fremail5').css("background-color",txtBgColor);
					cnt++;focs++;
				}
				else if(msgStatus==3){
					$('#divJobAlert').modal('show');
					//$('#errordiv').append("&#149; Job has been expired.<br>");
					if(focs==0)
						$('#fremail5').focus();

					$('#fremail5').css("background-color",txtBgColor);
					cnt++;focs++;
				}
				else{

				}

			}			
			else
			{
				alert(resourceJSON.msgServerErr);
			}
		}
	}
	xmlHttp.send(null);
	if(cnt==0)
	{	
		DSPQServiceAjax.callSelfServiceApplicationFlowOuter(jobId,'E',{ 
		async: false,
		callback: function(data)
		{
			if(data!=null && data=="NewSelfService")
			{
				
				window.location.href="newjafexternal.do";
			}
			else
			{
				if(isCoverLetterNeeded)
				{
					setCoverLetter();
					setClBlank();
					setCLEnable();
				}
				// Start .... dynamic portfolio for external job
				if(jobId > 0)
				{
					var candidateType="E";
					DistrictPortfolioConfigAjax.getPortfolioConfigByJobIdForExternal(jobId,candidateType,{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
							if(data!=null)
							{ 
								if(isCoverLetterNeeded)
								{
									if(data.districtMaster!=null && data.districtMaster.districtId==3628590)
									{
										data.coverLetter=0;
									}
									if(data.coverLetterOptional!=null && data.coverLetterOptional==false){
										data.coverLetter=0;
									}
									if(data.headQuarterMaster!=null){
										$("#headQuaterIdForDspq").val(data.headQuarterMaster.headQuarterId);
									if(data.headQuarterMaster.headQuarterId==1){
										data.coverLetter=0;
										if($("#myModalCL #myModalLabel").html()=="Cover Letter"){
											$("#myModalCL #myModalLabel").html("Kelly Educational Staffing");
										}
										$("#forKellyCvrLtr").hide();
										$("#kellYcvRLtrMain").show();
										$("#kellYnxtQ").hide();
										$("#kellYnxtInst").hide();
										$("#kellYnxtDisDivInf").hide();
										$("#forAllDistCvrLtr").show();
										$("#workForKelly2").prop("checked",false);
										$("#workForKelly1").prop("checked",false);
										$("#contactedKelly1").prop("checked",false);
										$("#contactedKelly2").prop("checked",false);
										$(".forAllDistCvrLtr").hide();
										$(".forKellyCvrLtr").show();										
										$(".continueBtnNxt").hide();
										//$("#headQuaterIdForDspq").val(data.headQuarterMaster.headQuarterId);
									}
									}
									
									if(data.districtMaster!=null &&  data.districtMaster.districtId==4218990 && data.isNonTeacher==true && data.coverLetter==1){
										$('#cvrltrTxt').hide();
										$('.philNT').hide();
										$('.philadelphiaNTCss').show();
										$('#isnontj').val(true);										
										//data.coverLetter=0;
									}else if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isSchoolSupportPhiladelphia==true && data.coverLetter==1){
										data.coverLetter=0;
										$('#isSchoolSupportPhiladelphia').val("1");
										$('#cvrltrTxt').hide();
										$('.philNT').hide();
										$('.philadelphiaNTCss').show();																				
										
									}else{
										$('#cvrltrTxt').hide();
									}
									
									if(data.districtMaster!=null && data.districtMaster.districtId==804800 && (data.jobCategoryName.indexOf("Hourly") !=-1 || data.jobCategoryName=="Substitute Teacher"))
									{
										data.coverLetter=0;
									}
									
									if(data.districtMaster!=null && data.districtMaster.districtId==804800 && data.jobCategoryName=="Administrator/Protech")
									{
										$("#expectedSalaryDiv").show();
									}
									if(data.districtMaster!=null && data.districtMaster.districtId==3700112){
										data.coverLetter=0;
										$("#expectedSalaryDiv").show();
									}

									$("#jeffcoSeachDiv").hide();
									if(data.districtMaster!=null && data.districtMaster.districtId==804800){
										$("#jeffcoSeachDiv").show();
									}

									if(data.coverLetter==1){
										document.getElementById("noCoverLetter").style.display="none";
									}
									else{
										document.getElementById("noCoverLetter").style.display="block";						
										document.getElementById("rdoCL2").checked="checked";
										
										$('#divCoverLetter').find(".jqte_editor").html("");
										document.getElementById("divCoverLetter").style.display="none";
			
										$('#errordivCL').empty();
										$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
									
								}
									if(data.jobCategoryName == 'Administrative Non-Instructional' || data.jobCategoryName == 'Administrative-Instructional')
									{
										if(data.coverLetter==1){
											
											document.getElementById("rdoCL2ForMartinAdmin").checked="checked";
											
											$('#divCoverLetterAdmin').find(".jqte_editor").html("");
											document.getElementById("divCoverLetterAdmin").style.display="block";
											
											$('#errordivCL').empty();
											$('#divCoverLetterAdmin').find(".jqte_editor").css("background-color", "");
										}
										
										else{
											
											document.getElementById("rdoCL2ForMartinAdmin").checked="checked";
											
											$('#divCoverLetterAdmin').find(".jqte_editor").html("");
											document.getElementById("divCoverLetterAdmin").style.display="block";
											
											$('#errordivCL').empty();
											$('#divCoverLetterAdmin').find(".jqte_editor").css("background-color", "");
										}
										
									}
									
								}else
								{
									$('#applyTeacherJob').submit();
								}
							}else
								$('#applyTeacherJob').submit();
						}
					});
				}
				// End .... dynamic portfolio for external job
			}
			
		},
		errorHandler:handleError  
		});
		
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}


function setCoverLetter(){
	document.getElementById("divCoverLetter").style.display="block";
	$('#myModalCL').modal('show');
	$('#divCoverLetter').find(".jqte_editor").focus();
	document.getElementById("rdoCL1").checked=true;

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
	
	$('#iconpophover101').tooltip();
	
}
function setCLEnable(){
	

	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="block";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}
function setClBlank(){
	
	
	$('#divCoverLetter').find(".jqte_editor").html("");
	document.getElementById("divCoverLetter").style.display="none";

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
}
/* ===== Gagan : It will call for external job apply from Job board url =====*/
function checkCL(){
	
	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
	$("#zipCodeCoverLtr").css("background-color", "");
	$("#empNumCoverLtr").css("background-color", "");
	var cnt=0;
	var focs=0;
	var ssn= $("#currentEmpSSN").val();
	
	//Dhananjay Verma For martin District.
	
	if($("#districtId").length >0 && $("#districtId").val()==1201290 ){
		
		document.getElementById("rdoCL1").checked=false;
		//document.getElementById("isAffilated").checked=false;
		
	}
	
	
	if(document.getElementById("rdoCL1").checked==true && $("#districtId").val()!=806900)
	{
		if ($('#divCoverLetter').find(".jqte_editor").text().trim()==""){
			
			$('#errordivCL').append("&#149; "+resourceJSON.msgTypeCoverLtr+"<br>");
			if(focs==0)
				$('#divCoverLetter').find(".jqte_editor").focus();
			$('#divCoverLetter').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;			
		}
	}
	
	if($("#districtId").length >0 && ($("#districtId").val()==4218990 || $("#districtId").val()==806810 || $("#districtId").val()==7800294) || $("#districtId").val()==1201290 ){
		
	}else{
	var staffType = $('input:radio[name=staffType]:checked').val();
	if(document.getElementById("isAffilated").checked==true && staffType==undefined)
	{
		$('#errordivCL').append("&#149; "+resourceJSON.msgStaffMemtype+"<br>");
		cnt++;
	}
}
	
if(document.getElementById("districtId").value==804800 && document.getElementById("isAffilated").checked==true){
		
		var cvrZip = $("#zipCodeCoverLtr").val().trim();
		var cvrEmpNum = $("#empNumCoverLtr").val().trim();
		if(cvrEmpNum==""){
			$('#errordivCL').append("&#149; Please provide employee number.<br>");		
			$("#empNumCoverLtr").css("background-color", "#F5E7E1");
				cnt++;focs++;
		}
		if(cvrZip==""){
			$('#errordivCL').append("&#149; Please provide Zip Code.<br>");	
			$("#zipCodeCoverLtr").css("background-color", "#F5E7E1");
				cnt++;focs++;
		}	
	}
	if(cnt==0 && document.getElementById("isMiami").value=="true" && $('#jCat').val()!="Aspiring Assistant Principal")
	{
		if(staffType=="I")
		{
			var msg= resourceJSON.msgCurrEmpOfMiami  +" <a href='http://jobs.dadeschools.net/teachers/Index.asp'>http://jobs.dadeschools.net/teachers/Index.asp</a>";
			$("#notApplyMsg").html(msg);
			$("#perform").val("RD");
			try{$('#jobApplyOrNot').modal('show');}catch(e){}
			try{$('#myModalCL').modal('hide');}catch(e){}
			doNotApplyJob();
			cnt++;
		}
	}
	if(document.getElementById("isAffilated").checked==true &&(ssn==null || ssn=="") && (document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nc.teachermatch.org"))
	{
		$('#errordivCLForSSN').empty();
		$('#errordivCLForSSN').append("&#149; Please enter SSN.<br>");	
		$("#errordivCLForSSN").css("background-color", "#F5E7E1");
			cnt++;focs++;
	}
	else if(document.getElementById("isAffilated").checked==true &&(ssn.length!=9) && (document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nc.teachermatch.org"))
	{
		$('#errordivCLForSSN').empty();
		$('#errordivCLForSSN').append("&#149; Please enter Exactly 9 number.<br>");	
		$("#errordivCLForSSN").css("background-color", "#F5E7E1");
			cnt++;focs++;
	}
	//isAffilated
	
	if(cnt==0){
		return true;
	}
	return false;
}


///////////// Cover Letter JOI ////////////////////
function getCoverLetter(jobForTeacherId){
	document.getElementById("divCoverLetter").style.display="block";
	$('#myModalCL').modal('show');

	document.getElementById("hiddenJobForTeacherId").value=jobForTeacherId;

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
	
	$('#iconpophover101').tooltip();
	
	DashboardAjax.getCoverLetter(jobForTeacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		$('#divCoverLetter').find(".jqte_editor").html(data);
		$('#divCoverLetter').find(".jqte_editor").focus();
		
	}
	});	
}
/////////////////////////////////////////////////////////
function saveCoverLetter()
{
	var jobForTeacherId = $("#hiddenJobForTeacherId").val();

	$('#errordivCL').empty();
	$('#divCoverLetter').find(".jqte_editor").css("background-color", "");
	var cnt=0;
	var focs=0;
	if ($('#divCoverLetter').find(".jqte_editor").text().trim()==""){
		$('#errordivCL').append("&#149; "+resourceJSON.msgTypeCoverLtr+"<br>");
		if(focs==0)
		{
			$('#divCoverLetter').find(".jqte_editor").focus();
			$('#divCoverLetter').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}			
	}else
	{
		var covermsg = $('#divCoverLetter').find(".jqte_editor").html();
		DashboardAjax.saveCoverLetter(jobForTeacherId,covermsg,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data==1)
			{
				$('#myModalCL').modal('hide');
				$('#message2show').html(resourceJSON.msgCoverLtrSave);
				$('#myModal2').modal('show');
			}
			
		}
		});	
	}

}

function getStateBeforeWithdraw(jobForTeacherId){

	DashboardAjax.getStateBeforeWithdraw(jobForTeacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		var msg = ""; 
		if(data==1)
			msg = resourceJSON.msgreinstatedButIncomp;
		else if(data==2)
			msg = resourceJSON.msgAppresubmitted;
		else if(data==3)
			msg = resourceJSON.msgAppresubmittedTimeout;
		
		$('#message2show').html(msg);
		$('#myModal2').modal('show');
		getJosOfIntrestGrid();

	}
	});	
}

function unHideJob(jobForTeacherId){
	DashboardAjax.unHideJob(jobForTeacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		var msg = ""; 
		
		if(data==1)
			msg = resourceJSON.msgNowjobAvail;
		
		$('#message2show').html(msg);
		$('#myModal2').modal('show');
		getJosOfIntrestGrid();
	}
	});	
}

function __testTP()
{
	$('h1').attr('id', 'element');

	$('#element').tooltip({
		title: 'My first tooltip'
	}); // Add the tooltip

	$('#element').tooltip('show'); // Show the tooltip

	$('#element').tooltip({
		title: 'I want to change this tooltip'
	}); // Try to change the tooltip

	$('#element').tooltip('show'); // The tooltip will still say 'My first tooltip'

	/****************************/

	$('#element').data('tooltip', false); // Remove the previous tooltip

	$('#element').tooltip({
		title: 'This is the new tooltip'
	}); // Try to change the tooltip

	$('#element').tooltip('show'); // The tooltip should say 'This is the new tooltip'
}

function displayStateData(){
	
	DWRAutoComplete.displayStateData({ 
		async: false,		
		callback: function(data){
			document.getElementById("stateDivId").innerHTML=data;
			getJBIbyStatus();
		}
	});
}

function showSchool()
{
	document.getElementById("schoolName1").value="";
	document.getElementById("schoolId1").value="";
	
	var districtId = $("#districtId").val();
	
	if(districtId.length>0)
	{	
		document.getElementById("schoolName1").disabled=false;
	}else{
		document.getElementById("schoolName1").disabled=true;
	}
}





function showSchoolList(schoolIds)
{	
	schoolIdsStore=schoolIds;
	DWRAutoComplete.displaySchoolList(schoolIds,noOfRowsSchool,pageforSchool,sortOrderStrSchool,sortOrderTypeSchool,{ 
		async: false,		
		callback: function(data)
		{
			$('#myModalforSchhol').modal('show');
			document.getElementById("schoolListDiv").innerHTML=data;
			applyScrollOnTb();
		}
	});	
}




function getSubjectByDistrict()
{
	var districtId = document.getElementById("districtId").value;
    DWRAutoComplete.getSubjectByDistrict(districtId,{ 
		async: false,		
		callback: function(data)
		{
			document.getElementById("subjectId").innerHTML=data;
		}
	});
}


function getAllZone()
{
	var districtId = document.getElementById("districtId").value;
	if(districtId!=null && districtId!="")
	{
		document.getElementById("stateId").disabled=true;
	}
	else
	{
		document.getElementById("stateId").disabled=false;
	}
	//alert("u got it"+districtId);
	ManageJobOrdersAjax.getZoneListAll(districtId,{ 
		async: false,		
		callback: function(data)
		{
			if(data!=null && data!="")
			{			
				$("#zone").html(data);
				$("#zone").prop('disabled', false);
				
				
			}
			else{
				$("#zone").html(data);
				$("#zone").prop('disabled', true);
			
			}
		
		}
	});
}

/*mathod for Anchor tag on Zone*/
var DistrictId;
function displayZoneSchoolList()
{
	var geozoneId=$('#geozoneId').val();
	delay(1000);
	ManageJobOrdersAjax.displayGeoZoneSchoolGrid(geozoneId,DistrictId,noOfRowsZS,pageZS,sortOrderStrZS,sortOrderTypeZS,{ 
		async: true,
		callback: function(data){
		$('#gridNo').val("3");
		$('#geozoneId').val(geozoneId);
		$('#geozoneschoolFlag').val(1);
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();	
	},
	errorHandler:handleError 
	});
}

function showZoneSchoolList(geozoneId,geozoneName,districtId)
{	
	DistrictId=districtId;
	$("#zoneName").html(geozoneName);
	delay(1000);
	$('#loadingDiv').show();
	ManageJobOrdersAjax.displayGeoZoneSchoolGrid(geozoneId,DistrictId,noOfRowsZS,pageZS,sortOrderStrZS,sortOrderTypeZS,{ 
		async: true,
		callback: function(data){
		$('#gridNo').val("3");
		$('#geozoneId').val(geozoneId);
		$('#geozoneschoolFlag').val(1);
		$("#geozoneschoolDiv").modal('show');
		$("#geozoneschoolGrid").html(data);
		applyScrollOnTblGeoZoneSchool();
		$('#loadingDiv').hide();
	},
	errorHandler:handleError 
	});
}
function doNotApplyJob()
{
	DistrictPortfolioConfigAjax.donotApplyJob({ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			
		}
	});
}




function getZoneforTeacher()
{
	var districtId=document.getElementById("districtId").value;
	ManageJobOrdersAjax.getZoneListforTeacher(districtId,{ 
		async: false,		
		callback: function(data)
		{
			if(data!=null && data!="")
			{			
				$("#zone").html(data);
				$("#zone").prop('disabled', false);
			}
			else{
				$("#zone").html(data);
				$("#zone").prop('disabled', true);
			}
		
		}
	});
}


function displayUSAStateData(countryid){
	DWRAutoComplete.displayUSAStateData(countryid,{ 
		async: false,		
		callback: function(data){
			document.getElementById("stateDivId").innerHTML=data;
			//getJBIbyStatus();
		}
	});
}

function checkInventoryStatus(teacherId)
{
	var id="";
	id = teacherId;
	try{
		QuestAjax.powerTracker(id,{
			async: false,
			callback: function(data){
			if(data>75)
			 {			
				document.getElementById("hidea").style.display = "none";
				document.getElementById("show").style.display = "block";
				document.getElementById("jobseekertext").style.display = "none";
														
			 }
			else
			 {
				document.getElementById("hidea").style.display = "block";
				document.getElementById("show").style.display = "none";
				document.getElementById("jobseekertext").style.display = "block";
			
		      }		
		}
		}		
	);	}catch(e){alert(e)}
}

function applyteacherjobfromquest(jobId)
{	
	var cnt=0;
	var focs=0;	
	//$('#errordiv').empty();
	var msgStatus ="";
	createXMLHttpRequest();  
	queryString = "applyteacherjobfromquest.do?jobId="+jobId+"&dt="+new Date().getTime();
	xmlHttp.open("GET", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{			
			if(xmlHttp.status==200)	
			{
				msgStatus = xmlHttp.responseText; 				
			}			
		}
	}
	xmlHttp.send(null);
	return msgStatus;
}
function jobApplicationHistory(jobID,districtID,exitURL,schoolID)
{
	QuestAjax.jobApplicationHistory(jobID,districtID,exitURL,schoolID,{ 
		async: true,
		callback: function(data){								
		if(data=="")
		{	
			$('#loadingDiv').hide();
			//hideDiv();				
		}
		else
		{
			//$("#signUpServerError").html(data);
			//$("#divServerError").show();
			return false;
		}				
		},errorHandler:handleError 
	});	
}

function checkInventoryStatusComplete()
{
	try{
		DashboardAjax.getInventoryStatus(
		{
			async: false,		
			callback: function(data){		
			if(data)
			 {			
			 	document.getElementById("epistatus").value = "complete";
													
			 }
			else
			 {
				document.getElementById("epistatus").value = "";							
		     }		
		}
		}		
	);	}catch(e){alert(e)}
}

function chkApplyJobNonClientDashJOI(jobId,appCriteria,exitUrl,districtId,schoolId)
{
	try
	{		
	$('#loadingDiv').show();
	checkInventoryStatusComplete();
	document.getElementById('tempjobid').value=jobId;
	document.getElementById('district').value=districtId;
	document.getElementById('school').value=schoolId;
	document.getElementById('criteria').value=appCriteria;
	var urlstatus = exitUrl.indexOf("http://");
	var urlstatus1 = exitUrl.indexOf("https://");
	if(urlstatus==-1 && urlstatus1==-1)
	{
		document.getElementById('exitUrl').value="http://"+exitUrl;
	}
	else
	{
		document.getElementById('exitUrl').value=exitUrl;
	}		
	if(appCriteria==1)
	{		
		var cnt=0;		
		var focs=0;	
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;		
						$('#loadingDiv').hide();						
						$("#registerModal").modal("show");	
						$('#fname').focus();
					}					
				}				
			}
		}
		xmlHttp.send(null);		
		if(cnt==0){				
			jstatus = applyteacherjobfromquest(jobId);
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");	
			}
			else
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(jobId,districtId,exitUrl,schoolId);		
			}
			if(document.getElementById('epistatus').value=="complete")
			{
				$('#applyJobComplete').modal('show');
			}
			else
			{
				getJosOfIntrestGrid();
			}
		}else{
			return false;
		}
	}
	else if(appCriteria==3)
	{		
		var cnt=0;		
		var focs=0;	
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;
						try
						{						
							$('#loadingDiv').hide();
							jobApplicationHistory(jobId,districtId,exitUrl,schoolId);
							checkPopup(document.getElementById('exitUrl').value);
						}catch(e){alert(e)}													
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){
			
			jstatus = applyteacherjobfromquest(jobId);			
			if(jstatus==1)
			{				
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);			
				$("#appliedJob").modal("show");	
			}
			else
			{				
				$('#loadingDiv').hide();
				jobApplicationHistory(jobId,districtId,exitUrl,schoolId);		
			}
			if(document.getElementById('epistatus').value=="complete")
			{
				$('#applyJobComplete').modal('show');
			}
			else
			{
				getJosOfIntrestGrid();
			}
			
		}else{
			return false;
		}
		
	}
	else if(appCriteria==0)
	{
		var cnt=0;
		var focs=0;	
		createXMLHttpRequest();  
		queryString = "chkJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}									
				}			
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){					
			applyteacherjobfrommain(jobId);			
			jobApplicationHistory(jobId,districtId,exitUrl,schoolId);			
		}else{
			$('#errordiv').show();
			return false;
		}
	}
	else if(appCriteria==2)
	{
		var cnt=0;		
		var focs=0;	
		createXMLHttpRequest();  
		queryString = "chkQuestJobDetails.do?jobId="+jobId+"&dt="+new Date().getTime();
		xmlHttp.open("GET", queryString, false);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{
					var msgStatus = xmlHttp.responseText; 
					
					if(msgStatus==4){					
						$('#divAlert').modal('show');
						cnt++;focs++;
					}
					else if(msgStatus==1){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==2){
						$('#errordiv').append("&#149;  "+resourceJSON.msgJobnotActive+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==3){
						$('#errordiv').append("&#149; "+resourceJSON.msgJobExpired+"<br>");
						if(focs==0)
							$('#fremail5').focus();

						$('#fremail5').css("background-color",txtBgColor);
						cnt++;focs++;
					}
					else if(msgStatus==5)
					{
						cnt++;focs++;		
						$('#loadingDiv').hide();
						$("#registerModal").modal("show");						
						$('#fname').focus();
					}					
				}				
			}
		}
		xmlHttp.send(null);
		
		if(cnt==0){				
			jstatus = applyteacherjobfromquest(jobId);
			if(jstatus==1)
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(document.getElementById('tempjobid').value,document.getElementById('district').value,document.getElementById('exitUrl').value,document.getElementById('school').value);
				$("#appliedJob").modal("show");
			}
			else
			{
				$('#loadingDiv').hide();
				jobApplicationHistory(jobId,districtId,exitUrl,schoolId);			
				if(document.getElementById('epistatus').value=="complete")
				{
					$('#applyJobComplete').modal('show');
				}
				else
				{
					checkInventory(0,jobId);
				}
			}	
		}else{
			return false;
		}
	}
	}
	catch(e){
		alert(e);
		}
}

function hideDiv()
{
	checkPopup(document.getElementById('exitUrl').value);
	getJosOfIntrestGrid();
	$('#applyJobComplete').modal('hide');
}

function getDashboardMilestonesNew()
{ 
		MyDashboardAjax.getMilestonesGrid({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#divMilestones').html(data);		
		tpJbIntrestDashboardEnable();
	}
	});
	
}

function getDashboardJobsofIntrestAvailableNew()
{
	MyDashboardAjax.showDashboardJbofIntresGridAvailable({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		try
		{
		$('#divFavTeacher').html(data);
		tpJbIntrestDashboardEnable();
		}
		catch(e){alert(e)}
	}
	});	
}


function getDashboardReportNew()
{
	MyDashboardAjax.getReportGrid({ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#divReport').html(data);
		tpJbIntrestDashboardEnable();
	}
	});	
}

//shadab start
function runScript(characterCode){
	//alert(characterCode.keyCode);
	if(characterCode.keyCode == 13)
	{
		//searchRecordsByEntityTypeES();
		serachByBtnES();
	}
	
}
function serachByBtnES(){
	/*if($.trim($("#searchTerm").val())=="")
	{
		serachByBtn();
	}
	else
	{*/
		defaultSet();
		selJBIStatus = document.getElementById("selJBIStatus").value;
		getJosOfIntrestGridES()
	//}	
	
}
function getJosOfIntrestGridES()
{
	//alert("hello");
	$('#loadingDiv').show();
	var searchBy = document.getElementById("selJBIStatus").value;
	var geoZoneId = document.getElementById("zone").value;
	var districtId = document.getElementById("districtId").value;
	var schoolId = document.getElementById("schoolId1").value;
	var stateId = document.getElementById("stateId").value;
	var cityId = document.getElementById("cityId").value;
	//var subjectId = document.getElementById("subjectId").value;
	var subjectId = document.getElementById("subjectId");
	var subjectIdList = "";
	for(var i=0; i<subjectId.options.length;i++)
	{	
		if(subjectId.options[i].selected == true){
			//subjectIdList = subjectIdList+subjectId.options[i].text+",";
			subjectIdList = subjectIdList+subjectId.options[i].value+" ";
		}
	}
	
	var subjectName=$("#subjectId option:selected").text();
	var stateName=$("#stateId option:selected").text();
	
	var dt=new Date().getTime();
	var searchTerm="";
	//DashboardAjax.getJobsOfInterestAjaxES(searchBy,noOfRows,page,sortOrderStr,sortOrderType,dt,districtId,schoolId,stateId,cityId,subjectId,geoZoneId,$("#searchTerm").val(),,subjectName,stateName,
	//alert("hello");
	DashboardAjax.getJobsOfInterestAjaxESTest(searchBy,noOfRows,page,sortOrderStr,sortOrderType,dt,districtId,schoolId,stateId,cityId,subjectIdList,geoZoneId,$("#searchTerm").val(),subjectName,stateName,
	{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null && data!="")
			{
				document.getElementById("divJobsOfIntrest").innerHTML=data;
				applyScrollOnTbl();				
				tpJbIEnable();			
				$('#loadingDiv').hide();
			}
			else
			{
				$('#loadingDiv').hide();
			}
		}
	});			
}

//shadab end


function generateSkillPDF(teacherId,groupName)
{
	PrintCertificateAjax.generateSkillPDF(teacherId,groupName,
			{ 
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					$("#spCertificate").modal("show");
					$('#spCert').attr('src',data);
				}
			});	
	
	
}




function chkApplyJobAuthenticate(jobId)
{
	$("#talentAuthenticateModel").modal("show");
	

}

function hideModalById(divId,flag){
	
	$('#'+divId).modal("hide");
	if(flag==1)
		window.location="signin.do";
}

function applySubmit(){
	$("#applyTeacherJob").submit();
}

function eRegConnect(){
	var teacherID = "";
		teacherID = document.getElementById("teacherID").value;
	try{
		$('#loadingDiv').show();		
		$("#spnMpro").html("");
		DashboardAjax.connectEREG(teacherID,
			{
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{ 
			//$('#popupenableid_kelly').show();
					if(data!=null && data.length>0){
						var res = data.split("###");
						if(res[0]=="Successful"){
							
							
							if(res[1]!=null && res[1].length>0){								
								var popup=window.open(res[1],'_blank');								
								if (popup) {
									  popup.onload = function () {
										  if(popup.innerHeight > 0){
										  //alert("popup is enabled-"+popup.innerHeight);
										  }else{
											  $('#popupenableid_kelly').show();
										  }
									  }
									} else {
										$('#popupenableid_kelly').show();
									}
								
								
							}else{
								$("#eRegSSOInfoModal").modal("show");
								$("#infoDiv").text("SSO Authentication was successful but no URL is supplied");
							}
							
							$(".eReg").text("Pending");
						}
						else if(res[0]=="Failed"){
							$("#eRegSSOInfoModal").modal("show");
							$("#infoDiv").text(res[1]);
							$(".eReg").text("Error");
						}
						else{
							$("#eRegSSOInfoModal").modal("show");
							$("#infoDiv").text(res[1]);
							try{
								$(".eReg").text("Error");
							}catch(e){}
						}
					}
					$("#loadingDiv").hide();
				}
			});
	}catch(e){}
}

function eRegSSOInfo(){
	$("#eRegSSOInfoModal").modal("hide");
}

function goNextOrNot(){	
	var nextStep=false;	
	if(document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nc.teachermatch.org")	
	{
		nextStep=internalNCcandidateFromExt();
	}else{
		nextStep=true;
	}
	genericExtForAll(nextStep);	
}
function genericExtForAll(nextStep)
{	
	var isNext=true;
	var cnt=0;
	var focs=0;
	if(nextStep)
	{
		try{		
			if($('[name="dID"]').val()!=null && ($('[name="dID"]').val()==804800 || $('[name="dID"]').val()==806900)){
				
				isNext=callForJeffcoDoNotHireAPI();
				
				if(isNext && $('[name="dID"]').val()==804800){
					
						var selectCoverRadioId=	$('input[name="reqType"]:checked').attr("Id");
						var coverLetterText=$('[name="coverLetter"]').parents().parents('.jqte').find(".jqte_editor").text();
						var callFlag=false;
						if((selectCoverRadioId.trim()=='rdoCL1' && coverLetterText.trim()!='') || selectCoverRadioId.trim()!='rdoCL1')
							callFlag=true;
						if(callFlag){
							var dobYear=$("#dobYear1").val().trim();
							var idobYear = new String(parseInt(dobYear));
							var dobMonth=$("#dobMonth1").val();
							var dobDay=$("#dobDay1").val();
							var ssn=$('#last4SSN').val().trim();
							var dob=trim(dobMonth.trim().length==1?"0"+dobMonth.trim():dobMonth.trim())+trim(dobDay.trim().length==1?"0"+dobDay.trim():dobDay.trim())+trim(dobYear);
							$('#loadingDiv').css('z-index','9999999');
							$('[name="donothireresponsevalue"]').val('');
							DistrictPortfolioConfigAjax.externalLinkJobApplyCheckForJeffco( $('[name="dID"]').val(),dob,ssn,{
								preHook:function(){$('#loadingDiv').show()},
								postHook:function(){$('#loadingDiv').hide()},
								async: false,
								errorHandler:handleError,
								callback: function(data)
								{
									var callbackInfo=data.split('@@')[0].split('##');
									$('[name="donothireresponsevalue"]').val(data.split('@@')[1]);
									console.log(callbackInfo);
									try{
										$('#myModalCL').parent().parent().addClass('addingClassFromExternalLink');
									}catch(e){}
									if(callbackInfo[0]==1){
										isNext=false;
										getConfirmBox(callbackInfo[1],callbackInfo[2], "Ok##addingClassFromExternalLink");
									}
								}
							});
						}
				}
				
			}
			
			
			
			if($("#districtId").val()==806810){
				$('#errordivCL1').html("");
					$('#errordivCL').html("");
					
					if(document.getElementById("rdoCL1").checked==true)
					{
						if ($('#divCoverLetter').find(".jqte_editor").text().trim()==""){
							$('#errordivCL').append("&#149; "+resourceJSON.msgTypeCoverLtr+"<br>");
							$('#divCoverLetter').find(".jqte_editor").css("background-color", "#F5E7E1");
							cnt++;focs++;	
						}
						else{
							$('#divCoverLetter').find(".jqte_editor").css("background-color", "#FFF");
							$('#errordivCL').html("");
						}
					}
					
				try{
					
					var	isAffilated=$('input[name=isAffilated]:checked').val();
					if(isAffilated!=1 && isAffilated!=2 && isAffilated!=3 && isAffilated!=4 && isAffilated!=5 && isAffilated!=0)
					{
						$('#errordivCL').append("&#149; "+resourceJSON.employeeStatusBox+"<br>");//EmployeeStatusBox
						
						return false;
					}
					else
					{
						$('#errordivCL').html('');
					}
					if(isAffilated==2){
						
						$('#nameWhileEmployed').css("background-color", "#FFF");
						if($("#nameWhileEmployed").val().trim()=="" ){
							$('#errordivCL1').append("&#149; Please Enter Name While Employed<br>");
									$('#nameWhileEmployed').css("background-color", "#F5E7E1");
									cnt++;focs++;
									isNext=false;
						}
						else{
							$('#nameWhileEmployed1').val($("#nameWhileEmployed").val().trim());
						}
						
					}
					
					if(isAffilated==1){
						
						$('#empfe2location').css("background-color", "#FFF");
						$('#empfe2position').css("background-color", "#FFF");
						if($("#empfe2location").val().trim()==""){
							$('#errordivCL1').append("&#149; Please Enter Location<br>");
									$('#empfe2location').css("background-color", "#F5E7E1");
									cnt++;focs++;
									isNext=false;
						}else{
							$('#locationn').empty();
							$('#locationn').val($("#empfe2location").val().trim());
						}
						if($("#empfe2position").val().trim()==""){
							$('#errordivCL1').append("&#149; Please Enter Position<br>");
								$('#empfe2position').css("background-color", "#F5E7E1");
								cnt++;focs++;
								isNext=false;
						}else{
							$('#pposition').empty();
							$('#pposition').val($("#empfe2position").val().trim());
						}
					}
					if(isAffilated==3){
						$('#empaproretdatefe6').css("background-color", "#FFF");
						if($("#empaproretdatefe6").val().trim()==""){
							$('#errordivCL1').append("&#149; Please Enter Retirement Date<br>");
									$('#empaproretdatefe6').css("background-color", "#F5E7E1");
									cnt++;focs++;
									isNext=false;
						}else{
							$('#datee').empty();
							$('#datee').val($("#empaproretdatefe6").val().trim());
						}
					}
				}catch(err){}
			}
			
			
			//Dhananjay Verma 23-02-2016 For Martin District
			
			if($("#districtId").val().trim() == '1201290'){
				
				try{
					
					$('#errordivCL').empty();
					$('#errordivCL').html("");
					
					$('#errordivCL1').empty();
					$('#errordivCL1').html("");
					
					var	isAffilated=$('input[name=isAffilated]:checked').val();
					
					var	coverLetterRadioButton=$('input[id=rdoCL2ForMartinAdmin]:checked').val();
					
					
					if(coverLetterRadioButton == 'NCL')
					{
						
					}
					if(coverLetterRadioButton == 'YCL')
					{
						var coverLetter=$('[name="coverLetter"]').parents().parents('.jqte').find(".jqte_editor").text();
						
						
						$('#coverLetter').css("background-color", "#FFF");
						
						if(coverLetter.trim() == "" ){
							
							$('#errordivCL1').append("&#149; Please type in your cover letter<br>");
								
								$('#coverLetter').css("background-color", "#F5E7E1");
								
								isNext=false;
								if(focs==0)
								{
									$('#coverLetter').focus();
									focs++;
								}
								cnt++;
							}
						
					}
					
					
					if(isAffilated==2){//Formal Employee
						
						
						$('#fromalempjobtitle').css("background-color", "#FFF");
						
						if( $("#fromalempjobtitle").val().trim()=="" ){
							
							$('#errordivCL1').append("&#149; Please Enter Job Title<br>");
				
								$('#fromalempjobtitle').css("background-color", "#F5E7E1");
								isNext=false;
								if(focs==0)
								{
									$('#fromalempjobtitle').focus();
									focs++;
								}
								cnt++;
							}
					
						
						if( $("#formalempdatesofemployee").val().trim()=="" ){
							
							$('#errordivCL1').append("&#149; Please Enter Dates Of Employment<br>");
				
								$('#formalempdatesofemployee').css("background-color", "#F5E7E1");
					
								isNext=false;
								if(focs==0)
								{
									$('#formalempdatesofemployee').focus();
									focs++;
								}
								cnt++;
							}
				
				
						if( $("#formalempnamewhileemployee").val().trim()=="" ){
					
							$('#errordivCL1').append("&#149; Please Enter Name While Employed<br>");

							$('#formalempnamewhileemployee').css("background-color", "#F5E7E1");
			
							isNext=false;
							if(focs==0)
							{
								$('#formalempnamewhileemployee').focus();
								focs++;
							}
							cnt++;
					}
				
				
					}
					
					
						if(isAffilated==3){//Retired Employee
						
							$('#retiredempdateofretirement').css("background-color", "#FFF");
						
							if( $("#retiredempdateofretirement").val().trim()=="" ){
							
							$('#errordivCL1').append("&#149; Please Enter Date of Retirement <br>");
				
								$('#retiredempdateofretirement').css("background-color", "#F5E7E1");
					
								isNext=false;
								if(focs==0)
								{
									$('#retiredempdateofretirement').focus();
									focs++;
								}
								cnt++;
							}
						
					}
					

						if(isAffilated==1){//Current Employee
			
								$('#currentempjobtitle').css("background-color", "#FFF");
			
								if( $("#currentempjobtitle").val().trim()=="" ){
				
									$('#errordivCL1').append("&#149; Please Enter Job Title<br>");

									$('#currentempjobtitle').css("background-color", "#F5E7E1");

									isNext=false;
									if(focs==0)
									{
										$('#currentempjobtitle').focus();
										focs++;
									}
									cnt++;
								}

			
								if( $("#currentemplocation").val().trim()=="" ){
				
									$('#errordivCL1').append("&#149; Please Enter Location<br>");

									$('#currentemplocation').css("background-color", "#F5E7E1");

									isNext=false;
									if(focs==0)
									{
										$('#currentemplocation').focus();
										focs++;
									}
									cnt++;
								}


								if( $("#currentempyearinlocation").val().trim()=="" ){

									$('#errordivCL1').append("&#149; Please Enter Year In Location<br>");

									$('#currentempyearinlocation').css("background-color", "#F5E7E1");

									isNext=false;
									if(focs==0)
									{
										$('#currentempyearinlocation').focus();
										focs++;
									}
									cnt++;
								}


								if( $("#currentempsupervisor").val().trim()=="" ){

									$('#errordivCL1').append("&#149; Please Enter Supervisor<br>");

									$('#currentempsupervisor').css("background-color", "#F5E7E1");


									isNext=false;
									if(focs==0)
									{
										$('#currentempsupervisor').focus();
										focs++;
									}
									cnt++;

								}
						}
						

					if(isAffilated==5){//Current Substitute Employee
			
							$('#currentsubstitutejobtitle').css("background-color", "#FFF");
			
							if( $("#currentsubstitutejobtitle").val().trim()=="" ){
				
								$('#errordivCL1').append("&#149; Please Enter Job Title<br>");

								$('#currentsubstitutejobtitle').css("background-color", "#F5E7E1");

								isNext=false;
								if(focs==0)
								{
									$('#currentsubstitutejobtitle').focus();
									focs++;
								}
								cnt++;
							}
							
					}
					
					if(isAffilated == undefined)
					{
						$('#errordivCL1').append("&#149; Please Check At Least One Employee Status<br>");
						
						isNext=false;
						if(focs==0)
						{
							
							focs++;
						}
						cnt++;
						
					}
					
				}catch(err){
					
					alert(err.message);
				}
			}
			//End
			
			
			
			if($("#districtId").val()==7800294){
				$('#errordivCL').html("");
				if(document.getElementById("rdoCL1").checked==true)
				{
					if ($('#divCoverLetter').find(".jqte_editor").text().trim()==""){
						$('#errordivCL').append("&#149; "+resourceJSON.msgTypeCoverLtr+"<br>");
						$('#divCoverLetter').find(".jqte_editor").css("background-color", "#F5E7E1");
						isNext=false;
						if(focs==0)
						{
							$('#divCoverLetter').find(".jqte_editor").focus();
							focs++;
						}
						
					}
					else{
						$('#divCoverLetter').find(".jqte_editor").css("background-color", "#FFF");
						$('#errordivCL').html("");
					}
				}
				try{
					$('#emppos').css("background-color", "");
					$('#districtEmail').css("background-color", "");
					var	isAffilated=$('input[name=isAffilated]:checked').val();
					if(isAffilated!=1 && isAffilated!=2 && isAffilated!=3 && isAffilated!=4 && isAffilated!=5 && isAffilated!=0)
					{
						$('#errordivCL').append("&#149; "+resourceJSON.employeeStatusBox+"<br>");//EmployeeStatusBox
						//alert('nahi jaunga');
						return false;
					}
					else
					{
						$('#errordivCL').html('');
					}
					if(isAffilated==2 && $("#emppos").val().trim()=="" ){
							$('#errordivCL').append("&#149; "+resourceJSON.msgEnterEmployeePos+"</BR>");
							$('#emppos').css("background-color", "#F5E7E1");
							isNext=false;
							if(focs==0)
							{
								$('#emppos').focus();
								focs++;
							}
					}
					if(isAffilated==1 && $('#districtEmail').val().trim()==""){
						$('#errordivCL').append("&#149; "+"Please enter District Email Address"+"<br>");
						$('#districtEmail').css("background-color", "#F5E7E1");
						isNext=false;
						if(focs==0)
						{
							$('#districtEmail').focus();
							focs++;
						}
					}
				}catch(err){}
			}
		}catch(e){alert(e);}
		
	}	
	return isNext;
	
}	


function closepopup(){
	$('#popupenableid_kelly').hide();
}
function internalNCcandidateFromExt()
{
	var nextStep=true;
	var buttonSSNClick=document.getElementById("isAffilated").checked;			
	var instTime=$('input[name="staffType"]:checked').val();
	var ssn= $("#currentEmpSSN").val();	
	if(buttonSSNClick && ssn.length==9)
	{
		 $('#errordivCLForSSN').empty();
		var jobIdForEmp="";
		jobIdForEmp=document.getElementById("jobId").value;
		HrmsLicenseAjax.internalCandidateForExternalLink(ssn,jobIdForEmp,{
			async: false,
			callback: function(data)
			{				
			if(data=='c' || data=='C')
			{
				
			}
			else
			{
				var message="We are unable to locate a record using the SSN you have entered. If you have entered your SSN correctly, please contact your local HR office. Click OK to continue with the full application or Cancel to return to the previous screen.";
				getConfirmNCForExt("TeacherMatch",message,resourceJSON.btnOk,function(data){
					if(data){						
						genericExtForAll(data);
					}
				});					
				nextStep= false;
				document.getElementById("isAffilated").checked=false;									
			}
			},
		errorHandler:handleError
		});
				
	}
	return nextStep;
}

function getConfirmNCForExt(header,confirmMessage,buttonNameChange,callback){	
	var div="<div class='modal hide in' id='confirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<!--<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>-->"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<button type='button' class='btn btn-default' id='confirmFalse'>"+resourceJSON.btnCancel+"</button>"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#myModalCL').after(div);
    confirmMessage = confirmMessage || '';
    //$('#myModalCL').modal('hide');
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#confirmbox #confirmMessage').html(confirmMessage);
    $('#confirmbox #myModalLabel').html(header);
    $('#confirmbox #confirmTrue').html(buttonNameChange);
    $('#confirmFalse').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        if (callback) callback(false);
    });
    $('#confirmTrue').click(function(){
    	$('#confirmbox').modal('hide');
        $('#confirmbox').remove();
        if (callback) callback(true);
    });
} 
