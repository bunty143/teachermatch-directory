var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayTempStatus();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayTempStatus();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
/*========  SearchDistrictOrSchool ===============*/
function validateStatusFile()
{
	var branchId	=	document.getElementById("branchId").value;
	var statusfile	=	document.getElementById("statusfile").value;
	var errorCount=0;
	var aDoc="";
	$('#errordiv').empty();
	
	if(headQuarterId!=0 && branchId==0)
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please enter Branch Name.<br>");
		errorCount++;
		aDoc="1";
	}	
	
	if(statusfile==""){
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please select xls or xlsx file to import.<br>");
		errorCount++;
		aDoc="1";
	}
	else if(statusfile!="")
	{
		var ext = statusfile.substr(statusfile.lastIndexOf('.') + 1).toLowerCase();	
		
		var fileSize = 0;
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{
			if(document.getElementById("statusfile").files[0]!=undefined)
			{
				fileSize = document.getElementById("statusfile").files[0].size;
			}
		}
		
		if(!(ext=='xlsx' || ext=='xls'))
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; Please select xls or xlsx file only.<br>");
			errorCount++;
			aDoc="1";
		}
		else if(fileSize>=10485760)
		{
			$('#errordiv').show();
			$('#errordiv').append("&#149; File size must be less than 10mb.<br>");
			errorCount++;
			aDoc="1";	
		}
	}
	
	if(aDoc==1){
		$('#statusfile').focus();
		$('#statusfile').css("background-color", "#F5E7E1");
	}
	
	if(errorCount==0){
		try{
			if(statusfile!=""){
					document.getElementById("statusUploadServlet").submit();
			}
		}catch(err){}
		
	}else{
		$('#errordiv').show();
		return false;
	}
}

function uploadDataStatus(fileName,sessionId){
	var branchId	=	document.getElementById("branchId").value;
	$('#loadingDiv').fadeIn();
	StatusUploadTempAjax.saveStatusTemp(fileName,sessionId,branchId,{ 
		async: true,
		callback: function(data){
			if(data=='1'){
					 window.location.href="statustemplist.do";
			}else{
				$('#loadingDiv').hide();
				$('#errordiv').show();
				$('#errordiv').append("&#149; Field(s) "+data+" does not match.<br>");
			}
		},
		errorHandler:handleError 
	});
	
}
function displayTempStatus()
{
	StatusUploadTempAjax.displayTempStatusRecords(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data)
		{
			$('#tempStatusGrid').html(data);
			applyScrollOnTbl();
		},
		errorHandler:handleError  
	});
}

function tempStatus(){
		window.location.href="importstatusdetails.do";
}
function tempStatusReject(sessionIdTxt){
	$('#loadingDiv').fadeIn();
	StatusUploadTempAjax.deleteTempStatus(sessionIdTxt,{ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			tempStatus();
		},
		errorHandler:handleError 
	});
}

function saveStatus(){
	$('#loadingDiv').fadeIn();
	StatusUploadTempAjax.saveStatus({ 
		async: true,
		callback: function(data){
			$('#loadingDiv').hide();
			$('#myModalMsg').modal('show');
		},
		errorHandler:handleError 
	});
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
