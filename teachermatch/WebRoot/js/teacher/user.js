var txtBgColor="#F5E7E1";
var xmlHttp=null;
function checkSession(responseText)
{
	if(responseText=='100001')
	{
		alert(resourceJSON.oops)
		window.location.href="signin.do";
	}	
}
function createXMLHttpRequest()
{
	if (window.ActiveXObject)
    	xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
   	else if (window.XMLHttpRequest)
		xmlHttp = new XMLHttpRequest();
        else
        	alert(resourceJSON.msgAjaxNotSupported);
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}


function showDiv(divId)
{
	var tempDivId;
	for(var j=0;j<divArray.length;j++)
	{
		tempDivId=document.getElementById(divArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}
function showDivMsg(divId,msgArray)
{
	
	var tempDivId;
	for(var j=0;j<msgArray.length;j++)
	{
		tempDivId=document.getElementById(msgArray[j]).id;
		document.getElementById(tempDivId).style.display="none";
	}
	if(divId!=null)
	{
		document.getElementById(divId).style.display="block";
	}
}
function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}
function deleteData(delUrl)
{
	if(window.confirm(resourceJSON.cnfMsgDelete))
	{
		var url = delUrl+"&td="+new Date().getTime();
		window.location.href=url;
	}
}


function showHideBankDiv(stBank,stChequeDD,stButton)
{
	document.getElementById("divBankList").style.display=stBank;
	document.getElementById("divChequeDD").style.display=stChequeDD;
	document.getElementById("divPayButton").style.display=stButton;
}

function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function chkPwdPtrn(str)
{
	var pat1 = /[^a-zA-Z]/g; //If any spcl char find it return true otherwise false
	var pat2 = /[a-zA-Z]/g;  //If any alphabet found it return true otherwise false
	//alert(pat1.test(str) && pat2.test(str))
	return pat1.test(str) && pat2.test(str);
}

function checkSubRegionsOnOff(dis)
{
	var sid=dis.id;
   var onOffvalue=$("#onOffRegion").val();
   var sub=document.getElementsByTagName("input");	
	if(dis.checked==true)
	{
		for(var i=0;i<sub.length;i++)
		{
			if(sub[i].type=="checkbox")
			{
				if((sub[i].id).indexOf(sid)!= -1)
				{
					sub[i].checked=true;
				}
			}
		}
		if(resourceJSON.locale=="fr" || onOffvalue==1 )
		{
		  $('.'+sid).show();
		}
	}else{
        for(var i=0;i<sub.length;i++)
		{
			if(sub[i].type=="checkbox")
			{

				if((sub[i].id).indexOf(sid)!= -1)
				{
					sub[i].checked=false;
				}
			}
		}
       if(resourceJSON.locale=="fr" || onOffvalue==1)
       {
		 $('.'+sid).hide();
       }		
	}
}

function checkTopRegionsOnOff(topRgnId)
{
	var count=0;
	var topRgnId=document.getElementById(topRgnId);
	//alert(topRgnId.checked+">")
	
	
	var sub=document.getElementsByTagName("input");
	for(var i=0;i<sub.length;i++)
	{
		if(sub[i].type=="checkbox")
		{
			if(((sub[i].id).indexOf(topRgnId.id)!= -1)&&sub[i].id!=topRgnId.id)
			{
				//alert(""+sub[i].id+"")
				//alert(document.getElementById(sub[i].id).checked)
				
				if(document.getElementById(sub[i].id).checked)
				{
					count=count+1;
				}
			}
		}
	}
	
	if(count==0)
	{
		topRgnId.checked=false;
	}
	else
	{
		topRgnId.checked=true;
	}
	
}

function setTopPref()
{
	//alert("OK")
	 var onOffvalue=$("#onOffRegion").val();
	 var sub=document.getElementsByTagName("input");
	for(var i=0;i<sub.length;i++)
	{
		if(sub[i].type=="checkbox" && (sub[i].id).indexOf("rgn")!= -1)
		{
			//alert(sub[i].id)
			//alert(document.getElementById(sub[i].id).checked)
			if(document.getElementById(sub[i].id).checked)
			{
				var chkId = (sub[i].id).split("rgn")[0]+"rgn"
				//alert(sub[i].id)
				//alert(chkId)
				
				document.getElementById(chkId).checked=true;
				 if(resourceJSON.locale=="fr" || onOffvalue==1){
				    $('.'+chkId).show();
				  }
			}else if(resourceJSON.locale=="fr" || onOffvalue==1){
				//alert(" hide Div : "+sub[i].id);
				 $('.'+sub[i].id).hide();
			}
		}
	}	
}


function chkPref()
{
	var geoChk=0;
	var schoolTypeChk=0;
	var regionsChk=0;
	var stateChk=0;
	var jobCatChk=0;
	
	var geo = document.getElementsByName("geo");
	var schoolType = document.getElementsByName("schoolType");    
	var regions = document.getElementsByName("regions");

	// kelly user specific
	var isKelly=document.getElementById("isKelly").value;
	var states = document.getElementsByName("state");
	var jobcategory = document.getElementsByName("jobCategory");
	
	
	var cnt=0;
	var focs=0;	
	document.getElementById("divSelectPref").style.display="none";
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	$('#geo').css("background-color","");
	$('#schoolType').css("background-color","");
	$('#regions').css("background-color","");
	
	
	
    for (var i=0; i <geo.length; i++)
	{    
        if (geo[i].checked)
        {
        	geoChk=geoChk+1;
        }
    }
    
    for (var i=0; i <schoolType.length; i++)
	{    
        if (schoolType[i].checked)
        {
        	schoolTypeChk=schoolTypeChk+1;
        }
    }

    for (var i=0; i <regions.length; i++)
	{    
        if (regions[i].checked)
        {
        	regionsChk=regionsChk+1;
        }
    }
  //kelly only  
    for (var i=0; i <states.length; i++)
	{    
        if (states[i].checked)
        {
        	stateChk=stateChk+1;
        }
    }
    for (var i=0; i <jobcategory.length; i++)
	{    
        if (jobcategory[i].checked)
        {
        	jobCatChk=jobCatChk+1;
        }
    }
    if(isKelly=="0" || isKelly=="")
    {
    	if(geoChk==0)
        {
        	$('#errordiv').append("&#149; "+resourceJSON.msgatleastoneGeography+"<br>");		
    		cnt++; 	
        }
        if(schoolTypeChk==0)
        {
    		$('#errordiv').append("&#149; "+resourceJSON.msgatleastoneschoolType+"<br>");	
    		cnt++;
    	}
        if(regionsChk==0)
        {
    		$('#errordiv').append("&#149; "+resourceJSON.msgatleastoneRegion+"<br>");
    		cnt++;
    	}
    }
    
    //kelly only
    if(isKelly=="1")
    {
	 if(jobCatChk==0)
	    {
	    	$('#errordiv').append("&#149; Please select at least one Job Category <br>");		
			cnt++; 	
	    }
	    if(stateChk==0)
	    {
	    	$('#errordiv').append("&#149; Please select at least one State <br>");		
			cnt++; 	
	    }
    }
   
    
    if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		$('html, body').animate({scrollTop : 0},800);
		return false;
	}
}





function checkEmailSetting(emailAddress)
{
	var res;
	createXMLHttpRequest();  
	queryString = "chksettingemailexist.do?emailAddress="+emailAddress+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{			
				checkSession(xmlHttp.responseText)
				res = xmlHttp.responseText;				
			}			
			else
			{
				alert("server Error");
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;
}
function validateEditSettings()
{	
		
	var firstName = document.getElementById("firstName");
	var lastName = document.getElementById("lastName");
	var emailAddress = document.getElementById("emailAddress");
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	$('#divServerError').css("background-color","");
	$('#firstName').css("background-color","");
	$('#lastName').css("background-color","");
	$('#emailAddress').css("background-color","");
		
	if(trim(firstName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
		if(focs==0)
			$('#firstName').focus();
		
		$('#firstName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(lastName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrLastName+"<br>");
		if(focs==0)
			$('#lastName').focus();
		
		$('#lastName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!isEmailAddress(trim(emailAddress.value)))
	{		
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrVldEmail+"<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(checkEmailSetting(trim(emailAddress.value)))
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgaladyemail+" <br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(cnt==0)
	{
		$('#loadingDiv').show();
		return true;
	}
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function showChangePWD()
{
	document.getElementById("oldpassword").value="";
	document.getElementById("newpassword").value="";
	document.getElementById("repassword").value="";
	$('#errordivPwd').empty();
	$('#divServerError').css("background-color","");
	$('#oldpassword').css("background-color","");
	$('#newpassword').css("background-color","");
	$('#repassword').css("background-color","");
	$('#myModal').modal('show');
	document.getElementById("oldpassword").focus();
	return false;
}


function chkPassword()
{
	var oldpassword = document.getElementById("oldpassword");
	var newpassword = document.getElementById("newpassword");
	var repassword = document.getElementById("repassword");
	
	
	var cnt=0;
	var focs=0;	
	$('#errordivPwd').empty();
	$('#divServerError').css("background-color","");
	$('#oldpassword').css("background-color","");
	$('#newpassword').css("background-color","");
	$('#repassword').css("background-color","");
	
	
	if(trim(oldpassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgcurrentpsw+" <br>");
		if(focs==0)
			$('#oldpassword').focus();
		
		$('#oldpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(!chkPasswordExist(oldpassword.value))
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgCurrentPassworddoesnotexis+" <br>");
		if(focs==0)
			$('#oldpassword').focus();
		
		$('#oldpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
		
	if(trim(newpassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgPleaseenterNewPassword+" <br>");
		if(focs==0)
			$('#newpassword').focus();
		
		$('#newpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	} 
	else if(!chkPwdPtrn(newpassword.value) || newpassword.value.length<6 )
	{		
		$('#errordivPwd').append("&#149; "+resourceJSON.msgpasswordcombination+" <br>");
		if(focs==0)
			$('#newpassword').focus();
		
		$('#newpassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(repassword.value)=="")
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgReenterPassword+"<br>");
		if(focs==0)
			$('#repassword').focus();
		
		$('#repassword').css("background-color",txtBgColor);
		cnt++;focs++;
	} 
	if(trim(newpassword.value)!="" && trim(repassword.value)!="" && !(trim(newpassword.value)==trim(repassword.value)))
	{
		$('#errordivPwd').append("&#149; "+resourceJSON.msgrepasswordnotmatch+" <br>");
		if(focs==0)
			$('#newpassword').focus();
		
		$('#newpassword').css("background-color",txtBgColor);
		$('#repassword').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	
	
	if(cnt==0)
	{
		
		$('#loadingDiv').show();
		document.getElementById("password").value=newpassword.value;
		$('#errordivPwd').empty();
		$('#divServerError').css("background-color","");
		$('#oldpassword').css("background-color","");
		$('#newpassword').css("background-color","");
		$('#repassword').css("background-color","");
		
		
		
		createXMLHttpRequest();  
		queryString = "changepwd.do?password="+newpassword.value+"&dt="+new Date().getTime();
		xmlHttp.open("POST", queryString, true);
		xmlHttp.onreadystatechange = function()
		{
			if(xmlHttp.readyState == 4)
			{
				if(xmlHttp.status==200)	
				{			
					checkSession(xmlHttp.responseText)
					$('#loadingDiv').hide();
					$('#myModal').modal('hide');
				}			
				else
				{
					alert("server Error");
				}
			}
		}
		xmlHttp.send(null);	
		
			
		return true;
	}
	else
	{
		$('#errordivPwd').show();
		return false;
	}
	
}




function chkPasswordExist(pwd)
{
	var res;
	createXMLHttpRequest();  
	queryString = "chkpwdexist.do?password="+pwd+"&dt="+new Date().getTime();
	xmlHttp.open("POST", queryString, false);
	xmlHttp.onreadystatechange = function()
	{
		if(xmlHttp.readyState == 4)
		{
			if(xmlHttp.status==200)	
			{		
				checkSession(xmlHttp.responseText)
				res = xmlHttp.responseText;				
			}			
			else
			{
				alert("server Error");
			}
		}
	}
	xmlHttp.send(null);		
	if(res==1)
		return true;
	else
		return false;

}



function chkForEnter(evt)
{
	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		chkPassword();
	}
	
	
}
