/*========= Paging and Sorting ===========*/
var page = 1;
var noOfRows = 100;
var sortOrderStr="";
var sortOrderType="";
/*function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	//getPFEmploymentGrid();
	getInvolvementGrid();
}*/
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=100;
	}
	showGrid();
}

var txtBgColor="#F5E7E1";
var chkCallFrom=0;
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	//var re =/^(0\.|[1-9]\d*\.)d{2}/ 
	if (!re.test(field.value)) {
//		alert('Value must be all numberic charcters, including "." or "," non numerics will be removed from field!');
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	if (decNum.length > 1)//here is the key u can just change from 2 to 3,45 etc to restict no of digits aftre decimal
	{
	//alert("Invalid more than 2 digits after decimal")
		//count=false;
	}
	//alert(count);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
    
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}
function showGrid()
{
	PFAcademics.getPFAcademinGrid(noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#divDataGrid').html(data);
			applyScrollOnTbl();
		}});
}
function showForm()
{
	document.getElementById("divFormGrid").style.display="block";
}

function delRow_academic(id)
{
	if(window.confirm(resourceJSON.msgsuredelete))
	{
		PFAcademics.deletePFAcademinGrid(id, { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			showGrid();
			resetUniversityForm();
			return false;
		}});
	}
}

function showUniversityForm()
{
	document.getElementById("international").checked=false;
	document.getElementById("divAcademicRow").style.display="block";
	document.getElementById("divDone").style.display="block";
	document.getElementById("divGPARow").style.display="none";
	document.getElementById("divGPARowOther").style.display="none";	
	document.getElementById("pathOfTranscript").style.display="none";
	document.getElementById("removeTranscript").style.display="none";
	document.getElementById("academicId").value="";
	document.getElementById("hiddenged").value="";
	document.getElementById("frmAcadamin").reset();
	document.getElementById("frmGPA").reset();
	document.getElementById("frmGPA1").reset();	
	document.getElementById("degreeName").focus();
	setDefColortoErrorMsg();
	$('#errordiv').empty();
	return false;
}

function resetUniversityForm()
{
	document.getElementById("divAcademicRow").style.display="none";
	document.getElementById("divDone").style.display="none";
	document.getElementById("divGPARow").style.display="none";
	document.getElementById("divGPARowOther").style.display="none";
	document.getElementById("frmAcadamin").reset();
	return false;
}



var finalcallStatus;
function insertOrUpdate(callStatus)
{
	finalcallStatus=callStatus;
	var academicId = document.getElementById("academicId");
	var degreeId = document.getElementById("degreeId");
	var degreeName = document.getElementById("degreeName");
	var universityId = document.getElementById("universityId");
	var universityName = document.getElementById("universityName");
	var fieldId = document.getElementById("fieldId");     
	var fieldName = document.getElementById("fieldName");     
	var attendedInYear = document.getElementById("attendedInYear"); 
	var leftInYear = document.getElementById("leftInYear");     
	var gpaFreshmanYear = document.getElementById("gpaFreshmanYear");     
	var gpaJuniorYear = document.getElementById("gpaJuniorYear");     
	var gpaSophomoreYear = document.getElementById("gpaSophomoreYear");     
	var gpaSeniorYear = document.getElementById("gpaSeniorYear");     
	var gpaCumulative = document.getElementById("gpaCumulative"); 
	var degreeType = document.getElementById("degreeType");
	var otherUniversityName=trim($("#otherUniversityName").val());
	var otherDegreeName=trim($("#otherDegreeName").val());
	var international=0;
	if(document.getElementById('international').checked){
        international=1;
    }
	
	var GEDFlag=false;
	if($('#degreeName').val()=="High School or GED"){
		GEDFlag=true;
	}

	
	var pathOfTranscriptFile = document.getElementById("pathOfTranscriptFile");
	var fileName = pathOfTranscriptFile.value;
	
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize=0;		
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{		
		if(pathOfTranscriptFile.files[0]!=undefined)
		fileSize = pathOfTranscriptFile.files[0].size;
	}
	var cnt=0;
	var focs=0;	
	
	$('#errordiv').empty();
	setDefColortoErrorMsg();	
	if(trim(degreeName.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.magPleaseenterDegree+"<br>");
		if(focs==0)
			$('#degreeName').focus();
		
		$('#degreeName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(resourceJSON.platformName=="NC" &&trim(degreeName.value)=="Otherss" && otherDegreeName=="" )
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrOtherDegree+"<br>");
		if(focs==0)
			$('#otherDegreeName').focus();
		
		$('#otherDegreeName').css("background-color",txtBgColor);
		cnt++;focs++;
	}	
	
if($('#degreeName').val()!="No Degree"){
	if(trim(universityName.value)=="" && GEDFlag==false)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgPleaseenterSchool+"<br>");
		if(focs==0)
			$('#universityName').focus();
		
		$('#universityName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(resourceJSON.platformName=="NC" && trim(universityName.value)=="Other" && otherUniversityName=="" && GEDFlag==false)
	{
		$('#errordiv').append("&#149; "+resourceJSON.PlzEtrOtherSchool+"<br>");
		if(focs==0)
			$('#otherUniversityName').focus();
		
		$('#otherUniversityName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(fieldName.value)=="" && GEDFlag==false)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgFieldofStudy+"<br>");
		if(focs==0)
			$('#fieldName').focus();
		
		$('#fieldName').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	 if(resourceJSON.locale!='fr'){
	if(trim(attendedInYear.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgdateattended+"<br>");
		if(focs==0)
			$('#attendedInYear').focus();
		
		$('#attendedInYear').css("background-color",txtBgColor);
		cnt++;focs++;
	}
		
	
	if(trim(leftInYear.value)=="")
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgdatetoattended+"<br>");
		if(focs==0)
			$('#leftInYear').focus();
		
		$('#leftInYear').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if((trim(attendedInYear.value)!="") && (trim(leftInYear.value)!="") &&(trim(attendedInYear.value) > trim(leftInYear.value)))
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgdateattendedgreaterthan+"<br>");
		if(focs==0)
			$('#attendedInYear').focus();
		
		$('#attendedInYear').css("background-color",txtBgColor);
		$('#leftInYear').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	 }
	
}
	
	if(GEDFlag==true){	
		if(trim(pathOfTranscriptFile.value)=="")
		{
			var hiddenged =	document.getElementById("hiddenged").value;
			if(hiddenged=="")
			{
				$('#errordiv').append("&#149; "+resourceJSON.msguploadtrascript+"<br>");
				if(focs==0)
					$('#pathOfTranscriptFile').focus();
				
				$('#pathOfTranscriptFile').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
		}
	}

	if(ext!="")
	if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgaacptfileformate+"<br>");
			if(focs==0)
				$('#pathOfTranscriptFile').focus();
			
			$('#pathOfTranscriptFile').css("background-color",txtBgColor);
			cnt++;focs++;	
			return false;
	}	
	else if(fileSize>=10485760)
	{
		$('#errordiv').append("&#149; "+resourceJSON.msgfilesizelessthan+"<br>");
			if(focs==0)
				$('#pathOfTranscriptFile').focus();
			
			$('#pathOfTranscriptFile').css("background-color",txtBgColor);
			cnt++;focs++;	
			return false;
	}
	
	if(trim(degreeType.value)=="B")
	{	
		if(trim(gpaFreshmanYear.value)=="" || trim(gpaFreshmanYear.value)==".")
		{
			/*$('#errordiv').append("&#149; Please enter valid Freshman GPA<br>");
			if(focs==0)
				$('#gpaFreshmanYear').focus();
			
			$('#gpaFreshmanYear').css("background-color",txtBgColor);
			cnt++;focs++;*/
		}
		else if(parseFloat(trim(gpaFreshmanYear.value))>5.00)
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgfreshmangpa+"<br>");
			if(focs==0)
				$('#gpaFreshmanYear').focus();
			
			$('#gpaFreshmanYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(trim(gpaSophomoreYear.value)=="" || trim(gpaSophomoreYear.value)==".")
		{
			/*$('#errordiv').append("&#149; Please enter valid Sophomore GPA<br>");
			if(focs==0)
				$('#gpaSophomoreYear').focus();
			
			$('#gpaSophomoreYear').css("background-color",txtBgColor);
			cnt++;focs++;*/
		}
		else if(parseFloat(trim(gpaSophomoreYear.value))>5.00)
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgSophomoreGPA+"<br>");
			if(focs==0)
				$('#gpaSophomoreYear').focus();
			
			$('#gpaSophomoreYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		
		if(trim(gpaJuniorYear.value)=="" || trim(gpaJuniorYear.value)==".")
		{
			/*$('#errordiv').append("&#149; Please enter valid Junior GPA<br>");
			if(focs==0)
				$('#gpaJuniorYear').focus();
			
			$('#gpaJuniorYear').css("background-color",txtBgColor);
			cnt++;focs++;*/
		}
		else if(parseFloat(trim(gpaJuniorYear.value))>5.00)
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgJuniorGPA+"<br>");
			if(focs==0)
				$('#gpaJuniorYear').focus();
			
			$('#gpaJuniorYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}
			
		if(trim(gpaSeniorYear.value)=="" || trim(gpaSeniorYear.value)==".")
		{
			/*$('#errordiv').append("&#149; Please enter valid Senior GPA<br>");
			if(focs==0)
				$('#gpaSeniorYear').focus();
			
			$('#gpaSeniorYear').css("background-color",txtBgColor);
			cnt++;focs++;*/
		}
		else if(parseFloat(trim(gpaSeniorYear.value))>5.00)
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgJuniorGPA+"<br>");
			if(focs==0)
				$('#gpaSeniorYear').focus();
			
			$('#gpaSeniorYear').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(trim(gpaCumulative.value)=="" || trim(gpaCumulative.value)==".")
		{
			/*$('#errordiv').append("&#149; Please enter valid Cumulative  GPA<br>");
			if(focs==0)
				$('#gpaCumulative').focus();
			
			$('#gpaCumulative').css("background-color",txtBgColor);
			cnt++;focs++;*/
		}
		else if(parseFloat(trim(gpaCumulative.value))>5.00)
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
			if(focs==0)
				$('#gpaCumulative').focus();
			
			$('#gpaCumulative').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		gpaCumulative = document.getElementById("gpaCumulative").value;
	}
	else if(GEDFlag==false)
	{
		gpaCumulative = document.getElementById("gpaCumulative1").value;
		if((trim(degreeName.value)!="") && (trim(gpaCumulative)=="" || trim(gpaCumulative)==".") && $('#degreeName').val()!="No Degree")
		{
			/*$('#errordiv').append("&#149; Please enter valid Cumulative  GPA<br>");
			if(focs==0)
				$('#gpaCumulative1').focus();
			
			$('#gpaCumulative1').css("background-color",txtBgColor);
			cnt++;focs++;*/
		}
		else if(trim(degreeName.value)!="" && parseFloat(trim(gpaCumulative))>5.00)
		{
			$('#errordiv').append("&#149; "+resourceJSON.msgCumulativeGPA+"<br>");
			if(focs==0)
				$('#gpaCumulative1').focus();
			
			$('#gpaCumulative1').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		gpaCumulative = document.getElementById("gpaCumulative1").value;
		document.getElementById("divGPARow").style.display="none";
		
	}
	
	
	if(cnt!=0)		
	{
		$('#errordiv').show();
		return false;
	}else{
		if(fileName!="")
		{
			$('#loadingDiv').show();
			document.getElementById("frmAcadamin").submit();
		}else
		{	
			PFAcademics.saveOrUpdateAcademics(academicId.value,degreeId.value,universityId.value,fieldId.value,
					attendedInYear.value,leftInYear.value,gpaFreshmanYear.value,gpaJuniorYear.value,
					gpaSophomoreYear.value,gpaSeniorYear.value,gpaCumulative,degreeType.value,fileName,international,otherUniversityName,otherDegreeName,
			{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
				{	
					showGrid();
					$("#hrefAcademics").addClass("stepactive");
					resetUniversityForm();
				}
			});

			if(callStatus==1 && ext=="")
			{
				//window.location.href="certification.do";
				sendToOtherPager();
			}
		}
	}
}
function saveAcademics(fileName)
{
	
	var academicId = document.getElementById("academicId");
	
	var degreeId = document.getElementById("degreeId");
	var universityId = document.getElementById("universityId");
	var fieldId = document.getElementById("fieldId");   
	
	var attendedInYear = document.getElementById("attendedInYear"); 
	var leftInYear = document.getElementById("leftInYear");     
	var gpaFreshmanYear = document.getElementById("gpaFreshmanYear");     
	var gpaJuniorYear = document.getElementById("gpaJuniorYear");     
	var gpaSophomoreYear = document.getElementById("gpaSophomoreYear");     
	var gpaSeniorYear = document.getElementById("gpaSeniorYear");     
	var gpaCumulative = document.getElementById("gpaCumulative"); 
	var degreeType = document.getElementById("degreeType");
	var otherUniversityName=trim($("#otherUniversityName").val());
	var otherDegreeName=trim($("#otherDegreeName").val());
	var international=0;
	if(document.getElementById('international').checked){
        international=1;
    }
	
	PFAcademics.saveOrUpdateAcademics(academicId.value,degreeId.value,universityId.value,fieldId.value,
			attendedInYear.value,leftInYear.value,gpaFreshmanYear.value,gpaJuniorYear.value,
			gpaSophomoreYear.value,gpaSeniorYear.value,gpaCumulative.value,degreeType.value,fileName,international,otherUniversityName,otherDegreeName,
	{ 
	async: false,
	errorHandler:handleError,
	callback: function(data)
		{	
			$('#loadingDiv').hide();
			showGrid();
			$("#hrefAcademics").addClass("stepactive");
			resetUniversityForm();
		}
	});
	
	if(finalcallStatus==1)
	{
		sendToOtherPager();
	}
}
function sendToOtherPager()
{
	
	$('#loadingDiv').hide();
	if(chkCallFrom==1)
	{
		window.location.href="certification.do";
	}
}

function showRecordToForm(academicId)
{

	document.getElementById("frmAcadamin").reset();	
	document.getElementById("international").checked=false;
	PFAcademics.showEditAcademics(academicId,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data)
	{
		//alert(data.leftInYear)
		//alert(data.degreeId.degreeId)
		//alert(data.degreeId.degreeName)
		
		
		document.getElementById("divAcademicRow").style.display="block";
		document.getElementById("divDone").style.display="block";
		
		document.getElementById("academicId").value=data.academicId;
		
		if(data.degreeId!=null){
		document.getElementById("degreeName").value=data.degreeId.degreeName;
		if(resourceJSON.platformName=="NC" && data.degreeId.degreeName=="Otherss")
		{
			$("#otherDegreeName").val(data.otherDegreeName);
			//$("#otherDegreeName").show();
		}
		else
		{
			//$("#otherDegreeName").hide();
		}
		document.getElementById("degreeId").value=data.degreeId.degreeId;		
		document.getElementById("degreeType").value=data.degreeId.degreeType;
		}
		
		document.getElementById("international").checked=data.international;
		if(data.universityId!=null){
		document.getElementById("universityId").value=data.universityId.universityId;
		document.getElementById("universityName").value=data.universityId.universityName;
		if(resourceJSON.platformName=="NC" && data.universityId.universityName=="Other")
		{
			$("#otherUniversityName").val(data.otherUniversityName);
			$("#otherUniversityName").show();
		}
		else
		{
			$("#otherUniversityName").hide();
		}
		}
			
		
		if(data.fieldId!=null){
			document.getElementById("fieldId").value=data.fieldId.fieldId;
			document.getElementById("fieldName").value=data.fieldId.fieldName;
		}
		
		if(data.pathOfTranscript!=null && data.pathOfTranscript!="")
		{
			document.getElementById("removeTranscript").style.display="block";
			document.getElementById("pathOfTranscript").style.display=data.pathOfTranscript;
			document.getElementById("divResumeName").innerHTML="<a href='javascript:void(0)' id='hrefEditTrans'" +
				"onclick=\"downloadTranscript('"+data.academicId+"','hrefEditTrans');if(this.href!='javascript:void(0)') " +
				"window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0');" +
				"return false;\">"+data.pathOfTranscript+"</a>";
			document.getElementById("hiddenged").value=data.pathOfTranscript
		}
		else
		{
			document.getElementById("removeTranscript").style.display="none";
			document.getElementById("divResumeName").innerHTML = "";
		}
		
		if(data.attendedInYear!=null)
			document.getElementById("attendedInYear".toString()+(data.attendedInYear)).selected=true;
			if(data.leftInYear!=null)
			document.getElementById("leftInYear".toString()+(data.leftInYear)).selected=true;
		
		if(document.getElementById("degreeType").value=="B" || document.getElementById("degreeType").value=="b")
		{
			document.getElementById("divGPARow").style.display="block";
			document.getElementById("gpaFreshmanYear").value =  data.gpaFreshmanYear;
			document.getElementById("gpaJuniorYear").value =  data.gpaJuniorYear;     
			document.getElementById("gpaSophomoreYear").value =  data.gpaSophomoreYear;     
			document.getElementById("gpaSeniorYear").value =  data.gpaSeniorYear;     
			document.getElementById("gpaCumulative").value =  data.gpaCumulative; 
		}else if(document.getElementById("degreeType").value=="ND"){
			document.getElementById("divGPARow").style.display="none";
			document.getElementById("divGPARowOther").style.display="block";
			document.getElementById("gpaCumulative1").value =  data.gpaCumulative;
		}
		else
		{
			document.getElementById("divGPARow").style.display="none";
			document.getElementById("divGPARowOther").style.display="block";
			document.getElementById("gpaCumulative1").value =  data.gpaCumulative;
		}
		if($('#degreeName').val()=="High School or GED"){
			document.getElementById('universityName').disabled=true;
			document.getElementById('fieldName').disabled=true;
			document.getElementById('gpaCumulative1').disabled=true;
		}else{
			document.getElementById('universityName').disabled=false;
			document.getElementById('fieldName').disabled=false;
			document.getElementById('gpaCumulative1').disabled=false;
		}
		
		document.getElementById("degreeName").focus();
		
	}})
	return false;
}
function downloadTranscript(academicId, linkId)
{	
		
		PFAcademics.downloadTranscript(academicId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{	if(data=="")
			{
				data="javascript:void(0)";
			}
			else if(data.indexOf(".doc")!=-1)
			{
				document.getElementById("ifrmTrans").src=data;
			}
			else
			{	
				document.getElementById(linkId).href = data;
			}
			return false;
		}});
}

function removeTranscript()
{
	var academicId = document.getElementById("academicId").value;
	if(window.confirm(resourceJSON.msgconfirmdeletescript))
	{
		PFAcademics.removeTranscript(academicId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){	
			document.getElementById("removeTranscript").style.display="none";
			showGrid();
			}
		});
	}
}


function saveAndContinuPFAcademics()
{
	//alert("");
	
	var idList = new Array("degreeName","universityName","fieldName");
	var otherUniversityName=trim($("#otherUniversityName").val());
	var count = 0;
	for(i=0;i<idList.length;i++)
	{
		if(trim(document.getElementById(idList[i]).value)!="")
		{
			count++;	
		}
	}
	if(resourceJSON.platformName=="NC" && trim($("#universityName").val())=="Other" && otherUniversityName=="")
	{
		count++;
	}
	if(count==0)
	{
		PFAcademics.updateAcademicsPortfolioStatus( { 
		async: true,
		errorHandler:handleError,
		callback:function(data)
		{
			window.location.href="certification.do";
		}});
		
	}
	else
	{
		chkCallFrom=1;
		insertOrUpdate(1);	
		return false;
	}
	
	
}

function setDefColortoErrorMsg()
{
	$('#divServerError').css("background-color","");	
	$('#degreeName').css("background-color","");
	$('#universityName').css("background-color","");
	$('#fieldName').css("background-color","");
	$('#attendedInYear').css("background-color","");
	$('#leftInYear').css("background-color","");
	$('#pathOfTranscriptFile').css("background-color","");	
	$('#gpaFreshmanYear').css("background-color","");
	$('#gpaJuniorYear').css("background-color","");
	$('#gpaSophomoreYear').css("background-color","");
	$('#gpaSeniorYear').css("background-color","");
	$('#gpaCumulative').css("background-color","");
	$('#gpaCumulative1').css("background-color","");
	$('#otherUniversityName').css("background-color","");
	//$('#otherDegreeName').css("background-color","");
	
}

function testAuto()
{
	
	//alert(document.getElementById("degreeType").value)
	//alert(">>"+document.getElementById("degreeId").value);
	DWRAutoComplete.getFieldOfStudyList("h", function(data)
	{
		alert(data);
	});
}


function uploadFiles() 
{
  var image = document.getElementById('imgToUpload');
  PFAcademics.uploadFiles(image,{ 
		async: true,
		errorHandler:handleError,
		callback:function(data) {
  }});
}

function clearTranscript()
{
	// line added by ashish ratan for IE 10-11
	// replace field with old html
	// comment by ashish ratan
	$('#pathOfTranscriptFile')
			.replaceWith(
					"<input id='pathOfTranscriptFile' name='pathOfTranscriptFile' type='file' width='20px;' />");

	document.getElementById("pathOfTranscriptFile").value=null;
	$('#pathOfTranscriptFile').css("background-color","");	
}
function chkForEnter(evt)
{	
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		insertOrUpdate();
	}	
}

function checkGED(){
	if($('#degreeName').val()=="High School or GED"){
		//document.getElementById('universityName').disabled=true;
		document.getElementById('fieldName').disabled=true;
		//document.getElementById('gpaCumulative1').disabled=true;
	}else{
		document.getElementById('universityName').disabled=false;
		document.getElementById('fieldName').disabled=false;
		document.getElementById('gpaCumulative1').disabled=false;
	}
}

function showHideOtherUniversity()
{
	var universityName = document.getElementById("universityName");
	if(resourceJSON.platformName=="NC" && trim(universityName.value)=="Other")
	{
		$("#otherUniversityName").show();
	}
	else
	{
		$("#otherUniversityName").hide();
	}	
}
function showHideOtherDegree()
{
	var degreeName = document.getElementById("degreeName");
	if(resourceJSON.platformName=="NC" && trim(degreeName.value)=="Other")
	{
		//$("#otherDegreeName").show();
	}
	else
	{
		//$("#otherDegreeName").hide();
	}	
}


