
function verify(){
	
	var loginFlag    =   $("[name='loginFlag']").val();
	var acceptordecline= $("[name='acceptordecline']").val();
	if (loginFlag==1 && acceptordecline==1){
		openLogInModal();
	}else if(loginFlag==0 ){
		var offerUrl    =   $("[name='offerUrl']").val();
		window.location.href=offerUrl;
	}
	
}
function verifyONB(){
		var offerUrl    =   $("[name='offerUrl']").val();
		window.location.href=offerUrl;
}
function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}
function validateCandidate(){
	$('#errordiv').html('');
	$('#errordiv').hide();
	var checkingLogin=true;
		var emailAddress    =   $("[name='emailAddress']").val();
		var loginEmailAddress=$('[name="loginEmailAddress"]').val();
		var password=$('[name="password"]').val();
		//alert("emailAddress:::"+emailAddress+"  loginEmailAddress::"+loginEmailAddress+" password::"+password);
		if(loginEmailAddress == ""){
		$('#errordiv').append('&#149; '+resourceJSON.PlzEtrEmail+' <br/>');
		checkingLogin=false;
		$('#errordiv').show();
		}if(password== ""){
		$('#errordiv').append('&#149; '+resourceJSON.msgPlgEtrPass+' <br/>');
		checkingLogin=false;
		$('#errordiv').show();
		}
		if(checkingLogin){
		if(isEmailAddress(loginEmailAddress))
			if(loginEmailAddress.trim()==emailAddress.trim()){
				var contextPath=$('[name="contxtPath"]').val();
				$.ajax({
				url: contextPath+"/service/checkSignin.do",
				type : 'POST',
		        async: false,
		        beforeSend: function() {$('#loadingDiv').show()},
            	complete: function() {$('#loadingDiv').hide()},
		        data : {'emailAddress' : loginEmailAddress,'password':password},
		        success:function(data){
		        	//alert("data:::"+data);
					if(data.trim()==1){
					//alert('eeeeeeeeeeeeee');
						getQQDistrictSpecificQuestionONB($("[name='jobId']").val(),$("[name='teacherId']").val());
		        		//$('#loginCandidateModal #confirmMessage').html("#########################");
		        	}else{
		        		$('#errordiv').append('&#149; '+resourceJSON.msgLogingAuthenticationYouerAccout3 );
		        		$('#errordiv').show();
		        	}
		        }
				});
			}else{
				$('#errordiv').append('&#149; '+resourceJSON.msgLogingAuthenticationYouerAccout3 +'<br/>');
				$('#errordiv').show();
			}
		else{
		$('#errordiv').append('&#149; '+resourceJSON.msgValidEmailAddress +' <br/>');
		$('#errordiv').show();
		}
		}
}

var totalQQQuestions=0;
function getQQDistrictSpecificQuestionONB(jobId,teacherId)
{	
	try{
	AssessmentCampaignAjax.getQQDistrictSpecificQuestionONB(jobId,teacherId,{
	preHook:function(){$('#loadingDiv').show()},
	postHook:function(){$('#loadingDiv').hide()},
	async: true,
	errorHandler:handleError,
	callback: function(data)
	{
		var dataArray = data.split("@##@");	
		totalQQQuestions = dataArray[1];
		if(dataArray[0]!=null && dataArray[0]!='' && totalQQQuestions!=0)
		{
			try
			{		
			var errorDiv="<div class='divErrorMsg' id='errordiv4question' style='display: block;padding-bottom:5px;'></div>"+
						"<div class='divErrorMsg' id='divServerError' style='display: block;'></div>";			             
				$('#loginCandidateModal .modal-dialog').css('width','850px');
				$('#loginCandidateModal .modal-body').css({'max-height': '500px','overflow-y':'scroll'});							
				$('#loginCandidateModal #confirmMessage').html(errorDiv+dataArray[0]);
				$('#loginCandidateModal .modal-footer').html("<div style='float:right;'><button class='btn fl btn-primary' type='button' id='continue' onclick='return setDistrictQuestions(\"notApply\");'>Continue<i class='icon'/></button></div>");
			}catch(err)
			{}
		}
		else
		{		
		}

	}});
	}catch(e){alert(e);}	
	
}

function setDistrictQuestions(fromWhere)
{
	$('#errordiv4question').empty();
	var arr =[];
	var jobOrder = {jobId:$("[name='jobId']").val()};
	var totalQuestions= $("[name='totalQuestions']").val();
	for(i=1;i<=totalQuestions;i++)
	{
		var districtSpecificQuestion = {questionId:dwr.util.getValue("Q"+i+"questionId")};
		var questionTypeShortName = dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionTypeMaster = {questionTypeId:dwr.util.getValue("Q"+i+"questionTypeId")};
		var qType = dwr.util.getValue("Q"+i+"questionTypeShortName");
		var questionSets = {ID:dwr.util.getValue("qqQuestionSetID")};
		if(qType=='tf' || qType=='slsel')
		{
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 )
			{
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			}else
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				document.getElementById("errordiv4question").scrollIntoView();
				return;
			}
			
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificQuestions" : districtSpecificQuestion,
				"questionSets"	: questionSets,
				"isValidAnswer" : dwr.util.getValue("Q"+optId+"validQuestion"),
				"jobOrder" : jobOrder,
			});

		}else if(qType=='ml')
		{
			var insertedText = dwr.util.getValue("Q"+i+"opt");
			if(insertedText!=null && insertedText!="")
			{
				arr.push({ 
					"insertedText"    : insertedText,
					"question"  : dwr.util.getValue("Q"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"districtSpecificQuestions" : districtSpecificQuestion,
					"questionSets"	: questionSets,
					"jobOrder" : jobOrder
				});
			}else
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				document.getElementById("errordiv4question").scrollIntoView();
				return;
			}
		}else if(qType=='et')
		{
			var optId="";

			if($("input[name=Q"+i+"opt]:radio:checked").length > 0 )
			{
				optId=$("input[name=Q"+i+"opt]:radio:checked").val();
			}else
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				document.getElementById("errordiv4question").scrollIntoView();
				return;
			}
			
			var insertedText = dwr.util.getValue("Q"+i+"optet");
			var isValidAnswer = dwr.util.getValue("Q"+optId+"validQuestion");

			if(isValidAnswer=="false" && insertedText.trim()=="")
			{
				$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
				document.getElementById("errordiv4question").scrollIntoView();
				return;
			}
				
			arr.push({ 
				"selectedOptions"  : optId,
				"question"  : dwr.util.getValue("Q"+i+"question"),
				"insertedText"    : insertedText,
				"questionTypeMaster" : questionTypeMaster,
				"questionType" : questionTypeShortName,
				"questionOption" : dwr.util.getValue("qOpt"+optId),
				"districtSpecificQuestions" : districtSpecificQuestion,
				"questionSets"	: questionSets,
				"isValidAnswer" : isValidAnswer,
				"jobOrder" : jobOrder,
			});
		}
	}
	if(arr.length==totalQuestions)
	{
		var teacherId=$('[name="teacherId"]').val();
		AssessmentCampaignAjax.setDistrictQuestionsBySetONB(arr,questionSets,teacherId,{
		//AssessmentCampaignAjax.setDistrictQuestions(arr,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data){
			if(data!=null)
			{
				if(data==1){
					 verifyONB();
				}
			}
		}
		});	
	}else
	{
		$("#errordiv4question").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
		document.getElementById("errordiv4question").scrollIntoView();
		$("#errordiv4question").show();
		return;
	}
}

//add By Ram Nath
function openLogInModal(){
	var div="<div class='modal hide in' id='loginCandidateModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>"+
			"<div class='modal-dialog' style='width:450px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<button type='button' class='close' data-dismiss='modal' aria-label='Close' onclick='window.location.href=\"logout.do\"'><span aria-hidden='true'>&times;</span></button>"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'>"+resourceJSON.headTm+"</h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	    
	      "<div class='row'>"+  
                          "<div class='col-md-offset-1'>"+
   							         "<div class='col-sm-10'>"+			                         
									 "<div class='divErrorMsg' id='errordiv' style='display: block;'></div>"+
									 "<div class='divErrorMsg' id='divServerError' style='display: block;'></div>"+			             
				   					 "</div>"+  
                         "</div></div>"+
                          "<div class='row top10'>"+
						 "<div class='col-md-offset-1'>"+ 						                     
                                   "<div class='col-sm-10'>"+
                                        "<label>"+resourceJSON.email+"</label>"+
                                   	    "<input type='text' name='loginEmailAddress' value='' placeholder='Enter your mail' class='form-control'  />"+ 
                                   "</div>"+  
                          "</div></div>"+  
                          "<div class='row top-12'>"+      
                          "<div class='col-md-offset-1'>"+            
                                  "<div class='col-sm-7' style=''>"+
                                        "<label>"+resourceJSON.msgPassword+"</label>"+
                                        "<input type='password' name='password' placeholder='Enter your password' class='form-control' maxlength='12' />"+
                                  "</div>"+
                                  "<div class='col-sm-5 top25' style='padding:0px;'>"+
                                  	"<button class='btn fl btn-primary' style=''  type='submit'"+ 
                                  	"id='submitLogin' onclick='return validateCandidate();'>"+resourceJSON.btnLogin+" <i class='icon'></i></button>"+
                                  "</div>"+                                                                
                          "</div></div>"+    
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#myModalVerify').after(div);
    confirmMessage = confirmMessage || '';
    $('#confirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });

    $('#loginCandidateModal').fadeIn();
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
	return true;
	if(!(charCode >= 48 && charCode <= 57)||(charCode==13)){
		return false;
	}
	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}
function getQuestionEvent(txtID,hiddenIDExplanation)
{
	document.getElementById(txtID).style.display='none';
	var hiddenExplanationValue=document.getElementById(hiddenIDExplanation).value;
	if(trim(hiddenExplanationValue)=="true"){
	document.getElementById(txtID).style.display='block';
	}
}