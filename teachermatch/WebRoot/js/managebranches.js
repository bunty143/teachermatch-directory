/* @Author: Ankit Sharma
 * @Discription: view of edit branch js.
*/
var noOfRows = 50;
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
var page = 1;
var noOfRows = 50;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	ShowDistrict();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	ShowDistrict();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
/*======= Show District on Press Enter Key ========= */
function chkForEnterShowDistrict(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		ShowDistrict();
	}	
}


//Search Records By EntityType

function DisplayHideSearchBox(entityId){
	var entity_type	=	document.getElementById("MU_EntityType").value; 
	
	if(entityId==1)
	{
		if(entity_type == 1)
		{
			$("#branchId").val("");
			$("#branchName").val("");
			$("#onlyHeadQuarterName").val("");
			$("#headQuarterHiddenId").val("");
			$("#headQuarterId").val("");
			$("#Searchbox").fadeIn();
			$('#SearchBranchTextboxDiv').fadeIn();
			$('#SearchHeadQuarterTextboxDiv').fadeIn();
		
		}
		
		if(entity_type == 5)
		{
			$("#branchId").val("");
			$("#branchName").val("");
			$("#onlyHeadQuarterName").val("");
			$("#headQuarterHiddenId").val("");
			$("#headQuarterId").val("");
			$("#Searchbox").fadeIn();
			$('#SearchHeadQuarterTextboxDiv').fadeIn();
			$('#SearchBranchTextboxDiv').hide();
			
		
		}
		if(entity_type == 6)
		{
			document.getElementById("branchId").value=0;
			$("#Searchbox").fadeIn();
			$('#SearchBranchTextboxDiv').hide();
			$('#SearchHeadQuarterTextboxDiv').hide();
		
		}
		
	}	
	else
	{
		if(entity_type ==2)
		{
			$("#Searchbox").fadeIn();
			$('#SearchTextboxDiv').fadeIn();
			$('#SearchBranchTextboxDiv').hide();
			$('#districtName').focus();
		}
		else if(entity_type ==1)
		{
			document.getElementById("branchId").value=0;
			$("#Searchbox").fadeIn();
			$('#SearchHeadQuarterTextboxDiv').fadeIn();
			$('#SearchBranchTextboxDiv').fadeIn();
			
		}
		else if(entity_type == 5)
		{
			$("#Searchbox").fadeIn();
			$('#SearchBranchTextboxDiv').hide();
			document.getElementById("branchId").value="";
			$('#SearchHeadQuarterTextboxDiv').hide();
		}
		else if(entity_type == 6)
		{
			$("#Searchbox").fadeIn();
			$('#SearchBranchTextboxDiv').fadeIn();
			$('#SearchTextboxDiv').hide();
			$('#SearchHeadQuarterTextboxDiv').hide();
			$('#branchName').focus();
		}	
		else{
			document.getElementById("districtName").value="";
			$("#Searchbox").fadeIn();
			$("#SearchTextboxDiv").hide();
			$('#SearchHeadQuarterTextboxDiv').hide();
		}
	}
	
}

function onLoadDisplayHideSearchBox(entity_type){	
	if(entity_type !=6){
		if(entity_type ==2){
			$("#Searchbox").hide();
			document.getElementById("MU_EntityType").value=2;
		}
		else if(entity_type ==5) // for headquarter
		{
			$('#SearchTextboxDiv').hide();
			document.getElementById("MU_EntityType").value=5;
			$('#SearchBranchTextboxDiv').hide();
		}
		else if(entity_type ==1){
			$("#Searchbox").fadeIn();
			$('#SearchHeadQuarterTextboxDiv').fadeIn();
			$('#SearchBranchTextboxDiv').fadeIn();
			
			document.getElementById("MU_EntityType").value=1;
		}else{
			$("#Searchbox").fadeIn();
			//$('#SearchTextboxDiv').hide();
		}
	}
	ShowDistrict();
}

/*========  Show District ===============*/
function searchDistrict()
{	
	page = 1;
	ShowDistrict();
}

function ShowDistrict()
{	
	$('#loadingDiv').fadeIn();
	var entityType = $("#entityType").val();
	var entityID	=	document.getElementById("MU_EntityType").value;		
	var searchText	="";
	var branchId = "";
	var hqId = 0;
	if(entityType=="5")
	var status_filter=document.getElementById("status_filter").value;
	
	try{
		//searchText=document.getElementById("districtName").value;
	}catch(err){
		searchText="";
	}	
	if(entityID==5){
		hqId = document.getElementById("headQuarterId").value;
	}
	else if(entityType==5 && entityID==6){
		//hqId = document.getElementById("headQuarterId").value;
		branchId = document.getElementById("branchId").value;
	}
	else if(entityType==6 && entityID==6){
		hqId = document.getElementById("headQuarterId").value;
		//branchId = document.getElementById("branchMasterId").value;
	}
	HeadQuarterAjax.SearchBranchRecords(entityID,searchText,noOfRows,page,sortOrderStr,sortOrderType,branchId,hqId,status_filter,{
		async: true,
		callback: function(data)
		{
			$('#divMain').html(data);
			applyScrollOnTbl();
			$('#loadingDiv').hide();			
		},
		errorHandler:handleError 
	});

}

function delay(sec){
	var starttime = new Date().getTime();
	starttime = starttime+sec;
	while(true){
		if(starttime< new Date().getTime()){
			break;
		}
	}
}

function activateDeactivateBranch(branch,status)
{
	document.getElementById("actbranch").value=branch;
	document.getElementById("branchstat").value=status;
	if(status=="I")
	{
		$('#myModalactMsgShow').modal('show');
	}
	
	else
	{
		toggleStatus();	
	}
}

function toggleStatus()
{
	var branch=document.getElementById("actbranch").value;
	var status=document.getElementById("branchstat").value;
	HeadQuarterAjax.activateDeactivateBranch(branch,status, { 
		async: true,
		callback: function(data)
		{		
			ShowDistrict();
		},
		errorHandler:handleError });	
}




//===========================================================


var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	DistrictAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
		errorHandler:handleError 
	});	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function mouseOverChk(txtdivid,txtboxId)
{	
	for(var i=0;i<length;i++)
	{
		$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
	}
}


function fireMouseOverEvent(event)
{
	for(var i=0;i<length;i++)
	{	
		document.getElementById('divResult'+event.data.param2+i).className='normal';
	}
    document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
   	document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
    index=event.data.param1;
}

function addnewBranch(url)
{
	//var currentdate = new Date();
	//var datetime =  currentdate.getDay()+""+currentdate.getMonth()+""+currentdate.getFullYear()+""+currentdate.getHours()+""+currentdate.getMinutes()+""+currentdate.getSeconds();
	url=url;
	window.location.href=url;
}
function setHeadQuarterId()
{
	$("#headQuarterId").val($("#headQuarterHiddenId").val());
	$("#branchName").val("");
	$("#branchId").val("");
	
}

function updateMsg(authKeyVal,entityType)
{
	if(authKeyVal!="0"){
		if(entityType==5){
			document.getElementById("updateMsg").innerHTML="Branch details has been updated successfully.";
			$('#myModalUpdateMsg').modal('show');
		}else if(entityType==6){
			document.getElementById("updateMsg").innerHTML="Branch details has been updated successfully.";
			$('#myModalUpdateMsg').modal('show');
		}
		
	}
}


function ExportKellyBranchToExcel(){
	$('#loadingDiv').fadeIn();
	var entityType  = $("#entityType").val();
	var entityID	=	document.getElementById("MU_EntityType").value;		
	var searchText	="";
	var branchId = "";
	var hqId = 0;
	var status_filter=document.getElementById("status_filter").value;	
	if(entityID==5){
		hqId = document.getElementById("headQuarterId").value;
	}else if(entityType==5 && entityID==6){
		branchId = document.getElementById("branchId").value;
	}else if(entityType==6 && entityID==6){
		hqId = document.getElementById("headQuarterId").value;
	}	
	HeadQuarterAjax.ExportKellyBranchToExcel(hqId,branchId,status_filter,entityID,{
		async: true,
		callback: function(data)
		{		
		    $('#loadingDiv').hide();		   
			if(deviceType)
			{			
					$("#exelfileNotOpen").css({"z-index":"3000"});
			    	$('#exelfileNotOpen').show();					
			}
			else
			{
				try
				{
					document.getElementById('ifrmTrans').src = "manageBranches/"+data+"";
				}
				catch(e)
				{}
			}
		},
	errorHandler:handleError
	});
}

	function ExportKellyBranchesToPDF()
	{	
		$('#loadingDiv').fadeIn();
		var entityType  = $("#entityType").val();
		var entityID	=	document.getElementById("MU_EntityType").value;		
		var searchText	="";
		var branchId = "";
		var hqId = 0;
		var status_filter=document.getElementById("status_filter").value;	
		if(entityID==5){
			hqId = document.getElementById("headQuarterId").value;
		}else if(entityType==5 && entityID==6){
			branchId = document.getElementById("branchId").value;
		}else if(entityType==6 && entityID==6){
			hqId = document.getElementById("headQuarterId").value;
		}	
		HeadQuarterAjax.ExportKellyBranchesToPDF(hqId,branchId,status_filter,entityID,{
			async: true,
			callback: function(data)
			{
			$('#loadingDiv').hide();	
			if(deviceType || deviceTypeAndroid)
			{
				window.open(data,'left=200,top=50,width=700,height=600,toolbar=1,resizable=0');			
			}else
			{				
				$('#modalDownloadHiredCandidates').modal('hide');
				document.getElementById('ifrmCJS').src = ""+data+"";	
				$('#modalDownloadHiredCandidates').modal('show');
		    }		
		     return false;
	}});
}

	function KellyManageBranchesPrintPre()
	{
		$('#loadingDiv').fadeIn();
		var flag="0";
		var entityType  = $("#entityType").val();
		var entityID	=	document.getElementById("MU_EntityType").value;		
		var searchText	="";
		var branchId = "";
		var hqId = 0;
		var status_filter=document.getElementById("status_filter").value;	
		if(entityID==5){
			hqId = document.getElementById("headQuarterId").value;
		}else if(entityType==5 && entityID==6){
			branchId = document.getElementById("branchId").value;
		}else if(entityType==6 && entityID==6){
			hqId = document.getElementById("headQuarterId").value;
		}
		HeadQuarterAjax.KellyManageBranchesPrintPre(hqId,branchId,status_filter,entityID,flag,{
			async: true,
			callback: function(data)
			{	
				$('#loadingDiv').hide();
				$('#printhiredApplicantsDataTableDiv').html(data);
				applyScrollOnPrintTable();
				$("#printHiredApplicantsDiv").modal('show');
			},
		errorHandler:handleError
		});
	}
	
	function printKellyBranchDATA()
	{
		$('#loadingDiv').fadeIn();
		var flag="1";
		var entityType  = $("#entityType").val();
		var entityID	=	document.getElementById("MU_EntityType").value;		
		var searchText	="";
		var branchId = "";
		var hqId = 0;
		var status_filter=document.getElementById("status_filter").value;	
		if(entityID==5){
			hqId = document.getElementById("headQuarterId").value;
		}else if(entityType==5 && entityID==6){
			branchId = document.getElementById("branchId").value;
		}else if(entityType==6 && entityID==6){
			hqId = document.getElementById("headQuarterId").value;
		}
		HeadQuarterAjax.KellyManageBranchesPrintPre(hqId,branchId,status_filter,entityID,flag,{	
			async: true,
			callback: function(data)
			{	
			$('#loadingDiv').hide();
			try{ 
				if (isSafari && !deviceType)
				    {
				    	window.document.write(data);
						window.print();
				    }else
				    {
				    	var newWindow = window.open();
				    	newWindow.document.write(data);	
				    	newWindow.print();
					 } 
				}catch (e) 
				{					
					$('#printmessage1').modal('show');							 
				}
			},
		errorHandler:handleError
		});
	}