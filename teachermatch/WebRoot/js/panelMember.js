/* @Author: Gagan 
 * @Discription: view of edit Panel js.
*/

/* =====================================   Panel Member js Method [ Start ] =========================================*/

/*========  For Sorting  ===============*/
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";
function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayPanelMember();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayPanelMember();
}
/*========  For Handling session Time out Error ===============*/
function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert(""+resourceJSON.msgServerErr+": "+exception.javaClassName);}
}
/*======= Save Panel on Press Enter Key ========= */
function chkForEnterSavePanel(evt)
{
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==13)
	{
		savePanelMember();
	}	
}


/*========  displayPanelMember ===============*/
function displayPanelMember()
{
	//var districtId	=	$("#districtId").val();
	var panelId			=	trim(document.getElementById("panelId").value);
	//alert(" panelId "+panelId);
	PanelAjax.displayPanelMember(panelId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: true,
		callback: function(data){
		$('#panelMemberGrid').html(data);
		applyScrollOnTbl();
	},
	errorHandler:handleError  
	});
}
/*========  Add New Panel===============*/
function addNewPanelMember()
{
	//var districtName= $("districtName").val();
	//alert(districtName+"  DDADUTADAD "+$("districtName").val());
	//$("distName").val($("districtName").val());
	//dwr.util.setValues({ panelMemberId:null,panelMemberName:null,multiplier:null,panelMemberUId:null});
	document.getElementById("divPanelForm").style.display	=	"block";
	$('#schoolName').focus();
	//document.getElementById("schoolName").focus();
	$('#errorpanelMemberdiv').hide();
	$('#schoolName').css("background-color", "");
	$('#memberId').css("background-color", "");
	$('#schoolName').val("");
	$('#schoolId').val("0");
	document.getElementById("memberOption").selected=true;
	//$('#displayInPDReport').attr('checked', false); // will uncheck the checkbox with id displayInPDReport
	document.getElementById("divDone").style.display		=	"block";
	document.getElementById("divManage").style.display		=	"none";
	return false;
}
/*========  Clear Panel Fields ===============*/
function clearPanelMember()
{
	document.getElementById("divPanelForm").style.display	=	"none";
	document.getElementById("divDone").style.display		=	"none";
	document.getElementById("divManage").style.display		=	"none";
	document.getElementById("panelMemberId").value="";
	//document.getElementById("panelMemberName").value		=	"";
	//document.getElementsByName("panelMemberStatus")[0].checked	=	true;
}
/*========  Save Panel  ===============*/
function savePanelMember()
{
	$('#errorpanelMemberdiv').empty();
	$('#schoolName').css("background-color", "");
	$('#memberId').css("background-color", "");
	var panelMemberId	=	trim(document.getElementById("panelMemberId").value);
	var panelId			=	trim(document.getElementById("panelId").value);
	var districtId		=	trim(document.getElementById("districtId").value);
	var schoolId		=	trim(document.getElementById("schoolId").value);
	var schoolName		=	trim(document.getElementById("schoolName").value);
	var memberId		=	trim(document.getElementById("memberId").value);
	var counter			=	0;
	var focusCount		=	0;
	//alert("schoolId "+schoolId+" memberId "+memberId);
	//return false;
	/*if (domainMaster	==	0)
	{
		$('#errordiv').show();
		$('#errordiv').append("&#149; Please select Domain Name<br>");
		if(focusCount	==	0)
			$('#domainMaster').focus();
		$('#domainMaster').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
	}*/
	if (schoolId=="" && schoolName!="")
	{
		//document.getElementById('errorpanelMemberdiv').innerHTML		=	'&#149; Please enter valid School Name';
		$('#errorpanelMemberdiv').show();
		$('#errorpanelMemberdiv').append("&#149; "+resourceJSON.msgentervalidSchoolName+"<br>");
		if(focusCount	==	0)
			document.getElementById("schoolName").focus();
		$('#schoolName').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		//return false;
	}
	
	if (memberId=="")
	{
		///document.getElementById('errorpanelMemberdiv').innerHTML		=	'&#149; Please select Member';
		$('#errorpanelMemberdiv').show();
		$('#errorpanelMemberdiv').append("&#149; "+resourceJSON.PlzSelectMember+"<br>");
		if(focusCount	==	0)
			document.getElementById("memberId").focus();
		$('#memberId').css("background-color", "#F5E7E1");
		counter++;
		focusCount++;
		
		//return false;
	}
	if(counter	==	0)
	{
		dwr.engine.beginBatch();		
		PanelAjax.savePanelMember(panelMemberId,panelId,districtId,schoolId,memberId,{ 
			async: true,
			callback: function(data)
			{
				//alert(" data "+data);
				if(data==3)
				{
					document.getElementById('errorpanelMemberdiv').innerHTML	=	'&#149;'+resourceJSON.PlzSelectUniqueMember;
					$('#errorpanelMemberdiv').show();
					//$('#panelMemberName').css("background-color", "#F5E7E1");
					//$('#panelMemberName').focus();
				}else
				{
					displayPanelMember();
					clearPanelMember();
				}
			},
			errorHandler:handleError 
		});
		dwr.engine.endBatch();
		return true;
	}
	else
	{
		$('#errorpanelMemberdiv').show();
		return false;
	}
}
/*========  deletePanel ===============*/
function deletePanelMember(panelMemberId)
{
	//alert(" delete Panel : panelMemberId : "+panelMemberId);
	$("#panelMemberId").val(panelMemberId);
		$('#deletePanelDiv').modal('show');
				
}
function deleteconfirm()
{
	var panelMemberId=$("#panelMemberId").val();
	
	PanelAjax.deletePanelMember(panelMemberId, { 
		async: true,
		callback: function(data)
		{
			$('#deletePanelDiv').modal('hide');
			displayPanelMember();
			clearPanelMember();
		},
		errorHandler:handleError 
	});
				
}

function go()
{
	//alert(" Go went gone ");
	window.location.href="panel.do";
}


/*========  Edit Panel ===============*/
/*function editPanelMember(panelMemberId)
{
	//alert(" panelMemberId "+panelMemberId);
	$('#errorpanelMemberdiv').hide();
	$('#panelMemberName').css("background-color", "");
	//var displayInPDReport									=	document.getElementById("displayInPDReport");
	document.getElementById("divDone").style.display		=	"none";
	document.getElementById("divPanelForm").style.display	=	"block";
	//var panelMemberStatus	=	document.getElementsByName("panelMemberStatus");
	
	$('#panelMemberName').focus();
		PanelAjax.editPanelMember(panelMemberId, { 
			async: true,
			callback: function(data)
			{
				dwr.util.setValues(data);
			},
		errorHandler:handleError 
		});
	
	document.getElementById("divManage").style.display="block";
	return false;
}*/

function getActiveUsers()
{
	var schoolId		=	trim(document.getElementById("schoolId").value);
	var schoolName		=	trim(document.getElementById("schoolName").value);
	var districtId		=	trim(document.getElementById("districtId").value);
	//alert(" schoolId : "+schoolId);
	if(schoolId=="")
		schoolId=0;
	//if(schoolId!="" && schoolId>0)
	//{
		PanelAjax.getUserBySchool(districtId,schoolId, { 
			async: true,
			callback: function(data)
			{
				//alert(" data : "+data);
				$("#memberId").html(data);
				//dwr.util.setValues(data);
			},
		errorHandler:handleError 
		});
	//}
	
}


///////////////////////////////////////////////////////////////////////////////////////////////
var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
hiddenId=hiddenId;
divid=txtdivid;
txtid=txtSearch.id;
if(event.keyCode==40){
downArrowKey(txtdivid);
} 
else if(event.keyCode==38) //up key
{
upArrowKey(txtdivid);
} 
else if(event.keyCode==13) // RETURN
{
if(document.getElementById(divid))
document.getElementById(divid).style.display='block';

document.getElementById("districtName").focus();

} 
else if(event.keyCode==9) // Tab
{

}
else if(txtSearch.value!='')
{
index=-1;
length=0;
document.getElementById(divid).style.display='block';
searchArray = getDistrictArray(txtSearch.value);
fatchData(txtSearch,searchArray,txtId,txtdivid);

}
else if(txtSearch.value==""){
document.getElementById(divid).style.display='none';
}
}

function getDistrictArray(districtName){
var searchArray = new Array();
BatchJobOrdersAjax.getFieldOfDistrictList(districtName,{ 
async: false,
callback: function(data){
hiddenDataArray = new Array();
showDataArray = new Array();
for(i=0;i<data.length;i++){
searchArray[i]=data[i].districtName;
showDataArray[i]=data[i].districtName;
hiddenDataArray[i]=data[i].districtId;
}
},
errorHandler:handleError
});	
return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
var result = document.getElementById(txtdivid);
try{
result.style.display='block';
result.innerHTML = '';
var items='';
count=0;
var len=searchArray.length;
if(document.getElementById(txtId).value!="")
{

for(var i=0;i<len;i++){
items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
searchArray[i].toUpperCase() + "</div>";
count++;
length++;

if(count==10)
break;

}

}
else {

}
if(count!=0)
result.innerHTML = items;
else{
result.style.display='none';
selectFirst="";
}
if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
{
document.getElementById('divResult'+txtdivid+0).className='over';
selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
}

scrolButtom();
}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
if(document.getElementById("districtName").value==""){
document.getElementById('schoolName').readOnly=true;
document.getElementById('schoolName').value="";
document.getElementById('schoolId').value="0";
}
document.getElementById("districtId").value="";
if(parseInt(length)>0){
if(index==-1){
index=0;
}
if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
document.getElementById(hiddenId).value=hiddenDataArray[index];
}
if(dis.value==""){
document.getElementById(hiddenId).value="";
}
else if(showDataArray && showDataArray[index]){
dis.value=showDataArray[index];
document.getElementById("districtId").value=hiddenDataArray[index];
document.getElementById('schoolName').readOnly=false;
document.getElementById('schoolName').value="";
document.getElementById('schoolId').value="0";
}

}else{
if(document.getElementById(hiddenId))
document.getElementById(hiddenId).value="";
}

if(document.getElementById(divId))
{
document.getElementById(divId).style.display="none";
}
index = -1;
length = 0;
}
function getSchoolAuto(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
hiddenId=hiddenId;
divid=txtdivid;
txtid=txtSearch.id;
if(event.keyCode==40){
downArrowKey(txtdivid);
} 
else if(event.keyCode==38) //up key
{
upArrowKey(txtdivid);
} 
else if(event.keyCode==13) // RETURN
{
if(document.getElementById(divid))
document.getElementById(divid).style.display='block';

document.getElementById("schoolName").focus();

} 
else if(event.keyCode==9) // Tab
{

}
else if(txtSearch.value!='')
{
index=-1;
length=0;
document.getElementById(divid).style.display='block';
searchArray = getSchoolArray(txtSearch.value);
fatchData2(txtSearch,searchArray,txtId,txtdivid);

}
else if(txtSearch.value==""){
document.getElementById(divid).style.display='none';
}
}

function getSchoolArray(schoolName){
var searchArray = new Array();
var districtId	=	document.getElementById("districtId").value;
BatchJobOrdersAjax.getFieldOfSchoolList(districtId,schoolName,{ 
async: false,
callback: function(data){
hiddenDataArray = new Array();
showDataArray = new Array();
for(i=0;i<data.length;i++){
searchArray[i]=data[i].schoolName;
showDataArray[i]=data[i].schoolName;
hiddenDataArray[i]=data[i].schoolMaster.schoolId;
}
},
errorHandler:handleError
});	
return searchArray;
}


selectFirst="";

var fatchData2= function(txtSearch,searchArray,txtId,txtdivid){

var result = document.getElementById(txtdivid);
try{
result.style.display='block';
result.innerHTML = '';
var items='';
count=0;
var len=searchArray.length;
if(document.getElementById(txtId).value!="")
{

for(var i=0;i<len;i++){
items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
searchArray[i].toUpperCase() + "</div>";
count++;
length++;

if(count==10)
break;

}

}
else {

}
if(count!=0)
result.innerHTML = items;
else{
result.style.display='none';
selectFirst="";
}
if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
{
document.getElementById('divResult'+txtdivid+0).className='over';
selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
}

scrolButtom();
}catch (err){}
}
function hideSchoolMasterDiv(dis,hiddenId,divId)
{
	document.getElementById("schoolId").value="";
	if(parseInt(length)>0){
	if(index==-1){
	index=0;
	}
	if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
	document.getElementById(hiddenId).value=hiddenDataArray[index];
	}
	if(dis.value==""){
		//document.getElementById(hiddenId).value="";
		document.getElementById("schoolId").value="";
	}
	else if(showDataArray && showDataArray[index]){
	dis.value=showDataArray[index];
	document.getElementById("schoolId").value=hiddenDataArray[index];
	
	}
	
	}else{
	if(document.getElementById(hiddenId))
		//alert(" Spring Roll Back Exception ");
		//document.getElementById(hiddenId).value="";
		document.getElementById("schoolId").value="";
	}
	
	if(document.getElementById(divId))
	{
	document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
	
	//alert(" hiddenDataArray[index] "+hiddenDataArray[index]);
	getActiveUsers();
}




var downArrowKey = function(txtdivid){
if(txtdivid)
{
if(index<length-1){
for(var i=0;i<10;i++){
{
if(document.getElementById('divResult'+txtdivid+i))
var div_id=document.getElementById('divResult'+txtdivid+i);
if(div_id)
{
if(div_id.className=='over')
index=div_id.id.split('divResult'+txtdivid)[1];
div_id.className='normal';
}
}
}
index++;
if(document.getElementById('divResult'+txtdivid+index))
{
var div_id=document.getElementById('divResult'+txtdivid+index);
div_id.className='over';
document.getElementById(txtid).value = div_id.innerHTML;
selectFirst=div_id.innerHTML;
}
}
}
}

var upArrowKey = function(txtdivid){
if(txtdivid)
{
if(index>0){
for(var i=0;i<length;i++){

var div_id=document.getElementById('divResult'+txtdivid+i);
if(div_id)
{
if(div_id.className=='over')
index=div_id.id.split('divResult'+txtdivid)[1];
div_id.className='normal';
}
}
index--;
if(document.getElementById('divResult'+txtdivid+index))
document.getElementById('divResult'+txtdivid+index).className='over';
if(txtid && document.getElementById('divResult'+txtdivid+index))
{
document.getElementById(txtid).value=
document.getElementById('divResult'+txtdivid+index).innerHTML;
selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
}
}
}
}

var overText=function (div_value,txtdivid) {

for(var i=0;i<length;i++)
{
if(document.getElementById('divResult'+i))
{
var div_id=document.getElementById('divResult'+i);

if(div_id.className=='over')
index=div_id.id.split(txtdivid)[1];
div_id.className='normal';
}
}
div_value.className = 'over';
document.getElementById(txtid).value= div_value.innerHTML;
}
function mouseOverChk(txtdivid,txtboxId)
{	
for(var i=0;i<length;i++)
{
$('#divResult'+txtdivid+i).mouseover({param1: i,param2:txtdivid, param3:txtboxId}, fireMouseOverEvent);
}
}


function fireMouseOverEvent(event)
{
for(var i=0;i<length;i++)
{	
document.getElementById('divResult'+event.data.param2+i).className='normal';
}
document.getElementById('divResult'+event.data.param2+event.data.param1).className='over';	       
document.getElementById(event.data.param3).value= $('#divResult'+event.data.param2+event.data.param1).text();
index=event.data.param1;
}

