var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
		noOfRows = document.getElementById("pageSize").value;
		displayDomain();
}

/*function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayDomain();
}*/
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayDomain();
}

function searchi4Ques()
{
	page = 1;
	displayDomain()
}


function displayDomain()
{
	var i4QuesStatus = document.getElementById("i4QuesStatus").value;
	var txtQuestion		=	document.getElementById("quesSearchText").value;
	var districtIdFilter		=	trim(document.getElementById("districtIdFilter").value);
	$("#loadingDiv").show();
	I4QuestionAjax.displayDomainRecords(noOfRows,page,sortOrderStr,sortOrderType,i4QuesStatus,txtQuestion,districtIdFilter,{ 
		async: true,
		callback: function(data){
		document.getElementById("i4QuesGrid").innerHTML=data;
		applyScrollOnTbl();
		document.getElementById("addQuesDiv").style.display="none";
		$("#loadingDiv").hide();
	},
	});
}


function saveQuestion()
{
	$('#errQuesdiv').empty();
	
	var quesId			=	trim(document.getElementById("quesId").value);
	var txtQuestion		=	trim(document.getElementById("txtQuestion").value);
	var districtId		=	trim(document.getElementById("districtId").value);
	var quesStatus		=	document.getElementsByName("quesStatus");
	var quesValue ="";	
	
	var counter				=	0;
	var focuscount			=	0;
	
	
	for(i=0;i<quesStatus.length;i++)
	{
		if(quesStatus[i].checked	==	true)
		{
			quesValue	=	quesStatus[i].value;
			break;
		}
	}
	
	
	if (districtId=="")
	{
		$('#errQuesdiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		//$('#errQuesdiv').show();
		$('#districtName').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	if (txtQuestion=="")
	{
		$('#errQuesdiv').append("&#149; "+resourceJSON.PlzEtrQuest+"");
	//	$('#errQuesdiv').show();
		$('#txtQuestion').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}else
	{
		if(txtQuestion.length>256)
		{
			document.getElementById('errQuesdiv').innerHTML		=	"&#149; "+resourceJSON.PlzEtrQuestLength+"";
			$('#errQuesdiv').show();
			$('#txtQuestion').css("background-color", "#F5E7E1");
			counter++;
			focuscount++;
		}
	}
	if(counter==0){
		$("#loadingDiv").show();
		I4QuestionAjax.saveI4Question(quesId,txtQuestion,quesValue,districtId,{ 
			async: true,
			callback: function(data)
			{
				if(data!=null)
				{
					if(data==1)
					{
						$('#errQuesdiv').show();
						$('#errQuesdiv').append("&#149; "+resourceJSON.MsgNotInsertDuplicateValue+"");
					}
					else
					{
						$('#errQuesdiv').hide();
						$('#errQuesdiv').empty();
						displayDomain();
						clearQues();
					}
				}
				$("#loadingDiv").hide();
			},
		});
	}
	
	
	
}

function clearQues()
{
	document.getElementById("txtQuestion").value="";
	document.getElementById("addQuesDiv").style.display="none";
	
	if(document.getElementById("entityType").value==1)
	{
		document.getElementById("districtId").value="";
		document.getElementById("districtName").value="";
	}
	document.getElementById("quesId").value="";
}

function addNewQues()
{
	document.getElementById("addQuesDiv").style.display="block";
	$("#errQuesdiv").empty();
	$( "span#i4QuesTxt" ).html("<textarea id='txtQuestion' ></textarea>");
	
	if(document.getElementById("entityType").value==1)
	{
		document.getElementById("districtId").value="";
		document.getElementById("districtName").value="";
	}
	$('#districtName').css("background-color", "");
	$('#txtQuestion').css("background-color", "");
	document.getElementById("districtName").disabled=false;
	document.getElementById("districtName").focus();
	
	var quesStatus		=	document.getElementsByName("quesStatus");
	
	quesStatus[0].disabled=false;
	quesStatus[1].disabled=false;
}

function editI4Question(quesId)
{
	$("#errQuesdiv").empty();
	var quesStatus		=	document.getElementsByName("quesStatus");
	$("#loadingDiv").show();
	I4QuestionAjax.editI4Question(quesId,{ 
		async: true,
		callback: function(data)
		{
			if(data!=null)
			{
				document.getElementById("addQuesDiv").style.display="block";
				$( "span#i4QuesTxt" ).html("<textarea id='txtQuestion' ></textarea>");
				
				document.getElementById("quesDate").value=data.dateCreated;
				document.getElementById("quesId").value=data.ID;
				document.getElementById("txtQuestion").value=data.questionText;
				
				 
				if(data.districtMaster!=null)
				document.getElementById("districtId").value=data.districtMaster.districtId;
				
				if(data.districtMaster!=null)
				document.getElementById("districtName").value=data.districtMaster.districtName;
				
				if(data.headQuarterMaster!=null)
					document.getElementById("headQuarterId").value=data.headQuarterMaster.headQuarterId;
					
				
				if(data.headQuarterMaster!=null)
				document.getElementById("headQuarterName").value=data.headQuarterMaster.headQuarterName;
				
				
				document.getElementById("districtName").disabled=true;
				if(data.status=='A')
					quesStatus[0].checked=true;
				else
					quesStatus[1].checked=true;
				
				quesStatus[0].disabled=true;
				quesStatus[1].disabled=true;
				
			}
			else{
				$("#removeQuesModal").modal('show');
			}
			
			$("#loadingDiv").hide();
		},
	});
}

function activateDeactivateQues(quesId,status)
{
	document.getElementById("addQuesDiv").style.display="block";
	$( "span#i4QuesTxt" ).html("<textarea id='txtQuestion' ></textarea>");
	$("#loadingDiv").show();
	I4QuestionAjax.activateDeactivateQuestion(quesId,status, { 
		async: true,
		callback: function(data)
		{
			if(data==true)
			{
				$("#removeQuesModal").modal('show');
			}
			displayDomain();
			clearQues();
			$("#loadingDiv").hide();
		},
	});
				
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}