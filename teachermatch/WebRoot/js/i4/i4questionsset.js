var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}else
	{
		page=1;
	}
		noOfRows = document.getElementById("pageSize").value;
		displayDomain();
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr	=	sortOrder;
	sortOrderType	=	sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayDomain();
}

function searchi4Ques()
{
	page = 1;
	displayDomain()
}


function displayDomain()
{
	var i4QuesSetStatus 	= document.getElementById("i4QuesSetStatus").value;
	var quesSetSearchText		=	document.getElementById("quesSetSearchText").value;
	var districtIdFilter		=	trim(document.getElementById("districtIdFilter").value);
	
	$("#loadingDiv").show();
	I4QuestionSetAjax.displayQuestionSet(noOfRows,page,sortOrderStr,sortOrderType,i4QuesSetStatus,quesSetSearchText,districtIdFilter,{ 
		async: true,
		callback: function(data){
		document.getElementById("i4QuesSetGrid").innerHTML=data;
		applyScrollOnTbl();
		document.getElementById("addQuesSetDiv").style.display="none";
		$("#loadingDiv").hide();
	},
	});
}


function saveQuestion()
{
	$('#errQuesSetdiv').empty();
	$('#quesSetName').css("background-color","");
	$('#districtName').css("background-color","");
	
	var quesSetId			=	trim(document.getElementById("quesSetId").value);
	var quesSetName			=	trim(document.getElementById("quesSetName").value);
	var districtId			=	trim(document.getElementById("districtId").value);
	var quesSetStatus		=	document.getElementsByName("quesSetStatus");
	var quesValue ="";	
	
	var counter				=	0;
	var focuscount			=	0;
	
	for(i=0;i<quesSetStatus.length;i++)
	{
		if(quesSetStatus[i].checked	==	true)
		{
			quesValue	=	quesSetStatus[i].value;
			break;
		}
	}
	
	
	if (districtId=="")
	{
		$('#errQuesSetdiv').append("&#149; "+resourceJSON.PlzEtrDistrict+"<br>");
		$('#errQuesSetdiv').show();
		$('#districtName').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	if (quesSetName=="")
	{
		$('#errQuesSetdiv').append("&#149; "+resourceJSON.PlzEtrQuest+"");
		$('#errQuesSetdiv').show();
		$('#quesSetName').css("background-color", "#F5E7E1");
		counter++;
		focuscount++;
	}
	
	if(counter==0)
	{
		$("#loadingDiv").show();
		I4QuestionSetAjax.saveI4QuestionSet(quesSetId,quesSetName,quesValue,districtId,{ 
			async: true,
			callback: function(data)
			{
				if(data==1)
				{
					$('#errQuesSetdiv').show();
					$('#errQuesSetdiv').append("&#149; "+resourceJSON.MsgNotInsertDuplicateQuestSet+"");
				}
				else
				{
					$('#errQuesSetdiv').hide();
					$('#errQuesSetdiv').empty();
					displayDomain();
					clearQues();
				}
				$("#loadingDiv").hide();
			},
		});
	}
	
	
	
}

function clearQues()
{
	document.getElementById("quesSetName").value="";
	document.getElementById("addQuesSetDiv").style.display="none";
	document.getElementById("quesSetId").value="";
}

function addNewQuesSet()
{
	document.getElementById("addQuesSetDiv").style.display="block";
	document.getElementById("quesSetName").value="";
	if(document.getElementById("entityType").value==1)
	{
		document.getElementById("districtId").value="";
		document.getElementById("districtName").value="";
	}
	$('#quesSetName').css("background-color", "");
	$('#districtName').css("background-color", "");
	
	$("#errQuesSetdiv").empty();

}

function editI4Question(quesSetId)
{
	$("#errQuesSetdiv").empty();
	document.getElementById("addQuesSetDiv").style.display="block";
	var quesSetStatus		=	document.getElementsByName("quesSetStatus");
	$("#loadingDiv").show();
	I4QuestionSetAjax.editI4QuestionSet(quesSetId,{ 
		async: true,
		callback: function(data)
		{
			if(data!=null)
			{
				document.getElementById("quesSetDate").value=data.dateCreated;
				document.getElementById("quesSetId").value=data.ID;
				document.getElementById("quesSetName").value=data.questionSetText;
				document.getElementById("districtName").value=data.districtMaster.districtName;
				document.getElementById("districtId").value=data.districtMaster.districtId;
				
				if(data.status=='A')
					quesSetStatus[0].checked=true;
				else
					quesSetStatus[1].checked=true;
			}
			$("#loadingDiv").hide();
		},
	});
}

function activateDeactivateQues(quesSetId,quesSetStatus)
{
	document.getElementById("addQuesSetDiv").style.display="block";
	//$( "span#i4QuesTxt" ).html("<textarea id='txtQuestion' ></textarea>");
	$("#loadingDiv").show();
	I4QuestionSetAjax.activateDeactivateQuestionSet(quesSetId,quesSetStatus, { 
		async: true,
		callback: function(data)
		{
			displayDomain();
			clearQues();
			$("#loadingDiv").hide();
		},
	});
				
}

function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}