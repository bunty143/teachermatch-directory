<%@ page import="java.text.NumberFormat" %>
<%@page import="java.util.Date"%>
<%
    NumberFormat nf = NumberFormat.getInstance();
    nf.setMaximumFractionDigits(2);
    nf.setMinimumFractionDigits(2);
%>

<%
			String sReturnOK="";
 			double totalMemoryMB=0.0;
		    double freeMemoryMB=0.0;
		    double usedmemoryMB =0.0;
		    
		    double totalMemoryGB=0.0;
		    double freeMemoryGB=0.0;
		    double usedmemoryGB =0.0;
		try 
		{
			// Get the Java runtime
		    Runtime runtime = Runtime.getRuntime();
		    // Run the garbage collector
		    //runtime.gc();
		    // Calculate the used memory
		    long totalMemory=runtime.totalMemory();
		    long freeMemory=runtime.freeMemory();
		    long usedmemory = totalMemory - freeMemory;
		    
		    totalMemoryMB=totalMemory/(1024*1024);
		    freeMemoryMB=freeMemory/(1024*1024);
		    usedmemoryMB = (totalMemory - freeMemory)/(1024*1024);
		    
		    totalMemoryGB=totalMemoryMB/1024;
		    freeMemoryGB=freeMemoryMB/1024;
		    usedmemoryGB = (totalMemoryGB - freeMemoryGB);
		    
		    /*System.out.println("Total Memory (Committed Heap): " + totalMemoryMB +" MB");
		    System.out.println("Total Used Memory (Used Heap): " + usedmemoryMB +" MB");
		    System.out.println("Total Free Memory : " + freeMemoryMB +" MB");*/
			
			System.out.println(new Date()+" Total Memory (Committed Heap): " + totalMemoryGB +" GB");
		    System.out.println("Total Used Memory (Used Heap): " + usedmemoryGB +" GB");
		    System.out.println("Total Free Memory : " + freeMemoryGB +" GB");
		    //6144
		    
		    long maxmemory = runtime.maxMemory();
		    double maxmemoryGB = maxmemory / 1024;
		    System.out.println("Total Max Memory : " + maxmemoryGB +" GB");
    
		    System.out.println("You are on server: [" + request.getServerName() + "]");
		    
		    if(totalMemoryMB>=12288)
			{
				sReturnOK="Status :Memory is greater than 12GB";
			}
			else if(totalMemoryMB>=10240)
			{
				sReturnOK="Status :Memory is greater than 10GB";
			}
			else
			{
				sReturnOK="Status :OK";
			}  
			
		} catch (Exception e) {
			// TODO: handle exception
		}
%>
			<%--
			Total Memory (Committed Heap): <%= nf.format(totalMemoryMB) %> MB</BR>
			Total Used Memory (Used Heap): <%= nf.format(usedmemoryMB) %> MB</BR>
			Total Free Memory : <%= nf.format(freeMemoryMB) %> MB</BR>
			<HR>
			 --%>
			<%
			if(sReturnOK=="Status :OK") 
			{ %>
			<body bgcolor="green" style="color: white;">
			<%= sReturnOK %></BR>
			<% }
			else if(sReturnOK=="Status :Memory is greater than 10GB")
			{
			%>
			<body bgcolor="yellow">
			<%
			}
			else
			{
			 %>
			 <body bgcolor="red">
			 <audio controls autoplay><source src="alert_red.mp3" type="audio/mpeg"> 
  			  Your browser does not support the audio element.</audio></BR></BR>
  			  
			 <font size="5">Need to verify log !!!</BR> <%= sReturnOK %></BR></font>
			 <% 
			 }
			 %>
			<%= new Date() %></BR> 
			Total Memory (Committed Heap): <%= nf.format(totalMemoryGB) %> GB</BR>
			<%--Total Used Memory (Used Heap): <%= nf.format(usedmemoryGB) %> GB</BR>
			Total Free Memory : <%= nf.format(freeMemoryGB) %> GB</BR> --%>
			
			<a href="https://platform-d.teachermatch.org/jobboard.do" target="_blank">D</a>&nbsp;&nbsp; || &nbsp;&nbsp;
			<a href="https://platform-e.teachermatch.org/jobboard.do" target="_blank">E</a>&nbsp;&nbsp; || &nbsp;&nbsp;
			<a href="https://platform-f.teachermatch.org/jobboard.do" target="_blank">F</a> &nbsp;&nbsp; || &nbsp;&nbsp;
			<a href="https://platform-g.teachermatch.org/jobboard.do" target="_blank">G</a> &nbsp;&nbsp; || &nbsp;&nbsp;
			<a href="https://platform-h.teachermatch.org/jobboard.do" target="_blank">H</a> &nbsp;&nbsp; || &nbsp;&nbsp;
			 </BR>
			 
			</body>
			