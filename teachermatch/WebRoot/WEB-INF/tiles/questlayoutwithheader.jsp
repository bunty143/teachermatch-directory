<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<%@ page import="tm.utility.Utility" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><tiles:insertAttribute name="title"/></title>	   
		<script type="text/javascript">
		var resourceJSON = <%=Utility.getJSLocaleJSON()%>;
		</script>
	    <link href="css/bootstrap3.css?ver=${resourceMap['css/bootstrap3.css']}" rel="stylesheet" type="text/css">
		<link href="css/style3.css?ver=${resourceMap['css/style3.css']}" rel="stylesheet" type="text/css">	
		<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">		
		<link href="css/font-awesome.min.css?ver=${resourceMap['css/font-awesome.min.css']}"  rel="stylesheet" type="text/css" >
		<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css">				
		<script src="js/jquery.js?var=${resourceMap['js/jquery.js']}"></script>	  								
	    <script src="twitter-bootstrap/js/bootstrap.min.js?var=${resourceMap['twitter-bootstrap/js/bootstrap.min.js']}" type="text/javascript"></script> 	      			    
	    <script type="text/javascript" src="dwr/engine.js"></script>
		<script type='text/javascript' src='dwr/util.js'></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-56937648-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
			<!-- Start of Async HubSpot Analytics Code -->
			  <script type="text/javascript">
			    (function(d,s,i,r) {
			      if (d.getElementById(i)){return;}
			      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
			      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/290901.js';
			      e.parentNode.insertBefore(n, e);
			    })(document,"script","hs-analytics",300000);
 		 </script>
					
	</head>		
	<body>
	<c:choose>
		<c:when test="${(empty login) or (teacherDetail eq null) }">
			<jsp:include page="feedbackandsupport3BeforeLogin.jsp"/> 
		</c:when>
		<c:otherwise>
			<jsp:include page="../tiles/feedbackandsupport3AfterLogin.jsp" />
		</c:otherwise>
	</c:choose>
		<tiles:insertAttribute name="header" />
		<div class="container" style="min-height:400px;">
			<tiles:insertAttribute name="body" />
		</div>	
		<tiles:insertAttribute name="footer" />	
		 
	</body>
</html>
