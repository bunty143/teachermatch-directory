<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<html>
<head>  
<script type="text/javascript" src="jquery/jquery.cookie.js"></script>
<script type="text/javascript">
function clearCookies()
{
	jQuery(function($){
 		  $.cookie('noShowWelcome',null)
  });
 }
</script> 
	<link rel="stylesheet" href="css/Menu/styledownload.css" type="text/css" />	 
</head> 
<script>
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
				    var siteURL =null,menuPage=null,JobOrderVal=null,entityID=null;
				  	try{
				  		if(this.location.href.indexOf('teachermatch.org/TMDEMO/')!=-1){
				  			siteURL = this.location.href.split('teachermatch.org/TMDEMO/');
				  		}else if(this.location.href.indexOf('teachermatch.org/')!=-1){
				  			siteURL = this.location.href.split('teachermatch.org/');
				  		}else if(this.location.href.indexOf('teachermatch/')!=-1){
				  			siteURL = this.location.href.split('teachermatch/');
				  		}else if(this.location.href.indexOf('8080/')!=-1){
				  			siteURL = this.location.href.split('8080/');
				  		}
						if(siteURL[1].indexOf("JobOrderType=2")!=-1 && siteURL[1].indexOf("batch")==-1){
							JobOrderVal=2;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("batch")==-1){
							JobOrderVal=3;
						}
						if(siteURL[1].indexOf("JobOrderType=2")!=-1 && siteURL[1].indexOf("batch")!=-1){
							JobOrderVal=2;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("batch")!=-1){
							JobOrderVal=3;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("entityID=3")!=-1){
							entityID=3;
						}
						menuPage=siteURL[1].substring(0,siteURL[1].indexOf(".do")+3);
					}catch(err){}					
					function CurrentPageWithMenuSelected(menuPageName,parentorderid,menuorderid,status){							
						if(menuPageName.indexOf("|"+menuPage+"|")!=-1){
							if(JobOrderVal==null){							
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
								
							}else if(JobOrderVal==2  && menuPageName.indexOf("managejoborders")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}else if(JobOrderVal==3  && menuPageName.indexOf("schooljoborders")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}else if(JobOrderVal==2  && menuPageName.indexOf("|batchjoborder.do|")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}else if(entityID==3 && JobOrderVal==3  && menuPageName.indexOf("|batchjoborder.do|")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}else if(JobOrderVal==3  && menuPageName.indexOf("batchschooljoborder")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}
						}
					}
					function CurrentPageWithParentMenuSelected(menuPageName,parentorderid){										
						if(menuPageName.indexOf(menuPage)!=-1){											
							document.getElementById("menuid"+parentorderid).className= "activeText";
							
						}
					}
					function generateScoreData()
					{
					    if(deviceType)
					    {
					    	$("#exelfileNotOpen").css({"z-index":"3000"});
			    			$('#exelfileNotOpen').show();
					    }
					    else
					    {				   
							$('#message2showConfirm').html("A candidate score spreadsheet will be downloaded.");
							$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/getcandidatedata.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
							$('#myModal3').modal('show');
						 }
					}
					function updateCgGridData()
					{
						if(deviceType)
					    {
					    	$("#exelfileNotOpen").css({"z-index":"3000"});
			    			$('#exelfileNotOpen').show();
					    }
					    else
					    {
						$('#message2showConfirm').html("Do you want to update CG Grid data?");
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./updatecgbyuser.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
						}
					}
					function processData()
					{
						$('#message2showConfirm').html("Do you want to update Mosaic data?");
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/processdata.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
					}
					function exportData()
					{ 
					    if(deviceType)
					    {
					    	$("#exelfileNotOpen").css({"z-index":"3000"});
			    			$('#exelfileNotOpen').show();
					    }
					    else
					    {
						$('#message2showConfirm').html("Do you want to Export data?");
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/getcandidatedataexport.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
						}
					}
				   </script>
<div class="container"  >
	<div style="float: left; margin-top:10px;" class="top15">
		<div> 
			 <% if(request.getServerName().contains("myedquest.org")){  %>						 
						 <a href="http://myedquest.org/" target="_blank" class="tmlogo"><img src="images/QuestLogoTM.png" alt="" class="top14"></a>
						 
						 <!-- Google Analytics code:  -->
							 <script>
							  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
							  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');							
							  ga('create', 'UA-56937648-1', 'auto');
							  ga('send', 'pageview');							
							</script>
						 
							<!-- Start of Async HubSpot Analytics Code -->
							  <script type="text/javascript">
							    (function(d,s,i,r) {
							      if (d.getElementById(i)){return;}
							      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
							      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/290901.js';
							      e.parentNode.insertBefore(n, e);
							    })(document,"script","hs-analytics",300000);
							  </script>
						 
						 
						 <%}else{ 
						 		if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<a href="http://www.teachermatch.org" target="_blank" class="tmlogo"><img src="images/logoimage.png" alt="" class="top14" style="width: 133px;height: 50px;"></a>	
								<%}else{ %>
									<a href="http://www.teachermatch.org" target="_blank" class="tmlogo"><img src="images/Logo with Beta300.png" alt="" class="top14"></a>	
								<%}%>
											
						<script type="text/javascript">
							(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
							ga('create','UA-40462543-1','teachermatch.org');
							ga('send','pageview');
						</script>
						
						 <%} %>
						 </div>
	</div>	
	<div class="pull-right">		     		  
					<div class="btn-group">					
						<c:choose>
			<c:when test="${(teacherDetail eq null) and (userMaster eq null)}">
			</c:when>
			<c:otherwise>	
			<c:if test="${userName ne null}">				
				 <div id="social_media_wrapper" style="position: inherit"> 
                <div class="pull-right">   		  
					<div class="btn-group">
					
					<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown" href="#">
						<span class='tabletbodyadmin'>${userName}</span><span class="carethead"></span> 
					</a>	
					<ul class="dropdown-menu pull-right">
						<li><a href="questusersettings.do"><spring:message code="lblSett"/></a></li>	
						<li class="divider">&nbsp;</li>						
					    <li><a href="logoutUser.do"><spring:message code="lnkSignout"/></a></li>
					</ul>		  							
					</div>
				  </div>
              </div>              
			</c:if>
			<c:if test="${teacherDetail.firstName!=null}">
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown" href="#">
			   					<span class='tabletbodyadmin'>${teacherDetail.firstName}&nbsp;${teacherDetail.lastName}</span>
			   					<span class="carethead"></span>
			    			</a>
			    			<ul class="dropdown-menu">    
								<li><a href="userpreference.do"><spring:message code="lnkJobPreferences"/></a></li>
								<li class="divider">&nbsp;</li>
								<li><a href="settings.do"><spring:message code="lblSett"/></a></li>
								<c:if test="${schoolSelectionflag ==1}">
								<li class="divider">&nbsp;</li>
									<li>
										<a href="selectschool.do"><spring:message code="lnkSchoolSelection"/></a>
									</li>
								</c:if>
								<li class="divider">&nbsp;</li>
								<li><a href="logout.do" onclick="clearCookies();"> <spring:message code="lnkSignout"/> </a></li>               
			    			</ul>		    	
			  			</c:if>		
		</c:otherwise>
		</c:choose>
					</div>
       </div>
       
	<div style="clear: both;"></div>
</div> 	
<div class="top10" style="margin-bottom: 20px;">			
	<ul id="css3menu" class="topmenu" style="width:100%; margin-bottom: 10px;">
		<div class="container">
		<c:choose>
						<c:when test="${userName ne null}">	
								<li class="toproot">
								<c:choose>
									<c:when test="${not empty userhomes}">
										<a href="#" class="activeText"><i class="fa fa-arrow-down arrowcolor"></i> <spring:message code="lblMos"/></a>
										<div class="submenu" style=" width:650px;" >								
										<div class='column'>
											<ul>
												<li><a href="userhome.do" class="activeText"><i class="fa fa-square arrowcolor"></i> <spring:message code="lblHome"/></a></li>
												<li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>									
											</ul>
										</div>									
										</div>	
									</c:when>
									<c:otherwise>
										<a href="#"><i class="fa fa-arrow-down arrowcolor"></i> <spring:message code="lblMos"/></a>
										<div class="submenu" style=" width:650px;" >								
										<div class='column'>
											<ul>
												<li><a href="userhome.do"><i class="fa fa-square arrowcolor"></i><spring:message code="lblHome"/>
</a></li>
												<li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>									
											</ul>
										</div>									
										</div>	
									</c:otherwise>
								</c:choose>
								</li>	
								<li class="toproot">
								<c:choose>
									<c:when test="${not empty questcandidatess}">
									<a href="#" class="activeText"><i class="fa fa-arrow-down arrowcolor"></i><spring:message code="lnkUsr"/></a>
									<div class="submenu" style=" width:650px;" >
									<div class='column'>	
										<ul>									
											<li><a href="questcandidates.do" class="activeText"><i class="fa fa-square arrowcolor"></i>  <spring:message code="divCandidates"/></a></li>
											<li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>
										</ul>
									</div>	
									</div>	
									</c:when>
									<c:otherwise>
									<a href="#"><i class="fa fa-arrow-down arrowcolor"></i> <spring:message code="lnkUsr"/></a>
									<div class="submenu" style=" width:650px;" >
									<div class='column'>	
										<ul>									
											<li><a href="questcandidates.do"><i class="fa fa-square arrowcolor"></i>  <spring:message code="divCandidates"/></a></li>
											<li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>
										</ul>
									</div>	
									</div>		
									</c:otherwise>
								</c:choose>
								</li>
							
								<li class="toproot">
								<c:choose>
										<c:when test="${(not empty manages) or (not empty manageschoolss)}">
										<a href="#" class="activeText"><i class="fa fa-arrow-down arrowcolor"></i><spring:message code="headManage"/></a>
										<div class="submenu" style=" width:650px;" >
										<div class='column'>	
										<ul>									
											<li><a href="manage.do"><i class="fa fa-square arrowcolor"></i> <spring:message code="optDistrict"/></a></li>
											<li><a href="manageschools.do"><i class="fa fa-square arrowcolor"></i>  <spring:message code="lblSchool"/></a></li>	
											<li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>								
										</ul>
										</div>
										</div>	
										</c:when>
										<c:otherwise>
										<a href="#"><i class="fa fa-arrow-down arrowcolor"></i><spring:message code="headManage"/></a>
										<div class="submenu" style=" width:650px;" >
										<div class='column'>	
										<ul>									
											<li><a href="manage.do"><i class="fa fa-square arrowcolor"></i><spring:message code="optDistrict"/></a></li>
											<li><a href="manageschools.do"><i class="fa fa-square arrowcolor"></i> <spring:message code="lblSchool"/></a></li>	
											<li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>								
										</ul>
										</div>
										</div>
										</c:otherwise>
								</c:choose>
								</li>							
						
								<li class="toproot">
								<c:choose>
										<c:when test="${not empty jobs}">
										<a href="#" class="activeText"><i class="fa fa-arrow-down arrowcolor"></i> <spring:message code="lblJo"/></a>
										<div class="submenu" style=" width:650px;" >
										<div class='column'>	
										<ul>	
											<li><a href="jobs.do" class="activeText"><i class="fa fa-square arrowcolor"></i> <spring:message code="lblDistrictJobOrders"/></a></li>		
											<li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>							
										</ul>
										</div>
										</div>
										</c:when>
										<c:otherwise>
										<a href="#"><i class="fa fa-arrow-down arrowcolor"></i> <spring:message code="lblJo"/></a>
										<div class="submenu" style=" width:650px;" >
										<div class='column'>	
										<ul>	
											<li><a href="jobs.do"><i class="fa fa-square arrowcolor"></i> <spring:message code="lblDistrictJobOrders"/></a></li>		
											<li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>							
										</ul>
										</div>
										</div>
										</c:otherwise>
								</c:choose>
								</li>
						
						</c:when>							
						</c:choose>	
					<c:if test="${(teacherDetail ne null) }">
						<c:set var="parentMenuIdChk" />
						<c:set var="menuIdCounter" value="0"/>
						<c:set var="parentMenu" value="100"/>
						<c:set var="flagMenu" value="0"/>
						<c:set var="imgStatusMain" value="1"/>
						<c:set var="imgStatusMainH" value="1"/>
						<c:set var="column" value="<div class='column'><ul>"/>
						<c:set var="columnEnd" value="</ul></div>"/>
			
					<c:forEach var="parentmenu" items="${lstMenuMaster}" varStatus="status">
						<c:if test="${!fn:contains(parentMenuIdChk,parentmenu.parentMenuId.menuId)}">
							<c:set var="parentMenuIdChk" value="||${parentMenuIdChk}${parentmenu.parentMenuId.menuId}||" />
							<li class="toproot">
							<c:set var="menuIdCounter" value="${menuIdCounter+1}"/>
							<a href="#"  id="menuid${menuIdCounter}" class=""> 
							<i class="fa fa-arrow-down arrowcolor"></i>
							${parentmenu.parentMenuId.menuName}</a>
						<div class="submenu" style=" width:650px;" >
							
							<c:set var="imgStatus" value="1"/>
							<c:set var="subMenuIdCounter" value="0"/>
							<c:set var="subMenuIdCheck" value="0"/>
							<c:set var="flag" value="0"/>
							<c:set var="roundDiv" value="0"/>
							  <c:forEach var="menu" items="${lstMenuMaster}" varStatus="status">
										<c:if test="${(parentmenu.parentMenuId.menuId==menu.parentMenuId.menuId)}">
										<c:set var="subMenuIdCounter" value="${subMenuIdCounter+1}"/> 
										 <c:if test="${(subMenuIdCounter%7!=0) && flag==0}">																													
											${column}  
											<c:set var="flag" value="1"/>
										</c:if>
										<li><a id="submenuid${menuIdCounter}${subMenuIdCounter}" class="" href="${menu.pageName}" >
										<i class="fa fa-square arrowcolor"></i>	
									
									${menu.menuName}</a></li>	
									<c:if test="${(subMenuIdCounter%7==0)}">
									<c:choose>
								        <c:when test="${roundDiv==0}">
								        <li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px; border-radius:0 0 0 10px; " ><i class="fa fa-square circlecolor"></i></a></li>
								       <c:set var="roundDiv" value="1"/> 
								        </c:when>								      
								        <c:otherwise>
								         <li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;" ><i class="fa fa-square circlecolor"></i></a></li>										
								        </c:otherwise>
   									 </c:choose>																		 		
										${columnEnd}
										<c:set var="flag" value="0"/>						
									</c:if>
									 <script>
															CurrentPageWithMenuSelected("${menu.pageNameWithSubPage}",${parentmenu.parentMenuId.orderBy},${subMenuIdCounter},"${menu.status}");
														</script>
									</c:if>
								   </c:forEach>
									<c:set var="Reminder" value="${(7-subMenuIdCounter%7)}"/>
										<c:if test="${(subMenuIdCounter>7 && Reminder!=0 && Reminder!=7)}">
										<c:forEach var="i" begin="1" end="${Reminder}">
  											 <li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b; " >g<i class="fa fa-square circlecolor"></i></a></li>
										</c:forEach>										
										</c:if>
										
									 <c:choose>
								        <c:when test="${(subMenuIdCounter<7)}">
								        <li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>
								       </c:when>
										<c:when test="${(Reminder==7)}">
								       									
										</c:when>								      
								        <c:otherwise>
								        <li class="bl"><a href="" style="border-width:0 0 1px 0;border-style:solid;border-color:#25333b;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 0; " ><i class="fa fa-square circlecolor"></i></a></li>
								        </c:otherwise>
   									 </c:choose>
								
						</div>
					</li>
				</c:if>
				
				  <c:if test="${parentmenu.parentMenuId.menuId==null && !fn:contains(parentMenuIdChk,parentmenu.menuId)}">
											
											<li class="topmenu">
											    <c:set var="parentMenu" />	
											     	
											     <c:set var="menuIdCounter" value="${menuIdCounter+1}"/>									    
													   <a href="${parentmenu.pageName}" id="menuid${menuIdCounter}" >
													   <i class="fa fa-square arrowcolor"></i>
								
													   	${parentmenu.menuName}	  </a>
											  </li>	
											  <script>
												  CurrentPageWithParentMenuSelected("${parentmenu.pageNameWithSubPage}",${menuIdCounter});
											 </script>									   	
										</c:if>
			</c:forEach>
			</c:if>
			<c:if test="${(teacherDetail eq null) and (userName eq null)}">		
							<c:choose>
								<c:when test="${not empty educationjob}">
								<li class="topmenu"><a href="educationjob.do" class="activeText"><i class="fa fa-square arrowcolor"></i> <spring:message code="lblHome"/></a></li>	
								</c:when>
								<c:otherwise>
								<li class="topmenu"><a href="educationjob.do"><i class="fa fa-square arrowcolor"></i> <spring:message code="lblHome"/></a></li>	
								</c:otherwise>
							</c:choose>	
							
							<c:choose>
								<c:when test="${not empty aboutus}">
								<li class="topmenu"><a href="aboutus.do" class="activeText"><i class="fa fa-square arrowcolor"></i> <spring:message code="lnkAboutUs"/></a></li>
								</c:when>
								<c:otherwise>
								<li class="topmenu"><a href="aboutus.do" ><i class="fa fa-square arrowcolor"></i> <spring:message code="lnkAboutUs"/></a></li>
								</c:otherwise>
							</c:choose>
							
							<c:choose>
								<c:when test="${not empty meetyourmentor}">
								<li class="topmenu"><a href="meetyourmentor.do" class="activeText"> <i class="fa fa-square arrowcolor"></i> <spring:message code="lnkResources"/></a></li>
								</c:when>
								<c:otherwise>
								<li class="topmenu"><a href="meetyourmentor.do"> <i class="fa fa-square arrowcolor"></i> <spring:message code="lnkResources"/></a></li>
								</c:otherwise>
							</c:choose>						
					
						<li class="topmenu"><a href="http://www.teachermatch.org/blog"><i class="fa fa-square arrowcolor"></i> <spring:message code="lnkBlog"/></a></li>
			</c:if>		
		</div>	
	</ul>
</div>
</html>
