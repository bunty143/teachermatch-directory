
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="js/jquery-te-1.3.2.2.min.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="css/jquery-te-1.3.2.2.css" charset="utf-8" />
<%@ page import="tm.utility.Utility" %>
<!-- Editor 2 -->
  <link href="css/rating_simple.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="js/rating_simple.js"></script>
  
  <script language="javascript" type="text/javascript">
  
          function setRating()
          {
            $(function() {
                $("#rating_simple1").webwidget_rating_simple({
                    rating_star_length: '5',
                    rating_initial_value: '',
                    rating_function_name: '',//this is function name for click
                    directory: 'images/'
                });

            });
           }
           //setRating();
        </script>
        
<!-- Feedback -->
<div  onclick="feedBack();" style="cursor: pointer;background-image: url(images/tab-right-dark.png); border-top-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-style: solid none solid solid; border-top-color: rgb(255, 255, 255); border-bottom-color: rgb(255, 255, 255); border-left-color: rgb(255, 255, 255); border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; -webkit-box-shadow: rgba(255, 255, 255, 0.247059) 1px 1px 1px inset, rgba(0, 0, 0, 0.498039) 0px 1px 2px; box-shadow: rgba(255, 255, 255, 0.247059) 1px 1px 1px inset, rgba(0, 0, 0, 0.498039) 0px 1px 2px; font-style: normal; font-variant: normal; font-weight: bold; font-size: 14px; line-height: 1em; font-family: Arial, sans-serif; position: fixed; right: 0px; top: 50%; z-index: 9999; background-color: rgb(0, 122, 180); margin-top: -100px; margin-right: 0px; display: block; background-position: 50% 0px; background-repeat: no-repeat no-repeat;">
	<br><br><br>
	
	 <%if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						     <a href="javascript:void(0);"><img src="images/feedback-tab-fr.png" alt="" ></a>
                                       <%}else{ %>
						        <a href="javascript:void(0);"><img src="images/feedback-tab.png"></a>
                                      <%} %>

	<br><br>
</div>


<div class="modal1 hide" style='width:510px;top:50%;left:69%; z-index: 2000;' id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  >
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headingTMFeedbackandSupport"/></h3>
	</div>
	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>

	<div  class="modal-body">
	
	<form id='frmFeedbackUpload' enctype='multipart/form-data' method='post' target='uploadFrame'  class="form-inline" onsubmit="return validateFeedBack();" accept-charset="UTF-8">
	<input type='hidden' id="visitedPageURL" name="visitedPageURL" /> 
	<input type='hidden' id="visitedPageTitle" name="visitedPageTitle" />
	<div class="control-group">
		<label class="radio">
           <input type="radio"  value="1" id="spt" name="reqType" checked="checked" onclick='showDisplay(this);'>
           <spring:message code="msgyesWantTOSendMsg"/>
   		</label>
   		<br/>
   		<label class="radio">
   		<input type="radio"  value="2"  name="reqType" onclick='showDisplay(this);'>
          <spring:message code="msgYesWantToSendFeedback"/>
   		</label>
		</div>
		
		<div class="control-group">
			<div class='divErrorMsg' id='errordivFeedBack' style="display: block;"></div>
		</div>
		
		<!-- feedback div -->
		<div id='feedback' style='display:none;'>
        
        <div class="control-group">
			<div style='' id="ratingDiv" style="padding-top:30px;">
				<input  value="0" id="rating_simple1" name="starRating" type="hidden">
			</div>
		</div>
          
        <div class="control-group">
			<div class="" id="msgFeed">
		    	<label><strong><spring:message code="lblFeedback"/></strong><span class="required">*</span></label>
	        	<textarea rows="5" class="span8" cols="" id="feedbackmsg" name="feedbackmsg" maxlength="1000"></textarea>
			</div>
		</div>
		
 		</div>
 		<!-- end feedback div -->
		
		<!-- support div -->
		<div id='support'>
		
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label><br/>
	        	<input id="msgSubject" name="msgSubject" type="text" class="span8" maxlength="80" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="msgSpt">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
	        	<textarea rows="5" class="span8" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        	<!--<div style='background-color: #FFFFCC;' >-->
	        	<div id='file1' style="padding-top:8px;">
	        	<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addFileType();'><spring:message code="lnkAttachFile"/>
</a>
	        	</div>
			</div>
		</div>
 		</div>
 		<spring:message code="msgPleaseCallTMSupport"/>
   
 		
 		<br/>
 		<div id="lodingImg" style='display:none;text-align:center;padding-top:4px;'><img src="images/loadingAnimation.gif" /> <spring:message code="msgMsgSending"/></div>
 		<!-- end support div -->
 		
 		</form>
 	</div>
 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
 		<button class="btn btn-primary" onclick="saveFeedBack()" ><spring:message code="btnSend"/></button>
 	</div>
</div>

<script language='javascript'>

function getVisitedURL()
{
	var ddd=document.URL;
	document.getElementById("visitedPageURL").value=""+ddd+"";
	document.getElementById("visitedPageTitle").value=""+document.title+"";
}
getVisitedURL();
function showDisplay(dis)
{
	$('#errordivFeedBack').empty();
	$('#lodingImg').hide();
	//alert(dis.value);
	if(dis.value==1)
	{
		$('#feedback').hide();
		$('#support').show();
		$('#msgSubject').focus();
		
	}else
 	{
 		$('#feedback').show();
 		$('#support').hide();
 		$('#feedbackmsg').focus();
 		//$('#feedbackmsg').css("background-color", "");
 	}
 	
 	//<input  value='0' id='rating_simple1' name='starRating' type='text'>
 	$('#ratingDiv').html("<input  value='0' id='rating_simple1' name='starRating' type='hidden'>");
 	setRating();
 	removeFile();
 	
 	$('#msgFeed').find(".jqte_editor").css("background-color", "");
	$('#msgSpt').find(".jqte_editor").css("background-color", "");
	$('#msgSubject').css("background-color", "");
	$('#msgFeed').find(".jqte_editor").html("");
	$('#msgSpt').find(".jqte_editor").html("");
 	document.getElementById("msgSubject").value="";
 	//document.getElementById("msg").value="";
 	//document.getElementById("f1").value="";
 	//document.getElementById("feedbackmsg").value="";
}
function validateFeedBack()
{

	$('#errordivFeedBack').empty();
	$('#msgSubject').css("background-color", "");
	//$('#msg').css("background-color", "");
	$('#f1').css("background-color", "");
	//$('#feedbackmsg').css("background-color", "");
	$('#msgFeed').find(".jqte_editor").css("background-color", "");
	$('#msgSpt').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	
	var reqType=$('input[name=reqType]:radio:checked').val();
	
	if(reqType==1)
	{
	
		var fileSize=0;		
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{	
			if(document.getElementById("f1"))
			if(document.getElementById("f1").files[0]!=undefined)
				fileSize = document.getElementById("f1").files[0].size;
		}
			//alert(fileSize);
		if(trim($('#msgSubject').val())=="")
		{
			$('#errordivFeedBack').append("&#149; <spring:message code='errMsgPleaseEnterSub'/><br>");
			if(focs==0)
				$('#msgSubject').focus();
			$('#msgSubject').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		/*if(trim($('#msg').val())=="")
		{
			$('#errordivFeedBack').append("&#149; Please enter Message.<br>");
			if(focs==0)
				$('#msg').focus();
			$('#msg').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}*/
		if ($('#msgSpt').find(".jqte_editor").text().trim()=="")
		{
			$('#errordivFeedBack').append("&#149; <spring:message code='errMsgPleaseEnterMSg'/><br>");
			if(focs==0)
				$('#msgSpt').find(".jqte_editor").focus();
			$('#msgSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else
		{
			var charCount=$('#msgSpt').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			//alert(count);
 			if(count>1000)
 			{
				$('#errordivFeedBack').append("&#149; <spring:message code='errMsgexceedcharlength'/><br>");
				if(focs==0)
					$('#msgSpt').find(".jqte_editor").focus();
				$('#msgSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		if(fileSize>=10485760)
		{		
			$('#errordivFeedBack').append("&#149; <spring:message code='errMsgFileSize10mb'/><br>");
			if(focs==0)
				$('#f1').focus();
			
			$('#f1').css("background-color","#F5E7E1");
			cnt++;focs++;	
		}
	}else
	{
		/*if(trim($('#feedbackmsg').val())=="")
		{
			$('#errordivFeedBack').append("&#149; Please enter Feedback.<br>");
			if(focs==0)
				$('#feedbackmsg').focus();
			$('#feedbackmsg').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}*/
		
		if ($('#msgFeed').find(".jqte_editor").text().trim()=="")
		{
			$('#errordivFeedBack').append("&#149; <spring:message code='msgPleaseEnterFeedback'/><br>");
			if(focs==0)
				$('#msgFeed').find(".jqte_editor").focus();
			$('#msgFeed').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else
		{
			var charCount=$('#msgFeed').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			//alert(count);
 			if(count>1000)
 			{
				$('#errordivFeedBack').append("&#149; <spring:message code='errMsgFeedLengthExceed'/><br>");
				if(focs==0)
					$('#msgFeed').find(".jqte_editor").focus();
				$('#msgFeed').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#errordivFeedBack').show();
		return false;
	}
}
function saveFeedBack()
{

	if(!validateFeedBack())
		return;
	
	$('#lodingImg').show();
	
	document.getElementById('frmFeedbackUpload').action="feedbackUploadServlet.do";
	$('#frmFeedbackUpload').submit();
	
}
function hideFeedBack(flg)
{
	
	
	//document.getElementById('f1').value="";
	//$('#errordivFeedBack').html("");
	$('#lodingImg').hide();
	$('#myModal1').modal('hide');
	if(flg==1)
	{
		$('#message2show').html("<spring:message code='msgSuccessfullySent'/>");
		$('#myModal2').modal('show');
	}
	else if(flg==2)
	{
		$('#message2show').html("<spring:message code='msgFeedbackSuccess'/>");
		$('#myModal2').modal('show');
	}
	else if(flg==3)
	{
		alert("<spring:message code='msgSessionExpired'/>");  document.location = 'signin.do';
	}
	
	document.getElementById('msgSubject').value="";
	document.getElementById('msg').value="";
	document.getElementById('feedbackmsg').value="";
}
function addFileType()
{
	$('#file1').empty();
	$('#file1').html("<a href='javascript:void(0);' onclick='removeFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='f1' name='f1' size='20' title='Choose a File' type='file'><br>&nbsp;<spring:message code='errMaxFileSizze'/>");
}
function removeFile()
{
	$('#file1').empty();
	$('#file1').html("<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addFileType();'><spring:message code='lnkAttachFile'/></a>");
}
function feedBack()
{
	$('#myModal1').modal('show');
	$('#spt').click();
	
	$('#file1').html("<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addFileType();'><spring:message code='lnkAttachFile'/></a>");
	$('#msgSubject').focus();
	$('#errordivFeedBack').empty();
	$('#msgSubject').css("background-color", "");
	document.getElementById('msgSubject').value="";
	
}
</script>

<div  class="modal1 hide"  id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>

<div  class="modal1 hide"  id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2showConfirm'>
		</div>
 	</div>
 	<div class="modal-footer" id='footerbtn'>
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>
<!-- End Feedback -->
<div  class="modal1 hide" style='width:650px; top: 40%;' id="myModalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtn'>x</button>
		<h3 id="myModalLabelId">TeacherMatch</h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y: auto;">
	<table width="100%"><tr>
	<td width="15%" valign="top" style="padding-top: 0px;">
	<div class="control-group" id='warningImg1'>
		</div>
	</td>
	<td width="85%" style="padding-left: 10px;">
	<div class="control-group" id='message2showConfirm1'>
		</div>
	</td>
	</tr></table>
	<div class="control-group" id='nextMsg'>
		</div>	
 	</div>
 	<div class="modal-footer" id='footerbtn1'>
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>

<div  class="modal1 hide" style='width:570px;' id="myModalvk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
	<table width="100%"><tr>
	<td width="15%" valign="top" style="padding-top: 0px;">
	<div class="control-group" id='warningImg1k'>
		</div>
	</td>
	<td width="85%" style="padding-left: 10px;">
	<div class="control-group" id='message2showConfirm1k'>
		</div>
	</td>
	</tr></table>
	<div class="control-group" id='nextMsgk'>
		</div>	
 	</div>
 	<div class="modal-footer" id='footerbtn1k'>
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>