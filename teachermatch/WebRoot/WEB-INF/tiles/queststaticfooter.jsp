<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="tm.bean.user.UserMaster"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<div style="min-height: 20px;"></div>
<div  style="background: #AFB3B6; min-height: 90px;" class="top20"><!-- footer -->
	   <div class="container" style="margin: auto;">   
		   <div style="float: left;position: relative;" class="top15">		   
		   <% if(request.getServerName().contains("myedquest.org")){  %>
						<a href="http://myedquest.org" target="_blank"><img src="images/QuestLogoTM.png" alt="" ></a>
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						 	<a href="http://www.teachermatch.org" target="_blank"><img src="images/GrayTM-no tag line_50.png" alt="" ></a>
						 
						 <%}else{ %>
						<a href="http://www.teachermatch.org" target="_blank"> <img src="images/tmgraylogo.png" alt="" > </a>
						 <%} %>
		   <br/>	   
		   </div>
		    <div style="float: right;" class="top20">		  		  
				    <a href="https://www.facebook.com/QuestTeacherJobs" target="_blank"><img src="images/footer-icons/fftricon.png"></a>     
                    <a href="http://www.google.com/+TeachermatchOrganization" target="_blank"><img src="images/footer-icons/gftricon.png"></a>  
                    <a href="https://www.linkedin.com/company/teachermatch" target="_blank"><img src="images/footer-icons/inftricon.png"></a> 
                    <a href="https://www.youtube.com/channel/UCLi14NFiTU0bf3hTktuTtSw" target="_blank"><img src="images/footer-icons/ytftricon.png"></a> 
                    <a href="https://twitter.com/EdQuestJobs" target="_blank"><img src="images/footer-icons/tftricon.png"></a>     
                    <a href="http://www.pinterest.com/EdQuestJobs/" target="_blank"><img src="images/footer-icons/pfticon.png"></a>      
	
			   	</div>			  
		  		 <div style="float: right;color: #4E4D52;" id="footertxtcolor"  class="gray">		   
			   <b><a href="aboutus.do" target="_blank"> <spring:message code="lnkAboutUs"/></a> | <a href="meetyourmentor.do" target="_blank"> <spring:message code="lnkResources"/></a> | <a href="http://www.teachermatch.com/blog" target="_blank"><spring:message code="lnkBlog"/></a> | <a href="https://www.teachermatch.org/contact-us/" target="_blank"><spring:message code="lnkContactUs"/></a> | <a href="https://www.teachermatch.org/press-and-media/" target="_blank"> <spring:message code="lnkPressMedia"/></a> | <a href="#"><spring:message code="lnkSiteMap"/></a>			   
			   &nbsp;&nbsp;&nbsp;&nbsp; &copy; <spring:message code="magTmTousDroitsRes"/></b>  
		   </div>
	    </div>	
  </div>  
<!-- footer End-->
