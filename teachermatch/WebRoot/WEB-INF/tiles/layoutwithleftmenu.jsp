<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="tm.utility.Utility" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><tiles:insertAttribute name="title"/></title>			
		<!--By alok to prevent to resize icon size on ie10.
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">		
		-->		 
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	    <link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css" >
		<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">    
	    <link href="css/font-awesome.min.css?ver=${resourceMap['css/font-awesome.min.css']}" rel="stylesheet" type="text/css">
	    
	    <link href="css/bootstrap3.css?ver=${resourceMap['css/bootstrap3.css']}" rel="stylesheet" type="text/css">
		<link href="css/style3.css?ver=${resourceMap['css/style3.css']}" rel="stylesheet" type="text/css">				
		<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/tmcommon.js?var=${resourceMap['js/tmcommon.js']}"></script>					    
   		<script type="text/javascript" src="<tiles:insertAttribute name="dynamicJS"/>">	</script>
   		
   		<script src="js/jquery.js?var=${resourceMap['js/jquery.js']}"></script>			
   		<script  type="text/javascript" language="javascript" src="js/drg.js?var=${resourceMap['js/drg.js']}"></script>					
	    <!--  <script src="js/bootstrap.js" type="text/javascript"></script>	-->							
	    <script src="twitter-bootstrap/js/bootstrap.min.js?var=${resourceMap['twitter-bootstrap/js/bootstrap.min.js']}" type="text/javascript"></script> 	
      
        			    
	</head>	
	<script type="text/javascript">
		var resourceJSON = <%=Utility.getJSLocaleJSON()%>;
	</script>	
	<body class="boxed" onload="setFocus('<tiles:insertAttribute name="focusId" />') <tiles:insertAttribute name="bodyLoad" /> ">
	
	<tiles:insertAttribute name="header" />
<!-- Start feedback and support -->
<jsp:include page="feedbackandsupport3AfterLogin.jsp" flush="true" /> 
	
<!-- End feedback and support --> 
	<div class="container" style="min-height: 400px;">
<div class="row top5">
	 <div class="col-sm-1 col-md-1" style="z-index: 999;">
		<tiles:insertAttribute name="leftMenu" />
	</div>
	<div class="col-sm-11 col-md-11" >
	<input type="hidden" id="videoSource" />
	<c:if test="${districtChatSupport ne null}">
		<div class="row">
			 <div class="col-sm-12 col-md-12" style="z-index: 999;">
				<div class="row" style="width:100%;margin-left:0px;">
					<div class="col-sm-10 col-md-10" >			         
			        </div> 
			         <div class="col-sm-2 col-md-2" style="text-align: right;">
			         <span href="#"  data-toggle="tooltip" data-placement="left" title="Show Me" onclick="showVideo()" style="cursor: pointer; padding-top:5px;"><i class="fa fa-television fa-lg" style="color: #007fb2;"></i>								
						</span>	
			         	<span href="#"  data-toggle="tooltip" data-placement="left" title="Chat with an Expert" style="cursor: pointer; padding-top:5px;"><i class="fa fa-weixin fa-lg" style="color: green"></i> </span>
			         </div>   
        		</div> 
			</div>
		</div>	
		 </c:if>	
		<tiles:insertAttribute name="body"  />
	</div>
	</div>
	</div>
	
		<c:set var="serverIpAddress" value="${initParam['serverIPAddress']}"/>
		<c:if test="${serverIpAddress =='166.78.100.150'}">
   			<div style="height: 1px; margin-top:23px; margin-bottom: -23px; z-index: 99999">
   		 		<img src="images/dot.gif" style="background-repeat: repeat-y;height: 1px; width: 100%;">
			</div>
		</c:if>
		
<tiles:insertAttribute name="footer" />	
	</body>
</html>
