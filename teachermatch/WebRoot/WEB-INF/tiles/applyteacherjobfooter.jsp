<script src="js/bootstrap.min.js"></script>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <%@ page import="tm.utility.Utility" %>
<style >
#footertxtcolor a
{
font-size: 11px;
}
</style>
<c:choose>
<c:when test="${(empty login) or (teacherDetail eq null) }">
<div  style="background: #AFB3B6; min-height: 90px;" class="top20"><!-- footer -->
	   <div class="container" style="margin: auto;">   
		   <div style="float: left;position: relative;" class="top15">	
 		<% if(request.getServerName().contains("myedquest.org")){  %>
						 <a href="http://myedquest.org/" target="_blank" class="tmlogo"><img src="images/QuestLogoTM.png" alt="" class="top14"></a>	
						 
						  <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						 	<a href="http://www.teachermatch.org" target="_blank" class="tmlogo"><img src="images/GrayTM-no tag line_50.png" alt="" ></a>
						 
						 <% }else{ %>
						<a href="http://www.teachermatch.org" target="_blank" class="tmlogo"><img src="images/tmgraylogo.png" alt="" class="top14"></a>	
						 <%} %>
		   <br/>	   
		   </div>		   
		    <div style="float: right;" class="top20">		   		  
			   	 	<a href="https://www.facebook.com/QuestTeacherJobs" target="_blank"><img src="images/footer-icons/fftricon.png"></a>     
                    <a href="http://www.google.com/+TeachermatchOrganization" target="_blank"><img src="images/footer-icons/gftricon.png"></a>  
                    <a href="https://www.linkedin.com/company/teachermatch" target="_blank"><img src="images/footer-icons/inftricon.png"></a> 
                    <a href="https://www.youtube.com/channel/UCLi14NFiTU0bf3hTktuTtSw" target="_blank"><img src="images/footer-icons/ytftricon.png"></a> 
                    <a href="https://twitter.com/EdQuestJobs" target="_blank"><img src="images/footer-icons/tftricon.png"></a>     
                    <a href="http://www.pinterest.com/EdQuestJobs/" target="_blank"><img src="images/footer-icons/pfticon.png"></a>      
	
		    </div>				  
		   <div style="float: right;color: #4E4D52;" id="footertxtcolor" class="top10" class="gray">		   
			   <b><a href="aboutus.do" style="font-size: 11px;"><spring:message code="lnkAboutUs"/></a> | <a href="resources.do"><spring:message code="lnkResources"/></a> | <a href="https://www.teachermatch.org/blog/"><spring:message code="lnkBlog"/></a> | <a href="https://www.teachermatch.org/contact-us/"><spring:message code="lnkContactUs"/></a> | <a href="https://www.teachermatch.org/press-and-media/"><spring:message code="lnkPressMedia"/></a> | <a href="#"><spring:message code="lnkSiteMap"/></a> 
			    &nbsp;&nbsp;&nbsp;&nbsp; &copy; <spring:message code="magTmTousDroitsRes"/>
			   </b> 
		   </div>
	  
  </div>  
</div>
</c:when>
<c:otherwise>
<div  style="background: #AFB3B6; min-height: 90px;" class="top20"><!-- footer -->
	   <div class="container" style="margin: auto;">   
		   <div style="float: left;position: relative;" class="top15">		   
		   <% if(request.getServerName().contains("myedquest.org")){  %>
						<a href="http://myedquest.org" target="_blank"><img src="images/QuestLogoTM.png" alt="" ></a>
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						 <a href="http://www.teachermatch.org" target="_blank"> 	<img src="images/GrayTM-no tag line_50.png" alt="" ></a>
						 
						 <%}else{ %>
						<a href="http://www.teachermatch.org" target="_blank"> <img src="images/tmgraylogo.png" alt="" > </a>
						 <%} %>
		   <br/>	   
		   </div>		   
		    <div style="float: right;" class="top20">		  
		    </div>			  
		   <div style="float: right;color: #4E4D52; margin-top:10px;" class="top10" class="gray">		   
			   <b>&nbsp;&nbsp;&nbsp;&nbsp; &copy; <spring:message code="magTmTousDroitsRes"/>
			   </b> 
		   </div>
	  
  </div>  
</div>
</c:otherwise>
</c:choose>
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-40462543-1', 'teachermatch.org');
  ga('send', 'pageview');
</script>
<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        placement : 'top'
    });
});
</script>