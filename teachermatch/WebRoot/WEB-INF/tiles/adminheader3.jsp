<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<html>
<head>
<style>
.tooltip{
    color:white;
    opacity:0;
    min-width:100px;
    max-width:200px;
    line-height: 20px;
    white-space:pre-wrap;
}
</style>
<script type="text/javascript" src="jquery/jquery.cookie.js"></script>
<script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>
<script type="text/javascript">
function clearCookies()
{
	jQuery(function($){
 		  $.cookie('noShowWelcome',null)
  });
 }
</script> 
	<link rel="stylesheet" href="css/Menu/styledownload.css" type="text/css" />
	 
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- remove for Optimization
		<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
		<link href="css/font-awesome-ie7.min.css" rel="stylesheet" type="text/css">	
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		-->
		
		<link href="css/toolkitessentials.css" rel="stylesheet" type="text/css">
</head> 
  <script>
var deviceTypeAndroid=$.browser.device = (/Android|webOS|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent.toLowerCase()));
var deviceType=$.browser.device = (/iPhone|iPad|iPod/i.test(navigator.userAgent.toLowerCase()));
				    var siteURL =null,menuPage=null,JobOrderVal=null,entityID=null;
				  	try{
				  		if(this.location.href.indexOf('teachermatch.org/TMDEMO/')!=-1){
				  			siteURL = this.location.href.split('teachermatch.org/TMDEMO/');
				  		}else if(this.location.href.indexOf('teachermatch.org/')!=-1){
				  			siteURL = this.location.href.split('teachermatch.org/');
				  		}else if(this.location.href.indexOf('teachermatch/')!=-1){
				  			siteURL = this.location.href.split('teachermatch/');
				  		}else if(this.location.href.indexOf('8080/')!=-1){
				  			siteURL = this.location.href.split('8080/');
				  		}
						if(siteURL[1].indexOf("JobOrderType=2")!=-1 && siteURL[1].indexOf("batch")==-1){
							JobOrderVal=2;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("batch")==-1){
							JobOrderVal=3;
						}
						if(siteURL[1].indexOf("JobOrderType=2")!=-1 && siteURL[1].indexOf("batch")!=-1){
							JobOrderVal=2;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("batch")!=-1){
							JobOrderVal=3;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("entityID=3")!=-1){
							entityID=3;
						}
						menuPage=siteURL[1].substring(0,siteURL[1].indexOf(".do")+3);
					}catch(err){}					
					function CurrentPageWithMenuSelected(menuPageName,parentorderid,menuorderid,status){
					//alert("sonu"+menuPageName);							
						if(menuPageName.indexOf("|"+menuPage+"|")!=-1){
							if(JobOrderVal==null){							
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
								
							}else if(JobOrderVal==2  && menuPageName.indexOf("managejoborders")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}else if(JobOrderVal==3  && menuPageName.indexOf("schooljoborders")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}else if(JobOrderVal==2  && menuPageName.indexOf("|batchjoborder.do|")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}else if(entityID==3 && JobOrderVal==3  && menuPageName.indexOf("|batchjoborder.do|")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}else if(JobOrderVal==3  && menuPageName.indexOf("batchschooljoborder")!=-1){
								document.getElementById("menuid"+parentorderid).className= "activeText";								
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "activeText";
							}
						}
					}
					function CurrentPageWithParentMenuSelected(menuPageName,parentorderid){										
						if(menuPageName.indexOf(menuPage)!=-1){											
							document.getElementById("menuid"+parentorderid).className= "activeText";
							
						}
					}
					function generateScoreData()
					{
					    if(deviceType)
					    {
					    	$("#exelfileNotOpen").css({"z-index":"3000"});
			    			$('#exelfileNotOpen').show();
					    }
					    else
					    {				   
							$('#message2showConfirm').html('<spring:message code="msgcandscorespreadsheetdownloaded"/>');
							$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/getcandidatedata.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
							$('#myModal3').modal('show');
						 }
					}
					function updateCgGridData()
					{
						if(deviceType)
					    {
					    	$("#exelfileNotOpen").css({"z-index":"3000"});
			    			$('#exelfileNotOpen').show();
					    }
					    else
					    {
						$('#message2showConfirm').html('<spring:message code="msgWanttoUpdateCGGrid"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./updatecgbyuser.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
						}
					}
					function processData()
					{
						$('#message2showConfirm').html('<spring:message code="msgUpdateMosaicData"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/processdata.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
					}
					function exportData()
					{ 
					    if(deviceType)
					    {
					    	$("#exelfileNotOpen").css({"z-index":"3000"});
			    			$('#exelfileNotOpen').show();
					    }
					    else
					    {
						$('#message2showConfirm').html('<spring:message code="msgwanttoExportdata"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/getcandidatedataexport.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
						}
					}
				   </script>
				   
				   
				   <input type="hidden" id="visitedPageURL" name="visitedPageURL" value=""> 
				   <input type="hidden" id="visitedPageTitle" name="visitedPageTitle" value="">
<div class="container"  >
	<div style="float: left; margin-top:10px;" class="top15">
		<div> 
			 <% if(request.getServerName().contains("myedquest.org")){  %>						 
						 <a href="http://myedquest.org/" target="_blank" class="tmlogo"><img src="images/QuestLogoTM.png" alt="" class="top14"></a>
						 
						 <!-- Google Analytics code:  -->
							 <script>
							  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
							  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');							
							  ga('create', 'UA-56937648-1', 'auto');
							  ga('send', 'pageview');							
							</script>
						 
							<!-- Start of Async HubSpot Analytics Code -->
							
							  <script type="text/javascript">
							   var url      = window.location.href; 

							if(!(url.indexOf("candidategrid.do") > -1)){
							
							    (function(d,s,i,r) {
							      if (d.getElementById(i)){return;}
							      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
							      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/290901.js';
							      e.parentNode.insertBefore(n, e);
							    })(document,"script","hs-analytics",300000);
							    }
							  </script>
						 
						 
						 <%}else{ %>
						 	<% if(!request.getServerName().contains("kelly.teachermatch.org")){%>						 
								<a href="http://www.teachermatch.org" target="_blank" class="tmlogo">
						 <%}
						 if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){
						 	if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){
							 %>
								<img src="images/logoimage.png" alt="" class="top14" style="width: 133px;height: 50px;">
							<%}else{ %>
								<img src="images/logowithouttagline.png" alt="" class="top14">
							<%}
						}else{ %>
								<% if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>	
								<c:choose>
										<c:when test="${(userMaster.districtId.districtId eq 804800 && not empty userMaster.districtId.logoPath) || (jobOrder.districtMaster.districtId eq 804800 && not empty jobOrder.districtMaster.logoPath)}">
										<c:choose>
	    									<c:when test="${not empty userMaster}">
	       												 <img src="<%=(Utility.getValueOfPropByKey("contextBasePath")+"district/") %>${userMaster.districtId.districtId}/${userMaster.districtId.logoPath}" alt="" style="width: 75px;height: 50px;">
	    									</c:when>
	    									<c:otherwise>
	        										<img src="<%=(Utility.getValueOfPropByKey("contextBasePath")+"district/") %>${jobOrder.districtMaster.districtId}/${jobOrder.districtMaster.logoPath}" alt="" style="width: 75px;height: 50px;">
	    									</c:otherwise>
										</c:choose>
										</c:when>	
									<c:otherwise>
										<img src="images/logoimage.png" alt="" class="top14" style="width: 133px;height: 50px;">
									</c:otherwise>
								</c:choose>
								<%}		else{ %>
									<img src="images/Logo with Beta300.png" alt="" class="top14">
								<%}%>
						 <%}%>
						</a>	
						
						<script type="text/javascript">
							(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
							ga('create','UA-40462543-1','teachermatch.org');
							ga('send','pageview');
						</script>
						
						 <%} %>

						 </div>
	</div>	
	<div class="pull-right">		     		  
					<div class="btn-group">					
						<c:if test="${userMaster.firstName!=null && userSession.firstName==null}">
								<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown"
									href="#"><span class='tabletbodyadmin'> ${userMaster.firstName} ${userMaster.lastName}</span><span class="carethead"></span> 
								</a>								
							    <ul class="dropdown-menu pull-right">
									<li>
										<a href="usersettings.do"><spring:message code="lblSett"/></a>
									</li>
									<li class="divider">
									</li>
									<c:if test="${userMaster.entityType ne 1 && userSession.entityType ne 1 }">
										<li>
											<a href="myfolder.do"><spring:message code="lnkMyFolders"/></a>
										</li>
										<li class="divider">
										</li>
										<li>
											<a href="notification.do"><spring:message code="lnkNotifications"/></a>
										</li>
										<li class="divider">
										</li>
										<li>
											<a href="changelocation.do"><spring:message code="lblSwitchLoc"/></a>
										</li>
										<li class="divider">
										</li>
								   </c:if>
								   <li>
										<a href="logout.do"><spring:message code="lnkSignout"/>
</a>
								   </li>
							  </ul>
						</c:if>	
						<c:if test="${userSession.firstName!=null && userMaster.firstName!=null}">
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown"
								href="#"><span class='tabletbodyadmin'>${userSession.firstName} ${userSession.lastName}</span> <span class="carethead"></span> 
							</a>
							<ul class="dropdown-menu pull-right">
								<li>
									<a href="usersettings.do"><spring:message code="lblSett"/></a>
								</li>
								<li class="divider">
									&nbsp;
								</li>
								<c:if test="${userMaster.entityType ne 1 && userSession.entityType ne 1 }">
									<li>
										<a href="myfolder.do"><spring:message code="lnkMyFolders"/></a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="notification.do"><spring:message code="lnkNotifications"/></a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="changelocation.do"><spring:message code="lblSwitchLoc"/></a>
									</li>
									<li class="divider">
									</li>
								</c:if>
								<li>
									    <a href="logout.do"><spring:message code="lnkSignout"/></a>
								</li>
							</ul>
						</c:if>
						<c:if test="${userSession.firstName!=null && userMaster.firstName==null}">
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown"
								href="#"><span class='tabletbodyadmin'>${userSession.firstName} ${userSession.lastName}</span> <span class="carethead"></span>
						    </a>
							<ul class="dropdown-menu pull-right">
								<li>
									<a href="usersettings.do"><spring:message code="lblSett"/></a>
								</li>
								<li class="divider">
									&nbsp;
								</li>
								<c:if test="${userMaster.entityType ne 1 && userSession.entityType ne 1 }">
									<li>
										<a href="myfolder.do"><spring:message code="lnkMyFolders"/></a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="notification.do"><spring:message code="lnkNotifications"/></a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="changelocation.do"><spring:message code="lblSwitchLoc"/></a>
									</li>
									<li class="divider">
									</li>
								</c:if>
								<li>
									<a href="logout.do"><spring:message code="lnkSignout"/></a>
								</li>
							</ul>
						</c:if>
						<c:if test="${teacherDetail.firstName!=null}">
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown" href="#">
			   					<span class='tabletbodyadmin'>${teacherDetail.firstName}&nbsp;${teacherDetail.lastName}</span>
			   					<span class="carethead"></span>
			    			</a>
			    			<ul class="dropdown-menu">  

								<li><a href="userpreference.do"><spring:message code="lnkJobPreferences"/></a></li>
								<li><a href="settings.do"><spring:message code="lblSett"/></a></li>
							
								<c:if test="${schoolSelectionflag ==1}">
									<li>
										<a href="selectschool.do"><spring:message code="lnkSchoolSelection"/></a>
									</li>
								</c:if>								
								<li><a href="logout.do" onclick="clearCookies();"><spring:message code="lnkSignout"/></a></li>               
			    			</ul>		    	
			  			</c:if>					
					</div>
					<div>
						<img src="images/IntelliGlance Logo.png" style="width: 133px;">
					</div>
       </div>
       
	<div style="clear: both;"></div>
</div> 	
<div class="top10" style="margin-bottom: 65px;">			
	<ul id="css3menu" class="topmenu" style="width:100%; margin-bottom: 10px;">
		<div class="container">
		
			<c:set var="parentMenuIdChk" />
			<c:set var="menuIdCounter" value="0"/>
			<c:set var="parentMenu" value="100"/>
			<c:set var="flagMenu" value="0"/>
			<c:set var="imgStatusMain" value="1"/>
			<c:set var="imgStatusMainH" value="1"/>
			<c:set var="column" value="<div class='column'><ul>"/>
			<c:set var="columnEnd" value="</ul></div>"/>			
			
			<c:forEach var="parentmenu" items="${lstMenuMaster}" varStatus="status">
				
				<!--   start  Only One Menu show if sub menu is exists only one  -->
				<c:set var="count" value="0" scope="page" />
				<c:if test="${not empty userMaster.districtId && userMaster.districtId.displaySubmenuAsaParentMenu}">
					<c:set var="oneMenu" />
					<c:forEach var="item" items="${lstMenuMaster}">
					  <c:if test="${(parentmenu.parentMenuId.menuId==item.parentMenuId.menuId && item.subMenuId.menuId==null)}">
					    <c:set var="oneMenu" value="${item}"/>
					    <c:set var="count" value="${count + 1}" scope="page"/>
					  </c:if>
					</c:forEach>
				</c:if>
			   <!-- <input type="text" value="${count}" /> -->
			   <!--   END  Only One Menu show if sub menu is exists only one  -->
			  
			  
				<c:if test="${!fn:contains(parentMenuIdChk,parentmenu.parentMenuId.menuId)}">
				<c:set var="parentMenuIdChk" value="||${parentMenuIdChk}${parentmenu.parentMenuId.menuId}||" />
					
				<!--   start Only One Menu show if sub menu is exists only one  -->
				<c:choose>
					<c:when test="${count eq 1}">
						<li class="topmenu">
										<c:set var="menuIdCounter" value="${menuIdCounter+1}"/>
										<a id="submenuid${menuIdCounter}${subMenuIdCounter}"  class="" href="${oneMenu.pageName}" style="margin-top:7%;">	
										<i class="fa fa-square arrowcolor"></i>							
										${oneMenu.menuName}</a>
						</li>
					</c:when>
				<c:otherwise>
				<!--   End  Only One Menu show if sub menu is exists only one  -->
					
						<li class="toproot">
						<c:set var="menuIdCounter" value="${menuIdCounter+1}"/>
						<a href="#" id="menuid${menuIdCounter}" class="">
								<i class="fa fa-arrow-down arrowcolor"></i>
														
						${parentmenu.parentMenuId.menuName}</a>
						
									<c:set var="menuWidth" value="100"/>
									<c:set var="TotalMenuList" value="0"/>
									<c:forEach var="menucheck" items="${lstMenuMaster}" varStatus="status">
										<c:if test="${(parentmenu.parentMenuId.menuId==menucheck.parentMenuId.menuId && menucheck.subMenuId.menuId==null)}">
										<c:set var="TotalMenuList" value="${TotalMenuList+1}"/>
										</c:if>
									</c:forEach>
								
									<c:if test="${TotalMenuList>7 && TotalMenuList<=14}">
									<c:set var="menuWidth" value="${500}"/>										
									</c:if>
									<c:if test="${TotalMenuList>14 && TotalMenuList<=21}">
									<c:set var="menuWidth" value="${800}"/>									
									</c:if>
									<c:if test="${TotalMenuList>21}">
									<c:set var="menuWidth" value="${1000}"/>									
									</c:if>
						<div class="submenu" style=" width:${menuWidth}px;" >
							
							<c:set var="imgStatus" value="1"/>
							<c:set var="subMenuIdCounter" value="0"/>
							<c:set var="subMenuIdCheck" value="0"/>
							<c:set var="flag" value="0"/>
							<c:set var="roundDiv" value="0"/>
							  <c:forEach var="menu" items="${lstMenuMaster}" varStatus="status">
										<c:if test="${(parentmenu.parentMenuId.menuId==menu.parentMenuId.menuId && menu.subMenuId.menuId==null)}">
										<c:set var="subMenuIdCounter" value="${subMenuIdCounter+1}"/> 
										 <c:if test="${(subMenuIdCounter%7!=0) && flag==0}">																													
											${column}  
											<c:set var="flag" value="1"/>
										</c:if>
									<li><a id="submenuid${menuIdCounter}${subMenuIdCounter}"  class="" href="${menu.pageName}"   data-placement="top"  data-original-title="${menu.description}" data-toggle="tooltip" >	
									<i class="fa fa-square arrowcolor"></i>									
									${menu.menuName} </a>
									<div class="submenu">
										<div class="column">
										<ul>
										<c:set var="childMenuIdCounter" value="0"/> 
										 <c:forEach var="menu1" items="${lstMenuMaster}" varStatus="status">
									     	<c:if test="${menu.menuId==menu1.subMenuId.menuId  }">
									     	<c:set var="childMenuIdCounter" value="${childMenuIdCounter+1}"/>
									     	<c:if test="${(childMenuIdCounter==1)}">
									     		 <c:if test="${(childMenuIdCounter!=0)}">
										  <li class="bl"><a  style="border-width:1px 1px 1px 1px;border-style:solid;border-color:#00658e;-webkit-border-top-right-radius:10px; border-radius:0 10px 0 0; " ><i class="fa fa-square circlecolor"></i></a></li>																						
										</c:if>
									     	</c:if>
									     	 <li >									     	 
									     	  <a  id="submenuid${menuIdCounter}${subMenuIdCounter}" href="${menu1.pageName}" data-placement="top" data-original-title="${menu1.description}" data-toggle="tooltip"><i class="fa fa-square arrowcolor"></i> ${menu1.menuName}</a>
									      	 	<c:set var="childMenuIdClass" value=""/>
									      	 </li>	
									      	 <script>
										 		CurrentPageWithMenuSelected("${menu.pageNameWithSubPage}",${parentmenu.parentMenuId.orderBy},${subMenuIdCounter},"${menu.status}");
											</script>							      	 
											</c:if>
										 </c:forEach>
										 <c:if test="${(childMenuIdCounter!=0)}">
										  <li class="bl"><a style="border-width:1px 1px 1px 1px;border-style:solid;border-color:#00658e;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>																						
										</c:if>
										</ul>
										</div>
									</div>
									
									
									
									</li>	
									<c:if test="${(subMenuIdCounter%7==0)}">
									<c:choose>
								        <c:when test="${roundDiv==0}">
								        <li class="bl"><a  style="border-width:1px 1px 1px 1px;border-style:solid;border-color:#00658e;-webkit-border-bottom-left-radius:10px; border-radius:0 0 0 10px; " ><i class="fa fa-square circlecolor"></i></a></li>
								       <c:set var="roundDiv" value="1"/> 
								        </c:when>								      
								        <c:otherwise>								        
								        <c:if test="${TotalMenuList==subMenuIdCounter}">								        
								        <li class="bl"><a  style="border-width:1px 1px 1px 1px;border-style:solid;border-color:#00658e;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 0; " ><i class="fa fa-square circlecolor"></i></a></li>
								       </c:if>
								       <c:if test="${TotalMenuList!=subMenuIdCounter}">								       
								           <li class="bl"><a  style="border-width:1px 1px 1px 1px;border-style:solid;border-color:#00658e;" ><i class="fa fa-square circlecolor"></i></a></li>
								        </c:if>								       
								        </c:otherwise>
   									 </c:choose>																		 		
										${columnEnd}
										<c:set var="flag" value="0"/>						
									</c:if>
									 <script>
											CurrentPageWithMenuSelected("${menu.pageNameWithSubPage}",${parentmenu.parentMenuId.orderBy},${subMenuIdCounter},"${menu.status}");
									</script>
									</c:if>
								   </c:forEach>
									<c:set var="Reminder" value="${(7-subMenuIdCounter%7)}"/>
										<c:if test="${(subMenuIdCounter>7 && Reminder!=0 && Reminder!=7)}">
										<c:forEach var="i" begin="1" end="${Reminder}">
  											 <li class="bl"><a  style="border-width:0px 0px 1px 0px;border-style:solid;border-color:#00658e; color:#00658e; " >g<i class="fa fa-square circlecolor"></i></a></li>
										</c:forEach>										
										</c:if>
										
									 <c:choose>
								        <c:when test="${(subMenuIdCounter<7)}">
								        <li class="bl"><a  style="border-width:1px 1px 1px 1px;border-style:solid;border-color:#00658e;-webkit-border-bottom-left-radius:10px;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 10px; " ><i class="fa fa-square circlecolor"></i></a></li>
								       </c:when>
										<c:when test="${(Reminder==7)}">
								       									
										</c:when>								      
								        <c:otherwise>
								        <li class="bl"><a  style="border-width:1px 1px 1px 1px;border-style:solid;border-color:#00658e;-webkit-border-bottom-right-radius:10px; border-radius:0 0 10px 0; " ><i class="fa fa-square circlecolor"></i></a></li>
								        </c:otherwise>
   									 </c:choose>
								
						</div>
					</li>
				  </c:otherwise>
				</c:choose>
				</c:if>
				
				 <c:if test="${parentmenu.parentMenuId.menuId==null && !fn:contains(parentMenuIdChk,parentmenu.menuId)}">
											
											<li class="topmenu">
											    <c:set var="parentMenu" />	
											     	
											     <c:set var="menuIdCounter" value="${menuIdCounter+1}"/>									    
													   <a href="${parentmenu.pageName}" id="menuid${menuIdCounter}" >
													   <i class="fa fa-square arrowcolor"></i>
									
													   	${parentmenu.menuName}	  </a>
											  </li>	
											  <script>
												  CurrentPageWithParentMenuSelected("${parentmenu.pageNameWithSubPage}",${menuIdCounter});
											 </script>									   	
										</c:if>
			</c:forEach>
			
			<c:if test="${userMaster.entityType eq 2 || userMaster.entityType eq 1}">
			<% if(request.getServerName().contains("titan.teachermatch.org")){  %>
				<div style="float: right;padding-top:8px; " ><a title="Configuration Wizard"  href="home.do" style="padding-right:0px;cursor:pointer;"><img  src="images/gear-icon.png" width="25px" height="25px"/></a></div>
			<%}%>
			</c:if>	
			
			<c:if test="${userMaster.entityType eq 1}">
			<% if(request.getServerName().contains("cloud.teachermatch.org") || request.getServerName().contains("canada-en-test.teachermatch.org")  || request.getServerName().contains("platform")){  %>
				<div style="float: right;padding-top:8px; " ><a title="Configuration Wizard"  href="home.do" style="padding-right:0px;cursor:pointer;"><img  src="images/gear-icon.png" width="25px" height="25px"/></a></div>
			<%}%>
			</c:if>	
				
		</div>			
	</ul>
</div>

</html>
<c:if test="${powerProfile eq 'hide'}">
	<script>
	try
	{
	document.getElementById("menuid5").style.display="none";
	document.getElementById("menuid2").style.display="none";
	}
	catch(err)
	{
	
	}
		
	</script>
</c:if>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
