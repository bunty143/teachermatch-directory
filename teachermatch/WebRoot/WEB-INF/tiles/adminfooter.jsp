
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <%@ page import="tm.utility.Utility" %>
<div class="row-fluid"
	style="background: #ececec; height: 98px; margin: 10px 0 0;">
	<div class="container footer-pad">
		<div class="span4">
		<% if(request.getServerName().contains("myedquest.org")){  %>
		      
						 <img src="images/QuestLogoTM.png" alt="" >
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						 	<a href="http://www.teachermatch.org" target="_blank"><img src="images/GrayTM-no tag line_50.png" alt="" ></a>
						 <%
						 }else{ %>
						 <a href="http://www.teachermatch.org" target="_blank"><img src="images/Footer Logo Small.png" width="202" height="33" alt=""></a>
						 <%} %>
			
			
			<p class="copyright-text">
				&copy;<spring:message code="magTmTousDroitsRes"/>
			</p>
		</div>
		<div class="span8" style="display: none;"> 
			<div class="Footerlink">
				<ul>
					<li>
						<a href="view-edit-user.html"> <spring:message code="headUser"/></a>
					</li>
					<li>
						<a href="manage-districts.html"> <spring:message code="headManage"/></a>
					</li>
					<li>
						<a href="manage-district-jobs.html"><spring:message code="lblJobOrders"/></a>
					</li>
					<li style="border-right: none;">
						<a href="#"> <spring:message code="lblReports"/></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="span4 pull-right">
			<div class="socialicon-Box">
				<div class="socialicon-icon">
					<ul>
						<li><a href="https://www.facebook.com/TeacherMatch" target="_blank"><img src="images/f-con.jpg" width="17" height="16" alt=""></a></li>
						<li><a href="http://www.linkedin.com/company/teachermatch" target="_blank"><img src="images/in-icon.jpg" width="17" height="16" alt=""></a></li>
						<li><a href="https://twitter.com/TeacherMatch" target="_blank"><img src="images/t-icon.jpg" width="17" height="16" alt=""></a></li>
					</ul>
				</div>
			</div>
			<div class="website-Box" style="display: none;">
				<img src="images/wesiteby-toi.jpg" width="73" height="15" alt="">
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-40462543-1', 'teachermatch.org');
  ga('send', 'pageview');
</script>
