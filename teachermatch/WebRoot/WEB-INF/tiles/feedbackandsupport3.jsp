<%@ page import="tm.utility.Utility" %>
<script type="text/javascript" src="js/jquery-te-1.3.2.2.min.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="css/jquery-te-1.3.2.2.css" charset="utf-8" />
<!-- Editor 2 -->
  <link href="css/rating_simple.css" rel="stylesheet" type="text/css">
  <script type="text/javascript" src="js/rating_simple.js"></script>  
      <script language="javascript" type="text/javascript">         
          function setRating()
          {  
           /*  $( "#myModal1").draggable({
			    handle: ".modal-header",cursor: "move"
			 });  
			*/
             $(function() {
                $("#rating_simple1").webwidget_rating_simple({
                    rating_star_length: '5',
                    rating_initial_value: '',
                    rating_function_name: '',//this is function name for click
                    directory: 'images/'
                });

            });
           }
           //setRating();
        </script>
        
<!-- Feedback -->
<div  onclick="feedBack();" class="feedbackposition" style="cursor: pointer;background-image: url(images/tab-right-dark.png); border-top-width: 1px; border-bottom-width: 1px; border-left-width: 1px; border-style: solid none solid solid; border-top-color: rgb(255, 255, 255); border-bottom-color: rgb(255, 255, 255); border-left-color: rgb(255, 255, 255); border-top-left-radius: 4px; border-top-right-radius: 0px; border-bottom-right-radius: 0px; border-bottom-left-radius: 4px; -webkit-box-shadow: rgba(255, 255, 255, 0.247059) 1px 1px 1px inset, rgba(0, 0, 0, 0.498039) 0px 1px 2px; box-shadow: rgba(255, 255, 255, 0.247059) 1px 1px 1px inset, rgba(0, 0, 0, 0.498039) 0px 1px 2px; font-style: normal; font-variant: normal; font-weight: bold; font-size: 14px; line-height: 1em; font-family: Arial, sans-serif; position: fixed; right: 0px;  z-index: 9999; background-color: rgb(0, 122, 180); display: block; background-position: 50% 0px; background-repeat: no-repeat no-repeat;">
	<br><br><br>
	 <%if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						     <a href="javascript:void(0);"><img src="images/feedback-tab-fr.png" alt="" ></a>
                                       <%}else{ %>
						        <a href="javascript:void(0);"><img src="images/feedback-tab.png"></a>
                                      <%} %>

	<br><br>
</div>

<div class="modal" style="display: none; z-index: 2000;" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
   <div class="modal-dialog-for-feedbackandsupport">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Feedback & Support</h3>
      </div>
      <iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	  </iframe>
      <div class="modal-body">
    
      	 <form id='frmFeedbackUpload' enctype='multipart/form-data' method='post' target='uploadFrame'  class="form-inline" onsubmit="return validateFeedBack();" accept-charset="UTF-8">
			<input type='hidden' id="visitedPageURL" name="visitedPageURL" /> 
			<input type='hidden' id="visitedPageTitle" name="visitedPageTitle" />
			<div class="control-group">
				<label class="radio-inline">
		           <input type="radio"  value="1" id="spt" name="reqType" checked="checked" onclick='showDisplay(this);'>
		           I want to send a message to support
		   		</label>   	
		   	</div>
		   	<div class="control-group">		
		   		<label class="radio-inline">
		   		<input type="radio"  value="2"  name="reqType" onclick='showDisplay(this);'>
		           I want to provide feedback
		   		</label>
			</div>
				                           	
			<div class="control-group">
				<div class='divErrorMsg' id='errordivFeedBack' style="display: block;"></div>
			</div>
				
				
				<div id='feedback' style='display:none;'>
		        
		        <div class="control-group">
					<div style='' id="ratingDiv" style="padding-top:30px;">
						<input  value="0" id="rating_simple1" name="starRating" type="hidden">
					</div>
				</div>
		      
		        <div class="control-group">
					<div class="" id="msgFeed">
				    	<label><strong>Feedback</strong><span class="required">*</span></label>
			        	<textarea rows="5" class="span8" cols="" id="feedbackmsg" name="feedbackmsg" maxlength="1000"></textarea>
					</div>
				</div>
				
		 		</div>
		 	
				<div id='support'>
				
				<div class="control-group">
					<div class="">
				    	<label><strong>Subject</strong><span class="required">*</span></label><br/>
			        	<input id="msgSubject" name="msgSubject" type="text" class="form-control" maxlength="80" />
					</div>
				</div>
			            
		        <div class="control-group">
					<div id="msgSpt">
				    	<label><strong>Message</strong><span class="required">*</span></label>
			        	<textarea rows="5" class="span8" cols="" id="msg" name="msg" maxlength="1000"></textarea>
			    
			        	<div style="clear: both"></div>
			        	<div id='file1' style="padding-top:12px;">
			        	
			        	<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> 
			        	<a href='javascript:void(0);' onclick='addFileType();'>Attach a File</a>
			        	</div>
					</div>
				</div>
		 		</div>
		 		Or contact our support team at 8559800511.   
		 		<br/>
		 		<div id="lodingImg" style='display:none;text-align:center;padding-top:4px;'><img src="images/loadingAnimation.gif" /> Sending...</div>
 		</form>
 	
      </div>
      <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
 		<button class="btn btn-primary" onclick="saveFeedBack()" >Send</button>
      </div>
    </div>
  </div>
</div>
<script language='javascript'>

function getVisitedURL()
{
	var ddd=document.URL;
	document.getElementById("visitedPageURL").value=""+ddd+"";
	document.getElementById("visitedPageTitle").value=""+document.title+"";
}
getVisitedURL();
function showDisplay(dis)
{
	$('#errordivFeedBack').empty();
	$('#lodingImg').hide();
	//alert(dis.value);
	if(dis.value==1)
	{
		$('#feedback').hide();
		$('#support').show();
		$('#msgSubject').focus();
		
	}else
 	{
 		$('#feedback').show();
 		$('#support').hide();
 		$('#feedbackmsg').focus();
 		//$('#feedbackmsg').css("background-color", "");
 	}
 	
 	//<input  value='0' id='rating_simple1' name='starRating' type='text'>
 	$('#ratingDiv').html("<input  value='0' id='rating_simple1' name='starRating' type='hidden'>");
 	setRating();
 	removeFile();
 	
 	$('#msgFeed').find(".jqte_editor").css("background-color", "");
	$('#msgSpt').find(".jqte_editor").css("background-color", "");
	$('#msgSubject').css("background-color", "");
	$('#msgFeed').find(".jqte_editor").html("");
	$('#msgSpt').find(".jqte_editor").html("");
 	document.getElementById("msgSubject").value="";
 	//document.getElementById("msg").value="";
 	//document.getElementById("f1").value="";
 	//document.getElementById("feedbackmsg").value="";
}
function validateFeedBack()
{

	$('#errordivFeedBack').empty();
	$('#msgSubject').css("background-color", "");
	//$('#msg').css("background-color", "");
	$('#f1').css("background-color", "");
	//$('#feedbackmsg').css("background-color", "");
	$('#msgFeed').find(".jqte_editor").css("background-color", "");
	$('#msgSpt').find(".jqte_editor").css("background-color", "");
	
	var cnt=0;
	var focs=0;
	
	var reqType=$('input[name=reqType]:radio:checked').val();
	
	if(reqType==1)
	{
	
		var fileSize=0;		
		if ($.browser.msie==true)
	 	{	
		    fileSize = 0;	   
		}
		else
		{	
			if(document.getElementById("f1"))
			if(document.getElementById("f1").files[0]!=undefined)
				fileSize = document.getElementById("f1").files[0].size;
		}
			//alert(fileSize);
		if(trim($('#msgSubject').val())=="")
		{
			$('#errordivFeedBack').append("&#149; Please enter Subject.<br>");
			if(focs==0)
				$('#msgSubject').focus();
			$('#msgSubject').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
		/*if(trim($('#msg').val())=="")
		{
			$('#errordivFeedBack').append("&#149; Please enter Message.<br>");
			if(focs==0)
				$('#msg').focus();
			$('#msg').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}*/
		if ($('#msgSpt').find(".jqte_editor").text().trim()=="")
		{
			$('#errordivFeedBack').append("&#149; Please enter Message.<br>");
			if(focs==0)
				$('#msgSpt').find(".jqte_editor").focus();
			$('#msgSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else
		{
			var charCount=$('#msgSpt').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			//alert(count);
 			if(count>1000)
 			{
				$('#errordivFeedBack').append("&#149; Message length cannot exceed 1000 characters.<br>");
				if(focs==0)
					$('#msgSpt').find(".jqte_editor").focus();
				$('#msgSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
		if(fileSize>=10485760)
		{		
			$('#errordivFeedBack').append("&#149; File size must be less then 10mb.<br>");
			if(focs==0)
				$('#f1').focus();			
			$('#f1').css("background-color","#F5E7E1");
			cnt++;focs++;	
		}
	}else
	{
		/*if(trim($('#feedbackmsg').val())=="")
		{
			$('#errordivFeedBack').append("&#149; Please enter Feedback.<br>");
			if(focs==0)
				$('#feedbackmsg').focus();
			$('#feedbackmsg').css("background-color", "#F5E7E1");
			cnt++;focs++;
		}*/
		
		if ($('#msgFeed').find(".jqte_editor").text().trim()=="")
		{
			$('#errordivFeedBack').append("&#149; Please enter Feedback.<br>");
			if(focs==0)
				$('#msgFeed').find(".jqte_editor").focus();
			$('#msgFeed').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}else
		{
			var charCount=$('#msgFeed').find(".jqte_editor").text().trim();
			var count = charCount.length;
 			//alert(count);
 			if(count>1000)
 			{
				$('#errordivFeedBack').append("&#149; Feedback length cannot exceed 1000 characters.<br>");
				if(focs==0)
					$('#msgFeed').find(".jqte_editor").focus();
				$('#msgFeed').find(".jqte_editor").css("background-color", "#F5E7E1");
				cnt++;focs++;
			}
		}
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#errordivFeedBack').show();
		return false;
	}
}
function saveFeedBack()
{

	if(!validateFeedBack())
		return;
	
	$('#lodingImg').show();
	
	document.getElementById('frmFeedbackUpload').action="feedbackUploadServlet.do";
	$('#frmFeedbackUpload').submit();
	
}
function hideFeedBack(flg)
{
	
	
	//document.getElementById('f1').value="";
	//$('#errordivFeedBack').html("");
	$('#lodingImg').hide();
	$('#myModal1').modal('hide');
	if(flg==1)
	{
		$('#message2show').html("Your message is successfully sent to TeacherMatch Support Team.");
		$('#myModal2').modal('show');
	}
	else if(flg==2)
	{
		$('#message2show').html("Your feedback is successfully sent to TeacherMatch Team.");
		$('#myModal2').modal('show');
	}
	else if(flg==3)
	{
		alert("Oops: Your Session has expired!");  document.location = 'signin.do';
	}
	
	document.getElementById('msgSubject').value="";
	document.getElementById('msg').value="";
	document.getElementById('feedbackmsg').value="";
}
function addFileType()
{
	$('#file1').empty();
	$('#file1').html("<a href='javascript:void(0);'onclick='removeFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='f1' class='left20 top-35' name='f1' size='20' title='Choose a File' type='file'>&nbsp;Maximum file size 10MB.");
}
function removeFile()
{
	$('#file1').empty();
	$('#file1').html("<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addFileType();'>Attach a File</a>");
}
function feedBack()
{
	$('#myModal1').modal('show');
	$('#spt').click();
	
	$('#file1').html("<a href='javascript:void(0);' onclick='addFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addFileType();'>Attach a File</a>");
	$('#msgSubject').focus();
	$('#errordivFeedBack').empty();
	$('#msgSubject').css("background-color", "");
	document.getElementById('msgSubject').value="";
	
}
function hideDiv()
{
	$('#docfileNotOpen').hide();
	$('#exelfileNotOpen').hide();
}
</script>

<div class="modal" id="myModal2" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
      </div>
      <div class="modal-body">
       <div class="control-group" id='message2show'>
		</div>
      </div>
      <div class="modal-footer">
        	<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" style="display: none;z-index: 5000" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
      </div>
      <div class="modal-body">
      	<div class="control-group" id='message2showConfirm'>
		</div>
      </div>
      <div class="modal-footer" id="footerbtn">
        	<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
      </div>
    </div>
  </div>
</div>



<!-- End Feedback -->
<div class="modal" style="display: none;z-index: 5000;top: 0px;" id="myModalv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog-for-cgmessage">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtn'>x</button>
		<h3 id="myModalLabelId">TeacherMatch</h3>
      </div>
      <div class="modal-body" style="max-height: 450px;overflow-y:auto;">
      	<table width="100%"><tr>
			<td width="15%" valign="top" style="padding-top: 0px;">
			<div class="control-group" id='warningImg1'>
				</div>
			</td>
			<td width="85%" style="padding-left: 10px;">
			<div class="control-group" id='message2showConfirm1'>
				</div>
			</td>
			</tr></table>
			<div class="control-group" id='nextMsg'>
				</div>	
		 	</div>
      <div class="modal-footer" id='footerbtn1'>
        	<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
      </div>
    </div>
  </div>
</div>


<div class="modal" style="display: none;z-index: 5000" id="myModalvk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog-for-cgmessage">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
      </div>
      <div class="modal-body">
      	<table width="100%"><tr>
			<td width="15%" valign="top" style="padding-top: 8px;">
			<div class="control-group" id='warningImg1k'>
				</div>
			</td>
			<td width="85%" style="padding-left: 10px;">
			<div class="control-group" id='message2showConfirm1k'>
				</div>
			</td>
			</tr>
		</table>
			<div class="control-group" id='nextMsgk'>
		</div>	
      </div>
      <div class="modal-footer" id='footerbtn1k'>
        	<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
      </div>
    </div>
  </div>
</div>

<!-- By alok for tablet -->
<div  class="modal hide"  id="docfileNotOpen"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog'>
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" >
			 Document is in MS Word and hence can not be opened.
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">Close</button> 		
 	</div>
  </div>
 </div>
</div>



<div  class="modal hide"  id="exelfileNotOpen"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog'>
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" >
			 Document is in Exel and hence can not be opened.
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">Close</button> 		
 	</div>
  </div>
 </div>
</div>

<div  class="modal hide"  id="printmessage"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class='modal-dialog'>
   <div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" >
			 Please make sure that pop ups are allowed on your browser.
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">Close</button> 		
 	</div>
   </div>
  </div>
</div>
