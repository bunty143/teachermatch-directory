<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>

<div  style="background: #AFB3B6; min-height: 90px;" class="top20">
	<div class="container" style="margin: auto;">
		<div style="float: left;position: relative;" class="top15">
			<%if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
				<a href="http://www.teachermatch.org" target="_blank"><img src="images/GrayTM-no tag line_50.png" alt="" ></a>
			<%
			}else{ %>
				<a href="http://www.teachermatch.org" target="_blank"><img src="images/tmgraylogo.png" alt="" > </a>
			<%} %>			
		   <br/>
		</div>
		<div style="float: right;color: #4E4D52; margin-top:10px;"  class="gray" >		   
			<b>&nbsp;&nbsp;&nbsp;&nbsp; &copy; <spring:message code="magTmTousDroitsRes"/></b>
		</div>
	</div>              
</div>

<script type="text/javascript">
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-40462543-1', 'teachermatch.org');
  ga('send', 'pageview');
</script>
