<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<header>
<div class="container-fluid p0" id='testheader' style='border:0px solid red;'>
<div class="header-bg">
     <div class="container">
     <div class="header-backround">       
          <div class="navbar navbar-default" role="navigation">         
              <div class="navbar-header">
                 <a href="http://www.teachermatch.org" target="_blank">
					 <% if(request.getServerName().contains("myedquest.org")){  %>
						 <img src="images/QuestLogoTM.png" alt="" >
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){
						 		if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" class="top14" style="width: 133px;height: 50px;">	
								<%}else{ %>
						 			<img src="images/logowithouttagline.png" alt="" class="top14">
						 		<%}
						 	}
						 	else if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){
							 %>
								<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
							<%}else{ %>
								<img src="images/Logo with Beta300.png" alt="" >	
							<%}%>
				 </a>								
	             <a class="btn btn-navbar btn-default navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	              <span class="nb_left pull-left"> <span class="fa fa-reorder"></span> </span> 
	              <span class="nb_right pull-right">menu</span> 
	             </a>
               </div>  			
          </div>
                                      
          <div id="social_media_wrapper"> 
                <div class="pull-right">      		  
					<div class="btn-group">							
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown" href="#">
			   					<span class='tabletbodyadmin'>${teacherDetail.firstName}&nbsp;${teacherDetail.lastName}</span>
			   					<span class="carethead"></span>
			    			</a>
			    			<ul class="dropdown-menu">    
							
								<li>
								<a href="settings.do"><spring:message code="lblSett"/></a>
								</li>
								<li class="divider">
									&nbsp;
								</li>
								<li>
									<a href="logout.do"><spring:message code="lnkSignout"/></a>
								</li>
								              
			    			</ul>	    	
			  							
					</div>				
				  </div>
              </div> 
         </div>     
     </div></div></div>
</header>
<div style="height: 83px;"></div>
