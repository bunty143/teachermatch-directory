<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>

<div class="container-fluid p0" id='testheader' style='border:0px solid red;'>
	<div class="header-bg">
		<div class="container header-backround">
			<div class="span6 logo">
			 <a href="http://www.teachermatch.org" target="_blank">
				<% if(request.getServerName().contains("myedquest.org")){  %>
						 <img src="images/QuestLogoTM.png" alt="" >
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){
						 		if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
								<%}else{ %>
						 			<img src="images/logowithouttagline.png" alt="" class="top14">
						 		<%}
						 	}
						 	else {
						 		if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" style="width: 133px;height: 50px;">	
								<%}else{ %>
									<img src="images/Logo with Beta300.png" alt="" />	
								<%}
							}%>
			 </a>
			</div>
			<div class="span8 pull-right">

				  <div class="pull-right">
					<div class="btn-group">
					
						<c:if test="${userMaster.firstName!=null && userSession.firstName==null}">
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown"
								href="#"><span class='topheader'>${userMaster.firstName} ${userMaster.lastName} </span><span class="carethead"></span> </a>
							<ul class="dropdown-menu pull-right">
							<c:if test="${userMaster.isExternal==null || userMaster.isExternal ne true}">
								<li>
									<a href="usersettings.do"><spring:message code="lblSett"/></a>
								</li>
								<li class="divider">
								</li>
								<c:if test="${userMaster.entityType ne 1 && userSession.entityType ne 1 }">
									<li>
										<a href="myfolder.do"><spring:message code="lnkMyFolders"/></a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="notification.do"><spring:message code="lnkNotifications"/></a>
									</li>
									<li class="divider">
									</li>
								</c:if>
							</c:if>
								<li>
									<a href="logout.do"><spring:message code="lnkSignout"/></a>
								</li>
							</ul>
						</c:if>
						<c:if test="${userSession.firstName!=null && userMaster.firstName!=null}">
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown"
								href="#"><span class='topheader'>${userSession.firstName} ${userSession.lastName}</span> <span class="carethead"></span> </a>
							<ul class="dropdown-menu pull-right">
								<li>
									<a href="usersettings.do"><spring:message code="lblSett"/></a>
								</li>
								<li class="divider">
									&nbsp;
								</li>
								<c:if test="${userMaster.entityType ne 1 && userSession.entityType ne 1 }">
									<li>
										<a href="myfolder.do"><spring:message code="lnkMyFolders"/></a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="notification.do"><spring:message code="lnkNotifications"/>
</a>
									</li>
									<li class="divider">
									</li>
								</c:if>
								<li>
									<a href="logout.do"><spring:message code="lnkSignout"/></a>
								</li>
							</ul>
						</c:if>
						<c:if test="${userSession.firstName!=null && userMaster.firstName==null}">
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown"
								href="#"><span class='topheader'>${userSession.firstName} ${userSession.lastName}</span> <span class="carethead"></span> </a>
							<ul class="dropdown-menu pull-right">
								<li>
									<a href="usersettings.do"><spring:message code="lblSett"/></a>
								</li>
								<li class="divider">
									&nbsp;
								</li>
								<c:if test="${userMaster.entityType ne 1 && userSession.entityType ne 1 }">
									<li>
										<a href="myfolder.do"><spring:message code="lnkMyFolders"/></a>
									</li>
									<li class="divider">
									</li>
									<li>
										<a href="notification.do"><spring:message code="lnkNotifications"/></a>
									</li>
									<li class="divider">
									</li>
								</c:if>
								<li>
									<a href="logout.do"><spring:message code="lnkSignout"/></a>
								</li>
							</ul>
						</c:if>
						<c:if test="${teacherDetail.firstName!=null}">
							<a class="btn dropdown-toggle John-Oliveri" data-toggle="dropdown" href="#">
			   					<span class='topheader'>${teacherDetail.firstName}&nbsp;${teacherDetail.lastName}</span>
			   					<span class="carethead"></span>
			    			</a>
			    			<ul class="dropdown-menu">    
								<li><a href="userpreference.do"><spring:message code="lnkJobPreferences"/></a></li>
								<c:if test="${InterTeacherDetail!=null && InterTeacherDetail==true}">
								<li><a href="internaltransfer.do?districtId=${districtIdForTeacher}&teacherId=${teacherDetail.teacherId}"><spring:message code="lblIntrCand"/></a></li>
								</c:if>
								<li><a href="settings.do"><spring:message code="lblSett"/></a></li>
								<li class="divider">&nbsp;</li>
								<li><a href="logout.do"><spring:message code="lnkSignout"/></a></li>                
			    			</ul>		    	
			  			</c:if>
					</div>
				  </div>
				  <!-- This method for selected menu as well as submenu. -->
				  <script>
				    var siteURL =null,menuPage=null,JobOrderVal=null,entityID=null;
				  	try{
				  		if(this.location.href.indexOf('teachermatch.org/TMDEMO/')!=-1){
				  			siteURL = this.location.href.split('teachermatch.org/TMDEMO/');
				  		}else if(this.location.href.indexOf('teachermatch.org/')!=-1){
				  			siteURL = this.location.href.split('teachermatch.org/');
				  		}else if(this.location.href.indexOf('teachermatch/')!=-1){
				  			siteURL = this.location.href.split('teachermatch/');
				  		}else if(this.location.href.indexOf('8080/')!=-1){
				  			siteURL = this.location.href.split('8080/');
				  		}
						if(siteURL[1].indexOf("JobOrderType=2")!=-1 && siteURL[1].indexOf("batch")==-1){
							JobOrderVal=2;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("batch")==-1){
							JobOrderVal=3;
						}
						if(siteURL[1].indexOf("JobOrderType=2")!=-1 && siteURL[1].indexOf("batch")!=-1){
							JobOrderVal=2;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("batch")!=-1){
							JobOrderVal=3;
						}
						if(siteURL[1].indexOf("JobOrderType=3")!=-1 && siteURL[1].indexOf("entityID=3")!=-1){
							entityID=3;
						}
						menuPage=siteURL[1].substring(0,siteURL[1].indexOf(".do")+3);
					}catch(err){}
					function CurrentPageWithMenuSelected(menuPageName,parentorderid,menuorderid,status){
						if(menuPageName.indexOf("|"+menuPage+"|")!=-1){
							if(JobOrderVal==null){
								document.getElementById("menuid"+parentorderid).className= "dropdown-toggle active";
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "active";
							}else if(JobOrderVal==2  && menuPageName.indexOf("managejoborders")!=-1){
								document.getElementById("menuid"+parentorderid).className= "dropdown-toggle active";
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "active";
							}else if(JobOrderVal==3  && menuPageName.indexOf("schooljoborders")!=-1){
								document.getElementById("menuid"+parentorderid).className= "dropdown-toggle active";
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "active";
							}else if(JobOrderVal==2  && menuPageName.indexOf("|batchjoborder.do|")!=-1){
								document.getElementById("menuid"+parentorderid).className= "dropdown-toggle active";
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "active";
							}else if(entityID==3 && JobOrderVal==3  && menuPageName.indexOf("|batchjoborder.do|")!=-1){
								document.getElementById("menuid"+parentorderid).className= "dropdown-toggle active";
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "active";
							}else if(JobOrderVal==3  && menuPageName.indexOf("batchschooljoborder")!=-1){
								document.getElementById("menuid"+parentorderid).className= "dropdown-toggle active";
								document.getElementById("submenuid"+parentorderid+menuorderid).className= "active";
							}
						}
					}
					function CurrentPageWithParentMenuSelected(menuPageName,parentorderid){
						if(menuPageName.indexOf(menuPage)!=-1){
							document.getElementById("menuid"+parentorderid).className= "active";
						}
					}
					function generateScoreData()
					{
						$('#message2showConfirm').html('<spring:message code="msgcandscorespreadsheetdownloaded"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/getcandidatedata.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
					}
					function updateCgGridData()
					{
						$('#message2showConfirm').html('<spring:message code="msgWanttoUpdateCGGrid"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./updatecgbyuser.do' >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
					}
					function processData()
					{
						$('#message2showConfirm').html('<spring:message code="msgUpdateMosaicData"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/processdata.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
					}
					function exportData()
					{
						$('#message2showConfirm').html('<spring:message code="msgwanttoExportdata"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/getcandidatedataexport.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
					}
				   </script>
					<div class="span16 pull-right navbarp navwdh">
					<div class="navbar navbar-static" id="navbar-example">
						<div class="navwidth">
							<div style="width: auto;" class="container">
								<ul role="navigation" class="nav">
									<c:set var="parentMenuIdChk" />
									<c:set var="menuIdCounter" value="0"/>
									<c:set var="parentMenu" value="100"/>
									<c:set var="flagMenu" value="0"/>
								
									<c:forEach var="parentmenu" items="${lstMenuMaster}" varStatus="status">
											<c:if test="${!fn:contains(parentMenuIdChk,parentmenu.parentMenuId.menuId)}">
											<c:set var="parentMenuIdChk" value="||${parentMenuIdChk}${parentmenu.parentMenuId.menuId}||" />
											<li class="dropdown">
											    <c:set var="menuIdCounter" value="${menuIdCounter+1}"/>
												  <a data-toggle="dropdown"  id="menuid${menuIdCounter}" class="dropdown-toggle" role="button" href="#">
													${parentmenu.parentMenuId.menuName}<b class="carethead"></b>
											      </a>
												  <ul aria-labelledby="drop1" role="menu" class="dropdown-menu">
												     <c:set var="subMenuIdCounter" value="0"/>
													 <c:forEach var="menu" items="${lstMenuMaster}" varStatus="status">
												    	<c:if test="${parentmenu.parentMenuId.menuId==menu.parentMenuId.menuId}">
												    	<c:set var="flagMenu" value="1"/>
														<c:set var="subMenuIdCounter" value="${subMenuIdCounter+1}"/>
														<li>	
															<a   id="submenuid${menuIdCounter}${subMenuIdCounter}" href="${menu.pageName}" tabindex="-1">${menu.menuName}</a>
														</li>
										
														<script>
															CurrentPageWithMenuSelected("${menu.pageNameWithSubPage}",${parentmenu.parentMenuId.orderBy},${subMenuIdCounter},"${menu.status}");
														</script>
														</c:if>
													  </c:forEach>
												  </ul>
											  </li>
										</c:if>
										<c:if test="${parentmenu.parentMenuId.menuId==null && !fn:contains(parentMenuIdChk,parentmenu.menuId)}">
											<li class="dropdown">
											    <c:set var="parentMenu" value="${parentMenu+1}"/>
													   <a  id="menuid${parentMenu}"  href="${parentmenu.pageName}">
														${parentmenu.menuName}
														  <c:set var="menuIdCounter" value="${menuIdCounter+1}"/>
												      </a>
											  </li>
										   	 <script>
												  CurrentPageWithParentMenuSelected("${parentmenu.pageNameWithSubPage}",${parentMenu});
											 </script>
										</c:if>
									</c:forEach>
									
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="height:68px;"></div>
<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create','UA-40462543-1','teachermatch.org');
	ga('send','pageview');
</script>