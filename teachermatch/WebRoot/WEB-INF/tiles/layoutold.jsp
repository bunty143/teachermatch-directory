<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ page import="tm.utility.Utility" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
	<script type="text/javascript">
		var resourceJSON = <%=Utility.getJSLocaleJSON()%>;
	</script>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<title><tiles:insertAttribute name="title"/></title>
		<link href="css/bootstrap.css?ver=${resourceMap['css/bootstrap.css']}" rel="stylesheet" type="text/css">
		<link href="css/style.css?ver=${resourceMap['css/style.css']}" rel="stylesheet" type="text/css">	
		
		<link href="css/font-awesome.min.css?ver=${resourceMap['css/font-awesome.min.css']}"  rel="stylesheet" type="text/css" >
		<link href="css/font-awesome-ie7.css?ver=${resourceMap['css/font-awesome-ie7.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome-ie7.min.css?ver=${resourceMap['css/font-awesome-ie7.min.css']}"  rel="stylesheet" type="text/css">
		<link href="css/font-awesome.css?ver=${resourceMap['css/font-awesome.css']}"  rel="stylesheet" type="text/css">
		<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
		<script type="text/javascript" src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}">
		</script>		
		
	    <script src="js/jquery.js?var=${resourceMap['js/jquery.js']}"></script>
		<script src="js/bootstrap.js?var=${resourceMap['js/bootstrap.js']}"></script>	
	
		<!--[if IE 7]>
		<style type="text/css">
		.span8 {
		    width: 422px!important;
		.socialicon-Box{ width:85px; float:right!important; padding:0px; margin:0 0 0 200px!important;}  
		}
		</style>
		<![endif]-->	
	</head>
		
	<body>
	
	<tiles:insertAttribute name="headerold" />


	<div class="container" style="min-height: 430px;">
		<tiles:insertAttribute name="body" />
	</div>

<tiles:insertAttribute name="footerold" />
	
	
	</body>
</html>
