<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>

<link href="css/bootstrap3.css" rel="stylesheet">
<link href="css/style3.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">
<style>
#testheader1 {
	
    background: none #ffffff;
    /*position: fixed;
  height: 390px;
    */
    top:0px;
    width: 100%;
    z-index: 50;
}

a{
 font-weight: normal;
}
</style>
<div class="container">
				<div class="row ">
					<div class="col-sm-12 col-md-12 top10">
						<center>
						<a href="https://www.teachermatch.org/">
						<% if(request.getServerName().contains("myedquest.org")){  %>
						 <img src="images/QuestLogoTM.png" alt="" >
						 <%} if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){
						 		if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){ %>
									<img src="images/logoimage.png" alt="" class="top14" style="width: 133px;height: 50px;">
								<%}else{ %>
								<img src="images/logowithouttagline.png" alt="" class="top14">
								<%}
							}else{ 
								if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){%>
									<img src="images/logoimage.png" alt="" class="top14" style="width: 133px;height: 50px;">
								<%}
								else{ %>
									<img src="images/Logo with Beta300.png" alt="">
								<%}						 	
						 	} %>
							<img class="mt10" src="images/greenline.png" width="100%"/>
						</a>
						</center>	
					</div>
				</div>
				
		        <div class="row">
				<div class="col-sm-8 col-md-8 mt10">	
				        <div class="row col-sm-12 col-md-12">				
						<font style="font-size:21px;font-family: Century Gothic;">
							<b class="titlecolor"><spring:message code="msg3StepforNext"/></b>
						</font>	
						</div>
						
						<div class="row mt10">					
						<div class="col-sm-4 col-md-4">
						 <div class="row">
						 	<div class="col-sm-6 col-md-6">
								<img src="images/1.png" height="70px" width="50px"/>
							</div>
							<div class="col-sm-6 col-md-6" style="margin-left:-18px;padding:0px;"><span style="color:#981B1E;" ><b><spring:message code="btnSearch"/></b></span>
									<br><spring:message code="msgScrollthroughAllJob"/>
							</div>
						 </div>							
						</div>	
										
						<div class="col-sm-4 col-md-4">
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<img src="images/2.png" height="60px" width="80px"/>
								</div>
								<div class="col-sm-6 col-md-6" style="padding-left: 10px;"><span style="color:#981B1E;" ><b><spring:message code="lblViewDetail"/></b></span>
									<br><spring:message code="msgScanPosting"/>
								</div>
							</div>							
						</div>
						
						<div class="col-sm-4 col-md-4">
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<img src="images/3.png" height="50px" width="70px"/>
								</div>
								<div class="col-sm-6 col-md-6" style="padding-left:0px;">
								<span style="color:#981B1E;"><b><spring:message code="lblApply1"/></b></span><br>
								<spring:message code="msgClickToApplyWriteCoverLtr"/>

								</div>
							</div>							
						</div>											
						
					</div>				
				</div>
				
					
				<div class="col-sm-4 col-md-4">	
						<div class="row" style="margin-right:-1px;">					
							<div class="col-sm-12 col-md-12"   style="background-color:#007AB4;color:#FFFFFF;text-align:center">
								<font size="3px;" ><b><spring:message code="msgWHATCOMING"/></b></font>
							</div>	
					  </div>
					  <div class="row">			
							<div class="col-sm-12 col-md-12"    style="color:#007AB4;">
								<ul>
									<li style="color:#9fcf68"><b><spring:message code="lblAdvancedJobSearch"/></b></li>
									<li><b><spring:message code="lblPersonalPlanningFolder"/></b></li>
									<li><b><spring:message code="lblMessageManagementCenter"/></b></li>
									<li><b><spring:message code="lblAddMore"/></b></li>
								</ul>
							</div>
						</div>	
							
	 	        </div>	
	            </div>
	 
	 
	 
	   <div class="row mt10 col-sm-12 col-md-12">
			
				<a href="https://www.teachermatch.org/teachers/" target="_blank">	
					<font  style="font-size:17px;font-family: Century Gothic;">
					<span class="titlecolor">
					Discover how <b>TeacherMatch EPI</b> can further assist you
					in advancing your career in education -- <span style="color:#9fcf68">starting today</span>! 
					</span>
					</font>
				</a>
			
	</div>
	

</div>
