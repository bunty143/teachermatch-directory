 <%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
 <%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
 <%@ page import="tm.utility.Utility" %>
 <div class="header-bg" style="position: fixed;z-index:100">
	 <div class="container">
		 <div class="login-header-backround"> 	
		                 
						 <% if(request.getServerName().contains("myedquest.org")){  %>						 
						 <a href="http://myedquest.org/" target="_blank" class="tmlogo"><img src="images/QuestLogoTM.png" alt="" class="top14"></a>
						 
						 <!-- Google Analytics code:  -->
							 <script>
							  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
							  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');							
							  ga('create', 'UA-56937648-1', 'auto');
							  ga('send', 'pageview');							
							</script>
						 
							<!-- Start of Async HubSpot Analytics Code -->
							  <script type="text/javascript">
							    (function(d,s,i,r) {
							      if (d.getElementById(i)){return;}
							      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
							      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/290901.js';
							      e.parentNode.insertBefore(n, e);
							    })(document,"script","hs-analytics",300000);
							  </script>
						 
						 
						 <%}else{ %>
											
						 	<% if(!request.getServerName().contains("kelly.teachermatch.org")){%>
								<a href="http://www.teachermatch.org" target="_blank" class="tmlogo">
						
						<%}
						 if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){  
						 
							 if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){
							 %>
								<img src="images/logoimage.png" alt="" class="top14" style="width: 133px;height: 50px;">
							<%}else{ %>
								<img src="images/logowithouttagline.png" alt="" class="top14">
							<%}
						}else{ 
							if(!Utility.getValueOfPropByKey("mainlogo").equalsIgnoreCase("yes")){
							%>
								<c:choose>
		    							<c:when test="${jobOrder.districtMaster.districtId eq 804800 && not empty jobOrder.districtMaster.logoPath}">								
											<img src="<%=(Utility.getValueOfPropByKey("contextBasePath")+"district/") %>${jobOrder.districtMaster.districtId}/${jobOrder.districtMaster.logoPath}" alt="" style="width: 85px;height: 60px;padding-top:10px;">
										</c:when>
		    							<c:otherwise>				
										 	<img src="images/logoimage.png" alt="" class="top14" style="width: 133px;height: 50px;">
										</c:otherwise>
								</c:choose>
							<%}
							else{ %>
								<img src="images/Logo with Beta300.png" alt="" class="top14">
							<%}
						} %>
						</a>	
				         <script type="text/javascript">
							(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
							(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
							m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
							ga('create','UA-40462543-1','teachermatch.org');
							ga('send','pageview');
						</script>
						
						 <%} %>
					  									                      
		 </div> 
	 </div>
 </div>
 <div style="height: 86px;">

 </div>