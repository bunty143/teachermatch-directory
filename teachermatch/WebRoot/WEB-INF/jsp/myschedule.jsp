<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/MyScheduleAjax.js?ver=${resourceMap['MyScheduleAjax.Ajax']}"></script>
<script type='text/javascript' src='js/myschedule.js?ver=${resourceMap["js/myschedule.js"]}'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
 

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome-ie7.min.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
<style>
 .icon-question-sign{
 	 font-size: 1.5em;
 }
 .popover{
  width: 400px;
  background-color: #ffffff;
  border: 1px solid #007AB4;
  }
  .circle{
  width: 48px;
  height: 48px;
  margin: 0em auto;
  }
  .popover-title {
  margin: 0;
  padding: 0px 0px 0px 0px;
  font-size: 0px;
  line-height: 0px;
  }
  .popover-content {
   margin: 0;
   padding: 1px;
  }
  .popover.right .arrow:after {
  border-width: 11px 11px 11px 0;
  border-right-color: #007AB4;;
  bottom: -11px;
  left: -1px;
  }
  div.t_fixed_header_main_wrapper {
	position 		: relative; 
	overflow 		: visible; 
  }
 div.t_fixed_header.ui .body th{
	padding: 0px;
  	line-height: 12px;
  	text-align: left;
  	vertical-align: top;
 	border-left-style:none;
 }
</style>
<style>
	.hide
	{
		display: none;
	}
</style>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});

function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
	        $j('#tblGrid').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 400,
		        width: 945,
		        minWidth: null,
		        minWidthAuto: false,
		         colratio:[140,130,120,150,100,335], // table header width
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
	        });
        });			
}
</script>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="LBLManageSchedule"/></div>	
         </div>
         
          <c:if test="${not empty userMaster}">
			<c:if test="${userMaster.entityType eq 3 or userMaster.entityType eq 2}">	        			
				<c:set var="hide" value="hide"></c:set>
			</c:if>
		</c:if>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
	
	<div class="row top10">    
	      <div class="col-sm-3 col-md-3">
		       <span class=""><label class=""><spring:message code="lblFrm"/></label>
		       <input type="text" id="fromDate"   name="fromDate" value="${fromDate}" class="help-inline form-control"></span>
	       </div>
	       <div class="col-sm-3 col-md-3">
		       	<span class=""><label class=""><spring:message code="lblTo"/></label>
		       	<input type="text" id="toDate" name="toDate" value="${toDate}" class="help-inline form-control"></span>
	       </div>
	       <div class="col-sm-2 col-md-2">
		       	<span class="">
		       		<button class="btn btn-primary top25-sm"  type="button" onclick="displayScheduleRecords();"><spring:message code="btnSearch"/> <i class="icon"></i></button>
		       	</span>
	       </div>
      </div>
     <div class="mt10" onkeypress="return chkForEnterRecordsByEntityType(event);">	
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="<c:out value="${hide}"/>">
		            <div  id="Searchbox" class="<c:out value="${hide}"/>">
	         		    <div class="row">
		             		<div class="com-sm-5-col-md-5">
		             			<label id="captionDistrictOrSchool"><spring:message code="lblDist/School"/></label>
			             		<input type="text" id="districtORSchoolName" name="districtORSchoolName" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"	/>
								<input type="hidden" id="districtOrSchooHiddenlId" value="${userMaster.districtId.districtId}"/>
								<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>
			             	<input type="text" id="userMasterEntityID" value="${userMaster.entityType}"/>
			             	<input type="hidden" id="userSessionEntityTypeId" value="${userSession.entityType}"/>
							<input type="hidden" id="schoolSessionId" value="${userSession.schoolId.schoolId}"/>
			             	<input type="hidden" id="districtSessionId" value="${userSession.districtId.districtId}"/>
			             	<input type="hidden" id="schoolHiddenId" value="${userMaster.schoolId.schoolId}"/>
			             	<input type="hidden" id="districtHiddenId" value="${userMaster.districtId.districtId}"/>
		             	</div>
	          		</div>
        	  	</div>
       		 </form>	           
	</div> 
	
<div class="row">
	<div class="col-sm-12 col-md-12" >
		<label class="checkbox inline">
	     <input type="checkbox" id="myScheduledCheck" onclick="displayScheduleRecords();"/>&nbsp;<spring:message code="MSGShowonlyparticipating"/>
	     </label>	
	</div>
</div>

<div class="TableContent top10">        	
            <div class="table-responsive" id="divMain">               	         
            </div>            
</div> 

<script type="text/javascript">
$('.accordion').on('show hide', function(e){
$(e.target).siblings('.accordion-heading').find('.accordion-toggle i').toggleClass('minus plus', 200);
});
</script>
<script type="text/javascript">
	displayScheduleRecords();
</script>
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("fromDate", "fromDate", "%m-%d-%Y");
     cal.manageFields("toDate", "toDate", "%m-%d-%Y");
    //]]>
</script>

  <script>

function endDateStartdate(msg)
{
	$('#loadingDiv').hide();
	$('#dateValidation .modal-body').html(msg);
	$('#dateValidation').modal('show');
}
</script>

<div class="modal hide" id="dateValidation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

             
                 