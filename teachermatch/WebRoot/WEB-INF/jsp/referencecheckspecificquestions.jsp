<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/ReferenceChkQuestionsAjax.js?ver=${resourceMap['ReferenceChkQuestionsAjax.ajax']}"></script>
<script type='text/javascript' src="js/referencechkquestions.js?ver=${resourceMap['js/referencechkquestions.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript">

var questionTypeJSON=${json};
function findSelected(idd)
{
	for (x in questionTypeJSON)
  	{
	  if(idd==x){
	  	return questionTypeJSON[x];
	  }
  	}
}
 $('textarea').jqte();
 //alert("dddd");
</script>
<style>
.myButton {
    background:url(./images/download.jpg) no-repeat;
    cursor:pointer;
    width: 30px;
    height: 30px;
    border: none;
}

.progress1 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar1 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent1 { position:absolute; display:inline-block; top:0px;bottom:0px; left:48%; }

.progress2 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar2 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent2 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress3 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar3 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent3 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress4 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar4 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent4 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress5 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar5 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent5 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress6 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar6 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent6 { position:absolute; display:inline-block; top:0px; left:48%; }

</style>

<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static"  >	
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 		
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="redirectToURL()"><spring:message code="btnOk"/></button></span> 		
 	</div>
</div>
</div>
</div>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblAdd/EditQues"/></div>	
         </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<input type="hidden" id="questionId" value="${param.questionId}">
<div  id="divAssessmentRow" style="display: block;">
	<div class="row top5">
	<div class="col-sm-8 col-md-8">			                         
	 			<div class='divErrorMsg' id='errordiv' ></div>
	 			<div class='divErrorMsg' id='errordiv1' ></div>
	</div>
	</div>

	<div class="row top10">
	<div class="col-sm-7 col-md-7" id="assQuestion"><label><strong><spring:message code="headQues"/><span class="required">*</span></strong></label>
	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
	<textarea class="form-control" name="question" id="question" maxlength="2500" rows="5" onkeyup="return chkLenOfTextArea(this,2500);" onblur="return chkLenOfTextArea(this,2500);"> ${districtSpecificQuestion.question }</textarea>
	</div>
	</div>

	<div class="row">  
	<div class="col-sm-3 col-md-3">
	<label><strong><spring:message code="headQuesType"/><span class="required">*</span></strong></label>
	<select  class="form-control " name="questionTypeMaster" id="questionTypeMaster" onchange="getQuestionOptions(this.value)">
	      <option value="0"><spring:message code="optSltQuesType"/></option>
			<c:forEach items="${questionTypeMasters}" var="questionTypeMaster" >	
				<option id="${questionTypeMaster.questionTypeId}" value="${questionTypeMaster.questionTypeId}" >${questionTypeMaster.questionType}</option>
			</c:forEach>	
	</select>
	</div>
	
		<div class="col-sm-3 col-md-3">
        	<label><spring:message code="lblQuesMaxScore"/><span class="required">*</span></label>
        	<input type="text" id="questionMaxScore" name="questionMaxScore" maxlength="3" value="" class="form-control" style="width:92%;" onkeypress="return checkForInt(event);">
      </div>
	</div>
	
	<div class="row top5" id="row0" style="display: none">
	<div class="spanfit1 col-sm5 col-md-5"><label><strong><spring:message code="lblOpt"/> <a  href="#" id="iconpophover" rel="tooltip" data-original-title="Select the check box against the valid option(s)"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
	</div>
	<div class="spanfit1 col-sm5 col-md-5"><label><strong>&nbsp;</strong></label>
	</div>
	</div>
	
	<div class="row" id="rowExplain" style="display: none;">
	<div class="col-sm-7 col-md-7" id="assExplanation">
	<label><strong><spring:message code="lblExp"/></strong></label>
	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
		<textarea class="span9" name="questionExplanation" id="questionExplanation" maxlength="2500" rows="4" onkeyup="return chkLenOfTextArea(this,2500);" onblur="return chkLenOfTextArea(this,2500);"></textarea>
	</div>
	</div>
	
	<div class="row top10" id="row1" style="display: none">	
	        <div class="spanfit1 left15 pull-left" >
			    <div class="top7 pull-left">1</div>
				<div class="pull-left" style="width: 200px;margin-left: 5px;">
				<input type="hidden" name="hid1" id="hid1"/><input type="text" name="opt1" id="opt1" class="form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left top7" style="width: 40px;margin-left: 5px;">
				<input type="checkbox" name="valid1" id="valid1">
				</div>				
				<div class="clearfix"></div>
			</div>	
	
	        <div class="spanfit1 pull-left" >
			    <div class="top7 pull-left">2</div>
				<div class="pull-left" style="width: 200px;margin-left: 5px;">
				<input type="hidden" name="hid2" id="hid2"/><input type="text" name="opt2" id="opt2" class="form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left top7" style="width: 40px;margin-left: 5px;">
				<input type="checkbox" name="valid2" id="valid2">
				</div>				
				<div class="clearfix"></div>
			</div>	
	</div>
	
	<div class="row top5" id="row2" style="display: none">	
	        <div class="spanfit1 left15 pull-left" >
			    <div class="top7 pull-left">3</div>
				<div class="pull-left" style="width: 200px;margin-left: 5px;">
			    <input type="hidden" name="hid3" id="hid3"/><input type="text" name="opt3" id="opt3" class="form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left  top7" style="width: 40px;margin-left: 5px;">
				<input type="checkbox" name="valid3" id="valid3">
				</div>				
				<div class="clearfix"></div>
			</div>	
	
	        <div class="spanfit1  pull-left" >
			    <div class="top7 pull-left">4</div>
				<div class="pull-left" style="width: 200px;margin-left: 5px;">
				 <input type="hidden" name="hid4" id="hid4"/><input type="text" name="opt4" id="opt4" class="form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left top7" style="width: 40px;margin-left: 5px;">
				<input type="checkbox" name="valid4" id="valid4">
				</div>				
				<div class="clearfix"></div>
			</div>	
	</div>
	
	
		<div class="row top5" id="row3" style="display: none">	
	        <div class="spanfit1 left15 pull-left" >
			    <div class="top7 pull-left">5</div>
				<div class="pull-left" style="width: 200px;margin-left: 5px;">
			    <input type="hidden" name="hid5" id="hid5"/><input type="text" name="opt5" id="opt5" class="form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left top7" style="width: 40px;margin-left: 5px;">
				<input type="checkbox" name="valid5" id="valid5">
				</div>				
				<div class="clearfix"></div>
			</div>	
	
	        <div class="spanfit1  pull-left" >
			    <div class="top7 pull-left">6</div>
				<div class="pull-left" style="width: 200px;margin-left: 5px;">
				<input type="hidden" name="hid6" id="hid6"/><input type="text" name="opt6" id="opt6" class="form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left top7" style="width: 40px;">
				&nbsp;&nbsp;<input type="checkbox" name="valid6" id="valid6">
				</div>				
				<div class="clearfix"></div>
			</div>	
	</div>

</div>

	<div class="row">
		<div class="col-sm-7 col-md-7" id="assInstruction"><label><strong><spring:message code="lblInst"/></strong></label>
		<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
		<textarea class="form-control" name="questionInstructions" id="questionInstructions" maxlength="2500" rows="4" onkeyup="return chkLenOfTextArea(this,2500);" onblur="return chkLenOfTextArea(this,2500);">${districtSpecificQuestion.questionInstructions }</textarea>
		</div>
	</div>
     
    <div class="row top20" style="display: block;" id="divManage">
<!--  @ Anurag  -->
    <c:set var="hqId" value=""></c:set>
    <c:if test="${heaQuarterMaster ne null }">
       <c:set var="hqId" value="${heaQuarterMaster.headQuarterId}"></c:set>
    </c:if>
    
  	<c:if test="${questSetId eq 0}">
  		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
        </c:if> 
     	<button id="saveQues"  onclick="saveQuestion('save')" class="btn btn-large btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
     	<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
        </c:if>
     	<button onclick="saveQuestion('continue')" class="btn btn-large btn-primary"><strong><spring:message code="btnSv&Conti"/> <i class="icon"></i></strong></button>
     	&nbsp;<a href="javascript:void(0);" onclick="cancelQuestion('${param.districtId}','${hqId }')">Cancel</a>
     	</c:if>
     	
     	
     	<c:if test="${questSetId ne 0}">
     	<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
        </c:if> 
     	<button id="saveQues"  onclick="saveQuestion('save')" class="btn btn-large btn-primary"><strong>Save <i class="icon"></i></strong></button>
     	&nbsp;<a href="javascript:void(0);" onclick="cancelQuestion('${param.districtId}','${hqId }')">Cancel</a>
     	</c:if>
     	 
     </div>
     </div>



<input type="hidden" id="questionId" value="${param.questionId}"/>
<input type="hidden" id="districtId" value="${param.districtId}"/>
<input type="hidden" id="questionSetId" value="${questSetId}"/>

<input type="hidden" id="headQuarterId" value="${heaQuarterMaster.headQuarterId}"/>

 <script language="javascript"><!--
		
 var assessQuesOpts=${assessQues}
 
	$(document).ready(function() {
		/*
			$('textarea').jqte();
  			$(".jqte").width(538);
  			$('#assQuestion').find(".jqte").width(538);
  			$('#assInstruction').find(".jqte").width(538);
  			$('#assExplanation').find(".jqte").width(538);
  		*/
	}) 
$('#iconpophover').tooltip();
setDistrictQuestion(${param.questionId});

</script>


    
