<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/InventoryInvitationsAjax.js?ver=${resourceMap['InventoryInvitationsAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/inventoryinvitations.js?ver=${resourceMap['js/inventoryinvitations.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tempTeacherTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 972,
        minWidth: null,
        minWidthAuto: false,
        colratio:[110,90,180,180,206,205], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
 </script>

<div class="row" style="margin:0px;">
	<div style="float: left;">
    	<img src="images/icon1.png" width="41" height="41" alt="">
	</div>        
    <div style="float: left;">
    	<div class="subheading" style="font-size: 13px;">Invitation List</div>	
	</div>
	<div style="clear: both;"></div>	
	<div class="centerline"></div>
</div>

<div style="margin:0px;">
	<div class="row">
		<div class="mt10 span16" id="tempTeacherGrid">
		</div>
	</div>
	<div class="row top10">
		<button class="btn btn-primary" type="button" onclick="return saveTeacher();">Accept <i class="icon"></i></button> &nbsp;&nbsp;<button class="btn btn-primary" type="button" onclick="return tempTeacherReject('${sessionIdTxt}');">Rejected<i class="icon"></i></button>	
	</div>
</div>

<div class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"   onclick="return tempTeacher();">x</button>
				<h3 id="myModalLabel">Candidate</h3>
			</div>
			<div class="modal-body"> 		
				<div class="control-group">Candidate details have been imported successfully.</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn  btn-primary"   onclick="return tempTeacher();" >Close</button></span>&nbsp;&nbsp;
 			</div>
		</div>
	</div>
</div>

<div style="display:none;" id="loadingDiv">
	<table  align="left" >
		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

<script type="text/javascript">
	displayTempTeacher();
</script>