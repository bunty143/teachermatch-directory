<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />
<script type="text/javascript" src="dwr/interface/HeadQuarterAjax.js?ver=${resourceMap['HeadQuarterAjax.Ajax']}"></script>
<script type="text/javascript" src="js/hqbrjoborder.js?ver=${resourceMap['js/hqbrjoborder.js']}"></script>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>



<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tempDistrictTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#savedDistrictTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 722,
        minWidth: null,
        minWidthAuto: false,
        colratio:[388,182,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>
<style>
.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
</style>


<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-editdistrictjob-order.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">
         	<c:set var="display" value=""></c:set>
         	<c:set var="label" value="Headquarters"></c:set>
         	 <c:if test="${not empty branchMasterId || not empty headQuarterId}">
         	 		<c:set var="disp" value="hide"></c:set>         
 					<c:set var="display" value="hide"></c:set>
         	 	</c:if>
         	 <c:if test="${not empty branchMasterId}">
         	 	<c:set var="display" value="hide"></c:set>
         		<c:set var="label" value="Branch"></c:set> 
         	 </c:if>
         	Add/Edit <span id="titleDistrictOrSchool">${label}</span> Job Order
         	</div>	
         </div>
 </div>
<c:set var="endDateRadio1Checked" value=""></c:set>
<c:set var="endDateRadio2Checked" value=""></c:set>
<c:set var="jobendDate" value="${jobEndDate}"></c:set>
<c:set var="jobendDateDis" value=""></c:set>
<c:choose>
    <c:when test="${jobEndDate eq '12-25-2099'}">
        <c:set var="endDateRadio2Checked" value="checked='checked'"></c:set>
        <c:set var="jobendDate" value=""></c:set>
        <c:set var="jobendDateDis" value="disabled='disabled'"></c:set>
    </c:when>
    <c:otherwise>
    	<c:set var="jobendDateDis" value=""></c:set>
        <c:set var="endDateRadio1Checked" value="checked='checked'"></c:set>
    </c:otherwise>
</c:choose>

    <c:set var="hiddenJobRadio1Checked" value=""></c:set>
    <c:set var="hiddenJobRadio2Checked" value=""></c:set>
    <c:if test="${hideJobFlag eq true}">
        <c:set var="hiddenJobRadio2Checked" value="checked='checked'"></c:set>
    </c:if>
    <c:if test="${hideJobFlag eq false}">
        <c:set var="hiddenJobRadio1Checked" value="checked='checked'"></c:set>
    </c:if>
	
<div class="row">
<div class="col-sm-12 col-md-12" ><br>
<div class="">
			<div class="control-group">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>
			
			<div class="control-group">	     			                         
			 	<div class='divErrorMsg' id='jobDescriptionErrordiv' ></div>
			</div>
</div>	</div>	</div>	
 		<c:if test="${empty branchMasterId && not empty headQuarterId}">
			<div class="row">
            <div class="col-sm-8 col-md-8">
               <label>Headquarters Name<span class="required">*</span></label>  <br/>          
	             <span>
			      <c:set var="readonly" value=""></c:set>
			      <c:if test="${not empty headQuarterName}">
			      <c:set var="readonly" value="readonly"></c:set>
			      </c:if>
			       <input class="form-control" type="text" id="onlyHeadQuarterName" name="onlyHeadQuarterName"   value="${headQuarterName}"
                 onfocus="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterId','');"
                 onkeyup="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterId','');"
                 onblur="hideHeadQuarterDiv(this,'headQuarterId','divTxtHeadQuarterData');"   ${readonly}   />
                       
                 <div id='divTxtHeadQuarterData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtHeadQuarterData','onlyHeadQuarterName')"  class='result' ></div>
                 <input type="hidden" id="headQuarterId" value="${headQuarterId}" />
                 <input type="hidden" id="isBranchExistsHiddenId" value=""/>  
			      
			      </span>

         </div>
 		</div>
 		</c:if>
 		
 		<div class="row">
            <div class="col-sm-8 col-md-8">
               <label>Branch Name<span class="required">*</span></label>  <br/>
               <c:set var="bnchId" value=""></c:set>
               <c:set var="editFlag" value=""></c:set>
               <c:if test="${not empty jobOrder.branchMaster.branchId}">
               <c:set var="bnchId" value="${jobOrder.branchMaster.branchId}"></c:set>
               <c:set var="editFlag" value="disabled"></c:set>
               </c:if>
               <c:if test="${not empty branchMasterId}">
               <c:set var="bnchId" value="${branchMasterId}"></c:set>
                <c:set var="editFlag" value="disabled"></c:set>
               </c:if>      
	             <span>
			      <input type="text" id="branchName" name="branchName"  class="form-control" value="${branchName}"
       					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');showBranchesJobCategory();"
						onchange="resetDistrict();"	${editFlag}/>
						<div id='divTxtShowDataBranch' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>
						<input  type="hidden" id="branchId" name="branchId" value="${bnchId}"/>								      
			      </span>
         </div>
 		</div>
 	<!-- 	shadab -->
 	<div class="row mt10">  

	 	<c:if test="${jobOrder ne null}">
		   <c:set var="disabled" value="disabled='disabled'"/>
		</c:if>

	   <div class="col-sm-8 col-md-8">  

	        <label>District Name</label>
       <c:set var="distEnable" value=""></c:set>
       <c:if test="${not empty jobOrder.districtMaster}">
       <c:set var="distEnable" value="disabled='disabled'"/>
       </c:if>
	        <c:if test="${entityType==1 || entityType==5 || entityType==6}">
		        <span>
		        	<input type="text"  id="districtName"  value="${jobOrder.districtMaster.districtName}" maxlength="100"  name="districtName" class="help-inline form-control" ${distEnable}
		          	onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
		      	</span>
		      	 <c:if test="${entityType==1 || entityType==5 || entityType==6}">
		      		<input type="hidden" id="districtId" value="${jobOrder.districtMaster.districtId}" />
		      	</c:if>
		        <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
      	   </c:if>
      	 
      	   <c:if test="${entityType==2}">
      	   	<input type="hidden" id="districtId" name="districtId" value="${DistrictId}" />
      	   	<input type="text" id="districtName" name="districtName" maxlength="100"  value="${DistrictName}" readonly="readonly" class="form-control" />	
      	   </c:if>		
		</div>
		
	</div>
 		
 	<!-- 	shadab -->	
 		
 		
	<div class="row">
		<div class="col-sm-8 col-md-8">
		<label>Job Title<span class="required">*</span></label>
		<input type="text"  id="jobTitle" maxlength="200" name="jobTitle" class="form-control" value="${jobOrder.jobTitle}" />
	</div>
	</div>
	

<div class="row">
      <div class="col-sm-3 col-md-3">
        <label>Posting Start Date<span class="required">*</span></label>
        <input type="text" id="jobStartDate" name="jobStartDate"   maxlength="0"    class="form-control" style="width:92%;" value="${jobStartDate}">
      </div>
      <div class="col-sm-3 col-md-3" style="padding-left:0px;">
        <label>Posting End Date<span class="required">*</span></label>
        
        <label class="radio" style="min-height:0px;margin-bottom:-6px;margin-top:6px;"><input type="radio" name="endDateRadio" ${endDateRadio1Checked}  id="endDateRadio1" onclick="return unDisableEndDate();"/></label>
        <input type="text" id="jobEndDate" name="jobEndDate"   maxlength="0"   class="form-control" style="width:82%;margin-left:20px;"  value="${jobendDate}"/>
        
      </div>
       <div class="col-sm-2 col-md-2" style="padding-left:0px;">
        <label>&nbsp;</label>
        <label class="radio" style="min-height:0Px;margin-bottom:-6px;margin-top:6px;">
        <input type="radio" name="endDateRadio"  id="endDateRadio2" ${endDateRadio2Checked} onclick="return disableEndDate();"/></label>
        <input type="text" id="" name="" disabled  value="Until Filled" class="form-control" style="width:86%;margin-left:20px;">
      </div>
	 <input type="hidden" id="changeDateHid" value=""/>
</div>

	<div class="row">
		<div class="col-sm-3 col-md-3">
					<label>Reference No./Job Code</label>
					<input type="text"  id="refno" maxlength="50" name="refno" class="form-control" style="width:92%;" readonly="readonly" value="${jobOrder.jobId}"/>
		</div>
		<div class="col-sm-5 col-md-5">
		</div>
	</div>



<div class="row" >
			<div class="col-sm-10 col-md-10">
				<label>Job Description <a href="#" id="iconpophover2" rel="tooltip" data-original-title="Detailed job description that will be displayed to the job applicants on TeacherMatch platform"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
				<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
					   to paste that text first into a simple text editor such as Notepad to remove the special characters and
 					   then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 					   and/or a Microsoft document may not display properly to the reader)</label>
			</div>	
			
			
	<div class="col-sm-11 col-md-11">			
		<div class="" id="jDescription">
			<textarea rows="8" id="jobDescription"   name="jobDescription"  class="form-control" style="resize: none;">${jobOrder.jobDescription}</textarea>
		</div>			
	</div>
</div>

<div class="row" >
			<div class="col-sm-4 col-md-4">
				<label>Upload Job Description</label>
				<input type="hidden" id="uploadedJobDiscriptionFile" name="uploadedJobDiscriptionFile"/>
					<form id='jobDescriptionDocUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='fileuploadservletjobdescription.do' class="form-inline">
					<c:if test="${not empty branchMasterId}">
					<input type="hidden" id="brHiddenId" name="brHiddenId" value="${branchMasterId}"/>
					</c:if>
					<c:if test="${not empty headQuarterId && empty branchMasterId}">
					<input type="hidden" id="hqHiddenId" name="hqHiddenId" value="${headQuarterId}"/>
					</c:if>
					<input  type="file" name="jobDescriptionDoc" id="jobDescriptionDoc" onchange="uploadJobDescriptionFile(this.value);">
				</form>
			</div>
			<div class="col-sm-5 col-md-5" style="padding-top: 21px;">			
				
				</div>
</div>

<div class="row" >
			<div class="col-sm-10 col-md-10">
				<label>Job Qualification <a href="#" id="iconpophover7" rel="tooltip" data-original-title="Detailed job Qualification that will be displayed to the job applicants on TeacherMatch platform"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
				<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document, please ensure
					   to paste that text first into a simple text editor such as Notepad to remove the special characters and
 					   then copy and paste it into the field below. Otherwise, the text directly cut and pasted from a web page 
 					   and/or a Microsoft document may not display properly to the reader)</label>
			</div>
			
			<div class="col-sm-11 col-md-11">	
				<div class="" id="jQualification">
				<textarea rows="8" id="jobQualification"   name="jobQualification"  class="form-conntrol" style="resize: none;">${jobOrder.jobQualification}</textarea>
				</div>	
			</div>
</div>

<input type="hidden" id="txtjobCategory" value="${jobCategory}" /> 
<div class="row">
<div class="col-sm-4 col-md-4">
<label class="">Job Category Name<span class="required">*</span></label>
		<div id="jobCategoryDiv" class="">
		<select id="jobCategoryId" name="jobCategoryId" class="form-control" style="" onchange="checkForHiddenJob(this.value);checkForDefaultJSI(this.value);">
			<option value="-1">Select Job Category</option>
				<c:if test="${not empty jobCategoryMastersList}">
					<c:forEach var="avlst" items="${jobCategoryMastersList}" varStatus="status">
					<c:set var="sel" value=""></c:set>
					<c:if test="${jobCat == avlst.jobCategoryId}">
						<c:set var="sel" value="selected"></c:set>
					</c:if>
						<option value="${avlst.jobCategoryId}"  ${sel} >${avlst.jobCategoryName}</option>
					</c:forEach>
				</c:if>
			</select>
		</div>
	</div>
</div>


<div class="row left7 hide">
		   <div class="col-sm-10 col-md-10">
           <label class="checkbox inline" style="padding-left: 0px;">
           <c:set var="isEHNE" value=""></c:set>
           <c:if test="${jobOrder.isExpHireNotEqualToReqNo}">
           <c:set var="isEHNE" value="checked=true"></c:set>
           </c:if>
					<input type="checkbox" id="isExpHireNotEqualToReqNo" name="isExpHireNotEqualToReqNo" ${isEHNE}/>
            		Multiple hires will be allocated to a single number
           </label>
           </div>
	</div>
	

	<div class="row col-sm-10 col-md-10 ${disp}">
		Which Administrator(s) can hire Candidate(s) from this job order applicant pool?
	</div>

	<div class="row col-sm-4 col-md-4 ${disp}">
		<div>
			<label class="radio">
			<c:set var="writePrivToBranch" value=""></c:set>
           <c:if test="${jobOrder.writePrivilegeToBranch}">
           <c:set var="writePrivToBranch" value="checked=true"></c:set>
           </c:if>
				<input type="radio" name="branchAdministrator"  id="branchAdministrator" value="1" ${writePrivToBranch}>
				Only Branch Administrator(s)
			</label>
		</div>

	</div>
	
	
	
	<div  id="addNoSchoolDiv" >
		 		 <div class="row">
	        <div class="col-sm-4 col-md-4 hide" style="border:0px solid blue;">
	        
	        </div>
	     </div>
	    		
		<div class="row ${disp}">
		<div class="col-sm-4 col-md-4" style="margin-left: 3%;">
		<label># of Expected Hire(s)</label>
		<c:set var="expHire" value=""></c:set>
		<c:if test="${not empty writePrivToBranch}">
		<c:set var="expHire" value="${jobOrder.noOfExpHires}"></c:set>
		</c:if>
		<input type="text" class="form-control" maxlength="3" id="noOfExpHires" name="noOfExpHires" value="${expHire}" />
		</div>
		</div>
</div>
	
	
	<div class="row left5 hide">
	<div class="col-sm-3 col-md-3">
			<label class="radio inline" style="padding-left: 0px;">
			<c:set var="adist" value=""></c:set>
			<c:if test="${attachD}">
				<c:set var="adist" value="checked=true"></c:set>
			</c:if>
				<input type="radio" name="attachDistrict" id="attachDistrict" ${adist}  value="1" onclick="showAddDistrictLink(4);">
				Attach District
			</label>
	</div>
	 <c:set var="showHideAddSchoolLink" value="hide"></c:set>		        			
		<c:set var="showHideAddSchoolLink" value=""></c:set>
		<div class="col-sm-6 col-md-6 left18 mt10">
				<div class="pull-right hide" id="addDistrictLink">
			            <a href="javascript:void(0);" onclick="addDistict();">+ Add District</a>
	            </div> 
		</div>
	</div>
	         <input type="hidden" id="gridNo" name="gridNo" value="">
                 <div class="row col-sm-9 col-md-9">
                 	<div  id="hqSavedDistrict"  style="padding-bottom: 10px;" ></div>
                 	 <div  id="hqDistrict"  style="padding-bottom: 10px;" onclick="">
                     </div>
                 </div>                   
                  <div id="addDistrictDiv" class="hide">
	                  <div class="row col-sm-10 col-md-10" style="max-width:755px;">
	                    <div>
	                      <label>District Name</label>
	             				<input type="text" id="districtName" name="districtName"  class="form-control"
			             		onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onkeyup="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onblur="hideDistMasterDiv(this,'districtId','divTxtShowData');"	/>
								<input type="hidden" id="districtId" value=""/>
							<div id='divTxtShowData' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>	
	                    </div>
	                  </div>
	                  <div class="row col-sm-5 col-md-5">
                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return addDistrict();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearDistrictsLink();">Cancel</a></div>
                	  </div>
                  </div>

 <c:if test="${JobPostingURL!=null}">
	<div class="row">
	<div class="col-sm-10 col-md-10 mt10">
	TeacherMatch Job Posting URL:<br>
	<a target="blank" href="${JobPostingURL}">${JobPostingURL}</a>
	</div>
	</div>
</c:if>

<c:set var="reqJSI" value=""></c:set>
<c:set var="notreqJSI" value=""></c:set>
			<c:if test="${jobOrder.isJobAssessment}">
				<c:set var="reqJSI" value="checked=true"></c:set>
				<c:set var="notreqJSI" value=""></c:set>
			</c:if>
			<c:if test="${not jobOrder.isJobAssessment}">
				<c:set var="reqJSI" value=""></c:set>
				<c:set var="notreqJSI" value="checked=true"></c:set>
			</c:if>
			<c:if test="${empty jobOrder}">
				<c:set var="reqJSI" value=""></c:set>
				<c:set var="notreqJSI" value=""></c:set>
			</c:if>

	<div class="row col-sm-10 col-md-10 ${display}">
		Which prescreen is needed?
	</div>
 	<div class="row col-sm-10 col-md-10 ${display}">
      	<label class="radio">
       <input type="radio" name="prescreen"  onclick="isJobAssessment(2);" id="isJobAssessment1" value="2" ${(jobOrder eq null || jobOrder.isJobAssessment == false)? 'checked' : ''}>
      			 No prescreen is needed
		</label>
     </div>

	<div>
	    <div class="row col-sm-10 col-md-10 ${display}" style="margin-top: -10px;">
			   <label class="radio">
		       <input type="hidden" name="isJobAssessmentVal" id="isJobAssessmentVal" value="${jobOrder.isJobAssessment == false ? '2':'1'}"/>
		     <input type="radio" name="prescreen"  id="isJobAssessment2" onclick="isJobAssessment(1);"  value="1"  ${(jobOrder.isJobAssessment == true) ? 'checked' : ''}>
		       	 This job requires a prescreen to be taken by applicants
		        </label>
        </div>
        <div  id="isJobAssessmentDiv" class="${display}">
	     	 <input type="hidden" name="attachJobAssessmentVal" id="attachJobAssessmentVal" value="${attachJobAssessmentVal}"/>
	     	 <input type="hidden" name="headQuarterRootPath" id="headQuarterRootPath" value="${headQuarterRootPath}"/>
	     	 <input type="hidden" name="HQssessmentUploadURL" id="HQssessmentUploadURL" value="${HQssessmentUploadURL}"/>
	     	 <input type="hidden" name="branchRootPath" id="branchRootPath" value="${branchRootPath}"/>
	     	 <input type="hidden" name="HQssessmentUploadURL" id="BRssessmentUploadURL" value="${BRssessmentUploadURL}"/>
	   		
	   		 <div class="row col-sm-10 col-md-10  left15" id="hideDAUploadURL" >
		      <label class="radio" style="margin-top: 0px; margin-bottom: 5px;">
		       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(1);" id="attachJobAssessment1" value="1"  ${(jobOrder.attachDefaultHeadQuarterPillar == '1') ? 'checked' : ''}>
		       Use the default prescreen set at the HeadQuarter&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide" id="ViewDAURL"></span>
		     </label>
		     </div>
		     		    
		    <c:set var="ishidejcjsidiv" value="hide"></c:set>
		    <c:if test="${jobOrder.attachDefaultJobSpecificPillar==1}">
		    	<c:set var="ishidejcjsidiv" value="show"></c:set><%-- Gagan : show means No need to hide in case of Edit Mode --%>
		    </c:if>
		    
		     <div class="row col-sm-10 col-md-10 left15 " id="jcjsidiv">
		      <label class="radio" style="margin-top: 0px; margin-bottom: 5px;">
		       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(4);" id="attachJobAssessment4" value="4" ${jobOrder.attachDefaultJobSpecificPillar == '1' ? 'checked' : ''}>
		      Use the default prescreen set at the Job Category&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="${ishidejcjsidiv}" id="ViewJSIURL"></span>
		     </label>
		     </div>
		    <iframe src="" id="ifrmJsi" width="100%" height="480px" style="display: none;">
				</iframe> 
				
			<div style="clear: both;">	
			<div id="jsiDiv" class="left30">
				    <div class="span11" style="margin-bottom: 5px;">
				      <label class="radio">
				       <input type="radio" name="attachJobAssessment" onclick="attachJobAssessment(2);" id="attachJobAssessment3" value="2" ${jobOrder.attachNewPillar == '1'  ? 'checked' : ''}>
				       Let me attach a prescreen
				     </label>
			  		</div>
			  		<div class="hide" id="attachJobAssessmentDiv">
					<div class="row col-sm-10 col-md-10">
					Acceptable Inventory formats include PDF, MS-Word, GIF, PNG, and JPEG  files.
					Maximum file size 10MB
					<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' >
					</iframe>
					</div>
					<form id='frmJobAssessmentUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='jobResumeUploadServlet.do' class="form-inline">
					 <input type="hidden" id="dateTime" name="dateTime"  value="${dateTime}"/>
					 <input type="hidden" id="JobOrderType" name="JobOrderType"  value="${JobOrderType}"/>
					 <c:if test="${empty branchMasterId && not empty headQuarterId}">
					 <input type="hidden" id="headQuarterHiddenId" name="headQuarterHiddenId" value="${headQuarterId}"/>
					 </c:if>
					 <c:if test="${not empty branchMasterId}">
					  <input type="hidden" id="branchMasterHiddenId" name="branchMasterHiddenId" value="${branchMasterId}"/>
					 </c:if>
					
				 	<div class="row">
						<div class="col-sm-5 col-md-5">
							<c:if test="${entityType==5 || entityType==6}" >
							<input name="assessmentDocument" id="assessmentDocument" type="file" >
							</c:if>
						</div>
						<div class="hide" id="divAssessmentDocument" name="divAssessmentDocument">
							<label>
								&nbsp;
							</label>
							<div class="col-sm-2 col-md-2">
								<c:if test="${JobOrderType==5 || JobOrderType==6}">
									<!--<a data-original-title='JSI' rel='tooltip' id='JSIdistrict1' href='javascript:void(0);' onclick="showFile('headQuarter','${headQuarterId}','${jobOrder.assessmentDocument}','JSIdistrict1');${windowFunc}"><c:out value="${(jobOrder.assessmentDocument==''||jobOrder.assessmentDocument==null)?'':'view'}"/></a>
									--><input name="assessmentDocumentFileName" id="assessmentDocumentFileName" type="hidden" value="${jobOrder.assessmentDocument}">
								</c:if>
							</div>
						</div>
					</div>				
					</form>
					</div>
					
					<div class="row hide1 JSIRadionDiv" id="JSIRadionDiv" style="margin-bottom: 5px; margin-top: 10px;">
						<div class="col-sm-10 col-md-10 checkbox inline top1">
							<span class="col-sm-4 col-md-4" id="epicheck2Span">
								<input type="radio" name="offerJSIRadio" id="offerJSIRadioMaindatory" value="1" ${jobOrder.jobAssessmentStatus == 0 || jobOrder.jobAssessmentStatus == 1 || jobOrder.jobAssessmentStatus == null ? 'checked' : ''}>
								<label><strong>JSI is mandatory</strong></label>
							</span>
							<span class="col-sm-4 col-md-4" id="epicheck2Span">
								<input type="radio" name="offerJSIRadio" id="offerJSIRadioOptional" value="2" ${jobOrder.jobAssessmentStatus == 2 ? 'checked' : ''}>
								<label><strong>JSI is optional</strong></label>
							</span>
						 </div>	
					</div>
			</div>	
	     </div> 
     </div>
        
              <div class="mt10 hide">
			<div class="row ">
				<div class="col-sm-5 col-md-5" id="">
							<input type="checkbox" name="isInviteOnly" id="isInviteOnly"  onclick="unableOrDisableInviteJob();" ${IsInviteOnlyFlag} >
							<label>Hidden job</label>
				</div>
			</div>
			
			 <div class="row left5 hide hiddenInviteDiv">
	            <div class="col-sm-4 col-md-4">
			         <label class="radio">
                       <input type="radio" name="hiddenJob" id="hiddenJob1" value="0" ${hiddenJobRadio1Checked}> This job is invite-only.
                     </label>
                 </div>
                 <div class="col-sm-8 col-md-8">
                    <label class="radio">
                      <input type="radio" name="hiddenJob" id="hiddenJob2" value="1" ${hiddenJobRadio2Checked}> This job is hidden job but applicants with job URL can apply.
                   </label>
                 </div>
	         </div>

	</div>


<div  id="showExitMessageDiv" class="">
<div class="row">
<div class="col-sm-10 col-md-10">
<c:set var="checked" value=""></c:set>
<c:if test="${not empty jobOrder.exitMessage}">
	<c:set var="checked" value="checked=true"></c:set>				
</c:if>
<label  class="radio">
<input type="radio" name="tmInventory"  id="tmInventory" value="1" ${checked} />
Display this message to talent when all application requirements are complete.<a href="#" id="iconpophover5" rel="tooltip" data-original-title="Message that should be displayed to the job applicant after completion of the TeacherMatch base EPI "><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
<div class="" id="hprocess">
	<textarea id="hiringprocess" name="exitMessage"  maxlength="1000" class="span8" rows="0" style="resize: none;">${jobOrder.exitMessage}</textarea>
</div>	
</div>
</div>
</div>

</div>
<c:set var="caption" value="Save Job Order"></c:set>
			<c:if test="${not empty jobOrder.jobId}">
				<c:set var="caption" value="Update Job Order"></c:set>				
			</c:if>
<div class="row top5">
   	<div class="col-sm-10 col-md-10 mt10 " >
      	<c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
   	<button class="btn btn-large btn-primary" onclick="validateAddEdithqJobOrder();">${caption} <i class="icon"></i></button>
  </c:if>
      	<a href="javascript:void(0);" onclick="redirectBack();">Cancel</a>
      </div>
</div>


	<iframe src="" id="iframeJSI" width="100%" height="480px" style="display: none;"></iframe>
	
<div class="modal hide"  id="modalSuccessMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="successRedirect();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Job Order <span id="jobIdText"></span> has been successfully created.
		</div>
 	</div>
 	<div class="modal-footer">
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true" onclick="successRedirect();">ok</button> 		
 	</div>
	</div>
	</div>
</div>


<div class="modal hide"  id="modalUpdateMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="successRedirect();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Job Order has been successfully updated.
		</div>
 	</div>
 	<div class="modal-footer">
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true" onclick="successRedirect();">ok</button> 		
 	</div>
	</div>
	</div>
</div>

<!--  Model display while inActive jobOrder make update-->

<div class="modal hide"  id="modalUpdateMsgforInactive" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="successRedirect();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="modelMessageInactive" />
		</div>
 	</div>
 	<div class="modal-footer"> 
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true" onclick="activateDeactivateJob(${jobOrder.jobId},'A'); successRedirect();"><spring:message code="lblYes" /></button> 
 	<button class="btn  btn-Info" data-dismiss="modal" aria-hidden="true" onclick="successRedirect();"><spring:message code="lblNo" /></button> 		
 	</div>
	</div>
	</div>
</div>


<div style="display:none;" id="loadingDiv"><div align="center"> 
     </div><table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>
<script type="text/javascript">
	deleteTemp();
	$('#iconpophover2').tooltip();
	$('#iconpophover5').tooltip();
	$('#iconpophover7').tooltip();

	</script>
   <script type="text/javascript">
    
    var cal = Calendar.setup( {
		onSelect : function(cal) {
			cal.hide()
		},
		showTime : true
	});
	cal.manageFields("jobEndDate", "jobEndDate", "%m-%d-%Y");
	cal.manageFields("jobStartDate", "jobStartDate", "%m-%d-%Y");
	 	
     </script>
<input type="hidden" id="jobId" value="${jobOrder.jobId}"/>
<input type="hidden" id="tempDistId" value=""/>
<input type="hidden" id="addDistFlag" value=""/>
<input type="hidden" id="headQuarterId" value="${headQuarterId}"/>
<input type="hidden" id="jobCatJsiFlag" value=""/>
<input type="hidden" id="jobCat" value="${jobCat}"/>
<input type="hidden" id="entityType" value="entityType"/>
<input type="hidden" id="jobStatus" value="${jobOrder.status}"/>

<script type="text/javascript">
// getDistrictDiv();
<c:if test="${attachD}">
 document.getElementById("addDistFlag").value=1;

</c:if>
</script>

<c:if test="${IsInviteOnlyFlag eq 'checked'}">
		 <script>
			$('.hiddenInviteDiv').show();
		</script>				
</c:if>
		<c:if test="${(HQssessmentUploadURL!=''  && HQssessmentUploadURL!=null && display eq '') || (BRssessmentUploadURL!='' && BRssessmentUploadURL!=null && display eq '')}">
		 <script>
			document.getElementById("hideDAUploadURL").style.display='inline';
			document.getElementById("attachJobAssessment1").checked=true;			
			document.getElementById("ViewDAURL").style.display='inline';
			document.getElementById("ViewDAURL").innerHTML="<a data-original-title='JSI' rel='tooltip' id='JSIdistrict2' href='javascript:void(0)' onclick=\"showFile('headQuarter','${headQuarterId}','${HQssessmentUploadURL}','JSIdistrict2');${windowFunc}\" href='#'><c:out value="${(HQssessmentUploadURL==''||HQssessmentUploadURL==null)?'':'view'}"/></a>";
		 </script>		
		</c:if>

		<c:if test="${ attachJobAssessmentVal=='1' &&  display eq ''}">
		 <script>
			document.getElementById("attachJobAssessmentDiv").style.display='inline';
		</script>				
		</c:if>
				
		 <c:if test="${jobOrder eq null || jobOrder.isJobAssessment == false}">
        	<script>
					document.getElementById("isJobAssessmentDiv").style.display='none';
			</script>	
        </c:if>
        
        <c:if test="${jobOrder.isJobAssessment == true && display eq ''}">
        	<script>
        			isJobAssessment(1);
					document.getElementById("isJobAssessmentDiv").style.display='inline';
			</script>	
        </c:if>
        
        <c:if test="${jobOrder.attachNewPillar == '1' }">
        	<script>
        			document.getElementById("attachJobAssessment3").checked=true;
			</script>	
        </c:if>
        
        <c:if test="${not empty jobOrder}">
        	 <c:if test="${entityType ne 5 || entityType ne 1 || entityType ne 6}">	
        	 	<script>
        	 		//displayJobCategoryByDistrict();
        	 		//document.getElementById("jobCategoryId").value=${jobCat};
        	 	</script>
        	 </c:if>
        </c:if>