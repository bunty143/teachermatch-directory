<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="redirectURL" value=""/>

<c:choose>
	<c:when test="${epiStandalone}">
		<c:set var="redirectURL" value="portaldashboard.do" />
	</c:when>
	<c:when test="${fn:contains(header.referer, 'jobsboard.do')}">
		<c:set var="redirectURL" value="jobsboard.do" />
	</c:when>
	<c:when test="${fn:contains(header.referer, 'userdashboard.do')}">
		<c:set var="redirectURL" value="userdashboard.do" />
	</c:when>
	<c:when test="${fn:contains(header.referer, 'jobsofinterest.do')}">
		<c:set var="redirectURL" value="jobsofinterest.do" />
	</c:when>
	<c:otherwise>
		<c:set var="redirectURL" value="jobsofinterest.do" />
	</c:otherwise>
</c:choose>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/search-icon.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headShareJob"/></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>


<form onsubmit="return chkShareJob()" method="post" action="sharejob.do">
<input type="hidden" id="jobId" name="jobId" value="${param.jobId}">
<input type="hidden" id="refererPageURL" name="refererPageURL" value="${header.referer}">
<div  class='row'>
	<div id="leftDiv" class="col-sm-6 col-md-6">	
		<div class="mt15">
			<div class="row"> 
				<div class="col-sm-12 col-md-12">
			    	<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
		        </div>	              	
			</div>
		<!--	Author: GAgan-->
		<c:set var="topPadding" value="padding-top: 4px;"></c:set>			
			<div class="row">								
				<div class="col-sm-6 col-md-6">
					<label style="${topPadding}"><strong><spring:message code="lblJoTil"/></strong></label></br>
					${jobOrder.jobTitle}
				</div>			
				<div class="col-sm-6 col-md-6">
					<label style="${topPadding}"><strong><spring:message code="lblDistrictName"/></strong></label></br>
					${jobOrder.districtMaster.districtName}
				</div>
			</div>
			<div class="row">								
				<div class="col-sm-12 col-md-12" align="justify">
					<label style="${topPadding}"><strong><spring:message code="headJobDescrip"/></strong></label>
					${jobOrder.jobDescription} <br/><br/>
				</div>			
			</div>	
			
			<c:choose>
				<c:when test="${empty tName}">
					<div class="row" style="">								
						<div class="col-sm-6 col-md-6">
							<label><strong><spring:message code="lblYourName"/></strong></label>
							<input type ="text" id="yrname" name="yrname" class="form-control" 
							maxlength="50"
							onkeyup="document.getElementById('spnTName').innerHTML=this.value"
							onblur="document.getElementById('spnTName').innerHTML=this.value"
							value="${tName}"/>
						</div>			
						<div class="col-sm-6 col-md-6">
							<label><strong><spring:message code="lblYourEmailAddress"/></strong></label>
							<input type ="text" id="yremail" name="yremail" class="form-control" maxlength="50" value="${tEmail}"/>
						</div>
					</div>
				</c:when>
				<c:otherwise>
					<input type ="hidden" id="yrname" name="yrname" class="form-control" value="${tName}"/>
					<input type ="hidden" id="yremail" name="yremail" class="form-control" maxlength="50" value="${tEmail}"/>
				</c:otherwise>
			</c:choose>	
			
			<div class="row">								
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblFirstFriendN"/></strong></label>
					<input type ="text" id="fname1" name="fname1"  class="form-control" maxlength="50"/>
				</div>			
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblFirstFriendE"/></strong></label>
					<input type ="text" id="fremail1" name="fremail1" class="form-control" maxlength="50"/>
				</div>
			</div>
			
			<div class="row">
										
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblSeFriendN"/></strong></label>
					<input type ="text" id="fname2" name="fname2"  class="form-control" maxlength="50"/>
				</div>			
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblSeFriendE"/></strong></label>
					<input type ="text" id="fremail2" name="fremail2" class="form-control" maxlength="50"/>
				</div>
			</div>
			
			<div class="row">
										
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblTFriendN"/></strong></label>
					<input type ="text" id="fname3" name="fname3"  class="form-control" maxlength="50"/>
				</div>			
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblTFriendE"/></strong></label>
					<input type ="text" id="fremail3" name="fremail3" class="form-control" maxlength="50"/>
				</div>
			</div>
			
			<div class="row">
										
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblFFriendN"/></strong></label>
					<input type ="text" id="fname4" name="fname4"  class="form-control" maxlength="50"/>
				</div>			
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblFFriendE"/></strong></label>
					<input type ="text" id="fremail4" name="fremail4" class="form-control" maxlength="50"/>
				</div>
			</div>
			
			<div class="row">
										
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblFiFriendN"/></strong></label>
					<input type ="text" id="fname5" name="fname5"  class="form-control" maxlength="50"/>
				</div>			
				<div class="col-sm-6 col-md-6">
					<label><strong><spring:message code="lblFiFriendE"/></strong></label>
					<input type ="text" id="fremail5" name="fremail5" class="form-control" maxlength="50"/>
				</div>
			</div>
			
			<div class="top20">			
				<button class="btn btn-large btn-primary" type="submit" >
					<spring:message code="btnSub"/> <i class="icon"></i>
				</button>
				&nbsp;&nbsp;&nbsp;&nbsp;<a href="${redirectURL}"><spring:message code="lnkCancel"/></a>		
				<br><br>
			</div>	
		</div>
	
	</div>
	<!-- Author Gagan: For displaying Job Message. -->
	<div id="rightDiv" align="left" class="col-sm-5 col-md-5" style="font-family: Cambria;">
	<br/>
	 <c:if test="${not empty jobOrder}">
		<c:if test="${jobOrder.createdForEntity eq 2}">	        			
			<c:set var="schoolOrDistrictName" value=""></c:set>
			<c:set var="schoolOrDistrictName" value="${jobOrder.districtMaster.districtName}"></c:set>
			<c:set var="schoolOrDistrictLocation" value=""></c:set>
			<c:set var="schoolOrDistrictLocation" value="${jobOrder.districtMaster.address}"></c:set>
		</c:if>
		<c:if test="${jobOrder.createdForEntity eq 3}">	        			
			<c:set var="schoolOrDistrictName" value=""></c:set>
			<c:set var="schoolOrDistrictLocation" value=""></c:set>
                 <c:forEach var="school" items="${jobOrder.school}" varStatus="status">
					<c:set var="schoolOrDistrictName" value="${school.schoolName}"></c:set>
					<c:set var="schoolOrDistrictLocation" value="${school.address}"></c:set>
				</c:forEach>
		</c:if>
	</c:if>
	<label style="font-family: Arial;"><strong>Job Message </strong></label>
	<div style="border-style: solid; border-color: #dddddd; border-width: 2px;border-radius:12px; padding-left: 5px; padding-top: 8px; padding-bottom: 4px; ">
		<p style="padding-top: 3px;">I thought you may be interested in this opportunity: ${jobOrder.jobTitle} at ${schoolOrDistrictName} in ${schoolOrDistrictLocation} listed at <a href='http://www.teachermatch.org/'>www.teachermatch.org</a>. Click on this link to learn more: <a href="${jobUrl}">${jobUrl}</a></p>
		<p><span id='spnTName'>${tName}</span></p>		
	</div>
</div>
</div>
</form>

<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<c:choose>
	<c:when test="${empty tName}">
		<script>
			document.getElementById("yrname").focus();
		</script>
	</c:when>
	<c:otherwise>
		<script>
			document.getElementById("fname1").focus();
		</script>
	</c:otherwise>
</c:choose>	

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/teacher/dashboard.js?ver=${resourceMap['js/teacher/dashboard.js']}"></script>

