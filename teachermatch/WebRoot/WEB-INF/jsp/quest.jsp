<%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/quest.js?ver=${resourceMap['js/quest.js']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>	
<script type="text/javascript" src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}"></script>
<script type='text/javascript' src='ac_quicktime.js'></script>
<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
<style type="text/css">	
#resumediv{
display: none;
}	
#closetag
{
float: right;margin-right:50px;margin-top: 0px;color:#b31516;font-size: 14px;
}
textarea:focus, input:focus, button:focus{
    outline: 0;
}
.cf:before, .cf:after{
   content:"";
   display:table;
}
 
.cf:after{
    clear:both;
}
.cf{
    zoom:1;
}  
.form-wrapper {	 
    margin: 0px;	   
}


	 
.form-wrapper input {
    width:245px;
    height: 40px;
    padding: 10px 5px;	   
    font-size:13px;
    border: 0;
    background: #eee;
    border-radius: 3px 0 0 3px;    
    border: 1px solid #c4c4c4;  
}
 
.form-wrapper input:focus {
    outline: 0;
    background: #fff;
    box-shadow: 0 0 2px rgba(0,0,0,.8) inset;
} 

/* Form submit button */
.form-wrapper button {
   overflow: visible;
    position: relative;	    
    border: 0;
    padding: 0;
    cursor: pointer;
    height: 40px;
    width: 150px;
    font: bold 15px/40px 'lucida sans', 'trebuchet MS', 'Tahoma';
    color: #fff;
    text-transform: uppercase;
    background: #d83c3c;
    border-radius: 0 3px 3px 0;      
    text-shadow: 0 -1px 0 rgba(0, 0 ,0, .3);
    border-radius: 10px; 
	-moz-border-radius: 45px; 
	-webkit-border-radius: 45px; 	
}    	
.postjob
{
 border-radius:2em;    
}	
#content{position:absolute;}	

#hero{z-index:-1;position:absolute;right:0;left:0;top:0;height:520px;overflow:hidden;min-width:1045px}	

.textwhite
{
color:white;
font-size: 13px;
padding-left: 5px;
padding-right: 5px;
}
.textwhite:hover
{
color:white;
font-size: 13px;
padding-left: 5px;
padding-right: 5px;
}
.stretch
{
  
    background-size: 100% 100%;
}
</style> 
<script type="text/javascript">
function divChange()
{
document.getElementById('initialdiv').style.display="none";
//document.getElementById('resumediv').style.display="block";
//$('.container').css('background', '#d6dcd5');
 $("#resumediv").fadeIn(2000);
}
function getInitial()
{
document.getElementById('initialdiv').style.display="block";
document.getElementById('resumediv').style.display="none";
 //$("#resumediv").fadeOut(1000);
}
</script> 
<div id="hero">
		<video id="Video1" autoplay="autoplay" preload="auto" loop="loop" width="100%" height="" style="display:none" >
			   <source src="http://23.253.165.119/video/Quest_Loop_360_part-tint.mp4" type="video/mp4">		
			   <source src="http://23.253.165.119/video/Quest_Loop_360_part-tint_converted.WebM" type="video/WebM">			  	 		  
			</video>
			<img id="backImg" alt="" src="images/backImage.gif" width="100%" height="100%" >
			<!-- <video id="Video1" width="100%" height="" autoplay loop none poster="images/backImage.gif" class="stretch"> -->
			

</div>						
			<div id="screenData">			 
		     <div id="content" class="quest-logo quest-text1 quest-logo-top" >
		     	
		     	
			     <div style="float: right; margin-right: -20px; " class="white signIn-text">
			     	<strong style="padding:8px; margin-right:10px; background: rgba(54, 25, 25, .5);">
			     		<a href="signin.do"  class="textwhite"><spring:message code="lblApplicantLogin1"/></a><b class="white">|</b><a href="educationjob.do"  class="textwhite">Sign Up</a>
			     	</strong>
			     	<a href="employer-sign-in.do">
			     		<button type="submit" class="rounded" style="text-transform:none;font-size: 1.2em;">Post a Job</button></a>&nbsp;&nbsp;&nbsp;
			     	<a href="questjobboard.do" target="_blank"><button type="submit" class="rounded" style="text-transform:none;background: #9fcf68;font-size: 1.2em;"  >Find a Job</button></a>
			     </div>
			 
			     <div style="clear: both;"></div>
			     <h1 style="margin-top: 50%;">
			     <strong class="white" style="font-size: .6em;"><spring:message code="lblYourPartnerfor1"/><br> <spring:message code="lblEducationJobConnections1"/> 
			     </strong>
			    </h1>		    
		      </div>			  
		      <form  action="questjobboard.do" method="get" id="quest" >			  
		      <div style="height: 100px;margin-top:-100px;width:100%;background-color:rgba(0, 0, 0, 0.6);position: absolute;">		       
			      <div style="text-align: center;">		      
				  <div style="padding-top: 32px;">	
				 		  
					<div class="form-wrapper" ><input type="text" name="quick" style="width:400px">
				       <!--  <input type="text" placeholder="City, State or Zip Code" > -->
				        &nbsp;&nbsp;
				           <button type="submit" class="rounded" onclick="formSubmit()" >Quick Search</button>
				        </div>	
				       
				  </div>
				  </div>
		       </div>
		      </form> 
		    </div> 
		      
	    <div style="height: 50px;text-align:center;margin-top: 540px;font-family:'Century Gothic','Open Sans', sans-serif;font-size: 30px;"><strong class="blackcolor"><spring:message code="lblGETRECRUITMENTREADYRIGHTNOW"/>!</strong>
	    </div>
	    
<div class="container">	
<div id="initialdiv" style="display: block;">	 
        <div class="top0"></div> 	
	    <div class="col-sm-4 col-md-4 top45"> 
			<center>
			    <a href="educationjob.do"><img class="img-responsive" src="images/search11.png"></a>
				<p style="font-size: 15px;" ><a href="educationjob.do"><b class="skycolor"><spring:message code="lblCreate1"/><br/>
				<spring:message code="lblPOWERPROFILEFREE1"/>!</b></a></p>
				<p class="font12 top20">		
				<spring:message code="MakeYourselfLeadingCandidate1"/>
				<br/>•</br>
			<spring:message code="lblSetPersonalJobSearchScreen1"/>
				<br/>•</br>
			<spring:message code="ManageYourCommunicationsCenter1"/>
				</p>	
			</center>
	   </div>
	
	<div class="col-sm-4 col-md-4">
		  <center><a href="educationjob.do" target="_blank"><img  class="img-responsive" src="images/ppi4.png" style="width:85%;"></a>	
		  <p class="top15" style="font-size: 20px;"><a href="educationjob.do"><b class="skycolor"><spring:message code="lblProfilePower1"/></b><b style="color:#474747;"><spring:message code="lblTracker1"/></a>
		  </b></p>
		  <p class="font12	 top20">		
		  <spring:message code="lblTrackthestrengthyourprofile1"/>
		  <b><spring:message code="msgMyQuestDashboard"/>.</b> <spring:message code="msglevelaccesswealthresources1"/>!
		  </p>
		  <br/>
		  <p><a href="javascript:void(0);" id="resume" onclick="divChange();"><b class="font15 skycolor"><spring:message code="lblResumeReflectionYoursWinner1"/>?</b></a><br/></p>
		  </center>
	</div>
	<div class="col-sm-4 col-md-4 top45" > 
		 <center>
		 <a href="talentsearch.do"><img  class="img-responsive" src="images/search33.png"></a>
		 <p style="font-size: 15px;" ><a href="talentsearch.do"><b class="skycolor"><spring:message code="lblStartYour1"/><br/>
		 <spring:message code="lblTALENTSEARCHFREE1"/>!</b></a></p>
		 <p class="font12 top20">		
		 <spring:message code="lblPostAllDistrictSchoolJobOpenings1"/>
		 <br/>•</br>
		 <spring:message code="lblReachTopCandidates1"/>
		 <br/>•</br>
	<spring:message code="lblEnjoyFullyAutomatedRecruitment1"/>
		 </p>
		 </center>
	</div> 
	</div>
</div> 
<div style="clear:both;"></div>
	<div id="resumediv" style="z-index: 1007;width:100%;background: #d6dcd5;min-height: 400px;margin-bottom:-23px;margin-top: 0px;">
	<div class="col-sm-12 col-md-12" style="padding: 20px;">
		<a href="javascript:void(0);" id="closetag" onclick="getInitial();"><b><spring:message code="lnkCLOXEX"/></b></a>
	</div> 	
	    <div class="col-sm-12 col-md-12" style="padding: 20px;margin-top: 10px;"> 
			<center>
			    <p style="font-size: 20px;line-height: 1.4;color:black;"><spring:message code="lblCheckOutResou1"/><br/>
					<spring:message code="lblBuildingResume1"/>
				</p>
				<br/>
				<p style="font-size: 12px;">
				<spring:message code="msgOnceYuResumeComlt1"/><br/>
				<b><spring:message code="lblQuestPersonalPlanning1"/></b><spring:message code="lblfolderandpointstoward1"/><br/>
				<spring:message code="msgExpertLevelStatusResou"/>!
				</p>
			</center>
	   </div>
	   <div class="col-sm-12 col-md-12" style="margin: auto;margin-top: 10px;">
	   <div class="col-sm-2 col-md-2" style="width:11.333333%;"></div>
	    <div class="col-sm-3 col-md-3"> 
			<center>
			   <a href="http://www.resumebuilder.org" target="_blank"><img class="img-responsive" src="images/homepage1.png" style="width:200px;"></a>	
			</center>
	   </div>
	    <div class="col-sm-3 col-md-3"> 
			<center>
			     <a href="https://www.resumegig.com" target="_blank"><img class="img-responsive" src="images/homepage2.png" style="width:200px;" /></a>	
			</center>
	   </div>
	    <div class="col-sm-3 col-md-3"> 
			<center>
				 <a href="https://resumegenius.com" target="_blank"><img class="img-responsive" src="images/homepage3.png" style="width:250px;height: 69px;"></a>	
			</center>
	  </div>
	  <div class="col-sm-1 col-md-1"></div>
	   </div>
	</div> 
<div class="modal hide" id="teacherModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">		
		 <div class="modal-dialog-for-cgmessageTop">
		  <div class="modal-content"><!--			
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='hideDiv();' style="color:#007AB4;">x</button>				
			-->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='hideDiv();'>
					x
				</button>
				<h3>
					TeacherMatch
				</h3>
			</div>	
			<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	        </iframe>
			<div class="modal-body" style="background-color: #f0f0f0">	
			 <form id='frmpowertracker'  method='post' target='uploadFrame' class="form-inline"  accept-charset="UTF-8" >		
			    <div class='divErrorMsg' id='divServerError' style="display: block;margin-bottom: 10px;"></div>			             
			    <div class='divErrorMsg' id='errordiv' style="display: block;margin-bottom: 10px;"></div>			    			
				<h1 class="font25 top0" style="">Sign In</h1>	
				<div style="height: 8px;"></div>			
				<div class="font20" style="color: black;">Email</div>
	            <input type="text" name="emailAddress" value="${qemailAddress}" id="emailAddress" class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
	            <br/><div class="font20" style="color: black;">Password</div>
	            <input type="password" id="password"  class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
	            <div class="pull-right"> <a href="forgotpassword.do"><h1 class="font20 top5"><b>Forgot your email or password?</b></h1></a></div>
	            <div style="clear: both;"></div>	          
	            <button class="signInButton top10" type="button" style="font-size: 20px; background-color:18729F;" onclick="return loginTeacher();">Sign In</button><br/>	    
            	<br/><label class="checkbox">
					<input type="checkbox" id="teacherTxtRememberme" name="teacherTxtRememberme" >  <span class="font20" style="color: black;">Remember me</span>
				</label>
			</form>
		 	</div> 			 
		</div>
	  </div>
</div>
<div class="modal hide" id="userModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">		
		 <div class="modal-dialog-for-cgmessageTop">
		  <div class="modal-content">
		   <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='hideDiv();' style="color:#007AB4;">x</button>				
			-->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='hideDiv();'>
					x
				</button>
				<h3>
					TeacherMatch
				</h3>
			</div>	
			<iframe id='useruploadFrameID' name='useruploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	        </iframe>
			<div class="modal-body" style="background-color: #f0f0f0">	
			 <form id='userform' method='post' target='useruploadFrame' class="form-inline"  accept-charset="UTF-8" >		
			    <div class='divErrorMsg' id='userdivServerError' style="display: block;margin-bottom: 10px;"></div>			             
			    <div class='divErrorMsg' id='usererrordiv' style="display: block;margin-bottom: 10px;"></div>			    			
				<h1 class="font25 top0" style="">Sign In</h1>	
				<div style="height: 8px;"></div>			
				<div class="font20" style="color: black;">Email</div>
	            <input type="text" id="userEmailAddress" value="${uemailAddress}" class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
	            <br/><div class="font20" style="color: black;">Password</div>
	            <input type="password" id="userPassword"  class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
	            <div class="pull-right"> <a href="forgotpassword.do"><h1 class="font20 top5"><b>Forgot your email or password?</b></h1></a></div>
	             <br/><div style="clear: both;"></div>	
	             <div class="pull-right"> <a href="javascript:void(0)" onclick="return questsignup(1);"><h1 class="font20 top5"><b>Sign Up with TeacherMatch?</b></h1></a></div>
	            <div style="clear: both;"></div>	          
	            <button class="signInButton top10" type="button" style="font-size: 20px; background-color:18729F;" onclick="return loginUser();">Sign In</button><br/>	    
            	<br/><label class="checkbox">
					<input type="checkbox" id="txtRememberme" name="txtRememberme">  <span class="font20" style="color: black;">Remember me</span>
				</label>
			</form>
		 	</div> 			 
		   </div>
	  </div>
</div>
<div class="modal hide" id="userSignUpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">		
		 <div class="modal-dialog-for-signup">
		  <div class="modal-content"><!--			
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='hideDiv();' style="color:#007AB4;">x</button>				
			-->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='hideDiv();'>
					x
				</button>
				<h3>
					TeacherMatch
				</h3>
			</div>	
			<iframe id='useruploadFrameID' name='signupuploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	        </iframe>
			<div class="modal-body" style="background-color: #f0f0f0">	
			 <form id='signupform' method='post' target='signupuploadFrame' class="form-inline"  accept-charset="UTF-8" >		
			    <div class='divErrorMsg' id='signUpServerError' style="display: block;margin-bottom: 10px;"></div>			             
			    <div class='divErrorMsg' id='signUpErrordiv' style="display: block;margin-bottom: 10px;"></div>			    			
				<h1 class="font25 top0" style="">Sign Up</h1>	
				<div style="height: 8px;"></div>	
				
				<div  id="districtForUser">				
					<div class="font20" style="color: black;display: block;">District<span class="required">*</span><div class="pull-right"><a href="javascript:void(0)" onclick="return showforotherDistrict()" > My District is not here</a></div></div>
	           		<input type="text" id="districtName" name="districtName"  type="text"  class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"
	           		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
					<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;'
					 onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
					<input type="hidden" id="districtId"/>
					<input type="hidden" id="other"/>
					
					<input type="text" id="otherdistrictName" class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;display: none;">
					<br/><br/>				
				</div>	           
	            		
				<div class="row">
					<div class="col-sm-6 col-md-6">
					<div class="font20" style="color: black;">First Name<span class="required">*</span></div>
		            <input type="text" id="fname" class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;">
		           
					</div>
					<div class="col-sm-6 col-md-6">
					 <div class="font20" style="color: black;">Last Name<span class="required">*</span></div>
		            <input type="text" id="lname" class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
		          
					</div>
				</div>				
				 
	            <br/>							
				<div class="font20" style="color: black;">Email<span class="required">*</span></div>
	            <input type="text" id="signupEmail" class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
	            <br/>
	              <div class="row">  
		               <div class="col-sm-6 col-md-6"> 
			            <div class="font20" style="color: black;">Password<span class="required">*</span></div>
			            <input type="password" id="signupPassword"  class="top5 font15" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;" onkeyup="return PasswordValue()"><br/>
			           </div>
	                        
                                   <div class="col-sm-6 col-md-6">
                                    <label>
										<strong class="font20" style="color: black;">Password Strength
										</strong>
									</label>									
                                   </div>
                                   
                                   <div class="col-sm-6 col-md-6">
                                   <table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td colspan="2" align="left" valign="top" nowrap>
											<table cellpadding="0" cellspacing="0"><tr>
												<td>
													<div style="height: 22px; width: 24px;  float: left;  border: thin solid #bcb4a4;">							
														<div id="d4" style="height: 22px; width: 24px; float: left; display: none;">
															&nbsp;
														</div>					
													</div>
												</td>
												<td style="padding-left:10px;">
													<div style="height: 22px; width: 24px;   float: left; border: thin solid #bcb4a4; margin-left: 5%;">
														<div id="d5" style="height: 22px; width: 24px; float: left; display: none;">
															&nbsp;
														</div>
													</div>
												</td>
												<td style="padding-left:10px;">
												<div style="height: 22px; width: 24px;  float: left; border:  thin solid #bcb4a4; margin-left: 5%;">
													<div id="d6"					style="height: 22px; width: 24px; float: left; background-color: lightgreen; display: none;">
														&nbsp;
													</div>
												</div>
												</td>
												<td style="padding-left:10px;">
												<div
													style="height: 22px; width: 24px;  float: left; border:  thin solid #bcb4a4; margin-left: 5%;">
													<div id="d7" 
														style="height: 22px; width: 24px; float: left; background-color: lightgreen; display: none;">
														&nbsp;
													</div>
												</div>
												
												</td>
												<td><table cellpadding="0" cellspacing="0"><tr><td>
													<div id="d8"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span >Weak</span>
													</div></td>
												<td>
													<div id="d9"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span >Normal</span>
													</div></td>
												<td>
													<div id="d10"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span >Strong</span>
													</div>
											</td>
										
											</tr> </table>
										</td>
									
										</tr></table>
										</td>
									    </tr>
								      </table>
							        </div>	
							        </div>
							        <br/>
							        
					   <div class="row">  		        					       
							        <div class="col-sm-12 col-md-12">
							        <label ><strong class="font20" style="color: black;">Please enter the sum of the two numbers shown in the picture in the field below<span class="required">*</span></strong></label>
							        </div>
					  </div>         
					  <div class="row">        
							        <div class="col-sm-6 col-md-6">							      
							          <input id="sumOfCaptchaText" type="text" class="top5 font15"	style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"onkeypress="return checkForInt(event);" maxlength="2"/>
							        </div>
							        <div class="col-sm-6 col-md-6 top10">							       
							            <div class="pull-left top5 font20" style="color: black;">
							               What is&nbsp;
							            </div>
							            <div class="pull-left">
							          	   <img id="imgCaptcha" style="width: 140px; height: 30px;" src="verify.png"/>                     
							            </div>
							            <div class="pull-left">
							             &nbsp; <img src="images/refresh.png" style="cursor: pointer;" onclick="changeCaptcha('sumOfCaptchaText')"/>
							            </div>
							           <div style="clear: both;"></div>			            
							        </div>							        
				      </div>                                   
                      <div class="row">	        
							        <div class="col-sm-12 col-md-12 top20" >
							          <button class="signInButton" type="button" onclick="return signUpUser(${userVal});">Sign Up</button>
							        </div>				       
                      </div>				
			</form>
		 	</div> 			 
		   </div>
	  </div>
</div>
<c:out value="${status}"></c:out>
<c:if test="${status eq 1}">
<div class="modal" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
				<p>You have been successfully registered with TeacherMatch.
                  We have sent you an email with login details.</p>			
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()">Ok</button>		 			
		 	</div>
		</div>
	  </div>
</div>
</c:if>
<c:if test="${status eq 0}">
<div class="modal" id="nonclientModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
				<p>Thank you for signing up for Quest! A member of our Partner Success team will contact you shortly to gather additional details and activate your account.
					If you have questions please reach to us at <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a></p>			
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()">Ok</button>		 			
		 	</div>
		</div>
	  </div>
</div>
</c:if>

<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>
<c:if test="${authmail}">
<div class="modal" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
				<p><%=request.getSession()!=null && request.getSession().getAttribute("authmsg")!=null?request.getSession().getAttribute("authmsg"):"" %></p>			
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()">Ok</button>		 			
		 	</div>
		</div>
	  </div>
</div>
</c:if>

<script>
var vid = document.getElementById("Video1");
vid.oncanplay = function() {
   // alert("Can start playing video");
   document.getElementById("Video1").style.display = 'block';
}; 

  /* 	var test_audio= document.createElement("audio") //try and create sample audio element
	var test_video= document.createElement("video") //try and create sample video element
	var mediasupport={audio: (test_audio.play)? true : false, video: (test_video.play)? true : false}
	//alert("Audio Element support: " + mediasupport.audio + "\n"	+ "Video Element support: " + mediasupport.video);
 	if(!mediasupport.video)
 	{
 		$("#Video1").hide();
 		//document.getElementById("Video1").style.display = 'none';
 	} */
	
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56937648-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/290901.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->

