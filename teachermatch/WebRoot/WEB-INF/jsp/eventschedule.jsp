<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="dwr/interface/EventScheduleAjax.js?ver=${resourceMap['EventScheduleAjax.ajax']}"></script>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript"	src="js/eventSchedule.js?ver=${resourceMap['js/eventSchedule.js']}"></script>
<script type="text/javascript"	src="dwr/interface/EventScheduleAjax.js?ver=${resourceMap['EventScheduleAjax.ajax']}"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<script type="text/javascript">
var $j=jQuery.noConflict();
</script>

<style>
.btn-dangerevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-dangerevents:hover,
.btn-dangerevents:focus,
.btn-dangerevents:active,
.btn-dangerevents.active,
.btn-dangerevents.disabled,
.btn-danger[disabled] {
  color: #ffffff;
  background-color: #bd362f;
  *background-color: #a9302a;
}

.btn-dangerevents:active,
.btn-dangerevents.active {
  background-color: #942a25 \9;
}

.btn-warningevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #faa732;
  *background-color: #f89406;
  background-image: -moz-linear-gradient(top, #fbb450, #f89406);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));
  background-image: -webkit-linear-gradient(top, #fbb450, #f89406);
  background-image: -o-linear-gradient(top, #fbb450, #f89406);
  background-image: linear-gradient(to bottom, #fbb450, #f89406);
  background-repeat: repeat-x;
  border-color: #f89406 #f89406 #ad6704;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-warningevents:hover,
.btn-warningevents:focus,
.btn-warningevents:active,
.btn-warningevents.active,
.btn-warningevents.disabled,
.btn-warningevents [disabled] {
  color: #ffffff;
  background-color: #f89406;
  background-color: #df8505;
}

.btn-warningevents:active,
.btn-warningevents.active {
  background-color: #c67605 \9;
}

strong {
font-weight: normal;
}

</style>


<script>
function applyScrollOnTblSchedule()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#scheduleTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[160,160,160,160,205,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}
function applyScrollOnTblCanSchedele()
{
   //alert("11")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#candidateSlotsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 690,
        minWidth: null,
        minWidthAuto: false,
        colratio:[220,220,250],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}
</script>

<div class="row"
	style="margin-left: 0px; margin-right: 0px; margin-top: ">
	<div style="float: left;">
		<img src="images/manageusers.png" width="41" height="41" alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">
			<spring:message code="headingEventSchedule" />
		</div>
	</div>
	<div class="pull-right add-employment1" id="addScheduleLink">
		<a href="javascript:void(0);" onclick="return addSchedule()"><spring:message
				code="linkAddSchedule" /> </a>
	</div>

	<div style="clear: both;"></div>
	<div class="centerline"></div>
</div>


<!--<div class="row top15" id="eventScheduleM">
	<div class="col-sm-12 col-md-12">
		<div id="scheduleGrid">
			<table id="scheduleTable" width="100%" border="0"
				class="table table-bordered mt10">
			</table>
		</div>
	</div>
</div>
--><div class="col-sm-12 col-md-12 top10">
	<div class='divErrorMsg' id='errordiv'></div>
</div>

<input type="hidden" id="gridNo" name="gridNo" value="">
<div class="row" id="valdays">
	<div class="col-sm-12 col-md-12 divErrorMsg" id="errDaysDiv">
	</div>
	<div class="col-sm-12 col-md-12">
		<spring:message code="msgMustComplete" />
	</div>
	<div class="col-sm-2 col-md-2 ">
		<input class="form-control" type="text" id="validfordays"
			value="${expiryDate}" maxlength="3" name="validfordays"
			onkeypress="return checkForInt1(event);" />
	</div>
	<div class="col-sm-10 col-md-10 ">
		<input class="form-control" type="text" id="locationUrl"
			name="locationUrl"
			value="https://teachermatch.interview4.com/test_taker_login.php"
			disabled="disabled" />
	</div>

</div>


<div class="hide" style="margin-left: -16px;" id="eventTypeWiseSchedule"></div>
<div id="newScheduleDiv" class="hide">
	<div class="row">
		<div class="col-sm-10 col-md-10">
			<div class="col-sm-3 col-md-3">
				<label>
					<spring:message code="lblTimeZone" />
					<span class="required">*</span>
				</label>
				<select id="timezone" name="timezone" class="form-control"></select>
			</div>
		</div>
	</div>

	<div>
		<div class="row" style="padding-left: 16px;">
			<div class="col-sm-16 col-md-16">
				<div class="col-sm-3 col-md-3">
					<label>
						<spring:message code="lblStartDate" />
						<span class="required">*</span>
					</label>
					<input type="text" id="eventStartDate" name="eventStartDate"
						maxlength="0" class="form-control">
				</div>
				<div class="col-sm-1 col-md-1 left25"
					style="width: 70px; padding-right: 0px; padding-left: 0px;">
					<label>
						<spring:message code="lbloptionHour" />
						<span class="required">*</span>
					</label>
					<select class="form-control " id="starthr" name="starthr"
						class="span3"">
						<option value="" selected="selected">
							<spring:message code="lbloptionHH" />
						</option>
						<c:forEach var="hrs" begin="1" end="12">
							<option value="${hrs}">
								${hrs}
							</option>
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-1 col-md-1 left25"
					style="width: 70px; padding-right: 0px; padding-left: 0px;">
					<label>
						<spring:message code="lbloptionMinute" />
						<span class="required">*</span>
					</label>
					<select class="form-control " id="startmin" name="startmin"
						class="span3"">
						<option value="" selected="selected">
							<spring:message code="lbloptionMM" />
						</option>
						<option value="00">
							<spring:message code="lbl00"/>
						</option>
						<option value="15">
							<spring:message code="lbl15"/>
						</option>
						<option value="30">
							<spring:message code="lbl30"/>
						</option>
						<option value="45">
							<spring:message code="lbl45"/>
						</option>
						<%-- 		<c:forEach var="min" begin="0" end="9">
										<option value="0${min}">
											0${min}
										</option>
									</c:forEach>
									<c:forEach var="min" begin="10" end="60">
										<option value="${min}">
											${min}
										</option>
									</c:forEach>
							--%>
					</select>
				</div>
				<div class="col-sm-1 col-md-1 left25"
					style="width: 70px; padding-right: 0px; padding-left: 0px;">
					<label>
						<spring:message code="sltAm/Pm" />
						<span class="required">*</span>
					</label>
					<select class="form-control " id="starttimeform"
						name="startminform" class="span3">
						<option value="" selected="selected">
							<spring:message code="sltAm/Pm" />
						</option>
						<option value="AM">
							<spring:message code="optAM" />
						</option>
						<option value="PM">
							<spring:message code="optPM" />
						</option>
					</select>
				</div>
				<div class="col-sm-5 col-md-5 left25">
					<label>
						<spring:message code="lblLocti" />
						<span class="required">*</span>
					</label>
					<input class="form-control" type="text" id="location"
						name="location" maxlength="100" />
				</div>
			</div>

		</div>
		<div class="row" style="padding-left: 16px;">
			<div class="col-sm-16 col-md-16">
				<div class="col-sm-3 col-md-3">
					<label>
						<spring:message code="lblEndDate" />
						<span class="required">*</span>
					</label>
					<input type="text" id="eventEndDate" name="eventEndDate"
						maxlength="0" class="form-control" />
				</div>
				<div class="col-sm-1 col-md-1 left25"
					style="width: 70px; padding-right: 0px; padding-left: 0px;">
					<label>
						<spring:message code="lbloptionHour" />
						<span class="required">*</span>
					</label>
					<select class="form-control " id="endhr" name="endhr" class="span3"">
						<option value="" selected="selected">
							<spring:message code="lbloptionHH" />
						</option>
						<c:forEach var="hrs" begin="1" end="12">
							<option value="${hrs}">
								${hrs}
							</option>
						</c:forEach>
					</select>
				</div>
				<div class="col-sm-1 col-md-1 left25"
					style="width: 70px; padding-right: 0px; padding-left: 0px;">
					<label>
						<spring:message code="lbloptionMinute" />
						<span class="required">*</span>
					</label>
					<select class="form-control " id="endtmin" name="endtmin"
						class="span3"">
						<option value="" selected="selected">
							<spring:message code="lbloptionMM" />
						</option>
						<option value="00">
							<spring:message code="lbl00"/>
						</option>
						<option value="15">
							<spring:message code="lbl15"/>
						</option>
						<option value="30">
							<spring:message code="lbl30"/>
						</option>
						<option value="45">
							<spring:message code="lbl45"/>
						</option>

						<%-- 		<c:forEach var="min" begin="0" end="9">
										<option value="0${min}">
											0${min}
										</option>
									</c:forEach>
									<c:forEach var="min" begin="10" end="60">
										<option value="${min}">
											${min}
										</option>
									</c:forEach>
									
							--%>
					</select>
				</div>
				<div class="col-sm-1 col-md-1 left25"
					style="width: 70px; padding-right: 0px; padding-left: 0px;">
					<label>
						<spring:message code="sltAm/Pm" />
						<span class="required">*</span>
					</label>
					<select class="form-control " id="endtimeform" name="endtimeform"
						class="span3">
						<option value="" selected="selected">
							<spring:message code="sltAm/Pm" />
						</option>
						<option value="AM">
							<spring:message code="optAM" />
						</option>
						<option value="PM">
							<spring:message code="optPM" />
						</option>
					</select>
				</div>
				<!--<div class="col-sm-5 col-md-5 left25" >
						<label>Location<span class="required">*</span></label>
						<input class="form-control" type="text" id="location" name="location" maxlength="100" />
					</div>
				-->
			</div>

		</div>
	</div>

	<div class="row" id="saveAndCancleScheduleLink"
		style="padding-left: 16px;">
		<div class="col-sm-5 col-md-5">
			<a href="#" onclick="saveSchedule(0)"><spring:message
					code="lnkImD" /> </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="cancelSchedule()"><spring:message
					code="btnClr" /> </a>
		</div>
	</div>

</div>

<!-- mukesh -->
<!--<div class="hide" id="addSchedulediv">
	<div class="row top10" id="scheduleStartEndDate">
		<div class="col-sm-4 col-md-4">
			
				<label>
					Start Date<span class="required">*</span>
				</label>
				<input type="text" id="eventStartDate" name="eventStartDate"
					maxlength="0" class="form-control">
			
		</div>
		<div class="col-sm-4 col-md-4">
			
				<label>
					End Date<span class="required">*</span>
				</label>
				<input type="text" id="eventEndDate" name="eventEndDate"
					maxlength="0" class="form-control"/>
					
		</div>
		
		<div class="col-sm-4 col-md-4">
			
				<label>
					Time Zone<span class="required">*</span>
				</label>
				<select id="timezone" name="timezone" class="form-control" ></select>
			
		</div>
		
	</div>

	<div class="row top10" id="evtimeslot">
		<div class="col-sm-12 col-md-12" style="font-family: 'Century Gothic','Open Sans', sans-serif;color: #474747;font-size: 13px;">
			Time Slots
		</div>
		<div class="col-sm-12 col-md-12 top5">
			<div class="col-sm-4 col-md-4">
				<label style="font-family: 'Century Gothic','Open Sans', sans-serif;color: #474747;">

					Start Time
				</label>
			</div>
			<div class="col-sm-4 col-md-4" style="margin-left: -5px;">
				<label style="font-family: 'Century Gothic','Open Sans', sans-serif;color: #474747;">
					End Time
				</label>
			</div>
			<div class="col-sm-3 col-md-3 ">
				<label>
					
				</label>
			</div>
		</div>
 ######################################################################################################### 

<div class="col-sm-8 col-md-8">
			<div class="row">
				<div class="col-sm-6 col-md-6">
				 <div class="row">
						<div class="col-sm-4 col-md-4">
						  <div class="row">
							<div class="col-sm-1 col-md-1" style="padding-top: 9px;">
									
							</div>
							<div class="col-sm-9 col-md-9" style="width: 70px;padding-right: 0px;padding-left: 0px;">
								Hour
							</div>
						 </div>
					   </div>

					  <div class="col-sm-4 col-md-4">
							Minute
					</div>

					<div class="col-sm-4 col-md-4">
						AM/PM
					</div>
			  </div>
			</div>
				
				<div class="col-sm-6 col-md-6">
				  <div class="row">
						<div class="col-sm-4 col-md-4">
							Hour
				       </div>
					<div class="col-sm-4 col-md-4">
						Minute
					</div>
					<div class="col-sm-4 col-md-4">
						AM/PM
					 </div>
					 
				</div>
			</div>
		</div>		
     </div>
    <div class="col-sm-4 col-md-4">
		Location
	</div>
 ######################################################################################################### 


	<c:forEach var="slots" begin="1" end="8">
		  <div class="col-sm-8 col-md-8 top10">
			<div class="row">
				<div class="col-sm-6 col-md-6">
				 <div class="row">
						<div class="col-sm-4 col-md-4">
						  <div class="row">
							<div class="col-sm-1 col-md-1" style="padding-top: 9px;">
									${slots}
							</div>
							<div class="col-sm-9 col-md-9" style="width: 70px;padding-right: 0px;padding-left: 0px;">
								<select class="form-control " id="starthr${slots}"
									name="starthr${slots}" class="span3"">
									<option value="" selected="selected">
										HH
									</option>
									<c:forEach var="hrs" begin="1" end="12">
										<option value="${hrs}">
											${hrs}
										</option>
									</c:forEach>
								</select>
							</div>
						 </div>
					   </div>

					  <div class="col-sm-4 col-md-4">
							<select class="form-control " id="startmin${slots}"
								name="startmin${slots}" class="span3"">
								<option value="" selected="selected">
									MM
								</option>
								<option value="00">00</option>
								<option value="15">15</option>
								<option value="30">30</option>
								<option value="45">45</option>								
								<c:forEach var="min" begin="0" end="9">
									<option value="0${min}">
										0${min}
									</option>
								</c:forEach>
								<c:forEach var="min" begin="10" end="60">
									<option value="${min}">
										${min}
									</option>
								</c:forEach>
							</select>
					</div>

					<div class="col-sm-4 col-md-4">
						<select class="form-control " id="starttimeform${slots}"
							name="startminform${slots}" class="span3"">
							<option value="" selected="selected">
								AM/PM
							</option>
							<option value="AM">
								AM
							</option>
							<option value="PM">
								PM
							</option>
						</select>
					</div>
			  </div>
			</div>
				
				<div class="col-sm-6 col-md-6">
				  <div class="row">
						<div class="col-sm-4 col-md-4">
							<select class="form-control " id="endhr${slots}"
								name="endhr${slots}" class="span3"">
								<option value="" selected="selected">
									HH
								</option>
								<c:forEach var="hrs" begin="1" end="12">
									<option value="${hrs}">
										${hrs}
									</option>
								</c:forEach>
							</select>
				       </div>
					<div class="col-sm-4 col-md-4">
						<select class="form-control " id="endmin${slots}"
							name="endmin${slots}" class="span3"">
							<option value="" selected="selected">
								MM
							</option>
							<option value="00">00</option>
							<option value="15">15</option>
							<option value="30">30</option>
							<option value="45">45</option>					
	
							<c:forEach var="min" begin="0" end="9">
								<option value="0${min}">
									0${min}
								</option>
							</c:forEach>
							<c:forEach var="min" begin="10" end="60">
								<option value="${min}">
									${min}
								</option>
							</c:forEach>
						</select>
					</div>
					<div class="col-sm-4 col-md-4">
						<select class="form-control " id="endtimeform${slots}"
							name="endtimeform${slots}" class="span3"">
							<option value="" selected="selected">
								AM/PM
							</option>
							<option value="AM">
								AM
							</option>
							<option value="PM">
								PM
							</option>
						</select>
					 </div>
					 
				</div>
			</div>
		</div>		
     </div>

	<div class="col-sm-4 col-md-4 top10">
		<input class="form-control" type="text" id="location${slots}"
			name="location${slots}" maxlength="100" />
	</div>
 </c:forEach>
</div>

	<div class="row top25" id="seldays">
		<div class="col-sm-12 col-md-12 left15">
			On Selected days only
		</div>

		<div class="col-sm-12 col-md-12 left15">
			<div class="col-sm-1 col-md-1">
				<label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="selecteddays" name="selecteddays"
						value="2" />
					M
				</label>
			</div>
			<div class="col-sm-1 col-md-1">
				<label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="selecteddays" name="selecteddays"
						value="3" />
					T
				</label>
			</div>
			<div class="col-sm-1 col-md-1">
				<label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="selecteddays" name="selecteddays"
						value="4" />
					W
				</label>
			</div>
			<div class="col-sm-1 col-md-1">
				<label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="selecteddays" name="selecteddays"
						value="5" />
					TH
				</label>
			</div>
			<div class="col-sm-1 col-md-1">
				<label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="selecteddays" name="selecteddays"
						value="6" />
					F
				</label>
			</div>
			<div class="col-sm-1 col-md-1">
				<label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="selecteddays" name="selecteddays"
						value="7" />
					SA
				</label>
			</div>
			<div class="col-sm-1 col-md-1">
				<label class="checkbox inline" style="padding-left: 0px;">
					<input type="checkbox" id="selecteddays" name="selecteddays"
						value="1" />
					SU
				</label>
			</div>
		</div>
	</div>

	<div class="row" id="valdays">
		<div class="col-sm-12 col-md-12 divErrorMsg" id="errDaysDiv">
		</div>
		<div class="col-sm-12 col-md-12">
			Valid for Days
		</div>
		<div class="col-sm-2 col-md-2 ">
			<input class="form-control" type="text" id="validfordays" value="${expiryDate}" maxlength="3"
				name="validfordays" onkeypress="return checkForInt1(event);"/>
		</div>
		<div class="col-sm-8 col-md-8 ">
			<input class="form-control" type="text" id="locationUrl"
				name="locationUrl" value="http://teachermatch.interview4.com/test_taker_login.php" disabled="disabled" />
		</div>

	</div>

	<div class="row" id="saveAndCancleScheduleLink">
		<div class="col-sm-5 col-md-5">
			<a href="#" onclick="saveSchedule(0)"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<a href="#" onclick="cancelSchedule()">Cancel</a>
		</div>
	</div>

</div>

-->
<!--<div class="row">
	<div class="col-sm-6 col-md-6 top15">
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<button onclick="continueSchedule()" class="btn btn-primary"
					id="save">
					<strong>Save and Continue <i class="icon"></i>
					</strong>
				</button>
			   &nbsp;&nbsp;
				<button onclick="exitSchedule()" class="btn btn-primary" id="save">
					<strong>Exit <i class="icon"></i>
					</strong>
				</button>
			</div>
		</div>

	</div>
</div>

-->

<div class="row" id="addScheduleLinkForSlots">
	<div class="col-sm-6 col-md-6 top15">
		<a href="javascript:void(0);" onclick="return addSchedule()"><spring:message
				code="lnkAddSlot" /> </a>
	</div>
</div>
<div class="row top15">

	<div class="col-sm-6 col-md-6">
		<button onclick="saveSchedule(0)" class="btn btn-warningevents"
			id="save">
			<strong><spring:message code="btnSaveExit" /> <i
				class="icon"></i> </strong>
		</button>
		&nbsp;&nbsp;
		<button onclick="exitSchedule();" class="btn btn-dangerevents"
			id="save">
			<strong><spring:message code="btnClr" /> <i class="icon"></i>
			</strong>
		</button>
	</div>
	<div class="col-sm-6 col-md-6" style="text-align: right">
		<button onclick="exitSchedule();" class="btn btn-primary" id="save">
			<i class="backicon"></i>
			<strong><spring:message code="btnBack" /> </strong>

		</button>
		&nbsp;&nbsp;
		<button onclick="saveSchedule(1)" class="btn btn-primary" id="save">
			<strong><spring:message code="btnNext" /> <i class="icon"></i>
			</strong>
		</button>
	</div>

</div>







<div class="modal hide" id="myModalactMsgShow" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showMessageDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='confirmDeactivate'>
					<spring:message code="msgWantToMove" />
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn  btn-primary"
						onclick="removeScheduleById();">
						<spring:message code="lblYes" />
						<i class="icon"></i>
					</button> </span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="hideMessageDiv()">
					<spring:message code="lblNo" />
				</button>
			</div>
			<input type="hidden" id="scheduledelId" name="scheduledelId" value="">

		</div>
	</div>
</div>

<!-- mukesh -->
 <div style="clear: both;"></div>

   <div class="modal hide"  id="scheduleSelectedcandidate"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:750px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hidediv();">x</button>
	   	<h3 id="">Candidates</h3>
	</div>
	
	<div class="modal-body" style="max-height: 600px; overflow-y: auto;"> 		
			<div class="" id="scheduleSelectedcandidateDiv" ></div>        
            
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='hidediv();'>Close</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>

<div class="modal" style="display: none;" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Add Participant</h3>
      </div>
      <div class="modal-body">
      	<div class="divErrorMsg" id='message2Error'>
		</div>
      	<div class="control-group" id='message21showConfirm'>
		</div>
      </div>
      <div class="modal-footer" id="footerbtn2">
        	<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
      </div>
    </div>
  </div>
</div>


<div style="display: none;z-index: 2000;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>
<input type="hidden" id="eventId" name="eventId"
	value="${eventDetails.eventId}">
<input type="hidden" id="inttype" name="inttype"
	value="${inttypeForEvent}">
<input type="hidden" id="format" name="format"
	value="${inttypeForEvent}">
<input type="hidden" id="scheduleId" name="scheduleId">
<input type="hidden" id="scheduleIdM" name="scheduleIdM" value="0">
<input type="hidden" id="inputDivCount" name="inputDivCount" value="1">
<input type="hidden" id="chkDATFlag" name="chkDATFlag" value="${chkDATFlag}">
<input type="hidden" id="scheduleIdForView" name="scheduleIdForView">
<input type="hidden" id="deleteScheduleId" name="deleteScheduleId">
<script>
    <%--  <c:if test="${eventDetails.eventFormatId.eventFormatId ne 1}">
         getTimeZone();
         displayEventSchedule();
      </c:if>
      
      <c:if test="${eventDetails.eventFormatId.eventFormatId ==1}">
          $("#eventScheduleM").hide();
          $("#saveAndCancleScheduleLink").hide();
          checkvideoSchedule(${eventDetails.eventId});
      </c:if>
	--%>
       displayEventSchedule();
</script>


<script type="text/javascript">	
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     function call(obj){
        cal.manageFields($(obj).attr('id'), $(obj).attr('name'), "%m-%d-%Y");
     }
     


     </script>
