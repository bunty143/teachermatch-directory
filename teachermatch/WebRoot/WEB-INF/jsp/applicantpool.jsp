<!-- @Author: Gagan 
 * @Discription: view of Application Pool Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>

<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js?ver=${resourceMap['CandidateGridAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ApplicantPoolAjax.js?ver=${resourceMap['ApplicantPoolAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/report/candidatereport.js?ver=${resourceMap['js/report/candidatereport.js']}"></script>
<script type='text/javascript' src="js/applicationpool.js?ver=${resourceMap['js/applicationpool.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#applicantTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 973,
        minWidth: null,
        minWidthAuto: false,
       // colratio:[292,195,146,146,195], // table header width
       	colratio:[292,195,145,207,165], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblHired()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#applicantHiredTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 973,
        minWidth: null,
        minWidthAuto: false,
        colratio:[292,195,146,146,195], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
 </script>
<!--    <h1>Welcome Gagan </h1><br>-->
<div class="container">
	<div class="row mt10">
		<div class="span16 centerline ">
		<div class="span1 m0"><img src="images/applicantpool.png" width="41" height="41"></div>
		<div class="span6 subheading"><spring:message code="headAppPool"/></div>
		</div>
		<div class="clearfix"></div>
	</div>
	<%-- Condition For Showing Hired At Location --%>
	<c:if test="${jobOrder.createdForEntity==2}">
		<c:set var="captionDistrictOrSchool" value=""></c:set>
		<c:set var="districtOrSchoolName" value=""></c:set>
			<c:set var="captionDistrictOrSchool" value="District"></c:set>
			<c:set var="districtOrSchoolName" value="${jobOrder.districtMaster.districtName}"></c:set>
			<c:if test="${jobOrder.districtMaster.displayName ne ''}">
				<c:set var="districtOrSchoolName" value="${jobOrder.districtMaster.displayName}"></c:set>
			</c:if>
        	    
     </c:if>
     <c:if test="${jobOrder.createdForEntity==3}">
		<c:set var="captionDistrictOrSchool" value=""></c:set>
		<c:set var="districtOrSchoolName" value=""></c:set>
			<c:set var="captionDistrictOrSchool" value="School"></c:set>
			<c:set var="districtOrSchoolName" value="${lstSchoolInJobOrder.schoolId.schoolName}"></c:set> 
   </c:if>
   	<c:set var="topPadding" value="padding-top: 4px;"></c:set>
	<div class="row mt10">
			<div class=" span16">
<!--	<div class="span14 offset1 mt10">-->
	<div class="row">
		<div class="span5"><label style="${topPadding}"><strong><spring:message code="lblJobOdr"/> </strong></label></div>
		<div class="span5"><label style="${topPadding}"><strong><spring:message code="lblJoTil"/></strong></label></div>
	</div>
	<div class="row">
		<div class="span5">${jobOrder.jobId}</div>
		<div class="span5">${jobOrder.jobTitle}</div>
		<input type="hidden" id="jobId" name="jobId" value="${jobOrder.jobId}">
		<input type="hidden" id="noteId" name="noteId" value=""/>
	</div>
<!--	</div>-->
<!--	<div class="span14 offset1 mt10">-->
		<div class="row">
		<div class="span11"><label style="${topPadding}"><strong>${captionDistrictOrSchool} <spring:message code="lblName"/></strong></label></div>
		<div class="span11">${districtOrSchoolName}</div>
		</div>
<!--	</div>-->
	
   
<!--	<div class="offset1 span14"><br></div>-->
	<div class="row">
		<div class=" span16 mb"><br>Applicant Details: ${totalNoOfApplicants} Applicants</div>
	</div>
	
	<div class="row"><input type="hidden" id="gridNo" name="gridNo" value="">
	<div id="applicantGrid" class=" span16" onclick="getSortFirstGrid()">
	</div>
	</div>
	
	<div class="row">
		<div class=" span16 mt10 mb">Hired Pool: ${totalHiredApplicants} Applicants Hired</div>
	</div>
	
	<div class="row">
	<div id="applicantHiredGrid"  class=" span16" onclick="getSortSecondGrid()">
	</div>
	</div>
	</div>
</div>
</div>

<div  class="modal1 hide"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='blockMessage'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>

<div class="modal hide"  id="myModalNotes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="lblName"/></button>
		<h3 id="myModalLabel"><spring:message code="headNot"/></h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="divNotes" style="padding-left: 15px; " onclick="getSortThirdNotesGrid()">						
		</div>
				
		<div class="row mt10">
			<div class='span10 divErrorMsg' id='errordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong><spring:message code="lblEtrNot"/><span class="required">*</span></strong></label>
		    	<div class="span14" id='divTxtNode' style="padding-left: 0px; margin-left: 0px; " >
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
		    	<textarea readonly id="txtNotes" name="txtNotes" class="span10" rows="4"   ></textarea>
		    	</div>        	
			</div>						
		</div>
 	</div>
 	 	
 	<div class="modal-footer">
 		<c:set var="chkSaveDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSaveDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSaveDisp" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSaveDisp" value="none" />
			</c:otherwise>
		</c:choose>
 		<span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()">
<spring:message code="lnkSava"/></a></span>
 		<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button class="btn btn-large btn-primary"  onclick="saveNotes()">
<spring:message code="lnkCancel"/> <i class="icon"></i></button></span>
 		<button class="btn" data-dismiss="modal" aria-hidden="true">
<spring:message code="btnClose"/></button> 		
 	</div>
</div>

<div class="modal1 hide" style='width:510px; top:50%;' id="myModalMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<input type="hidden" id="teacherDetailId" name="teacherDetailId">
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
<spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCandidate"/></h3>
	</div>
	<div  class="modal-body">
		<div class="control-group">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		<div class="control-group">
    		<label><strong><spring:message code="lblTo"/><br/></strong><span id="emailDiv"></span></label>
   		</div>
		<div id='support'>
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="span8" maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
	        	<textarea rows="5" class="span8" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        	<!--<div style='background-color: #FFFFCC;' >-->
	        </div>
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
 	<iframe src="" id="ifrmResume" width="100%" height="480px" style="display: none;">
 </iframe>  
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel"/></button>
 		<button class="btn btn-primary" onclick="validateMessage()" ><spring:message code="btnSend"/></button>
 	</div>
</div>
<!--End message Div by  Sekhar  -->
<div  class="modalTrans hide"  id="modalDownloadPDR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headPdRep"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="">
<iframe src="" id="ifrmPDR" width="100%" height="480px">
 </iframe>        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
 	</div>
</div>
<div style="display:none;" id="loadingDiv">
     <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<!-- <tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'>Your report is being generated...</td></tr> -->
	</table>
 </div>
<script type="text/javascript">
	displayApplicantGrid();
	displayHiredApplicantGrid();
	
	$(document).ready(function(){
	$('#divTxtNode').find(".jqte").width(870);
	});
</script>

<script type="text/javascript">
$('#myModal').modal('hide');
</script>