<!-- @Author: Gagan 
 * @Discription: view of edit domain Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/DomainAjax.js?ver=${resourceMap['DomainAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/domain.js?ver=${resourceMap['js/domain.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#domainTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[536,146,146,146], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headManageDomains"/></div>	
         </div>			
		 <div class="pull-right add-employment1">
			<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
			<a href="javascript:void(0);" onclick="return addNewDomain()"><spring:message code="lnAddDomain"/></a>
		</c:if>

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="TableContent top15">        	
         <div class="table-responsive" id="domainGrid">          
         </div>            
</div> 

	
<div  id="divDomanForm" style="display: none;" class="top10" onkeypress="return chkForEnterSaveDomain(event);" >
	<div class="row col-sm-12 col-md-12">			                         
	 	<div class='divErrorMsg' id='errordiv' ></div>
	</div>
	<form  id="frmDomin" onsubmit="return false;">
		
		<div class="row">
			<div class="col-sm-5 col-md-5"><label><strong><spring:message code="lblDomName"/><span class="required">*</span></strong></label>
				<input type="hidden" name="domainId" id="domainId" >
				<input type="hidden" name="multiplier" id="multiplier" >
				<input type="text" name="domainName" id="domainName" maxlength="100" class="form-control" placeholder="">
				
			</div>
			<div class="col-sm-2 col-md-2"><label><strong><spring:message code="lblDomainUId"/><span class="required">*</span></strong></label>
				<input type="text" name="domainUId" id="domainUId" maxlength="1" class="form-control" onKeyPress='return isAlphabetsKey(event)' placeholder="">
			</div>
	     </div>
	     
		<div class="row top5">
		
			<div class="col-sm-2 col-md-2">
					<label><spring:message code="lblUseInPdRep"/></label></br>
					<input type="checkbox"  value="1" id="displayInPDReport"  name="displayInPDReport" > 
			</div>
			
			<div class="col-sm-2 col-md-2">
			                 <label><strong><spring:message code="lblStatus"/><span class="required">*</span></strong></label>					 
					         <div class="left20">								 
								<label class="radio p0 top5">
									<input type="radio"  value="A" id="domainStatus1"  name="domainStatus" checked="checked" > <spring:message code="lblActive"/>
								</label>							
							</div>
							
							  <div style="margin-left:80px;margin-top: -30px;">						
						        <label class="radio p0 top5">
						        	<input type="radio" value="I" id="domainStatus2" name="domainStatus" ><spring:message code="lblInActiv"/>
								</label>  						             
         	                </div>
			</div>
		</div>
		
			
		 <div class="row" style="display: none;" id="divManage">
		  	<div class="col-sm-4 col-md-4">
		  		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
		     		<button onclick="saveDomain();" class="btn  btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
		     	</c:if>
		     	 &nbsp;<a href="javascript:void(0);" onclick="clearDomain();"><spring:message code="lnkCancel"/></a>  
		    </div>
		  </div>
		
		  <div class="row">
		<div class="col-sm-4 col-md-4 idone" id="divDone" style="display: none;">
		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
			<a href="javascript:void(0);" onclick="saveDomain();"><spring:message code="lnkImD"/></a>
		</c:if>
			 &nbsp;<a href="javascript:void(0);" onclick="clearDomain();"><spring:message code="lnkCancel"/></a> 
		</div>
		</div>
	</form>
</div>

           


<script type="text/javascript">
	displayDomain();
</script>



<!-- @Author: Gagan 
 * @Discription: view of edit domain Page.
 -->