<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap" value="${applicationScope.resouceMap}" />
<script type="text/javascript" 	src="dwr/interface/QuestAjax.js?ver=${resouceMap['QuestAjax.ajax']}"></script>
<script type='text/javascript' 	src="js/quest.js?ver=${resouceMap['js/quest.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" 	href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" 	href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
 

<style>
textarea {
	width: 420px;
	height: 100px;
}

.table-bordered {
	border-collapse: separate;
	border-color: #cccccc;
}

.table-bordered td,th {
	border-left: 0px solid #dddddd;
}

.tdscore0 {
	width: 1px;
	height: 40px;
	float: left;
}

.tddetails0 {
	width: 1px;
	height: 40px;
	float: left;
}

tr.bgimage {
	background-image: url(images/color-gradients.png);
	background-repeat: no-repeat no-repeat;
}

.dropdown-menu {
	min-width: 40px;
}


/*.popover{
  width: 770px;
  background-color: #ffffff;
  border: 1px solid #007AB4;
  }*/
.popover-title {
	margin: 0;
	padding: 0px 0px 0px 0px;
	font-size: 0px;
	line-height: 0px;
}

.popover-content {
	margin: 0;
	padding: 0px 0px 0px 0px;
}

.popover.right .arrow {
	top: 100px;
	left: -10px;
	margin-top: -10px;
	border-width: 10px 10px 10px 0;
	border-right-color: #ffffff;
}

.popover.right .arrow:after {
	border-width: 11px 11px 11px 0;
	border-right-color: #007AB4;;
	bottom: -11px;
	left: -1px;
}

.pull-left {
	margin-left: -52px;
	margin-top: 7px;
}

.tblborder {
	
}

.nobground2 {
	padding: 6px 7px 6px 7px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}

.nobground1 {
	padding: 6px 10px 6px 10px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}

.nobground3 {
	padding: 6px 4px 6px 4px;
	-webkit-border-top-left-radius: 30px;
	border-top-left-radius: 30px;
	-moz-border-radius-topleft: 30px;
	-webkit-border-top-right-radius: 30px;
	border-top-right-radius: 30px;
	-moz-border-radius-topright: 30px;
	-webkit-border-bottom-right-radius: 30px;
	border-bottom-right-radius: 30px;
	-moz-border-radius-bottomright: 30px;
	-webkit-border-bottom-left-radius: 30px;
	border-bottom-left-radius: 30px;
	-moz-border-radius-bottomright: 30px;
}

.icon-ok-circle {
	font-size: 3em;
	color: #00FF00;
}

.icon-circle {
	font-size: 3em;
	color: #E46C0A;
}

.icon-remove-circle {
	font-size: 3em;
	color: red;
}

.icon-circle-blank {
	font-size: 3em;
	color: red;
}



.icon-inner {
	font-size: 2.3333333333333333em;
	font-family: 'Bauhaus 93';
	color: red;
	margin-left: -26px;
	vertical-align: 5%;
}

.icon-inner2 {
	font-size: 2.3333333333333333em;
	font-family: 'Bauhaus 93';
	color: red;
	margin-left: -24px;
	vertical-align: 8%;
}

.circle {
	width: 48px;
	height: 48px;
	margin: 0em auto;
}

.subheading {
	font-weight: none;
}

.icon-folder-open,icon-copy,icon-cut,icon-paste,icon-remove-sign,icon-edit
	{
	color: #007AB4;
}

.marginleft20 {
	margin-left: -20px;
}

.margintop20 {
	margin-top: -20px;
}

div.t_fixed_header_main_wrapper {
	position: relative;
	overflow: visible;
}

.modal_header_profile {
	border-bottom: 1px solid #eee;
	background-color: #0078b4;
	text-overflow: ellipsis;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3); *
	border: 1px solid #999;
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	outline: none;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}

.scrollspy_profile {
	height: 500px;
	overflow-y: auto;
	overflow-x: hidden;
	padding-left: 5px;
}

.divwidth {
	width: 728px;
}

.tablewidth {
	width: 900px;
}

.net-widget-footer {
	border-bottom: 1px solid #cccccc;
	border-left: 1px solid #cccccc;
	border-right: 1px solid #cccccc;
	line-height: 40px;
	background-color: #F2FAEF;
	background-image: -moz-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFFFF),
		to(#FFFFFF) );
	background-image: -webkit-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: -o-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: linear-gradient(to bottom, #FFFFFF, #FFFFFF);
	background-repeat: repeat-x;
	color: #4D4D4E;
	vertical-align: middle;
}

.modaljob {
	position: fixed;
	top: 40%;
	left: 45%;
	z-index: 2000;
	overflow: auto;
	width: 980px;
	margin: -250px 0 0 -440px;
	background-color: #ffffff;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3); *
	border: 1px solid #999;
	/* IE6-7 */
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}

.net-corner-bottom {
	-moz-border-radius-bottomleft: 12px;
	-webkit-border-bottom-left-radius: 12px;
	border-bottom-left-radius: 12px;
	-moz-border-radius-bottomright: 12px;
	-webkit-border-bottom-right-radius: 12px;
	border-bottom-right-radius: 12px;
}

.custom-div-border1 {
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3);
	-moz-border-radius: 6px;
	border-radius: 6px;
}

.modal-border { /*border: 1px solid #0A619A;*/
	
}

.custom-div-border {
	padding: 1px;
	border-bottom-left-radius: 2em;
	border-bottom-right-radius: 2em;
}

.pdfDivBorder {
	z-index: 5000;
	border: 1px solid #999;
	border: 1px solid rgba(0, 0, 0, 0.3);
	-webkit-border-radius: 6px;
	-moz-border-radius: 6px;
	border-radius: 6px;
	-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
	-webkit-background-clip: padding-box;
	-moz-background-clip: padding-box;
	background-clip: padding-box;
}

.custom-border {
	margin-left: -32px;
	margin-top: 2px;
}

.dropdown-menu a {
	display: block;
	padding: 3px 15px;
	margin-padding: 100px;
	clear: both;
	font-weight: normal;
	line-height: 20px;
	color: #333333;
	white-space: nowrap;
}

.dropdown-menu a:hover {
	color: #007AB4;
}

.status-notes-image {
	margin-left: 40px;
}

.status-notes-text {
	margin-top: -20px;
	margin-left: 80px;
}
</style>



<c:set var="numcols" value="0" />
<c:if test="${prefMap['achievementScore']}">
	<c:set var="numcols" value="${numcols + 2}" />
</c:if>
<c:if test="${prefMap['tFA']}">
	<c:set var="numcols" value="${numcols + 1}" />
</c:if>
<c:if test="${prefMap['expectedSalary']}">
	<c:set var="numcols" value="${numcols + 1}" />
</c:if>
<c:if test="${prefMap['teachingOfYear']}">
	<c:set var="numcols" value="${numcols + 1}" />
</c:if>


<script type="text/javascript"><!--

//alert("efewf");
/*
function ReadCookie()
{
   var allcookies = document.cookie;
  // alert("All Cookies : " + allcookies );

   // Get all the cookies pairs in an array
   cookiearray  = allcookies.split(';');
	alert(cookiearray.length);
   // Now take key value pair out of this array
   for(var i=0; i<cookiearray.length; i++){
      name = cookiearray[i].split('=')[0];
      value = cookiearray[i].split('=')[1];
      alert("Key is : " + name + " and Value is : " + value);
   }
}
ReadCookie();*/

var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });

function applyScrollOnTblGeoZoneSchool()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#geozoneSchoolTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 588,
        minWidth: null,
        minWidthAuto: false,
        //colratio:[55,266,70,90,100,105,70,66,151], // table header width
        //mukesh to set the table
        colratio:[380,250],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
} 
function applyScrollOnTbl()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,        
        width: 940,    
        minWidth: null,
        minWidthAuto: false,    
      //roleId
	/*	<c:if test="${entityID==1}">
               colratio:[300,110,60,100,90,100,80,110],
        </c:if>       
        <c:if test="${numcols==1 && entityID!=1}">
          colratio:[380,150,150,150,139],
        </c:if>
		 <c:if test="${numcols==2 && entityID!=1}">
         colratio:[340,115,120,125,135,103],
        </c:if>
        <c:if test="${numcols==2 && entityID==3}">
         colratio:[340,150,150,150,139],
        </c:if>
		 <c:if test="${numcols==3 && entityID!=1}">
         colratio:[310,110,90,105,110,130,103],
        </c:if>
		 <c:if test="${numcols==4 && entityID!=1}">
          colratio:[310,90,75,85,105,100,90,100],
        </c:if>
         <c:if test="${numcols==5 && entityID!=1}">
           colratio:[280,90,90,90,50,85,85,80,100],
        </c:if>
		<c:if test="${numcols==0 && entityID!=1}">
          colratio:[300,310,220,100],
        </c:if>*/
         colratio:[444,400,94],
	    addTitles: false,
        zebra: true,
        //zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTranscript()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTrans').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,150,150,150,150,125], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnCertification()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblCert').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,50,350,200,175], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}


function applyScrollOnJob()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 235,
        width: 950,
        minWidth: null,
        minWidthAuto: false,
       // colratio:[35,35,200,100,120,80,100,75,100,110], // table header width
        
        <c:choose>
    	<c:when test="${entityID==1}">
         colratio:[60,55,185,110,120,80,100,65,70,110], // table header width
        </c:when>
        <c:otherwise>
        <c:choose>
		<c:when test="${prefMap['fitScore'] && prefMap['demoClass']}">
        colratio:[60,55,185,110,130,80,100,65,70,110],
        </c:when>
        <c:when test="${!prefMap['fitScore'] && !prefMap['demoClass']}">
        colratio:[60,55,290,110,130,80,110,120],
        </c:when>
        <c:otherwise>
        colratio:[50,60,150,60,90,110,80,110,60,80,100],
        </c:otherwise>
        </c:choose>
        </c:otherwise>
     </c:choose>
     
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 50,
        wrapper: false
        });
            
        });		
}
//applyScrollOnJob();

// New code by Ramesh


// Start Div Grid

function applyScrollOnTblWorkExp()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblWorkExp_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        
       <c:choose>
          <c:when test="${entityID==2}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        //colratio:[148,222,111,119,111],
       // colratio:[148,278,90,103,111],
        <c:choose>
          <c:when test="${entityID==2}">
             colratio:[168,298,110,123,131],
           </c:when>
           <c:otherwise>
	         colratio:[148,278,90,103,111],
	       </c:otherwise>
       </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblWorkExp();
function applyScrollOnTblEleRef_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleleReferences_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
         <c:choose>
          <c:when test="${entityID==2}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[150,130,100,160,100,80],
          //colratio:[188,87,90,170,80,65,50],
         // colratio:[120,85,90,160,80,82,65,45],		//changed by Ashish 14-012014
         <c:choose>
          <c:when test="${entityID==2 && DistrictId==7800038}">
             colratio:[160,130,160,135,90,45,110],
           </c:when>
           <c:when test="${entityID==2 && DistrictId ne 7800038}">
             colratio:[120,75,90,150,100,85,55,45,170],
           </c:when>
           <c:otherwise>
	         colratio:[160,130,160,135,90,45,110],
	       </c:otherwise>
       </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblEleRef_profile();


function applyScrollOnTblVideoLinks_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblelevideoLink_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        <c:choose>
          <c:when test="${entityID==2}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[600,110],
         <c:choose>
          <c:when test="${entityID==2}">
              colratio:[620,110,100],
           </c:when>
           <c:otherwise>
	          colratio:[620,110],
	       </c:otherwise>
       </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblVideoLinks_profile();

function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
       // width: 728,
        <c:choose>
          <c:when test="${entityID==2}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        //colratio:[500,228,],
         <c:choose>
          <c:when test="${entityID==2}">
             colratio:[550,280],
           </c:when>
           <c:otherwise>
	         colratio:[500,230],
	       </c:otherwise>
       </c:choose>
        
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}



function applyScrollOnTblProfileVisitHistory()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblProfileVisitHistory_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 500,
        width: 570,
        minWidth: null,
        minWidthAuto: false,
          colratio:[460,110],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblProfileVisitHistory();

function applyScrollOnTblTeacherAcademics_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherAcademics_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        <c:choose>
          <c:when test="${entityID==2}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         <c:choose>
          <c:when test="${entityID==2}">
             colratio:[435,205,85,85],
           </c:when>
           <c:otherwise>
	        colratio:[410,180,60,60],
	       </c:otherwise>
       </c:choose>
       // colratio:[350,260,60,60],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblTeacherAcademics_profile();

function applyScrollOnTblTeacherCertifications_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherCertificates_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        <c:choose>
          <c:when test="${entityID==2}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        //  colratio:[500,230],
         <c:choose>
          <c:when test="${entityID==2}">
             colratio:[225,125,125,155,255],
           </c:when>
           <c:otherwise>
	         colratio:[200,100,100,130,200],
	       </c:otherwise>
       </c:choose>
	
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}
//applyScrollOnTblTeacherCertifications_profile();

function applyScrollOnJobsTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobTableLst').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 950,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[30,40,55,300,100,140,170,120],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}
function applyepiAndJsiTable()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#epiAndJsiTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 635,
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
  }
  
  function applyOhioScrollOnPrintTable()
{
   //alert("11")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridOhioCandidateList').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
       // height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[130,115,115,180,100,100,,100],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}
</script>

<style>
.icon-large2 {
	font-size: 2em;
}

.icon-large3 {
	font-size: 1.33em;
}
</style>



 <!-- PDF by shriram -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadOhioCandidatesList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Ohio Candidate List</h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS1" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
	 	</div>
     </div>
	 	</div>
	</div>

 <!-- printing Start by shriram  -->
    
  <div style="clear: both;"></div>

   <div class="modal hide"  id="printOhioCandidateListDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1000px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id="">Print Preview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="printhiredApplicantsDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printNobleJobsDATA();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
     <div style="clear: both;"></div>

   <div class="modal hide"  id="printmessageOhio"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   <h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			<spring:message code="msgPlzPopAreAlloued"/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    <!-- printing End --> 

         <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans2" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
<input type="hidden"  name="isReqNoForHiring" id="isReqNoForHiring" value="${isReqNoForHiring}"/>
<div class="row">
	<input type="hidden" id="teacherIdForprofileGrid" name="teacherIdForprofileGrid" />
	<input type="hidden" id="teacherIdForprofileGridVisitLocation"
		name="teacherIdForprofileGridVisitLocation" value="Teacher Pool" />
</div>

<div class="row" style="margin-left: 0px; margin-right: 0px;">
	<div style="float: left;">
		<img src="images/districtjobs-orders.png" width="41" height="41"
			alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">
			<spring:message code="headAppPool"/>
		</div>
	</div>
	<div style="clear: both;"></div>
	<div class="centerline"></div>
</div>
<div class="top10">
	<div class="span16"
		onkeypress="return chkForEnterSearchTeacher(event);">
		<form class="bs-docs-example">
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<span class=""><label class="">
							<spring:message code="lblFname"/>
						</label>
						<input type="text" id="firstName" name="firstName" maxlength="50"
							class="help-inline form-control">
					</span>
				</div>
				<div class="col-sm-3 col-md-3">
					<div class="">
						<span class=""><label class="">
								<spring:message code="lblLname"/>
							</label>
							<input type="text" id="lastName" name="lastName" maxlength="50"
								class="help-inline form-control">
						</span>
					</div>
				</div>
				<div class="col-sm-4 col-md-4" style='white-space: nowrap;'>
					<div>
						<label class="">
							<spring:message code="lblEmailAddress"/>
						</label>
						<input type="text" id="emailAddress" name="emailAddress"
							maxlength="75" class="form-control fl">
					</div>
				</div>
				<div class="col-sm-2 col-md-2" style="padding-left: 20px;">
					<button class="btn btn-primary top25-sm2" type="button"
						onclick="searchTeacher()" style="width: 95px;">
						<spring:message code="btnSearch"/>
						<i class="icon"></i>
					</button>
				</div>
			</div>
			<div class="row top7">
				<div class="col-sm-6 col-md-6">
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<label>
								<strong><spring:message code="lblCndAppForJobOurDistL"/></strong>
							</label>
						</div>
						<div class="col-sm-12 col-md-12">
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 70px;">
								<input type="radio" value="0" id="jb1" name="jobApplied"
									checked="checked" onclick="showHideJbApld(0);">
								<spring:message code="lbl7D"/>

							</label>
							<label class="col-sm-2 col-md-2 radio "
								style="margin-top: 0px; width: 80px;">
								<input type="radio" value="1" id="jb1" name="jobApplied"
									onclick="showHideJbApld(0);">
								<spring:message code="lbl15D"/>
							</label>
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 80px;">
								<input type="radio" value="2" id="jb2" name="jobApplied"
									onclick="showHideJbApld(0);">
								<spring:message code="lbl30D"/>
							</label>
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 80px;">
								<input type="radio" value="3" id="jb3" name="jobApplied"
									onclick="showHideJbApld(0);">
								<spring:message code="lbl60D"/>
							</label>
							<!--<label class=" span60px radio">
									<input type="radio" value="4" id="jb4" name="jobApplied" onclick="showHideJbApld(0);">
							    	90 Days
						        </label>
						        -->
							<label class="col-sm-2 col-md-2 radio"
								style="margin-top: 0px; width: 70px;">
								<input type="radio" value="5" id="jb5" name="jobApplied"
									onclick="showHideJbApld(1);">
								<spring:message code="lblOthr"/>
							</label>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-6" style="margin-left: -15px;">
					<div class="col-sm-6 col-md-6" style="display: none;" id="jaFrom">
						<span class=""><label class="">
								<spring:message code="lblJobAppF"/>
							</label> <input type="text" id="jobAppliedFromDate" name="fromDate"
								class="help-inline form-control">
						</span>
					</div>
					<div class="col-sm-6 col-md-6" style="display: none;" id="jaTo">
						<span class=""><label class="">
								<spring:message code="lblJobAppTo"/>
							</label> <input type="text" id="jobAppliedToDate" name="toDate"
								class="help-inline form-control">
						</span>
					</div>
				</div>
			</div>

  <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadApplicantpool" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headAppPool"/></h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->  
<div class="row"> 
<div class="col-sm-6 col-md-">  
<div id='tpMenuActionDiv' style='display:none;'>
	<table  class='tblborder' cellpadding=0>
		<tr>
			<td style='font-size: 12px;' vertical-align='middle' ><spring:message code="lblAct"/>&nbsp;&nbsp;</td>
			<td>
				<span class="btn-group">
					<a data-toggle="dropdown" href="javascript:void(0);">
						<span class='icon-collapse  iconcolor icon-large'></span>
					</a>
					<ul class="dropdown-menu pull-left">
						<li>
							<a href="javascript:void(0);" onclick='sendMessageMassTP()'><spring:message code="btnSendMsg"/></a>
						</li>
					</ul>
				</span>
			</td>			
		</tr>
	</table>
</div>
</div>
<!--<div class="col-sm-6 col-md-6">
 <div class="row hide mt5" style="padding-right:20px;" id="exportIconsDiv">
       <table class="marginrightForTablet">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="downloadApplicantPoolExcelReport();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='downloadApplicantPoolPDFReport();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				 <td>
					<a data-original-title='Print' rel='tooltip' id='printId' href='javascript:void(0);' onclick="printApplicantPoolPreview();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
		</div>
	</div>
--></div>

<div style="clear: both;"></div>
     <br/>
         <table class="lableForTest" style="float: right; margin-bottom: -23px;">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' class="activate" href='javascript:void(0);'  onclick="getCandidateListOfAllJobs();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  class="activate" href='javascript:void(0);' onclick='getOhioCandidateListPdf();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td>
					<a data-original-title='Print Preview' rel='tooltip' class="activate" href='javascript:void(0);' onclick="generateOhioCandidatListPrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td></tr>
		</table>
           <div style="clear: both;"></div>
  
  
       
<div class="TableContent" style="margin-top:20px;">
	<div class="table-responsive" id="teacherGrid">

	</div>
</div>

<div id="teacherGridProfile">

</div>
<br>
<br>


<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<!-- <tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'>Your report is being generated...</td></tr> -->
	</table>
</div>

<div class="modal hide" id="myModalProfile" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="hideProfile()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headProf"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divProfile" class="">
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						<br />
						&nbsp;
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="hideProfile()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>
<!--@Start
	@Ashish :: Add Qualification Div -->
<div class="modal hide" id="qualificationDiv"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 520px;">
		<div class='modal-content'>
			<div class="modal-header" id="qualificationDivHeader">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick=''>
					x
				</button>
				<h3>
					<spring:message code="headQualif"/>
				</h3>
			</div>

			<div class="modal-body"
				style="max-height: 400px; padding-top: 0px; overflow-y: scroll;">
				<div class='divErrorMsg' id='errorQStatus' style="display: block;"></div>
				<div id="qualificationDivBody"></div>
			</div>
			<div class="modal-footer">
				<table width=470 border=0>
					<tr>
						<td width=100 nowrap align=left>
							<span id="qStatusSave"><button
									class="btn  btn-large btn-orange"
									onclick='saveQualificationNoteForTP(0)'>
									<spring:message code="btnIn-Progress"/>
								</button>&nbsp;&nbsp;</span>
						</td>

						<td width=200 nowrap>
							<span id="qStatusFinalize"><button
									class="btn  btn-large btn-primary"
									onclick='saveQualificationNoteForTP(1)'>
									<spring:message code="btnFinalize"/>
									<i class="iconlock"></i>
								</button>&nbsp;&nbsp;</span>
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick='cancelQualificationIssuse();'>
								<spring:message code="btnClr"/>
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<!--@End
	@Ashish :: Add Qualification Div -->


<!-- @Start
@Ashish :: draggable div for Teacher Profile -->
<div class="modal hide custom-div-border1" id="draggableDivMaster"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true" data-backdrop="static">
	<div id="draggableDiv">

	</div>
</div>







<!-- @End
@Ashish :: draggable div for Teacher Profile -->
<div class="modal hide" id="myModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					×
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='blockMessage'>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnOk"/>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="divAlert" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static" style="z-index: 5000;">
	<!--<div class="modal-header dragHeader ui-dialog-titlebar ui-widget-header  " >
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="setZIndexActDiv()">x</button>
		<h3 id="myModalLabel" >TeacherMatch</h3>
	</div>
	-->
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" id='vcloseBtnk'>
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divAlertText">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span><button class="btn" data-dismiss="modal"
						aria-hidden="true" onclick="setZIndexActDiv()">
						<spring:message code="btnClose"/>
					</button>
				</span>
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="myModalPhoneShow" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPhone"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divPhone" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalPicture" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPic"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divPicture" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="modal hide" id="modalDownloadPDR" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
		<div class="modal-content" style="background-color: #ffffff;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showProfilePopover()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPdRep"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="" class="">
						<iframe src="" id="ifrmPDR" width="100%" height="450px">
						</iframe>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showProfilePopover()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="myModalJobList" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 1012px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setPageFlag();">
					x
				</button>
				<h3 id="jobDetailHeaderText">
					<spring:message code="hdJDetail"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id='divJobHeaderError' class='divErrorMsg'
						style="display: block;"></div>
					<div id="divJobHeader" class='row mt10'></div>
					<div class='top15'>
						<div class="span6" id="divJobHeaderCheckBox"
							style="display: none;">
							<label class="checkbox inline">
								<input type="checkbox" id="isOverrideForAllJob"
									name="isOverrideForAllJob" />
								<spring:message code="lblOverChangForStrJob"/>
							</label>
						</div>
					</div>
					<div id="divJob" class="table-responsive"
						style="margin-bottom: 15px; min-width: 920px; overflow: hidden;">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setPageFlag();">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="modalDownloadsTranscript" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
		<div class="modal-content" style="background-color: #ffffff;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setZIndexTrans()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="HeadTran"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="" class="">
						<iframe src="" id="ifrmTrans" width="100%" height="450px">
						</iframe>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setZIndexTrans()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div  class="modal hide pdfDivBorder"  id="modalDownloadsCommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-contenat" style="background-color: #ffffff;">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabelText"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTransCommon" width="100%" height="100%" scrolling="auto">
				 </iframe>  
				<!--<embed  src="" id="ifrmTrans" width="100%" height="100%" scrolling="no"/> -->     	
			    <!--<object data="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
				    <p>Insert your error message here, if the PDF cannot be displayed.</p>
				    </object>
			    -->	  
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
 	</div>
</div>
<div class="modal hide" id="myModalTranscript" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class='modal-dialog' style="width: 936px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="defaultTeacherGrid()";>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="HeadTran"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divTranscript" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="defaultTeacherGrid()";>
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalCertification" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="defaultTeacherGrid()";>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headCertifi/Lice"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divCertification" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="defaultTeacherGrid()";>
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="left">
		<tr>
			<td style="padding-top: 270px; padding-left: 450px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px; padding-left: 450px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<tr id='paymentMessage'>
			<td style='padding-top: 0px; padding-left: 450px;' id='spnMpro'
				align='center'></td>
		</tr>
	</table>
</div>


<div class="modal hide" id="myModalMsg" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static" style="z-index: 5000;">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" id='vcloseBtnk' onclick="setZIndexJobDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='myModalMsgShow'>
				</div>
			</div>
			<div class="modal-footer">
				<span><button class="btn" data-dismiss="modal"
						aria-hidden="true" onclick="setZIndexJobDiv()">
						<spring:message code="btnClose"/>
					</button>
				</span>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalCoverLetter" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class="modal-dialog-for-cgcoverletter">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setZIndexJobDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headCoverLetr"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="">
						<span id="lblCL"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setZIndexJobDiv()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="myModalDesc" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	style="z-index: 5000;" data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="setZIndexJobDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headJobDescrip"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="">
						<span id="description"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="setZIndexJobDiv()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalCommunications" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 700px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="closeCommunication();">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headCom"/>
				</h3>
			</div>
			<div class="modal-body" style="max-height: 450px; overflow-y: auto">
				<div class="control-group">
					<div class="" id="divCommTxt">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="closeCommunication();">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<!--Start Profile Div-->
<div class="modal hide" id="myModalPhoneShowPro" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="max-width: 370px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showProfilePopover()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPhone"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="divPhoneByPro" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showProfilePopover()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalProfileVisitHistoryShow"
	tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	aria-hidden="true" data-backdrop="static">

	<div class="modal-dialog" style="width: 100px;" id="mydiv">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showProfilePopover()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTeacherProfVisitHistory"/>
				</h3>
			</div>
			<div class="modal-body"
				style="max-height: 400px; overflow-y: scroll; padding-right: 18px;">
				<div class="control-group">
					<div id="divteacherprofilevisithistory" class="">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showProfilePopover()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="myModalReferenceNoteView" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog' style="width: 520px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showProfilePopover()">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headRefrNot"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divRefNotesInner">
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showProfilePopover()">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<!-- Ref Note -->
<div class="modal hide" id="myModalReferenceNotesEditor" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog-for-cgreference">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick='closeRefNotesDivEditor();'>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headNot"/>
				</h3>
			</div>

			<div class="modal-body" style="margin: 10px;">
				<div class="row" id="divNotes" style="padding-left: 15px;">
				</div>
				<iframe id='uploadNoteFrameID_ref' name='uploadNoteFrame_ref'
					height='0' width='0' frameborder='0' scrolling='yes'
					sytle='display:none;'>
				</iframe>
				<form id='frmNoteUpload_ref' enctype='multipart/form-data'
					method='post' target='uploadNoteFrame_ref' class="form-inline"
					onsubmit="return saveReferenceNotes();"
					action='referenceNoteUploadServlet.do' accept-charset="UTF-8">
					<input type="hidden" name="eleRefId" id="eleRefId">
					<input type="hidden" id="teacherIdForNote_ref"
						name="teacherIdForNote_ref">

					<div class="row mt10">
						<div class='span10 divErrorMsg' id='errordivNotes_ref'
							style="display: block;"></div>
						<div class="span10">
							<label>
								<strong><spring:message code="lblEtrNot"/><span class="required">*</span>
								</strong>
							</label>
							<div class="span10" id='divTxtNode_ref'
								style="padding-left: 0px; margin-left: 0px;">
								<textarea readonly id="txtNotes_ref" name="txtNotes_ref"
									class="span10" rows="4"></textarea>
							</div>
							<div id='fileRefNotes' style="padding-top: 10px;">
								<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img
										src='images/attach.png' />
								</a>
								<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><spring:message code="lnkAttachFile"/></a>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick='closeRefNotesDivEditor();'>
					<spring:message code="btnClr"/>
				</button>
				&nbsp;
				<span id="spnBtnSave" style="display: inner"><button
						class="btn btn-primary" onclick="saveReferenceNotes()">
						<spring:message code="btnSave"/>
						<i class="icon"></i>
					</button>&nbsp;</span>
			</div>
		</div>
	</div>
</div>
<!-- End Ref Note -->


<!--End Profile Div-->




<div class="modal hide" id="myModalNotes" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-dialog" style="width: 673px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick='showCommunicationsDiv();'>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headNot"/>
				</h3>
			</div>

			<div class="modal-body">
				<div class="row" id="divNotes" style="padding-left: 15px;">
				</div>
				<iframe id='uploadNoteFrameID' name='uploadNoteFrame' height='0'
					width='0' frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form id='frmNoteUpload' enctype='multipart/form-data' method='post'
					target='uploadNoteFrame' class="form-inline"
					onsubmit="return saveNotes();" action='noteUploadServlet.do'
					accept-charset="UTF-8">

					<div class="mt10">
						<div class='span10 divErrorMsg' id='errordivNotes'
							style="display: block;"></div>
						<div class="span10">
							<label>
								<strong><spring:message code="lblEtrNot"/><span class="required">*</span>
								</strong>
							</label>
							<div class="span10" id='divTxtNode'
								style="padding-left: 0px; margin-left: 0px;">
								<label class="redtextmsg">
									<spring:message code="msgHowToCopyPastCutDouc"/>
								</label>
								<textarea readonly id="txtNotes" name="txtNotes" class="span10"
									rows="4"></textarea>
							</div>
							<input type="hidden" id="teacherIdForNote"
								name="teacherIdForNote" value="">
							<input type="hidden" name="noteDateTime" id="noteDateTime"
								value="${dateTime}" />
							<div id='fileNotes' style="padding-top: 8px;">
								<a href='javascript:void(0);' onclick='addNoteFileType();'><img
										src='images/attach.png' />
								</a>
								<a href='javascript:void(0);' onclick='addNoteFileType();'><spring:message code="lnkAttachFile"/></a>
							</div>
						</div>
					</div>
				</form>
			</div>

			<div class="modal-footer">
				<c:set var="chkSaveDisp" value="inline" />
				<c:choose>
					<c:when test="${userMaster.entityType eq 1}">
						<c:set var="chkSaveDisp" value="none" />
					</c:when>
					<c:otherwise>
						<c:set var="chkSaveDisp" value="inline" />
					</c:otherwise>
				</c:choose>
				<!-- <span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()"><spring:message code="btnClr"/></a></span> -->
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick='showCommunicationsDiv();'>
					<spring:message code="btnClr"/>
				</button>
				&nbsp;
				<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button
						class="btn btn-primary" onclick="saveNotes()">
						<spring:message code="btnSave"/>
						<i class="icon"></i>
					</button>&nbsp;</span>
			</div>
		</div>
	</div>
</div>
<!--Add message Div by  Sekhar  -->
<div class="modal hide" id="myMsgShow" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showMessageDiv()">
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='message2show'>
					<spring:message code="msgYuMsgIsfullySentToCandidate"/>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showMessageDiv()">
					<spring:message code="btnOk"/>
				</button>
			</div>
		</div>
	</div>
</div>



<div class="modal hide" id="tfaMsgShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'>
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='showTfaMsgBody'>
					<spring:message code="msgchangeCandidatePortfolioAndTFAStatus"/> 
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="saveTFAbyUser();"><spring:message code="btnSave"/><i class="icon"></i></button>
				<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>






<input type="hidden" id="teacherDetailId" name="teacherDetailId">
<div class="modal hide" id="myModalMessage" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">

	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-dialog" style="width: 690px">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick='showCommunicationsDivForMsg();'>
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPostMsgToCandidate"/>
				</h3>
			</div>
			<iframe id='uploadMessageFrameID' name='uploadMessageFrame'
				height='0' width='0' frameborder='0' scrolling='yes'
				sytle='display:none;'>
			</iframe>
			<form id='frmMessageUpload' enctype='multipart/form-data'
				method='post' target='uploadMessageFrame' class="form-inline"
				onsubmit="return validateMessage();"
				action='messageCGUploadServlet.do' accept-charset="UTF-8">

				<div class="modal-body-cgstatusnotes" style="max-height: 500px;">
					<div class="" id="divMessages">
					</div>
					<div class="control-group" style="margin-top: 5px">
						<div class='divErrorMsg' id='errordivMessage'
							style="display: block;"></div>
					</div>

					<%---------- Gagan :District wise Dynamic Template [Start]  --%>
					<div id="templateDiv" style="display: none;">
						<div class="row col-md-12">
							<div class="row col-sm-6" style="max-width: 300px;">
								<label>
									<strong><spring:message code="lblTemplate"/></strong>
								</label>
								<br />
								<select class="form-control" id="districttemplate"
									onchange="setTemplate()">
									<option value="0">
										<spring:message code="sltTemplate"/>
									</option>
								</select>
							</div>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<button class="btn btn-primary hide top26"
								id="btnChangetemplatelink" onclick="return getTemplate();">
								<spring:message code="btnAplyTemplate"/>
							</button>
							<input type="hidden" class="span1"
								id="confirmFlagforChangeTemplate"
								name="confirmFlagforChangeTemplate" value="0">
						</div>
					</div>
					<%---------- Gagan :District wise Dynamic Template [END]  --%>

	 			<%---------- Rahul :District wise Document [START]  --%>

                    	<div class="row col-md-12" id="documentdiv">
		    	  <div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong><spring:message code="lblDoc"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="districtDocument" onchange="showDocumentLink()">
		        		<option value=""><spring:message code="sltDoc"/></option>
		        	</select>
		        	
			
		         </div>	<br><br>
		         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		        	<a href="javascript:void(0);" id="viewDoc" class="top26 hide" onclick="return vieDistrictFile(0)"><spring:message code="lnkVFile"/></a>
		    	</div>
		    	

                    <%---------- Rahul :District wise Document [END]  --%>


					<div class="control-group row"
						style="padding-left: 15px; padding-top: 5px;">
						<label>
							<strong><spring:message code="lblTo"/><br />
							</strong><span id="emailDiv"
								style="width: 612px; word-wrap: break-word; padding-left: 1px;"></span>
					</div>
					<div id='support' class="row" style="padding-left: 15px;">
						<div class="control-group">
							<div class="">
								<label>
									<strong><spring:message code="lblSub"/></strong><span class="required">*</span>
								</label>
								<input id="messageSubject" name="messageSubject" type="text"
									class="form-control" style='width: 612px;' maxlength="100" />
							</div>
						</div>

						<div class="control-group">
							<div class="" id="messageSend" style="width: 612px;">
								<label>
									<strong><spring:message code="lblMsg"/></strong><span class="required">*</span>
								</label>
								<label class="redtextmsg">
									<spring:message code="msgHowToCopyPastCutDouc"/>
								</label>
								<textarea rows="5" class="form-control" cols="" id="msg"
									name="msg" maxlength="1000"></textarea>
							</div>
							<input type="hidden" id="teacherIdForMessage"
								name="teacherIdForMessage" value="">
							<input type="hidden" name="messageDateTime" id="messageDateTime"
								value="${dateTime}" />
							<div id='fileMessages' style="padding-top: 10px;">
								<a href='javascript:void(0);' onclick='addMessageFileType();'><img
										src='images/attach.png' />
								</a>
								<a href='javascript:void(0);' onclick='addMessageFileType();'><spring:message code="lnkAttachFile"/></a>
							</div>
						</div>
					</div>
					<div id='lodingImage'
						style="display: block; text-align: center; padding-top: 4px;"></div>
					<!-- end support div -->
				</div>

			</form>
			<c:set var="chkSendDisp" value="inline" />
			<c:choose>
				<c:when test="${userMaster.entityType eq 1}">
					<c:set var="chkSendDisp" value="none" />
				</c:when>
				<c:otherwise>
					<c:set var="chkSendDisp" value="inline" />
				</c:otherwise>
			</c:choose>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick='showCommunicationsDivForMsg();'>
					<spring:message code="btnClr"/>
				</button>
				&nbsp;
				<button class="btn btn-primary" onclick="validateMessage()"
					style="display: ${chkSendDisp}">
					<spring:message code="btnSend"/>
				</button>
				&nbsp;
			</div>
		</div>
	</div>
</div>

<%-- ---------- GAgan : Apply Template Confirm Pop up [Start] ----------------%>
<div class="modal hide" id="confirmChangeTemplate" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog-for-cgmessage">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" id=''>
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="ddd">
						<spring:message code="tmpForExistingTemplateAndMsgOverrideClickOnOk/Cancel"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-large btn-primary"
						onclick="confirmChangeTemplate()">
						<spring:message code="btnOk"/>
						<i class="icon"></i>
					</button>
				</span>
				<span><button class="btn" data-dismiss="modal"
						aria-hidden="true">
						<spring:message code="btnClr"/>
					</button>
				</span>
			</div>
		</div>
	</div>
</div>
<%------- GAgan : Apply Template Confirm Pop up [END] ---------%>



<div class="modal hide" id="myModalPhone" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog-for-teachercontented'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showCommunicationsForPhone();">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headPhone"/>
				</h3>
			</div>

			<div class="modal-body">
				<div class="control-group">
					<div id="divPhoneGrid"></div>
					<iframe id='uploadPhoneFrameID' name='uploadPhoneFrame' height='0'
						width='0' frameborder='0' scrolling='yes' sytle='display:none;'>
					</iframe>
					<form id='frmPhoneUpload' enctype='multipart/form-data'
						method='post' target='uploadPhoneFrame' class="form-inline"
						onsubmit="return savePhone();" action='phoneUploadServlet.do'
						accept-charset="UTF-8">
						<div class="mt10">
							<div class="span6 hide" id='calldetrailsdiv'>
								<div class='divErrorMsg' id='errordivPhone'
									style="display: block;"></div>
								<label>
									<strong><spring:message code="lblEtrCallDetail"/><span class="required">*</span>
									</strong>
								</label>
								<div class="span10" id='divTxtPhone'
									style="padding-left: 0px; margin-left: 0px;">
									<label class="redtextmsg">
										<spring:message code="msgHowToCopyPastCutDouc"/>
									</label>
									<textarea readonly id="divTxtPhone" name="divTxtPhone"
										class="span6" rows="4"></textarea>
								</div>
								<input type="hidden" id="teacherIdForPhone"
									name="teacherIdForPhone" value="">
								<input type="hidden" name="phoneDateTime" id="phoneDateTime"
									value="${dateTime}" />
								<div id='filePhones' style="padding-top: 8px;">
									<a href='javascript:void(0);' onclick='addPhoneFileType();'><img
											src='images/attach.png' />
									</a>
									<a href='javascript:void(0);' onclick='addPhoneFileType();'><spring:message code="lnkAttachFile"/></a>
								</div>
							</div>
						</div>
					</form>

				</div>
			</div>
			<c:set var="chkSavePhone" value="inline" />
			<c:choose>
				<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
					<c:set var="chkSavePhone" value="inline" />
				</c:when>
				<c:otherwise>
					<c:set var="chkSavePhone" value="none" />
				</c:otherwise>
			</c:choose>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showCommunicationsForPhone();">
					<spring:message code="btnClr"/>
				</button>
				<button class="btn btn-primary hide"
					style="display: ${chkSavePhone}" id='calldetrailsbtn'
					onclick="savePhone()">
					<spring:message code="btnSave"/>&nbsp;
				</button>
			</div>

		</div>
	</div>
</div>

<%--- ============================================ Gagan: Share Folder Div ======================================================== --%>
<input type="hidden" id="entityType" name="entityType"
	value="${userMaster.entityType}">

<div class="modal hide" id="shareConfirm" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					Teachermatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="saveAndShareConfirmDiv" class="">
						<spring:message code="msgYuHavaSuccssSharedCandidateToStrUser"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>
</div>

<div class="modal hide" id="deleteShareCandidate"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					Teachermatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<spring:message code="msgDeletetStrCondidate"/>

				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="deleteCandidate()">
						<spring:message code="btnOk"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>
</div>

<div class="modal hide" id="duplicatCandidate"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					Teachermatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					
<spring:message code="msgCandidateSvav/Cancel"/>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="saveWithDuplicateRecord()">
						<spring:message code="btnOk"/>
						<i class="icon"></i>
					</button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="myModalQAEXEditor" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="showPreviousModel();">
					x
				</button>
				<h3>
					<spring:message code="lblExp"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="row mt10">
					<div class="span10">
						<div id='divExplain' style="margin-top: -15px;">
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="showPreviousModel();">
					<spring:message code="btnClr"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="jobApplied" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					Teachermatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div id="jobSuccess" class="">
						<spring:message code="msgYuHaveSuccApplTheStrJob"/>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="epiAndJsi" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'style="width: 700px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					x
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headAssDetl"/>
				</h3>
			</div>
			<div class="modal-body" style="max-height: 450px;overflow-y:auto">
				<div id="epiAndJsiData"></div>
				
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>

<div class="modal hide " id="geozoneschoolDiv" tabindex="-1"
	role="dialog" data-keyboard="false" aria-labelledby="myModalLabel"
	aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 650px; margin-top: 20px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="zoneSchoolFlags();">
					x
				</button>
				<h3 id="myModalLabelGeo"></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<input type="hidden" id="geozoneId" value="0" />
					<input type="hidden" id="geozoneschoolFlag" value="0" />
					<div id="geozoneschoolGrid"></div>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""> </span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true"
					onclick="zoneSchoolFlags();">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>



<!--  -->

<div class="modal hide"  id="myModalMessageMassTP"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailIdsMassTP" name="emailIdsMassTP">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCand"/></h3>
	</div>
	<iframe id='uploadMessageFrameIDMassTP' name='uploadMessageFrameMassTP' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUploadMassTP' enctype='multipart/form-data' method='post' target='uploadMessageFrameMassTP'  class="form-inline" onsubmit="return validateMessageMassTP(1);" action='messageUploadServletMassTP.do' accept-charset="UTF-8">
	
	<div class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessagesMassTP">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessageMassTP' style="display: block;"></div>
		</div>
		<!--<div class="control-group row" style="max-width:300px;padding-left: 15px;">
			    	<label><strong><spring:message code="lblTemplate"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="districttemplate" onchange="setTemplate()">
		        		<option value="0"><spring:message code="sltTemplate"/></option>
		        	</select>
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary top26" id="btnChangetemplatelink" onclick="return getTemplate();">Apply Template</button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplate" name="confirmFlagforChangeTemplate" value="0">
		         </div>
		
		-->
		<div id="templateDivTP" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong><spring:message code="lblTemplate"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="districttemplateTP" onchange="setTemplateTP()">
		        		<option value="0"><spring:message code="sltTemplate"/></option>
		        	</select>
		         </div>	
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary hide top26" id="btnChangetemplatelinkTP" onclick="return getTemplateTP();"><spring:message code="btnAplyTemplate"/></button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplateTP" name="confirmFlagforChangeTemplateTP" value="0">
			
		    	</div>
		    </div>
		    <!-- Rahul:17/11/2014    Document Div------>
		    
		       	<div class="row col-md-12" id="documentdivTP">
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong><spring:message code="lblDoc"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="districtDocumentTP" onchange="showDocumentLinkTP()">
		        		<option value="0"><spring:message code="sltDoc"/>t</option>
		        	</select>
		         </div>	<br><br>
		           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		            <a href="javascript:void(0);" id="viewDocTP" class="top26 hide" onclick="return vieDistrictFile(1)"><spring:message code="lnkVFile"/></a>
		    	</div>
		    
		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong><spring:message code="lblTo"/><br/></strong><span id="emailDivMassTP" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="messageSubjectMassTP" name="messageSubjectMassTP" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSendMassTP" style="width: 612px;">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 				</label>
	        	<textarea rows="5" class="form-control" cols="" id="msgMassTP" name="msgMassTP" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessageMassTP" name="teacherIdForMessageMassTP" value="">
	    	<div id='fileMessagesMassTP' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'>
        		<img src='images/attach.png'/></a> 
        		<a href='javascript:void(0);' onclick='addMessageFileTypeMassTP();'><spring:message code="lnkAttachFile"/></a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessageMassTP(1)"><spring:message code="btnSend"/></button>&nbsp;
 	</div>
</div>
</div>
</div>

<input type="hidden" id="sourcefileNameMassTP" value=""/>
<input type="hidden" id="sourcefilePathMassTP" value=""/>

<!--  -->

<div  class="modal hide"  id="myModalMsgShow_SendMessageMassTP"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog' style="width:35%;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show_SendMessage'><spring:message code="msgYuMsgSuccessSentToCand"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
   </div>
  </div>
</div>

<!--  -->
<!-- add epi timer div  -->
<div class="modal hide" id="epiTimeModal" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					×
				</button>
				<h3 id="myModalLabel"><span id="teachername"></span>					
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id="assesmentRec">
					
				</div>
			</div>
			<div class="modal-footer">
			<span><button type="button" class="btn btn-large btn-primary" id="assessmentSave" onclick="return insertOrUpdateAssessmentTime();"><strong><spring:message code="btnSave"/> <i class="icon" style='margin-top:5px!important;'></i></strong></button>&nbsp;&nbsp;</span>		 		
				<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>

<!--Rahul:17/11/2014 district attachment div-->

<div  class="modal hide distAttachmentShowDiv"  id="distAttachmentShowDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11"><spring:message code="hdAttachment"/></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv1()" ><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!-- end timer div -->


<iframe src="" id="ifrmRef" width="100%" height="480px"
	style="display: none;"></iframe>
<input type="hidden" id="jobId" name="jobId" value="" />
<input type="hidden" id=phoneType name="phoneType" value="0" />
<input type="hidden" id=msgType name="msgType" value="0" />
<input type="hidden" id="noteId" name="noteId" value="" />
<input type="hidden" id="jobForTeacherGId" name="jobForTeacherGId"
	value="" />
<input type="hidden" id="commDivFlag" name="commDivFlag" value="" />


<input type="hidden" name="userFPD" id="userFPD" />
<input type="hidden" name="userFPS" id="userFPS" />
<input type="hidden" name="userCPD" id="userCPD" />
<input type="hidden" name="userCPS" id="userCPS" />
<input type="hidden" name="folderId" id="folderId" />
<input type="hidden" name="checkboxshowHideFlag"
	id="checkboxshowHideFlag" />
<input type="hidden" name="teachersharedId" id="teachersharedId" />
<input type="hidden" name="pageFlag" id="pageFlag" value="0" />
<input type="hidden"  name="currentPopUpId" id="currentPopUpId" value=""/>
<jsp:include page="statuscgupdatefrom_tp.jsp"></jsp:include>

<script type="text/javascript">
$('#internalCand').tooltip();
$('#createIcon').tooltip();
$('#renameIcon').tooltip();
$('#cutIcon').tooltip();
$('#copyIcon').tooltip();
$('#pasteIcon').tooltip();
$('#deleteIcon').tooltip();
displayTeacherGrid();
$('#myModal').modal('hide');
$(document).ready(function(){
$('#divTxtNode').find(".jqte").width(612);
$('#messageSend').find(".jqte").width(612);
$('#statusNotes').find(".jqte").width(670);
});
</script>
<c:choose>
	<c:when test="${isTeacherPoolOnLoad or entityID==1}">
		<script>
				displayTeacherGrid();
		</script>
	</c:when>
<c:otherwise>
	<script>
			$('#message2show').html("To search for Candidates, please enter the search criteria and click on \"Search\" button");
			$('#myModal2').modal('show');
	</script>      
</c:otherwise>
</c:choose>


<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("jobAppliedFromDate", "jobAppliedFromDate", "%m-%d-%Y");
     cal.manageFields("jobAppliedToDate", "jobAppliedToDate", "%m-%d-%Y");
     
    //]]>
</script>


<!--End : @ashish :: for Teacher Profile {draggableDiv} -->
<script type="text/javascript">
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
</script>
<!-- Popup window -->
<div class="modal hide"  id="chgstatusRef1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeleteVideoLnk"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="saveDataLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDataSaved"/> 
		</div>
 	</div>
 	<div class="modal-footer">
 	
 		<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button> 		
 	</div>
</div>
	</div>
</div>