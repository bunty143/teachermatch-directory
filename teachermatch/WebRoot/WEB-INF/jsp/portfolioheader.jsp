<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>


 
<% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>   
              
	 <div class="row" style="width: 100%;margin-left: 68px;">
	<div class="col-sm-2 col-md-2 stepline stepDSPQ1">
	<!--<a title='fghgfhgfhg' href='#' class='stepactive'></a><span style=''>ghgfhggfhgfg</span>-->
		<a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}">  </c:if> href="personalinfo.do"><div style="cursor:pointer;width: 100%;margin-left: -70px;" class="stepactive stepDSPQ1"></div></a><span style="margin-left: -124px;    font-weight: bold;color: #000000;    font-family: tahoma;"><strong><spring:message code="lblPersonal"/></strong></span>
	</div>	
	<div class="col-sm-2 col-md-2 stepline stepDSPQ1">
	<a id="hrefAcademics" <c:if test="${portfolioStatus.isAcademicsCompleted}">  </c:if> href="academics.do"><div style="cursor:pointer;width: 100%; margin-left: -35px;" class="stepactive stepDSPQ1"></div></a><span style="margin-left: -70px;    font-weight: bold;color: #000000;    font-family: tahoma;"><strong><spring:message code="headAcad"/></strong></span></div>
	<div class="col-sm-2 col-md-2 stepline stepDSPQ1">
	<a id="hrefCertifications" <c:if test="${portfolioStatus.isCertificationsCompleted}">  </c:if> href="certification.do"><div style="cursor:pointer;width: 100%;" class="stepactive stepDSPQ1"></div></a><span style="    font-weight: bold;color: #000000;    font-family: tahoma;"><strong><spring:message code="lblCrede"/></strong></span></div>
	<div class="col-sm-2 col-md-2 stepline stepDSPQ1">
	<a id="hrefExperiences" <c:if test="${portfolioStatus.isExperiencesCompleted}">  </c:if> href="experiences.do"><div style="cursor:pointer;width: 100%; margin-left: 50px;" class="stepactive stepDSPQ1"></div></a><span style="    margin-left: 100px;    font-weight: bold;color: #000000;    font-family: tahoma;"><strong><spring:message code="headExp"/></strong></span></div>
	<div class="col-sm-2 col-md-2 stepline stepDSPQ1" style="width: 17%;">
	<a id="hrefAffidavit" <c:if test="${portfolioStatus.isAffidavitCompleted}">  </c:if> href="affidavit.do"><div style="cursor:pointer;width: 100%;margin-left: 80px;" class="stepactive stepDSPQ1"></div></a><span style="    margin-right: -140px;    font-weight: bold;color: #000000;    font-family: tahoma;"><strong><spring:message code="headAffidavit"/></strong></span>
	</div>
  </div>

<%}else {%>
<div class="step">
	<ul>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="personalinfo.do"><span><spring:message code="lblPersonal"/></span></a></li>
		<li><a id="hrefAcademics" <c:if test="${portfolioStatus.isAcademicsCompleted}"> class="stepactive" </c:if> href="academics.do"><span><spring:message code="headAcad"/></span></a></li>
		<li><a id="hrefCertifications" <c:if test="${portfolioStatus.isCertificationsCompleted}"> class="stepactive" </c:if> href="certification.do"><span><spring:message code="lblCrede"/></span></a></li>
		<li><a id="hrefExperiences" <c:if test="${portfolioStatus.isExperiencesCompleted}"> class="stepactive" </c:if> href="experiences.do"><span><spring:message code="headExp"/></span></a></li>
		<li><a id="hrefAffidavit" <c:if test="${portfolioStatus.isAffidavitCompleted}"> class="stepactive" </c:if> href="affidavit.do"><span><spring:message code="headAffidavit"/></span></a></li>
	</ul>
	<div class="clearfix">&nbsp;</div>
</div>
						 <%} %>


