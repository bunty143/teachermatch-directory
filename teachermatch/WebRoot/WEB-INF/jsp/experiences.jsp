<jsp:include page="/portfolioheader.do"></jsp:include>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/teacher/pfExperiences.js?ver=${resourceMap['js/teacher/pfExperiences.js']}"></script>
<script type="text/javascript" src="dwr/interface/PFExperiences.js?ver=${resourceMap['PFExperiences.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
 <script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />   
 <script type="text/javascript">

var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });
        
function applyScrollOnTbl()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#employeementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 734,
        minWidth: null,
        minWidthAuto: false,
        colratio:[148,111,111,119,111,144],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
applyScrollOnTbl();
function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#involvementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 744,
        minWidth: null,
        minWidthAuto: false,
        colratio:[148,111,186,186,111], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnHonors()
{
        var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#honorsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 734,
        minWidth: null,
        minWidthAuto: false,
        colratio:[634,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
</script>
<style>
.rowmargin
{
margin-left: -30px;
}
.rowmargin1
{
margin-left: -13px;
}
</style>
<!-- add by ankit -->
<div  class="modal hide"  id="myID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
      <div class='modal-dialog'>
      <div class='modal-content'>
      <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideProfile()">x</button>
            <h3 id="myModalLabel"><spring:message code="lblMsg" /></h3>
      </div>
      <div class="modal-body">            
            
      </div>
      <div class="modal-footer">
            <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button> 
      </div>
  </div>
 </div>
</div>
<div class="row" style="margin:0px;">
         <div style="float: left;">
         		<img src="images/experiences.png">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headExp" /></div>	
         </div>		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<div class="row">
<div class="col-sm-12 col-md-12">
	<p><spring:message code="msgAtrYuUploadYurResumeCritiToPopulateAllAppliEmp" /></p>
</div>
</div>
		<div class="accordion" id="accordion2" style="min-width:900px;">		
			<%-- resume section start --%>
			<div class="accordion-group">
				<div class="accordion-heading">
                	<a href="#collapseOne" data-parent="#accordion2" id="resumeDiv" data-toggle="collapse" class="accordion-toggle minus">
                      <spring:message code="lblResume" />
                    </a>
				</div>
				<div class="accordion-body in" id="collapseOne" style="height:auto;">
                	<div class="accordion-inner">
						<div class="offset1">
                      		<div class="row">
                      			<div class="col-sm-12 col-md-12">
                      				<spring:message code="msgAbtYurResume" />
									<br><br>
									<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
									</iframe>
    								<%--<form class="form-inline"> --%>
    								<div></div>
    								<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
    								<form id='frmExpResumeUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='expResumeUploadServlet.do?f=${teacherExperience.resume}' class="form-inline">
    									<input type="hidden" id="hdnResume" value="${teacherExperience.resume}" /> 
    									<label><spring:message code="lblResume" /><span class="required">*</span></label><br/>
										<input name="resume" id="resume" type="file"><br/>
   										<!-- <input type="text" class="input-medium search-query">  <button type="submit" class="btn">Attach</button>-->
   										<div id="divResumeTxt" style="margin-top:5px;">
   											<spring:message code="lblRecentResOnFi" />
   											<label id="lblResume">
   												<c:if test="${empty teacherExperience.resume}">
   													<spring:message code="lblNone" />
   												</c:if>
   												<a href="javascript:void(0)" id="hrefResume" onclick="downloadResume();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;">${teacherExperience.resume}</a>
   											</label>
   										</div>
    									<div class="row">
    										<div class="span3"><!--a href="#" onclick="return removeResume()"> Remove</a  --></div>
    									</div>
    								</form>
                      			</div>
                      		</div>
                      	</div>
						<div class="clearfix">&nbsp;</div>
                   	</div>
				</div>
        	</div>
        	<%-- resume section start --%>
        	
        	
        	<%-- Employment section start --%>
			<div class="accordion-group">
				<div class="accordion-heading">
                	<a href="#collapseTwo" onclick="getDivNo(2)" data-parent="#accordion2" id="employmentDiv" data-toggle="collapse" class="accordion-toggle plus">
                    	<spring:message code="lblEmplHis" />
                    </a>
				</div>
				<input type="hidden" id="divNo" name="divNo" value="">
                <div class="accordion-body collapse" id="collapseTwo" style="height: 0px;" onkeypress="chkForEnterEmployment(event)">
                	<div class="accordion-inner">
                    	<div class="offset1" style="margin-left: 80px;">
	                      	<div class="row">
		                      	<div class="col-sm-10 col-md-10">
			                      	<div class="pull-right" ><a href="#" onclick="return showEmploymentForm()" ><spring:message code="lnkAddEmp" /></a>
			                      	</div>
			                     </div> 
			                     <div class="col-sm-12 col-md-12">	
				                      	<div  id="divDataEmployment" class="row mt30">
				                      	</div>	
				                 </div>    			                      	
									<div class="mt30" style="display: none" id="divEmployment">									
										<div class="col-sm-12 col-md-12 rowmargin1">	
										<spring:message code="msgEmloymentRoles" />
									    </div>  
									    <div class="col-sm-12 col-md-12 rowmargin1">
										<div class='divErrorMsg' id='errordivEmployment' style="display: block;"></div>
										</div>
											<form class="" id="frmEmployment" name="frmEmployment" onsubmit="return false;">
											<input type="hidden" id="roleId" name="roleId" >
											<div class="row">
											
											<div class="col-sm-5 col-md-5">
													<label><spring:message code="lblPosition" /></label>
													<!--<input type="text" class="span4" placeholder="">-->
													<select class="form-control" id="empPosition" name="empPosition">
														
														<option value="0"><spring:message code="optSlt" /></option>
														<option value="1"><spring:message code="lblStudentTeaching" /></option>
														<option value="2"><spring:message code="lblFullTimeTeaching" /></option>
														<option value="3"><spring:message code="lblSubstituteTeaching" /></option>
														<option value="4"><spring:message code="lblOtherWorkExperience" /></option>
																										
													</select>
												</div>
											
												<div class="col-sm-5 col-md-5">
													<label><spring:message code="lblRole" /></label>
													<input type="text" id="role" name="role" class="form-control" placeholder="" maxlength="50">
												</div>
												
												
												<div class="col-sm-5 col-md-5">
													<label><spring:message code="lblField" /></label>
													<!--<input type="text" class="span4" placeholder="">-->
													<select class="form-control" id="fieldId" name="fieldId">
														<option value="">  <spring:message code="optSlt" /> </option>													
														<c:forEach items="${lstFieldMaster}" var="fieldMaster">
															<option  value="${fieldMaster.fieldId}">${fieldMaster.fieldName}</option>
														</c:forEach>
													</select>
													
												</div>
												
												<div class="col-sm-5 col-md-5">
													<label><spring:message code="lblNameofOrganization" /></label><span class="required">*</span>
													<input type="text" id="empOrg" name="empOrg" class="form-control" placeholder="" maxlength="50">
												</div>
												
												<!--<div class="col-sm-5 col-md-5"><label>Name of Employer<span class="required">*</span></label>
												<input type="text" id="employeerName" class="form-control" name="employeerName"/>
												</div>
												
											--></div>
											<div class="row">
												<div class="col-sm-5 col-md-5">
												<label><spring:message code="lblDuration" /></label>
												
												<label class="checkbox" style="margin-top: 0px;">
												<input type="checkbox" id="currentlyWorking" name="currentlyWorking" onclick="hideToExp()"> <spring:message code="lblICurrWrkHr" />
												</label>
												</div>
												<!--<div class="col-sm-5 col-md-5">
													<label>Name of Organization</label>
													<input type="text" id="empOrg" name="empOrg" class="form-control" placeholder="" maxlength="50">
												</div>-->
											</div>
										
											<div class="row">
								                       <div class="col-sm-5 col-md-5"><label><spring:message code="lblCity" /><span class="required">*</span></label>
									                        <input type="text" id="cityEmp" class="form-control" name="cityEmp"/>
								                       </div>
								                      
								                       <div class="col-sm-5 col-md-5"><label><spring:message code="lblStateOfOrganization" /><span class="required">*</span></label>
									                         <input type="text" id="stateOfOrg" class="form-control" name="stateOfOrg"/>
								                       </div>
						                  </div>								
											<div class="row">
												<div class="col-sm-3 col-md-3"><label><spring:message code="lblFrm" /></label>
													<select id="roleStartMonth" name="roleStartMonth" class="form-control">
														<option value="0"><spring:message code="optStrMonth" /></option>
														<c:forEach items="${lstMonth}" var="month" varStatus="status">
															<option value="${status.count}">${month}</option>
														</c:forEach>
														
													</select>
												</div>
												<div class="col-sm-2 col-md-2">
													<label>&nbsp;</label>
													<select id="roleStartYear" name="roleStartYear" class="form-control">
														<option value="0" ><spring:message code="optYear" /></option>														
														<c:forEach items="${lstYear}" var="year" >
															<option  value="${year}">${year}</option>
														</c:forEach>
													</select>
												</div>
												<div id="divToMonth" class="col-sm-3 col-md-3"><label><spring:message code="lblTo" /></label>
													<select class="form-control" id="roleEndMonth" name="roleEndMonth">
														<option value="0"><spring:message code="optStrMonth" /></option>
														<c:forEach items="${lstMonth}" var="month"  varStatus="status">
															<option value="${status.count}">${month}</option>
														</c:forEach>
													</select>
												</div>
												<div id="divToYear" class="col-sm-2 col-md-2">
													<label>&nbsp;</label>
													<select id="roleEndYear" name="roleEndYear" class="form-control">
														<option value="0"><spring:message code="optYear" /></option>
														<c:forEach items="${lstYear}" var="year">
															<option value="${year}">${year}</option>
														</c:forEach>
													</select>
												</div>
											</div>
											
											<div class="row">
												<div id="divToMonth" class="col-sm-3 col-md-3"><label><spring:message code="lblAnnualSlryAmount" /></label>
											    	<input type="text" id="amount" name="amount" class="form-control"   onkeypress="return checkForInt(event);" maxlength="8">
												</div>
											
											</div>
											
											<div class="row">
												<div class="col-sm-3 col-md-3"><label><spring:message code="lblTypOfRole" /></label>
												</div>
											</div>								
											<div class="row">
											     <%int i=0;%>
											    	 <c:forEach items="${lstEmpRoleTypeMaster}" var="empRoleType">
											    	 <%i++;%>
											    		
											    		 <% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){ %> 
											    		      <% if(i==1){%>
											    	          
											    	           <div class="col-sm-2 col-md-2" >	
											    	             <label class="radio inline" style="margin-top: 0px;">
													    	       <input type="radio"  value="${empRoleType.empRoleTypeId}"  name="empRoleTypeId" name="optionsRadios"> <span> ${empRoleType.empRoleTypeName} </span>
													             </label>
													            </div>
													            <%}%>
													        
													            
											    		          <% if(i==2){%>
											    	          
											    	           <div class="col-sm-2 col-md-2" >	
											    	              <label class="radio inline" style="margin-top: 0px; margin-right: -25px;">
													    	        <input type="radio"  value="${empRoleType.empRoleTypeId}"  name="empRoleTypeId" name="optionsRadios"> <span> ${empRoleType.empRoleTypeName} </span>
													              </label>
													            </div>
													            <%}%>
													        
													            <% if(i==3){%>
											    	          
											    	           <div class="col-sm-2 col-md-2" >	
											    	             <label class="radio inline" style="margin-top: 0px; margin-right: -15px;">
													    	       <input type="radio"  value="${empRoleType.empRoleTypeId}"  name="empRoleTypeId" name="optionsRadios"> <span> ${empRoleType.empRoleTypeName} </span>
													             </label>
													            </div>
													            <%}%>
													        
											    	     <%}else{%>	
											    	
											    	    <div class="col-sm-2 col-md-2" style=" margin-right: -0px;">	
											    	      							
														<label class="radio inline" style="margin-top: 0px;">
													    	<input type="radio"  value="${empRoleType.empRoleTypeId}"  name="empRoleTypeId" name="optionsRadios"> <span> ${empRoleType.empRoleTypeName} </span>
													    </label>
													    </div>
													    <%} %>
													</c:forEach>											    	
											   
											</div>								
											<div class="row"><div class="col-sm-8 col-md-8"><label><spring:message code="lblRespondibinThRole" /></label></div>
											    <div class="col-sm-8 col-md-8" id='primaryRespdiv'>
											    <label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" />
 												</label>
											    <textarea id="primaryResp" name="primaryResp" class="from-control" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea>
											    </div>
											</div>
											<div class="row">
												<div class="col-sm-8 col-md-8"><br><label><spring:message code="lblSignContriInThisRole" /></label></div>
											    <div class="col-sm-8 col-md-8" id='mostSignContdiv'>
											    <label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" />
 												</label>
											    <textarea class="from-control" id="mostSignCont" name="mostSignCont" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea>
											    </div>
											</div>
											
											<div class="row">
												<div class="col-sm-8 col-md-8"><br><label><spring:message code="lblReasonForLeaving" /></label></div>
											    <div class="col-sm-8 col-md-8" id='reasonForLeadiv'>
											    <label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" />
 												</label>
											    <textarea class="from-control" id="reasonForLea" name="reasonForLea" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea>
											    </div>
											</div>
											
											    <div class="row">
											    <div class="col-sm-8 col-md-8 idone">
											    	<a href="#" onclick="return insertOrUpdateEmployment()"><spring:message code="lnkImD" /></a>&nbsp;
											    	<a href="#"	onclick="return hideEmploymentForm()">
														<spring:message code="lnkCancel" />
													</a>
											    </div>
											</div>							
										</form>
									</div>
								</div>
							</div>
						</div>
                      	<div class="clearfix">&nbsp;</div>
					</div>
				</div>			
			<%-- Employment section End --%>
			
			<%-- Involvment section Start --%>				
				<div class="accordion-group" >
					<div class="accordion-heading">
						<a href="#collapseThree" onclick="getDivNo(3)" data-parent="#accordion2" id="involvementDiv" data-toggle="collapse" class="accordion-toggle plus">
					    	<spring:message code="lnkInvol/VolunWork/StuTch" /> 
					  	</a>
					</div>
                  	<div class="accordion-body collapse" id="collapseThree" style="height: 0px;" onkeypress="chkForEnterInvolvement(event)">
                    	<div class="accordion-inner">
	                      	<div class="offset1" style="margin-left: 90px;">
	                      		<div class="row">
	                      			    <div class="col-sm-10 col-md-10">	                      			
	                      				<div class="pull-right"><a href="#" onclick="return showInvolvementForm()"><spring:message code="lnkAddInvol" /></a>
	                      				</div>
	                      				</div>
	                      				 
	                      				<div id="divDataInvolvement" class="row mt30">												
										</div>
								</div>		
										<div class="mt30" style="display: none" id="divInvolvement">
										<div class="row">
										<div class="col-sm-10 col-md-10 rowmargin">
											<p><spring:message code="pListOrganiz" /></p>
										</div>										
										<div class="col-sm-10 col-md-10 rowmargin">
										<div class='divErrorMsg' id='errordivInvolvement' style="display: block;"></div>
										</div>
										</div>
										<form id="frmInvolvement" name="frmInvolvement">
											<input type="hidden" id="involvementId"name="involvementId"	/>
											<div class="row" style="margin-left: -45px;">												
												<div class="col-sm-3 col-md-3"><label><spring:message code="lblOrga" /></label>
													<input type="text" id="organization" name="organization" maxlength="50" class="form-control" >
												</div>
												
												<div class="col-sm-3 col-md-3"><label><spring:message code="lblTyOfOrga" /></label>
													<select class="form-control " id="orgTypeId" name="orgTypeId">
														<option value=""><spring:message code="optSlt" /></option>
														<c:forEach items="${lstOrgTypeMasters}" var="orgType">
															<option value="${orgType.orgTypeId}">${orgType.orgType}</option>
														</c:forEach>
													</select>
												</div>
																							
												<div class="col-sm-4 col-md-4"><label><spring:message code="lblNumOfPeopleInOrga" /></label>
													<select class="form-control " id="rangeId" name="rangeId">
														<option value=""><spring:message code="optSlt" /></option>
														<c:forEach items="${lstPeopleRangeMasters}" var="peopleRange">
															<option value="${peopleRange.rangeId}">${peopleRange.range}</option>
														</c:forEach>
													</select>
												</div>
												
											</div>	
											<div class="row top5" style="margin-left: -45px;">
											        <div class="col-sm-4 col-md-4">
											         <label style="margin-right: -21px;"><spring:message code="lblLeadPeopleInThisOrga" /></label>
											       													    
												     <div class="" id="" style="height: 40px;">
												    	<div class="radio inline col-sm-1 col-md-1" style="margin-top:0px;">
													    	<input type="radio"  id="rdo1" value="option1"  name="optionsRadios" onclick="showAndHideLeadNoOfPeople('1')"><spring:message code="optY" />
													    </div>
													    </br>
													    <div class="radio inline col-sm-1 col-md-1" style="margin-left:20px;margin-top:-20px;">
													    	<input type="radio" checked="t" value="option1"  name="optionsRadios" onclick="showAndHideLeadNoOfPeople('0')">  <spring:message code="optN" />
													    </div>
												     </div>								    
											          </div>
											    <div class="col-sm-2 col-md-2" id="divLeadNoOfPeople" name="divLeadNoOfPeople" style="display: none;"><label><spring:message code="lblHowMnyPeople" /></label>
													<input type="text" id="leadNoOfPeople" name="leadNoOfPeople" class="form-control" onkeypress="return checkForInt(event);" maxlength="4"/>
												</div>
										    </div>	
										    <div class="row" style="margin-left: -45px;">	
											    <div class="col-sm-6 col-md-6 idone">
											    	<a href="#" onclick="return saveOrUpdateInvolvement()"><spring:message code="lnkImD" /></a>&nbsp;
											    	<a href="#"	onclick="return hideInvolvement()">
														<spring:message code="lnkCancel" />
													</a>
											    </div>
											</div>	
										</form>
										</div>
	                      		
								
							</div>
	                      	<div class="clearfix">&nbsp;</div>
                    		</div>
          				</div>
        			</div>
        		<%-- Involvment section End --%>
        		
        		<%-- Honers section Start--%>
        		
				<div class="accordion-group" >
                	<div class="accordion-heading">
	                    <a href="#collapseFour" data-parent="#accordion2" id="honorDiv" data-toggle="collapse" class="accordion-toggle plus">
	                     <spring:message code="lblHonors" /> <i class="plus pull-right"></i>
	                    </a>
                  </div>
                  <div class="accordion-body collapse" id="collapseFour" style="height: 0px;" onkeypress="chkForEnterAward(event)">
						<div class="accordion-inner">
                      		<div class="offset1" >
							
    							
    									<div class="row">
    									<div class="col-sm-10 col-md-10">
									    	<div class="span4 pull-right add-employment"><a href="#" onclick="return showHonorForm()"><spring:message code="lnkAddHonors" /></a></div>
									    </div>
									    </div>
									    	
									    			    	
									    <div id="divDataHoner">
									    	
									    </div>
									    <form id="frmHonor" name="frmHonor" onsubmit="return false;">
									    <input type="hidden" id="honorId" name="honorId"/>
									    <div  id="divHonor" style="display: none;">
										    <br>
										    <div class="row col-sm-10 col-md-10 rowmargin">
										    <div class='divErrorMsg' id='errordivHonor'  style="display: block;"></div>
										    </div>	
										    
										    <div class="row ">								    
										    <div class="col-sm-4 col-md-4 rowmargin1"><label><spring:message code="lblAward" /></label>
										    	<input type="text" id="honor" name="honor" class="form-control" placeholder="" maxlength="50">
										    </div>
										    <div class="col-sm-2 col-md-2"><label>&nbsp;</label>
												<select id="honorYear" name="honorYear" class="form-control">
													<option value="0" ><spring:message code="optYear" /></option>														
													<c:forEach items="${lstYear}" var="year">
														<option>${year}</option>
													</c:forEach>
												</select>
											</div>
											</div>								
											<div class="row col-sm-4 col-md-4 idone top10 rowmargin">
												<a href="#" onclick="return saveOrUpdateHonors()"><spring:message code="lnkImD" /></a>
												&nbsp;<a href="#"	onclick="return hideHonor()">
													<spring:message code="lnkCancel" />
												</a>
											</div>
									    </div>
									    
									    </form>
									
    							
                      		</div>
                    		<div class="clearfix">&nbsp;</div>
                  		</div>
                  	</div>
        		</div>
        		<div class="mt30"><button class="btn btn-large btn-primary"  onclick="saveAndContinueExperiences()"><spring:message code="btnSv&Conti" /> <i class="icon"></i></button><br><br></div>
			</div>		
	
	
	<iframe src="" id="ifrmResume" width="100%" height="480px" style="display: none;">
 	</iframe>  
<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'><spring:message code="msgYrFileBegUpLod" /></td></tr>
	</table>
</div>

<div  class="modal hide"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='blockMessage'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button>
 	</div>
</div>
</div>
</div>

<script type="text/javascript">
$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});
</script>
<script type="text/javascript">
$('#iconpophover1').tooltip();

getPFEmploymentGrid();
getInvolvementGrid();
getHonorsGrid();
$('#myModal').modal('hide');
</script>

<script>
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#myModalDymanicPortfolio').modal('hide');
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>