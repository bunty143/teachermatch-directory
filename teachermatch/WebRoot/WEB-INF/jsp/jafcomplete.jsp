<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
        $j(document).ready(function() {
       });
</script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/group01.js?ver=${resouceMap['js/jobapplicationflow/group01.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jobapplflowcommon.js?ver=${resouceMap['js/jobapplicationflow/jobapplflowcommon.js']}"></script>

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />


<p id="portfolioHeader"></p>
<input type="hidden" id="sNextURL" name="sNextURL" value="${sNextURL}"/>
<input type="hidden" id="sPreviousURL" name="sPreviousURL" value="${sPreviousURL}"/>
<input type="hidden" id="sCurrentPageShortCode" name="sCurrentPageShortCode" value="${sCurrentPageShortCode}"/>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:30px;" id="comptopheaderDivId">
         <div style="float: left;">
         	<img src="images/academics.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="DSPQlblCompleted"/></div>	
         </div>
         
         <div style="float: right;" id="toprightcompbtn">
          	<div style='text-align:right; padding-right: 0px;' class="col-sm-3 col-md-3"><button class='flatbtn' style='width:130px;'  type='button' onclick='return validateJAFCompleteProcess();'><spring:message code="btnContinueDSPQ"/> <i class='icon'></i></button></div>
         </div>
         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<p id="completedDivId"></p>

<!-- <div class="row">
	<div class="col-sm-12 col-md-12 mt10" id="completedDivId" style="height: 250px;"></div>
</div>
 -->
 										
<!-- start ... Attach jaf jsp files -->
	<jsp:include page="jafinnercommon.jsp"></jsp:include>
<!-- end ... Attach jaf jsp files -->


<script type="text/javascript">//<![CDATA[
 var cal = Calendar.setup({
  onSelect: function(cal) { cal.hide() },
  showTime: true
 });
 //]]>
    
</script>

<script>

var dspqPortfolioNameId=${dspqPortfolioName};
var candidateType="${candidateType}";
var groupId=${group};
var iJobId=${iJobId};

getPortfolioHeader(iJobId,dspqPortfolioNameId,candidateType,groupId);
//getPreScreenData(iJobId,dspqPortfolioNameId,candidateType,groupId);
//getPreScreenFooter(iJobId,dspqPortfolioNameId,candidateType,groupId);

getJAFCompleteProcess(iJobId,dspqPortfolioNameId,candidateType,groupId);

</script>

<style>
.notClk
{
	/*pointer-events:none;*/
}
</style>

<script>
$( document ).ready(function(){


/*
$(".stepDSPQ1").addClass("notClk");
$(".notClk").click(function() {
  alert("You are already completed this job.");
  return false;
});
*/
//setTimeout("jafCompleteRedirection()", 5000);


});



</script>