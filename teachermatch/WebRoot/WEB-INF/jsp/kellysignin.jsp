<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js?ver=${resourceMap['']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js?ver=${resourceMap['']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BranchesAjax.js?ver=${resourceMap['BranchesAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/KesSignupAjax.js?ver=${resourceMap['KesSignupAjax.Ajax']}"></script>
<script type='text/javascript' src="js/smartpractices.js?ver=${resourceMap['js/smartpractices.js']}"></script>
<script type='text/javascript' src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}"></script>
<script type="text/javascript" src="js/kellysignin.js?ver=${resourceMap['js/kellysignin.js']}"></script>
<link rel="stylesheet" href="css/dialogbox.css" />
<style>
.popover-inner {
padding: 3px;
overflow: hidden;
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0,0,0,0.3);
color:black;
}

/*.popover.left .arrow {
top: 50%;
right: 0;
margin-top: -5px;
border-top: 5px solid transparent;
border-bottom: 5px solid transparent;
border-left: 5px solid #000;
}
*/

.popover.left .arrow
{
	width: 20px;
	height: 20px;
	border: none;
	transform: rotate(45deg);
	position: absolute;
	top: 98px;
	left: 263px;
	background-color: white;
	box-shadow: 0 9px 0 0px white, -9px 0 0 0px white, 1px -1px 1px #818181;
}

.popover .arrow {
position: absolute;
width: 0;
height: 0;
}
element.style {
top: 231.5px;
left: 155px;
display: block;
}
.popover.left {
margin-left: -5px;
}
.fade.in {
opacity: 1;
}
.popover {
position: absolute;
top: 0;
left: 0;
z-index: 1010;
display: none;
}
.fade {
opacity: 0;
-webkit-transition: opacity 0.15s linear;
-moz-transition: opacity 0.15s linear;
-ms-transition: opacity 0.15s linear;
-o-transition: opacity 0.15s linear;
transition: opacity 0.15s linear;
}
</style>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
$(document).ready(function(){

	$('.popover').hide();
	if($('#environment').val()=='QAEnvironment')
	{
		$('#captchaMsgId').hide();
		$('#captchaContainerId').hide();
	}
	$("#password4signup").focus(function(){
		$('.popover').show();
	});
	
	$("#fname").focus(function(){ $('.popover').hide(); });
	$("#lname").focus(function(){ $('.popover').hide(); });
	$("#email4signup").focus(function(){ $('.popover').hide(); });
	$("#branchName").focus(function(){ $('.popover').hide(); });
	$("#sumOfCaptchaText").focus(function(){ $('.popover').hide(); });
	/*$("#formBody").click(function(){ $('.popover').hide(); });*/
	
	$( "#seemore" ).click(function() {
		//$("#password").focus();
		$('.password-guidelines').show();
		$('#seemore').hide();
		$('#seeless').show();
	});
	
	$( "#seeless" ).click(function() {
		//$("#password").focus();
		//$('.popover').show();
		$('.password-guidelines').hide();
		$('#seemore').show();
		$('#seeless').hide();
	});
});

</script>
<script type='text/javascript'>
    var captchaContainer = null;
    var loadCaptcha = function() {
      captchaContainer = grecaptcha.render('captcha_container', {
        'sitekey' : '6LeMmwATAAAAAGB1iwuORAfQ8z9ZvJAT2Lhad4AT',
        'callback' : function(response) {
          if(response !=null)
          {
          	document.getElementById('captcharesponse').value="success";
          }
          else
          {
          	document.getElementById('captcharesponse').value="failed";
          }
        }
      });
    };
</script>

<!-- This script is used to identify that, Is current login user is type 3 and if user is type 3
	then also identify that Is he belongs to multiple schools? If yes then open popup -->
<script  type="text/javascript">
function applyScrollOnBranchLookup()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#branchLookupTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 630,
        minWidth: null,
        minWidthAuto: false,
        colratio:[230,150,150,70], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function validateUser()
{
	var flag = loginUserValidateForKelly();
  	if(flag==true)
  	{
  		$.ajax(
  		{
  			url: "isSA.do",
  			type: "POST",
  			async: false,
  			data:
  			{
  				emailAddress: $("#emailAddress1").val(),
  				password: $("#password1").val(),
  			},
  			success:function(result)
  			{
  				var myresult = jQuery.parseJSON(''+result+'');
  				//alert("myresult.isType3User:- "+myresult.isType3User+"\nmyresult.isAuthorizedUser:- "+myresult.isAuthorizedUser+"\myresult.noOfSchools:- "+myresult.noOfSchools+"\nmyresult.noOfSchools>1:- "+(myresult.noOfSchools>1));
  				if(myresult.isType3User=="true" && myresult.isAuthorizedUser=="true" && myresult.noOfSchools>1)
  				{
  					//$("#selectSchoolName").focus();
  					
  					$('#selectSchoolName').find('option').remove();//remove the previous element.
  					$("#modalErrorMsg").text("");//remove the error message.
  					$('#selectSchoolName').append("<option value='none'><spring:message code="msgStSchool"/></option>");
  					$.each(myresult.schoolsDetails, function(index, data)
  					{
  						$('#selectSchoolName').append("<option value="+data.schoolId+">"+data.schoolName+"</option>");
  					});
  					$("#selectSchoolDiv").modal("show");
  					
  				}
  				else if(myresult.isType3User=="false" && myresult.isAuthorizedUser=="true" && myresult.noOfBranches>1)
  				{
  					$('#branchNameSel').find('option').remove();//remove the previous element.
  					$("#modalBranchErrorMsg").text("");//remove the error message.
  					$('#branchNameSel').append("<option value='none'>Select Branch</option>");
  					$.each(myresult.branchDetails, function(index, data)
  					{
  						$('#branchNameSel').append("<option value="+data.branchId+">"+data.branchName+"</option>");
  					});
  					$("#selectBranchDiv").modal("show");
  					
  				}
  				else
  				{
  					try
  					{
  					if(myresult.noOfBranches!="" && myresult.noOfBranches==1)
  					{
  						$.each(myresult.branchDetails, function(index, data)
  						{
  							$('#branchNameSel').append("<option value="+data.branchId+" selected>"+data.branchName+"</option>");
  							$("#branchID").val(data.branchId);
  							$("#brName").val(data.branchName);
  						});
  					}
  					else
  					if(myresult.noOfSchools!="" && myresult.noOfSchools==1)
  					{
  						$.each(myresult.schoolsDetails, function(index, data)
  						{
  							$("#schoolID").val(data.schoolId);
  							$("#schoolName").val(data.schoolName);
  						});
  					}
  					}
  					catch(err){}
  					
  					$("#signinForm").submit();
  				}
  			},
  			error:function(jqXHR, textStatus, errorThrown)
  			{
  				alert("server error status:- "+textStatus+", error Thrown:- "+errorThrown+", error jqXHR:- "+jqXHR);
  			}
  		});
  	}
  	
  	return false;
}
/*When user click on ok then send the schoolName and school Id to the server*/
function ok()
{
	var schoolID = $("#selectSchoolName").val();
	var schoolName = $("#selectSchoolName option:selected").text();
	
	if(schoolID=="none")
		$("#modalErrorMsg").text("Please select school for login.");
	else
	{
		$("#selectSchoolDiv").modal("hide");	
		$("#schoolName").val(schoolName);
		$("#schoolID").val(schoolID);
		$("#signinForm").submit();
	}
}
function cont()
{
	var branchID = $("#branchNameSel").val();
	var branchName = $("#branchNameSel option:selected").text();
	if(branchID=="none")
		$("#modalBranchErrorMsg").text("Please select branch for login.");
	else
	{
		$("#selectBranchDiv").modal("hide");	
		$("#brName").val(branchName);
		$("#branchID").val(branchID);
		$("#signinForm").submit();
	}
}

function cancel()
{
	$("#selectSchoolDiv").modal("hide");
	$("#emailAddress").val("");
	$("#password").val("");
	$("#emailAddress").focus();
}
</script>


<input type="hidden" value="<%=request.getContextPath()%>" name="contextPath" />
<input type="hidden" id="headQuarterId" value="1"/>

<div class="row">
	<div class="col-sm-5 col-md-5" style="border-color: #0e78a0; border-width: 2px; border-style: solid; min-height: 554px; border-radius: 15px;">
		<div class="row top10">
			<div class="col-sm-12 col-md-12">
 				<div class="subheadinglogin">KES Staff and Existing Substitute Talent</div>
			</div>
		</div>
		<div class="tabletbody"> 
	<!-- <form action="signin.do" method="post" onsubmit="return loginUserValidate();"> -->
			<form action="signin.do" method="post" id="signinForm" >
				<c:set var="test" value="${fn:escapeXml(param.key)}"/>
				<c:choose>
      				<c:when test="${fn:contains(test,'script')}">     
      					<input type="hidden" id="val" name="val" value="1">
      					<input type="hidden" id="key" name="key" value="">
      				</c:when>
      				<c:otherwise>
	      				<input type="hidden" id="key" name="key" value="${fn:escapeXml(param.key)}">
      				</c:otherwise>
				</c:choose>
				<c:set var="test" value="${fn:escapeXml(param.id)}"/>
				<c:choose>
      				<c:when test="${fn:contains(test,'script')}">
        				<input type="hidden" id="val" name="val" value="1">
      					<input type="hidden" id="id" name="id" value="">
      				</c:when>
      				<c:otherwise>
         				<input type="hidden" id="id" name="id" value="${fn:escapeXml(param.id)}">
      				</c:otherwise>
				</c:choose>
				
				<input type="hidden" id="schoolName" name="schoolName" value="">
				<input type="hidden" id="schoolID" name="schoolID" value="">


				<input type="hidden" id="brName" name="brName" value="">
				<input type="hidden" id="branchID" name="branchID" value="">
				
				<div class="row">  
   					<div class="col-sm-12 col-md-12">			                         
						<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
						<div class='divErrorMsg' id='divServerError' style="display: block;">${msgError}</div>			             
				   	</div>  
				</div>
                <div class="row top15">
                    <div class="col-sm-12 col-md-12">
                       	<label>Email</label>
                       	<input type="text" name="emailAddress1" value="${emailAddress}" id="emailAddress1" placeholder="Enter your Email" class="form-control"  /> 
					</div>  
				</div>
				<div class="row">                
					<div class="col-sm-12 col-md-12">
						<label>Password</label>
						<input type="password" name="password1" id="password1" placeholder="Enter your password" class="form-control" maxlength="12" />
					</div>
				</div>
                <div class="row top20">
                	<div class="col-sm-3 col-md-3" style="width:30%;padding-right: 0px;" >
	                	<div style="float: left;">
	                    	<label class="checkbox">
								<input type="checkbox" id="txtRememberme" name="txtRememberme"> Remember me
							</label>								                                                             
						</div>                               
					</div>
					<div class="col-sm-1 top5" style="width:1%;padding-right: 0px;padding-left: 0px;font-size: 20px;">
	                	<span><b>.</b></span>             
					</div>
					<div class="col-sm-4 col-md-4 top10" style="padding-left: 5px;">
                    	<a href="forgotpassword.do">Forgot Password?</a>                   
					</div>
                	<div class="col-sm-4 col-md-4">
						<button class="btn fl btn-primary" style="padding: 6px 17px;"  type="submit"
                                id="submitLogin" onclick="return validateUser();">Login <i class="icon"></i>
						</button>
					</div>
				</div>  
                <div class="row">
                	<div class="col-sm-12 col-md-12 top30">
                		By logging into this site, you agree to TeacherMatch's <a href="termsofuse.do">Terms of Use</a>        
					</div>
				</div>
				<div class="row top5">
				 	<div class="col-sm-12 col-md-offset-12 systemsetup">						     
						<a href="systemsetup.do" target="_blank">&nbsp;</a> <button  class="btn fl btn-secondary" type="button" onclick="return testYourSetUp();"><spring:message code="btnTestSetup"/> <i class="icon2"></i></button>						   
					</div>
				</div>
				<input type="hidden" id="talentType" name="talentType" value="${refType}"/> 
				<input  type="hidden" id="jobId" name="jobId" value="${kellyJobId}">
				<input  type="hidden" id="teacherId" name="teacherId" value="${teacherId}">
			</form>
			<c:if test="${teacherblockflag eq 1}">
					<div class="modal" id="teacherblockdiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >		
					 <div class="modal-dialog">
			          <div class="modal-content">
						<div class="modal-header">	  		
							<h3 id="myModalLabel">TeacherMatch</h3>
						</div>
						<div class="modal-body">				
							<div class="control-group">
								<div class="" id="ddd">
									<p>Due to three unsuccessful attempts, this account has been temporarily blocked for security reasons. Please click on "I forgot" link on Login Page to reset your password.</p>
								</div>
							</div>
					 	</div> 	
					 	<div class="modal-footer">
					 			<span><button class="btn btn-large btn-primary" onclick="hideDive()">Ok <i class="icon"></i></button></span>
					 	</div>
					</div>
				  </div>
				</div>		
			</c:if>
		</div>	
	</div>
	<div class="co2-sm-1 col-md-1"></div>
	<div class="col-sm-6 col-md-6" style="border-color: #0e78a0; border-width: 2px; border-style: solid; border-radius: 15px; padding-bottom: 10px; min-height: 554px;">
		<div class="row top10">
			<div class="col-sm-12 col-md-12">
 				<div class="subheadinglogin ">New Substitute Talent</div>
			</div>
		</div>
		<form id="signupForm" autocomplete="off">
			<div class="row">
   				<div class="col-sm-12 col-md-12">
						<div id="errorDiv4Signup" class='divErrorMsg' style="display: none;"></div>
				</div>
			</div>
			<input type="hidden"  name="eventId"  id="eventId" value="${eventDetails.eventId}"/>
			<div class="row top15">                                  
				<div class="col-sm-6 col-md-6">
                	<label>First Name<span class="required">*</span></label>
                    <input name="fname" id="fname" class="form-control" maxlength="40"/>
				</div>
				<div class="col-sm-6 col-md-6">
					<label>Last Name<span class="required">*</span></label>
					<input name="lname" id="lname" class="form-control" maxlength="40"/>
				</div>
			</div>                                   
			<div class="row">             
				<div class="col-sm-12 col-md-12">
					<label>Email<span class="required">*</span></label>
					<input name="email4signup" id="email4signup" class="form-control" maxlength="75" />
				</div>
			</div>                                   
			<div class="row">                
				<div class="col-sm-12 col-md-12">
					<label>Password<span class="required">*</span></label>
					<input type="password" id="password4signup" name="password4signup"  class="form-control"   maxlength="12"  onkeyup="return passwordInfo();"/>
				</div>
				<!-- *********div for passowrd****** -->
				<div  class="col-sm-8 col-md-8 top10" style="position: relative;z-index: 10">
					<div class="popover fade left in" style="left: -275px; top: -123.5px; display: block;">
						<div class="arrow"></div>
						<div class="popover-inner">
							<div class="popover-title" style="background-color: #007AB4;color:white;">Password Requirements</div>
							<div class="popover-content"></div>
							<div id="password-rules">
								<ol >
									<li>At least 8 Characters</li>
									<li>Include at least 2 of the 3 elements:
                            			<ul type="disc">
	                            			<li>One Character</li>
											<li>One Digit</li>
											<li>One Special Symbol</li>
										</ul>
									</li>
									<li>At most 12 Characters.</li>
								</ol>
							</div>
							<div>
								<a href="javascript:void(0);" role="see-more" id="seemore" style="padding-left: 12px;">more info</a>
								<a href="javascript:void(0);" role="see-more" id="seeless" style="color: red; padding-left: 12px; display: none;">less info</a>
							</div>
							<div class="password-guidelines hide" style="padding: 12px 15px; text-align: justify;">
								<span><b>Password guidelines:</b></span>
								<ul>
									<li>Make your password significantly different each time and make it hard to guess. Don't include personal information or your user name in your password.</li>
									<li>Avoid using repeating or sequential characters in your password, e.g. teachermatch1234.</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			<!-- *********End of div for passowrd****** -->
			</div>
			<c:set var="editable" value=""></c:set>
			<c:if test="${not empty branchName && branchName ne '0'}">
			<c:set var="editable" value="readonly"></c:set>
			</c:if>
			<div class="row">                
				<div class="col-sm-12 col-md-12">
					<label>Branch Name<span class="required">*</span></label><a style="margin-left: 10px;" href="javascript:void:(0);" onclick="showBranchLookup()"><b></>Branch Lookup</b></a>
			        <input type="text" id="branchName" name="branchName"  class="form-control"
			       		   onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						   onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						   onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');" value="${branchName}" ${editable}/>
					<input  type="hidden" id="branchId" name="branchId" value="${branchId}" />
					<div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>
				</div>
			</div>
			<div class="row">            
			
				<div class="col-sm-12 col-md-12 top10">
					<label style="margin-right: 40%;">
						<strong>Password Strength</strong>
					</label>
				</div>
				<div class="col-sm-2 col-md-2 top10">
                	<table cellpadding="0" cellspacing="0" border="0">
						<tr>
							<td colspan="2" align="left" valign="top" nowrap>
								<table cellpadding="0" cellspacing="0">
									<tr>
										<td>
											<div style="height: 12px; width: 14px;  float: left;  border: thin solid #bcb4a4;">							
												<div id="d4" style="height: 12px; width: 14px; float: left; display: none;">&nbsp;</div>					
											</div>
										</td>
										<td style="padding-left:10px;">
											<div style="height: 12px; width: 14px;   float: left; border: thin solid #bcb4a4; margin-left: 5%;">
												<div id="d5" style="height: 12px; width: 14px; float: left; display: none;">&nbsp;</div>
											</div>
										</td>
										<td style="padding-left:10px;">
											<div style="height: 12px; width: 14px; float: left; border:  thin solid #bcb4a4; margin-left: 5%;">
												<div id="d6" style="height: 12px; width: 14px; float: left; background-color: lightgreen; display: none;">&nbsp;</div>
											</div>
										</td>
										<td style="padding-left:10px;">
											<div style="height: 12px; width: 14px; float: left; border:  thin solid #bcb4a4; margin-left: 5%;">
												<div id="d7" style="height: 12px; width: 14px; float: left; background-color: lightgreen; display: none;">&nbsp;</div>
											</div>
										</td>
										<td>
											<table cellpadding="0" cellspacing="0">
												<tr>
													<td>
														<div id="d8" style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
															<span >Weak</span>
														</div>
													</td>
													<td>
														<div id="d9" style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
															<span >Normal</span>
														</div>
													</td>
													<td>
														<div id="d10" style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
															<span >Strong</span>
														</div>
													</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
			</div>
			<input type="hidden" name="environment" id="environment" value="${environment}"/>
			<div class="col-sm-12 col-md-12 top10" id="captchaMsgId">
				<label style="margin-left:-2%;" >
					<strong>Please Select Checkbox</strong>
					<span class="required">*</span>
				</label>
			</div>        
			<div class="row  top5" id="captchaContainerId">        
				<div class="col-sm-12 col-md-12 top-s110">
					<div id="captcha_container"></div>							         							        
				</div>
			</div>          
			<div class="row">	        
				<div class="col-sm-12 col-md-12 top13" >
					<button class="btn btn-primary" style="padding: 6px 17px;" type="button" onclick="return validateKellySignup()">Sign Up <i class="icon"></i></button>
				</div>
			</div>
			<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>
		</form>
	</div>
</div>

<div style="margin: auto; width: 75%;"  >
	<input type="hidden" name="captcharesponse" id="captcharesponse"/> 
</div>
<br/><br/>
<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>	
</div>



<div  class="modal hide"  id="myModalMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="backToSignUp()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group row">
		<div  class="col-sm-12 col-md-12" id="mailInfo"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="backToSignUp();">Ok</button>
 	</div>
</div> 	
</div>
</div>


<div id="displayDiv"></div>

<script>   
	document.getElementById("emailAddress1").focus();
</script>

<!-- -------------Show this popup if user is type 3 and has multiple schools--------------- -->
<div class="modal hide" id="selectSchoolDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content"  style="top:141px;left:-50px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" onclick="cancel()" aria-hidden="true">x</button>
				<h3><spring:message code="headTm"/></h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="modalErrorMsg" class="required" style="display:block;width:100%;height:10px;margin-top:-10px;"></div>
					<div class="row top10">
						<div class="col-sm-offset-0">
							<div class="col-sm-10" >
								<label><spring:message code="lblPlzSltSchool"/></label>
								<select id="selectSchoolName" name="selectSchoolName" class="form-control" style="font-family:'Century Gothic', 'Open Sans', sans-serif;font-size: 11px;line-height: 20px;">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" onclick="ok()"><spring:message code="btnLogin"/> <i class="icon"></i></button>
				<!-- <button class="btn btn-primary" onclick="ok()">Ok</button>  -->
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancel()"><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>
<!-- --------------------------------End of showing popup-------------------------------- -->


<!-- -------------Show this popup if user is type 6 and has multiple branches--------------- -->
<div class="modal hide" id="selectBranchDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content"  style="top:141px;left:-50px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" onclick="cancel()" aria-hidden="true">x</button>
				<h3>TeacherMatch</h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="modalBranchErrorMsg" class="required" style="display:block;width:100%;height:10px;margin-top:-10px;"></div>
					<div class="row top10">
						<div class="col-sm-offset-0">
							<div class="col-sm-10" >
								<label>Please select branch</label>
								<select id="branchNameSel" name="branchNameSel" class="form-control" style="font-family:'Century Gothic', 'Open Sans', sans-serif;font-size: 11px;line-height: 20px;">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" onclick="cont()">Login <i class="icon"></i></button>
				<!-- <button class="btn btn-primary" onclick="ok()">Ok</button>  -->
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="close()">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!-- --------------------------------End of showing popup-------------------------------- -->

<script  type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>
 	
<c:set var="isAuthmail"><%=request.getParameter("authmail")%></c:set>	
<c:if test="${isAuthmail eq  'true'}">
	<script>
		var divMsg="<div><%=request.getSession()!=null && request.getSession().getAttribute("authmsg")!=null?request.getSession().getAttribute("authmsg"):"" %></div>";
		addDialogContent("am","TeacherMatch",divMsg,"displayDiv");
		showDialog("am",actionForward,"560","200");
		function actionForward(){
			window.location.href="smartpractices.do";
		}
	</script>
	<style>
.ui-dialog-titlebar-close {
  visibility: hidden;
}
	</style>

</c:if>

<c:if test="${popup}">
	<script>
		//var msg = "You have been successfully registered with TeacherMatch.<br/>We have sent you an email with login details and an authentication link.<br/><font color='red'>Please check your email to authenticate.</font><br/>If you did not receive the authentication email, please check your spam box or please send an email to us at <a href='mailto:clientservices@teachermatch.com'>clientservices@teachermatch.com</a><br/><br/>Clicking <font color='red'>Ok</font> will log into the Smart Practices system, but before your second login you must first confirm your account via the email from noreply@teachermatch.net";
		var msg = "<p>Welcome valuable Kelly Educational Staffing&reg;(KES&reg;) talent. You have been successfully registered with TeacherMatch for the Smart Practices&#8482; professional development pre-hire training series.</p>";
		msg +="<p>We have sent you an email with login details and an authentication link.<br/><font color='red'>Please check your email to authenticate.</font></p>";
		msg +="<p>If you did not receive the authentication email, please check your spam box or please send an email to us at <a href='mailto:clientservices@teachermatch.com'>clientservices@teachermatch.com</a></p>";
		//msg +="<p>Clicking <font color='red'>Ok</font> will log into the Smart Practices&#8482; professional development pre-hire training series created exclusively for KES talent, but before your second login you must first confirm your account via the email from noreply@teachermatch.net</p>"
		
		var url = "smartpractices.do"; // "spuserdashboard.do";
		showInfoAndRedirect(0,msg,'',url);
		
	</script>
	</c:if>
	<input type="hidden" id="refType" value="${refType}"/> 
	<input  type="hidden" id="jobId" name="jobId" value="${kellyJobId}">
	<div class="modal hide" id="branchLookupDiv" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 700px; ">
	<div class="modal-content">
	<div class="modal-header" >
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick=''>x</button>
		<h3 style="padding-left: 5px;">Kelly Educational Staffing </h3>
		<h3 style="padding-left: 5px; margin-top: -10px;">Branch Lookup</h3>
	</div>	
	<div class="modal-body" style="max-height: 400px; padding-top:0px; overflow-y: scroll;">
	        <div class="row top15">                                  
				<div class="col-sm-6 col-md-6">
                	<label>State</label>
                    <select id="stateIdForDSPQ" name="stateIdForDSPQ" Class="form-control">
					<option value="" id="slst"><spring:message code="optSltSt" /></option>
						<c:forEach items="${listStateMaster}" var="st">									
							<option id="st${st.stateId}" value="${st.stateId}">${st.stateName}</option>
						</c:forEach> 							
					</select>
				</div>
			</div>                                                                     
			<div class="row top10">                                  
				<div class="col-sm-6 col-md-6">
					<label>Key Word Search</label>
					<input name="kewWord" id="keyWord" class="form-control" maxlength="50"/>
				</div>
				<div class="col-sm-6 col-md-6">
					
					<button type="button" id="" class="btn btn-primary top25"  maxlength="50" onclick="searchBranchLookup()">Search<i class="icon"></i></button>
					
				</div>
			</div>  
			
			
			<div id="gridBranchLookup" class="table-responsive" style="margin-top: 10px;" ></div>
	</div>
   </div>
  </div>
 </div>
 <div style="display:none; z-index: 5000;" id="loadingDiv" >
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>					
	</table>
</div>