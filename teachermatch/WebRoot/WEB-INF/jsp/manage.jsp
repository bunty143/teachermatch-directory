<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type='text/javascript' src="js/userhome.js?ver=${resourceMap['js/userhome.js']}"></script>
<script type="text/javascript" src="dwr/interface/UserHomeAjax.js?ver=${resourceMap['UserHomeAjax.ajax']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[330,285,150,100,80], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>
<div class="row top15" style="margin-left: 0px;margin-right: 0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headManageDist"/></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<c:if test="${userMaster.entityType eq 1}">
   <div class="mt10"></div>
   </c:if>   
	<div class="row" onkeypress="return chkForEnterShowDistrict(event);" >		
			<form class="bs-docs-example" onsubmit="return false;">
				<div id="Searchbox" class="<c:out value="${hide}"/>">
					<div class="col-sm-3 col-md-3">
						<label><spring:message code="lblEntityType"/></label>
					    <select class="form-control" id="MU_EntityType" name="MU_EntityType" class="form-control" onchange="DisplayHideSearchBox();">
								<option value="1" selected><spring:message code="sltTM"/></option>
								<option value="2"><spring:message code="lblDistrict"/></option>
						</select>
	              	</div>
		            <div id="SearchTextboxDiv"  <c:out value="${hide}"/>">	         		   
		             		<div  class="col-sm-5 col-md-5">
		             			<label id="captionDistrictOrSchool"><spring:message code="lblDistrict"/></label>
			             		<input type="text" id="districtName" name="districtName"  class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
								onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
							<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>	
			             	</div>		             
	          		</div>
					<div class="col-sm-2 col-md-2">					
						<c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
						<button class="btn btn-primary top25-sm" type="button" onclick="searchDistrict();"> <spring:message code="btnSearch"/><i class="icon"></i></button>
						</c:if>
					</div>
					 
        	  	</div>
       		 </form>
		</div>
		
<div class="TableContent top15">        	
       <div class="table-responsive" id="divMain">           	         
 </div>            
</div> 
	
    <br><br>
    <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>



<div id="editdistrictDiv" style="display: none;margin-top: -25px;">
	<div class="row">
	<div class="col-sm-8 col-md-8">
	<div class='divErrorMsg' id='errordiv' style="display: block;"></div>	
	</div>
	</div>
	
	<div class="row">
	<div class="col-sm-8 col-md-8">
	<label>
<spring:message code="lblDistrict"/> 
	</label>
	<input id="district" name="district"   type="text"  class="form-control" readonly="readonly" >
	</div>
	</div>
	
	<div class="row">
	<div class="col-sm-4 col-md-4">
	<label>
	<spring:message code="lblFname"/>
	</label>
	<input id="fname"  name="fname"  type="text"  class="form-control" >
	</div>
	
	<div class="col-sm-4 col-md-4">
	<label>
	<spring:message code="lblLname"/>
	</label>
	<input id="lname"  name="lname"  type="text"  class="form-control" >
	</div>
	
	<div class="col-sm-8 col-md-8">
	<label>
	<spring:message code="lblEmail"/> 
	</label>
	<input id="email"  name="email"  type="text"  class="form-control" readonly="readonly">
	</div>
	</div>	
	<div class="row">
	        <div class="col-sm-4 col-md-4">
		    	<label><strong><spring:message code="lblPass"/></strong><span class="required" >*</span></label>
	        	<input id="password" type="password" class="form-control" maxlength="12" readonly="readonly" />
	        	<input id="savepassword" type="hidden" class="form-control"  />
			</div>	
			<div class="col-sm-3 col-md-3 top30-sm"><!--
		        	    	<a href="#" onclick="return showChangePWD()" data-original-title="Click here to Change Password" rel="tooltip" id="changepassword">Change Password</a>    	    					
		        	    	
			--></div>      
	</div> 		
	<div class="row top10">
	    <div class="col-sm-9 col-md-9">
		    <label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
	   		<input type="radio" value="1"  name="reqType" checked="checked"  ${jobApplicationCriteria1} /><spring:message code="msgIWantAppToProvideName"/></span>
	   	    </label>
	    </div>
	    <div class="col-sm-9 col-md-9">
		    <label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
	   		<input type="radio" value="2" name="reqType"  ${jobApplicationCriteria2} /><spring:message code="msgIWantAppToTakeEPI"/></span>	   		
	   	    </label>
	    </div>
	    <div class="col-sm-9 col-md-9">
		    <label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
	   		<input type="radio" value="3"  name="reqType"  ${jobApplicationCriteria3} /><spring:message code="msgIwantAppToDirectedMyJob"/> </span>
	   	    </label>
	    </div>	
	</div>

    <div class="row top25">
    <div class="col-sm-4 col-md-4">
     <button style="margin-top: -4px;" class="btn fl btn-primary" type="button" onclick="return updateUser('${userMaster.emailAddress}')"><spring:message code="btnUpdate"/>&nbsp;<i class="icon"></i></button>			                
     &nbsp;&nbsp;&nbsp;<a  href="javascript:void(0)" onclick="cancelUpdateUserr();"><spring:message code="lnkCancel"/></a>
    </div>
    </div>

</div>
	
	
<div  class="modal hide"  id="myModalUpdateMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="headManageDist"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div id="messageText">
                </div>		
			</div>
	 	</div>
	 	<div class="modal-footer">	 	
	 	 <button class='btn btn-primary' data-dismiss='modal'   aria-hidden='true' onclick="hidemessageDiv();">Ok</button>				
 		</div>
	     </div>
       </div>
</div>





<div class="modal hide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">		
		 <div class="modal-dialog-for-cgmessageTop">
		  <div class="modal-content">						
		  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"  style="color:#007AB4;">x</button>		
			 <div class="modal-body" style="background-color: #f0f0f0">				
				<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class='divErrorMsg' id='errordivPwd' style="display: block;margin-bottom: 10px;"></div>	
				</div>	
				</div>	
				<h1 class="font25 top0" style=""><spring:message code="headChngPass"/></h1>	
				<div style="height: 8px;"></div>					
					<input type="hidden" id="savepassword">
					<div class="font20" style="color: black;"><spring:message code="lblCurrPass"/><span class="required">*</span></div>
	                <input type="password" id="oldpassword" class="top5 font15" type="password" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
	                <br/>
	                
	                <div class="font20" style="color: black;"><spring:message code="lblNePass"/><span class="required">*</span></div>
	                <input type="password" id="newpassword" class="top5 font15" type="password" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
	                <br/>
	                
	                <div class="font20" style="color: black;"><spring:message code="lblRe-EtrPass"/><span class="required">*</span></div>
	                <input type=password id="repassword" class="top5 font15" type="password" style="height: 40px; outline: 0;border: 1px solid white;width: 100%;padding: 10px;"><br/>
	                <br/>     
					<div class="pull-right">
					<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>&nbsp;&nbsp;
 		            <button class="btn btn-primary" onclick="chkPassword()"><spring:message code="btnSvChng"/></button>
 		            </div>
 		            <div style="clear: both;"></div>
				
		 	</div>		 
		   </div>
	  </div>
</div>

<script>

onLoadDisplayHideSearchBox(<c:out value="${userMaster.entityType}"/>);
$('#changepassword').tooltip();	
</script>

	



