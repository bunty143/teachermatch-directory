<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/examslotselection.js?ver=${resourceMap['js/examslotselection.js']}"></script>
<link href="css/kendo.default.min.css" rel="stylesheet">
<script src="js/jquery.min.js"></script>
<script src="js/kendo.all.min.js"></script>
<link href="css/kendo.dataviz.min.css" rel="stylesheet" />
<link href="css/kendo.common.min.css" rel="stylesheet">
<link href="css/kendo.default.min.css" rel="stylesheet">
<link href="css/kendo.rtl.min.css" rel="stylesheet" />
<link href="css/kendo.material.min.css" rel="stylesheet">  
<link href="css/kendo.dataviz.material.min.css" rel="stylesheet" />

 <style type="text/css">
 .k-event{ 
		height: 21px !important;
  }
 .k-scheduler-footer,.k-view-workweek
  {
  display:none !important;
  }
  .tdcolor{
  background-color:white !important;
  }
  .k-state-selected
{
      background-color: #ebebeb !important;
}
td{
      background-color: #ebebeb !important;
}
 .k-scheduler-content{
 height: 435px !important;
 } 
 #scheduler{
 height: 455px !important;
 margin-bottom: 40px;
 }
</style>
<script>
var disableIds = [];


var txtBgColor = "#F5E7E1";
var page = 1;
var noOfRows = 10;
var sortOrderStr="";
var sortOrderType="";


function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert("Oops: Your Session has expired!");  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}

function getPaging(pageno)
{
	if(pageno!='')
	{
		page=pageno;	
	}
	else
	{
		page=1;
	}
	
	noOfRows = document.getElementById("pageSize").value;
	displayAssessmentSlotDetails();
}
function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		page=pageno;	
	}else{
		page=1;
	}
	sortOrderStr=sortOrder;
	sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		noOfRows = document.getElementById("pageSize").value;
	}else{
		noOfRows=10;
	}
	displayAssessmentSlotDetails();
}
function scheduler_save(e) {
	//alert('scheduler_save::::');
	// saveAssessmentInvite(e);
}
function scheduler_add (e) {
   // kendoConsole.log("save");
}
/*function getDayByDate(date){
    var d = new Date(date);
    var weekday = new Array(7);
    weekday[0]=  "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";
    var n = weekday[d.getDay()];
    return n;
}*/
var tmpCnt=0;
var array=null;
var enableDays=[];
var colorDays=[];
function displayExamCalendar() 
{
	
	
	$("#displayExamCalendarDiv").show();
	$('#loadingDiv').show();
    var districtAssessmentId=document.getElementById("districtAssessmentId").value;
    var centreScheduleId=document.getElementById("centreScheduleId").value;
    var teacherId=document.getElementById("teacherId").value;
    var jobId=document.getElementById("jobId").value;
    var startDate = "";
    if($("#examDate").val()!=null)
    	startDate = $("#examDate").val();
    
    $("#scheduler").empty();
	var scheduler = $("#scheduler").data("kendoScheduler");
	if(scheduler!=null)
		scheduler.destroy();
    
    CGServiceAjax.displayExamCalendar(districtAssessmentId,centreScheduleId,teacherId,jobId,startDate,{
    	async : false,
    	callback : function(data) {
    	//alert(JSON.stringify(data[0]));
    	 var bookings = [{field: "id",dataSource: data[1]}];
    	 array = data[2];
    	 for (i = 0; i < array.length; i++) { 
    	   var entry=array[i];
    	   var name;
    	      for (name in entry) {
    	         if(name=="day"){
    	        	 	enableDays.push(entry[name]);
    	         }
    	   }
    	 }
    	 
    	 var current = new Date();     // get current date
    	 if( !(startDate==null || startDate=="") )
    	 {
    		 current = new Date();     // get current date
    		 var dateInArray = startDate.split("-")
    		 current.setDate(dateInArray[1]);
    		 current.setMonth(dateInArray[0]-1);
    		 current.setFullYear(dateInArray[2]);
    	 }
    	 
    	 var weekstart = current.getDate() - current.getDay() +1;
    	 var monday = new Date(current.setDate(weekstart));
    	 
    	 $("#scheduler").kendoScheduler({    
         height: 490,
         allDaySlot: false,
         showWorkHours:true,
         editable: false,
         date:monday,
    	 views: [{type: "workWeek", selected: true }],
    	        selectable: true,
    	        save: scheduler_save,
    	        add: scheduler_add,
    	        change:scheduler_change,
    	        dataSource: data[0],
    	      	resources: bookings,
    	      	navigate:scheduler_navigate,    	      	
    	      	
    	      	  edit: function (e) {
    	        	    	  if (e.event.id==0) { 
    	        	             e.preventDefault();
    	        	          }
    	        	    	  else
    	        	    	  {
	    	    	        	  disableSlotIds();
	    	        	    	  for(var id = 0 ;id<disableIds.length;id++)
	    	        	    	  {
	    	        	    		  if (e.event.id==disableIds[id]) {
	    	        	      	            e.preventDefault();
	    	    	          	        }
	    	        	    	  }
	    	        	    	  for(var i=0;i<disableIds.length;i++)
	    	      					{
	    	      						disableIds.pop();
	    	      					}
    	        	    	  }    	       
    	    },
    	    dataBound: function(e) {
                var scheduler = this;
                var view = scheduler.view();
                var count=0;
                view.content.find("td").each(function() {
                var slot = scheduler.slotByElement($(this));
                 //check if current slot should be colored based on your custom logic                 
                /*  var n = getDayByDate(slot.startDate);
                  if (enableDays.indexOf(n)> -1) {                    	 
                              var pos = enableDays.indexOf(n);
                              var color = colorDays[pos];
                              $(this).css("background", "#ebebeb"));
                  }else{
                         $(this).css("background", "#ebebeb"));
                  }*/
                })
                }
    	});
    	
    	$('#loadingDiv').hide();
    	}
    });
}
var event = "";
function scheduler_navigate(e)
{
	if(e.action=="next" || e.action=="previous" || e.action=="changeDate" || e.action=="today")
	{
		var mondayDate = getMonday(e.date);
		var mydate = (mondayDate.getMonth()+1)+"-"+mondayDate.getDate()+"-"+mondayDate.getFullYear();
		$("#examDate").val(mydate);
		displayExamCalendar();
	}
	
	if(e.action="workView")
	{
		event = "";
	}
}

function getMonday(d)
{
	d = new Date(d);
	var day = d.getDay(),
	diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
	return new Date(d.setDate(diff));
}

function scheduler_change(e) {
	  var events = e.events; //list of selected Scheduler events
      var centreScheduleId =  events[events.length - 1].id;
      document.getElementById("centreScheduleId").value=centreScheduleId;
      var	districtAssessmentId=document.getElementById("districtAssessmentId").value;
      var	centreScheduleId=document.getElementById("centreScheduleId").value;
      var	teacherId=document.getElementById("teacherId").value;
      var dt = events[events.length - 1].start;
      if( event!="workView")
      {
    	  var n = ("0" + (dt.getMonth() + 1)).slice(-2) + '/' + ("0" + (dt.getDate())).slice(-2) + '/' +  dt.getFullYear();
          //alert("date select in kendo :::: "+n);
          if (enableDays.indexOf(n)> -1) {
        		$('#loadingDiv').show();
	               CGServiceAjax.getStartEndDateByAssessment(centreScheduleId,districtAssessmentId,teacherId,{ 
	  		  		async: false,
	  		  		callback: function(data){
	  		    	  if(data.length>0)
	  		    	  {	  		    		
	  			    	  $("#dataWindow").html(data);
	  			    	  var win = $("#showpopupKendoWindow").data("kendoWindow");
	  			    	  win.center();
	  			    	  win.open();
	  			    	$('#loadingDiv').hide();
	  		    	  }
	  		    	  else
	  		    	  {
	  		    		$('#loadingDiv').hide();
	  		    	  }
	  		  		},
	  		  		errorHandler:handleError,
	  		  	});
          }else{
    	       		//e.preventDefault();
    	       		$('#loadingDiv').hide();
    	       }
      }
}

function closePopUp()
{
	 var win = $("#showpopupKendoWindow").data("kendoWindow");
	 win.close();
}
function saveSlot()
{
	saveAssessmentInvite();
}

function objToString (obj) {
	var str = '';
	for (var p in obj) {
		  if (obj.hasOwnProperty(p)) {
				str += p + '::' + obj[p] + '\n';
		  }
	}
	return str;
}
function saveAssessmentInvite(){
	
	var teacherId=document.getElementById("teacherId").value;
	var jobId=document.getElementById("jobId").value;
	var	districtAssessmentId=document.getElementById("districtAssessmentId").value;
	var	centreScheduleId=document.getElementById("centreScheduleId").value;
	CGServiceAjax.saveAssessmentInvite(teacherId,jobId,districtAssessmentId,centreScheduleId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		closePopUp();
			displayExamCalendar();
			
		}
	});
}
function displayAssessmentSlotDetails(){
	var teacherId = document.getElementById('teacherId').value;
	$('#loadingDiv').show();
	try
	{
		CGServiceAjax.showAssessmentDetails(teacherId,noOfRows,page,sortOrderStr,sortOrderType,{ 
		async: false,
		callback: function(data)
		{
			if(data!='')
			{
				$('#loadingDiv').hide();
				try{$('#examSlotData').html(data);	}catch(e){alert(e);}		
				try{applyScrollOnTbl();}catch(e){alert(e);}
			}
			else
			{
				//alert("No Data");
			}
			
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}
}

function disableSlotIds()
{
	try
	{
		CGServiceAjax.getDisabledSlotIds({ 
		async: false,
		callback: function(data)
		{
			if(data!='')
			{
				for(var i=0;i<disableIds.length;i++)
				{
					disableIds.pop();
				}
				var result = data.split(",");
				for(var i=0;i<result.length;i++)
				{
					disableIds.push(result[i]);
				}
			}
		},
		errorHandler:handleError 
	});
	}catch(e){alert(e)}
}
</script>

<div class="row" style="margin:0px;">
                
         <div style="float: left;">
             <div class="subheading" style="font-size: 13px;margin-left:0px;"><img src="images/assessment.png" style="width:15%;">&nbsp;&nbsp;Assessment Slot Selection</div>    
         </div>            
         
        <div style="clear: both;"></div>    
        <div class="centerline"></div>
</div>
<br>
<div class="row" style="margin:0px;">
<div style="font-size: 12px;float:left;margin-bottom: 10px;">
        Please select a slot to book an appointment with the Test Center for taking your test 
</div>
</div>
    <div class="row" style="margin:0px;">
        <div style="float:left;margin-bottom: 10px;">
             
             <table>
<tr>
            <td class="tdcolor"><span class="examslotlabel" style="font-size: 7px;font-weight: bold;background-color: #ebebeb;"></span></td>
            <td class="tdcolor" align="left" style="color: black;">&nbsp;Not Available</td>
            <td class="tdcolor" width="40"></td>
<td class="tdcolor"><span class="examslotlabel" style="font-size: 7px;font-weight: bold;background-color: white"></span></td>
            <td class="tdcolor" align="left" style="color: black;">&nbsp;Available</td>
            <td class="tdcolor" width="40"></td>
<td class="tdcolor"><span class="examslotlabel" style="font-size: 7px;font-weight: bold;background-color: #00882B;"></span></td>
            <td class="tdcolor" align="left" style="color: black;">&nbsp;Your Bookings</td>
          </tr>
             </table>
    </div>
<div style="font-size: 12px;float:right;margin-bottom: 10px;"><a href="myassessments.do"  target="_blank">My Appointments</a></div>

<div id="showpopupKendoWindow">
	<div id="dataWindow"></div>
</div>
	 
</div>
 
    <div id="example">
    <div class="box">
        <div class="box-col"></div>
    </div>
	 
    <div id="scheduler"></div>
	</div>
 
<input type="hidden" name="examDate" id="examDate" value=""/>              
<input type="hidden"  name="districtAssessmentId" id="districtAssessmentId" value="${districtAssessmentId}"/>
<input type="hidden"  name="centreScheduleId" id="centreScheduleId" value="1"/>
<input type="hidden"  name="teacherId" id="teacherId"  value="${teacherId}"/>
<input type="hidden"  name="jobId" id="jobId" value="${jobId}"/>
<div  class="modal hide"  id="slotSelection"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
        <h3 id="myModalLabel">Teacher Match</h3>
    </div>
    <div class="modal-body">
        <div class="control-group" id='confirmDeactivate'>
       <spring:message code="msgDoYuWantToRemoveFacilitator"/>
        </div>
     </div>
     <div class="modal-footer">
         <span id=""><button class="btn  btn-primary" onclick="removeFacilitatorById();"><spring:message code="lblYes"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
         <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideMessageDiv()"><spring:message code="lblNo"/></button>
     </div>
     <input type="hidden" id="facilitatordelId" name="facilitatordelId">
</div>     
</div>
</div>
 
<div  class="modal hide"  id="assessmentSlotDetails"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
        <h3 id="myModalLabel">Teacher Match</h3>
    </div>
    <div class="modal-body">
        <div class="control-group">
            <div id="assessmentData"></div>
        </div>
     </div>
     <div class="modal-footer">
         <button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button>
     </div>
     <input type="hidden" id="facilitatordelId" name="facilitatordelId">
</div>     
</div>
</div>

<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

  <script>
 
  displayExamCalendar();
  </script>

<script>
$("#showpopupKendoWindow").kendoWindow({
  	//actions: [ "Minimize", "Maximize","Close" ]
  	actions: ["Close"],
	modal:true,
	autoFocus: true,
	draggable: false,
	//maxHeight: 600,
	minHeight: 120,
	maxWidth: 280,
	minWidth: 280,
	pinned: true,
	resizable: false,
	title: true,
	title: "Assessment Time",
});

//$("#kendoWindow").data("kendoWindow").center();	//set default position to center
$("#showpopupKendoWindow").data("kendoWindow").center();	//set default position to center

//$("#kendoWindow").data("kendoWindow").close();	//default close window.
$("#showpopupKendoWindow").data("kendoWindow").close();	//default close window.
</script>
  