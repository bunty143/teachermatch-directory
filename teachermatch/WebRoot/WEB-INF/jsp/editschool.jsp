<!-- @Author: Gagan 
 * @Discription: view of edit school Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/SchoolAjax.js?ver=${resourceMap['SchoolAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/UserAjax.js?ver=${resourceMap['UserAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/editschool.js?ver=${resourceMap['js/editschool.js']}"></script>
  <!-- Optionally enable responsive features in IE8 -->
    <script type='text/javascript' src="js/bootstrap-slider.js"></script>
    <link rel="stylesheet" type="text/css" href="css/slider.css" />  
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  


<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });

function applyScrollOnTblNotes()
{
	//alert("applyScrollOnTblNote   ");
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#schoolNotesTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 690,
        minWidth: null,
        minWidthAuto: false,
        colratio:[288,182,144,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblDistAttachment()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#distAttachmentTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 690,
        minWidth: null,
        minWidthAuto: false,
        colratio:[268,170,142,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script> 
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	
	.tooltip {
  position: absolute;
  z-index: 1030;
  display: block;
  font-size: 12px;
  line-height: 1.4;
  opacity: 0;
  filter: alpha(opacity=0);
  visibility: visible;
}

.tooltip.in {
  opacity: 0.9;
  filter: alpha(opacity=90);
}

.tooltip.top {
  padding: 5px 0;
  margin-top: -3px;
}

.tooltip.right {
  padding: 0 5px;
  margin-left: 3px;
}

.tooltip.bottom {
  padding: 5px 0;
  margin-top: 3px;
}

.tooltip.left {
  padding: 0 5px;
  margin-left: -3px;
}

.tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #ffffff;
  text-align: center;
  text-decoration: none;
  background-color: #000000;
  border-radius: 4px;
}

.tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}

.tooltip.top .tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-left .tooltip-arrow {
  bottom: 0;
  left: 5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-right .tooltip-arrow {
  right: 5px;
  bottom: 0;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-right-color: #000000;
  border-width: 5px 5px 5px 0;
}

.tooltip.left .tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-left-color: #000000;
  border-width: 5px 0 5px 5px;
}

.tooltip.bottom .tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-left .tooltip-arrow {
  top: 0;
  left: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-right .tooltip-arrow {
  top: 0;
  right: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}
.divwidth
{
float: left;
width: 42px;
}

.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}	
</style>

<!--  <h1> Welcome Gagan</h1>-->
<form:form commandName="schoolMaster"  method="post"  enctype="multipart/form-data" id="editSchoolForm">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/editschool.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headAdEdSch"/></div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
	
	<div class="">
    <div class="mt10">
		<div class="panel-group" id="accordion">
        	<div class="panel panel-default">
				<div class="accordion-heading">
					<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle minus">
                    <spring:message code="lnkGenInfo"/></a>
				</div>
				<div class="accordion-body in" id="collapseOne" style="height:auto;">
            	<div class="accordion-inner">
              	<div class="offset1">
				<div class="row">
					<div class="col-sm-12 col-md-12">			                         
					 	<div class='required' id='errorlogoddiv'></div>
					</div>
				</div>
                      		
                      		<div class="row">
								<div class="col-sm-6 col-md-6">
									<label><spring:message code="lblNm"/></label>
									<form:input path="schoolName" cssClass="form-control" maxlength="100" placeholder="" readonly="true"  tabindex="-1"/>
									<form:input path="schoolId" type="hidden" cssClass="form-control" maxlength="50" placeholder="" readonly="true" tabindex="-1" />
								</div>
							</div>
							
							<div class="row">	
								<div class="col-sm-2 col-md-2">
									<label><spring:message code="lblNCESSchID"/></label>
									<form:input path="ncesSchoolId" cssClass="form-control" maxlength="50" placeholder="" readonly="true"  tabindex="-1" />
								</div>
								<div class="col-sm-2 col-md-2">
									<label><spring:message code="lblNCESDistID"/></label>
									<input type="text" id=districtId name="districtId" value="${schoolMaster.districtId.districtId}" class="form-control"  tabindex="-1"  readonly="true">
								</div>
								<div class="col-sm-2 col-md-2">
									<label><spring:message code="lblNCESStID"/></label>
<!--									<input type="text" placeholder="" class="span3">-->
									<form:input path="ncesStateId" cssClass="form-control" maxlength="11" placeholder="" readonly="true"  tabindex="-1"/>
								</div>
						  </div>
						  
						  <div class="row">			
								<div class="col-sm-6 col-md-6">
									<label><spring:message code="lblDistrictName"/></label>
									<form:input path="districtName" cssClass="form-control" maxlength="50" placeholder="" readonly="true"  tabindex="-1"/>
								</div>
						  </div>
						  
						  <div class="row">									
								<!--<form>-->
								<div class="col-sm-4 col-md-4">
									<label><spring:message code="lblAdd"/></label>
									<form:input path="address" cssClass="form-control" maxlength="250" placeholder="" readonly="true"  tabindex="-1"/>
								</div>

								<div class="col-sm-2 col-md-2">
									<label><spring:message code="lblZepCode"/></label>
<!--									<input type="text" placeholder="" class="span3">-->
									<form:input path="zip" cssClass="form-control" maxlength="11" placeholder="" readonly="true"  tabindex="-1"/>
								</div>
						 </div>
						 
						  <div class="row">									
								<div class="col-sm-3 col-md-3">
									<label><spring:message code="lblSt"/></label>
									<input type="text" id="stateName" name="stateName" value="${stateMaster.stateName}" class="form-control"  readonly="true"  tabindex="-1">
								</div>

								<div class="col-sm-3 col-md-3">
									<label><spring:message code="lblCity"/></label>
									<form:input path="cityName"   class="form-control"  readonly="true"  tabindex="-1" />
								</div>
						</div>
						
						<div class="row">								
									<div class="col-sm-2 col-md-2">
										<label><spring:message code="lblPhone"/></label>
										<form:input path="phoneNumber" maxlength="20"   class="form-control"  readonly="true"  tabindex="-1" />
									</div>
									<div class="col-sm-2 col-md-2">
				                      	<label><spring:message code="lblAltPh"/></label>
										<form:input path="alternatePhone" cssClass="form-control" maxlength="20" placeholder=""/>
				                    </div>
								
									<div class="col-sm-2 col-md-2">
										<label><spring:message code="lblFax"/></label>
											<form:input path="faxNumber"  maxlength="20" class="form-control" />
									</div>
					   </div>
					   
						<div class="row">				
									<div class="col-sm-6 col-md-6">
										<label><spring:message code="lblWebUrl"/></label>
										<form:input path="website"  maxlength="100"  class="form-control"/>
									</div>
						</div>
						
						<div class="row">		
	                      			<div class="col-sm-2 col-md-2">
			                        	<label><spring:message code="lblToOfTech"/> <a href="#" id="iconpophover6" rel="tooltip" data-original-title="Total number of teachers in the school per NCES records. If this information is not accurate, please notify your TeacherMatch Account Manager"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
										<form:input path="noOfTeachers"   class="form-control" tabindex="-1"  readonly="true" />
			                      	</div>
			                      	<div class="col-sm-2 col-md-2">
			                        	<label><spring:message code="lblToOfStu"/><a href="#" id="iconpophover10" rel="tooltip" data-original-title="Total number of students in the school per NCES records. If this information is not accurate, please notify your TeacherMatch Account Manager"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
										<form:input path="noOfStudents"   class="form-control" tabindex="-1"   readonly="true"  />
			                      	</div>
			            </div> 	
			            		
			
              <div class="row">
              <div class="col-sm-3 col-md-3">
               <label><spring:message code="lblGrd"/></label>
               </div>
              </div>
            
             <div class="row row col-sm-12 col-md-12 left5">
             <div class="divwidth">
               <c:if test="${schoolMaster.pkOffered eq 1}">	        			
         			<c:set var="statusPKOFFRD" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="PK" id="PKOFFRD" name="Grade" ${statusPKOFFRD}  disabled="disabled" tabindex="-1">
                 <spring:message code="lblPK"/>
               </label>
             </div>  
             <div class="divwidth">
               <c:if test="${schoolMaster.kgOffered eq 1}">	        			
         			<c:set var="statusKGOFFRD" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="KG" id="KGOFFRD" name="Grade"  ${statusKGOFFRD} disabled="disabled" tabindex="-1">
                 <spring:message code="lblKG"/>
               </label>
              </div>  
             <div class="divwidth">  
                 <c:if test="${schoolMaster.g01Offered eq 1}">	        			
         			<c:set var="statusg01Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="1" id="G01OFFRD" name="Grade" ${statusg01Offered} disabled="disabled" tabindex="-1">
                 1 
               </label>
              </div>  
             <div class="divwidth">  
                <c:if test="${schoolMaster.g02Offered eq 1}">	        			
         			<c:set var="statusg02Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
               
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="2" id="G02OFFRD" name="Grade" ${statusg02Offered} disabled="disabled" tabindex="-1">
                 2 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g03Offered eq 1}">	        			
         			<c:set var="statusg03Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="3" id="G03OFFRD" name="Grade" ${statusg03Offered} disabled="disabled" tabindex="-1">
                 3 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g04Offered eq 1}">	        			
         			<c:set var="statusg04Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="4" id="G04OFFRD" name="Grade"  ${statusg04Offered}disabled="disabled" tabindex="-1">
                 4 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g05Offered eq 1}">	        			
         			<c:set var="statusg05Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="5" id="G05OFFRD" name="Grade" ${statusg05Offered} disabled="disabled" tabindex="-1">
                 5 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g06Offered eq 1}">	        			
         			<c:set var="statusg06Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;" >
                 <input type="checkbox" value="6" id="G06OFFRD" name="Grade" ${statusg06Offered} disabled="disabled" tabindex="-1">
                 6 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g07Offered eq 1}">	        			
         			<c:set var="statusg07Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="7" id="G07OFFRD" name="Grade"  ${statusg07Offered}disabled="disabled" tabindex="-1">
                 7 
               </label>
                </div>  
             <div class="divwidth">
                <c:if test="${schoolMaster.g08Offered eq 1}">	        			
         			<c:set var="statusg08Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="8" id="G08OFFRD" name="Grade" ${statusg08Offered} disabled="disabled" tabindex="-1">
                 8 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g09Offered eq 1}">	        			
         			<c:set var="statusg09Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="9" id="G09OFFRD" name="Grade"  ${statusg09Offered} disabled="disabled" tabindex="-1">
                 9 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g10Offered eq 1}">	        			
         			<c:set var="statusg10Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="10" id="G10OFFRD" name="Grade" ${statusg10Offered} disabled="disabled" tabindex="-1">
                 10 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g11Offered eq 1}">	        			
         			<c:set var="statusg11Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="11" id="G11OFFRD" name="Grade" ${statusg11Offered} disabled="disabled" tabindex="-1">
                 11 
               </label>
               </div>  
             <div class="divwidth"> 
                <c:if test="${schoolMaster.g12Offered eq 1}">	        			
         			<c:set var="statusg12Offered" value="checked='checked'"></c:set>	         			
        		</c:if>	
                <label class="checkbox inline" style="padding-left: 0px;">
                 <input type="checkbox" value="12" id="G12OFFRD" name="Grade" ${statusg12Offered} disabled="disabled" tabindex="-1">
                 12 
               </label>
           
             </div>
           </div>
			
			
			
			<div class="row">
				<div class="col-sm-6 col-md-6">
               		<label><spring:message code="lblDplyN"/><a data-original-title="Name that should be displayed on job postings to the job applicant(s)" rel="tooltip" id="iconpophover1" href="#"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>

					<form:input path="displayName"   class="form-control" />
             	</div>
            </div> 
            	
                <div class="row">
				<div class="col-sm-6 col-md-6">
					<label><spring:message code="lblChar"/><a data-original-title="Includes geographic categorization, type and geographic region for the School per NCES records" rel="tooltip" id="iconpophover3" href="#"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
			    </div> 		
					<div class="col-sm-6 col-md-6 left15" ><!-- Printing Geography Name -->Geography: ${schoolMaster.geoMapping.geoId.geoName}  <br/></div>
					<div class="col-sm-6 col-md-6 left15" ><!-- Printing School Type Name --> Type: ${schoolMaster.schoolTypeId.schoolTypeName} <br/>	</div>
					<div class="col-sm-6 col-md-6 left15" ><!-- Printing Region Name -->Region: ${schoolMaster.regionId.regionName} </div>
				</div>
				
				<div class="row">	
				<div class="col-sm-3 col-md-3" style="padding-top: 8px;">
					<label><spring:message code="lblLg"/> <a data-original-title="Logo image for the school " rel="tooltip" id="iconpophover2" href="#"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></label>
                  	 	 <c:if test="${fn:indexOf(roleAccessGI,'|9|')!=-1}">
                  	 	<form:input path="logoPathFile" type="file"/>
						<!-- <a href="#" onclick="return clearFile(1)">Clear</a>-->
						</c:if>
				</div>
       			<div class="col-sm-3 col-md-3">
       				<label>&nbsp;</label><span id="showLogo">&nbsp;</span>
       			</div>
       		    </div>	             
                  

                   
                    <div class="row top10"  id="sDescription">
                    <div class="col-sm-6 col-md-6"><spring:message code="lblDecr"/></div>
                    </div> 
                                      
                    <div class="row"> 
                    <div class="col-sm-8 col-md-8">
                    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 						</label>
                    </div>                    
                    </div>  
                                                        
                    <div class="row">                   
                      <div class="col-sm-8 col-md-8 mt08">
	                     <span>
	                     	<form:textarea path="description" rows="3" maxlength="1500" class="form-control" onkeyup="return chkLenOfTextArea(this,1500);" onblur="return chkLenOfTextArea(this,1500);"/>
	                     </span>		              
                    </div>
                    </div>                    
                    
                    <div class="row">
                    <div class="col-sm-9 col-md-9">
                      <p><spring:message code="pImproveServuceQualAndEffi"/> <a data-original-title="Periodically, TeacherMatch surveys its customers for feedback on its quality, responsiveness and efficacy" rel="tooltip" id="iconpophover9" href="#"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></p>
                    </div>
                    </div>
                
					<div class="row">
                    <div class="col-sm-6 col-md-6 left20">
						<label class="radio inline" style="padding-left: 0px;">                        
                        <form:radiobutton path="canTMApproach" value="1" />
                     <spring:message code="lblYes"/> 
                        </label>                        
                        </br>
                        <div style="padding-left: 55px;margin-top: -20px;">
                      	<label class="radio inline" style="padding-left: 0px;">
                        <form:radiobutton path="canTMApproach" value="0" />
                      <spring:message code="lblNo"/> </label>
                        </div>
                    </div>
    				</div>
    				
                </div>               
              </div>
            </div>
        </div>
        
			<div class="panel panel-default">
			<div class="accordion-heading"> <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="headContInfo"/> </a> </div>
             <div class="accordion-body collapse" id="collapseTwo" style="height: 0px;">           
           	<div class="accordion-inner">
				<div class="offset1">			
				<div class="row">
					<div class="col-sm-12 col-md-12">			                         
					 	    <div class='required' id='errorschoolcontactinfodiv'></div>
						 	<div class='required' id='errorschoolkeydiv'><%-- Gagan--%></div>
						 	<div class='required' id='errorschoolpwddiv'><%-- Gagan--%></div>
					</div>
				</div>						 	
                <div class="row">
                             <c:set var="disableDmFields" value=""></c:set>					
						     <c:if test="${schoolMaster.dmName ne '' && schoolMaster.dmPassword ne ''}">
				         	 <c:set var="disableDmFields" value="true"></c:set>	 
						     </c:if> 
                    <div class="col-sm-4 col-md-4">
						<label><spring:message code="lblFnlDecMkrN"/><span class="required">*</span> <a href="#" id="iconpophover4" rel="tooltip" data-original-title="The individual who has final decision-making authority over all functions of the account"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
                      	<form:input path="dmName" cssClass="form-control" readonly="${disableDmFields}" maxlength="50" placeholder=""/>
                    </div>
                    <div class="col-sm-3 col-md-3">
                      	<label><spring:message code="lblEmail"/><span class="required">*</span></label>
                      	<form:input path="dmEmailAddress" cssClass="form-control" readonly="${disableDmFields}" maxlength="100" placeholder=""/>
                    </div>
                    <div class="col-sm-2 col-md-2">
                      	<label><spring:message code="lblPhone"/></label>
                      	<form:input path="dmPhoneNumber" cssClass="form-control" readonly="${disableDmFields}" maxlength="20" placeholder=""/>
                    </div>                   
                    <c:if test="${schoolMaster.dmName ne '' && schoolMaster.dmPassword ne ''}">
					<c:set var="hideDmPwd" value="hide"></c:set>
					</c:if>
	                    <div id="dmPwdDiv" class="col-sm-2 col-md-2 ${hideDmPwd} }">
	                      	<label><spring:message code="lblPass"/><span class="required">*</span></label>
	                      	<form:input path="dmPassword" type="password" cssClass="form-control" maxlength="20" placeholder=""/>
	                    </div>
					<c:if test="${schoolMaster.dmName ne '' && schoolMaster.dmPassword ne ''}">
		                 <div id="cnfmPwdDiv" class="col-sm-2 col-md-2">
	                      	<label><spring:message code="lblPass"/></label>
	                      	 <input type="password" id="cnfmPassword" name="cnfmPassword" class="form-control" maxlength="20" placeholder=""/>
<!--	                      	 <button type="button" id="btnDm" name="btnDm" class="btn btn-large btn-primary" onclick="editSchoolDmFields()"><strong>Go</strong></button>-->
	                    </div>           
	                    
	                    <div class="col-sm-1 col-md-1" style="margin-left:-10px;margin-top:3px;">
	                  	 <label>&nbsp;</label>
	                  	   <c:if test="${fn:indexOf(roleAccessCI,'|13|')!=-1}">
	                      	 <button type="button" id="btnDm" name="btnDm" class="btn btn-small btn-primary" onclick="editSchoolDmFields()" style="width: 45px;"><strong>Go</strong></button>
	                      </c:if>
	                    </div>
	                    
					</c:if>
                    <input type="hidden" id="entityTypeOfSession" name="entityTypeOfSession" class="form-control" maxlength="20" value="${userSession.entityType}" placeholder=""/>                    
                </div>
                
                
                <div class="row">
					<div class="col-sm-4 col-md-4">
                    	<label><spring:message code="lblStuAssCoord"/><span class="required">*</span> <a href="#" id="iconpophover11" rel="tooltip" data-original-title="The individual in charge of and most familiar with student standardized testing programs"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="acName" cssClass="form-control" maxlength="50"/>
                  	</div>
                  	<div class="col-sm-3 col-md-3">
                    	<label><spring:message code="lblEmail"/></label>
						<form:input path="acEmailAddress" cssClass="form-control" maxlength="100" placeholder=""/>
                  	</div>
                  	<div class="col-sm-2 col-md-2">
                    	<label><spring:message code="lblPhone"/></label>
						<form:input path="acPhoneNumber" cssClass="form-control" maxlength="20" placeholder=""/>
                  	</div>                 
                </div>
                
                <div class="row">
                		<c:set var="disableAccountManagerFields" value=""></c:set>					
						<c:if test="${userSession.entityType ne 1}">
				         	<c:set var="disableAccountManagerFields" value="true"></c:set>	 
						</c:if>
					<div class="col-sm-4 col-md-4">
                    	<label><spring:message code="lblAccMngFrTm"/><span class="required">*</span> <a href="#" id="iconpophover5" rel="tooltip" data-original-title="The individual from TeacherMatch who is responsible for all matters related to TeacherMatch contract"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						<form:input path="amName" cssClass="form-control" maxlength="50" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>
                  	<div class="col-sm-3 col-md-3">
                    	<label><spring:message code="lblEmail"/></label>
						<form:input path="amEmailAddress" cssClass="form-control" maxlength="100" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>
                  	<div class="col-sm-2 col-md-2">
                    	<label><spring:message code="lblPhone"/></label>
						<form:input path="amPhoneNumber" cssClass="form-control" maxlength="20" readonly="${disableAccountManagerFields}" placeholder=""/>
                  	</div>                 
                </div>
                 <c:if test="${userSession!=null && userSession.entityType eq 1 || (userSession.entityType eq 2 && userSession.roleId.roleId eq 7)}">        
                  <div class="row">                               
                      <div class="col-sm-11 col-md-11"><spring:message code="lblOthrKeCont"/>
	                      <div class="pull-right">
	                      <c:if test="${fn:indexOf(roleAccessCI,'|1|')!=-1}">
	                      <a href="javascript:void(0);" onclick="showSchoolKeyContact();"><spring:message code="lnkAddKyContact"/></a>
	                      </c:if>
	                      </div>
                    </div>                 
                  </div>  
                 </c:if>         
                <div class="row">
                   <div  class="col-sm-11 col-md-11 mt08">
                    <table id="keyContactTable" width="100%" border="0" class="table table-bordered">
                      <thead class="bg">
                        <tr>
                          <th><spring:message code="lblContTy"/></th>
                          <th><spring:message code="lblContN"/></th>
                          <th><spring:message code="lblEmail"/></th>
                          <th><spring:message code="lblPhone"/></th>
                          <th><spring:message code="lblTitle"/></th>
                          <th><spring:message code="lblOpt"/></th>
                        </tr>
                      </thead>                      
                    </table>
                  </div>
                </div>
                
               <div id="addKeyContactSchoolDiv"  class="hide">               
                <div class="row">
                    <div class="col-sm-4 col-md-4">
                      <label><spring:message code="lblContTy"/></label>
                      <select class="form-control" id="dropContactType" name="dropContactType">
                        <c:forEach items="${contactTypeMaster}" var="ctm">
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${st.stateId eq ctm.contactTypeId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
							<option id="${ctm.contactTypeId}" value="${ctm.contactTypeId}" $selected >${ctm.contactType}</option>
						</c:forEach>
                      </select>
                      <input type="hidden" id="keyContactId" name="keyContactId" placeholder="" class="form-control">
                    </div>
                  <div class="col-sm-4 col-md-4">
                    <label><spring:message code="lblContFName"/><span class="required">*</span> </label>
                    <input type="text" id="keyContactFirstName" name="keyContactFirstName" maxlength="50" placeholder="" class="form-control">
                  </div>
                   <div class="col-sm-3 col-md-3">
                    <label><spring:message code="lblContLName"/><span class="required">*</span> </label>
                    <input type="text" id="keyContactLastName" name="keyContactLastName" maxlength="50" placeholder="" class=" form-control">
                  </div>
                  </div>
                  
                  <div class="row">
                  <div class="col-sm-4 col-md-4">
                    <label><spring:message code="lblContEmail"/><span class="required">*</span> </label>
                    <input type="text" id="keyContactEmailAddress" name="keyContactEmailAddress" placeholder=""  maxlength="100" class=" form-control">
                  </div>
                  <div class="col-sm-4 col-md-4">
                    <label><spring:message code="lblPhone"/></label>
                    <input type="text" id="keyContactPhoneNumber" name="keyContactPhoneNumber" placeholder="" maxlength="20" class=" form-control">
                  </div>
                    <div class="col-sm-3 col-md-3">
                      <label><spring:message code="lblTitle"/></label>
                      <input type="text"  id="keyContactTitle" name="keyContactTitle" maxlength="25" class="form-control">
                    </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-5 col-md-5 idone mt10"><a href="javascript:void(0);" onclick="return addKeyContact();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearKeyContact();"><spring:message code="lnkCancel"/></a></div>
                </div>
                </div>       
              
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>
        
		  <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> <spring:message code="lnkUsr"/></a> </div>
          <div class="accordion-body collapse" id="collapseThree" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
              
               <div class="row">
	                <div class="col-sm-9 col-md-9">
	              		 <div id="errorschooldiv" class="required"></div>
	                </div>
                </div>   
                      
               <div class="row">
                  <div class="col-sm-10 col-md-10"><label><spring:message code="lblAdmin"/></label>
	                  <div class="pull-right">
	                   <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
	                   <c:if test="${not empty userSession.districtId.saAddUser }">
                   				<c:if test="${userSession.entityType eq 3 and userSession.districtId.saAddUser eq true}">	
									  <a href="javascript:void(0);" onclick="addAdministrator(3);"><spring:message code="lblAddAmin"/></a>
								</c:if>  
                   		</c:if>  
                   		<c:if test="${userSession.entityType ne 3 }">	
							<a href="javascript:void(0);" onclick="addAdministrator(3);"><spring:message code="lblAddAmin"/></a>
						</c:if>  	
<!--	                  <a href="javascript:void(0);" onclick="addAdministrator(3);"><spring:message code="lblAddAmin"/></a>-->
	                  </c:if>
	                   </div>
                   </div>
                </div>              
                
                 <div class="row col-sm-10 col-md-10">                  
                    <div id="schoolAdministratorDiv" class="row">
                    </div>                  
                </div>
                
                 
                <div class="row">
                  <div class="col-sm-10 col-md-10 top20"><label><spring:message code="lblAlys"/></label>
	                  <div class="pull-right"> 
	                      <c:if test="${fn:indexOf(roleAccessU,'|1|')!=-1}">
	                       <c:if test="${not empty userSession.districtId.saAddUser }">
                   				<c:if test="${userSession.entityType eq 3 and userSession.districtId.saAddUser eq true}">	
								<a href="javascript:void(0);" onclick="addAdministrator(6);"><spring:message code="lnkAddAlys"/></a>	
								</c:if>  
                   			</c:if>  
                   			<c:if test="${userSession.entityType ne 3 }">	
								<a href="javascript:void(0);" onclick="addAdministrator(6);"><spring:message code="lnkAddAlys"/></a>	
							</c:if>
<!--		                  <a href="javascript:void(0);" onclick="addAdministrator(6);"><spring:message code="lnkAddAlys"/></a>-->
		                  </c:if>
	                  </div>
                  </div>
                </div>  
              
                
                <div class="row col-sm-10 col-md-10">                  
                    <div id="schoolAnalystDiv" class="row">
                    </div>                  
                </div>

                <div id="addAdministratorDiv" class="hide" style="padding-top: 8px;">
                <div class="row col-sm-10 col-md-10">
                <div class="row">
                     <div class="col-sm-3 col-md-3">
						<label><strong><spring:message code="lblSalutation"/></strong></label>
						<select class="form-control" id="salutation" name="salutation">
							<option value="1"><spring:message code="optMr"/></option>
							<option value="2"><spring:message code="optMs"/></option>
							<option value="3"><spring:message code="optMrs"/></option>
							<option value="4"><spring:message code="optDr"/></option>
						</select>
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblFname"/></strong><span class="required">*</span></label>
                    	 <input class=" form-control" type="hidden" id="roleId" name="roleId" value="" placeholder="">
                    	 <input class=" form-control" type="text" id="firstName" name="firstName"  maxlength="50"placeholder="">
					</div>
					<div class="col-sm-3 col-md-3">
                    	<label><strong><spring:message code="lblLname"/></strong><span class="required">*</span></label>
<!--                    	 <input class=" span3" type="text" placeholder="">-->
						<input class=" form-control" type="text" id="lastName" name="lastName"  maxlength="50"placeholder="">
					</div>
				</div>
			   </div>	
			   
                 <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-9 col-md-9">
                    	<label><strong><spring:message code="lblTitle"/></strong></label>
                    	<input class=" form-control" type="text" id="title" name="title"  maxlength="200" placeholder="">
                  </div>
                </div>
                </div>
                
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-9 col-md-9">
                    	<label><strong><spring:message code="lblEmail"/></strong><span class="required">*</span></label>
						<input class=" form-control" type="text" id="emailAddress" name="emailAddress"  maxlength="75"placeholder="">
                  </div>
                  </div>
                 </div>
                 
                <div class="row col-sm-10 col-md-10">
                   <div class="row">
					<div class="col-sm-5 col-md-5">
                    	<label><strong><spring:message code="lblPhone"/></strong></label>
						<input class=" form-control" type="text" id="phone" name="phone"  maxlength="20"placeholder="">
                  	</div>
                  	<div class="col-sm-4 col-md-4">
                    	<label><strong><spring:message code="lblMobile"/></strong></label>
                    	<input class=" form-control" type="text" id="mobileNumber" name="mobileNumber"  maxlength="20"placeholder="">
                  	</div>
                </div>
                </div>
                
                <div class="row">
                  <div class="col-sm-3 col-md-3 idone"><a href="javascript:void(0);" onclick="validateSchoolAdministratorOrAnalyst();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearUser();">Cancel</a></div>
                </div>
                
            </div><%-- End of addAdministratorDiv --%>
                
                
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>
        
        <%-- ================ Gagan : Adding accordion Div for Social ======================  --%>
	    <div class="panel panel-default">
          <div class="accordion-heading">  <a href="#collapseFour" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"> Social </a> </div>
          <div class="accordion-body collapse" id="collapseFour" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
<%----------------- Gagan :School Social offset Div -----------------------------  --%>
              
              <div id="errordistrictdiv" class="required"></div>

                
                   <div class="row mt10">
                  		<div class="col-sm-12 col-md-12">
	                      <spring:message code="lblSolNetPost"/>
					  	</div>
                  	</div>
                  
                 <div class="row">
	                  <div class="col-sm-12 col-md-12">
	                        <label class="checkbox" style="margin-bottom: 0px;">
							 <form:checkbox path="postingOnDistrictWall" id="postingOnDistrictWall"  value="1"/>
			                <spring:message code="msgPostJobLnk"/><a href="#" id="iconpophover12" rel="tooltip" data-original-title="The box will be On or Off based on the settings in the District"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> 
							</label>
					  </div>
                  </div>
                  
                   <div class="row">
	                  <div class="col-sm-12 col-md-12">
	                        <label class="checkbox" style="margin-bottom: 0px;">
	                        <form:checkbox path="postingOnSchoolWall" id="postingOnSchoolWall"  value="1"/>
							<spring:message code="msgPostJobLnkOnSch"/>

							</label>
					   </div>
                   </div>
                  
                    <div class="row">
	                  <div class="col-sm-12 col-md-12">
	                        <label class="checkbox" style="margin-bottom: 0px;">
	                        <form:checkbox path="postingOnTMWall" id="postingOnTMWall"   disabled="${disableForTm}"  value="1"/>
						   	<spring:message code="msgAllowAlJoLnkSch"/> <a href="#" id="iconpophover13" rel="tooltip" data-original-title='Posting on TeacherMatch social network may provide enhanced visibilty'><img src="images/qua-icon.png" width="15" height="15" alt=""></a> 
							</label>
					   </div>
                   </div>
                
                
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>    
        
        
        
        
        <div class="panel panel-default">
          <div class="accordion-heading"> <a href="#collapseFive" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"><spring:message code="lblMos"/></a> </div>
          <div class="accordion-body collapse" id="collapseFive" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">
<%----------------- Gagan : Mosaic offset Div collapseSix -----------------------------  --%>
              <div class="row col-sm-12 col-md-12">
	              <div id="errorschooldiv" class="required"></div>
               	  <div class='required' id='errormosaicinfodiv'></div>
			   </div> 
             
				   <div class="row mt10">
                  			<div class="col-sm-4 col-md-4">
                  		<spring:message code="lblCanDFeeCret"/> <a href="#" id="iconpophover17" rel="tooltip" data-original-title="Set criteria to view the Candidates in your Action Feed on Mosaic" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                		</div>
                  </div>

                  <div class="row mt10"> 
                  <fmt:bundle basename="resource"><fmt:message key="sliderMaxLength" var="sliderMaxLength" /></fmt:bundle>
                  <fmt:bundle basename="resource"><fmt:message key="sliderRationMaxLength" var="sliderRationMaxLength" /></fmt:bundle>
                  
             <%--------------------------- Slider Candidate Feed Functionality Check Start here ------------------------------------ --%>
                 <c:choose>
				    <c:when test="${empty schoolMaster.candidateFeedNormScore}">
				    <%--   <fmt:bundle basename="resource"><fmt:message key="defaultcandidateFeedNormScore" var="candidateFeedNormScoreValue" /></fmt:bundle> --%>
				       	<c:choose>
						    <c:when test="${empty schoolMaster.districtId.candidateFeedNormScore}">
						       <fmt:bundle basename="resource"><fmt:message key="defaultcandidateFeedNormScore" var="candidateFeedNormScoreValue" /></fmt:bundle>
						    </c:when>
						    <c:otherwise>
						        <c:set var="candidateFeedNormScoreValue" value="${schoolMaster.districtId.candidateFeedNormScore}"></c:set>
						    </c:otherwise>
						</c:choose>
				       
				    </c:when>
				    <c:otherwise>
				        <c:set var="candidateFeedNormScoreValue" value="${schoolMaster.candidateFeedNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty schoolMaster.candidateFeedDaysOfNoActivity}">
				       	
				       	<c:choose>
						    <c:when test="${empty schoolMaster.districtId.candidateFeedDaysOfNoActivity}">
						       <fmt:bundle basename="resource"><fmt:message key="defaultcandidateFeedDaysOfNoActivity" var="candidateFeedDaysOfNoActivityValue" /></fmt:bundle>
						    </c:when>
						    <c:otherwise>
						        <c:set var="candidateFeedDaysOfNoActivityValue" value="${schoolMaster.districtId.candidateFeedDaysOfNoActivity}"></c:set>
						    </c:otherwise>
						</c:choose>
				       	
				    </c:when>
				    <c:otherwise>
				        <c:set var="candidateFeedDaysOfNoActivityValue" value="${schoolMaster.candidateFeedDaysOfNoActivity}"></c:set>
				    </c:otherwise>
				</c:choose>
				
			 <%--------------------------- Slider Functionality Check ENDDDDDDDDDDD here ------------------------------------ --%>
				
          <%--         GAgan     <c:out value="${candidateFeedNormScoreValue } "></c:out> value--------  ${schoolMaster.districtId.canSchoolOverrideCandidateFeed} --%> 
               	
               	<c:choose>
				    <c:when test="${schoolMaster.districtId.canSchoolOverrideCandidateFeed eq true}">
				       <c:set var="dsliderCandidateFeedValue" value="1"></c:set>
				    </c:when>
				    <c:otherwise>
				        <c:set var="dsliderCandidateFeedValue" value="0"></c:set>
				    </c:otherwise>
				</c:choose>
                
                <c:choose>
				    <c:when test="${schoolMaster.districtId.canSchoolOverrideJobFeed eq true}">
				       <c:set var="dsliderJobFeedValue" value="1"></c:set>
				    </c:when>
				    <c:otherwise>
				        <c:set var="dsliderJobFeedValue" value="0"></c:set>
				    </c:otherwise>
				</c:choose>  
                  
                  
                  
                  			<div class="col-sm-12 col-md-12">
								<label> <spring:message code="msgPostCandActInMos"/></label>
							</div>
                  
							<div class="col-sm-4 col-md-4" style="max-width: 250px;">
								<label>  <spring:message code="lblCondNoScrHi"/>  </label>
							</div>
							<div class="col-sm-8 col-md-8" style=" margin-top: -8px;"> 
								<label> 
									<iframe id="ifrm1"  src="slideract.do?name=slider1&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedNormScoreValue}&dslider=${dsliderCandidateFeedValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden cssClass="span2" path="candidateFeedNormScore" value="${candidateFeedNormScoreValue }"/>
								</label>
							</div>
							
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&"/> </label>
							</div>
							
							<div class="col-sm-6 col-md-6" style="max-width: 390px;">
								<label class="radio">
								<input type="hidden" id="mosaicRadiosFlag" name="mosaicRadiosFlag" value="1">
								<input type="radio" <c:if test="${!schoolMaster.districtId.canSchoolOverrideCandidateFeed}">disabled="disabled" </c:if>  name="mosaicRadios" id="mosaicRadios1" value="1" <c:if test="${candidateFeedDaysOfNoActivityValue ge 0}">checked </c:if> onclick="disableSlider()">
									<%--<form:radiobutton path="noSchoolUnderContract"  value="1" />--%>
			                     <spring:message code="msgCommWithCand"/>
		                        </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px;">
								<label>
								<c:set var="dsliderValueWhendbValue0" value="${dsliderCandidateFeedValue}"></c:set>
								<c:if test="${dsliderCandidateFeedValue eq 1}">
									<c:if test="${candidateFeedDaysOfNoActivityValue eq 0}">
										<c:set var="dsliderValueWhendbValue0" value="0"></c:set>
									</c:if>
								</c:if>
								<iframe id="ifrm2"  src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedDaysOfNoActivityValue}&dslider=${dsliderValueWhendbValue0}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe> 
									<form:hidden cssClass="span2" path="candidateFeedDaysOfNoActivity" value="${candidateFeedDaysOfNoActivityValue }"/>
								</label>
							</div>
							
							<div class="col-sm-2 col-md-2" style="min-width: 140px;">
								<label>&nbsp;&nbsp; <spring:message code="lblDyApp"/></label>
							</div>
							
							<div style="clear: both;"></div>
							<div class="col-sm-12 col-md-12" style="margin-top: -10px;">
								<label class="radio">
								<input type="radio"  <c:if test="${!schoolMaster.districtId.canSchoolOverrideCandidateFeed}">disabled="disabled" </c:if>   name="mosaicRadios" id="mosaicRadios2" value="0" <c:if test="${candidateFeedDaysOfNoActivityValue eq 0}">checked </c:if> onclick="disableSlider()" >
									<%--<form:radiobutton path="noSchoolUnderContract"  value="1" />--%>
			                        <spring:message code="lblTheyHvApp"/>
		                        </label>
							</div>
                   </div>
                  
                  <div class="row">
                  		<div class="col-sm-12 col-md-12">
                    		<spring:message code="lblJoFeeCrit"/> <a href="#" id="iconpophover14" rel="tooltip" data-original-title="Set criteria to view the jobs in your Action Feed on Mosaic"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  </div>
                  
                  
                  <div class="row">                  
                  <c:choose>
				    <c:when test="${empty schoolMaster.jobFeedCriticalJobActiveDays}">
				       
				       <c:choose>
						    <c:when test="${empty schoolMaster.districtId.jobFeedCriticalJobActiveDays}">
						       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalJobActiveDays" var="jobFeedCriticalJobActiveDaysValue" /></fmt:bundle>
						    </c:when>
						    <c:otherwise>
						        <c:set var="jobFeedCriticalJobActiveDaysValue" value="${schoolMaster.districtId.jobFeedCriticalJobActiveDays}"></c:set>
						    </c:otherwise>
						</c:choose>
				       
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalJobActiveDaysValue" value="${schoolMaster.jobFeedCriticalJobActiveDays}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty schoolMaster.jobFeedCriticalCandidateRatio}">
				       
				       <c:choose>
						    <c:when test="${empty schoolMaster.districtId.jobFeedCriticalCandidateRatio}">
						       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalCandidateRatio" var="jobFeedCriticalCandidateRatioValue" /></fmt:bundle>
						    </c:when>
						    <c:otherwise>
						        <c:set var="jobFeedCriticalCandidateRatioValue" value="${schoolMaster.districtId.jobFeedCriticalCandidateRatio}"></c:set>
						    </c:otherwise>
						</c:choose>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalCandidateRatioValue" value="${schoolMaster.jobFeedCriticalCandidateRatio}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty schoolMaster.jobFeedCriticalNormScore}">
				       <c:choose>
						    <c:when test="${empty schoolMaster.districtId.jobFeedCriticalNormScore}">
						       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalNormScore" var="jobFeedCriticalNormScoreValue" /></fmt:bundle>
						    </c:when>
						    <c:otherwise>
						        <c:set var="jobFeedCriticalNormScoreValue" value="${schoolMaster.districtId.jobFeedCriticalNormScore}"></c:set>
						    </c:otherwise>
						</c:choose>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalNormScoreValue" value="${schoolMaster.jobFeedCriticalNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
<%-------------- Label -------------------- --%>				
				<c:choose>
				    <c:when test="${empty schoolMaster.lblCriticialJobName}">
				       
				       <c:choose>
						    <c:when test="${empty schoolMaster.districtId.lblCriticialJobName}">
						       <c:set var="lblCriticialJobNameValue" value="Critical"></c:set>
						    </c:when>
						    <c:otherwise>
						        <c:set var="lblCriticialJobNameValue" value="${schoolMaster.districtId.lblCriticialJobName}"></c:set>
						    </c:otherwise>
						</c:choose>
				    </c:when>
				    <c:otherwise>
				        <c:set var="lblCriticialJobNameValue" value="${schoolMaster.lblCriticialJobName}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				 <c:choose>
				    <c:when test="${empty schoolMaster.jobFeedAttentionJobActiveDays}">
				       
				        <c:choose>
						    <c:when test="${empty schoolMaster.districtId.jobFeedAttentionJobActiveDays}">
						       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedAttentionJobActiveDays" var="jobFeedAttentionJobActiveDaysValue" /></fmt:bundle>
						    </c:when>
						    <c:otherwise>
						        <c:set var="jobFeedAttentionJobActiveDaysValue" value="${schoolMaster.districtId.jobFeedAttentionJobActiveDays}"></c:set>
						    </c:otherwise>
						</c:choose>
				       
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedAttentionJobActiveDaysValue" value="${schoolMaster.jobFeedAttentionJobActiveDays}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty schoolMaster.jobFeedAttentionNormScore}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedAttentionNormScore" var="jobFeedAttentionNormScoreValue" /></fmt:bundle>
				       
				       <c:choose>
						    <c:when test="${empty schoolMaster.districtId.jobFeedAttentionNormScore}">
						       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedAttentionNormScore" var="jobFeedAttentionNormScoreValue" /></fmt:bundle>
						    </c:when>
						    <c:otherwise>
						        <c:set var="jobFeedAttentionNormScoreValue" value="${schoolMaster.districtId.jobFeedAttentionNormScore}"></c:set>
						    </c:otherwise>
						</c:choose>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedAttentionNormScoreValue" value="${schoolMaster.jobFeedAttentionNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty schoolMaster.lblAttentionJobName}">
				        
				        <c:choose>
						    <c:when test="${empty schoolMaster.districtId.lblAttentionJobName}">
						        <c:set var="lblAttentionJobNameValue" value="Attention"></c:set>
						    </c:when>
						    <c:otherwise>
						        <c:set var="lblAttentionJobNameValue" value="${schoolMaster.districtId.lblAttentionJobName}"></c:set>
						    </c:otherwise>
						</c:choose>
				        
				    </c:when>
				    <c:otherwise>
				        <c:set var="lblAttentionJobNameValue" value="${schoolMaster.lblAttentionJobName}"></c:set>
				    </c:otherwise>
				</c:choose>
							
			 <%--------------------------- Slider Functionality Check ENDDDDDDDDDDD here ------------------------------------ --%>
				
                  			<div class="col-sm-4 col-md-4 top15">	
							<spring:message code="lblPostJoInMosaic"/>
							</div>	
							<div class="col-sm-3 col-md-3 top10" style="margin-left: -25px;">	
								<form:input path="lblCriticialJobName" cssClass="form-control" value="${lblCriticialJobNameValue}" maxlength="50" disabled="${!schoolMaster.districtId.canSchoolOverrideJobFeed}" />
							</div>
							<div class="col-sm-1 col-md-1 top15" style="margin-left: -25px;">
							  ,&nbsp;if ${schoolMaster.districtId.canSchoolOverrideCandidateFeed}
							</div>
							
							<div class="clearfix"></div>
							<div class="col-sm-3 col-md-3" style="max-width: 170px;">
								<label> <spring:message code="lblJobActive"/> </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px;">
								<label>
							        <iframe id="ifrm3"  src="slideract.do?name=slider3&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedCriticalJobActiveDaysValue}&dslider=${dsliderJobFeedValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedCriticalJobActiveDays" cssClass="span2" value="${jobFeedCriticalJobActiveDaysValue}"/>
								</label>
							</div>
							
							<div class="col-sm-1 col-md-1 top7" style="min-width: 60px;">
								<label>&nbsp; <spring:message code="lblDays"/></label>
							</div>
							
								<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&"/>  </label>
							</div>
							
							<div class="col-sm-6 col-md-6" style="max-width: 400px;">
								<label> <spring:message code="msgCondRatio"/>  </label>
							</div>
							
							<div class="col-sm-3 col-md-3">
								<label> 
								    <iframe id="ifrm4"  src="slideract.do?name=slider4&tickInterval=1&max=${sliderRationMaxLength }&swidth=200&svalue=${jobFeedCriticalCandidateRatioValue}&dslider=${dsliderJobFeedValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedCriticalCandidateRatio" cssClass="span2" value="${jobFeedCriticalCandidateRatioValue}"/>
								</label>
							</div>
							
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lblWhere"/>  </label>
							</div>
							
							<div class="col-sm-5 col-md-5" style="max-width: 290px;">
								<label><spring:message code="lblAppScoreGtrThan"/>  </label>
							</div>
							
							<div class="col-sm-7 col-md-7">
								<label>
								<iframe id="ifrm5"  src="slideract.do?name=slider5&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedCriticalNormScoreValue}&dslider=${dsliderJobFeedValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe> 
									<form:hidden path="jobFeedCriticalNormScore" cssClass="span2" value="${jobFeedCriticalNormScoreValue}"/>
								</label>
							</div>
							
							<div class="col-sm-4 col-md-4 top15">	
								<spring:message code="lblPostJoActInMosaic"/>
							</div>	
							<div class="col-sm-3 col-md-3 top10" style="margin-left: -25px;">	
								<form:input path="lblAttentionJobName" cssClass="form-control" value="${lblAttentionJobNameValue}" maxlength="50" disabled="${!schoolMaster.districtId.canSchoolOverrideJobFeed}"/>
							</div>
							<div class="col-sm-1 col-md-1 top15" style="margin-left: -25px;">
							  ,&nbsp;if ${schoolMaster.districtId.canSchoolOverrideJobFeed}
							</div>
													
							<div class="clearfix"></div>
							<div class="col-sm-3 col-md-3" style="max-width: 170px;">
								<label> <spring:message code="lblJobActive"/>   </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px">
								<label>
								<iframe id="ifrm6"  src="slideract.do?name=slider6&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedAttentionJobActiveDaysValue}&dslider=${dsliderJobFeedValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe> 
									<%-- <input type="text" class="span2" id="slidervalue3">--%>
									<form:hidden path="jobFeedAttentionJobActiveDays" cssClass="span2" value="${jobFeedAttentionJobActiveDaysValue}"/>
								</label>
							</div>
							
							<div class="col-sm-1 col-md-1 top7" style="min-width: 60px">
								<label>&nbsp;<spring:message code="lblDays"/> </label>
							</div>
							
								<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&"/>  </label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label class="checkbox">  
									<form:checkbox path="jobFeedAttentionJobNotFilled" disabled="${!schoolMaster.districtId.canSchoolOverrideJobFeed}" value="1"/> if not filled completely (still has openings)
								</label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&"/>  </label>
							</div>
							
							<div class="col-sm-4 col-md-4" style="max-width: 280px;">
								<label> <spring:message code="lblAppScoreGtrThan"/>  </label>
							</div>
							
							<div class="col-sm-4 col-md-4">
								<label>
								<iframe id="ifrm7"  src="slideract.do?name=slider7&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedAttentionNormScoreValue}&dslider=${dsliderJobFeedValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe> 
									<form:hidden path="jobFeedAttentionNormScore" cssClass="span2" value="${jobFeedAttentionNormScoreValue}"/>
								</label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label>    <spring:message code="msgNotCommWithMsgPhone"/></label>
							</div>							
                   </div>
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>    
        
        
        
	   <div class="panel panel-default">
          <div class="accordion-heading">  <a href="#collapseSix" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle plus"><spring:message code="lblAccInfo"/> </a> </div>        
            <div class="accordion-body collapse" id="collapseSix" style="height: 0px;">
            <div class="accordion-inner">
              <div class="offset1">                                
                  <div class="row">
		                <div class="">
							<div class='required' id='errorschoolaccountinfodiv'><%--- Gagan--%></div>
							<div class='required' id='errorschoolnotediv'></div>
							<div class='required' id='errordatediv'></div>
						</div>	
					</div>	
					<div class="row">
		                <div class="col-sm-12 col-md-12" style="margin-left:-15px;"> 	
		                  <p> <spring:message code="msgInitialContractingProcess"/></a></p>
		                </div>
	                </div>                  
                </div>
                
                 <c:if test="${userSession.entityType ne 1}">	
        			<c:set var="disable" value="true"></c:set>
         		</c:if>
                
                <div class="row">
                  <div class="span12 offset1">
                     <div class="row">
                        <div class="col-sm-3 col-md-3">
                          <label><spring:message code="lblDateInit"/></label>
						<form:input path="initiatedOnDate" disabled="${disable}" cssClass="form-control" value="${initiatedOnDate}"/>
						</div>
                        <div class="col-sm-3 col-md-3">
                          <label><spring:message code="lblcontStDate"/></label>
						<form:input path="contractStartDate"  disabled="${disable}" cssClass="form-control"   value="${contractStartDate}"/>
						</div>
                        <div class="col-sm-3 col-md-3">
                          <label><spring:message code="lblContEdDate"/></label>
						<form:input path="contractEndDate"  disabled="${disable}" cssClass="form-control"  value="${contractEndDate}"/>
						</div>
                    </div>
                    
                    <div class="row">
                      <div class="col-sm-3 col-md-3">
                        <label><spring:message code="lblTchUdrCont"/></label>
						<form:input path="teachersUnderContract"  disabled="${disable}" cssClass="form-control" maxlength="9" placeholder=""/>
                      </div>
                      <div class="col-sm-3 col-md-3">
                        <label><spring:message code="lblStudUdrCont"/></label>
						<form:input path="studentsUnderContract"  disabled="${disable}" cssClass="form-control" maxlength="9" placeholder=""/>
                      </div>
                    </div>
                    
                    <div class="row">
                      <c:if test="${userSession.entityType eq 1}">	
                      	 <div class="col-sm-6 col-md-6">
                      	<label><spring:message code="lblAuthKy"/></label>
						<input type="text" name="authKeyVal" readonly="readonly" class="form-control" value="${authKey}"   />
	                 	</div>
                     </c:if>
                  </div>
                    
                    <%-- Add Notes Field ! It will display only for Tm --%>
                  <c:if test="${userSession.entityType eq 1}">	 
	                  <div class="row top20">
	                    <div class="col-sm-9 col-md-9">
	                     <spring:message code="headNot"/>	                   
	                   <div class="pull-right">
	                      <c:if test="${fn:indexOf(roleAccessAI,'|1|')!=-1}">
	                    <a href="javascript:void(0);" onclick="addNotes();"><spring:message code="lnkAddNote"/></a>
	                    </c:if>
	                    </div>
	                    </div>
	                  </div>
	                  
	                  <div class="row">
	                    <div class="col-sm-10 col-md-10">
		                  <div id="schoolNotesGrid">
			                  <table id="schoolNotesTable" width="100%" border="0" class="table table-bordered mt10">
			                  </table>
		                  </div>
		                  </div>
	                  </div>
	                  
	                  <div id="addNotesDiv"  class="hide">
		                  <div class="row">
		                    <div class="col-sm-9 col-md-9"  id="sNote">
		                      <label><spring:message code="headNot"/></label>
		                      <label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 							  </label>
		                      <textarea class="span12" id="note" name="note" maxlength="500" rows="2" onkeyup="return chkLenOfTextArea(this,500);" onblur="return chkLenOfTextArea(this,500);"></textarea>
		                    </div>
		                  </div>
		                  
		                   <div class="row col-sm-4 col-md-4">
	                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return saveNotes();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearNotes();"><spring:message code="lnkCancel"/></a></div>
	                	  </div>
	                  </div>
	                  
        		 </c:if>	<%-- Add Notes check end here  --%>		
                  
                   	  <div class="clearfix"></div>
	        		   <div class="row col-sm-12 col-md-12 left5 top10">	                  
		                  <label class="checkbox inline"   style="padding-left: 0px;">
		                    <input type="hidden" name="aJobRelation" value="${aJobRelation}"/>
		                   <input type="checkbox" ${aJobRelation} id="cbxUploadAssessment" name="cbxUploadAssessment" onclick="uploadAssessment()" ${(schoolMaster.assessmentUploadURL != null ) ? 'checked' : ''}>
		                 <spring:message code="lblAttachDefJoSpeciInventory"/>
		                  </label>
	                  </div>
	                  
                  <div  id="uploadAssessmentDiv" class="row hide top10" >					
						<c:if test="${fn:indexOf(roleAccessAI,'|9|')!=-1}">
						<div class="col-sm-12 col-md-12">
						<spring:message code="msgUnloadJoSpeciInventory"/>
						</div>
						<div class="col-sm-4 col-md-4">
						<form:input path="assessmentUploadURLFile" type="file"/>
						</div>
						</c:if>
						<div class="col-sm-4 col-md-4">
						<a  data-original-title='School' rel='tooltip' id='JSISchool' href='javascript:void(0)'   onclick="showFile(2,'school','${schoolMaster.schoolId}','${schoolMaster.assessmentUploadURL}','JSISchool');${windowFunc}" ><c:out value="${(schoolMaster.assessmentUploadURL==''||schoolMaster.assessmentUploadURL==null)?'':'view'}"/></a>
						</div>				
                  </div>
                  
                   <div class="row mt10">
	                  <div class="col-sm-12 col-md-12">
                   <spring:message code="lblJoBrdURL"/>
                    <div class="">
						<a target="blank" href="${JobBoardURL}">${JobBoardURL}</a>
                    </div>
                  </div>
                  </div>
                  
                   <c:if test="${userSession.entityType ne 1}">	
        			<c:set var="disableForTm" value="true"></c:set>
         		   </c:if>
                  
                   
                    <c:if test="${userSession.entityType eq 1}">
	                   <div class="row top10 left5">
			                        <div class="col-sm-4 col-md-4">
										<label class="checkbox inline" style="padding-left: 0px;">
		                        <form:checkbox path="isPortfolioNeeded" id="isPortfolioNeeded"  value="1"/>
								<spring:message code="lblIsPortNed"/> <a href="#" id="iconpophover15" rel="tooltip" data-original-title="Select this checkbox if you want the Candidates to complete their Portfolio first to complete the jobs of this school. Unselect this checkbox if you allow the Candidates to complete the jobs of this school without completing the Portfolio."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
								</label>
						   </div>
	                   </div>
	                   
	                   <div class="row mt10 left5">
		                  <div class="col-sm-12 col-md-12">
		                        <label class="checkbox inline" style="padding-left: 0px;">
		                        <form:checkbox path="isWeeklyCgReport" id="isWeeklyCgReport"  value="1"/>
								<spring:message code="lblWekCGRept"/> <a href="#" id="iconpophover16" rel="tooltip" data-original-title="Select this checkbox if you want to receive the links for weekly CG Reports for all your jobs, in your mail box. Unselect this checkbox if you do not want receive the links for weekly CG Reports for all your jobs, in your mail box."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
								</label>
						  </div>
					   </div>
				   </c:if>
				   
                   <div class="row mt10 left5">
	                  <div class="col-sm-6 col-md-6">
	                        <label class="checkbox inline" style="padding-left: 0px;">
	                        <form:checkbox path="allowMessageTeacher" id="allowMessageTeacher"   onclick="emailForTeacherDivCheck();"   value="1"/>
						<spring:message code="lblAllMsgfrTch"/>
							</label>
					   </div>
                   </div>
                 
                 
                  <%-- Add District Field ! It will display only for Tm --%>
                  <c:if test="${userSession.entityType eq 1}">	 
	                  <div class="row">
	                    <div class="col-sm-9 col-md-9">
	                      <spring:message code="hdAttachment"/>
	                    <div class="pull-right">
	                     <c:if test="${fn:indexOf(roleAccessAI,'|1|')!=-1}">
	                    	<a href="javascript:void(0);" onclick="addDistAttachment();"><spring:message code="lnkAddN"/></a>
	                     </c:if>
	                    </div>
	                     </div>
	                  </div>
	                  
	                   <div class="row">
	                    <div class="col-sm-10 col-md-10">
	                    <div id="distAttachmentGrid">
		                  <table id="distAttachmentTable" width="100%" border="0" class="table table-bordered mt10">
		                  </table>
	                    </div>
	                   </div>
	                  </div>
	                  
	                  <div id="distAttachmentDiv"  class="hide">
		                  <div class="row">
		                    <div class="col-sm-9 col-md-9" id="dNote">
		                      <label><spring:message code="hdAttachment"/></label>
		                      <label class="redtextmsg"><spring:message code="msgDistAttach"/></label>
	 				 		  <label><spring:message code="msgAcceptInventFileMaxSize10MB"/></label>
					<iframe id='uploadFrameID' name='uploadDistFrame' height='0' width='0'  frameborder='1' scrolling='yes'  sytle='display:none;'>
					</iframe>
					<form id='frmDistAttachmentUpload' enctype='multipart/form-data' method='post' target='uploadDistFrame' action='distAttachmentUploadServlet.do' class="form-inline">
						<table>
							<tr>
								<td>
									<input type="file" name="distAttachmentDocument" id="distAttachmentDocument">
								</td>
							</tr>
						</table>
					</form>
		                    </div>
		                  </div>
		                  <div class="row col-sm-4 col-md-4">
	                 		 <div class="span2 idone"><a href="javascript:void(0);" onclick="return uploadDistFile();"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearNotes();"><spring:message code="btnCancel"/></a></div>
	                	  </div>
	                  </div>
	                  
	                  <div id="distAttachmentEditDiv"  class="hide">
		                  <div class="row">
		                    <div class="col-sm-9 col-md-9" id="dNote">
		                      <label><spring:message code="hdAttachment"/></label>
		                      <label class="redtextmsg"><spring:message code="msgDistAttach"/></label>
	 				 		  <label><spring:message code="msgAcceptInventFileMaxSize10MB"/></label>
					<iframe id='uploadFrameID' name='uploadDistFrame' height='0' width='0'  frameborder='1' scrolling='yes'  sytle='display:none;'>
					</iframe>
					<form id='frmDistAttachmentUpload' enctype='multipart/form-data' method='post' target='uploadDistFrame' action='distAttachmentUploadServlet.do' class="form-inline">
						<table>
							<tr>
								<td>
									<input type="file" name="distAttachmentEditDocument" id="distAttachmentEditDocument">
								</td>
								<td>
									<span id="myModalViewDist"></span>
								</td>
							</tr>
						</table>
					</form>
		                    </div>
		                  </div>
		                  <div class="row col-sm-4 col-md-4">
	                 		 <div class="span2 idone"><span id="myModalEditDist"></span>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearNotes();"><spring:message code="btnCancel"/></a></div>
	                	  </div>
	                  </div>
	                  
        		 </c:if>
        		 <div class="clearfix"></div>  
                 
                 	<%-- Add District Attachment check end here  --%>
                 
                   <div id="emailForTeacherDiv"  class="hide row mt10">
                  	 <div class="col-sm-6 col-md-6">
                      <label><spring:message code="msgRecMsgFrTch"/></label>
						<form:input path="emailForTeacher" cssClass="form-control" maxlength="100"/>
					 </div>
	                </div>
                  
                    <input type="hidden" id="assessmentUploadURLVal" name="assessmentUploadURLVal" value="${(schoolMaster.assessmentUploadURL != null ) ? '1' : '0'}" />
                    <c:if test="${schoolMaster.flagForURL ne 1 && schoolMaster.flagForMessage ne 1}">	
        			<c:set var="checked" value=""></c:set>
         			<c:set var="checked" value="checked"></c:set>        			
        			</c:if>
                     <c:if test="${(schoolMaster.flagForMessage ne 1 && schoolMaster.flagForURL ne 1 && userSession.entityType eq 3)||(schoolMaster.flagForURL eq 1 && userSession.entityType eq 3)|| userSession.entityType eq 1}">
                    <div class="row">
                    <div class="col-sm-12 col-md-12">
                      <label class="radio">
                          <form:radiobutton path="flagForURL" value="1" onclick="uncheckedMessageRadio()" checked="${checked}" />
                       <spring:message code="msgCandHasFinishedInvent"/> <a href="#" id="iconpophover8" rel="tooltip" data-original-title="Lorem ipsum dolor sit&#013;&#10;amet, consectetur&#013;&#10;adipiscing elit. Morbi&#013;&#10;non quam nunc"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> </label>
                    
                       <div class="row">
                       <div class="col-sm-9 col-md-9">
                           <form:input path="exitURL" class="form-control" maxlength="250"/>
                        </div>
                        </div>
                        
                      </div>
                    </div>
                    </c:if>
                    
                    <c:if test="${(schoolMaster.flagForMessage eq 1 && userSession.entityType eq 3)|| userSession.entityType eq 1}">
               		  <div class="row">
                    	<div class="col-sm-12 col-md-12">
                      		<label class="radio">
	                        <form:radiobutton path="flagForMessage"  value="1" onclick="uncheckedUrlRadio()"  />
	                       <spring:message code="msgCandWillFinishTmInventAftrComplDistApp"/> <a href="#" id="iconpophover9" rel="tooltip" data-original-title="Lorem ipsum dolor sit&#013;&#10;amet, consectetur&#013;&#10;adipiscing elit. Morbi&#013;&#10;non quam nunc"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> </label>
                         </div>
                        </div>
                     
                    
                    <div class="row">
                    
                       <div class="col-sm-12 col-md-12">
                        <p><spring:message code="lblThankYuMsg"/> <a href="#" id="iconpophover8" rel="tooltip" data-original-title="Lorem ipsum dolor sit&#013;&#10;amet, consectetur&#013;&#10;adipiscing elit. Morbi&#013;&#10;non quam nunc"><img width="15" height="15" alt="" src="images/qua-icon.png"></a></p>
                       </div> 
                       
                       
                        <div class=""  id="eMessage">
	                        <div class="col-sm-9 col-md-9">
	                        <label  class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
	 						 </label>
	 						  </div>
	 						 <div class="col-sm-9 col-md-9">
								<form:textarea path="exitMessage" class="form-control" rows="3" maxlength="1000" onkeyup="return chkLenOfTextArea(this,1000);" onblur="return chkLenOfTextArea(this,1000);" />
	                        </div>
                        </div>
                     
                      
                    </div>
                    
                    </c:if>
                    <div class="clearfix"></div>
                  </div>
                </div>
              </div>
            </div>          
        </div>
        
      </div>      
    </div>
  </div>
  <div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);"><spring:message code="btnX"/></button>
			<h3 id="myModalLabel"><spring:message code="headKyCont"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(1);"><spring:message code="btnOk"/></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);"><spring:message code="btnClose"/></button> 		
 		</div>
	</div>
	</div> 
	</div>
	<div  class="modal hide distAttachmentShowDiv"  id="distAttachmentShowDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnX"/></button>
				<h3 id="UserProfileNameEEOC11"><spring:message code="hdAttachment"/></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideAttachmentDiv();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>
  <div class="mt30 mb30 ">
   		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
          <button type="button"  onclick="return validateEditSchool();" class="btn btn-large btn-primary" ><strong><spring:message code="lblSveSch"/><i class="icon"></i></strong></button>
        </c:if>
          &nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelSchool();"><spring:message code="lnkCancel"/></a>
  </div>
   <iframe src="" id="iframeJSI" width="100%" height="480px" style="display: none;">
 </iframe>  
  <div style="display:none;" id="loadingDiv">
		<table  align="center" >
			<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
			<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>
</form:form>
<script> 
	displaySchoolKeyContact()
	displayNotes()
	displaySchoolAdministrator(3);
	displaySchoolAnalyst(6);
	displayDistAttachment();
	document.getElementById("alternatePhone").focus();
	uploadAssessment(${(schoolMaster.assessmentUploadURL != null ) ? '1' : '0'});
	showFile(1,'school','${schoolMaster.schoolId}','${schoolMaster.logoPath}',null);
</script>
<script type="text/javascript">

var selectIds = $('#collapseOne,#collapseTwo,#collapseThree,#collapseFour,#collapseFive,#collapseSix');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
    })
});


$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});
</script>

<script>
	function disableSlider()
	{
		if($('#mosaicRadios1').is(':checked')) 
		{ 
	   		$("#mosaicRadiosFlag").val("1");
	   		$("#candidateFeedDaysOfNoActivity").val("0");
	   				try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedDaysOfNoActivityValue}&dslider=1";
    			}
	    		catch(e)
	    		{} 
	    }
	    else
	    {
	    	if($('#mosaicRadios2').is(':checked')) 
			{ 
	    	
	    		$("#mosaicRadiosFlag").val("0");
	    		$("#candidateFeedDaysOfNoActivity").val("0");
	    		try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=0&dslider=0";
    			}
	    		catch(e)
	    		{} 
		    }
	    }
	}
	
</script>

<script type="text/javascript">
$('#myModal').modal('hide');
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophover7').tooltip();
$('#iconpophover8').tooltip();
$('#iconpophover9').tooltip();
$('#iconpophover10').tooltip();
$('#iconpophover11').tooltip();
$('#iconpophover12').tooltip();
$('#iconpophover13').tooltip();
$('#iconpophover14').tooltip();
$('#iconpophover15').tooltip();
$('#iconpophover16').tooltip();
$('#iconpophover17').tooltip();
$(document).ready(function(){
		//$('#eMessage').find(".jqte").width(724);
		//$('#sNote').find(".jqte").width(724);
		$('#sDescription').find(".jqte").width(535);
}) 

function emailForTeacherDivCheck(){
	var allowMessageTeacher=document.getElementById("allowMessageTeacher").checked;
	if(allowMessageTeacher==1){
		$("#emailForTeacherDiv").fadeIn();
		$("#emailForTeacher").focus();
	}else{
		$("#emailForTeacherDiv").hide();
		$("#allowMessageTeacher").focus();
		document.getElementById("emailForTeacher").value="";
	}
}
emailForTeacherDivCheck();
</script>
<script type="text/javascript">//<![CDATA[
    var cal = Calendar.setup({
        onSelect: function(cal) { cal.hide() },
        showTime: true
    });
  	 cal.manageFields("initiatedOnDate", "initiatedOnDate", "%m-%d-%Y");
     cal.manageFields("contractStartDate", "contractStartDate", "%m-%d-%Y");
     cal.manageFields("contractEndDate", "contractEndDate", "%m-%d-%Y");
    //]]></script>
