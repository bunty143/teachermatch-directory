<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript"
	src="dwr/interface/EventAjax.js?ver=${resourceMap['EventAjax.ajax']}"></script>
<script type="text/javascript"
	src="dwr/interface/ManageTemplateAjax.js?ver=${resourceMap['ManageTemplateAjax.ajax']}"></script>

<script type="text/javascript"
	src="js/manageevents.js?ver=${resourceMap['js/manageevents.js']}"></script>
	<script type="text/javascript"
	src="js/managetemplate.js?ver=${resourceMap['js/managetemplate.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript"
	src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax']}"></script>
<script type="text/javascript"
	src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>

<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css"
	href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type='text/javascript' src="js/bootstrap-slider.js"></script>
<link rel="stylesheet" type="text/css"
	href="css/base.css?ver=${resourceMap['css/base.css']}" />
<link rel="stylesheet" type="text/css" href="css/slider.css" />

<style>
.btn-dangerevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-dangerevents:hover,
.btn-dangerevents:focus,
.btn-dangerevents:active,
.btn-dangerevents.active,
.btn-dangerevents.disabled,
.btn-danger[disabled] {
  color: #ffffff;
  background-color: #bd362f;
  *background-color: #a9302a;
}

.btn-dangerevents:active,
.btn-dangerevents.active {
  background-color: #942a25 \9;
}

.btn-warningevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #faa732;
  *background-color: #f89406;
  background-image: -moz-linear-gradient(top, #fbb450, #f89406);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));
  background-image: -webkit-linear-gradient(top, #fbb450, #f89406);
  background-image: -o-linear-gradient(top, #fbb450, #f89406);
  background-image: linear-gradient(to bottom, #fbb450, #f89406);
  background-repeat: repeat-x;
  border-color: #f89406 #f89406 #ad6704;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-warningevents:hover,
.btn-warningevents:focus,
.btn-warningevents:active,
.btn-warningevents.active,
.btn-warningevents.disabled,
.btn-warningevents [disabled] {
  color: #ffffff;
  background-color: #f89406;
  background-color: #df8505;
}

.btn-warningevents:active,
.btn-warningevents.active {
  background-color: #c67605 \9;
}

strong {
font-weight: normal;
}

</style>
<script>
function applyScrollOnTblEvent()
{
	if($("#dynamicColonm").val()==0)
	{
			var $j=jQuery.noConflict();
	        $j(document).ready(function() {
	        $j('#templateTable').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        //height: 280,
	        width: 862,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:[290,280,150,120], // table header width
	        addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        //sortType:[],
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });
	   });
	}
	else if($("#dynamicColonm").val()==1)
	{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#templateTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 862,
        minWidth: null,
        minWidthAuto: false,
        colratio:[540,170,140], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   		});
	}
}

</script>


<div class="row"
	style="margin-left: 0px; margin-right: 0px; margin-top: ">
	<div style="float: left;">
		<img src="images/manageusers.png" width="41" height="41" alt="">
	</div>
	<div style="float: left;">
		<div class="subheading" style="font-size: 13px;">

		<spring:message code="headMangeTmp"/>
	

		</div>
	</div>
	<div class="pull-right add-employment1 hide" id="addLink">
		<a href="javascript:void(0);" onclick="return addTemplate()"><spring:message code="lnkAddTmp"/></a>
	</div>
	<div style="clear: both;"></div>
	<div class="centerline"></div>
</div>

 
    <div class="row" id="eventsearchDiv">
    <% String id=session.getAttribute("displayType").toString();%>
				<input type="hidden" id="displayTypeId" value="<%=id%>"/>
				<%if(id.equalsIgnoreCase("0")){ %>
       		<div class="col-sm-5 col-md-5">
				<label>
				<spring:message code="lblDistrict"/>
				</label>
				<br />
				<c:if test="${districtName==null}">
				<input type="text" id="districtName" name="districtName"
					class="form-control"
					onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');onDistrictChange();" />
					<input type="hidden" id="districtId" value="${districtId}" />
				</c:if>
				<c:if test="${districtName!=null}">
			             	<input type="hidden" id="districtId" value="${districtId}" />
							<input type="text" id="districtName" value="${districtName}"  class="form-control" name="districtName" disabled="disabled"/>
				</c:if>
				<div id='divTxtShowData'
					style='display: none; position: absolute; z-index: 5000;'
					onmouseover="mouseOverChk('divTxtShowData','districtName')"
					class='result'>
				</div>
			</div>
       		<%}%>
       		<div class="col-sm-3 col-md-3">
				<label><spring:message code="lblTemplateType"/></label>
				<select class="form-control" disabled="disabled" id="templateType" name="templateType" class="span3" onchange="showStatusDiv();">
					<option value="0" selected="selected"><spring:message code="sltsltTmpTpe"/></option>
					<option value="1"><spring:message code="optCommu"/></option>
					<option value="2"><spring:message code="optEventDescr"/></option>
					<option value="3"><spring:message code="optEventFacil"/></option>
					<option value="4"><spring:message code="optEventPartic"/></option>
					<option value="5"><spring:message code="optStatus"/></option>
				</select>
			</div>
            
            <div class="hide" id="statusDiv">
	            <div class="col-sm-4 col-md-4" >
	            		<label><spring:message code="lblStatusName"/></label>
						<select class="form-control " id="statusName" name="statusName" class="span3" onchange="onDistrictChange();"></select>
				</div>
				<%-- 
				<div class="" >
	            		</br>
	            		<a href="#" onclick="openAddStatuaDivForStatus();">Add Status</a>
				</div>
				--%>
			</div>          
		
		<div class="col-sm-2 col-md-2 top26" style="width: 150px;">
		        <label class=""></label>
		        <button class="btn btn-primary " type="button" onclick="searchEventByButton();">&nbsp;&nbsp;&nbsp;<spring:message code="btnSearch"/>&nbsp;&nbsp;&nbsp;<i class="icon"></i></button>
		 </div>
		
	</div>       



<!--********************** Main Grid *************************-->

    <div class="TableContent top15" id="maineventtableDiv">  	
             <div class="table-responsive" id="eventGrid" >    
             </div>       
    </div>



<!--*********************** End ******************************-->
<input type="hidden" id="gridNo" name="gridNo" value="">
<div class="col-sm-12 col-md-12 top10">
	<div class='divErrorMsg' id='errordiv'></div>
</div>

<div id="addeditevent" class="hide">
	<!--<input type="hidden" id="eventId" name="eventscheduleId" />
	-->
	<input type="hidden" id="templateId" name="templateId" value="" />
<%-- 
		<div class="row">
			<div class="col-sm-3 col-md-3">
				<label>Template Type</label>
				<select class="form-control " id="templateType" name="templateType" class="span3">
					<option value="0" selected="selected">Select</option>
					<option value="1">District</option>
					<option value="2">Event</option>
					<option value="3">Status</option>
				</select>
			</div>
		</div>
--%>		
		
		
<%--	
		<div class="row">

			<div class="col-sm-6 col-md-6">
				<label>
					District<span class="required">*</span>
					
				</label>
				<input type="text" id="districtName" name="districtName"
					class="form-control"
					onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');getQuestionSetByDistrict();"
					onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');getQuestionSetByDistrict();"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');getDistrictEventTemplate();getQuestionSetByDistrict();" />
				<div id='divTxtShowData'
					style='display: none; position: absolute; z-index: 5000;'
					onmouseover="mouseOverChk('divTxtShowData','districtName')"
					class='result'>
				</div>

			</div>

		</div>
--%>
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<label>
				<spring:message code="lblTemplateName"/><span class="required">*</span>
				
			</label>
			<input class="form-control" type="text" id="eventtitle" name="eventtitle" maxlength="200" />
		</div>
	</div>

	<div class="row" id="subjectDiv">
		<div class="col-sm-8 col-md-8" id="">
			<label>
				<spring:message code="lblSub"/><span class="required">*</span>
			</label>
			<input type="text" id="subjectforParticipants"
				name="subjectforParticipants" maxlength="100" class="form-control" />
		</div>
	</div>

	<div class="row" id="msgtoparticipantdiv">
		<div class="control-group col-sm-8 col-md-8">
			<div class="" id="messagetext">
				<label>

					<strong><spring:message code="lblTemplateBody"/><span class="required">*</span> <a href="#"  id="evtsch" rel="tooltip" onclick="showTemplateTooltioDiv();" data-html="true" data-original-title="
									<table style='text-align: left;'>
										<tr><td>Along with the static text, the following dynamic fields are available in this exact format:</td></tr>
										<tr><td>&lt; Teacher First Name &gt;</td></tr>
										<tr><td>&lt; Teacher Last Name &gt;</td></tr>
										<tr><td>&lt; Teacher Email &gt;</td></tr>
										<tr><td>&lt; Admin First Name &gt;</td></tr>
										<tr><td>&lt; Admin Last Name &gt;</td></tr>
										<tr><td>&lt; Admin Email &gt;</td></tr>
										<tr><td>&lt; Job Title &gt;</td></tr>
										<tr><td>&lt; District or School Name &gt;</td></tr>
										<tr><td>&lt; District or School Address &gt;</td></tr>
									</table>"><img width="15" height="15" alt="" src="images/qua-icon.png"></a>
					</strong>
					</strong>
					<!-- <span class="hide" id="tooltipSpan" style="display:none;">
					<a href="#" id="templatetooltip" rel="tooltip" data-original-title="Please Click" onclick="showTemplateTooltioDiv();">
					<img width="15" height="15" alt="" src="images/qua-icon.png"></a>
					</span>
					-->
				</label>
				<textarea rows="8" class="form-control" cols="" id="msgtoprncpl"
					name="msg" maxlength="1000"></textarea>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8 col-md-8 top15">
			<div class="row">
				<div class="col-sm-6 col-md-6">
				
					<button onclick="saveTemplate()" class="btn btn-primary" id="save">
						<strong><spring:message code="btnSave"/> <i class="icon"></i></strong>
					</button>
					&nbsp;&nbsp;
					<button onclick="cancelEvent()" class="btn btn-dangerevents" id="save">
						<strong><spring:message code="btnClr"/> <i class="icon"></i>
						</strong>
					</button>
					&nbsp;&nbsp;
				</div>	
				
				</div>
			</div>
		</div>
	

</div>


<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>


<!-- printing Start  -->

<div style="clear: both;"></div>
<div class="modal hide" id="templateTTDiv" tabindex="-1" role="dialog"
	data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 500px; margin-top: 0px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="canelStatus();">
					x
				</button>
				<h3 id="">
					<spring:message code="headTm"/>
				</h3>
			</div>

			<div class="modal-body" style="max-height: 500px; overflow-y: auto;">
				<div class="" id="statusDataDiv">
					<label>Along with the static text, the following dynamic fields are available in this exact format:</label>
					<br/>&lt; Teacher First Name &gt;
					<br/>&lt; Teacher Last Name &gt;
					<br/>&lt; Teacher Email &gt;
					<br/>&lt; Admin First Name &gt;
					<br/>&lt; Admin Last Name &gt;
					<br/>&lt; Admin Email &gt;
					<br/>&lt; Job Title &gt;
					<br/>&lt; District or School Name &gt;
					<br/>&lt; District or School Address &gt;
				
					
				</div>
			</div> 
			<div class="modal-footer">
				<table border=0 align="right">
					<tr>
						<td nowrap>
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick='canelStatus();'>
								OK
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<%--
<div class="modal hide" id="vviDiv" tabindex="-1" role="dialog"
	data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 80%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="canelPrint();">
					x
				</button>
				<h3 id="">
					Virtual Video Interview
				</h3>
			</div>

			<div class="modal-body">
				<div id="interViewDiv">
					<iframe src='' id='ifrmTrans' width='100%' height='480px'></iframe>
				</div>
			</div>
			<div class="modal-footer">
				<table border=0 align="right">
					<tr>
						<td nowrap>
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick=''>
								OK
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
--%>
<div style="clear: both;"></div>
<%-- 
<div class="modal hide" id="printmessage1" tabindex="-1" role="dialog"
	data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 400px; margin-top: 0px;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick="canelPrint();">
					x
				</button>
				<h3 id="myModalLabel">
					Teachermatch
				</h3>
			</div>

			<div class="modal-body" style="max-height: 400px; overflow-y: auto;">
				<div class="control-group">
					<div class="">
						Please make sure that pop ups are allowed on your browser.
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<table border=0 align="right">
					<tr>
						<td nowrap>
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick='canelPrint();'>
								Ok
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>

--%>

<input type="hidden" id="dynamicColonm" value="0" />

<!--<input type="hidden" id="eventIdM" value="0" />
<input type="hidden" id="entityTypeId" value="${entityID}" />
-->

<script>
//displayEventGrids();
applyScrollOnTblEvent();
onDistrictChange();
//getEventType();
//getEmailTemplatesByDistrictId(${districtId});
//<c:if test="${entityID ne 1}">
  //getEmailTemplatesByDistrictId(${districtId});
 // getEmailTemplatesFacilitatorsByDistrict(${districtId});
//</c:if>/
//getTemplatesByDistrictId();
//getQuestionSetByDistrict();

//displayTemplateGrids();
$('#evtsch').tooltip();
 </script>
 <script>
 var displayID=$("#displayTypeId").val();
	if(displayID!=0){
		setEntityType();
	}
 </script>