<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<input type="hidden" id="updateStatusHDRFlag_msu" name="updateStatusHDRFlag_msu"/>
<input type="hidden" id="updateStatusName_msu" name="updateStatusName_msu"/>
<div class="modal hide" id="myModalStatus_msu"tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeStatus_msu()">x</button>
		<h3 id="myModalLabel"><spring:message code="lblStatus"/></h3>
	</div>
    <img class="img-responsive" src="images/headstatus.png" />
	<div class="modal-body-cgstatus">		
		<div class="control-group">
			<div id="divAllStatusByTIDAndJID_msu">&nbsp;</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeStatus_msu()"><spring:message code="btnDone"/></button>	
   </div>
  </div>
 </div>
</div>

<!-- Status Info Window -->
<div class="modal hide" id="myModalStatusInfo_msu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeStatusInfo_msu()">x</button>
		<h3 id="myModalLabel"><span id="statusInfoTitle_msu"><spring:message code="lblStatus"/></span></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="statusInfoText_msu"><spring:message code="statusInfoText_msu"/></div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeStatusInfo_msu()"><spring:message code="btnDone"/></button>	
 	</div>
	</div>
  </div>
</div>

<!-- Status Details -->

<div class="modal hide"  id="myModalStatusDetailsInfo_msu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog" style="width:720px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeStatusInfo_msu()'>x</button>
		<h3 id="myModalLabel"><span id="statusDetailsTitle_msu"><spring:message code="lblStatus"/></span></h3>
		<input type="hidden" id="flag_msu" name="flag_msu" value="">
		
		<input type="hidden" id="isEmailTemplateChanged" name="isEmailTemplateChanged" value="0">
		<input type="hidden" id="isEmailTemplateChangedTeacher" name="isEmailTemplateChangedTeacher" value="0">
		
	</div>
	<div class="modal-body-cgstatusnotes">
		<div class="control-group">
			<input type="hidden" name="scoreProvided" id="scoreProvided" />
			<div class='divErrorMsg' id='errorStatusNote_msu' style="display: block;"></div>
			<div id="divStatusDetailsInfo" style='width:656px;overflow:hidden;'></div>
			
			<div class="mt10">
	           	<div style="text-align:right;width: 654px;">
	           		<span><a href="#" onclick="openNoteDiv_msu()"><spring:message code="lnkAddNote"/></a></span>	
		    	</div>
	        </div>	        
	        <div class="mt10">
	           	<div class="span10">
	           		<table border='0' class='table' id='tblStatusNote_msu' style="width: 645px;">
	           			<thead class='bg'>
	           				<tr>
	           					<th valign=top><spring:message code="blNote"/></th>
	           					<th valign=top><spring:message code="hdAttachment"/></th>
	           					<th valign=top><spring:message code="lblcretedDate"/></th>
	           					<th valign=top><spring:message code="lblAct"/></th>
	           				</tr>
	           			</thead>
	           			
	           			<tr>
	           			<td colspan=4><spring:message code="lblNoNoteAdded"/></td>
	           			</tr>
	           			
	           		</table>	
		    	</div>
	        </div>
	            
			<div id="noteMainDiv_msu" class="hide">
			<%---------- Gourav :Status wise Dynamic Template [Start]  --%>
			<div id="templateDivStatusTP" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-8" style="max-width:300px;padding-top:10px;">		    	
			    	<label><strong><spring:message code="lblTemplate"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="statuswisesectionTP" onchange="getsectionwiselistTPGolu(this.value)">
		        		<option value="0"><spring:message code="sltTemplate"/></option>
		        	</select>
		         </div>	
		    	</div>
		    	<div class="row col-md-12" id="showTemplateslistTP" style="display:none">		    	
		    	<div class="row col-sm-12" style="padding-top:10px;">
		    	<label><strong><spring:message code="lblSubTmpt"/></strong></label>
		        	<br/>
		        	<span id="addTemplateslistTP"></span>
		         </div>	        	
			</div>
		    </div>
		<%---------- Gourav :Status wise Dynamic Template [END]  --%>
				<div class="mt5" >
				    	<div class="span6" id='statusNotes_msu' >
				    	<label class=""><spring:message code="blNote"/><span class="required">*</span></label>
				    		<textarea readonly id="statusNotes" name="statusNotes" class="span6" rows="2"></textarea>
				    	</div> 
				</div>  
				<iframe id='uploadStatusFileFrameID_msu' name='frmStatusFileFrame_msu' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form id='frmStatusNote_msu' enctype='multipart/form-data' method='post' target='frmStatusFileFrame_msu' onsubmit="saveStatusNoteOrSliderScore(0);"  class="form-inline" action='statusNoteUploadServlet_msu.do' accept-charset="UTF-8">
				<input type="hidden" id="statusNotesTeacherId" name="statusNotesTeacherId" value=""/>
				<input type="hidden" id="statusNotesJobIds" name="statusNotesJobIds" value=""/>
				<div class="mt5">  	
						<div class="span4" >
				    		<div id='statusNoteFileNames_msu' style="padding-top:6px;">
			        			<a href='javascript:void(0);' onclick='addStatusNoteFileType_msu();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addStatusNoteFileType_msu();'><spring:message code="lnkAttachFile"/></a>
			        		</div> 
				    	</div>
				    	<div class="span4" id="showStatusNoteFile_msu"  style="padding-top:4px;">&nbsp;
			        	</div>   
				</div>
				</form>
		 	</div> 
		 	
		 	<div class="mt10" id="schoolAutoSuggestDivId" style="display: none;">
            	<div class="span11">
            		<c:if test="${entityType eq 2}">
					  	<label><spring:message code="lblSchoolName"/><span class="required">*</span></label>
		 				<input type="text" id="schoolName" maxlength="100" name="schoolName" style="width: 645px;" placeholder="" onfocus="getSchoolAuto_djo(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
							onkeyup="getSchoolAuto_djo(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
							onblur="hideSchoolMasterDiv_djo(this,'districtOrSchoolId','divTxtShowData2'),requisitionNumbers();"	/>
							
					 	 <input type="hidden" id="schoolId"/>
						 <div id='divTxtShowData2' style=' display:none;' onmouseover="mouseOverChk_djo('divTxtShowData2','schoolName')" class='result' ></div>
				 	</c:if>
		    	</div>
            </div>
            
            <div class="row" id="requisitionNumbersGrid"  style="display: none;">
            		<c:if test="${entityType ne 1}">
					<label style='padding-top:10px; '><spring:message code="msgRequisitionNumbers"/><span class="required">*</span> <spring:message code="msgHiringCand"/></label>
 					<span id="requisitionNumbers"> </span>
 					</c:if>
            </div>
            <span id="spanOverride" style='display:none;'>
            	<div class="mt10">
	            	<div class="span10">
            			<label class='checkbox inline'>
            				<input type='checkbox' name='chkOverride' id='chkOverride' value='1'/>
            				<spring:message code="lblOverrideStatus"/>
            			</label>
			    	</div>
	            </div>
            </span>
            
		 	<span id="checkOptions_msu" style='display:none;'>
	            <c:set var="disabledChecked" value=""></c:set>
	            <c:set var="hideView" value=""> </c:set>
			 	<c:if test="${entityType eq 1}">
			 		<c:set var="disabledChecked" value="disabled=\"disabled\""> </c:set>
			 		<c:set var="hideView" value="hide"> </c:set>
			 	</c:if>
			 	<div class="mt10" id="email_da_sa_div">
	            	<div class="span10">
	            		<label class="checkbox inline">
					 	<input type="checkbox" name="emailToDASA_msu" ${disabledChecked} id="emailToDASA_msu"  value="1"/>
	                 	<spring:message code="msgNotifyAllAssoDistAndSchlAdminOfStatus"/></label>
						<a href='javascript:void(0)' class="${hideView}" style="padding-left: 90px;" onclick="getStatusWiseEmailForAdmin_msu()"><spring:message code="lnkV/Emag"/></a>
			    	</div>
	            </div>
	            
	            <div class="mt10" id="email_ca_div">
	            	<div class="span10">
	            		<label class="checkbox inline">
					 	<input type="checkbox" name="emailToCA_msu" ${disabledChecked} id="emailToCA_msu"  value="1"/>
	                 	<spring:message code="msgNotifycandidateofstatuschange"/></label>
			    	</div>
	            </div>
            </span>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table width=655 border=0>
	 		<tr>
		 		<td width=115 nowrap align=left>
		 		<span id="statusInProcess" style='display:none;'><button class="btn  btn-large btn-orange"  onclick='saveStatusNoteOrSliderScore(0)'><spring:message code="btnIn-Progress"/></button>&nbsp;&nbsp;</span>
		 		</td>
		 		<td width=125  nowrap align=left>
			 		<span id="unHire_msu" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR_msu(2)'><spring:message code="btnUnHire"/> </button></span>
			 		<span id="unDecline_msu" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR_msu(3)'><spring:message code="btnUnDecline"/> </button></span>
			 		<span id="unRemove_msu" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR_msu(4)'><spring:message code="btnUnReject"/> </button></span>
		 		</td>
		 		<td width=405  nowrap>
				<span id="statusFinalize_msu" style='display:none;'><button class="btn  btn-large btn-primary" onclick='saveStatusNoteOrSliderScore(1)'><spring:message code="btnFinalize"/> <i class="iconlock"></i></button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='closeStatusInfo_msu()'><spring:message code="btnClr"/></button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
</div>
</div>
</div>
 
<!-- Show Attach FIle -->
<div  class="modal hide" id="modalDownloadIAFN_msu" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header" id="modalDownloadIAFNMove_msu" style="cursor: move;">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelInstructionHeader_msu"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="">
				<iframe src="" id="ifrmAttachInstructionFileName_msu" width="100%" height="480px">
				 </iframe>        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
   </div> 
  </div>
</div>  
 
<!-- HR De Re Confirm --> 
 
 <div class="modal hide" id="modalUpdateStatusNoteMsg_msu"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeStatusNoteMsg_msu(0);'>x</button>
		<h3 id="myModalLabel"><spring:message code="lblStatus"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="updateStatusNoteMsg_msu">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeUpdateStatusNoteMsg_msu(1);'><spring:message code="btnOk"/></button> 
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeUpdateStatusNoteMsg_msu(0);'><spring:message code="btnClr"/></button> 		
 	</div>
</div>
</div>
</div>
 
<!-- Mass Update Success Message --> 

<div class="modal hide" id="myModalStatusSuccessMessage_msu"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="min-width: 500px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeSuccessUpdate_msu()">x</button>
		<h3 id="myModalLabel"><span id="myModalStatusSuccessMessageTitle_msu"><spring:message code="lblStatus"/></span></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="statusSuccessMessageText_msu"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeSuccessUpdate_msu()"><spring:message code="btnDone"/></button>	
 	</div>
    </div>
  </div>
</div>

<!-- Email Template for DA/SA -->
	<div class="modal hide" id="myModalEmail_msu" >
		<div class="modal-dialog">
	    <div class="modal-content">	
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='sendOriginalEmail_msu()'>x</button>
			<h3 id="myModalLabel">Teachermatch</h3>
		</div>
		
		<div  class="modal-body">
			<div class="control-group">
				<div class='divErrorMsg' id='errordivEmail_msu'></div>
			</div>
			<div id='support'>
				<div class="control-group">
					<div class="">
				    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
			        	<input  type="text" id="subjectLine_msu" name="subjectLine_msu"class="span8" maxlength="250" readonly="readonly" />
					</div>
				</div>
			            
		        <div class="control-group">
					<div class="" id="mailBody_msu">
				    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
			        	<textarea rows="5" class="span8" cols="" id="" name="" readonly="readonly"></textarea>
					</div>
				</div>
	 		</div>
	 	</div>
	 	
	 	<div class="modal-footer">
	 		<button class="btn"  onclick='sendOriginalEmail_msu()'><spring:message code="btnClr"/></button>
	 		<button class="btn btn-primary"  onclick='return sendChangedEmail_msu();' ><spring:message code="btnOk"/></button>
	 	</div>
	</div>
   </div>
</div>
 
<script type="text/javascript">
$(document).ready(function()
{
	$('#statusNotes_msu').find(".jqte").width(655);
});
applyScrollOnStatusNote_msu();
</script>