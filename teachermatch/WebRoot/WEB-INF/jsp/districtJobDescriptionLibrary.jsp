<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/districtJobDescriptionLibrary.js?ver=${resourceMap['js/districtJobDescriptionLibrary.js']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictJobDescriptionLibraryAjax.js?ver=${resourceMap['DistrictJobDescriptionLibraryAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<!-- 
<style>
div.t_fixed_header div.body {
	overflow-x	: auto;
}
</style>
 -->
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});


function applyScrollOnTblOfferReady()
{
   //alert("11")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridOfferReady').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[189,189,189,139,100,139],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnPrintTable()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridOfferReady').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 300,
         width: 940,
        minWidth: null,
        minWidthAuto: false,
        colratio:[160,180,100,215,180,110],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

</script>

<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<div class="row" style="margin:0px;">
<div class="col-sm-10 col-md-10">
         <div style="float: left;">
         	<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage Job Description Library</div>
<!--         	<spring:message code="headOfferReByStaffer"/></div>	-->
         </div>			
	</div>
		 
	<c:if test="${updateflag ne '0'}"> 	
	<div class="col-sm-2 col-md-2 top15">
	<a href="#" onclick="addnewJobDescriprion('0','0')">+ Add Job Description</a>
	</div>
	</c:if>
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>


          <div class="row">
		       <div class="col-sm-4 col-md-4">
				     <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
				        <c:if test="${DistrictOrSchoolName==null}">
					       <span>
					        <input type="text" maxlength="255" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
					            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
													onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
													onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData','districtOrSchooHiddenlId');jobcategorybaseondistrict();cleandistrictfield();"/>
							 </span>
							 <input type="hidden" id="districtOrSchooHiddenlId"/>
										
				         </c:if>
							<c:if test="${DistrictOrSchoolName!=null}"> 
							<span>
					        <input type="text" maxlength="255"  class="help-inline form-control" value="${DistrictOrSchoolName}"  disabled="disabled"/>
							 </span>
				             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
				             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
				             	
				             	<script type="text/javascript">
				             	jobcategorybaseondistrict();
				             	</script>
				            </c:if>
				            
				        	 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
							             	
               </div>
                <div class="col-sm-4 col-md-4 hide" id="jobCategoryDiv">
				     <label id="captionDistrictOrSchool"><spring:message code="lblJobCat"/></label>
				     <div id="jobcategoryIdid"></div>
				     <!--<select id="jobcategoryId" class="form-control">
				     	<option value="0" >Select Job Category</option>

					</select>
	     	 
	     	  --></div>
            </div>
            
	      
			         
			        <div class="row">
	               <div class="col-sm-3 col-md-3">
	               		<label id="captionDistrictOrSchool">Status</label>
	               		<span>
							  <select id="statusId" name="status" class="form-control">
							      <option value="0">All</option>
							      <option value="A">Active</option>
							      <option value="I">InActive</option>
						      </select>
						</span>
               		</div>			        
				        <!-- End New Search Criteria -->
				        
				        
				          <div class="col-sm-2 col-md-2 top25-sm2" style="width: 150px;">
					        <label class=""></label>
					        <button class="btn btn-primary " type="button" onclick="searchRecordJobDescriptionLibrary();">&nbsp;&nbsp;<spring:message code="btnSearch"/>&nbsp;&nbsp; <i class="icon"></i></button>
			  			 </div>

         </div>




         <div class="row">
		
	    </div>
 

  
	  
	  
	  

 <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    
              <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
    
    <input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
    <div style="clear: both;"></div>
     <br/>
   
          
   
      
         
       	
 
           <div style="clear: both;"></div>
    
           <div class="TableContent">  	
             <div class="table-responsive" id="divMainOfferReady" > </div>       
         </div>

 			  
 			   <div style="clear: both;"></div>
 			  <div class="control-group">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>
 			  
 			   <div style="clear: both;"></div>
 			  	
             <div class="table-responsive" id="divAddEditRecord"  style="display: none;">
             <input type="hidden" id="districtjobcateLibraryID" >
           
              <div class="row top15">
	               <div class="col-sm-4 col-md-4 ">
	                <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
				        <c:if test="${DistrictOrSchoolName==null}">
					       <span>
					        <input type="text" maxlength="255" id="districtSchoolName" name="districtSchoolName" class="help-inline form-control"
					            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData1', 'districtSchoolName','districtOrSchoolId','');"
													onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData1', 'districtSchoolName','districtOrSchoolId','');"
													onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData1','districtOrSchooHiddlId');jobcategorybaseondistrictAddEdit();"/>
							 </span>
							 <input type="hidden" id="districtOrSchooHiddlId"/>	
							  <div id='divTxtShowData1'  onmouseover="mouseOverChk('divTxtShowData1','districtSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>						
				         </c:if>
				       <c:if test="${DistrictOrSchoolName!=null}"> 
							<span>
					        <input type="text" maxlength="255" id="districtSchoolName" class="help-inline form-control" value="${DistrictOrSchoolName}"  disabled="disabled"/>
							 </span>
				             	<input type="hidden" id="districtOrSchooHiddlId" value="${DistrictOrSchoolId}"/>
				             	
				             	<script type="text/javascript">
				             	jobcategorybaseondistrictAddEdit();
				             	</script>
				            </c:if>
                 
                 </div>
                 
                  <div class="col-sm-4 col-md-4 none" id="jobcategoryDivId123">
				     <label id="captionDistrictOrScl"><spring:message code="lblJobCat"/></label>
				     <div id="jobcategoryeditId"></div>
				    
	     	  </div>
	     	   <div class="col-sm-4 col-md-4">
	     	     <label id="captionDistrictOrScl"><spring:message code="lbljobDesctriptionname"/></label>
	     	     <input type="text" id="districtjobdescriptionnameId" class="form-control"> 
	     	   </div>
         	  </div> 
         	  
         	   <div class="row">
		         	     <div class="col-sm-4 col-md-4">
							     <label id="status"><spring:message code="lblStatus"/></label>
							     <select id="addeditstatusId" class="form-control">
								     	<option value="0">All</option>
								      	<option value="A">Active</option>
								      	<option value="I">Inactive</option>
								</select>
				     	  </div>
	         	  </div> 
         	  
	         	  <div class="row">
		         	    <div class="col-sm-11 col-md-11" id="jobdescriptionNote">		
			                <label id="jobDescriptionNameId"><spring:message code="lbljobdescription"/></label>
			                	
							<div class="" id="jDescription">
								<textarea rows="8" id="jobLibraryDescriptionId"   name="jobDescriptionName"  class="form-control"></textarea>
							</div>			
						</div>
	         	  </div> 
	         	  
	         	  <div class="row">
	         	 		 <div class="col-sm-4 col-md-4 top25-sm2" style="width: 250px;">
					        <label class=""></label>
					        <button class="btn btn-primary " type="button" onclick="addEditdistrictjobdescriptionLibrary();">&nbsp;&nbsp;<spring:message code="btnSave"/>&nbsp;&nbsp; <i class="icon"></i></button><a href="#" onclick="divclosemethod()">&nbsp;&nbsp;<spring:message code="btnClr"/></a>
			  			 </div> 
			  	 </div>
         	  </div>    
                       
         
            <div style="clear: both;"></div>
    

			<input type="hidden" id="districtrelatedId" value="${DistrictOrSchoolId}">
			<input type="hidden" id="actionhiddenId" >
			

<div class="modal hide"  id="popupErrorMessage"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();"><spring:message code="btnX"/></button>
	   <h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			<spring:message code="msgPlzPopAreAlloued"/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>

<input type="hidden" value="${testvalue}" id="testvalueId">

<script type="text/javascript">

	searchRecordJobDescriptionLibrary();
</script>

	        	
	 
