<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/districtassessment.js?ver=${resourceMap['js/districtassessment.js']}"></script>
<script type="text/javascript">

var questionTypeJSON=${json};
 // alert(findSelected(3));
function findSelected(idd)
{
	for (x in questionTypeJSON)
  	{
	  if(idd==x)
	  return questionTypeJSON[x];
  	}
}
</script>
<style>
.myButton {
    background:url(./images/download.jpg) no-repeat;
    cursor:pointer;
    width: 30px;
    height: 30px;
    border: none;
}

.progress1 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar1 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent1 { position:absolute; display:inline-block; top:0px;bottom:0px; left:48%; }

.progress2 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar2 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent2 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress3 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar3 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent3 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress4 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar4 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent4 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress5 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar5 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent5 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress6 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar6 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent6 { position:absolute; display:inline-block; top:0px; left:48%; }

</style>

<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static"  >	
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 		
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="redirectToURL()"><spring:message code="btnOk"/></button></span> 		
 	</div>
</div>
</div>
</div>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblAdd/EditQues"/></div>	
         </div>			 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>


<div class="row">
<div class="col-sm-8 col-md-8 mt10">
<a href="districtassessment.do"><spring:message code="lnkAss"/></a>: ${assessmentSection.districtAssessmentDetail.districtAssessmentName} 
<br/> 
<a href="districtassessmentquestions.do?assessmentId=${assessmentSection.districtAssessmentDetail.districtAssessmentId}&sectionId=${assessmentSection.districtSectionId}">
<spring:message code="lnkSection"/></a>: ${assessmentSection.sectionName}
<br/>
<div id="divUId"></div>      
</div>
</div>

<input type="hidden" id="districtassessmentId" value="${assessmentSection.districtAssessmentDetail.districtAssessmentId}">
<div  id="divAssessmentRow" style="display: block;" onkeypress="chkForEnterSaveAssessmentQuestion(event);">
		<div class="row">
		<div class="col-sm-8 col-md-8">		                         
		 			<div class='divErrorMsg' id='errordiv' ></div>
		 			
		</div>
		</div>
		

		<div class="row" style="padding-top: 13px;">
		<div class="col-sm-7 col-md-7" id="assQuestion"><label><strong><spring:message code="headQues"/><span class="required">*</span></strong></label>
		<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
		 </label>
		<textarea class="form-control" name="question" id="question" maxlength="2500" rows="5" onkeyup="return chkLenOfTextArea(this,2500);" onblur="return chkLenOfTextArea(this,2500);"></textarea>
		</div>
		</div>

		<div class="row" style="padding-top: 13px;">
		<div class="col-sm-3 col-md-3" style="width: 240px;">
		<label><strong><spring:message code="headQuesType"/><span class="required">*</span></strong></label>
		<select  class="form-control" name="questionTypeMaster" id="questionTypeMaster" onchange="getQuestionOptions(this.value)">
		      <option value="0"><spring:message code="optSltQuesType"/></option>
				<c:forEach items="${questionTypeMasters}" var="questionTypeMaster" >	
					<option id="${questionTypeMaster.questionTypeId}" value="${questionTypeMaster.questionTypeId}" >${questionTypeMaster.questionType}</option>
				</c:forEach>	
		</select>
		</div>  
		<div class="col-sm-3 col-md-3" style="width: 240px;"><label><strong><spring:message code="lblWeightage"/></strong></label>
		<input type="text" name="questionWeightage" id="questionWeightage" onkeypress='return checkForDecimalTwo(event)' value="${weightageVal}" maxlength="6" class="form-control" placeholder="">
		<input type="hidden" name="maxMarks" id="maxMarks" class="span2"/>
		<input type="hidden" name="questionImage" id="questionImage" class="span2"/>
		</div>
		</div> 
		<div class="row" style="display: none;" id="slmlScoreDiv" >
		  <div class="col-sm-3 col-md-3">
		    <label><strong><spring:message code="lblScr"/><span class="required">*</span></strong></label>
		    <input type="text" name="slmlScore" id="slmlScore"  maxlength="6" class="form-control" placeholder="" onkeypress='return checkForDecimalTwo(event)'/>
		  </div>
		</div>
		
		<%--Image Type Question --%>
		<div id="imageuploadQuestion" style="display: none">
	  	  <form id="myForm0" action="uploaderServlet.do?reqType=reqType" method="post" enctype="multipart/form-data">
		    <table>
				<tr>
					<td>  </td>
					<td style="width: 225px;"><div id='fileDiv0'><input type="file" name="file0" id="file0" /></div><div id='img0'></div></td>
					<td>
						<input data-original-title='Upload' rel='tooltip' id='imgTp0' type="submit" class="myButton" value="     " onclick='handleSubmit(0)'/>
						<a href="javascript:void(0);"><img id='rm0' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(0);"></a>
					</td>
					<td> 
		    			&nbsp;<div class="progress0" id='p1' style="display: none;"><div class="bar0"></div ><div class="percent0">0%</div > </div> 
					</td>
					<td>
						<div id="status0"></div>
					</td>
				</tr>
			</table>
		  </form>
		</div>
		
		
		
		<div class="row" id="row0" style="display: none">
		<div class="left15" style="float: left;width: 205px;">
		<div class="spanfit1"><label><strong><spring:message code="lblOpt"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="lblScr"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="lblRk"/></strong></label>
		</div>
		</div>
	    <div class="" style="float: left;width: 205px;margin-left: 35px;">
		<div class="spanfit1"><label><strong><spring:message code="lblOpt"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="lblScr"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<spring:message code="lblRk"/></strong></label>
		</div>
		</div>
		<div class="clearfix"></div>
		</div>
		
		<div class="row" id="row1" style="display: none">		
			<div class="spanfit1 left15 pull-left" >
			    <div class="top7 pull-left">1</div>
				<div class="pull-left" style="width: 110px;margin-left: 5px;">
				<input type="hidden" name="hid1" id="hid1"/><input type="text" name="opt1" id="opt1" class="spanfit form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="score1" id="score1" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="rank1" onkeypress='return checkForIntUpto6(event)' id="rank1" maxlength="1" class="form-control" placeholder="">
				</div>
				<div class="clearfix"></div>
			</div>			
		    <div class="spanfit1 left30 pull-left">	
				<div  class="top7 pull-left">2</div>
				<div class="pull-left" style="width: 110px;margin-left: 5px;">
				<input type="hidden" name="hid2" id="hid2"/><input type="text" name="opt2" id="opt2" class="spanfit form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">				
				<input type="text" name="score2" id="score2" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="rank2" onkeypress='return checkForIntUpto6(event)' id="rank2" maxlength="1" class="form-control" placeholder="">
				</div>
				<div class="clearfix"></div>
		   </div>	
		   <div class="clearfix"></div>	
		</div>		
		
		<div class="row top5" id="row2" style="display: none">			
			<div class="spanfit1 left15 pull-left" >
			    <div class="top7 pull-left">3</div>
				<div class="pull-left" style="width: 110px;margin-left: 5px;">
				<input type="hidden" name="hid3" id="hid3"/><input type="text" name="opt3" id="opt3" class="spanfit form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="score3" id="score3" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
			    <input type="text" name="rank3" onkeypress='return checkForIntUpto6(event)' id="rank3" maxlength="1" class="form-control" placeholder="">
				</div>
				<div class="clearfix"></div>
			</div>			
		    <div class="spanfit1 left30 pull-left" >
			    <div class="top7 pull-left">4</div>
				<div class="pull-left" style="width: 110px;margin-left: 5px;">
				<input type="hidden" name="hid4" id="hid4"/><input type="text" name="opt4" id="opt4" class="spanfit form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="score4" id="score4" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
			    <input type="text" name="rank4" onkeypress='return checkForIntUpto6(event)' id="rank4" maxlength="1" class="form-control" placeholder="">
				</div>
				<div class="clearfix"></div>
			</div>
		 <div class="clearfix"></div>	
		</div>	
		
		
		
		
		<div class="row top5" id="row3" style="display: none">	
			<div class="spanfit1 left15 pull-left" >
				    <div class="top7 pull-left">5</div>
					<div class="pull-left" style="width: 110px;margin-left: 5px;">
					<input type="hidden" name="hid5" id="hid5"/><input type="text" name="opt5" id="opt5" class="spanfit form-control" maxlength="750" placeholder="">
					</div>
					<div class="pull-left" style="width: 40px;margin-left: 5px;">
					<input type="text" name="score5" id="score5" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
					</div>
					<div class="pull-left" style="width: 40px;margin-left: 5px;">
				   <input type="text" name="rank5" onkeypress='return checkForIntUpto6(event)' id="rank5" maxlength="1" class="form-control" placeholder="">
					</div>
					<div class="clearfix"></div>
			</div>		
			<div class="spanfit1 left30 pull-left" >
					    <div class="top7 pull-left">6</div>
						<div class="pull-left" style="width: 110px;margin-left: 5px;">
						<input type="hidden" name="hid6" id="hid6"/><input type="text" name="opt6" id="opt6" class="spanfit form-control" maxlength="750" placeholder="">
						</div>
						<div class="pull-left" style="width: 40px;margin-left: 5px;">
					    <input type="text" name="score6" id="score6" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
						</div>
						<div class="pull-left" style="width: 40px;margin-left: 5px;">
					   <input type="text" name="rank6" onkeypress='return checkForIntUpto6(event)' id="rank6" maxlength="1" class="form-control" placeholder="">
						</div>
						<div class="clearfix"></div>
			</div>
 			<div class="clearfix"></div>	
		</div>	

	    <div id='imageType' style="display: none;">
		<div><label><strong>Options&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Score</strong></label></div>
		<div>
		<form id="myForm1" action="uploaderServlet.do" method="post" enctype="multipart/form-data">
		<table>
		<tr>
		<td> 1 </td>
		<td style="width: 225px;"><div id='fileDiv1'><input type="file" name="file1" id="file1" /></div><div id='img1'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp1' type="submit" class="myButton" value="     " onclick='handleSubmit(1)'/>
		<a href="javascript:void(0);"><img id='rm1' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(1);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid7" id="hid7"/><input type="hidden" name="opt7" id="opt7" ><input type="text" name="score6" id="score7" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress1" id='p1' style="display: none;"><div class="bar1"></div ><div class="percent1">0%</div > </div> 
		</td>
		<td>
		<div id="status1"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm2" action="uploaderServlet.do" method="post" enctype="multipart/form-data">
		<table>
		<tr>
		<td> 2 </td>
		<td style="width: 225px;"><div id='fileDiv2'><input type="file" name="file2" id="file2" /></div><div id='img2'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp2' type="submit" class="myButton" value="     " onclick='handleSubmit(2)'/>
		<a href="javascript:void(0);"><img id='rm2' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(2);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid8" id="hid8"/><input type="hidden" name="opt8" id="opt8" ><input type="text" name="score8" id="score8" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress2" id='p2' style="display: none;"><div class="bar2"></div ><div class="percent2">0%</div > </div> 
		</td>
		<td>
		<div id="status2"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm3" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td> 3 </td>
		<td style="width: 225px;"><div id='fileDiv3'><input type="file" name="file3" id="file3" /></div><div id='img3'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp3' type="submit" class="myButton" value="     " onclick='handleSubmit(3)'/>
		<a href="javascript:void(0);"><img id='rm3' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(3);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid9" id="hid9"/><input type="hidden" name="opt9" id="opt9" ><input type="text" name="score9" id="score9" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress3" id='p3' style="display: none;"><div class="bar3"></div ><div class="percent3">0%</div > </div> 
		</td>
		<td>
		<div id="status3"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm4" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td> 4 </td>
		<td style="width: 225px;"><div id='fileDiv4'><input type="file" name="file4" id="file4" /></div><div id='img4'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp4' type="submit" class="myButton" value="     " onclick='handleSubmit(4)'/>
		<a href="javascript:void(0);"><img id='rm4' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(4);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid10" id="hid10"/><input type="hidden" name="opt10" id="opt10" ><input type="text" name="score10" id="score10" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress4" id='p4' style="display: none;"><div class="bar4"></div ><div class="percent4">0%</div > </div> 
		</td>
		<td>
		<div id="status4"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm5" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td> 5 </td>
		<td style="width: 225px;"><div id='fileDiv5'><input type="file" name="file5" id="file5" /></div><div id='img5'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp5' type="submit" class="myButton" value="     " onclick='handleSubmit(5)'/>
		<a href="javascript:void(0);"><img id='rm5' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(5);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid11" id="hid11"/><input type="hidden" name="opt11" id="opt11" ><input type="text" name="score11" id="score11" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress5" id='p5' style="display: none;"><div class="bar5"></div ><div class="percent5">0%</div > </div> 
		</td>
		<td>
		<div id="status5"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm6" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td > 6 </td>
		<td style="width: 225px;"><div id='fileDiv6'><input type="file" name="file6" id="file6" /></div><div id='img6'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp6' type="submit" class="myButton" value="     " onclick='handleSubmit(6)'/>
		<a href="javascript:void(0);"><img id='rm6' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(6);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid12" id="hid12"/><input type="hidden" name="opt12" id="opt12" ><input type="text" name="score12" id="score12" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress6" id='p6' style="display: none;"><div class="bar6"></div ><div class="percent6">0%</div > </div> 
		</td>
		<td>
		<div id="status6"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		</div>		
<!-- Image Type end-->
</div>

<div class="row" style="padding-top: 13px;">
<div class="col-sm-7 col-md-7" id="assInstruction"><label><strong><spring:message code="lblInst"/></strong></label>
<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 </label>
<textarea class="form-control" name="questionInstruction" id="questionInstruction" maxlength="2500" rows="4" onkeyup="return chkLenOfTextArea(this,2500);" onblur="return chkLenOfTextArea(this,2500);"></textarea>
</div>
</div>

 <div class="row top20"  id="divManage">
  	<div class="col-sm-6 col-md-5">
  		 
     		<button id="saveQues"  onclick="saveQuestion(${assessmentSection.districtAssessmentDetail.districtAssessmentId},${assessmentSection.districtSectionId},'save')" class="btn btn-large btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
     	
     	   	<button onclick="saveQuestion(${assessmentSection.districtAssessmentDetail.districtAssessmentId},${assessmentSection.districtSectionId},'continue')" class="btn btn-large btn-primary"><strong><spring:message code="btnSv&Conti"/> <i class="icon"></i></strong></button>
     	    &nbsp;<a href="javascript:void(0);" onclick="cancelQuestion(${assessmentSection.districtAssessmentDetail.districtAssessmentId},${assessmentSection.districtSectionId})"><spring:message code="lnkCancel"/></a> 
      
     </div>
  </div>


<input type="hidden" id="districtAssessmentQuestionId" value="${districtAssessmentQuestions.districtAssessmentQuestionId}"/>






<script language="javascript">

function handleSubmit(ff)
{
 var qType =	findSelected(dwr.util.getValue("questionTypeMaster"));

     (function() {
    
			var bar = $('.bar'+ff);
			var percent = $('.percent'+ff);
			var status = $('#status'+ff);
			
			  
			$('#myForm'+ff).ajaxForm({
			    beforeSubmit: function(arr) { 
				   // return false;// to cancel submit     
				  //$('#message2show').html(dwr.util.toDescriptiveString(arr[0].value.size,5));
					//$('#myModal2').modal('show');
					var fileName = arr[0].value.name;
					if(fileName==undefined || fileName=="")
					{
						//alert("Please select photo for option "+ff);
						if(qType=="itq")
						{
							$('#errordiv1').html("&#149; Please select photo of option question <br>");
						}else
							$('#errordiv1').html("&#149; Please select photo of option "+ff+" <br>");
						$('#errordiv1').show();
						return false;
					}else
					{
						var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();


						var fileSize=0;		
						if ($.browser.msie==true)
						{	
							fileSize = 0;	   
						}
						else
						{	
							fileSize = arr[0].value.size;
						}
						
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png'))
						{
							$('#errordiv1').html("&#149; Please select Acceptable image formats include GIF, PNG, and JPEG files <br>");
							$('#errordiv1').show();
							return false;
				
						}
						else if(fileSize>=10485760)
						{		
							$('#errordiv1').html("&#149; File size must be less then 10mb. <br>");
							$('#errordiv1').show();
							return false;
						}
						
						$('#p'+ff).show();
					}
				            
				},
			    beforeSend: function() {
			        status.empty();
			        var percentVal = '0%';
			        bar.width(percentVal)
			        percent.html(percentVal);
			        
			    },
			    uploadProgress: function(event, position, total, percentComplete) {
			        var percentVal = percentComplete + '%';
			        bar.width(percentVal)
			        percent.html(percentVal);
					//console.log(percentVal, position, total);
			    },
			    success: function(data) {
			        var percentVal = '100%';
			        bar.width(percentVal)
			        percent.html(percentVal);
			       // alert(data);
			        $('#img'+ff).html("<a target='_blank' href='"+data.split("###")[1]+"'><img src='"+data.split("###")[1]+"' height='100' width='180'></a>");
			        $('#p'+ff).hide();
			    },
				complete: function(xhr) {
					status.html("Uploaded: <a target='_blank' href='"+xhr.responseText.split("###")[1]+"'>"+xhr.responseText.split("###")[1]+"</a> ("+xhr.responseText.split("###")[0]+" bytes)");
					$('#fileDiv'+ff).hide();
					$('#errordiv1').hide();
					$('#imgTp'+ff).hide();
					$('#rm'+ff).show();
					if(qType=="itq")
						$('#questionImage').val(xhr.responseText.split("###")[2]);
					else
						$('#opt'+(ff+6)).val(xhr.responseText.split("###")[2]);
				},
				 error: function(){}
			}); 

})();       

}

function removeImage(ff)
{
	$('#fileDiv'+ff).html("<input type='file' name='file"+ff+"' id='file"+ff+"' />");
	$('#fileDiv'+ff).show();
	$('#img'+ff).html("");
	$('#imgTp'+ff).show("");
	$('#rm'+ff).hide("");
	$('#status'+ff).html("");
	$('#opt'+(ff+6)).val("");
	
}
  function processJson(data) { 
    // 'data' is the json object returned from the server 
    alert(data); 
}

// pre-submit callback 
function showRequest() { 
  // alert("dd");
    return true; 
}
		for(j=1;j<=6;j++)
		$('#imgTp'+j).tooltip();
		
 var assessQuesOpts=${assessQues}
 
setAssessmentQuestion(${param.assessmentQuestionId});
	//$('#domainMaster').focus();
	
	$(document).ready(function() {
  		//$(".jqte").width(538);
  		$('#assQuestion').find(".jqte").width(538);
  		$('#assInstruction').find(".jqte").width(538);
	}) 

</script>


<script src="js/jquery.form.js"></script> 

    
