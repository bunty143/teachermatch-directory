<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">
<style type="text/css">
#circulardiv
{
border-radius: 50%;
text-align:center;
padding: 15px;

}
.row b
{
color:black;
}
ul
{
    list-style-type: none;
}
.fontleft
{
text-align: left;
}
.fontleft li
{
margin-bottom:5px;
font-size:12px;
font-weight: bold;
}
td
{
width: :10%;
}
.leftalign
{
text-align: :left;
}
.head
{
color: Maroon;
font-weight: bold;
}
</style>
<div class="container">
<center>

<div style="margin-top:18px; padding-right:50px;"> 
<b  class="font25" style="color:black;"><spring:message code="headEasyReExLvlStatus"/></b>
<div style=" font-size: 14px; margin-top:8px;">
<p><spring:message code="msgHowYoCnPoitUnlockExclusReso"/>.<br/>
<spring:message code="lblGoToYur"/><a href="signin.do"><b style="color: #007AB5; "><spring:message code="lblPersonalPlng"/> </b></a> <spring:message code="Fdr&CmltInfoL"/><br/>
<spring:message code="lblLMkYurQuestSucc"/>!</p>
</div>
</div>

<div  class="row" style="width:80%; margin: auto; padding-right:55px; padding-top: 5px;">
<div class="col-sm-3 col-md-3">
<div id="circulardiv">
<img src="images/ppi1.png" width="154px" height="134px"/>
</div>
</div>

<div class="col-sm-3 col-md-3">
<div id="circulardiv">
<img src="images/ppi2.png" width="154px" height="134px"/>
</div>
</div>

<div class="col-sm-3 col-md-3">
<div id="circulardiv">
<img src="images/ppi3.png" width="154px" height="134px"/>
</div>
</div>

<div class="col-sm-3 col-md-3">
<div id="circulardiv">
<img src="images/ppi4.png" width="154px" height="134px"/>
</div>
</div>

<div class="col-sm-12 col-md-12">
<div id="circulardiv" style="padding-left: 40px;">
<img src="images/quest_mark.png" width="550px;"/>
</div>
</div>
</div>

<table width="45%">
 <tr>
  <td></td>
  <td class="head"><spring:message code="lblMinimumPoints1"/> </td>
  <td class="head"><spring:message code="lblMaximumPoints1"/></td>
 </tr>
 <tr>
  <td colspan="3" class="head"><spring:message code="lblActiv"/></td>
  
 </tr>
 <tr>
  <td >- <spring:message code="lblAuthenticatedRegistration1"/></td>
  <td><img src="images/boxquest.png"/> </td>
  <td><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td>- <spring:message code="lblSignedAffidavit1"/></td>
  <td><img src="images/boxquest.png"/> </td>
  <td><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td>- <spring:message code="Preferenceslbl"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td class="head"><spring:message code="lblCrede"/></td>
  <td></td>
  <td></td>
 </tr>
  <tr>
  <td>- <spring:message code="headAcad"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/></td>
 </tr>
 <tr> 
  <td>- <spring:message code="lblCertifications"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/><img src="images/boxquest.png"/></td>
 </tr>
<tr>
  <td>- <spring:message code="llbEndorsedReferences1"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/><img src="images/boxquest.png"/><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td>- <spring:message code="lblOpenSubstitute1"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td>- <spring:message code="lblEmplHis"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td>- <spring:message code="lblVolunteerWork1"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td class="head"><spring:message code="lblTmEPI"/></td>
  <td><img src="images/boxquest.png"/><img src="images/boxquest.png"/><img src="images/boxquest.png"/><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/><img src="images/boxquest.png"/><img src="images/boxquest.png"/><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td class="head"><spring:message code="lblVideoProfile1"/></td>
  <td><img src="images/boxquest.png"/><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td class="head"><spring:message code="lblResume"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/></td>
 </tr>
 <tr>
  <td> <span class="head"><spring:message code="lblJobsApplied"/> </span><spring:message code="lblatleastone1"/></td>
  <td><img src="images/boxquest.png"/></td>
  <td><img src="images/boxquest.png"/><img src="images/boxquest.png"/></td>
 </tr>
</table>


</center>
</div>


