<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/ApproveJobAjax.js?ver=${resourceMap['ApproveJobAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/approveJob.js?ver=${resourceMap['js/approveJob.js']}"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />
<style>
.table th, .table td { 
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
</style>
			<div class="row top15">
            <div class="col-sm-8 col-md-8">
               <label ><b><spring:message code="lblDistrictName"/></b></label>  <br/>          
                <c:if test="${SchoolDistrictNameFlag==0}">	            
			    <c:if test="${JobOrderType==3}">
			      <input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
              	</c:if>
              	 <c:if test="${JobOrderType==2}">
			        <input type="hidden" id="districtOrSchooHiddenlId" value="${jobOrder.districtMaster.districtId}"/>
              	</c:if>
			 </c:if>
    		
			<c:if test="${SchoolDistrictNameFlag!=0}">
             	${DistrictOrSchoolName}	             
             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>             	  
             </c:if>       		 
         </div>

   
   
   		<c:if test="${jobOrder.districtMaster.isZoneRequired eq 1}">
   				<div class="col-sm-3 col-md-3" id="displayZone" >
		         	<label id="zoneLable"><b><spring:message code="lblZone"/></b></label>
					<br/>
					<label>${jobzones}</label>
	     		</div>	
   		</c:if>        
 		</div>
 		
 		
		<c:if test="${entityType==1}">
			<input type="hidden" id=isReqNoRequired name="isReqNoRequired" value="${jobOrder.districtMaster.isReqNoRequired}" />
		</c:if>
		<c:if test="${entityType==2}">
			<input type="hidden" id=isReqNoRequired name="isReqNoRequired" value="${userMaster.districtId.isReqNoRequired}" />
		</c:if>
		<c:if test="${jobOrder.districtMaster.isZoneRequired eq 1}">
			<input type="hidden" id="isZoneAvailable" name="isZoneAvailable" value="1"/>	
		</c:if>
		<input type="hidden" id="isZoneAvailable" name="isZoneAvailable" value=""/>
		<input type="hidden" id="windowFunc" name="windowFunc" value="${windowFunc}" />
		<input type="hidden" id="jobId" name="jobId" value="${jobOrder.jobId}" />
		<input type="hidden" id="jobCatId" name="jobCatId" value="${jobOrder.jobCategoryMaster.jobCategoryId}" />
		<input type="hidden" id="entityType" name="entityType" value="${entityType}">
		<input type="hidden" id="jobOrderType" name="jobOrderType" value="${JobOrderType}">
		<input type="hidden" id="distHiddenId" name="distHiddenId" value="0" />
		<input type="hidden" id="districtHiddenId" name="districtHiddenId" value="${districtHiddenId}" />
		<input type="hidden" id="writePrivilegeValue" name="writePrivilegeValue" value="" />
		<input type="hidden" id="stateId" name="stateId" value="${stateId}">
		
		<c:set var="colVal" value="col-sm-8 col-md-8"></c:set>
		<c:set var="isJeffcoDistrict" value="0"></c:set>
		<c:if test="${JobId!=0 && jobOrder.districtMaster!=null && jobOrder.districtMaster.districtId eq 804800}">
			<c:set var="colVal" value="col-sm-4 col-md-4"></c:set>
			<c:set var="isJeffcoDistrict" value="1"></c:set>
		</c:if>
		
	    <div class="row">
		<div class="${colVal}">
		<label><b><spring:message code="lblJoTil"/> </b></label></br>
		<label>${fn:escapeXml(jobOrder.jobTitle)}</label>
		</div>
		
		<c:if test="${isJeffcoDistrict eq '1'}">
			<div class="${colVal}">
				<label><b><spring:message code="lblRequis"/></b></label><br>
		   		<label>${jobOrder.requisitionNumber}</label> 	
			</div>
		</c:if>
		
		<c:if test="${JobId!=0}">
		<div class="col-sm-3 col-md-3">
		         	<label><b><spring:message code="lblJoI/JCode"/></b></label><br/>	
		         
		         	<label>${jobId}</label> 	         	
	    </div>
	    </c:if>
		
	</div>
	
	<input type="hidden" id="copy_text" />
	<c:set var="hide" value="hide"></c:set>
		<div class="row ${hide}" id="requisitionDiv">
				<div class="col-sm-2 col-md-2"  id="" >
					<label><b><spring:message code="lblRequis"/></b></label>
				   	<label>${jobOrder.requisitionNumber}</label> 	
				</div>
		</div>	

<div class="row">
      <div class="col-sm-2 col-md-2">
        <label><b><spring:message code="lblPostStDate"/> </b></label></br>
         <input type="hidden" id="jobStartDate"  value="${jobStartDate}" >
         <label>${jobStartDate}</label>   
      </div>
      <div class="col-sm-2 col-md-2">
        <label><b><spring:message code="lblPostEnDate"/></b> </label></br>
        	<label>${jobEndDate}</label>         
      </div>
      <div class="col-sm-4 col-md-4">
			<label><b><spring:message code="lblRefNo/JoCode"/></b></label></br>
				<label>${jobOrder.apiJobId}</label> 
		</div>
      
      <!-- Mukesh -->
		   <div class="col-sm-3 col-md-3">
	      	   <label><b><spring:message code="lblJoTy"/>  </b> </label><br/>
	      	   		<c:if test="${jobType eq 'F'}">
	      	    <label><spring:message code="optFTime"/></label> 
	      	    </c:if>
	      	      <c:if test="${jobType eq 'P'}">
	      	    <label><spring:message code="optPTime"/></label> 
	      	    </c:if>
	      	      <c:if test="${jobType eq 'FP'}">
	      	    <label><spring:message code="optF/PTime"/> </label> 
	      	    </c:if>	      	  
             </div>
</div>
		<!-- Only For Jeffco -->
		<c:if test="${jobOrder!=null && jobOrder.districtMaster!=null && jobOrder.districtMaster.districtId == '804800'}">
						<div class="row">
						<c:if test="${not empty jobOrder.fte}">
							<div class="col-sm-2 col-md-2 mt10">
							<strong>FTE</strong><br/>
								${jobOrder.fte}
							</div>
						</c:if>
						<c:if test="${not empty jobOrder.daysWorked}">
							<div class="col-sm-2 col-md-2 mt10">
							<strong><spring:message code="lblDaysWorked" /></strong><br/>
								${jobOrder.daysWorked}
							</div>
						</c:if>
						
						<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Hourly' || (not empty jobOrder.jobCategoryMaster.parentJobCategoryId && jobOrder.jobCategoryMaster.parentJobCategoryId.jobCategoryName eq 'Hourly')}">
							<c:if test="${not empty jobOrder.fsalary}">
								<div class="col-sm-2 col-md-2 mt10"> 
								<strong><spring:message code="lblSalary" />(Hourly)</strong><br/>
								$<fmt:formatNumber type="number" maxFractionDigits="2" value="${jobOrder.fsalary}"/>
								</div>
							</c:if>
						</c:if>
						<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator/Protech' || (not empty jobOrder.jobCategoryMaster.parentJobCategoryId && jobOrder.jobCategoryMaster.parentJobCategoryId.jobCategoryName eq 'Administrator/Protech')}">
							<c:if test="${not empty jobOrder.fsalaryA || not empty jobOrder.ssalary}">
								<div class="col-sm-2 col-md-2 mt10">
								<strong><spring:message code="lblSalary" />(${jobOrder.jobCategoryMaster.jobCategoryName})</strong><br/>
								$<fmt:formatNumber type="number" maxFractionDigits="0" value="${jobOrder.fsalaryA}"/> - $<fmt:formatNumber type="number" maxFractionDigits="0" value="${jobOrder.ssalary}"/>
								</div>
							</c:if>
						</c:if>
						<c:if test="${not empty jobOrder.payGrade}">
							<div class="col-sm-2 col-md-2 mt10">
							<strong><spring:message code="lblPayGrade" /></strong><br/>
								${jobOrder.payGrade}
							</div>
						</c:if>
						</div>
				</c:if>
			<!-- ENded Only For Jeffco -->
			
<div class="row">	
	<div class="col-sm-12 col-md-12">
		<div id="showCertificationDiv"></div>
	</div>	
	<div class="col-sm-12 col-md-12 top20">
	<label><b><spring:message code="lblCert/LiceName"/> </b>
	</label>
		<div id="certificationRecords"></div>
	</div>
</div>	

<div class="row" >
			<div class="col-sm-10 col-md-10">
				<label><b><spring:message code="headJobDescrip"/></b> </label>
				</div>	
			
			
	<div class="col-sm-11 col-md-11">			
		<div class="" id="jDescription">
			<label rows="8" id="jobDescription">${jobOrder.jobDescription}</label>
		</div>			
	</div>
</div>



<div class="row top10" >
			<div class="col-sm-10 col-md-10">
				<label><b><spring:message code="lblJoQuali"/></b></label>
			</div>
			
			<div class="col-sm-11 col-md-11">	
				<div class="" id="jQualification">
				<label rows="8" id="jobQualification"   name="jobQualification"  class="form-conntrol">${jobOrder.jobQualification}</label>
				</div>	
			</div>
</div>



<div class="row top10">
<div class="col-sm-4 col-md-4">
<label class=""><b><spring:message code="lblJoCatN"/></b> </label>	 
	   <!-- <c:if test="${not empty jobCategoryMasterlst}">
        <c:forEach var="jobCategoryMaster" items="${jobCategoryMasterlst}" varStatus="status">
        	<c:set var="jCIdSvalue" value=""></c:set>
        	<c:if test="${jobOrder.jobCategoryMaster.jobCategoryId==jobCategoryMaster.jobCategoryId}">
        		<c:set var="jCIdSvalue" value="selected"></c:set>
        		<option value="${jobCategoryMaster.jobCategoryId}||${jobCategoryMaster.baseStatus}||${jobCategoryMaster.assessmentDocument}" ><c:out value="${jobCategoryMaster.jobCategoryName}"/></option>
        	</c:if>			
		</c:forEach>
        </c:if> -->
        <br/>${jobOrder.jobCategoryMaster.jobCategoryName} 
</div>

<div class=" col-sm-4 col-md-4">
<label class=""><b><spring:message code="lblSubN"/></b></label>

	   <c:if test="${not empty subjectList}">
        <c:forEach var="subjectList" items="${subjectList}" varStatus="status">
        	<c:choose>
			    <c:when test="${jobOrder.subjectMaster.subjectId eq subjectList.subjectId}">
			    	<c:set var="subEditId" value="selected"></c:set>
			    		<option value="${subjectList.subjectId}"><c:out value="${subjectList.subjectName}" /></option>
			    </c:when>
			    <c:otherwise>
			    	<c:set var="subEditId" ></c:set>
			    </c:otherwise>              
			</c:choose>
        
		</c:forEach>
	   </c:if>	

</div>
</div>


<div class="row">
<div class="col-sm4 col-md-4 hide"  id="addHiresDiv" >
	<label><b><spring:message code="lblOfExpHi"/></b> </label>
	<label>${noOfExpHires}</label> 	
</div>
</div>


    <input type="hidden" name="allSchoolGradeDistrictVal" id="allSchoolGradeDistrictVal" value="${allSchoolGradeDistrictVal}"/>
	<div class="hide" id="hideForSchool">	
	<c:set var="hideVar" value="hide"/>
	<c:if test="${entityType==1}">
		<c:if test="${jobOrder.districtMaster.isReqNoRequired}">
			<c:set var="hideVar" value=""/>
		</c:if>
	</c:if>
	<c:if test="${entityType==2}">
		<c:if test="${userMaster.districtId.isReqNoRequired}">
			<c:set var="hideVar" value=""/>
		</c:if>
	</c:if>
	
	<c:set var="expHireFlagforMultiple" value=""/>
	<c:if test="${expHireNotEqualToReqNoFlag}">
		<c:set var="expHireFlagforMultiple" value="checked=checked"/>
	</c:if>
	
	
	<div class="row top15">
		   <div class="col-sm-10 col-md-10">
	           <label>				
	            		<b><spring:message code="msgMulHiWiAllToSngleReqNo"/></b>
	           </label>
	           <c:choose>
				    <c:when test="${jobOrder.isExpHireNotEqualToReqNo ne null}">
					   
				    	<c:choose>
				    	<c:when test="${jobOrder.isExpHireNotEqualToReqNo eq true}"> : <spring:message code="lblYes"/></c:when>
				    	<c:otherwise> : <spring:message code="lblNo"/></c:otherwise>
				    	</c:choose>
				    	
				    	 
				    </c:when>
				</c:choose>	
           </div>
	</div>
	
	
	<div class="row col-sm-10 col-md-10 top10">
		<b><spring:message code="msgWhAdminCanHiCandFrThJoOrdAppPo"/></b>
	</div>
	
	<c:set var="disableRadioDACH" value=""></c:set>
	<c:set var="disableRadioSACH" value=""></c:set>
	<c:if test="${disableRadioDA eq 0}">
		<c:set var="disableRadioDACH" value="disabled='disabled'"></c:set>
	</c:if>
	
	<c:if test="${disableRadioSA eq 0}">
		<c:set var="disableRadioSACH" value="disabled='disabled'"></c:set>
	</c:if>
	
	<div class="hide">
			<select multiple id="tempList" name="tempList" class="span4" style=> 
				<c:if test="${not empty attachedList}">
				        <c:forEach var="attlst" items="${attachedList}" varStatus="status">
				        	<c:set var="disable" value=""></c:set>
				        	<c:if test="${attlst.status eq 1}">
				        		<c:set var="disable" value="disabled"></c:set>
				        	</c:if> 
							<option value="${attlst.districtRequisitionNumbers.districtRequisitionId}" ${disable} >${attlst.districtRequisitionNumbers.requisitionNumber}</option>
						</c:forEach>
				    </c:if>
			</select>
	</div>
	
	
	<div class="row col-sm-4 col-md-4">
		<div>			
			<c:if test="${jobOrder.noSchoolAttach == '1' || PageFlag==0}">
			<label class="radio">
				<input type="radio" name="allSchoolGradeDistrict"  id="allSchoolGradeDistrict" value="1" ${(jobOrder.noSchoolAttach == '1' || PageFlag==0) ? 'checked' : ''} ${disableRadioDACH} >
				<b><spring:message code="msgOnlyDistAdnin"/></b>
			</label>
			</c:if>
			
		</div>
		<c:set var="hideAddReqLink" value="hide"></c:set>
		<c:if test="${isReqNoFlagByDistrict eq 1 && jobOrder.jobId>0}">
			<c:set var="hideAddReqLink" value="hide"></c:set>
		</c:if>		
	</div>
	
	
	<c:if test="${jobOrder.noSchoolAttach == '1' || PageFlag==0}">
		<div  id="addNoSchoolDiv" >
			 <div class="row">
		        <div class="col-sm-4 col-md-4 hide" style="border:0px solid blue;">
		          <div id="districtReqNumberDiv" class="">
		             </div>
		        </div>
		     </div>	    		
			<div class="row left15">
			<div class="col-sm-4 col-md-4">
			<label><spring:message code="lblOfExpHi"/></label>	</br>
			<label>${jobOrder.noOfExpHires}</label>
			<input type="hidden" class="span1" id="districtReqNumFlag" name="districtReqNumFlag" value="${jobOrder.districtMaster.isReqNoRequired}">
			<input type="hidden" class="span4" id="arrReqNum" name="arrReqNum" value="">
			<input type="hidden" class="span1" id="delReqId" name="delReqId" value="">
			<input type="hidden" class="span1" id="isReqNoFlagByDistrict" name="isReqNoFlagByDistrict" value="${isReqNoFlagByDistrict}">
			</div>
			</div>
			<c:set var="hideReq" value="hide"></c:set>
			<c:if test="${(jobOrder.districtMaster.isReqNoRequired eq true) || (entityType eq 2 && districtReqNumFlag eq true)}">
		    		 <c:set var="hideReq" value=""></c:set>
		    </c:if> 
		</div>
	</c:if>	
	
	<div class="row left5 top10">
	<div class="col-sm-11 col-md-11">			
			<c:if test="${jobOrder.selectedSchoolsInDistrict == '1'}">
			<label class="radio inline" style="padding-left: 0px;">
				<input type="radio" 
				name="allSchoolGradeDistrict" id="allSchoolGradeDistrict"  value="2" ${jobOrder.selectedSchoolsInDistrict == '1' ? 'checked' : ''} ${disableRadioSACH}>
				<b><spring:message code="lblAdmnfrmDiffSchls"/></b>
			</label>
			</c:if>
			
			<input type="hidden" class="span4" id="jobAuthKey" name="jobAuthKey" value="${param.jobAuthKey}">
			<input type="hidden" class="span2" id="currentSchoolId" name="currentSchoolId" value="">
			<input type="hidden" class="span1" id="currentSchoolExpectedHires" name="currentSchoolExpectedHires" value="">
			<input type="hidden" class="span4" id="arrReqNumforSchool" name="arrReqNumforSchool" value=""/> 
			<input type="hidden" class="span2" id="arrReqNumAndSchoolId" name="arrReqNumAndSchoolId" value=""/> 
			<input type="hidden" class="span6" id="arrTotalReqNumforSchool" name="arrTotalReqNumforSchool" value=""/>
			<input type="hidden" class="span8" id="arrTotalReqNumAndSchoolId" name="arrTotalReqNumAndSchoolId" value=""/>
			<input type="hidden" class="span1" id="flagIfEqualSchoolReqNoAndNoofExpHire" name="flagIfEqualSchoolReqNoAndNoofExpHire" value="">
			<input type="hidden" class="span1" id="requiSchoolIdsForHire" value="${requisitionSchoolList}">
		
	</div>
	</div>

	<div class="hide"  id="addSchoolDiv">	
			<div class="col-sm-5 col-md-5 mt10">	
		<label><spring:message code="lblSchoolName"/></label>
		 <input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
											onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
											onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2'); lockGeozone(1);"	/>
		 	 <input type="hidden" id="schoolId"/>
			 <div id='divTxtShowData2' style=' display:none;position:absolute;'   class='result' ></div>
		</div>
	</div>	
	<div class="col-sm-9 col-md-9" id=schoolRecords></div>
</div>

	<div class="row col-sm-10 col-md-10 mt10">
		<b><spring:message code="msgWhJoSpeInveIsNed"/></b>
	</div>
 	<div class="row col-sm-10 col-md-10">     
       <c:if test="${jobOrder.isJobAssessment == false || PageFlag==0}">        
        	<label class="radio">
        <input type="radio" name="isJobAssessment"  id="isJobAssessment1" value="2" ${(jobOrder.isJobAssessment == false || PageFlag==0)? 'checked' : ''}>
        <spring:message code="msgNoJoSpeciInveIsNed"/>
        </label>
       </c:if>		
     </div>

	<div>
	    <div class="row col-sm-10 col-md-10" style="margin-top: -10px;">			
		        <input type="hidden" name="isJobAssessmentVal" id="isJobAssessmentVal" value="${jobOrder.isJobAssessment == false || PageFlag==0 ? '2':'1'}"/>  	        
		         <c:if test="${jobOrder.isJobAssessment == true}">
		        <label class="radio">
		         <input type="radio" name="isJobAssessment"  id="isJobAssessment2" value="1"  ${(jobOrder.isJobAssessment == true) ? 'checked' : ''}>
		       	 <spring:message code="msgTJoQeqJoSpeciInveTaByApp"/>
		        </label>
		        </c:if>	
		        
        </div>
	     <div  id="isJobAssessmentDiv">	     	
	     	 <input type="hidden" name="attachJobAssessmentVal" id="attachJobAssessmentVal" value="${attachJobAssessmentVal}"/>
	     	 <input type="hidden" name="districtRootPath" id="districtRootPath" value="${districtRootPath}"/>
	     	 <input type="hidden" name="schoolRootPath" id="schoolRootPath" value="${schoolRootPath}"/>  
	     	 <input type="hidden" name="DAssessmentUploadURL" id="DAssessmentUploadURL" value="${DAssessmentUploadURL}"/>
	   		
	   		 <div class="row col-sm-10 col-md-10 hide left15" id="hideDAUploadURL" >
		      <label class="radio" style="margin-top: 0px; margin-bottom: 5px;">
		       <input type="radio" name="attachJobAssessment"  id="attachJobAssessment1" value="1"  ${(jobOrder.attachDefaultDistrictPillar == '1'|| PageFlag==0) ? 'checked' : ''}>
		       <spring:message code="msgUsTheDefJoSpeciInveSetAtDist"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide" id="ViewDAURL"></span>
		     </label>
		     </div>
		     
		     <div class="row col-sm-10 col-md-10 hide left15" id="hideSAUploadURL">
		      <label class="radio" style="margin-top: 0px; margin-bottom: 5px;">
		       <input type="radio" name="attachJobAssessment"  id="attachJobAssessment2" value="3" ${jobOrder.attachDefaultSchoolPillar == '1' ? 'checked' : ''}>
		      <spring:message code="msgUsTheDefJoSpeciInveSetAtSch"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hide" id="ViewSAURL"></span>
		     </label>
		     </div>
		    
		    <c:set var="ishidejcjsidiv" value="hide"></c:set>
		    <c:if test="${jobOrder.attachDefaultJobSpecificPillar==1}">
		    	<c:set var="ishidejcjsidiv" value="show"></c:set><%-- Gagan : show means No need to hide in case of Edit Mode --%>
		    </c:if>
		    
		     <div class="row col-sm-10 col-md-10 hide left15 ${ishidejcjsidiv}" id="jcjsidiv">
		      <label class="radio" style="margin-top: 0px; margin-bottom: 5px;">
		       <input type="radio" name="attachJobAssessment"  id="attachJobAssessment4" value="4" ${jobOrder.attachDefaultJobSpecificPillar == '1' ? 'checked' : ''}>
		      <spring:message code="msgUsTheDefJoSpeciInveSetAtCate"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="${ishidejcjsidiv}" id="ViewJSIURL"></span>
		     </label>
		     </div>
		    <iframe src="" id="ifrmJsi" width="100%" height="480px" style="display: none;">
				</iframe> 
				
			<div style="clear: both;">	
			<div id="jsiDiv" class="left30">				   
			  		<div class="hide" id="attachJobAssessmentDiv">					
					<form id='frmJobAssessmentUpload' enctype='multipart/form-data' method='post' target='uploadFrame'  class="form-inline">
					 <input type="hidden" id="dateTime" name="dateTime"  value="${dateTime}"/>
					 <input type="hidden" id="JobOrderType" name="JobOrderType"  value="${JobOrderType}"/>
						<c:if test="${JobOrderType==3}">
					      <input type="hidden" id="districtSchoolHiddenId" name="districtSchoolHiddenId" value="${SchoolId}"/>
				 	  	</c:if>
		              	<c:if test="${JobOrderType==2}">
					       <input type="hidden" id="districtSchoolHiddenId" name="districtSchoolHiddenId" value="${DistrictOrSchoolId}"/>
				 	 	</c:if>				
					</form>
					</div>
			</div>		
			
	     </div> 
     </div>
     
  <c:if test="${JobOrderType==2}">
    <div class="row col-sm-10 col-md-10 top8">
	<b><spring:message code="lblR/WPriv"/></b>
  </div>
    
    <c:set var="isprivilegeChecked" value=""></c:set>
  <c:choose>
  <c:when test="${jobOrder.writePrivilegeToSchool ne null}">

  	<c:if test="${jobOrder.writePrivilegeToSchool eq true}">
  		 <c:set var="isprivilegeChecked" value="checked='checked'"></c:set>
  	</c:if> 
  </c:when>
  <c:otherwise> 
  	<c:if test="${writePrivilegfordistrict eq true}">
  		 <c:set var="isprivilegeChecked" value="checked='checked'"></c:set>
  	</c:if> 
   </c:otherwise>              
   </c:choose>


    <div class="row top15" >
       <div class="col-sm-10 col-md-10">
             <label style="margin-left: 0px;">			  
            	<b><spring:message code="msgDistAnAttchSchR/WPriv"/> </b>            	
            	<c:choose>
            	<c:when test="${isprivilegeChecked}">
            	: <spring:message code="lblYes"/>
            	</c:when>
            	<c:otherwise>
            	
            	: <spring:message code="lblNo"/></c:otherwise>
            	</c:choose>
            	
		</label>
	</div>
</div>

   	</c:if>
<%--Mukesh --%>   	
   	<c:if test="${jobOrder.isInviteOnly eq true}">
   	    <div class="row top15" >
       		<div class="col-sm-10 col-md-10">
             	<label style="margin-left: 0px;">			  
            		<b>Visibility: </b>  hidden          	
				</label>
			</div>
		</div>
   	</c:if>
 
 <c:if test="${JobPostingURL!=null}">
	<div class="row">
	<div class="col-sm-10 col-md-10 mt10">
	<b><spring:message code="msgTmJoPostUrl"/></b><br>
	<a target="blank" href="${JobPostingURL}">${JobPostingURL}</a>
	</div>
	</div>
</c:if>


<div class="row hide" id="interviewPanel">
	 <div class="col-sm-10 col-md-10 mt10 checkbox inline" style="margin-left: -3px;">
		<b><spring:message code="lblPnl"/></b>
	</div>
	<div class="col-sm-10 col-md-10" style="margin-top: -10px;">
	   <div id="secStatusList"></div>
	</div>
</div>

<div class="hide" id="showExitURLDiv">
<div class="row">
 <div class="col-sm-10 col-md-10 mt10">
	<b><spring:message code="lblRe-DirU"/></b>
</div>

<div class="col-sm-11 col-md-11" style="margin-top: -5px;" >
<input type="hidden" name="exitURLMessageVal" id="exitURLMessageVal" value="${DistrictExitURLMessageVal}"/> 
<label  class="radio"><input type="radio" name="exitUrlMessage"  id="exitUrlMessage1" value="1" checked/><b> After completion  of TM Base Inventory, the applicant should be directed to</b></label>
<label>${DistrictExitURL}</label>
</div>
</div>
</div>
<input type="hidden"  name="approveButton" id="approveButton" value="${approveButton}"/>
<input type="hidden"  name="jobId" id="jobId" value="${jobId}"/>
<input type="hidden"  name="userId" id="userId" value="${userId}"/>
<input type="hidden"  name="entityType" id="entityType" value="${userMaster.entityType}"/>
<c:if test="${approveButton eq 1}">
<button class="btn btn-large btn-primary" onclick="return approveJob()" id="approveButtonHide"><spring:message code="btnApprove"/><i class="icon"></i></button>
<c:if test="${not empty userMaster.districtId && userMaster.districtId.districtId eq 804800}">
	<button class="btn btn-large btn-primary" id="denyButton" onclick="return denyJob()" id="denyButtonHide">Deny<i class="icon"></i></button>
</c:if>
</c:if>
</div>
<div class="modal hide" id="jobMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
			<p><spring:message code="msgJobAlrdyApprove"/></p>				
		 	</div> 	
		 	<div class="modal-footer">
		 			<span><button class="btn btn-large btn-primary" data-dismiss='modal' aria-hidden='true'><spring:message code="btnOk"/></button></span>
		 	</div>
		</div>
	  </div>
</div>

<div class="modal hide" id="wrongUrlMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
			<p><spring:message code="msgNotValidUrl"/></p>				
		 	</div> 	
		 	<div class="modal-footer">
		 			<span><button class="btn btn-large btn-primary" onclick="hideDiv()"><spring:message code="btnOk"/></button></span>
		 	</div>
		</div>
	  </div>
</div>

<div class="modal hide" id="confirmMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
			<p><spring:message code="msgWantToApproveJob"/></p>				
		 	</div> 	
		 	<div class="modal-footer">
		 	        <button class="btn btn-primary" onclick="approveJobOk()"><spring:message code="btnApprove"/></button>
		 	        <button class='btn' data-dismiss='modal' aria-hidden='true'><spring:message code="btnClr"/> </button>

		 	</div>
		</div>
	  </div>
</div>

<div class="modal hide" id="confirmShowMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
			<p><spring:message code="msgApproveSuccess"/></p>				
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="afterApproveJob()"><spring:message code="btnOk"/></button>
		 		
		 			
		 	</div>
		</div>
	  </div>
</div>

<div class="modal hide" id="unAuthorizeUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
			<p><spring:message code="msgNotAuthorized"/></p>				
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()"><spring:message code="btnOk"/></button>		 			
		 	</div>
		</div>
	  </div>
</div>

<div class="modal hide" id="approvedByUser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
			<p><spring:message code="msgAllreadyApproved"/></p>				
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()">Ok</button>		 			
		 	</div>
		</div>
	  </div>
</div>
<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>		
		<c:if test="${PageFlag!=0 ||DistrictExitURLMessageVal!=null }">
        	<script>  
        	var executeScript=1;        
        	if(document.getElementById("approveButton").value==2)
        	{
        	       $('#jobMessage').modal('show');        	  
        	} 
        	else if(document.getElementById("approveButton").value==0)
        	{
        		 $('#wrongUrlMessage').modal('show');
        	}  
        	else if(document.getElementById("approveButton").value==3)
        	{        	  
        		 $('#unAuthorizeUser').modal('show');
        	}else if(document.getElementById("approveButton").value==4)
        	{        	  
        		 $('#approvedByUser').modal('show');
        	}
        	   	     
        		if(document.getElementById("exitURLMessageVal").value==2){
					document.getElementById("exitURLMessageVal").value=2;
					document.getElementById("exitUrlMessage2").checked=true;
					document.getElementById("showExitURLDiv").style.display='none';
					document.getElementById("showExitMessageDiv").style.display='inline';
					
				}else{
					document.getElementById("exitURLMessageVal").value=1;
					document.getElementById("exitUrlMessage1").checked=true;
					document.getElementById("showExitURLDiv").style.display='inline';
					document.getElementById("showExitMessageDiv").style.display='none';
				}
			
			</script>	           
        </c:if>        
        <script type="text/javascript">
        if($("#jobId").val()!="" && $("#jobCatId").val()!=""){
			setStatusList($("#jobId").val(),$("#districtOrSchooHiddenlId").val(),$("#jobCatId").val());
		}		
		displayJobRecords();
		DisplayCertification();	
		ShowSchool();	
	</script>	

		
	
