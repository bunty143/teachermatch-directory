<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="step">
		<ul>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="personalinfo.do"><span><spring:message code="lblPersonal"/></span></a></li>
		<li><a id="hrefAcademics" <c:if test="${portfolioStatus.isAcademicsCompleted}"> class="stepactive" </c:if> href="academics.do"><span><spring:message code="headAcad"/></span></a></li>
		<li><a id="hrefCertifications" <c:if test="${portfolioStatus.isCertificationsCompleted}"> class="stepactive" </c:if> href="certification.do"><span><spring:message code="lblCrede"/></span></a></li>
		<li><a id="hrefExperiences" <c:if test="${portfolioStatus.isExperiencesCompleted}"> class="stepactive" </c:if> href="experiences.do"><span><spring:message code="headExp"/></span></a></li>
		<li><a id="hrefAffidavit" <c:if test="${portfolioStatus.isAffidavitCompleted}"> class="stepactive" </c:if> href="affidavit.do"><span><spring:message code="headAffidavit"/></span></a></li>
	</ul>
	<div class="clearfix">&nbsp;</div>
</div>
