<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/ExamAjax.js?ver=${resourceMap['ExamAjax.ajax']}"></script>
<script type="text/javascript" src="js/externaldashboard.js"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentCampaignAjax.js?ver=${resouceMap['DistrictAssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/districtAssessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>

<link rel="stylesheet" href="css/dialogbox.css" />

<style>
.heading{font-weight: bold; font-size:13px; color:#007AB4; padding:0px; margin-left:0px;  line-height: 35px;}
</style>



<div class="row">  
 <div class="col-sm-6">			                         
 	<div class='divErrorMsg' id='divExternalErrorMsg' style="display: block;"></div>
 </div>  
</div>
                         
<div class="row top10">
    <div class="col-sm-6">
         <label><spring:message code="lblExamCode"/></label>
    	    <input type="text" name="examinationCode" value="" id="examinationCode" placeholder="Enter Examination Code" class="form-control"  /> 
    </div>
    <div class="col-sm-4 top25">
       	<button class="btn fl btn-primary" style="padding: 6px 17px;"  type="submit" id="submitLogin" onclick="return validateExamCode()"><spring:message code="lblValidateCode"/><i class="icon"></i></button>
       </div>  
</div>  
<div id='examData' style="display: block;"></div>

<div style="display:none;" id="loadingDivInventory">
	 <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'><spring:message code="msgAssIsBeingLoaded"/></td></tr>
	</table>
</div>

<script type="text/javascript">
//checkDistrictInventory(1,1,3);
</script>

