<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="dwr/interface/ManageFacilitatorsAjax.js?ver=${resourceMap['ManageFacilitatorsAjax.ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/managefacilitators.js?ver=${resourceMap['js/managefacilitators.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<link rel="stylesheet" type="text/css" href="css/slider.css" />  
<script>
function applyScrollOnTblUser()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#facilitatorTempTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 600,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,150,150,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}
</script>

<div class="row top15">
	                    <div class="col-sm-12 col-md-12">
	                    <div id="facilitatorTempGrid">
		                  <table id="facilitatorTempTable" width="100%" border="0" class="table table-bordered mt10">
		                  </table>
	                    </div>
	                   </div>
	                  </div>

<div class="row top25">

           
	     	<button onclick="uploadFacilitator()" class="btn btn-primary" id="save"><strong><spring:message code="btnApt"/><i class="icon"></i></strong></button>	     	
	  
	        &nbsp;&nbsp;
	     	<button onclick="rejectFacilitator()" class="btn btn-primary" id="save"><strong><spring:message code="btnRjct"/> <i class="icon"></i></strong></button>	     	
	    
	      
</div>	                  
	<input type="hidden" id="eventIds" name="eventIds" value="${eventId}">                  
<script>
getTempFacilitatorList();
</script>