<!-- @Author: Gagan 
 * @Discription: view of edit domain Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>

<script type="text/javascript" src="dwr/interface/I4QuestionsSetQuesAjax.js?ver=${resourceMap['I4QuestionsSetQuesAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/i4/i4questionssetques.js?ver=${resourceMap['js/i4/i4questionssetques.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 






<style type="text/css">
textarea{
	width:500px;
	height:100px;
   }
   
   .jqte{
   }
   .errMsg {
   color: red;
}
</style>

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });
        
function applyScrollOnTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
	        $j('#i4QuesTable').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 280,
		        width: 945,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:[682,100,166], // table header width
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
	        });
        });			
}

function applyScrollOnTbl_1()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
	        $j('#i4QuesSetQuesTable').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 280,
		        width: 945,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:[782,166], // table header width
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
	        });
        });			
}

</script>

<input type="hidden" id="districtId"  value="${distID}"/>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="msgManageVirtualVideoQuest"/> </div>	
         </div>			
		 <div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="return addNewQues()"><spring:message code="lnkAddQues"/> </a>
		</div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row col-sm-12 col-md-12">
		<div class="row">
			<div class="mt10">
				<div class="subheading" style="font-size: 13px;">${quesSetName}</div>
			</div>
		</div>
</div>
<div class="TableContent top15">        	
         <div class="table-responsive" id="i4QuesSetQuesGrid"></div>            
</div> 
<div class="hide" id="addQuesDiv">
	
		<div class="row col-sm-12 col-md-12">			                         
		 	<div class='errMsg' id="errQuesdiv" ></div>
		</div>
		<div class="row">
			 <div class="col-sm-12 col-md-12">
				<div >
					<label><strong><spring:message code="lblQues"/> <span class="required">*</span></strong></label>
				</div>
				<div >
					<input type="hidden" name="quesId" id="quesId" >
					<input type="hidden" name="quesDate" id="quesDate" >
					<input type="hidden" name="quesSetId" id="quesSetId" value="${quesSetId}">
					<span id="i4QuesTxt"></span>
				</div>
			 </div>
		 </div> 
		 <div class="row mt10" id="divManage">
		  	<div class="col-sm-4 col-md-4">
		  			<button onclick="saveQuestion();" class="btn  btn-primary"><strong><spring:message code="btnSave"/>  <i class="icon"></i></strong></button>
		     	 &nbsp;<a href="javascript:void(0);" onclick="clearQues();"><spring:message code="btnClr"/></a>  
		    </div>
		  </div>
</div>
<!-- Search  -->
<div class="row" style="margin:0px;">
         <div style="float: left;">
         		
         </div>	
</div>
<div class="row col-sm-12 col-md-12">
		<div class="row">
			<div class="mt10">
				<div class="subheading" style="font-size: 13px;"><spring:message code="headAddFrmQuesPool"/> </div>
				 <div class="col-sm-6 col-md-6">
					<label> Question</label>
					<input type="text" class="form-control fl" name="quesSearchText" id="quesSearchText" >
				 </div>
				<div class="col-sm-2 col-md-2" style="margin-top:26px;">
			 		<button class="btn btn-primary " type="button" onclick="searchi4Ques()"><spring:message code="headSearch"/>  <i class="icon"></i></button>
			 	</div>
			 </div>
		 </div> 
</div>
<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="left">
		<tr>
			<td style="padding-top: 270px; padding-left: 450px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px; padding-left: 450px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<tr id='paymentMessage'>
			<td style='padding-top: 0px; padding-left: 450px;' id='spnMpro'
				align='center'></td>
		</tr>
	</table>
</div>
<div class="TableContent top15">        	
         <div class="table-responsive" id="i4QuesGrid"></div>            
</div> 
<div class="row col-sm-12 col-md-12">
			<div class="mt10">
				<a href="virtualvideoquestionsSet.do"><spring:message code="btnClr"/></a>
			</div>
</div>

<script type="text/javascript">
	displayQues();
	displayQuesSetQues();
	// $("#jqte editior txtQuestion").find(".jqte_editor").attr("contenteditable","false");
	
	//$(".jqte_editor").prop('contenteditable','false');
</script>


<!-- @Author: Gagan 
 * @Discription: view of edit domain Page.
 -->