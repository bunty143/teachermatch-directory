
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/LinkToKsnTalentsTempAjax.js?ver=${resourceMap['LinkToKsnTalentsTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/linktoksntalents.js?ver=${resourceMap['js/linktoksntalents.js']}"></script>

<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>

	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headImpTalentDetails"/></div>	
         </div>         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>


<input type="hidden" value="${entityType}" id="entityValue"/>

<c:if test="${entityType == 5 || entityType == 6}">
	<input type="hidden" id="headQuarterId" value="${headQuarterId}"/>
	</c:if>
	<c:if test="${entityType == 6}">
	<input type="hidden" id="branchId" value="${branchId}"/>
	</c:if>
	
<!--                End              -->


<div class="row top20">
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-5 col-md-5 importborder" style="margin-left: 15px;margin-right: 15px;">
	
	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	
		<form id='talentUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='talentUploadServlet.do' class="form-inline">
		<table cellpadding="5" align="center" >
		<tr><td colspan="2" style="padding-top:10px;">&nbsp;
			 <div class="control-group">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>
			</td>
		</tr>
		
		  <c:if test="${entityType ne 1}">
		  
		  <c:set var="showOrHide" scope="session" value="none"/>
		  
		  </c:if>
		  <c:if test="${entityType eq null || entityType eq '' || entityType eq 1}">
		     <c:set var="showOrHide" scope="session" value="fixed"/>
		   </c:if>
		
		<tr><td ><label>Talent Details <span class="required">*</span></label></td><td><input name="talentfile" id="talentfile" type="file"></td></tr>	
		<tr><td><button class="btn btn-primary fl" type="button" onclick="return validateTalentFile();" style="width: 100px;"><spring:message code="btnImpt"/> <i class="icon"></i></button></td></tr>
		</table>
		</form>
	</div>
</div>
<div class="row">	
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-8 col-md-8 mt10" style="margin-left: 5px;margin-right: 5px;">
			<table>
			<tr><td></td><td class="required">
			<spring:message code="headN"/></td></tr>
			<tr><td valign="top">*</td><td> <spring:message code="msgExelFi&SubsequntRData"/></td></tr>
			<tr><td valign="top">*</td><td><spring:message code="msgExcelFColN"/></td></tr>
			<tr><td></td><td>${job_Id_discription}</td></tr>
			<tr><td></td><td>talentId</td></tr>
			<tr><td></td><td>talent_first_name</td></tr>
			
			<tr><td></td><td>talent_last_name</td></tr>
			
			<tr><td></td><td>talent_email</td></tr>
			<tr><td></td><td>jobId</td></tr>
			<tr><td></td><td>ksnId</td></tr>
			<tr><td></td><td>tmksnId</td></tr>
			<tr><td></td><td>createdDateTime</td></tr>
			
			<tr><td valign="top">*</td><td><spring:message code="msgColNaOdr"/></td></tr>
			<tr><td valign="top">*</td><td><spring:message code="msgColNaCaseInSent"/></td></tr>
		</table>
	</div>
</div>
<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div> 
