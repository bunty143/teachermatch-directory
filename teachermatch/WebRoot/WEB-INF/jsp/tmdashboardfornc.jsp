<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/NcDashBoardAjax.js?ver=${resourceMap['NcDashBoardAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

 <link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<style>
.icon-largetemp {
    padding-top: 5px !important;
    padding-left: 8px !important;

}
.fa-comments-small:before {
    content: "\f086";
    font-size: 14px;
}
.fa-users-small:before {
    content: "\f0c0";
    font-size: 14px;
    }
.circle{
border-radius: 115em !important;
box-shadow: 0 0px 0px !important;

}

.positioniconColor {
	color: #009DF2;
}

.jobpostingColor {
	color: #83c000;
}

.applicationiconColor {
	color: #F984EC;
}

.candidateiconColor {
	color: #DD0A27;
}


.positioncircle {
	display: block;
	width: 40px;
	height: 40px;
	margin: 1em auto;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center center;
	-webkit-border-radius: 99em;
	-moz-border-radius: 99em;
	border-radius: 99em;
	border: 1px solid red;
	box-shadow: 0 3px 2px rgba(0, 0, 0, 0.3);
}

#thin-donut-chart {
    width: 300px; 
    height: inherit;
	position:absolute;
    top:8.2px;
    float:left;
    left:5%;
    margin:auto;   
}
.center-label, .bottom-label {
    text-anchor: middle;
    alignment-baseline: middle;
    pointer-events: none;
    font-size: 27px;
    font-weight: bold;
    color:white;
   }
.half-donut .center-label {
    alignment-baseline: initial;
}
a {
  color: #007AB4;
  text-decoration: none;  
  font-size: 11px; /* it is uncommented for tm dashboard text*/
}
div#wrapper {
	margin:0 auto;
/* width:986px;   */
	background:#FFF;

}
div#feed {
	//width:230px;
	//padding:10px;
	float:left;
}
div#sideBar {
	width:684px;
	//padding:10px;
	margin-left:10px;
	float:left;
	//border: 1px solid red;
}
.clear { 
	clear:both; 
}
div#sticker {
	padding:10px;
	//margin:10px 0;
	//background:#AAA;
	width:684px;
}
.stick {
	position:fixed;
	top:0px;
}

<!-- ---------------------------- -->

.items{
margin-top: 0px;
}
.dcard2{
border: 1px solid #dddddd;
width: 285px;
margin-bottom: 7px;
background-color: #F6F6F6;
}

#scrolling {
    position: absolute;
    top: 110px;
    right: 20px;
    border: 1px solid black;
    //background: #eee;
}
#scrolling.fixed {
    position: fixed;
    top: 0px;
}
.circle1 {
  display: block;
  width: 60px;
  height: 60px;
  margin: 1em auto;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  -webkit-border-radius: 99em;
  -moz-border-radius: 99em;
  border-radius: 99em;
  border: 1px solid #eee;
  box-shadow: 0 3px 2px rgba(0, 0, 0, 0.3);  
  background-color: pink;
}

.test_content{}
.scroller_anchor{}
.scroller{}

.circle_img{
 -moz-border-radius:52px;
 -webkit-border-radius:52px;
 border-radius:52px;
width:60px;
height:60px;

}
.tool{}
.profile{}
.icon-circle{
font-size: 6em;
color:pink;
}
  .popover-title {
  margin: 0;
  padding: 0px 0px 0px 0px;
  font-size: 0px;
  line-height: 0px;
  }
  .popover-content {
   margin: 0;
   padding: 0px 0px 0px 0px;
  }
.popover.right .arrow {
  top: 100px;
  left: -10px;
  margin-top: -10px;
  border-width: 10px 10px 10px 0;
  border-right-color: #ffffff;
}
.popover.right .arrow:after {
  border-width: 11px 11px 11px 0;
  border-right-color: rgba(0, 0, 0, 0.25);
  bottom: -11px;
  left: -1px;
}

.popover.bottom {
  margin-left: 10px;
}
.popover.bottom .arrow {
  top: -10px;
  left: 15%;
  margin-left: -10px;
  border-width: 0 10px 10px;
  border-bottom-color: #007AB4;
}
.popover.bottom .arrow:after {
  border-width: 0 11px 11px;
  border-bottom-color: rgba(0, 0, 0, 0.25);
  top: -1px;
  left: -11px;
}
.modal_header_profile {

  border-bottom: 1px solid #eee;
  background-color: #0078b4;
  text-overflow: ellipsis; 
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  outline: none;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
          background-clip: padding-box;
}

.net-widget-footer 
{
	border-bottom: 1px solid #cccccc; 
	border-left: 1px solid #cccccc;
	border-right: 1px solid #cccccc;
	line-height:40px; 
	background-color: #F2FAEF;
	background-image: -moz-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFFFF), to(#FFFFFF));
	background-image: -webkit-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -o-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: linear-gradient(to bottom,#FFFFFF, #FFFFFF);
	background-repeat: repeat-x;
	color:#4D4D4E;
	vertical-align: middle;
	
}
.net-corner-bottom 
{ 
	-moz-border-radius-bottomleft: 12px; -webkit-border-bottom-left-radius: 12px; border-bottom-left-radius: 12px; -moz-border-radius-bottomright: 12px; -webkit-border-bottom-right-radius: 12px; border-bottom-right-radius: 12px; 
}
.joborderwidth
{
	width: 990px;
	margin-left:-446px;
}


.scrollspy_profile { height:500px; overflow-y:auto; overflow-x:hidden;padding-left:5px;
}
.divwidth
{
	width:728px;
}
.tablewidth
{
width: 900px;
}
.custom-div-border1
{
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-moz-border-radius: 6px;
border-radius: 6px;
}
.custom-div-border
{
padding:1px;
border-bottom-left-radius:2em;
border-bottom-right-radius:2em;
}
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
.custom-border
{
margin-left: -32px;
margin-top: 2px;
}

 .fa-circle:before {
  content: "\f04d";
}
.iconcolor1{
color: #FF1919
}

.iconcolor2{
color: #FFF019
}
.iconcolor3{
color:#A4D53A
}

.toolTip {
    position: absolute;
    display: none;
    width: auto;
    height: auto;
    background: none repeat scroll 0 0 white;
    border: 0 none;
    border-radius: 8px 8px 8px 8px;
    box-shadow: -3px 3px 15px #888888;
    color: blue;
    font: 15px sans-serif;
    padding: 5px;
    text-align: center;
    
}
 .legend {
	            padding: 5px;
	            font: 10px sans-serif;
	            background: yellow;
	            box-shadow: 2px 2px 1px #888;
            }

</style>


<!-- Anurag for new  dashboard-->

		<style>
			.customicon{
				vertical-align:middle;
				width:35px;
				height:35px;
				border:none;
			}
			.ribbon  {
				display:inline-block;
			}
			
			.ribbon #link{ 
				color:#000;
				text-decoration:none;
			    float:left;
			    height:52px;
				overflow:hidden;
			}
			.ribbon span {
				/*background:#F4F6F0;*/
				display:inline-block;
			 	line-height:1.5em;
				padding:0 5px;
				margin-top:0.5em;
				position:relative;
				-webkit-transition: background-color 0.2s, margin-top 0.2s;  /* Saf3.2+, Chrome */
				-moz-transition: background-color 0.2s, margin-top 0.2s;  /* FF4+ */
				-ms-transition: background-color 0.2s, margin-top 0.2s;  /* IE10 */
				-o-transition: background-color 0.2s, margin-top 0.2s;  /* Opera 10.5+ */
				transition: background-color 0.2s, margin-top 0.2s;
			}
			

  
  .result_num{

font-family: 'Arial Black', 'Arial Bold', Gadget, sans-serif;
	font-size: 22px;
	font-style: normal;
	font-variant: normal;
	font-weight: 400;

	}

	.result_text{ 
		color:gray; 
	}
	.result_table{
	
	width:120px; 
	float:right;  border-collapse: collapse;
	height: 46px;
	text-align: center;
	
	}
	
 .mybtn{
  -webkit-box-shadow: 0px 1px 2px #eee;
  -moz-box-shadow: 0px 1px 2px #eee;
   box-shadow: 0px 1px 2px #eee;
  
  }
  
</style>


 

<div id="wrapper" style="">
	<div class="row top10 hide">
		<div class="col-sm-6 col-md-6">
			<p style="font-size: 14px; font-weight: bold;">
				
			</p>
		</div>
		<div class="col-sm-6 col-md-6 ">
			<div class="btn-group btn-group-xs" style="float: right" role="group"
				aria-label="View">
				<label style="float: left; margin-right: 10px; font-weight: bold">
					View Mode
				</label>
				<button type="button" class="btn btn-default mybtn" id="weekly"
					style="background: #fff;" onclick="setBasicAdvanceColor('basic')">
					Basic
				</button>
				<button type="button" class="btn btn-default mybtn" id="monthly"
					style="background: #fff;" onclick="setBasicAdvanceColor('advance')">
					Advanced
				</button>
			</div>
		</div>
	</div>
	
	
	
	
	
	<%-- Mukesh Rabin Strip --%>
	<div class="row top10">
		<div class="col-md-3 col-sm-3">
			<div class='ribbon'>
				<div id="link">
					<span> <span class="fa-comments-small icon-large icon-largetemp jobpostingColor circle" style="border-color: #83c000; border-width: 4px;margin-top:6px"></span>
						<table class="result_table" cellspacing="0" cellpadding="0">
							<tr>
								<td style="padding-top: 6px;">
									<p id="activeJobs" class="result_num" style="margin: 0px; color: #83c000">0
									</p>
									<p class="result_text" style="margin: 0px;">
										Jobs Posted
									</p>
								</td>
							</tr>

						</table> </span>
				</div>

			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class='ribbon'>
				<div id="">
					<span> <span class="icon-user icon-large icon-largetemp positioniconColor circle" style="border-color: #009DF2; border-width: 4px;"></span>
						<table class="result_table" cellspacing="0" cellpadding="0">
							<tr>
								<td style="padding-top: 6px;">
									<p id="totalPosition" class="result_num positioniconColor" style="margin: 0px;">0
									</p>
									<p class="result_text" style="margin: 0px;">
										Positions Available
									</p>
								</td>
							</tr>

						</table> </span>
				</div>

			</div>
		</div>
		<div class="col-md-3 col-sm-3">
			<div class='ribbon'>
				<div id="link">
					<span> <span> <span class="icon-book icon-large icon-largetemp applicationiconColor circle" style="border-color: #F984EC; border-width: 4px;margin-top:1px"></span>
						<table class="result_table" cellspacing="0" cellpadding="0">
							<tr>
								<td style="padding-top: 6px;">
									<p id="totalApplication" class="result_num applicationiconColor" style="margin: 0px;">0
									</p>
									<p class="result_text" style="margin: 0px;">
										Job Applications
									</p>
								</td>
							</tr>

						</table> </span>
				</div>

			</div>
		</div>
		
		<div class="col-md-3 col-sm-3">
			<div class='ribbon'>
				<div id="link">
					<span><span> <span class="fa-users-small icon-large icon-largetemp candidateiconColor circle" style="border-color: #DD0A27; border-width: 4px;margin-top:1px"></span>
							<br> </span>
						<table class="result_table" cellspacing="0" cellpadding="0">
							<tr>
								<td style="padding-top: 6px;">
									<p id="uniqueCandidate" class="result_num candidateiconColor"  style="margin: 0px;">0
									</p>
									<p class="result_text" style="margin: 0px;">
										Candidates
									</p>
								</td>
							</tr>

						</table> </span>
				</div>

			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-4 col-md-4" >
			 <div id='container1'  style="height: 350px;margin-left: 0px;padding-bottom: 0px;padding-top: 15px;"></div>
			 <div>
			  <p class="center" style="font-weight: bold;">
				  <span id="unFilledId" style="color: red; font-size: 20px;"></span> Unfilled<br>
				  <span id="underFilledId" style="color: red; font-size: 20px;"></span> Under-filled Schools
			  </p>
			 </div>
		</div>	
		
		<div class="col-sm-4 col-md-4"  >
			<div id='container2'  style="height: 350px;margin-left: 0px;padding-bottom: 5px;"></div>
		    <div>
				<p class="center" style="font-weight: bold;">
				   <span id="unCompetitaveId" style="color: red; font-size: 20px;"></span> Uncompetitive<br>
				   <span id="underCompetitaveId" style="color: red; font-size: 20px;"></span> Under-competitive Schools
				</p>
			</div>	
		</div>	
		
		<div class="col-sm-4 col-md-4"  >
			<div id='container3' class="" style="height: 350px;margin-left: 0px;padding-bottom: 5px;"></div>
		    <div>
		        <p class="center" style="font-weight: bold;">
				   <span id="inActiveLEId" style="color: red; font-size: 20px;"></span> Inactive Schools<br>
				   <span id="underinActiveLEId" style="color: red; font-size: 20px;"></span> Under-Active Schools
				</p>
			</div>	
		</div>	
		
	  </div>
	  
	  <div class="row">
	  	<div id="listUnFilledDistrictDiv" ></div> 
	  </div>
 	
    <div class="clear"></div>
  
</div>
 

<div style="display:none; z-index: 5000;" id="loadingDiv" >
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
 </div>

 
  
 
<script src="highcharts/js/highcharts.js"></script>
<script src="highcharts/js/modules/exporting.js"></script>
<script src="highslide/highslide-full.min.js"></script>
<script src="highslide/highslide.config.js"></script>
<link rel="stylesheet" type="text/css" href="highslide/highslide.css" />
  
 
 <script type="text/javascript" src="js/tmdashboardfornc.js"></script>
<script type="text/javascript" src="js/canvasjs.min.js"></script>


<script>
   
setBasicAdvanceColor('basic');
getTotalJobsAndCandidateDetail();

//first chart(position filled and hired)
positionFilledAndHiredChart();

//positionCompetitive
positionCompetitiveChart();

//LEAs Active
leasActiveChart();

//LEAS Active District/School
listUnFilledDistrict();

</script>


