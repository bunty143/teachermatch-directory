<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet" href="css/dialogbox.css" />
<script  type="text/javascript">
function validateUser()
{
	var flag = loginUserValidate();
  	if(flag==true)
  	{
  		$.ajax(
  		{
  			url: "isSA.do",
  			type: "POST",
  			async: false,
  			data:
  			{
  				emailAddress: $("#emailAddress").val(),
  				password: $("#password").val(),
  			},
  			success:function(result)
  			{
  				var myresult = jQuery.parseJSON(''+result+'');
  				if(myresult.isType3User=="true" && myresult.isAuthorizedUser=="true" && myresult.noOfSchools>1)
  				{
  					//$("#selectSchoolName").focus();
  					
  					$('#selectSchoolName').find('option').remove();//remove the previous element.
  					$("#modalErrorMsg").text("");//remove the error message.
  					$('#selectSchoolName').append("<option value='none'>Select school</option>");
  					$.each(myresult.schoolsDetails, function(index, data)
  					{
  						$('#selectSchoolName').append("<option value="+data.schoolId+">"+data.schoolName+"</option>");
  					});
  					$("#selectSchoolDiv").modal("show");
  					
  				}
  				else
  					$("#signinForm").submit();
  			},
  			error:function(jqXHR, textStatus, errorThrown)
  			{
  				alert("server error status:- "+textStatus+", error Thrown:- "+errorThrown+", error jqXHR:- "+jqXHR);
  			}
  		});
  	}
  	
  	return false;
}
/*When user click on ok then send the schoolName and school Id to the server*/
function ok()
{
	var schoolID = $("#selectSchoolName").val();
	var schoolName = $("#selectSchoolName option:selected").text();
	
	if(schoolID=="none")
		$("#modalErrorMsg").text("Please select school for login.");
	else
	{
		$("#selectSchoolDiv").modal("hide");	
		$("#schoolName").val(schoolName);
		$("#schoolID").val(schoolID);
		$("#signinForm").submit();
	}
}

function cancel()
{
	$("#selectSchoolDiv").modal("hide");
	$("#emailAddress").val("");
	$("#password").val("");
	$("#emailAddress").focus();
}
</script>

<div id="displayDiv"></div>
<div class="tabletbody">
<form  action="signincode.do" method="post" id="signinForm" >

<input type="hidden" id="key" name="key" value="${param.key}">
<input type="hidden" id="id" name="id" value="${param.id}">
<input type="hidden" id="schoolName" name="schoolName" value="">
<input type="hidden" id="schoolID" name="schoolID" value="">

                        <c:if test="${param.y eq 1}">
                        <div class="row"  style='padding-bottom:5px;padding-top:40px;'>
                          <div  class="col-sm-6 col-md-offset-3" style="padding-top: 0px;"  >
                                  <spring:message code="msgContWithAppProcess"/>
                          </div>
                         </div>
                        </c:if>
                        <div class="row top15">
						  <div class="col-sm-3 col-md-offset-3">
						   	  <div class="subheadinglogin"></div>
						  </div>
						  						  
						  <div class="col-sm-3 col-md-offset-3 systemsetup">						     
						      <a href="systemsetup.do" target="_blank">&nbsp;</a> <button  class="btn fl btn-secondary" type="button" onclick="return testYourSetUp();"><spring:message code="btnTestSetup"/><i class="icon2"></i></button>						   
						  </div>
						  
						 </div> 
						 <div class="row">  
                         <div class="col-md-offset-3">
   							        <div class="col-sm-6">			                         
									<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
									<div class='divErrorMsg' id='divServerError' style="display: block;">${msgError}</div>			             
				   					</div>  
                         </div>
                         </div>
                         <div class="row top10">
						 <div class="col-md-offset-3"> 						                     
                                   <div class="col-sm-6">
                                        <label><spring:message code="lblEmail"/></label>
                                   	    <input type="text" name="emailAddress" value="${emailAddress}" id="emailAddress" placeholder=" <spring:message code="msgEtrYuEmail"/>" class="form-control"  /> 
                                   </div>  
                          </div>
                          </div>  
                          <div class="row top-sm0">      
                          <div class="col-md-offset-3">            
                                  <div class="col-sm-4" style="width:33%;">
                                        <label><spring:message code="lblPass"/></label>
                                        <input type="password" name="password" id="password" placeholder=" <spring:message code="txtEnterPass"/>" class="form-control" maxlength="12" />
                                  </div>
                                  <div class="col-sm-3 top25">
                                  	<button class="btn fl btn-primary" style="padding: 6px 17px;"  type="submit" 
                                  	id="submitLogin" onclick="return validateUser()"><spring:message code="btnLogin"/> <i class="icon"></i></button>
                                  	<!-- <button class="btn fl btn-primary" type="submit" style="padding: 6px 11px;" onclick="return loginUserValidate()">Login <i class="icon"></i></button>  -->
                                  </div>                                                                
                          </div>
                          </div>  
                          <div class="row">                          
						  <div class="col-md-offset-3" >          
                                 <div class="col-sm-3" style="width:17%;padding-right: 0px;" >
	                              <div style="float: left;">
	                              	<label class="checkbox">
								    	<input type="checkbox" id="txtRememberme" name="txtRememberme"> <spring:message code="boxRemeberPass"/>
									</label>								                                                             
	                              </div>                               
                              </div>
                               <%--  
                              <div class="col-sm-1 top5" style="width:1%;padding-right: 0px;padding-left: 0px;font-size: 20px;">
	                                              <span><b>.</b></span>             
                              </div> 
                              <div class="col-sm-3 top10" style="padding-left: 5px;"><a href="forgotpassword.do">Forgot Password?</a></div> --%>                                
                         </div> 
                          </div>  
                 
</form>
<c:if test="${teacherblockflag eq 1}">
		<div class="modal" id="teacherblockdiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body">				
				<div class="control-group">
					<div class="" id="ddd">
						<p><spring:message code="msgDueToUnsuccessfullAccClickOnIforgot"/></p>
					</div>
				</div>
		 	</div> 	
		 	<div class="modal-footer">
		 			<span><button class="btn btn-large btn-primary" onclick="hideDive()"><spring:message code="btnOk"/> <i class="icon"></i></button></span>
		 	</div>
		</div>
	  </div>
	</div>		
</c:if>
<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 	</table>
</div>
</div>

<!-- -------------Show this popup if user is type 3 and has multiple schools--------------- -->
<div class="modal fade" id="selectSchoolDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content"  style="top:141px;left:-50px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" onclick="cancel()" aria-hidden="true">x</button>
				<h3><spring:message code="headTm"/></h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="modalErrorMsg" class="required" style="display:block;width:100%;height:10px;margin-top:-10px;"></div>
					<div class="row top10">
						<div class="col-sm-offset-0">
							<div class="col-sm-10" >
								<label><spring:message code="lblPlzSltSchool"/></label>
								<select id="selectSchoolName" name="selectSchoolName" class="form-control" style="font-family:'Century Gothic', 'Open Sans', sans-serif;font-size: 11px;line-height: 20px;">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" onclick="ok()"><spring:message code="btnLogin"/> <i class="icon"></i></button>
				<!-- <button class="btn btn-primary" onclick="ok()">Ok</button>  -->
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancel()"><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>
<!-- --------------------------------End of showing popup-------------------------------- -->

<script  type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>
<c:if test="${authmail}">
	<script>
		var divMsg="<div>${authmsg}</div>";
		addDialogContent("am","TeacherMatch",divMsg,"displayDiv");
		showDialog("am",actionForward,"560","200");
		function actionForward(){
			window.location.href="signin.do";
		}
	</script>
</c:if>
<script>
	$("#selectSchoolDiv").hide();
	$('#emailAddress').val("");
	$('#loadingDiv').hide();
	$("#teacherblockdiv").draggable();
	
	function divOn(){
		$('#loadingDiv').show();
	}
	document.getElementById("emailAddress").focus();
	function testYourSetUp() 
	{
		window.open("systemsetup.do");
	}
</script>