<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface//ReferenceCheckAjax.js?ver=${resourceMap['ReferenceCheckAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<script type="text/javascript" src="js/referencecheck.js?ver=${resouceMap['js/referencecheck.js']}"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />
<script>
$( document ).ready(function() {
   $('html, body').animate({scrollTop : 0},800);
});
</script>
<input type="hidden" name="elerefAutoId" id="elerefAutoId" value="${teacherElectronicReferences.elerefAutoId}"/>
<input type="hidden" name="districtId" id="districtId" value="${districtMaster.districtId}"/>
<input type="hidden" name="headQuarterId" id="headQuarterId" value="${headQuarterMaster.headQuarterId}"/>
<input type="hidden" name="jobId" id="jobId" value="${jobOrder.jobId}"/>
<input type="hidden" name="teacherId" id="teacherId" value="${teacherDetail.teacherId}"/>
<div id="loadingDiv"> <!-- style="display:none; z-index: 5000;"  -->
  	<table align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>

	<div class="row" style="margin:0px;">
	       <div style="float: left;">
	       	<img src="images/manageusers.png" width="41" height="41" alt="">
	       </div>        
			<div style="float: left;">
				<div class="subheading" style="font-size: 13px;"><spring:message code="headE-Ref"/></div>	
			</div>
			<c:choose>
				<c:when test="${districtMaster.displayName eq null}">
					<div class="pull-right add-employment1">${districtMaster.districtName}</div>							
				</c:when>
				<c:when test="${districtMaster.displayName eq ''}">
					<div class="pull-right add-employment1">${districtMaster.districtName}</div>							
				</c:when>
				<c:otherwise>
					<div class="pull-right add-employment1">${districtMaster.displayName}</div>	
				</c:otherwise>
			</c:choose>
		 		
		<div style="clear: both;"></div>
	   	<div class="centerline"></div>
	</div>

	
	<div>
		<div class='divErrorMsg' id='errordiv4question' style="display: block;padding-bottom: 7px;"></div>
		<table width="100%" border="0"  class="table table-bordered table-striped" id="tblGrid">

		</table>
		
		<button class="btn btn-large btn-primary" type="button" onclick="setDistrictQuestions('dashboard');" ><strong><spring:message code="btnConti"/> <i class="icon"></i></strong></button>
		
	</div>
	<form id="frmApplyJob" name="frmApplyJob" method="Post" action="applynow.do" onsubmit="return checkCL_dynamicPortfolio();">
	
	
	<div class="modal hide" id="myModalReferenceCheckConfirm" >
		<div class="modal-dialog" style="width:728px;">
	        <div class="modal-content">		
				<div class="modal-header">
			  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(1)'>x</button>
					<h3 id="statusTeacherTitle"><spring:message code="headE-RefConf"/></h3>
				</div>
				<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
				<div  class="modal-body">
					<div class="" id="referenceRecord">
						<spring:message code="msgOne/MoreScoresValueZero"/><BR/>
					</div>
			 	</div>

			 	<div class="modal-footer">
			 		<button class="btn btn-large btn-primary" type="button" onclick="setDistrictQuestions('auto');"><strong><spring:message code="btnConti"/> <i class="icon"></i></strong></button>
			 		<button type="button" class="btn"  onclick='closeReferenceCheck(1)'><spring:message code="btnClr"/></button>
			 	</div>
		    </div>
		</div>
	</div>
	
	
<script>showReferenceRecord();</script>

