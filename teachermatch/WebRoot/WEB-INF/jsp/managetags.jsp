<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageTagsAjax.js?ver=${resourceMap['ManageTagsAjax.Ajax']}"></script>
<script type='text/javascript' src="js/managetags.js?ver=${resourceMap['js/managetags.js']}"></script>
 <script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tagsTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 862,
        minWidth: null,
        minWidthAuto: false,
        colratio:[175,225,100,232,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
           
        });			
}
 </script>
 <div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMngT"/></div>	
         </div>
          <div class="pull-right add-employment1">
          <c:if test="${entityID eq 3 && userSession.districtId ne null && userSession.districtId.isCreateTags ne null && userSession.districtId.isCreateTags eq true}">
			  <a href="javascript:void(0);" onclick="return addNewTags()"><spring:message code="lnkAddT"/></a>
		  </c:if>
		 <c:if test="${entityID ne 3}">
		      <a href="javascript:void(0);" onclick="return addNewTags()"><spring:message code="lnkAddT"/></a>
		 </c:if>
		</div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>
<% String id=session.getAttribute("displayType").toString();%>
<input type="hidden" id="displayTypeId" value="<%=id%>"/>
<%if(id.equalsIgnoreCase("0")){ %>
 <c:if test="${entityID eq 1}">
<div  onkeypress="return chkForEnterSearchTags(event);">		
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="row mt10 <c:out value="${hide}"/>">
					<div class="col-sm-3 col-md-3">
						<label><spring:message code="lblEntityType"/></label>
					    <select class="form-control" id="entityType" name="entityType" class="span3" onchange="displayOrHideSearchBox();">
						 <c:if test="${not empty userMaster}">
			         			<option value="1"   selected="selected" ><spring:message code="sltTM"/></option>
								<option value="2"><spring:message code="optDistrict"/></option>
						</c:if>	
			            </select>			
	              	</div>
		            <div  id="Searchbox" class="col-sm-5 col-md-5 <c:out value="${hide}"/>">	         		   
		             			<label id="captionDistrictOrSchool"><spring:message code="lblDistrict"/></label>
			             		<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"	/>
								<input type="hidden" id="districtIdFilter" value="${userMaster.districtId.districtId}"/>
								<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>				           
	          		</div>
					<div class="col-sm-2 col-md-2">
						<label>&nbsp;</label>					
						<button class="btn btn-primary  top25-sm2" type="button" onclick="searchTags()">Search <i class="icon"></i></button>					
					</div>
        	  	</div>
       		 </form>
		          
</div>
</c:if>	

<%} %>
<div class="TableContent top15">        	
            <div class="table-responsive" id="tagsGrid">          
            </div>            
</div> 

<div class="row ">
	  <div class="col-sm-12 col-md-12">			                         
 			<div class='divErrorMsg' id='errordiv' ></div>
	  </div>	
</div>   
<div id="tagsDiv" class="hide top10" onkeypress="return chkForEnterSaveTags(event);">
	   <c:if test="${entityID ne 1}">
	   	<input type="hidden" id="districtId" value="${districtId}"/>
	   </c:if>	
	   <c:if test="${entityID eq 1}">	
		  <div class="row ">
          		<div  class="col-sm-5 col-md-5">
          		<label><spring:message code="lblDistrict"/></label>
           		<input type="text" id="districtName" name="districtName"  class="form-control"
           		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
				<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;'
				 onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
				<input type="hidden" id="districtId"/>
           	</div>
	      </div>
       </c:if>
	  <div class="row">
		<div class="col-sm-5 col-md-5"><label><strong><spring:message code="lblTName"/><span class="required">*</span></strong></label>
			<input type="text" name="tags" id="tags" maxlength="50" class="form-control" placeholder="">
			<input type="hidden" name="secStatusId" id="secStatusId">
			<input type="hidden" id="entityID" value="${entityID}"/>
		</div>
		<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id="frmTagsIcon" enctype='multipart/form-data' method='post' target='uploadFrame' action='fileuploadservletfortagsicon.do'>
			<div class="col-sm-3 col-md-3">
					<label>
						<spring:message code="lblTIcon"/>
					</label>
					<input id="pathOfTagsIconFile" name="pathOfTagsIconFile" type="file" width="20px;">
					<a href="javascript:void(0)" onclick="clearTagsFile()"><spring:message code="lnkClear"/></a>
			</div>
			<input type="hidden" id="pathOfTagsIcon"/>
			<span class="col-sm-3 col-md-3 top20" id="removeTagsIcon" name="removeTagsIcon" style="display: none;">
				<label>
				
				</label>
				<span id="divTagsName"></span>
				<!-- &nbsp;&nbsp;<br><a href="javascript:void(0)" onclick="removeTagsIconFile()">remove</a>
				<a href="#" id="iconpophover6" rel="tooltip" data-original-title="Remove tags icon!">
				<img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
			</span>
		</form>
	  </div>
	 
	  <div class="row mt15">
	  	<div class="col-sm-4 col-md-4">
	     	<button onclick="saveAndUploadTagsIcon();" class="btn btn-primary"><strong><spring:message code="btnSave"/><i class="icon"></i></strong></button>	     	
	 
	
	     &nbsp;<a href="javascript:void(0);" onclick="cancelTagsDiv()"><spring:message code="lnkCancel"/></a>  
	    </div>
	  </div>
</div>

<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 			<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='' id='spnMpro' align='center'><spring:message code="msgYrFileBegUpLod"/></td></tr>
	</table>
</div>
<input type="hidden" name="hqId" id="hqId" value="${hqId}"/>
<input type="hidden" id="branchId"  value="${branchId}"/>
<script>
	$('#iconpophover1').tooltip();
	$('#iconpophover6').tooltip();
	if($("#showSessionStatus").val()!=undefined){
		if($("#entityID")!=2){
			setEntityType();
		}else{
		displayOrHideSearchBox();
		}
	}else{
	displayOrHideSearchBox();
	}
	displayTags();
</script>

<script>
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>
