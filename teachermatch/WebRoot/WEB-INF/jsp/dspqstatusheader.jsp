<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="stepDSPQ">
	<ul>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblCoverLetter"/></span></a></li>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblPersonal"/></span></a></li>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblAcademic"/></span></a></li>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblCredentials"/></span></a></li>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblProfessional"/></span></a></li>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblPreScreen"/></span></a></li>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblEPI"/></span></a></li>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblQuestions"/></span></a></li>
		<li><a id="hrefPersonaInfo" <c:if test="${portfolioStatus.isPersonalInfoCompleted}"> class="stepactive" </c:if> href="#"><span><spring:message code="DSPQlblCompleted"/></span></a></li>
		
	</ul>
	<div class="clearfix">&nbsp;</div>
</div>
