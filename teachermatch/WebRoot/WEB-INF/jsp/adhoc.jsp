<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

                                
                                <div class="row">						  
							        <div class="col-sm-12">
									        <div style="float: left;">
									         	<div class="subheading">Ad-Hoc Reports</div>	
									        </div>
									        <div style="clear: both;"></div>	
									</div>
								       <div style="clear: both;"></div>							
			                    </div>
			                     <div class="row" style="margin-left: 0px; margin-right: 0px;">						  
							        <div class="col-sm-12">
							        	 <div style="float: left;">
							          <p>Have a special analytical report, statistical analysis or other summary to address a specific business question or issue?</p>
                                      <p>Our <B>Ad Hoc</B> tool allows you to create your own report, chart or table as the need arises for review and analysis. Powerful visualizations, intuitive dashboards, and on-demand reports all within reach. Contact your TeacherMatch representative or the Partner Success Center to learn more about adding ad-hoc reporting to your account.</p>
							          </div>
							        </div>
							      </div>	
							       <div class="row">						  
							        <div class="col-sm-12">
									        <div style="float: left;">
									         	<div class="subheading">Custom Reports</div>	
									        </div>
									        <div style="clear: both;"></div>	
									</div>
								       <div style="clear: both;"></div>							
			                    </div>
			                     <div class="row" style="margin-left: 0px; margin-right: 0px;">						  
							        <div class="col-sm-12">
							        	 <div style="float: left;">
							          <p>Our <B>Custom Reports</B> library includes any reports customized specifically to meet your business needs, parameters, language, etc.</p> <p>You can contact your <b>TeacherMatch</b> Partner Success Representative to help you design your own custom report.</p>
							          </div>
							        </div>
							      </div>
							       <div style="clear: both;"></div>	
							       <c:if test="${showdescription eq '1'}">	
							      <div class="row">						  
							        <div class="col-sm-3 "></div>
									        <div class="col-sm-9 " style="font-size: larger;color: #06799F">Description</div>
									</div>
									</c:if>
							      	 <div style="clear: both;"></div>		
							      <div class="row" style="width: 100%;float: left;">
							      <div class="col-sm-12" style="margin-left: 1%;">
								     <table width="100%" border="1" >
									     <c:forEach var="menuCustomReports" items="${menuCustomReports}" varStatus="status">
											<tr style=""><td align="center" style="width: 20%;"><a href="${menuCustomReports.pageName}" target="blank">${menuCustomReports.menuName}</a></td><td style="width: 80%;">${menuCustomReports.description}</td></tr>
						   				</c:forEach>
										</table>	
							      </div></div>
							      <div class="row">						  
							        <div class="col-sm-12">
									        <div style="float: left;">
									         	<div class="subheading">Standard Reports</div>	
									        </div>
									        <div style="clear: both;"></div>	
									</div>
								       <div style="clear: both;"></div>							
			                    </div>
			                    <div class="row" style="width: 100%;float: left;">
			                     <div class="col-sm-12" style="margin-left: 1%;">
							     <table width="100%" border="1" >
							     <c:forEach var="menuStandardReports" items="${menuStandardReports}" varStatus="status">
									<tr style=""><td align="left" style="width: 20%;"><a href="${menuStandardReports.pageName}" target="blank">${menuStandardReports.menuName}</a></td><td  style="80%;">${menuStandardReports.description}</td></tr>
								</c:forEach>
								</table></div> </div>		
	<div class="row top10">		
  </div>
  <script type="text/javascript">
                    function updateCgGridData()
					{
						$('#message2showConfirm').html('<spring:message code="msgWanttoUpdateCGGrid"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./updatecgbyuser.do' >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
					}
					function exportData()
					{
						$('#message2showConfirm').html('<spring:message code="msgwanttoExportdata"/>');
						$('#footerbtn').html("<button class='btn btn-primary' onclick=\"$('#myModal3').modal('hide');window.location.href='./service/getcandidatedataexport.do'\" >Ok</button> <button class='btn' data-dismiss='modal' aria-hidden='true'>Cancel</button>");
						$('#myModal3').modal('show');
					}

</script>
