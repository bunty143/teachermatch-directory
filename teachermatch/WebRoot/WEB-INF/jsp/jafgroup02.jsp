<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
        $j(document).ready(function() {
       });
</script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/group01.js?ver=${resouceMap['js/jobapplicationflow/group01.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jobapplflowcommon.js?ver=${resouceMap['js/jobapplicationflow/jobapplflowcommon.js']}"></script>

<!-- Academics -->
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>
<!--<script type='text/javascript' src="js/teacher/dynamicPortfolio_AutoCompAcademics.js?ver=${resouceMap['js/teacher/dynamicPortfolio_AutoCompAcademics.js']}"></script>
-->
<script type="text/javascript" src="js/jobapplicationflow/jafAutoCompAcademics.js?ver=${resouceMap['js/jobapplicationflow/jafAutoCompAcademics.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />


<p id="portfolioHeader"></p>
<input type="hidden" id="sNextURL" name="sNextURL" value="${sNextURL}"/>
<input type="hidden" id="sPreviousURL" name="sPreviousURL" value="${sPreviousURL}"/>
<input type="hidden" id="sCurrentPageShortCode" name="sCurrentPageShortCode" value="${sCurrentPageShortCode}"/>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:30px;">
         <div style="float: left;">
         	<img src="images/academics.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="DSPQlblAcademic"/></div>	
         </div>
         
         <div style="float: right;">
          	<div style='text-align:right; padding-right: 0px;' class="col-sm-3 col-md-3"><button class='flatbtn' style='width:190px;'  type='button' onclick='return validateDSPQAcademicGroup(0);'><spring:message code="btnSaveAndNextDSPQ"/> <i class='icon'></i></button></div>
         </div>
         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<p id="group02_2"></p>
<p id="group02"></p>

<div class="modal hide"  id="deleteAcademicRecord"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					<spring:message code="msgDeleteTheRecord" />
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	   <input type="hidden" id="academicID"/>
	 	     <span id=""><button class="btn  btn-primary" onclick="deleteAcademicRecord()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
	 	</div>
	  </div>
	</div>
</div>


<div class="modal hide"  id="deleteAcademicTrans"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					Are you sure, you would like to delete the Transcript?
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	   <input type="hidden" id="academicID"/>
	 	     <span id=""><button class="btn  btn-primary" onclick="removeTranscriptL2()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
	 	</div>
	  </div>
	</div>
</div>

<div class="modal hide"  id="academicGroupError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 500px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group" id="academicGroupErrorMsg">
			
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="CloseAcademicGroupError()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<!-- <button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> --> 		
 	</div>
	</div>
	</div>
</div>

<!-- start ... Attach jaf jsp files -->
	<jsp:include page="jafinnercommon.jsp"></jsp:include>
<!-- end ... Attach jaf jsp files -->

<script type="text/javascript">//<![CDATA[
 var cal = Calendar.setup({
  onSelect: function(cal) { cal.hide() },
  showTime: true
 });
 //]]>
    
</script>

<script>
var dspqPortfolioNameId=${dspqPortfolioName.dspqPortfolioNameId};
var candidateType="${candidateType}";
var groupId=${group};
var iJobId=${iJobId};

getPortfolioHeader(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup02_2(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup02(iJobId,dspqPortfolioNameId,candidateType,groupId);
</script>