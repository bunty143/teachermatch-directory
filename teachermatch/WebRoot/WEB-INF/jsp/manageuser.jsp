<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/UserAjax.js?ver=${resourceMap['UserAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/manageuser.js?ver=${resourceMap['js/manageuser.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 862,
        minWidth: null,
        minWidthAuto: false,
        colratio:[152,152,137,100,240,70], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>
<style>
	.hide
	{
		display: none;
	}
</style>	
<input type="hidden" id="userEntityType" value="${userMaster.entityType}"/>
<input type="hidden" id="isBracnchExists" value="${isBracnchExists}"/>
<c:if test="${(userMaster.entityType == 5)}">
<input type="hidden" id="headQuarterId" value="${userMaster.headQuarterMaster.headQuarterId}"/>
</c:if>
<c:if test="${(userMaster.entityType == 6)}">
<input type="hidden" id="branchId" value="${userMaster.branchMaster.branchId}"/>
</c:if>
	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headManageUser"/></div>	
         </div>
          <div class="pull-right add-employment1">
			<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
				<c:if test="${userMaster.entityType eq 2 and userMaster.roleId.roleId ne 7}">	 
					<c:if test="${not empty userKeyContact }">	 
						<a href="addedituser.do"><spring:message code="lnkAddUser"/></a>
					</c:if>
				</c:if>
				<c:if test="${userMaster.entityType eq 2 and userMaster.roleId.roleId eq 7}">	 
					<a href="addedituser.do"><spring:message code="lnkAddUser"/></a>
				</c:if>
				<c:if test="${not empty userMaster.districtId.saAddUser }">
					<c:if test="${userMaster.entityType eq 3 and userMaster.districtId.saAddUser eq true}">	 
						<a href="addedituser.do"><spring:message code="lnkAddUser"/></a>
					</c:if>
				</c:if>
				<c:if test="${userMaster.entityType ne 2 and userMaster.entityType ne 3}">	 
					<a href="addedituser.do"><spring:message code="lnkAddUser"/></a>
				</c:if>
			</c:if>
		 </div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>
	 <c:if test="${not empty userMaster}">
		<c:if test="${userMaster.entityType eq 3}">	        			
			<c:set var="hide" value=""></c:set>
			<c:set var="hide" value="hide"></c:set>
		</c:if>
	</c:if>
	<div onkeypress="return chkForEnterRecordsByEntityType(event);">	
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="row mt10 <c:out value="${hide}"/>">
				<div class='col-sm-12 col-md-12 divErrorMsg' id='errordiv' style="display: block;"></div>
					<div class="col-sm-2 col-md-2" style="width:180px;">
						<label><spring:message code="lblEntityType"/></label>
						
					    <select class="form-control " id="MU_EntityType" name="MU_EntityType" class="span3" onchange="displayOrHideSearchBox(<c:out value="${userMaster.entityType}"/>)">
					    <c:if test="${(userMaster.entityType == 5) || (userMaster.entityType == 6) }">
					    <c:forEach items="${roleCombo}" var="roleCombo">
								<option value="${roleCombo.key}" ${roleCombo.key==selectedRoleCombo ? 'selected' : ''}>
									${roleCombo.value}
								</option>
						</c:forEach>
						</c:if>	
						 <c:if test="${not empty userMaster}">
						 	 <c:if test="${userMaster.entityType eq 1}">	
						 	 	<option value="0"><spring:message code="optAll"/></option>          			
			         			<option value="1" selected="selected" ><spring:message code="sltTM"/></option>
							<c:forEach items="${roleCombo}" var="roleCombo">
								<option value="${roleCombo.key}" ${roleCombo.key==selectedRoleCombo ? 'selected' : ''}>
									${roleCombo.value}
								</option>
								</c:forEach>
								<option value="2"><spring:message code="optDistrict"/></option>
								<option value="3"><spring:message code="optSchool"/></option>
							</c:if>	
			        		<c:if test="${userMaster.entityType eq 2}">	
			         			<option value="0" ><spring:message code="optAll"/></option>        			
			         			<option value="2"  selected="selected" ><spring:message code="optDistrict"/></option>
								<option value="3"><spring:message code="optSchool"/></option>
							</c:if>
			        		<c:if test="${userMaster.entityType eq 3}">	        			
								<option value="3" selected="selected"><spring:message code="optSchool"/></option>
 			        		</c:if>
			              </c:if>
						</select>			
	              	</div>
	              	<div class="col-sm-4 col-md-4 hide hqClass" id="headQuarterClass">
                    	<label>Head Quarter</label>
                    	<input class="form-control clearback" type="text" id="onlyHeadQuarterName" name="onlyHeadQuarterName"   value="${headQuarterId}"
                    			onfocus="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
								onkeyup="getOnlyHeadQuarterAutoComp(this, event, 'divTxtHeadQuarterData', 'onlyHeadQuarterName','headQuarterHiddenId','');"
								onblur="hideHeadQuarterDiv(this,'headQuarterHiddenId','divTxtHeadQuarterData');"	/>
								
								<div id='divTxtHeadQuarterData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtHeadQuarterData','onlyHeadQuarterName')"  class='result' ></div>
								<input type="hidden" id="headQuarterHiddenId" value=""/>
								<input type="hidden" id="isBranchExistsHiddenId" value=""/>	
					</div>
					<div class="col-sm-4 col-md-4 hide branchClass" id="branchClass">
                    	<label>Branch</label>
                    	<input class="form-control clearback" type="text" id="onlyBranchName" name="onlyBranchName"   value="${branchId}"
                    			onfocus="getOnlyBranchAutoComp(this, event, 'divTxtBranchData', 'onlyBranchName','branchHiddenId','');"
								onkeyup="getOnlyBranchAutoComp(this, event, 'divTxtBranchData', 'onlyBranchName','branchHiddenId','');"
								onblur="hideBranchDiv(this,'branchHiddenId','divTxtBranchData');"	/>
								
								<div id='divTxtBranchData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtBranchData','onlyBranchName')"  class='result' ></div>
								<input type="hidden" id="branchHiddenId" value=""/>	
					</div>
	              
	              	<div class="col-sm-5 col-md-5 hide schoolClass" id="districtClass">
                    	<label><spring:message code="lblDistrict"/></label>
                    	<input class="form-control clearback" type="text" id="onlyDistrictName" name="onlyDistrictName"   value="${districtId}"
                    			onfocus="getOnlyDistrictAutoComp(this, event, 'divTxtDistrictData', 'onlyDistrictName','districtHiddenId','');"
								onkeyup="getOnlyDistrictAutoComp(this, event, 'divTxtDistrictData', 'onlyDistrictName','districtHiddenId','');"
								onblur="hideDistrictDiv(this,'districtHiddenId','divTxtDistrictData');"	/>
								
								<div id='divTxtDistrictData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtDistrictData','onlyDistrictName')"  class='result' ></div>
								<input type="hidden" id="districtHiddenId" value=""/>	
					</div>
		            <div  id="Searchbox" class="<c:out value="${hide}"/>">
		             		<div class="col-sm-5 col-md-5 DSClass">
		             			<label id="captionDistrictOrSchool"><spring:message code="lblDist/School"/></label>
		             			 <c:if test="${userMaster.entityType eq 1}">
		             			 <input type="text" id="districtORSchoolName" name="districtORSchoolName" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"	/>
								</c:if>
								 <c:if test="${(userMaster.entityType == 2) || (userMaster.entityType == 5) || (userMaster.entityType == 6)}">		
								 <input type="text" id="districtORSchoolName" name="districtORSchoolName" class="form-control clearback"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName',${userMaster.districtId.districtId},'');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName',${userMaster.districtId.districtId},'');"
								onblur="hideDistrictMasterDiv(this,${userMaster.districtId.districtId},'divTxtShowData');"	/>
								 </c:if>
								<input type="hidden" id="districtOrSchooHiddenlId" value=""/>
								<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>
			             	<input type="hidden" id="userSessionEntityTypeId" value="${userMaster.entityType}"/>
							<input type="hidden" id="schoolSessionId" value="${userSession.schoolId.schoolId}"/>
			             	 <input type="hidden" id="districtSessionId" value="${userSession.districtId.districtId}"/>
			             	<input type="hidden" id="schoolHiddenId" value="${userMaster.schoolId.schoolId}"/>
			             	<input type="hidden" id="districtHiddenId" value=""/>
		            
	          		</div>
					
				</div>   
        	  	<c:if test="${userMaster.entityType eq 3}">
        	  	<div class="top10"></div>
        	  	</c:if>     	  	
        	  	<div class="row">
				       <div class="col-sm-3 col-md-3">
				       <span class=""><label class=""><spring:message code="lblFname"/></label><input type="text" id="firstName" name="firstName"  maxlength="50"  class="help-inline form-control"></span>
				       </div>
				       <div class="col-sm-4 col-md-4">
					       <div class="">
					       	<span class=""><label class=""><spring:message code="lblLname"/></label><input type="text" id="lastName" name="lastName"  maxlength="50"  class="help-inline form-control"></span>
					       </div>
				       </div>				       
				       <div class="col-sm-3 col-md-3" style='white-space:nowrap; '>
					       <div><label class=""><spring:message code="lblEmailAddress"/></label><input type="text" id="emailAddress" name="emailAddress"  maxlength="75"  class="form-control fl">
					       </div>					       
				       </div>
				       <div class="col-sm-2 col-md-2">
				       <c:if test="${(userMaster.entityType == 5) || (userMaster.entityType == 6) }">
				       <button class="btn btn-primary top25-sm2" type="button" onclick="searchUser()">Search <i class="icon"></i></button>
				       </c:if>
				       <c:if test="${(userMaster.entityType != 5) && (userMaster.entityType != 6) }">
				       <c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
						<button class="btn btn-primary top25-sm2" type="button" onclick="searchUser()"><spring:message code="btnSearch"/><i class="icon"></i></button>
						</c:if>
						</c:if> 				      
				       </div>
			      </div>
       		 </form>
		          
	</div>
	
	
	<div class="TableContent top15">        	
            <div class="table-responsive" id="divMain">          
            </div>            
    </div> 
        
	
	<div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnX"/></button>
			<h3 id="myModalLabel"><spring:message code="headUser"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return editUser();"><spring:message code="btnOk"/></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
 		</div>
	</div>
	</div>
	</div>
	<div  class="modal hide"  id="myModalUserMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnX"/></button>
			<h3 id="myModalLabel"><spring:message code="headUser"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="UserMsg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
 		</div>
	</div>
	</div>
	</div>
	<div  class="modal hide"  id="authorisedMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnX"/></button>
			<h3 id="myModalLabel"><spring:message code="headUser"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="AuthorisedMsg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return authorisedUnauthorisedUser();"><spring:message code="btnOk"/></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
 		</div>
	</div>
	</div>
	</div>

<script type="text/javascript">

$('.accordion').on('show hide', function(e){
$(e.target).siblings('.accordion-heading').find('.accordion-toggle i').toggleClass('minus plus', 200);
});

</script>
<script type="text/javascript">
	displayUserMasterRecords(<c:out value="${userMaster.entityType}"/>);
</script>
<c:if test="${userId ne 0 && insertUpdateCheck ne 0}">
	<script type="text/javascript">
	//	alert('User has been added successfully');
	document.getElementById("UserMsg").innerHTML="User has been added successfully";
	$('#myModalUserMsg').modal('show');
	</script>
</c:if>
<c:if test="${userId ne 0 && insertUpdateCheck eq 0}">
	<script type="text/javascript">
	//	alert('User Details has been updated successfully');
		document.getElementById("UserMsg").innerHTML="User Details has been updated successfully";
		$('#myModalUserMsg').modal('show');
	</script>
</c:if>	                 