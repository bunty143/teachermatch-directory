<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link href="css/toolkitessentials.css" rel="stylesheet" type="text/css">
<script language='javascript' type='text/javascript' src='js/toolkitessentials.js'></script>
<div id="toolkitdivs">
<div id='toolkitDiv'>
<!--<a href='javascript:void(0);' onclick='callInfoLink("home");'>-->
<span class='titlecolor' style='font-size: 30px;'>
					Toolkit Essentials 
</span>					
<br/><img alt='Toolkit Essentials' src='images/ToolkitEssentials.png' style="" class='toolkitimage'>
<!--</a>-->

</div>

<div class='toolkitEMsgDiv'>
<p class='MsoListParagraph' >
<span class='toolkitEMessage' >
Welcome to Quest Toolkit Essentials, a collection of<br/> multi-media resources that you can use in the classroom to further<br/> engage your students and enhance your instructional practice.
</span>
</p>
</div>

<div class='row imagelink' >
	<div class='col-sm-6 col-md-6 '>
		<div class='row'>
			<div class='col-sm-6 col-md-6 cssCenter'>
				<a href='javascript:void(0);' onclick='callInfoLink("videos");'>  
					<img alt='Videos' src='images/tquest/videos.png'
						width='225px' height='150px' class='imgmarginlink' > 
					<span class='titlecolor' >
						Videos </span>	
				</a>
			</div>
			<div class='col-sm-6 col-md-6 cssCenter' >
				<a href='javascript:void(0);' onclick='callInfoLink("books");'>  
					<img alt='Books' src='images/tquest/books.png'
						width='225px' height='150px' class='imgmarginlink'>
					<span class='titlecolor'>
						Books </span>	
				</a>
			</div>
		</div>
	</div>
	<div class='col-sm-6 col-md-6 '>
		<div class='row'>
			<div class='col-sm-6 col-md-6 cssCenter' >
				<a href='javascript:void(0);' onclick='callInfoLink("infographics");'>  
					<img alt='Infographics' src='images/tquest/infographics.png'
						width='225px' height='150px' class='imgmarginlink'>
					<span class='titlecolor' >
						Infographics </span>	
				</a>
			</div>
			<div class='col-sm-6 col-md-6 cssCenter' >
				<a href='javascript:void(0);' onclick='callInfoLink("worksheets");'>  
					<img alt='Graphics Organizers and Worksheets' src='images/tquest/worksheet.png'
						width='225px' height='150px' class='imgmarginlink'> 
					<span class='titlecolor' >
						Graphics Organizers and Worksheets </span>	
				</a>
			</div>
		</div>
	</div>
</div>
<br/>

<div class='row' id='home' >
	<div class='col-sm-6 col-md-6 hfinfo'>
		<div class='row'>
			<div class='col-sm-6 col-md-6 ' style="margin-left:-12px;">
				From Science and Animals to climate and health -- explore expertly curated video selections to enhance the classroom experience. 
			</div>
			<div class='col-sm-6 col-md-6 '>
				Browse our popular and classic titles, sorted by grade level to aid teachers with the book recommendation process.
			</div>
		</div>
	</div>
	<div class='col-sm-6 col-md-6 hfinfo'>
		<div class='row'>
			<div class='col-sm-6 col-md-6 ' >
				Stay abreast of the latest trends in education quickly and easily with our selection of insightful infographics.
			</div>
			<div class='col-sm-6 col-md-6 '>
				Discover graphic tools and worksheets available to help advance the understanding of core lesson plan principals.
			</div>
		</div>
	</div>
</div>

<div class='row' id='videos'>
		<br/>
			<div class='infotoolkitElink'>
				 <span class='titlecolor' style='font-size: 25px;color:#4D4D4F;'>
						Videos </span> <br /><br />
				<p align='left'>
				Using video in the classroom is a great way to supplement or enhance a lesson or unit. According to a 
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.cisco.com/web/strategy/docs/education/ciscovideowp.pdf");'>Cisco Report</a>
				<!--<br/>--> citing a large number of studies conducted by colleges and universities, on-demand video has been shown
				<!--<br/>--> to impact grades and test performances. Some of the studies conclude that students who engage in [viewing
				<!--<br/>--> streaming video] out perform peers who are in a traditional face-to-face classroom.
				</p>

			</div>
			<!--<br/>
			<div style=' width: 100%; margin-top: 10px;margin-left: 10px;z-index: 1008;'>
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/tweet1.jpg' width='20px' height='20px'>&nbsp;Tweet</a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Share' src='images/tquest/linkdin.png' width='20px' height='20px'>&nbsp;Share</a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Like' src='images/tquest/f_like.png' width='60px' height='20px'></a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/fb-share1.png' width='60px' height='20px'></a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/g+_icon.jpg' width='40px' height='20px'></a>
			</div>
			-->
			<br/>
			<div class='col-sm-6 col-md-6 infobmargin'>		
				<ul>
				<p><b>Science</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/ea6eea11-1754-44b0-8559-7f91951110b9/ea6eea11-1754-44b0-8559-7f91951110b9/");'>Where does carbon dioxide come from? </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/arct14.sci.dscenter/what-is-center-if-gravity/");'>What is the center gravity? </a></li>
				</ul> 
				<ul><p><b>Animals</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/nat14.sci.lifsci.masters/honey-badgers-masters-of-mayhem/");'>Honey Badgers: Masters of Mayhem </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/cecfba5a-e23d-4e42-a7f7-0514a509afe9/shark-biology/");' >Shark Biology </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/03f07118-ce19-48b5-bf10-fb8818108f69/the-shark-with-the-tall-tail/");'>The Shark with the Long Tail </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/7922c412-a2ea-403a-9451-b5ff2d584030/whale-sharks/");'>Whale Sharks </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/6254750a-fc61-40c2-8649-f39bbe2766f6/floridas-wild-side/");'>Florida's Wild Side </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/klvx08.sci.klvxtort/desert-tortoise/");'>Desert Tortoise</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/c8af35a7-ba5d-4951-9228-b8febe21d4e8/cool-critters-great-horned-owl/");'>Cool Critters: Great Horned Owl </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/857be844-8c77-466a-8854-e2209998b679/857be844-8c77-466a-8854-e2209998b679/");'>Cool Critters: Dwarf Cuttlefish </a></li>
				</ul> 
			</div>
			<div class='col-sm-6 col-md-6 infobmargin rightDiv' >
				<ul><p><b>Climate</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/5f8b08fb-6d51-4f4f-9f2a-10a562c85f20/5f8b08fb-6d51-4f4f-9f2a-10a562c85f20/");'>Climate Change Around the World </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/1e3a8e34-9912-4542-b739-f40033665bcd/1e3a8e34-9912-4542-b739-f40033665bcd/");'>Rapid Climate Change and the Oceans</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/4c13ee6c-2ed1-4ab3-bef6-4a1acaebb348/4c13ee6c-2ed1-4ab3-bef6-4a1acaebb348/");'>A Sick Planet</a></li>
				</ul> 
				<ul><p><b>Health</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/eb12ac7a-a6fe-4758-80e2-7b12f588beba/hand-washing/");'>Hand washing</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.pbslearningmedia.org/resource/8d692f62-b525-4e08-a8ad-3285f8f7aae8/8d692f62-b525-4e08-a8ad-3285f8f7aae8/");'>Internet Safety</a></li>				
				</ul> 
				<ul><p><b>Miscellaneous</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.onlinecollegecourses.com/2011/11/28/20-incredible-ted-talks-you-should-show-your-high-school-students/");'>20 TED Talks to Show Your High School Students </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.watchknowlearn.org/Video.aspx?VideoID=9067&CategoryID=4937");'>How to Write an Argument Essay</a></li>
				</ul> 
			</div>
		</div>
	

<div class='row' id='books' >
		<br/>
			<div class='infotoolkitElink'>
				 <span class='titlecolor' style='font-size: 25px;color:#4D4D4F;'>
						Books </span> <br /><br />
				<p align='left'>
				Literacy and reading comprehension are core skills that students focus on throughout K-12 learning plans. That's
				<!--<br/>-->why we've created lists of popular and classic titiles, sorted by grade level, to aid you in the process of recommend-
				<!--<br/>-->ing titles to your students.
				</p>

			</div>
			<!--<br/>
			<div style=' width: 100%; margin-top: 10px;margin-left: 10px;'>
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/tweet1.jpg' width='20px' height='20px'>&nbsp;Tweet</a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Share' src='images/tquest/linkdin.png' width='20px' height='20px'>&nbsp;Share</a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Like' src='images/tquest/f_like.png' width='60px' height='20px'></a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/fb-share1.png' width='60px' height='20px'></a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/g+_icon.jpg' width='40px' height='20px'></a>
			</div>
			-->
			<br/>
			<div class='col-sm-6 col-md-6 infobmargin' >		
				<ul><p><b>Books for 1st Graders</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/537296.Frog_and_Toad_Are_Friends");'>Frogs and Toads are Friends </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/108980.Ivy_and_Bean");'>Lvy and Bean </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/332575.The_Empty_Pot");'>The Empty Pot</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/24178.Charlotte_s_Web");'>Charlott's Web</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/61549.Mr_Popper_s_Penguins");'>Mr. Popper's Penguins</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://store.scholastic.com/shop/Books/4502~4519?isbn13=9780395557013");'>Five Little Monkeys Jumping on the Bed</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/114308.Swimmy");'>Swimmy</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.scholastic.com/parents/book/make-way-ducklings");'>Make Way for Ducklings</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/144974.The_Velveteen_Rabbit");'>The Velvteen Rabbit</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/19543.Where_the_Wild_Things_Are");'>Where the Wild Things Are</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.amazon.com/Very-Busy-Spider-Lift-Flap/dp/0448444216");'>The Very Busy Spider</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.amazon.com/Kittens-First-Full-Kevin-Henkes/dp/0060588284/ref=sr_1_1?s=books&ie=UTF8&qid=1416418754&sr=1-1&keywords=kittens+first+full+moon");'>Kitten's First Full Moon</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/16963.The_Hello_Goodbye_Window");'>The Hello, Goodbye Window</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.amazon.com/House-Night-Susan-Marie-Swanson/dp/0547577699/ref=sr_1_1?s=books&ie=UTF8&qid=1416418774&sr=1-1&keywords=the+house+at+night");'>The House in the Night</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/147732.Miss_Nelson_Is_Missing_");'>Miss Nelson is Missing!</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/46677.Alexander_and_the_Terrible_Horrible_No_Good_Very_Bad_Day");'>Alexander and the Terrible, Horrible, No Good, Very Bad Day</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/774001.Amelia_Bedelia");'>Amelia Bedelia</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/197084.Are_You_My_Mother_");'>Are You My Mother?</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/824734.Dinosaurs_Before_Dark");'>Dinosaurs Before Dark</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/268955.Mummies_in_the_Morning");'>Mummies in the Morning</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.amazon.com/Nate-Great-Marjorie-Weinman-Sharmat/dp/044046126X/ref=sr_1_1?s=books&ie=UTF8&qid=1416418793&sr=1-1&keywords=nate+the+great");'>Nate the Great</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/harold-and-the-purple-crayon-crockett-johnson/1100058493?ean=9780064430227");'>Harold and the PUrple Crayon</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/1062516.No_David_");'>No, David!</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/twister-on-tuesday-mary-pope-osborne/1100292287?ean=9780679890690");'>Twister on Tuesday</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/extra-yarn-mac-barnett/1103168080?ean=9780061953385");'>Extra Yarn</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/786256.Stellaluna");'>Stellaluna</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/23772.Green_Eggs_and_Ham");'>Green Eggs and Ham</a></li>
				</ul> 
				<ul><p><b>Books for 2nd Graders</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/dear-max-sally-grindley/1008517668?ean=9781416934431");'>Dear Max</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/painted-dreams-karen-lynn-williams/1003001236?ean=9780688139018");'>Painted Dreams</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/clara-and-the-bookwagon-nancy-smiler-levinson/1000492322?ean=9780064441346");'>Clara and the Bookwagon</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/mom-and-dad-are-palindromes-mark-shulman/1103739930?ean=9781452136431");'>Mom and Dad are Palindromes</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/mr-george-baker-a-hest/1102041421?ean=9780763633080");'>Mr. George Baker</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/my-dads-a-birdman-david-almond/1100752408?ean=9780763653453");'>My Dad's a Birdman</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.barnesandnoble.com/w/the-puppy-sister-don-smith/1111661060?ean=9781106912572");'>The Puppy Sister</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/370493.The_Giving_Tree");'>The Giving Tree</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/297249.The_Boxcar_Children");'>The Boxcar Children</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/100915.The_Lion_the_Witch_and_the_Wardrobe");'>The Lion, the Withch, and the Wardrobe</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/8337.Little_House_in_the_Big_Woods");'>Little House in the Big Woods</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/17331434-the-year-of-billy-miller");'>The Year of Billy Miller</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/439173.Frindle");'>Frindle</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/474858.A_Bad_Case_of_Stripes");'>A Bad Case of Stripes</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/37738.Freckle_Juice");'>Freckle Juice</a></li>					
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/608393.Cam_Jansen_and_the_Ghostly_Mystery");'>Cam Jansen and the Ghostly Mystery</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/7823678-the-ugly-truth");'>The Ugly Truth</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/310258.The_Snowy_Day");'>The Snowy Day</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/231850.Corduroy");'>Corduroy</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/857445.Officer_Buckle_Gloria");'>Officer Buckles & Gloria</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/881.Pompeii_Buried_Alive_");'>Pompeii...Buried Alive!</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/765148.Giraffes_Can_t_Dance");'>Giraffes Can't Dance</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/4502507-the-last-olympian");'>The Last Olympian</a></li>					
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/6670.The_Magic_Finger");'>The Magic Finger</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/11360892-balloons-over-broadway");'>Ballons Over Broadway: The True Story of the Puppeteer of Macy's Parade</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/6310.Charlie_and_the_Chocolate_Factory");'>Charlie and the Chocolate Factory</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/97598.Cat_and_Mouse_in_a_Haunted_House");'>Cat and Mouse in a Haunted House</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/479229.Ruby_Lu_Brave_and_True");'>Ruby Lu, Brave and True</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/6277757-lunch-lady-and-the-cyborg-substitute");'>The Lunch Lady and the Cyborg Substitute</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/407429.The_Stinky_Cheese_Man");'>The Stinky Cheese Man: And Other Fairly Stupid Tales</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/825081.Lilly_s_Purple_Plastic_Purse");'>Lilly's Purple Plastic Purse</a></li>					
				</ul> 
			</div>
			<div class='col-sm-6 col-md-6 infobmargin rightDiv' >
				<ul><p><b>Books for 3rd Graders</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/37186.The_Miraculous_Journey_of_Edward_Tulane");'>The Miraculous Journey of Edward Tulane</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/84981.Tuck_Everlasting");'>Tuck Everlasting</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/281235.Bunnicula");'>Bunnicula</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/3293821-the-last-straw");'>The Last Straw</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/232109.The_Mouse_and_the_Motorcycle");'>The Mouse and the Motorcycle</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/28187.The_Lightning_Thief");'>The Lightning Thief</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/6693.Fantastic_Mr_Fox");'>Fantastic Mr. Fox</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/378.The_Phantom_Tollbooth");'>The Phantom Tollbooth</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/930612.Judy_Moody");'>Judy Moody</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/38709.Holes");'>Holes</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/5.Harry_Potter_and_the_Prisoner_of_Azkaban");'>Harry Potter and the Prisoner of Azkaban</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/3.Harry_Potter_and_the_Sorcerer_s_Stone");'>Harry Potter and the Sorcerer's Stone</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/138959.Stuart_Little");'>Stuart Little</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/8366238-the-trouble-with-chickens");'>The Trouble With Chickens</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/5983694-where-the-mountain-meets-the-moon");'>Where the Mountain Meets the Moon</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/7684993-school-drool-and-other-daily-disasters");'>School, Drool, and Other Daily Disasters</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/91248.Ramona_the_Brave");'>Ramona the Brave</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/15779.Sideways_Stories_from_Wayside_School");'>Sideways Stories from Wayside School</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/236093.The_Wonderful_Wizard_of_Oz");'>The Wonderful Wizard of Oz</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/15818222-stick-dog");'>Stick Dog</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/11104413-violet-mackerel-s-brilliant-plot");'>Violet Mackerel's Brilliant Plot</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/6345760-the-magician-s-elephant");'>The Magician's Elephant</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/3077977-never-glue-your-friends-to-chairs");'>Zita the Spacegirl</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.goodreads.com/book/show/3077977-never-glue-your-friends-to-chairs");'>Never Glue Your Friends to Chairs</a></li>
				</ul>  
				<ul><p><b>Books for High School</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/19063.The_Book_Thief");'>The Book Thief</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/10210.Jane_Eyre");'>Jane Eyre</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/4381.Fahrenheit_451");'>Fahrenheit 451</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/48855.The_Diary_of_a_Young_Girl");'>The Diary of a Young Girl</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/5470.1984");'>1984</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/12296.The_Scarlet_Letter");'>The Scarlet Letter</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/7613.Animal_Farm");'>Animal Farm</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/5107.The_Catcher_in_the_Rye");'>The Catcher in the Rye</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/7624.Lord_of_the_Flies");'>Lord of the Flies</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/4671.The_Great_Gatsby");'>The Great Gatsby</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/2657.To_Kill_a_Mockingbird");'>To Kill a Mockingbird</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/18135.Romeo_and_Juliet");'>Romeo and Juliet</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/3636.The_Giver");'>The Giver (The Giver, #1)</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/588138.The_Hero_With_a_Thousand_Faces");'>The Hero With a Thousand Faces</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/18250707-spilled-milk");'>Spilled Milk: Based on a True Story</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/22821333-learning-to-kiss-in-the-snow");'>Learning to Kiss in the Snow</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/11569.Different_Seasons");'>Different Seasons</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/13335037-divergent");'>Divergent (Divergent, #1)</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/561909.The_Hiding_Place");'>The Hiding Place</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/11206.In_the_Time_of_the_Butterflies");'>In the Time of the Butterflies</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/231804.The_Outsiders");'>The Outsiders</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/3.Harry_Potter_and_the_Sorcerer_s_Stone");'>Harry Potter and the Sorcerer's Stone (Harry Potter, #1)</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/12996.Othello");'>Othello</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/17267.The_Great_Divorce");'>The Great Divorce</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/4981.Slaughterhouse_Five");'>Slaughterhouse-Five</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/58345.The_Awakening");'>The Awakening</a></li>	
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/12957.Much_Ado_About_Nothing");'>Much Ado About Nothing</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/133518.The_Things_They_Carried");'>The Things They Carried</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/944648.The_Picture_of_Dorian_Gray_and_Other_Stories");'>The Picture of Dorian Gray and Other Stories</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/117833.The_Master_and_Margarita");'>The Master and Margarita</a></li>			
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/12532265-the-kite-runner");'>The Kite Runner</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/18405.Gone_with_the_Wind");'>Gone with the Wind</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.goodreads.com/book/show/92057.The_Autobiography_of_Malcolm_X");'>The Autobiography of Malcolm X</a></li>					
				</ul> 
				
			</div>		
		</div>
		<div class='row commingsoon'>
			<div style='background-color: #0878AE;padding-left:50px;padding-right:50px;padding-top:30px;padding-bottom:25px;text-align: center;'>
				 <span style='font-size: 25px;color:white;text-align: center;font-weight: bold;'>
						COMING SOON! </span> <br /><br />
				<div align='center' style='color:white;font-size: 15px;line-height:30px;'>
				    Books for 4th Graders  &nbsp;&nbsp;<img class='bcircle' src='images/tquest/circle.png'/>&nbsp;&nbsp;   Books for 5th Graders   &nbsp;&nbsp;<img class='bcircle' src='images/tquest/circle.png'/>&nbsp;&nbsp;   Books for 6th Graders
						<br/>Books for 7th Graders   &nbsp;&nbsp;<img class='bcircle' src='images/tquest/circle.png' />&nbsp;&nbsp;   Books for 8th Graders					
				</div>
					
				</div>
			</div>
			

<div class='row' id='infographics' >
		<br/>
			<div class='infotoolkitElink'>
				 <span class='titlecolor' style='font-size: 25px;color:#4D4D4F;'>
						Infographics </span> <br /><br />
				<p align='left'>
				Infographics are a great way to visually digest information. Below are some of our favorite infographics that can
				<!--<br/>--> help you prepare for and understand emerging trends in education and instruction.
				</p>

			</div>
			<!--<br/>
			<div style=' width: 100%; margin-top: 10px;margin-left: 10px;'>
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/tweet1.jpg' width='20px' height='20px'>&nbsp;Tweet</a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Share' src='images/tquest/linkdin.png' width='20px' height='20px'>&nbsp;Share</a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Like' src='images/tquest/f_like.png' width='60px' height='20px'></a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/fb-share1.png' width='60px' height='20px'></a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/g+_icon.jpg' width='40px' height='20px'></a>
			</div>
			-->
			<br/>
			<div class='infographicssrlink' >	
			<p><b>Job Search</b><br/>					
			<a href='javascript:void(0);' onclick='openNewWindow("http://dailyinfographic.com/wp-content/uploads/2012/04/Anatomy-of-a-Job-Interview.jpg");'>http://dailyinfographic.com/wp-content/uploads/2012/04/Anatomy-of-a-Job-Interview.jpg</a>
			</p>
											
			<p><b>First Day of Class</b><br/>					
			<a href='javascript:void(0);' onclick='openNewWindow("https://magic.piktochart.com/output/373549-first-day-of-class");'>http://magic.piktochart.com/output/373549-first-day-of-class</a>
			</p>
			<p><b>Tach in the Classroom</b><br/>					
			<a href='javascript:void(0);' onclick='openNewWindow("http://www.edtechmagazine.com/higher/article/2012/11/where-does-gamification-fit-higher-education-infographic");'>http://www.edtechmagazine.com/higher/article/2012/11/where-does-gamification-fit-higher-education-infographic</a>
			</p>
			<p><b>Common Core & Learning</b><br/>				
			<a href='javascript:void(0);' onclick='openNewWindow("http://www.teachthought.com/wp-content/uploads/2012/07/blended-learning.jpg");'>http://www.teachthought.com/wp-content/uploads/2012/07/blended-learning.jpg</a>
			</p>
			<p><b>Classroom Management</b><br/>					
			<a href='javascript:void(0);' onclick='openNewWindow("http://info.nspt4kids.com/adhd-infographic");'>http://info.nspt4kids.com/adhd-infographic/</a>
			</p>
			</div>			
		</div>

<div class='row' id='worksheets'>
		<br/>
			<div class='infotoolkitElink'>
				 <span class='titlecolor' style='font-size: 25px;color:#4D4D4F;'>
					Graphic Organizers and Worksheets	 </span> <br /><br />
				<p align='left'>
				Graphic organizers and student worksheets are essential to any lesson plan...and online teacher resources have
				<!--<br/>-->made it easy to find and share them. We've created a robust repository that you can print and use in your class-
				<!--<br/>-->room to help students develop a strong understanding of lesson topics.  
				</p>

			</div>
			<!--<br/>
			<div style=' width: 100%; margin-top: 10px;margin-left: 10px;'>
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/tweet1.jpg' width='20px' height='20px'>&nbsp;Tweet</a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Share' src='images/tquest/linkdin.png' width='20px' height='20px'>&nbsp;Share</a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Like' src='images/tquest/f_like.png' width='60px' height='20px'></a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/fb-share1.png' width='60px' height='20px'></a>
			&nbsp;&nbsp;
			<a href='javascript:void(0);'><img alt='Tweet' src='images/tquest/g+_icon.jpg' width='40px' height='20px'></a>
			</div>
			-->
			<br/>
			<div class='col-sm-6 col-md-6 infobmargin'>	
			<ul><h5><b>Math</b></h5></ul>
				<ul><p><b>Multiplication</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.superteacherworksheets.com/generator-basic-multiplication.html#ws");'>Basic Multiplication</a></li>					
				</ul> 
				<ul><p><b>Substraction</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.superteacherworksheets.com/generator-basic-subtraction.html#ws");'>Basic Substraction</a></li>								
				</ul> 
				<ul><p><b>Division</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.superteacherworksheets.com/generator-basic-division.html#ws");'>Basic Division</a></li>					
				</ul> 
				<ul><p><b>Fractions</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.lauracandler.com/filecabinet/math/PDF/orderfractions.pdf");'>Order Fractions</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("https://www.teacherspayteachers.com/Product/FREE-Factor-Race-Math-Game-225089");'>Factor Race Math Game</a></li>	
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.lauracandler.com/filecabinet/math/PDF/FractionSpinners.pdf");'>Fraction Spinners</a></li>									
				</ul> 
				<ul><p><b>Time</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/timeskills.html");'>Time Skills Workbook</a></li>					
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/platinum/time.html");'>Telling Time Worksheet Maker</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/early_childhood/clocks/what1.html");'>What Time Is It? 1 Minute Intervals </a><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/early_childhood/clocks/what1answers.html");'>| Answer Key</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/early_childhood/clocks/what5.html");'>What Time Is It? 5 Minute Intervals </a><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/early_childhood/clocks/what5answers.html");'>| Answer Key</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/early_childhood/clocks/what15.html");'>What Time Is It? 15 Minute Intervals </a><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/early_childhood/clocks/what15answers.html");'>| Answer Key</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/early_childhood/clocks/what30.html");'>What Time Is It? 30 Minute Intervals </a><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/early_childhood/clocks/what30answers.html");'>| Answer Key</a></li>					
				</ul> 
				<ul><p><b>Measurement</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.lauracandler.com/filecabinet/math/PDF/CustomaryMeasurePractice.pdf");'>Customary Measure Practice</a></li>								
				</ul> 
				<ul><p><b>Decimal</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.lauracandler.com/filecabinet/math/PDF/DecimalWordNamestoNumbers.pdf");'>Names to Numbers</a></li>								
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.lauracandler.com/filecabinet/math/PDF/DecimalstoFractions.pdf");'>Decimals to Fractions</a></li>
				</ul>
			</div>
			<div class='col-sm-6 col-md-6 infobmargin rightDiv' >
			<ul><h5><b>English/Language Arts</b></h5></ul>
				<ul><p><b>Phonics</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/tutorials/teaching/phonics/");'>A Model Lesson Plan for Teaching Phonics </a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/lettersounds.html");'>Letter and Sound Recognition Workbook</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/phonics3.html");'>Long Vowels, Compound Words, & Contractions</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/phonics4.html");'>Phonics Assorted Skills Workbook</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/phonics5.html");'>Phonics Assorted Skills, Volume 2 Workbook</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/phonics6.html");'>Phonics Assorted Skills, Volume 3 Workbook</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/phonics7.html");'>Phonics Assorted Skills, Volume 4 Workbook</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/phon.html");' >Phonics Consonant Blends and H Digraphs Workbook</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/new/conblends.html");'>Initial Consonant Blends</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/teachers/lesson_plans/language_arts/phonics/");' >Phonics Lesson Plans</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/shorta.html");'>Short Vowels Review</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/shorti.html");'>Short Vowels & Double Consonants</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/wordfamilies.html");'>Word Families Workbook</a></li>					
				</ul> 
				<ul><p><b>Vocabulary</b></p>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/prekvocabulary.html");'>Pre-Kindergarten Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/kindergartenvocabulary.html");'>Kindergarten Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/firstgradevocabulary.html");'>First Grade Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/secondgradevocabulary.html");'>Second Grade Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/thirdgradevocabulary.html");'>Third Grade Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/mostcommonnouns.html");'>Most Common Nouns Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/mostcommonverbs.html");'>Most Common Verbs Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/mostcommonadjectives.html");'>Most Common Adjectives Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/mostcommonadverbs.html");'>Most Common Adverbs Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/misspelledvocab38.html");'>The 100 Most Often Misspelled Vocabulary Words For Grade 3-8</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/misspelledwords.html");'>The 100 Most Often Misspelled English Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/sciencevocabularywords.html");'>The 100 Most Often Used Science Vocabulary Words</a></li>
					<li><a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/gold/vocabulary/socialstudiesvocabularywords.html");'>The 100 Most Often Used Social Studies Vocabulary Words</a></li>									
				</ul>
				<ul><p><b>Handwriting</b></p>
				<li><a>Standard Core Identified Font (lower case letters)<br/>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowera.pdf");'>a,</a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerb.pdf");'>b,</a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerc.pdf");'>c,</a> 
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerd.pdf");'>d,</a> 
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowere.pdf");'>e, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerf.pdf");'>f, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerg.pdf");'>g, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerh.pdf");'>h, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreloweri.pdf");'>i, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerj.pdf");'>j, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerk.pdf");'>k, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerl.pdf");'>l, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerm.pdf");'>m, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowern.pdf");'>n, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowero.pdf");'>o, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerp.pdf");'>p, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerq.pdf");'>q, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerr.pdf");'>r, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowers.pdf");'>s, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowert.pdf");'>t, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreloweru.pdf");'>u, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerv.pdf");'>v, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerw.pdf");'>w, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerx.pdf");'>x, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowery.pdf");'>y, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/corelowerz.pdf");'>z </a></a></li>
				<li><a>Standard Core Identified Font (UPPER CASE LETTERS)<br/>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreuppera.pdf");'>A,</a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperb.pdf");'>B, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperc.pdf");'>C, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperd.pdf");'>D, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreuppere.pdf");'>E, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperf.pdf");'>F, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperg.pdf");'>G, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperh.pdf");'>H, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperi.pdf");'>I, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperj.pdf");'>J, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperk.pdf");'>K, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperl.pdf");'>L, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperm.pdf");'>M, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreuppern.pdf");'>N, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreuppero.pdf");'>O, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperp.pdf");'>P, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperq.pdf");'>Q, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperr.pdf");'>R, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreuppers.pdf");'>S, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreuppert.pdf");'>T, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperu.pdf");'>U, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperv.pdf");'>V, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperw.pdf");'>W, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperx.pdf");'>X, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreuppery.pdf");'>Y, </a>
				<a href='javascript:void(0);' onclick='openNewWindow("http://www.teach-nology.com/worksheets/language_arts/handwriting/coreupperz.pdf");'>Z</a></a></li>
				</ul> 
			</div>
			
		</div>
		</div>
		
	



