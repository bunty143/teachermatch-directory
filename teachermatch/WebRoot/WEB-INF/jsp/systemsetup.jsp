<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='js/dwrajax.js'></script>
<link rel="stylesheet" href="css/dialogbox.css" />
<script  type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>
<input type="hidden" id="teacherId">


<style>
	.hide
	{
		display: none;
	}
	.whitefont{color: #FFFFFF;}
</style>
<div class="row col-md-10">
 <div class="TableContent">     
    <spring:message code="msgSuccProctoredByTm"/>           
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table table-striped TableStyle" style="border: none;">
                	<thead>
                      <tr>
                        <th></th>
                        <th  class="sysheader" style="color: #1A5486;padding-left: 10px;">
<spring:message code="lblMinimum"/></th>
                        <th  class="sysheader" style="color: #1A5486;"><spring:message code="lblRecommended"/></th>
                        <th  class="sysheader" style="color: #1A5486;"><spring:message code="lblYuSett"/></th>                        				
                      </tr>
                     </thead>
                     <tbody class="divborder">
                      <tr>
                        <td class="bg1 whitefont" align="left"  width="23%" style="padding: 40px;padding-left: 15px;"  nowrap=""><spring:message code="lblBrowserVersion"/></td>
                        <td style="padding-left: 10px;">
                            <spring:message code="lblInternetExplorer10"/><br>
							<spring:message code="lblChrome30.0.1581.2"/><br>
							<spring:message code="lblFirefox25.0"/><br>
							<spring:message code="lblSafari5.1.5"/>
                        </td>
                        <td>
                            <spring:message code="lblInternetExplorer11"/><br>
							<spring:message code="lblChrome37.0.2041"/><br>
							<spring:message code="lblFirefox32.0"/><br>
					<spring:message code="lblSafari5.1.7"/>                   
                        </td> 
                        <td id='browserVersion'>                         
                        </td>                                    
                      </tr>
                      
                       <tr>
                        <td class="bg1 whitefont" align="left" nowrap="" style="padding: 15px;"><spring:message code="lblPop-up"/></td>
                        <td style="padding-left: 10px;"><spring:message code="lblEnabled"/></td>
						<td><spring:message code="lblEnabled"/></td>
						<td id="popupResult">                                      
                      </tr>
                      
                       <tr>
                        <td class="bg1 whitefont" align="left" nowrap="" style="padding: 15px;"><spring:message code="lblPDFPlugin"/></td>
                        <td style="padding-left: 10px;"><spring:message code="lblAvailable"/></td>
						<td><spring:message code="lblAvailable"/></td>
						<td id="plugin"></td>                                      
                      </tr>
                      
                       <!--<tr>
                        <td class="bg1 whitefont" align="left"  nowrap="" style="padding: 15px;">Camera</td>
                        <td style="padding-left: 10px;">628 x 480 resolution</td>
						<td >1280 x 720 resolution</td>
						<td id="camera1">Not Detected <img src="images/red_cancel.png"></td>                              
                      </tr>
                      
                       -->
                       <tr>
                        <td class="bg1 whitefont" align="left" nowrap="" style="padding: 15px;"><spring:message code="lblCookies"/></td>
                      	<td style="padding-left: 10px;"><spring:message code="lblEnabled"/></td>
						<td><spring:message code="lblEnabled"/></td>
						<td id="cook"></td>                                  
                      </tr>    
                      
                        <!--<tr>
                        <td class="bg1 whitefont" align="left" nowrap="" style="padding: 15px;">Flash</td>
                       	<td style="padding-left: 10px;">Installed</td>
						<td>Installed</td>
						<td id="flash"></td>                                  
                      </tr>                                        
                     --></tbody>
                </table>           
            </div>
         <spring:message code="msgInternetExplorer10Compatibility"/>
     <div id="browsepic" style="display: none;"></div>
		<div id="takeapicweb"></div>
		<div id="teacherIdfdfg"></div>
		<input type="hidden" id="teacherId" name="teacherId" value="1">
		<div id="noCameraCheck" style="display: none;"></div>    
 </div>	   	
</div>	
	<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 	</table>
</div>

<script>
	//<![CDATA[
	 
 
	var PopupWarning = {
	 
	init : function()
	{	
		var popups_are_disabled = this.popups_are_disabled();
		//alert(popups_are_disabled);
		if(popups_are_disabled == true)
		{ 
			//alert('init - Popup is Blocked');
			this.redirect_to_instruction_page();
		}else{
			//alert('init - Popup is NOT Blocked');
			this.redirect_to_action_page();
		}
	},
	 
	redirect_to_instruction_page : function()
	{
		//alert('redirect - Popup is Blocked');
		document.getElementById("popupResult").innerHTML="Disabled <img src='images/red_cancel.png'/>";
		
	},
	 
	redirect_to_action_page : function()
	{
		//alert('redirect - Popup is NOT Blocked');
		document.getElementById("popupResult").innerHTML="Enabled <img src='images/green_ok.jpg'/>";
	},
	 
	popups_are_disabled : function()
	{
		var popup = window.open("", "popup_tester", "width=1,height=1,left=0,top=0");
		//var popup = window.open("http://www.cerebrum.com.br/magento/popup_with_chrome_js.html", "popup_tester", "width=1,height=1,left=0,top=0");
		if(!popup || popup.closed || typeof popup == 'undefined' || typeof popup.closed=='undefined')
		{
		    return true;
		}
	 
		
	 
		if(navigator && (navigator.userAgent.toLowerCase()).indexOf("chrome") > -1)
		{			
			//alert("Google Chrome");
			var on_load_test = function(){PopupWarning.test_chrome_popups(popup);};	
			var timer = setTimeout(on_load_test, 100);
			return;
		}
	 	window.focus();
		popup.blur();	
		popup.close();
		return false;
	},
	 
	test_chrome_popups : function(popup)
	{
		popup = window.open("", "popup_tester", "width=1,height=1,left=0,top=0");
		if(!popup || popup.outerHeight === 0) {
	        $('#popupResult').text('Disabled');
	        this.redirect_to_instruction_page();
	    } else {
			$('#popupResult').text('Enabled');
	        this.redirect_to_action_page();
	    } 
		popup.close();
		
		//this.redirect_to_instruction_page();
	}
	};
		$('#loadingDiv').show();
		PopupWarning.init();
		
		$('#loadingDiv').hide();
		//$('#myModalMsg').modal('show');
		
	//]]>
</script>


<div class="container" style="display: hide">

	
	<div class="accordion" id="accordion2">
	   	<div class="accordion-body collapse" id="collapseTwo" style="height: 0px;" onkeypress="chkForEnterEmployment(event)">
      			<div class="accordion-inner">
                		<div class="span16 ">
									<div class="row">
										<div class="span">
											<table width="900" border="0">
												<tr>
													<td colspan="2" valign="top">
													 Take a picture of any one of these:
												  </td>
												</tr>
												<tr>
													<td colspan="2" valign="top">
													 <ol type="1" style="list-style-position:inside;">
												        <li>Valid, unexpired, U.S. photo driver license or photo ID card</br></li>
												        <li>Valid, unexpired, license from another country</li>
														<li>Valid, unexpired, United States passport</li>
														<li>Valid, unexpired, active duty, retiree, or reservist military ID card</li> 
														<li>Valid, unexpired, United States passport</li>
														<li>Valid, unexpired, foreign passport</li>
												    </ol>  
												  </td>
												</tr>
												<tr>
													<td valign="top" colspan="2"> </br></br>Previous Picture</br></td>
												</tr>
												<tr>
													<td valign="bottom">
														<div id="teacherPicture"> </div>
													</td>
													<td valign="bottom" align="left">
													  <div id="screen"></div>
													</td>
												</tr>
												<tr><td valign="top" colspan="1" width="50%">&nbsp;</td>
													<td valign="top" colspan="2" align="left" style="white-space:nowrap;">
																<table border="0">
																	<tr><td colspan="2" height="40" valign="middle">
																				<div class='divErrorMsg' id='errorPic' style="display: block;"></div>					
																		</td>
																	</tr>
																	<tr>
																		<td nowrap>
																		 <div  id="takeapicweb"   style="display:none;">
																	    	<a type="button" href="javascript:void(0)" id="shootButton" onclick="takeAPicture();" class="btn btn-large btn-primary">Take a picture <i class="icon"></i></a>&nbsp;
																	      </div>
																	     </td>
																	    <td nowrap>
																		<div id="browsepic"> 
																			<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
																			</iframe>
																			<form id='frmIdentityVerificationUpload' name="frmIdentityVerificationUpload" enctype='multipart/form-data' method='post' target='uploadFrame' action='IdentityVerificationServlet.do' class="form-inline">
																			<input type="hidden" id="teacherIdForPicture" name="teacherIdForPicture"  value="${teacherpersonalinfo.teacherId}"/>
																			<input name="identityVerificationPicture"  id="identityVerificationPicture" type="file" onchange="SubmitForm();">
																			</form>
																		</div>
																		</td>
																		<td>
																			<div id="takeapic"  style="display:none;">
																			<a type="button" href="javascript:void(0)" onclick="tryAgain();" class="btn btn-large btn-primary">Try again <i class="icon"></i></a>&nbsp; 
																				<a type="button" href="javascript:void(0)" onclick="savePicture();" class="btn btn-large btn-primary">Use this picture <i class="icon"></i></a>
																			 </div>
																		 </td>
														   			 </tr>
													   			 </table>
																
															 <div id="buttons">
															   <div  id="usepicweb"  style="display:none;">	
														    	<a type="button" href="javascript:void(0)" id="cancelButton" onclick="tryAgainWeb();" class="btn btn-large btn-primary">Try again <i class="icon"></i></a>
														    	<a type="button" href="javascript:void(0)" id="uploadButton"  onclick="tryAgainWeb();"  class="btn btn-large btn-primary">Use this picture <i class="icon"></i></a>
														       </div>
														  	</div>
														
														</td>
												</tr>
											</table>						
										</div>
									</div>
									
						</div>			
                	   	<div class="clearfix">&nbsp;</div>
					</div>

			</div>
			
		</div>
</div>

<script type="text/javascript">

$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});
</script>
<script type="text/javascript" src="js/teacher/portfolio.js"></script>
			<div style="display:none;" id="loadingDiv">
			     <table  align="left" >
			 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
			 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
			 	 </table> 
			</div>


<script type="text/javascript">
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
</script>

<script src="js/flash_detect.js"></script>
<script src="assets/webcam/webcam.js"></script>
<script src="assets/js/script.js"></script>
<script src="js/detect.js"></script>
