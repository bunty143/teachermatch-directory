<!-- @Author: Gagan 
 * @Discription: view of edit district Page.
-->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DistrictDomainAjax.js?ver=${resourceMap['DistrictDomainAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax']}"></script>
<script type="text/javascript" src="dwr/interface/UserAjax.js?ver=${resourceMap['UserAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/editdistrict.js?ver=${resourceMap['js/editdistrict.js']}"></script>
  <!-- Optionally enable responsive features in IE8 -->
    <script type='text/javascript' src="js/bootstrap-slider.js"></script>
    <link rel="stylesheet" type="text/css" href="css/slider.css" />  
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  
<style>
.table th, .table td {
    padding: 8px;
    line-height: 20px;
    text-align: left;
    vertical-align: top;  
}
.divwidth
{
float: left;
width: 40px;
}
.input-group-addon {
    background-color: #fff;
	border: none;
}
</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        




 </script>  
<script type="text/javascript">
	
	
</script>
<style>
	.hide
	{
		display: none;
	}
	.inline
	{
		display: inline;
	}
	.nowrap
	{
		white-space: nowrap
	}
	.setSlider
	{
		margin-left: -20px; 
		margin-top: -8px;
	}
	
	.tooltip {
  position: absolute;
  z-index: 1030;
  display: block;
  font-size: 12px;
  line-height: 1.4;
  opacity: 0;
  filter: alpha(opacity=0);
  visibility: visible;
}

.tooltip.in {
  opacity: 0.9;
  filter: alpha(opacity=90);
}

.tooltip.top {
  padding: 5px 0;
  margin-top: -3px;
}

.tooltip.right {
  padding: 0 5px;
  margin-left: 3px;
}

.tooltip.bottom {
  padding: 5px 0;
  margin-top: 3px;
}

.tooltip.left {
  padding: 0 5px;
  margin-left: -3px;
}

.tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #ffffff;
  text-align: center;
  text-decoration: none;
  background-color: #000000;
  border-radius: 4px;
}

.tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid;
}

.tooltip.top .tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-left .tooltip-arrow {
  bottom: 0;
  left: 5px;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.top-right .tooltip-arrow {
  right: 5px;
  bottom: 0;
  border-top-color: #000000;
  border-width: 5px 5px 0;
}

.tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-right-color: #000000;
  border-width: 5px 5px 5px 0;
}

.tooltip.left .tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-left-color: #000000;
  border-width: 5px 0 5px 5px;
}

.tooltip.bottom .tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-left .tooltip-arrow {
  top: 0;
  left: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}

.tooltip.bottom-right .tooltip-arrow {
  top: 0;
  right: 5px;
  border-bottom-color: #000000;
  border-width: 0 5px 5px;
}
</style>
	<!--<h1>Welcome Gagan </h1><br/>
	<h3>Manage District</h3>-->
<form:form commandName="districtMaster"  method="post" onsubmit="return validateEditDistrict();"  enctype="multipart/form-data">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-district.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Action Feeds</div>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

    <input type="hidden" name="headQuarterId" id="headQuarterId" value="${headQuarterId}"/>
    <input type="hidden" name="branchId" id="branchId" value="${branchId}"/>
	<div>
		<div class="mt10">
      		<div class="panel-group" id="accordion">

	        
         <%-- ================ Gagan : Adding accordion Div for Social ======================  --%>
	   <div class="panel panel-default">
              <div class="offset1">
<%----------------- Gagan : Mosaic offset Div collapseSix -----------------------------  --%>
              <div class="row col-sm-12 col-md-12">
	              <div id="errordistrictdiv" class="required"></div>
	               <div class='required' id='errormosaicinfodiv'></div>
			   </div> 		
                  	<div class="row mt10">
	                  	<div class="col-sm-4 col-md-4">
							<label><spring:message code="lblDistributionEmail" /></label>
	                      	<form:input path="distributionEmail" cssClass="form-control" maxlength="100" />
	                    </div>
                  	</div>
					
					<div class="row mt10">
                  			<div class="col-sm-4 col-md-4">
                    		<spring:message code="lblCanDFeeCret" /> <a href="#" id="iconpophover13" rel="tooltip" data-original-title="<spring:message code="lblSetCriteriatoView" />" data-placement="right"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  	</div>

                  <div class="row mt10"> 
                  <fmt:bundle basename="resource"><fmt:message key="sliderMaxLength" var="sliderMaxLength" /></fmt:bundle>
                  <fmt:bundle basename="resource"><fmt:message key="sliderRationMaxLength" var="sliderRationMaxLength" /></fmt:bundle>
                  
             <%--------------------------- Slider Candidate Feed Functionality Check Start here ------------------------------------ --%>
                 <c:choose>
				    <c:when test="${empty districtMaster.candidateFeedNormScore}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultcandidateFeedNormScore" var="candidateFeedNormScoreValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="candidateFeedNormScoreValue" value="${districtMaster.candidateFeedNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.candidateFeedDaysOfNoActivity}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultcandidateFeedDaysOfNoActivity" var="candidateFeedDaysOfNoActivityValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="candidateFeedDaysOfNoActivityValue" value="${districtMaster.candidateFeedDaysOfNoActivity}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				
			 <%--------------------------- Slider Functionality Check ENDDDDDDDDDDD here ------------------------------------ --%>
				
          <%--         GAgan     <c:out value="${candidateFeedNormScoreValue } "></c:out> --%> 
                  
                  
                  			<div class="col-sm-12 col-md-12">
								<label> <spring:message code="msgPostCandActInMos" /></label>
							</div>
                  
							<div class="col-sm-4 col-md-4" style="max-width: 250px;">
								<label>  <spring:message code="lblCondNoScrHi" />   </label>
							</div>
							<div class="col-sm-8 col-md-8" style=" margin-top: -8px;"> 
								<label> 
									<iframe id="ifrm1"  src="slideract.do?name=slider1&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedNormScoreValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden cssClass="span2" path="candidateFeedNormScore" value="${candidateFeedNormScoreValue }"/>
								</label>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&" />  </label>
							</div>
							
							<div class="col-sm-6 col-md-6" style="max-width: 390px;">
								<label class="radio">
								
								<input type="hidden" id="mosaicRadiosFlag" name="mosaicRadiosFlag" value="1">
								<input type="radio"  name="mosaicRadios" id="mosaicRadios1" value="1" <c:if test="${districtMaster.candidateFeedDaysOfNoActivity ge 0}">checked </c:if> onclick="disableSlider()">
									<%--<form:radiobutton path="noSchoolUnderContract"  value="1" />--%>
			                        <spring:message code="msgCommWithCand" /> 
		                        </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px;">
								<label> 
									<c:set var="dsliderValueWhendbValue0" value="1"></c:set>
											<c:if test="${candidateFeedDaysOfNoActivityValue eq 0}">
												<c:set var="dsliderValueWhendbValue0" value="0"></c:set>
											</c:if>
								<iframe id="ifrm2"  src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedDaysOfNoActivityValue}&dslider=${dsliderValueWhendbValue0}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden cssClass="span2" path="candidateFeedDaysOfNoActivity" value="${candidateFeedDaysOfNoActivityValue }"/>
								</label>
							
						    </div>
							<div class="col-sm-2 col-md-2" style="min-width: 140px;">
								<label>&nbsp;&nbsp; <spring:message code="lblDyApp" /></label>
							</div>
							<div style="clear: both;"></div>
							<div class="col-sm-12 col-md-12" style="margin-top: -10px;">
								<label class="radio">
								<input type="radio"  name="mosaicRadios" id="mosaicRadios2" value="0" <c:if test="${districtMaster.candidateFeedDaysOfNoActivity eq 0}">checked </c:if> onclick="disableSlider()" >
									<%--<form:radiobutton path="noSchoolUnderContract"  value="1" />--%>
			                        <spring:message code="lblTheyHvApp" />
		                        </label>
							</div>
							
							<div class="col-sm-12 col-md-12">
					 			<label class="checkbox">
								<%-- <form:checkbox path="postingOnDistrictWall" id="postingOnDistrictWall"  value="1"/> --%>
			                 	<form:checkbox path="canSchoolOverrideCandidateFeed"/>	<spring:message code="lblSchoolOverrideSet" />
 
								</label>
							</div>				   
                   </div>
                  
                  <div class="row">
                  		<div class="col-sm-12 col-md-12">
                    		<spring:message code="lblJoFeeCrit" /> <a href="#" id="iconpophover14" rel="tooltip" data-original-title="<spring:message code="lblSetCriteriatoViewJobs" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  		</div>
                  </div>
                  
                  
                  <div class="row">
                  
                    <%--------------------------- Slider Jobbbbbbb Feed Functionality Check Start here ------------------------------------ --%>
                 <c:choose>
				    <c:when test="${empty districtMaster.jobFeedCriticalJobActiveDays}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalJobActiveDays" var="jobFeedCriticalJobActiveDaysValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalJobActiveDaysValue" value="${districtMaster.jobFeedCriticalJobActiveDays}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.jobFeedCriticalCandidateRatio}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalCandidateRatio" var="jobFeedCriticalCandidateRatioValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalCandidateRatioValue" value="${districtMaster.jobFeedCriticalCandidateRatio}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.jobFeedCriticalNormScore}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedCriticalNormScore" var="jobFeedCriticalNormScoreValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedCriticalNormScoreValue" value="${districtMaster.jobFeedCriticalNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.lblCriticialJobName}">
				       <c:set var="lblCriticialJobNameValue" value="Critical"></c:set>
				    </c:when>
				    <c:otherwise>
				        <c:set var="lblCriticialJobNameValue" value="${districtMaster.lblCriticialJobName}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				 <c:choose>
				    <c:when test="${empty districtMaster.jobFeedAttentionJobActiveDays}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedAttentionJobActiveDays" var="jobFeedAttentionJobActiveDaysValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedAttentionJobActiveDaysValue" value="${districtMaster.jobFeedAttentionJobActiveDays}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.jobFeedAttentionNormScore}">
				       <fmt:bundle basename="resource"><fmt:message key="defaultjobFeedAttentionNormScore" var="jobFeedAttentionNormScoreValue" /></fmt:bundle>
				    </c:when>
				    <c:otherwise>
				        <c:set var="jobFeedAttentionNormScoreValue" value="${districtMaster.jobFeedAttentionNormScore}"></c:set>
				    </c:otherwise>
				</c:choose>
				
				<c:choose>
				    <c:when test="${empty districtMaster.lblAttentionJobName}">
				        <c:set var="lblAttentionJobNameValue" value="Attention"></c:set>
				    </c:when>
				    <c:otherwise>
				        <c:set var="lblAttentionJobNameValue" value="${districtMaster.lblAttentionJobName}"></c:set>
				    </c:otherwise>
				</c:choose>
							
			 <%--------------------------- Slider Functionality Check ENDDDDDDDDDDD here ------------------------------------ --%>
							<div class="col-sm-4 col-md-4 top15">							
								<label><spring:message code="lblPostJoActInMosaic" /></label>
							</div>	
													
							<div class="col-sm-3 col-md-3 top10">		
								<form:input path="lblCriticialJobName" cssClass="form-control" value="${lblCriticialJobNameValue}" maxlength="50"/> 
							</div>
							<div class="col-sm-4 col-md-4 top15" style="margin-left: -20px;">
							 ,&nbspif
							</div>                 			
							
							<div class="clearfix"></div>
							<div class="col-sm-3 col-md-3" style="max-width: 170px;">
								<label> <spring:message code="lbljobActivefor" /> </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px;">
								<label> 
								<iframe id="ifrm3"  src="slideract.do?name=slider3&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedCriticalJobActiveDaysValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedCriticalJobActiveDays" cssClass="form-control" value="${jobFeedCriticalJobActiveDaysValue}"/>
								</label>
							</div>
							<div class="col-sm-1 col-md-1" style="min-width: 60px;">
								<label>&nbsp; <spring:message code="lblDays" /> </label>
							</div>
								<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&" />  </label>
							</div>
							
							<div class="col-sm-6 col-md-6" style="max-width: 400px;">
								<label>  <spring:message code="msgCondRatio" />  </label>
							</div>
							
							<div class="col-sm-3 col-md-3">
								<label> 
								<iframe id="ifrm4"  src="slideract.do?name=slider4&tickInterval=1&max=${sliderRationMaxLength }&swidth=200&svalue=${jobFeedCriticalCandidateRatioValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedCriticalCandidateRatio" cssClass="form-control" value="${jobFeedCriticalCandidateRatioValue}"/>
								</label>
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lblWhere" />  </label>
							</div>
							
							<div class="col-sm-5 col-md-5" style="max-width: 290px;">
								<label> <spring:message code="msgEachAppNormScore" />  </label>
							</div>
							<div class="col-sm-7 col-md-7">
								<label> 
								<iframe id="ifrm5"  src="slideract.do?name=slider5&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedCriticalNormScoreValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedCriticalNormScore" cssClass="span2" value="${jobFeedCriticalNormScoreValue}"/>
								</label>
							</div>
							
							<div class="col-sm-4 col-md-4 top15">							
								<label><spring:message code="lblPostJoActInMosaic" /></label>
							</div>	
													
							<div class="col-sm-3 col-md-3 top10">		
								<form:input path="lblAttentionJobName" cssClass="form-control" value="${lblAttentionJobNameValue}" maxlength="50"/>
							</div>
							<div class="col-sm-4 col-md-4 top15" style="margin-left: -20px;">
							  ,&nbspif
							</div>
							<div class="clearfix"></div>
							<div class="col-sm-3 col-md-3" style="max-width: 170px;">
								<label>  <spring:message code="lbljobActivefor" />   </label>
							</div>
							
							<div class="col-sm-3 col-md-3" style="min-width: 240px">
								<label> 
								<iframe id="ifrm6"  src="slideract.do?name=slider6&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedAttentionJobActiveDaysValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedAttentionJobActiveDays" cssClass="span2" value="${jobFeedAttentionJobActiveDaysValue}"/>
								</label>
							</div>
							<div class="col-sm-1 col-md-1" style="min-width: 60px">
								<label>&nbsp; <spring:message code="lblDays" /> </label>
							</div>
								<div class="clearfix"></div>
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&" />  </label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label class="checkbox">  
									<form:checkbox path="jobFeedAttentionJobNotFilled" value="1"/> <spring:message code="msgFillingCompletly" />
								</label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label>  <spring:message code="lbl&" />  </label>
							</div>
							
							<div class="col-sm-4 col-md-4" style="max-width: 280px;">
								<label>  <spring:message code="lblAppScoreGtrThan" />  </label>
							</div>
							
							<div class="col-sm-4 col-md-4">
								<label> 
								<iframe id="ifrm7"  src="slideract.do?name=slider7&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${jobFeedAttentionNormScoreValue}" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;"></iframe>
									<form:hidden path="jobFeedAttentionNormScore" cssClass="span2" value="${jobFeedAttentionNormScoreValue}"/>
								</label>
							</div>
							
							<div class="col-sm-12 col-md-12">
								<label>    <spring:message code="msgNotCommWithMsgPhone" /></label>
							</div>
							
							
							<%--
							<div class="span11">
								<label> Label for Critical Jobs &nbsp;&nbsp;&nbsp;<form:input path="lblCriticialJobName" cssClass="span3" value="${lblCriticialJobNameValue}" maxlength="50"/> </label>
							</div>
							<div class="span11">
								<label> Label for Logging Jobs &nbsp;&nbsp;<form:input path="lblAttentionJobName" cssClass="span3" value="${lblAttentionJobNameValue}" maxlength="50"/></label>
							</div>
							 --%>
							 
							<div class="col-sm-6 col-md-6">
					 			<label class="checkbox">
								<%-- <form:checkbox path="postingOnDistrictWall" id="postingOnDistrictWall"  value="1"/> --%>
			                 	<form:checkbox path="canSchoolOverrideJobFeed"  value="1"/>
			                 		<spring:message code="msgSchoolCanOverRide" /> 
								</label>
							</div>				   
                   </div>               
              </div>
              <div class="clearfix">&nbsp;</div>
            </div>
          </div>
        </div>    


        <%-- ================ rajendra : End of accordion Div for Privilege For Domain =======  --%>
    
       <!-- End Account Section -->
       
       <div class="modal hide"  id="myModalEmail" >
			<div class="modal-dialog-for-cgmessage">
		    <div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmail'></div>
				</div>
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong><spring:message code="lblFrm" /></strong><span class="required">*</span></label>
				        	<input type="text" id="fromAddress" name="fromAddress"   class="form-control" maxlength="250" />
						</div>
					</div>
					
					<div class="control-group">
						<div class="">
					    	<label><strong><spring:message code="lblSub" /></strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLine" name="subjectLine"class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBody">
					    	<label><strong><spring:message code="lblMsg" /></strong><span class="required">*</span></label>
					    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" /></label>
				        	<textarea rows="5" class="form-control" cols="" id="" name=""></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button>
		 		<button class="btn btn-primary"  onclick='return saveEmailFormat();' ><spring:message code="btnSave" /></button>
		 	</div>
	</div>
	 	</div>
	</div>
       
       <div class="mt30">
        <c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
        <button type="submit" class="btn btn-large btn-primary"><strong> <spring:message code="msgSaveDistrict" /> <i class="icon"></i></strong></button>
        </c:if><!--
          &nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelDistrict();"><spring:message code="lnkCancel" /></a><br>
          --><br>
        </div>
      </div>
      <iframe src="" id="iframeJSI" width="100%" height="480px" style="display: none;">
  </iframe> 
   
  <div  class="modal hide"  id="myModalMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);">x</button>
			<h3 id="myModalLabel"><spring:message code="headKyCont" /></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="Msg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(1);"><spring:message code="btnOk" /></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="return aKeyContactDiv(0);"><spring:message code="btnClose" /></button> 		
 		</div>
	</div>
	</div> 
	</div>

	<div style="display:none;" id="loadingDiv">
		<table  align="center" >
			 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>
</form:form>

<script> 
	loadStatusMaster();
	document.getElementById("alternatePhone").focus();
	displayKeyContact();
	displayNotes();
	
	displayDistrictAdministrator(2);
	displayDistrictAnalyst(5);
	displayDistrictSchools();
	uploadAssessment(${(districtMaster.assessmentUploadURL != null ) ? '1' : '0'});
	showFile(1,'district','${districtMaster.districtId}','${districtMaster.logoPath}',null);
	
</script>


<script type="text/javascript">

var selectIds = $('#collapseOne,#collapseTwo,#collapseThree,#collapseFour,#collapseFive,#collapseSix,#collapseSeven,#collapseEight,#collapseNine,#collapseTen,#collapseEle,#collapseFifteen,#collapseSixteen');
$(function ($) {
    selectIds.on('show.bs.collapse hidden.bs.collapse', function () {
        $(this).prev().find('.accordion-toggle').toggleClass('plus minus');
    })
});


$('.accordion').collapse();
$('.accordion').on('show', function (e) {
$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
});

$('.accordion').on('hide', function (e) {
$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
});


function emailForTeacherDivCheck(){
	var allowMessageTeacher=document.getElementById("allowMessageTeacher").checked;
	if(allowMessageTeacher==1){
		$("#emailForTeacherDiv").fadeIn();
		$("#emailForTeacher").focus();
	}else{
		$("#emailForTeacherDiv").hide();
		$("#allowMessageTeacher").focus();
		document.getElementById("emailForTeacher").value="";
	}
}
emailForTeacherDivCheck();
</script>
<script type="text/javascript">
$('#myModal').modal('hide');
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophover7').tooltip();
//$('#iconpophover8').tooltip();
$('#iconpophover9').tooltip();
$('#iconpophover10').tooltip();
$('#iconpophover11').tooltip();
$('#iconpophover12').tooltip();
$('#iconpophover13').tooltip();
$('#iconpophover14').tooltip();
$('#iconpophover15').tooltip();
$('#iconpophover16').tooltip();
$('#iconpophovercg').tooltip();
$('#iconpophover17').tooltip();
$('#iconpophover22').tooltip();

$(document).ready(function(){
  		$('#eMessage').find(".jqte").width(724);
  		$('#dNote').find(".jqte").width(724);
  		//$('#dDescription').find(".jqte").width(535);
}) 

</script>
<script><!--
	function disableSlider()
	{
		if($('#mosaicRadios1').is(':checked')) 
		{ 
	   		$("#mosaicRadiosFlag").val("1");
	   		$("#candidateFeedDaysOfNoActivity").val("0");
	   			try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=${candidateFeedDaysOfNoActivityValue}&dslider=1";
    			}
	    		catch(e)
	    		{} 
	    }
	    else
	    {
	    	if($('#mosaicRadios2').is(':checked')) 
			{ 
	    	
	    		$("#mosaicRadiosFlag").val("0");
	    		$("#candidateFeedDaysOfNoActivity").val("0");
	    		try
	    		{
	    			document.getElementById("ifrm2").src="slideract.do?name=slider2&tickInterval=10&max=${sliderMaxLength }&swidth=200&svalue=0&dslider=0";
    			}
	    		catch(e)
	    		{} 

	    //		alert(" mosaicRadios2 is checked ");
		    }
	    }
	}
getDistrictDomains();
chkDStatus(${setAssociatedStatusToSetDPoints});
showOfferVVI();
showMarksDiv();
showLinkDiv();
showReminderDiv();
showOfferAMT();
chkTimeAndDayForVVI();

</script>



    
    <script type="text/javascript">
	$('#addPop').click(function() {
		if ($('#lstDistrictAdmins option:selected').val() != null) {
			 $('#lstDistrictAdmins option:selected').remove().appendTo('#attachedDAList');
			 $("#attachedDAList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
	         $("#attachedDAList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	 }
	});
	
	$('#removePop').click(function() {
	       if ($('#attachedDAList option:selected').val() != null) {
	             $('#attachedDAList option:selected').remove().appendTo('#lstDistrictAdmins');
	           //  sortSelectList();
	             $("#attachedDAList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').addAttr("selected");
	             
	}
	});
	displayAllDistrictAssessment();
	$("#DistrictAdminListDiv").hide();
</script>
<!-- @Author: Gagan 
 * @Discription: view of edit district Page.
 -->
 
 <script><!--
 var sendNotificationOnNegativeQQ322 = '${sendNotificationOnNegativeQQValue}';
$(document).ready(function(){
	
	if(sendNotificationOnNegativeQQ322!=null)
		if(sendNotificationOnNegativeQQ322!="")
			$("#sendNotificationOnNegativeQQId").attr("checked","checked");
	
	buildApprovalGroup322();
	displayAllActiveDistrictUser();
});


 
function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
--></script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeMemberConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeMemberGroupId="" removeMemberId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					<spring:message code="msgRemoveThisMembr" />
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeMember()"><spring:message code="btnOk" /> <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal"><spring:message code="lnkCancel" /></button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeGroupConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeGroupId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					<spring:message code="msgRemoveThisGrp" />
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeGroup()">  <spring:message code="btnOk" />  <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal"><spring:message code="lnkCancel" /></button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>