
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/districtassessment.js?ver=${resourceMap['js/districtassessment.js']}"></script>
<style>
.table-striped tbody tr:nth-child(odd) td, .table-striped tbody tr:nth-child(odd) th {
background-color: #F2FAEF;
}
.table th, .table td {
 padding:10px 4px;
}
</style>
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMngQuesSec"/></div>	
         </div>			
		 <div class="pull-right add-employment1">
		<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
		<a href="districtassessmentsectionquestions.do?sectionId=${assessmentSection.districtSectionId}" ><spring:message code="lnkAddQues"/></a>
		</c:if>

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row">
<div class="col-sm-12 col-md-12 mt10">

		
<a href="districtassessment.do"><spring:message code="lnkAss"/></a>: ${assessmentSection.districtAssessmentDetail.districtAssessmentName}
<br/> 
</div>
<div class="left15 mt10">
<div class="pull-left top5">
<a href="districtassessmentsections.do?assessmentId=${assessmentSection.districtAssessmentDetail.districtAssessmentId}"><spring:message code="lnkSection"/></a>: 
</div>

 
<div class="col-sm-6 col-md-6 pull-left" style="margin-left: -10px;">
	                <select  class="form-control" name="assessmentSections" id="assessmentSection1" onchange="getSectionQuestions(this);">
					<c:forEach items="${assessmentSections}" var="assessmentSection1">	
						<c:set var="selected" value=""></c:set>
							<c:if test="${assessmentSection1.districtSectionId == assessmentSection.districtSectionId }">
								<c:set var="selected" value="selected"></c:set>
							</c:if>
							<option id="${assessmentSection1.districtSectionId}" value="${assessmentSection1.districtSectionId}" ${selected}>${assessmentSection1.sectionName}</option>
					</c:forEach>	
					</select>  
</div>				         
                  
</div>
</div>

<input type="hidden" id="districtAssessmentId" value="${assessmentSection.districtAssessmentDetail.districtAssessmentId}">
<input type="hidden" id="districtSectionId" value="${assessmentSection.districtSectionId}">

<div class="top15">
<table width="100%" border="0" class="table table-bordered table-striped" id="tblGrid">
</table>
</div>


<script type="text/javascript">
//document.charset = 'UTF-8';
getAssessmentSectionQuestionsGrid();
</script>