<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap" value="${applicationScope.resouceMap}" />
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherInfotAjax.js?ver=${resouceMap['TeacherInfoAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resouceMap['TeacherProfileViewInDivAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridAjaxNew.js?ver=${resouceMap['CandidateGridAjaxNew.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/MassStatusUpdateService.js?ver=${resouceMap['MassStatusUpdateService.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resouceMap['ManageStatusAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resouceMap['ManageJobOrdersAjax.ajax']}"></script>

<script type='text/javascript' src="js/report/candidategridnew.js?ver=${resouceMap['js/report/candidategrid.js']}"></script>
<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resouceMap['js/certtypeautocomplete.js']}"></script>
<script type="text/javascript" src="js/massStatusUpdate/massstatusupdate.js?ver=${resouceMap['js/massStatusUpdate/massstatusupdate.js']}"></script>
<script type="text/javascript" src="js/searchjoborder.js?ver=${resouceMap['js/searchjoborder.js']}"></script>
<script type='text/javascript' src="js/teacherinfonew.js?ver=${resouceMap['js/teacherinfonew.js']}"></script>

<script type="text/javascript" src="js/compress/c_jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type='text/javascript' src="js/report/cgstatusNew.js?ver=${resouceMap['js/report/cgstatusNew.js']}"></script>
<script type='text/javascript' src="js/report/schoolAutoSuggestForDJO.js?ver=${resouceMap['js/report/schoolAutoSuggestForDJO.js']}"></script>
	<script type="text/javascript" src="dwr/interface/ManageApplicantReportAjax.js?ver=${resourceMap['ManageApplicantReportAjax.Ajax']}"></script>
	<script type='text/javascript' src="js/applicantreport.js?ver=${resourceMap['js/applicantreport.js']}"></script>
<style>
.scrollspy_profile { height:500px; overflow-y:auto; overflow-x:hidden;padding-left:5px;
</style>
<style>
div.t_fixed_header div.body {	
	overflow-x	: auto;
}
</style>

<script type="text/javascript">
	var $j=jQuery.noConflict();
	$j(document).ready(function() {
});

function applyScrollOnReportTbl() {
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#applicantReportGridTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,200,200,150,150,150,100,100,250,200,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
     });			
}

function applyScrollOnTbl_AssessmentDetails()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentDetails').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        width: 830,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[230,200,200,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
   });			
}


function applyScrollOnTblTeacherCertifications_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherCertificates_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,100,100,130,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
   });			
}

function applyScrollOnTblTeacherAcademics_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherAcademics_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,    
        colratio:[410,180,60,60],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
    });			
}

function applyScrollOnTblWorkExp()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblWorkExp_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[148,278,90,103,111],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblEleRef_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleleReferences_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[120,85,90,160,80,82,65,45],    //changed: by rajendra	
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
    });			
}

function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 728,
        minWidth: null,
        minWidthAuto: false,
        colratio:[500,228,],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblVideoLinks_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblelevideoLink_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[620,110],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
    });			
}

//////////Message scroll 
function applyScrollOnMessageTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblMessages').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 70,
        width: 614,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,140,100,215,60], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
    });
}
</script>
<style>
.dropdown-menu11{ min-width:40px; margin-left: -60px;}
 
.dropdown-menu11 {  
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 170px;
  padding: 5px 0;
  margin-top: 10px;
  list-style: none;
  background-color: #ffffff;
  border: 1px solid #ccc;
  /*border: 1px solid rgba(0, 0, 0, 0.2);*/
  *border-right-width: 2px;
  *border-bottom-width: 2px;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  min-width:40px;

}
.navbar .nav > li > .dropdown-menu11:before {
	content: '';
	display: inline-block;
	border-left: 7px solid transparent;
	border-right: 7px solid transparent;
	border-bottom: 7px solid #ccc;
	/*border-bottom-color: rgba(0, 0, 0, 0.2);*/
	position: absolute;
	top: -7px;
	left: 10px;      
}
.navbar .nav > li > .dropdown-menu11:after {
	content: '';
	display: inline-block;
	border-left: 6px solid transparent;
	border-right: 6px solid transparent;
	border-bottom: 6px solid #ffffff;
	position: absolute;
	top: -6px;
	left: 10px;
}
.dropdown-menu11.pull-right {
  right: 0;
  left: auto;
}
.dropdown-menu11.divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.dropdown-menu11 > li > a {
  display: block;
  padding: 0px 20px;
  margin-top:5px;
  clear: both;
  font-weight: normal;
  line-height: 1.428571429;
  color: #333333;
  white-space: nowrap;
}
.dropdown-menu11 > li > a:hover,
.dropdown-menu11 > li > a:focus {
  text-decoration: none;
  color: #428bca;
  //background-color: #428bca;
}
.dropdown-menu11 > .active > a,
.dropdown-menu11 > .active > a:hover,
.dropdown-menu11 > .active > a:focus {
  color: #ffffff;
  text-decoration: none;
  outline: 0;
  background-color: #428bca;
}
.dropdown-menu11 > .disabled > a,
.dropdown-menu11 > .disabled > a:hover,
.dropdown-menu11 > .disabled > a:focus {
  color: #999999;
}
.dropdown-menu11 > .disabled > a:hover,
.dropdown-menu11 > .disabled > a:focus {
  text-decoration: none;
  background-color: transparent;
  background-image: none;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
  cursor: not-allowed;
}
.open > .dropdown-menu11 {
  display: block;
}
.open > a {
  outline: 0;
}

.pull-right > .dropdown-menu11 {
  right: 0;
  left: auto;
}

.dropup .dropdown-menu11,
.navbar-fixed-bottom .dropdown .dropdown-menu11 {
  top: auto;
  bottom: 100%;
  margin-bottom: 1px;
}
</style>

<div id="loadingDiv"> <!-- style="display:none; z-index: 5000;"  -->
  	<table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>

<div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
	<iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
	</iframe>  
</div>


<!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadApplicantList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="msgApplicantList" /></h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose" /></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End --> 



<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="msgApplicantReport" /></div>	
         </div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

   <div class="row">
			 	 
			 	 <div class="col-sm4 col-md-4">
	 				<label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
						<c:if test="${DistrictOrSchoolName==null}">
		   				<span>
						<input type="text" maxlength="255" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
						 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
									onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
									onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');chkschoolBydistrict();"/>
			 		</span>
			 			<input type="hidden" id="districtOrSchooHiddenlId"/>							
		 			</c:if>
			
					<c:if test="${DistrictOrSchoolName!=null}"> 
					<span>
					<input type="text" maxlength="255"  class="help-inline form-control" value="${DistrictOrSchoolName}"  disabled="disabled"/>
					 </span>
					
				
				<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
				<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
				<input type="hidden" id="districtHiddenlIdForSchool"/>
			</c:if>
			 <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId!=null}">
				<input type="hidden" id="districtHiddenlIdForSchool" value="${DistrictOrSchoolId}"/>
			  </c:if>
			   <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId==null}">
				<input type="hidden" id="districtHiddenlIdForSchool"/>
			  </c:if>
			 <input type="hidden" id="JobOrderType" value="${JobOrderType}"/>
			 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
							
		</div>
			 	 
			 	 <div class="col-sm-3 col-md-3 form-group"">
		                  <label><spring:message code="lblStatus" /></label>
				      <select class="form-control" id="Status">
				    <option value="All" selected="selected"><spring:message code="optAll" /></option>   
				    <option value="Available"><spring:message code="optAvble" /></option>
                   <option value="Ofcl Trans / Veteran Pref"><spring:message code="msgOfclTransVeteranPref" /></option>
                    </select>
					</div>
			     <div class="col-sm-3 col-md-3">
			 	 <label><spring:message code="lblJoI/JCode" /></label>
		         <input id="jobOrderId" name="jobOrderId" maxlength="100" class="help-inline form-control" type="text" "/>
			    </div>
		     <div class="col-sm-2 col-md-2 ">
		 	 <label>   <spring:message code="lblHighNeedSchool1"/>  </label>
			<input type="checkbox" name="HighNeedschool" value="check" class="checkbox inline" id="HighNeedschool"/>
		    </div>
		    
	    </div>
	        <div id="advanceSearchDiv_1" class="row">
		
		            <div class="col-sm-4 col-md-4" >
								<span class=""><label class="">
										<spring:message code="lblJobAppF" />
									</label> <input type="text" id="sfromDate" name="sfromDate"
										class="help-inline form-control">
								</span>
							</div>
							<div class="col-sm-3 col-md-3">
								<span class=""><label class="">
										<spring:message code="lblJobAppTo" />
									</label> <input type="text" id="stoDate" name="stoDate"
										class="help-inline form-control">
								</span>
							</div>
							
							 <div class="col-sm-3 col-md-3 form-group"">
		                  <label><spring:message code="lblCurrentEmployee1" /> </label>
				      <select class="form-control" id="empstatus">
				    <option value="All" selected="selected"><spring:message code="optAll" /></option>  
                     <option value="yes"><spring:message code="lblYes"/></option>
                   <option value="no"><spring:message code="lblNo"/></option>
                    </select>
					</div>

		   <div class="col-sm-2 col-md-2 top25-sm2" style="width: 150px;text-align:right;padding:0px;">
		        <label class=""></label>
		        <button class="btn btn-primary " type="button" onclick=" displayApplicatRecords();">&nbsp;&nbsp;&nbsp;<spring:message code="btnSearch" />&nbsp;&nbsp; <i class="icon"></i></button>
		   </div>

	</div>

<input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
       
		
	<div  class="modal hide pdfDivBorder"  id="modalDownloadJobOrder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><span id="pdfDistrictOrSchoolTitle"></span> <spring:message code="lblJobOrders" /> </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose" /></button> 		
	 	</div>
     </div>
 	</div>
</div>
<div class="row hide" style="padding-right:10px;" id="iconsDiv"><!-- 
	            <div class="col-sm-6 col-md-" >
	           <div id="searchLinkDiv" class="mt5">
			    <a href="javascript:void:(0);" onclick="displayAdvanceSearch()"><spring:message code="lnkOpAdvSch"/></a>
	     	 </div> 
			
			<div id="hidesearchLinkDiv" class=" hide mt10">
			   <a href="javascript:void:(0);" onclick="hideAdvanceSearch()"><spring:message code="lnkClAdvSch"/></a>
			</div>
       </div>
        -->
        <div class="col-sm-12 col-md-12">
          <table class="a" align="right">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='<spring:message code="lblExpToExcel"/>' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="exportRecordIntoExcel();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='<spring:message code="lblExporttoPDF"/>' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='getRecordListPDF()'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				 <td>
					<a data-original-title='<spring:message code="btnP"/>' rel='tooltip' id='printId' href='javascript:void(0);' onclick="printApplicantRecord();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		   </table>
        </div>
</div>

<div class="TableContent">        	
    <div class="table-responsive" id="divApplicantRecord"></div>            
</div> 

<!--  Communication Div For User Profile -->
<div  class="modal hide"  id="myModalCommunications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 700px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForSave();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headCom" /></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divCommTxt">
			</div> 
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick='showCommunicationsDivForSave();'><spring:message code="btnClose" /></button> 		
 	</div>
   </div>
 </div>
</div>

<!-- candidate profile div -->
<div class="modal hide custom-div-border1" id="cgTeacherDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div id="cgTeacherDivBody"> </div>
</div>

<div  class="modal hide pdfDivBorder"  id="modalDownloadPDR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headPdRep" /></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		
		  <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   <iframe id="ifrmPDR" width="100%" height="480px" src=""></iframe>		  
		  </div>   
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose" /></button> 		
   </div>
  </div>
 </div>
</div>

<iframe src="" id="ifrmRef" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmTrans" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmCert" width="100%" height="480px" style="display: none;"></iframe>

<div  class="modal hide pdfDivBorder"  id="modalDownloadsCommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-contenat" style="background-color: #ffffff;">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabelText"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTransCommon" width="100%" height="100%" scrolling="auto">
				 </iframe>  
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()"><spring:message code="btnClose" /></button> 		
 	</div>
</div>
 	</div>
</div>

<input type="hidden" name="eleRefId" id="eleRefId">
<div  class="modal hide"  id="myModalReferenceNoteView"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog' style="width: 520px;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headRefrNot" /></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" id="divRefNotesInner">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose" /></button> 		
 	</div>
  </div>
 </div>
</div>

<!-- Ref Note -->
<div class="modal hide"  id="myModalReferenceNotesEditor"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog-for-cgreference">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot" /></h3>
	</div>

	<div class="modal-body"  style="margin:10px;">	
		<div class="row" id="divNotesRef" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID_ref' name='uploadNoteFrame_ref' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload_ref' enctype='multipart/form-data' method='post' target='uploadNoteFrame_ref'  class="form-inline" onsubmit="return saveReferenceNotes();" action='referenceNoteUploadServlet.do' accept-charset="UTF-8">
		
			<input type="hidden" name="eleRefId" id="eleRefId">
			<input type="hidden" id="teacherIdForNote_ref" name="teacherIdForNote_ref">
			
			<div class="row mt10">
				<div class='span10 divErrorMsg' id='errordivNotes_ref' style="display: block;"></div>
				<div class="span10" >
			    	<label><strong><spring:message code="lblEtrNot" /><span class="required">*</span></strong></label>
			    	<div class="span10" id='divTxtNode_ref' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea readonly id="txtNotes_ref" name="txtNotes_ref" class="span10" rows="4"   ></textarea>
			    	</div>  
			    	<div id='fileRefNotes' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'><spring:message code="lnkAttachFile" /></a>
		        	</div>      	
				</div>						
			</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'><spring:message code="btnClr" /></button>&nbsp; 
 		<span id="spnBtnSave" style="display:inner"><button class="btn btn-primary"  onclick="saveReferenceNotes()"><spring:message code="btnSave" /> <i class="icon"></i></button>&nbsp;</span>
 	</div>
  </div>
 </div>
</div>
<!-- End Ref Note -->
<input type="hidden"  name="checkboxshowHideFlag" id="checkboxshowHideFlag"/>
<input type="hidden" name="teachersharedId" id="teachersharedId"/>
<input type="hidden" name="pageFlag" id="pageFlag" value="0"/>
<input type="hidden" name="userFPD" id="userFPD"/>
<input type="hidden" name="userFPS" id="userFPS"/>
<input type="hidden" name="userCPD" id="userCPD"/>
<input type="hidden" name="userCPS" id="userCPS"/>
<input type="hidden" name="folderId" id="folderId"/>
<input type="hidden" name="teacherIdForprofileGrid" id="teacherIdForprofileGrid"/>
<input type="hidden" name="teacherIdForprofileGridVisitLocation" id="teacherIdForprofileGridVisitLocation" value="CG View" />
<input type="hidden" name="hideIcons" id="hideIcons" value="-1"/>
<input type="hidden" name="teacherDetailId" id="teacherDetailId">
<input type="hidden" name="phoneType" id=phoneType value="0"/>
<input type="hidden" name="msgType" id=msgType value="0"/>
<input type="hidden" name="jobForTeacherGId" id="jobForTeacherGId" value=""/>
<input type="hidden" name="commDivFlag" id="commDivFlag" value=""/>
<input type="hidden" name="commDivFlagPnrCheck" id="commDivFlagPnrCheck" value="1"/>
<input type="hidden" id="jobId" name="jobId" />
<div class="modal hide"  id="saveToFolderDiv"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog" style="width:900px;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="defaultTeacherGrid()";>x</button>
		<h3 id="myModalLabel"><spring:message code="headSaveCandidate" /></h3>
	</div>
	<div class="modal-body" style="max-height: 500px;overflow-y: scroll;"> 	 		
		<div class="control-group">
		<div  style="border: 0px solid green;width:220px;height:450px;float: left;" class="span5">	
		<div class="span5" id="tree_menu" style=" padding: 8px 0px 10px 2px; " >
			<a data-original-title='Create' rel='tooltip' id='createIcon'><span id="btnAddCode" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Rename' rel='tooltip' id='renameIcon'><span id="renameFolder" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Cut' rel='tooltip' id='cutIcon'><span id="cutFolder" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Copy' rel='tooltip' id='copyIcon'><span id="copyFolder" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Paste' rel='tooltip' id='pasteIcon'><span id="pasteFolder" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Delete' rel='tooltip' id='deleteIcon'><span id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span></a>
		</div>
		<iframe id="iframeSaveCandidate"  src="tree.do" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>
	</div>
	<%-- ======================================== Right Div ========================================================================= --%>			
		<div class="span10" style="padding-top:39px; border: 0px solid green;float: left;">
				<input type="hidden" id="savecandidatearray" name="savecandidatearray">
				<input type="hidden" id="txtoverrideFolderId" name="txtoverrideFolderId">
				<input type="hidden" id="teachetIdFromPoPUp" name="teachetIdFromPoPUp" value="">
				<input type="hidden" id="teacherIdForHover" name="teacherIdForHover" value="">
				
				<input type="hidden" id="txtflagpopover" name="txtflagpopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
				<div id="savedCandidateGrid">
				
				</div>
		</div>
		<div style="clear: both"></div>	
		</div>
		
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="saveCandidateToFolderByUser()" ><spring:message code="btnSave" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="defaultTeacherGrid()";><spring:message code="btnClose" /></button> 		
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="duplicatCandidate" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgCandidateSvav/Cancel" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="saveWithDuplicateRecord()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
   </div>
  </div>
</div>

<!--  Share Folder -->

<div class="modal hide"  id="shareDiv" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='defaultSaveDiv()'>x</button>
		<h3 id="myModalLabel"><spring:message code="headShareCandidate" /></h3>
		<input type="hidden" id="entityType" name="entityType" value="${entityType}">
		<input type="hidden" id="savecandiadetidarray" name="savecandiadetidarray"/>
		<input type="hidden" id="JFTteachetIdFromSharePoPUp" name="JFTteachetIdFromSharePoPUp">
		<input type="hidden" id="txtflagSharepopover" name="txtflagSharepopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">			
			<div class="">
					<div class="row">
					    <div class="col-md-10">
						<div class='divErrorMsg' id='errorinvalidschooldiv' ></div>
					    </div>
					      <div class="col-sm-4 col-md-4">
					       <label><spring:message code="lblDistrictName" /></label> </br>
				            <c:if test="${DistrictName!=null}">
				             	${DistrictName}
				             	<input type="hidden" id="districtId" value="${DistrictId}"/>
				             	<input type="hidden" id="districtName" value="${DistrictName}" name="districtName"/>
				             </c:if>				            
			              <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
						
					      </div>
					      
					      <div class="col-sm-6 col-md-6">
					        <label><spring:message code="lblSchoolName" /></label>
				          	<c:if test="${SchoolName==null}">
				           	<input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
							    <input type="hidden" id="schoolId" value="0"/>
							</c:if>
							  <c:if test="${SchoolName!=null}">
				             	<%-- ${SchoolName}--%>	
				             	<input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" value="${SchoolName}"	/>
				             	<input type="hidden" id="schoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolName" value="${SchoolName}"/>
				             </c:if>
							 <div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
				        
					      </div>
					      <div class="col-md-2 top25">
					        <label>&nbsp;</label>
					        <input type="button" id="" name="" value="Go" onclick="searchUserthroughPopUp(1)" class="btn  btn-primary" >
					      </div>
  					</div>
				</div>
		
			<div id="divShareCandidateToUserGrid" style="border: 0px solid green;" class="mt30">
				<br/><br/><br/><br/><br/><br/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="shareCandidatethroughPopUp()" ><spring:message code="btnShare" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='defaultSaveDiv()'><spring:message code="btnClr" /></button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="shareConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="saveAndShareConfirmDiv" class="">
		    	<spring:message code="msgSucceSharedCand" />     	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button> 		
 	</div>
  </div>
 </div>
</div>

<div class="modal hide"  id="deleteShareCandidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeletetStrCondidate" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteCandidate()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
   </div>
  </div>
</div>

<!--  Message Div -->
<div class="modal hide"  id="myModalMessage"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCandidate" /></h3>
	</div>
	<iframe id='uploadMessageFrameID' name='uploadMessageFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUpload' enctype='multipart/form-data' method='post' target='uploadMessageFrame'  class="form-inline" onsubmit="return validateMessage();" action='messageCGUploadServlet.do' accept-charset="UTF-8">
	
	<div class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessages">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		
		<%---------- Gagan :District wise Dynamic Template [Start]  --%>
			<div id="templateDiv" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong><spring:message code="lblTemplate" /></strong></label>
		        	<br/>
		        	<select class="form-control" id="districttemplate" onchange="setTemplate()">
		        		<option value="0"><spring:message code="sltTemplate" /></option>
		        	</select>
		         </div>	
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary hide top26" id="btnChangetemplatelink" onclick="return getTemplate();"><spring:message code="btnAplyTemplate" /></button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplate" name="confirmFlagforChangeTemplate" value="0">
			
		    	</div>
		    </div>
		<%---------- Gagan :District wise Dynamic Template [END]  --%>
		
		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong>To<br/></strong><span id="emailDiv" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub" /></strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend" style="width: 612px;">
		    	<label><strong><spring:message code="lblMsg" /></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" />
 				</label>
	        	<textarea rows="5" class="form-control" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessage" name="teacherIdForMessage" value="">
    		<input type="hidden"  name="messageDateTime" id="messageDateTime" value="${dateTime}"/>
	    	<div id='fileMessages' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileType();'>
        		<img src='images/attach.png'/></a> 
        		<a href='javascript:void(0);' onclick='addMessageFileType();'><spring:message code="lnkAttachFile" /></a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<c:set var="chkSendDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSendDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSendDisp" value="inline" />
			</c:when>
			<c:otherwise>
				<c:set var="chkSendDisp" value="none" />
			</c:otherwise>
	</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'><spring:message code="btnClr" /></button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessage()" style="display: ${chkSendDisp}"><spring:message code="btnSend" /></button>&nbsp;
 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="myModalMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'><spring:message code="msgYuMsgIsfullySentToCandidate" />
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()"><spring:message code="btnOk" /></button>
 	</div>
</div> 	
</div>
</div>


<!--  Add Phone -->
<div  class="modal hide"   id="myModalPhone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   data-backdrop="static">
	<div class='modal-dialog-for-teachercontented'>
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showCommunicationsForPhone();">x</button>
		<h3 id="myModalLabel"><spring:message code="headPhone" /></h3>
	</div>	
	<div class="modal-body">			
			<div id="divPhoneGrid"></div>
			<iframe id='uploadPhoneFrameID' name='uploadPhoneFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
			</iframe>
			<form id='frmPhoneUpload' enctype='multipart/form-data' method='post' target='uploadPhoneFrame'  class="form-inline" onsubmit="return savePhone();" action='phoneUploadServlet.do' accept-charset="UTF-8">
			<div class="mt10">
				<div class="span6 hide" id='calldetrailsdiv'>
					<div class='divErrorMsg' id='errordivPhone' style="display: block;margin-bottom: 5px;"></div>
					<label><strong><spring:message code="lblEtrCallDetail" /><span class="required">*</span></strong></label>
			    	<div class="span6" id='divTxtPhone' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea readonly id="divTxtPhone" name="divTxtPhone" class="span6" rows="4"></textarea>
			    	</div> 
			    	<input type="hidden" id="teacherIdForPhone" name="teacherIdForPhone" value="">
		    		<input type="hidden"  name="phoneDateTime" id="phoneDateTime" value="${dateTime}"/>
			    	<div id='filePhones' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addPhoneFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addPhoneFileType();'><spring:message code="lnkAttachFile" /></a>
		        	</div>   
		    	</div>     	
			</div>
			</form>
 	</div>
 	<c:set var="chkSavePhone" value="inline"/>
		<c:choose>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSavePhone" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSavePhone" value="none" />
			</c:otherwise>
		</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="showCommunicationsForPhone();"><spring:message code="btnClr" /></button>
 		<button class="btn btn-primary hide" style="display: ${chkSavePhone}" id='calldetrailsbtn' onclick="savePhone()" ><spring:message code="btnSave" />&nbsp;</button>
 	</div>
	</div> 	
  </div> 	
</div>
<!-- End Phone -->

<!--  Starts Notes DIV -->
<input type="hidden" id="noteId" name="noteId" value=""/>
<div class="modal hide" id="myModalNotes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-dialog" style="width:935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot" /></h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID' name='uploadNoteFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload' enctype='multipart/form-data' method='post' target='uploadNoteFrame'  class="form-inline" onsubmit="return saveNotes();" action='noteUploadServlet.do' accept-charset="UTF-8">
			
		<div class="mt10">
			<div class='span10 divErrorMsg' id='errordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong><spring:message code="lblEtrNot" /><span class="required">*</span></strong></label>
		    	<div class="span10" id='divTxtNode' style="padding-left: 0px; margin-left: 0px;" >
		    		<textarea style="width: 100%" readonly id="txtNotes" name="txtNotes" class="span10" rows="4"></textarea>
		    	</div>  
		    	<input type="hidden" id="teacherIdForNote" name="teacherIdForNote" value="">
		    	<input type="hidden"  name="noteDateTime" id="noteDateTime" value="${dateTime}"/>
		    	<div id='fileNotes' style="padding-top:10px;">
	        		<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'><spring:message code="lnkAttachFile" /></a>
	        	</div>      	
			</div>						
		</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<c:set var="chkSaveDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSaveDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSaveDisp" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSaveDisp" value="none" />
			</c:otherwise>
		</c:choose>
 		<!-- <span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()">Cancel</a></span> -->
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'><spring:message code="btnClr" /></button>&nbsp; 
 		<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button class="btn btn-primary"  onclick="saveNotes()"><spring:message code="btnSave" /> <i class="icon"></i></button>&nbsp;</span>
 	</div>
   </div>
</div>
</div>


 <!-- PDF -->
 <style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>
<div  class="modal hide pdfDivBorder"  id="modalDownloadOfferReady" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headAppPNRDashbord" /> </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose" /></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->  


<input type="hidden" id="teacherIdForHover" name="teacherIdForHover" value="">

<div  class="modal hide" id="myModalProfileVisitHistoryShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:100px;" id="mydiv">
		<div class='modal-content'>
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
			<h3 id="myModalLabel"><spring:message code="headTeacherProfVisitHistory" /></h3>
		</div>
		<div class="modal-body" style="max-height: 400px;overflow-y:scroll;padding-right: 18px;">		
			<div class="control-group">
				<div id="divteacherprofilevisithistory" class="">
			    		        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose" /></button> 		
	 	</div>
		</div>
	</div>
</div>
<div class="modal hide"  id="saveDataLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDataSaved" /> 
		</div>
 	</div>
 	<div class="modal-footer">
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide" id="myModalUndoStatusConfirm" >
<input type="hidden" name="eligibilityIdSet" id="eligibilityIdSet">
<input type="hidden" name="onrStatusType" id="onrStatusType"/>
	<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='hideDivUndoStatus()'>x</button>
				<h3><span id="myModalStatusUndoLabel"></span></h3>
			</div>
			<div  class="modal-body">
				<div class="" id="referenceRecord">
					<spring:message code="cngMsgUndocurrentstatus" /><BR/>
				</div>
		 	</div>

		 	<div class="modal-footer">
		 		<button class="btn btn-large btn-primary" type="button" onclick="undoStatus();"><strong><spring:message code="btnConti" /> <i class="icon"></i></strong></button>
		 		<button type="button" class="btn"  onclick='hideDivUndoStatus()'><spring:message code="btnClr" /></button>
		 	</div>
	    </div>
	</div>
</div>

<div class="modal hide custom-div-border1" id="draggableDivMaster"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="draggableDiv"></div>
</div>
<input type="hidden" id="pageNumberSet" name="pageNumberSet" value="1" />
<!-- candidate profile div -->
<!--
<div class="modal hide custom-div-border1" id="cgTeacherDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="cgTeacherDivBody"></div>
</div>
-->
<!--  Message DIV  -->

<div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   <h3 id="myModalLabel">Teachermatch</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			 <spring:message code="msgPlzPopAreAlloued" />
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk" /></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>


<script>
function applyScrollOnPhone()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblPhones').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 110,
        width: 615,
        minWidth: null,
        minWidthAuto: false,
        colratio:[400,100,112], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

$(document).ready(function(){

	/* ---------------------- Create Folder Method Start here --------------- */
	$("#btnAddCode").click(function(){
		//document.getElementById('iframeSaveCandidate').contentWindow.createUserFolder();
	});
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
	$("#renameFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.renameFolder();
	});
/*-----------------  Cut Copy Paste Folder ------------------------*/
	$("#cutFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.cutFolder();	
	});

	$("#copyFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.copyFolder();		
	});

	$("#pasteFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.pasteFolder();	
	});
		
	$("#deletFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.deletFolder();
	});	
	 $('#divTxtPhone').find(".jqte");
}) 
</script>

<script type="text/javascript">//<![CDATA[
	  var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("sfromDate", "sfromDate", "%m-%d-%Y");
     cal.manageFields("stoDate", "stoDate", "%m-%d-%Y");
</script>

<script type="text/javascript">
 $('#chartId').tooltip();
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
  
$(document).mouseup(function (e)
{
    var container = $(".DynarchCalendar-topCont");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
}); 
//getYesterdayApplicants();
//getTotalsOfDateApplicants();
</script>
<script type="text/javascript">
	chkschoolBydistrict();
	displayApplicatRecords();
</script>


