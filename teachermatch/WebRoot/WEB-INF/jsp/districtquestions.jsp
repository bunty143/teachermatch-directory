<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="dwr/interface/DistrictQuestionsAjax.js?ver=${resourceMap['DistrictQuestionsAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type='text/javascript' src="js/districtquestions.js?ver=${resourceMap['js/districtquestions.js']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>
<style>
.table-striped tbody tr:nth-child(odd) td, .table-striped tbody tr:nth-child(odd) th {
background-color: #F2FAEF;
}
.table th, .table td {
 padding:10px 4px;
}
input[type="radio"], input[type="checkbox"] {
 margin: 0 0 0;
line-height: normal;
}
</style>

<c:if test="${userMaster.entityType==1}">
<c:set var="notShow" value="display:none;"/>	
	<% String id=session.getAttribute("displayType").toString();%>
				<input type="hidden" id="displayTypeId" value="<%=id%>"/>
				<%if(id.equalsIgnoreCase("0")){ %>
	<div class="row  col-sm-8 col-md-8 modalTM1" id='searchItem' style="position:fixed; margin-top: 15px;padding-top: 15px;padding-bottom:15px;margin-left:15px;">	
     <div class='divErrorMsg' id='errordiv' style="padding-left: 15px;"></div>
        <div class="col-sm-10 col-md-10">
        <label><spring:message code="lblDistrictName"/></label>
           <span>
           <input type="text" id="districtName" autocomplete="off" maxlength="100"  name="districtName" class="help-inline form-control"
          		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
				 onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
				 onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');" tabindex="1"/>
      	   </span>
      		<input type="hidden" id="districtId" value="${districtId}"/>
      		<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
	   </div>
 
	    <div class="col-sm-1 col-md-1" >
	        <label  style="float: right;margin-right:-25px;" id='closePan'>&nbsp;</label>
			<button class="btn btn-primary top25-sm left25-sm" style="width:90px;" type="button" onclick="searchData();"><spring:message code="btnSearch"/>&nbsp;<i class="icon"></i></button>         	   	
	    </div>
	   </div>

 
   <div class="modalsa" style="display: none;" id="sa">
	<div class="" style="padding-top: 5px;">
	<a href='javascript:void(0);' onclick="getSearchPan()"><span class='icon-search icon-large iconcolor' style="font-size: 0.99em;"></span><b>&nbsp;<spring:message code="lnkSearchAg"/><img src="images/arrow_left_animated.gif"/></b></a>
	</div>
	</div>
	 <%}%>
</c:if>	




<div class="row" style="margin:0px;">
<c:if test="${userMaster.entityType eq 1}">
		<div  id="managedqCLine" class="hide">
	    <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;" id="managedqText"><spring:message code="headMngDistSpeciQues"/></div>	
         </div>			
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
	</div>
</c:if>
<c:if test="${userMaster.entityType ne 1}">
		<div  id="managedqCLine">
	    <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;" id="managedqText"><spring:message code="headMngDistSpeciQues"/></div>	
         </div>			
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
	</div>
</c:if>

<c:if test="${userMaster.entityType==1}">
	<div class="span3 pull-right add-employment1 hide"  id='addQuestionInstrunction' style="margin-top: 5px">
		<a href="#" onclick="addQuestionInstruction();"><spring:message code="lnkAddInst"/></a>
	</div>
</c:if>
<c:if test="${userMaster.entityType ne 1}">
	<div class="span3 pull-right add-employment1"  id='addQuestionInstrunction' style="margin-top: 5px">
		<a href="#" onclick="addQuestionInstruction();"><spring:message code="lnkAddInst"/></a>
	</div>
</c:if>
</div>

<c:if test="${userMaster.entityType == 1}">
	<div id='tblGridQInsID' class="hide">
	<div class="span2" style="margin-left:4px;"><a href='javascript:void(0)'><B><spring:message code="lblInst"/></B></a></div>
	<table width="100%" border="0"  class="table-bordered mt15" id="tblGridQIns">
	</table>
	</div>
</c:if>

<c:if test="${userMaster.entityType ne 1}">
	<div id='tblGridQInsID'>
	<div class="span2" style="margin-left:4px;"><a href='javascript:void(0)'><B><spring:message code="lblInst"/></B></a></div>
	<table width="100%" border="0"  class="table-bordered mt15" id="tblGridQIns">
	</table>
	</div>
</c:if>


<div class="span3 pull-right add-employment1 " style="margin-top:-10px;" id='addQuestion'>
	<c:if test="${userMaster.entityType gt 1}">
	<a href="districtspecificquestions.do?districtId=${districtId}" >+ <br></br>Add Question</a>
	</c:if>
</div>
<c:set var="notShow" />



<div class=" row">
<div class="span16" style="padding-top: 5px;" >
<c:if test="${(userMaster.entityType gt 1) || districtId gt 0}">
<input type="hidden" id="districtId" value="${districtId}"/>
<script type="text/javascript">
searchData();
//getDistrictQuestions();
</script>	
</c:if>
<c:if test="${userMaster.entityType ne 1}">
	<div id='tblGridQInsID' class="hide">
	<div class="span2" style="margin-left:4px;"><a href='javascript:void(0)'><B><spring:message code="lblInst"/></B></a></div>
	<table width="100%" border="0" class="table-bordered mt15" id="tblGridQIns">
	</table>
	</div>
</c:if>

<c:if test="${userMaster.entityType == 1}">
	<div class="span2 hide" id="qdetails" style="margin-left:14px;margin-top:10px;"><a href='javascript:void(0)'><B><spring:message code="lnkQuesDetails"/></B></a></div>
	<c:if test="${dt ne null}">
		<table  border="0" class="table table-bordered table-striped" id="tblGrid" style="margin-left: 15px;width: 97%;"></table>
	</c:if>	
	<c:if test="${dt eq null}">
		<table  border="0" class="table table-bordered table-striped hide" id="tblGrid" style="margin-left: 15px;width: 97%;"></table>
	</c:if>
</c:if>

<c:if test="${userMaster.entityType ne 1}">
	<div class="span2" id="qdetails" style="margin-left:14px;margin-top: 10px;"><a href='javascript:void(0)'><B><spring:message code="lnkQuesDetails"/></B></a></div>
	<table  border="0" class="table table-bordered table-striped" style="margin-left: 15px;width: 97%;" id="tblGrid">
	</table>
</c:if>

</div>

</div>


<div class="modal hide fade"  id="questionInstructionModel"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabel"><spring:message code="lblInst"/></h3>
	</div>
	<div class="modal-body"> 
		<div class='divErrorMsg' id='QIerrordiv'  ></div>
		<div id="questionInstructions">
		<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
		<textarea   rows="4" 
			onkeyup="return chkLenOfTextArea(this,300);" onblur="return chkLenOfTextArea(this,300);">
		</textarea>
		</div>
 	</div>
 	<div class="modal-footer">
 	    <span id=""><button class="btn  btn-primary" onclick="saveQuestionInstruction();"><spring:message code="btnSave"/>&nbsp;<i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" class="close" aria-hidden="true" ><spring:message code="btnClr"/></button> 		
 	</div>
</div>
</div>
</div>

<div class="modal hide fade" id="deleteInstructionModel"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabel"><spring:message code="lblInst"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
				<spring:message code="msgDelteInst"/>  	
		</div>
 	</div>
 	<div class="modal-footer">
 	    <span id=""><button class="btn  btn-primary" onclick="deleteQuestionInstruction();"><spring:message code="btnOk"/>&nbsp;<i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" class="close" aria-hidden="true" ><spring:message code="btnClr"/></button> 		
 	</div>
</div>
</div>
</div>


<script type="text/javascript">
var displayID=$("#displayTypeId").val();
	if(displayID!=0){
		setEntityType();
	}
$('#districtName').focus();
$(document).ready(function(){
  		$('#questionInstructions').find(".jqte_editor").height(225);
}) 
</script>
