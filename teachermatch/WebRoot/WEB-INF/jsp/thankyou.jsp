<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row offset4 msgPage">
	<!--<div class="span8 mt30">
			You have been successfully registered with TeacherMatch. <br/>We have sent you an email with login details and an authentication link.<br/> 
			Please check your email to authenticate. 
			<br/><a href="signin.do">Click here</a> to go back.
		</div>
	 -->
	<div class="span8 mt30">
			<spring:message code="msgSuccessfullyRegdWithTm"/><BR/>
			<spring:message code="msgSuccessfullyRegdWithTm2"/><br/> 
			<font color="red"><spring:message code="msgSuccessfullyRegdWithTm3"/></font><BR/>
			<spring:message code="msgSuccessfullyRegdWithTm4"/> <BR/> <spring:message code="msgSuccessfullyRegdWithTm5"/> 
			
			<c:choose>
      			<c:when test="${isKelly == 1}">
      				<a href="mailto:applicants@teachermatch.org">applicants@teachermatch.org</a></br></br>
      			</c:when>
      			<c:otherwise>
	      		    <a href="mailto:clientservices@teachermatch.com">clientservices@teachermatch.com</a></br></br>
      			</c:otherwise>
			</c:choose>
			
			<c:choose>
      			<c:when test="${smartpractices eq 1}">
      				<a href="smartpractices.do"><spring:message code="lnkRtnToSignInPsge"/></a>
      			</c:when>
      			<c:otherwise>
	      		<a href="signin.do"><spring:message code="lnkRtnToSignInPsge"/></a>
      			</c:otherwise>
			</c:choose>
	</div>
</div>

