<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resourceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resourceMap['DashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src="js/assessment-campaign.js?ver=${resourceMap['js/assessment-campaign.js']}"></script>

<script type='text/javascript' src="js/districtquestioncampaign.js?ver=${resourceMap['js/districtquestioncampaign.js']}"></script>
<script type="text/javascript" src="dwr/interface/PFExperiences.js?ver=${resourceMap['PFExperiences.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Experiences.js?ver=${resourceMap['js/teacher/dynamicPortfolio_Experiences.js']}"></script>
<script type='text/javascript' src="js/teacher/jobcompletenow.js?ver=${resourceMap['js/teacher/jobcompletenow.js']}"></script>

<!-- Start ... Dynamic Portfolio -->
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resourceMap['DistrictPortfolioConfigAjax.ajax']}"></script>
<script type='text/javascript' src="js/teacher/districtportfolioconfig_ExternalJob.js?ver=${resourceMap['js/teacher/districtportfolioconfig_ExternalJob.js']}"></script>
<script type="text/javascript" src="js/teacher/dynamicPortfolio_Common.js?ver=${resourceMap['js/teacher/dynamicPortfolio_Common.js']}"></script>

<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<!-- Academics -->
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resourceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFAcademics.js?ver=${resourceMap['PFAcademics.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Academics.js?ver=${resourceMap['js/teacher/dynamicPortfolio_Academics.js']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_AutoCompAcademics.js?ver=${resourceMap['js/teacher/dynamicPortfolio_AutoCompAcademics.js']}"></script>
<!-- Certifications -->
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Certifications.js?ver=${resourceMap['js/teacher/dynamicPortfolio_Certifications.js']}"></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resourceMap['PFCertifications.js']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_AutoCompCertificate.js?ver=${resourceMap['js/teacher/dynamicPortfolio_AutoCompCertificate.js']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.js']}"></script>

<!--  Non-Client Jobs -->
<script type="text/javascript" src="dwr/interface/JBServiceAjax.js?ver=${resourceMap['JBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>


<!-- Resume -->

<!-- For Searching -->
<script type="text/javascript" src="js/teacher/dashboard.js?ver=${resourceMap['js/teacher/dashboard.js']}"></script>
<script type="text/javascript" src="js/searchjoborder.js?ver=${resourceMap['js/searchjoborder.js']}"></script>

<!-- New Job Apply Process -->
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafstart.js?ver=${resouceMap['js/jobapplicationflow/jafstart.js']}"></script>

<script type="text/javascript">
var $j=jQuery.noConflict();
$j(document).ready(function(){});
</script>

<!-- End ... Dynamic Portfolio -->

<link rel="stylesheet" type="text/css" href="css/base.css" />  

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridJoF').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[138,103,150,80,115,145,75,165], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTb()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#schoolListGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 680,
        minWidth: null,
        minWidthAuto: false,
        colratio:[350,300],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblGeoZoneSchool()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#geozoneSchoolTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 588,
        minWidth: null,
        minWidthAuto: false,
        //colratio:[55,266,70,90,100,105,70,66,151], // table header width
        //mukesh to set the table
        colratio:[380,250],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}
</script>
<input type="hidden" id="isaffilatedstatushiddenId" name="isaffilatedstatushiddenId" value="0"/>
<input type="hidden" id="savejobFlag" name="savejobFlag" value=""/>	
<input type="hidden" id="completeNow" name="completeNow" value="0"/>
<input type="hidden" id="inventory" name="inventory" value=""/>
<input type="hidden" id="portfolioStatus" name="portfolioStatus" value=""/>
<input type="hidden" id="prefStatus" name="prefStatus" value=""/>	
<input type="hidden" id="jobId" name="jobId" value=""/>
<input type="hidden" id="noCl" name="noCl" value="0"/>




<input type="hidden" id="gridNo" name="gridNo" value="">
<div  class="modal hide" onclick="getSortSecondGrid()" id="myModalforSchhol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 742px;">
    <div class="modal-content">
	<div class="modal-header" id="schoolHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headSchL"/></h3>
	</div>
	<div class="modal-body" id="schoolListDiv">
		
	</div> 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
	</div>
 	</div>
</div>

<!--<div class="row centerline offset1 mt30">-->
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/applyfor-job.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headTchrJoOfInterest"/></div>	
         </div>		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row" style="width:650px;margin-top:10px;margin-bottom:-10px">
			<!--<div class="col-sm-12 col-md-12">
				<label>Keyword Search </label>
					<input  type="text" 
									class="form-control"
									maxlength="100"
									id="searchTerm" 
									value=""
									
									name="searchTerm" 
									 onkeypress="return runScript(event)"
									/>
			</div>
						
		--></div>

	                  <div class="row top15">						
							<div class="col-sm-8 col-md-8">
								<div class="row">
								<div class="col-sm-6 col-md-6">								
									<label><strong><spring:message code="lblDistrict"/></strong></label>
								<input type="hidden" id="gridNameFlag" value="jobofinterest">
							 	<input  type="text" 
									class="form-control"
									maxlength="100"
									id="districtName" 
									value=""
									name="districtName" 
									onfocus="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId',''); showSchool(); getSubjectByDistrict();"
									onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId',''); showSchool(); getSubjectByDistrict();" 
									onblur="hideDistrictMasterDiv(this,'districtId','divTxtDistrictData'); showSchool(); getSubjectByDistrict(); getZoneforTeacher();"/>
								<input  type="hidden" id="districtId" name="districtId" value="">
								<div id='divTxtDistrictData' style=' display:none;position:absolute; z-index: 2000;' 
								onmouseover="mouseOverChk('divTxtDistrictData','districtName')" class='result' ></div>
								</div>
								  
								<div class="col-sm-6 col-md-6">
								
								<label><spring:message code="lblSchoolName"/></label>
									<input  type="hidden" id="schoolId1" name="schoolId1" value="">
									<input  type="text" 
										class="form-control"					
										maxlength="100"
										id="schoolName1" 
										disabled="disabled"
										autocomplete="off"
										value=""
										name="schoolName1" 
										onfocus="getSchoolMasterAutoComp(this, event, 'divTxtShowData4', 'schoolName1','schoolId1','districtId','');"
										onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtShowData4', 'schoolName1','schoolId1','districtId','');" 
										onblur="hideSchoolMasterDiv(this,'schoolId1','divTxtShowData4');setBlankZip();"/>
									<div id='divTxtShowData4' style=' display:none;position:absolute; z-index: 2000;' 
									onmouseover="mouseOverChk('divTxtShowData4','schoolName1')" class='result' ></div>
								
								</div>
								<div class="col-sm-6 col-md-6">
								
								<span class=""><label class=""><spring:message code="lblCity"/></label></span>
						       	<div id="cityDivId">
									<select id="cityId" name="cityId" class="form-control" disabled="disabled">
										<option value="0" selected="selected"><spring:message code="optAll"/></option>
									</select>
								</div>
								
								</div>
								<div class="col-sm-6 col-md-6">
									
								<label>
									<spring:message code="lblSt"/>
								</label>
								<div id="stateDivId">
								</div>
							 	
							    </div>
							    <div class="col-sm-4 col-md-4">
					<label>
						<spring:message code="lblZone"/>
					</label>
					<select id="zone" name="zone" class="form-control" disabled="disabled">
					
				    </select>
		      </div>
								 <div class="col-sm-3 col-md-3" style="max-width: 205px;">
									<label>
										<spring:message code="lblStatus"/>
									</label>
									<select id="selJBIStatus" name="selJBIStatus" class="form-control" >
										<option value="all" <c:if test="${param.searchby eq 'all'}">selected</c:if> ><spring:message code="optAll"/></option>
										<option value="avlbl" <c:if test="${param.searchby eq 'avlbl'}">selected</c:if> ><spring:message code="optAvble"/></option>
										<option value="comp" <c:if test="${param.searchby eq 'comp'}">selected</c:if>  ><spring:message code="optCmplt"/></option>
										<option value="icomp"  <c:if test="${param.searchby eq 'icomp'}">selected</c:if> ><spring:message code="optIncomlete"/></option>
										<option value="vlt" <c:if test="${param.searchby eq 'vlt'}">selected</c:if>  ><spring:message code="optTOut"/></option>	
										<option value="widrw" <c:if test="${param.searchby eq 'widrw'}">selected</c:if>  ><spring:message code="optWithdrawn"/></option>		
										<option value="hide" <c:if test="${param.searchby eq 'hide'}">selected</c:if>  ><spring:message code="optHid"/></option>
									</select>			
								</div>
								
								<div class="col-sm-1 col-md-1 top25-sm2" style="width:180px;">
			   						<button class="btn btn-primary " type="button" onclick="serachByBtn()"> <spring:message code="btnSearch"/> <i class="icon"></i></button>
			   						</div>
							</div>	 
								</div>
								<div class="col-sm-4 col-md-4">							
							<label ><spring:message code="lblSub"/></label> 
							<select id="subjectId" name="subjectId" class="form-control" multiple="multiple" size="4">
								<option value="0"><spring:message code="optSltDistForReleventSubjectList"/></option>
							</select>
							</div>
							</div>	
								<!-- 
								<div class="col-sm-6 col-md-6">
								
								<span class=""><label class=""><spring:message code="lblCity"/></label></span>
						       	<div id="cityDivId">
									<select id="cityId" name="cityId" class="form-control" disabled="disabled">
										<option value="0" selected="selected"><spring:message code="optAll"/></option>
									</select>
								</div>
								
								</div>
								<div class="col-sm-6 col-md-6">
									
								<label>
									<spring:message code="lblSt"/>
								</label>
								<div id="stateDivId">
								</div>
							 	
							    </div>								
								</div>							
							</div>
							<div class="col-sm-4 col-md-4">							
							<label ><spring:message code="lblSub"/></label> 
							<select id="subjectId" name="subjectId" class="form-control" multiple="multiple" size="4">
								<option value="0"><spring:message code="optSltDistForReleventSubjectList"/></option>
							</select>
							</div>
												
						</div>	
						
						
					
				<!-- End  :: Ashish :: Searching -->
		
	<!--<form class="bs-docs-example">	
			<div class="row" >			
			   <div class="col-sm-4 col-md-4">
					<label>
						<spring:message code="lblZone"/>
					</label>
					<select id="zone" name="zone" class="form-control" disabled="disabled">
					
				    </select>
		      </div>	     
			
			   <div class="col-sm-3 col-md-3" style="max-width: 205px;">
				<label>
					<spring:message code="lblStatus"/>
				</label>
				<select id="selJBIStatus" name="selJBIStatus" class="form-control" >
					<option value="all" <c:if test="${param.searchby eq 'all'}">selected</c:if> ><spring:message code="optAll"/></option>
					<option value="avlbl" <c:if test="${param.searchby eq 'avlbl'}">selected</c:if> ><spring:message code="optAvble"/></option>
					<option value="comp" <c:if test="${param.searchby eq 'comp'}">selected</c:if>  ><spring:message code="optCmplt"/></option>
					<option value="icomp"  <c:if test="${param.searchby eq 'icomp'}">selected</c:if> ><spring:message code="optIncomlete"/></option>
					<option value="vlt" <c:if test="${param.searchby eq 'vlt'}">selected</c:if>  ><spring:message code="optTOut"/></option>	
					<option value="widrw" <c:if test="${param.searchby eq 'widrw'}">selected</c:if>  ><spring:message code="optWithdrawn"/></option>		
					<option value="hide" <c:if test="${param.searchby eq 'hide'}">selected</c:if>  ><spring:message code="optHid"/></option>
				</select>			
			</div>
			<div class="col-sm-1 col-md-1 top25-sm2" style="width:180px;">
			   <button class="btn btn-primary " type="button" onclick="serachByBtn()">Search <i class="icon"></i></button> 
			<!-- shadab --><!--
			 <button class="btn btn-primary " type="button" onclick="serachByBtnES()">Search <i class="icon"></i></button>
			</div>-->
			<!--<div class="col-sm-3 col-md-3 top25-sm2" style="margin-top:43px;align:center">
				 <a href="userpreference.do" target="_blank" >Change my job search preferences</a>
			</div>
		</div>
	</form>-->

<div class="row">
	<div class ="mt10 col-sm-9 col-md-9"></div>
	<div class="col-sm-3 col-md-3 top25-sm2" style="margin-top:43px;    margin-left: -61px;   align:center "><a href="userpreference.do" target="_blank" style="        margin-right: -60px;" > <spring:message code="lblChangemyjob"/></a></div>
</div>

<div class="row">
	<div class="TableContent top15">        	
    	<div class="table-responsive" id="divJobsOfIntrest" onclick="getSortFirstGrid();">          
    </div>
</div>
             
</div> 
<br/>
<br/>

<div class="row">
	<c:if test="${epiStandalone}">
	<div class ="mt10 col-sm-3 col-md-3"> 
		<a href="portaldashboard.do"><spring:message code="btnClr"/></a>
	</div>
	</c:if>
</div>
   <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
	</div>

	<br/><br/>
<!-- Cover letter Start-->	

<div  class="modal hide"  id="myModalCL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headCoverLetr"/></h3>
	</div>
	<div class="modal-body" >
	<p id="cvrltrTxt" class="hide"><spring:message code="msgIfYuApplyCentealOfficePosition"/></p>
		<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>
		<label>
	        <spring:message code="msgPlzTyYurCoverLtr"/>
	        <input type="hidden" id='hiddenJobForTeacherId' >
   		</label>
   		<div id="divCoverLetter">
   			<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
   			<textarea id="coverLetter"  name="coverLetter" rows="7" cols="150"></textarea>
   		</div>							
		
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-large btn-primary" type="submit"  onclick="saveCoverLetter();"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>
						
<!-- Cover letter End -->
<div  class="modal hide"  id="myModalDASpecificQuestions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
		<div class="modal-dialog-for-cgdemoschedule">
	    <div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
			<h3 id="myModalLabel"><spring:message code="headMandatoryDecler"/></h3>
		</div>
		<div class="modal-body" style="height: 450px;overflow-y:auto" >
		<span id='textForDistrictSpecificQuestions' class="divlablevalins"><spring:message code="msgDistReqAllAnsOfQues"/></span>
			<div class='divErrorMsg' id='errordiv4question' style="display: block;padding-bottom: 7px;"></div>
			<table width="100%" border="0" class="table table-bordered table-striped" id="tblGrid">
			</table>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn btn-large btn-primary" type="button" onclick="setDistrictQuestions('dashboard');" ><strong><spring:message code="btnConti"/> <i class="icon"></i></strong></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="cancelDSQdiv()"><spring:message code="btnClose"/></button> 		
	 	</div>
	</div>
		</div>
</div>	
<div class="modal hide" id="geozoneschoolDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >

	<div class="modal-dialog" style="width:650px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);"><spring:message code="btnX"/></button>
		<h3 id="zoneName"></h3>
	</div>
	<div class="modal-body" style=""> 		
		<div class="control-group">
				<input type="hidden" id="geozoneId" value="0"/>
				<input type="hidden" id="geozoneschoolFlag" value="0"/>
				<div id="geozoneschoolGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id="">
 		</span>&nbsp;&nbsp;<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>
			<input type="hidden" name="tempjobid" id="tempjobid"/>
		 	<input type="hidden" name="exitUrl" id="exitUrl"/>
		 	<input type="hidden" name="district" id="district"/>
		 	<input type="hidden" name="school" id="school"/>
		 	<input type="hidden" name="criteria" id="criteria"/>
		 	<input type="hidden" name="epistatus" id="epistatus"/>
		 	<input type="hidden" id="ncInternalcandidateForExtJob" value="0">
		 	
<div  class="modal hide"  id="applyJobComplete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 99999;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  ><spring:message code="btnX"/></button>
		<h3><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span><b><spring:message code="msgSucceApldJob"/></b></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnOk"/></button> 		
 	</div>
</div>
</div>
</div>
<jsp:include page="dp_common.jsp"></jsp:include>		

<!-- start ... New Job Apply Process jsp files -->
	<jsp:include page="jafstartcommon.jsp"></jsp:include>
<!-- end ... New Job Apply Process jsp files -->
		
<script type="text/javascript">
$('#iconpophover1').tooltip();
$('#iconpophover2').tooltip();
$('#iconpophover3').tooltip();
$('#iconpophover4').tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophover7').tooltip();
$('#iconpophover8').tooltip();
$('#iconpophover9').tooltip();
$('#iconpophover10').tooltip();
$('#iconpophover11').tooltip();
$('#iconpophover12').tooltip();
$('#iconpophover13').tooltip();
$('#iconpophover14').tooltip();
$('#iconpophover15').tooltip();
$('#iconpophover16').tooltip();
$('#iconpophover17').tooltip();
$('#iconpophover18').tooltip();

displayUSAStateData(223);
//getJosOfIntrestGrid();
getJBIbyStatus();
</script>
<script type="text/javascript">
   $(document).ready(function () {
      $('#ssn_pi').bind('paste', function (e) {
         e.preventDefault();
        // alert("You cannot paste text");
      });
   });
</script>
