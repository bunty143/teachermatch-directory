<!-- @Author: Gagan 
 * @Discription: view of edit panelMember Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src='js/panelMember.js?ver=${resourceMap["js/panelMember.js"]}'></script>
<script type="text/javascript" src="dwr/interface/PanelAjax.js?ver=${resourceMap['PanelAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        
            
        });
        
function applyScrollOnTbl()
{
	//alert("Hi");
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#panelMemberTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[775,200], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script>

<div id="panelMemberDiv">
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblManageMember"/></div>	
         </div>			
		<div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="return addNewPanelMember()"><spring:message code="lnkAddMember"/></a>
		</div>
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>


  <div class="TableContent top15">        	
            <div class="table-responsive" id="panelMemberGrid">          
                	         
            </div>            
  </div> 
<div  id="divPanelForm" style="display: none;" class="mt10 span16" onkeypress="return chkForEnterSavePanel(event);" >
	<div class="row">	
		<div class="col-sm-6 col-md-6">			                         
		 	<div class='divErrorMsg' id='errorpanelMemberdiv' ></div>
		</div>
	</div>
	<form  id="frmPanelMember" onsubmit="return false;">
		<div class="row">
			<div class="col-sm-6 col-md-6"><label><strong><spring:message code="lblDistrictName"/></strong></label></br>
				<input type="hidden" name="districtId" id="districtId" value="${districtPanelMaster.districtMaster.districtId}">
				${districtPanelMaster.districtMaster.districtName}
				
			</div>
		</div>
		
		
		<div class="row" style="padding-top: 5px;">
			<div class="col-sm-6 col-md-6"><label><strong><spring:message code="lblPanelName"/></strong></label>
				<input type="hidden" id="panelId" name="panelId" value="${districtPanelMaster.panelId}">
				<input type="hidden" id="panelMemberId" name="panelMemberId" value="">
				${districtPanelMaster.panelName}
			</div>
		</div>
		
		
		<div class="row" >
			<div class="col-sm-6 col-md-6"><label><spring:message code="lblSchoolName"/></label>
				<input type="text" id="schoolName" autocomplete="off" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
				onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
				onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
				<input type="hidden" id="schoolId" name="schoolId" value="0"/>
				<input type="hidden" id="schoolName1" value="" name="schoolName1"/>
				<div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
			</div>
      	</div>
		
		<div class="row">
			<div class="col-sm-6 col-md-6"><label><strong><spring:message code="lblMembers"/></strong></label>
				<input type="hidden" name="districtId" id="districtId" value="${districtPanelMaster.districtMaster.districtId}">
				<select id="memberId" name="memberId" class="form-control">
					<option value="" id="memberOption" selected="selected"><spring:message code="optionSelectMember"/></option>
					<c:forEach items="${lstuserMaster}" var="lsum" >
						<option id="${lsum.userId}" value="${lsum.userId}">${lsum.firstName} ${lsum.lastName}</option>
					</c:forEach>
				</select>
				
			</div>
		</div>
		
		
		 <div class="row mb30" style="display: none;" id="divManage">
		  	<div class="col-sm-6 col-md-6 mt30">
		     		<button onclick="savePanelMember();" class="btn btn-large btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
		     	 &nbsp;<a href="javascript:void(0);" onclick="clearPanelMember();"><spring:message code="btnClr"/></a>  
		    </div>
		  </div>
		  <br/>
		  <div class="row">
		<div class="col-sm-6 col-md-6 idone" id="divDone" style="display: none;">
			<a href="javascript:void(0);" onclick="savePanelMember();"><spring:message code="lnkImD"/></a>
			 &nbsp;<a href="javascript:void(0);" onclick="clearPanelMember();"><spring:message code="btnClr"/></a> 
		</div>
		</div>
	</form>
</div>

    <div class="row">
	    <div class="col-sm-3 col-md-3"  id="">
		<br/>
			<button onclick="go();" class="btn btn-large btn-primary"><strong><spring:message code="btnSub"/> <i class="icon"></i></strong></button>
		</div> 
    </div>               
 </div>             



<div class="modal hide"  id="deletePanelDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeletepanel"/>  
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteconfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
    </div>
	</div>
</div>

<script type="text/javascript">
	displayPanelMember();
	//$('#iconpophover1').tooltip();
	//$('#iconpophover2').tooltip();
</script>