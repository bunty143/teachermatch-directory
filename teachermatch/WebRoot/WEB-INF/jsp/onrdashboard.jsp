<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/ONRAjax.js?ver=${resourceMap['ONRAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resourceMap['TeacherProfileViewInDivAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type="text/javascript" src='dwr/util.js'></script>

<script type="text/javascript" src="calender/js/jscal2.js"></script>
<script type="text/javascript" src="calender/js/lang/en.js"></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resouceMap['PFCertifications.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resouceMap['BatchJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFAcademics.js?ver=${resouceMap['PFAcademics.ajax']}"></script>
<script type='text/javascript' src="js/report/cgTranscriptOrCertifications.js?ver=${resouceMap['js/report/cgTranscriptOrCertifications.js']}"></script>
<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resouceMap['js/certtypeautocomplete.js']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherInfotAjax.js?ver=${resouceMap['TeacherInfoAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js?ver=${resouceMap['CandidateReportService.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherSendMessageAjax.js?ver=${resouceMap['TeacherSendMessageAjax']}"></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resouceMap['PFCertifications.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resouceMap['TeacherProfileViewInDivAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resouceMap['ManageStatusAjax.ajax']}"></script>
<script type="text/javascript" src="js/teacherinfonew.js?ver=${resouceMap['js/teacherinfonew.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type='text/javascript' src="js/report/candidategrid.js?ver=${resouceMap['js/report/candidategrid.js']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridAjax.js?ver=${resouceMap['CandidateGridAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateGridSubAjax.js?ver=${resouceMap['CandidateGridSubAjax.ajax']}"></script>
<script type="text/javascript" src="js/onr_common.js?ver=${resouceMap['js/onr_common.js']}"></script>
<script type="text/javascript" src="dwr/interface/ONBServiceAjax.js?ver=${resourceMap['ONBServiceAjax.Ajax']}"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css?ver=${resouceMap['css/dialogbox.css']}" />
<link rel="stylesheet" type="text/css" href="css/onrdashboard.css?ver=${resouceMap['css/onrdashboard.css']}" />
<script type="text/javascript" src="dwr/interface/CandidateGridAjaxNew.js?ver=${resouceMap['CandidateGridAjaxNew.ajax']}"></script>

<script type='text/javascript' src="js/report/sspfCandidategridnew.js?ver=${resourceMap['js/report/sspfCandidategridnew.js']}"></script>
<script type="text/javascript" src="dwr/interface/SelfServiceCandidateProfileService.js?ver=${resourceMap['SelfServiceCandidateProfileService.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax .ajax']}"></script>
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>

<style>
.scrollspy_profile { height:500px; overflow-y:auto; overflow-x:hidden;padding-left:5px;
.profileContent
{
	padding: 0px;
	padding-left: 12px;
	padding-top: 10px;
	color: #474747;
	font-weight: normal;
	font-size: 10px;
}
</style>
<script type="text/javascript">
	var $j=jQuery.noConflict();
	$j(document).ready(function() {
});

 function applyScrollOnLEACandidatePorfolio()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLEACandidatePorfolioGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
       	<c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[200,100,200,150,100,80],
           </c:when>
           <c:otherwise>
             colratio:[200,100,150,150,80,50],
           </c:otherwise>
        </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


 function applyScrollOnTblLicenseDate()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertificationsGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[400,220,210],
           </c:when>
           <c:otherwise>
             colratio:[350,220,160],
           </c:otherwise>
        </c:choose>
       	addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


function applyScrollOnAssessments()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnTbl_AssessmentDetails()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentDetails').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        width: 830,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[230,200,200,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblLanguage()
{
    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[277,276,276],
           </c:when>
           <c:otherwise>
             colratio:[244,243,243],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}


function applyScrollOnHonors()
{
		var $j=jQuery.noConflict();
      $j(document).ready(function() {
      $j('#honorsGrid').fixheadertable({ //table id 
      caption: '',
      showhide: false,
      theme: 'ui',
      height: 350,
      width: 730,
      minWidth: null,
      minWidthAuto: false,
      colratio:[250,250,175,176,111], // table header width
      addTitles: false,
      zebra: true,
      zebraClass: 'net-alternative-row',
      sortable: false,
      sortedColId: null,
      //sortType:[],
      dateFormat: 'd-m-y',
      pager: false,
      rowsPerPage: 10,
      resizeCol: false,
      minColWidth: 100,
      wrapper: false
      });            
      });
}


function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
      $j(document).ready(function() {
      $j('#involvementGrid').fixheadertable({ //table id 
      caption: '',
      showhide: false,
      theme: 'ui',
      height: 350,
      width: 730,
      minWidth: null,
      minWidthAuto: false,
      colratio:[250,250,175,176,111], // table header width
      addTitles: false,
      zebra: true,
      zebraClass: 'net-alternative-row',
      sortable: false,
      sortedColId: null,
      //sortType:[],
      dateFormat: 'd-m-y',
      pager: false,
      rowsPerPage: 10,
      resizeCol: false,
      minColWidth: 100,
      wrapper: false
      });            
      });
}





function applyScrollOnTblOnr(userRoleId)
{	
	if(${ncCheck}==true){
		if(userRoleId == 8){
		 	var columnSpan = [360,100,120,80,180,60,80];
		 	var tableWidth = 980;
		 }else if(userRoleId == 9){
		 	var columnSpan = [260,100,100,100,150,60,60,50,50,50,50,70];
		 	var tableWidth = 1100;
		 }else {
		 	var columnSpan = [30,260,100,120,120,150,70,70,60,60,60,60,70];
		 	var tableWidth = 1230;
		 }
	
	}else{
			if(userRoleId == 8){
		 	var columnSpan = [320,40,100,120,80,180,60,80];
		 	var tableWidth = 980;
		 }else if(userRoleId == 9){
		 	var columnSpan = [220,40,80,80,80,130,50,50,50,50,50,50,50,50,70];
		 	var tableWidth = 1100;
		 }else {
		   if($("#districtId").val()==806900 || $("#sadistrictId").val()==806900){
		     if($("#sadistrictId").val()==806900 && $("#entityType").val()==3){
		   	 	 var columnSpan = [30,220,40,80,80,130,50,50,50,50,50,50,50,50,50,50,50,50,50,50,70];
		 	  	 var tableWidth = 1350;
		 	  }else{
		 	  	 var columnSpan = [30,220,40,50,80,80,130,50,50,50,50,50,50,50,50,50,50,50,50,50,50,70];
		 	  	 var tableWidth = 1400;
		 	  }
		   }else{
		 	  var columnSpan = [30,220,40,80,80,80,130,50,50,50,50,50,50,50,50,50,50,70];
		 	  var tableWidth = 1230;
		 	}
		 	if($("#sadistrictId").val()==1200390 && $("#entityType").val()==3){
	             columnSpan = [30,220,40,80,80,130,50,50,50,50,50,50,50,50,50,50,70];
	             tableWidth= 1150;
	           	 }
		 }
	}
  console.log(columnSpan);
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#onrGridTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: tableWidth,
        minWidth: null,
        minWidthAuto: false,
        colratio:columnSpan, // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 10,
        wrapper: false
        });
        });			
}
function applyScrollOnTblI9()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#firstTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblFP()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#fpTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 830,
        minWidth: null,
        minWidthAuto: false,
        colratio:[80,130,80,80,100,130,150,80], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
function applyScrollOnTblDT()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#dtTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,180,80,80,130,266], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblTrans()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#transTableHistory').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnTblTransAcademic()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#academicGridAcademic').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[35,150,180,200,185,76],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function applyScrollOnTblEEOC()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#EEOCTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[90,200,100,180,256], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblTeacherCertifications_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherCertificates_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,100,100,130,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblTeacherAcademics_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherAcademics_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,    
        colratio:[410,180,60,60],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblWorkExp()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblWorkExp_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[148,278,90,103,111],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblEleRef_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleleReferences_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[120,85,90,160,80,82,65,45],    //changed: by rajendra	
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 728,
        minWidth: null,
        minWidthAuto: false,
        colratio:[500,228,],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblVideoLinks_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblelevideoLink_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 730,
        minWidth: null,
        minWidthAuto: false,
        colratio:[450,150,130,],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

//////////Message scroll 
function applyScrollOnMessageTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblMessages').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 70,
        width: 614,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,140,100,215,60], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

</script>

<style>
.dropdown-menu11{
 	min-width:40px;
 	margin-left: -60px; 
 }
 
.dropdown-menu11 {  
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 170px;
  padding: 5px 0;
  margin-top: 10px;
  list-style: none;
  background-color: #ffffff;
  border: 1px solid #ccc;
  /*border: 1px solid rgba(0, 0, 0, 0.2);*/
  *border-right-width: 2px;
  *border-bottom-width: 2px;
  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
  -webkit-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  box-shadow: 0 5px 10px rgba(0, 0, 0, 0.2);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding;
  background-clip: padding-box;
  min-width:40px;

}
.navbar .nav > li > .dropdown-menu11:before {
content: '';
display: inline-block;
border-left: 7px solid transparent;
border-right: 7px solid transparent;
border-bottom: 7px solid #ccc;
/*border-bottom-color: rgba(0, 0, 0, 0.2);*/
position: absolute;
top: -7px;
left: 10px;      
}
.navbar .nav > li > .dropdown-menu11:after {
content: '';
display: inline-block;
border-left: 6px solid transparent;
border-right: 6px solid transparent;
border-bottom: 6px solid #ffffff;
position: absolute;
top: -6px;
left: 10px;
}
.dropdown-menu11.pull-right {
  right: 0;
  left: auto;
}
.dropdown-menu11.divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5;
}
.dropdown-menu11 > li > a {
  display: block;
  padding: 0px 20px;
  margin-top:5px;
  clear: both;
  font-weight: normal;
  line-height: 1.428571429;
  color: #333333;
  white-space: nowrap;
}
.dropdown-menu11 > li > a:hover,
.dropdown-menu11 > li > a:focus {
  text-decoration: none;
  color: #428bca;
  //background-color: #428bca;
}
.dropdown-menu11 > .active > a,
.dropdown-menu11 > .active > a:hover,
.dropdown-menu11 > .active > a:focus {
  color: #ffffff;
  text-decoration: none;
  outline: 0;
  background-color: #428bca;
}
.dropdown-menu11 > .disabled > a,
.dropdown-menu11 > .disabled > a:hover,
.dropdown-menu11 > .disabled > a:focus {
  color: #999999;
}
.dropdown-menu11 > .disabled > a:hover,
.dropdown-menu11 > .disabled > a:focus {
  text-decoration: none;
  background-color: transparent;
  background-image: none;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
  cursor: not-allowed;
}
.open > .dropdown-menu11 {
  display: block;
}
.open > a {
  outline: 0;
}

.pull-right > .dropdown-menu11 {
  right: 0;
  left: auto;
}

.dropup .dropdown-menu11,
.navbar-fixed-bottom .dropdown .dropdown-menu11 {
  top: auto;
  bottom: 100%;
  margin-bottom: 1px;
}
</style>

<input type="hidden" name="sAddStatus" id="sAddStatus" value="${sAddStatus}" />

<div id="loadingDiv"> <!-- style="display:none; z-index: 5000;"  -->
  	<table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>

<div id="loadingDivHZS" style="display:none; z-index: 5000;">
  	<table align="center">
 		<tr><td style="padding-top:270px;padding-left:0px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:0px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>

	<div class="row">						  
  	<div class="col-sm-6">
    <div style="float: left;"><img src="images/dashboard-icon.png" width="41" height="41" alt=""></div>
    <div style="float: left;">
     	<div class="subheading"><spring:message code="lblPNRDashboard"/></div>	
     	<input type="hidden" name="districtId" id="districtId" value="${districtId}"/>
    </div>
    <div style="clear: both;"></div>
</div>
	  <div class="col-sm-6"></div>	
      <div style="clear: both;"></div>							
      <div class="col-sm-12" style='width: 1171px;'>
   	  <div class="centerline"></div>  
   	  <div style="clear: both;"></div>	  
  </div>							 						
</div>			
<div id="panelDiv">
<c:if test="${entityType ne 5 && entityType ne 6}">
<div class="row mt10">
<input type="hidden" name="statusType" id="statusType" /> 
<table width='1090'  border='0' cellspacing='1' cellpadding='1'>
  <tr>
    <td>
	    <c:choose>
			<c:when test="${(DistrictId eq 1200390 || DistrictId eq 806900) && entityType eq 3}">	
				<div class="col-sm-3 col-md-2 hide">
			 		<span class=""><label class=""><spring:message code="lblSSN"/></label><input type="text" id="ssn" name="ssn" class="help-inline form-control" onkeypress="return chkForEnterDisplayPanel(event);"/></span>
				</div>	
			</c:when>
			<c:otherwise>
				<div class="col-sm-3 col-md-2">
		 			<span class=""><label class=""><spring:message code="lblSSN"/></label><input type="text" id="ssn" name="ssn" class="help-inline form-control" onkeypress="return chkForEnterDisplayPanel(event);"/></span>
				</div>
		    </c:otherwise>
		</c:choose>
		<div class="col-sm-3 col-md-3">
			<span class=""><label class=""><spring:message code="lblFname"/></label><input type="text" id="firstName" name="firstName"  maxlength="50"  class="help-inline form-control" onkeypress="return chkForEnterDisplayPanel(event);" /></span>
		</div>
		<div class="col-sm-3 col-md-3">
		 <div class="">
		 	<span class=""><label class=""><spring:message code="lblLname"/></label><input type="text" id="lastName" name="lastName"  maxlength="50"  class="help-inline form-control" onkeypress="return chkForEnterDisplayPanel(event);"/></span>
		 </div>
		</div>	
		<c:if test="${DistrictId ne 806900}">		       
			<div class="col-sm-3 col-md-2">
			 <div><label class=""><spring:message code="lblPosition"/> #</label><input type="text" id="position" name="position"  maxlength="75"  class="form-control fl" onkeypress="return chkForEnterDisplayPanel(event);"/>
			</div>				       
			</div>
		</c:if>
		<div class="col-sm-2 col-md-2" style="padding-left: 20px;">
			<button class="btn btn-primary top25-sm2" type="button" onclick="searchPanelButton()" style="width: 95px;"><spring:message code="lnkSearch"/><i class="icon"></i></button> 				      
		</div>

    </td>
    <td style="width:250px;">
    	<table width='100%'  border='0' cellspacing='5' cellpadding='5'>
		  <tr>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #00882B;"></span></td>
			<td align="left"><spring:message code="optCmplt"/></td>
			<td>&nbsp;</td>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #FF0000;"></span></td>
			<td align="left"><spring:message code="lblFailed"/></td>
		  </tr>
		   <tr>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #FFFF00;"></span></td>
			<td align="left"><spring:message code="lblWaived"/></td>
			<td>&nbsp;</td>
		    <td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #000000;"></span></td>
		    <td align="left"><spring:message code="lblReprint"/></td>
		  </tr>
		  <tr>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #0000CC;"></span></td>
			<td align="left"><spring:message code="lblPending"/></td>
			<td>&nbsp;</td>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #7F7F7F;"></span></td>
			<td align="left"><spring:message code="optIncomlete"/></td>
		  </tr>
		  <tr>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #473F3F;"></span></td>
			<td align="left"><spring:message code="lblPrinted"/></td>
			<td>&nbsp;</td>
			<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #FFA500;"></span></td>
			<td align="left"><spring:message code="lblCHRPending"/></td>
		  </tr>
		  <c:if test="${(DistrictId eq 806900)}">
			  <tr>
				<td><span class="nobground1" style="font-size: 7px;font-weight: bold;background-color: #9400D3;"></span></td>
				<td align="left"><spring:message code="lblSendtoBISI"/></td>
			  </tr>
		  </c:if>
		</table>
    </td>
  </tr>
</table>

</div>
</c:if>
<c:if test="${entityType ne 5 && entityType ne 6}">
       <div class="row"> 
	       <div class="col-sm-12">
         <table class="">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="generatePNRExel();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='generatePNRPDF();'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td>
					<a data-original-title='Print' rel='tooltip' id='printId' href='javascript:void(0);' onclick="printPNRDATA();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			</tr>
		</table>
    </div>
   </div> 
</c:if>
<c:if test="${entityType eq 5 || entityType eq 6}">
 	<div class="row"> 
	       <div class="col-sm-12 top20 subheading" style="text-align: center;font-size: 25px;">
	       Coming Shortly
	       </div>
	</div>
</c:if>
<c:choose>

<c:when test="${(DistrictId eq 806900)}">	
<div class="row" style="margin-right: -476px;">		
</c:when>
<c:otherwise>
	<div class="row" style="margin-right: -350px;">	
</c:otherwise>
</c:choose>	

	    <div class="col-sm-12">
	    	<div class="table-responsive" id="panelGrid" style="">		
		    </div>
       </div>
  </div>
 </div>
  <div  class="modal hide onr_popup"  id="onr_popup"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3><span id="myModalLabelI9"></span></h3>
				<input type="hidden" id="myModalLabelI9Hidden" name="myModalLabelI9Hidden" />
			</div>
			<div class="modal-body">
			<div style="padding-left:760px;"><a href="#" onclick="return open_div()"><span id="addStatusI9"></span></a></div>
			<input type="hidden" id="addStatusI9Hidden" name="addStatusI9Hidden" />
				<div class="control-group">
					<div class="" id="divTranscriptI9">
					 
					</div>
				 </div>
					<div class="row ">
						  <div class="control-group span16">                   
					 			<div class='divErrorMsg' id='errordiv' style="padding-left:15px;"></div>
						  </div>
					</div>
					<div class="" id="add_action" class="add_action" style="display:none;">
						 <table width="100%"  border="0">
							  <tr>
							    <td width="17%"><spring:message code="optStatus"/><span class="required">*</span></td>
							    <td>
									<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Yes" id="status1" name="status"> <spring:message code="optY"/>
				        			</label>
				        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="No" id="status2" name="status"> <spring:message code="optN"/>
				        			</label>
				        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Waived" id="status3" name="status"> <spring:message code="lblWaived"/>
				        			</label>
								</td>
							  </tr>
							  <tr>
							    <td><spring:message code="headNot"/></td>
							    <td><div id="notesmsg"><textarea rows="5" class="form-control" cols="25" name="notes" id="notes" maxlength="1000"></textarea></div></td>
							  </tr>
							  <tr><td>&nbsp;</td></tr>
							  <tr>
							    <td><spring:message code="tdHeadingAF"/></td>
							    <td>
							    <iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
								</iframe>
									<form id="frmonrI9Upload" enctype='multipart/form-data' method='post' target='uploadFrame' action='onrI9UploadServlet.do'>						    	
										<input type="hidden" id="jsiFileName" name="jsiFileName" value="" />
										<input type="hidden" id="teacherId" name="teacherId" />
										<input type="hidden" id="eligibilityId" name="eligibilityId" />
										<input type="hidden" id="jobId" name="jobId" />
							    		<input id="eligibilyFile" name="eligibilyFile" type="file" width="20px;" style="">
							  		</form>
							  </tr>
							  <tr><td>&nbsp;</td></tr>
							  <tr>
							    <td>
							    	<a href="javascript:void(0);" onclick="saveNotesPnr()"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;&nbsp;
							    	<a href="javascript:void(0);" onclick="cancelDiv()"><spring:message code="btnClr"/></a>
							    </td>
							  </tr>
						</table>
					</div>
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>
<!--  FP Open DIV -->
  <div  class="modal hide onr_popup_fp"  id="onr_popup1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3><span id="myModalLabelFp"></span></h3></h3>
				<input type="hidden" id="myModalLabelFpHidden" name="myModalLabelFpHidden" />
			</div>
			<div class="modal-body">
				<div style="padding-left:760px;"><a href="#" onclick="return open_div_fp()"><span id="addStatusFP"></span></a></div>
				<input type="hidden" id="addStatusFPHidden" name="addStatusFPHidden" />
				<div class="control-group">
					<div class="" id="divFP">

					</div>
				</div>
					<div class="" id="add_action_fp" class="add_action_fp" style="display:none;">
						<div class="row">
						  <div class="control-group span16"> 
					 			<div class='divErrorMsg' id='errordiv1' style="padding-left:15px;"></div>
						  </div>
						</div>
						 <table width="100%"  border="0">
							  <tr>
							    <td width="14.7%"><spring:message code="lblStatus"/><span class="required">*</span></td>
							    <td>
									<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Clear" id="status4" name="statusFP" onClick="fpClearDate()"> <spring:message code="lnkClear"/>
				        			</label>
				        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Failed" id="status5" name="statusFP"  onClick="fpFailedDate()"> <spring:message code="lblFailed"/>
				        			</label>
				        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Pending" id="status6" name="statusFP" onClick="fpCloseDate()"> <spring:message code="lblCHRPending"/>
				        			</label>
				        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Reprint" id="status7" name="statusFP" onClick="fpReprintDate()"> <spring:message code="lblReprint"/>
				        			</label>
				        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="Printed" id="statusP" name="statusFP" onClick="fpPrintedDate()"> <spring:message code="lblPrinted"/>
				        			</label>
								</td>
							  </tr>
							  <tr>
							  	<td colspan=2>
							  	 <div id="ExaminationDateDiv"  style="display:none;">
							  		<table>
							  			<tr>
							  				<td><spring:message code="tdHeadingExamDate"/><span class="required">*</span></td>
							  				<td style="padding-left:13px;">
							  					<input type="text" id="examStartDateFp" name="examStartDateFp"  maxlength="0" class="help-inline form-control">
							  				</td>
							  				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							  				<td><spring:message code="tdHeadingClearDate"/><span class="required">*</span></td>
							  				<td style="padding-left:13px;"><input id="examClearDateFp" name="examClearDateFp" type="text" maxlength="0" class="help-inline form-control"></td>
							  			</tr>
							  		</table>
							  	</div>
							  	</td>
							  </tr>
							  <tr>
							  	<td colspan=2>
							  	 <div id="FailedDateDiv"  style="display:none;">
							  		<table>
							  			<tr>
							  				<td><spring:message code="tdHeadingExamDate"/><span class="required">*</span></td>
							  				<td style="padding-left:13px;"><input type="text" id="examStartDateFp1" name="examStartDateFp1"  maxlength="0" class="help-inline form-control"></td>
							  				<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							  				<td><spring:message code="tdHeadingFailedDate"/><span class="required">*</span></td>
							  				<td style="padding-left:13px;"><input id="failedDateFp" name="failedDateFp" type="text" maxlength="0" class="help-inline form-control"></td>
							  			</tr>
							  		</table>
							  	</div>
							  	</td>
							  </tr>
							  <tr>
							  	<td colspan=2>
							  	 <div id="ReprintDateDiv"  style="display:none;">
							  		<table>
							  			<tr>
							  				<td><spring:message code="tdHeadingExamDate"/><span class="required">*</span></td>
							  				<td style="padding-left:13px;"><input type="text" id="examStartDateFp2" name="examStartDateFp2"  maxlength="0" class="help-inline form-control"></td>
								  			<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							  				<td><spring:message code="tdHeadingprtdate"/><span class="required">*</span></td>
							  				<td style="padding-left:13px;"><input id="reprintDateFp" name="reprintDateFp" type="text" maxlength="0" class="help-inline form-control"></td>
							  			</tr>
							  		</table>
							  	</div>
							  	</td>
							  </tr>
							  <tr>
							    <td><spring:message code="headNot"/></td>
							    <td><div id="notesmsgFp"><textarea rows="5" class="form-control" cols="25" name="notes" id="notes" maxlength="1000"></textarea></div></td>
							  </tr>
							  <tr><td>&nbsp;</td></tr>
							  <tr>
							    <td><spring:message code="tdHeadingAF"/></td>
							    <td>
							    	<form id="frmoFpUpload" enctype='multipart/form-data' method='post' target='uploadFrame' action='onrFpUploadServlet.do'>
								    	<input type="hidden" id="jsiFileNameFp" name="jsiFileNameFp" value="" />
								    	<input type="hidden" id="teacherId1" name="teacherId1" />
								    	<input type="hidden" id="eligibilityId1" name="eligibilityId1" />
								    	<input type="hidden" id="jobId1" name="jobId1" />
								    	<input id="eligibilyFileFp" name="eligibilyFileFp" type="file" width="20px;" style="">
							    	</form>
							    </td>
							  </tr>
							  <tr><td>&nbsp;</td></tr>
							  <tr>
							    <td colspan="2">
							    	<a href="javascript:void(0);" onclick="saveNotesFP()"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;&nbsp;
							    	<a href="javascript:void(0);" onclick="cancelDiv()"><spring:message code="btnClr"/></a>
							    </td>
							  </tr>
						</table>
					</div>
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!--  DT Open DIV -->
  <div  class="modal hide onr_popup_dt"  id="onr_popup2"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3><span id="myModalLabelDt"></span></h3></h3>
				<input type="hidden" id="myModalLabelDtHidden" name="myModalLabelDtHidden" />
			</div>
			<div class="modal-body">
				<div style="padding-left:760px;"><a href="#" onclick="return open_div_dt()"><span id="addStatusDT"></span></a></div>
				<input type="hidden" id="addStatusDTHidden" name="addStatusDTHidden" />
				<div class="control-group">
					<div class="" id="divDT">
					 
					</div>
				</div>
				<div class="" id="add_action_dt" class="add_action_dt" style="display:none;">
					<div class="row">
						  <div class="control-group span16">
					 			<div class='divErrorMsg' id='errordiv3' style="padding-left:15px;"></div>
						  </div>
					</div>
					 <table width="100%"  border="0">
						  <tr>
						    <td width="14.7%">Status<span class="required">*</span></td>
						    <td>
								<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
				    				<input type="radio" value="Pass" id="status8" name="statusDT" onClick="dtClearDate()"> Pass
			        			</label>
			        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
				    				<input type="radio" value="Failed" id="status9" name="statusDT" onClick="dtClearDate()"> Failed
			        			</label>
			        			<label class="col-sm-2 col-md-2 radio"  style="margin-top: 0px;">
				    				<input type="radio" value="Wavied" id="status10" name="statusDT" onClick="dtClearDate()"> Waived
			        			</label>
			        			<c:if test="${not empty districtId && districtId eq 806900}">
				        			<label class="col-sm-3 col-md-3 radio"  style="margin-top: 0px;">
					    				<input type="radio" value="BC" id="status11" name="statusDT" onClick="dtClearDate()"> Send to BISI
				        			</label>
			        			</c:if>
							</td>
						  </tr>
						  <tr>
						  	<td colspan=2>
						  	 <div id="ExaminationDateDivDT"  style="display:none;">
						  		<table>
						  			<tr>
						  				<td>Examination Date <span id="requiredExam"><c:if test="${not empty districtId && districtId ne 806900}"><span class="required">*</span></span></c:if></td>
						  				<td style="padding-left:23px;"><input type="text" id="examStartDateDt" name="examStartDateDt"  maxlength="0" class="help-inline form-control"></td>
						  			</tr>
						  		</table>
						  	</div>
						  	</td>
						  </tr>
						  <tr>
						    <td>Notes</td>
						    <td><div id="notesmsgDt"><textarea rows="5" class="form-control" cols="25" name="notes" id="notes" maxlength="1000"></textarea></div></td>
						  </tr>
						  <tr><td>&nbsp;</td></tr>
						  <tr>
						    <td><spring:message code="tdHeadingAF"/></td>
						    <td>
				    	<form id="frmDtUpload" enctype='multipart/form-data' method='post' target='uploadFrame' action='onrDtUploadServlet.do'>
					    	<input type="hidden" id="jsiFileNameDt" name="jsiFileNameDt" value="" />
					    	<input type="hidden" id="teacherId2" name="teacherId2" />
					    	<input type="hidden" id="eligibilityId2" name="eligibilityId2" />
					    	<input type="hidden" id="jobId2" name="jobId2" />
					    	<input id="eligibilyFileDt" name="eligibilyFileDt" type="file" width="20px;" style="">
				    	</form>
						    </td>
						  </tr>
						  <tr><td>&nbsp;</td></tr>
						  <tr>
						    <td colspan="2"><a href="javascript:void(0);" onclick="saveNotesDT()"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;&nbsp;
						    <a href="javascript:void(0);" onclick="cancelDiv()"><spring:message code="btnClr"/></a></td>
						  </tr>
					</table>
				</div>
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!--  Trans Open DIV -->
  <div  class="modal hide onr_popup_trans"  id="onr_popup_trans"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3><span id="myModalLabelTrans"></span></h3></h3>
				<input type="hidden" id="myModalLabelTransHidden" name="myModalLabelTransHidden" />
			</div>
			<div class="modal-body">
				<div style="padding-left:760px;"><a href="#" onclick="return open_div_trans()"><span id="addStatusTrans"></span></a></div>
				<input type="hidden" id="addStatusTransHidden" name="addStatusTransHidden" />
				<div class="control-group">
					<div class="" id="divTrans">

					</div>
				</div>
				<div class="" id="add_action_academic" class="add_action_academic" style="display:none;margin-top:36px;">
					<div class="row">
						  <div class="control-group span16" id="degreeConferred" style="padding-left:15px;">
					 			<div style='color:red;' id='errordivTrans'></div>
						  </div>
					</div>
					 <table width="100%"  border="0">
						  	<div id="divTransAcademic">

							</div>
						  <tr>
						    <td>
						    	<input type="hidden" id="teacherIdTrans" name="teacherIdTrans" />
				    			<input type="hidden" id="eligibilityIdTrans" name="eligibilityIdTrans" />
				    			<input type="hidden" id="jobIdTrans" name="jobIdTrans" />
						    </td>
						  </tr>
						  <tr><td>&nbsp;</td></tr>
					</table>
				</div>		
				</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button>
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!-- Notes DIV  -->
<div  class="modal hide onrNotesShow"  id="onrNotesShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameI9"></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divOnrNotes">
					 
					</div>
				</div>
				
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDivTop();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!-- FP Notes DIV  -->
<div  class="modal hide onrNotesShowFP"  id="onrNotesShowFP"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameFP"></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divOnrNotesFP">
					 
					</div>
				</div>
				
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDivFP();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!-- DT Notes DIV  -->
<div  class="modal hide onrNotesShowDT"  id="onrNotesShowDT"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameDT"></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divOnrNotesDT">
					 
					</div>
				</div>
				
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDivDT();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!-- Trans Notes DIV  -->
<div  class="modal hide onrNotesShowTrans"  id="onrNotesShowTrans"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameTrans"></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divOnrNotesTrans">
					 
					</div>
				</div>
				
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDivTrans();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!--  FRS Code Open DIV -->
  <div class="modal hide onr_popup_frsCode"  id="onr_popup_frsCode"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3><span id="myModalLabelFrsCode"></span></h3></h3>
				<input type="hidden" id="myModalLabelEEOCHidden" name="myModalLabelEEOCHidden" />
			</div>
			<div class="modal-body">
				<div style="padding-left:760px;"><a href="#" onclick="return openDivEEOC()"><span id="addStatusEEOC"></span></a></div>
				<input type="hidden" id="addStatusEEOCHidden" name="addStatusEEOCHidden" />
				<div class="control-group">

				<div class="" id="divFrsCode"></div>
				<div id="EEOCDivOpen" style="display: none;">
					<table  id='frsCodeTableHistory' border='0' class=''>
							<tr>
								<td colspan="4">
									<div class="control-group span16" id="degreeConferred">
										<div style='color:red;' id='errordivFrsCode'></div>
									</div>
								</td>
							</tr>
							<tr><td>&nbsp;</td></tr>
							<tr>
								<td colspan="4">
									<label style="vertical-align: middle;"><spring:message code="lblRetirement"/>&nbsp;&nbsp;</label>
									<label style="vertical-align: middle;"><input class="help-inline form-control" name="sapFrsCode" id="sapFrsCode" style="width:60px;" type="text" maxlength="2"></label>
								</td>
							</tr>
						<tr>
							<td width="25%" valign="top" style="padding-top: 5px;">
									<div class="span3 mt10" style="margin-top:0px;">
										<label><B><spring:message code="lblEthOrig"/></B></label>
											<c:forEach items="${lstEthnicOriginMasters}" var="org">
												<c:if test="${org.ethnicOriginId eq teacherpersonalinfo.ethnicOriginId.ethnicOriginId}">	        			
					         						<c:set var="checked" value="checked"></c:set>	         			
					        					</c:if>
								        		<label class="radio">		
													<input type="radio" name="sapEthnicOriginId[]" id="sapEthnicOriginId_${org.ethnicOriginId}" ${checked} value="${org.ethnicOriginId}">${org.ethnicOriginName}</br>
												</label>
												<c:set var="checked" value=""></c:set>	         			
											</c:forEach>				
							</td>
							
							<td width="25%" valign="top" style="padding-top: 5px;">
								<div class="span3 mt10" style="margin-top:0px;">
									<label><B><spring:message code="lblEth"/></B></label>
										<c:forEach items="${lstEthinicityMasters}" var="ecm">
												<c:if test="${ecm.ethnicityId eq teacherpersonalinfo.ethnicityId.ethnicityId}">	        			
							         				<c:set var="checked" value="checked"></c:set>	         			
							        			</c:if>
								        		<label class="radio">		
													<input type="radio" name="sapEthnicityId[]" id="sapEthnicityId_${ecm.ethnicityId}" ${checked} value="${ecm.ethnicityId}">${ecm.ethnicityName}</br>
												</label>
												<c:set var="checked" value=""></c:set>	         			
										</c:forEach>				
									
								</div>	
							</td>
							
							<td width="30%" valign="top" style="padding-top: 5px;">
									<div class="span4 mt10" style="margin-top:0px;">
											
												<label><B><spring:message code="lblRac"/></B></label>
																							
													<c:forEach items="${listRaceMasters}" var="race">
													
													<c:if test="${(DistrictId eq 1200390)  && (race.raceId != 4)}">	
													
													<c:forEach items="${splitRaceId}" var="raceSP">
															
														<c:if test="${race.raceId eq raceSP}">	        			
															<c:set var="checked" value="checked"></c:set>	         			
														</c:if>												
											        			         			
													</c:forEach>
															<label class="checkbox">		
																<input type="checkbox" name="sapRaceId[]" id="sapRaceId_${race.raceId}" ${checked} value="${race.raceId}">${race.raceName}</br>
															</label>
															<c:set var="checked" value=""></c:set>	         			
													</c:if>	
												</c:forEach>				
												
											</div>
							</td>
							
							<td width="20%" valign="top" style="padding-top: 5px;">
								<div class="span4 mt10" style="margin-top:0px;">
									<label><B><spring:message code="lblGend"/></B></label>
											<c:forEach items="${listGenderMasters}" var="gender">
												<c:if test="${gender.genderId eq teacherpersonalinfo.genderId.genderId}">		        			
								         				<c:set var="checked" value="checked"></c:set>	         			
								        		</c:if>	
								        		<label class="radio">
													<input type="radio" name="sapGenderId[]" id="sapGenderId_${gender.genderId}" ${checked} value="${gender.genderId}">${gender.genderName}</br>
												</label>
												<c:set var="checked" value=""></c:set>	
										</c:forEach>	
								 </div>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<table>
									<tr>
										<td  style="width:80px;"><spring:message code="headNot"/></td>
										<td><div id="notesmsgEEOC"><textarea rows="5" class="form-control" cols="25" name="notes" id="notes" maxlength="1000"></textarea></div></td>
									</tr>
									<tr><td colspan="2">&nbsp;</td></tr>
									<tr>
									<td><spring:message code="tdHeadingAF"/></td>
						    		<td>
						    		<form id="frmoEEOCUpload" enctype='multipart/form-data' method='post' target='uploadFrame' action='onrEEOCUploadServlet.do'>
								    	<input type="hidden" id="jsiFileNameEEOC" name="jsiFileNameEEOC" value="" />
								    	<input type="hidden" id="teacherIdEEOC" name="teacherIdEEOC" />
			    						<input type="hidden" id="eligibilityIdEEOC" name="eligibilityIdEEOC" />
			    						<input type="hidden" id="jobIdEEOC" name="jobIdEEOC" />
								    	<input id="eligibilyFileEEOC" name="eligibilyFileEEOC" type="file" width="20px;" style="">
							    	</form>
						    		</td>
						    	</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td colspan="4">
								<div style="margin-top:10px;">
									<a href="javascript:void(0);" onclick="saveEEOCData()"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="javascript:void(0);" onclick="cancelDiv()"><spring:message code="btnClr"/></a>
								</div>
							</td>
						</tr>
						</table>				
				</div>
				
					
				</div>
			   </div>
			   <!-- <div class="" id="divFrsCode"></div>  -->
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();"><spring:message code="btnClose"/></button>
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!-- EEOC Notes DIV  -->
<div  class="modal hide onrNotesShowEEOC"  id="onrNotesShowEEOC"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC">Teachermatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divOnrNotesEEOC">

					</div>
				</div>
				
		 	</div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDivEEOC();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!--  Communication Div For User Profile -->
<div  class="modal hide"  id="myModalCommunications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 700px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForSave();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headCom"/></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divCommTxt">
			</div> 
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick='showCommunicationsDivForSave();'><spring:message code="btnClose"/></button> 		
 	</div>
   </div>
 </div>
</div>

<!-- candidate profile div -->
<div class="modal hide custom-div-border1" id="cgTeacherDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="cgTeacherDivBody">


</div>
</div>

<div  class="modal hide pdfDivBorder"  id="modalDownloadPDR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headPDRep"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		
		  <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   <iframe id="ifrmPDR" width="100%" height="480px" src=""></iframe>		  
		  </div>   

		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
   </div>
  </div>
 </div>
</div>

<iframe src="" id="ifrmRef" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmTrans" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmCert" width="100%" height="480px" style="display: none;"></iframe>

<div  class="modal hide pdfDivBorder"  id="modalDownloadsCommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-contenat" style="background-color: #ffffff;">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabelText"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTransCommon" width="100%" height="100%" scrolling="auto">
				 </iframe>  
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
 	</div>
</div>

<input type="hidden" name="eleRefId" id="eleRefId">
<div  class="modal hide"  id="myModalReferenceNoteView"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog' style="width: 520px;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headRefrNot"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" id="divRefNotesInner">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
  </div>
 </div>
</div>

<!-- Ref Note -->
<div class="modal hide"  id="myModalReferenceNotesEditor"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog-for-cgreference">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot"/></h3>
	</div>

	<div class="modal-body"  style="margin:10px;">	
		<div class="row" id="divNotesRef" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID_ref' name='uploadNoteFrame_ref' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload_ref' enctype='multipart/form-data' method='post' target='uploadNoteFrame_ref'  class="form-inline" onsubmit="return saveReferenceNotes();" action='referenceNoteUploadServlet.do' accept-charset="UTF-8">
		
			<input type="hidden" name="eleRefId" id="eleRefId">
			<input type="hidden" id="teacherIdForNote_ref" name="teacherIdForNote_ref">
			
			<div class="row mt10">
				<div class='span10 divErrorMsg' id='errordivNotes_ref' style="display: block;"></div>
				<div class="span10" >
			    	<label><strong><spring:message code="lblEtrNot"/><span class="required">*</span></strong></label>
			    	<div class="span10" id='divTxtNode_ref' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea readonly id="txtNotes_ref" name="txtNotes_ref" class="span10" rows="4"   ></textarea>
			    	</div>  
			    	<div id='fileRefNotes' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'><spring:message code="lnkAttachFile"/></a>
		        	</div>      	
				</div>						
			</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'><spring:message code="btnClr"/></button>&nbsp; 
 		<span id="spnBtnSave" style="display:inner"><button class="btn btn-primary"  onclick="saveReferenceNotes()"><spring:message code="btnSave"/> <i class="icon"></i></button>&nbsp;</span>
 	</div>
  </div>
 </div>
</div>
<!-- End Ref Note -->
<input type="hidden"  name="checkboxshowHideFlag" id="checkboxshowHideFlag"/>
<input type="hidden" name="teachersharedId" id="teachersharedId"/>
<input type="hidden" name="pageFlag" id="pageFlag" value="0"/>
<input type="hidden" name="userFPD" id="userFPD"/>
<input type="hidden" name="userFPS" id="userFPS"/>
<input type="hidden" name="userCPD" id="userCPD"/>
<input type="hidden" name="userCPS" id="userCPS"/>
<input type="hidden" name="folderId" id="folderId"/>
<input type="hidden" name="teacherIdForprofileGrid" id="teacherIdForprofileGrid"/>
<input type="hidden" name="teacherIdForprofileGridVisitLocation" id="teacherIdForprofileGridVisitLocation" value="CG View" />
<input type="hidden" name="hideIcons" id="hideIcons" value="-1"/>
<input type="hidden" name="teacherDetailId" id="teacherDetailId">
<input type="hidden" name="phoneType" id=phoneType value="0"/>
<input type="hidden" name="msgType" id=msgType value="0"/>
<input type="hidden" name="jobForTeacherGId" id="jobForTeacherGId" value=""/>
<input type="hidden" name="commDivFlag" id="commDivFlag" value=""/>
<input type="hidden" name="commDivFlagPnrCheck" id="commDivFlagPnrCheck" value="1"/>

<div class="modal hide"  id="saveToFolderDiv"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog" style="width:900px;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="defaultTeacherGrid()";>x</button>
		<h3 id="myModalLabel"><spring:message code="headSaveCandidate"/></h3>
	</div>
	<div class="modal-body" style="max-height: 500px;overflow-y: scroll;"> 	 		
		<div class="control-group">
		<div  style="border: 0px solid green;width:220px;height:450px;float: left;" class="span5">	
		<div class="span5" id="tree_menu" style=" padding: 8px 0px 10px 2px; " >
			<a data-original-title='Create' rel='tooltip' id='createIcon'><span id="btnAddCode" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Rename' rel='tooltip' id='renameIcon'><span id="renameFolder" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Cut' rel='tooltip' id='cutIcon'><span id="cutFolder" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Copy' rel='tooltip' id='copyIcon'><span id="copyFolder" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Paste' rel='tooltip' id='pasteIcon'><span id="pasteFolder" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Delete' rel='tooltip' id='deleteIcon'><span id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span></a>
		</div>
		<iframe id="iframeSaveCandidate"  src="tree.do" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>
	</div>
	<%-- ======================================== Right Div ========================================================================= --%>			
		<div class="span10" style="padding-top:39px; border: 0px solid green;float: left;">
				<input type="hidden" id="savecandidatearray" name="savecandidatearray">
				<input type="hidden" id="txtoverrideFolderId" name="txtoverrideFolderId">
				<input type="hidden" id="teachetIdFromPoPUp" name="teachetIdFromPoPUp" value="">
				<input type="hidden" id="teacherIdForHover" name="teacherIdForHover" value="">
				
				<input type="hidden" id="txtflagpopover" name="txtflagpopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
				<div id="savedCandidateGrid">
				
				</div>
		</div>
		<div style="clear: both"></div>	
		</div>
		
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="saveCandidateToFolderByUser()" ><spring:message code="btnSave"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="defaultTeacherGrid()";><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="duplicatCandidate" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgCandidateSvav/Cancel"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="saveWithDuplicateRecord()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
   </div>
  </div>
</div>

<!--  Share Folder -->

<div class="modal hide"  id="shareDiv" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='defaultSaveDiv()'>x</button>
		<h3 id="myModalLabel"><spring:message code="headShareCandidate"/></h3>
		<input type="hidden" id="entityType" name="entityType" value="${entityType}">
		<input type="hidden" id="savecandiadetidarray" name="savecandiadetidarray"/>
		<input type="hidden" id="JFTteachetIdFromSharePoPUp" name="JFTteachetIdFromSharePoPUp">
		<input type="hidden" id="txtflagSharepopover" name="txtflagSharepopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">			
			<div class="">
					<div class="row">
					    <div class="col-md-10">
						<div class='divErrorMsg' id='errorinvalidschooldiv' ></div>
					    </div>
					      <div class="col-sm-4 col-md-4">
					       <label><spring:message code="lblDistrictName"/></label> </br>
				            <c:if test="${DistrictName!=null}">
				             	${DistrictName}
				             	<input type="hidden" id="districtId" value="${DistrictId}"/>
				             	<input type="hidden" id="districtName" value="${DistrictName}" name="districtName"/>
				             </c:if>				            
			              <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
						
					      </div>
					      
					      <div class="col-sm-6 col-md-6">
					        <label><spring:message code="lblSchoolName"/></label>
				          	<c:if test="${SchoolName==null}">
				           	<input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
							    <input type="hidden" id="schoolId" value="0"/>
							</c:if>
							  <c:if test="${SchoolName!=null}">
				             	<%-- ${SchoolName}--%>	
				             	<input type="text" id="schoolName" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" value="${SchoolName}"	/>
				             	<input type="hidden" id="schoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolName" value="${SchoolName}"/>
				             </c:if>
							 <div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
				        
					      </div>
					      <div class="col-md-2 top25">
					        <label>&nbsp;</label>
					        <input type="button" id="" name="" value="<spring:message code="btnGo"/>" onclick="searchUserthroughPopUp(1)" class="btn  btn-primary" >
					      </div>
  					</div>
				</div>
		
		
			<div id="divShareCandidateToUserGrid" style="border: 0px solid green;" class="mt30">
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="shareCandidatethroughPopUp()" ><spring:message code="btnShare"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='defaultSaveDiv()'><spring:message code="btnClr"/></button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="shareConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="saveAndShareConfirmDiv" class="">
		    	<spring:message code="msgSucceSharedCand"/>     	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
  </div>
 </div>
</div>

<div class="modal hide"  id="deleteShareCandidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeletetStrCondidate"/>
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteCandidate()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
   </div>
  </div>
</div>

<!-- Attachment Show  -->
<div  class="modal hide pnrAttachment"  id="pnrAttachment"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11"><spring:message code="hdAttachment"/></h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideAttachmentDiv();"><spring:message code="btnClose"/></button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>


<!--  Message Div -->
<div class="modal hide"  id="myModalMessage"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCandidate"/></h3>
	</div>
	<iframe id='uploadMessageFrameID' name='uploadMessageFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUpload' enctype='multipart/form-data' method='post' target='uploadMessageFrame'  class="form-inline" onsubmit="return validateMessage();" action='messageCGUploadServlet.do' accept-charset="UTF-8">
	
	<div class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessages">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		
		<%---------- Gagan :District wise Dynamic Template [Start]  --%>
			<div id="templateDiv" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong>Template</strong></label>
		        	<br/>
		        	<select class="form-control" id="districttemplate" onchange="setTemplate()">
		        		<option value="0"><spring:message code="sltTemplate"/></option>
		        	</select>
		         </div>	
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary hide top26" id="btnChangetemplatelink" onclick="return getTemplate();"><spring:message code="btnAplyTemplate"/></button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplate" name="confirmFlagforChangeTemplate" value="0">
			
		    	</div>
		    </div>
		<%---------- Gagan :District wise Dynamic Template [END]  --%>
		
		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong><spring:message code="lblTo"/><br/></strong><span id="emailDiv" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend" style="width: 612px;">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/>
 				</label>
	        	<textarea rows="5" class="form-control" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessage" name="teacherIdForMessage" value="">
    		<input type="hidden"  name="messageDateTime" id="messageDateTime" value="${dateTime}"/>
	    	<div id='fileMessages' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileType();'>
        		<img src='images/attach.png'/></a> 
        		<a href='javascript:void(0);' onclick='addMessageFileType();'><spring:message code="lnkAttachFile"/></a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<c:set var="chkSendDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSendDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSendDisp" value="inline" />
			</c:when>
			<c:otherwise>
				<c:set var="chkSendDisp" value="none" />
			</c:otherwise>
	</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'><spring:message code="btnClr"/></button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessage()" style="display: ${chkSendDisp}"><spring:message code="btnSend"/></button>&nbsp;
 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="myModalMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'><spring:message code="msgYuMsgIsfullySentToCandidate"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()"><spring:message code="btnOk"/></button>
 	</div>
</div> 	
</div>
</div>


<!--  Add Phone -->
<div  class="modal hide"   id="myModalPhone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   data-backdrop="static">
	<div class='modal-dialog-for-teachercontented'>
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showCommunicationsForPhone();">x</button>
		<h3 id="myModalLabel"><spring:message code="headPhone"/></h3>
	</div>	
	<div class="modal-body">			
			<div id="divPhoneGrid"></div>
			<iframe id='uploadPhoneFrameID' name='uploadPhoneFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
			</iframe>
			<form id='frmPhoneUpload' enctype='multipart/form-data' method='post' target='uploadPhoneFrame'  class="form-inline" onsubmit="return savePhone();" action='phoneUploadServlet.do' accept-charset="UTF-8">
			<div class="mt10">
				<div class="span6 hide" id='calldetrailsdiv'>
					<div class='divErrorMsg' id='errordivPhone' style="display: block;margin-bottom: 5px;"></div>
					<label><strong><spring:message code="lblEtrCallDetail"/><span class="required">*</span></strong></label>
			    	<div class="span6" id='divTxtPhone' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea readonly id="divTxtPhone" name="divTxtPhone" class="span6" rows="4"></textarea>
			    	</div> 
			    	<input type="hidden" id="teacherIdForPhone" name="teacherIdForPhone" value="">
		    		<input type="hidden"  name="phoneDateTime" id="phoneDateTime" value="${dateTime}"/>
			    	<div id='filePhones' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addPhoneFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addPhoneFileType();'><spring:message code="lnkAttachFile"/></a>
		        	</div>   
		    	</div>     	
			</div>
			</form>
 	</div>
 	<c:set var="chkSavePhone" value="inline"/>
		<c:choose>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSavePhone" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSavePhone" value="none" />
			</c:otherwise>
		</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="showCommunicationsForPhone();"><spring:message code="btnClr"/></button>
 		<button class="btn btn-primary hide" style="display: ${chkSavePhone}" id='calldetrailsbtn' onclick="savePhone()" ><spring:message code="btnSave"/>&nbsp;</button>
 	</div>
	</div> 	
  </div> 	
</div>
<!-- End Phone -->

<!--  Starts Notes DIV -->
<input type="hidden" id="noteId" name="noteId" value=""/>
<div class="modal hide" id="myModalNotes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-dialog" style="width:935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot"/></h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID' name='uploadNoteFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload' enctype='multipart/form-data' method='post' target='uploadNoteFrame'  class="form-inline" onsubmit="return saveNotes();" action='noteUploadServlet.do' accept-charset="UTF-8">
			
		<div class="mt10">
			<div class='span10 divErrorMsg' id='errordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong><spring:message code="lblEtrNot"/><span class="required">*</span></strong></label>
		    	<div class="span10" id='divTxtNode' style="padding-left: 0px; margin-left: 0px;" >
		    		<textarea style="width: 100%" readonly id="txtNotes" name="txtNotes" class="span10" rows="4"></textarea>
		    	</div>  
		    	<input type="hidden" id="teacherIdForNote" name="teacherIdForNote" value="">
		    	<input type="hidden"  name="noteDateTime" id="noteDateTime" value="${dateTime}"/>
		    	<div id='fileNotes' style="padding-top:10px;">
	        		<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'><spring:message code="lnkAttachFile"/></a>
	        	</div>      	
			</div>						
		</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<c:set var="chkSaveDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSaveDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSaveDisp" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSaveDisp" value="none" />
			</c:otherwise>
		</c:choose>
 		<!-- <span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()"><spring:message code="btnClr"/></a></span> -->
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'><spring:message code="btnClr"/></button>&nbsp; 
 		<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button class="btn btn-primary"  onclick="saveNotes()"><spring:message code="btnSave"/> <i class="icon"></i></button>&nbsp;</span>
 	</div>
   </div>
</div>
</div>


 <!-- PDF -->
 <style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>
<div  class="modal hide pdfDivBorder"  id="modalDownloadOfferReady" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headAppPNRDashbord"/> </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->  








<!--  End Notes DIV -->
<input type="hidden" id="teacherIdForHover" name="teacherIdForHover" value="">

<div  class="modal hide" id="myModalProfileVisitHistoryShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:100px;" id="mydiv">
		<div class='modal-content'>
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
			<h3 id="myModalLabel"><spring:message code="msgWhAdminCanHiCandFrThJoOrdAppPo"/></h3>
		</div>
		<div class="modal-body" style="max-height: 400px;overflow-y:scroll;padding-right: 18px;">		
			<div class="control-group">
				<div id="divteacherprofilevisithistory" class="">
			    		        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
	 	</div>
		</div>
	</div>
</div>
<div class="modal hide"  id="saveDataLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDataSaved"/> 
		</div>
 	</div>
 	<div class="modal-footer">
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide" id="myModalUndoStatusConfirm" >
<input type="hidden" name="eligibilityIdSet" id="eligibilityIdSet">
<input type="hidden" name="onrStatusType" id="onrStatusType"/>
	<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='hideDivUndoStatus()'>x</button>
				<h3><span id="myModalStatusUndoLabel"></span></h3>
			</div>
			<div  class="modal-body">
				<div class="" id="referenceRecord">
					<spring:message code="cngMsgUndocurrentstatus"/><BR/>
				</div>
		 	</div>

		 	<div class="modal-footer">
		 		<button class="btn btn-large btn-primary" type="button" onclick="undoStatus();"><strong><spring:message code="btnConti"/> <i class="icon"></i></strong></button>
		 		<button type="button" class="btn"  onclick='hideDivUndoStatus()'><spring:message code="btnClr"/></button>
		 	</div>
	    </div>
	</div>
</div>

<div class="modal hide custom-div-border1" id="draggableDivMaster"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="draggableDiv"></div>
</div>
<input type="hidden" id="pageNumberSet" name="pageNumberSet" value="1" />
<!-- candidate profile div -->
<!--
<div class="modal hide custom-div-border1" id="cgTeacherDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="cgTeacherDivBody"></div>
</div>
-->

<!--  Message DIV  -->
<script>
function applyScrollOnPhone()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblPhones').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 110,
        width: 615,
        minWidth: null,
        minWidthAuto: false,
        colratio:[400,100,112], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

$(document).ready(function(){

	/* ---------------------- Create Folder Method Start here --------------- */
	$("#btnAddCode").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.createUserFolder();
	});
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
	$("#renameFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.renameFolder();
	});
/*-----------------  Cut Copy Paste Folder ------------------------*/
	$("#cutFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.cutFolder();	
	});

	$("#copyFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.copyFolder();		
	});

	$("#pasteFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.pasteFolder();	
	});
		
	$("#deletFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.deletFolder();
	});	
	 $('#divTxtPhone').find(".jqte");
}) 
</script>
<script type="text/javascript">//<![CDATA[
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("examStartDateFp", "examStartDateFp", "%m-%d-%Y");
     cal.manageFields("examStartDateDt", "examStartDateDt", "%m-%d-%Y");
     cal.manageFields("examClearDateFp", "examClearDateFp", "%m-%d-%Y");

     cal.manageFields("examStartDateFp1", "examStartDateFp1", "%m-%d-%Y");
     cal.manageFields("examStartDateFp2", "examStartDateFp2", "%m-%d-%Y");
     cal.manageFields("failedDateFp", "failedDateFp", "%m-%d-%Y");
     cal.manageFields("reprintDateFp", "reprintDateFp", "%m-%d-%Y");
     cal.manageFields("reprintDateFp", "reprintDateFp", "%m-%d-%Y");

     $(document).ready(function(){
		$('#notesmsg').find(".jqte_editor").width(700);
		$('#notesmsgFp').find(".jqte_editor").width(700);
		$('#notesmsgDt').find(".jqte_editor").width(700);
		$('#notesTrans').find(".jqte_editor").width(700);
		$('#notesmsgEEOC').find(".jqte_editor").width(700);
	});
    //]]>
	$("#ssn").focus();		
</script>

<script type="text/javascript">
 $('#chartId').tooltip();
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
  
$(document).mouseup(function (e)
{
    var container = $(".DynarchCalendar-topCont");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
}); 
//getYesterdayApplicants();
//getTotalsOfDateApplicants();
</script>

<script>
function fileContainsVirusDiv(msg)
{
	$('.onr_popup').modal('hide');
	$('.onr_popup_fp').modal('hide');
	$('.onr_popup_dt').modal('hide');
	$('.onr_popup_trans').modal('hide');
	$('.onr_popup_frsCode').modal('hide');
	
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
<c:if test="${entityType ne 5 && entityType ne 6}">
window.onload = displayPNRGrid();
</c:if>
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal""><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<!--  Display  Video Play Div -->
<div  class="modal hide" id="videovDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%; " >
		<div class="modal-content">		
			<div class="modal-header">
			 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="">x</button>
				<h3 id="myModalLabel"><spring:message code="lblVid"/></h3>
			</div>
			<div class="modal-body">		
				<div class="control-group">
						<div style="display:none;" id="loadingDivWaitVideo">
						     <table  align="center" >						 		
						 		<tr><td align="center"><img src="images/loadingAnimation.gif"/></td></tr>
							</table>
						</div>						
						<div id="interVideoDiv"></div>
				</div>
			</div>
			<div class="modal-footer">	 			
	 		<button class="btn" id="closeBtn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button>
 			</div>			
		</div> 
	</div>
</div>

<!------------------------------------------- Start :: Qualification Questions ------------------------------------------->
<div class="modal hide" id="qualificationDiv"
	style="border: 0px solid blue;" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog" style="width: 520px;">
		<div class='modal-content'>
			<div class="modal-header" id="qualificationDivHeader">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true" onclick=''>
					x
				</button>
				<h3>
				<spring:message code="headQualif"/>
				</h3>
			</div>

			<div class="modal-body"
				style="max-height: 400px; padding-top: 0px; overflow-y: scroll;">
				<input type="hidden" id="teacherIdforPrint" name="teacherIdforPrint" />
				<input type="hidden" id="districtIdforPrint" name="districtIdforPrint" />
				<input type="hidden" id="headQuarterIdforPrint" name="headQuarterIdforPrint" />
				<input type="hidden" id="branchIdforPrint" name="branchIdforPrint" />
				<div class='divErrorMsg' id='errorQStatus' style="display: block;"></div>
				<div id="qualificationDivBody"></div>
			</div>
			<div class="modal-footer">
				<table width=470 border=0>
					<tr>
						<td width=100 nowrap align=left>
<!--							<span id="qStatusSave"><button-->
<!--									class="btn  btn-large btn-orange"-->
<!--									onclick='saveQualificationNoteForTP(0)'>-->
<!--									<spring:message code="btnIn-Progress"/>-->
<!--								</button>&nbsp;&nbsp;</span>-->
						</td>

						<td width=200 nowrap>
							<span id="qStatusFinalize"><button
									class="btn  btn-large btn-primary"
									onclick='saveQualificationNoteForONB(1)'>
									<spring:message code="btnFinalize"/>
									<i class="iconlock"></i>
								</button>&nbsp;&nbsp;</span>
								<button class="btn btn-large btn-primary" data-dismiss="modal"
								aria-hidden="true" onclick="printQualificationDivForONB();">
								<spring:message code="btnP"/>
							</button>
							
							<button class="btn btn-large" data-dismiss="modal"
								aria-hidden="true" onclick='cancelQualificationIssuse();'>
								<spring:message code="btnClr"/>
							</button>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();"><spring:message code="btnClose"/><spring:message code="btnX"/></button>
	   <h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			 <spring:message code="msgPlzPopAreAlloued"/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
<input type="hidden" id="sadistrictId" value="${DistrictId}"/>
<!------------------------------------------- End :: Qualification Questions ------------------------------------------->
<jsp:include page="selfserviceapplicantprofilecommon.jsp"></jsp:include>