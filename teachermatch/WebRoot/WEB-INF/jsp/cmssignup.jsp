<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script src="js/tmhome.js"></script>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">	
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<link rel="stylesheet" type="text/css" href="css/slider.css" />  
<script>
function applyScrollOnTblEvent()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#eventTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 770,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,200,420], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}
applyScrollOnTblEvent();
</script>

<script>
$(document).ready(function(){

$('.popover').hide();


$("#password").focus(function(){
	$('.popover').show();

});

$("#firstName").focus(function(){ $('.popover').hide(); });
$("#lastName").focus(function(){ $('.popover').hide(); });
$("#emailAddress").focus(function(){ $('.popover').hide(); });
$("#sumOfCaptchaText").focus(function(){ $('.popover').hide(); });
/*$("#formBody").click(function(){ $('.popover').hide(); });*/

$( "#seemore" ).click(function() {
	//$("#password").focus();
	$('.password-guidelines').show();
	$('#seemore').hide();
	$('#seeless').show();
});

$( "#seeless" ).click(function() {
	//$("#password").focus();
	//$('.popover').show();
	$('.password-guidelines').hide();
	$('#seemore').show();
	$('#seeless').hide();
});
});

</script>
 <script type='text/javascript'>
    var captchaContainer = null;
    var loadCaptcha = function() {
      captchaContainer = grecaptcha.render('captcha_container', {
        'sitekey' : '6LeMmwATAAAAAGB1iwuORAfQ8z9ZvJAT2Lhad4AT',
        'callback' : function(response) {
          if(response !=null)
          {
          	document.getElementById('captcharesponse').value="success";
          }
          else
          {
          	document.getElementById('captcharesponse').value="failed";
          }
        }
      });
    };
    
    </script>
<style>
.popover-inner {
padding: 3px;
overflow: hidden;
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0,0,0,0.3);
color:black;
}

/*.popover.left .arrow {
top: 50%;
right: 0;
margin-top: -5px;
border-top: 5px solid transparent;
border-bottom: 5px solid transparent;
border-left: 5px solid #000;
}
*/

.popover.left .arrow
{
	width: 20px;
	height: 20px;
	border: none;
	transform: rotate(45deg);
	position: absolute;
	top: 98px;
	left: 263px;
	background-color: white;
	box-shadow: 0 9px 0 0px white, -9px 0 0 0px white, 1px -1px 1px #818181;
}

.popover .arrow {
position: absolute;
width: 0;
height: 0;
}
element.style {
top: 231.5px;
left: 155px;
display: block;
}
.popover.left {
margin-left: -5px;
}
.fade.in {
opacity: 1;
}
.popover {
position: absolute;
top: 0;
left: 0;
z-index: 1010;
display: none;
}
.fade {
opacity: 0;
-webkit-transition: opacity 0.15s linear;
-moz-transition: opacity 0.15s linear;
-ms-transition: opacity 0.15s linear;
-o-transition: opacity 0.15s linear;
transition: opacity 0.15s linear;
}
</style>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="${logo}">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 21px;"> ${districtHQBRName}</div>	
         	<div style="font-size: 10px;"> ${address}</div>
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div><br/>
</div>
<div class="row">
	<div class="col-sm-6 col-md-6">
			<div class="row top10">
<div class="col-sm-12 col-md-12">
 <div class="subheadinglogin">	<img src="${eventlogo}" height="80" width="80"></div>
</div>
</div>
		
		<div style="height: 390px; padding-top:10px; overflow: scroll;">
			 ${eventDetails.description}
		</div>
	</div>
	
	<div class="col-sm-6 col-md-6">
	<div class="row top10">
<div class="col-sm-12 col-md-12">
 <div class="subheadinglogin "><spring:message code="msgPlzRegisterBelow" /></div>
</div>
</div>
<form:form commandName="teacherDetail" onsubmit="return validateCMSSignUp()" >

					
						 <div class="row">
   								<div class="col-sm-8 col-md-8">
                                      <div  id='divServerError' class='divErrorMsg' style="display: block;">${msgError}</div>
						      				<div class='divErrorMsg' id='errordiv' style="display: block;"></div>						      				
											<div  id='divFirstName' class='divErrorMsg'><spring:message code="msgPlzEtrFname" /></div>
											<div  id='divLastName' class='divErrorMsg'><spring:message code="msgPlzEtrLname" /></div>
											<div  id='divEmailAddress' class='divErrorMsg'><spring:message code="msgPlzEtrEmail" /></div>
											<div  id='divValidEmail' class='divErrorMsg'><spring:message code="msgPlzEtrValidEmail" /></div>
											<div  id='divEmailAlreadyExist' class='divErrorMsg'><spring:message code="msgIfMemberAlreadyRegPlzProvideOtherEmail" /> 
											</div>
											<div  id='divPassword' class='divErrorMsg'><spring:message code="msgPlgEtrPass" /></div>
											<div  id='divPwdLength' class='divErrorMsg'><spring:message code="msgPassCompleteFormat" />
											</div>
											<div  id='divSumOfCaptchaText' class='divErrorMsg'><spring:message code="msgPleaseAddBothNumbers" /></div>
											<div  id='divReenterCaptcha' class='divErrorMsg'><spring:message code="msgSumBothNumbersWrong" /></div>
						                	
                               </div>
						</div>
						<input type="hidden"  name="eventId"  id="eventId" value="${eventDetails.eventId}"/>
					   <div class="row top15">                                  
                                   <div class="col-sm-6 col-md-6">
                                        <label><spring:message code="lblFname" /><span class="required">*</span></label>
                                       <form:input path="firstName" cssClass="form-control" maxlength="50"/>
                                   </div>
                                   
                                   <div class="col-sm-6 col-md-6">
                                        <label><spring:message code="lblLname" /><span class="required">*</span></label>
                                        <form:input path="lastName" cssClass="form-control" maxlength="50"/>
                                   </div>
                       </div>                                   
                      <div class="row">             
                                   <div class="col-sm-12 col-md-12">
                                        <label><spring:message code="lblEmail" /><span class="required">*</span></label>
                                       	<form:input path="emailAddress" cssClass="form-control" maxlength="75" value="${emailId}"/>
                                   </div>
                      </div>                                   
                      <div class="row">                
                                   <div class="col-sm-12 col-md-12">
                                        <label><spring:message code="lblPass" /><span class="required">*</span></label>
                                         <form:password cssClass="form-control" path="password" id="password" maxlength="12"	onkeyup="return PasswordImage()"/></div>
                      </div>
                                   
                         <div class="row">            
                                   
                                   <!-- *********div for passowrd****** -->
                                   <div  class="col-sm-8 col-md-8 top10" style="position: relative;z-index: 10">
                      		<div class="popover fade left in" style="left: -275px; top: -123.5px; display: block;">
                      			<div class="arrow"></div>
                      			<div class="popover-inner">
                      				<div class="popover-title" style="background-color: #007AB4;color:white;"><spring:message code="msgPassReq" /></div>
                      				<div class="popover-content"></div>
                      				<div id="password-rules">
                            			<ol >
                            				<li><spring:message code="listAtLeast8Chr" /></li>
                            				<li><spring:message code="liIncludeAtLeast2-3Elt" />
                            					<ul type="disc">
	                            					<li><spring:message code="liOneChar" /></li>
													<li><spring:message code="liOneDigit" /></li>
													<li><spring:message code="liOneSplSymbol" /></li>
												</ul>
											</li>
											<li><spring:message code="liAtMost12Char" /></li>
										</ol>
									</div>
									
									<div>
									<a href="javascript:void(0);" role="see-more" id="seemore" style="padding-left: 12px;"><spring:message code="lnkMoreInfo" /></a>
									<a href="javascript:void(0);" role="see-more" id="seeless" style="color: red; padding-left: 12px; display: none;"><spring:message code="lnkLessInfo" /></a>
									</div>
									
									<div class="password-guidelines hide" style="padding: 12px 15px; text-align: justify;">
										<span><b><spring:message code="msgPassQuid" /></b></span>
										<ul>
											<li><spring:message code="liEachTimeMakeHardPass" /></li>
											<li><spring:message code="liUseRepeatingOrSequentalCharInPass" /></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						  <!-- *********End of div for passowrd****** -->
						  <div class="col-sm-12 col-md-12 top10">
                                    <label style="margin-right: 40%;">
										<strong><spring:message code="lblpassStrength" />
										</strong>
									</label>									
                                   </div>
                                   
                                   <div class="col-sm-2 col-md-2 top10">
                                   
                                   
                                   <table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td colspan="2" align="left" valign="top" nowrap>
											<table cellpadding="0" cellspacing="0"><tr>
												<td>
													<div style="height: 12px; width: 14px;  float: left;  border: thin solid #bcb4a4;">							
														<div id="d4" style="height: 12px; width: 14px; float: left; display: none;">
															&nbsp;
														</div>					
													</div>
												</td>
												<td style="padding-left:10px;">
													<div style="height: 12px; width: 14px;   float: left; border: thin solid #bcb4a4; margin-left: 5%;">
														<div id="d5" style="height: 12px; width: 14px; float: left; display: none;">
															&nbsp;
														</div>
													</div>
												</td>
												<td style="padding-left:10px;">
												<div style="height: 12px; width: 14px; float: left; border:  thin solid #bcb4a4; margin-left: 5%;">
													<div id="d6"					style="height: 12px; width: 14px; float: left; background-color: lightgreen; display: none;">
														&nbsp;
													</div>
												</div>
												</td>
												<td style="padding-left:10px;">
												<div
													style="height: 12px; width: 14px; float: left; border:  thin solid #bcb4a4; margin-left: 5%;">
													<div id="d7" 
														style="height: 12px; width: 14px; float: left; background-color: lightgreen; display: none;">
														&nbsp;
													</div>
												</div>
												
												</td>
												<td><table cellpadding="0" cellspacing="0"><tr><td>
													<div id="d8"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span ><spring:message code="spWeak" /></span>
													</div></td>
												<td>
													<div id="d9"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span ><spring:message code="spNormal" /></span>
													</div></td>
												<td>
													<div id="d10"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span ><spring:message code="spStrong" /></span>
													</div>
											</td>
										
											</tr> </table>
										</td>
									
										</tr></table>
										</td>
									    </tr>
								      </table>
							        </div>
							       </div> 	
								       
					  
					   <div class="col-sm-12 col-md-12 top10">
							        <label style="margin-left:-2%;" >
							        <strong>
							        <spring:message code="lblPlzStChBox" />
							        </strong>
							        <span class="required">*</span></label>
							        </div>        
					  <div class="row  top5">        
							      <div class="col-sm-12 col-md-12 top-s110">
							        <div id="captcha_container"></div>							         							        
							        </div>
							        
				      </div>          
					
				      
				       
					                                    
                      <div class="row">	        
							        <div class="col-sm-12 col-md-12 top20" >
							          <button class="btn btn-primary" type="submit"><spring:message code="btnSign" /> <i class="icon"></i></button>
							        </div>
							        
							     
                              
                      </div>
                      </div> 
                 
                      
        
<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>
</form:form>
	</div>
</div>
<div  style="margin: auto; width: 75%;"  >

<input type="hidden" name="captcharesponse" id="captcharesponse"/> 
</div>
<br/><br/>
<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>	
</div>
<script>   
	document.getElementById("firstName").focus();
</script>