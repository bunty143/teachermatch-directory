<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />	
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script src="jquery/jquery-ui.custom.js" type="text/javascript"></script>
<script src="jquery/jquery.cookie.js" type="text/javascript"></script>
<link href="jquery/skin/ui.dynatree.css" rel="stylesheet" type="text/css">
<script src="jquery/jquery.dynatree.js" type="text/javascript"></script>
<!-- jquery.contextmenu,  A Beautiful Site (http://abeautifulsite.net/) -->
<script src="contextmenu/jquery.contextMenu-custom.js" type="text/javascript"></script>
<link href="contextmenu/jquery.contextMenu.css" rel="stylesheet" type="text/css" >

<!-- Start_Exclude: This block is not part of the sample code -->
<link href="contextmenu/prettify.css" rel="stylesheet">
<script src="contextmenu/prettify.js" type="text/javascript"></script>
<link href="contextmenu/sample.css" rel="stylesheet" type="text/css">
<script src="contextmenu/sample.js" type="text/javascript"></script>
<!-- End_Exclude -->

<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js?ver=${resourceMap['CandidateReportAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>

<script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resourceMap['ManageStatusAjax.Ajax']}"></script>
<script type="text/javascript" src="js/managestatus.js?ver=${resourceMap['js/managestatus.js']}"></script>
<script type="text/javascript" src="dwr/interface/BranchesAjax.js?ver=${resourceMap['BranchesAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HeadQuarterBranchServiceAjax.js?ver=${resourceMap['HeadQuarterBranchServiceAjax.Ajax']}"></script>
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>
<script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
	<style>
	.hide
	{
		display: none;
	}
	 .icon-folder-open
	{
		 font-size: 1.5em;
		color:#007AB4;
	}
	.icon-copy
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-edit
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-cut
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.icon-paste
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	
	.icon-remove-sign
	{
		font-size: 1.5em;
		color:#007AB4;
	}
	.marginleft20
	{
		margin-left:-20px;
	}
	
	.scrollit {
    	max-height:100px;
    	overflow-y:auto;
    	overflow-x:hidden;
	}
	
	/*<!--add by Ram nath	-->*/
	#divGridQuestionList .table-bordered .net-widget-footer.net-corner-bottom{
	width: 100% !important;
	}	
	/*<!--end by Ram nath	-->*/
	
	</style>
	<%try{ %>
	<input type="hidden" id="displayTypeId" value="<%=session.getAttribute("displayType").toString()%>"/>
	<%}catch(Exception e){ %>
	<input type="hidden" id="displayTypeId" value="0"/>
	<%} %>
	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage Status</div>	
         </div>
       	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>
	
	<input type="hidden" id="renameEnterFlag" value="0" />
	<input type="hidden" id="distID" value="${DistrictId}" />
	<input type="hidden" id="distName" value="${DistrictName}" />
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<div class='divErrorMsg' id='errordiv' style="padding-left: 20px; padding-top: 10px;"></div>
		</div>
	</div>
	
	<c:if test="${entityType eq 5}">
	<div class="row mt10">  
	   <div class="col-sm-5 col-md-5">  	
<label>Head Quarter Name</label><br/>
	       	 ${headQuarterName}
		</div>
		 <input type="hidden" id="headQuarterId" name="headQuarterId" value="${headQuarterId}" />
		
 		
		<div class="col-sm-5 col-md-5">
           		<label id="captionBranch">Branch Name</label>  							
          						<input type="text" id="branchName" name="branchName" value="${BranchName}" class="form-control"
          						onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');"	/>
			<div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>
	    </div>	
	</div>
	 </c:if>
	<c:if test="${entityType ne 5}">
	  <input type="hidden" id="headQuarterId" name="headQuarterId" value=0 />
	  <input type="hidden" id="branchId" name="branchId" value=0 />
	 </c:if>
	
	<c:if test="${entityType eq 5}">
	  <input  type="hidden" id="branchId" name="branchId" value="${branchId}">
	  <input type="hidden" id="headQuarterId" name="headQuarterId" value="${headQuarterId}" />
	</c:if>
	
	<div class="row mt10">  
	<c:set var="disabled" value="disabled='disabled'"/>
	<% String id=session.getAttribute("displayType").toString();%>
				<%if(id.equalsIgnoreCase("0")){ %>
	   <div class="col-sm-5 col-md-5">  
	   <c:if test="${entityType ne 5 || entityType ne 1}">	
	        <label>District Name</label>	        
		</c:if>	
	        <c:if test="${entityType==1 || entityType==5}">
		        <span>
		        	<input type="text" id="districtName"  value="${DistrictName}" maxlength="100"  name="districtName" class="help-inline form-control"
		          	onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData'); resetJobCategory();displayJobCategoryByDistrict();"	/>
		      	</span>
			<c:if test="${entityType==1 || entityType==5}">
		      		<input type="hidden" id="districtId" value="${DistrictId}" />
		      	</c:if>
		        <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
      	   </c:if>
      	 
      	   <c:if test="${entityType==2}">
      	   	<input type="hidden" id="districtId" name="districtId" value="${DistrictId}" />
      	   	<input type="text" id="districtName" name="districtName" maxlength="100"  value="${DistrictName}" readonly="readonly" class="form-control" />	
      	   </c:if>		
		</div>
		<%}%>
		<div class="col-sm-5 col-md-5">
	        <label>Job Category&nbsp;<a href="#" id="jobCategoryTooltip" rel="tooltip" data-original-title="Select a Job Category if you want to define the Status Life Cycle for that Job Category. No need to select a Job Category if you want to define District Specific Status Life Cycle."><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
	        	<div id="jobCategory_div">
		        	<select class="form-control" name="jobCategory" id="jobCategory" ${disabled} onchange="resetJobCategory();">
		        		<option selected="selected" value="">All the categories</option>
		        	</select>
	        	</div>
	        	
	    </div>	    
        <input type="hidden" id="txtjobCategory" value="${jobCategory}" /> 
        <input type="hidden" id="txtjobSubCategory" value="${jobSubCateId}" />     
	</div>
	
	<div class="row mt10">  
	   <div class="col-sm-5 col-md-5">
	        <label>Job Sub Category</label>
	        	<div id="jobSubCateDiv"></div>
	    </div>	    
	    <div class="col-sm-2 col-md-2">	    	
       		<button type="button" class="btn btn-primary top25-sm"  onclick="return displayStatusBody();"><strong>Go <i class="icon" ></i></strong></button>
        </div> 
	<input type="hidden" id="txtjobCategory" value="${jobCategory}" />
	</div>
	
	
	<!-- Definition of context menu -->
	<c:if test="${(userMaster.roleId.roleId ne 12 && userMaster.roleId.roleId ne 13)}">
	<div class="row">
	<div class="col-sm-4 col-md-4">
	<ul id="myMenu" class="contextMenu">
		<li id='createMenu'><a href="#create"><span class='icon-folder-open  icon-large  marginleft20'></span> Create</a></li>
		<li id='renameMenu'><a href="#edit"><span class='icon-edit  icon-large iconcolor  marginleft20'></span> Rename</a></li>
		<li id='cutMenu'><a href="#cut"><span class='icon-cut  icon-large iconcolor  marginleft20'></span> Cut</a></li>
		<li id='copyMenu'><a href="#copy"><span class='icon-copy  icon-large  iconcolor marginleft20'></span> Copy</a></li>
		<li id='pasteMenu'><a href="#paste"><span class='icon-paste  icon-large  iconcolor marginleft20'></span> Paste</a></li>
		<li id='deleteMenu'><a href="#delete"><span class='icon-remove-sign  icon-large  iconcolor marginleft20'></span> Delete</a></li>
		<li id='questionMenu'><a id="questionIcon_popup" href="#question" style="display: none;"><span class='fa-list-alt  icon-large  iconcolor marginleft20'></span> Attributes</a></li>
	</ul>
	
	<div  id="folderIcons" class="top20 hide">
		<div id="tree_menu"  >
			<a data-original-title='Create' rel='tooltip' id='createIcon'><span id="btnAddCode" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Rename' rel='tooltip' id='renameIcon'><span id="renameFolder" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Cut' rel='tooltip' id='cutIcon'><span id="cutFolder" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Copy' rel='tooltip' id='copyIcon'><span id="copyFolder" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Paste' rel='tooltip' id='pasteIcon'><span id="pasteFolder" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Delete' rel='tooltip' id='deleteIcon'><span id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Attributes' rel='tooltip' id='questionIcon' style="display: none;"><span id="addQuestion" class='fa-list-alt icon-large iconcolor'></span></a>
			
		</div>
	</div>
	</div>
	</div>
	</c:if>
		<input type="hidden"  name="currentObject" id="currentObject"/>
		<input type="hidden"  name="nodeTempTitle" id="nodeTempTitle"/>
	    <div class='divErrorMsg span12'id='errordeletetreediv' > </div>
		<div class='divErrorMsg span12' id='errortreediv' ></div>
		<!-- Definition tree structure Gagan ${name} ---------- Tree  -->
 		<div class="row" style="height: 300px;">
			  <div class="col-sm-3 col-md-3">
			   <div id="tree"></div>
				<table>
					<tr><td><div id="tree1" class="span5" style="border: 0px solid green;">${tree1}</div> </td></tr>
					<tr><td><div id="tree2" class="span5" style="border: 0px solid green;">${tree2}</div> </td></tr>
				</table>
			</div> 
<%-- ======================================== Right Div ========================================================================= --%>			
			<div class="col-sm-6 col-md-6">
				<span id='statusdashboard'></span>
			</div>
		</div>
		
 	<div class="modal hide"  id="deleteFolder"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group" id="deleteFolder_body">
			
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="deleteStatusConfirm()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
	</div>	
	</div>
	</div>	
	<!-- End_Exclude -->
	
	
<!-- Start ... Add Question  -->

<div  class="modal hide"  id="myModalMaxQuestion"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
<div class="modal-dialog" style="width:845px;">
<div class="modal-content">
<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelForJSIQuestion"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group" style="padding:20px;">
				<div class="row">
					<div class="span3">
					<label class="checkbox">
					<input type="checkbox" id="jobcategorywisestatusprivilegeJSI" name="jobcategorywisestatusprivilegeJSI"> School(s) can set status
					</label>
					</div>
					
					<div class="span2">
					<label class="checkbox">
					<input type="checkbox" id="panelJSI" name="panelJSI"> Panel
					</label>
					</div>
					
				</div>
				<div class="row">
			<div id="errorOption2" style="color: red;padding-left:20px;"></div>
				 <div class="span3">
					<label class="checkbox">
					<input type="checkbox" id="autoUpdateStatus2" name="autoUpdateStatus2" onclick="unableOrDisablexheckBoxes2()"> Auto update status
					</label>
				  </div>
				</div>
	          <div class="row hide updatejobcategoryDiv2">
			                <div class="span4">
					          <label class="radio">
		                        <input type="radio" name="updatejobRadio2" id="updatejobRadio21" value="0"> Update within this job category
		                      </label>
	                        </div>
	                       <div class="span4">
		                     <label class="radio">
		                       <input type="radio" name="updatejobRadio2" id="updatejobRadio22" value="1"> Update across all the job  categories
		                    </label>
		                   </div>
		                   
		                <div class="span3 hide displayDistLevel2">
	                   <label class="radio">
	                       <input type="radio" name="updatejobRadio2" id="updatejobRadio23" value="2"> Update across all the zones
	                   </label>
		            </div>
	           </div>
	           
				<div class="row mt10">
				<div class="span3 hide updatejobcategoryDiv2">
					          <label class="radio-inline">Auto Update status till</label><input type="text" style="width:135px;" id="autoUpdateStatusTill2" name="autoUpdateStatusTill2"   maxlength="0"   value="" class=" span5">
			   </div>
		    		<div class="span4" style="height: 50px;"  id="divMaxScoreQuestion" style="display: none;">
				       	<label id="scoreLabel">Max score for each question</label>				       	
				       		<br/><iframe id="ifrmJSIScore" src="slideract.do?name=sliderJSIScore&tickInterval=10&max=100&swidth=360&svalue=0&step=1" scrolling="no" frameBorder="0" style="padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:410px;margin-top:-10px;"></iframe>
				       	
				   </div>
				</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<span><button type="button" class="btn btn-large btn-primary" onclick="return insertOrUpdateQuestion(2);"><strong>Save <i class="icon" style='margin-top:5px!important;'></i></strong></button>&nbsp;&nbsp;</span>		 		
 		<button id='cancel' class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick="cancelQuestion();">Cancel</button> 		
 	</div>
</div>
</div>
</div>

<input type="hidden" name="addAQuestion" id="addAQuestion" value="0"/>
<input type="hidden" name="myFolderId" id="myFolderId" />
<input type="hidden" name="previousScore" id="previousScore" />
<input type="hidden" name="questionId" id="questionId" />


<div  class="modal hide" id="myModalQuestionList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog" style="width:845px;">
	<div class="modal-content">
	<div class="modal-header" id="myModalQuestionList_header" style="cursor: move;">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelForQuestion"></h3>
	</div>
	<div class="modal-body" style="">		
		<div class="control-group">
			<div class="row">
			      <div class="col-sm-8 col-md-8">
						<div class='divErrorMsg' id='errMultiDisDiv' style="color:red;"></div>
				</div>
			</div>
			<div class="row">
			<c:if test="${entityType ne 5 && entityType ne 6}">
				<div class="col-sm-4 col-md-4 ">
					<label class="checkbox">
					<input type="checkbox" id="jobcategorywisestatusprivilege" name="jobcategorywisestatusprivilege"> School(s) can set status
					</label>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<label class="checkbox">
					<input type="checkbox" id="panel" name="panel"> Panel
					</label>
				</div>
			</c:if>
			<c:if test="${entityType eq 5 || entityType eq 6}">
				<div class="col-sm-4 col-md-4 ">
					<label class="checkbox">
					<input type="checkbox" name="isBranchHQSet" id="isBranchSet" value="0" onchange="toggleFlag('isBranchSet','isHqSet');" /><spring:message code="lblbranchcanset"/>
					</label>
				</div>
				
				<div class="col-sm-4 col-md-4">
					<label class="checkbox">
					<input type="checkbox" name="isBranchHQSet" id="isHqSet" value="1" onchange="toggleFlag('isHqSet','isBranchSet');"/> <spring:message code="lblhqcanset"/>
					</label>
				</div>
			</c:if>
			</div>	
			
			<div class="row">
			<div id="errorOption"></div>
				 <div class="col-sm-4 col-md-4 ">
					<label class="checkbox" style="margin-top: 0px;">
					<c:if test="${entityType ne 5 && entityType ne 6}">
						<input type="checkbox" id="autoUpdateStatus" name="autoUpdateStatus" onclick="unableOrDisablexheckBoxes()"> <spring:message code="lblautoupdatestatus"/>
					</c:if>
					<c:if test="${entityType eq 5 || entityType eq 6}">
						<input type="checkbox" id="autoUpdateStatus" name="autoUpdateStatus" onclick="hideshowHqCheckBoxes()"> <spring:message code="lblautoupdatestatus"/>
					</c:if>
					</label>
				  </div>
		    </div>
		    
	          <div class="row hide updatejobcategoryDiv">
	          			<div class="col-sm-4 col-md-4 "  >
					          <label class="radio" style="margin-top: 0px;">
		                        <input type="radio" name="updatejobRadio" id="updatejobRadio1" value="0"> Update within this job category
		                      </label>
	                        </div>
	                       <div class="col-sm-4 col-md-4">
		                     <label class="radio" style="margin-top: 0px;">
		                       <input type="radio" name="updatejobRadio" id="updatejobRadio2" value="1"> Update across all the job  categories
		                    </label>
		                   </div>
	                      
	                      <div class="col-sm-4 col-md-4 hide displayDistLevel">
		                     <label class="radio" style="margin-top: 0px;">
		                       <input type="radio" name="updatejobRadio" id="updatejobRadio3" value="2"> Update across all the zones
		                    </label>
		                   </div>	                      
	          </div>
	          
	           <div class="hide autoStatusDivForkelly">
						<div class="row left10">
							<div class="col-sm-7 col-md-7">
								<label class="checkbox" style="margin-top: 0px;">
									<input type="checkbox" name="autoStatusForAllAny"
										id="autoStatusForAll" value="0" onchange="toggleFlag('autoStatusForAll','autoStatusForAny');">
									<spring:message code="lblautoupdateforall" />
								</label>
							</div>
						</div>
						<div class="row left10">
							<div class="col-sm-7 col-md-7">
								<label class="checkbox" style="margin-top: 0px;">
									<input type="checkbox" name="autoStatusForAllAny"
										id="autoStatusForAny" value="1" onchange="toggleFlag('autoStatusForAny','autoStatusForAll');">
									<spring:message code="lblautoupdateforany" />
								</label>
							</div>
						</div>
						<div class="row left10">
							<div class="col-sm-4 col-md-4">
								<label class="checkbox" style="margin-top: 0px;">
								<input type="checkbox" name="autoStatusForDate"
										id="autoStatusForDate" value="1">
									<spring:message code="lblautoupdatefordate" />
								
								<input type="text" id="autoUpdateStatusTill"
									name="autoUpdateStatusTill" maxlength="0" value=""
									class="form-control"/>
									</label>
							</div>
						</div>
					</div>
	          
	          <c:if test="${entityType ne 5 && entityType ne 6}">
	          <div class="row">
		          <div class="col-sm-4 col-md-4 hide updatejobcategoryDiv ">
						          <label>Auto Update status till</label><input type="text" id="autoUpdateStatusTill" name="autoUpdateStatusTill"   maxlength="0"   value="" class="form-control">
				  </div>			  
	       
	          <div class="col-sm-4 col-md-4 ">
	            <iframe id='uploadInstructionFrame' name='uploadInstructionFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
			       <form id="frmStatusInstruction" name="frmStatusInstruction" enctype='multipart/form-data' method='post' target='uploadInstructionFrame' action='instructionsUploadServlet.do' >
			       			<input type="hidden" id="instructionFileId" name="instructionFileId" value="0">
							<input type="hidden" name="secondaryStatusId_INS" id="secondaryStatusId_INS" />
			       	   <div class='divErrorMsg' id='errordivInstruction' style="display: block;"></div>
				       <div class="span4" style="margin-left: 0px;">
							<label>
								Attach Instructions
							</label>
							<input id="instructionFN" name="instructionFN" type="file" width="20px;">
							<a href="javascript:void(0)" onclick="clearInstructionFile()">Clear</a>
						</div>
								<span id="removeInstructionFileNameSpan" style="display: none;">
							 <label>&nbsp;&nbsp;</label> 
							<span id="divInstructionFileName"></span>
							
							<a href="javascript:void(0)" onclick="removeInstructionConfirmMsg()">Remove</a>&nbsp;
							<a href="#" id="iconInstructionFN" rel="tooltip" data-original-title="Remove instruction file !"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
							
						</span>
						<iframe src="" id="ifrmInstruction" width="100%" height="480px" style="display: none;"></iframe>
			       </form>
				</div>
	          </div>           
				<div class="row">
					 <div class="col-sm-12 col-md-12">
						<label>Send Status notification to </label>
					</div>
				</div>
				<div class="row">
					 <div class="col-sm-12 col-md-12 left5">
						<div class="col-sm-3 col-md-3 "  >
							<label class="radio" style="margin-top: 0px;">
								<input type="radio" name="districtStatusCHK" id="alldisAdm" value="0" onclick="chkDistrictAdmins(0);"> All District Admins
							</label>
						</div>
						<div class="col-sm-4 col-md-4">
						     <label class="radio" style="margin-top: 0px;">
						       <input type="radio" name="districtStatusCHK" id="selecteddisAdm" value="1" onclick="chkDistrictAdmins(1);"> Selected District Admins
						    </label>
						</div>
					</div>
				</div>
				<div class="hide" id="multiDisAdminDiv">
					<div class="row left5">
						<div class="col-sm-5 col-md-5" style="border: 0px solid red;">
							<label>Select District Admins</label>
						</div>
					</div>
					<div class="row left5">
				 		 <div class="col-sm-5 col-md-5" style="border: 0px solid red;">
					 		<div id="1stSchoolDiv">
								<select multiple id="lstDistrictAdmins" name="lstDistrictAdmins" class="form-control" style="height: 150px;" > 
								</select>
							</div>
						</div>		
						<div class="col-sm-1 col-md-1 left20-sm"> 
							<div class="span2"> <span id="addPop" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
							<div class="span2"> <span id="removePop" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
						</div>
						<div class="col-md-5 col-md-5" style="border: 0px solid green;">
							<div id="2ndSchoolDiv">
								<select multiple class="form-control" id="attachedDAList" name="attachedSchoolList" style="height: 150px;">
								</select>
							</div>
						</div>	
					</div>
				</div>
				
				<div class="row">
					 <div class="col-sm-12 col-md-12 left5" style="margin-top: 0px;">
					 	<div class="col-sm-4 col-md-4">
							<label class="checkbox">
								<input type="checkbox" id="allSchoolAdm" name="allSchoolAdm" onclick=""> All School Admins
							</label>
						</div>
					</div>
				</div>
				
				<div class="row">
					 <div class="col-sm-12 col-md-12" style="margin-top: 0px;">
							<label class="checkbox">
								<input type="checkbox" id="chkNotifyAll" name="chkNotifyAll" onclick=""> Auto Notify all the associated District and School Admins of the status change for selected Statuses
							</label>
					</div>
				</div>
				
				<!-- Amit Kumar start -->
				<div class="row">
					 <div class="col-sm-12 col-md-12 left5" style="margin-top: 0px;">
						<div class="col-sm-5 col-md-5">
					 		<label class="checkbox">
								<input type="checkbox" id="overrideAdm" name="overrideAdm" onclick=""> Override Admin User Notification Settings
								<a href="#" id="overrideAdmTooltip" rel="tooltip" data-original-title="Some admins may have opted out of receiving notifications for status changes. When this option is selected, the admins selected here will receive notifications for this status change regardless of their notification settings."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
							</label>
						</div>
					</div>
				</div>
				<!-- Amit Kumar end -->
				
				<!-- added by 05-06-2015 -->
				<div class="row">
					 <div class="col-sm-12 col-md-12">
						<label class="checkbox"><input type="checkbox" id='bothIntAndExtCand' value="1" />Status/Node required</label>
						<label class="checkbox" style="padding-left:40px;"><input value="3" type="checkbox" id='externalCand' />Status/Node required for external candidates</label>
						<label class="checkbox" style="padding-left:40px;"><input value="2" type="checkbox" id='internalCand' />Status/Node required for internal candidates</label>
					</div>
				</div>
				
				<div class="row">
					 <div class="col-sm-12 col-md-12">
						<label class="checkbox"><input type="checkbox" id='statusForFirstTimeJobApply' value="1" /><spring:message code="lblSetStatusFirstTimeJobApply"/></label>
					</div>
				</div>
		
				<div class="row">
					 <div class="col-sm-12 col-md-12">
						<label class="checkbox"><input type="checkbox" id='finalizeforevnt' value="1" />Finalizing this node invites applicant to an event</label>
					</div>
				</div>
		
				<div class="row">
				 <div class="col-sm-12 col-md-12">
					<div class="add-Question  pull-right" >
							<a href="javascript:void(0);" onclick="showQuestionForm();">+ Add a Question</a>
					</div>	
				</div>
				</div>
				
				<div class="row"><div class="col-sm-4 col-md-4 " id="divGridQuestionList"></div></div>
				
				<div id="divQuestionList" class="mt10 " style="display: none;">
						<div class="row">
					       <div class="col-sm-8 col-md-8">
								<div class='divErrorMsg' id='errorQuestion'></div>
							</div>
				      	</div>

			    		<div class="row">
					       <div class="col-sm-8 col-md-8">
					       <span class=""><label>Question</label>
					       	<input type="text" id="question" name="question"  maxlength="2500" class="form-control"/>
					       </span>
					       </div>
				      	</div>
				      	
				    	<div class="row">
				    		<div class="col-sm-8 col-md-8">
						       	<span><label>Attribute to Score&nbsp;<a href="#" id="scorecaptionInfo" rel="tooltip" data-original-title="Select an attribute to score"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
						       	<select class="form-control" id="scoringCaption" name="scoringCaption">
						       		<option value="" selected="selected">Select Attribute</option>
						       		<c:forEach items="${skillAttributesMasters}" var="sam">
										<option value="${sam.skillAttributeId}">${sam.skillName}</option>
						       		</c:forEach>
								</select>
						       </span>
						   </div>
				    	</div>
			    		
			    		<div class="row">
							 <div class="col-sm-12 col-md-12">
								<label class="checkbox"><input type="checkbox" id='requiredtofinalizenode'/>Question is required to finalize node</label>
							</div>
						</div>
			    		
			    		<div class="row top15" style="height: 50px;">
				    			<div class="col-sm-8 col-md-8">
						       	<span><label id="scoreLabel">Score</label></br>
						       	<span>
						       		<iframe id="ifrmScore" src="slideract.do?name=sliderScore&tickInterval=10&max=100&swidth=360&svalue=0&step=1" scrolling="no" frameBorder="0" style="padding: 0px; margin-left: -2px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:410px;margin-top:-10px;"></iframe>
						       	</span>
					       			
						       </span>
						   </div>
				       </div>
				    
				       <div class="row top10">
							<div class="col-sm-5 col-md-5" id="divDone">
							<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="insertOrUpdateQuestion(0)">
								<label> <spring:message code="lnkImD"/> </label>
							</a>&nbsp;&nbsp;
							<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="cancelQuestion()">
								Cancel
							</a>
						</div>
					   </div>
				 </div>
				    </c:if>
				    <c:if test="${entityType eq 5}">
				    	<div class="row">
							<div class="col-sm-7 col-md-7">
								<label class="checkbox" style="margin-top: 0px;">
									<input type="checkbox" name="statusnotify"
										id="statusnotify" value="1">
									<spring:message code="lblstatusnotification" />
								</label>
							</div>
						</div>
						<div id="statusNotifyDiv">
						<div class="row left10">
							<div class="col-sm-7 col-md-7">
								<label class="checkbox" style="margin-top: 0px;">
									<input type="checkbox" name="selectedhqbradmin"
										id="selectedhqadmin" value="0" onchange="toggleFlag('selectedhqadmin','branchadmins');hideShowUsersDiv('selectedhqadmin','hqAdminsDiv','1');"/>
									<spring:message code="lblselectedhqadmins" />
								</label>
							</div>
						</div>
						<div id="hqAdminsDiv" class="row left30">
							<div class="col-sm-5 col-md-5" style="border: 0px solid red;">
								<div id="1stHQDiv">
									<select multiple id="lstHQAdmins" name="lstHQAdmins"
										class="form-control" style="height: 150px;">
									</select>
								</div>
							</div>
							<div class="col-sm-1 col-md-1 left20-sm">
								<div class="span2">
									<span id="addHQPop" style="cursor: pointer;"><img alt=""
											src="images/rightarrow.jpg" width="30px"> </span>
								</div>
								<div class="span2">
									<span id="removeHQPop" style="cursor: pointer;"><img
											alt="" src="images/leftarrow.jpg" width="30px"> </span>
								</div>
							</div>
							<div class="col-md-5 col-md-5" style="border: 0px solid green;">
								<div id="2ndHQDiv">
									<select multiple class="form-control" id="attachedHQList"
										name="attachedHQList" style="height: 150px;">
									</select>
								</div>
							</div>
						</div>
						
						<div class="row left10">
							<div class="col-sm-7 col-md-7">
								<label class="checkbox" style="margin-top: 0px;">
									<input type="checkbox" name="selectedhqbradmin"
										id="branchadmins" value="1" onchange="toggleFlag('branchadmins','selectedhqadmin');"/>
									<spring:message code="lblbranchadmins" />
								</label>
							</div>
						</div>
						<div id="selAllBranches">
							<div class="row left30">
								<div class="col-sm-4 col-md-4">
									<label class="checkbox" style="margin-top: 0px;">
										<input type="checkbox" name="allorselectedbranchadmins"
											id="allbranchadmins" value="0"
											onchange="toggleFlag('allbranchadmins','selectedbranchadmins');hideShowUsersDiv('allbranchadmins','branchAdminsDiv','0');" />
										<spring:message code="lblallbranchadmins" />
									</label>
								</div>
								<div class="col-sm-4 col-md-4">
									<label class="checkbox" style="margin-top: 0px;">
										<input type="checkbox" name="allorselectedbranchadmins"
											id="selectedbranchadmins" value="1"
											onchange="toggleFlag('selectedbranchadmins','allbranchadmins');hideShowUsersDiv('selectedbranchadmins','branchAdminsDiv','1');" />
										<spring:message code="lblselectedbranchadmins" />
									</label>
								</div>
							</div>
						</div>
						
						<div id="branchAdminsDiv" class="row left30">
							<div class="col-sm-5 col-md-5" style="border: 0px solid red;">
								<div id="1stBranchDiv">
									<select multiple id="lstBranchAdmins" name="lstBranchAdmins"
										class="form-control" style="height: 150px;">
									</select>
								</div>
							</div>
							<div class="col-sm-1 col-md-1 left20-sm">
								<div class="span2">
									<span id="addBAPop" style="cursor: pointer;"><img alt=""
											src="images/rightarrow.jpg" width="30px"> </span>
								</div>
								<div class="span2">
									<span id="removeBAPop" style="cursor: pointer;"><img
											alt="" src="images/leftarrow.jpg" width="30px"> </span>
								</div>
							</div>
							<div class="col-md-5 col-md-5" style="border: 0px solid green;">
								<div id="2ndBranchDiv">
									<select multiple class="form-control" id="attachedBAList"
										name="attachedBAList" style="height: 150px;">
									</select>
								</div>
							</div>
						</div>
						</div>
						
				    </c:if>
		</div>
 	</div>
 	<div class="modal-footer">
 	<c:if test="${entityType ne 5 && entityType ne 6}">
 		<span><button type="button" class="btn btn-large btn-primary" onclick="return insertOrUpdateQuestion(1);"><strong>Save <i class="icon" style='margin-top:5px!important;'></i></strong></button>&nbsp;&nbsp;</span>		 		
 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick="cancelQuestion();">Cancel</button>
 	</c:if>
 	<c:if test="${entityType eq 5}">
 		<span><button type="button" class="btn btn-large btn-primary" onclick="validateAttributesHQ();"><strong>Save <i class="icon" style='margin-top:5px!important;'></i></strong></button>&nbsp;&nbsp;</span>		 		
 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick="cancelAttribute();">Cancel</button>
 	</c:if> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="myModalMsgShow_NodeInfo"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3>TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group">The folder you selected is a node but not a status. We cannot set attributes against a node.
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="myModalSaveConfirm"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3>TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" >Attributes have been saved successfully.
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
 	</div>
</div>
</div>
</div>
<!-- End ... Add Question  -->

<div  class="modal hide"  id="modalDownloadIAFN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header" id="modalDownloadIAFNMove" style="cursor: move;">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelInstructionHeader"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="">
				<iframe src="" id="ifrmAttachInstructionFileName" width="100%" height="480px">
				 </iframe>        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>
</div>
</div>
<div class="modal hide"  id="modalDownloadIAFNdeleteFile"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Do you really want to delete?
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="removeInstructionFile()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
</div>
</div>
</div>
<div style="display:none; z-index: 5000;" id="loadingDiv" >
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<input type="hidden" id="entityType" value="${entityType}"/>	
 <script>
 var displayID=$("#displayTypeId").val();
	if(displayID!=0){
		setEntityType();
	}
 </script>
<script>
	
	//displayStatusDashboard();
	$('#createIcon').tooltip();
	$('#renameIcon').tooltip();
	$('#cutIcon').tooltip();
	$('#copyIcon').tooltip();
	$('#pasteIcon').tooltip();
	$('#deleteIcon').tooltip();
	$('#questionIcon').tooltip();
	$('#scorecaptionInfo').tooltip();
	$('#jobCategoryTooltip').tooltip();
	$('#overrideAdmTooltip').tooltip();
	
	
	displayJobCategoryByDistrict();
//	getJobSubCateList();
	//$('#jobCategory option:selected').val(${jobCategory});
	copyDataFromDistrictToJobCategory();
	displayStatusBodyOnPageLoad();
	
	getBranchDistrictFilter();
	 var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("autoUpdateStatusTill", "autoUpdateStatusTill", "%m-%d-%Y");
     cal.manageFields("autoUpdateStatusTill2", "autoUpdateStatusTill2", "%m-%d-%Y");


$(document).mouseup(function (e)
{
    var container = $(".DynarchCalendar-topCont");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
});  
        
</script>

<script type="text/javascript">
	$('#addPop').click(function() {
		if ($('#lstDistrictAdmins option:selected').val() != null) {
			 $('#lstDistrictAdmins option:selected').remove().appendTo('#attachedDAList');
			 $("#attachedDAList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
	         $("#attachedDAList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	 }
	});
	
	$('#removePop').click(function() {
	       if ($('#attachedDAList option:selected').val() != null) {
	             $('#attachedDAList option:selected').remove().appendTo('#lstDistrictAdmins');
	           //  sortSelectList();
	             $("#attachedDAList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').addAttr("selected");
	             
	}
	});
	
	try{
		$("#statusNotifyDiv").hide();
	$('#addHQPop').click(function() {
		if ($('#lstHQAdmins option:selected').val() != null) {
			 $('#lstHQAdmins option:selected').remove().appendTo('#attachedHQList');
			 $("#attachedHQList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
	         $("#attachedHQList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	 }
	});
	
	$('#removeHQPop').click(function() {
	       if ($('#attachedHQList option:selected').val() != null) {
	             $('#attachedHQList option:selected').remove().appendTo('#lstHQAdmins');
	             $("#attachedHQList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstHQAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstHQAdmins").attr('selectedIndex', '-1').addAttr("selected");
	}
	});
	
	$('#addBAPop').click(function() {
		if ($('#lstBranchAdmins option:selected').val() != null) {
			 $('#lstBranchAdmins option:selected').remove().appendTo('#attachedBAList');
			 $("#attachedBAList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
	         $("#attachedBAList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	 }
	});
	
	$('#removeBAPop').click(function() {
	       if ($('#attachedBAList option:selected').val() != null) {
	             $('#attachedBAList option:selected').remove().appendTo('#lstBranchAdmins');
	             $("#attachedBAList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstBranchAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstBranchAdmins").attr('selectedIndex', '-1').addAttr("selected");
	}
	});
	
	$("#branchadmins").click(function(){
	if($('input:checkbox[name=selectedhqbradmin]')[1].checked == true){
					$("#selAllBranches").show();
					$("#hqAdminsDiv").hide();
		}
		else{
					$("#selAllBranches").hide();
					$("#branchAdminsDiv").hide();
		}
	});
	
	$("#selectedhqadmin").click(function(){
	if($('input:checkbox[name=selectedhqbradmin]')[0].checked == true){
			$("#branchAdminsDiv").hide();
			$("#selAllBranches").hide();
		}
	});
	
	$("#statusnotify").click(function(){
	if($('#statusnotify').is( ":checked" )){
			$("#statusNotifyDiv").show();
		}
		else{
		$("#statusNotifyDiv").hide();
		}
	});
		$("#selAllBranches").hide();
	
	}catch(e){}
	//displayAllAndSelecedDist();
</script>