<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<style>
	p
	{
		text-align: justify;
		padding-right: 5px;
		/*text-indent: 20px;*/
	}
	.spnIndent
	{
		padding-left: 20px;
	}
	.spnIndent40
	{
		padding-left: 40px;
	}
	ul
	{
	margin-right: 10px;
	}
</style>
			

		
<div class="container">
			<div class='divErrorMsg' id='errordiv' style="display: block;">				
			</div>
		</div>
	

		<div class="container">
			 <div class="row centerline">
					<img src="images/affidavit.png" width="47" height="48" alt="">
					<b class="subheading" style="font-size: 13px;">
					TM Web site (Policies and Procedures for Consumption)
				    </b>
		     </div>
		     
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br><span>&nbsp;&nbsp;&nbsp;Our Innovative Solution</span>
		    </br></br><div style="margin-left:27px;">Assessment Development: Policies and Procedures</div>
		   
		    </div>		
		</div>
        <div class="container top15">
             <div class="row" align="justify">
             <div class="termsheight right10">
				<p style="text-indent: 0px;padding:0px 10px 0px 40px">TeacherMatch assessments provide hiring authorities with information and tools that enable them to select high quality teachers and to customize professional development plans in order to effectively support teacher development. 
				</p>
				<p style="text-indent: 0px;padding:0px 10px 0px 40px">The development of TeacherMatch assessments is supervised by professional psychometricians, content developers, and subject matter experts, and adheres to professional and technical standards ensuring assessment reliability and validity, notably guidelines covered by the American Educational Research Association, the American Psychological Association, and the National Council on Measurement in Education.
				</p>
				<p style="text-indent: 0px;padding:0px 10px 0px 40px">
				As part of our ongoing effort to enhance the value of TeacherMatch assessments, certain security measures are in place to protect the integrity of the assessments. 
				</p>
				<p style="text-indent: 0px;padding:0px 10px 0px 40px">
					Please familiarize yourself with the following policies.
				</p>
				<p style="text-indent: 0px;padding:0px 10px 0px 40px">
					<b>Testing Rules</b></br>
					<b>Psychometric Procedures</b></br>
					<b>Challenging an Exam Item (Grievance Policy)</b></br>
					<b>Retake Policy</b></br>
					<b>Disclosure of Test Data</b></br>
					<b>Scoring</b></br>
					<b>Equivalent Forms</b></br>
					<b>Assessment Timing</b>
				</p>
			</div>
		</div>				  
	</div>
	
	<div class="container">
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span NAME="tr">Testing Rules</span>
		    </div>	
		    
		    <ul style="margin-left:-18px">
			    <li>Candidates may not use recording devices, such as paper/pencil, cameras, PDAs, computers, or communication devices, such as telephones, or pagers.</li>
			    <li>Candidates must read and accept the terms of the non-disclosure agreement presented prior to the start of the EPI.</li>
			    <li>Candidates must not reproduce exam content outside of the testing area.</li>
			    <li>Candidates must abide by the terms of the retake policy.</li>
			    <li>Candidates found to have violated these rules may lose any existing EPI scores and may be made permanently ineligible for additional assessments.</li>
		    
		    </ul>
		    	
		    <p style="text-indent: 0px;">For additional information regarding this policy and requests for accommodations, please contact TeacherMatch Client Services at:
		    </p>
		    <p><a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a> <i>or by phone at 1.888.312.7231 ext. 106.</i></p>
		</div>
		
		<div class="container">
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span id="psychometric">Psychometric Procedures</span>
		    </div>	
		    <p style="text-indent: 0px;">During the psychometric beta phase, item analysis is conducted on all items prior to including any item within a live assessment form. At this juncture, analysis of item-level data gathered from beta testing involves the computation and examination of any statistical property of candidates’ responses to an individual test item. Item parameters examined fall into three categories:
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Item validity: item difficulty, mean, and variance of item responses - describes the distribution of responses to a single item,</li>
		    <li>Item discrimination: describes the degree of relationship between responses to the item and total assessment scores, and</li>
		    <li>Item reliability: a function of item variance and relationship to assessment scores</li>
		    </ul>
		    
		    <p style="text-indent: 0px;">
		    	Each of these measures is observed in conjunction with the distribution of total scores to arrive at decisions regarding item selection. Items surviving psychometric beta are subjected to further predictive modeling in order to select items that best discriminate between high- and low-performing test candidates.
		    </p>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span>Challenging an Exam Item (Fairness and Grievance Policy)</span>
		    </div>
		    <p style="text-indent: 0px;">
		    	TeacherMatch is committed to developing assessments that operate as valid and reliable measures of competency and meet applicable professional standards of test development and administration. In keeping with this commitment, it is our desire to create grievance policies and processes to ensure that all test candidates are treated fairly and impartially.
		    </p>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span>Accommodations</span>
		    </div>
		    <p style="text-indent: 0px;">
		    	If you believe you are entitled accommodations, please contact the hiring authority of the district to which you are applying for employment.  TeacherMatch will work with the district hiring authority to ensure that you are provided accommodations as needed. 
		    	</p>
		    	
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span>Technical Issues</span>
		    </div>
		    <p style="text-indent: 0px;">If a candidate has a technical problem with an assessment, TeacherMatch Client Services is the first point of contact. TeacherMatch will review all technical test issues and provide appropriate feedback whenever possible. In the event that an assessment crashes, TeacherMatch may: 
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Trouble-shoot the issue and ask the candidate to restart the assessment, which will automatically return them to the last recorded item. Since data are saved at 1-item intervals, candidates will not need to repeat items.</li>
		    <li>Candidates are asked to complete the assessment in the requested time constraints and multiple violations of the rules may prevent full completion of the assessment.</li>
		    </ul>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span>Substantive Issues</span>
		    </div>
		    <p style="text-indent: 0px;">TeacherMatch Client Services is not authorized to remedy substantive concerns including the following: 
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Item scoring</li>
		    <li>Item content</li>
		    <li>Candidate responses to assessment items</li>
		    <li>Assessment timing; additional time cannot be added to an assessment.</li>
		    </ul>
		    <p style="text-indent: 0px;">
		    Consequently, if Client Services is unable to satisfy a candidate’s concern, TeacherMatch staff will direct the candidate to TeacherMatch management. Issues/complaints will be logged and processed in a timely manner. Candidate complaints will be managed on an informal or formal basis depending on the nature of the issue. Complaints must be received within three business days of the date the relevant assessment was administered.
		    </p>
		    <p style="text-indent: 0px;">
		    Informal Complaints: TeacherMatch will manage the resolution of complaints that do not require investigation, including complaints regarding:
		    </p>
		    <ul style="margin-left:-18px">
		    <li>System failures/issues</li>
		    <li>Total assessment time</li>
		    <li>Understanding/clarity of items (no complaint on a particular item)</li>
		    <li>Total score (no complaint on a particular item)</li>
		    <li>Equipment issues (e.g., performed the task but didn’t see the result)</li>		    
		    </ul>
		    <p style="text-indent: 0px;">
		    Formal Complaints. If one or more standard- informal - responses are not applicable or fail to satisfy the candidate’s concerns, TeacherMatch will investigate each complaint and propose a resolution. Examples of formal complaint issues include:
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Scoring of a particular item</li>
		    <li>Understanding/clarity of a particular item</li>
		    <li>Validity of a particular item (item content, methods of completion), and/or</li>
		    <li>Item bias</li>
		    </ul>
		    <p style="text-indent: 0px;">
		    	Client Services will send an e-mail message to the complaint candidate summarizing and confirming the complaint.
		    </p>
		    <p style="text-indent: 0px;">
		    	Client Services will also summarize the candidate’s complaint in an e-mail to the assessment content manager along with the following information:
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Candidate name and identification number</li>
		    <li>Form number of the assessment</li>
		    <li>Date the assessment was taken</li>
		    <li>Assessment score and result</li>
		    <li>Number of attempts</li>
		    <li>A concise description for the assessment content at issue, and</li>
		    <li>A concise rationale for reversal of the item or assessment result</li>
		    </ul>
		    
		    <p style="text-indent: 0px;">
		    	Once the complaint is received, the TeacherMatch management will:
		    </p>
		    <ul style="margin-left:-18px">
	    	<li>Investigate</li>
			<li>Make a determination whether the item content or scoring is flawed</li>
			<li>Make a determination as to whether a score change will be awarded</li>
			<li>Prepare a formal response to the test candidate</li>
			<li>Forward the response to the individual who initiated the complaint </li>
			</ul>
			<p style="text-indent: 0px;">
			Communication:Communication with the candidate regarding the clarification of the complaint and the resolution of the complaint will be handled exclusively by Client Services.
			</p>
			<p style="text-indent: 0px;">
			Timing:Client Services will investigate and resolve each complaint within five business days of its receipt.
			</p>
			<p style="text-indent: 0px;">
			Disclosure: In the interest of assessment security, TeacherMatch will only disclose its findings or reasoning for its determinations to the complaint candidate if TeacherMatchmanagement deems it appropriate, including whether altering the score on the contested item affected the outcome.
			</p>
			<p style="text-indent: 0px;">
			Closure: Since all items are reviewed for clarity, accuracy, and bias prior to the release of the assessment, the TeacherMatch manager’s decision will be regarded as final and no further evaluation will occur.
			</p>
			
			<div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span>Retake Policy</span>
		    </div>
		    
		    <p style="text-indent: 0px;">The purpose of the TeacherMatch retake policy is to ensure that appropriate measures are taken in order to protect assessment content, by minimizing the number of occasions a test candidate may take an assessment.
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Anyone completing an assessment must wait 12 months before taking the same assessment again, unless test objectives have changed. Immediate retakes are not allowed.</li>
			<li>If test objectives have changed, candidates may retake the new assessment form at any time.The new form scores will not replace previous scores, rather, a second score report will be made available.</li>
			<li>Candidates violating the “time-out” policy may NOT retake the assessment. They may, however, take a new – equivalent – form of the assessment after waiting a minimum of 14 days.</li>
			<li>Candidates who violate the “time-out” policy a second time will be permanently ineligible for TeacherMatch assessments. </li>
			<li>Eligible candidates may retake the same form of an assessment after waiting 12 months.</li>
			<li>If caught violating the Non-Disclosure Agreement, the candidate will be permanently ineligible for any TM assessment.</li>
		    </ul> 
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span>Disclosure of Test Data</span>
		    </div>
		    
		    <p style="text-indent: 0px;">The objective of the TM Disclosure of Test Data – Data Sharing – Policy, is to provide stake holders with information required to make sound judgments regarding the quality of assessments and the appropriate interpretation of assessment scores; and to provide rationale for limiting the disclosure of test data and securing individual candidate records.
		    </p>
		    <p style="text-indent: 0px;">
		    To protect the value of secured assessments whose psychometric integrity depends upon candidates not having prior access to test materials and vital test information, the disclosure of test data policy is based on certain ethical and legal obligations to provide these data. As detailed in this policy, TM will take appropriate steps to:
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Prevent the misuse of assessment data, and</li>
		    <li>Maintain the confidentiality of candidate assessment scores</li>
		    </ul>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span>Candidate Confidentiality</span>
		    </div>
		    <p style="text-indent: 0px;">
		    TeacherMatch does not disclose individual candidate data to third parties. Individual candidate data and aggregate reports are generated based on the following hierarchy:
		    </p>
		    <p style="text-indent: 0px;">
		    Candidates: Individual access to professional development reports are available for purchase. Access to candidate grid results and/or item-level data are not disclosed to individual test candidates. 
		    </p>
		    
		    <p style="text-indent: 0px;">
		    School Administrators:Access to candidate grid reports are available – includes individual candidates’ scores by domain. Results are summarized by groups of test candidates.
		    </p>
		    
		    <p style="text-indent: 0px;">
		    District Administrators: Access to all of the School Administrator reports in the school in addition to the following reports:
		    </p>
		    <ul style="margin-left:-18px"><li>User reports –search any candidate in the district and view assessment scores taken by the candidate.</li></ul>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span>Normative Data</span>
		    </div> 
		    <p style="text-indent: 0px;">
		    It is the objective of TM to preserve third party interests to summative results. The following summative data will be made available to third parties (e.g., state representatives) who make a formal request:
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Number of assessments delivered (also by assessment form, district, school)</li>
			<li>Normative scores by assessment</li>
		    </ul>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span >Scoring</span>
		    </div>
		    <p style="text-indent: 0px;">
		    The number of points possible for a TM assessment varies, and each item is scored against the end-result of a set of independent tasks within the assessment. While some items involve more tasks than others, because all tasks are scored independently, candidates are scored on their total assessment score rather than on a pass/fail basis per item. 
		    </p>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span >Equivalent Forms</span>
		    </div>
		    
		    <p style="text-indent: 0px;">
		    The purpose of establishing equivalent forms is to create comparable scores on different assessments that measure the same construct. For TM assessments, developing equivalent forms helps to ensure integrity by minimizing compromises to test security that can result from overexposure, cheating, and/or assessment retakes.
		    </p>
		    <p style="text-indent: 0px;">
		    The first step in establishing equivalent scores is to ensure that each assessment aligns with the test specifications; this provides content validity assurance that each form is measuring the same construct with equal representation. The next step is to ensure statistical equivalence so that candidates are not unduly penalized or rewarded for testing on a more or less difficult assessment form.
		    </p>
		    <p style="text-indent: 0px;">
		    To ensure that TM assessments are statistically equivalent between forms, the Rasch common item linking procedure is used. The design represents a between-groups nonequivalent quasi-experiment, where separate groups of test candidates are each administered different forms of an assessment. The forms are internally linked by a set of anchor items that represent a sample of all items across the TM item bank. These anchor items are used to adjust for population differences between groups and to investigate differential item functioning.
		    </p>
		    <p style="text-indent: 0px;">
		    Immediately following, or concurrent with, the development of the first form of an assessment, TM will release a second equivalent form making use of new assessment items. New forms must contain at least 30% new item content. Development of additional forms are considered as the psychometric staff recommends.
		    </p>
		    
		    <div style="text-align: left; padding-bottom: 5px;font-family:Century Gothic;font-weight:bold;font-size:16px;">
		    </br>
		    <span id="assessment">Assessment Timing</span>
		    </div>
		    
		    <p style="text-indent: 0px;">
		    Each question in the EPI has a stipulated time limit of 75 seconds,and you must respond to each question within its stipulated time limit. You must answer all of the questions in one sitting. 
		    </p>
		    <ul style="margin-left:-18px">
		    <li>Make sure that you have at least 90 minutes of uninterrupted time to complete the EPI.</li>
		    <li>Make sure that you have a stable and reliable Internet connection.</li>
		    <li>Do not close your browser or hit the "back" button on your browser.</li>
		    <li>You are not able to skip questions.</li>
		    </ul>
		    
		    <p style="text-indent: 0px;">
		    If you do not follow these guidelines your status may become "timed out" and you will not be able to complete the EPI for twelve months.  
		    </p>
		    
		</div>