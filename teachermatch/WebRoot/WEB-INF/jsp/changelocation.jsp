<!--
@author Amit 
@date 11-Mar-15
-->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/interface/SocialServiceAjax.js"></script>
<script type="text/javascript" src="js/usersettings.js?ver=${resourceMap['js/usersettings.js']}"></script>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});

function applyScrollOnTblList()
{
	var $j=jQuery.noConflict();
    $j(document).ready(function() {
        $j('#tblGridList').fixheadertable({ //table id 
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	       //height: 400,
	        width: 945,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:[25,300,300,160,160],
	        addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
		});
	});	
}

</script>

<div class="row" style="margin:0px;">
	<div style="float: left;">
		<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
    </div>
    <div style="float: left;">
    	<div class="subheading" style="font-size: 13px;"><spring:message code="lblSwitchLoc" /></div>
    </div>
    <div style="clear: both;"></div>	
	<div class="centerline"></div>
</div>
<br> 

<div style="display:none;" id="loadingDiv">
	<table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

<div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
	<iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no"></iframe>
</div>

<input type="hidden" id="headQuarterId" name="headQuarterId" value="${headQuarterId}">
<input type="hidden" id="branchId" name="branchId" value="${branchId}">
<input type="hidden" id="DistrictId" name="DistrictId" value="${DistrictId}">
<input type="hidden" id="emailAddress" name="emailAddress" value="${emailAddress}">
<input type="hidden" id="entityType" name="entityType" value="${entityType}">
<input type="hidden" id="userMaster" name="userMaster" value="${userMaster}">
<div style="clear: both;"></div>
<br/>

<div class="TableContent">
	<div class="table-responsive" id="divMainOfList" ></div>       
</div>


<div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
		<div class="modal-content">
			<div class="modal-header">
	   			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   			<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
				<div class="control-group">
					<div class="" >
						<spring:message code="msgPlzPopAreAlloued" />
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
				<table border=0 align="right">
	 				<tr>
		 				<td nowrap>
		 					<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk" /></button>
						</td>
	 				</tr>
	 			</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	displayRecordsByDistrictAndEmail();
</script>
