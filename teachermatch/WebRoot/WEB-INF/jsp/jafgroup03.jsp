<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
        $j(document).ready(function() {
       });
</script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/group01.js?ver=${resouceMap['js/jobapplicationflow/group01.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jobapplflowcommon.js?ver=${resouceMap['js/jobapplicationflow/jobapplflowcommon.js']}"></script>

<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafAutoCompCertificate.js?ver=${resouceMap['js/jobapplicationflow/jafAutoCompCertificate.js']}"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<input type="hidden" id="gridNameFlag" name="gridNameFlag"/>
<p id="portfolioHeader"></p>

<input type="hidden" id="sNextURL" name="sNextURL" value="${sNextURL}"/>
<input type="hidden" id="sPreviousURL" name="sPreviousURL" value="${sPreviousURL}"/>
<input type="hidden" id="sCurrentPageShortCode" name="sCurrentPageShortCode" value="${sCurrentPageShortCode}"/>

<iframe src="" id="ifrmGK" width="100%" height="480px" style="display: none;"></iframe>

<style>
.divwidth
{
float: left;
width: 40px;
}
</style>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:30px;">
         <div style="float: left;">
         	<img src="images/certifications.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="DSPQlblCredentials"/></div>	
         </div>	
         
         <div style="float: right;">
          	<div style='text-align:right; padding-right: 0px;' class="col-sm-3 col-md-3"><button class='flatbtn' style='width:190px;'  type='button' onclick='return validateDSPQCredentialsGroup(0);'><spring:message code="btnSaveAndNextDSPQ"/> <i class='icon'></i></button></div>
         </div>
         	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<p id="group03_21"></p>
<p id="group03_23"></p>

<p id="group03_7"></p>
<p id="group03_8"></p>
<p id="group03_9"></p>
<p id="group03_10"></p>
<p id="group03_11"></p>

<p id="group03_27"></p>

<p id="group03_28"></p>

<!--<p id="group03_22">TFA</p>-->

<p id="group03"></p>

<div  class="modal hide" id="draggableDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-cgdemoschedule">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="lblCert/LiceName" /></h3>
	</div>
	<div class="modal-body">
		<p><spring:message code="msgWeArSoryYuNotFindingYurCerti/Lice" /></p>
		<p><spring:message code="msgPlzTypingStringInFldCerti/Lice" /></p>
		<p><spring:message code="pNotifyEmailService" /> 
		<a href="mailto:clientservices@teachermatch.net" target="_top">clientservices@teachermatch.net</a>
		 <spring:message code="pHlpYuLoadRiLice/Certi" /></p> 
		<p><spring:message code="pThkForYurHlpInThisMtr" /></p>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button>
 	</div>
</div>
	</div>
</div>


<div class="modal hide"  id="deleteCertRec" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					<spring:message code="msgDeleteTheRecord" />
				</div>
		 	</div>
		 	<div class="modal-footer">
		 		<input type="hidden" id="certificateTypeID"/>
		 		<span id=""><button class="btn  btn-primary" onclick="deleteCertificateRecord()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
		 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
		 	</div>
		</div>
 	</div>
</div>


<div class="modal hide"  id="chgstatusRef1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUActRef" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeActRef" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
 	</div>
</div>
	</div>
</div>


<div class="modal hide"  id="deleteRefAttachFile"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					Are you sure, you would like to delete the Rec. Letter?
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	   <input type="hidden" id="academicID"/>
	 	     <span id=""><button class="btn  btn-primary" onclick="removeReferencesL2()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
	 	</div>
	  </div>
	</div>
</div>

<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeleteVideoLnk" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
	</div>
</div>


<!--  Display  Video Play Div -->
<div  class="modal hide" id="videovDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%; " >
		<div class="modal-content">		
			<div class="modal-header">
			 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="">x</button>
				<h3 id="myModalLabel"><spring:message code="lblVid" /></h3>
			</div>
			<div class="modal-body">		
				<div class="control-group">
						<div style="display:none;" id="loadingDivWaitVideo">
						     <table  align="center" >						 		
						 		<tr><td align="center"><img src="images/loadingAnimation.gif"/></td></tr>
							</table>
						</div>	
						<div id="interVideoDiv"></div>
				</div>
			</div>	
			<div class="modal-footer">	 			
	 		<button class="btn" id="closeBtn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button>
 			</div>		
		</div>
	</div>
</div>


<div class="modal hide"  id="removeUploadedDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="removeUploadedDocument()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
 	</div>
	</div>
	</div>
</div>


<div class="modal hide"  id="credentialGroupError" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 500px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group" id="credentialGroupErrorMsg">
			
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="CloseCredentialGroupError()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<!--  <button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button>--> 		
 	</div>
	</div>
	</div>
</div>
<div class="modal hide"  id="deleteSubjectArea"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteSubjectAreaConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
 	</div>
</div>

<!-- start ... Attach jaf jsp files -->
	<jsp:include page="jafinnercommon.jsp"></jsp:include>
<!-- end ... Attach jaf jsp files -->

<script type="text/javascript">//<![CDATA[
 var cal = Calendar.setup({
  onSelect: function(cal) { cal.hide() },
  showTime: true
 });
 //]]>
    
</script>

<script>
var dspqPortfolioNameId=${dspqPortfolioName.dspqPortfolioNameId};
var candidateType="${candidateType}";
var groupId=${group};
var iJobId=${iJobId};

getPortfolioHeader(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup03_21(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup03_8(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup03_9(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup03_10(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup03_11(iJobId,dspqPortfolioNameId,candidateType,groupId);

getGroup03_27(iJobId,dspqPortfolioNameId,candidateType,groupId);
getGroup03_28(iJobId,dspqPortfolioNameId,candidateType,groupId);

getGroup03(iJobId,dspqPortfolioNameId,candidateType,groupId);

function showcertiDiv()
{
	$('#draggableDivMaster').modal('show');
}

</script>