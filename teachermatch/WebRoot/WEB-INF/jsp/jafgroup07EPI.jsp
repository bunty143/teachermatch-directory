<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="javax.swing.Spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script>
var $j=jQuery.noConflict();
        $j(document).ready(function() {
       });
</script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/group01.js?ver=${resouceMap['js/jobapplicationflow/group01.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafInventory.js?ver=${resouceMap['js/jobapplicationflow/jafInventory.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jobapplflowcommon.js?ver=${resouceMap['js/jobapplicationflow/jobapplflowcommon.js']}"></script>

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />


<p id="portfolioHeader"></p>
<input type="hidden" id="sNextURL" name="sNextURL" value="${sNextURL}"/>
<input type="hidden" id="sPreviousURL" name="sPreviousURL" value="${sPreviousURL}"/>
<input type="hidden" id="sts" name="sts" value="${sts}"/>
<input type="hidden" id="at" name="sts" value="${at}"/>
<input type="hidden" id="sCurrentPageShortCode" name="sCurrentPageShortCode" value="${sCurrentPageShortCode}"/>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:30px;">
         <div style="float: left;">
         	<img src="images/affidavit.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="DSPQlblEPIInner"/></div>	
         </div>
         
         <div style="float: right;" class="hide" id="topRightInvDivId">
          	<div style='text-align:right; padding-right: 0px;' class="col-sm-3 col-md-3" id="topRightInvDivIdL2">
          		<button id="toprightbtnInvCompVlt" class='flatbtn hide' style='width:130px;'  type='button' onclick='return validateEPI(0);'>
          			<spring:message code="btnContinueDSPQ"/> <i class='icon'></i>
          		</button>
          		
          		<button id="toprightbtnInvL2" class='flatbtn hide' style='width:102px;'  type='button' onclick='return startEPI(0);'>
          			<spring:message code="btnContinueDSPQ"/> <i class='icon'></i>
          		</button>
          		
          		<button id="toprightbtnInvL3" class='flatbtn hide' style='width:102px;'  type='button' onclick='return startInventory();'>
          			<spring:message code="btnContinueDSPQ"/> <i class='icon'></i>
          		</button>
          		
          	</div>
         </div>
         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row">
	<div class="col-sm-12 col-md-12" id="epiDataAreaId" style="max-height: 200px;"></div>
</div>

<div class="hide" id="epiSuccessMsg">
	<table width="100%">
		<tr>
			<td width="5%" valign="top" style="padding-top: 0px; padding-left: 0px;">
				<div><img src='images/info.png' align='top'></div>
			</td>
			<td width="85%">
				<div><spring:message code="msgTeacherMatchBaseInventory"/></div>
			</td>
		</tr>
	</table>
</div>

<!--<div class="row mt10">
	<div class="col-sm-12 col-md-12" id="epiSuccessMsg1" style="height: 150px;"><spring:message code="msgTeacherMatchBaseInventory"/></div>
</div>
-->

<div class="hide" id="epiDataAreaIdL2">
	<table width="100%">
		<tr>
			<td width="5%" valign="top" style="padding-top: 0px;">
				<div id='jafwarningImg'></div>
			</td>
			<td width="85%">
				<div id='jafmessage2showConfirm'></div>
			</td>
		</tr>
	</table>
			
	<div class="row"><div class="col-sm-12 col-md-12" id='jafnextMsgk'></div></div>
	
	<div class='row'>
		<div class='col-sm-9 col-md-9 mt10'>
			<button class='flatbtn' id='btnprev' type='button' style='width:16%' onclick='return getPreviousPage();'><i class='backicon'></i> <spring:message code="btnPrevDSPQ"/></button>
			&nbsp;&nbsp;<button class='flatbtn' style='background: #da4f49;width:16%'  type='button' onclick='return cancelApplyJob();'><spring:message code="btnClr"/> <i class='fa fa-times-circle'></i></button>
		</div>
		
		<div class='col-sm-3 col-md-3'>
			<div class='mt10' style='text-align:right;' id="bottombtn">
				<button class='flatbtn' style='width:100px;' id='btnnext' type='button' onclick='return startInventory();'><spring:message code="btnContinueDSPQ"/> <i class='icon'></i></button>
			</div>
		</div>
	</div>
</div>
										
<p id="epiFooter"></p>

<!-- start ... Attach jaf jsp files -->
	<jsp:include page="jafinnercommon.jsp"></jsp:include>
<!-- end ... Attach jaf jsp files -->

<div class="modal hide"  id="epiVltMsg"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					<spring:message code="jafportfoflioCloseVltMsg" />
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	     <span id=""><button class="btn  btn-primary" onclick="CloseEPIVltMsg()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button> 		
	 	</div>
	  </div>
	</div>
</div>

<script type="text/javascript">//<![CDATA[
 var cal = Calendar.setup({
  onSelect: function(cal) { cal.hide() },
  showTime: true
 });
 //]]>
    
</script>

<style>
.jafTop150
{
	padding-top: 180px;
}
</style>

<script>

var dspqPortfolioNameId=${dspqPortfolioName};
var candidateType="${candidateType}";
var groupId=${group};
var iJobId=${iJobId};

getPortfolioHeader(iJobId,dspqPortfolioNameId,candidateType,groupId);
getEPIData(iJobId,dspqPortfolioNameId,candidateType,groupId);
//getEPIFooter(iJobId,dspqPortfolioNameId,candidateType,groupId);
jafEPISuccessMsg();

$( document ).ready(function() 
{
	var txtEPIStatus="";
	try { txtEPIStatus=document.getElementById("txtEPIStatus").value; } catch (e) {}
	//toprightEPIbtnCompVlt
	$('#topRightInvDivId').show();
	if(txtEPIStatus=="" || txtEPIStatus=="icomp")
	{
		$('#toprightbtnInvCompVlt').hide();
		$('#toprightbtnInvL2').show();
		$('#toprightbtnInvL3').hide();
	}
	else if(txtEPIStatus=="comp" || txtEPIStatus=="vlt")
	{
		$('#toprightbtnInvCompVlt').show();
		$('#toprightbtnInvL2').hide();
		$('#toprightbtnInvL3').hide();
		setTimeout("validateEPI(0)", 5000);
	}
});

</script>