<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/ParticipantsAjax.js?ver=${resourceMap['ParticipantsAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/participants.js?ver=${resourceMap['js/participants.js']}"></script>
<script type="text/javascript" src="dwr/interface/CGInviteInterviewAjax.js?ver=${resourceMap['CGInviteInterviewAjax.ajax']}"></script>


<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css"	href="css/base.css?ver=${resouceMap['css/base.css']}" />
<style>
	.hide
	{
		display: none;
	}
</style>

<style>
.btn-dangerevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-dangerevents:hover,
.btn-dangerevents:focus,
.btn-dangerevents:active,
.btn-dangerevents.active,
.btn-dangerevents.disabled,
.btn-danger[disabled] {
  color: #ffffff;
  background-color: #bd362f;
  *background-color: #a9302a;
}

.btn-dangerevents:active,
.btn-dangerevents.active {
  background-color: #942a25 \9;
}

.btn-warningevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #faa732;
  *background-color: #f89406;
  background-image: -moz-linear-gradient(top, #fbb450, #f89406);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#fbb450), to(#f89406));
  background-image: -webkit-linear-gradient(top, #fbb450, #f89406);
  background-image: -o-linear-gradient(top, #fbb450, #f89406);
  background-image: linear-gradient(to bottom, #fbb450, #f89406);
  background-repeat: repeat-x;
  border-color: #f89406 #f89406 #ad6704;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fffbb450', endColorstr='#fff89406', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-warningevents:hover,
.btn-warningevents:focus,
.btn-warningevents:active,
.btn-warningevents.active,
.btn-warningevents.disabled,
.btn-warningevents [disabled] {
  color: #ffffff;
  background-color: #f89406;
  background-color: #df8505;
}

.btn-warningevents:active,
.btn-warningevents.active {
  background-color: #c67605 \9;
}
strong {
font-weight: normal;
}

</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#participantsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[175,175,275,185,135],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function applyScrollOnCandidateTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#candidatesGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[35,205,250,305,150],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function applyScrollOnProsTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#prosGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[35,281,281,348],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function applyScrollOnTempTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tempGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
       	colratio:[175,175,300,295],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}
</script> 
	<input type="hidden" id="gridVal"/>
	<input type="hidden" id="eventId" value="${eventDetails.eventId}"/>
	<input type="hidden" id="eventTypeId" value="${eventDetails.eventTypeId.eventTypeId}"/>
	<input type="hidden" id="formatId" value="${eventDetails.eventTypeId.eventTypeId}"/>
	<input type="hidden" id="sessionId" value=""/>
		<div id="mainDivOpen">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
        <div style="float: left;">
        	<img src="images/add-edituser.png" width="41" height="41" alt="">
        </div>        
        <div style="float: left;">
        	<div class="subheading" style="font-size: 13px;">Invite Candidate/Prospects</span></div>	
        </div>    
        
         <div class="pull-right add-employment1" id="addParticipantsDiv">
			<%-- 	<a href="javascript:void(0);" onclick="showSelectionGrid();">+ Add Participant(s)</a> --%>
		 </div>			
              			
	<div style="clear: both;"></div>	
    <div class="centerline"></div>
</div>
<div id="errSendInviteDiv" class="required mt10 hide" style="margin-left: 15px; "></div> 
 <div class="row">  
   <div id="errorDivCandidate" class="required mt10" style="margin-left: 15px; "></div> 
		<div id="partselectiongrid" class="mt10 hide">
			<div class="col-sm-3 col-md-3">
				<label class="radio"><input type="radio" id="selCand" onclick="showselgrid('selCand');" name="selType"> Select Candidates</label>			
			</div>
			<div class="col-sm-3 col-md-3">
				<label class="radio"><input type="radio" id="selPros" onclick="showselgrid('selPros');" name="selType"> Select Prospects</label>			
			</div>
			<div class="col-sm-3 col-md-3">
				<label class="radio"><input type="radio" id="selImport" onclick="showselgrid('selImport');" name="selType">Import Prospects</label>			
			</div>
			<div class="col-sm-3 col-md-3">		
				<label class="radio"><input type="radio" id="addPros" onclick="showselgrid('addPros');" name="selType">Enter Prospect</label>	
			</div>			
	</div>
</div>  
   
   
   <div class="row">
			 	<div class="col-sm-10 col-md-10">
				 	    		<label>Candidates</label><div class="right" style="margin-top: -20px;" id="impCan"><a href="javascript:void(0);" onclick="showselgridImport();">Import</a></div>
				         		<input type="text" id="searchTxt" name="searchTxt"  class="form-control"
					         		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'searchTxt','searchTxtId','');"
									onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'searchTxt','searchTxtId','');"
									onblur="hideDistrictMasterDiv(this,'searchTxtId','divTxtShowData');"	/>
									<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','searchTxt')" class='result' ></div>
								<input type="hidden" id="searchTxtId"/>
				 </div>
				 <div class="col-sm-2 col-md-2 top25">
				 	    		<button class="btn btn-primary" type="button" onclick="saveCandidate(0);">&nbsp;&nbsp;Add &nbsp;&nbsp;<i class="icon"></i>&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;
				 </div>
	</div> 
	
	<div class="row">
			 	<div class="col-sm-10 col-md-10">
				 	    		<label>Prospects</label><div class="right" style="margin-top: -20px;"><a href="javascript:void(0);" onclick="showselgrid('addPros');" >Add New</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="showselgridImport();" id="impPros">Import</a></div>
				         		<input type="text" id="participantTxt" name="participantTxt"  class="form-control"
					         		onfocus="getProspectAutoComp(this, event, 'divTxtShowData1', 'participantTxt','participantId','');"
									onkeyup="getProspectAutoComp(this, event, 'divTxtShowData1', 'participantTxt','participantId','');"
									onblur="hideProspectDiv(this,'participantId','divTxtShowData1');"	/>
									<div id='divTxtShowData1' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData1','participantTxt')" class='result' ></div>
								<input type="hidden" id="participantId"/>
				 </div>
				 <div class="col-sm-2 col-md-2 top25">
				 				<button class="btn btn-primary" type="button" onclick="saveCandidate(1);">&nbsp;&nbsp;Add &nbsp;&nbsp;<i class="icon"></i>&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;
				 </div>
	</div>  

<div class="row">
    <div class="col-sm-10 col-md-10">
		<div id="partgrid" class="mt10"></div>
	</div>
</div>

<!-- NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNEEEWWWWWWWWWW -->
<!--********************** Main Grid *************************-->

	<div class="row col-sm-12 col-md-12">                
		<div id="participantsNewDiv"  class="row"></div>
	</div>
	
	<div class="clearfix">&nbsp;</div>
           





<%-- 
<div class="row">  
   <div id="errorDivCandidate" class="required mt10" style="margin-left: 15px; "></div> 
		<div id="partselectiongrid" class="mt10 hide">
			<div class="col-sm-3 col-md-3">
				<label class="radio"><input type="radio" id="selCand" onclick="showselgrid('selCand');" name="selType"> Select Candidates</label>			
			</div>
			<div class="col-sm-3 col-md-3">
				<label class="radio"><input type="radioz" id="selPros" onclick="showselgrid('selPros');" name="selType"> Select Prospects</label>			
			</div>
			<c:if test="${eventDetails.eventFormatId.eventFormatId ne 1 && importacilitatorCheck ne 1}">
			<div class="col-sm-3 col-md-3">
				<label class="radio"><input type="radio" id="selImport" onclick="showselgrid('selImport');" name="selType">Import Prospects</label>			
			</div>
			</c:if>
			<div class="col-sm-3 col-md-3">		
				<label class="radio"><input type="radio" id="addPros" onclick="showselgrid('addPros');" name="selType">Enter Prospect</label>	
			</div>			
	</div>
</div>
--%>
    
<div id="selCandDis" class="hide">
	<div class="divErrorMsg" style="display:block" id="canError"></div>
		<div class="row">
			<div class="col-sm-3 col-md-3" style="">		
				<label class="radio"><input type="radio" id="selAllCan" name="selJobCan" checked="checked" name="selAllCan">All</label>	
			</div>
			<div class="col-sm-6 col-md-6">
				<div class="col-sm-6 col-md-6" ><label class="radio" style="margin-left: -14px;"><input type="radio" id="selJobCan" name="selJobCan">Associated With Job#</label></div>
				<div class="col-sm-6 col-md-6"><input type="textbox" name="searchJob" id="searchJob" value="" class="form-control" onkeypress="return checkForInt(event);" placeholder="Please Enter Job Id"></div>
			</div>
			<div class="col-sm-3 col-md-3">		
				<button class="btn btn-primary top5" type="button" onclick="searchTeacher()" style="width: 95px;">Search<i class="icon"></i></button>	
			</div>
		</div>
		 <div class="row">
			 	<div class="col-sm-3 col-md-3">
			 	 <label>
				    First Name
				  </label>
				  <input id="firstName" name="firstName" maxlength="50" class="help-inline form-control" type="text" />
			    </div>
			     <div class="col-sm-3 col-md-3">
			 	 <label>
				    Last Name
				  </label>
				  <input id="lastName" name="lastName" maxlength="50" class="help-inline form-control" type="text" />
			    </div>
		     <div class="col-sm-6 col-md-6">
		 	 <label>
			    Email Address
			  </label>
			  <input id="emailAddress" name="emailAddress" maxlength="50" class="help-inline form-control" type="text" />
		    </div>
		</div>
		<div class="row top20">
		<div class="col-sm-10 col-md-10">
			<div id="selCandDisData"></div>
			</div>			
		</div>
</div>
<div id="selProsdDis" class="mt10 hide">
         <div class="row">
			 	<div class="col-sm-3 col-md-3">
				 	 <label>
					    First Name
					 </label>
					 <input id="firstNamePros" name="firstNamePros" maxlength="50" class="help-inline form-control" type="text" />
			     </div>
			     <div class="col-sm-3 col-md-3">
				 	 <label>
					    Last Name
					  </label>
					  <input id="lastNamePros" name="lastNamePros" maxlength="50" class="help-inline form-control" type="text" />
			    </div>
		        <div class="col-sm-4 col-md-4">
		 	          <label>
			            Email Address
			          </label>
			          <input id="emailAddressPros" name="emailAddressPros" maxlength="75" class="help-inline form-control" type="text" />
		       </div>
		    
		      <div class="col-sm-2 col-md-2 top25-sm2" style="width: 150px;">
		        <label class=""></label>
		        <button class="btn btn-primary " type="button" onclick=" searchCandidateFromProspects();">&nbsp;Search&nbsp; <i class="icon"></i></button>
		      </div>
		</div>

	<div class="row top20">
			<div class="col-sm-10 col-md-10" ><div id="selProsDisData"></div></div>
		</div>
</div>

<!--<div id="selImportDis" class="mt10 hide">
	<div class="row">
	<div id="errorDivImport" class="required"></div>
	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='facilitatorUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='facilitatorUploadServlet.do' class="form-inline">
		<div class="col-sm-1 col-md-1" id="selImportDisData">
			Browse :
		</div>
			<div class="col-sm-3 col-md-3" id="selImportDisData">
				<label clas="radio"><input name="teacherfile" id="facilitatorfile" type="file"> </label>
			</div>
			<div class="col-sm-3 col-md-3" id="selImportDisData">
				<button class="btn btn-primary fl" type="button" onclick="validateFacilitatorFile();" style="width: 100px;">Import <i class="icon"></i></button>
			</div>
		</div>
		</form>
		
		<div class="row"> 
      <div class="col-sm-8 col-md-8 mt10" style="margin-right: 5px;">
                  <table>
                  <tr><td></td><td class="required">NOTE:</td></tr>
                  <tr><td valign="top">*</td><td> Make sure that first row of excel file (xls/xlsx) has the column names and subsequent rows has data.</td></tr>
                  <tr><td valign="top">*</td><td>Excel file must have following column names -</td></tr>
                  <tr><td></td><td>participantFirstName</td></tr>
                  <tr><td></td><td>participantLastName</td></tr>
                  <tr><td></td><td>participantEmailAddress</td></tr>
                  <tr><td valign="top">*</td><td>Column names are case insensitive.</td></tr>
            </table>
      </div>
      </div>
		
</div>

-->


<div class="row top10">

	 <div class="col-sm-6 col-md-6">
			<c:if test="${eventDetails.eventTypeId.eventTypeId ne 1}">
			  <button class="btn btn-primary" type="button" onclick="sendEmails()">Send Invite&nbsp;<i class="icon"></i></button>
			</c:if>
		
			<c:if test="${eventDetails.eventTypeId.eventTypeId eq 1}">
			  <button class="btn  btn-primary" id="sendInviteBtn" onclick="sendInvite();" >Send Invite&nbsp;<i class="icon"></i></button>
			</c:if>
		&nbsp;&nbsp;<button class="btn btn-dangerevents" data-dismiss="modal" aria-hidden="true" onclick='exitfrompage();'><strong>Cancel&nbsp;</strong><i class="icon"></i></button>
	</div>
	
	<div class="col-sm-6 col-md-6" style="text-align: right;">
		<button class="btn btn-primary" type="button" onclick="checkPageRedirect()"><i class="backicon"></i> Back &nbsp;</button>
	</div>
	

</div>

</div>

<!--<div id="tempIdopen" class="hide mt10">
	<div class="row">
		<div class="col-sm-10 col-md-10" >
			<div id="tempGridData"></div>
		</div>
	</div>

<div class="row mt10">
<div class="col-sm-12 col-md-12 disButtons">
<button class="btn btn-primary" type="button" onclick="acceptImport()" style="width: 95px;">Accept<i class="icon"></i></button>&nbsp;&nbsp;
<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='clearAllTempRec();'>Cancel</button>
</div>
</div>
</div>
--><!-- modal divs start -->
<div style="display:none; z-index: 5000;" id="loadingDiv" >
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'>
	 		<td style='padding-top:0px;" id='spnMpro' align='center'>
	 		</td>
 		</tr>
	</table>
</div>
<div class="modal hide"  id="saveDataLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group tempMsg">
			Data has been saved successfully. 
		</div>
 	</div>
 	<div class="modal-footer">
 	
 		<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true">ok</button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="AddNewPros" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 800px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3>TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group tempMsg">
			 <div id="addProsdDis" class="mt10 ">
				<div class="divErrorMsg" style="display:block" id="addProsError"></div>
				<div class="row">
					<div class="col-sm-3 col-md-3">
						<label>First Name<span class="required">*</span></label>
						<input type="text" class="form-control" id="fName" size="50"/>
					</div>
					<div class="col-sm-3 col-md-3">
						<label>Last Name<span class="required">*</span></label>
						<input type="text" class="form-control" id="lName" size="50"/>
					</div>
					<div class="col-sm-6 col-md-6">
						<label>Email Address<span class="required">*</span></label>
						<input type="text" class="form-control" id="emailAddress1" size="75"/>
					</div>		
				</div>
				<div class="row">		
					<div class="col-sm-12 col-md-12 idone" style="padding-top: 10px;">
						<a id="hrefDone" href="javascript:void(0);" style="cursor: pointer; margin-left: 0px; text-decoration: none;" onclick="addProsCand()">Save Section</a>&nbsp;&nbsp;
						<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration: none;" onclick="return hideForm()">Cancel</a>
					</div>
				</div>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 	
 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>
	</div>
</div>


<div class="modal hide"  id="importProspectDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 850px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3>TeacherMatch</h3>
	</div>
	<div class="modal-body"> 
		<div class="control-group tempMsg">
					<div id="" class="mt10">
						<div class="row">
							<div id="errorDivImport" class="left20 required"></div>
							<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
						    </iframe>
							<form id='facilitatorUploadServlet' enctype='multipart/form-data'
								method='post' target='uploadFrame'
								action='facilitatorUploadServlet.do' class="form-inline">
								<div class="col-sm-1 col-md-1" id="selImportDisData">
									Browse:
								</div>
								<div class="col-sm-4 col-md-4" id="selImportDisData">
									<label clas="radio">
										<input name="teacherfile" id="facilitatorfile" type="file">
									</label>
								</div>
								<div class="col-sm-3 col-md-3 left5" id="selImportDisData">
									<button class="btn btn-primary fl" type="button"
										onclick=
											validateFacilitatorFile();
										style="width: 100px;">
										Import
										<i class="icon"></i>
									</button>
								</div>
							</form>
						</div>


						<div class="row">
							<div class="col-sm-8 col-md-8 mt10" style="margin-right: 5px;">
								<table>
									<tr>
										<td></td>
										<td class="required">
											NOTE:
										</td>
									</tr>
									<tr>
										<td valign="top">
											*
										</td>
										<td>
											Make sure that first row of excel file (xls/xlsx) has the
											column names and subsequent rows has data.
										</td>
									</tr>
									<tr>
										<td valign="top">
											*
										</td>
										<td>
											Excel file must have following column names -
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											participantFirstName
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											participantLastName
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											participantEmailAddress
										</td>
									</tr>
									<tr>
										<td valign="top">
											*
										</td>
										<td>
											Column names are case insensitive.
										</td>
									</tr>
								</table>
							</div>
						</div>

					</div>
				</div>
 	</div>
 	<div class="modal-footer">
 	
 		<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>
	</div>
</div>



<div class="modal hide"  id="deletePartDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
		   Do you really want to remove the participant from this event?
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="delteId">
 	
 	<span id=""><button class="btn  btn-primary" onclick="doneDelete();">Yes <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
 	</div>
</div>
	</div>
</div>
<!-- modal divs end -->
<div class="modal hide"  id="sendEmailtoParticipants" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="return exitfrompage();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group tempMsg" id="succesEmailMsg">
			Great! Event has been scheduled
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn  " data-dismiss="modal" aria-hidden="true" onclick="return exitfrompage();">Close</button> 		
 	</div>
</div>
	</div>
</div>


<div style="clear: both;"></div>

   <div class="modal hide"  id="printOfferReadyDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1005px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();">x</button>
	   	<h3 id="">TeacherMatch</h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="pritOfferReadyDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='acceptImport();'>&nbsp;&nbsp;&nbsp;Accept&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='clearAllTempRec();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>


 <div style="display:none;" id="loadingDiv1">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
<!--javascript start-->
<script type="text/javascript">
	 $j( document ).ready(function() {

   // participantsGrid();
    checkForRecord();
    checkInviteInterviewButton();
    chkVideo();
    
    participantsGridNew();
    
});

 </script>