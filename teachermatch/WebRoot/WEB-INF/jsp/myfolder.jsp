<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
 <%@ page import="tm.utility.Utility" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/myfolder.js?ver=${resourceMap['js/myfolder.js']}"></script>
<script type="text/javascript" src="js/tmcommon.js?ver=${resourceMap['js/tmcommon.js']}">	
</script>

	<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js?ver=${resourceMap['CandidateReportAjax.Ajax']}"></script>
	<script type="text/javascript" src="dwr/interface/CandidateGridAjax.js?ver=${resourceMap['CandidateGridAjax.Ajax']}"></script>
	<script type="text/javascript" src="dwr/interface/CandidateGridSubAjax.js?ver=${resourceMap['CandidateGridSubAjax.Ajax']}"></script>
	<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>
	<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resourceMap['TeacherProfileViewInDivAjax.Ajax']}"></script>
	<script type="text/javascript" src="dwr/interface/TeacherInfotAjax.js?ver=${resourceMap['TeacherInfotAjax.Ajax']}"></script>
	<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resourceMap['PFCertifications.Ajax']}"></script>
	
	<script type="text/javascript" src="dwr/engine.js"></script>
	<script type='text/javascript' src='dwr/util.js'></script>
	<script type='text/javascript' src="js/report/candidategrid.js?ver=${resourceMap['js/report/candidategrid.js']}"></script>
	
	<script src="jquery/jquery-ui.custom.js" type="text/javascript"></script>
	<script src="jquery/jquery.cookie.js" type="text/javascript"></script>

	<link href="jquery/skin/ui.dynatree.css" rel="stylesheet" type="text/css">
	<script src="jquery/jquery.dynatree.js" type="text/javascript"></script>

	
	<!-- jquery.contextmenu,  A Beautiful Site (http://abeautifulsite.net/) -->
	<script src="contextmenu/jquery.contextMenu-custom.js" type="text/javascript"></script>
	<link href="contextmenu/jquery.contextMenu.css" rel="stylesheet" type="text/css" >

	<!-- Start_Exclude: This block is not part of the sample code -->
	<link href="contextmenu/prettify.css" rel="stylesheet">
	<script src="contextmenu/prettify.js" type="text/javascript"></script>
	<link href="contextmenu/sample.css" rel="stylesheet" type="text/css">
	<script src="contextmenu/sample.js" type="text/javascript"></script>
	<!-- End_Exclude -->
	<!-- 
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome-ie7.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome-ie7.min.css" rel="stylesheet" type="text/css">
	<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
	 -->
	 
    <link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
    <script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
    
    
<script type="text/javascript" src="dwr/interface/SelfServiceCandidateProfileService.js?ver=${resourceMap['SelfServiceCandidateProfileService.ajax']}"></script>
<script type='text/javascript' src="js/report/sspfCandidategridnew.js?ver=${resourceMap['js/report/sspfCandidategridnew.js']}"></script>
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resourceMap['CGServiceAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax .ajax']}"></script>
    

<script><!--
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnHonors()
{
	
	   //	var $j=jQuery.noConflict();
	   	 $(document).ready(function() {
	   	
      $('#honorsGrid').fixheadertable({ //table id 
      caption: '',
      showhide: false,
      theme: 'ui',
      height: 350,
      width: 830,
      minWidth: null,
      minWidthAuto: false,
      colratio:[250,250,175,176,111], // table header width
      addTitles: false,
      zebra: true,
      zebraClass: 'net-alternative-row',
      sortable: false,
      sortedColId: null,
      //sortType:[],
      dateFormat: 'd-m-y',
      pager: false,
      rowsPerPage: 10,
      resizeCol: false,
      minColWidth: 100,
      wrapper: false
      });            
      });
}
 function applyScrollOnLEACandidatePorfolio()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLEACandidatePorfolioGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
       	<c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[200,100,200,150,100,80],
           </c:when>
           <c:otherwise>
             colratio:[200,100,150,150,80,50],
           </c:otherwise>
        </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


 function applyScrollOnTblLicenseDate()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertificationsGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[400,220,210],
           </c:when>
           <c:otherwise>
            colratio:[350,220,160],
           </c:otherwise>
        </c:choose>
       	addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


function applyScrollOnInvl()
{

	//	var $j=jQuery.noConflict();
      $(document).ready(function() {
    	   	  
      $('#involvementGrid').fixheadertable({ //table id 
      caption: '',
      showhide: false,
      theme: 'ui',
      height: 350,
      width: 830,
      minWidth: null,
      minWidthAuto: false,
      colratio:[250,250,175,176,111], // table header width
      addTitles: false,
      zebra: true,
      zebraClass: 'net-alternative-row',
      sortable: false,
      sortedColId: null,
      //sortType:[],
      dateFormat: 'd-m-y',
      pager: false,
      rowsPerPage: 10,
      resizeCol: false,
      minColWidth: 100,
      wrapper: false
      });            
      });
}
function applyScrollOnTblBackgroundCheck_profile()
{
        $(document).ready(function() {
        $('#tbleBgCheck_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
	    width: 830,
        minWidth: null,
        minWidthAuto: false,
	    colratio:[332,243,255],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function showReferencesForm()
{
	 $("#divElectronicReferences").show();
	 resetReferenceform();
	 document.getElementById("salutation").focus();
}
//======================= Mukesh References ===============================
function insertOrUpdateElectronicReferencesM(sbtsource)
{
	var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	 var firstName="";
	 var lastName="";
	 		//resetReferenceform();   
	 		var referenceDetails	= trim($('#referenceDetailstext').find(".jqte_editor").html());
			var teacherId		=   trim(document.getElementById("teacherId").value);
			var elerefAutoId	=	document.getElementById("elerefAutoId");
			var salutation		=	document.getElementById("salutation");
			
			if(myfolder==1)
			{
			 firstName		=	trim(document.getElementById("firstName1").value);
			 lastName		= 	trim(document.getElementById("lastName1").value);
			}else{
			 firstName		=	trim(document.getElementById("firstName").value);
			 lastName		= 	trim(document.getElementById("lastName").value);
			}
			var designation		= 	trim(document.getElementById("designation").value);
			
			var organization	=	trim(document.getElementById("organization").value);
			var email			=	trim(document.getElementById("email").value);
			var longHaveYouKnow	=	trim(document.getElementById("longHaveYouKnow").value);
			var contactnumber	=	trim(document.getElementById("contactnumber").value);
			var rdcontacted0	=   document.getElementById("rdcontacted0");
			var rdcontacted1	=   document.getElementById("rdcontacted1");
			var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
			
			var cnt=0;
			var focs=0;	
			
			var fileName = pathOfReferencesFile.value;
			var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
			var fileSize=0;		
			if ($.browser.msie==true){	
			    fileSize = 0;	   
			}else{		
				if(pathOfReferencesFile.files[0]!=undefined)
				fileSize = pathOfReferencesFile.files[0].size;
			}
			$('#errordivElectronicReferences').empty();
			setDefColortoErrorMsgToElectronicReferences();
			if(firstName=="")
			{
				$('#errordivElectronicReferences').append("&#149; <spring:message code='msgPlzEtrFname'/> <br>");
				if(focs==0)
				{ 
					if(myfolder==1)
					 $('#firstName1').focus();
					else
					 $('#firstName').focus();
				}
				if(myfolder==1)
				 $('#firstName1').css("background-color",txtBgColor);
				else
				 $('#firstName1').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(lastName=="")
			{
				$('#errordivElectronicReferences').append("&#149; <spring:message code='msgPlzEtrLname'/><br>");
				if(focs==0){
					if(myfolder==1)
					  $('#lastName1').focus();
					else
					  $('#lastName').focus();
				}
				if(myfolder==1)
				 $('#lastName1').css("background-color",txtBgColor);
				else
				 $('#lastName').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(email=="")
			{
				$('#errordivElectronicReferences').append("&#149; <spring:message code='msgPlzEtrEmail'/><br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			else if(!isEmailAddress(email))
			{		
				$('#errordivElectronicReferences').append("&#149; <spring:message code='msgPlzEtrValidEmail'/> <br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}if(contactnumber=="")
			{
				$('#errordivElectronicReferences').append("&#149; <spring:message code='msgPleaseEnterContactNumber'/><br>");
				if(focs==0)
					$('#contactnumber').focus();
				
				$('#contactnumber').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(organization=="")
			{
				$('#errordivElectronicReferences').append("&#149; <spring:message code='msgPlZEnterOrganization'/><br>");
				if(focs==0)
					$('#organization').focus();
				
				$('#organization').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(longHaveYouKnow=="" && $("#districtId").val()==4218990)
			{
				$('#errordivElectronicReferences').append("&#149; <spring:message code='msgHowLongKnownPerson'/><br>");
				if(focs==0)
					$('#longHaveYouKnow').focus();
				
				$('#longHaveYouKnow').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			var rdcontacted_value;
			
			if (rdcontacted0.checked) {
				rdcontacted_value = false;
			}
			else if (rdcontacted1.checked) {
				rdcontacted_value = true;
			}
			
			if(ext!=""){
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errordivElectronicReferences').append("&#149; <spring:message code='msgAcceptableFileFormats'/><br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}	
				else if(fileSize>=10485760)
				{
					$('#errordivElectronicReferences').append("&#149; <spring:message code='msgFileSizeMustLess10mb'/><br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}
			}
			

			if(cnt!=0)		
			{
				$('#errordivElectronicReferences').show();
				return false;
			}
			else
			{
				var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null};
				dwr.engine.beginBatch();
				
				dwr.util.getValues(teacherElectronicReferences);
				teacherElectronicReferences.rdcontacted=rdcontacted_value;
				teacherElectronicReferences.salutation=salutation.value;
				teacherElectronicReferences.firstName=firstName;
				teacherElectronicReferences.lastName=lastName;;
				teacherElectronicReferences.designation=designation;
				teacherElectronicReferences.organization=organization;
				teacherElectronicReferences.email=email;
				teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
				teacherElectronicReferences.referenceDetails=referenceDetails;
				
				var refFile = document.getElementById("pathOfReference").value;
				if(refFile!=""){
					teacherElectronicReferences.pathOfReference=refFile;
				}
				if(fileName!="")
				{	
					
					PFCertifications.findDuplicateReferences(teacherElectronicReferences,teacherId,{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
						if(data=="isDuplicate")
						{
							$('#errordivElectronicReferences').append("&#149; <spring:message code='msgElectronicReferenceRegistered'/><br>");
							if(focs==0)
								$('#email').focus();
							$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
							$('#errordivElectronicReferences').show();
							return false;
						}else{
							document.getElementById("frmElectronicReferences").submit();
						}
						}
					});
					
				}else{
					PFCertifications.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
						async: true,
						errorHandler:handleError,
						callback: function(data)
						{
							if(data=="isDuplicate")
							{
								$('#errordivElectronicReferences').append("&#149; <spring:message code='msgElectronicReferenceRegistered1'/> <br>");
								if(focs==0)
									$('#email').focus();
								$('#email').css("background-color",txtBgColor);
									cnt++;focs++;
								$('#errordivElectronicReferences').show();
								return false;
							}
							hideElectronicReferencesForm();
							getElectronicReferencesGrid_DivProfile(teacherId);
						}
					});
				}
				
				dwr.engine.endBatch();
				return true;
			}
			
}

function applyScrollOnTblLanguage()
{
    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[277,276,276],
           </c:when>
           <c:otherwise>
             colratio:[244,243,243],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}

function setDefColortoErrorMsgToElectronicReferences()
{
	var myfolder=0;
	var sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	$('#salutation').css("background-color","");
	
	if(myfolder==1){
	 	$('#firstName1').css("background-color","");
	 	$('#lastName1').css("background-color","");
	}else{
		$('#firstName').css("background-color","");
	 	$('#lastName').css("background-color","");
	}
	
	$('#designation').css("background-color","");
	$('#organization').css("background-color","");
	$('#email').css("background-color","");
	$('#contactnumber').css("background-color","");
}

function hideElectronicReferencesForm()
{
	resetReferenceform();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="none";
	//resetReferenceform();
	return false;
}
function resetReferenceform()
{
	var myfolder=0;
	var sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	    document.getElementById("rdcontacted1").checked = true
	 	document.getElementById("salutation").value=0;
	    if(myfolder==1){
			document.getElementById("firstName1").value='';
			document.getElementById("lastName1").value='';
	    }else{
	    	document.getElementById("firstName").value='';
			document.getElementById("lastName").value='';
	    }
		document.getElementById("designation").value='';
		document.getElementById("organization").value='';
		document.getElementById("email").value='';
		document.getElementById("contactnumber").value='';
		$('#referenceDetailstext').find(".jqte_editor").html("");
		document.getElementById("pathOfReferenceFile").value="";
		document.getElementById("pathOfReferenceFile").value=null;
}


function saveNobleReference(fileName)
{
    var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	    var firstName="";
	    var lastName="";
       var referenceDetailstext	= trim($('#referenceDetailstext').find(".jqte_editor").text());
		var teacherId		=   trim(document.getElementById("teacherId").value);	
	    var elerefAutoId	=	document.getElementById("elerefAutoId");
		var salutation		=	document.getElementById("salutation");
		if(myfolder==1){
		 firstName		=	trim(document.getElementById("firstName1").value);
		 lastName		= 	trim(document.getElementById("lastName1").value);
		}else{
			 firstName		=	trim(document.getElementById("firstName").value);
			 lastName		= 	trim(document.getElementById("lastName").value);
		}
		var designation		= 	trim(document.getElementById("designation").value);
		var longHaveYouKnow = 	trim(document.getElementById("longHaveYouKnow").value);
		
		var organization	=	trim(document.getElementById("organization").value);
		var email			=	trim(document.getElementById("email").value);
		
		var contactnumber	=	trim(document.getElementById("contactnumber").value);
		var rdcontacted0	=   document.getElementById("rdcontacted0");
		var rdcontacted1	=   document.getElementById("rdcontacted1");
		
		var cnt=0;
		var focs=0;	
		
		var rdcontacted_value;
		if (rdcontacted0.checked) {
			rdcontacted_value = false;
		}
		else if (rdcontacted1.checked) {
			rdcontacted_value = true;
		}
		
		var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,pathOfReference:null,longHaveYouKnow:null};
		dwr.engine.beginBatch();
	
		dwr.util.getValues(teacherElectronicReferences);
		teacherElectronicReferences.rdcontacted=rdcontacted_value;
		teacherElectronicReferences.salutation=salutation.value;
		teacherElectronicReferences.firstName=firstName;
		teacherElectronicReferences.lastName=lastName;;
		teacherElectronicReferences.designation=designation;
		teacherElectronicReferences.organization=organization;
		teacherElectronicReferences.email=email;
		teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
		teacherElectronicReferences.pathOfReference=fileName;
		teacherElectronicReferences.referenceDetails=referenceDetailstext;
		

		PFCertifications.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data=="isDuplicate")
				{
					///updateReturnThreadCount("ref");
					$('#errordivElectronicReferences').append("&#149; <spring:message code='msgElectronicReferenceRegistered1'/> <br>");
					if(focs==0)
						$('#email').focus();
					$('#email').css("background-color",txtBgColor);
						cnt++;focs++;
					$('#errordivElectronicReferences').show();
					return false;
				}
				hideElectronicReferencesForm();
				getElectronicReferencesGrid_DivProfile(teacherId);
				
			}
		});
	
		dwr.engine.endBatch();
		return true;
	}
function editFormElectronicReferences(id)
{
	var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;

	 $('#errordivElectronicReferences').empty();
	 setDefColortoErrorMsgToElectronicReferences();
	 PFCertifications.editElectronicReferences(id,
		{ 
		  async: false,
		  errorHandler:handleError,
		  callback: function(data)
		  {
	        showReferencesForm();
	        dwr.util.setValues(data);
	      
	        if(sUrl.indexOf("myfolder") > -1){
	    	  document.getElementById("firstName1").value  =data.firstName;
			  document.getElementById("lastName1").value	   =data.lastName;
	        }
		       
			$('#referenceDetailstext').find(".jqte_editor").html(data.referenceDetails);
			if(data.pathOfReference!=null && data.pathOfReference!="")
			{	
				 document.getElementById("pathOfReference").value=data.pathOfReference;
			}else{
				document.getElementById("pathOfReference").value="";
			}
			
			return false;
			}
		});
	return false;
}

function removeReferences()
{
	var teacherId =  document.getElementById("teacherId").value
	var referenceId = document.getElementById("elerefAutoId").value;
	if(window.confirm("<spring:message code='msgDeleteRecommendationLetter'/>"))
	{
		PFCertifications.removeReferencesForNoble(referenceId,teacherId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				getElectronicReferencesGrid_DivProfile(teacherId);
			//	document.getElementById("removeref").style.display="none";
				hideElectronicReferencesForm();
				
			}
		});
	}
}
var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	if(status==1)
	$('#chgstatusRef1').modal('show');
	else if(status==0)
	 $('#chgstatusRef2').modal('show');	
}
function changeStatusElectronicReferencesConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');
	
		PFCertifications.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			    getElectronicReferencesGrid_DivProfile(teacherId);
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}


function setDefColortoErrorMsgToVideoLinksForm()
{
	$('#videourl').css("background-color","");
}
function showVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="block";
	document.getElementById("videourl").focus();
	setDefColortoErrorMsgToVideoLinksForm();
	$('#errordivvideoLinks').empty();
	return false;
}

function hideVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="none";
	return false;
}

function insertOrUpdatevideoLinks()
{
	 
	 var teacherId =  document.getElementById("teacherId").value
	var elerefAutoId = document.getElementById("videolinkAutoId");
	var videourl = trim(document.getElementById("videourl").value);
	var cnt=0;
	var focs=0;	
	
	$('#errordivvideoLinks').empty();
	setDefColortoErrorMsgToVideoLinksForm();
	
	if(videourl=="")
	{
		$('#errordivvideoLinks').append("&#149; <spring:message code='PlzEtrVideoLink'/> <br>");
		if(videourl==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt!=0 )		
	{
		$('#errordivvideoLinks').show();
		return false;
	}
	else
	{	
		var teacherVideoLink = {videolinkAutoId:null,videourl:null, createdDate:null};
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherVideoLink);
		teacherVideoLink.videourl=videourl;
		PFCertifications.saveOrUpdateVideoLink(teacherVideoLink,teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				hideVideoLinksForm();
				getVideoLinksGrid_DivProfile(teacherId);
			}
		});
		dwr.engine.endBatch();
		return true;
	}
}


function editFormVideoLink(id)
{
	PFCertifications.editVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

function deleteFormVideoLink(id)
{
	PFCertifications.deleteVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}
function delVideoLnkConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value	
	 $('#delVideoLnk').modal('hide');
		PFCertifications.deleteVIdeoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getVideoLinksGrid_DivProfile(teacherId);	
			//getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;
}
function applyScrollOnAssessments()
{	
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}


--></script>


<style>

 textarea{
 	width:420px;
 	height:100px;
 }
 .table-bordered {
    border-collapse: separate;
    border-color:#cccccc;
 }
 .table-bordered td,th {
  border-left:0px solid #dddddd;
 }
  .tdscore0{
	    width: 1px;
	    height:40px;
	    float: left;
  }
  .tddetails0{
	    width: 1px;
	    height:40px;
	    float: left;
  }
  tr.bgimage {
    background-image : url(images/color-gradients.png);
     background-repeat: no-repeat no-repeat;
 }
 .dropdown-menu{
 	min-width:40px;
 } 
 /*.popover{
  width: 770px;
  background-color: #ffffff;
  border: 1px solid #007AB4;
  }*/  
  .popover-title {
  margin: 0;
  padding: 0px 0px 0px 0px;
  font-size: 0px;
  line-height: 0px;
  }
  .popover-content {
   margin: 0;
   padding: 0px 0px 0px 0px;
  }
  
  .popover.right .arrow {
  top: 100px;
  left: -10px;
  margin-top: -10px;
  border-width: 10px 10px 10px 0;
  border-right-color: #ffffff;
}

  .popover.right .arrow:after {
  border-width: 11px 11px 11px 0;
  border-right-color: #007AB4;;
  bottom: -11px;
  left: -1px;
	}
	
 .pull-left {
  margin-left:-52px;
  margin-top:7px;
 }	
 .tblborder{

 }
 .nobground2{
	padding: 6px 7px 6px 7px;
	-webkit-border-top-left-radius:30px;border-top-left-radius:30px;-moz-border-radius-topleft:30px;-webkit-border-top-right-radius:30px;border-top-right-radius:30px;-moz-border-radius-topright:30px;-webkit-border-bottom-right-radius:30px;border-bottom-right-radius:30px;-moz-border-radius-bottomright:30px;-webkit-border-bottom-left-radius:30px;border-bottom-left-radius:30px;-moz-border-radius-bottomright:30px;
 }
  .nobground1{
	padding: 6px 10px 6px 10px;
	-webkit-border-top-left-radius:30px;border-top-left-radius:30px;-moz-border-radius-topleft:30px;-webkit-border-top-right-radius:30px;border-top-right-radius:30px;-moz-border-radius-topright:30px;-webkit-border-bottom-right-radius:30px;border-bottom-right-radius:30px;-moz-border-radius-bottomright:30px;-webkit-border-bottom-left-radius:30px;border-bottom-left-radius:30px;-moz-border-radius-bottomright:30px;
 }
  .nobground3{
	padding: 6px 4px 6px 4px;
	-webkit-border-top-left-radius:30px;border-top-left-radius:30px;-moz-border-radius-topleft:30px;-webkit-border-top-right-radius:30px;border-top-right-radius:30px;-moz-border-radius-topright:30px;-webkit-border-bottom-right-radius:30px;border-bottom-right-radius:30px;-moz-border-radius-bottomright:30px;-webkit-border-bottom-left-radius:30px;border-bottom-left-radius:30px;-moz-border-radius-bottomright:30px;
 }
 .icon-ok-circle{
 	font-size: 3em;
	 color:#00FF00;
 }
 .icon-circle{
 	font-size: 3em;
	 color:#E46C0A;
 }
 .icon-remove-circle{
 	font-size: 3em;
	 color:red;
 }
 .icon-circle-blank{
 	 font-size: 3em;
	 color:red;
 }
 .icon-question-sign{
 	 font-size: 1.3em;
 }
 
 .icon-inner{
 	 font-size: 2.3333333333333333em;
 	 font-family: 'Bauhaus 93';
	 color:red;
	 margin-left:-26px;
	 vertical-align:5%;
 }
 .icon-inner2{
 	 font-size: 2.3333333333333333em;
 	 font-family: 'Bauhaus 93';
	 color:red;
	 margin-left:-24px;
	 vertical-align:8%;
 } 
 .circle{
  width: 48px;
  height: 48px;
  margin: 0em auto;
 }
 .subheading{
  font-weight:none; 
 }
  .icon-folder-open,icon-copy,icon-cut,icon-paste,icon-remove-sign,icon-edit
{
	color:#007AB4;
}
.marginleft20
{
	margin-left:-20px;
}

.margintop20
{
	margin-top:-20px;
} 
 
   div.t_fixed_header_main_wrapper {
	position 		: relative; 
	overflow 		: visible; 
  }
  
.modal_header_profile {

  border-bottom: 1px solid #eee;
  background-color: #0078b4;
  text-overflow: ellipsis; 
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  outline: none;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
          background-clip: padding-box;
}


.scrollspy_profile { height:500px; overflow-y:auto; overflow-x:hidden;padding-left:5px;
}
.divwidth
{
	width:728px;
}
.tablewidth
{
width: 900px;
}
.net-widget-footer 
{
	border-bottom: 1px solid #cccccc; 
	border-left: 1px solid #cccccc;
	border-right: 1px solid #cccccc;
	line-height:40px; 
	background-color: #F2FAEF;
	background-image: -moz-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFFFF), to(#FFFFFF));
	background-image: -webkit-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -o-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: linear-gradient(to bottom,#FFFFFF, #FFFFFF);
	background-repeat: repeat-x;
	color:#4D4D4E;
	vertical-align: middle;
	
}
.modaljob {
  position: fixed;
  top: 40%;
  left: 45%;
  z-index: 2000;
  overflow: auto;
  width: 980px;  
  margin: -250px 0 0 -440px;
  background-color: #ffffff;
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  /* IE6-7 */

  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding-box;
  background-clip: padding-box;
}
.net-corner-bottom 
{ 
	-moz-border-radius-bottomleft: 12px; -webkit-border-bottom-left-radius: 12px; border-bottom-left-radius: 12px; -moz-border-radius-bottomright: 12px; -webkit-border-bottom-right-radius: 12px; border-bottom-right-radius: 12px; 
}
.custom-div-border1
{
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-moz-border-radius: 6px;
border-radius: 6px;
}
.modal-border
{
/*border: 1px solid #0A619A;*/
}
.custom-div-border
{
padding:1px;
border-bottom-left-radius:2em;
border-bottom-right-radius:2em;
}
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
.custom-border
{
margin-left: -32px;
margin-top: 2px;
}


.status-notes-image
{
  margin-left: 40px; 
}
.status-notes-text
{
  margin-top:-20px;
  margin-left: 80px;
}
</style>
			
 <div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">
         	    <spring:message code="LBLManageReSavedCandidates"/>
         	</div>	
         </div>          		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>		

	<script type="text/javascript">
	
	// --- Implement Cut/Copy/Paste --------------------------------------------
	var clipboardNode = null;
	var pasteMode = null;

	function copyPaste(action, node) {
		//alert("---- Gagan : action ==== "+action+" and node value  "+node);
		switch( action ) {
		case "cut":
		case "copy":
			clipboardNode = node;
			pasteMode = action;
			break;
		case "paste":
			if( !clipboardNode ) {
				//alert("Clipoard is empty.");
				break;
			}
			if( pasteMode == "cut" ) {
			//alert("cut pasteMode "+pasteMode+" key  "+node.data.key);
				// Cut mode: check for recursion and remove source
				var isRecursive = false;
				var dictKey=null;
				var i="";
				var cb = clipboardNode.toDict(true, function(dict){
					// If one of the source nodes is the target, we must not move
					//alert("----- Key "+dict.key);
					if( dict.key == node.data.key )
						isRecursive = true;
					dictKey	=	dict.key;
					//alert("----- DICTKey"+i+" ----------"+dictKeyi);
					i+=dict.key+",";
					//i++;
				});
				if( isRecursive ) {
					//alert("Cannot move a node to a sub node.");
					return;
				}
					//alert(" \n value of i ==="+i)
					//var n=str.split("");
					var n=i.split(",");
					//alert(" Length "+n.length+" n[0] "+n[0])
					var cutFolderKey=n[0];
					//alert(" dictKey "+dictKey+" cutFolderKey "+cutFolderKey+"  node.data.key "+node.data.key);
					CandidateGridAjax.cutPasteFolder(node.data.key,cutFolderKey,
					{ 
						async: false,
						errorHandler:handleError,
						callback:function(data)
						{	
							//alert("=== data ===="+data);
						}
					});
				
				
				
				//copyPasteFolder('cut',dict.key,node.data.key);
				node.addChild(cb);
				clipboardNode.remove();
			} else {
			var dictKey=null;
			var dictTitle=null;
				//alert(" pasteMode "+pasteMode+" key  "+node.data.key);
				// Copy mode: prevent duplicate keys:
				var cb = clipboardNode.toDict(true, function(dict){
					dictTitle=dict.title;
					dict.title = "Copy of " + dict.title;
					dictKey	=	dict.key;
					delete dict.key; // Remove key, so a new one will be created
				});
				//alert(" dictTitle "+dictTitle+" dictKey "+dictKey);
				
				CandidateGridAjax.copyPasteFolder(node.data.key,dictTitle,
				{ 
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						//alert("=== data ===="+data);
						var childNode = node.addChild({
							title: data.folderName,
							tooltip: "",
							isFolder: true,
							expand:true,
							key: data.folderId
						});
						//node.addChild(cb);
					}
				});
				
			}
			clipboardNode = pasteMode = null;
			break;
		default:
			alert("Unhandled clipboard action '" + action + "'");
		}
	};

	// --- Contextmenu helper --------------------------------------------------
	// --- Contextmenu helper --------------------------------------------------
	function bindContextMenu(span) {
	
		//alert("------ bindContextMenu ----------");
		// Add context menu to this node:
		$(span).contextMenu({menu: "myMenu"}, function(action, el, pos) {
			// The event was bound to the <span> tag, but the node object
			// is stored in the parent <li> tag
			var node = $.ui.dynatree.getNode(el);
			//alert(" action "+action+" === delete ===="+node);
			switch( action ) {
			case "cut":
			case "copy":
			case "paste":
				copyPaste(action, node);
				break;
			/*============ Gagan : For Edit Folder Code Here ===================*/
			case "edit":
			editNode(node);
			break;
			case "create":
			//alert(" node "+node);
			node.activate(true);
			createUserFolder(node);
			break;
			case "delete":
			checkFolderIsHomeOrSharedMYF(node);
			break;
			default:
				//alert("---------------------: appply action '" + action + "' to node " + node);
			}
		});
	};
	
	function checkFolderIsHomeOrSharedMYF(rootNode)
	{
		$('#errortreediv').empty();
		$('#errordeletetreediv').empty();
		$('#errordeletetreediv').hide();
		var folderId="";
		if(rootNode==null)
		{
			rootNode =$("#tree").dynatree("getActiveNode");
			folderId=rootNode.data.key;
		}
		else
		{
			// When root Node is not null
			folderId=rootNode.data.key;
		}
		
		CandidateGridAjax.checkFolderIsHomeOrShared(folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
				if(data==3)
				{
					$('#errordeletetreediv').show();
					$('#errortreediv').hide();
					$('#errordeletetreediv').html("&#149; <spring:message code='msgFolderCanNotDeleted'/>  ");
				}
				else 
				{
					if(data==4)
					{
						try{
							deleteUDfolder(rootNode);
						}catch(err)
						  {}
					}
				}
				
			}
		});
	$('#errortreediv').hide();
	}
	
	function deleteUDfolder(rootNode)
	{
		$('#currentObject').val(rootNode.data.key);
		$('#deleteFolder').modal('show');
	}
	
	function deleteUDConfirm()
	{
		$('#deleteFolder').modal('hide');
		var rootNodeKey =$('#currentObject').val();
		var pageFlag= document.getElementById("pageFlag").value;
		 // pageFlag =0 : Means it is inside iframe
		if(pageFlag	==	0) 
		{
			document.getElementById('iframeSaveCandidate').contentWindow.deleteFolder(rootNodeKey)
		}
		//alert(" ----pageFlag "+pageFlag);
		deleteFolder(rootNodeKey);
	}
	
	
	
	function checkFolderId()
	{
		var rootNode =$("#tree").dynatree("getActiveNode");
		if(rootNode==null)
		{
			$("#frame_folderId").val("");
			//document.getElementById('errortreediv').innerHTML		=	'&#149; Please enter Domain Name';
		}
		else
		{
			$("#frame_folderId").val(rootNode.data.key);
		}
	}
	

	function deleteFolder(rootNodeKey)
	{
		/* ---------- Deleting Node here without executing query  because query will be already execute for removing Node inside iframe --------------*/
		var pageFlag= document.getElementById("pageFlag").value;
		//alert(" deleteFolder "+rootNodeKey+" pageFlag "+pageFlag);
		if(pageFlag	==0 && rootNodeKey!=0)  
		{
			rootNode =$("#tree").dynatree("getTree").getNodeByKey(rootNodeKey);
			rootNode.remove();
			return false;
		}
		if(rootNodeKey!=0)
		{
			rootNode =$("#tree").dynatree("getTree").getNodeByKey(rootNodeKey);
			rootNode.activate(true);
		}
		
		
		$('#errortreediv').empty();
		var folderId="";
		if(rootNode==null)
		{
			rootNode =$("#tree").dynatree("getActiveNode");
			folderId=rootNode.data.key;
		}
		else
		{
			// When root Node is not null
			folderId=rootNode.data.key;
		}
		
		
		//alert("Before Created"+parentId);
		CandidateGridAjax.deleteUserFolder(folderId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert(" ==== data === "+data);
				rootNode.remove();
				$('#deleteFolder').modal('hide');
				$("#frame_folderId").val("");
				// Showing Gride By Default HoMe
				
				var tree = $('#tree').dynatree("getTree");
				var root = tree.getRoot();
				var nodeList = root.getChildren();
				nodeList[0].activate(true);
				var folderId 				=	nodeList[0].data.key;
				//alert("tree 355 folderId "+folderId);
				window.parent.displaySavedCGgridByFolderId(1,folderId,0);
			}
		});
	$('#errortreediv').hide();
}
	
	function createUserFolder(rootNode)
{
		$('#errortreediv').empty();
		if(rootNode==null)
		{
			$('#errortreediv').html("&#149; <spring:message code='msgPlzSlctAnyFolder'/>");
			$('#errortreediv').show();
			return false;
		}
		var parentId=rootNode.data.key;
		CandidateGridAjax.createUserFolder(parentId,
		{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{	
				//alert("=== data ===="+data);
				var childNode = rootNode.addChild({
					title: data.folderName,
					tooltip: "",
					isFolder: true,
					expand:true,
					key: data.folderId
				});
			}
		});
	$('#errortreediv').hide();
}
	function editNode(node){
	if(node==null)
	{
		return false;
	}
	//alert("Calling Edit Function")
	var prevTitle = node.data.title,
		tree = node.tree;
	// Disable dynatree mouse- and key handling
	tree.$widget.unbind();
	// Replace node with <input>
	$(".dynatree-title", node.span).html("<input id='editNode' style='width:100px;'  maxlength='50' value='" + prevTitle + "'>");
	// Focus <input> and bind keyboard handler
	$("input#editNode")
		.focus()
		.keydown(function(event){
			switch( event.which ) {
			case 27: // [esc]
				// discard changes on [esc]
				$("input#editNode").val(prevTitle);
				$(this).blur();
				break;
			case 13: // [enter]
				// simulate blur to accept new value
				$(this).blur();
				break;
			}
		}).blur(function(event){
			// Accept new value, when user leaves <input>
			var title = $("input#editNode").val();
			//alert("Title "+title+" node Id "+node.data.key);
			if(title=="" || title.length<0 || title==prevTitle)
			{
				//alert("1");
				node.setTitle(prevTitle);
			}
			else
			{
				//alert("2");
				node.setTitle(title);
			//---------------------- Edit Or Rename Folder Name --------------------------
				CandidateGridAjax.renameUserFolder(node.data.key,title,
				{ 
					async: false,
					errorHandler:handleError,
					callback:function(data)
					{	
						//alert("=== data ===="+data);
					}
				});
			}
			// Re-enable mouse and keyboard handlling
			tree.$widget.bind();
			node.focus();
		});
}
	// --- Init dynatree during startup ----------------------------------------
// $ is now an alias to the jQuery function; creating the new alias is optional.
 var $ = jQuery.noConflict();
	$(function(){
		//alert("Treeview.js")
		$("#tree").dynatree({
			persist: true,
			onActivate: function(node) {
				$('#errortreediv').hide();
				$('#errordeletetreediv').hide();
				$("#echoActivated").text(node.data.title + ", key=" + node.data.key);
				$("#frame_folderId").val(node.data.key);
				window.parent.defaultSaveDiv();
				window.parent.displaySavedCGgridByFolderId(1,node.data.key,1);
			},
			onClick: function(node, event) {
				// Close menu on click
				if( $(".contextMenu:visible").length > 0 ){
					$(".contextMenu").hide();
//					return false;
				}
			},
			onKeydown: function(node, event) {
				// Eat keyboard events, when a menu is open
				if( $(".contextMenu:visible").length > 0 )
					return false;

				switch( event.which ) {

				// Open context menu on [Space] key (simulate right click)
				case 32: // [Space]
					$(node.span).trigger("mousedown", {
						preventDefault: true,
						button: 2
						})
					.trigger("mouseup", {
						preventDefault: true,
						pageX: node.span.offsetLeft,
						pageY: node.span.offsetTop,
						button: 2
						});
					return false;

				// Handle Ctrl-C, -X and -V
				case 67:
					if( event.ctrlKey ) { // Ctrl-C
						copyPaste("copy", node);
						return false;
					}
					break;
				case 86:
					if( event.ctrlKey ) { // Ctrl-V
						copyPaste("paste", node);
						return false;
					}
					break;
				case 88:
					if( event.ctrlKey ) { // Ctrl-X
						copyPaste("cut", node);
						return false;
					}
					break;
				}
			},
			/*Bind context menu for every node when it's DOM element is created.
			  We do it here, so we can also bind to lazy nodes, which do not
			  exist at load-time. (abeautifulsite.net menu control does not
			  support event delegation)*/
			onCreate: function(node, span){
			//alert(" onCreate Method is calling "); 
			 	node.expand(false);
			 	//$("#"+node.data.key).append("<li>Gagan</li>");
			 	//alert("Hi");
				bindContextMenu(span);
			},
			onFocus: function(node) {
				$("#echoFocused").text(node.data.title);
			},
			onBlur: function(node) {
				$("#echoFocused").text("-");
			},
			/*Load lazy content (to show that context menu will work for new items too)*/
			onLazyRead: function(node){
				node.appendAjax({
					url: "contextmenu/sample-data2.json"
				});
			},
		/* D'n'd, just to show it's compatible with a context menu.
		   See http://code.google.com/p/dynatree/issues/detail?id=174 */
		dnd: {
			preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
			onDragStart: function(node) {
				return true;
			},
			onDragEnter: function(node, sourceNode) {
				if(node.parent !== sourceNode.parent)
					return false;
				return ["before", "after"];
			},
			onDrop: function(node, sourceNode, hitMode, ui, draggable) {
				sourceNode.move(node, hitMode);
			}
		}
		});
		
		
/* ---------------------- Create Folder Method Start here --------------- */
		$("#btnAddCode").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			createUserFolder(activeNode);
		});
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
	$("#renameFolder").click(function(){
			//alert("Rename clicked ")
			var activeNode = $("#tree").dynatree("getActiveNode");
			//alert("activeNode "+activeNode)
			editNode(activeNode);
		});

/*-----------------  Cut Copy Paste Folder ------------------------*/
	$("#cutFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			//alert("activeNode "+activeNode);
			copyPaste('cut', activeNode);
		});

	$("#copyFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			//alert("activeNode "+activeNode);
			copyPaste('copy', activeNode);
		});

	$("#pasteFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			//alert("activeNode "+activeNode);
			copyPaste('paste', activeNode);
		});
		
	$("#deletFolder").click(function(){
			var activeNode = $("#tree").dynatree("getActiveNode");
			//alert("delete this activeNode "+activeNode);
			 checkFolderIsHomeOrSharedMYF(activeNode);
			//window.parent.deletesavefolder();
			//deleteFolder();
		});	
		
		
/*----------------- Events  Start Here -------------------------------------------------*/	
		$(function(){
	var isMac = /Mac/.test(navigator.platform);
	$("#tree").dynatree({
		title: "Event samples",
		onClick: function(node, event) {
			if( event.shiftKey ){
				editNode(node);
				return false;
			}
		},
		onDblClick: function(node, event) {
			editNode(node);
			return false;
		},
		onKeydown: function(node, event) {//-----------------
			if( $(".contextMenu:visible").length > 0 )
					return false;

				switch( event.which ) {
				// Open context menu on [Space] key (simulate right click)
				case 32: // [Space]
					$(node.span).trigger("mousedown", {
						preventDefault: true,
						button: 2
						})
					.trigger("mouseup", {
						preventDefault: true,
						pageX: node.span.offsetLeft,
						pageY: node.span.offsetTop,
						button: 2
						});
					return false;

				// Handle Ctrl-C, -X and -V
				case 67:
					if( event.ctrlKey ) { // Ctrl-C
						copyPaste("copy", node);
						return false;
					}
					break;
				case 86:
					if( event.ctrlKey ) { // Ctrl-V
						copyPaste("paste", node);
						return false;
					}
					break;
				case 88:
					if( event.ctrlKey ) { // Ctrl-X
						copyPaste("cut", node);
						return false;
					}
					break;
				
				case 113: // [F2]
				editNode(node);
				return false;
				case 13: // [enter]
					if( isMac ){
						editNode(node);
						return false;
					}
				}
			 //======== Function End ============
		}
		//---------------
		
	});
});

/*----------------- Events  End Here -------------------------------------------------*/		
	});
	
	

	
	
	
</script>

<%--- ============================================ Gagan: Share Folder Div ======================================================== --%>

<input type="hidden" id="teacherIdForprofileGrid" name="teacherIdForprofileGrid"/>
<input type="hidden" id="teacherIdForprofileGridVisitLocation" name="teacherIdForprofileGridVisitLocation" value="My Folders" />

<input type="hidden"  name="pageFlag" id="pageFlag" value="1"/>
<input type="hidden"  name="userFPD" id="userFPD"/>
<input type="hidden"  name="userFPS" id="userFPS"/>
<input type="hidden"  name="userCPD" id="userCPD"/>
<input type="hidden"  name="userCPS" id="userCPS"/>
<input type="hidden" id="commDivFlag" name="commDivFlag" value=""/>
<input type="hidden" id="jobForTeacherGId" name="jobForTeacherGId" value=""/>

<input type="hidden"  name="home_folderId" id="home_folderId"/>
<input type="hidden"  name="folderId" id="folderId"/>
<input type="hidden"  name="checkboxshowHideFlag" id="checkboxshowHideFlag"/>

<!-- @Start@Ashish :: draggable div for myFolder Teacher Profile -->
<div class="modal hide custom-div-border1" id="draggableDivMaster" style="" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="draggableDiv"></div>
</div>
<!-- @End
@Ashish :: draggable div for myFolder Teacher Profile -->
<div class="modal hide"  id="shareDiv" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='defaultSaveDiv()'>x</button>
		<h3 id="myModalLabel"><spring:message code="headShareCandidate"/> </h3>
		<input type="hidden" id="entityType" name="entityType" value="${entityType}">
		<input type="hidden" id="teacherIdFromSharePoPUp" name="teacherIdFromSharePoPUp" value="">
		<input type="hidden" id="txtteacherIdShareflagpopover" name="txtteacherIdShareflagpopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
		<input type="hidden" id="commDivFlag" name="commDivFlag" value=""/>
		<input type="hidden" id="jobForTeacherGId" name="jobForTeacherGId" />
		<input type="hidden" id="jobId" name="jobId" value="0"/>
		<input type="hidden" id=phoneType name="phoneType" value="0"/>
		<input type="hidden" id=msgType name="msgType" value="0"/>	
		<input type="hidden" id="noteId" name="noteId" value=""/>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			
			<div class="">
					<div class="row">
					 	<div class="col-md-10">
						<div class='divErrorMsg span12' id='errorinvalidschooldiv' ></div>
						</div>
					      <div class="col-sm-4 col-md-4">
					       <label><spring:message code="lblDistrictName"/></label></br>
				             <%--<c:if test="${DistrictName==null}"> //Admin
				             <span>
				             <input type="text" id="districtName"  maxlength="100"  name="districtName" class="help-inline span8"
				            		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
												onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
												onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
						      </span>
						      <input type="hidden" id="districtId" value="0"/>
						      </c:if>
						      --%>
						      <c:if test="${DistrictName!=null}">
				             	${DistrictName}
				             	<input type="hidden" id="districtId" value="${DistrictId}"/>
				             	<input type="hidden" id="districtName" value="${DistrictName}" name="districtName"/>
				             </c:if>
			              <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
						
					      </div>
					      
					      <div class="col-sm-6 col-md-6">
					        <label><spring:message code="lblSchoolName"/></label>
				          	<c:if test="${SchoolName==null}">
				           	<input type="text" id="schoolName" maxlength="100" name="schoolName" class=" form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
							    <input type="hidden" id="schoolId" value="0"/>
							</c:if>
							  <c:if test="${SchoolName!=null}">
				             	<input type="text" id="schoolName" maxlength="100" name="schoolName" class=" form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" value="${SchoolName}"	/>
				             	<input type="hidden" id="schoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolName" value="${SchoolName}"/>
				             </c:if>
							 <div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
				        
					      </div>
					      <div class="col-md-2 top25">
					        <label>&nbsp;</label>
					        <input type="button" id="" name="" value="Go" onclick="searchUser(1)" class="btn  btn-primary" >
					      </div>
  					</div>
				</div>
		
		
			<div id="divShareCandidateToUserGrid" style="border: 0px solid green;" class="mt30">
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="shareCandidate()" ><spring:message code="lblShare"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='defaultSaveDiv()'><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>

<div  class="modal hide"  id="shareConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="saveAndShareConfirmDiv" class="">
		    	<spring:message code="msgSucceSharedCand"/>     	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>


<div class="modal hide"  id="deleteShareCandidate" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeletetStrCondidate"/>
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteCandidate()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>




<div>

	<!-- Definition of context menu -->
	<ul id="myMenu" class="contextMenu">
		<li><a href="#create"><span class='icon-folder-open  icon-large  marginleft20'></span><spring:message code="lnkCret"/></a></li>
		<li><a href="#edit"><span class='icon-edit  icon-large iconcolor  marginleft20'></span> <spring:message code="lnkReNename"/> </a></li>
		<li><a href="#cut"><span class='icon-cut  icon-large iconcolor  marginleft20'></span> <spring:message code="lnkCut"/></a></li>
		<li><a href="#copy"><span class='icon-copy  icon-large  iconcolor marginleft20'></span> <spring:message code="lnkCpy"/></a></li>
		<li><a href="#paste"><span class='icon-paste  icon-large  iconcolor marginleft20'></span> <spring:message code="lnkPa"/></a></li>
		<li><a href="#delete"><span class='icon-remove-sign  icon-large  iconcolor marginleft20'></span> <spring:message code="lnkDlt"/> </a></li>
		<li><a href="#share" onclick="displayShareUsergridForShare()"><span class='marginleft20'><img src="images/option04.png" style="width:21px; height:21px;"></span><spring:message code="btnShare"/></a></li>
		<%-- <li class="quit separator"><a href="#quit">Quit</a></li>--%>
	</ul>
	
	<div  style="border: 0px solid green;width:250px;height:auto;float: left;" class="span5">	
		<div class="" id="tree_menu" style=" padding: 28px 0px 5px 2px; " >
			<a data-original-title='<spring:message code="lnkCret"/>' rel='tooltip' id='createIcon'><span id="btnAddCode" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='<spring:message code="lnkReNename"/>' rel='tooltip' id='renameIcon'><span id="renameFolder" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='<spring:message code="lnkCut"/>' rel='tooltip' id='cutIcon'><span id="cutFolder" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='<spring:message code="lnkCpy"/>' rel='tooltip' id='copyIcon'><span id="copyFolder" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='<spring:message code="lnkPa"/>' rel='tooltip' id='pasteIcon'><span id="pasteFolder" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='<spring:message code="lnkDlt"/>' rel='tooltip' id='deleteIcon'><span id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span></a>
			<a data-original-title='<spring:message code="btnShare"/>' rel='tooltip' id='shareIcon'><span onclick="displayShareUsergridForShare()"><img src="images/option04.png" style="width:21px; height:21px;" ></span></a>
		</div>
		
		<div id="tree">
				${tree}
				<input type="hidden" id="shareFlag" name="shareFlag" value=""/>
				<input type="hidden" id="savecandiadetidarray" name="savecandiadetidarray"/>
				<input type="hidden" id="frame_folderId" name="frame_folderId" value=""/>
				<div class="hide"><spring:message code="lblSchoolName"/>  <span id="echoActivated">-</span></div>
				<div class="hide"><spring:message code="msgFocusednode"/> <span id="echoFocused">-</span></div>
				<input type="hidden"  name="currentObject" id="currentObject"/>
		        <input type="hidden"  name="currentObject" id="currentObject"/>
			     
		</div> 
	</div>
	
		<div class='span12' style="color:red;" id='errordeletetreediv' > </div>
		<div class='divErrorMsg span12 top10' id='errortreediv' ></div>
		
	<!-- Definition tree structure Gagan ${name} ---------- Tree  -->
 	    <div  style="padding-top:30px; border: 0px solid green;float: left;">		
<%-- ======================================== Right Div ========================================================================= --%>			
			
				<input type="hidden" id="savecandidatearray" name="savecandidatearray">
				<input type="hidden" id="txtoverrideFolderId" name="txtoverrideFolderId">
				<div id="savedCGgridMyFolder" class="">				
				</div>			            
		</div> 		
		<div style="clear: both"></div>
</div>
	
 		
	<!-- End_Exclude -->
<div  class="modal hide"  id="myModalCommunications" style='' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 700px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForSaveMyFolder();showProfilePopover();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headCom"/></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divCommTxt">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick='showCommunicationsDivForSaveMyFolder();showProfilePopover();'><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="saveToFolderDiv"  style=''  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:900px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeSaveDiv()">x</button>
		<h3 id="myModalLabel"><spring:message code="headSaveCandidate"/></h3>
	</div>
	<div class="modal-body" style="max-height: 500px;overflow-y: scroll;"> 		
		<div class="control-group">
		<div  style="border: 0px solid green;width:220px;height:450px;float: left;" class="span5">	
		<div class="span5" id="tree_menu" style=" padding: 8px 0px 10px 2px; " >
			<a data-original-title='Create' rel='tooltip' id='createIcon1'><span id="btnAddCode1" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Rename' rel='tooltip' id='renameIcon1'><span id="renameFolder1" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Cut' rel='tooltip' id='cutIcon1'><span id="cutFolder1" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Copy' rel='tooltip' id='copyIcon1'><span id="copyFolder1" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Paste' rel='tooltip' id='pasteIcon1'><span id="pasteFolder1" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Delete' rel='tooltip' id='deleteIcon1'><span id="deletFolder1" class='icon-remove-sign  icon-large iconcolor'></span></a>
		</div>
		<iframe id="iframeSaveCandidate"  src="tree.do?pageFlag=0" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>
	</div>
	<%-- ======================================== Right Div ========================================================================= --%>			
			<div class="span10" style="padding-top:39px; border: 0px solid green;float: left;">
				<input type="hidden" id="savecandidatearray" name="savecandidatearray">
				<input type="hidden" id="txtoverrideFolderId" name="txtoverrideFolderId">
				<input type="hidden" id="teachetIdFromPoPUp" name="teachetIdFromPoPUp" value="">
				<input type="hidden" id="txtflagpopover" name="txtflagpopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
				<div id="savedCandidateGrid">
					<%--<div style='text-align:center;'><img src='images/loadingAnimation.gif' /> <br>Loading...</div> --%>
				</div>
			</div>	
			<div style="clear: both"></div>
		</div>
		
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="saveCandidateToFolderByUserFromMyFolder()" ><spring:message code="btnSave"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeSaveDiv()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="deleteFolder" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoyouWantToDeleteFolder"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="deleteUDConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="duplicatCandidate" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgCandidateSvav/Cancel"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="saveWithDuplicateRecordFromMyFolder()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="myModalNotes"  style='' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-dialog" style="width:935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot"/></h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID' name='uploadNoteFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload' enctype='multipart/form-data' method='post' target='uploadNoteFrame'  class="form-inline" onsubmit="return saveNotes();" action='noteUploadServlet.do' accept-charset="UTF-8">
			
		<div class="mt10">
			<div class='span10 divErrorMsg' id='errordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong><spring:message code="lblEtrNot"/><span class="required">*</span></strong></label>
		    	<div class="span10" id='divTxtNode' style="padding-left: 0px; margin-left: 0px; " >
		    		<textarea style="width: 100%" readonly id="txtNotes" name="txtNotes" class="span10" rows="4"   ></textarea>
		    	</div>  
		    	<input type="hidden" id="teacherIdForNote" name="teacherIdForNote" value="">
		    	<input type="hidden"  name="noteDateTime" id="noteDateTime" value="${dateTime}"/>
		    	<div id='fileNotes' style="padding-top:10px;">
	        		<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'><spring:message code="lnkAttachFile"/></a>
	        	</div>      	
			</div>						
		</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<c:set var="chkSaveDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSaveDisp" value="none" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSaveDisp" value="inline" />
			</c:otherwise>
		</c:choose>
 		<!-- <span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()">Cancel</a></span> -->
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'><spring:message code="btnClr"/></button>&nbsp; 
 		<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button class="btn btn-primary"  onclick="saveNotes()"><spring:message code="btnSave"/> <i class="icon"></i></button>&nbsp;</span>
 	</div>
</div>
	</div>
</div>
<!--Add message Div by  Sekhar  -->
<div  class="modal hide"  id="myMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'><spring:message code="msgYuMsgIsfullySentToCandidate"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()"><spring:message code="btnOk"/></button>
 	</div>
</div>
	</div>
</div>
<div  class="modal hide"  id="myModalMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'><spring:message code="msgYuMsgIsfullySentToCandidate"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()"><spring:message code="btnOk"/></button>
 	</div>
</div>
	</div>
</div>
<input type="hidden" id="teacherDetailId" name="teacherDetailId">
<div class="modal hide"  id="myModalMessage" style='' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headPostMsgToCandidate"/></h3>
	</div>
	<iframe id='uploadMessageFrameID' name='uploadMessageFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUpload' enctype='multipart/form-data' method='post' target='uploadMessageFrame'  class="form-inline" onsubmit="return validateMessage();" action='messageCGUploadServlet.do' accept-charset="UTF-8">
	
	<div  class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessages">						
		</div>
		<div class="control-group">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
			
		<%---------- Gagan :District wise Dynamic Template [Start]  --%>
		<div id="templateDiv" style="display: none;">
			<div class="row col-md-12" >
		    	<div class="row col-sm-6" style="max-width:300px;">
		    	<label><strong><spring:message code="lblTemplate"/></strong></label>
	        	<br/>
	        	<select class="form-control" id="districttemplate" onchange="setTemplate()">
	        		<option value="0"><spring:message code="sltTemplate"/></option>
	        	</select>
	        	 </div>	
	        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<button class="btn btn-primary hide top26"  id="btnChangetemplatelink" onclick="return getTemplate();"><spring:message code="btnAplyTemplate"/></button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplate" name="confirmFlagforChangeTemplate" value="0">
			</div>
		</div>	
		<%---------- Gagan :District wise Dynamic Template [END]  --%>
		
		<div class="control-group"  style="padding-top: 5px;">
    		<label><strong>To<br/></strong><span id="emailDiv" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support'  class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend" style="width: 612px;">
		    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
	        	<textarea rows="5" class="form-control" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessage" name="teacherIdForMessage" value="">
    		<input type="hidden"  name="messageDateTime" id="messageDateTime" value="${dateTime}"/>
	    	<div id='fileMessages' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addMessageFileType();'><spring:message code="lnkAttachFile"/></a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<c:set var="chkSendDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSendDisp" value="none" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSendDisp" value="inline" />
			</c:otherwise>
	</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'><spring:message code="btnClr"/></button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessage()" style="display: ${chkSendDisp}"><spring:message code="btnSend"/></button>&nbsp;
 	</div>
</div>
	</div>
</div>

<%-- ---------- GAgan : Apply Template Confirm Pop up [Start] ----------------%>
<div class="modal hide"  id="confirmChangeTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id=''>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="ddd"><spring:message code="tmpForExistingTemplateAndMsgOverrideClickOnOk/Cancel"/>
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span id=""><button class="btn btn-large btn-primary" onclick="confirmChangeTemplate()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button></span> 		
 	</div>
</div>
	</div>
</div>
<%------- GAgan : Apply Template Confirm Pop up [END] ---------%>



<!--Start Profile Div-->
<div  class="modal hide"  id="myModalPhoneShowPro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="lblPhone"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divPhoneByPro" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>

<div  class="modal hide" style="" id="myModalProfileVisitHistoryShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:100px;" id="mydiv">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headTeacherProfVisitHistory"/></h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y: scroll;padding-right: 18px;">		
		<div class="control-group">
			<div id="divteacherprofilevisithistory" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>


<div  class="modal hide pdfDivBorder"  id="modalDownloadPDR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headPdRep"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   <iframe id="ifrmPDR" width="100%" height="480px" src=""></iframe>		  
		  </div> 
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>
<!--<iframe src="" id="ifrmTrans" width="100%" frameborder="0" height="0px">
</iframe> 
--><div style="display:none; z-index: 5000;" id="loadingDiv" >
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

<div  class="modal hide"  id="myModalJobList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 980px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setPageFlag()">x</button>
		<h3 id="myModalLabel"><spring:message code="hdJDetail"/></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">		
		<div class="control-group">
			<div id="divJob" class="table-responsive" style=" margin-bottom:15px;min-width:920px;overflow-x:hidden;">
		    		  
			
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setPageFlag()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>
 
<div  class="modal hide"  id="myModalReferenceNoteView" style='' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog' style="width: 520px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel"><spring:message code="headRefrNot"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" id="divRefNotesInner">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>

<!-- Ref Note -->
<div class="modal hide"  id="myModalReferenceNotesEditor"  style='' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog-for-cgreference">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'>x</button>
		<h3 id="myModalLabel"><spring:message code="headNot"/></h3>
	</div>
	
	<div class="modal-body" style="margin:10px;">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID_ref' name='uploadNoteFrame_ref' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload_ref' enctype='multipart/form-data' method='post' target='uploadNoteFrame_ref'  class="form-inline" onsubmit="return saveReferenceNotes();" action='referenceNoteUploadServlet.do' accept-charset="UTF-8">
		
			<input type="hidden" name="eleRefId" id="eleRefId">
			<input type="hidden" id="teacherIdForNote_ref" name="teacherIdForNote_ref">
			
			<div class="row mt10">
				<div class='span10 divErrorMsg' id='errordivNotes_ref' style="display: block;"></div>
				<div class="span10" >
			    	<label><strong><spring:message code="lblEtrNot"/><span class="required">*</span></strong></label>
			    	<div class="span10" id='divTxtNode_ref' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea style="width: 100%" readonly id="txtNotes_ref" name="txtNotes_ref" class="span10" rows="4"   ></textarea>
			    	</div>  
			    	<div id='fileRefNotes' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'><spring:message code="lnkAttachFile"/></a>
		        	</div>      	
				</div>						
			</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'><spring:message code="btnClr"/></button>&nbsp; 
 		<span id="spnBtnSave" style="display:inner"><button class="btn btn-primary"  onclick="saveReferenceNotes()"><spring:message code="btnSave"/> <i class="icon"></i></button>&nbsp;</span>
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="myModalQAEXEditor"  style='' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showPreviousModel();">x</button>
		<h3><spring:message code="msgExplaination"/></h3>
	</div>
	<div class="modal-body">	
			<div class="row mt10">
				<div class="span10" >
			    	<div id='divExplain' style="margin-top:-15px;">
		        	</div>      	
				</div>						
			</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showPreviousModel();"><spring:message code="btnClr"/></button>
 	</div>
</div>
	</div>
</div>
<!-- End Ref Note --> 

<div  class="modal hide pdfDivBorder"  id="modalDownloadsCommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content" style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabelText"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTransCommon" width="100%" height="100%" scrolling="auto">
				 </iframe>  
				<!--<embed  src="" id="ifrmTrans" width="100%" height="100%" scrolling="no"/> -->     	
			    <!--<object data="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
				    <p>Insert your error message here, if the PDF cannot be displayed.</p>
				    </object>
			    -->	  
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
	</div>
</div>
<!--End Profile Div-->


<div class="modal hide" id="tfaMsgShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'>
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='showTfaMsgBody'>
					<spring:message code="msgchangeCandidatePortfolioAndTFAStatus"/> 
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="saveTFAbyUser();"><spring:message code="btnSave"/><i class="icon"></i></button>
				<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>

<!-- Popup window -->
<div class="modal hide"  id="chgstatusRef1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUActRef"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeActRef"/> 
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeleteVideoLnk"/>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button> 		
 	</div>
</div>
	</div>
</div>
	<div class="modal hide"  id="saveDataLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDataSaved"/> 
		</div>
 	</div>
 	<div class="modal-footer">
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide" id="certTextDivDSPQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="certTextDivClose();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y:auto;"> 		
		<div class="control-group certTextContent">
			
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn" data-dismiss="modal" onclick="certTextDivClose();" aria-hidden="true"><spring:message code="btnClose"/>	</button></span>&nbsp;&nbsp; 		
 	</div>
</div>
	</div>
</div>
<jsp:include page="selfserviceapplicantprofilecommon.jsp"></jsp:include>

<input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
	<script>
	displayCandidatesOfHomeFolder();
	//createHomeAndSharedFolderIFnotCreated();
	$('#createIcon').tooltip();
	$('#renameIcon').tooltip();
	$('#cutIcon').tooltip();
	$('#copyIcon').tooltip();
	$('#pasteIcon').tooltip();
	$('#deleteIcon').tooltip();
	 $('#createIcon1').tooltip();
	 $('#renameIcon1').tooltip();
	 $('#cutIcon1').tooltip();
	 $('#copyIcon1').tooltip();
	 $('#pasteIcon1').tooltip();
	 $('#deleteIcon1').tooltip();
	 $('#shareIcon').tooltip();
	//$('#deleteFolder').modal('show');
	$(document).ready(function(){
	
	 /* ---------------------- Create Folder Method Start here --------------- */
 $("#btnAddCode1").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.createUserFolder();
 });
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
 $("#renameFolder1").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.renameFolder();
 });
/*-----------------  Cut Copy Paste Folder ------------------------*/
 $("#cutFolder1").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.cutFolder(); 
 });
 
 $("#copyFolder1").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.copyFolder();  
 });
 
 $("#pasteFolder1").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.pasteFolder(); 
 });
  
 $("#deletFolder1").click(function(){
  document.getElementById('iframeSaveCandidate').contentWindow.deletFolder();
 });
	
	//$('#divTxtNode').find(".jqte").width(612);
	$('#messageSend').find(".jqte").width(612);
	
	});

	</script>
	
<!--Start : @ashish :: for myFolder Teacher Profile {draggableDiv} -->
<script>
$(function() {
$( "#draggableDivMaster" ).draggable({
handle:'#teacherDiv', 
containment:'window',
revert: function(){
var $this = $(this);
var thisPos = $this.position();
var parentPos = $this.parent().position();
var x = thisPos.left - parentPos.left;
var y = thisPos.top - parentPos.top;
if(x<0 || y<180)
{
return true; 
}else{
return false;
}
}
});
});
</script>
<!--End : @ashish :: for myFolder Teacher Profile {draggableDiv} --> 

	

