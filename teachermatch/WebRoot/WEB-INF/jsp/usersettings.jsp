<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="dwr/interface/PersonalEmailTrackAjax.js?ver=${resourceMap['PersonalEmailTrackAjax.ajax']}"></script>
<div onkeypress="chkForEnter(event)" class="modal hide"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-feedbackandsupport">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headChngPass"/></h3>
	</div>
	<div class="modal-body">
		<div class="col-sm-12 col-md-12">
			<div class='divErrorMsg' id='errordivPwd' style="display: block;">
			    
			    
			</div>
		</div>		
		
			<div class="col-sm-12 col-md-12">
		    	<label><strong><spring:message code="lblCurrPass"/></strong><span class="required">*</span></label>
	        	<input id="oldpassword" type="password" class="form-control" maxlength="12" />
			</div>	            
     
			<div class="col-sm-12 col-md-12">
		    	<label><strong><spring:message code="lblNePass"/></strong><span class="required">*</span></label>
	        	<input id="newpassword" type="password" class="form-control" maxlength="12"/>
			</div>		
		
			<div class="col-sm-12 col-md-12">
		    	<label><strong><spring:message code="lblRe-EtrPass"/></strong><span class="required">*</span></label>
	        	<input id="repassword" type="password" class="form-control" maxlength="12" />
			</div>
		
 	</div>
 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
 		<button class="btn btn-primary" onclick="chkPassword()"><spring:message code="btnSvChng"/></button>
 	</div>
</div>
	</div>
</div>

<form:form commandName="userMaster" onsubmit="return validateEditSettings()" >

<div class="row col-sm-offset-3 col-md-offset-3 top15">
	
		<div class="subheadinglogin"><spring:message code="lblSett"/></div>				
				<div class="row"> 
	      			<div class="col-sm-6 col-md-">
	      				<div  id='divServerError' class='divErrorMsg' style="display: block;">${msgServer}</div>
						<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
	              	</div>
				</div>
		
		
				<div class="row"> 
	      			<div class="col-sm-4 col-md-4">
	      				<label><strong><spring:message code="lblFname"/></strong><span class="required">*</span></label>
	                	<form:input path="firstName" cssClass="form-control" maxlength="50"/>
	              	</div>
	              	<div class="col-sm-4 col-md-4">
	      				<label><strong><spring:message code="lblLname"/></strong><span class="required">*</span></label>
	                	<form:input path="lastName" cssClass="form-control" maxlength="50"/>
	              	</div>
				</div>
		
			
		    	<div class="row">
			    	<div class="col-sm-8 col-md-8">
			      		<label><strong><spring:message code="lblEmail"/></strong><span class="required">*</span></label>
			           	<form:input path="emailAddress" cssClass="form-control" maxlength="75" />
					</div>
		        </div>
	            
		    <div class="row">
		    	<div class="col-sm-4 col-md-4">
		        	<label><strong><spring:message code="lblPass"/></strong><span class="required">*</span></label>		        	
		        	<form:password path="password" cssClass="form-control" maxlength="12" readonly="true" value="0123456789" />  
		       	</div>
				<div class="col-sm-3 col-md-3 top30-sm">
		        	    	<a href="#" onclick="return showChangePWD()"><spring:message code="lnkChngPass"/></a>
				</div>
			</div>
			
			<div class="row">
		    	<div class="col-sm-8 col-md-8">
		      		<label><strong><spring:message code="lblAuthPin"/></strong></label>
		           	<form:input path="authenticationCode" cssClass="form-control" maxlength="8" disabled="true" />
				</div>
			</div>
			
			<div class="row">
		    	<div class="col-sm-8 col-md-8">
		      		<label><strong><spring:message code="lblNetSett"/></strong></label>
		      		<div class="">
		      		  <c:choose>
					      <c:when test="${twitter==1}">
					      <img src='images/on.png' id="twrImg" />
					      </c:when>
					      <c:otherwise><img src='images/off.png' id="twrImg" />
					      </c:otherwise>
					    </c:choose>
			           <c:choose>
					      <c:when test="${linkedIn==1}">
					      <img src='images/on.png' id="inImg" />
					      </c:when>
					      <c:otherwise><img src='images/off.png' id="inImg" />
					      </c:otherwise>
					    </c:choose>
					    <c:choose>
					      <c:when test="${facebook==1}">
					      <img src='images/on.png' id="fbImg" />
					      </c:when>
					      <c:otherwise><img src='images/off.png' id="fbImg" />
					      </c:otherwise>
					    </c:choose>
			           </div>
				</div>
				<!--------------------------- shriram ------------------------------------------------------------------------------------------------->
				  
				 <!--<c:choose>
					      <c:when test="${emailCredentials==true}">
					      <a href="#" style="text-align: none;" onclick="fetchEmailRecord();" ><spring:message code="msgEmailCredentials"  /></a>
					      
							   
					       
					    
<div onkeypress="return chkForEnter1(event);" class="modal hide"  id="emailCrid"  >
	<div class="modal-dialog-for-feedbackandsupport">
	<div class="modal-content">
	<div class="modal-header">
  	
 	<button type="button" class="close"  onclick="emailPopUpHide();">x</button>
	   <h3 id="myModalLabel2"><spring:message code="lblUpdateEmailCredentials"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			   Error Span 
				<div 
							id="errorDiv">
							<span id="emailIdError" style="color: red;"></span>
							<span id="passwordError" style="color: red;"></span>
							<span id="mailHostError" style="color: red;"></span>
							<span id="mailPortError" style="color: red;"></span>
						</div>
	
			<div class="row" style="display: none;">
		    	<div class="col-sm-12 col-md-12">
		      			<form:input path="userId" cssClass="form-control" maxlength="8" disabled="true" />
				</div>
			</div>	
	    		<div class="row">
			    	<div class="col-sm-12 col-md-12">
			      		<label><strong><spring:message code="lblEmailAddress"/></strong><span class="required">*</span></label>
			      		<input  type="text" name="personalEmailAddress"  id="personalEmailAddress"  class="form-control" maxlength="75" />
			      		
					    </div>
	        	</div>
				    		<div class="row">
			                 	<div class="col-sm-12 col-md-12" >
			            		<label ><strong><spring:message code="lblEmailPassword"/></strong><span class="required">*</span></label>
			            		<input type="password" name="mailPassword" id="mailPassword"   class ="form-control" maxlength="75" />
			    		    </div>
		</div>
		 
		     <div class="row">
			    	<div class="col-sm-12 col-md-12">
			      		<label><strong><spring:message code="lblMailHost"/></strong><span class="required">*</span></label>
			           <input type="text" name="mailHost" id="mailHost" class="form-control" maxlength="75" />
					</div>
		        </div>
		        
		        
		        		        <div class="row">
			    	<div class="col-sm-12 col-md-12">
			      		<label><strong><spring:message code="lblMailPort"/></strong><span class="required">*</span></label>
			           	<input type="text" name="mailPort" id="mailPort" class="form-control" maxlength="75" />
					</div>
		        </div>
		           
		       
 	
 </div>
 </div>
 	<div class="modal-footer">
	 	<table width="100%">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large  pull-right" aria-hidden="true" id="cancelUser1" data-dismiss="modal"><spring:message code="btnCancel"/></button>
		 			<button type="button" class="btn btn-large btn-primary pull-right" style="margin-right:5px;" onclick="return saveEmailRecord();"><spring:message code="btnSave"/></button>
		 		    
				
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>			     
					     
					     
					     
					       
					   
					     
					    
					      </c:when>
					      <c:otherwise>
					      </c:otherwise>
					    </c:choose>
			          
				    
				
				
				
				
				--><div class="col-sm-8 col-md-8">
		           	<a href="javascript:void(0);"><img src='images/twi.png' onclick="showTwitterConfigDiv();return false;"/></a>
		           	<a href="https://www.linkedin.com/uas/oauth2/authorization?response_type=code&client_id=${inAppId}&scope=r_fullprofile%20r_emailaddress%20r_network%20rw_nus&state=${linkedIn==1}&redirect_uri=${redirectURL}linkedInsettings.do" class="popup2"><img src='images/in.png' /></a>
		           	<a href="javascript:void(0);"><img src='images/fb.png' onclick="fblogin();return false;"/></a>
				</div>
			</div>	
		          <!-- shriram -->
		           <!-- Shriram 8 Oct 2015 -->
		          <c:choose>
					      <c:when test="${emailCredentials==true}">
					      <div style="margin-right: 100px;margin-top: 8px">
					      <a href="#" style="text-align: none;" onclick="fetchEmailRecord();" ><spring:message code="msgEmailCredentials"  /></a>
					      </div>
							   
					     <!--  -->
					    
<div onkeypress="return chkForEnter1(event);" class="modal hide"  id="emailCrid" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true">
	<div class="modal-dialog-for-feedbackandsupport">
	<div class="modal-content">
	<div class="modal-header">
  	
 	<button type="button" class="close"  aria-hidden="true" onclick="emailPopUpHide();">x</button>
	   <h3 id="myModalLabel2"><spring:message code="lblUpdateEmailCredentials"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<!--   Error Span -->
				<div 
							id="errorDiv">
							<span id="emailIdError" style="color: red;"></span>
							<span id="passwordError" style="color: red;"></span>
							<span id="mailHostError" style="color: red;"></span>
							<span id="mailPortError" style="color: red;"></span>
						</div>
	
			<div class="row" style="display: none;">
		    	<div class="col-sm-12 col-md-12">
		      			<form:input path="userId" cssClass="form-control" maxlength="8" disabled="true" />
				</div>
			</div>	
					    		<div class="row">
			    	<div class="col-sm-12 col-md-12">
			      		<label><strong><spring:message code="lblEmailAddress"/></strong><span class="required">*</span></label>
			      		<input  type="text" name="personalEmailAddress" id="personalEmailAddress" autofocus Class="form-control" maxlength="75" />
					    </div>
		</div>
				    		<div class="row">
			    	<div class="col-sm-12 col-md-12" >
			      		<label ><strong><spring:message code="lblEmailPassword"/></strong><span class="required">*</span></label>
			      		<input type="password" name="mailPassword" id="mailPassword" Class ="form-control" maxlength="75" />
			      		
					    </div>
		</div>
		 
		     <div class="row">
			    	<div class="col-sm-12 col-md-12">
			      		<label><strong><spring:message code="lblMailHost"/></strong><span class="required">*</span></label>
			           <input type="text" name="mailHost" id="mailHost" Class="form-control" maxlength="75" />
					</div>
		        </div>
		        
		        
		        		        <div class="row">
			    	<div class="col-sm-12 col-md-12">
			      		<label><strong><spring:message code="lblMailPort"/></strong><span class="required">*</span></label>
			           	<input type="text" name="mailPort" id="mailPort" Class="form-control" maxlength="75" />
					</div>
		        </div>
		           
		       
 	
 </div>
 </div>
 	<div class="modal-footer">
	 	<table width="100%">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large  pull-right" aria-hidden="true" id="cancelUser1" data-dismiss="modal"><spring:message code="btnCancel"/></button>
		 			<button type="button" class="btn btn-large btn-primary pull-right" style="margin-right:5px;" onclick="return saveEmailRecord();"><spring:message code="btnSave"/></button>
		 		    
				
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>			     
					     
					     
					     
					     <!--  -->
					   
					     
					    
					      </c:when>
					      <c:otherwise>
					      </c:otherwise>
					    </c:choose>
			          
				  <!-- end  -->
		          
		          
		          <!--  -->
	         <div class="row col-sm-3 col-md-3 top20" style="margin-top: 8px;">        	       	
		           <button class="btn btn-primary" type="submit" onclick="validateEditSettings()"><spring:message code="btnSub"/> <i class="icon"></i></button>			
	        </div>		
	</div>

<div class="clearfix"></div>

</form:form> 
<br/><br/>

    <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>


<div onkeypress="chkForEnter(event)" class="modal hide"  id="myModalTwr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-feedbackandsupport">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><img src='images/twi.png'/><spring:message code="headTwConf"/></h3>
	</div>
	<div class="modal-body">
		<div class="col-sm-12 col-md-12">
			<div class='divErrorMsg' id='errordivTwr' style="display: block;"></div>
		</div>
		
		
			<div class="col-sm-12 col-md-12">
				<label><strong><spring:message code="lblConsKey"/></strong><span class="required">*</span></label>
	        	<input id="consumerKey" type="text" class="form-control" maxlength="100" />
	        	<input id="twitterConfigId" type="hidden" />
			</div>	            
    
			<div class="col-sm-12 col-md-12">
		    	<label><strong><spring:message code="lblConsSct"/></strong><span class="required">*</span></label>
	        	<input id="consumerSecret" type="text" class="form-control" maxlength="100"/>
			</div>		
		
			<div class="col-sm-12 col-md-12">
		    	<label><strong><spring:message code="lblAccTkn"/></strong><span class="required">*</span></label>
	        	<input id="accessToken" type="text" class="form-control" maxlength="100" />
			</div>	
	
			<div class="col-sm-12 col-md-12">
		    	<label><strong><spring:message code="lblAccTknSct"/></strong><span class="required">*</span></label>
	        	<input id="accessTokenSecret" type="text" class="form-control" maxlength="100" />
			</div>
		
 	</div>
 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
 		<button class="btn btn-primary" onclick="saveTwiterConfig();"><spring:message code="btnSvChng"/></button>
 	</div>
</div>
	</div>
</div>


<script type="text/javascript" src="dwr/interface/SocialServiceAjax.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/usersettings.js?ver=${resouceMap['js/usersettings.js']}"></script>
<script>
	document.getElementById("firstName").focus();
	$('#myModal').modal('hide');
</script>
<div id="fb-root"></div>
<script type="text/javascript">
        
		  	window.fbAsyncInit = function() {
	         FB.init({
		        appId: '${appId}',
		        status: true, 
		        oauth : true, 
		        cookie: true,
		        xfbml: true,
		        channelURL : 'usersettings.do'   
	        });
	};
		(function() {
		//var e = document.createElement('script'); e.async = true;
		var e = document.createElement('script'); e.async = false;
		e.src = document.location.protocol +
		'//connect.facebook.net/en_US/all.js';
		document.getElementById('fb-root').appendChild(e);
		}());
          	
          	///////////////////////////
          	//your fb login function

			function fblogin() {

				/*FB.getLoginStatus(function(response) {
				  if (response.status === 'connected') {
				    // the user is logged in and has authenticated your
				    // app, and response.authResponse supplies
				    // the user's ID, a valid access token, a signed
				    // request, and the time the access token 
				    // and signed request each expire
				    var uid = response.authResponse.userID;
				    var accessToken = response.authResponse.accessToken;
				    alert("connected");
				  } else if (response.status === 'not_authorized') {
				    // the user is logged in to Facebook, 
				    // but has not authenticated your app
				  } else {
				    // the user isn't logged in to Facebook.
				    alert("the user isn't logged in to Facebook.");
				  }
				 }); */
				
				FB.login(function(response) 
	            {
	            	
	            	var uid = response.authResponse.userID;
	            	var access_token = response.authResponse.accessToken;
	            	
	            	//$('#message2show').html("uid : "+uid+"<br>access_token : "+access_token+" data:"+dwr.util.toDescriptiveString(response,5));
	            	
					//$('#myModal2').modal('show');
	            	saveFacebookConfig(uid,access_token);
	            	//FB.logout();
	            	//alert(uid);
	            	//alert(access_token);
	            	//prompt('j',access_token);
				//}, {scope:'user_interests,user_activities,user_birthday,user_location,email,publish_stream,user_hometown,offline_access,manage_pages'});
				}, {scope:'email,publish_stream,publish_actions,offline_access,manage_pages'});
	        //publish_actions
			}
</script>

<script type="text/javascript">
	//initialize the 3 popup css class names - create more if needed
	var matchClass=['popup1','popup2','popup3'];
	//Set your 3 basic sizes and other options for the class names above - create more if needed
	var popup1 = 'width=400,height=300,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=20,top=20';
	var popup2 = 'width=800,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=20,top=20';
	var popup3 = 'width=1000,height=750,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=20,top=20';
	
	//The pop-up function
	function tfpop(){
			var x = 0;
			var popClass;
			//Cycle through the class names
			while(x < matchClass.length){
					popClass = "'."+matchClass[x]+"'";
					//Attach the clicks to the popup classes
					$(eval(popClass)).click(function() {
							//Get the destination URL and the class popup specs
							var popurl = $(this).attr('href');
							var popupSpecs = $(this).attr('class');
							//Create a "unique" name for the window using a random number
							var popupName = Math.floor(Math.random()*10000001);
							//Opens the pop-up window according to the specified specs
							newwindow=window.open(popurl,popupName,eval(popupSpecs));
							return false;
					});							
			x++;
			} 
	}
	
	//Wait until the page loads to call the function
	
	
   function clearEmailcredentials()
   {
   $("#personalEmailAddress").val("");
   $("#mailPassword").val("");
   $("#mailHost").val("");
   $("#mailPort").val("");
}



	
	$(function() {
		tfpop();
	});
	
	function emailPopUpShow()
{
	$("#emailCrid").modal('show');
	clearEmailcredentials();
	$(this).find("[autofocus]:first").focus();
	//$("#personalEmailAddress").focus();
    $("#errorDiv").hide();
}
function emailPopUpHide()
{

	$('#emailCrid').modal('hide');
		
}
jQuery(document).ready(function(){

  jQuery("#cancelUser1").click(function(){
$('#emailCrid').modal('hide');
    
     return false;
  });
});
	
</script>
