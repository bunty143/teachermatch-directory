<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/ManageFacilitatorsAjax.js?ver=${resourceMap['ManageFacilitatorsAjax.ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<script type="text/javascript" src="js/managefacilitators.js?ver=${resourceMap['js/managefacilitators.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<link rel="stylesheet" type="text/css" href="css/slider.css" />  

<style>
.btn-dangerevents {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #da4f49;
  *background-color: #bd362f;
  background-image: -moz-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ee5f5b), to(#bd362f));
  background-image: -webkit-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: -o-linear-gradient(top, #ee5f5b, #bd362f);
  background-image: linear-gradient(to bottom, #ee5f5b, #bd362f);
  background-repeat: repeat-x;
  border-color: #bd362f #bd362f #802420;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffee5f5b', endColorstr='#ffbd362f', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}
.btn-dangerevents:hover,
.btn-dangerevents:focus,
.btn-dangerevents:active,
.btn-dangerevents.active,
.btn-dangerevents.disabled,
.btn-danger[disabled] {
  color: #ffffff;
  background-color: #bd362f;
  *background-color: #a9302a;
}

.btn-dangerevents:active,
.btn-dangerevents.active {
  background-color: #942a25 \9;
}

strong {
font-weight: normal;
}

</style>


<script>

function applyScrollOnTblUser()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#userTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[45,120,150,150,265,265], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}
function applyScrollOnTblFacilitator()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#facilitatorTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,150,270,300,75], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}

</script>



<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblInvFaci"/></div>	
         </div>
          <div class="pull-right add-employment1">
         
				<!--<a href="javascript:void(0);" onclick="return showFacilitatorDiv()">+Add Facilitator(s)</a>
		 --></div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>
 <!--<div class="row top10">
	 <div class="col-sm-12 col-md-12">
	 <b style="font-family: 'Century Gothic','Open Sans', sans-serif;color: #474747;font-size: 13px;">Invite Facilitator</b>
	  
 </div>

  </div>
 
  -->
  <div class="row">
	  <div class="col-sm-12 col-md-12">			                         
	 			<div class='divErrorMsg top10' id='errordivAuto' ></div>
	  </div>
  </div>	
  <div class="row top10">
    <div class="col-sm-10 col-md-10" >
		 <label id="captionDistrictOrSchool"><spring:message code="lblFacilitator"/></label>
		 <div class="pull-right">
                   <a href="javascript:void(0);" onclick="return showaddfacilitatoraDiv()"><spring:message code="lnkAddNew"/></a>
           &nbsp;&nbsp;<a href="javascript:void(0);" onclick="return importfacilitatoraDiv()"> <spring:message code="lnkImport"/> </a>
          </div>
					     <span>
					     <input type="text" id="facilatatorName" maxlength="100"
									name="facilatatorName" class="form-control" placeholder=""
									onfocus="getFacilitatorAuto(this, event, 'divTxtShowData2', 'facilatatorName','facilitatorId','');"
									onkeyup="getFacilitatorAuto(this, event, 'divTxtShowData2', 'facilatatorName','facilitatorId','');"
									onblur="hideFacilitatorDiv(this,'facilitatorId','divTxtShowData2');" />
						      </span>
						      <input type="hidden" id="facilitatorId"/>
			<div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','facilatatorName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>			      
      </div>
      <div class="col-sm-2 col-md-2 top26">
		        <label class=""></label>
		        <button class="btn btn-primary" type="button" onclick="addFacilatatorByAutoSujjest();">&nbsp;&nbsp;<spring:message code="btnAdd"/>&nbsp;&nbsp;<i class="icon"></i>&nbsp;&nbsp;</button>
		       
	  </div>
  </div>
  
  
<!--********************** Main Grid *************************-->

    <div class="TableContent top15">  	
             <div class="table-responsive" id="facilitatorGrid" >    
             </div>       
    </div>


       
               
              <div class="row col-sm-12 col-md-12">                
                    <div id="facilitatorNewDiv"  class="row">
                                       
                  </div>
                </div>

            
              <div class="clearfix">&nbsp;</div>
          

<!--*********************** End ******************************-->
	 
	                  
	                  
<div class="col-sm-12 col-md-12 top10">			                         
 			<!--<div class='divErrorMsg' id='errordiv' ></div>
	  --></div>	
	                    
  <div id="addfacilitator" class="hide">
<div class="row">
<div class="col-sm-4 col-md-4">
<label class="radio">
<input type="radio" name="chooseoption" id="chooseoption" value="selectfacilitator" checked="" onclick="getFacilitatorAddDiv()">
<spring:message code="lblSelectFacilitator"/>
</label>
</div>
<c:if test="${importacilitatorCheck ne 1}">
<div class="col-sm-4 col-md-4">
<label class="radio">
<input type="radio" name="chooseoption" id="chooseoption" value="importfacilitator"  onclick="getFacilitatorAddDiv()">
<spring:message code="lblImportFacilitator"/>
</label>
</div>
</c:if>

<div class="col-sm-4 col-md-4">
<label class="radio">
<input type="radio" name="chooseoption" id="chooseoption" value="addfacilitator"  onclick="getFacilitatorAddDiv()">
<spring:message code="lblEtrFacilitator"/> 
</label>
</div>
</div>
<div id="selFact" class="hide">



		


 <div class="row top15">
	                    <div class="col-sm-12 col-md-12">
	                    <div id="userGrid">
		                  <table id="userTable" width="100%" border="0" class="table table-bordered mt10">
		                  </table>
	                    </div>
	                   </div>
	                  </div>

 <input type="hidden" id="gridNo" name="gridNo" value="">
</div>	


</div>
<div class="row top10">
     
       <div class="col-sm-6 col-md-6">
          
           <button onclick="exitFacilitator()" class="btn btn-dangerevents" id="save"><strong><spring:message code="btnClr"/> <i class="icon"></i></strong></button>
	   </div>
	   <div class="col-sm-6 col-md-6" style="text-align: right;">  
	    <button onclick="backFacilitator()" class="btn btn-primary" id="save"><strong><i class="backicon"></i> <spring:message code="btnBack"/></strong></button> &nbsp;&nbsp;
	       <button onclick="moveParticipant()" class="btn btn-primary" id="save"><strong><spring:message code="btnNext"/> <i class="icon"></i></strong></button>
	   </div>
</div>

<div  class="modal hide"  id="myModalactMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm2"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='confirmDeactivate'>
		<spring:message code="msgDoYuWantToRemoveFacilitator"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn  btn-primary" onclick="removeFacilitatorById();"><spring:message code="btnYes"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideMessageDiv()"><spring:message code="btnNo"/></button>
 	</div>
 	<input type="hidden" id="facilitatordelId" name="facilitatordelId">
 	
</div> 	
</div>
</div>


 <div style="clear: both;"></div>

 <div class="modal hide"  id="addfacilitatoraDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:850px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
	   	<h3 id=""><spring:message code="headTm"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			  
					 <div class="col-sm-12 col-md-12 row top10">			                         
 			   <div class='divErrorMsg' id='errordiv' ></div>
	        </div>	
		   		<div class="row">
		  			<div class="col-sm-3 col-md-3">
		    		 	<label><spring:message code="lblFname"/><span class="required">*</span></label>
		    				 <input class="form-control" type="text" id="facfirstName" name="facfirstName" maxlength="50"/>
		  			</div>
					<div class="col-sm-3 col-md-3">
		    			 <label><spring:message code="lblLname"/><span class="required">*</span></label>
		     				<input class="form-control" type="text" id="faclasttName" name="faclasttName" maxlength="50"/>
	   				 </div>
					<div class="col-sm-6 col-md-6">
		    			 <label><spring:message code="lblEmailAddress"/><span class="required">*</span></label>
		     				<input class="form-control" type="text" id="facemailAddress" name="facemailAddress" maxlength="50"/>
					</div>
    			</div>
				<div class="row top10">
			   		<div class="col-sm-12 col-md-12">
			      				<a href="javascript:void(0);" onclick="return addFacilitator(0);"><spring:message code="lnkImD"/></a>&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onclick="cancelFacilitator();"><spring:message code="lnkCancel"/></a>
			   		</div>
		       </div>
             
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
	


<div style="clear: both;"></div>

 <div class="modal hide"  id="importfacilitatoraDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:750px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
	   	<h3 id=""><spring:message code="headImportFacilatator"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="col-sm-12 col-md-12 top10">			                         
 			   			<div class='divErrorMsg' id='errordivImport' ></div>
	       			 </div>	
			<div class="" id="pritOfferReadyDataTableDiv"  >  
					 
	       			 
	       			 <div class="row">
	       			 	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
						</iframe>
						<form id='facilitatorUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='facilitatorUploadServlet.do' class="form-inline">
						<div class="col-sm-3 col-md-3">
						    <spring:message code="lblBrowse"/>
						   <input name="teacherfile" id="facilitatorfile" type="file">
						</div>	
						
						<div class="col-sm-9 col-md-9">
						<button class="btn btn-primary top13"  type="button" onclick="return validateFacilitatorFile();"  style="width: 100px;"><spring:message code="btnImport"/> <i class="icon"></i></button>
						</div>
						</form>
					</div>
					<div class="row">	
					<div class="col-sm-8 col-md-8 mt10">
							<table>
							<tr><td></td><td class="required"><spring:message code="headN"/></td></tr>
							<tr><td valign="top">*</td><td> <spring:message code="msgExelFi&SubsequntRData"/></td></tr>
							<tr><td valign="top">*</td><td><spring:message code="msgExcelFColN"/></td></tr>
							<tr><td></td><td><spring:message code="lblfacilitatorFName"/></td></tr>
							<tr><td></td><td><spring:message code="lblfacilitatorLName"/></td></tr>
							<tr><td></td><td><spring:message code="lblfacilitatorEmailAddress"/></td></tr>
							<tr><td valign="top">*</td><td><spring:message code="msgColNaCaseInSent"/></td></tr>
						</table>
					</div>
					</div>
				
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick="hideimportDiv()"><spring:message code="btnClsoe"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>

<div style="clear: both;"></div>

 <div class="modal hide"  id="tempfacilitatoraDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:850px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
	   	<h3 id=""><spring:message code="headImportFacilatator"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="tempfacilitatoraDivView" >  
					 
		   		
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
				<td  nowrap>
		 			<button onclick="uploadFacilitator()" class="btn btn-primary" id="save"><strong><spring:message code="btnApt"/><i class="icon"></i></strong></button>	     	
			        &nbsp;&nbsp;
			        <button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick="rejectFacilitator()"><spring:message code="btnClr"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>




 <div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>
<input type="hidden" id="eventId" name="eventId" value="${eventId}">
<script type="text/javascript">

getFacilitatorGrid();
getFacilitatorGridNew();
</script>