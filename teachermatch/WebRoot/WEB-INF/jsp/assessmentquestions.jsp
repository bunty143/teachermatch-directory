
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/AssessmentAjax.js?ver=${resourceMap['AssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/assessment.js?ver=${resourceMap['js/assessment.js']}"></script>
<style>
.table-striped tbody tr:nth-child(odd) td, .table-striped tbody tr:nth-child(odd) th {
background-color: #F2FAEF;
}
.table th, .table td {
 padding:10px 4px;
}
</style>
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMngQuesSec" /></div>	
         </div>			
		 <div class="pull-right add-employment1">
		<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
		<a href="assessmentsectionquestions.do?sectionId=${assessmentSection.sectionId}" ><spring:message code="lnkAddQues" /></a>
		</c:if>

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<div class="row">
<div class="col-sm-12 col-md-12 mt10">
<c:set var="assType" value="Base" />
		<c:if test="${assessmentSection.assessmentDetail.assessmentType==2}">
			<c:set var="assType" value="Job Specific" />
		</c:if>
		<c:if test="${assessmentSection.assessmentDetail.assessmentType==3}">
			<c:set var="assType" value="Smart Practices" />
		</c:if>
		<c:if test="${assessmentSection.assessmentDetail.assessmentType==4}">
			<c:set var="assType" value="IPI" />
		</c:if>
<a href="assessment.do"><spring:message code="msgInventory2" /></a>: ${assessmentSection.assessmentDetail.assessmentName} (${assType})
<br/> 
</div>
<div class="left15 mt10">
<div class="pull-left top5">
<a href="assessmentsections.do?assessmentId=${assessmentSection.assessmentDetail.assessmentId}"><spring:message code="lnkSection" /></a>: 
</div>

 
<div class="col-sm-6 col-md-6 pull-left" style="margin-left: -10px;">
	                <select  class="form-control" name="assessmentSections" id="assessmentSection1" onchange="getSectionQuestions(this);">
					<c:forEach items="${assessmentSections}" var="assessmentSection1">	
						<c:set var="selected" value=""></c:set>
							<c:if test="${assessmentSection1.sectionId == assessmentSection.sectionId }">
								<c:set var="selected" value="selected"></c:set>
							</c:if>
							<option id="${assessmentSection1.sectionId}" value="${assessmentSection1.sectionId}" ${selected}>${assessmentSection1.sectionName}</option>
					</c:forEach>	
					</select>  
</div>				         
                  
</div>
</div>

<input type="hidden" id="assessmentId" value="${assessmentSection.assessmentDetail.assessmentId}">
<input type="hidden" id="sectionId" value="${assessmentSection.sectionId}">


<c:if test="${assessmentSection.assessmentDetail.assessmentType==1 || assessmentSection.assessmentDetail.assessmentType==3 || assessmentSection.assessmentDetail.assessmentType==4}">
<div class="row top15">
             <div class="col-sm-3 col-md-3">
				<label><spring:message code="msgDomain2" /><span class="required">*</span></label>
		         <select  class="form-control" name="domainMaster" id="domainMaster" onchange="getCompetencies(this.value)">
		         <option value="0"><spring:message code="StrSltDom" /></option>
					<c:forEach items="${domainMasters}" var="domainMaster">	
						<option id="${domainMaster.domainId}" value="${domainMaster.domainId}" >${domainMaster.domainName}</option>
					</c:forEach>	
					</select>	
              </div>
              
            <div class="col-sm-3 col-md-3">           
             <div class="span6"><label><spring:message code="msgCompetency2" /><span class="required">*</span></label>
            <select class="form-control" name="competencyMaster" id="competencyMaster" onchange="getObjectives(this.value)">
            <option value="0"><spring:message code="lblSelectCompetency1" /></option>
				</select>
			</div>		
            </div>
            
			<div class="col-sm-3 col-md-3">            
            <div class="span6"><label><spring:message code="msgObjective2" /><span class="required">*</span></label>
            <select class="form-control" name="objectiveMaster" id="objectiveMaster">
            <option value="0"><spring:message code="msgSelectObjective" /></option>
			</select>
			</div>		
            </div>
          
             <div class="col-sm-2 col-md-2">		
			<button class="btn btn-primary fl top25-sm" onclick="getAssessmentSectionQuestionsGrid();" type="button"><spring:message code="btnSearch" /> <i class="icon"></i>
			</button>
		  </div>
</div>
</c:if>
<div class="top15">
<table width="100%" border="0" class="table table-bordered table-striped" id="tblGrid">
</table>
</div>


<script type="text/javascript">
//document.charset = 'UTF-8';
getAssessmentSectionQuestionsGrid();
</script>