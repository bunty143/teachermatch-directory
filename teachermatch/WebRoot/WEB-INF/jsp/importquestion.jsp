<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="dwr/interface/QuestionUploadTempAjax.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/importquestion.js"></script>
	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headImpQues"/></div>	
         </div>         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>

<div class="row top20">
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-5 col-md-5 importborder" style="margin-left: 15px;margin-right: 15px;">
		<iframe id="uploadFrameID" name="uploadFrame" height="0" width="0" frameborder="0" scrolling="yes" sytle="display:none;"></iframe>
		<form id='questionUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='questionUploadServlet.do' class="form-inline">
			<input type="hidden" name="assessmentId" id="assessmentId" value="${assessmentId}">
			<input type="hidden" name="sectionId" id="sectionId" value="${sectionId}"> 
			<table cellpadding="5" align="center" >
				<tr>
				<td colspan="2" style="padding-top:10px;">&nbsp;
				 <div class="control-group">			                         
				 	<div class='divErrorMsg' id='errordiv'></div>
				 </div>
				</td>
			</tr>
			<tr><td ><label><spring:message code="headImpQues"/><span class="required">*</span></label></td><td><input name="teacherfile" id="teacherfile" type="file"></td></tr>	
			<tr><td><button class="btn btn-primary fl" type="button" onclick="return validateTeacherFile();" style="width: 100px;"><spring:message code="btnImpt"/> <i class="icon"></i></button></td></tr>
			</table>
		</form>
	</div>
</div>
<div class="row">	
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-8 col-md-8 mt10" style="margin-left: 5px;margin-right: 5px;">
			<table>

				<tr><td></td><td class="required"><spring:message code="headN"/></td></tr>
				<tr><td valign="top">*</td><td><spring:message code="msgExelFi&SubsequntRData"/></td></tr>
				<tr><td valign="top">*</td><td><spring:message code="msgExcelFColN"/></td></tr>
				<tr><td></td><td><spring:message code="lblDomN"/></td></tr>
				<tr><td></td><td><spring:message code="lblCompetN"/></td></tr>
				<tr><td></td><td><spring:message code="lblObjN"/></td></tr>
				<tr><td></td><td><spring:message code="lblSt1"/></td></tr>
				<tr><td></td><td><spring:message code="lblSt2"/></td></tr>
				<tr><td></td><td><spring:message code="lblSt3"/></td></tr>
				<tr><td></td><td><spring:message code="lblQues"/></td></tr>
				<tr><td></td><td><spring:message code="isExperimental"/></td></tr>
				<tr><td></td><td><spring:message code="lblQueWei"/></td></tr>
				<tr><td></td><td><spring:message code="lblQueTy"/></td></tr>
				<tr><td></td><td><spring:message code="lblItemCode"/></td></tr>
				<tr><td valign="top">*</td><td><spring:message code="lblQuesTyAtLOptReq"/></td></tr>
				<tr><td></td><td><spring:message code="lblSglSel"/></td></tr>
				<tr><td></td><td><spring:message code="lblLiSca"/></td></tr>
				<tr><td></td><td><spring:message code="lblRaTy"/></td></tr>
				<tr><td></td><td><spring:message code="lblT/F"/></td></tr>
				<tr><td valign="top">*</td><td><spring:message code="lblFollFiMaind"/></td></tr>
				<tr><td></td><td><spring:message code="lblMaxMark"/></td></tr>
				<tr><td></td><td><spring:message code="lblInst"/></td></tr>
				<tr><td valign="top">*</td><td><spring:message code="lblColNaOdr"/></td></tr>
				<tr><td valign="top">*</td><td><spring:message code="msgColNaCaseInSent"/></td></tr>

			</table>
	</div>
</div>
<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div> 
