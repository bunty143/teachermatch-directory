<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/RequisitionAjax.js?ver=${resourceMap['RequisitionAjax.Ajax']}"></script>
<script type='text/javascript' src="js/managerequisitionno.js?ver=${resourceMap['js/managerequisitionno.js']}"></script>
 
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#reqNoTable').fixheadertable({//table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[300,150,105,100,80,100,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
 </script> 
    <div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headManageReqNo"/></div>	
         </div>
          <div class="pull-right add-employment1">
				<a href="javascript:void(0);" onclick="return addNewRequisitionNo()"><spring:message code="lnkAddReqNo"/></a>
		 </div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>

<c:if test="${entityID eq 1}">	
<div  onkeypress="return chkForEnterSearchReqNo(event);">
	   
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="row mt10 <c:out value="${hide}"/>">
					<div  id="Searchbox" class="c:out value="${hide}"/>
	         		   
		             		<div class="col-sm-5 col-md-5">
		             			<label id="captionDistrictOrSchool"><spring:message code="lblDistrict"/></label>
			             		<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"	/>
								<input type="hidden" id="districtIdFilter" value="${userMaster.districtId.districtId}"/>
								<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>	
			             		<div class="col-sm-5 col-md-5">
		             			<label id="captionDistrictOrSchool"><spring:message code="lblReqNo"/></label>	
		             			<input type="text" id="requisitionNoId" name="requisitionNoId" class="form-control"/>
		             			</div>    	
	          		</div>
					<div class="col-sm-2 col-md-2">
						<label>&nbsp;</label>
						<button class="btn btn-primary top25-sm2" type="button" onclick="searchReqNoList()"><spring:message code="btnSearch"/> <i class="icon"></i></button>
					</div>
        	  	</div>
       		 </form>
	        
</div>
</c:if>
<c:if test="${entityID eq 2}">
	   	<input type="hidden" id="districtId" value="${districtId}"/>
	   	 <div  onkeypress="return chkForEnterSearchReqNo(event);">
	   	 <div class="row ">
          		<div class="col-sm-5 col-md-5">
          		<label><spring:message code="lblDistrict"/></label>
           		<input type="text" id="districtName" name="districtName" value="${districtName}" class="form-control" disabled="disabled"/>
			</div>
			<div class="col-sm-5 col-md-5">
				<label id="captionDistrictOrSchool">
					<spring:message code="lblReqNo" />
				</label>
				<input type="text" id="requisitionNoId" name="requisitionNoId"
					class="form-control" />
			</div>
			<div class="col-sm-2 col-md-2">
						<label>&nbsp;</label>
						<button class="btn btn-primary top25-sm2" type="button" onclick="searchReqNoList()"><spring:message code="btnSearch"/> <i class="icon"></i></button>
						</div>
					</div>
	    </div>
	   </c:if>	
<div class="TableContent top15">        	
           <div class="table-responsive" id="reqNoGrid">          
           </div>            
</div> 

<div class="row mt10">
	  <div class="col-sm-12 col-md-12">			                         
 			<div class='divErrorMsg' id='errordiv' ></div>
	  </div>	
</div>   
<div id="reqNoDiv" class="hide" onkeypress="return chkForEnterSaveReqNo(event);">
	 <input type="hidden" id="entityID" value="${entityID}">	
	  
	   <c:if test="${entityID eq 1}">	
		  <div class="row ">
          		<div  class="col-sm6 col-md-6">
          		<label><spring:message code="lblDistrict"/></label>
           		<input type="text" id="districtName" name="districtName"  class="form-control"
           		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
				<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;'
				 onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
				<input type="hidden" id="districtId"/>
           	</div>
	      </div>
       </c:if>	

	  <div class="row">
		<div class="col-sm-3 col-md-3"><label><strong><spring:message code="lblReqNo"/><span class="required">*</span></strong></label>
			<input type="hidden" id="districtRequisitionId" name="districtRequisitionId"/>
			<input type="text" name="reqno" id="reqno" maxlength="50" class="form-control" />
		</div>
		<div class="col-sm-3 col-md-3"><label><strong><spring:message code="lblPostionT"/><span class="required">*</span></strong></label>			
			<select class="form-control" id="posType">			
			<option value="F"><spring:message code="optFullTime"/></option>
			<option value="P"><spring:message code="optPartTime"/></option>
			</select>
		</div>
	  </div>
	  <div class="row top15">
	  	<div class="col-sm-4 col-md-4 top8">
	     	<button onclick="saveRequisitionNo();" class="btn btn-primary"><strong><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>	     	 
	
     	
     	<a href="javascript:void(0);" onclick="cancel()"><spring:message code="lnkCancel"/></a>  
     	
	  </div>
	   </div>
</div> 
<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
		<!-- <tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'>Your report is being generated...</td></tr> -->
	</table>
</div>
<script>
	//$('#iconpophover1').tooltip();
	//$('#iconpophover6').tooltip();	
	DisplayRequisitionNos();
</script>
