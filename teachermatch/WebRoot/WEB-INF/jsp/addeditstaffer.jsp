<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>


<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="dwr/interface/StafferAjax.js?ver=${resourceMap['StafferAjax.ajax']}"></script>
<script type='text/javascript' src='js/addeditstaffer.js'?ver=${resourceMap['js/addeditstaffer.js']}></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.ajax']}"></script>

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#stafferTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 300,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[230,240,185,180,180], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
 </script>


	<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headA/E"/>
					<span id="districtOrSchoolTitle"><spring:message code="headStaffer"/> </span></div>	
         </div>			
		 <div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="addStaffer();"><spring:message code="lnkAddStaffer"/></a>
		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
	</div>
	
	<c:set var="notShow" />
	<script>
		getStafferList();
	</script>
	<c:if test="${userMaster.entityType==1}">
		<c:set var="notShow" value="display:none;" />
		<div id='searchItem'>
		     <div class="row"><div class="col-sm-5 col-md-5">
		     <div class='divErrorMsg' id='errordivDistrict'></div>
		     </div>
		     </div>			
			<div class="row">
				<div class="col-sm-5 col-md-5">
					<label>
						<spring:message code="lblDistrictName"/>
						<span class="required">*</span>
					</label>
					<span> <input type="text" style="" 
							id="districtName" autocomplete="off" maxlength="100"
							name="districtName" class="help-inline form-control"
							onfocus="getDistrictAuto(this, event, 'divTxtShowData1', 'districtName','districtId','');showSchool();"
							onkeyup="getDistrictAuto(this,event,'divTxtShowData1', 'districtName','districtId','');showSchool();"
							onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData1');showSchool();"
							tabindex="1" /> </span>
					<input type="hidden" id="districtId" name="districtId"
						value="${districtId}" />
					<div id='divTxtShowData1'
						onmouseover="mouseOverChk('divTxtShowData1','districtName')"
						style='display: none; position: absolute; z-index: 5000;'
						class='result'></div>
				</div>
				<div class="col-sm-5 col-md-5">
				<label>
					<spring:message code="lblSchoolName"/>
					<span class="required">*</span>
				</label>
				<input type="text" id="schoolName1" disabled="disabled" class="form-control"
					maxlength="100" name="schoolName"  placeholder=""
					onfocus="getSchoolAuto(this, event, 'divTxtShowData3', 'schoolName1','schoolId1','');"
					onkeyup="getSchoolAuto(this, event, 'divTxtShowData3', 'schoolName1','schoolId1','');"
					onblur="hideSchoolMasterDiv(this,'schoolId1','divTxtShowData3');" />
				<input type="hidden" id="schoolId1" name="schoolId" />
				<div id='divTxtShowData3' style='display: none; position: absolute;'
					onmouseover="mouseOverChk('divTxtShowData3','schoolName1')"
					class='result'></div>
			</div>
			<div class="col-sm-2 col-md-2">				
				<button class="btn btn-primary top25-sm" type="button" 	onclick="getStafferList();">
					<spring:message code="btnSearch"/>&nbsp;
					<i class="icon"></i>
				</button>
			</div>
		</div>
		</div>
	</c:if>
	 <div class="TableContent top15">        	
            <div class="table-responsive" id="stafferGrid">          
                	         
            </div>            
   </div>
	
	
   <div class="TableContent top15">        	
            <div class="table-responsive" id="divMain">          
                	         
            </div>            
   </div>
	

	<div id="stafferDiv" class="hide" onkeypress="return chkForEnterStaffer(event);">		
			<c:set var="notShow" value="display:none;" />
			<div id='searchItem'>
			<div class="row">
			<div class="col-sm-8 col-md-8">
				<div class='divErrorMsg' id='errordivDistrict1'
					style=" background: white;"></div>
				<div class='divErrorMsg' id='errordivSchoolName'
					style=" background: white;"></div>
				<div class='divErrorMsg' id='errordivStafferName'
					style=" background: white;"></div>
			</div>
			</div>
					
			<div class="row">
			    <div class="col-sm-5 col-md-5">
					<label>
					<spring:message code="lblDistrictName"/>
						<span class="required">*</span>
					</label>
					<span> <input type="text" class='form-control'
							disabled="disabled" id="districtName1" autocomplete="off"
							maxlength="100" name="districtName1" class="help-inline span14"
							onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName1','districtId1','');"
							onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName1','districtId11','');"
							onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');clearSchool();"
							tabindex="1" /> </span>
					<input type="hidden" id="districtId" name="districtId"
						value="${districtId}" />
					<div id='divTxtShowData'
						onmouseover="mouseOverChk('divTxtShowData','districtName1')"
						style='display: block; position: absolute; z-index: 5000;'
						class='result'></div>
				</div>
			</div>	
			<div class="row">
			    <div class="col-sm-5 col-md-5">	
						<label>
					<spring:message code="lblSchoolName"/>
					<span class="required">*</span>
				</label>
				
				<input type="text" id="schoolName" maxlength="100" name="schoolName"
					class="form-control" placeholder=""
					onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','schoolId','');stafferBySchoolId(true);"
					onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','schoolId','');"
					onblur="hideSchoolMasterDiv(this,'schoolId','divTxtShowData2');stafferBySchoolId(true);" />
				<input type="hidden" id="schoolId" />
				<div id='divTxtShowData2' style='display: none; position: absolute;z-index: 5000;'
					onmouseover="mouseOverChk('divTxtShowData2','schoolName')"
					class='result'></div>
			</div>
			</div>	
			<div class="row">
			    <div class="col-sm-5 col-md-5">		
					<label>
					<spring:message code="headStaffer"/>
					<span class="required">*</span>
				</label>
				<input type="hidden" id="stafferIdBean" />
				
				<input type="text" id="stafferName" maxlength="100"
					name="stafferName" class="form-control" placeholder=""
					onfocus="getStafferAuto(this, event, 'divTxtShowData5', 'stafferName','stafferId','');"
					onkeyup="getStafferAuto(this, event, 'divTxtShowData5', 'stafferName','stafferId','');"
					onblur="hideStafferMasterDiv(this,'stafferId','divTxtShowData5');" />
				<input type="hidden" id="stafferId" />
				
	             <div id='divTxtShowData5' style='display: none; position: absolute; '
					onmouseover="mouseOverChk('divTxtShowData5', 'stafferName');" class='result'></div>
			</div>
			</div>
			<div class="row">
			<div class="col-sm-5 col-md-5">		
			<div class="mt10">
				<a id="seveBtn" href="javascript:void(0);"
					onclick='validateStaffer();stafferBySchoolId(false);'>
					<spring:message code="lnkImD"/>&nbsp;
				</a>
				&nbsp;
				<a href="javascript:void(0);" onclick=cancelStaffer();><spring:message code="lnkCancel"/></a>
			</div>						
				</div>
			</div>
	
		</div>
	</div>

<div style="display: none; z-index: 5000;" id="loadingDiv">
	<table align="center">
		<tr>
			<td style="padding-top: 270px;" align="center">
				<!-- <img src="images/please.jpg"/> -->
			</td>
		</tr>
		<tr>
			<td style="padding-top: 0px;" align="center">
				<img src="images/loadingAnimation.gif" />
			</td>
		</tr>
	</table>
</div>

<div class="modal hide" id="deleteStaffer" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
				<spring:message code="btnX"/>
				</button>
				<h3 id="myModalLabel">
				<spring:message code="headTM"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<spring:message code="msgDltStaffer"/>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick=deleteStaffer();;>
					<spring:message code="btnOk"/>
						<i class="icon"></i>
					</button> </span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true">
					<spring:message code="btnClose"/>
				</button>
			</div>
		</div>
	</div>
</div>



<div class="modal hide" id="mystaffer" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
				<spring:message code="btnx"/>
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTM"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
			<spring:message code="msgStafferAvlForSchl"/>
					<br />
					<spring:message code="msgPlsInputNewSchlAddNewStaffer"/>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="editSchool();">
						<spring:message code="btnOk"/>
						<i class="icon"></i>
					</button> </span>
				
			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="mystafferMsg" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true"
	data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">
					<spring:message code="btnX"/>
				</button>
				<h3 id="myModalLabel">
					<spring:message code="headTM"/>
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group hide" id="successMsg">
			<spring:message code="msgStafferAddSuccessffully"/>
				</div>
				<div class="control-group hide" id="editMsg">
					<spring:message code="msgStafferEditSuccessfully"/>
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary"
						onclick="closeMsg();">
							<spring:message code="btnoK"/>
						<i class="icon"></i>
					</button> </span>
				
			</div>
		</div>
	</div>
</div>

<c:if test="${userMaster.entityType==2}">
	<input type="hidden" id="districtId" name="districtId"
		value="${districtId}" />
	<script>
	getStafferList();
</script>
</c:if>