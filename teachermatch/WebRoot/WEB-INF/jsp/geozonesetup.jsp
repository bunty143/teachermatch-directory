<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript" src="dwr/interface/GeoZoneAjax.js?ver=${resourceMap['GeoZoneAjax.Ajax']}"></script>
<script type='text/javascript' src="js/geozone.js?ver=${resourceMap['js/geozone.js']}"></script>
 <script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j("#geozoneTable").fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 862,
        minWidth: null,
        minWidthAuto: false,
        colratio:[127,170,250,100,150,80], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
 </script>
 <div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headGZSetup"/></div>	
         </div>
          <div class="pull-right add-employment1">
				<a href="javascript:void(0);" onclick="return addnew();"><spring:message code="lnkAddGZ"/></a>
		 </div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>
<input type="hidden" id="entityType" value="${entityType}"/>
<% String id=session.getAttribute("displayType").toString();%>
<input type="hidden" id="displayTypeId" value="<%=id%>"/>
<%if(id.equalsIgnoreCase("0")){ %>
<div  onkeypress="return chkForEnterSearchZone(event);">	 				
		  <div  id="Searchbox"/>
		   <c:if test="${entityType eq 1}">
       		    <div class="row top10" style="margin-bottom: 15px;">
	            	<div class="col-sm-5 col-md-5">					   
					    	<label><spring:message code="lblDistrict"/></label>
					    	<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control" 
		             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
							onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
							onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"	/>
							<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
							 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>				
	             	</div>       		
					<div class="col-sm-3 col-md-3">	
					<label>&nbsp;</label>			
					<button class="btn btn-primary top25-sm2" type="button" onclick="searchZoneList()">Search <i class="icon"></i></button>					
					</div>		 
				</div>
			</c:if> 
			<input type="hidden" id="districtIdFilter" value="${DistrictMaster.districtId}"/>
		</div>	       
</div>
<%}else{ %>
<input type="hidden" id="districtIdFilter"/>
<%} %>
<div class="TableContent top15">        	
            <div class="table-responsive" id="geozoneGrid">          
            </div>            
</div> 
 
<div class="row top5">
	  <div class="col-sm-12 col-md-12">			                         
 			<div class='divErrorMsg' id='errordiv' ></div>
	  </div>	
</div>   
<div id="geozonediv" class="hide">
<div class="row top10">
	<div  class="col-sm-5 col-md-5">
		<label><label><spring:message code="lblDistrict"/></label><span class="required">*</span></label></br>
		<c:if test="${entityType eq 2}">
	    	<label>${DistrictMaster.districtName}</label>
	    </c:if>
	    <c:if test="${entityType ne 2}">
	    	<input type="text" id="districtName" name="districtName"  class="form-control"
		    onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
			onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
			onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');getSchoolByDistrict();"	/>
			<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;'
			onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
	    </c:if>	    
	    <input type="hidden" id="entityType" value="${entityType}"/>
	    <input type="hidden" id="districtId" value="${DistrictMaster.districtId}"/>
    </div>
</div>
<div class="row">
		<input type="hidden" name="geozoneId" id="geozoneId">
		<div class="col-sm-6 col-md-6"><label><strong><spring:message code="lblGZDisN"/><span class="required">*</span></strong></label>
			<input type="text" name="geozonename" id="geozonename" maxlength="100" class="form-control" placeholder="">
		</div>
		<div class="col-sm-5 col-md-5"><label><strong><spring:message code="lblGZCode"/><span class="required">*</span></strong></label>
			<input type="text" name="geozonecode" id="geozonecode" maxlength="5" class="form-control" placeholder="">
		</div>
</div>
<div class="row" id="ZNDescription" >
<div class="col-sm-11 col-md-11">
	<label><spring:message code="lblDecr"/></label>
	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>	
	<textarea rows="8" id="zoneDescription"   name="zoneDescription"  class="form-control"></textarea>
	</div>
	
</div>
<div class="row mt10" >
			<div class="col-sm-5 col-md-5" style="padding-right: 25px;">
		 		<label><spring:message code="lblR/MSch"/></label>
				<select multiple id="avlbListSchool"  name="avlbListSchool" class="form-control" name="avlbListSchool"  style="height: 150px;" > 
				</select>
			</div>		
			<div class="col-sm-1 col-md-1 top70-sm left5" > 
				<div class="span1"> <span id="addPopSchool" style="cursor: pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
				<div class="span1"> <span id="removePopSchool" style="cursor: pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
			</div>
			<div class="col-sm-5 col-md-5">
				<label><spring:message code="lblSltSch"/><span class="required">*</span></label>	
					<select multiple class="form-control" id="attachedListSchool" name="attachedListSchool" style="height: 150px;">
					</select>
				<input type="hidden" id="editOption"/>
			</div>
</div>

<div class="row top5">
		<div class="col-sm-4 col-md-4 idone" style="width: 125px;">
		<div><a href="javascript:void(0);" onclick="saveZoneAndSchool();"><spring:message code="lnkImD"/></a>
		
	    </div>
	    </div>
	    <div class="col-sm-2 col-md-2 idone" style="width: 80px;">
	    	<a href="javascript:void(0)" onclick="cancel();"><spring:message code="lnkCancel"/></a>
	    </div>
</div>
</div>
	


<div style="display:none;" id="loadingDiv">
     <table  align="center" >
	 	<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td  id='spnMpro' align='center'></td></tr>
	</table>
</div>
<div  class="modal hide"  id="textMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  ><spring:message code="btnX"/></button>
		<h3><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="textMessage"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnOk"/></button> 		
 	</div>
</div>
</div>
</div>


<div class="modal hide"  id="deleteGeoZone"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Do you really want to delete this Geo Zone?.
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteGeoZone();" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
  </div>
 </div>
</div>


<script type="text/javascript">
if($("#showSessionStatus").val()!=undefined){
	if($("#entityType").val()!=2){
		setEntityType();
	}
}

		$('#addPopSchool').click(function() {
			    if ($('#avlbListSchool option:selected').val() != null) {
			         $('#avlbListSchool option:selected').remove().appendTo('#attachedListSchool');
			         $("#avlbListSchool").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
			         $("#attachedListSchool").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			 } else {
			 	if($("#districtId").val()!=""){
			 		document.getElementById("textMessage").innerHTML="Please select any school # before add.";
				    $("#textMessageModal").modal("show");
			 	}
			 }
			});
		
			$('#removePopSchool').click(function() {
			       if ($('#attachedListSchool option:selected').val() != null) {
			             $('#attachedListSchool option:selected').remove().appendTo('#avlbListSchool');
			             sortSelectList();
			             $("#attachedListSchool").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
			             $("#avlbListSchool").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			             $("#avlbListSchool").attr('selectedIndex', '-1').addAttr("selected");
			             
			} else {
			 	if($("#districtId").val()!=""){
				    document.getElementById("textMessage").innerHTML="Please select any school # before remove.";
				    $("#textMessageModal").modal("show");
				}    
			}
			});
		
		function sortSelectList(){
		  	var selectListAvlbList = $('#avlbList option');
		  	var selectListavlbListSchool = $('#avlbListSchool option');
			selectListAvlbList.sort(function(a,b){
			    a = a.value;
			    b = b.value;
			    return a-b;
			});
			$('#avlbList').html(selectListAvlbList);
			selectListavlbListSchool.sort(function(a,b){
			    a = a.value;
			    b = b.value;
			    return a-b;
			});
			$('#avlbListSchool').html(selectListavlbListSchool);
		  }			
		$(document).ready(function(){
		  		//$('#ZNDescription').find(".jqte").width(950);
		});
		displayGeoZoneGrid();
</script>
