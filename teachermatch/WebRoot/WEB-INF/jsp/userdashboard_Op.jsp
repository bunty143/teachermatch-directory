<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resouceMap['DashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/MyDashboardAjax.js?ver=${resouceMap['MyDashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src="dwr/util.js"></script>
<script type='text/javascript' src="js/districtquestioncampaign.js?ver=${resouceMap['js/districtquestioncampaign.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" /> 

<!-- Start ... Dynamic Portfolio -->
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resouceMap['DistrictPortfolioConfigAjax.ajax']}"></script>
<script type='text/javascript' src="js/teacher/districtportfolioconfig_ExternalJob.js?ver=${resouceMap['js/teacher/districtportfolioconfig_ExternalJob.js']}"></script> <!-- For External Job Apply -->
<script type="text/javascript" src="js/teacher/dynamicPortfolio_Common.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Common.js']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="jquery/jquery.cookie.js"></script>

<!-- Academics -->
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DistrictPortfolioConfigAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFAcademics.js?ver=${resouceMap['PFAcademics.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Academics.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Academics.js']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_AutoCompAcademics.js?ver=${resouceMap['js/teacher/dynamicPortfolio_AutoCompAcademics.js']}"></script>

<!-- Certifications -->
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Certifications.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Certifications.js']}"></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resouceMap['PFCertifications.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_AutoCompCertificate.js?ver=${resouceMap['js/teacher/dynamicPortfolio_AutoCompCertificate.js']}"></script>

<!-- Resume -->
<script type="text/javascript" src="dwr/interface/PFExperiences.js?ver=${resouceMap['PFExperiences.ajax']}"></script>
<script type='text/javascript' src="js/teacher/dynamicPortfolio_Experiences.js?ver=${resouceMap['js/teacher/dynamicPortfolio_Experiences.js']}"></script>

<!--  Non-Client Jobs -->
<script type="text/javascript" src="dwr/interface/JBServiceAjax.js?ver=${resourceMap['JBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobboard/jobboard.js" charset="utf-8"></script>
<script type='text/javascript' src="js/teacher/jobcompletenow.js?ver=${resouceMap['js/teacher/jobcompletenow.js']}"></script>
<!-- Edit Dsqp -->

<script type="text/javascript" src="dwr/interface/PrintCertificateAjax.js?ver=${resourceMap['PrintCertificateAjax.Ajax']}"></script>

<script type="text/javascript" src="js/teacher/dynamicPortfolioEditSave.js?ver=${resouceMap['js/teacher/dynamicPortfolioEditSave.js']}"></script>

<!-- New Job Apply Process -->
<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafstart.js?ver=${resouceMap['js/jobapplicationflow/jafstart.js']}"></script>

<script type="text/javascript">
jQuery(function($){
 $(document).ready(function()
 {
 // $("#initialdiv").delay(3999).fadeOut(0);
   //$("#statusdiv").delay(4000).fadeIn(1000);
   
	  if ($.cookie('noShowWelcome')!=null)
	  {
	  	$('#statusdiv').hide();
	  }
	    else {
	          $("#initialdiv").delay(3999).fadeOut(0);
	           $("#statusdiv").delay(4000).fadeIn(1000); 
	           var date = new Date();
	           var minutes = 30;  // after minutes cookie expire
	          date.setTime(date.getTime() + (minutes * 60 * 1000));
	          //alert(" Cookies Expire At  :: "+date);
	            $.cookie('noShowWelcome', 'opened', { expires: date });
	   }

	 });
  });
 
function getInitial()
{
	$("#statusdiv").delay(1).fadeOut(0);
  $("#initialdiv").delay(2).fadeIn(1000); 
}
function hideEpiInfo()
{
	$('#epiInfoModal').modal('hide');
}
function openEpiInfo()
{
	$('#epiInfoModal').modal('show');
}
function startEpi()
{
	$('#epiInfoModal').modal('hide');
	checkInventory(0,null);
}

	//shadab
function hideSPiInfo()
{
	$('#spInfoModal').modal('hide');
}
function openSPInfo()
{
	$('#spInfoModal').modal('show');
}
function startSP()
{
	$('#spInfoModal').modal('hide');
	window.location.href='service/inventory.do';
	//checkInventory(0);
}	
function readLesson()
{
	$('#lessonInfoModal').modal('show');
}

function startLesson()
{
	$('#lessonInfoModal').modal('hide');
	//window.location.href='smartpractices.do';
	window.location.href='spuserdashboard.do';
}
function hideLesson()
{
	$('#lessonInfoModal').modal('hide');
}
	//end
</script>
<style>
.toolTip {
z-index:1001;
    position: absolute;
    display: none;
    width: 280px;
    height: auto;
    background: rgba(0,0,0,0.6);
    border: 0 none;    
    color: white;
    font: 11px;
    padding-left:5px;
    padding-right:5px;
    padding-top:5px;
    padding-bottom:5px;
    text-align: center;    
    margin:auto;
  	border-radius: 5px 5px 5px 5px;
  	box-shadow: -3px 3px 15px #888888;
    border:1px solid;
    border-color: rgba(0, 0, 0, 0.8);  
	
}
 .table td {
 padding:10px 4px;
}
.table-bordered thead:first-child tr:first-child th:first-child, .table-bordered tbody:first-child tr:first-child td:first-child
{
border-top-left-radius:0px;
webkit-border-top-left-radius: 0px;
}
.table th
{
border-top-left-radius:0px;
padding:5px;
}
  .table
  {
  border: 0px;
  }
.bg
{
background-color: #007ab4;
}

.fa-downcase-e:before {
    font-family:Segoe Print; /* font family */
    font-weight: bold;
    content: 'e';
	font-size:30px;
	color:#007AB4;
	line-height: 0.8 !important;
}
.fa-downcase-e-grey:before {
    font-family:Segoe Print; /* font family */
    font-weight: bold;
    content: 'e';
	font-size:30px;
	color: grey;
	line-height: 0.8 !important;
}
</style>
<style type="text/css">
#thin-donut-chart {
    width: 200px; 
    height: inherit;
	position:absolute;
    top:8.2px;
    float:left;
    left:-12%;
    margin:auto;   
}
.center-label, .bottom-label {
    text-anchor: middle;
    alignment-baseline: middle;
    pointer-events: none;
    font-size: 27px;
    font-weight: bold;
    color:white;
   }
.half-donut .center-label {
    alignment-baseline: initial;
}
#search222
{
position:absolute;
display:inline-block;
left:6.2px;
top:3px;
width:inherit;
height:inherit;
}
#title
{
position:absolute;
top:103%;
margin:auto;
text-transform:uppercase;
text-align:center;
font-family: 'Century Gothic', sans-serif;
color:white;
font-size: 10px;
font-weight: bold;
width:inherit;
height:inherit;
padding:2px;
line-height: 1.4;
}
.boldcolor
{
color:#007AB4;
}
.pclass
{
line-height: 1.2px;
font-size: 10px;
font-weight: bold;
}
.bucket
{
font-family: 'Century Gothic','Open Sans', sans-serif;
font-weight: bold;
color: white;
padding: 0px;
margin-left: 10px;
line-height: 35px;
}
.bucketheader
{
background-image: url(images/bucket_header2.png);
}
.step
{
background: url(images/step-line.png) center 19px no-repeat;
display: block;
max-width: 90%;
height: auto;
margin-top:35px;
}
.step ul li a span {
    color: #000000;
    display: block;
    font-family: tahoma;
    font-size: 11px;;
    font-weight: bold;
    padding: 40px 0 0;
}
.step ul li {
float: left;
margin: 0px 2%;
}
.step ul li:first-child { margin-left:-5px;}
.step ul li:last-child { margin-right:-9px;}

.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
$j(document).ready(function(){
});
</script>

<!-- End ... Dynamic Portfolio -->

<script language="javascript">
	if("${param.mm}"!='')
	{
		showInfo("An email has been sent to your selected friend(s) about this job.");
		//window.location.href='userdashboard.do';
	}else if("${param.w}"!='')
	{
		var msg ="You have been returned to the Dashboard because you did not acknowledge that you timed out. Failure to respond to a question within the time limits again may result in timed out status."
		var nextMsg =" When you are ready to complete EPI without interruption and within the time parameters, click the Resume Base Inventory icon to the right on Base Inventory on the Dashboard.";
		if("${param.w}"==1){
			if("${param.attempt}"==1){
				msg = "<b>You Did Not Take Action!</b><br>";
				nextMsg = "You have been returned to your Dashboard because you did not acknowledge your <b>FIRST </b><font color='red'>&#8220;Timed Out&#8221;</font> alert. You have <b>ONE MORE</b> attempt. Be sure to respond to questions within the <font color='red'>75-second</font> time limit or you may get a <font color='red'>&#8220;Timed Out&#8221;</font> message again. When you are ready to complete the <b>EPI</b> without interruption, click <b>&#8220;Resume EPI&#8221;</b> on your Dashboard.";
			}
			else if("${param.attempt}"==2){
				msg = "<b>Are you still here?</b><br>";
				nextMsg = "You have been returned to your Dashboard because you did not acknowledge your <b>SECOND </b><font color='red'>&#8220;Timed Out&#8221;</font> alert. You have <b>ONE FINAL</b> attempt. Be sure to respond to questions within the <font color='red'>75-second</font> time limit or you may get your last <font color='red'>&#8220;Timed Out&#8221;</font> message. After that, you will need to wait 12 months before taking the EPI again. When you are ready to complete the <b>EPI</b> without interruption, click <b>&#8220;Resume EPI&#8221;</b> on your Dashboard.";
			}
		}
		else if("${param.w}"==2){
			msg ="You have been returned to the Dashboard because you did not acknowledge that you timed out. Failure to respond to a question within the time limits again may result in timed out status."
			nextMsg =" When you are ready to complete Job Specific Inventory without interruption and within the time parameters, click the Complete Now icon to the right on Job Specific Inventory on the Dashboard. If you have multiple incomplete Job Specific Inventories, it will display you a list of incomplete Job Specific Inventories. Click the Complete Now icon against the Job Title.";
		}
		showInfo(msg+nextMsg);
		//window.location.href='userdashboard.do';
	}
function showInfo(msg)
{	
	//alert(" user dashboard Jsp Metho showInfo(msg) ")
	//$("#myModalv").css({ top: '60%' });	
	$('#warningImg1').html("<img src='images/info.png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html("");	
	var onclick = "onclick=window.location.href='userdashboard.do'";	
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');	
}
</script>
<%-- GAgan [ Start ] :Check Only for external job apply with affilated status 1 mean( In case of reseting status and isAffilated value  in jobforteacher )  --%>
<c:if test="${param.isaffilatedstatus eq 1 && param.savejobFlag eq 1}">
<script>
	$('#myModalv').modal('hide');
	document.getElementById("show").style.display = "none";
	document.getElementById("hidea").style.display = "none";
	$( document ).ready(function() {
});
	
</script>
</c:if>
<%-- GAgan [ END ] :Check Only for external job apply with affilated status 1 mean( In case of reseting status and isAffilated value  in jobforteacher )  --%>

<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<script type="text/javascript" src="dwr/interface/CandidateReportAjax.js?ver=${resouceMap['CandidateReportService.ajax']}"></script>
<script type='text/javascript' src="js/report/candidatereport.js?ver=${resouceMap['js/report/candidatereport.js']}"></script>
<!-- <script type="text/javascript" src="js/report/candidategridreport.js?ver=${resouceMap['js/report/candidategridreport.js']}"></script> -->
<script type='text/javascript' src="js/quest.js?ver=${resourceMap['js/quest.js']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>


<c:if test="${jobOrder.jobId>0}">
<c:if test="${param.epiFlag ne 'on'}">
<script language="javascript">
var url="${jobOrder.exitURL}";

if(url.indexOf("http")==-1)
url="http://${jobOrder.exitURL}";

var showurl="";
if(url.length>70)
{
  for(var i=0;i<url.length;i=i+70)
  {
	  showurl=showurl+"\n"+url.substring(i, i+70)
  } 
}
var sts = "0";
var mf="${mf}";
//alert(mf);
var divMsg="Thank you for completing";
if(mf=='n')
{
	divMsg="You have not completed";
	sts = "2";
}

divMsg=divMsg+" the TeacherMatch part of the job application. You will be directed to <a href='"+url+"' target='_blank'>"+showurl+"</a> in a new browser window, however please note that your existing TeacherMatch session is also available in your current browser window.";
</script>

<c:if test="${compltd eq 'Y' || param.es eq 1}">
	<c:if test="${JAFExternal ne 1}">
		<script language="javascript">
			showInfoAndRedirect(sts,divMsg,'',url,2);
		</script>
	</c:if>
</c:if>

</c:if>
</c:if>


<div  class="modal hide"  id="myModalDASpecificQuestions" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
		<div class="modal-dialog-for-cgdemoschedule">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"> <spring:message code="headMandatoryDecler"/></h3>
		</div>
	<div class="modal-body" style="height: 450px;overflow-y:auto" >
		<span id='textForDistrictSpecificQuestions' class="divlablevalins"><spring:message code="msgDistReqAllAnsOfQues"/></span>
			<div class='divErrorMsg' id='errordiv4question' style="display: block;padding-bottom: 7px;"></div>
			<table width="100%" border="0" class="table table-bordered table-striped" id="tblGrid">
			</table>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn btn-large btn-primary" type="button" onclick="setDistrictQuestions('dashboard');" ><strong><spring:message code="btnConti"/> <i class="icon"></i></strong></button>
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="cancelDSQdiv()"> <spring:message code="btnClose"/></button> 		
	 	</div>
		</div>
	</div>
</div>

<div id="displayDiv"></div>
<!--shadab   -->
<!--<c:if test="${teacherDetail.userType eq 'N'}">  -->
<c:if test="${powerProfile ne 'hide'}">
<c:if test="${isImCandidate eq false}">
	<div class="" style="background-color: white;margin-bottom:10px;position: relative;height: auto;position: relative;" align="center">
	<div id="statusdiv" class="container" style="position: relative;height: auto;min-height:205px;display: none;background: #e9ebea;border-radius:5px;">
	<div style="float:right;margin-right: 50px;margin-top: 10px;font-size: 14px;"><b><a  href="javascript:void(0);" onclick="getInitial();" style="color:#b31516;"> <spring:message code="lnkCLOXEX"/></a></b></div>
	<center><span class="top5" id="" style="display: block;font-size: 11px;" >
	<div style="color:black;font-size: 15px;vertical-align: middle;margin-top: 40px;">
		<div style="font-size: 18px;"> <spring:message code="lblYuArCurrently"/> <span id="statusText"></span> <spring:message code="lblQuJOSeeker"/> </div>
	</div>
	<spring:message code="lblAdHvComp"/> <b><spring:message code="lblPwrFrof"/></b>  <spring:message code="lblCatChk"/><br/></span></center>
	<div class="col-sm-12 col-md-12">
	<div class="col-sm-1 col-md-1" style="width:8.7%;"></div>
	<div class="col-sm-2 col-md-2" style="padding-right:0px;width:18.666667%">
	<div id="statusimage">
	</div>	
	</div>
	<div class="col-sm-8 col-md-8" style="padding-left:0px;">
	<div class="step">
	 <ul>
			<li style="margin-left:-54px;"><div id="hrefPersonaInfo" class="stepactive"><span> <spring:message code="lblActiv"/></span></div></li>
			
			<li style="margin-left:15px;"><a id="hrefCertifications" target="_blank" <c:if test="${(not empty teacherCertificate) and (fn:length(teacherCertificate) > 1)}"> class="stepactive" </c:if> href="certification.do?openDiv=Certifications"><span><spring:message code="lblCrede"/></span></a></li>
			<li style="margin-left:15px;"><c:choose><c:when test="${epistatus eq 'comp'}"> <div id="hrefEpi" class="stepactive"><span>TeacherMatch<br/><spring:message code="lblEPI"/></span></div> </c:when><c:otherwise><a id="hrefAcademics"  href="javascript:void(0);" onclick="checkInventory(0,null);"><span>TeacherMatch<br/> <spring:message code="lblEPI"/></span></a></c:otherwise></c:choose></li>
			<li style="margin-left:15px;"><a id="hrefCertifications" target="_blank" <c:if test="${not empty videoProfile}"> class="stepactive" </c:if> href="certification.do?openDiv=Video"><span> <spring:message code="lblVid"/><br/><spring:message code="lblProf"/></span></a></li>
			<li style="margin-left:15px;"><a id="hrefExperiences" target="_blank" <c:if test="${not empty teacherExperience}"> class="stepactive" </c:if> href="experiences.do"><span><spring:message code="lnkResume"/></span></a></li>
			<li style="margin-left:15px;"><a id="hrefExperiences" target="_blank" <c:if test="${ jobsApplied != null  and jobsApplied >= 5}"> class="stepactive" </c:if> href="jobsofinterest.do"><span><spring:message code="lblJo"/><br/> <spring:message code="lblAplid"/></span></a></li>
		</ul>
		<div class="clearfix">&nbsp;</div>
	</div>
	</div>
	</div>
	<div style="clear: both;"></div>
	<div style="font-size: 12px;vertical-align: middle;margin-top: 10px;padding-bottom: 32px;">
	 <spring:message code="msgAddUpPointsComletInfo"/><br/>
	<b><spring:message code="lblPersonalPlng"/></b>  <spring:message code="msgFldrReExLelWhrYuCnUnlockExclusive"/><br/>
	<spring:message code="msgResoToHelpMkeYuJoQuestASucce"/> 
	
	                    <!--  by  khan  -->
	                    
	                    <%if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){%>
						    <b><spring:message code="msgClkForMoreInfo"/></b>
						 <%
						 }else{ %>
						<b><a href="meetyourmentor.do"  target="_blank" ><spring:message code="msgClkForMoreInfo"/></a></b>
						 <%
						 } 
						 %>
	
	
	
	
	</div>
	</center>
	</div>
	<div id="initialdiv" class="container" style="position: relative;height: auto;min-height:205px;">
			<div class="col-sm-4 col-md-4">
			<div id="thin-donut-chart">
			
			<!-- khan -->
			
                       <% if(Utility.getValueOfPropByKey("locale").equalsIgnoreCase("fr")){
                       out.print(Utility.getValueOfPropByKey("locale"));
                       %>
						 	 <div id="search222"><div id="ppitext"></div><a href="javascript:void(0)" target="_blank" data-toggle="tooltip" data-original-title='<spring:message code="MsgToIncreasePowerProfile"/>' id="search222Tooltip"><img src="images/search_ppi.png" id="ppiimage" style="width:175px;height: 155px;"/></a></div>
						 <%
						 }else{ %>
						 
						         <div id="search222"><div id="ppitext"></div><a href="powerprofile.do" target="_blank" data-toggle="tooltip" data-original-title='<spring:message code="MsgToIncreasePowerProfile"/>' id="search222Tooltip"><img src="images/search_ppi.png" id="ppiimage" style="width:175px;height: 155px;"/></a></div>
						 <%} %>
			
		<div id="title">
		</div>
		</div>
	</div>
	<div class="" style="float:left;left:180px;position:relative;margin: auto;margin-top: -10px;width:680px;height: auto;text-align: left;">
			<span class="top5" id="jobseekertext" style="text-align: left;display: none;font-size: 11px;" ><div style="color:black;font-size: 15px;vertical-align: middle;margin-top: -7px;"><br/><b> <spring:message code="lblAllJoSeekers"/></b></div><BR/>
			 <spring:message code="msgTkYurCandStrengthOnYor"/> <b> <spring:message code="lblProPwrTrack"/></b>
			 <spring:message code="lblWhYuComplt"/><c:choose><c:when test='${epistatus eq "comp"}'><b> <spring:message code="lblTmEPI"/></b></c:when><c:otherwise><a href="javascript:void(0);" onclick="openEpiInfo();"><b> <spring:message code="lblTmEPI"/></b></a></c:otherwise></c:choose> 
			  <spring:message code="lblWhrYuUnlockallTheGmChangToolsAndSupp"/></i>
			 <spring:message code="lblHlpYuGetJob"/></span>
	<div id="show" align="center" style="height: 120px; margin-top: 10px;width:100%;margin: auto;position: relative;display: none;">
	<div class="row" style="width:100%;">
		<div id="" style="background-color:white;height: 110px;margin-top:42px;width:120px;float:left;left:20px;">
		<a href="questconnect.do" target="_blank"><img src="images/questconnectimg.png" style="width: 80%;"/></a><br/>
			<b class="boldcolor"> <spring:message code="lblQuestCot"/></b><br/>	
				<p class="pclass" style="margin-top: 11px;"> <spring:message code="lblProfess"/></p>
				<p class="pclass" style="margin-top: -4px; line-height: 9px"><spring:message code="lblPerspectivesAdSupt"/></p>
		</div>
		<!-- Amit Kumar -->
		<div id="sdiv2" style="background-color:white;height: 100px;margin-top:55px;width:120px;float:left;">	
			<img src="images/questacad.png" style="width: 68%"/>
			<div style="width: 100%; height: 35%;">
				<b id="sb2" class="boldcolor"> <spring:message code="lblQuestAcad"/></b>
				<img id="simg2" src="images/comingsoon.png" />
			</div>	
			<p class="pclass" style="margin-top: -4px;"> <spring:message code="lblRefAd"/></p>
			<p class="pclass"><spring:message code="lblLearnCentr"/></p>
		</div>
		
		<!--<div id="" style="background-color:white;height: 100px;margin-top:40px;width:120px;float:left;">
		<a href="javascript:void(0);" data-toggle="tooltip" data-original-title="Coming Soon">
			<img src="images/questacad.png" style="width: 80%"/></a><br/>
			<b class="boldcolor">Quest Academy</b>	
			<br/>	
			<p class="pclass">Reference and</p>
			<p class="pclass">Learning Center</p>
		</div> -->
		
		<div id="" style="background-color:white;height: 110px;width:155px;margin-top:43px;float:left;">
		<a target="_blank" href="meetyourmentor.do">
			 <img src="images/MeetYourMentor.png" style="width: 60%"/></a><br/>
			 <b class="boldcolor"> <spring:message code="lblMetYuMentor"/></b>	
			<p class="pclass" style="margin-top: 10px;"><spring:message code="lblInsightfulGoTo"/></p>
			<p class="pclass"> <spring:message code="lblIntrAd"/></p>
		</div>
		
	
		<div id="" style="background-color:white;height: 110px;margin-top:20px;width:120px;float:left;">
			<a href="toolkitessentials.do" target="_blank"><br/>
			<img src="images/ToolkitEssentials.png" style="width: 80%"/></a><BR/>
			<b class="boldcolor"><spring:message code="lblToolktEssent"/></b><br/>	
			<p class="pclass" style="margin-top: 9px;"> <spring:message code="lblSmtTp"/>,</p>
			<p class="pclass"><spring:message code="lblToolsAdAids"/></p>
		</div>
		
	
		<div id="" style="background-color:white;height: 110px;margin-top:20px;width:120px;float:left;">
	  <a href="intheq.do" target="_blank"><br/>
			<img src="images/intheq.png" style="width: 80%"/></a><br/>
			<b class="boldcolor"><spring:message code="lblInTheQ"/></b><br/>
			<p class="pclass" style="margin-top: 9px;"> <spring:message code="lblLifeStl"/></p>
			<p class="pclass"><spring:message code="lblPerspect"/></p>
		</div>
		
	</div>
	</div>
	</div>
	<!-- Amit Kumar -->
	<div id="hidea"  style="left:70px;float:left;height: 180px;width:100%;margin: auto;display: none;position: relative;margin-top:30px;">
	<div align="center"  style="width:100%;">
		<div id="hdiv1" style="background-color:none;height: 100px;margin-top:23px;width:142px;float:left;left:20px;">
			<img id="img" src="images/questconnectimg.png" style="width: 68%;"/>
			<div id="divv" style="width: 100%; height: 35%;">
				<b id="hb1" class="boldcolor"> <spring:message code="lblQuestCot"/></b>
				<img id="himg1" src="images/unlockstatus.png" />
			</div>
			<p class="pclass" style="margin-top: -4px;"><spring:message code="lblProfess"/></p>
			<p class="pclass"><spring:message code="lblPerspectivesAdSupt"/></p>
		</div>
	
		<div id="hdiv2" style="background-color:white;height: 100px;margin-top:24px;width:142px;float:left;">	
			<img src="images/questacad.png" style="width: 68%"/>
			<div style="width: 100%; height: 35%; margin-top: -2px;">
				<b id="hb2" class="boldcolor"><spring:message code="lblQuestAcad"/></b>
				<img id="himg2" src="images/comingsoon.png" />
			</div>	
			<p class="pclass" style="margin-top: -4px;"> <spring:message code="lblRefAd"/></p>
			<p class="pclass"> <spring:message code="lblLearnCentr"/></p>
		</div>
		
		<div id="hdiv3" style="background-color:white;height: 85px;width:193px;margin-top:10px;float:left;">	
			 <img src="images/MeetYourMentor.png" style="width: 56%"/>
			 <div style="width: 100%; height: 35%;">
			 	<b id="hb3" class="boldcolor"><spring:message code="lblMetYuMentor"/></b>
			 	<img id="himg3" src="images/unlockstatus.png" />
			 </div>
			<p class="pclass" style="margin-top: -3px;"><spring:message code="lblInsightfulGoTo"/></p>
			<p class="pclass"> <spring:message code="lblIntrAd"/></p>
		</div>
		
		<div id="hdiv4" style="background-color:white;height: 120px;margin-top:2px;width:143px;float:left;">
			<br/>
			<img src="images/ToolkitEssentials.png" style="width: 68%"/>
			<div style="width: 100%; height: 30%;">
				<b id="hb4" class="boldcolor"> <spring:message code="lblToolktEssent"/></b>
				<img id="himg4" src="images/unlockstatus.png" />
			</div>
			<p class="pclass" style="margin-top: -6px"> <spring:message code="lblSmtTp"/></p>
			<p class="pclass"> <spring:message code="lblToolsAdAids"/></p>
		</div>
		
		<div id="hdiv5" style="background-color:white;height: 100px;margin-top:3px;width:140px;float:left;"> 
		<br/> 
			<img src="images/intheq.png" style="width: 68%"/>
			<div style="width: 100%; height: 35%;">
				<b id="hb5" class="boldcolor"> <spring:message code="lblInTheQ"/></b>
				<img id="himg5" src="images/unlockstatus.png" />
			</div>
			<p class="pclass" style="margin-top: -4px;"><spring:message code="lblLifeStl"/></p>
			<p class="pclass"><spring:message code="lblPerspect"/></p>
		</div>
	</div>
	</div>
	</div>
	
	<div style="display:none;" id="loadingDiv">
	    <table  align="center">
	 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
		</div>
	</div>
</c:if>
</c:if>
<!--</c:if> -->

<!--shadab   -->
<c:if test="${isImCandidate eq true}">
	<div class="row"
		style="margin-left: 0px; margin-right: 0px; margin-bottom:10px; ">
		<div style="float: left;">
			<img src="${logoPathSSO}" width="41" height="41" alt="">
		</div>
		<div
			style="font-size: 15px; font-family: 'Century Gothic', 'Open Sans', sans-serif;  padding: 0px; margin-left: 50px;">
			 <spring:message code="msgWelcomToInternalAppProcess1"/> ${districtOrSchoolName}  <spring:message code="msgWelcomToInternalAppProcess2"/>
			${selectionyear}  <spring:message code="msgWelcomToInternalAppProcess3"/>
		</div>
	</div>
	<div class="row" style="margin-bottom: 5px;">
	<div class="col-sm-6 col-sm-6">
		<a href="#"> <spring:message code="lblFAQ"/></a>
		</div>
	</div>	
</c:if>

<div style="clear:both;"></div>
<div class="row" style="clear:both;top:30px;">
	<div class="box col-sm-6 col-sm-6" id="divMilestones">
		<table width='100%' border='0' class='table table-bordered table-striped'>
			<thead class='bg' style="">
			<tr>
	         <th><img class='fl' src='images/personal_planning_bucket.png' style='height:35px;width:35px;'> <h4><spring:message code="lblPersonalPlng"/></h4></th>
				<th> <spring:message code="lblStatus"/></th>
				<th> <spring:message code="lblAct"/></th>
			</tr>
			</thead>
			<tr>
				<td colspan="3">
					<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br><spring:message code="lblLoding"/></div>
				</td>
			</tr>
		</table>
	</div>
	
		<div class="box col-sm-6 col-sm-6" id="divFavTeacher">
		<table width='100%' border='0' class='table table-bordered table-striped'>
			 <thead class='bg'>
			 <tr>
				 <th width='50%'><img class='fl' src='images/jobs_of_interest_bucket.png' style='height:35px;width:35px;'> <h4> <spring:message code="lblJoOfInterest"/></h4></th>
				 <th width='25%'><spring:message code="lblStatus"/></th>
				 
				 <th width='25%'> <spring:message code="lblDistrictName"/></th>
				 
				 <th width='25%'><spring:message code="lblAct"/></th>
			 </tr>
			 </thead>
			 <tr>
				<td colspan="3">
					<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br><spring:message code="lblLoding"/></div>
				</td>
			</tr>
		</table>
	</div>
	
	
</div>

<div class="row">
	<div class="box col-sm-6 col-sm-6" id="divReport">
		<table width='100%' border='0' class='table table-bordered table-striped'>
			 <thead class='bg'>
			 <tr>
				 <!--<th><img class='fl' src='images/reports-icon.png'> <h4>Reports</h4></th>-->
				 <th><img class='fl' src='images/communications_bucket.png' style='height:35px;width:35px;'> <h4><spring:message code="headCom"/></h4></th>
				<th> <spring:message code="lblStatus"/></th>
				<th> <spring:message code="lblAct"/></th>
			 </tr>
			 </thead>
			 <tr>
				<td colspan="3">
					<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br><spring:message code="lblLoding"/></div>
				</td>
			</tr>
		 </table>
	</div>
	<div class="box col-sm-6 col-sm-6" id="divJbInterest">
		<table width='100%' border='0' class='table table-bordered table-striped'>
			 <thead class='bg'>
			 <tr>
				 <th width='50%' ><img class='fl' src='images/job_applications_bucket.png' style='height:35px;width:35px;'> <h4> <spring:message code="lblJoApp"/></h4></th>
				 <th width='25%'><spring:message code="lblAct"/></th>
				 <th width='25%'><spring:message code="lblStatus"/></th>
			 </tr>
			 </thead>
			 <tr>
				<td colspan="3">
					<div style='text-align:center;padding-top:150px;'><img src='images/loadingAnimation.gif' /> <br><spring:message code="lblLoding"/></div>
				</td>
			</tr>
		</table>
	</div>
	
</div>

<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'>
 		<td style='padding-top:0px;' id='spnMpro' align='center'><spring:message code="lblYrRpotIsBeingQenerated"/></td>
 		</tr>
	</table>
</div>


 <div style="display:none;" id="loadingDivInventory">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'><spring:message code="lblInventBengLoaded"/></td></tr>
	</table>
 </div>
 	
<script type="text/javascript" src="js/teacher/dashboard.js?ver=${resouceMap['js/teacher/dashboard.js']}">
</script>

 <div  class="modal hide"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='blockMessage'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
    </div>
  </div>
</div>
<input type="hidden" id="jobId" name="jobId" value="${param.jobId}"/>
<input type="hidden" id="ok_cancelflag" name="ok_cancelflag" value="${param.ok_cancelflag}">
<input type="hidden" id="isaffilatedstatushiddenId" name="isaffilatedstatushiddenId" value="${param.isaffilatedstatus}"/>
<input type="hidden" id="savejobFlag" name="savejobFlag" value="${param.savejobFlag}"/>	
<input type="hidden" id="completeNow" name="completeNow" value="0"/>
<input type="hidden" id="inventory" name="inventory" value=""/>
<input type="hidden" id="portfolioStatus" name="portfolioStatus" value=""/>
<input type="hidden" id="prefStatus" name="prefStatus" value=""/>
<input type="hidden" id="ok_cancelflag" name="ok_cancelflag" value="${param.stp}">
<input type="hidden" id="noCl" name="noCl" value="0"/>
<input type="hidden" id="dashbd" name="dashbd" value="1"/>	
<input type="hidden" id="last4SSN" name="last4SSN" value="${last4SSN}"/>
<input type="hidden" id="empNumCoverLtr" name="empNumCoverLtr" value=""/>
<input type="hidden" id="coverShowOrNot" name="coverShowOrNot" value="1"/>
<input type="hidden" id="dob" name="dob" value="${dob}"/>

<input type="hidden" name="ff_name" id="ff_name"/>
<input type="hidden" name="ll_name" id="ll_name"/>
<input type="hidden" name="locationn" id="locationn" value=""/>
<input type="hidden" name="pposition" id="pposition" value=""/>
<input type="hidden" name="datee" id="datee" value=""/>
<input type="hidden" id="dobForAdams" name="dobForAdams" value="${dobForAdams}" class="dobValue"/>
<input type="hidden" id="firstNameForAdams" name="firstNameForAdams" value="${firstName}"/>
<input type="hidden" id="lastNameForAdams" name="lastNameForAdams" value="${lastName}"/>
<%-----  Gagan :-> Start Changes for District Specific Questions before applyingh Job  --%>
<c:if test="${param.savejobFlag eq 1}">
</c:if>
 
	<c:if test="${param.epiFlag eq 'on' || param.isaffilatedstatus eq 1}">
		<div class="modal1 hide"  id="epiIncompAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >		
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
				<div class="control-group">
					<c:if test="${param.epiFlag eq 'on'}">
						<div class="" id="ddd">
							<p><spring:message code="msgBaseInventory"/></p>
							<c:if test="${param.portfolioStatus eq 'required'}">
								<p><img src='images/red_cancel.png'/> <spring:message code="lblPortfCmplt"/></p>
							</c:if>
							<c:if test="${param.portfolioStatus eq ''}">
								<p><img src='images/green_ok.jpg'/><spring:message code="lblPortfCmplt"/></p>
							</c:if>
							<c:if test="${param.epimsjstatus eq 'icomp'}">
							<p><img src='images/red_cancel.png'/>  <spring:message code="lblEPICmplt"/></p>
							</c:if>
						</div>
					</c:if>
					<c:if test="${param.isaffilatedstatus eq 1}">
						<c:if test="${param.ok_cancelflag eq 1}">
							<div class="" id="divAlertText1" ><spring:message code="msgNoticedYuHvAppliedForOthrJoInDist"/><br/><br/><spring:message code="msgSltYesIfYuLiketoUdateStatus"/><br/><br/> <spring:message code="msgSltNoIfYuLiketoUdateStatus"/><br/><br/><spring:message code="msgSltCancelIfYuBack"/></div>
						</c:if>
						<c:if test="${param.ok_cancelflag eq 0}">
							<div class="" id="divAlertText1" > <spring:message code="msgNoticedYuHvAppliedForOthrJoInDistdeclared"/><br/><br/><spring:message code="msgSltYesIfYuLiketoUdateStatus"/><br/><br/> <spring:message code="msgSltNoIfYuLiketoUdateStatus"/><br/><br/> <spring:message code="msgSltCancelIfYuBack"/></div>
						</c:if>
					</c:if>
				</div>
		 	</div> 	
		 	<div class="modal-footer">
		 		<c:if test="${param.isaffilatedstatus ne 1}">
		 			<span><button class="btn btn-large btn-primary" onclick="window.location.href='userdashboard.do'"> <spring:message code="optY"/> <i class="icon"></i></button></span>
		 			<span><button class="btn" data-dismiss="modal" aria-hidden="true" > <spring:message code="lnkCancel"/></button></span>
		 		</c:if>
		 		<c:if test="${param.isaffilatedstatus eq 1}">
			 		<button class="btn btn-large btn-primary" aria-hidden="true" onclick="resetjftIsAffilated()" ><strong> <spring:message code="optY"/> <i class="icon"></i></strong></button>&nbsp;
			 		<span><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" onclick="canceljftIsAffilated()"><strong> <spring:message code="optN"/> <i class="icon"></i></strong></button></span>
			 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" > <spring:message code="lnkCancel"/></button></span>
		 		</c:if> 
		 	</div>
		</div>
	</c:if>
	 
	<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3><input type="hidden" id="isresetStatus" name="isresetStatus" value="0"><input type="hidden" id="isAffilatedValue" name="isAffilatedValue" value="0">
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 	
 		<button class="btn btn-large btn-primary" aria-hidden="true" onclick="resetAffilatedAndContinue(${param.savejobFlag})" ><strong><spring:message code="optY"/> <i class="icon"></i></strong></button>&nbsp;
 		<span><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" onclick="doNotResetAffilatedAndContinue(${param.savejobFlag})"><strong><spring:message code="optN"/> <i class="icon"></i></strong></button></span> 
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" > <spring:message code="lnkCancel"/></button></span>		
 	</div>
</div>
</div>
</div>
<!--
	 start ... Attach dynamic portfolio jsp files 
	-->
	<jsp:include page="dp_common.jsp"></jsp:include>
	
	<!-- start ... New Job Apply Process jsp files -->
	<jsp:include page="jafstartcommon.jsp"></jsp:include>
	<!-- end ... New Job Apply Process jsp files -->
	
	  <c:if test="${JAFExternal eq 1}">
	  	<script>
	  	//alert("userdashboard.jsp");
			callNewSelfServiceApplicationFlow(${jobOrder.jobId},'E');
		</script>
	  </c:if>
	
	<c:if test="${JAFExternal eq 0}">
	<c:if test="${kesDataFlag ne true}">
	<c:if test="${(jobOrder.isInviteOnly eq false || empty jobOrder.isInviteOnly) ||  (jobOrder.isInviteOnly eq true && jobOrder.hiddenJob eq true)}">
		<c:choose>
        <c:when test="${param.pShowFlg eq 1}">
             <script>
				getTeacherCriteria(${jobOrder.jobId});
			</script>
        </c:when>
        <c:when test="${dspqFlag==false|| getQQ==false}">
             <script>
				getDistrictQuestionsSet(${jobOrder.jobId});
			</script>
        </c:when>
        <c:otherwise>
        <c:if test="${param.savejobFlag eq 1}">
			<script>
				//callDynamicPortfolio_externalJobApply(${param.savejobFlag});
				checkAffiBeforeaAplyJob(${param.savejobFlag});
			</script>  
		</c:if>          
        </c:otherwise>
    </c:choose>
    </c:if> 
   </c:if> 
   </c:if> 
	
	<c:if test="${(openDspq eq false && jobOrder.isInviteOnly eq true) || (jobOrder.isInviteOnly eq true &&  jobOrder.hiddenJob eq true)}">
	  <script>
	    window.location.href="applyjob.do?jobId="+${jobOrder.jobId}
	  </script> 
	</c:if>
	<c:if test="${openDspq eq true && jobOrder.isInviteOnly eq true &&  jobOrder.hiddenJob eq false}">
	    <script>
	         $('#divAlert').modal('hide');
	         $('#checkInvitationDiv').modal('show');
		     document.getElementById("InvitationErrMsgDiv").innerHTML="This job is a closed job and can only be accessed by those who are specifically invited by the district. Please contact the district Human Resources Department for more information.";
			 $('#myModalDymanicPortfolio').modal('hide');
	    </script>
	    
	</c:if>
	 <input type="hidden" id='kesData' name="kesData" value="${kesData}" >
	
	<c:if test="${kesDataFlag eq true }">
	    <script>
	   			var kesData = $("#kesData").val();
				$('#message2show').html("Dear valuable Kelly Educational Staffing&reg; (KES&reg;) talent, Our records indicate that you may have an existing file. Please contact the KES branch listed below.</br>"+kesData);
				$('#myModal2').modal('show');
	    </script>
	    
	</c:if>
	<!--
	 end ... Attach dynamic portfolio jsp files 

--><div  class="modal hide"  id="myModalCL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog-for-cgreference">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"> <spring:message code="headCoverLetr"/></h3>
	</div>
	<div class="modal-body" >
	<p id="cvrltrTxt" class="hide"> <spring:message code="msgIfYuApplyCentealOfficePosition"/></p>
		<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>
		<label>
	       <spring:message code="msgPlzTyYurCoverLtr"/>
	        <input type="hidden" id='hiddenJobForTeacherId' >
   		</label>
   		<div id="divCoverLetter">
   			<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>
   			<textarea id="coverLetter"  name="coverLetter" rows="7" cols="150"></textarea>
   		</div>							
		
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-large btn-primary" type="submit"  onclick="saveCoverLetter();"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
  </div>
</div>
</div>


<input type="hidden" id="teacherID" value="${teacherDetail.teacherId}"></div>
<input type="hidden" name="tempjobid" id="tempjobid"/>
<input type="hidden" name="tempjobid" id="tempjobid"/>
<input type="hidden" name="exitUrl" id="exitUrl"/>
<input type="hidden" name="district" id="district"/>
<input type="hidden" name="school" id="school"/>
<input type="hidden" name="criteria" id="criteria"/>
<input type="hidden" name="epistatus" id="epistatus"/>
<input type="hidden" name="editDspqJobId" id="editDspqJobId"/>

<div  class="modal hide"  id="applyJobComplete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span><b> <spring:message code="msgSucceApldJob"/></b></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideUserInfoBlog();"><spring:message code="btnOk"/></button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="epiInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
			<c:choose>
			<c:when test="${isKellyUser}"> 
				<span>Kelly Educational Staffing&reg; (KES&reg;) and TeacherMatch&reg; share a common philosophy and believe high-quality substitute teachers are the most critical factor in fostering student achievement. While developed specifically for full-time teaching candidates, taking the EPI with KES offers you a great opportunity to gain valuable insights into the essential teaching skill sets that are critical to success in the classroom.  The EPI is NOT used as a hiring assessment tool by KES, and your performance will NEVER be used as a qualifier or filter for you to be able to view or accept substitute teaching positions with KES school districts.  The EPI is for KES substitute teacher talent only.</span>
			</c:when>
			<c:otherwise>
		    	<span><spring:message code="msgEPIIsUsedBySomeDistToScrAply"/></span>
		    </c:otherwise>	  
		    </c:choose>	      	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="startEpi();">Continue <i class="icon"></i></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideEpiInfo();">Cancel</button> 		
 	</div>
</div>
</div>
</div>

<!-- shadab -->
<div  class="modal hide"  id="spInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span>Start SmartPractices Professional Assessment.</span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="startSP();">Continue <i class="icon"></i></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideSPInfo();">Cancel</button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="lessonInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span>The SmartPractices Professional Development Status (SP) .</span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="startEpi();"> <spring:message code="btnConti"/> <i class="icon"></i></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideEpiInfo();"> <spring:message code="lnkCancel"/></button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide pdfDivBorder"  id="spCertificate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog" style="width:900px;">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<!--<div id="spCert"><div>-->
			<div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
			<iframe id="spCert" width="100%" height="450px" src=""></iframe>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideEpiInfo();"> <spring:message code="lnkCancel"/></button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="eRegSSOInfoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="eRegSSOInfo();">x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="row">
				<div class="col-sm-12 col-sm-12">
			    	<span id="infoDiv"></span>
			    </div>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="eRegSSOInfo();"> <spring:message code="btnOk"/></button> 		
 	</div>
</div>
</div>
</div>

<input type="hidden" value="${staffTypeSession}" id="staffTypeSession">
<script type="text/javascript">
	<c:if test="${isImCandidate eq false}">
	 	getDashboardMilestones();
	 	getDashboardReport();
	 	getDashboardJobsofIntrestAvailable();
    </c:if>
    <c:if test="${isImCandidate eq true}">
	 	getDashboardMilestonesNew();
		getDashboardReportNew();
		getDashboardJobsofIntrestAvailableNew();
    </c:if>	
    	
   getDashboardJobsofIntrest();
   $('#myModal').modal('hide');

for(i=1;i<=4;i++)
{
	$('#iconpophover'+i).tooltip();
	$('#tpWithdraw'+i).tooltip();
	$('#tpShare'+i).tooltip();
	$('#tpApply'+i).tooltip();
	$('#tpHide'+i).tooltip();
}

for(i=1;i<=8;i++)
{
$('#tpShare'+i).tooltip();
$('#iconpophover5').tooltip();
$('#iconpophover6').tooltip();
$('#iconpophoverRe').tooltip();
$('#iconpophoverBase').tooltip({
        toggle: 'toolip',
        placement: 'right'
    });

}

</script>
<script type="text/javascript">
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip({
        toggle: 'toolip',
        placement : 'top'
    });
    
$('[data-toggle="tooltip"]').tooltip();
 	
 	$("#search222Tooltip").mouseover(function(){
 		$('.tooltip').css('left','-5%')
 		$('.tooltip').css('top','-35px')
 		$('.tooltip').css('position','absolute')
 	});
    
});

</script>
<script type="text/javascript" src="js/d3.js?ver=${resourceMap['js/d3.js']}"></script>
 <script type='text/javascript'>
 $('#iconpophoverscore').tooltip();
 //<c:if test="${teacherDetail.userType eq 'N'}">
 <c:if test="${powerProfile ne 'hide'}">
 <c:if test="${isImCandidate eq false}">
  getPowerTracker_Op(${teacherDetail.teacherId});
 </c:if>
 </c:if>
 //</c:if>
 //powerProfile();
 </script>
