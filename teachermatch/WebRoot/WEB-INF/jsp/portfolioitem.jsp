<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resourceMap['DistrictPortfolioConfigAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManagePortfolioItemAjax.js?ver=${resourceMap['ManagePortfolioItemAjax.Ajax']}"></script>
<script type='text/javascript' src='js/portfolioitem.js?ver=${resourceMap["js/portfolioitem.js"]}'></script>
 
<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#itemTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 972,
        minWidth: null,
        minWidthAuto: false,
        colratio:[475,410,200], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
           
        });			
}
 </script>
<div class="container">
<div class="row  mt10">
	<div class="span16 centerline ">
		<div class="span1 m0"><img src="images/manageusers.png" width="41" height="41"></div>
		<div class="span7 subheading"><spring:message code="lblManagePortfolioItem"/></div>
		<div class=" pull-right add-employment1">
			<a href="javascript:void(0);" onclick="return addNewItem()">+<spring:message code="lblAddItem1"/></a>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="row mt10" style="margin-top:-20px;" onkeypress="">
		<div class="span16">
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="row mt10 <c:out value="${hide}"/>">
				    <div  id="Searchbox" class="span6 <c:out value="${hide}"/>">
	         		    <div class="row">
		             		<div class="span6">
		             			<label id="captionDistrictOrSchool"><spring:message code="optDistrict"/></label>
		             			<c:if test="${entityID eq 1}">
			             		<input type="text" id="districtNameFilter" name="districtNameFilter" class="span6 "
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"	/>
								<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>
								 </c:if>	
			             	
				             	<c:if test="${entityID eq 2}">
				             		<label>${userMaster.districtId.districtName}</label>
								</c:if>
				             	<input type="hidden" id="districtIdFilter" value="${userMaster.districtId.districtId}"/>
			             	</div>
		             	</div>
	          		</div>
	          		<c:if test="${entityID eq 1}">
						<div class="span5">
							<label>&nbsp;</label>
							<button class="btn btn-primary fl" type="button" onclick="getPortfolioItemList();"><spring:message code="btnSearch"/> <i class="icon"></i></button>
						</div>
					</c:if>
        	  	</div>
       		 </form>
		</div>            
</div>
<div class="row"><br/>
	<div class="span16" style="margin-top:-10px;"  id="itemGrid"></div>
</div>
  
<div id="itemDiv" class="hide" onkeypress="">
	  <div class="row mt10">
	  		<div class="control-group span16">			                         
 				<div class='divErrorMsg' id='errordiv' ></div>
	  		</div>
           <div  class="span6">
       		<label><spring:message code="optDistrict"/><span class="required">*</span></label>
       		<c:if test="${entityID eq 1}">	
         		<input type="text" id="districtName" name="districtName"  class="span6"
       			onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
				onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
				<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;'
		 		onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
			</c:if>	
			 <c:if test="${entityID eq 2}">
	   			<label>${userMaster.districtId.districtName}</label>
	  		 </c:if>
	  		 <input type="hidden" id="districtId" value="${districtId}"/>	
          	</div>
      </div>
	  <div class="row ">
		<div class="span3">
			<label><strong><spring:message code="lblItem1"/><span class="required">*</span></strong></label>
			<input type="hidden" id="districtPortfolioConfigId"/>
			<select id="itemId" name="itemId" class="span5" onchange="chkRequired(this.value);">
				<option value="0"><spring:message code="lblSelectPortfolioItem1"/></option>
				<c:if test="${not empty portfolioitemlist}" >
					<c:forEach var="portfolioitem" items="${portfolioitemlist}">
						<option value="${portfolioitem.portfolioItemId}"/>${portfolioitem.portfolioItemName}</option>
					</c:forEach>
				</c:if>
			</select>
		</div>
	  </div>
	  
	 <div class="row mt5" id="itemquantityDiv">
		<div class="span5"><label><strong><spring:message code="lblQuantity1"/><span class="required">*</span></strong></label>
			<input type="text" id="itemquantity" maxlength="2" style="width:30px;" onkeypress="return checkForInt(event);"/>
		</div>
	 </div>
	 
	  <div class="row ">
	  	<div class="span10 mt10">
	     	<button onclick="savePortfolioItem();" class="btn btn-primary"><strong><spring:message code="btnAdd"/> <i class="icon"></i></strong></button>
	     	 &nbsp;<a href="javascript:void(0);" onclick="hideItemDiv();"><spring:message code="btnClr"/></a>  
	    </div>
	  </div>
</div>
</div>
<script>
getPortfolioItemList();
applyScrollOnTbl();
</script>