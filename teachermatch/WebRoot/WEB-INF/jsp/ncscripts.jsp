<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

	<style>
	
	#question
	{
		
	 font-weight: bold;
	  font-size: medium;
	
	color: black;
	}
	
	p
	{

		text-align: justify;
		padding-right: 5px;
		/*text-indent: 20px;*/
		font-size: medium;
		
	}
	.spnIndent
	{
		padding-left: 20px;
	}
	.spnIndent40
	{
		padding-left: 40px;
	}
	ul
	{
	margin-right: 10px;
	}
	li
	{
	
		text-align: left;
		padding-right: 5px;
		/*text-indent: 20px;*/
		font-size: medium;
	
	}
</style>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="js/internaltransfer.js"></script>

<script type="text/javascript" src="dwr/interface/InternalTransferAjax.js"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/ncscript.js"></script>
<script type="text/javascript" src="dwr/interface/ScriptAjax.js"></script>

	<div class="container">
			 <div class="row centerline">
					<img src="images/affidavit.png" width="47" height="48" alt="">
					<b class="subheading" style="font-size: 13px;">
					NC Scripts
				    </b>
		     </div>

		     <div class="row top15">						
							
				<div class="col-sm-6 col-md-6">								
					<label><strong><spring:message code="lblDistrict"/></strong></label>
				<input type="hidden" id="gridNameFlag" value="jobofinterest">
			 	<input  type="text" 
					class="form-control"
					maxlength="100"
					id="districtName" 
					value=""
					name="districtName" 
					onfocus="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId',''); "
					onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData', 'districtName','districtId',''); " 
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtDistrictData');"/>
				<input  type="hidden" id="districtId" name="districtId" value="">
				<div id='divTxtDistrictData' style=' display:none;position:absolute; z-index: 2000;' 
				onmouseover="mouseOverChk('divTxtDistrictData','districtName')" class='result' ></div>
				</div>
			    <div class="col-sm-2 col-md-2">	
				<button class="btn btn-primary fl top25-sm" onclick="getDistrictSettings();" type="button"><spring:message code="btnSearch"/> <i class="icon"></i>
				</button>
				</div>
			</div>		        
		    <div class="TableContent top15">        	
         		<div class="table-responsive" id="divMain">          
            </div>            
</div> 

</div>
	