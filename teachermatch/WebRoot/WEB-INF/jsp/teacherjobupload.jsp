
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/TeacherUploadTempAjax.js?ver=${resourceMap['TeacherUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JobUploadTempAjax.js?ver=${resourceMap['JobUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/teacherupload.js?ver=${resourceMap['js/teacherupload.js']}"></script>

<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>

	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headImpCandDet"/></div>	
         </div>         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>

<!--                      Adding By Deepak           -->

<input type="hidden" value="${entityType}" id="entityValue"/>

<c:if test="${entityType == 5 || entityType == 6}">
	<input type="hidden" id="headQuarterId" value="${headQuarterId}"/>
	</c:if>
	<c:if test="${entityType == 6}">
	<input type="hidden" id="branchId" value="${branchId}"/>
	</c:if>
	
<!--                End              -->


<div class="row top20">
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-5 col-md-5 importborder" style="margin-left: 15px;margin-right: 15px;">
	
	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	
		<form id='teacherUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='teacherUploadServlet.do' class="form-inline">
		<input type="hidden" name="invitejobId" id="invitejobId" value="${jobId}"/>
		<table cellpadding="5" align="center" >
		<tr><td colspan="2" style="padding-top:10px;">&nbsp;
			 <div class="control-group">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>
			</td>
		</tr>
<!--  Adding By Deepak 		-->
		<!--<tr>
		
		<td> 	
		<c:if test="${(entityType == 5) || (entityType == 6) }">	
		
		<div class="col-sm-12 col-md-12">
						<label><strong>Entity Type</strong></label>
						<select onchange="checkEntity();" id="etype" class="form-control ">						
                         <option value="5">HeadQuarter</option>
						<option value="6">Branch</option>	
						</select>
						
					</div>
		</c:if>
		</td>
		<td>
		<div class="row hide" id="bshow">
            <div class="col-sm-12 col-md-12">
               <label>Branch Name<span class="required">*</span></label>  <br/>
               <c:set var="bnchId" value=""></c:set>
               <c:set var="editFlag" value=""></c:set>
               <c:if test="${not empty jobOrder.branchMaster.branchId}">
               <c:set var="bnchId" value="${jobOrder.branchMaster.branchId}"></c:set>
               </c:if>
               <c:if test="${not empty branchMasterId}">
               <c:set var="bnchId" value="${branchMasterId}"></c:set>
                <c:set var="editFlag" value="readonly"></c:set>
               </c:if>      
	             <span>
			      <input type="text" id="branchName" name="branchName"  class="form-control" value="${branchName}"
       					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
						onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');showBranchesJobCategory();"	${editFlag}/>
						<input  type="hidden" id="branchId" name="branchId" value="${bnchId}">
						<div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>								      
			      </span>
         </div>
 		</div>
		
		
		</td>
		</tr>
		
		
			
		  -->
		
		  <c:if test="${entityType ne 1}">
		  
		  <c:set var="showOrHide" scope="session" value="none"/>
		  
		  </c:if>
		  <c:if test="${entityType eq null || entityType eq '' || entityType eq 1}">
		     <c:set var="showOrHide" scope="session" value="fixed"/>
		   </c:if>
		
		  <tr  style='display:${showOrHide};'>
			<td colspan="2"><label><spring:message code="lblDistrictName"/><span class="required">*</span></label></td>
			</tr>
			 <tr  style='display:${showOrHide};'><td colspan="2">
				 <input type="text" id="districtName" name="districtName" class="help-inline form-control"
				    		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
				             onkeyup="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
				             onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"/>
				 <input type="hidden" id="districtId" value="${districtId}" />
				 	
				 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
			 </td>
		  </tr>	
		
		<tr><td ><label><spring:message code="lnkCandidateDetails"/> <span class="required">*</span></label></td><td><input name="teacherfile" id="teacherfile" type="file"></td></tr>	
		<tr><td><button class="btn btn-primary fl" type="button" onclick="return validateTeacherFile();" style="width: 100px;"><spring:message code="btnImpt"/> <i class="icon"></i></button></td></tr>
		</table>
		</form>
	</div>
</div>
<div class="row">	
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-8 col-md-8 mt10" style="margin-left: 5px;margin-right: 5px;">
			<table>
			<tr><td></td><td class="required">
<spring:message code="headN"/></td></tr>
			<tr><td valign="top">*</td><td> <spring:message code="msgExelFi&SubsequntRData"/></td></tr>
			<tr><td valign="top">*</td><td><spring:message code="msgExcelFColN"/></td></tr>
			<tr><td></td><td>${job_Id_discription}</td></tr>
			<tr><td></td><td><spring:message code="lblUFName"/></td></tr>
			
			<tr><td></td><td><spring:message code="lblULName"/></td></tr>
			
			<tr><td></td><td><spring:message code="lblUEmail"/></td></tr>
			
			<tr><td valign="top">*</td><td> <spring:message code="msgDate_Applied"/></td></tr>
			
			<tr><td valign="top">*</td><td><spring:message code="msgColNaOdr"/></td></tr>
			<tr><td valign="top">*</td><td><spring:message code="msgColNaCaseInSent"/></td></tr>
		</table>
	</div>
</div>
<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div> 
