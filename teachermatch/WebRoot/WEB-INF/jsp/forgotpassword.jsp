<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%--<form:form commandName="teacherDetail" action="signin.do" onsubmit="return loginUserValidate();"> --%>
<form action="forgotpassword.do" method="post" onsubmit="return chkForgotpwd();">

<c:set var="test" value="${fn:escapeXml(param.key)}"/>
<c:choose>
      <c:when test="${fn:contains(test,'script')}">
       <input type="hidden" id="val" name="val" value="1">
      <input type="hidden" id="key" name="key" value="">
      </c:when>
      <c:otherwise>
      <input type="hidden" id="key" name="key" value="${fn:escapeXml(param.key)}">
      </c:otherwise>
</c:choose>
<c:set var="test" value="${fn:escapeXml(param.id)}"/>
<c:choose>
      <c:when test="${fn:contains(test,'script')}">
      <input type="hidden" id="val" name="val" value="1">
      <input type="hidden" id="id" name="id" value="">
      </c:when>
      <c:otherwise>
         <input type="hidden" id="id" name="id" value="${fn:escapeXml(param.id)}">
      </c:otherwise>
</c:choose>

                <div class="row col-md-offset-3 top15">                                
                                 <div class="col-md-8">
                                       <div class="subheadinglogin"><spring:message code="msgForgotPass"/></div>
                                 </div> 
                </div>					
				<div class="row col-md-offset-3">  	                        
                                 <div class="col-md-8">
	                                 <div class="control-group">			                         
						     			<div id='divServerError'  class='divErrorMsg' style="display: block;">${msgError}</div>
										<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
								    </div>
							      </div>
							      </div>					
				<div class="row col-md-offset-3"> 						      
					            <div class="left15">
	                                  <p><spring:message code="msgResetPass"/></p>
	                            </div>
                 </div>   
                 <div class="row col-md-offset-3">          
                                <div class="col-sm-6">
                                       <label><strong><spring:message code="lblEmail"/></strong></label>		        		
						        		<%--<form:input path="emailAddress" placeholder="Enter your user name" cssClass="span5" />--%>
						        		<input type="text" class="form-control"  name="emailAddress" value="${emailAddress}" id="emailAddress" placeholder="<spring:message code="msgEtrYuEmail"/>"/>
						        </div>	
				</div>
					
				<div class="row col-md-offset-3 top15">  				                                 
                                <div class="col-sm-6">                                   
											<spring:message code="lblwhatis"/> <img id="imgCaptcha" class="imagewidth" src="verify.png" />
											<img src="images/refresh.png" style="cursor: pointer;" onclick="changeCaptcha('sumOfCaptchaText')"/>
								
                                  </div>
                </div>	
				<div class="row col-md-offset-3 top15">   
                                
                                   <div class="col-sm-12">
                                       <label><strong><spring:message code="msgSumTwoNumber"/></strong></label>
							       </div>  
				 </div>	
				<div class="row col-md-offset-3">  			           
								  <div class="col-sm-6">
									         <input id="sumOfCaptchaText" type="text" class="form-control" placeholder="<spring:message code="msgSumNum"/>"
											onkeypress="return checkForInt(event);" maxlength="2"/>
						           </div>
                </div>	
				<div class="row col-md-offset-3">           
	                              <div class="col-sm-6 top20">
	                                  <div class="controls" style=" text-align:left">			                
							            <button class="btn fl btn-primary" type="submit" ><spring:message code="btnSub"/><i class="icon"></i></button>			                
							           </div>	
	                              </div>  
                              
                 </div>  
                          
</form>
<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>

<script>
	document.getElementById("emailAddress").focus();
</script>
<script>
var value=document.getElementById("val").value;
if(value==1)
alert("URL is not valid")

</script>

