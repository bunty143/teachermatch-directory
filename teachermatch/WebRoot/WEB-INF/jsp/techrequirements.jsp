
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet" href="css/dialogbox.css" />

<script src="js/flash_detect.js"></script>
<script src="assets/webcam/webcam.js"></script>
<script src="assets/js/script.js"></script>
<script src="js/detect.js"></script>

	<div class="span8 mt30">
		<div class="subheadinglogin"><spring:message code="headTechReq"/></div>
		<div><spring:message code="msgToSuccessfullyProctored"/></div>
		
		
		<div class="classname">
		    <div class="control-group">			              
				<div class="controls">
					<table cellpadding="10" width="800">
						<tr>
							<td width="24%">&nbsp;</td>
							<td width="24%" class="headtable" align="center"><spring:message code="lblMinimum"/> </td>
							<td  width="24%"  class="headtable" align="center"><spring:message code="lblRecommended"/> </td>
							<td  width="28%"  class="headtable" align="center"><spring:message code="lblYoursSystem"/></td>
						</tr>
					</table>	
					<table cellpadding="10" width="800" border="2">
						<tr>
							<td width="24%" nowrap><spring:message code="lblBrowserVersion"/></td>
							<td width="24%" nowrap>
								<spring:message code="lblInternetExplorer7"/><br/>
								<spring:message code="lblChrome26.0.1410.64"/><br/>
						<spring:message code="lblFirefox20.0"/>
							 </td>
							<td width="24%" nowrap="nowrap">
								<spring:message code="lblInternetExplorer8"/><br/>
								<spring:message code="lblChrome26.0.1410.64"/><br/>
							<spring:message code="lblFirefox20.0"/>
							</td>
							<td width="28%" id='browserVersion' nowrap> </td>
						</tr>
						<tr>
							<td width="24%"><spring:message code="lblPop-up"/></td>
							<td width="24%"><spring:message code="lblEnable"/></td>
							<td width="24%"><spring:message code="lblEnable"/></td>
							<td width="28%"  id="pop"></td>
						</tr>
						<tr>
							<td width="24%"><spring:message code="lblPDFPlugin"/></td>
							<td width="24%" ><spring:message code="lblAvailable"/></td>
							<td width="24%"><spring:message code="lblAvailable"/></td>
							<td width="28%" id="plugin"></td>
						</tr>
						<tr>
							<td width="24%"><spring:message code="lblCamera"/></td>
							<td width="24%" nowrap><spring:message code="lbl628x480resolution"/></td>
							<td width="24%" nowrap><spring:message code="lbl1280x720resolution"/></td>
							<td width="28%" id="camera" nowrap><spring:message code="lblDetected"/></td>
						</tr>
						<tr>
							<td width="24%"><spring:message code="lblCookies"/></td>
							<td width="24%" ><spring:message code="lblEnable"/></td>
							<td width="24%"><spring:message code="lblEnable"/></td>
							<td width="28%"  id="cook"></td>
						</tr>
						<tr>
							<td width="24%"> <spring:message code="lblFlash"/></td>
							<td width="24%"><spring:message code="lblInstalled"/></td>
							<td width="24%"><spring:message code="lblInstalled"/></td>
							<td width="28%"  id="flash"></td>
						</tr>
					</table>
		         </div>
			</div>
			<div id="browsepic" style="display: none;"></div>
			<div id="takeapicweb"></div>
			<div id="teacherId"></div>
		</div>		
	</div>
<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 	</table>
</div>
<script>
detectPopupBlocker();

</script>
