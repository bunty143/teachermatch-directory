<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/candidatejobstatus.js?ver=${resourceMap['js/candidatejobstatus.js']}"></script>
<script type="text/javascript" src="dwr/interface/CandidatejobstatusAjax.js?ver=${resourceMap['CandidatejobstatusAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>

<script type='text/javascript' src="js/certtypeautocomplete.js?ver=${resourceMap['js/certtypeautocomplete.js']}"></script>
<!--<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>
-->
<script type="text/javascript" src="dwr/interface/CGServiceAjax.js?ver=${resouceMap['CGServiceAjax.ajax']}"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css"
	href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />


<style>
div.t_fixed_header div.body {
	overflow-x	: auto;
}
</style>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});


function applyScrollOnTblEEC()
{
   //alert("11")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridEEC').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 300,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[190,180,140,87,87,90,80,100,100,180,90,90,65,60,60,60,80],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

function applyScrollOnPrintTable()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridEECPrint').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 300,
         width: 1038,
        minWidth: null,
        minWidthAuto: false,
        colratio:[190,180,140,87,87,90,80,100,180,90,90,90,65,60,60,60,80],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}

</script>

<style>
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
</style>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/districtjobs-orders.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headCandJoStRep"/></div>	
         </div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>

         <div class="row">
			 	<div class="col-sm-3 col-md-3">
			 	 <label>
				 <spring:message code="lblFname"/>
				  </label>
				  <input id="firstName" name="firstName" maxlength="50" class="help-inline form-control" type="text" />
			    </div>
			     <div class="col-sm-3 col-md-3">
			 	 <label>
				    <spring:message code="lblLname"/>
				  </label>
				  <input id="lastName" name="lastName" maxlength="50" class="help-inline form-control" type="text" />
			    </div>
		     <div class="col-sm-4 col-md-4">
		 	 <label>
			 <spring:message code="lblEmailAddress"/>
			  </label>
			  <input id="emailAddress" name="emailAddress" maxlength="50" class="help-inline form-control" type="text" />
		    </div>
		    
	      <div class="col-sm-2 col-md-2 top25-sm2" style="width: 150px;">
		        <label class=""></label>
		        <button class="btn btn-primary " type="button" onclick=" searchRecordsByEntityTypeEEC();">&nbsp;&nbsp;Search&nbsp;&nbsp; <i class="icon"></i></button>
		   </div>
		    
	    </div>
 


  <div id="advanceSearchDiv" class="hide">

    <div class="row">
		       <div class="col-sm-6 col-md-6">
				     <label id="captionDistrictOrSchool"><spring:message code="lblDistrictName"/></label>
				        <c:if test="${DistrictOrSchoolName==null}">
					       <span>
					        <input type="text" maxlength="255" id="districtORSchoolName" name="districtORSchoolName" class="help-inline form-control"
					            		 onfocus="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
													onkeyup="getDistrictORSchoolAuto(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');chkschoolBydistrict();"
													onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');getCertificationList();chkschoolBydistrict();getStataussList();"/>
							 </span>
							 <input type="hidden" id="districtOrSchooHiddenlId"/>							
				         </c:if>
				    		
							<c:if test="${DistrictOrSchoolName!=null}"> 
							<span>
					        <input type="text" maxlength="255"  class="help-inline form-control" value="${DistrictOrSchoolName}"  disabled="disabled"/>
							 </span>
				             		
				             	
				             	<input type="hidden" id="districtOrSchooHiddenlId" value="${DistrictOrSchoolId}"/>
				             	<input type="hidden" id="districtORSchoolName" name="districtORSchoolName"/>
				             	<input type="hidden" id="districtHiddenlIdForSchool"/>
				            </c:if>
				             <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId!=null}">
				              	<input type="hidden" id="districtHiddenlIdForSchool" value="${DistrictOrSchoolId}"/>
				              </c:if>
				               <c:if test="${DistrictOrSchoolName==null && DistrictOrSchoolId==null}">
				              	<input type="hidden" id="districtHiddenlIdForSchool"/>
				              </c:if>
				       		 <input type="hidden" id="JobOrderType" value="${JobOrderType}"/>
				        	 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
							             	
               </div>
            
	      
			          <c:choose>
			              <c:when  test="${DistrictOrSchoolName==null}">
			               <div class="col-sm-6 col-md-6">
			           </c:when>
		                 <c:otherwise>
		                   <div class="col-sm-6 col-md-6">
		                 </c:otherwise>
		              </c:choose> 
			          <label><spring:message code="lblSchoolName"/></label>
			            <c:choose>
						    <c:when test="${(userMaster.entityType eq 3 && JobOrderType eq 2 && writePrivilegFlag eq false) || (userMaster.entityType eq 3 && JobOrderType eq 3)}">
						      <br/> ${schoolName}
						       	<input type="hidden" id="schoolName" name="schoolName" value="${schoolName}"/>
	         					<input type="hidden" id="schoolId" name="schoolId" value="${schoolId}"/> 
						    </c:when>
						    <c:otherwise>
								<input type="text" id="schoolName" maxlength="100"
									name="schoolName" class="form-control" placeholder=""
									onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtOrSchoolId','');"
									onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');" 
								/>	
								<input type="hidden" id="schoolName" name="schoolName"/>
	         					<input type="hidden" id="schoolId" name="schoolId" value="0"/> 
						    </c:otherwise>
						</c:choose>
						<div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
			        </div>
	
         </div>
   
   
	
	      <div class="row">
			      	  <div class="col-sm-6 col-md-6"">
					       	<span class=""><label class="">Certification/Licensure State</label>
					       	<select class="form-control" id="stateId" name="stateId" onchange="activecityType();" >
							 <option value="0">All Certification/Licensure State</option>  
							 <c:if test="${not empty lstStateMaster}">
							 	<c:forEach var="stateObj" items="${lstStateMaster}">
									<option value="${stateObj.stateId}">${stateObj.stateName}</option>
								</c:forEach>	
				        	 </c:if>
							</select>	
					       	</span>
				       </div>
				       <div class="col-sm-6 col-md-6"">
					       <span><label class=""><spring:message code="lblCert/LiceState"/></label>
					       </span>
					       <div>
							<input type="hidden" id="certificateTypeMaster" value="0">
							<input type="text" 
								class="form-control"
								maxlength="200"
								id="certType" 
								value=""
								name="certType" 
								onfocus="getFilterCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
								onkeyup="getFilterCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
								onblur="hideFilterCertificateTypeDiv(this,'certificateTypeMaster', 'divTxtCertTypeData');"/>
							<div id='divTxtCertTypeData' style=' display:none;position:absolute;' class='result' onmouseover="mouseOverChk('divTxtCertTypeData','certType')"></div>
						</div>       
				      </div>
			 </div>
	
	
	
	
	  
	
			<div class="row">
			  <div class="col-sm-6 col-md-6">
				   <div class="row">
				    <div class="col-sm-6 col-md-6">
							<span class=""><label class="">
									<spring:message code="lblJoStFr"/>
								</label> <input type="text" id="sfromDate" name="sfromDate"
									class="help-inline form-control">
							</span>
						</div>
						<div class="col-sm-6 col-md-6">
							<span class=""><label class="">
									<spring:message code="lblJoStTo"/>
								</label> <input type="text" id="stoDate" name="stoDate"
									class="help-inline form-control">
							</span>
						</div>
				   </div>
			  </div>
			  
			  
			   <div class="col-sm-6 col-md-6">
		         		  <div class="row">
				     <div class="col-sm-6 col-md-6">
							<span class=""><label class="">
									<spring:message code="lblJoEnFrom"/>
								</label> <input type="text" id="endfromDate" name="endfromDate"
									class="help-inline form-control">
							</span>
						</div>
						<div class="col-sm-6 col-md-6">
							<span class=""><label class="">
									<spring:message code="lblJoEnTo"/>
								</label> <input type="text" id="endtoDate" name="endtoDate"
									class="help-inline form-control">
							</span>
						</div>
				   </div>			
			  </div>
		</div>	
			 
			 
	<div class="row">
			
			  <div class="col-sm-6 col-md-6">
			       <div class="row">
				    <div class="col-sm-6 col-md-6">
						<span class=""><label class="">
								<spring:message code="lblAppFr"/>
							</label> <input type="text" id="appsfromDate" name="appsfromDate"
								class="help-inline form-control">
						</span>
						</div>
						<div class="col-sm-6 col-md-6">
							<span class=""><label class="">
									<spring:message code="lblAppTo"/>
								</label> <input type="text" id="appstoDate" name="appstoDate"
									class="help-inline form-control">
							</span>
						</div>
				   </div>
			  </div>
			   <div class="col-sm-6 col-md-6">
		  	     <div class="row">
				    <div class="col-sm-6 col-md-6">
						<span class=""><label class="">
								<spring:message code="lblHiFr"/>
							</label> <input type="text" id="hiredfromDate" name="hiredfromDate"
								class="help-inline form-control">
						</span>
						</div>
						<div class="col-sm-6 col-md-6">
							<span class=""><label class="">
									<spring:message code="lblHiTo"/>
								</label> <input type="text" id="hiredtoDate" name="hiredtoDate"
									class="help-inline form-control">
							</span>
						</div>
				  </div>
		      </div>
     </div>
		
		<div class="row">
	      <div class="col-sm-6 col-md-6">
		 		<label><spring:message code="lblJobCat"/></label>
				<select multiple id=jobCategoryId name="jobCategoryId" class="form-control" style="height: 95px;"> 
				 <option value="0"><spring:message code="optAll"/> </option>
 				  <c:if test="${not empty jobCategoryMasterlst}">
                    <c:forEach var="jobCategoryMaster" items="${jobCategoryMasterlst}" varStatus="status">
		         	  <option value="${jobCategoryMaster.jobCategoryId}" ><c:out value="${jobCategoryMaster.jobCategoryName}"/></option>
		            </c:forEach>
                 </c:if>
				</select>
		  </div>	
		
         <div class="col-sm-6 col-md-6">
       	     <label class=""><spring:message code="lblApplicationStatus"/></label>
     			<select multiple class="form-control"  style="height: 95px;" id="statusId" name="statusId" >
				  <option value="0"><spring:message code="optAll"/></option>  
				   <c:if test="${not empty lstsecStr}">
	 				 <c:forEach var="strStatusObj" items="${lstsecStr}">
					  <option value="${strStatusObj}">${strStatusObj}</option>
					</c:forEach>	
	     		 </c:if>
			   </select>	
	     </div>	
     </div>
	  
		
		<div class="row">
			  <div class="col-sm-6 col-md-6">
			        <label class="">
							<spring:message code="lblNormScore"/>
							</label>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td valign="top">
										<select style='width: 70px;' id="normScoreSelectVal"
											name="contacted" class="form-control">
											<option value="0" selected="selected">
											<spring:message code="optAll"/>
											</option>
											<option value="6">
											<spring:message code="optN/A"/>
											</option>
											<option value="5">
												>=
											</option>
											<option value="1">
												=
											</option>
											<option value="2">
												<
											</option>
											<option value="3">
												<=
											</option>
											<option value="4">
												>
											</option>
										</select>
									</td>
									<td valign="top">
										<iframe id="ifrmNorm"
											src="slideract.do?name=normScoreFrm&tickInterval=10&max=100&swidth=360&svalue=0"
											scrolling="no" frameBorder="0"
											style="padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top; height: 40px; width: 410px; margin-top: -10px;"></iframe>
										<input type="hidden" name="normScore" id="normScore" />
									</td>
								</tr>
							</table>
			  </div>
		      
		      
		      <div class="col-sm-3 col-md-3">
				 <label class=""><spring:message code="lblIntrCand"/></label>
	       				<select class="form-control" id="internalcandidate" name="internalcandidate" >
						 <option value="2" selected="selected"><spring:message code="optAll"/></option>  
								<option value="1"><spring:message code="optY"/></option>
								<option value="0"><spring:message code="optN"/></option>
						</select>	
				</div>
				
				
	   		<div class="col-sm-3 col-md-3">
		 		<label><spring:message code="lblJoI/JCode"/></label>
		         <input id="jobOrderId" name="jobOrderId" maxlength="100" class="help-inline form-control" type="text" onkeypress="return checkForInt1(event);"/>
             </div>	
	   </div>
	 
	   <div class="row">
			  <div class="col-sm-6 col-md-6">
			  </div>
		      <div class="col-sm-3 col-md-3">
				 <label class=""><spring:message code="lblJobStatus"/></label>
						<select class="form-control" id="jobStatus">
					    <option value="All" selected="selected"><spring:message code="optAll"/></option>  
	                     <option value="A"><spring:message code="optAct"/></option>
	                   	<option value="I"><spring:message code="optInActiv"/></option>
        			</select>	
				</div>
				
	   		<div class="col-sm-3 col-md-3">
		 		<label><spring:message code="lblHiddenJob"/></label>
		         <select class="form-control" id="hiddenjob" name="hiddenjob" >
						 <option value="2" selected="selected"><spring:message code="optAll"/></option>  
								<option value="1"><spring:message code="optY"/></option>
								<option value="0"><spring:message code="optN"/></option>
						</select>	
             </div>	
	   </div>	 
	</div>			
	
	
	
	
	 <script type="text/javascript">

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("sfromDate", "sfromDate", "%m-%d-%Y");
      
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("stoDate", "stoDate", "%m-%d-%Y");
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("endfromDate", "endfromDate", "%m-%d-%Y");
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("endtoDate", "endtoDate", "%m-%d-%Y");
       
       var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("appsfromDate", "appsfromDate", "%m-%d-%Y");
       var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("appstoDate", "appstoDate", "%m-%d-%Y");
      
       var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("hiredfromDate", "hiredfromDate", "%m-%d-%Y");
      
       var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
      cal.manageFields("hiredtoDate", "hiredtoDate", "%m-%d-%Y");
      
  	
     </script>	
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  

 <div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
    
              <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
		  </div> 
    
    <input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}">
    <div style="clear: both;"></div>
     <br/>
   
          
       <div class="row"> 
	         <div class="col-sm-6 col-md-">
	           <div id="searchLinkDiv" class="mt5">
			    <a href="#" onclick="displayAdvanceSearch()"><spring:message code="lnkOpAdvSch"/></a>
	     	 </div> 
			
			<div id="hidesearchLinkDiv" class=" hide mt10">
			   <a href="#" onclick="hideAdvanceSearch()"><spring:message code="lnkClAdvSch"/></a>
			</div>
       </div>
        <div class="col-sm-6 col-md-6">
         <table class="marginrightForTablet">
		      <tr>
		         <td  style='padding-left:5px;'>
					<a data-original-title='Export to Excel' rel='tooltip' id='exlId' href='javascript:void(0);'  onclick="generateCandidateJobStatusxcel();"><span class='icon-file-excel icon-large iconcolor'></span></a>
				 </td>
				 <td>
				  <a data-original-title='Export to PDF' rel='tooltip'  href="javascript:void(0);" id='pdfId' onclick='downloadCandidateJobStatusReport()'><span class='pdf-eoc icon-large iconcolor'></span></a>
				 </td>
				
				 <td >
					<a data-original-title='Print Preview' rel='tooltip' id='printId' href='javascript:void(0);' onclick="generateCandidateJobStatusPrintPre();"><span class='icon-print icon-large iconcolor'></span></a>
				 </td>  
			<!-- 	  <td  style='padding-left:5px;'>
					<a data-original-title='Chart' rel='tooltip' id='chartId' href='javascript:void(0);' onclick="generateGenderChart1();generateEthnicityChart();generateRaceChart();generateEthnicOriginChart();"><span class='fa-bar-chart-o icon-large iconcolor'></span></a>
				 </td>
				  -->
			</tr>
		</table>
        </div>
       	
   </div>   
           <div style="clear: both;"></div>
           
           
           
     <!-- PDF -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadCandidateJobStatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headCandJoStRep"/> </h3>
	</div>
		<div class="modal-body">		
			<div class="control-group">
				    <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
	                  <iframe src="" id="ifrmCJS" width="100%" height="480px">
	               </iframe>        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose"/></button> 		
	 	</div>
     </div>
 	</div>
</div>

<!-- PDF End -->      
           
    <!-- printing Start  -->
    
  <div style="clear: both;"></div>

   <div class="modal hide"  id="printCandidateJobSatatusDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:1100px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();"><spring:message code="btnX"/></button>
	   	<h3 id=""><spring:message code="headPrRrv"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 500px; overflow-y: auto;"> 		
			<div class="" id="pritCandidateJobSatatusDataTableDiv"  >          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 style="margin-left:850px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printCandidateJobStatusDATA();'>&nbsp;&nbsp;&nbsp;<spring:message code="btnP"/>t&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnClr"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
     <div style="clear: both;"></div>

   <div class="modal hide"  id="errDateCheckDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="canelPrint();"><spring:message code="btnX"/></button>
	   	<h3 id=""><spring:message code="headArt"/></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="" id="errDateMsg">          
             </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrint();'><spring:message code="btnOk"/></button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>
    
    
    <!-- printing End -->       
       <div class="TableContent">  	
             <div class="table-responsive" id="divMainEEC" >    
           </div>       
                       
         </div>




<script type="text/javascript">
  $('#chartId').tooltip();
  $('#pdfId').tooltip();
  $('#exlId').tooltip();
  $('#printId').tooltip();
</script>

<!--  
<c:if test="${userMaster.entityType eq 1}">
		<script type="text/javascript">
			
			 var today = new Date();
			    var dd = today.getDate();
			    var mm = today.getMonth()+1; //January is 0!
			
			    var yyyy = today.getFullYear();
			    if(dd<10){
			        dd='0'+dd
			    } 
			    if(mm<10){
			        mm='0'+mm
			    } 
			    var today1 = mm+'-'+dd+'-'+yyyy;
			
				//alert("today1 :: "+today1);
			
				document.getElementById("stoDate").value=today1;
			
				today.setDate(today.getDate()-70)
			
				var today2 = new Date(today);
			    var dd = today2.getDate();
			    var mm = today2.getMonth()+1; //January is 0!
			
			    var yyyy = today2.getFullYear();
			    if(dd<10){
			        dd='0'+dd
			    } 
			    if(mm<10){
			        mm='0'+mm
			    } 
			    var today2 = mm+'-'+dd+'-'+yyyy;
			    document.getElementById("sfromDate").value=today2;
			//alert("today2 :: "+today2)
      		
		</script>
	 </c:if>
	-->
<script type="text/javascript">
	chkschoolBydistrict();
	searchRecordsByEntityTypeEEC();
</script>

	        	
	 
