<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface//ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<!--<script type="text/javascript" src="js/jquery-1.4.1.js"></script>


--><link rel="stylesheet" href="css/dialogbox.css" />

<div id="displayDiv"></div>
<div class="tabletbody">
<br><br><br>
<form  action="approvejobsignin.do" method="post"  id="signinForm" >
<input type="hidden" id="redirectURL" name="redirectURL" value=""/>
<input type="hidden" id="schoolName" name="schoolName" value=""/>
<input type="hidden" id="schoolID" name="schoolID" value=""/>

						<div class="row top10">
						 <div class="col-md-offset-3"> 						                     
                                   <div class="col-sm-6">
                                        <div id="errordiv" style="color: red"> </div> 
                                   </div>  
                          </div>
                          </div>
                          
	                     <div class="row top10">
						 <div class="col-md-offset-3"> 						                     
                                   <div class="col-sm-6">
                                        <label>Email</label>
                                   	    <input type="text" name="emailAddress" value="${emailAddress}" id="emailAddress" placeholder="Enter your Email" class="form-control"  /> 
                                   </div>  
                          </div>
                          </div>  
                          <div class="row top-sm0">      
                          <div class="col-md-offset-3">            
                                  <div class="col-sm-4" style="width:33%;">
                                        <label>Password</label>
                                        <input type="password" name="password" id="password" placeholder="Enter your password" class="form-control" maxlength="12" />
                                  </div>
                                  <div class="col-sm-3 top25">
                                  	<button class="btn fl btn-primary" style="padding: 6px 17px;"  type="button" 
                                  	id="submitLogin" onclick="return validateUser()">Login <i class="icon"></i></button>
                                  	<!-- <button class="btn fl btn-primary" type="submit" style="padding: 6px 11px;" onclick="return loginUserValidate()">Login <i class="icon"></i></button>  -->
                                  </div>                                                                
                          </div>
                          </div>  
                          <div class="row">                          
						  <div class="col-md-offset-3" >          
                                 <div class="col-sm-3" style="width:17%;padding-right: 0px;" >
	                              <div style="float: left;">
	                              	<label class="checkbox">
								    	<input type="checkbox" id="txtRememberme" name="txtRememberme"> Remember me
									</label>								                                                             
	                              </div>                               
                              </div>  
                             
                         </div> 
                          </div>  
                                   
                 
</form>


<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 	</table>
</div>
</div>

<!-- -------------Show this popup if user is type 3 and has multiple schools--------------- -->
                                   
<div class="modal fade hide" id="selectSchoolDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content"  style="top:141px;left:-50px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" onclick="cancel()" aria-hidden="true">x</button>
				<h3><spring:message code="headTm"/></h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="modalErrorMsg" class="required" style="display:block;width:100%;height:10px;margin-top:-10px;"></div>
					<div class="row top10">
						<div class="col-sm-offset-0">
							<div class="col-sm-10" >
								<label><spring:message code="lblPlzSltSchool"/></label>
								<select id="selectSchoolName" name="selectSchoolName" class="form-control" style="font-family:'Century Gothic', 'Open Sans', sans-serif;font-size: 11px;line-height: 20px;">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" onclick="ok()"><spring:message code="btnLogin"/> <i class="icon"></i></button>
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancel()"><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>
<!-- --------------------------------End of showing popup-------------------------------- -->


<!-- -------------Show this popup if user is type 6 and has multiple branches--------------- -->
<div class="modal fade hide" id="selectBranchDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content"  style="top:141px;left:-50px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" onclick="cancel()" aria-hidden="true">x</button>
				<h3>TeacherMatch</h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="modalBranchErrorMsg" class="required" style="display:block;width:100%;height:10px;margin-top:-10px;"></div>
					<div class="row top10">
						<div class="col-sm-offset-0">
							<div class="col-sm-10" >
								<label>Please select branch</label>
								<select id="branchName" name="branchName" class="form-control" style="font-family:'Century Gothic', 'Open Sans', sans-serif;font-size: 11px;line-height: 20px;">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" onclick="cont()">Login <i class="icon"></i></button>
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="close()">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!-- --------------------------------End of showing popup-------------------------------- -->

<script  type="text/javascript">
function validateUser()
{
	var flag = loginUserValidate();
  	if(flag==true)
  	{
  		$.ajax(
  		{
  			url: "isSA.do",
  			type: "POST",
  			async: false,
  			data:
  			{
  				emailAddress: $("#emailAddress").val(),
  				password: $("#password").val(),
  			},
  			success:function(result)
  			{
  				var myresult = jQuery.parseJSON(''+result+'');
  				$("#schoolName").val("");
  				$("#schoolID").val("");
  				
  				if(myresult.isType3User=="true" && myresult.isAuthorizedUser=="true" && myresult.noOfSchools>1)
  				{
  					//$("#selectSchoolName").focus();
  					
  					$('#selectSchoolName').find('option').remove();//remove the previous element.
  					$("#modalErrorMsg").text("");//remove the error message.
  					$('#selectSchoolName').append("<option value='none'><spring:message code="msgStSchool"/></option>");
  					$.each(myresult.schoolsDetails, function(index, data)
  					{
  						if(data.status=="A")
  						$('#selectSchoolName').append("<option value="+data.schoolId+">"+data.schoolName+"</option>");
  					});
  					$("#selectSchoolDiv").modal("show");
  					
  				}
  				else if(myresult.isType3User=="false" && myresult.isAuthorizedUser=="true" && myresult.noOfBranches>1)
  				{
  					$('#branchName').find('option').remove();//remove the previous element.
  					$("#modalBranchErrorMsg").text("");//remove the error message.
  					$('#branchName').append("<option value='none'>Select Branch</option>");
  					$.each(myresult.branchDetails, function(index, data)
  					{
  						if(data.status=="A")
  						$('#branchName').append("<option value="+data.branchId+">"+data.branchName+"</option>");
  					});
  					$("#selectBranchDiv").modal("show");
  					
  				}
  				else
  				{
  					$("#signinForm").submit();
  				}
  			},
  			error:function(jqXHR, textStatus, errorThrown)
  			{
  				alert("server error status:- "+textStatus+", error Thrown:- "+errorThrown+", error jqXHR:- "+jqXHR);
  			}
  		});
  	}
  	
  	return false;
}
/*When user click on ok then send the schoolName and school Id to the server*/
function ok()
{
	var schoolID = $("#selectSchoolName").val();
	var schoolName = $("#selectSchoolName option:selected").text();
	
	if(schoolID=="none")
		$("#modalErrorMsg").text("Please select school for login.");
	else
	{
		$("#selectSchoolDiv").modal("hide");	
		$("#schoolName").val(schoolName);
		$("#schoolID").val(schoolID);
		$("#signinForm").submit();
	}
}

function cont()
{
	var branchID = $("#branchName").val();
	var branchName = $("#branchName option:selected").text();
	if(branchID=="none")
		$("#modalBranchErrorMsg").text("Please select branch for login.");
	else
	{
		$("#selectBranchDiv").modal("hide");	
		$("#brName").val(branchName);
		$("#branchID").val(branchID);
		$("#signinForm").submit();
	}
}

function cancel()
{
	$("#selectSchoolDiv").modal("hide");
	$("#emailAddress").val("");
	$("#password").val("");
	$("#emailAddress").focus();
}

var txtBgColor="#F5E7E1";
function loginUserValidate()
{
	
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");
	
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	
	//document.getElementById("divServerError").style.display="none";	
	$('#emailAddress').css("background-color","");
	$('#password').css("background-color","");
	
	
	if(trim(emailAddress.value) == '')
	{
		$('#errordiv').append("&#149; Please enter Email<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value) != '' && !isEmailAddress(trim(emailAddress.value)))
	{		
		$('#errordiv').append("&#149; Please enter valid Email<br>");
		if(focs==0)
			$('#emailAddress').focus();
		$('#emailAddress').css("background-color", txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value) == '')
	{
		$('#errordiv').append("&#149; Please enter Password<br>");
		if(focs==0)
			$('#password').focus();
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	else if(password.value.length>12)
	{		
		$('#errordiv').append("&#149; Password length must be a maximum of 12 characters.<br>");
		if(focs==0)
			$('#password').focus();
		
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}
}

var redirectURL = "${id}";
var msg = "${msg}";
//alert("hello :::::::::  \n"+redirectURL);
$(document).ready(function(){
	//alert("Now:- "+redirectURL);
	//alert("msg:- "+msg+"\nredirectURL:- "+redirectURL);
	if(redirectURL!=null && redirectURL!="")
	{
		//alert("redirectURL:- "+redirectURL);
		$("#redirectURL").val(redirectURL);
		//alert( "redirectURL:- "+$("#redirectURL").val() );
	}
	if(msg!=null && msg!="")
	{
		$("#errordiv").append(msg);
	}
});

function redirectPage(redirectURL, errormessage)
{
	if(errormessage!=null && errormessage!="")
		alert(errormessage);
	else
		alert("redirect Page:- "+redirectURL);
}
</script>
