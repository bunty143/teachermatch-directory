<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<c:if test="${redirectTo eq 1}">
<link rel="stylesheet" href="css/dialogbox.css" />

<div id="displayDiv"></div>

<script type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>

<script>

var divMsg="You are using Invalid URL. Please check it again or contact to the concerned person";

addDialogContent("dm","TeacherMatch",divMsg,"displayDiv");

showDialog("dm",my,"650","200");

function my(){

window.location.href="signin.do";

}

</script>

</c:if>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<!--<script type="text/javascript" src="js/tmhome.js"></script>
--><c:set var="resourceMap" value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/searchjoborder.js?ver=${resourceMap['js/searchjoborder.js']}"></script>

<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.ajax']}"></script>
<script type='text/javascript' src="js/branchautoSuggest.js?ver=${resourceMap['js/branchautoSuggest.js']}"></script>
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<script>
function applyScrollOnTbl()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[62,175,90,100,145,160,100,110], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
     });			
}

function applyScrollOnTblHideZone()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[62,175,130,155,180,115,125], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
     });			
}

 function applyScrollOnTb()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#schoolListGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 680,
        minWidth: null,
        minWidthAuto: false,
        colratio:[350,330],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblGeoZoneSchool()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#geozoneSchoolTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 588,
        minWidth: null,
        minWidthAuto: false,
        //colratio:[55,266,70,90,100,105,70,66,151], // table header width
        //mukesh to set the table
        colratio:[380,250],
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });	
}
</script>


<link rel="stylesheet" type="text/css" href="css/base.css" />  
<style>
.tmlogo{
	pointer-events: none;
}
</style>   

<input type="hidden" id="gridNo" name="gridNo" value="">
<div  class="modal hide" onclick="getSortSecondGrid()" id="myModalforSchhol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 742px;">
	<div class="modal-content">
	<div class="modal-header" id="schoolHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headSchL"/></h3>
	</div>
	<div class="modal-body" id="schoolListDiv">
		
	</div> 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  onkeypress="chkForEnter(event)" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<input type="hidden" id="hdnDistrictName" name="hdnDistrictName" value="${districtMaster.districtName}"/>
	<input type="hidden" id="hdnSchoolName" name="hdnSchoolName" value="${schoolMaster.schoolName}"/>
	<input type="hidden" id="hdnSenderName" name="hdnSenderName" value="${teacherDetail.firstName} ${teacherDetail.lastName}"/>
	<input type="hidden" id="hdnSenderEmail" name="hdnSenderEmail" value="${teacherDetail.emailAddress}"/>
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headSignUpForJobAlerts"/></h3>
	</div>
	<div class="modal-body">	
		<div class="row col-sm-12 col-md-12">
			<div class='divErrorMsg' id='errordiv' style="display: block;"></div>			
		</div>				
		<div class="row">
			<div class="col-sm-6 col-md-6">
		    	<label><strong><spring:message code="lblName"/><span class="required">*</span></strong></label>
		    	<c:choose>
		    		<c:when test="${not empty teacherDetail.firstName}">
		    			<input id="senderName" name="senderName" type="text" class="form-control" maxlength="75" value="${teacherDetail.firstName} ${teacherDetail.lastName}"/>
		    		</c:when>
		    		<c:otherwise>
		    			<input id="senderName" name="senderName" type="text" class="form-control" maxlength="75" value=""/>
		    		</c:otherwise>
		    	</c:choose>	        	
			</div>
			<div class="col-sm-6 col-md-6">
		    	<label><strong><spring:message code="lblEmailAddress"/><span class="required">*</span></strong></label>
	        	<input id="senderEmail" name="senderEmail" type="text" class="form-control" maxlength="75" value="${teacherDetail.emailAddress}" />
			</div>
			
		</div>		
		<div class="row">
			<div class="col-sm-6 col-md-6">
		    	<label><strong><spring:message code="lblDistrict"/></strong></label>      	
	        	
				<c:if test="${not empty districtMaster}">
					
				</c:if>			
				<input  type="hidden" id="districtId1" name="districtId1" value="${districtMaster.districtId}">
				<%-- 
				<input  type="hidden" id="districtId" name="districtId" value="${param.districtId}">
				--%>
				<input  type="text" 
					class="form-control"
					maxlength="100"					
					id="districtName1" 
					autocomplete="off"				
					value="${districtMaster.districtName}"
					name="districtName1" 
					onfocus="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData1', 'districtName1','districtId1','');"
					onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtDistrictData1', 'districtName1','districtId1','');" 
					onblur="hideDistrictMasterDiv(this,'districtId1','divTxtDistrictData1');"/>
				<div id='divTxtDistrictData1' style=' display:none;position:absolute; z-index: 2000;' 
				onmouseover="mouseOverChk('divTxtDistrictData1','districtName1')" class='result' ></div>	
	        	        	
			</div>
			<div class="col-sm-6 col-md-6">
		    	<label><strong><spring:message code="lblSchool"/></strong></label>	        	
				<c:if test="${not empty schoolMaster}">					
				</c:if>
				<input  type="hidden" id="schoolId1" name="schoolId1" value="${schoolMaster.schoolId}">
				<input  type="text" 
					class="form-control"					
					maxlength="100"
					id="schoolName1" 
					autocomplete="off"
					value="${schoolMaster.schoolName}"
					name="schoolName1" 
					onfocus="getSchoolMasterAutoComp(this, event, 'divTxtSchoolData1', 'schoolName1','schoolId1','');"
					onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtSchoolData1', 'schoolName1','schoolId1','');" 
					onblur="hideSchoolMasterDiv(this,'schoolId1','divTxtSchoolData1');setBlankZip();"/>
				<div id='divTxtSchoolData1' style=' display:none;position:absolute; z-index: 2000;' 
				onmouseover="mouseOverChk('divTxtSchoolData1','schoolName1')" class='result' ></div>	
			</div>
		</div>	            
        
		<div class="row">
			<div class="col-sm-6 col-md-6">
		    	<label><strong><spring:message code="lblJobCat"/></strong></label>
	        	<select id="jobCategoryId1" name="jobCategoryId1" class="form-control" >
					<option value="0"><spring:message code="optAll"/></option>
					<c:forEach var="jbCategory" items="${lstJobCategoryMasters}">
					<option value="${jbCategory.jobCategoryId}">${jbCategory.jobCategoryName}</option>				
				</c:forEach>			
			</select>	
			</div>
			<div class="col-sm-6 col-md-6">
		    	<label><strong><spring:message code="lblZepCode"/></strong></label>
	        	<input id="zipCode1" name="zipCode1" type="text" class="form-control" maxlength="6" />
			</div>
		</div>		
 	</div> 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr"/></button>
 		<button class="btn btn-primary" onclick="saveJobAlert()"><spring:message code="btnSvChng"/></button>
 	</div>
</div>
</div>
</div>


<%-- ============== Confirmation Div Message    =========== --%>
<div  class="modal hide"  id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>
</div>
</div>


<div class="modal hide"  id="geozoneschoolDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:650px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);">x</button>
		<h3 id="zoneName"></h3>
	</div>
	<div class="modal-body" style=""> 		
		<div class="control-group">
				<input type="hidden" id="geozoneId" value="0"/>
				<input type="hidden" id="geozoneschoolFlag" value="0"/>
				<div id="geozoneschoolGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id="">
 		</span>&nbsp;&nbsp;<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$('#geozoneschoolFlag').val(0);"><spring:message code="btnClose"/></button> 		
 	</div>
</div>
</div>
</div>


<input id="redirectTo" name="redirectTo" type="hidden" value="${redirectTo}"/>
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="${logoPath}">
         </div>        
         <div style="float: left;">
         	<c:if test="${(jobBoardSource eq 1) || (jobBoardSource eq 2) || (jobBoardSource eq 4)}">
         	
         	<c:choose>
			    <c:when test="${not empty districtMaster.displayName}">
			       <div class="subheading" style="font-size: 21px;">${districtMaster.displayName} Job Board</div>	
			        <br />
			    </c:when>    
			    <c:otherwise>
			       <div class="subheading" style="font-size: 21px;">${districtMaster.districtName} Job Board</div>	
			        <br />
			    </c:otherwise>
			</c:choose>
         	
         	<!-- <div class="subheading" style="font-size: 21px;">${districtMaster.districtName} Job Board</div>-->	
           	</c:if>
         	<c:if test="${(jobBoardSource eq 3) && headQuarterMaster.headQuarterId eq 1}">
         	<div class="subheading" style="font-size: 21px;margin-top:20px;">Kelly Quest</div>
         	</c:if>	
         	<c:if test="${(jobBoardSource eq 3) && headQuarterMaster.headQuarterId eq 3}">
         		<div class="subheading" style="font-size: 21px;">The Ohio 8 Quest</div>
         	</c:if>	
         	
         	
         	<c:if test="${districtMaster.districtId ne 7800292}">
         	<div class="left10" style="font-size: 10px;">${address}</div>
         	</c:if>	
         </div>
		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>


<div  onkeypress="chkForEnterJobSearch(event)" >
  <c:set var="readOnlyCheck" value=""/>
				<c:if test="${not empty districtMaster}">
					<c:set var="readOnlyCheck" value="readOnly"/>	              
		             	  <script type="text/javascript">
		             	    getZoneDAForEJob(${districtMaster.districtId});
		             	  </script>
	          	</c:if>	
	     
	      	<!-- shadab start -->
				    <div class="row" style="width:99%;">
					<div class="col-sm-10 col-md-10">
						<label>Keywords </label>
							<input  type="text" 
											class="form-control"
											maxlength="100"
											id="searchTerm" 
											value=""
											placeholder="Search for jobs by any keyword, branch name, district name, zip code and more….. "
											name="searchTerm" 
											style="width:83%"
											 onkeypress="return runScript(event)"
											/>
						</div>
									
					</div>
				
				<!-- shadab end -->     	
		
		<c:if test="${(jobBoardSource eq 3) || (jobBoardSource eq 4) || (jobBoardSource eq 5)}">
		<!-- Start Kelly Design -->
		<div class="row">
	                    
                      <c:if test="${jobBoardSource eq 4}">
                      	<div class="col-sm-4 col-md-4">
                      	<label>HeadQuarter Name</label><br/>
             			<label>${sHQName}</label>
             			</div>
             			<input  type="hidden" id="branchId" name="branchId" value=${branchMaster.branchId}>
             		  </c:if>		
	                    
	                    <c:if test="${jobBoardSource eq 3}">
		                    <c:if test="${not empty headQuarterMaster}">
		                    	<c:if test="${headQuarterMaster.isBranchExist eq true}">
		                    		<div class="col-sm-4 col-md-4">
				                      <label>Branch Name</label>
			           					<input type="text" id="branchName" name="branchName"  class="form-control"
			           					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
										onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchId','');"
										onblur="hideBranchMasterDiv(this,'branchId','divTxtShowDataBranch');"	/>
										<input  type="hidden" id="branchId" name="branchId" value="">
										<div id='divTxtShowDataBranch' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>							
				                    </div>
		                    	</c:if>
		                    </c:if>
	                    </c:if>
	                     
	                    <div class="col-sm-4 col-md-4">
	                      <label>District Name</label>
           					<input type="text" id="districtName" name="districtName"  class="form-control"
           					onfocus="getDistrictAuto(this, event, 'divTxtShowDataDistrict', 'districtName','districtId','');"
							onkeyup="getDistrictAuto(this, event, 'divTxtShowDataDistrict', 'districtName','districtId','');"
							onblur="hideDistMasterDiv(this,'districtId','divTxtShowDataDistrict');getJobCategoryByDistrict();"	/>
							<input  type="hidden" id="districtId" name="districtId" value="">
							<div id='divTxtShowDataDistrict' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataDistrict','districtName')" class='result' ></div>							
	                    </div>
	    </div>
		<!--  End kelly Design -->
		</c:if>
		
		<div class="row">
            <input  type="hidden" id="districtId" name="districtId" value="${districtMaster.districtId}">
	        <div id='divTxtDistrictData' style=' display:none;position:absolute; z-index: 2000;' 
			onmouseover="mouseOverChk('divTxtDistrictData','districtName')" class='result' ></div>		
			
			<c:if test="${districtMaster.districtId ne 804800}">
			<div class="col-sm-8 col-md-8">
				<div class="row">
				
					<c:if test="${(jobBoardSource eq 1) || (jobBoardSource eq 2) || (jobBoardSource eq 5)}">
					<div class="col-sm-6 col-md-6">
							<label><spring:message code="lblSchool"/></label>
						<c:set var="readOnlyCheck" value=""/>
						<c:if test="${not empty schoolMaster}">
							<c:set var="readOnlyCheck" value="readOnly"/>
						</c:if>
						<input  type="hidden" id="schoolId" name="schoolId" value="${schoolMaster.schoolId}">
						 
						
						<input  type="text" 
							class="form-control"
							${readOnlyCheck}
							maxlength="100"
							id="schoolName" 
							autocomplete="off"
							value="${schoolMaster.schoolName}"
							name="schoolName" 
							onfocus="getSchoolMasterAutoComp(this, event, 'divTxtSchoolData', 'schoolName','schoolId','');"
							onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtSchoolData', 'schoolName','schoolId','');" 
							onblur="hideSchoolMasterDiv(this,'schoolId','divTxtSchoolData');"/>
						<div id='divTxtSchoolData' style=' display:none;position:absolute; z-index: 2000;' 
						onmouseover="mouseOverChk('divTxtSchoolData','schoolName')" class='result' ></div>	
					</div>
					</c:if>
					
					<div class="col-sm-6 col-md-6">
							<label>
								Jobs Category
							</label>
							<!--
						    <select id="jobCategoryId" name="jobCategoryId" class="form-control" onblur="getJobSubCateList();">
								-->
								  <select id="jobCategoryId" name="jobCategoryId" class="form-control" >
								<option value="0">All</option>
								<c:forEach var="jbCategory" items="${lstJobCategoryMasters}">
								<option value="${jbCategory.jobCategoryId}">${jbCategory.jobCategoryName}</option>				
								</c:forEach>			
							</select>
					</div>		
				<c:if test="${flag ne true}">				
				<c:if test="${ jobBoardSource ne 4 && (empty schoolMaster && districtMaster.includeZipCode) or jobBoardSource eq 3}">
					<div class="col-sm-6 col-md-6">
					          <c:choose>
								<c:when test="${(empty schoolMaster && districtMaster.includeZipCode) or jobBoardSource eq 3}">
								<div>
									<label>
										<spring:message code="lblZepCode"/>
									</label>
									<input id="zipCode" name="zipCode" type="text" class="form-control">	
								</div>
								</c:when>
								<c:otherwise><input id="zipCode" name="zipCode" type="hidden" class="form-control"></c:otherwise>
							</c:choose>
							<c:if test="${lstJobOrder.districtMaster.isZoneRequired eq 1 && districtMaster.includeZone}">
				   				<div id="displayZone">
						         	<label id="zoneLable"><spring:message code="lblZone"/></label>
									<select id="zone" class="form-control" >
										
									</select>
					     		</div>	
				   		   </c:if> 
					</div>
					</c:if>
					<!--<c:if test="${jobBoardSource ne 5 && jobBoardSource ne 4}"> -->
						<c:if test="${jobBoardSource eq 3 or districtMaster.includeState}" >
						<div class="col-sm-6 col-md-6">
								<label>
									State
								</label>
							    <select id="stateId" name="stateId" class="form-control">
									<option value="0">All</option>
									<c:forEach var="stState" items="${lstState}">
									<option value="${stState.stateId}">${stState.stateName}</option>				
									</c:forEach>			
								</select>
						</div>
						</c:if>
					<!--</c:if>-->
					 <c:choose>
       					<c:when test="${userMaster.districtId.districtId ne 804800}">
       					<c:if test="${(jobBoardSource eq 1 || jobBoardSource eq 2) && districtMaster.includeZone}">									
							<div class="col-sm-6 col-md-6">
								<label id="zoneLable">Zone</label>
								<select id="zone" class="form-control" >							
						 	</select>					 	
							</div>	
						</c:if>
         			</c:when>
         			<c:otherwise>
         				 <div class="col-sm-4 col-md-4 hide">
							<label id="zoneLable">Zone</label>
							<select id="zone" class="form-control" >
							</select>					 	
						</div>	
					</c:otherwise>
					</c:choose>
					</c:if>
					<c:if test="${districtMaster ne null && flag eq true}">
							<div class="col-sm-6 col-md-6">
								<label class=""><spring:message code="lblSchN"/></label>
								<!--<input type="text" name="positionStartDate" class="form-control" id="positionStartDate">-->
								<select id="gradeLevel" name="gradeLevel" class="form-control">
							 	   <option value="" ><spring:message code="optStlScl"/></option>	
							 	   <option value="Elementary School" ><spring:message code="optElementarySchool"/></option>	
							 	   <option value="Middle School" ><spring:message code="optMiddleSchool"/></option>	
							 	   <option value="High School" ><spring:message code="optHighSchool"/></option>	
								</select>			 	
							</div>
							<div class="col-sm-6 col-md-6">
								<label id="lblpositionStartDate"><spring:message code="lblPositionStartDate"/></label>
								<!--<input type="text" name="positionStartDate" class="form-control" id="positionStartDate">-->
								 <select name="positionStart" class="form-control" id="positionStart">
							          <option value=""><spring:message code="lblSelectPositionStartDate"/></option>
						          	  <c:if test="${fn:length(positionStartList) gt 0}"></c:if>
						          	   <c:forEach items="${positionStartList}" var="positiontStr">
						          	    <option value="${positiontStr}" <c:if test="${jobOrder.positionStart ne null && jobOrder.positionStart eq positiontStr}">selected</c:if>>${positiontStr}</option>
					          	    </c:forEach>
                				 </select>				 	
							</div>
					</c:if>
					
					<c:if test="${jobBoardSource eq 5}">
					<div class="col-sm-6 col-md-6">
		                 		<label>Job Sub Category</label>
				               <div id="jobSubCateDiv"></div>
		        	</div>	
		        	</c:if>
					
						<div class="col-sm-6 col-md-6">			
							<c:if test="${empty schoolMaster}">
								<div class="span5">
									<label>
									</label>
								</div>
							</c:if>		
						</div>
					
				</div>
			</div>
			</c:if>
			
			<!--  Jeffco Only Code  -->
			<c:if test="${districtMaster.districtId eq 804800}">
			<div class="col-sm-8 col-md-8">
				<div class="row">
				
					<c:if test="${(jobBoardSource eq 1) || (jobBoardSource eq 2) || (jobBoardSource eq 5)}">
					<div class="col-sm-6 col-md-6">
							<label><spring:message code="lblSchool"/></label>
						<c:set var="readOnlyCheck" value=""/>
						<c:if test="${not empty schoolMaster}">
							<c:set var="readOnlyCheck" value="readOnly"/>
						</c:if>
						<input  type="hidden" id="schoolId" name="schoolId" value="${schoolMaster.schoolId}">
						 
						
						<input  type="text" 
							class="form-control"
							${readOnlyCheck}
							maxlength="100"
							id="schoolName" 
							autocomplete="off"
							value="${schoolMaster.schoolName}"
							name="schoolName" 
							onfocus="getSchoolMasterAutoComp(this, event, 'divTxtSchoolData', 'schoolName','schoolId','');"
							onkeyup="getSchoolMasterAutoComp(this, event, 'divTxtSchoolData', 'schoolName','schoolId','');" 
							onblur="hideSchoolMasterDiv(this,'schoolId','divTxtSchoolData');"/>
						<div id='divTxtSchoolData' style=' display:none;position:absolute; z-index: 2000;' 
						onmouseover="mouseOverChk('divTxtSchoolData','schoolName')" class='result' ></div>	
					</div>
					</c:if>
					
					
							
				<c:if test="${ jobBoardSource ne 4}">
					<div class="col-sm-6 col-md-6">
					          <c:choose>
								<c:when test="${empty schoolMaster && districtMaster.includeZipCode}">
								<div>
									<label>
										<spring:message code="lblZepCode"/>
									</label>
									<input id="zipCode" name="zipCode" type="text" class="form-control">	
								</div>
								</c:when>
								<c:otherwise><input id="zipCode" name="zipCode" type="hidden" class="form-control"></c:otherwise>
							</c:choose>
							<c:if test="${lstJobOrder.districtMaster.isZoneRequired eq 1 && districtMaster.includeZone}">
				   				<div id="displayZone">
						         	<label id="zoneLable"><spring:message code="lblZone"/></label>
									<select id="zone" class="form-control" >
										
									</select>
					     		</div>	
				   		   </c:if> 
					</div>
					</c:if>
					
					<div class="col-sm-6 col-md-6">
							<label>
								Job Category
							</label>
							<!--
						    <select id="jobCategoryId" name="jobCategoryId" class="form-control" onblur="getJobSubCateList();">
								-->
								  <select id="jobCategoryId" name="jobCategoryId" class="form-control" onchange="getJobSubcategory();" >
								<option value="0">All</option>
								<c:forEach var="jbCategory" items="${lstJobCategoryMasters}">
								<option value="${jbCategory.jobCategoryId}">${jbCategory.jobCategoryName}</option>				
								</c:forEach>			
							</select>
					</div>		
					
					
					 <div class="col-sm-6 col-md-6">
		                 		<label>Sub Job Category</label>
				               <div id="jobSubCateDiv">
				                <select id="jobSubCategoryId" name="jobSubCategoryId" class="form-control" >
				                <option value="0">Select Job Sub Category Name</option>
				                </select>
				               </div>
		        	</div>	
					
					<!--<c:if test="${jobBoardSource ne 5 && jobBoardSource ne 4}">-->
					<c:if test="${jobBoardSource eq 3 or districtMaster.includeState}" >
					<div class="col-sm-6 col-md-6">
							<label>
								State
							</label>
						    <select id="stateId" name="stateId" class="form-control">
								<option value="0">All</option>
								<c:forEach var="stState" items="${lstState}">
								<option value="${stState.stateId}">${stState.stateName}</option>				
								</c:forEach>			
							</select>
					</div>
					</c:if>
					<!--</c:if> -->
					 <c:if test="${districtMaster.includeZone}" >
         				 <div class="col-sm-4 col-md-4 hide">
							<label id="zoneLable">Zone</label>
							<select id="zone" class="form-control" >
							</select>					 	
						</div>	
					</c:if>	
					
				<!--  Jeffco Only Code  -->
				
						<div class="col-sm-6 col-md-6">			
							<c:if test="${empty schoolMaster}">
								<div class="span5">
									<label>
									</label>
								</div>
							</c:if>		
						</div>
					
				</div>
			</div>
			</c:if>
			<c:if test="${(jobBoardSource eq 1) || (jobBoardSource eq 2)}">
			<div class="col-sm-4 col-md-4">
					<label>
						Subject(s)
					</label>			
					<select id="subjectId" name="subjectId" class="form-control" multiple="multiple" size="4">
						<option value="">All</option>	
						<c:forEach var="subjectList" items="${lstSubjectMasters}">
							<option value="${subjectList.subjectId}">${subjectList.subjectName}</option>				
						</c:forEach>
					</select>
		    </div>
		    </c:if>
	    </div>
     
	    <div class="row top10">				
        <input type="hidden" id="isZoneAvailable" name="isZoneAvailable"/>
			<div class="col-sm-3 col-md-3">
				<!--<button class="btn btn-large btn-primary"  onclick="return searchJob()">Search <i class="icon"></i></button>
				-->
				<button class="btn btn-large btn-primary"  onclick="return esSearchdistrictjobboard()">Search <i class="icon"></i></button>
				
			</div>
			<c:set var="color" value=""></c:set>
			<c:if test="${not empty param.hqId}">
			<c:set var="color" value="color:maroon"></c:set>
			</c:if>
			<div class="col-sm-3 col-md-3">&nbsp;</div>
			<div class="col-sm-6 col-md-6" style="text-align: right;${color}"><br>
			Already registered with TeacherMatch, please <a href="javascript:void(0)" onclick="window.open('signin.do');">click here</a> to login
				<br/>Do not have a TeacherMatch account, <a href="javascript:void(0)" onclick="window.open('signup.do');">signup</a> here
			</div>	
			
	    </div>	
	    
</div>	



	<div class="TableContent top15">        	
            <div class="table-responsive"  id="divJobsBoard" onclick="getSortFirstGrid()">          
                	         
            </div>            
    </div> 



<div style="display:none;" id="loadingDiv">
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>
<c:if test="${jobBoardSource eq 1 || jobBoardSource eq 2}">
<br/>Interested in receiving automated alerts for jobs from this District? Please <a href="javascript:void(0);" onclick="showJobAlertForm()">sign-up</a>
<br/>&nbsp;
</c:if>

<c:if test="${jobBoardSource eq 4}">
<br/><div class="pull-right"><a target="_blank" href="javascript:void(0);" onclick="window.open('jobsboard.do?hqId=1321');">Click here for more Kelly Educational Staffing opportunities</a></div>
</c:if>

<input type="hidden" id="headQuarterId" value="${headQuarterMaster.headQuarterId}"/>
<input type="hidden" id="jobBoardSource" value="${jobBoardSource}"/>
<input type="hidden" id="isBranchExist" value="${headQuarterMaster.isBranchExist}"/>

<input type="hidden" id="talentRef" value="${talentRef}"/>
<input type="hidden" id="teacherId" value="${teacherId}"/>
<input type="hidden" id="requestType" value="1" />"
<input type="hidden" id="subjobCategoryhiddenId" value="${subjobCategoryIdCheck}" />

<script>
//displayState();
//searchJob();
//selectSubJobCategoryMethod();
esSearchdistrictjobboard();
setJobBoardReferralURL();
$('#myModal').modal('hide');
</script>


<script type="text/javascript">
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("positionStartDate", "positionStartDate", "%m-%d-%Y");
     
  	$(document).ready(function(){
	  		 cal.manageFields("positionStartDate", "positionStartDate", "%m-%d-%Y");
	}); 
			
     </script>
