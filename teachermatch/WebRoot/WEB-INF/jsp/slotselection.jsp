<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src="js/slotselection.js?ver=${resourceMap['js/slotselection.js']}"></script>

<script type="text/javascript" src="dwr/interface/SlotSelectionAjax.js?ver=${resourceMap['SlotSelectionAjax.ajax']}"></script>
 
 
<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});

function applyScrollOnTblEvent()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#eventTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[170,180,150,160,195,90], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}

function applyScrollOnTblSlots()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#scheduleTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 280,
        width: 895,
        minWidth: null,
        minWidthAuto: false,
        colratio:[45,180,180,180,310], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
   });			
}
 

</script>
<div class="row" style="margin:0px;">
                
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headTimeSlotSelectionforEvents"/></div>	
         </div>			
		 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<br>



 <div class="TableContent">  	
     <div class="table-responsive" id="eventGrid" >    
     </div>                  
 </div>


 <div class="TableContent">  	
     <div class="table-responsive" id="eventGridNew" >    
     </div>                  
 </div>
 
	                  
 <input type="hidden" id="gridNo" name="gridNo" value="">	                  

 

<div class="modal hide"  id="slotsdiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="background:rgba(0,0,0,0.6);">
	<div class="modal-dialog" style="width:950px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideSlotDiv();">x</button>
	   	<h3 id=""><spring:message code="headScheduleGrid"/></h3>
	</div>
	
	<div class="modal-body" style=" overflow-y: auto;"> 
	<div id="errordivSchedule" class="hide" style="color: red;padding-bottom: 10px;"></div>		
	    <div id="scheduleGrid">
	                  <table id="scheduleTable" width="100%" border="0" class="table table-bordered mt10">
	                  </table>
	    </div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" id="apprdiv" aria-hidden="true" onclick='approveSlots();'>&nbsp;&nbsp;&nbsp;Confirm Slot&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='hideSlotDiv();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>



	<div style="display:none;" id="loadingDiv">
	     <table  align="center" >
	 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
			<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	</div>

    <div id="" class="hide" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
			 <iframe src="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
			 </iframe>  
    </div> 


 <input type="hidden" id="teacherId" name="teacherId" value="${teacherId}">

  <script type="text/javascript">
	 displayEventGridsNew(${eventId},'${email}');
	 //var slotSelectionNeeded = ${slotSelectionNeeded};
	 //if(slotSelectionNeeded=="false")
	   //window.location.href="myassessments.do";
  </script>	                  
  
  <input type="hidden" id="eventId" name="eventId" value="${eventId}">
  <input type="hidden" id="email" name="email" value="${email}">
  