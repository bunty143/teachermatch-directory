<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resouceMap['css/base.css']}" />
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>

<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafstart.js?ver=${resouceMap['js/jobapplicationflow/jafstart.js']}"></script>

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});

</script>
<style>
	.hide
	{
		display: none;
	}
</style>
<input type="hidden" id="jobId" value='${jobId}'/>
<input type="hidden" id="teacherId" value='${tcrId}'/>


<div  class="modal hide"  id="myModal2Panel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2showPanel'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnOk"/></button>
 	</div>
</div>
</div>
</div>
<iframe id='jsiFrameID' name='jsiFrame' height='0' width='0'  frameborder='50' scrolling='yes' style='display:none;'>
	</iframe>
<div class="modal hide"  id="myModalLogin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-feedbackandsupport">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="btnLogin"/></h3>
	</div>
	<form action="jsisignin.do"  method="post"  target='jsiFrame' onsubmit="return loginUserValidate();">
	<div class="modal-body">
	
		<div class="col-sm-12 col-md-12">
			<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
			<div class='divErrorMsg' id='divServerError' style="display: block;"></div>
		</div>
			<div class="col-sm-12 col-md-12">
				<label><strong><spring:message code="lblEmail"/></strong><span class="required">*</span></label>
	        	<input id="emailAddress" name="emailAddress" type="text" class="form-control" maxlength="100" />
			</div>	            
    
			<div class="col-sm-12 col-md-12">
		    	<label><strong><spring:message code="lblPass"/></strong><span class="required">*</span></label>
	        	<input id="password" name="password" type="password" class="form-control" maxlength="100"/>
	        	<input id="tId" name="tId" type="hidden" value='${tcrId}'/>
			</div>	
			<div class="col-sm-12 col-md-12">
	        	<label class="checkbox">
					<input type="checkbox" id="txtRememberme" name="txtRememberme"> <spring:message code="boxRemeberPass"/>
				</label>	               
			</div>	
		
 	</div>
 	
 	<div class="modal-footer">
 		<button class="btn btn-primary" ><spring:message code="btnLogin"/></button>
 	</div>
 	</form>
</div>
	</div>
</div>
<script type="text/javascript">

	if(${errorMsg}==0)
	{	
		//$('#message2showPanel').html("You are going to start the JSI.");
		//$('#myModal2Panel').modal('show');
		checkInventory(${jobId},null);
		
	}else if(${errorMsg}==1)
	{	
		$('#message2showPanel').html("This is not a valid URL.");
		$('#myModal2Panel').modal('show');
		
	}if(${errorMsg}==2)
	{	
		$('#message2showPanel').html("Your account is deactivated. Please contact to TeacherMatch Client Service.");
		$('#myModal2Panel').modal('show');
			
	}if(${errorMsg}==3)
	{	
		//$('#message2showPanel').html("You need to login first.");
		//$('#myModal2Panel').modal('show');
		$('#emailAddress').focus();
		$('#myModalLogin').modal('show');
		
	}
	if(${errorMsg}==4)
	{	
		$('#message2showPanel').html("JSI is Timed-Out.");
		$('#myModal2Panel').modal('show');
	}if(${errorMsg}==5)
	{	
		$('#message2showPanel').html("JSI is Completed.");
		$('#myModal2Panel').modal('show');
	}if(${errorMsg}==6)
	{	
		$('#message2showPanel').html("JSI is not active.");
		$('#myModal2Panel').modal('show');
	}

var txtBgColor="#F5E7E1";

function loginUserValidate()
{
	
	var emailAddress = document.getElementById("emailAddress");
	var password = document.getElementById("password");
	
	
	var cnt=0;
	var focs=0;	
	$('#errordiv').empty();
	
	document.getElementById("divServerError").style.display="none";	
	$('#emailAddress').css("background-color","");
	$('#password').css("background-color","");
	
	
	if(trim(emailAddress.value) == '')
	{
		$('#errordiv').append("&#149; Please enter Email<br>");
		if(focs==0)
			$('#emailAddress').focus();
		
		$('#emailAddress').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	if(trim(emailAddress.value) != '' && !isEmailAddress(trim(emailAddress.value)))
	{		
		$('#errordiv').append("&#149; Please enter valid Email<br>");
		if(focs==0)
			$('#emailAddress').focus();
		$('#emailAddress').css("background-color", txtBgColor);
		cnt++;focs++;
	}
	if(trim(password.value) == '')
	{
		$('#errordiv').append("&#149; Please enter Password<br>");
		if(focs==0)
			$('#password').focus();
		$('#password').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt==0)
		return true;
	else
	{
		$('#errordiv').show();
		return false;
	}
}

function getSuccessRes(dd)
{
	if(dd.teacherblockflag==2)
	{
		$('#errordiv').append("&#149; "+dd.msgError+"<br>");
		$('#errordiv').show();
	}else if(dd.authmail==false)
	{
		$('#errordiv').append("&#149; "+dd.msgError+"<br>");
		$('#errordiv').show();
	}else if(dd.authmail)
	{
		checkInventory(${jobId},null);
		$('#myModalLogin').modal('hide');
	}else
	{
		$('#errordiv').append("&#149; "+dd.msgError+"<br>");
		$('#errordiv').show();
	}
	
	
}
</script>
                
                 