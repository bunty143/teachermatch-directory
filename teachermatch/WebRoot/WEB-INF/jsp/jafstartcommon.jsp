<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="modal hide"  id="DSPQNCDivNotification"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					Portfolio is not configured for this job. Please contact to TeacherMatch.
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	     <span id=""><button class="btn  btn-primary" onclick="DSPQNCNotificationClose()"><spring:message code="btnOk" /> <i class="icon"></i></button></span>
	 	     <!-- 
	 	     &nbsp;&nbsp;
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button>
	 		 --> 		
	 	</div>
	  </div>
	</div>
</div>

<div class="modal hide"  id="DSPQCompletedDivNotification"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="DSPQCompletedDivNotificationClose()">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div id="deact" class="modal-body"> 		
				<div class="control-group">
					You have already completed the job.
				</div>
		 	</div>
	 	 <div class="modal-footer">
	 	     <span id=""><button class="btn  btn-primary" onclick="DSPQCompletedDivNotificationClose()"><spring:message code="btnOk" /> <i class="icon"></i></button></span>
	 	     <!-- 
	 	     &nbsp;&nbsp;
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="lnkCancel" /></button>
	 		 --> 		
	 	</div>
	  </div>
	</div>
</div>

<iframe id='downloadCustomFileUploadIFrameId' name='downloadCustomFileUploadIFrameId' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>