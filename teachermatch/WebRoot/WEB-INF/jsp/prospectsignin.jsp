<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/quest.js?ver=${resourceMap['js/quest.js']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>	
<script type="text/javascript" src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}"></script>	
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<link rel="stylesheet" href="css/dialogbox.css" />
<div id="displayDiv"></div>
<div class="tabletbody">
<form id='frmpowertracker'  method='post' target='uploadFrame'>
<input type="hidden" id="key" name="key" value="${param.key}">
<input type="hidden" id="id" name="id" value="${param.id}">
						<!--<div class="row"  style='padding-bottom:20px;'>
						  <div  class="col-sm-6 col-md-offset-3"  style="border: 1px solid;color:red;margin-left:260px;">
						   	  <div>	Please note that TeacherMatch will be performing scheduled maintenance at - <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sunday, May 18, 2014 from 3:00 AM - 5:00 AM CST</b></br>
								During this time, the application will be unavailable.<br/>
								</div>
						  </div>
						 </div>
                        --><div class="row top15">
						  <div class="col-sm-3 col-md-offset-3">
						   	  <div class="subheadinglogin"><spring:message code="btnLogin"/></div>
						  </div>
						  						  
						  <div class="col-sm-3 col-md-offset-3 systemsetup">						     
						      <a href="systemsetup.do" target="_blank">&nbsp;</a> <button  class="btn fl btn-secondary" type="button" onclick="return testYourSetUp();"><spring:message code="btnTestSetup"/> <i class="icon2"></i></button>						   
						  </div>
						  
						 </div> 
						 <div class="row">  
                         <div class="col-md-offset-3">
   							        <div class="col-sm-6">			                         
									<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
									<div class='divErrorMsg' id='divServerError' style="display: block;">${msgError}</div>			             
				   					</div>  
                         </div>
                         </div>
                         <div class="row top10">
						 <div class="col-md-offset-3"> 						                     
                                   <div class="col-sm-6">
                                        <label><spring:message code="lblEmail"/></label>
                                   	    <input type="text" name="emailAddress" value="${qemailAddress}" id="emailAddress" placeholder="Enter your Email" class="form-control"  /> 
                                   </div>  
                          </div>
                          </div>  
                          <div class="row top-sm0">      
                          <div class="col-md-offset-3">            
                                  <div class="col-sm-6">
                                        <label><spring:message code="lblPass"/></label>
                                        <input type="password" name="password" id="password" placeholder="Enter your password" class="form-control" maxlength="12"/>
                                  </div>
                                  <div class="col-sm-6 top30-sm">
                                     <a href="forgotpassword.do"><spring:message code="lnkIForgot"/></a>   
                                  </div>                                                                
                          </div>
                          </div>  
                          <div class="row">                          
						  <div class="col-md-offset-3" >          
                                 <div class="col-sm-6">
	                              <div style="float: left;">
	                              	<label class="checkbox">
								    	<input type="checkbox" id="teacherTxtRememberme" name="teacherTxtRememberme"> <spring:message code="boxRemeberPass"/>
									</label>	                               
	                              </div>	                             
	                              <div style="clear: both;"></div>                                    
                              </div>                                  
                         </div> 
                          </div>  
                          <div class="row">                         
                          <div class="col-md-offset-3 top5">                           
                          	<div class="left15">     		
		            		  <button class="btn fl btn-primary" type="submit" onclick="return loginTeacher();"><spring:message code="btnLogin"/> <i class="icon"></i></button>			                
		              		</div>        
                              
                              <br/><br/>                             
                              <div class="left15 top10">
                              <!-- Are you currently a Teacher looking for opportunities? Please <a href="signup.do">Sign Up</a> -->
                              <spring:message code="lblDontHaveLoginYet"/> <a href="questsignup.do?user=0"><spring:message code="btnSign"/></a>
                              </div> 
                              
                              <!-- <div class="left15" >
                               Are you a District or School interested in hiring great teachers, please contact us <a href="http://www.teachermatch.org/contact-us/" target="_blank">here</a>
                              </div> 
                               -->
                              <div class="left15" style="margin-top:100px;margin-bottom: -10px;"  >
                               <spring:message code="msgLogSiteTM"/> <a href="termsofuse.do"><spring:message code="lnkTermsUse"/></a>        
                              </div>                            
                          </div> 
                     </div>         
                 
</form>
<c:if test="${teacherblockflag eq 1}">
		<div class="modal" id="teacherblockdiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
				<div class="control-group">
					<div class="" id="ddd">
						<p><spring:message code="msgDueToUnsuccessfullAccClickOnIforgot"/></p>
					</div>
				</div>
		 	</div> 	
		 	<div class="modal-footer">
		 			<span><button class="btn btn-large btn-primary" onclick="hideDive()"><spring:message code="btnOk"/> <i class="icon"></i></button></span>
		 	</div>
		</div>
	  </div>
	</div>		
</c:if>
<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 	</table>
</div>
</div>
<script  type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>
<c:if test="${authmail}">
	<script>
		var divMsg="<div>${authmsg}</div>";
		addDialogContent("am","TeacherMatch",divMsg,"displayDiv");
		showDialog("am",actionForward,"560","200");
		function actionForward(){
			window.location.href="signin.do";
		}
	</script>

</c:if>
<script>
	$('#loadingDiv').hide();
	$("#teacherblockdiv").draggable();
	function divOn(){
		$('#loadingDiv').show();
	}
	document.getElementById("emailAddress").focus();
	function testYourSetUp() 
	{
		window.open("systemsetup.do");
	}
</script>
