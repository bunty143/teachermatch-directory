<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type='text/javascript' src="js/districtApprovalGroup.js?ver=${resourceMap['js/districtApprovalGroup.js']}"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JobCategoryAjax.js?ver=${resourceMap['JobCategoryAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAjax.js?ver=${resourceMap['DistrictAjax.ajax']}"></script>
<script type='text/javascript' src="js/jobcategory.js?ver=${resourceMap['js/jobcategory.js']}"></script>
<script type="text/javascript" src="dwr/interface/I4QuestionSetAjax.js?ver=${resourceMap['I4QuestionSetAjax']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictAssessmentAjax.js?ver=${resourceMap['DistrictAssessmentAjax']}"></script>
<script type="text/javascript" src="dwr/interface/HqJobCategoryAjax.js?ver=${resourceMap['HqJobCategoryAjax.Ajax']}"></script>

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        

function applyScrollOnTbl() {
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobCategoryTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[240,310,70,70,80,70,90], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
     });			
}
</script>
<style>
.input-group-addon
{
	background-color: #fff;
	border: none;
}
</style>
 
<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMngJoCate"/> </div>	
         </div>			
		 <div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="addNewJobCategory();addJobCategoryApprovalHideGroup();"><spring:message code="lnkAdJoCate"/></a>		
		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<!-- Div For Domain Grid   -->
<c:if test="${entityID eq 1}">	
<div  style="margin-top:-10px;" onkeypress="return chkForEnterSearchJobOrder(event);">
	   <div class="row top15">
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="<c:out value="${hide}"/>">
					<div class="col-sm-3 col-md-3">
						<label><spring:message code="lblEntityType"/> </label>
					    <select class="form-control" id="entityType" name="entityType" class="form-control" onchange="displayOrHideSearchBox();">
						 <c:if test="${not empty userMaster}">
						 	 <c:if test="${userMaster.entityType eq 1}">	
			         			<option value="1"   selected="selected" ><spring:message code="sltTM"/> </option>
								<option value="2"><spring:message code="lblDistrict"/> </option>
							</c:if>	
			        		<c:if test="${userMaster.entityType eq 2}">	
			         			<option value="2"  selected="selected" ><spring:message code="lblDistrict"/> </option>
							</c:if>
			              </c:if>
						</select>			
	              	</div>
		            <div  id="Searchbox" class="<c:out value="${hide}"/>">	         		 
		             		<div class="col-sm-7 col-md-7">
		             			<label id="captionDistrictOrSchool"><spring:message code="lblDistrict"/> </label>
			             		<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"/>
								<input type="hidden" id="districtIdFilter" value="${userMaster.districtId.districtId}"/>
								<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>		             
	          		</div>
					<div class="col-sm-2 col-md-2">	
					 <div class='mt25'>				
						<button class="btn btn-primary" style="width:100%" type="button" onclick="searchJobCat()"><spring:message code="headSearch"/> <i class="icon"></i></button>					
					 </div>
					</div>
        	  	</div>
       		 </form>
		</div>            
</div>
</c:if>

<div class="TableContent top15">        	
         <div class="table-responsive" id="jobCategoryGrid">          
         </div>            
</div> 

	<div class="row top5">
	  <div class="col-sm-8 col-md-8">			                         
 			<div class='divErrorMsg' id='errordiv' ></div>
	  </div>	
	</div>   
	<div id="jobCatDiv" class="hide" onkeypress="return chkForEnterSaveJobOrder(event);">
	   <c:if test="${entityID eq 2}">
	   	<input type="hidden" id="districtId" value="${districtId}"/>
	   </c:if>	
	   	<input type="hidden" name="jobcategoryId" id="jobcategoryId">
		<input type="hidden" id="entityID" value="${entityID}"/>
	  
		  <div class="row top3">
			   <c:if test="${entityID eq 1}">	
		          	<div  class="col-sm-6 col-md-6">
		          		<label class="mb0"><spring:message code="lblDistrict"/> </label>
		           		<input type="text" id="districtName" name="districtName"  class="form-control"
		           		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');getVVIDefaultFields();displayAllDistrictAssessment();getReferenceByDistrictList();getQQuestionSetByDistrictAndJobCategoryList();getJobCategoryByDistrictId();"
						onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');getVVIDefaultFields();displayAllDistrictAssessment();getReferenceByDistrictList();getQQuestionSetByDistrictAndJobCategoryList();getJobCategoryByDistrictId();"
						onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');getVVIDefaultFields();displayAllDistrictAssessment();getReferenceByDistrictList();getQQuestionSetByDistrictAndJobCategoryList();addJobCategoryApproval();"	/>
						<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
						<input type=hidden id="districtId"/>
		           	</div>
	           	</c:if>
	        </div>    	
	      <div class="row top3 mt10">      	
           	<div class="col-sm-6 col-md-6"><label class="mb0"><strong><spring:message code="lblJobCat"/> <span class="required">*</span></strong></label>
				<label class="radio" style="min-height:0px;margin-bottom:-6px;margin-top:6px;"><input type="radio" name="jobCatRadio" id="jobCatRadio1"/></label>
				<input type="text" name="jobcategory" id="jobcategory" maxlength="100" class="form-control" placeholder="" style="width:96%;margin-left:20px;">
			</div>
			<div class="col-sm-6 col-md-6"><label class="mb0"><strong><span class="required">&nbsp;</span></strong></label>
                <label class="radio" style="min-height:0px;margin-bottom:-6px;margin-top:6px;"><input type="radio" name="jobCatRadio" id="jobCatRadio2"/></label>
                <!--<input type="text" name="jobcategory" id="jobcategory" maxlength="100" class="form-control" placeholder="" style="width:82%;margin-left:20px;">-->
                <select id="parentJobCategoryId" name="parentJobCategoryId" class="form-control" style="width:96%;margin-left:20px;">
                    <option value="0"><spring:message code="optStrJobCat"/> </option>
                    <c:forEach var="jbCategory" items="${lstJobCategoryMasters}">
                        <option value="${jbCategory.jobCategoryId}">${jbCategory.jobCategoryName}</option>              
                    </c:forEach>
                </select>
            </div>            	
	      </div> 
	      <div class="row mt10 hide" id="subJobCatDiv">
	      	    <div class="col-sm-5 col-md-5"><label><strong>Sub Job Category<span class="required">*</span></strong></label>
                	<input type="text" name="subjobcategory" id="subjobcategory" maxlength="100" class="form-control" placeholder="" style="width:88%">             
            	</div>
            	<div class="col-sm-10 col-md-10 checkbox inline" style="padding-left: 15px;">
					<div class="left20">
						<input type="checkbox" name="attachDSPQFromJC" id="attachDSPQFromJC">
						<label><strong>Attach District Specific Mandatory Items from parent Job Category</strong></label>
					</div>
				</div>
				<div class="col-sm-10 col-md-10 checkbox inline" style="padding-left: 15px;">
					<div class="left20">
						<input type="checkbox" name="attachSLCFromJC" id="attachSLCFromJC">
						<label><strong>Attach Status Life Cycle from parent Job Category </strong></label>
					</div>
				</div><!--
	      	    <div class="col-sm-5 col-md-5 hide"  ><label><strong><spring:message code="msgSubJobCategory"/> <span class="required">*</span></strong></label>
                <input type="text" name="subjobcategory" id="subjobcategory" maxlength="100" class="form-control" placeholder="" style="width:88%">             
            </div>
	      --></div>	
	    
	    <!-- Select EPI Group Strat -->
	    <c:if test="${not empty userMaster}">
			<c:if test="${userMaster.entityType eq 1}">
				<div class="row mt10 hide epiGroupDiv">
					<div class="col-sm-6 col-md-6"><label class="mb0"><strong>Select EPI Group <span class="required">*</span></strong></label>
						<select id="assessmentGroupId" name="assessmentGroupId" class="form-control">
							<option value="0">Select EPI Group</option>
						</select>
					</div>
				</div>
			</c:if>
		</c:if>
		<c:if test="${not empty userMaster}">
			<c:if test="${userMaster.entityType eq 2}">
				<div class="row mt10 hide epiGroupDiv">
					<div class="col-sm-6 col-md-6"><label class="mb0"><strong>Select EPI Group <span class="required">*</span></strong></label>
						<select id="assessmentGroupId" name="assessmentGroupId" class="form-control" disabled="disabled">
							<option value="0">Select EPI Group</option>
						</select>
					</div>
				</div>
			</c:if>
		</c:if>
	    <!-- Select EPI Group End -->
	    
		<div class="row mt10 inline">
			<div class="col-sm-5 col-md-5 checkbox top5 inline mb0" style="padding-left: 15px;">
				<label><strong><spring:message code="lblFoExtCand"/> </strong></label></br>
				<div class="left20">
					<input type="checkbox" name="epicheck" id="epicheck" onclick="showHideEpiGroup();">
					<label><strong><spring:message code="lblEPIReq"/>  </strong></label>
				</div>
				
			</div>
		</div>
		
		<div class="row mt10">
			<div class="col-sm-12 col-md-12 checkbox inline mb0 top1">
			<label style="margin-left: -5px;"><strong><spring:message code="lblFoIntCand"/> </strong></label></br>
				<span class="col-sm-3 col-md-3 " id="epicheck2Span">
					<input type="checkbox" name="epicheck2" id="epicheck2" onclick="showHideEpiGroup();">
					<label><strong><spring:message code="lblEPIReq"/> </strong></label>
				</span>
				<span class="col-sm-3 col-md-3" id="offerPortfolioNeededjs">
					<input type="checkbox" name="offerPortfolioNeeded" id="offerPortfolioNeeded">
					<label><strong><spring:message code="lblOfrPortf"/></strong></label>
				</span>
				<span class="col-sm-3 col-md-3 " id="epicheck2Span">
					<input type="checkbox" name="offerDistrictSpecificItems" id="offerDistrictSpecificItems">
					<label><strong><spring:message code="lblOfDistSpeMandItm"/> </strong></label>
				</span>
				<span class="col-sm-3 col-md-3" id="epicheck2Span">
					<input type="checkbox" name="offerQualificationItems" id="offerQualificationItems">
					<label><strong><spring:message code="lblOfQualiItm"/> </strong></label>
				</span>
			</div>
		</div>		
		<div class="row mt0">
			<div class="col-sm-12 col-md-12 checkbox inline top1 mb0" >
					<span class="col-sm-4 col-md-4" id="epicheck2Span">
						<input type="checkbox" name="offerJSI" id="offerJSI">
						<label><strong><spring:message code="lblOfrJSI"/> </strong></label>
					</span>
					<span class="col-sm-4 col-md-4" id="" style="margin-left:-78px;">
						<input type="checkbox" name="offerVVIForInternal" id="offerVVIForInternal" >
						<label><spring:message code="lblOfrVVINtr"/> </label>
					</span>
			</div>
		</div>

		<div class="hide JSIRadionDiv" id="JSIRadionDiv">
			<div class="row checkbox inline top0 mb0" >
				<span class="col-sm-2 col-md-2" id="epicheck2Span">
					<input type="radio" name="offerJSIRadio" id="offerJSIRadioMaindatory">
					<label><strong><spring:message code="lblJSIMand"/> </strong></label>
				</span>
				<span class="col-sm-2 col-md-2" id="epicheck2Span">
					<input type="radio" name="offerJSIRadio" id="offerJSIRadioOptional">
					<label><strong><spring:message code="lblJSIOpt"/> </strong></label>
				</span>
			</div>	
		</div>


<!-- ********edit by ankit********* -->
		
		<div class="row mt10">
			<div class="col-sm-12 col-md-12 checkbox inline top1 mb0" >
			<label style="margin-left:-5px;"><strong><spring:message code="msgForInternalMovement"/> </strong></label></br>
				<span class="col-sm-3 col-md-3" id="imepicheck2Span">
					<input type="checkbox" name="imepicheck2" id="imepicheck2" onclick="showHideEpiGroup();">
					<label><strong> <spring:message code="lblEPIReq"/> </strong></label>
				</span>
				<span class="col-sm-3 col-md-3" id="imofferPortfolioNeededimjs">
					<input type="checkbox" name="imofferPortfolioNeeded" id="imofferPortfolioNeededim">
					<label><strong> <spring:message code="lblOfrPortf"/> </strong></label>
				</span>
				<span class="col-sm-3 col-md-3" id="imepicheck2Span">
					<input type="checkbox" name="imofferDistrictSpecificItems" id="imofferDistrictSpecificItems">
					<label><strong><spring:message code="lblOfDistSpeMandItm"/> </strong></label>
				</span>
				<span class="col-sm-3 col-md-3" id="imepicheck2Span">
					<input type="checkbox" name="imofferQualificationItems" id="imofferQualificationItems">
					<label><strong><spring:message code="lblOfQualiItm"/> </strong></label>
				</span>
			</div>
		</div>

		<!--<div class="row">
			<div class="col-sm-10 col-md-10 checkbox inline top1" >
				<span class="col-sm-4 col-md-4 left0" id="imepicheck2Span">
					<input type="checkbox" name="imofferDistrictSpecificItems" id="imofferDistrictSpecificItems">
					<label><strong><spring:message code="lblOfDistSpeMandItm"/> </strong></label>
				</span>
				<span class="col-sm-5 col-md-5" id="imepicheck2Span">
					<input type="checkbox" name="imofferQualificationItems" id="imofferQualificationItems">
					<label><strong><spring:message code="lblOfQualiItm"/> </strong></label>
				</span>
			</div>
		</div>
		
		--><div class="row mt0">
			<div class="col-sm-12 col-md-12 checkbox inline top1 mb0">
					<span class="col-sm-4 col-md-4" id="imepicheck2Span">
						<input type="checkbox" name="imofferJSI" id="imofferJSI">
						<label><strong> <spring:message code="lblOfrJSI"/> </strong></label>
					</span>
					
			</div>
		</div>
		
			<div class="row hide JSIRadionDiv" id="imJSIRadionDiv">
			<div class="col-sm-10 col-md-10 checkbox inline top1" >
				<span class="col-sm-4 col-md-4" id="imepicheck2Span">
					<input type="radio" name="imofferJSIRadio" id="imofferJSIRadioMaindatory">
					<label><strong><spring:message code="lblJSIMand"/> </strong></label>
				</span>
				<span class="col-sm-4 col-md-4" id="imepicheck2Span">
					<input type="radio" name="imofferJSIRadio" id="imofferJSIRadioOptional">
					<label><strong><spring:message code="lblJSIOpt"/> </strong></label>
				</span>
			</div>	
		</div>
		 
		 <!-- ********SWADESH********* -->
		<div class="row mt0">
			<div class="col-sm-12 col-md-12 checkbox inline top1 mb0">
						<input type="checkbox" name="autoRejectCheck" id="autoRejectCheck">
						<label><strong><spring:message code="lblAutoReject"/> </strong></label>
			</div>
		</div>
		 
		 <div class="row mt0">
			<div class="col-sm-12 col-md-12 checkbox inline top1 mb0 hide" id="autoRejectDiv">
						<label id="lblautoCheckSlider"> 
						 <iframe id="autoCheckSlider"  src="slideract.do?name=autoReject&tickInterval=10&max=100&swidth=250&svalue=0" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:420px;"></iframe>
						</label>
			</div>
		</div>
		 
		<!-- ********edit end by ankit********* -->
		
		<!-- Start Grouping Functionality  at Job Category Level -->
		<div id="groupApprovalDiv" style="margin-left: 15px;margin-bottom: 10px;">
		<div id="privilegeForDistrictErrorDiv"> </div>
		
		<div class="row">
			<div class="col-sm-5 col-md-5" >
				<label class="checkbox" style="padding-left:5px;">
					<input type="checkbox" id="approvalBeforeGoLive1" name="approvalBeforeGoLive1" onclick="enableDisableApprovals()">
					Approval process for positions prior to them going live
				</label>
			</div>
			
			<div class="col-sm-3 col-md-3" style="margin: 10px -57px 0px 0px;">
				Number of Approvals Needed
			</div>
			
			<div class="col-sm-2 col-md-2" >
				<select class="form-control" id="noOfApprovalNeeded" name="noOfApprovalNeeded" style="width:60px;padding-left:5px;">
					<option value="0">0</option>
					<c:forEach begin="0" end="9" varStatus="loop">
						<option value="${loop.count}">${loop.count}</option>
					</c:forEach>
				</select>
			</div>
		</div>
        <div class="row mt10" style='margin-top: 0px;' id="buildApprovalGroupsDivId" class="hideGroup">
			<div class="span11">
               	<label class="checkbox hideGroup">
               		<input type="checkbox" id="buildApprovalGroups" name="buildApprovalGroups">               
               		Build Approval Groups &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
               		<a href="javascript:void(0);" onclick="showAddGroup()" style="margin-left: 65%;"> +Add Member </a>
               	</label>
               </div>
		</div>
			
		<div id="groupMembersList" class="hideGroup" style="display: block;margin-left: 21px;margin-top: -11px;"> </div>
		
        <div class="bs-example bs-example-form hideGroup" id="addApprovalGroupAndMemberDiv">
        	<div class="row" style="margin-left: -20px;margin-top: 15px;">
           		<input type="hidden" value="1" id="noOfApproval" name="noOfApproval" class="form-control"  maxlength="4">
           		<div class="col-lg-3">
           			<label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Enter Group Name <font color="red">*</font> </label>
           			<div class="input-group">
           				<span class="input-group-addon">
							<input type="radio" name="groupRadioButtonId" id="groupRadioButtonId1" value="radioButton1" checked="checked" onclick="groupRadioButton()">
						</span>
						<input type="text" class="form-control" name="addGroupName" id="addGroupName">
					</div> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
               		<label style="margin-top: 11px; margin-left: 5px;">
               			<a href='javascript:void(0);' onclick="addGroupORMember()"> <spring:message code="lnkImD"/> </a> &nbsp;&nbsp;
               			<a href='javascript:void(0);' onclick="hideAddApprovalGroupAndMemberDiv()"> Cancel </a>
               		</label>
               	</div>
               	                		
               	<div class="col-lg-3">
               		<label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Select Group Name <font color="red">*</font> </label>
               		<div class="input-group">
               			<span class="input-group-addon">
               				<input type="radio" name="groupRadioButtonId" id="groupRadioButtonId2" value="radioButton2" onclick="groupRadioButton()">
               			</span>
               			<select class="form-control" id="groupName" name="groupName">
               			</select>
               		</div>
               	</div>
               	
               	<div class="col-lg-3">
               		<label> Select Members <font color="red">*</font> </label>
               		<div class="input-group">
               			<select multiple="multiple" class="form-control" id="groupMembers" name="groupMembers" style="height: 70px; min-width: 260px;">
               			</select>
               		</div>
               	</div>
        	</div>
		</div>
		</div> 
		<!-- End Grouping Functionality  at Job Category Level -->
		<div class="row" id="approvalBPGroupsDiv">
			<div class="mt5 left15">
				<div class="col-sm-12 col-md-12 checkbox inline top1" id="" >
						<input type="checkbox" name="approvalByPredefinedGroups" id="approvalByPredefinedGroups" >
						<label><strong>Approval By Predefined Groups</strong></label>
				</div>
			</div>
		</div>
			
		<div class="" id="offerVVIForinternalDiv">
			<div class="row mt10">
				<div class="col-sm-12 col-md-12 checkbox mb0 inline top1" id="" >
				<label style="margin-left: -5px;"><spring:message code="lblVVI"/> </label></br>
				<span class="col-sm-4 col-md-4" >
							<input type="checkbox" name="offerVirtualVideoInterview" id="offerVirtualVideoInterview" onclick="showOfferVVI();">
							<label><strong><spring:message code="lblOfrVVInr"/></strong></label>
							</span>
				</div>
			</div>
			<div class="hide left15" id="offerVVIDiv" >
				<div class="row mt0">
					<div class="col-sm-6 col-md-6" id="">
						<label id="captionDistrictOrSchool"><spring:message code="lblPlzSeltQuesSet"/></label>
	            		<input type="hidden" id="quesId" name="quesId" value=""> 
	             		<input type="text" id="quesName" name="districtName"  class="form-control"
	             		onfocus="getQuestionSetAutoComp(this, event, 'divTxtShowData1', 'quesName','quesId','');"
						onkeyup="getQuestionSetAutoComp(this, event, 'divTxtShowData1', 'quesName','quesId','');"
						onblur="hideDistrictMasterDiv(this,'quesId','divTxtShowData1');"	
						value=""/>
						<div id='divTxtShowData1' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData1','quesName')" class='result' ></div>
					</div>
				</div>
				<div class="row mt5 checkbox mb0">
					<div class="col-sm-10 col-md-10" >
								<input type="checkbox" name="wantScore" id="wantScore" onclick="showMarksDiv();"/>
								<label class="mb0"><spring:message code="lblWantSco"/></label>
					</div>
				</div>
				<div class="hide row left15" id="marksDiv">
					<div class="col-sm-2 col-md-2 mb10" id="">
						<label class="mb0"><spring:message code="lblMMarks"/></label>
						<input type="text" id="maxScoreForVVI" name="maxScoreForVVI" class="form-control" maxlength="3" onkeypress="return checkForInt(event)">
					</div>
				</div>
				<div class="row mt0 checkbox mb0">
					<div class="col-sm-10 col-md-10 " >
								<input type="checkbox" name="sendAutoVVILink" id="sendAutoVVILink" onclick="showLinkDiv();">
								<label><spring:message code="lblWntToSendInrLnkAuto"/>  </label>
								
					</div>
				</div>
				<div  class="hide" id="autolinkDiv">
					<div class="row">
						<div class="col-sm-11 col-md-11" >
									<div class="col-sm-5 col-md-5 top5" style="max-width:430px;">
										<label class="radio inline">
											<input type="radio"  name="jobCompletedVVILink" id="jobCompletedVVILink1" onclick="showHideVVILink(0)"/>
											<spring:message code="lblLnkWiBeSendAudoOnceAdminSet"/> 
										</label>
									</div>
									<div class="col-sm-3 col-md-3 top10" style="margin-left:-70px;">
										<div id="statusDiv"></div>
									</div>
									<div class="col-sm-3 col-md-3 top15">
										<label class="mb0">&nbsp;<spring:message code="lblStAgApp"/></label>
									</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-11 col-md-11">
							<div class="col-sm-5 col-md-5 top0" style="max-width:430px;">
								<label class="radio inline">
								   	<input type="radio"  name="jobCompletedVVILink" id="jobCompletedVVILink2"  onclick="showHideVVILink(1)"/>
									<spring:message code="lbljobCompletedVVILink" /> 
								</label>
							</div>
					    </div>
					</div>
				</div>
				<div class="row mt5">
					<div class="col-sm-3 col-md-3" id="">
						<label class="mb0"><spring:message code="msgTimeAllowedPerQuestion"/>  	</label>
						<input type="text" class="form-control" maxlength="4" id="timePerQues" onkeypress="return checkForInt(event)" />
					</div>
					<div class="col-sm-3 col-md-3" id="">
						<label class="mb0"><spring:message code="lblVVItrExInDays"/></label>
						<input type="text" class="form-control" maxlength="2" id="vviExpDays" onkeypress="return checkForInt(event)" />
					</div>
				</div>
			</div>
		</div>
		
		
			<div class="">
					<div class="row mt10">
						<div class="col-sm-12 col-md-12 checkbox inline top1 mb0" id="" >
						<label style="margin-left: -5px;"><spring:message code="msgOnlineAssessment"/> </label></br>
						<span class="col-sm-4 col-md-4" >
									<input type="checkbox" name="offerAssessmentInviteOnly" id="offerAssessmentInviteOnly" onclick="showOfferAMT();">
									<label><strong><spring:message code="lblOffAssToCndOnInvite"/></strong></label>
					</span>
			           				
						</div>
					</div>
					<div class="left15 hide" id="multiDisAdminDiv">
							   	<div class="row left5">
										<div class="col-sm-5 col-md-5" style="border: 0px solid red;">
											<label><spring:message code="lblSltDistAss"/></label>
										</div>
									</div>
									<div class="row left5 mb10">
										 <div class="col-sm-5 col-md-5" style="border: 0px solid red;">
											<div id="1stSchoolDiv">
												<select multiple id="lstDistrictAdmins" name="lstDistrictAdmins" class="form-control" style="height: 150px;" > 
												</select>
											</div>
										</div>		
										<div class="col-sm-1 col-md-1 left20-sm"> 
											<div class="span2"> <span id="addPop" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
											<div class="span2"> <span id="removePop" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
										</div>
										<div class="col-md-5 col-md-5" style="border: 0px solid green;">
											<div id="2ndSchoolDiv">
												<select multiple class="form-control" id="attachedDAList" name="attachedDAList" style="height: 150px;">
												</select>
											</div>
										</div>	
									</div>
							</div>
					    </div>
					<div class="row mt0">
						<div class="col-sm-12 col-md-12 checkbox inline mb0 top0" id="" >
							<span class="col-sm-4 col-md-4" >
								<input type="checkbox" name="schoolSelection" id="schoolSelection" value="1">
								<label class="mb0"><strong><spring:message code="lnkSchoolSelection"/></strong></label>
							</span>
				       	</div>
				    </div>
		<!--  Reference Check -->
		<div class="row mt10" >
			<div class="col-sm-10 col-md-10"> 
				<label class="mb0"><strong><spring:message code="msgEReferenceQuestionSet"/>  </strong></label></br>
			</div>
		</div>
		<div class="row ">
			<div id="reqNumberDiv">	
				<div class="row left15">
				 <div class="col-sm-6 col-md-6 " style="border: 0px solid red; margin-left: -15px;">
						<label class="mb0"><spring:message code="msgSetQuestionSet"/> </label>
						<select id="avlbList" name="avlbList" class="form-control"> 
						   <c:if test="${not empty avlbList}">
							<c:forEach var="avlst" items="${avlbList}" varStatus="status">
								<option value="${avlst.districtRequisitionId}">${avlst.requisitionNumber}</option>
							</c:forEach>
						</c:if>
					</select>
				</div>		
			</div>
			</div>
		</div>
		
		
		<!--  QQ Question set -->
		<div class="row mt10" >
			<div class="col-sm-10 col-md-10"> 
				<label class="mb0"><strong><spring:message code="msgQualificationQuestionSet"/> </strong></label></br>
			</div>
		</div>
		<div class="row ">
			<div id="qqSetDiv">	
				<div class="row left15 mt0">
				 <div class="col-sm-6 col-md-6 " style="border: 0px solid red; margin-left: -15px;">
						<label class="mb0"><spring:message code="msgSetQualificationQuestionSet"/></label>
						<select id="qqAvlbList" name="qqAvlbList" class="form-control"> 
						   
					    </select>
				</div>
				</div>
				<div class="row left15 mt5">
				<div class="col-sm-6 col-md-6 " style="border: 0px solid red; margin-left: -15px;">
						<label class="mb0">Set Qualification Question Set to Job Category For Onboarding</label>
						<select id="qqAvlbListForOnboard" name="qqAvlbListForOnboard" class="form-control"> 
						   
					    </select>
				</div>	
						
			</div>
			</div>
		</div>
		
		
		 <div class="row mt10">
			<div class="col-sm-5 col-md-5" id="jsiDiv">
			<label class="mb0"><strong><spring:message code="msgJobSpecificInventory"/>  </strong></label></br>
				<label class="mb0"><spring:message code="lblAttJSI"/></label>
				<form id='frmJsiUpload' enctype='multipart/form-data' method='post' target='uploadFrame' action='jobCategorywiseJsiUploadServlet.do' class="form-inline">

					<span>
					 	<input type="hidden" id="districtHiddenId" name="districtHiddenId" value="${districtId}"/>
						<input type="hidden"  id="jsiFileName" name="jsiFileName" value="" />
						<input name="assessmentDocument" id="assessmentDocument" type="file">
					</span>

				</form>
				<div id="removeJsi" name="removeJsi" style="display: none">
					<div style='width:20px;padding-left: 0px;' class='col-sm-2 col-md-2'  id="divJsiName"></div>
					<div class="col-sm-1 col-md-1">
					<a href="javascript:void(0)" onclick="removeJsi()"><spring:message code="lnkRemo"/></a>
					</div>
					<div class="col-sm-1 col-md-1 left20 top5">
						 <a href="#" id="iconpophover" rel="tooltip" data-original-title="Remove Jsi">
						 <img src="images/qua-icon.png" width="15" height="15" alt="">
					     </a>
					</div>
				</div>
				<iframe src="" id="ifrmJsi" width="100%" height="480px" style="display: none;">
					</iframe> 
		    </div>
		</div>
		
				
		
		 <div class="row top18">
		  	<div class="col-sm-5 col-md-5">
		     	<button onclick="saveJobCategory();" class="btn btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
		     	 &nbsp;<a href="javascript:void(0);" onclick="cancelJobCateDiv()"><spring:message code="btnClr"/></a>  
		    </div>
		</div>
		
		<div style="display:none; z-index: 5000;" id="loadingDiv" >
	     <table  align="center" >
	 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
		</table>
	    </div>
</div>

<script>
	displayOrHideSearchBox();
	displayJobCategories();
	getReferenceByDistrictList();
	//getReferenceByDistrictAndJobCategoryList();
	$('#iconpophover').tooltip();
</script>
<!-- add by ankit -->
<script>
		$('#imiconpophover').tooltip();
</script>
<!--add end by ankit -->

<script type="text/javascript">
	$('#addPop').click(function() {
		if ($('#lstDistrictAdmins option:selected').val() != null) {
			 $('#lstDistrictAdmins option:selected').remove().appendTo('#attachedDAList');
			 $("#attachedDAList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
	         $("#attachedDAList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	 }
	});
	
	$('#removePop').click(function() {
	       if ($('#attachedDAList option:selected').val() != null) {
	             $('#attachedDAList option:selected').remove().appendTo('#lstDistrictAdmins');
	           //  sortSelectList();
	             $("#attachedDAList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
	             $("#lstDistrictAdmins").attr('selectedIndex', '-1').addAttr("selected");
	             
	}
	});
	displayAllDistrictAssessment();
</script>

<input type="hidden" id="ApprovalDistrictId" name="ApprovalDistrictId">
<input type="hidden" id="ApprovalJobCategoryId" name="ApprovalJobCategoryId" value="">

<input type="hidden" id="ApprovalNoOfApprovalNeeded" name="ApprovalNoOfApprovalNeeded" value="">
<input type="hidden" id="ApprovalBuildApprovalGroup" name="ApprovalBuildApprovalGroup" value="">
<input type="hidden" id="ApprovalApprovalBeforeGoLive" name="ApprovalApprovalBeforeGoLive" value="">
<input type="hidden" id="ApprovalGroupFromEditFlag" name="ApprovalApprovalGroupFlag" value="false">

<script>

var districtMaster = "";
var noOfApprovalNeeded="";
var approvalBeforeGoLive="";
var buildApprovalGroup="";

var DistM = "${districtMaster}";
if(DistM!=null && DistM!="")
{
	districtMaster = "${districtMaster.districtId}";
	noOfApprovalNeeded = "${districtMaster.noOfApprovalNeeded}";
	approvalBeforeGoLive = "${districtMaster.approvalBeforeGoLive}";
	buildApprovalGroup = "${districtMaster.buildApprovalGroup}";
}

$("#ApprovalDistrictId").val(districtMaster);
$("#ApprovalJobCategoryId").val("-1");
$("#ApprovalNoOfApprovalNeeded").val(noOfApprovalNeeded);
$("#ApprovalBuildApprovalGroup").val(approvalBeforeGoLive);
$("#ApprovalApprovalBeforeGoLive").val(buildApprovalGroup);

$(document).ready(function(){
	//addJobCategoryApproval();
});

function fileContainsVirusDiv(msg)
{
	$('#loadingDiv').hide();
	$('#virusDivId .modal-body').html(msg);
	$('#virusDivId').modal('show');
}
</script>

<div class="modal hide" id="virusDivId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showDSPQDiv();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeMemberConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeMemberGroupId="" removeMemberId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Do you want to remove this Member?
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeMember()">Ok <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal">Cancel</button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>

<div class="modal hide" id="removeGroupConfirmation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false" removeGroupId="">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					Do you want to remove this Group?
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="removeGroup()">Ok <i class="icon"></i></button>&nbsp;
				<button class="btn" data-dismiss="modal">Cancel</button> &nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>



<!--	 Modal for displaying job Category  history  -->
<div  class="modal hide"  id="jobCategoryHistoryModalId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 850px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="hideById('jobCategoryHistoryModalId')">x</button>
		<h3 id="myModalLabel">Job Category History</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divJobHisTxt">
			
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
<!-- 		 <button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;-->
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick="hideById('jobCategoryHistoryModalId')">Close</button> 		
 	</div>
   </div>
 </div>
</div>