<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/AssessmentAjax.js?ver=${resourceMap['AssessmentAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/assessment.js?ver=${resourceMap['js/assessment.js']}"></script>
<script type="text/javascript">

var questionTypeJSON=${json};
 // alert(findSelected(3));
function findSelected(idd)
{
	for (x in questionTypeJSON)
  	{
	  if(idd==x)
	  return questionTypeJSON[x];
  	}
}
</script>
<style>
.myButton {
    background:url(./images/download.jpg) no-repeat;
    cursor:pointer;
    width: 30px;
    height: 30px;
    border: none;
}

.progress1 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar1 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent1 { position:absolute; display:inline-block; top:0px;bottom:0px; left:48%; }

.progress2 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar2 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent2 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress3 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar3 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent3 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress4 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar4 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent4 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress5 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar5 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent5 { position:absolute; display:inline-block; top:0px; left:48%; }

.progress6 { position:relative; width:200px; border: 1px solid #ddd; padding: 1px; border-radius: 3px; }
.bar6 { background-color: #B4F5B4; width:0%; height:15px; border-radius: 3px; }
.percent6 { position:absolute; display:inline-block; top:0px; left:48%; }

</style>

<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static"  >	
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 		
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="redirectToURL()">OK</button></span> 		
 	</div>
</div>
</div>
</div>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Add/Edit Question</div>	
         </div>			 
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>


<div class="row">
<div class="col-sm-8 col-md-8 mt10">

<c:set var="assType" value="Base" />
<c:set var="weightageVal" value="" />
		<c:if test="${assessmentSection.assessmentDetail.assessmentType==2}">
			<c:set var="assType" value="Job Specific" />
			<c:set var="weightageVal" value="0" />
		</c:if>
		<c:if test="${assessmentSection.assessmentDetail.assessmentType==3}">
			<c:set var="assType" value="Smart Practices" />
			<c:set var="weightageVal" value="" />
		</c:if>
		<c:if test="${assessmentSection.assessmentDetail.assessmentType==4}">
			<c:set var="assType" value="IPI" />
			<c:set var="weightageVal" value="" />
		</c:if>
<a href="assessment.do">Inventory</a>: ${assessmentSection.assessmentDetail.assessmentName} (${assType})
<br/> 
<a href="assessmentquestions.do?assessmentId=${assessmentSection.assessmentDetail.assessmentId}&sectionId=${assessmentSection.sectionId}">Section</a>: ${assessmentSection.sectionName}
<br/>
<div id="divUId"></div>      
</div>
</div>


<input type="hidden" id="assessmentId" value="${param.assessmentId}">
<div  id="divAssessmentRow" style="display: block;" onkeypress="chkForEnterSaveAssessmentQuestion(event);">
		<div class="row">
		<div class="col-sm-8 col-md-8">		                         
		 			<div class='divErrorMsg' id='errordiv' ></div>
		 			
		</div>
		</div>
		<c:if test="${assessmentSection.assessmentDetail.assessmentType==1 || assessmentSection.assessmentDetail.assessmentType==3 || assessmentSection.assessmentDetail.assessmentType==4}">
 		 <div class="row mt10">
             <div class="col-sm-3 col-md-3">
				<label>Domain<span class="required">*</span></label>
		         <select  class="form-control" name="domainMaster" id="domainMaster" onchange="getCompetencies(this.value)">
		         <option value="0">Select Domain</option>
					<c:forEach items="${domainMasters}" var="domainMaster">
						<option id="${domainMaster.domainId}" value="${domainMaster.domainId}" >${domainMaster.domainName}</option>
					</c:forEach>
					</select>
              </div>
              
             <div class="col-sm-3 col-md-3">
	             <div class="span6"><label>Competency<span class="required">*</span></label>
	            		<select class="form-control " name="competencyMaster" id="competencyMaster" onchange="getObjectives(this.value)">
	            			<option value="0">Select Competency</option>
						</select>
				</div>
			 </div>

			 <div class="col-sm-3 col-md-3">
	             <div class="span6"><label>Objective<span class="required">*</span></label>
	            		<select class="form-control " name="objectiveMaster" id="objectiveMaster">
	            			<option value="">Select Objective</option>
						</select>
				  </div>
			 </div>
          </div>

			<div class="row mt10">
	         	<div class="col-sm-3 col-md-3">
					<label>Stage One<span class="required">*</span></label>
			         	<select  class="form-control" name="stageOneStatus" id="stageOneStatus">
			         		<option value="0">Select Stage One</option>
							<c:forEach items="${stageOneStatus}" var="stageOneStatus">
								<option id="${stageOneStatus.stage1Id}" value="${stageOneStatus.stage1Id}" >${stageOneStatus.name}</option>
							</c:forEach>
						</select>
		         </div>

		         <div class="col-sm-3 col-md-3">
					<label>Stage Two<span class="required">*</span></label>
						<select  class="form-control" name="stageTwoStatus" id="stageTwoStatus">
			         		<option value="0">Select Stage Two</option>
			         		<c:forEach items="${stageTwoStatus}" var="stageTwoStatus">
			         			<option id="${stageTwoStatus.stage2Id}" value="${stageTwoStatus.stage2Id}">${stageTwoStatus.name}</option>
			         		</c:forEach>
			         	</select>
				 </div>

				 <div class="col-sm-3 col-md-3">
					<label>Stage Three<span class="required">*</span></label>
						<select  class="form-control" name="stageThreeStatus" id="stageThreeStatus">
			         		<option value="0">Select Stage Three</option>
			         		<c:forEach items="${stageThreeStatus}" var="stageThreeStatus">
			         			<option id="${stageThreeStatus.stage3Id}" value="${stageThreeStatus.stage3Id}">${stageThreeStatus.name}</option>
			         		</c:forEach>
			         	</select>
				 </div>
				 

	        </div>

		</c:if>
		<div class="row" style="padding-top: 13px;">
        	<div class="col-sm-3 col-md-3" style="width: 240px;"><label><strong>Item Code</strong></label>
				<input type="text" name="itemCode" id="itemCode" maxlength="50" class="form-control">
			</div>
			<c:if test="${assessmentSection.assessmentDetail.assessmentType==1 || assessmentSection.assessmentDetail.assessmentType==3 || assessmentSection.assessmentDetail.assessmentType==4}">
				<div class="col-sm-3 col-md-3" style="width: 240px;"><label><strong>Is Experimental</strong></label><br>
					<input class="span0" type="checkbox" name="isExperimental" id="isExperimental" class="form-control">
				</div>
			</c:if>
		</div>
		
		<div class="row" style="padding-top: 13px;">
		<div class="col-sm-7 col-md-7" id="assQuestion"><label><strong>Question<span class="required">*</span></strong></label>
		<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document,
		 please ensure to paste that text first into a simple text editor such as Notepad to remove the 
		 special characters and then copy and paste it into the field below. Otherwise, the text 
		 directly cut and pasted from a web page and/or a Microsoft document may not display properly to the reader)
		 </label>
		<textarea class="form-control" name="question" id="question" maxlength="2500" rows="5" onkeyup="return chkLenOfTextArea(this,2500);" onblur="return chkLenOfTextArea(this,2500);"></textarea>
		</div>
		</div>

		<div class="row" style="padding-top: 13px;">
		<div class="col-sm-3 col-md-3" style="width: 240px;">
		<label><strong>Question Type<span class="required">*</span></strong></label>
		<select  class="form-control" name="questionTypeMaster" id="questionTypeMaster" onchange="getQuestionOptions(this.value)">
		      <option value="0">Select Question Type</option>
				<c:forEach items="${questionTypeMasters}" var="questionTypeMaster" >	
					<option id="${questionTypeMaster.questionTypeId}" value="${questionTypeMaster.questionTypeId}" >${questionTypeMaster.questionType}</option>
				</c:forEach>	
		</select>
		</div>  
		<div class="col-sm-3 col-md-3" style="width: 240px;"><label><strong>Weightage</strong></label>
		<input type="text" name="questionWeightage" id="questionWeightage" onkeypress='return checkForDecimalTwo(event)' value="${weightageVal}" maxlength="6" class="form-control" placeholder="">
		<input type="hidden" name="maxMarks" id="maxMarks" class="span2"/>
		</div>
		</div> 
		
		<div class="row" id="row0" style="display: none">
		<div class="left15" style="float: left;width: 205px;">
		<div class="spanfit1"><label><strong>Options&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Score&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rank</strong></label>
		</div>
		</div>
	    <div class="" style="float: left;width: 205px;margin-left: 35px;">
		<div class="spanfit1"><label><strong>&nbsp;&nbsp;&nbsp;&nbsp;Options&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Score&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rank</strong></label>
		</div>
		</div>
		<div class="clearfix"></div>
		</div>
		
		<div class="row" id="row1" style="display: none">		
			<div class="spanfit1 left15 pull-left" >
			    <div class="top7 pull-left">1</div>
				<div class="pull-left" style="width: 110px;margin-left: 5px;">
				<input type="hidden" name="hid1" id="hid1"/><input type="text" name="opt1" id="opt1" class="spanfit form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="score1" id="score1" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="rank1" onkeypress='return checkForIntUpto6(event)' id="rank1" maxlength="1" class="form-control" placeholder="">
				</div>
				<div class="clearfix"></div>
			</div>			
		    <div class="spanfit1 left30 pull-left">	
				<div  class="top7 pull-left">&nbsp;&nbsp;2</div>
				<div class="pull-left" style="width: 110px;margin-left: 5px;">
				<input type="hidden" name="hid2" id="hid2"/><input type="text" name="opt2" id="opt2" class="spanfit form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">				
				<input type="text" name="score2" id="score2" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="rank2" onkeypress='return checkForIntUpto6(event)' id="rank2" maxlength="1" class="form-control" placeholder="">
				</div>
				<div class="clearfix"></div>
		   </div>	
		   <div class="clearfix"></div>	
		</div>		
		
		<div class="row top5" id="row2" style="display: none">			
			<div class="spanfit1 left15 pull-left" >
			    <div class="top7 pull-left">3</div>
				<div class="pull-left" style="width: 110px;margin-left: 5px;">
				<input type="hidden" name="hid3" id="hid3"/><input type="text" name="opt3" id="opt3" class="spanfit form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="score3" id="score3" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
			    <input type="text" name="rank3" onkeypress='return checkForIntUpto6(event)' id="rank3" maxlength="1" class="form-control" placeholder="">
				</div>
				<div class="clearfix"></div>
			</div>			
		    <div class="spanfit1 left30 pull-left" >
			    <div class="top7 pull-left">&nbsp;&nbsp;4</div>
				<div class="pull-left" style="width: 110px;margin-left: 5px;">
				<input type="hidden" name="hid4" id="hid4"/><input type="text" name="opt4" id="opt4" class="spanfit form-control" maxlength="750" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
				<input type="text" name="score4" id="score4" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
				</div>
				<div class="pull-left" style="width: 40px;margin-left: 5px;">
			    <input type="text" name="rank4" onkeypress='return checkForIntUpto6(event)' id="rank4" maxlength="1" class="form-control" placeholder="">
				</div>
				<div class="clearfix"></div>
			</div>
		 <div class="clearfix"></div>	
		</div>	
		
		
		
		
		<div class="row top5" id="row3" style="display: none">	
			<div class="spanfit1 left15 pull-left" >
				    <div class="top7 pull-left">5</div>
					<div class="pull-left" style="width: 110px;margin-left: 5px;">
					<input type="hidden" name="hid5" id="hid5"/><input type="text" name="opt5" id="opt5" class="spanfit form-control" maxlength="750" placeholder="">
					</div>
					<div class="pull-left" style="width: 40px;margin-left: 5px;">
					<input type="text" name="score5" id="score5" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
					</div>
					<div class="pull-left" style="width: 40px;margin-left: 5px;">
				   <input type="text" name="rank5" onkeypress='return checkForIntUpto6(event)' id="rank5" maxlength="1" class="form-control" placeholder="">
					</div>
					<div class="clearfix"></div>
			</div>		
			<div class="spanfit1 left30 pull-left" >
					    <div class="top7 pull-left">&nbsp;&nbsp;6</div>
						<div class="pull-left" style="width: 110px;margin-left: 5px;">
						<input type="hidden" name="hid6" id="hid6"/><input type="text" name="opt6" id="opt6" class="spanfit form-control" maxlength="750" placeholder="">
						</div>
						<div class="pull-left" style="width: 40px;margin-left: 5px;">
					    <input type="text" name="score6" id="score6" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
						</div>
						<div class="pull-left" style="width: 40px;margin-left: 5px;">
					   <input type="text" name="rank6" onkeypress='return checkForIntUpto6(event)' id="rank6" maxlength="1" class="form-control" placeholder="">
						</div>
						<div class="clearfix"></div>
			</div>
 			<div class="clearfix"></div>	
		</div>	
	
	
		<!-- Start 4th Row -->
			
			<div class="row top5" id="row4" style="display: none">	
			<div class="spanfit1 left15 pull-left" >
				    <div class="top7 pull-left">7</div>
					<div class="pull-left" style="width: 110px;margin-left: 5px;">
					<input type="hidden" name="hid7" id="hid7"/><input type="text" name="opt7" id="opt7" class="spanfit form-control" maxlength="750" placeholder="">
					</div>
					<div class="pull-left" style="width: 40px;margin-left: 5px;">
					<input type="text" name="score7" id="score7" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
					</div>
					<div class="pull-left" style="width: 40px;margin-left: 5px;">
				   <input type="text" name="rank7" onkeypress='return checkForIntUpto6(event)' id="rank7" maxlength="1" class="form-control" placeholder="">
					</div>
					<div class="clearfix"></div>
			</div>		
			<div class="spanfit1 left30 pull-left" >
					    <div class="top7 pull-left">&nbsp;&nbsp;8</div>
						<div class="pull-left" style="width: 110px;margin-left: 5px;">
						<input type="hidden" name="hid8" id="hid8"/><input type="text" name="opt8" id="opt8" class="spanfit form-control" maxlength="750" placeholder="">
						</div>
						<div class="pull-left" style="width: 40px;margin-left: 5px;">
					    <input type="text" name="score8" id="score8" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
						</div>
						<div class="pull-left" style="width: 40px;margin-left: 5px;">
					   <input type="text" name="rank8" onkeypress='return checkForIntUpto6(event)' id="rank8" maxlength="1" class="form-control" placeholder="">
						</div>
						<div class="clearfix"></div>
			</div>
 			<div class="clearfix"></div>	
		</div>
			
		<!-- End 4th Row -->
			
			<div class="row top5" id="row5" style="display: none">	
			<div class="spanfit1 left15 pull-left" >
				    <div class="top7 pull-left">9</div>
					<div class="pull-left" style="width: 110px;margin-left: 5px;">
					<input type="hidden" name="hid9" id="hid9"/><input type="text" name="opt9" id="opt9" class="spanfit form-control" maxlength="750" placeholder="">
					</div>
					<div class="pull-left" style="width: 40px;margin-left: 5px;">
					<input type="text" name="score9" id="score9" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
					</div>
					<div class="pull-left" style="width: 40px;margin-left: 5px;">
				   <input type="text" name="rank9" onkeypress='return checkForIntUpto6(event)' id="rank9" maxlength="1" class="form-control" placeholder="">
					</div>
					<div class="clearfix"></div>
			</div>		
			<div class="spanfit1 left30 pull-left" >
					    <div class="top7 pull-left">10</div>
						<div class="pull-left" style="width: 110px;margin-left: 5px;">
						<input type="hidden" name="hid10" id="hid10"/><input type="text" name="opt10" id="opt10" class="spanfit form-control" maxlength="750" placeholder="">
						</div>
						<div class="pull-left" style="width: 40px;margin-left: 5px;">
					    <input type="text" name="score10" id="score10" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="form-control" placeholder="">
						</div>
						<div class="pull-left" style="width: 40px;margin-left: 5px;">
					   <input type="text" name="rank10" onkeypress='return checkForIntUpto6(event)' id="rank10" maxlength="1" class="form-control" placeholder="">
						</div>
						<div class="clearfix"></div>
			</div>
 			<div class="clearfix"></div>	
		</div>
	
		
	
		<!-- End 5th Row -->
		
	    <div id='imageType' style="display: none;">
		<div><label><strong>Options&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Score</strong></label></div>
		<div>
		<form id="myForm1" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td> 1 </td>
		<td style="width: 225px;"><div id='fileDiv1'><input type="file" name="file1" id="file1" /></div><div id='img1'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp1' type="submit" class="myButton" value="     " onclick='handleSubmit(1)'/>
		<a href="javascript:void(0);"><img id='rm1' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(1);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid100" id="hid100"/><input type="hidden" name="opt100" id="opt100" ><input type="text" name="score100" id="score100" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress1" id='p1' style="display: none;"><div class="bar1"></div ><div class="percent1">0%</div > </div> 
		</td>
		<td>
		<div id="status1"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm2" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td> 2 </td>
		<td style="width: 225px;"><div id='fileDiv2'><input type="file" name="file2" id="file2" /></div><div id='img2'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp2' type="submit" class="myButton" value="     " onclick='handleSubmit(2)'/>
		<a href="javascript:void(0);"><img id='rm2' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(2);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid101" id="hid101"/><input type="hidden" name="opt101" id="opt101" ><input type="text" name="score101" id="score101" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress2" id='p2' style="display: none;"><div class="bar2"></div ><div class="percent2">0%</div > </div> 
		</td>
		<td>
		<div id="status2"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm3" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td> 3 </td>
		<td style="width: 225px;"><div id='fileDiv3'><input type="file" name="file3" id="file3" /></div><div id='img3'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp3' type="submit" class="myButton" value="     " onclick='handleSubmit(3)'/>
		<a href="javascript:void(0);"><img id='rm3' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(3);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid102" id="hid102"/><input type="hidden" name="opt102" id="opt102" ><input type="text" name="score102" id="score102" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress3" id='p3' style="display: none;"><div class="bar3"></div ><div class="percent3">0%</div > </div> 
		</td>
		<td>
		<div id="status3"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm4" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td> 4 </td>
		<td style="width: 225px;"><div id='fileDiv4'><input type="file" name="file4" id="file4" /></div><div id='img4'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp4' type="submit" class="myButton" value="     " onclick='handleSubmit(4)'/>
		<a href="javascript:void(0);"><img id='rm4' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(4);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid103" id="hid103"/><input type="hidden" name="opt103" id="opt103" ><input type="text" name="score103" id="score103" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress4" id='p4' style="display: none;"><div class="bar4"></div ><div class="percent4">0%</div > </div> 
		</td>
		<td>
		<div id="status4"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm5" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td> 5 </td>
		<td style="width: 225px;"><div id='fileDiv5'><input type="file" name="file5" id="file5" /></div><div id='img5'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp5' type="submit" class="myButton" value="     " onclick='handleSubmit(5)'/>
		<a href="javascript:void(0);"><img id='rm5' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(5);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid104" id="hid104"/><input type="hidden" name="opt104" id="opt104" ><input type="text" name="score104" id="score104" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress5" id='p5' style="display: none;"><div class="bar5"></div ><div class="percent5">0%</div > </div> 
		</td>
		<td>
		<div id="status5"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		
		<div>
		<form id="myForm6" action="uploaderServlet.do" method="post">
		<table>
		<tr>
		<td > 6 </td>
		<td style="width: 225px;"><div id='fileDiv6'><input type="file" name="file6" id="file6" /></div><div id='img6'></div></td>
		<td>
		<input data-original-title='Upload' rel='tooltip' id='imgTp6' type="submit" class="myButton" value="     " onclick='handleSubmit(6)'/>
		<a href="javascript:void(0);"><img id='rm6' height="20" width="20" style="display: none;" alt="" src="images/can-icon.png" onclick="removeImage(6);"></a>
		</td>
		<td>
		    <input type="hidden" name="hid105" id="hid105"/><input type="hidden" name="opt105" id="opt105" ><input type="text" name="score105" id="score105" onkeypress='return checkForDecimalTwo(event)'  maxlength="6" class="span1" placeholder="">
		</td>
		<td> 
		    &nbsp;<div class="progress6" id='p6' style="display: none;"><div class="bar6"></div ><div class="percent6">0%</div > </div> 
		</td>
		<td>
		<div id="status6"></div>
		</td>
		</tr>
		</table>
		</form>
		</div>
		</div>		
<!-- Image Type end-->
</div>

<div class="row" style="padding-top: 13px;">
<div class="col-sm-7 col-md-7" id="assInstruction"><label><strong>Instructions</strong></label>
<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document,
 please ensure to paste that text first into a simple text editor such as Notepad to remove the 
 special characters and then copy and paste it into the field below. Otherwise, the text 
 directly cut and pasted from a web page and/or a Microsoft document may not display properly to the reader)
 </label>
<textarea class="form-control" name="questionInstruction" id="questionInstruction" maxlength="2500" rows="4" onkeyup="return chkLenOfTextArea(this,2500);" onblur="return chkLenOfTextArea(this,2500);"></textarea>
</div>
</div>

 <div class="row top20" style="display: block;" id="divManage">
  	<div class="col-sm-6 col-md-5">
  		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
     		<button id="saveQues"  onclick="saveQuestion(${assessmentSection.assessmentDetail.assessmentId},${assessmentSection.sectionId},'save')" class="btn btn-large btn-primary"><strong>Save <i class="icon"></i></strong></button>
     	</c:if> 
     	<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
     	<button onclick="saveQuestion(${assessmentSection.assessmentDetail.assessmentId},${assessmentSection.sectionId},'continue')" class="btn btn-large btn-primary"><strong>Save & Continue <i class="icon"></i></strong></button>
     	</c:if>
     	&nbsp;<a href="javascript:void(0);" onclick="cancelQuestion(${assessmentSection.assessmentDetail.assessmentId},${assessmentSection.sectionId})">Cancel</a> 
     </div>
  </div>


<input type="hidden" id="assessmentQuestionId" value="${param.assessmentQuestionId}"/>
<input type="hidden" id="questionId" value=""/>

 <script language="javascript">

function handleSubmit(ff)
{
     (function() {
    
			var bar = $('.bar'+ff);
			var percent = $('.percent'+ff);
			var status = $('#status'+ff);
			
			  
			$('#myForm'+ff).ajaxForm({
			    beforeSubmit: function(arr) { 
				   // return false;// to cancel submit     
				  //$('#message2show').html(dwr.util.toDescriptiveString(arr[0].value.size,5));
					//$('#myModal2').modal('show');
					var fileName = arr[0].value.name;
					if(fileName==undefined || fileName=="")
					{
						//alert("Please select photo for option "+ff);
						$('#errordiv1').html("&#149; Please select photo of option "+ff+" <br>");
						$('#errordiv1').show();
						return false;
					}else
					{
						var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();


						var fileSize=0;		
						if ($.browser.msie==true)
						{	
							fileSize = 0;	   
						}
						else
						{	
							fileSize = arr[0].value.size;
						}
						
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png'))
						{
							$('#errordiv1').html("&#149; Please select Acceptable image formats include GIF, PNG, and JPEG files <br>");
							$('#errordiv1').show();
							return false;
				
						}
						else if(fileSize>=10485760)
						{		
							$('#errordiv1').html("&#149; File size must be less then 10mb. <br>");
							$('#errordiv1').show();
							return false;
						}
						
						$('#p'+ff).show();
					}
				            
				},
			    beforeSend: function() {
			        status.empty();
			        var percentVal = '0%';
			        bar.width(percentVal)
			        percent.html(percentVal);
			        
			    },
			    uploadProgress: function(event, position, total, percentComplete) {
			        var percentVal = percentComplete + '%';
			        bar.width(percentVal)
			        percent.html(percentVal);
					//console.log(percentVal, position, total);
			    },
			    success: function(data) {
			        var percentVal = '100%';
			        bar.width(percentVal)
			        percent.html(percentVal);
			       // alert(data);
			        $('#img'+ff).html("<a target='_blank' href='"+data.split("###")[1]+"'><img src='"+data.split("###")[1]+"' height='100' width='180'></a>");
			        $('#p'+ff).hide();
			    },
				complete: function(xhr) {
					status.html("Uploaded: <a target='_blank' href='"+xhr.responseText.split("###")[1]+"'>"+xhr.responseText.split("###")[1]+"</a> ("+xhr.responseText.split("###")[0]+" bytes)");
					$('#fileDiv'+ff).hide();
					$('#errordiv1').hide();
					$('#imgTp'+ff).hide();
					$('#rm'+ff).show();
					$('#opt'+(ff+6)).val(xhr.responseText.split("###")[2]);
				},
				 error: function(){}
			}); 

})();       

}

function removeImage(ff)
{
	$('#fileDiv'+ff).html("<input type='file' name='file"+ff+"' id='file"+ff+"' />");
	$('#fileDiv'+ff).show();
	$('#img'+ff).html("");
	$('#imgTp'+ff).show("");
	$('#rm'+ff).hide("");
	$('#status'+ff).html("");
	$('#opt'+(ff+6)).val("");
	
}
  function processJson(data) { 
    // 'data' is the json object returned from the server 
    alert(data); 
}

// pre-submit callback 
function showRequest() { 
  // alert("dd");
    return true; 
}
		for(j=1;j<=6;j++)
		$('#imgTp'+j).tooltip();
		
 var assessQuesOpts=${assessQues}
 
setAssessmentQuestion(${param.assessmentQuestionId});
	$('#domainMaster').focus();
	
	$(document).ready(function() {
  		//$(".jqte").width(538);
  		$('#assQuestion').find(".jqte").width(538);
  		$('#assInstruction').find(".jqte").width(538);
	}) 

</script>


<!-- <script src="http://malsup.github.com/jquery.form.js"></script> --> 
<script src="js/jquery.form.js"></script> 

    
