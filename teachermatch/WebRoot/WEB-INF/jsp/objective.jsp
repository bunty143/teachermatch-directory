<!-- @Author: Gagan 
 * @Discription: view of edit Objective Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/AssessmentAjax.js?ver=${resourceMap['']}"></script>
<script type="text/javascript" src="dwr/interface/ObjectiveAjax.js?ver=${resourceMap['']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/assessment.js?ver=${resourceMap['js/assessment.js']}"></script>
<script type='text/javascript' src="js/objective.js?ver=${resourceMap['js/objective.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#objectiveTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[240,240,263,68,126], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
 </script>
 <div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Manage Objectives</div>	
         </div>			
		 <div class="pull-right add-employment1">
			<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
				<a href="javascript:void(0);" onclick="return addObjective()">+Add Objective</a>
			</c:if>
		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<div class="TableContent top15">        	
         <div class="table-responsive" id="objectiveGrid">          
         </div>            
</div> 

	<div  id="divObjectiveForm" style="display: none;" class="mt10 span16" onkeypress="return chkForEnterSaveObjective(event);" >
		<div class="row">
			<div class="col-sm-8 col-md-8">
			<div class="control-group">			                         
	 			<div class='divErrorMsg' id='errordiv' ></div>
			</div>
			</div>
		</div>
		
		<form  id="frmObjective" onsubmit="return false;">
			<div class="row">
			
				<div class="col-sm-3 col-md-3">
					<label>Domain<span class="required">*</span></label>
		         	<select  class="form-control" name="domainMaster" id="domainMaster" onchange="getCompetencies(this.value)">
		         		<option value="0">Select Domain</option>
						<c:forEach items="${domainMasters}" var="domainMaster">	
							<option id="${domainMaster.domainId}" value="${domainMaster.domainId}" >${domainMaster.domainName}</option>
						</c:forEach>	
					</select>	
              	</div>
              	
				<div class="col-sm-3 col-md-3">	          
	             	<div class="" id="sss"><label>Competency<span class="required">*</span></label>
	            		<div id="competencyListByDomainId">
		            		<select class="form-control " name="competencyMaster" id="competencyMaster">
		            			<option value="0">Select Competency</option>
		            			<c:forEach items="${competencyMasters}" var="competencyMaster">	
									<option id="${competencyMaster.competencyId}" value="${competencyMaster.competencyId}" >${competencyMaster.competencyName}</option>
								</c:forEach>
							</select>
						</div>	
					</div>			
              </div>
          </div>
		
		<div class="row">
			<div class="col-sm-6 col-md-6"><label><strong>Objective Name<span class="required">*</span></strong></label>
				<input type="hidden" name="objectiveId" id="objectiveId" >
				<input type="text" name="objectiveName" id="objectiveName" maxlength="150" class="form-control" placeholder="">
			</div>
			<div class="col-sm-2 col-md-2"><label><strong><spring:message code="lblObjectiveUId"/><span class="required">*</span></strong></label>
				<input type="text" name="objectiveUId" id="objectiveUId" maxlength="1" class="form-control" onKeyPress='return isAlphabetsKey(event)' placeholder="">
			</div>
		</div>

		<div class="row ">
			<div class="col-sm-4 col-md-4">
				<label><strong>Status<span class="required">*</span></strong></label>
			</div>            
		</div>
		
		<div class="row left5">
			<div class="col-sm-1 col-md-1">
			  	<label class=" span1 radio p0" style="margin-top: 0px;">
					<input type="radio"  value="A" id="objectiveStatus1"  name="objectiveStatus" checked="checked" >Active
				</label>
			</div>
			<div class="col-sm-1 col-md-1">
		        <label class=" span2 radio p0" style="margin-top: 0px;">
		        	<input type="radio" value="I" id="objectiveStatus2" name="objectiveStatus" >Inactive
				</label>
		     </div>
		</div>

		 <div class="row top10" style="display: none;" id="divManage">
		  	<div class="col-sm-4 col-md-4">
		  		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
		     		<button onclick="saveObjective();" class="btn btn-large btn-primary"><strong>Save <i class="icon"></i></strong></button>
		     	</c:if>
		     	 &nbsp;<a href="javascript:void(0);" onclick="clearObjective();">Cancel</a>   
		    </div>
		 </div>		  
		
		<div class="row">
		<div class="col-sm-4 col-md-4 idone" id="divDone" style="display: none;">
			<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
				<a href="javascript:void(0);" onclick="saveObjective();"><spring:message code="lnkImD"/></a>
			</c:if>
			 &nbsp;<a href="javascript:void(0);" onclick="clearObjective();"><spring:message code="lnkCancel"/></a>
		</div>
		</div>
	</form>
</div>           



<script type="text/javascript">
	DisplayObjective();
</script>
                 
                 
                 

<!-- @Author: Gagan 
 * @Discription: view of edit Objective Page.
 -->