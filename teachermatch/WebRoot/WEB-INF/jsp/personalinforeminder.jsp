<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/PortfolioReminderAjax.js?ver=${resourceMap['PortfolioReminderAjax.Ajax']}"></script>

<script type="text/javascript" src="js/personalinforeminder.js?ver=${resourceMap['js/personalinforeminder.js']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resouceMap['DistrictPortfolioConfigAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFExperiences.js?ver=${resouceMap['PFExperiences.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/MyAjax.js?ver=${resourceMap['MyAjax.js']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherInfotAjax.js?ver=${resourceMap['TeacherInfoAjax.js']}"></script>
<script type="text/javascript" src="dwr/interface/PFAcademics.js?ver=${resourceMap['PFAcademics.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/PFCertifications.js?ver=${resourceMap['PFCertifications.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js?ver=${resouceMap['DWRAutoComplete.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>

<script type='text/javascript' src="js/dwrajax.js?ver=${resourceMap['js/dwrajax.js']}" ></script>
<script type="text/javascript" src="dwr/interface/StateAjax.js?ver=${resourceMap['StateAjax.js']}"></script>
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resouceMap['DashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/teacher/pfAutoCompCertificate.js?ver=${resourceMap['js/teacher/pfAutoCompCertificate.js']}"></script>

<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />
<link rel="stylesheet" type="text/css" href="css/style3.css?ver=${resourceMap['css/base.css']}" />
<link rel="stylesheet" type="text/css" href="css/style.css?ver=${resourceMap['css/base.css']}" /> 

<script src="calender/js/jscal2_dspq.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" href="css/dialogbox.css" />
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type="text/javascript">//<![CDATA[
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
</script>
<style><!--
.hide { display: none; }
.inline { display: inline; }
.nowrap { white-space: nowrap }
label, input, button, select, textarea { font-size: 11px; font-weight: normal;line-height: 20px;}
.divwidth {
  float: left;
  width: 40px;
}
.radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
   float:none;
   margin-left: -19px; 
}
</style>

<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:30px;">
         <div style="float: left;">
         	<img src="images/personalinformation.png" width="41" height="41" alt="">
         </div>
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Portfolio Update</div>	
         </div>
		<div style="clear: both;"></div>
	    <div class="centerline"></div>
</div>
<input type="hidden" id="academicsStatus" name="academicsStatus" value="${portfolioReminders.academics}"/>
<input type="hidden" id="firstname_conf" name="firstname_conf" value="${portfolioReminders.firstName}"/>
<input type="hidden" id="lastname_conf" name="lastname_conf" value="${portfolioReminders.lastName}"/>
<input type="hidden" id="ssn_config" name="ssn_config" value="${portfolioReminders.SSN}"/>
<input type="hidden" id="dateOfBirth" name="dateOfBirth" value="${portfolioReminders.dob}"/>
<input type="hidden" id="race" name="race" value="${portfolioReminders.raceId}"/>
<input type="hidden" id="genderId" name="genderId" value="${portfolioReminders.genderId}"/>
<input type="hidden" id="ethnicorigin" name="ethnicorigin" value="${portfolioReminders.ethnicOriginId}"/>
<input type="hidden" id="ethinicity" name="ethinicity" value="${portfolioReminders.ethnicityId}"/>
<input type="hidden" id="address" name="address" value="${portfolioReminders.addressLine1}"/>
<input type="hidden" id="phoneNumber" name="phoneNumber" value="${portfolioReminders.phoneNumber}"/>
<input type="hidden" id="veteran" name="veteran" value="${portfolioReminders.veteran}"/>
<input type="hidden" id="retirementnumber" name="retirementnumber" value="${portfolioReminders.retirementnumber}"/>
<input type="hidden" id="formeremployee" name="formeremployee" value="${portfolioReminders.formeremployee}"/>
<input type="hidden" id="honors_conf" name="honors_conf" value="${portfolioReminders.honors}"/>
<input type="hidden" id="videoLink_conf" name="videoLink_conf" value="${portfolioReminders.videoLink}"/>
<input type="hidden" id="certification_conf" name="certification_conf" value="${portfolioReminders.certificationPortfolio}"/>
<input type="hidden" id="tfaAffiliate_conf" name="tfaAffiliate_conf" value="${portfolioReminders.tfaAffiliatePortfolio}"/>
<input type="hidden" id="employment_conf" name="employment_conf" value="${portfolioReminders.employmentHistory}"/>
<input type="hidden" id="involvement_conf" name="involvement_conf" value="${portfolioReminders.involvement}"/>
<input type="hidden" id="additionalDocuments_conf" name="additionalDocuments_conf" value="${portfolioReminders.additionalDocuments}"/>
<input type="hidden" id="references_conf" name="references_conf" value="${portfolioReminders.referencesPortfolio}"/>
<input type="hidden" id="acedminTranscript_conf" name="acedminTranscript_conf" value="${portfolioReminders.academicTranscript}"/>
<input type="hidden" id="dspq_conf" name="dspq_conf" value="${dSPQ}"/>

<input type="hidden" id="references0_conf" name="references0_conf" value="${portfolioReminders.referenceDspq}"/>
<input type="hidden" id="honors_conf" name="honors_conf" value="${portfolioReminders.honors}"/>
<input type="hidden" id="certification0_conf" name="certification0_conf" value="${portfolioReminders.certification}"/>
<input type="hidden" id="employment0_conf" name="employment0_conf" value="${portfolioReminders.employment}"/>
<input type="hidden" id="tfaAffiliate0_conf" name="tfaAffiliate0_conf" value="${portfolioReminders.tfaAffiliate}"/>
<input type="hidden" id="wst0_config" name="ws	t0_config" value="${portfolioReminders.willingAsSubstituteTeacher}"/>
<input type="hidden" id="academicDspq_config" name="academicDspq_config" value="${portfolioReminders.academicDspq}"/>

<input type="hidden" id="wst_config" name="wst_config" value="${portfolioReminders.substituteTeacher}"/>
<input type="hidden" id="resume_config" name="resume_config" value="${portfolioReminders.resume}"/>
<input type="hidden" id="exp_config" name="exp_config" value="${portfolioReminders.expCertTeacherTraining}"/>
<input type="hidden" id="nbc_config" name="nbc_config" value="${portfolioReminders.nationalBoardCert}"/>
<input type="hidden" id="affidavit_config" name="affidavit_config" value="${portfolioReminders.affidavit}"/>
<input type="hidden" id="generalKnowledge_config" name="generalKnowledge_config" value="${portfolioReminders.generalKnowledgeExam}"/>
<input type="hidden" id="subjectAreaExam_config" name="subjectAreaExam_config" value="${portfolioReminders.subjectAreaExam}"/>

<input type="hidden" id="DiststatId" name="DiststatId" value="${DiststatId}"/>
<input type="hidden" id="InfostatId" name="InfostatId" value="${InfostatId}"/>
<input type="hidden" id="portfolioStatus" name="portfolioStatus" value=""/>
<input type="hidden" id="jobId" name="jobId" value="${jobId}"/>
<input type="hidden" id="isaffilatedstatushiddenId" name="isaffilatedstatushiddenId" value="0"/>
<input type="hidden" id="isMiami" name="isMiami" value="${isMiami}"/>
<input type="hidden" id="academicTranscriptFlag" value="${districtPortfolioConfig.academicTranscript}">
<input type="hidden" id="isPrinciplePhiladelphia" value="${isPrinciplePhiladelphia}">
<input type="hidden" id="rId" value="${teacherpersonalinfo.raceId}">

<input type="hidden" id="formeremployee_config" value="">
<input type="hidden" id="personalinfo_config" value="">
<input type="hidden" id="dateOfBirth_config" value="">
<input type="hidden" id="address_config" value="">
<input type="hidden" id="veteran_config" value="">
<input type="hidden" id="retireNo_config" value="">
<input type="hidden" id="phoneNumber_config" value="">
<input type="hidden" id="additionDocumentId" value="">

<input type="hidden" id="isJobAssessment" name="isJobAssessment" value=""/>
<input type="hidden" id="updateFlag" name="updateFlag" />
<input type="hidden" id="isnontj" name="isnontj" value=""/>
<input type="hidden" id="isSchoolSupportPhiladelphia" value=""/>
<input type="hidden" id="jobcategoryDsp" value=""/>
<input type="hidden" id="jobTitleFeild" value=""/>


<input type="hidden" id="allowNext" value="1">
<input type="hidden" id="ethnicOrigin_config" value="">
<input type="hidden" id="ethinicity_config" value="">
<input type="hidden" id="employment_config" value="">
<input type="hidden" id="yaxis" value="0">
<input type="hidden" id="gender_config" value="">
<input type="hidden" id="videoLink_config" value="">
<input type="hidden" id="involvement_config" value="">
<input type="hidden" id="honors_config" value="">
<input type="hidden" id="displayGKAndSubject" value="">
<input type="hidden" id="displayPassFailGK" value="">
<input type="hidden" id="displayPassFailSubject" value="">
<input type="hidden" id="IsSIForMiami" value="">
<input type="hidden" id="isItvtForMiami" value="">
<input type="hidden" id="threadCount" value="0">
<input type="hidden" id="returnThreadCount" value="0">
<input type="hidden" id="callForwardCount" value="-2">
<input type="hidden" id="threadCount_aca" value="0">
<input type="hidden" id="threadCount_cert" value="0">
<input type="hidden" id="threadCount_emp" value="0">
<input type="hidden" id="threadCount_ref" value="0">
<input type="hidden" id="threadCount_subarea" value="0">
<input type="hidden" id="threadCount_adddoc" value="0">
<input type="hidden" id="dSPQuestions_config" value="">


<input type="hidden" id="tFA_config" value="">
<input type="hidden" id="wst_config" value="">
<input type="hidden" id="resume_config" value="">
<input type="hidden" id="phone_config" value="">
<input type="hidden" id="exp_config" value="">
<input type="hidden" id="nbc_config" value="">
<input type="hidden" id="affidavit_config" value="">
<input type="hidden" id="race_config" value="">
<input type="hidden" id="generalKnowledge_config" value="">
<input type="hidden" id="subjectAreaExam_config" value="">
<input type="hidden" id="additionalDocuments_config" value="">

<iframe src="" id="ifrmResume" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmTrans" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmCert" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmRef" width="100%" height="480px" style="display: none;"></iframe> 

<!--<script>
$('textarea').jqte();
</script>-->
<script type="text/javascript">
function applyScrollOnTbl_Acad()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#academicGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
        colratio:[185,130,136,144,136,125,100], //144,130,136,185,136,146,97
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
function applyScrollOnTbl_Certificat()
{
	//alert("01");
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[300,115,100,200,145,100], //125,90,400,120,80
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnInvlvement()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#involvementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[250,250,175,176,111], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnHon()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#honorsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[862,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnTbl_EmployeementHistry()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#employeementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
          colratio:[200,200,150,110,200,105],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
        colratio:[440,416,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
function applyScrollOnTblEleRef()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#eleReferencesGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
          colratio:[150,90,120,240,80,80,70,127], //170,100,120,180,80,70,110
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_SubjectAreas()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#subjectAreasGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,200,300,157,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblVideoLinks()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#videoLinktblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
          colratio:[650,200,112],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

 </script>







<c:if test="${districtMaster ne null}">
	<input type="hidden" id="districtIdForDSPQ" value="${districtMaster.districtId}">
</c:if>

<c:if test="${districtMaster eq null}">
	<input type="hidden" id="districtIdForDSPQ" > 
</c:if>
<input type="hidden" id="teacherIdForDSPQ" value="">



<!-- Start ... call Dynamic Portfolio -->
<input type="hidden" id="txtDistrictPortfolioConfig" name="txtDistrictPortfolioConfig" value="0"/>
<input type="hidden" id="gridNameFlag" name="gridNameFlag"/>
<input type="hidden" id="txtCandidateType" name="txtCandidateType" value="${isAffilated}"/>

<div class="accordion" id="accordion5">
<div id="collapseOne" style="height:auto;" >
	<div class="accordion-inner" style="margin-top: 2px;">
	
	<div class="row left10" style="margin-bottom:10px;">
	<div class="col-sm-12 col-md-12">
		
	<div class='divErrorMsg'  id="errPersonalInfoAndSSN"></div>
	<div class='divErrorMsg' style="display: none;" id="errDOB"></div>
    <div class="divErrorMsg hide"  id="errSSN"></div>
    <div class="divErrorMsg hide" id="errAddress1"></div>
    <div class="divErrorMsg hide" id="errCountry"></div>
    <div class="divErrorMsg hide" id="errZip"></div>
	<div class="divErrorMsg hide" id="errState"></div>
	<div class="divErrorMsg hide" id="errCity"></div>
    <div class='divErrorMsg' id='errordiv_bottomPart_phone'></div>
    <div class='divErrorMsg' style="display: none;" id="errRetireNo"></div>
    <div class='divErrorMsg' style="display: none;" id="errFormerEmployee"></div>
    <div class='divErrorMsg' style="display: none;" id="errEthnicOrigin"></div>
    <div class='divErrorMsg' style="display: none;" id="errEthinicity"></div>
    <div class='divErrorMsg' style="display: none;" id="errRace"></div>
    <div class='divErrorMsg' style="display: none;" id="errGender"></div>
    <div class='divErrorMsg' style="display: none;" id="errExpSalary"></div>
    <div class='divErrorMsg' id='errordiv_bottomPart_TFA' style="display: none;"></div>
    <div class='divErrorMsg' id='errordiv_bottomPart_tfaOptions' style="display: none;"></div>
    <div class='divErrorMsg' id='errordiv_bottomPart_wst' style="display: none;"></div>
    <div class='divErrorMsg' id='errordiv_bottomPart_resume' style="display: block;"></div>
    <div class='divErrorMsg' style="display: none;" id="errExpCTT"></div>
    <div class='divErrorMsg' style="display: none;"id="errNBCY"></div>
    
	
	</div>
	</div>


		 <div class="row left10" style="margin-bottom:10px;">
			<div class="col-sm-12 col-md-12">
				<!-- 
				<div class="span4" style="color: #007AB4;font-family: 'Century Gothic','Open Sans', sans-serif;font-weight: bold;">
							Please review and ensure that the following information is correct and up-to-date:<BR/>
							 - Phone Number<BR/>
							 - Certification Expiration Date<BR/>
							 - High Needs School question
				</div>
				 -->
				<div class="table-responsive" id="messageApplicantManagement" style="color: #007AB4;font-family: 'Century Gothic','Open Sans', sans-serif;font-weight: bold;">	
		    	</div>
			</div>
			<!--<div class="col-sm-12 col-md-12">
				<div class="span4 portfolio_Subheading"><spring:message code="headPrsnlInfo" /></div>
			</div>
			<div class="col-sm-3 col-md-3">
				<label><strong><spring:message code="lblSalutation" /> </strong></label>					
				<select id="salutation_pi" class="form-control" >
					<option value="0" <c:if test="${teacherpersonalinfo.salutation eq 0}">selected="selected" </c:if>><spring:message code="msgSelectSalutation" /></option>   
					<option  value="4" <c:if test="${teacherpersonalinfo.salutation eq 4}">selected="selected" </c:if>><spring:message code="optDr" /></option>
					<option  value="3" <c:if test="${teacherpersonalinfo.salutation eq 3}">selected="selected" </c:if>><spring:message code="optMiss" /></option>
					<option  value="2" <c:if test="${teacherpersonalinfo.salutation eq 2}">selected="selected" </c:if>><spring:message code="optMr" /></option>
					<option  value="1" <c:if test="${teacherpersonalinfo.salutation eq 1}">selected="selected" </c:if>><spring:message code="optMrs" /></option>
					<option  value="5" <c:if test="${teacherpersonalinfo.salutation eq 5}">selected="selected" </c:if>><spring:message code="optMs" /></option>											
				</select>
			</div>-->
			<input type="hidden" id="teacherId_pi" />
			<div id="firstNameDiv">											
			<div class="col-sm-4 col-md-4" style="margin-bottom:10px;">
				<label><strong><spring:message code="lblFname" /><span class="required">*</span></strong></label>
				<input type="text" id="firstName_pi" class="form-control" maxlength="50"  value="${teacherpersonalinfo.firstName}"/>
			</div>
			</div>
			
			<!--<div class="col-sm-4 col-md-4" style="margin-bottom:10px;">
				<label><strong><spring:message code="lblMiddleName" /></strong></label>
				<input type="text" id="middleName_pi" class="form-control" maxlength="50" <c:if test="${teacherpersonalinfo.middleName ne '' or teacherpersonalinfo.middleName ne null}"> value="${teacherpersonalinfo.middleName}" </c:if> />
			</div>
			
			--><div id="lastNameDiv">				
			<div class="col-sm-3 col-md-3" style="margin-bottom:10px;">
				<label><strong><spring:message code="lblLname" /><span class="required">*</span></strong></label>
				<input type="text" id="lastName_pi" class="form-control" maxlength="50" value="${teacherpersonalinfo.lastName}"/>
			</div>
			</div>
		
			<div class="col-sm-3 col-md-3 hide" id="anotherNameDiv">
				<div>
					<label><strong><spring:message code="lblAname" /> <a href="#" id="iconpophoverAnotherName" rel="tooltip" data-original-title="If you  previously worked here, did you work under another name?"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
					<input type="text" id="anotherName" class="form-control" />
				</div>
			</div>
		
	
	<div id="ssnDiv">
	<div class="col-sm-4 col-md-4" >
		
			<div>
				<label  style="width: 200px;"><strong><spring:message code="lblSSNumber" /><span class="required">*</span></strong></label>
				<input type="text" id="ssn_pi" class="form-control" maxlength="9" onkeypress="return checkForInt(event);" onclick="chkDistrict();" value="${ssn}" />
			</div>
		</div>
	</div>
	
	 </div>
	
	<div class="row left10" style="margin-bottom:15px;">
	<input type="hidden" id="dob" name="dob" class="span2" maxlength="4"/>
	<div  id="dobDiv">
			<div class="col-sm-12 col-md-12" style="margin-bottom:-8px;">
				<label><strong class="portfolio_Subheading"><spring:message code="lblDOB1" /></strong></label>
			</div>
				
			<div class="col-sm-3 col-md-3" >
			<label><strong><spring:message code="lblMonth" /><span class="required optionalCss">*</span></strong></label>
			<select class="form-control" id="dobMonth" name="dobMonth" >
				<option value="0"><spring:message code="optStrMonth" /></option>
				<c:forEach items="${lstMonth}" var="dobMonth" varStatus="dobStatus">
				<c:set var="selected" value=""></c:set>					
			        		<c:if test="${dobStatus.count eq month}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
				
					<option value="${dobStatus.count}" ${selected}>${dobMonth}</option>
				</c:forEach>
			</select>
			</div>
					
			<div class="col-sm-4 col-md-4">
			<label><strong><spring:message code="lblDay1" /><span class="required optionalCss">*</span></strong></label>
			<!-- <input type="text" id="dobDay" name="dobDay" class="span1" maxlength="2" onkeypress="return checkForInt(event);" /> -->
	
			<select class="form-control" id="dobDay" name="dobDay">
				<option value="0"><spring:message code="lblSelectDay" /></option>
				<c:forEach items="${lstDays}" var="dobDay" varStatus="dobStatusDays">
				<c:set var="selected" value=""></c:set>					
			        		<c:if test="${dobStatusDays.count eq day}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
					<option value="${dobStatusDays.count}" ${selected}>${dobDay}</option>
				</c:forEach>
			</select>					
			</div>
					
			<div class="col-sm-4 col-md-4">
			<label><strong><spring:message code="optYear" /><span class="required optionalCss">*</span></strong></label>
			<input type="text" id="dobYear" name="dobYear" class="form-control" maxlength="4" onkeypress="return checkForInt(event);" onblur="return validateDOBYearForDSPQ();" value="${year}" />
			</div>
				
	</div>
</div>

<!-- Start Address -->
<div class="row left10" style="margin-bottom:15px;">
	<div id="addressDiv">
			
				<div class="col-sm-2 col-md-2">
					<div class="portfolio_Subheading"><spring:message code="lblAdd" /></div>
				</div>
			
						   
			 <div class="row left10">
			    <div class="col-sm-3 col-md-3">
			    </div>
			 </div>  
			 			   
			   	
	    		<div class="col-sm-7 col-md-7" style="margin-bottom:10px;">
					<label><strong><spring:message code="lblAddrL1" /><span class="required">*</span></strong></label>
					<input type="text" name="addressLine1" id="addressLine1" class="form-control" maxlength="50"  value="${teacherpersonalinfo.addressLine1}" />
				</div>
	   		   
	   
		   	
			  	<div class="col-sm-7 col-md-7" style="margin-bottom:10px;">
					<label><strong><spring:message code="lblAddrL2" /></strong></label>
					<input type="text" name="addressLine2" id="addressLine2" class="form-control" maxlength="50" value="${teacherpersonalinfo.addressLine2}" />
				</div>
		     
   			   
			 <div class="row">
			    <div class="col-sm-6 col-md-6">
			   
			    </div>
			 </div> 
			 
			
			
					<div class="col-sm-3 col-md-3" style="margin-bottom:10px;">
						<label><strong><spring:message code="lblCnty" /><span class="required">*</span></strong></label>
						<select id="countryId" class="form-control" onchange="getStateByCountryForDspq('dspq')">   
							<option value=""><spring:message code="lblSelectCountry" /></option>
							<c:forEach items="${listCountryMaster}" var="ctry">
								<c:set var="selected" value=""></c:set>	
								<c:if test="${ctry.countryId eq teacherpersonalinfo.countryId.countryId}">	        			
				         			<c:set var="selected" value="selected"></c:set>	         			
				        		</c:if>				
								<option id="ctry${ctry.countryId}" value="${ctry.countryId}" ${selected} >${ctry.name}</option>
							</c:forEach>				
						</select>
			           		
					</div>
			
		
	  			<div class="col-sm-4 col-md-4" style="margin-bottom:10px;">
					<label><strong><spring:message code="lblZepCode" /><span class="required">*</span></strong></label>
					<input type="text" Class="form-control" id="zipCode" onblur="changeCityStateByZipForDSPQ()" maxlength="10"  value="${teacherpersonalinfo.zipCode}" />
				</div>
				<div class="col-sm-4 col-md-4" id="divUSAState" style="display: none;margin-bottom:10px;">
					<label><strong><spring:message code="lblSt" /><span class="required">*</span></strong></label>
					<select id="stateIdForDSPQ" Class="form-control" onchange="getCityListByStateForDSPQMain()">
					<!--<select id="stateIdForDSPQ" Class="form-control" >
					
					--><option value="" id="slst"><spring:message code="optSltSt" /></option>
						<c:forEach items="${listStateMaster}" var="st">
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${st.stateId eq teacherpersonalinfo.stateId.stateId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>									
							<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
						</c:forEach> 			
					
					</select>
			
				</div>
				<div class="col-sm-3 col-md-3" id="divUSACity" style="display: none;" style="margin-bottom:10px;">
					<label><strong><spring:message code="lblCity" /><span class="required">*</span></strong></label>
					<select id="cityIdForDSPQ"  name="cityIdForDSPQ" Class="form-control">   
						<option  id="slcty" value=""><spring:message code="lblSelectCity" /></option>
						<c:forEach items="${listCityMasters}" var="city">	
							<c:set var="selected" value=""></c:set>					
			        		<c:if test="${city.cityId eq teacherpersonalinfo.cityId.cityId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>							
							<option id="ct${city.cityId}" value="${city.cityId}" ${selected} >${city.cityName}</option>
						</c:forEach>	
					</select>
				</div>			
				<div class="col-sm-3 col-md-3" id="divotherstate" style="display: none;">
					<label><strong><spring:message code="lblSt/Cnty" /><span class="required">*</span></strong></label>
					<input type="text" id="otherState" name="otherState" class="form-control" maxlength="50"/>
				</div>
				<div class="col-sm-3 col-md-3" id="divothercity" style="display: none;">
					<label><strong><spring:message code="lblCity" /><span class="required">*</span></strong></label>
					<input type="text" id="otherCity" name="otherCity" class="form-control" maxlength="50"/>
				</div>
		  
	   </div>
	</div>
	
<!-- End Address -->

<!-- Phone  -->
<div id="phoneDiv">	
			<div class="row left10">
				<div class="col-sm-3 col-md-3" style="width:950px;">
					<div class="span4 portfolio_Subheading"><spring:message code="lblPhoneNo" /></div>
				</div>
		    </div>	
	       <div class="row left10">	
			<div class="col-sm-6 col-md-6">
				
			</div>
		   </div> 
		   <div class="row left10">	
			   <div class="col-sm-3 col-md-3" style="margin-bottom:10px;">				
					<label><strong><spring:message code="headPhone" /><span class="required">*</span></strong></label>
					 <%-- 
					<input type="text" name="phoneNumber" id="phoneNumber" class="span3" maxlength="20"  value="${teacherpersonalinfo.phoneNumber}"/>
					--%>
					<div>
						<div style="float: left;"><input type="text" name="phoneNumber1" id="phoneNumber1" class="form-control" maxlength="3"  value="${phone1}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
						<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber2" id="phoneNumber2" class="form-control" maxlength="3"  value="${phone2}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
						<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber3" id="phoneNumber3" class="form-control" maxlength="4"  value="${phone3}" style='width:66px; padding-left:10px;'' onkeypress="return checkForInt(event);"/></div>
					</div>
					
					<!--<div>
						<div style="float: left;"><input type="text" name="phoneNumber1" id="phoneNumber1" class="form-control" maxlength="3"  value="${ph1}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
						<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber2" id="phoneNumber2" class="form-control" maxlength="3"  value="${ph2}" onkeypress="return checkForInt(event);" style='width:56px; padding-left:10px;'/></div><div style="float: left;color: #cccccc;margin-left: 4px;margin-top: 6px">-</div>
						<div style="float: left;margin-left: 5px;"><input type="text" name="phoneNumber3" id="phoneNumber3" class="form-control" maxlength="4"  value="${ph3}" style='width:66px; padding-left:10px;' onkeypress="return checkForInt(event);"/></div>
					</div>
				
			   --></div>		
	     </div>
</div>	
<!-- End Phone -->
<div id="veteranDiv" >
      <div class="row left10">
		<div class="col-sm-12 col-md-12 top5">
			<label class="span15">
				<strong><spring:message code="lblDp_commons4" /><span class="required">*</span></strong>
			</label>
		</div>
	   
	   <div class="row left20">		
			 <div class="col-sm-1 col-md-1 radio top1">
		    	<input type="radio" id="vt1" value="1" name="veteran" onclick="showdistrictspeciFicVeteran();" <c:if test="${teacherpersonalinfo.isVateran eq 1}">checked</c:if> >
		    	<spring:message code="lblYes" />
	        </div>
	        </br>				
			<div class="col-sm-1 col-md-1 radio" style="margin-top:-18px;">
				<input type="radio" id="vt2" value="0" name="veteran" onclick="showdistrictspeciFicVeteran();" <c:if test="${teacherpersonalinfo.isVateran eq 0 or teacherpersonalinfo.isVateran eq null}">checked</c:if>>
				 <spring:message code="lblNo" />					
	        </div>	       
        </div>
        <div class="col-sm-12 col-md-12 top5 hide" id="vetranOptionDiv" class="hide">
        	
			<table>
				<tbody>
					<tr>
						<td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="1"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;"><font size="2" color="red"><spring:message code="lblDp_commonsA" /> </font> <spring:message code="msgDp_commons5" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="2"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsB" /> </font> <spring:message code="msgDp_commons6" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="3"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsC" /> </font> <spring:message code="msgDp_commons7" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="4"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsD" /> </font> <spring:message code="msgDpcommo15" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 40px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="5"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsE" /> </font> <spring:message code="msgDp_commons8" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="6"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsF" /> </font> <spring:message code="msgDp_commons9" /> </td>
					</tr>
					<tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranOptS" name="veteranOptS" value="7"></div></td>
						<td style="border:0px;background-color: transparent;padding-left:5px;" ><font size="2" color="red"><spring:message code="lblDp_commonsG" /> </font> <spring:message code="msgDp_commons10" /> </td>
					</tr>
				</tbody>
			</table>
			
			<div class="top10"><spring:message code="msgDp_commons11" /></div>
            <table>
	            <tbody>
	               <tr>
	               <td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranPreference1" name="veteranPreference1" value="1"></div> </td>
	               <td style="border:0px;background-color: transparent;padding-left:5px;" ><spring:message code="lblYes" /> </td></tr>
	               <tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranPreference1" name="veteranPreference1" value="2"></div> 
	               </td><td style="border:0px;background-color: transparent;padding-left:5px;"><spring:message code="lblNo" /> </td>
	               </tr>
	               </tbody>
	        </table>
	        <div class="top10"><spring:message code="msgDp_commons12" /></div>
			<table>
			    <tbody>
			    <tr>
			        <td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranPreference2" name="veteranPreference2" value="1"></div> </td>
			        <td style="border:0px;background-color: transparent;padding-left:5px;" ><spring:message code="lblYes" /> </td>
			    </tr>
			    <tr><td style="border:0px;background-color: transparent;"><div style="display: block;min-height: 20px;padding-left: 5px;vertical-align: middle;"><input type="radio" id="veteranPreference2" name="veteranPreference2" value="2"></div> </td>
			        <td style="border:0px;background-color: transparent;padding-left:5px;"><spring:message code="lblNo" /> </td>
			    </tr>
			    </tbody>
			</table>
        </div>
       </div> 
   </div>

<div  id="retirenoDiv">
	        <div class="row left10">
				<div class="col-sm-6 col-md-6">
					<div class="span15 portfolio_Subheading"><spring:message code="lblTeacherretir" /></div>		
				</div>	
		 	</div>	
		 		
			<div class="col-sm-6 col-md-6">
				
			</div>
			
			
			
			 <div class="row left30">			
				<div class="col-sm-9 col-md-9">
	        		<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px; margin-left: 0px;">
								<input type="checkbox" id="isretired" onclick="chkRetired();" style=" margin-left:-15px;"/>
		            			<spring:message code="lblRetireForDist" />
		           	</label>
				</div>	         			
			</div>
			
			<div class="hide" id="isRetiredDiv">
				<div class="row left20">			
					<div class="col-sm-6 col-md-6">
							<div id="SearchTextboxDiv">	         		   
			             			<label id="captionDistrictOrSchool"><spring:message code="lblRetFrmDist" /><span class="required">*</span></label>
				             		<input type="hidden" id="retireddistrictId" name="retireddistrictId" value=""/>
				             		<input type="text" id="retireddistrictName" name="retireddistrictName"  class="form-control"
					             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData1', 'retireddistrictName','retireddistrictId','');"
										onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData1', 'retireddistrictName','retireddistrictId','');"
										onblur="hideDistrictMasterDiv(this,'retireddistrictId','divTxtShowData1');"	/>
									<div id='divTxtShowData1' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData1','retireddistrictName')" class='result' ></div>	
				           </div>
		        	</div>	         			
				</div>
				<div class="row left20">			
					<div class="col-sm-3 col-md-3">
							<label><strong><spring:message code="lblRetFrmStat" /><span class="required">*</span></strong></label>
							<select  class="form-control" id="stMForretire" onchange="getPraxis(),clearCertType();">   
								<option value=""><spring:message code="optSltSt" /></option>
								<c:forEach items="${listStateMaster}" var="st">
					        		<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
								</c:forEach>				
							</select>	
		        	</div>	         			
				
						
					<div class="col-sm-3 col-md-3">
		        		<label><strong><spring:message code="lblTeacherRetirementNumber" /><span class="required">*</span></strong></label>	
							<input type="text" id="retireNo" class="form-control" maxlength="10"/>
					</div>	         			
				</div>
		</div>
	</div>

<div id="formerEmployeeDiv" class="row left10">
     
	        
				<div class="col-sm-6 col-md-6">
					<div class="span4 portfolio_Subheading"><spring:message code="lblCurrentEmploymnt" /></div>
				</div>
		
			<div class="row left7" >
			
				<div class="col-sm-6 col-md-6">
			    
			    </div>
			
			
				<div class="col-sm-9 col-md-9">
					<label class="span6 radio" style="margin-top: 2px; margin-bottom:2px;">
			    		<input type="radio" id="fe1" name="fe" onclick="showEmpNoDiv(1);" <c:if test="${teacherpersonalinfo.employeeType eq 0}">checked</c:if>/>
			    		<spring:message code="msgDp_commons13" /> <span id='displayDistrictNameForExt1'>${districtDName}</span>
			    	</label>
			    </div>	
			
		    	<div id="fe1Div" class="hide">
			    	<div class="row" >
				        <div class="col-sm-4 col-md-4" >
							<label style="width: 70%;margin-left: 65px;"><strong><spring:message code="lblEmpNum" /></strong></label>
							<input type="text" id="empfe1" style="width: 70%;margin-left: 65px;" class="form-control" maxlength="50"  <c:if test="${teacherpersonalinfo.employeeType eq 0}"> value="${teacherpersonalinfo.employeeNumber}" </c:if>/>
						</div>
					</div>
					  <div class="row left70">
					      <div class="col-sm-12 col-md-12">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
								<input type="checkbox" id="empchk11" onclick="showEmpDate(0);" <c:if test="${teacherpersonalinfo.employeeType eq 0 and teacherpersonalinfo.retirementdate ne null}">checked</c:if>/>
		            			<spring:message code="msgDp_commons14" />
		           				</label> 
			           		</div>	
			           		 <div class="col-sm-3 col-md-3 left25">
			           			<div class='mt5 hide' id='rtDateDiv'>
			           				<label><strong><spring:message code="lblRetirementDate" /></strong></label>
									<input type="text" id="rtDate" name="rtDate" class="form-control" maxlength="4" <c:if test="${teacherpersonalinfo.employeeType eq 0}"> value="${teacherpersonalinfo.retirementdate	}" </c:if>/>
			           			</div>
							</div>
						</div>
						<div class="row left70">
						    <div class="col-sm-12 col-md-12">
							<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;" <c:if test="${teacherpersonalinfo.employeeType eq 0 and teacherpersonalinfo.moneywithdrawaldate ne null}">checked</c:if>>
								<input type="checkbox" id="empchk12"  onclick="showEmpDate(1);"/>
		            			<spring:message code="msgDp_commons15" />
		            		</label>
		            		</div>	
							<div class="col-sm-3 col-md-3 left25">
		           			<div class='mt5 hide' id='wtDateDiv'>
		           				<label><strong><spring:message code="msgDateOfWithdrawl" /></strong></label>
								<input type="text" id="wdDate" name="wdDate" class="form-control" maxlength="4" <c:if test="${teacherpersonalinfo.employeeType eq 0}"> value="${teacherpersonalinfo.moneywithdrawaldate	}" </c:if>/>
		           			</div>
							</div>
						</div>
           		</div>
					
			
		
		
		        
			
				     <div class="col-sm-9 col-md-9">
						<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
							<input type="radio" id="fe2" name="fe" onclick="showEmpNoDiv(2);" <c:if test="${teacherpersonalinfo.employeeType eq 1}">checked
							</c:if>/><spring:message code="msgDp_commons17" /> <span id='displayDistrictNameForExt2'>${districtDName}</span>
							<c:if test="${teacherpersonalinfo.employeeType eq 1}">
							<script>showEmpNoDiv(2);
							</script>
							</c:if>
						</label>
					 </div>	
				
				
				 <div id="fe2Div" class="hide">
					<div class="row" >
						<div class="col-sm-4 col-md-4">
							<label style="width: 70%;margin-left: 65px;"><strong><spring:message code="lblEmpNum" /><span class="required">*</span></strong></label>
							<input type="text" id="empfe2"" style="width: 70%;margin-left: 65px;" class="form-control" maxlength="50" onblur="checkEmployeeCode();"  <c:if test="${teacherpersonalinfo.employeeType eq 1}"> value="${teacherpersonalinfo.employeeNumber}" </c:if>/>
						</div>
				</div>
           		
           		 <div class="row left70">
							<label class="span6 radio">
								<input type="radio" id="rdCEmp1" name="rdCEmp" <c:if test="${teacherpersonalinfo.isCurrentFullTimeTeacher eq 1}">checked </c:if>/><spring:message code="msgDp_commons18" />
							</label>
							<c:set var="showVal"  value="none"/>
							<c:if test="${isMiami}">
							<c:set var="showVal"  value="block"/>
							</c:if>
							<label class="span7 radio p0 left20" style="display: ${showVal};" id="partInsEmp">
								<input type="radio" id="rdCEmp3" name="rdCEmp" <c:if test="${teacherpersonalinfo.isCurrentFullTimeTeacher eq 2}">checked </c:if>/><spring:message code="msgDp_commons19" />
							</label>
							<label class="span6 radio">
						    <input type="radio" id="rdCEmp2" name="rdCEmp" <c:if test="${teacherpersonalinfo.isCurrentFullTimeTeacher eq 0}">checked </c:if>/><spring:message code="msgDp_commons20" />
					         </label>
				</div>
				</div>
	
		
		
		
				<div class="col-sm-9 col-md-9">
					<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
					<input type="radio" id="fe3" name="fe" onclick="showEmpNoDiv(3);"  <c:if test="${teacherpersonalinfo.employeeType eq 2}">checked</c:if>/>I have never been employed by <span id='displayDistrictNameForExt3'>${districtDName}</span>
				</label>
			</div>
		</div>
	</div>
<div id="ethnicOriginDiv">
	<div class="row left10">
		<div class="col-sm-9 col-md-9">
			<div class="portfolio_Subheading" id="eeodForMiamiHeader"><spring:message code="msgDp_commons22" /></div>
	   </div>
	
			</div>
			<div class="row left15">
				<div class="col-sm-12 col-md-12" id="eeodForMiami" style="display: none;">
					<label><strong>
						<spring:message code="msgDp_commons23" />
						</BR><spring:message code="msgDp_commons24" />
						</BR><spring:message code="msgDp_commons25" />
						</BR><spring:message code="msgDp_commons26" /> <a href="mailTo:crc@dadeschools.net">crc@dadeschools.net</a>.
					</strong></label>
				</div>
				
				<div class="col-sm-12 col-md-12 top5" id="eeodForOthers" style="display: none;">
					<label><strong>
					   <spring:message code="msgDp_commons27" />
					</strong></label>
				</div>
				
			
	
	
	
	
	
		<div class="row top5">
			<div class="col-sm-12 col-md-12 hide" id="eeoctext">				
		   </div>
		</div>
	
	
	
	
			<c:choose>
		<c:when test="${districtMaster.districtId == 7800038}">

			<div class="row nobleCssShow">
				<div class="col-sm-12 col-md-12">
				<spring:message code="msgDp_commons28" />
				</div>
			</div>
		</c:when>
		<c:otherwise>
		<div class="row hide nobleCssShow">
				<div class="col-sm-12 col-md-12">
			   <spring:message code="msgDp_commons28" />
				</div>
			</div>
		</c:otherwise>
		</c:choose>
		</div>
		<div class="row left10">
		
			<div class="col-sm-6 col-md-6">
				<div class="span15 portfolio_Subheading"><spring:message code="lblEthOrig" /></div>		
			</div>	
		 	
		 	
			<div class="col-sm-9 col-md-9">
				
			</div>
			</div>
			
		 <div class="row left15">
			
				<c:forEach items="${lstethnicOriginMasters}" var="ethnicOrigin">
					<div class="col-sm-3 col-md-3">
	        		<label class="radio" style="margin-top: 2px;margin-bottom: 2px;">		
						<input type="radio" name="ethnicOriginId" id="ethnicOriginId" value="${ethnicOrigin.ethnicOriginId}" <c:if test="${teacherpersonalinfo.ethnicOriginId.ethnicOriginId eq ethnicOrigin.ethnicOriginId}">checked</c:if>>${ethnicOrigin.ethnicOriginName} 
					</label>					
					</div>	         			
				</c:forEach>
				</div>						
	
	 </div>
	 
     <div id="ethinicityMasterDiv">
		<div class="row left10">
			<div class="col-sm-6 col-md-6">
				<div class="span4 portfolio_Subheading"><spring:message code="lblEth" /></div>		
			</div>
			<div class="col-sm-9 col-md-9">
				
		    </div>
	  </div>
		
			
		    
	   
			<div  class="row left15">		
				<c:forEach items="${lstEthinicityMasters}" var="ethinicityMaster">
					<div class="col-sm-3 col-md-3">
	        		<label class="radio" style="margin-top: 2px;margin-bottom: 2px;">		
						<input type="radio" name="ethinicityId" id="ethinicityId" value="${ethinicityMaster.ethnicityId}" <c:if test="${teacherpersonalinfo.ethnicityId.ethnicityId eq ethinicityMaster.ethnicityId}">checked</c:if>>${ethinicityMaster.ethnicityName}</br> 
					</label>
					<!--<c:set var="checked" value=""></c:set>-->
					</div>	         			
				</c:forEach>		
		
			</div>
	   </div>
	   
	   <div id="raceDiv">
		<div class="row left10">
			<div class="col-sm-6 col-md-6" >
				<div class="portfolio_Subheading"><spring:message code="lblRac" /></div>		
			</div>
		
		
		<div class="col-sm-9 col-md-9" >
		
		</div>
		
		
		
		<div class="col-sm-12 col-md-12" style="margin-left:10px;">
			<div  id="raceData"></div>
		</div>
	    </div>
	 </div>
	<div id="genderDiv">
		<div class="row left10">
			<div class="col-sm-6 col-md-6" >
				<div class="span4 portfolio_Subheading"><spring:message code="lblGend" /></div>		
			</div>
		</div>
		
		<div class="row left15">
			<div class="span15" " id="genderCheckDiv">
			 	<c:forEach items="${lstGenderMasters}" var="lstGenderMasters">
					<div class="col-sm-2 col-md-2">
		        		<label class="radio" id="displayGender${lstGenderMasters.genderId}" style="margin-top: 2px;margin-bottom: 2px;">		
							<input type="radio" name="genderId" id="genderId" value="${lstGenderMasters.genderId}"  <c:if test="${teacherpersonalinfo.genderId.genderId eq lstGenderMasters.genderId}">checked</c:if>>${lstGenderMasters.genderName}</br> 
						</label>
				</div>	         			
				</c:forEach>				
			</div>
		</div>
	 </div>
	 
	  <!--  Start :: Expected salary Body-->
<div class="hide" id="expectedSalaryDiv">
	<div class="row">
		<div class="col-sm-6 col-md-6" >
		<div class="span4 portfolio_Subheading"><spring:message code="ExptSalary" /> $<span class="required expSalaryCss">*</span></div>		
	</div>
	</div>
	
	<div class="row">
		<div class="col-sm-3 col-md-3"> 	
			
		</div>	 
	</div>

	<div class="row">	
		<div class="col-sm-3 col-md-3"> 
		<input type="text" name="expectedSalary" id="expectedSalary" onkeypress="return checkForInt(event);" class="form-control" maxlength="20"  value="${teacherpersonalinfo.expectedSalary}"/>
		</div>		
	</div>
</div>

 </div>
     </div>
     </div>
	 
	 <div id="academicsDiv">		
		<div class="row portfolio_Section_Gap">
			<div class="col-sm-12 col-md-12">				
				<div style="float: left" class="portfolio_Subheading">
					<spring:message code="headAcad" /><span class="required hide" id="reqAcademicAstrick">*</span>
					<span id="acaddemicHelpDiv"><span>
				</div>
				<div style="float: right" class="addPortfolio">
						<a href="#" onclick="return showUniversityForm()"><spring:message code="lnkAddSch" /> </a>
				</div>
				<div style="clear: both;"></div>	
				
			</div>
		</div>		
		<div class="row" onmouseover="setGridNameFlag('academics')">
		   <div class="col-sm-12 col-md-12">
			   <div id="divDataGridAcademin"></div>
			</div>
		</div>

		<div class="portfolio_Section_ImputFormGap" id="divAcademicRow" style="display: none;" onkeypress="chkForEnter_Academic(event)">
		<iframe id='uploadFrameAcademicID' name='uploadFrameAcademic' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form class="textfield11" id="frmAcadamin" enctype='multipart/form-data' method='post' target='uploadFrameAcademic' action='fileuploadservlet.do'>
			<input type="hidden" value="" id="academicId" name="academicId"/>
			<div class="row">							
					<div class="col-sm-9 col-md-9">
		      			<div class='divErrorMsg' id='errordiv_AcademicForm' style="display: block;"></div>					              	
				    </div>
			</div>
			<div class="row">
					<div class="col-sm-3 col-md-3">
						<label>
							<spring:message code="lblDgr" /><span class="required nondgrReq">*</span>
							<a href="#" id="degreeTooltip" rel="tooltip" data-original-title=<spring:message code="msgDp_commons29" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
							<a href="#" onclick="document.getElementById('degreeName').value='Others';document.getElementById('degreeName').focus();return false;" style="margin-left: 20px;"><spring:message code="msgDp_commons30" /></a>
						</label>					
						<input  type="hidden" id="degreeId" value="">
						<input  type="hidden" id="degreeType" value="">
						<input  type="text"
							maxlength="50" 
							class="form-control input-small"
							id="degreeName" 
							autocomplete="off"
							value=""
							name="degreeName" 
							onfocus="getDegreeMasterAutoComp(this, event, 'divTxtShowData', 'degreeName','degreeId','');"
							onkeyup="getDegreeMasterAutoComp(this, event, 'divTxtShowData', 'degreeName','degreeId','');" 
							onblur="hideDegreeMasterDiv(this,'degreeId','divTxtShowData');checkGED();"/>
						<div id='divTxtShowData' style=' display:none;' class='result' 
						onmouseover="mouseOverChk('divTxtShowData','degreeName')"></div>				
					</div>
					
					<div class="col-sm-4 col-md-4">
						<label >
							<spring:message code="optSchool" /> <span class="required nondgrReq">*</span><a href="#" onclick="document.getElementById('universityName').value='Other';document.getElementById('universityName').focus();return false;" style="margin-left: 130px;"><spring:message code="lnkMySchNotLi" /></a>
						</label>					
						<input  type="hidden" id="universityId" value="">
						<input  type="text" 
							class="form-control"
							maxlength="100"
							id="universityName" 
							autocomplete="off"
							value=""
							name="universityName" 
							onfocus="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');"
							onkeyup="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');" 
							onblur="hideUniversityDiv(this,'universityId','divTxtUniversityData');"/>
						<div id='divTxtUniversityData' style=' display:none;' 
						onmouseover="mouseOverChk('divTxtUniversityData','universityName')" class='result' ></div>
						
					</div>					
					<div class="col-sm-5 col-md-5">
						<label >
							<spring:message code="lblFildOfStudy" /> <span class="required nondgrReq">*</span><a  href="#" onclick="document.getElementById('fieldName').value='Other'; document.getElementById('fieldName').focus();return false;" style="margin-left: 180px;"><spring:message code="lnkMyFldNotLi" /></a>
						</label>						
						<input  type="hidden" id="fieldId" value="">
						<input  type="text" 
							class="form-control"
							maxlength="50"
							autocomplete="off"
							id="fieldName"
							value=""
							name="fieldName" 
							onfocus="getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyData', 'fieldName','fieldId','');"
							onkeyup="getFieldOfStudyAutoComp(this, event, 'divTxtFieldOfStudyData', 'fieldName','fieldId','');" 
							onblur="hideFeildOfStudyDiv(this,'fieldId','divTxtFieldOfStudyData');"/>
						<div id='divTxtFieldOfStudyData' style=' display:none;' 
						onmouseover="mouseOverChk('divTxtFieldOfStudyData','fieldName')"  class='result' ></div>					
					</div>				
			</div>
			<div class="row">				
						<div class="col-sm-12 col-md-12">
							<label>
								<spring:message code="lblDatAtt" /><span class="required nondgrReq1">*</span>
							</label>
						 </div>
					  	
					  	<div class="col-sm-2 col-md-2">
						<select class="form-control" id="attendedInYear" name="attendedInYear">
							<option id="attendedInYearSelect" value=""><spring:message code="optSlt" /></option>
							<c:forEach var="year" items="${lstLastYear}">							
								<option id="attendedInYear${year}" value="${year}">${year }</option>
							</c:forEach>					
						</select>
						</div>
						<div class="fl to" style="margin-left:-7px;">
							<spring:message code="lblTo" />
						</div>
						<div class="col-sm-2 col-md-2" style="margin-left:-7px;">
							<select class="span1 form-control" id="leftInYear" name="leftInYear">
								<option id="attendedInYearSelect" value=""><spring:message code="optSlt" /></option>
								<c:forEach var="year" items="${lstLastYear}">
									<option id="leftInYear${year}" value="${year}">${year }</option>
								</c:forEach>						
						</select>
					    </div>
					<input type="hidden" id="sbtsource_aca" name="sbtsource_aca" value="0"/>					
					
					<c:set var="transUDis" value=""></c:set>
					<c:if test="${districtMaster.districtId == 7800038}">					
						<c:set var="transUDis" value="hide"></c:set>
					</c:if>
					
				<c:if test="${districtMaster.districtId != 1200390}">	
						<div  id="transcriptDiv" class="nobleCssHide ${transUDis}">
						<div  class="col-sm-3 col-md-3" style="margin-top: -23px;">
							<label>
								<spring:message code="lblTDC" /> <span class="required" id="acadTransReq">*</span><a href="#" id="transUploadTooltip" rel="tooltip" data-original-title="Please upload your transcript. Transcripts can be unofficial."><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
							</label>							
							<input id="pathOfTranscript" name="pathOfTranscript" type="file" width="20px;">
							<a href="javascript:void(0)" onclick="clearTranscript()"><spring:message code="lnkClear" /></a>
						</div>
						<span class="col-sm-3 col-md-3" id="divResumeSection" name="divResumeSection" style="display: none;">
							<label>
								&nbsp;&nbsp;
							</label>
							<span id="divResumerName"></span>
							&nbsp;&nbsp;<a href="javascript:void(0)" onclick="removeTranscript()" class="left5"><spring:message code="lnkRemo" /></a>
						</span>					  </div>
					</c:if>

			</div>
			
		</form>
	</div>
	
	<div id="divGPARow" style="display: none; " onkeypress="chkForEnter_Academic(event)">
	<form  id="frmGPA">
		   <div class="row">
				<div class="col-sm-2 col-md-2">
					<spring:message code="lblGPA" />
					<a href="#" id="iconpophover1" rel="tooltip"
						data-original-title="Grade Point Average"><img
							src="images/qua-icon.png" width="15" height="15" alt="">
					</a>
				</div>
			</div>
        <div class="row">
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblFresh" />
			</label>
			<input type="text" id="gpaFreshmanYear"  name="gpaFreshmanYear"  onkeypress='return checkForDecimalTwo(event)' class="input-small form-control" id="gpaFreshman" maxlength="4">
		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblSoph" />
			</label>
			<input type="text" id="gpaSophomoreYear"  name="gpaSophomoreYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaSophomore" maxlength="4">
		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblJunior" />
			</label>
			<input type="text" id="gpaJuniorYear"  name="gpaJuniorYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaJunior" maxlength="4">
		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblSenior" />
			</label>
			<input type="text" id="gpaSeniorYear"  name="gpaSeniorYear" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaSenior" maxlength="4">

		</div>
		<div class="col-sm-2 col-md-2">
			<label>
				<spring:message code="lblCumulative" /><span class="required nondgrReq philNT" id="crequired">*</span>
			</label>
			<input type="text" id="gpaCumulative"  name="gpaCumulative" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaCumulative" maxlength="4">
		</div>
				
		<c:choose>
		<c:when test="${districtMaster.districtId == 7800038}">
		<div class="col-sm-2 col-md-2 nobleCssShow">
            <label class="checkbox" style="margin-top:31px;">
                  <input type="checkbox" id="international" name="international" onclick="return chkInternational();">&nbsp;&nbsp;<spring:message code="lblInternational" />
                  &nbsp;<a href="http://www.foreigncredits.com/Resources/GPA-Calculator/" class="calculatortooltip" rel="tooltip" data-original-title="Click here to change International GPA Score"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  </label>
                  
            </div>
		</c:when>
		<c:otherwise>
		<div class="col-sm-2 col-md-2 hide nobleCssShow">
            <label class="checkbox" style="margin-top:31px;">
                  <input type="checkbox" id="international" name="international" onclick="return chkInternational();">&nbsp;&nbsp;<spring:message code="lblInternational" /> <a href="http://www.foreigncredits.com/Resources/GPA-Calculator/" class="calculatortooltip" rel="tooltip" data-original-title="Click here to change International GPA Score"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  </label>
                  
            </div>
		</c:otherwise>
		</c:choose>
		</div>
	</form>
	</div>
	
	
	<div id="divGPARowOther" style="display: none;" onkeypress="chkForEnter_Academic(event)">
		<form id="frmGPA1">		
				<div class="row" >
					<div class="col-sm-2 col-md-2">
						<spring:message code="lblGPA" />
						<a href="#" id="iconpophover2" rel="tooltip"
							data-original-title="Grade Point Average"><img
								src="images/qua-icon.png" width="15" height="15" alt="">
						</a>
					</div>
				</div>
				<div class="row">		
					<div class="col-sm-2 col-md-2">
						<label>
							<spring:message code="lblCumulative" /><span class="required nondgrReq philNT" id="crequired2">*</span>
						</label>
						<input type="text" id="gpaCumulative1"  name="gpaCumulative1" onkeypress='return checkForDecimalTwo(event)'  class="input-small form-control" id="gpaCumulative" maxlength="4">
					</div>
					<c:choose>
		<c:when test="${districtMaster.districtId == 7800038}">
		<div class="col-sm-2 col-md-2">
            <label class="checkbox" style="margin-top:31px;">
                  <input type="checkbox" id="international2" class="international1" name="international" onclick="return chkInternational();">&nbsp;&nbsp;<spring:message code="lblInternational" />&nbsp; <a href="http://www.foreigncredits.com/Resources/GPA-Calculator/" class="calculatortooltip" rel="tooltip" data-original-title=<spring:message code="msgClickIntnGPA" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  </label>
            </div>
		</c:when>
		<c:otherwise>
		<div class="col-sm-2 col-md-2 hide nobleCssShow">
            <label class="checkbox" style="margin-top:31px;">
                  <input type="checkbox" id="international2" class="international1" name="international" onclick="return chkInternational();">&nbsp;&nbsp;<spring:message code="lblInternational" /> <a href="http://www.foreigncredits.com/Resources/GPA-Calculator/" class="calculatortooltip" rel="tooltip" data-original-title=<spring:message code="msgClickIntnGPA" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
                  </label>
            </div>
		</c:otherwise>
		</c:choose>
				</div>
		</form>
	</div>
	
	
	<div class="row">
		<div class="col-sm-2 col-md-2" id="divDone" style="display: none; ">
			<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="insertOrUpdate_Academic(0)">
				<spring:message code="lnkImD" />
			</a>&nbsp;&nbsp;
			<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="resetUniversityForm()">
				<spring:message code="btnClr" />
			</a>
		</div>
	</div>
  </div>
 <div id="certificationDiv">	
	<div class="row portfolio_Section_Gap">
			<div class="col-sm-12 col-md-12">			
				<div style="float: left" class="portfolio_Subheading">
					<spring:message code="lnkCerti/Lice" /><span class="required hide" id="reqCerificationAstrick">*</span>
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="javascript:void(0);" onclick="showForm_Certification(),clearForm_Certification();"><spring:message code="lnkAddCerti/Lice" /></a>
				</div>
				<div style="clear: both;"></div>	
		    </div>
		    <div class="col-sm-12 col-md-12 hide newBerlinCss"><spring:message code="msgDp_commons31" /></div>
		    <div class="col-sm-12 col-md-12 hide philadelphiaNTCss"><spring:message code="msgDp_commons32" /></div>
		    <div class="col-sm-12 col-md-12 hide philadelphiaNTCss1"><spring:message code="msgDp_commons33" /></div>	
	</div>
		
	<div class="row" onmouseover="setGridNameFlag('certification')">
	    <div class="col-sm-12 col-md-12">
	    <div id="divDataCertificate"></div>
			<!--<div id="divDataGridCertifications"></div>
		--></div>
	</div>
	
	<!-- Input form -->
	<div class="portfolio_Section_ImputFormGap" id="divMainForm" style="display: none;" onkeypress="chkForEnter_Certifications(event)">
		<iframe id='uploadFrameCertificationsID' name='uploadFrameCertifications' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
		<form id="frmCertificate" name="frmCertificate" enctype='multipart/form-data' method='post' target='uploadFrameCertifications' action='fileuploadservletforcertification.do' >
			<input type="hidden" id="certId" name="certId"/>			
				<div class="row">
				  <div class="col-sm-12 col-md-12">
		 			<div id='divServerError' class='divErrorMsg' style="display: block;">${msgError}</div>
		 			<div class='divErrorMsg' id='errordiv_Certification' style="display: block;"></div>
		 		  </div>
				</div>
		
		<div class="row">
				<div class="col-sm-4 col-md-4"><label><strong><spring:message code="lblCerti/LiceSt" /><span class="required">*</span> <a href="#" id="clsToolTip" class="hide" rel="tooltip" data-original-title=<spring:message code="msgDp_commons32" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
			<select  class="form-control" id="certificationStatusMaster" onchange="fieldsDisable(this.value);">  
				<option value=""><spring:message code="lblSltCerti/LuceStatus" /></option>
				<c:forEach items="${lstCertificationStatusMaster}" var="csml">
	        		<option id="csml${csml.certificationStatusId}" value="${csml.certificationStatusId}" ${selected} >${csml.certificationStatusName}</option>
				</c:forEach>
			</select>
		</div>
		
		<div class="col-sm-3 col-md-3 certClass">
			<label>
				<strong><spring:message code="lblCertiRp" /><span class="required">*</span>
				<a href="#" id="certificationtypeMasterTool" rel="tooltip" data-original-title="Add your administrator, teacher, and other certificates"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</strong>
			</label>
			<select id="certificationtypeMaster" name="certificationtypeMaster" class="form-control">
				<option value="0"><spring:message code="optSlt" /></option>
				 <c:forEach var="cvar" items="${listCertTypeMasters}">
					<option  value="${cvar.certificationTypeMasterId}">${cvar.certificationType}</option>
						</c:forEach>				
		</select>
		</div>
										 
		<div class="col-sm-4 col-md-4 certClass"><label><strong>State<span class="required">*</span></strong></label>
			<select  class="form-control" id="stateMaster" onchange="getPraxis(),clearCertType();">   
				<option value=""><spring:message code="optSltSt" /></option>
				<c:forEach items="${listStateMaster}" var="st">
	        		<option id="st${st.stateId}" value="${st.stateId}" ${selected} >${st.stateName}</option>
				</c:forEach>				
			</select>	
		</div>
		
		<div class="col-sm-2 col-md-2 certClass" style="width:14%;"><label><strong><spring:message code="lblYearRece" /><span class="required">*</span></strong></label>
			<select id="yearReceived" name="yearReceived"  class="form-control" >
				<option id="yearReceivedSelect" value=""><spring:message code="optSlt" /></option>
				<c:forEach var="year" items="${lstLastYear}">							
					<option id="yearReceived${year}" value="${year}">${year }</option>
				</c:forEach>					
			</select>
		</div>
		
		<div class="col-sm-2 col-md-2 certClass" style="width:19.5%;">	
			<label><strong><spring:message code="lblYearExp" /><span class="required"></span></strong></label>
			<select class="form-control" name="yearexpires" id="yearexpires" style="padding-left:6px;">
				<option value=""><spring:message code="optDoesNotExpire" /></option>
				<c:forEach var="year" items="${lstComingYear}">							
					<option id="yearExpires${year}" value="${year}">${year }</option>
				</c:forEach>
			</select>
		</div>
									
		<div class="col-sm-7 col-md-7 certClass">
			<label><strong><spring:message code="lblCert/LiceName" /><span class="required">*</span></strong>
				<a href="#" style="margin-left: 180px;" onclick="showcertiDiv()"><spring:message code="lnkCerti/LiceNotLi" /></a>
			</label>
			
			<input  type="hidden" id="certificateTypeMaster" value="">
			<input  type="text" 
				class="form-control"
				maxlength="500"
				id="certType" 
				value=""
				name="certType" 
				onfocus="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
				onkeyup="getCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
				onblur="hideCertificateType(this,'certificateTypeMaster','divTxtCertTypeData');"/>
			<div id='divTxtCertTypeData' style=' display:none;' class='result' ></div>
			
		</div>
		
		<!--
		
		<div class="span3"><label><strong>Certification/Licensure Status<span class="required">*</span></strong></label>
			<select  class="span3" id="certificationStatusMaster">   
				<option value="">Select Certification/Licensure Status</option>
				<c:forEach items="${lstCertificationStatusMaster}" var="csml">
	        		<option id="csml${csml.certificationStatusId}" value="${csml.certificationStatusId}" ${selected} >${csml.certificationStatusName}</option>
				</c:forEach>
				
			</select>
		</div>		
		-->
		<div class="col-sm-2 col-md-2 certClass dOENumberDiv" >
			<label class="show doeShow"><strong><spring:message code="lblDOENum" /><span class="required hide doeReq">*</span></strong></label>
			<label class="hide ppdiShow"><strong><spring:message code="lblPPID" /> <a href="#" id="iconpophoverppdi" rel="tooltip" data-original-title=<spring:message code="msgDp_commons34" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a></strong></label>
			<input type="text" class="form-control" name="doenumber" id="doenumber" maxlength="25" value="" />
		</div>
		
		<div class="col-sm-9 col-md-9 certClass cluDiv">
				<label><strong><spring:message code="lblCerti/LiceUrl" /> <a href="#" id="iconpophoverCertification" rel="tooltip" data-original-title=<spring:message code="tooltipMyPortfolio1" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a>		 
			</strong>
						
				</label>
			
				<input  type="text" 
					class="form-control"
					maxlength="500"
					id="certUrl" 
					value=""
					name="certUrl" />
				
			</div>
		</div>
		
		<div class="row" id="certiExpirationDiv">
			<div class="col-sm-2 col-md-2">
				<label><strong>Certificate Expiration</strong></label>
				<input type="text" id="certiExpiration" class="form-control" name="certiExpiration"/>
			</div>
		</div>
		
		<div class="row certClass gradeLvl">	
			<div class="col-sm-12 col-md-12">
			<label><strong><spring:message code="lblGradeLvl" /></strong></label>
			</div>
			</div>
			<div class="row col-sm-12 col-md-12 certClass gradeLvl">	
			<div class="divwidth">
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="pkOffered" name="pkOffered" >
							<spring:message code="lblPK" /> 
							</label>
			</div>	
			<div class="divwidth">			
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="kgOffered" name="kgOffered" >
							<spring:message code="lblKG" />
							</label>
			</div>	
			<div class="divwidth">	
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g01Offered" name="g01Offered" >
							<spring:message code="lbl1" /> 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g02Offered" name="g02Offered" >
						<spring:message code="lbl2" />
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g03Offered" name="g03Offered" >
							<spring:message code="lbl3" />
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g04Offered" name="g04Offered" >
							<spring:message code="lbl4" /> 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g05Offered" name="g05Offered" >
							<spring:message code="lbl5" /> 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g06Offered" name="g06Offered" >
							<spring:message code="lbl6" /> 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g07Offered" name="g07Offered" >
							<spring:message code="lbl7" /> 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g08Offered" name="g08Offered" >
							<spring:message code="lbl8" />
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g09Offered" name="g09Offered" >
							<spring:message code="lbl9" />
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g10Offered" name="g10Offered" >
							<spring:message code="lbl10" /> 
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g11Offered" name="g11Offered" >
							<spring:message code="lbl11" />
							</label>
			</div>	
			<div class="divwidth">					
							<label class="checkbox inline">
							<input type="checkbox" value="1" id="g12Offered" name="g12Offered" >
							<spring:message code="lbl12" /> 
							</label>
							
			</div>
	     </div>
         
         
         
         
         
	         <div id='praxisArea' class="row certClass" >
	         <div class="col-sm-2 col-md-2">
	         <label ><strong><spring:message code="lblReqPrxITests" /></strong></label>
	         </div>
	         <div class="col-sm-3 col-md-3" >
	         <label ><strong id='reading'></strong></label>
	         <div class="row">
			 <div class="col-sm-8 col-md-8">
			 <label ><strong ><spring:message code="lblYurScore" /> <input type='text' maxlength="3" id='readingQualifyingScore' class="form-control" onkeypress="return checkForInt(event)"></strong></label>
			 </div>
			 </div>
	         </div>
	         <div class="col-sm-3 col-md-3">
	         <label ><strong id='writing'></strong></label>
	          <div class="row">
			 <div class="col-sm-8 col-md-8">
			 <label ><strong ><spring:message code="lblYurScore" /> <input type='text' maxlength="3" id='writingQualifyingScore' class="form-control" onkeypress="return checkForInt(event)"></strong></label>
			 </div>
			 </div>
	         </div>
	         <div class="col-sm-3 col-md-3" >
	         <label ><strong id='maths'></strong></label>
	         <div class="row">
			 <div class="col-sm-8 col-md-8">
			 <label ><strong ><spring:message code="lblYurScore" /> <input type='text' maxlength="3" id='mathematicsQualifyingScore' class="form-control" onkeypress="return checkForInt(event)"></strong></label>
			 </div>
			 </div>
	         </div>
	         
	         </div>
	    
	    <div class="row certClass certSrc">
	   		<div class="col-sm-3 col-md-3" id='ieinNumberDiv'>
	    			<label><spring:message code="lblIEINNumber" /><span class="required" style="color: red;">*</span><a href="#" id="iconpophoveriein" rel="tooltip" data-original-title=<spring:message code="msgIEINnumber2" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
	    			<input type="text" id="IEINNumber" name="IEINNumber" maxlength="50" class="form-control" >
	    	</div>
	    	
	    	<div class="col-sm-4 col-md-4">
	    		<label class="doeShow">
				<spring:message code="msgCLLetter" /><a href="#" id="proofCertTooltip" rel="tooltip" data-original-title=<spring:message code="msgTeacherESACTECert" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</label>
				<label class="hide ppdiShow">
					<spring:message code="msgDp_commons35" />
				</label>
				<input type="hidden" id="sbtsource_cert" name="sbtsource_cert" value="0"/>
				<input id="pathOfCertificationFile" name="pathOfCertificationFile" type="file">
				<a href="javascript:void(0)" id="clearLink" onclick="clearCertification()"><spring:message code="lnkClear" /></a>
	    	</div>
					
		<input type=hidden id="pathOfCertification" value=""/>
			
			    <div class="col-sm-5 col-md-5">
				<span  id="removeCert" name="removeCert" style="display: none;">
					<label>
						&nbsp;&nbsp;
					</label>
					<span id="divCertName"></span>
					<a href="javascript:void(0)" onclick="removeCertificationFile()"><spring:message code="lnkRemo" /></a>&nbsp;
					<a href="#" id="iconpophover6" rel="tooltip" data-original-title=<spring:message code="msgCertificationLicence3" />>
					<img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</span>
				</div>
			</div>
			<div class="row certText">
				<div class="col-sm-9 col-md-9 top10">
				<labe><spring:message code="lblExpOfCurrPendCert" />&nbsp;<a href="javascript:void(0)" class="calculatortooltip" rel="tooltip" data-original-title=<spring:message code="msgDp_commons36" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
					<textarea id="certTextvalue" rows="" cols=""></textarea>
				</div>
			</div>			
 		</form>
 		<div class="row">								
	    <div class="col-sm-3 col-md-3 idone">
			<a id="hrefDone" href="#" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="return insertOrUpdate_Certification(0);" ><spring:message code="lnkImD" /></a>&nbsp;&nbsp;
			<a class="idone" style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="return hideForm_Certification()">
				<spring:message code="btnClr" />
			</a>
		</div>
		</div>
	</div>
</div>


<iframe id='multifileuploadformiframe' name='multifileuploadformTarget' target='uploadFrameResume'  height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
<c:if test="${jobId=='1'}">
<div id="generalKnowledgeDiv">
<!-- Start :: Pass/Fail General Knowledge Exam Body -->
<form:form method="post" action="multifileupload.do" id='multifileuploadform' target='multifileuploadformTarget'  modelAttribute="uploadForm" enctype="multipart/form-data">	
<input type="hidden" id="uploadFileNumber" name="uploadFileNumber" value="1"/>
	
	<div class="row portfolio_Section_Gap">
		<div class="col-sm-8 col-md-8">
			<div class="span6 portfolio_Subheading"><spring:message code="lblPFGKE" /></div>
		</div>
	</div>
	<div class="row">
	<div class="col-sm-8 col-md-8">
		<div class='divErrorMsg' style="display: none;" id="errGeneralKnowledge"></div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblStatus" /><span class="required">*</span></label>
			<select class="form-control" id="generalKnowledgeExamStatus" name="generalKnowledgeExamStatus">
				<option value="0"><spring:message code="sltStatis" /></option>   
				<option value="P"><spring:message code="optPass" /></option>
				<option value="F"><spring:message code="optFail" /></option>
			</select>
		</div>
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblExamDate" /><span class="required">*</span></label>
			<input type="text" id="generalKnowledgeExamDate" name="generalKnowledgeExamDate"   maxlength="0" class="form-control">
		</div>
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblScorereport" /><span class="required">*</span></label>
					<input type="hidden" id="generalKnowledgeScoreReportHidden" name="generalKnowledgeScoreReportHidden" value=""/>
					<input id="generalKnowledgeScoreReport" name="files[0]" type="file" width="20px;">
					<a href="javascript:void(0)" onclick="clearGeneralKnowledgeScoreReport()"><spring:message code="lnkClear" /></a>
		
		</div>
		<div class="col-sm-3 col-md-3">		
				<span class="span4" id="removeGeneralKnowledgeScoreReportSpan" name="removeGeneralKnowledgeScoreReportSpan" style="display: none;">
					<!--<label>
						&nbsp;&nbsp;
					</label>
					--><span id="divGeneralKnowledgeScoreRep">
					</span>
					<!-- <a href="javascript:void(0)" onclick="removeGeneralKnowledgeScoreReport()">Remove</a>&nbsp;
						<a href="#" id="iconpophover7" rel="tooltip" data-original-title="Remove General Knowledge Score Report"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
				</span>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6 col-md-6 top10" id="generalExamNote">
		<textarea name="generalExamNote"></textarea>		
		</div>
	</div>
</form:form>
</div>
<!-- End :: Pass/Fail General Knowledge Exam Body -->
	
<!-- Start :: Pass/Fail Subject Area Exam Body -->
	
<div id="subjectAreaDiv">		
	<div class="row portfolio_Section_Gap">
			<div class="col-sm-12 col-md-12">				
				<div style="float: left" class="portfolio_Subheading">
					<spring:message code="lblPFSAE" />
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="#" onclick="return showSubjectAreaForm()"><spring:message code="lnkAddFassFailSubArea" /> </a>
				</div>
				<div style="clear: both;"></div>				
			</div>
		</div>
	
	
	<div class="row" onmouseover="setGridNameFlag('subjectAreas')">
	    <div class="col-sm-9 col-md-9">
		<div id="divGridSubjectAre"></div>
		</div>
	</div>
	
<iframe id='uploadFrameSubjectArea' name='uploadFrameSubjectArea' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
<form id="frmSubjectArea" name="frmSubjectArea" enctype='multipart/form-data' method='post' target='uploadFrameSubjectArea' action='teacherSubjectAreaServlet.do' >

	<div class="mt30" style="display: none" id="divSubjectAreaInput">
		
		<div class="row">
		<input type="hidden" id="teacherSubjectAreaExamId" value="0">
		<input type="hidden" id="sbtsource_subArea" name="sbtsource_subArea" value="0"/>
		 <div class="col-sm-12 col-md-12">
		<div class='divErrorMsg' style="display: none;" id="errSubjectArea"></div>
		</div>
		
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblStatus" /><span class="required">*</span></label>
			<select class="form-control" id="examStatus" name="examStatus">
				<option value="0"><spring:message code="sltStatis" /></option>   
				<option value="P"><spring:message code="optPass" /></option>
				<option value="F"><spring:message code="optFail" /></option>
			</select>
		</div>
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblExamDate" /><span class="required">*</span></label>
			<input type="text" id="examDate" name="examDate"   maxlength="0" class="form-control">
		</div>
		<div class="col-sm-3 col-md-3" >
			<label><spring:message code="lblSub" /><span class="required">*</span></label>
			<div id="subjectShowDiv">
				<select class="form-control" id="subjectIdforDSPQ" name="subjectIdforDSPQ">
					<option value="0"><spring:message code="optStlSub" /></option>   
					 <c:if test="${not empty subjectList}">
					 	<c:set var="subEditId" ></c:set>
				        <c:forEach var="subjectList" items="${subjectList}" varStatus="status">
				        	<!--<c:choose>
							    <c:when test="${teacherSubjectAreaExam.subjectAreaExamMaster.subjectAreaExamId eq subjectList.subjectAreaExamId}">
							    	<c:set var="subEditId" value="selected"></c:set>
							    </c:when>
							    <c:otherwise>
							    	<c:set var="subEditId" ></c:set>
							    </c:otherwise>              
							</c:choose>-->
				        	<option value="${subjectList.subjectAreaExamId}" ${subEditId}><c:out value="${subjectList.subjectAreaExamName}" /></option>
						</c:forEach>
					</c:if>	
				</select>
			</div>
		</div>
		</div>
		<div class="row top10">
			<div class="col-sm-6 col-md-6" id="subjectExamTextarea">
				<label><spring:message code="blNote" /></label>
				<textarea name="subjectExamTextarea"></textarea>
			</div>	
		<div class="col-sm-3 col-md-3">
			<label><spring:message code="lblScorereport" /><span class="required">*</span></label>
					<input type="hidden" id="scoreReportHidden" name="scoreReportHidden" value=""/>
					<input id="scoreReport" name="files[1]" type="file" width="20px;">
					<a href="javascript:void(0)" onclick="clearScoreReport()"><spring:message code="lnkClear" /></a>
		</div>
		
		<div class="col-sm-3 col-md-3">		
				<span  id="removeScoreReportSpan" name="removeScoreReportSpan" style="display: none;">
					<!--<label>
						&nbsp;&nbsp;
					</label>
					--><span id="divScoreReport">
					</span>
					<!-- <a href="javascript:void(0)" onclick="removeScoreReport()">Remove</a>&nbsp;
						<a href="#" id="iconpophover7" rel="tooltip" data-original-title="Remove Score Report"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>-->
				</span>
		</div>
		</div>
		<div class="row">
		<div class="col-sm-12 col-md-12 idone" >
			<a id="hrefDone" href="#" style="cursor: pointer; margin-left: 0px; text-decoration:none;" onclick="return saveSubjectAreas(0)" ><spring:message code="lnkImD" /></a>&nbsp;&nbsp;
			<a style="cursor: pointer; margin-left: 0px; text-decoration:none;"	onclick="return cancelSubjectAreaForm()">
				<spring:message code="btnClr" />
			</a>
		</div>
		
	</div>
	
	</div>
	
</form>	
	
</div>
</c:if>

<div clas="hide" id="involvementDiv">
	<div class="row portfolio_Section_Gap">	
		<div class="col-sm-12 col-md-12">		
			<div style="float: left" class="portfolio_Subheading">
				<spring:message code="lblIVWork" />
			</div>
			<div style="float: right" class="addPortfolio">
					<a href="#" onclick="return showInvolvementForm()" ><spring:message code="lnkAddInvol" /></a>
				</div>
		</div>
	</div>

	<div class="row" onmouseover="setGridNameFlag('involvement')">
		<div class="col-sm-12 col-md-12">
			<div id="divDataInvolve" class="span15"></div>
		</div>
	</div>
	
						<div class="mt30" style="display: none" id="divInvolvement">
										<div class="row">
										<div class="col-sm-10 col-md-10">
											<p><spring:message code="pListOrganiz" /></p>
										</div>										
										<div class="col-sm-10 col-md-10">
										<div class='divErrorMsg' id='errordivInvolvement' style="display: block;"></div>
										</div>
										</div>
										<form id="frmInvolvement" name="frmInvolvement">
											<input type="hidden" id="involvementId"name="involvementId"	/>
											<div class="row">												
												<div class="col-sm-3 col-md-3"><label><spring:message code="lblOrga" /></label>
													<input type="text" id="organizationInv" name="organizationInv" maxlength="50" class="form-control" >
												</div>
												
												<div class="col-sm-3 col-md-3"><label><spring:message code="lblTyOfOrga" /></label>
													<select class="form-control " id="orgTypeId" name="orgTypeId">
														<option value=""><spring:message code="optSlt" /></option>
														<c:forEach items="${lstOrgTypeMasters}" var="orgType">
															<option value="${orgType.orgTypeId}">${orgType.orgType}</option>
														</c:forEach>
													</select>
												</div>
																							
												<div class="col-sm-4 col-md-4"><label><spring:message code="lblNumOfPeopleInOrga" /></label>
													<select class="form-control " id="rangeId" name="rangeId">
														<option value=""><spring:message code="optSlt" /></option>
														<c:forEach items="${lstPeopleRangeMasters}" var="peopleRange">
															<option value="${peopleRange.rangeId}">${peopleRange.range}</option>
														</c:forEach>
													</select>
												</div>
												
											</div>	
											<div class="row top5">
											        <div class="col-sm-4 col-md-4">
											         <label><spring:message code="lblLeadPeopleInThisOrga" /></label>
											       													    
												     <div class="" id="" style="height: 40px;">
												    	<div class="radio inline col-sm-1 col-md-1" style="margin-top:0px;">
													    	<input type="radio"  id="rdo1" value="option1"  name="optionsRadios" onclick="showAndHideLeadNoOfPeople('1')"> <spring:message code="lblYes" />
													    </div>
													    </br>
													    <div class="radio inline col-sm-1 col-md-1" style="margin-left:20px;margin-top:-20px;">
													    	<input type="radio" checked="t" value="option1"  name="optionsRadios" onclick="showAndHideLeadNoOfPeople('0')"> <spring:message code="lblNo" />
													    </div>
												     </div>								    
											          </div>
											    <div class="col-sm-2 col-md-2" id="divLeadNoOfPeople" name="divLeadNoOfPeople" style="display: none;"><label><spring:message code="lblHowMnyPeople" /></label>
													<input type="text" id="leadNoOfPeople" name="leadNoOfPeople" class="form-control" onkeypress="return checkForInt(event);" maxlength="4"/>
												</div>
										    </div>	
										    <div class="row">	
											    <div class="col-sm-6 col-md-6 idone">
											    	<a href="#" onclick="return saveOrUpdateInvolvement()"><spring:message code="lnkImD" /></a>&nbsp;
											    	<a href="#"	onclick="return hideInvolvement()">
														<spring:message code="btnClr" />
													</a>
											    </div>
											</div>	
										</form>
										</div>
</div>
<div clas="hide" id="honorsDiv">
	<div class="row portfolio_Section_Gap">	
		<div class="col-sm-12 col-md-12">		
			<div style="float: left" class="portfolio_Subheading">
				<spring:message code="lblHonors" />
			</div>
			<div style="float: right" class="addPortfolio">
					<a href="#" onclick="return showHonorForm()" ><spring:message code="lnkAddHonors" /></a>
				</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-10 col-md-10" id="divDataHon"></div>
	</div>
	
	
	<form id="frmHonor" name="frmHonor" onsubmit="return false;">
									    <input type="hidden" id="honorId" name="honorId"/>
									    <div  id="divHonor" style="display: none;">
										    <br>
										    <div class="row col-sm-10 col-md-10">
										    <div class='divErrorMsg' id='errordivHonor'  style="display: block;"></div>
										    </div>	
										    
										    <div class="row ">								    
										    <div class="col-sm-4 col-md-4 rowmargin1"><label><spring:message code="lblAward" /></label>
										    	<input type="text" id="honor" name="honor" class="form-control" placeholder="" maxlength="50">
										    </div>
										    <div class="col-sm-2 col-md-2"><label>&nbsp;</label>
												<select id="honorYear" name="honorYear" class="form-control">
													<option value="0" ><spring:message code="optYear" /></option>														
													<c:forEach items="${lstYear}" var="year">
														<option>${year}</option>
													</c:forEach>
												</select>
											</div>
											</div>								
											<div class="row col-sm-4 col-md-4 idone top10 rowmargin">
												<a href="#" onclick="return saveOrUpdateHonors()"><spring:message code="lnkImD" /></a>
												&nbsp;<a href="#"	onclick="return hideHonor()">
													<spring:message code="btnClr" />
												</a>
											</div>
									    </div>
									    
									    </form>
	</div>
	
	<div id="employmentDiv">		
							
                   		   <div class="row portfolio_Section_Gap">
								<div class="col-sm-12 col-md-12">				
									<div style="float: left" class="portfolio_Subheading" >
										<spring:message code="lblEmplHis" /><span class="required hide" id="reqEmpAstrick">*</span>
									</div>
									<div style="float: right" class="addPortfolio">
									    <input type="hidden" id="sbtsource_emp" name="sbtsource_emp" value="0"/>
										<a href="javascript:void(0);" onclick="return showEmploymentForm();"><spring:message code="lnkAddEmp" /></a>
									</div>
									<div style="clear: both;"></div>	
									<div class="col-sm-12 col-md-12 hide" id="empSecText"></div>			
								</div>
							</div>						
							
                    		<div class="row" onmouseover="setGridNameFlag('workExperience')">
									 <div class="col-sm-9 col-md-9">
	                    					<div id="divDataGridEmploy"></div>
	                    			</div>	
                    		</div>
                    				                      	
				<div class="top20" style="display: none" id="divEmployment">
					<div class="row">					
						<div class="col-sm-12 col-md-12">
						<spring:message code="msgEmloymentRoles" />				
						</div>
						<div class="col-sm-12 col-md-12">
						<div class='divErrorMsg' id='errordivEmployment' style="display: block;"></div>
						</div>			
					</div>	
					<form class="" id="frmEmployment" name="frmEmployment" onsubmit="return false;">
						<input type="hidden" id="roleId" name="roleId">
						<div class="row">
						
						
                          <div class="col-sm-3 col-md-3">
                                <label>
                                <spring:message code="lblPosition" /><span class="required">*</span>
                                </label>
                                <select class="form-control" id="empPosition">
                                <option value="0"><spring:message code="optSlt" /></option>
								<option value="1"><spring:message code="lblStudentTeaching" /></option>
								<option value="2"><spring:message code="lblFullTimeTeaching" /></option>
								<option value="3"><spring:message code="lblSubstituteTeaching" /></option>
								<option value="4"><spring:message code="lblOtherWorkExperience" /></option>
                                </select>
                            </div>
                        
							<div class="col-sm-4 col-md-4">
								<label><spring:message code="lblTitDesigRol" /><span class="required">*</span></label>
								<input type="text" id="role" name="role" class="form-control" placeholder="" maxlength="50">
							</div>
							<div class="col-sm-5 col-md-5">
								<label><spring:message code="lblIndustryField" /><span class="required">*</span></label>
								<!--<input type="text" class="span4" placeholder="">-->
								<select class="form-control" id="fieldId2" name="fieldId">
									<option value=""><spring:message code="optSlt" /></option>													
									<c:forEach items="${lstFieldMaster}" var="fieldMaster">
										<option  value="${fieldMaster.fieldId}">${fieldMaster.fieldName}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
                        <div class="row">
                                <div class="col-sm-3 col-md-3"><label><spring:message code="lblNameofOrganization" /><span class="required">*</span></label>
                                         <input type="text" id="empOrg" class="form-control" name="empOrg"/>
                                </div>
                        
                        
								<div class="col-sm-4 col-md-4"><label>City<span class="required">*</span></label>
									<input type="text" id="cityEmp" class="form-control" name="cityEmp"/>
								</div>
								<div class="col-sm-5 col-md-5"><label><spring:message code="lblStateOfOrganization" /><span class="required">*</span></label>
									<input type="text" id="stateOfOrg" class="form-control" name="stateOfOrg"/>
								</div>
						</div>	
						<div class="row top15">
                            <div class="col-sm-3 col-md-3">
                                <label><spring:message code="lblDuration" /></label>
                            </div>
                        </div>  
                        <div class="row">
                            <div class="col-sm-3 col-md-3">
                                <label class="checkbox">
                                <input type="checkbox" id="currentlyWorking" name="currentlyWorking" onclick="hideToExp()"> <spring:message code="lblICurrWrkHr" />
                                </label>
                            </div>
                        </div>
						<div class="row" id="empDateDiv">
							<div class="col-sm-2 col-md-2"><label><spring:message code="lblFrm" /><span class="required">*</span></label>
								<select id="roleStartMonth" name="roleStartMonth" class="form-control">
									<option value="0"><spring:message code="optStrMonth" /></option>
									<c:forEach items="${lstMonth}" var="month" varStatus="status">
										<option value="${status.count}">${month}</option>
									</c:forEach>
									
								</select>
							</div>
							<div class="col-sm-2 col-md-2">
								<label>&nbsp;</label>
								<select id="roleStartYear" name="roleStartYear" class="form-control">
									<option value="0" ><spring:message code="optYear" /></option>														
									<c:forEach items="${lstYear}" var="year" >
										<option  value="${year}">${year}</option>
									</c:forEach>
								</select>
							</div>
							<div id="divToMonth" class="col-sm-2 col-md-2 "><label><spring:message code="lblTo" /><span class="required">*</span></label>
								<select   class="form-control" id="roleEndMonth" name="roleEndMonth">
									<option value="0"><spring:message code="optStrMonth" /></option>
									<c:forEach items="${lstMonth}" var="month"  varStatus="status">
										<option value="${status.count}">${month}</option>
									</c:forEach>
								</select>
							</div>
							<div id="divToYear" class="col-sm-2 col-md-2">
								<label>&nbsp;</label>
								<select id="roleEndYear" name="roleEndYear" class="form-control">
									<option value="0"><spring:message code="optYear" /></option>
									<c:forEach items="${lstYear}" var="year">
										<option value="${year}">${year}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						
						<div class="row">
							<div id="empHisAnnSal" class="col-sm-8 col-md-8"><label><spring:message code="lblAnnualSlryAmount" /><span class="required expsalaryInwork hide">*</span></label>
						    	<input type="text" style="width: 30%;" id="amount" name="amount" class="form-control"   onkeypress="return checkForInt(event);" maxlength="8">
							</div>
						
						</div>
						
						<div class="row">
							<div class="col-sm-3 col-md-3"><spring:message code="lblTypOfRole" /><span class="required philNT tORR">*</span>
							</div>
						</div>								
						<div class="row">						   
						    	<c:forEach items="${lstEmpRoleTypeMaster}" var="empRoleType">														
									 <div class="col-sm-2 col-md-2">
	        		                    <label class="radio" style="margin-top: 0px;">
								    	<input type="radio"  value="${empRoleType.empRoleTypeId}"  name="empRoleTypeId" name="optionsRadios">${empRoleType.empRoleTypeName} 
								    	</label>					
					                 </div>	
								</c:forEach>						  
						</div>	
						 							
						
							<div class="row hide pritr"><div class="col-sm-8 col-md-8"><br><label><spring:message code="lblRespondibinThRole" /><span class="required philNT pritreq">*</span> <a href="#" id="prrtool" rel="tooltip" data-original-title=<spring:message code="msgDp_commons37" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label></div>
							</div>
							<div class="row hide ">
							    <div class="col-sm-8 col-md-8" id='primaryRespdiv'>						   
							    <textarea id="primaryResp" name="primaryResp" class="span12" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea></div>
							</div>
						
						
						
						<div class="row hide mscitr">
							<div class="col-sm-4 col-md-4"><br><label><spring:message code="lblSignContriInThisRole" /><span class="required hide mscitrReq">*</span></label></div>
							</div>
						<div class="row hide mscitr">
						    <div class="col-sm-8 col-md-8" id='mostSignContdiv'>
						  
						    <textarea class="span12" id="mostSignCont" name="mostSignCont" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea></div>						    
						</div>
						 
						 <div class="row reasonForLeavdiv">
							<div class="col-sm-3 col-md-3"><br><label><spring:message code="lblRsnforLeaving" /></label></div>
							</div>
						<div class="row reasonForLeavdiv">
						    <div class="col-sm-8 col-md-8" id='reasonForLeadiv'>
						  
						    <textarea class="span12" id="reasonForLea" name="reasonForLea" rows="4"  maxlength="250" onkeyup="return chkLenOfTextArea(this,250);" onblur="return chkLenOfTextArea(this,250);"></textarea></div>						    
						</div>
						 
						 <div class="row">	
						    <div class="col-sm-4 col-md-4 idone">
						    	<a href="#" onclick="return insertOrUpdateEmployment(0)"><spring:message code="lnkImD" /></a>&nbsp;
						    	<a href="#"	onclick="return hideEmploymentForm()">
									<spring:message code="btnClr" />
								</a>
						    </div>
						    </div>
													
					</form>
				</div>
		
</div>

<div id="additionalDocumentsDiv">		
		<div class="row top20">
			<div class="col-sm-12 col-md-12">				
				<div style="float: left" class="portfolio_Subheading">
					<spring:message code="lnkAddDocu" />
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="#" onclick="return showAddDocumentsForm()"><spring:message code="lnkAddAdditionalDoc" /> </a>
				</div>
				<div style="clear: both;"></div>				
			</div>
			<div class="col-sm-12 col-md-12 hide philadelphiaCss"><spring:message code="msgSchoolDistrictPhiladelphia" /></div>
			<div class="col-sm-12 col-md-12 hide additionalDocumentsHeaderText"></div>
		</div>
		
		<div class="row" onmouseover="setGridNameFlag('additionalDocuments')">
		  <div class="col-sm-9 col-md-9">
			<div id="divGridAdditionalDoc"></div>
			</div>
		</div>

		<div  id="divAdditionalDocumentsRow" style="display: none; margin-top: 30px;" onkeypress="chkForAdditionalDocuments(event)">
				<iframe id='uploadFrameAdditionalDocumentsID' name='uploadFrameAdditionalDocuments' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form class="span15 textfield11" id="additionalDocumentsForm" name="multifileuploadformTarget2" enctype='multipart/form-data' method='post' target='uploadFrameAdditionalDocuments' action='additionalDocumentsUploadServlet.do'>
	
			            <div class="row">
							<div class="col-sm-9 col-md-9">
				      			<div class='divErrorMsg' id='errAdditionalDocuments' style="display: block;"></div>
							</div>
						</div>              	
					<div class="row">
						<div class="col-sm-4 col-md-4">
							<label><spring:message code="lnkAddDocu" /><span class="required">*</span></label>
							<input type="hidden" id="sbtsource_aadDoc" name="sbtsource_aadDoc" value="0"/>
							<input type="text" id="documentName" name="documentName" class="form-control" placeholder="" maxlength="250">
						</div>
						<div class="col-sm-3 col-md-3">
							<label>
								<spring:message code="lblDoc(PlzUploadAF)" /><span class="required">*</span>
							</label>
							<input type="hidden" id="uploadedDocumentHidden" name="uploadedDocumentHidden" value=""/>
							<input id="uploadedDocument" name="uploadedDocument" type="file" width="20px;">
							<a href="javascript:void(0)" onclick="clearUploadedDocument()"><spring:message code="lnkClear" /></a>
						</div>
						<div class="col-sm-3 col-md-3" style="margin-top: 25px;" id="removeUploadedDocumentSpan" name="removeUploadedDocumentSpan" style="display: none;">
						<label>
							&nbsp;&nbsp;
						</label>
						<div id="divUploadedDocument">
						</div>
						</div>
					</div>			
		       </form>			
			<div class="row">
				<div class="col-sm-4 col-md-4" id="divDocumnetDone" style="display: none; ">
				<a class="idone" style="cursor: pointer;text-decoration:none;" onclick="insertOrUpdate_AdditionalDocuments(0)">
					<spring:message code="lnkImD" />
				</a>&nbsp;&nbsp;
				<a class="idone" style="cursor: pointer;text-decoration:none;"	onclick="resetAdditionalDocumentsForm()">
					<spring:message code="btnClr" />
				</a>
				</div>
			</div>
	</div>
</div>

<div id="videoLinkDiv">
	<div class="row portfolio_Section_Gap">
	
		<div class="col-sm-12 col-md-12">		
				<div style="float: left" class="portfolio_Subheading">
					 <spring:message code="lnkViLnk" />
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="#" onclick="return showVideoLinksForm()" ><spring:message code="lnkAddViLnk" /></a>
				</div>
				<div style="clear: both;"></div>
                      
 				<div class="col-sm-12 col-md-12" style="margin-left: -15px;" id="commonTextVideo">
 				<spring:message code="headPlzIncludeURLToAnyVideos" />
 				</div>	   	
 				<div class="row" onmouseover="setGridNameFlag('videolinks')"> 				
							    <div class="col-sm-10 col-md-10">
								<div id="divDataVideoLink" ></div>
								</div>
							</div>
						<iframe id='uploadFrameVideoID' name='uploadFrameVideo' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
						</iframe>
				<div class="top15" style="display: none" id="divvideoLinks" onkeypress="return checkForEnter(event);">
				<div class='divErrorMsg' id='errordivvideoLinks' style="display: block;"></div>
				<form class="" id="frmvideoLinks" name="frmvideoLinks" method='post'  enctype='multipart/form-data' target='uploadFrameVideo' action='certificationVideoUploadServlet.do'>
				<input type="hidden" id="sbtsource_videoLink" name="sbtsource_videoLink" value="0"/>
					<div class="row">
						<div class="col-sm-12 col-md-12" style="max-width: 880px;">
							<label><spring:message code="lnkViLnk" /></label>
							<input type="hidden" id="videolinkAutoId" name="videolinkAutoId">
							<input type="text" id="videourl" name="videourl" class="form-control" placeholder="" maxlength="200">
						</div>
					</div>
					<div class="row">
							<div class="col-sm-6 col-md-6">
								<br/>
									<label><spring:message code="lblVid" /></label>												
									<input type="file" id="videofile" name="videofile">												
									<span style="display: none; visibility: hidden;" id="video"></span>
							</div>										
							<span class="col-sm-6 col-md-6">
							<br/><br/>												
								<spring:message code="lblSupportedvideos" />												
							</span>
					 </div>
					<div class="row top10">
					    <div class="col-sm-4 col-md-4 idone">
						<a href="javascript:void(0)" onclick="insertOrUpdatevideoLinks();"><spring:message code="lnkImD" /></a>&nbsp;
						<a href="#"	onclick="return hideVideoLinksForm()"><spring:message code="btnClr" />
							</a>
					    </div>
					</div>
				</form>
			</div>
		</div>
	</div>	
</div>

<div id="referenceDiv" style="margin-top:-20px;">
	<div class="row portfolio_Section_Gap">
	
		<div class="col-sm-12 col-md-12">		
				<div style="float: left" class="portfolio_Subheading">
					<spring:message code="lblRef" />
				</div>
				<div style="float: right" class="addPortfolio">
					<a href="#" onclick="return showElectronicReferencesForm()" ><spring:message code="lnkAddRef" /></a>
				</div>
				<div style="clear: both;"></div>
				<!-- new berlin -->
				<div class="col-sm-12 col-md-12 hide newBerlinCss" style="margin-left: -15px;"><spring:message code="msgDp_commons40" /></div>				
				<!-- new berlin -->
				<div class="col-sm-12 col-md-12 hide philadelphiaCss philadelphiaCssRef" style="margin-left: -15px;"><spring:message code="msgDp_commons41" /></div>
				<div class="col-sm-12 col-md-12 hide refTextHeader" style="margin-left: -15px;"></div>
				<c:if test="${districtMaster.districtId == 1200390}">
 				<div class="col-sm-12 col-md-12" style="margin-left: -15px;">
				<label>
				<spring:message code="lblPlease" /> <a onclick="openPDF();" href="javascript:void(0);"><spring:message code="lnkClickHere" /></a>
				<spring:message code="msgDp_commons43" />
				</label>
				</div>
					</c:if>	
				<c:choose>
				<c:when test="${districtMaster.districtId == 7800038}">
					<div class="col-sm-12 col-md-12" style="margin-left: -15px;">
 				<label class="required"><spring:message code="lbl3Rq5cm" /></label><p><spring:message code="msgDp_commons44" /></p>
 				</div>
		</c:when>
		<c:otherwise>
			<div class="col-sm12 col-md-12 hide nobleCssShow" style="margin-left: -15px;">
 				<label class="required"><spring:message code="lbl3Rq5cm" /></label><p><spring:message code="msgDp_commons44" /></p>
 				</div>
		</c:otherwise>
		</c:choose>
 							   	
		</div>
	</div>	
	<div class="row" onmouseover="setGridNameFlag('reference')">
	    <div class="col-sm-12 col-md-12">
			<div id="divDataElectronicReferenc" class="span15"></div>
		</div>
	</div>
	
	<!-- Input form -->	
	<div class="portfolio_Section_ImputFormGap" style="display: none;" id="divElectronicReferences" onkeypress="chkForEnterElectronicReferences(event)">
		    <div class="row">
		        <div class="col-sm-12 col-md-12">
		        <div class='divErrorMsg' id='errordivElectronicReferences' style="display: block;"></div>
		        </div>
	        </div>
			
			<iframe id='uploadFrameReferencesID' name='uploadFrameReferences' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
			<form id="frmElectronicReferences" name="frmElectronicReferences" enctype='multipart/form-data' method='post' target='uploadFrameReferences' action='fileuploadservletforreferences.do' >
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblSalutation" /></label>
					<select class="form-control" id="salutation" name="salutation">
						<option value="0"></option>   
						<option value="4"><spring:message code="optDr" /></option>
						<option value="3"><spring:message code="optMiss" /></option>
						<option value="2"><spring:message code="optMr" /></option>
						<option value="1"><spring:message code="optMrs" /></option>
						<option value="5"><spring:message code="optMs" /></option>													
					</select>
				</div>
					
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblFname" /><span class="required">*</span></label>
					<input type="hidden" id="elerefAutoId">
					<input type="text" id="firstName" name="firstName" class="form-control" placeholder="" maxlength="50">
				</div>
				
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblLname" /><span class="required ">*</span></label>
					<input type="text" id="lastName" name="lastName" class="form-control" placeholder="" maxlength="50">
				</div>
					
			</div>
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblTitle" /><span class="required hide PHLreq">*</span>&nbsp;<a href="#" id="destool" rel="tooltip" data-original-title=<spring:message code="msgRecommenderTitle" />><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
					<input type="text" id="designation" name="designation" class="form-control" placeholder="" maxlength="50">
				</div>
				
				<div class="col-sm-6 col-md-6">
					<label><spring:message code="lblOrganizationEmp" /><span class="required hide nobleCssShow PHLreq">*</span></label>
					<input type="text" id="organization" name="organization" class="form-control" placeholder="" maxlength="50">
				</div>
				
			</div>
			
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<label><spring:message code="lblContNum" /><span class="required">*</span>&nbsp;<a href="#" id="iconpophover5" rel="tooltip" data-original-title="Phone number (area code first)"><img src="images/qua-icon.png" width="15" height="15" alt=""></a></label>
					<input type="text" id="contactnumber" name="contactnumber" class="form-control" placeholder="" maxlength="50">
				</div>
				
				<div class="col-sm-6 col-md-6">
					<label><spring:message code="lblEmail" /><span class="required">*</span></label>
					<input type="text" id="email" name="email" class="form-control" placeholder="" maxlength="50">
				</div>
			</div>
			
			<div class="row top15 nobleCssHide ${transUDis}">
				<div class="col-sm-4 col-md-4">
					<label>
						<spring:message code="msgRecommendationLetter" /><span class="required hide" id="recommLetter">*</span>
					</label>
					<input type="hidden" id="sbtsource_ref" name="sbtsource_ref" value="0"/>
					<input id="pathOfReferenceFile" name="pathOfReferenceFile" type="file" width="20px;">
				    <a href="javascript:void(0)" onclick="clearReferences()"><spring:message code="lnkClear" /></a>
			    </div>
				<input type="hidden" id="pathOfReference"/>
				<div class="col-sm-3 col-md-3" id="removeref" name="removeref" style="display: none;">
					<label>
						&nbsp;&nbsp;
					</label>
					<span id="divRefName">
					</span>
					<a href="javascript:void(0)" onclick="removeReferences()"><spring:message code="lnkRemo" /></a>&nbsp;&nbsp;&nbsp;
						<a href="#" id="iconpophover7" rel="tooltip" data-original-title="<spring:message code="msgRemoveRecommendation" />"><img src="images/qua-icon.png" width="15" height="15" alt=""></a>
				</div>
		    </div>
			
			<div class="row top15 nobleCssHide ${transUDis}">
			    <div class="col-sm-5 col-md-5">
				    <label><spring:message code="lblPrsnDirectlyContByHiringAuth" /></label>
				    <div class="" id="" style="height: 40px;">
				    	<div class="radio inline col-sm-1 col-md-1">
					    	<input type="radio" checked="checked" id="rdcontacted1" value="1"  name="rdcontacted"> <spring:message code="lblYes" />
					    </div>
					    </br>
					    <div class="radio inline col-sm-1 col-md-1" style="margin-left:20px;margin-top:-10px;">
					    	<input type="radio" id="rdcontacted0" value="0" name="rdcontacted"> <spring:message code="lblNo" />
					    </div>
				    </div>
			    </div>
			    <div class="col-sm-3 col-md-3 hide onlyPHL">
					<label><spring:message code="qnhowlongknwperson" /><span class="required">*</span></label>
					<input type="text" id="longHaveYouKnow" name="longHaveYouKnow" class="form-control" placeholder="">
				</div>
			</div>
			
		
				<div class="row top10">
			    <div class="col-sm-9 col-md-9" >
				    <label><spring:message code="qnhowlongknwperson" /></label>
				    <div id="referenceDetailText"><textarea rows="" cols=""></textarea></div>
				    				    
			    </div>
			</div>
			
			<div class="row">
			    <div class="col-sm-3 col-md-3">
			    	<a href="#" style="cursor: pointer; text-decoration:none;" onclick="return insertOrUpdateElectronicReferences(0)"><spring:message code="lnkImD" /></a>&nbsp;
			    	<a href="#" style="cursor: pointer; text-decoration:none;" onclick="return hideElectronicReferencesForm()">
						<spring:message code="btnClr" />
					</a>
			    </div>
			</div>
											
			</form>
		
	</div>
</div>
<br/>
<!--<div class="accordion" id="accordion50">
<div id="collapseOne" style="height:auto;" >
	<div class="accordion-inner">-->

<div id="tfaTeacherDiv" class="row left10" style="margin-top:-35px;">
		<div class="row portfolio_Section_Gap">
			<div class="col-sm-4 col-md-4">
				<div class="span4 portfolio_Subheading"><spring:message code="msgTeachTFAAffiliate" /></div>
			</div>
		</div>
			
		<div class="row">
			<div class="col-sm-9 col-md-9">
				
			</div>
		</div>	
		<div class="row">
			<div class="col-sm-4 col-md-4">
				<label>
					<strong><spring:message code="msgTeachTFAAffiliate" /><span class="required philNT tfarequired" id="tfarequired" style="display: none;">*</span>
					</strong>
				</label>
				<select  class="form-control" id="tfaAffiliate" onchange="hideTFAFields_DP()">   
					<option value=""><spring:message code="lblSelectTFA" /></option>
						<c:forEach items="${lstTFAAffiliateMaster}" var="lstTFAAffilate">
						<c:set var="selected" value=""></c:set>					
			        		<c:if test="${lstTFAAffilate.tfaAffiliateId eq teacherExperience.tfaAffiliateMaster.tfaAffiliateId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
			        		<option id="${lstTFAAffilate.tfaAffiliateId}" value="${lstTFAAffilate.tfaAffiliateId}" ${selected}>${lstTFAAffilate.tfaAffiliateName}</option>
						</c:forEach>
						
				</select>		
			</div>
			<c:set var="hide" value=""></c:set>
			<c:if test="${teacherExperience.tfaAffiliateMaster.tfaAffiliateId eq 3}">
				<c:set var="hide" value="hide"></c:set>
			</c:if>
			
			<div id="tfaFieldsDiv" class="${hide}">
				<div class="col-sm-3 col-md-3">
					<label>
						<strong><spring:message code="lblCorpsYear" /><span class="required tfarequired">*</span></strong>
					</label>
					<select  class="form-control" id="corpsYear">   
						<option value=""><spring:message code="lblSltCorpsYear" /></option>
							 <c:forEach var="corps" items="${lstCorpsYear}">
							 <c:set var="selected" value=""></c:set>					
			        		<c:if test="${corps eq teacherExperience.corpsYear}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
							 	
								<option id="corps${corps}" value="${corps}" ${selected}>${corps}</option>
							</c:forEach>	
					</select>		
				</div>
				<div class="col-sm-4 col-md-4">
					<label>
						<strong><spring:message code="lblTFARegion" /><span class="required philNT tfarequired">*</span></strong>
					</label>
					<select  class="form-control"  id="tfaRegion" onchange="">  
						<option value=""><spring:message code="lblSelectTFARegion" /></option>
						 <c:forEach items="${lstTFARegionMaster}" var="lstTFARegion">
						 
						 <c:set var="selected" value=""></c:set>					
			        		<c:if test="${lstTFARegion.tfaRegionId eq teacherExperience.tfaRegionMaster.tfaRegionId}">	        			
			         			<c:set var="selected" value="selected"></c:set>	         			
			        		</c:if>	
						 
			        		<option id="${lstTFARegion.tfaRegionId}" value="${lstTFARegion.tfaRegionId}" ${selected}>${lstTFARegion.tfaRegionName}</option>
						</c:forEach>
						
					</select>		
				</div>
			</div>
		</div>
	</div>
	
	<!-- tfa option phil-->
	<div id="tfaDistSpecificoption" class="hide">	
	<div class="row portfolio_Section_Gap">
			<div class="col-sm-12 col-md-12">
				<div class=""><font class="portfolio_Subheading"><spring:message code="lblProgramParticipation" /></font><span class="required philNT">*</span></div>
			</div>
		</div>
		<div class="row">		
			<div class="col-sm-9 col-md-9">
				
			</div>
		</div>
		<div class="row" id="tfaDistOptData">		
		</div>
		
	</div>
	<!--end tfa option -->
	
	<div id="substituteTeacherDiv" class="row left10" style="margin-top:-20px;">
		<div class="row portfolio_Section_Gap">
			<div class="col-sm-6 col-md-6">
				<div class="span4 portfolio_Subheading"><spring:message code="lblSubstituteTeacher" /></div>
			</div>
		</div>
			
		<div class="row">		
			<div class="col-sm-9 col-md-9">
				
			</div>
		</div>	
		<div class="row">	
			<div class="col-sm-12 col-md-12">
				<label class="span15">
					<strong><spring:message code="qnWillingToserveSubTeacher" /><span class="required philNT" id="sSubTrequired" style="display: none;">*</span>
					</strong>
				</label>
			</div>	
		</div>
		  
	   <div class="row left1">  	        
		        
		       <div class="col-sm-1 col-md-1 radio top1" style="margin-top:-0px;">
		        <input type="radio" id="canServeAsSubTeacher0" value="0" name="canServeAsSubTeacher"  <c:if test="${teacherExperience.canServeAsSubTeacher eq 0 or teacherExperience.canServeAsSubTeacher eq null}">checked</c:if> > 
		        <spring:message code="lblNo" />
		        </div>
		       
		         <div class="col-sm-1 col-md-1 radio top1" style="margin-top:-0px;">
		        <input type="radio" id="canServeAsSubTeacher1" value="1" name="canServeAsSubTeacher" <c:if test="${teacherExperience.canServeAsSubTeacher eq 1}">checked</c:if>> 
		       <spring:message code="lblYes" />
		        </div>
		</div>
	</div>
	<div id="resumeDiv" class="row left10" style="margin-top:-30px;">
<iframe id='uploadFrameResumeID' name='uploadFrameResume' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
	<form id='frmExpResumeUpload' enctype='multipart/form-data' method='post' target='uploadFrameResume' action='expResumeUploadServlet.do?f=${teacherExperience.resume}' class="form-inline">	
	<div class="row portfolio_Section_Gap">
		<div class="col-sm-6 col-md-6">
			<div class="span4 portfolio_Subheading"><spring:message code="lblResume" />&nbsp;<spring:message code="msgPDFPreferred" /></div>
		</div>
	</div>
	
	<div class="row">
    	<div class="col-sm-12 col-md-12" style="margin-bottom:-10px;">
        	<spring:message code="msgAbtYurResume" />
			
		</div>
		<br>
			<div class="col-sm-12 col-md-12">
				
			</div>	
			<div class="col-sm-4 col-md-4"> 
				<input type="hidden" id="hdnResume" value="" /> 
				<label><spring:message code="lblResumePleaseUploadAFile" /><span class="required" id="requiredRessume"></span></label><br/>
				<input name="resume" id="resume" type="file"><br/>
				<div id="divResumeTxt">
					 <label id="lblResume">
						<spring:message code="lblRecentResOnFi" />
						<c:if test="${empty teacherExperience.resume}">
   													None
   						</c:if>
						<a href="javascript:void(0)" id="hrefResume" onclick="downloadResume();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;">${teacherExperience.resume}</a>
						</label>
				</div>
			</div>	
       </div>
       </form>
   </div>

	<div class="portfolio_Section_Gap" id="expCertTeacherTrainingDiv" style="margin-top:5px; margin-bottom:-15px;">
	<div class="row left10">
	    <div class="row">
			<div class="col-sm12 col-md-12">
				
			</div>	
		</div>
	   	<div class="row">	
	   	<div class="col-sm-3 col-md-3"> 	
	    		<label>
					<strong style="max-width: 230px;"><spring:message code="lblCertiTeachExp" /><span class="required teacherExpReq">*</span>
						
					</strong>
				</label>						
				<input type="text" id="expCertTeacherTraining" name="expCertTeacherTraining"  <c:if test="${teacherExperience.isNonTeacher}">disabled="true"</c:if> class="form-control" onkeypress="return checkForDecimalTwo(event);" maxlength="5" value="${teacherExperience.expCertTeacherTraining }">    		
	   	
	   	</div>
	   	<div class="col-sm-4 col-md-4">
	   	
	   		<input type="checkbox" id="isNonTeacher" name="isNonTeacher" <c:if test="${teacherExperience.isNonTeacher}">checked</c:if>  onclick="return chkNonTeacher();" style="margin-top:12%;">&nbsp;&nbsp;<spring:message code="lblIMNotACertiTech" />
	   	</div>
	   	 </div>
	   	  </div>
   </div>
   
    <div class=" portfolio_Section_Gap" id="nationalBoardCertDiv" style="margin-bottom:-25px;">
     <div class="row left10">
         <div class="row">
	   		<div class="col-sm12 col-md-12">
	   			
	   		</div>
	    	<div class="col-sm12 col-md-12">
	    		<div>
		    		<label>
						<strong><spring:message code="lblNatiBoardCerti/Lice" /><span class="required">*</span></strong>
					</label>
				</div>
			</div>	
	    </div>
	       <div class="row left1">			
	      
	       <div class="col-sm-1 col-md-1 radio top1" style="margin-top:-0px;">
		       <input type="radio" value="nbc2" id="nbc2" name="nationalBoardCert" <c:if test="${teacherExperience.nationalBoardCert eq false or teacherExperience.nationalBoardCert eq null}">checked</c:if>   onclick="chkNBCert('2')">
		        <spring:message code="lblNo" />
		          </div>
		     <div class="col-sm-1 col-md-1 radio top1" style="margin-top:-0px;">   
		        <input type="radio" value="nbc1" id="nbc1" name="nationalBoardCert" <c:if test="${teacherExperience.nationalBoardCert}">checked</c:if> onclick="chkNBCert('1')">
		       <spring:message code="lblYes" />
		     </div>    
		  </div>
		  
		  	
		  <div class="row top15">		
		  <div class="col-sm-3 col-md-3">
			<div class='hide' id="divNbdCert" name="divNbdCert"  <c:choose>  <c:when test="${teacherExperience.nationalBoardCert}">style='display: block;'</c:when><c:otherwise>'</c:otherwise></c:choose> />
				<select id="nationalBoardCertYear" name="nationalBoardCertYear"  class="form-control" style="max-width: 230px;" >
					<option value="" id="psl"><spring:message code="optSlt" /></option>
					<c:forEach var="year" items="${lstLastYear}">	
						<c:set var="selected" value=""></c:set>					
		        		<c:if test="${year eq teacherExperience.nationalBoardCertYear}">	        			
		         			<c:set var="selected" value="selected"></c:set>	         			
		        		</c:if>							
						<option id="nationalBoardCertYear${year}" value="${year}" ${selected} >${year }</option>
					</c:forEach>					
				</select>
				</div>	
			</div>				
    	</div>
   </div> 
     </div> 
     
     <div id="QuestionDiv" class="row left10">	
	<div class="row portfolio_Section_Gap">
		<div class="col-sm-6 col-md-6">
			<div class="span4 portfolio_Subheading"><spring:message code="lblDistrictSpecificQuestions" /></div>
			 <div class='divErrorMsg' id='errordivspecificquestion' style="display: block;"></div>
			 <div class='divErrorMsg' id='errordivspecificquestionUpload' style="display: block;"></div>
		</div>
	</div>

	<div class="row">
	    <div class="col-sm-9 col-md-9">
		<div id="divGridDistrictSpecificQuestions"></div>
		</div>
	</div>
	
   </div>
   
 <c:if test="${tportfolioStatus.isAffidavitCompleted eq false or tportfolioStatus.isAffidavitCompleted eq null}">
   <div  id="affidavitDiv" class="row left10">
	      <div class="row">
	   		<div class="col-sm12 col-md-12">
   			<div class='divErrorMsg' style="display: none;" id="errAffidavit"></div>
   			</div>
   			<div class="col-sm12 col-md-12">
   			    <div class="row">
   			    	<div class="col-sm1 col-md-1" style="width: 20px;">
	   				<input type="checkbox" id="affidavit" name="affidavit" <c:if test="${tportfolioStatus.isAffidavitCompleted}">checked</c:if>>
	   				</div>
		   			<div class="col-sm10 col-md-10">					
						<spring:message code="pConfirm" />
						<a href="#" onclick="showAffidavitDiv()"><spring:message code="lnkShowAffidavitDetails" /></a>
					</div>
   			    </div>	   				   
			</div>
	      </div> 	       
	  </div> 
  
   </c:if>
   
<!--   </div>
 	</div>
 	</div>-->
   
   <div class="portfolio_Section_Gap" style="margin-top:40px;">
   <div class="row left10">
		<button  class="btn btn-large btn-primary" type="button" id="dynamicSaveDspq" onclick="saveAndContinueDynamicPortfolio();"  ><strong><spring:message code="lblSavePortfolio" /> <i class="icon"></i></strong></button>
	</div>
 	</div>
 	
 	
 	
   
  
   
   <!-- Popup -->
   
   
   <div  class="modal hide" id="affidavitDataDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-teacherprofile">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
		<h3 id="myModalLabel"><spring:message code="headAffidavit" /></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto; overflow-x: hidden;">
		<div class="scrollspy-example" data-offset="0" data-target="#navbarExample" data-spy="scroll">
			
			<p style="text-indent: 0px;"><spring:message code="pUseCarefullyBeforeUsingTheTm" />
			</p>
			
			<div style="text-align: center; padding-bottom: 5px;">
				<spring:message code="pCopyrightTm" /><br/>
				<spring:message code="pAllRiRe" /><br/>
			</div>
			
			<p>
				<b><spring:message code="pOWNERSHIP" /></b>  <spring:message code="pEachOfItsCompIsCopyrightedPropertyOfTm" />  
			</p>
			<p>
				<b><spring:message code="pAgeAdRespo" /></b>  <spring:message code="pYuRepresntAtLeast(18)Yr" />  
			</p>
			<p>
				<b><spring:message code="pAccesingThePorAdAccSecurity" /></b>  <spring:message code="pEachTchCandMayBeProvidedWithUseraAdPss" />			
				<br/><br/><span  class="spnIndent"></span><spring:message code="pInConnWithEstablishingYurAccWithUs" /></span> 
				<br/><br/><span  class="spnIndent"></span><spring:message code="pYuAcknowledgeAdAgreeThatTmHasNoResponsibility" /> <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a>.  <spring:message code="pInAddDelAccCodeAtAnyTimeAdReason" /></span>
			</p>
			<p>
				<b><spring:message code="pSubmissions" /></b>  <spring:message code="pAllInfoOfTm" />
				<br/><br/><span  class="spnIndent"></span><spring:message code="pNoConfidentialRelEstabBySubMinssionByAnyCandTch" />  
			</p>
			<p>
				<b><spring:message code="pRulOfCon" /></b> <spring:message code="pWhileUThPortYuWillApplicableLaws" />
				<div class="spnIndent40">
				<ul>  
					<li><spring:message code="pManyInfoOfTm" /></li>
					<li><spring:message code="pCivilLiability" /></li>
					<li><spring:message code="pVirusWormTrHorsEasterEggTime" /></li>
					<li><spring:message code="pAnyPersIdentiInfoOfAthrIndividual" /></li>
					<li><spring:message code="pMateralNonPuInfoAbtCompany" /></li>
				</ul>
				</div>
				<spring:message code="pFurtherInConnWithYurUseOfPortal" />
				<div class="spnIndent40" style="padding-top: 5px;">
				<ul>
					<li><spring:message code="pUseThePorForFraudulent" /></li>
					<li><spring:message code="pImperAnyPersonOrEntity" /></li>
					<li><spring:message code="pInterfaceWiDisruptUseNet" /></li>
					<li><spring:message code="pRestOrInhibit" /></li>
					<li><spring:message code="pModiAdTransReveEngineer" /></li>
					
				</ul>
				</div>
			</p>
			<p>
				<b><spring:message code="pUSEOFPORTAL" />
			</p>  
			<p>
				<b><spring:message code="pPRIVACY" /></b><br/>				
				
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY1" /><br/> <br/>
					</div>
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY2" /><br/>  
					<br/><spring:message code="pPRIVACY3" />    <br/><br/>
					</div>					
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY4" /><br/><br/>  
					</div>
					<div  style="padding-left: 50px; "><spring:message code="pPRIVACY5" /><br/>
					<ul>
						<li><spring:message code="pPRIVACYL1" /></li>
						<li><spring:message code="pPRIVACYL2" /></li>
						<li><spring:message code="pPRIVACYL3" /></li>
						<li><spring:message code="pPRIVACYL4" /></li>
						<li><spring:message code="pPRIVACYL5" /></li>
					</ul>
					</div>
				
			</p>
			<p>
				<b><spring:message code="p8USEOFDATA" /></b> <spring:message code="p8USEOFDATA1" />  
				<br/><br/><span  class="spnIndent"></span><spring:message code="p8USEOFDATA2" />  
				<br/><br/><span  class="spnIndent"></span><spring:message code="p8USEOFDATA3" />  
				<br/><br/><span  class="spnIndent"><spring:message code="p8USEOFDATA4" />  
				<br/><br/><span  class="spnIndent"><spring:message code="p8USEOFDATA5" /></span>  
			</p>
			<p>
				<b><spring:message code="head9EXCLUSIONOFWARRANTY" /></b> <spring:message code="pEXCLUSIONOFWARRANTY" />
			</p>
			<p>
				<b><spring:message code="head10LIMITATIONOFLIABILITY" /></b>  <spring:message code="p10LIMITATIONOFLIABILITY" /> 

				<span  class="spnIndent"></span><br/><br/><spring:message code="p10LIMITATIONOFLIABILITY2" />
				
				<span  class="spnIndent"></span><br/><br/><spring:message code="p10LIMITATIONOFLIABILITY3" />
			</p>
			<p>
				<b><spring:message code="head11INDEMNIFICATION" /></b>  <spring:message code="p11INDEMNIFICATION" />
			</p>
			<p>
				<b><spring:message code="head12RELATIONSHIP" /> </b> <spring:message code="p12RELATIONSHIP" />
			</p>
			<p>
				<b><spring:message code="head13GOVERNINGLAW" /></b>  <spring:message code="p13GOVERNINGLAW" />
			</p>
			<p>
				<b><spring:message code="head14ASSIGNMENT" /></b>  <spring:message code="p14ASSIGNMENT" />
			</p>
			<p>
				<b><spring:message code="head15MODIFICATION" /></b>  <spring:message code="p15MODIFICATION" />  
			</p>
			<p>
				<b><spring:message code="head16SEVERABILITY" /></b>  <spring:message code="p16SEVERABILITY" />
			</p>
			<p>
				<b><spring:message code="head17HEADINGS" /></b>  <spring:message code="p17HEADINGS" />
			</p>
			<p>
				<b><spring:message code="head18ENTIREAGREEMENT" /></b>  <spring:message code="p18ENTIREAGREEMENT" />
			</p>	
			<p  style="text-indent: 0px;">
				<spring:message code="headHOWTOCONTACTUS" /> <spring:message code="pComments" /> <a href="mailto:clientservices@teachermatch.net">clientservices@teachermatch.net</a> <spring:message code="pWrtUsAt" />
			</p>
			
				<div style="margin-left: 260px;">			
				<spring:message code="pTmLLC" /><br/>
				<spring:message code="p4611NRaveU" /><br/>
				<spring:message code="pChiIL60640" /><br/>
				<spring:message code="pAttentionTe" /><br/><br/>
				</div>
				
			
			<spring:message code="pLaRevJune" />
			<hr/>
			<p style="text-indent: 0px;" >
				<spring:message code="msgDp_commons45" />  
			</p>
			<hr/>
			<p style="text-indent: 0px;" >
				<spring:message code="pTmPotentialEmp" />
			</p>  
		</div>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button>
 	</div>
</div>
	</div>
</div>
   
   
   <div  class="modal hide" id="draggableDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-cgdemoschedule">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="lblCert/LiceName" /></h3>
	</div>
	<div class="modal-body">
		<p><spring:message code="msgWeArSoryYuNotFindingYurCerti/Lice" /></p>
		<p><spring:message code="msgPlzTypingStringInFldCerti/Lice" /></p>
		<p><spring:message code="pNotifyEmailService" /> 
		<a href="mailto:clientservices@teachermatch.net" target="_top">clientservices@teachermatch.net</a>
		 <spring:message code="pHlpYuLoadRiLice/Certi" /></p> 
		<p><spring:message code="pThkForYurHlpInThisMtr" /></p>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
 	</div>
</div>
	</div>
</div>
   
   <div class="modal hide"  id="chgstatusRef1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUActRef" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeActRef" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
	</div>
</div>
   <div class="modal hide"  id="deleteCertRec" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="certificateTypeID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteCertificateRecord()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
 	</div>
</div>
   <div class="modal hide"  id="removeUploadedDocument" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="removeUploadedDocument()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
	</div>
	</div>
</div>
   <div class="modal hide"  id="deleteAcademicRecord"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="academicID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteAcademicRecord()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
	</div>
</div>	
<div class="modal hide"  id="deleteEmpHistory"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDeleteTheRecord" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<input type="hidden" id="empHistoryID"/>
 	<span id=""><button class="btn  btn-primary" onclick="deleteEmploymentDSPQ()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgDoUDeleteVideoLnk" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button> 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="wrongDivPHiL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" onclick="closeDspqOPenCl();" aria-hidden="true">x</button>
        <h3 id="myModalLabel">TeacherMatch</h3>
    </div>
    <div class="modal-body">        
        <div class="control-group" id="dynamicInfoMsg"></div>
    </div>
    <div class="modal-footer">    
    <span id=""><button class="btn  btn-primary" onclick="closeDspqOPenCl();" ><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;               
    </div>
</div>
    </div>
</div>

<div class="modal hide" id="updateAndSaveDSPQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideAllProfaileDiv();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="redirectLabel" class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgProfupdSucc" />
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="hideAllProfaileDiv();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp; 		
 	</div>
</div>
	</div>
</div>

   <div style="display:none;" id="loadingDiv">
	     <table  align="left" >
	 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
	 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'><spring:message code="msgYrFileBegUpLod" /></td></tr>
		  </table>
   </div>
<input type="hidden" id="countryCheck" value="">
<script type="text/javascript">
showPortfolioReminder();
//getCityListByStateForDSPQ();
//document.getElementById("isMiami").value=${isMiami};
</script>
<!--<script type="text/javascript">
	$('.accordion').collapse();
	$('.accordion').on('show', function (e) {
		$(e.target).prev('.accordion-heading').find('.accordion-toggle').addClass('minus').removeClass('plus');
	});

	$('.accordion').on('hide', function (e) {
		$(this).find('.accordion-toggle').not($(e.target)).removeClass('minus').addClass('plus');
	});
</script>

--><div style="display:none; z-index: 5000;" id="loadingDiv" >
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>					
	</table>
</div>
<script type="text/javascript">//<![CDATA[
     cal.manageFields("certiExpiration", "certiExpiration", "%m-%d-%Y");
     dspqCalInstance=cal;
</script>
<script language="javascript">
//getRacedata();
document.getElementById("isMiami").value=${isMiami};
try{document.getElementById("districtIdForDSPQ").value=${districtMasterDSPQ.districtId};}catch(ee){}
try{document.getElementById("teacherIdForDSPQ").value=${teacherIdForDSPQ.teacherId};}catch(ee){}
</script>
<!-- <script type="text/javascript">
    $('#iconpophover1').tooltip();
    $('#iconpophoverSSN').tooltip();
	$('#iconpophover2').tooltip();
	$('#iconpophover4').tooltip();
	$('#iconpophover10').tooltip();
	$('#iconpophoverOECQ').tooltip();
	$('#iconpophoverSolutation').tooltip();
	$('#iconpophoverPhone').tooltip();
	$('#academicHelpTooltip').tooltip();
	$('#ssubtTooltip').tooltip();
	$('#iconpophover11').tooltip();
</script>  -->

