<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="tm.utility.Utility,tm.bean.TeacherPersonalInfo,java.lang.String" %>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DWRAutoComplete.js"></script>
<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resourceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/TeacherProfileViewInDivAjax.js?ver=${resourceMap['TeacherProfileViewInDivAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resourceMap['js/assessment-campaign.js']}"></script>
<script type='text/javascript' src="js/teacher/dashboard.js?ver=${resourceMap['js/teacher/dashboard.js']}"></script>
<script type="text/javascript" src="js/jquery-te-1.3.2.2.min.js" charset="utf-8"></script>
<link type="text/css" rel="stylesheet" href="css/jquery-te-1.3.2.2.css" charset="utf-8" />

<!-- Start ... Dynamic Portfolio -->
<script type="text/javascript" src="dwr/interface/DistrictPortfolioConfigAjax.js?ver=${resourceMap['DistrictPortfolioConfigAjax.ajax']}"></script>
<script type="text/javascript" src="js/teacher/dynamicPortfolio_Common.js?ver=${resourceMap['js/teacher/dynamicPortfolio_Common.js']}"></script>

<script type="text/javascript" src="dwr/interface/DSPQServiceAjax.js?ver=${resourceMap['DSPQServiceAjax.ajax']}"></script>

<!-- End ... Dynamic Portfolio -->
<!--  Non-Client Jobs -->
<script type="text/javascript" src="dwr/interface/DashboardAjax.js?ver=${resouceMap['DashboardAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JBServiceAjax.js?ver=${resourceMap['JBServiceAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/interface/HrmsLicenseAjax.js?ver=${resourceMap['HrmsLicenseAjax.ajax']}"></script>
<script type="text/javascript" src="js/jobboard/jobboard.js" charset="utf-8"></script>


<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>

<script src="js/bootstrap.min.js"></script>
 <script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<script>
function applyScrollOnTbl()
{
	   			
}



function applyScrollOnTb()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#schoolListGrid').fixheadertable({  
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 680,
        minWidth: null,
        minWidthAuto: false,
        colratio:[350,330],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: true,
        sortedColId: null,
        sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });		
}


</script>

<script type="text/javascript">
var isCoverLetterNeeded = ${isCoverLetterNeeded};

    $(function() {
        var hq=$("#haedQId").val();  
    	var data = $("#myDiv").html();
        var limit = 1000;
        var chars = $("#myDiv").text(); 
          if(hq==2)
        {   
        }
        else
        {  
        if (chars.length > limit) 
        {
			document.getElementById('linkVal').innerHTML="Expand";
            var visiblePart = $("<div class='topbar' id='tmpDiv'> "+ data +"</div>");
            var readMore = $("<span class='read-more'></span>");
            $("#myDiv").empty().append(visiblePart).append(readMore);
                $("#linkD").click(function()
                {
					if(document.getElementById('linkVal').innerHTML=="Collapse")
					{
						document.getElementById('linkVal').innerHTML="Expand";
				 		$("#myDiv").empty().append(visiblePart).append(readMore);   
                		$("#tmpDiv").fadeIn();          
					}
					else{
						if($("#linkVal").text()=="Expand")
						{
			              	$("#linkVal").text("Collapse");
			              	$("#myDiv").hide();
			              	$("#myDiv").append(data);
			              	$("#myDiv").fadeIn();
			              	$("#tmpDiv").hide();
						}
					}
	  		});
	  		$("#linkS").click(function()
                {
					if(document.getElementById('linkVal').innerHTML=="Collapse")
					{
						document.getElementById('linkVal').innerHTML="Expand";
				 		$("#myDiv").empty().append(visiblePart).append(readMore);   
                		$("#tmpDiv").fadeIn();          
					}
					else{
						if($("#linkVal").text()=="Expand")
						{
			              	$("#linkVal").text("Collapse");
			              	$("#myDiv").hide();
			              	$("#myDiv").append(data);
			              	$("#myDiv").fadeIn();
			              	$("#tmpDiv").hide();
						}
					}
	  	});
        }
        else
        {
        	//alert("Gagan");
        	//document.getElementById('linkVal').innerHTML="Expand";
        }
        }
    });
</script>
<style type="text/css">
    .topbar{
    display:block;
    width:100%;
    height:45px;
    overflow:hidden;
    }
    .padding8
    {
    	padding-top: 8px;
    }
    .padding13
    {
    	padding-top: 13px;
    }
    
    a {
  color: #007AB4;/* Gagan : Linlk: href color*/
  text-decoration: none;
  font-size: 13px;
}
 a:hover
 {
 	color: #000000;
 	font-size: 13px;
 }   
    
</style>

<link rel="stylesheet" href="css/dialogbox.css" />
<link rel="stylesheet" type="text/css" href="css/base.css" />  
<style>
.tmlogo{
	pointer-events: none;
}
</style>   

<c:choose>
	<c:when test="${fn:contains(header.referer, 'jobsboard.do')}">
		<c:set var="redirectURL" value="${header.referer}" />
	</c:when>
	<c:otherwise>
		<c:set var="redirectURL" value="http://www.teachermatch.org/" />
	</c:otherwise>
</c:choose>

<input type="hidden" id="isDspqReqForKelly" value="${isDspqReqForKelly}"/>
<input type="hidden" id="isKelly" value="${isKelly}"/>
<input type="hidden" id="isExternal" value="1"/>
<input type="hidden" id="dID" name="dID" value="${jobOrder.districtMaster.districtId}"/>
<input type="hidden" id="jCat" name="jCat" value="${jobOrder.jobCategoryMaster.jobCategoryName}"/>
<input type="hidden" id="teacherId" name="teacherId" value="${teacherId}"/>
<input type="hidden" id="haedQId" value="${HQID}" />

<div class="row">  
   <div class="col-sm-4 col-md-4">     
   <div class="subheading" style="margin: 0px;"><spring:message code="lnkApplyForJob" /></div>
   </div> 	             
</div>
		<div class="row" >
			<div class="col-sm-12 col-md-12">
				<table  border="0px" cellpadding="0" cellspacing="0" width="940px;">
					<c:set var="emailForTeacher"></c:set>
					<c:set var="districtOrSchoolMessageFlag"  value="0"></c:set>
					<c:choose>
						<c:when test="${not empty schoolMaster}">
						<c:set var="emailForTeacher" value="${schoolMaster.emailForTeacher}"/>
						
							<c:if test="${schoolMaster.allowMessageTeacher==1}">
								<c:set var="districtOrSchoolMessageFlag" value="1"/>
							</c:if>
						
							<tr>						
								<td>${schoolMaster.schoolName}</td>
								<td style="width:100px;"><b>Job posting ${jobOrder.jobId}</b></td>
							</tr>
							<tr>												
								<td>${schoolFullAddress }</td><!-- ${schoolMaster.address} -->
								<c:set var="endDateDis" value=""></c:set>
							<c:choose>
								<c:when test="${endDate eq 'Dec 25, 2099'}">
									<c:set var="endDateDis" value="Until Filled"></c:set>
								</c:when>
								<c:otherwise>
									<c:set var="endDateDis" value="${endDate},11:59 PM CST"></c:set>
								</c:otherwise>
							</c:choose>
								<td style="width:420px;"><b><spring:message code="lbljobPostedAs" /> ${startDate},<spring:message code="lblDateUntill" /> ${endDateDis}</b></td>								
							</tr>
							<tr>						
								<td style="padding-top:10px;padding-bottom:10px;"><img  src="${logoPath}"></td>
								<td align="right"  valign="bottom"   style="padding-bottom:10px;width:50px;">
									<c:set var="description" value="${schoolMaster.description}"/>
									<a href="#" id="linkS"><div id="linkVal"><%--Gagan --%></div></a>
								</td>
							</tr>
							<tr>
								<td colspan="2"   style="text-align: justify;" >
									<div id="myDiv" style="border: 0px solid ;">${description}</div>
								</td>
							</tr>
						</c:when>
						<c:otherwise>
						<c:set var="emailForTeacher" value="${districtMaster.emailForTeacher}"/>
							<c:if test="${districtMaster.allowMessageTeacher==1}">
								<c:set var="districtOrSchoolMessageFlag" value="1"/>
							</c:if>
							<tr>						
								<td>
								<c:choose>
							    <c:when test="${not empty districtMaster.displayName}">
							       ${districtMaster.displayName}
							    </c:when>    
							    <c:otherwise>
							       ${districtMaster.districtName}
							    </c:otherwise>
							</c:choose>
								</td>
								<td style="width:100px;"><b><spring:message code="lblJobposting" /> ${jobOrder.jobId}</b></td>
							</tr>
							<tr>						
								<td>${districtFullAddress}</td>
								<c:set var="endDateDis" value=""></c:set>
							<c:choose>
								<c:when test="${endDate eq 'Dec 25, 2099'}">
									<c:set var="endDateDis" value="Until Filled"></c:set>
								</c:when>
								<c:otherwise>
									<c:set var="endDateDis" value="${endDate},11:59 PM CST"></c:set>
								</c:otherwise>
							</c:choose>
								<td style="width:420px;"><b><spring:message code="lbljobPostedAs" /> ${startDate},<spring:message code="lblDateUntill" /> ${endDateDis}</b></td>								
							</tr>
							<tr>						
								<td style="padding-top:10px;padding-bottom:10px;"><img  src="${logoPath}"></td>
								<td align="right" valign="bottom"    style="padding-bottom:10px;width:50px;">
									<c:set var="description" value="${districtMaster.description}"/>
									<a href="#" id="linkD"><div id="linkVal"></div></a>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align: justify;">
									<div id="myDiv" style="border: 0px solid ;">${description}</div>
								</td>
							</tr>
						</c:otherwise>
					</c:choose>		
					</table>
			</div>
			<div class="col-sm-2 col-md-2" style="text-align: right;padding-right: 20px;">
				<c:choose>
				<c:when test="${districtOrSchoolMessageFlag==1}">
					<a href="#" onclick="return openMessageDiv();"><strong><spring:message code="lnkContactUs" /></strong></a>
				</c:when>
				</c:choose>
			</div>
		</div>
		
		
 <div class="centerline top10"></div> 
				
			<div class="row">
				<div class='divErrorMsg  col-sm-12 col-sm-12' id='errordiv' style="padding-bottom: 10px; display: block;" ></div>	
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-6 mt10">				
						<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblJoTil" /></strong></div>					
						${jobOrder.jobTitle}${batchJobOrder.jobTitle}
				</div>
				<div class="mt10 pull-right" style="margin-right: 15px;">
					<c:choose>
								<c:when test='${(jobOrder.districtMaster.jobApplicationCriteriaForProspects ne 0 && empty jobOrder.headQuarterMaster) }'>
		        <button class="btn btn-large btn-primary" type="button" onclick="chkApplyJobNonClient('${jobOrder.jobId}','${jobOrder.districtMaster.jobApplicationCriteriaForProspects}','${jobOrder.exitURL}','${jobOrder.districtMaster.districtId}','');"><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" ><spring:message code="btnClr" /></a><br>
		        </c:when>
		        
		        
		        <c:when  test="${ not empty teacherDetail  && not empty teacherId && teacherId ne 0}">
		         <button class="btn btn-primary  " type="button" onclick="chkApplyJobAuthenticate('${jobOrder.jobId}') "><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" id="backbtn"><spring:message code="btnClr" /></a><br>
		       </c:when>
		         <c:otherwise>
		         		<c:choose>
					         <c:when test="${not empty kellyUrl && kellyUrl && not empty refType && refType eq 'R'}">
					         	<button class="btn btn-primary  " type="button" onclick="applySubmit();"><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" id="backbtn"><spring:message code="btnClr" /></a><br>
					         </c:when>
					         <c:otherwise>
					          	<button class="btn btn-primary  " type="button" onclick="chkApplyJob('${jobOrder.jobId}'); "><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" id="backbtn"><spring:message code="btnClr" /></a><br>
					         </c:otherwise>
			          </c:choose>
		       </c:otherwise>
		          
		         
		        </c:choose>
			    </div>
			</div>
			<c:if test="${jobOrder!=null && jobOrder.districtMaster!=null && (jobOrder.districtMaster.districtId == '7800047'||jobOrder.districtMaster.districtId=='7800292') && positionStart!=null && (not empty positionStart)}">
				<div class="row">
					<div class="col-sm-6 col-md-6 mt10 subheading" style="margin-left: 15px;">
						<strong>
								<spring:message code="lblPositionStartDate" />
						</strong>
					</div>
				</div>
						${positionStart}
		  </c:if>
		
			
			<div class="row">
			  <div class="col-sm-6 col-md-6 mt10">					
						<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblDistrictName" /></strong></div>
				
					<c:choose>
						<c:when test="${not empty jobOrder.districtMaster.displayName}">
							<c:choose>
							    <c:when test="${not empty districtMaster.displayName}">
							       ${districtMaster.displayName}
							    </c:when>    
							    <c:otherwise>
							       ${districtMaster.districtName}
							    </c:otherwise>
							</c:choose>
							<input type="hidden" id="districtId" value="${jobOrder.districtMaster.districtId}">
						</c:when>
						<c:otherwise>
						<input type="hidden" id="districtId" value="${jobOrder.districtMaster.districtId}">
							<c:choose>
							    <c:when test="${not empty districtMaster.displayName}">
							       ${districtMaster.displayName}
							    </c:when>    
							    <c:otherwise>
							       ${districtMaster.districtName}
							    </c:otherwise>
							</c:choose>${batchJobOrder.districtMaster.districtName}
						</c:otherwise>
					</c:choose>
				</div>
			</div>
		
		<c:if test="${not empty jeffcoSpecificSchool}">		
		<div class="row">
			<div class="col-sm-6 col-md-6 mt10 subheading" style="margin-left: 15px;">
				<strong>
				<c:choose>
					<c:when test="${jobOrder.districtMaster.districtId==804800}">
						<spring:message code="lblSchoolOrDistrict" />
					</c:when>
					<c:otherwise>
						<spring:message code="lblSchoolName" />
					</c:otherwise>
				</c:choose>
				
				
				
				</strong>
			</div>
		</div>
				${jeffcoSpecificSchool}
		</c:if>
		
		
			<c:if test="${jobCertifications ne ''}">
					<div class="row">
						<div class="col-sm-6 col-md-6 mt10" >
							<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblCerti/Lice" />  <spring:message code="lblrequired" /></strong></div>
						</div>							
					</div>
					${jobCertifications}
			</c:if>
				
		<c:if test="${jobOrder!=null && jobOrder.districtMaster!=null && jobOrder.districtMaster.districtId == '804800'}">
			<div class="row">
				<div class="col-sm-6 col-md-6 mt10 subheading">
					<strong>&nbsp;<spring:message code="lblJobStatus" /></strong><br>
				</div>
			</div>
			<c:if test="${jobOrder.jobApplicationStatus == 1}">
				<spring:message code="lblOngoing"/>
			</c:if>
			<c:if test="${jobOrder.jobApplicationStatus == 2}">
				<spring:message code="lblTemporary"/>
			</c:if>
		</c:if>
		
		<c:if test="${jobOrder.districtMaster.districtId==804800}">
			<div class="row">
				<div class="col-sm-2 col-md-2 mt10">
					<strong class='subheading' style="margin-left: 0px;"><spring:message code="lblHourPerDay" /></strong>
					<br />
					${jobOrder.hoursPerDay}
				</div>
				<c:if test="${not empty jobOrder.fte}">
				<div class="col-sm-2 col-md-2 mt10">
					<strong class='subheading' style="margin-left: 0px;">FTE</strong>
					<br />
					${jobOrder.fte}
				</div>
				</c:if>
				<c:if test="${not empty jobOrder.daysWorked}">
				<div class="col-sm-2 col-md-2 mt10">
					<strong class='subheading' style="margin-left: 0px;">Days Worked</strong>
					<br />
					${jobOrder.daysWorked}
				</div>
				</c:if>
				<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Hourly' || (not empty jobOrder.jobCategoryMaster.parentJobCategoryId && jobOrder.jobCategoryMaster.parentJobCategoryId.jobCategoryName eq 'Hourly')}">
					<c:if test="${not empty jobOrder.fsalary}">
						<div class="col-sm-2 col-md-2 mt10">
						<strong class='subheading' style="margin-left: 0px;"><spring:message code="lblSalary" />(Hourly)</strong><br/>
						$<fmt:formatNumber type="number" maxFractionDigits="2" value="${jobOrder.fsalary}"/>
						</div>
					</c:if>
				</c:if>
				<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrator/Professional Technical' || (not empty jobOrder.jobCategoryMaster.parentJobCategoryId && jobOrder.jobCategoryMaster.parentJobCategoryId.jobCategoryName eq 'Administrator/Professional Technical')}">
					<c:if test="${not empty jobOrder.fsalaryA || not empty jobOrder.ssalary}">
						<div class="col-sm-2 col-md-2 mt10">
						<strong class='subheading' style="margin-left: 0px;"><spring:message code="lblSalary" />(Yearly)</strong><br/>
						$<fmt:formatNumber type="number" maxFractionDigits="0" value="${jobOrder.fsalaryA}"/> - $<fmt:formatNumber type="number" maxFractionDigits="0" value="${jobOrder.ssalary}"/>
						</div>
					</c:if>
				</c:if>
				<c:if test="${not empty jobOrder.payGrade}">
				<div class="col-sm-2 col-md-2 mt10">
					<strong class='subheading' style="margin-left: 0px;">Pay Grade</strong>
					<br />
					${jobOrder.payGrade}
				</div>
				</c:if>
			</div>
			
		</c:if>
		<%-- Mukesh --%>
		<c:if test="${not empty jobOrder.positionStartDate}">
		<div class="row">
					<div class="col-sm-6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong>Position Start Date</strong></div>
					${positionStartDate}
					</div>
				</div>
		</c:if>
				<div class="row">
					<div class="col-sm-6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblPostedon" /></strong></div>
					${startDate},12:01 AM CST
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6 col-md-6 mt10">
					<div class="subheading" style="margin-left: 0px;"><strong><spring:message code="lblExpiryDate" /></strong></div>

					<%--<c:if test="${endDateDis eq 'filled'}">
						<spring:message code="lblUntil" /> 
					</c:if>--%>
					${endDateDis}
					</div>
				</div>
		
<!--for strive only	-->
		<c:if test="${schoolType ne '' && schoolType ne null}">
			<div class="row">
				<div class="col-sm-6 col-md-6  subheading" style="margin-left: 15px;">
					<label><spring:message code="lblSchN"/></label>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6 col-md-6" >
						<span>${schoolType}</span>
				</div>
			</div>
		</c:if>
<!--	.........end	-->
		<c:if test="${not empty jobOrder.jobDescriptionHTML or not empty jobOrder.pathOfJobDescription}">		
		
			<div class="row">
			<div class="col-sm-6 col-md-6 mt10 subheading" style="margin-left: 15px;">
				<strong>
				<spring:message code="headJobDescrip" />
				<c:if test="${not empty jobOrder.pathOfJobDescription}">
					<a href="#" id="iconpophover16" rel="tooltip" data-original-title="Click here to view job description">
						<span id="hrefJobDesc"  class='fa-paperclip icon-large'
						onclick="downloadJobDescription();if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;"></span>
					</a>	
				</c:if>
				</strong>
			</div>
			</div>
			<div id='jobDescriptionHTMLConvert'>${jobOrder.jobDescriptionHTML}${batchJobOrder.jobDescriptionHTML}&nbsp;</div>
			
		
		</c:if>
		
		
		<c:if test="${not empty jobOrder.jobQualificationHTML}">
		 <div class="row">
			<div class="col-sm-6 col-md-6 mt10 subheading" style="margin-left: 15px;">
				<c:choose>
					<c:when test="${jobOrder.districtMaster.districtId eq 804800}">
						<strong><spring:message code="headJobAdditionalInformation"/></strong>
					</c:when>
					<c:otherwise>
						<strong><spring:message code="lblJoQuali" /></strong>
					</c:otherwise>
				</c:choose>
	
			
			</div>
		</div>	
			${jobOrder.jobQualificationHTML}&nbsp;
		
		</c:if>
		
		<c:if test="${jobOrder.districtMaster.districtId eq 804800}">
			<div id='jeffcoURLLINK'>
				<br/>Salary:<br/><a href="http://www.jeffcopublicschools.org/employment/salaries/index.html">http://www.jeffcopublicschools.org/employment/salaries/index.html</a><br/>Benefits:<br/><a href="http://www.jeffcopublicschools.org/employment/benefits/">http://www.jeffcopublicschools.org/employment/benefits/</a><br/><br/>
			</div>
		</c:if>
		
		
	<div class="span10">

			<c:if test="${jobOrder ne null}">
			
				<form method="post" action="setcl.do" id="applyTeacherJob" onsubmit="return checkCL();">
				
				<div  class="modal hide" id="myModalCL" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
					<div class="modal-dialog-for-cgmessage"> 
					<div class="modal-content"> 
					<div class="modal-header">
				  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					
						<c:if test="${jobOrder.districtMaster.districtId ne 806900}">	
							<h3 id="myModalLabel"><spring:message code="headCoverLetr" /></h3>
						</c:if>
						<c:if test="${jobOrder.districtMaster.districtId eq 806900}">	
							<h3 id="myModalLabel"><spring:message code="lblCoverLetter"/> <c:out value="${jobOrder.districtMaster.districtName}"/></h3>
						</c:if>
					</div>
					<div class="modal-body hide forKellyCvrLtr" style="max-height: 450px;overflow-y:auto; overflow-x: hidden;">
								<div id="kellYcvRLtrMain">
										<div class="row">
											<div class="col-sm-12 col-md-12"><b>Outside of the TeacherMatch system</b>, have you ever applied to or worked for Kelly ServicesÂ®?
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-12">
												<label class="checkbox-inline">
												  <input type="radio" id="workForKelly1" name="workForKelly"  value="option1"> Yes
												</label>
												<label class="checkbox-inline">
												  <input type="radio" id="workForKelly2" name="workForKelly" value="option2"> No
												</label>
											</div>
										</div>
									</div>
									<!-- next question -->
									<div class="hide" id="kellYnxtQ">
										<div class="row">
											<div class="col-sm-12 col-md-12">Have you contacted the Kelly branch?
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12 col-md-12">
												<label class="checkbox-inline">
												  <input type="radio" id="contactedKelly1" name="contactedKelly"  value="option1"> Yes
												</label>
												<label class="checkbox-inline">
												  <input type="radio" id="contactedKelly2" name="contactedKelly" value="option2"> No
												</label>
											</div>
										</div>
									</div>
									<!-- end next question -->
									<div class="hide" id="kellYnxtInst">
										<div class="row">
											<div class="col-sm-12 col-md-12">If you have been instructed to continue your application, please click "Continue"
											</div>
										</div>
									</div>
									<div class="hide" id="kellYnxtDisDivInf">
										<div class="row">
											<div class="col-sm-12 col-md-12" id="kellYnxtDisDivData">
											</div>
										</div>
									</div>
								</div>
								
								
								<div class="modal-body forAllDistCvrLtr" style="max-height: 450px;overflow-y:auto; overflow-x: hidden;">							
								<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>
								<div class='divErrorMsg' id='errordivCL1' style="display: block;"></div>
								
						<div style="padding-top: 10px;padding-bottom: 10px;">
							<label class="redtextmsg"><spring:message code="msgWhenYuClickContiIfYuAreNotTm" /></label>
				   		</div>	

				   						   		
				   		<div <c:if test="${jobOrder.districtMaster.districtId eq 806900 || jobOrder.districtMaster.districtId eq 1201290}">style="display:none;"</c:if>>
				   		

							<p id="cvrltrTxt" class="hide"><spring:message code="msgIfYuApplyCentealOfficePosition" /></p>
							
							<label class="radio" id="noCoverLetter" >
						        <input type="radio"  value="1" id="rdoCL2" name="reqType"  onclick='setClBlank()'>
						       <spring:message code="msgIdoNotWantToAddCorLetter" />
					   		</label>
					   								
							<label class="radio">
						        <input type="radio"  value="1" id="rdoCL1" name="reqType" onclick="setCLEnable()">
						        <spring:message code="msgPlzTyYurCoverLtr" />	
					   		</label>
					   		
					   			
							<div id="divCoverLetter">
								<c:if test="${jobOrder.districtMaster.districtId ne 806900 && jobOrder.districtMaster.districtId ne 1201290}">	
									<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" /></label>
								</c:if>
								<c:if test="${jobOrder.districtMaster.districtId ne 806900 && jobOrder.districtMaster.districtId ne 1201290}">	
								<textarea id="coverLetter"  name="coverLetter" ></textarea>
								</c:if>
					   		</div>
					   		
				   		</div>
				   		
						<!--For jeffco only-->
						<div class="jeffcoRequired hide" style="width:100%;">
							<c:if test="${jobOrder.districtMaster.districtId eq 806900}">
									<div class="col-sm-10 col-md-10" style="width:100%;">
									   <p><spring:message code="msgAdamsEmpStatus"/></p>
									   <p>Last 4 digits of your Social Security Number and your month/day of your Date of Birth are required to verify your applicant type. Those fields will only be used by Human Resources to verify if you are a current employee, former employee, or external applicant.</p>
										<div class="col-sm-4 col-md-4" style="margin-left: -32px;">
												<label><spring:message code="lblFname"/><span class="required">*</span></label>
													<input type="text" id="firstNameForAdams" name="firstNameForAdams" class="form-control" style="width:160px !important;" value="${teacherpersonalinfo.firstName}"/>
											</div>
											<div class="col-sm-4 col-md-4" style="margin-left: 38px;">
												<label><spring:message code="lblLname"/><span class="required">*</span></label>
												<input type="text" id="lastNameForAdams" name="lastNameForAdams" class="form-control" style="width:160px !important;" value="${teacherpersonalinfo.lastName}"/>
										</div>
									</div>
								</c:if>	
								
							<div class="col-sm-10 col-md-10 mt5" style="width:100%;padding:0px;">
								<label>
									<strong>
										Date of Birth<span class="required">*</span>
										<input type='hidden' name='candidateDOB' value=''/>
										<input type='hidden' name='donothireresponsevalue' value=''/>
									</strong>
								</label>
								<div>
									<div class="col-sm-4 col-md-4" style="padding:0px;">
									<label><strong>Month<span class="required optionalCss dobRemAst">*</span></strong></label>
									<select class="form-control" id="dobMonth1" name="dobMonth1" onchange="getDay();">
									</select>
									</div>
									
									<div class="col-sm-4 col-md-4" style="padding-left:4px;padding-right:4px;">
									<label><strong>Day<span class="required optionalCss dobRemAst">*</span></strong></label>
									<select class="form-control" id="dobDay1" name="dobDay1">
									</select>					
									</div>
									
									<div class="col-sm-4 col-md-4" style="padding:0px;<c:if test="${jobOrder.districtMaster.districtId eq 806900}">display:none;</c:if>">
									<label><strong>Year<span class="required optionalCss dobRemAst">*</span></strong></label>
									<input type="text" id="dobYear1" name="dobYear1" class="form-control" maxlength="4" onkeypress="return checkForInt(event);" onblur="return validateDOB('dobDay1','dobMonth1','dobYear1','errordivCL1');">
									</div>
									
								</div>
							</div>
							<div class="col-sm-5 col-md-5" style="width:100%;padding:0px;">
									<label style="width:100%;"><strong>Last 4 of SSN<span class="required">*</span></strong></label>
									<input type="text" maxlength="4" id="last4SSN" name="last4SSN" class="form-control" value=""  maxlength="50" style="width:170px;"/>
							</div>
							<div class="col-sm-1 col-md-1">
									<label></label>
							</div>
									
						</div>	
						<!--end jeffco only-->
						<c:if test="${jobOrder.districtMaster.districtId ne '806810' && jobOrder.districtMaster.districtId ne '7800294' && jobOrder.districtMaster.districtId ne '1201290'}">
					   		<label class="checkbox notforJeffco" style='width:98%;'>
						        <input type="checkbox"  value="1" id="isAffilated" name="isAffilated" onclick="validateCandidateTypeForExternalJobApply();">
						         <spring:message code="msgApplyJob7" /><br/>
								 <!--<span class="redtextmsg">(If you are currently a substitute Teacher, please don't check this box.)</span>&nbsp; <a href="#" id="iconpophover101" rel="tooltip" data-original-title="Please select this box if you are currently working with this District and/or any School within this District as a full time or part time employee or consultant/contractor"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> -->
					   		</label>
					   		</c:if>
					   		
				   			<div class="row left25 hide empCvrrLtrDiv notforJeffco">
								<div class="col-sm-5 col-md-5">
								<label><strong>Employee Number<span class="required">*</span></strong></label>
								<input type="text" id="empNumCoverLtr" name="empNumCoverLtr" class="form-control" value="${teacherpersonalinfo.employeeNumber}" maxlength="50"/>
								</div>
								<div class="col-sm-5 col-md-5">
								<label><strong>Zip Code<span class="required">*</span></strong></label>
								<input type="text" id="zipCodeCoverLtr" name="zipCodeCoverLtr" class="form-control" value="${teacherpersonalinfo.zipCode}" maxlength="10"/>
								</div>
							</div>
							
							<c:if test="${jobOrder.districtMaster.districtId ne '806810' && jobOrder.districtMaster.districtId ne '7800294' && jobOrder.districtMaster.districtId ne '1201290'}">
				   			<div class="notforJeffco" style="padding-left: 15px;">
					   		<label class="radio" id="inst" style="display: none;">
						        <input type="radio"  value="I" name="staffType" >
						        <spring:message code="msgApplyJob8" />
					   		</label>
					   		<label class="radio" id="partinst" style="display: none;">
						        <input type="radio"  value="PI" name="staffType" >
						        <spring:message code="msgApplyJob9" />
					   		</label>
					   		<label class="radio" id="noninst" style="display: none;">
						        <input type="radio"  value="N" name="staffType" >
						        <spring:message code="msgApplyJob10" />
					   		</label>
				   		</div>
				   		</c:if>
				   		<div class='divErrorMsg' id='errordivCLForSSN' style="display: block;"></div>
				   		<div style="padding-left: 15px;" class="col-sm-4 col-md-4 hide" id="ncCurrentEmp">
							SSN
							<input type="text" class="form-control" id="currentEmpSSN" name="currentEmpSSN" maxlength="9" onkeypress="return checkForInt(event);">							
							</div>
				   		
				   		<c:if test="${jobOrder.districtMaster.districtId eq '806810' || jobOrder.districtMaster.districtId eq '7800294'}">
				   		<input type="hidden" id="isaffiliateds" name="isaffiliated">
								<input type="hidden" id="staffType" name="staffType">
								
								<div id="formerEmployeeDiv">
							        
									<b>Employee Status Check</b><span class="required">*</span>
									<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
									    		<input type="radio" id="isAffilated" name="isAffilated" value="2"  onclick="showEmpNoDiv(1);"/><spring:message code="msgDp_commons13" /> <span id='displayDistrictNameForExt1'>${districtMaster.districtName}</span>
									    	</label>
									    </div>	
									</div>
								    	<div id="fe1Div" class="hide">
									    	<div class="row left25">
										        <div class="col-sm-4 col-md-4 hide">
													<label id="lblempnumber"><strong><spring:message code="lblEmpNum" /></strong></label>
													<input type="text" id="empfe1" class="form-control" maxlength="50" onkeypress="return checkForIntEmpNum(event);"/>
												</div>
												<div id="positionDiv" class="col-sm-4 col-md-4" style="display: none;">
													<label><strong>Location</strong></label>
													<input type="text" id="empfe5" class="form-control" maxlength="50"/>
												</div>
												
												<div id="locationDiv" class="col-sm-4 col-md-4" style="display: none;">
													<label><strong>Position</strong></label>
													<input type="text" id="empfe6" class="form-control" maxlength="50"/>
												</div>
											
												
												<!--Name While Employed-->
												<div id="nameDiv" class="col-sm-6 col-md-6" style="">
													<label><strong><spring:message code="lblNameWhileEmployed" /></strong></label>
													<input type="text" id="nameWhileEmployed" class="form-control" maxlength="50"/>
												</div>
												
												<!--end of Name While Employed-->
												<div class="row left25 hide" id="fePosDiv">
										    		<div class="col-sm-6 col-md-6">
														<label id="lblempposition"><strong><spring:message code="lblEmpPosition" /><span class="required">*</span></strong></label>
														<input type="text" id="emppos" name="emppos" class="form-control" maxlength="50" onkeypress="" value="${teacherpersonalinfo.lastPositionWhenEmployed}"/>
													</div>
										    	</div>
											</div>
											  <div class="row left25 ntPhiLfeild">
											      <div class="col-sm-12 col-md-12 hide">
													<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
														<input type="checkbox" id="empchk11" onclick="showEmpDate(0);"/>
								            			<spring:message code="msgDp_commons14" />
								           				</label> 
									           		</div>	
									           		 <div class="col-sm-3 col-md-3 left25">
									           			<div class='mt5 hide' id='rtDateDiv'>
									           				<label id="lblRetirementDate1"><strong><spring:message code="lblRetirementDate" /></strong></label>
															<input type="text" id="rtDate" name="rtDate" value="" class="form-control" maxlength="4"/>
									           			</div>
													</div>
												</div>
												<div class="row left25 ntPhiLfeild">
												    <div class="col-sm-12 col-md-12 hide">
													<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
														<input type="checkbox" id="empchk12"  onclick="showEmpDate(1);"/>
								            			<spring:message code="msgDp_commons15" />
								            		</label>
								            		</div>	
													<div class="col-sm-3 col-md-3 left25">
								           			<div class='mt5 hide' id='wtDateDiv'>
								           				<label><strong><spring:message code="lblDateofwithdrawn" /></strong></label>
														<input type="text" id="wdDate" name="wdDate" value="" class="form-control" maxlength="4"/>
								           			</div>
													</div>
												</div>
												
												<div class="row left25 ntPhiLfeild hide" id="empchk13Div">
												    <div class="col-sm-12 col-md-12">
													<label class="checkbox inline" style="margin-top: 2px;margin-bottom: 2px;">
														<input type="checkbox" id="empchk13"   onclick="showEmpDate(2);"/>
								            			I retired from an employer covered by the Colorado's Public Employers Retirement Association (PERA).
								            		</label>
								            		</div>	
								            		<div class="col-sm-3 col-md-3 left25">
								           			<div class='mt5 hide' id='rwtDateDiv'>
								           				<label><strong><spring:message code="lblRetirementDate" /></strong></label>
														<input type="text" id="rwdDate" name="rwdDate" class="form-control" maxlength="4"/>
								           			</div>
													</div>
												</div>
												
												<div class="row left25 ntPhiLfeild hide" id="empfewDiv">
													<div class="col-sm-6 col-md-6 top10">
														<label><strong><spring:message code="msgDp_commons16" /></strong></label>
														<textarea name="empfew" id="empfew" class="form-control" maxlength="1000" rows="3"></textarea>
													</div>
												</div>
						           		</div>
								
								
								         <div class="row">
									
										     <div class="col-sm-9 col-md-9">
												<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
													<input type="radio" id="isAffilated" name="isAffilated" value="1" onclick="showEmpNoDiv(2);"/><spring:message code="msgDp_commons17" /> <span id='displayDistrictNameForExt2'>${districtMaster.districtName}</span>
												</label>
											 </div>	
										</div>
										
										 <div id="fe2Div" class="hide">
											<div class="row left25">
												<div class="col-sm-4 col-md-4 hide">
													<label><strong><spring:message code="lblEmpNum" /><span class="required">*</span></strong></label>
													<input type="text" id="empfe2" class="form-control" maxlength="50" onkeypress="return checkForIntEmpNum(event);"/>
												</div>
												<div id="positionfe2Div" class="col-sm-4 col-md-4">
													<label><strong>Location</strong></label>
													<input type="text" id="empfe2location" class="form-control" maxlength="50"/>
												</div>
												
												<div id="locationfe2Div" class="col-sm-4 col-md-4">
													<label><strong>Position</strong></label>
													<input type="text" id="empfe2position" class="form-control" maxlength="50"/>
												</div>
												<div class="col-sm-3 col-md-3 hide" id="senNumDiv">
													<label><strong><spring:message code="msgSeniorityNumber" /><span class="required"></span></strong></label>
													<input type="text" id="seniorityNumb" class="form-control" maxlength="50"/>
												</div>
						
												<div class="col-sm-5 col-md-5 top20 hide" id="jeffcoSeachDiv">
													<button class="btn btn-large btn-primary" type="button" onclick="openEmployeeNumberInfo();"><strong>Employee Information<i class="icon"></i></strong></button>
												</div>
											</div>
						
										
										<!-- 
										<div class="span6">
											<label class="checkbox inline">
												<input type="checkbox" id="empchk2"/>
							            		I am current full time teacher
							           		</label>
						           		</div>
						           		 -->
						           		<div class="row left25">
						           		 <div class="col-sm-9 col-md-9 optionalCss hide">
													<label class="span6 radio">
														<input type="radio" id="rdCEmp1" name="rdCEmp" /><spring:message code="msgDp_commons18" />
													</label>
													<c:set var="showVal"  value="none"/>
													<c:if test="${isMiami}">
													<c:set var="showVal"  value="block"/>
													</c:if>
													<label class="span7 radio p0 left20" style="display: ${showVal};" id="partInsEmp">
														<input type="radio" id="rdCEmp3" name="rdCEmp" /><spring:message code="msgDp_commons19" />
													</label>
													<label class="span6 radio">
												    <input type="radio" id="rdCEmp2" name="rdCEmp" /><spring:message code="msgDp_commons20" />
											         </label>
										</div>
									</div>   		
						           		<div class="row left25 col-sm-4 col-md-4 hide" id="districtEmailDiv">					
											<label><strong>District Email Address</strong><span class="required">*</span></label>
											<input type="text" id="districtEmail" name="districtEmail" class="form-control" value="${teacherpersonalinfo.districtEmail}"/>
										</div>
						           </div>
									<div class="row" id="fe3Div">
										<div class="col-sm-9 col-md-9">
											<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
											<input type="radio" id="isAffilated" name="isAffilated" value="3" onclick="showEmpNoDiv(6);"/><spring:message code="msgDp_commons60" />&nbsp;<span >Colorado's Public Employers Retirement Association (PERA)</span>
										</label>
										</div>										
										<div id="position6Div" class="col-sm-5 col-md-5 left25 hide" style="">
												<label><strong>Approximate Retirement Date</strong></label>
												 <input type="text" id="empaproretdatefe6" name="aproRetDate" class="help-inline form-control">
										</div>
									</div>
									<div class="row">
											<div class="col-sm-9 col-md-9">
												<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
												<input type="radio" id="isAffilated" name="isAffilated" value="0" onclick="showEmpNoDiv(3);"/><spring:message code="msgDp_commons21" /> <span id='displayDistrictNameForExt3'>${districtMaster.districtName}</span>
											</label>
										</div>
									</div>
								
								<div class="row" id="fe5Div">
										<div class="col-sm-9 col-md-9">
											<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
											<input type="radio" id="isAffilated" value="5" name="isAffilated" onclick="showEmpNoDiv(5); validateCandidateType()"/>I am employed as a substitute teacher for <span id='displayDistrictNameForExt5'>${districtMaster.districtName}</span>
										</label>
									</div>
								</div>
							</div>
						</c:if>
				   		
				   		<!-- Dhananjay Verma 20-02-2016 -->
						<c:if test="${jobOrder.districtMaster.districtId eq '1201290'}">
								<input type="hidden" id="isaffiliateds" name="isaffiliated">
								<input type="hidden" id="staffType" name="staffType">
								
								<c:if test="${jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrative Non-Instructional' || jobOrder.jobCategoryMaster.jobCategoryName eq 'Administrative-Instructional'}">
								<div>
									<p id="cvrltrTxt" class="hide"><spring:message code="msgIfYuApplyCentealOfficePosition" /></p>
										<div class='divErrorMsg' id='errordivCL' style="display: block;"></div>
										<div class='divErrorMsg hide' id='errordivCL1' style="display: block;"></div>
										
										
									
							<!-- Dhananjay Verma -->
								<div id="divCoverLetterForMartin">	
									<label class="radio">
										<input type="radio" value="YCL" id="rdoCL2ForMartinAdmin" name="reqType" onclick="setCLEnableForMartinAdmin()">
										<spring:message code="msgPlzTyYurCoverLtr" />
									</label>
									
									</div>
												
									<div id="divCoverLetterAdmin" >	
										
											<label class="redtextmsg">
												<spring:message code="msgHowToCopyPastCutDouc" />
											</label>
										
										<textarea id="coverLetter" name="coverLetter" rows="7"	cols="150"></textarea>
										
									</div>
									
									<c:if test="${fn:length(latestCoverletter)gt 0}">
										<div class="top10">
											<label class="radio">
												<input type="radio" value="3" id="rdoCL3" name="reqType"
													onclick='setLatestCoverLetterForMartinAdmin()'>
												<spring:message code="msgApplyJob6" />
											</label>
										</div>
									</c:if>
									
								</div>
								
								</c:if>
							
								<div>
							        
									<b>Employee Status Check</b><span class="required">*</span>
									
									<!-- Current Employee -->
									 <div class="row">
									
										     <div class="col-sm-9 col-md-9">
												<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
													<input type="radio" id="isAffilated" name="isAffilated" value="1" onclick="showEmpNoDiv('CEmpOfMartin');"/><spring:message code="msgDp_commons17" /> <span id='displayDistrictNameForExt2'>${jobOrder.districtMaster.districtName}</span>
												</label>
											 </div>	
										</div>
										
										
											<div class="row left25">
												
											<div id="currentEmployeeOfMartin" class="hide">
											
												<div id="currentEmployeeOfMartinJT" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblJT" /> </strong></label>
													<input type="text"  name="currentempjobtitle" id="currentempjobtitle" class="form-control" maxlength="50"/>
												</div>
												
												<div id="currentEmployeeOfMartinLoc" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblLoc" /></strong></label>
													<input type="text" name="currentemplocation" id="currentemplocation" class="form-control" maxlength="50"/>
												</div>
												
												<div id="currentEmployeeOfMartinYrInLoc" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblYearInLoc" /></strong></label>
													<input type="text" name="currentempyearinlocation" id="currentempyearinlocation" class="form-control" maxlength="50"/>
												</div>
												
												<div id="currentEmployeeOfMartinSupervisor" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblSupervisor" /></strong></label>
													<input type="text" name="currentempsupervisor" id="currentempsupervisor" class="form-control" maxlength="50"/>
												</div>
											</div>
											</div>
						           		<!-- End Current Employee -->
						           		
									
									<!-- Formal Employee -->
									<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span6 radio" style="margin-top: 2px;margin-bottom: 2px;">
									    		<input type="radio" id="isAffilated" name="isAffilated" value="2"  onclick="showEmpNoDiv('FormalEmpOfMartin');"/><spring:message code="msgDp_commons13" /> <span id='displayDistrictNameForExt1'>${jobOrder.districtMaster.districtName}</span>
									    	</label>
									    </div>	
									</div>
								    	
								    	
									    	<div class="row left25">
												
											<div id="formalEmployeeOfMartin" class="hide">
												<div id="formalEmployeeOfMartinJT" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblJT" /> </strong></label>
													<input type="text" id="fromalempjobtitle" name="fromalempjobtitle" class="form-control" maxlength="50"/>
												</div>
												
												<div id="formalEmployeeDatesOfEmployee" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="datesofemp" /></strong></label>
													<input type="text" id="formalempdatesofemployee"  name="formalempdatesofemployee" class="form-control" maxlength="50"/>
												</div>
												
												<div id="formalEmployeeOfMartinNameWhileEmployee" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblNameWhil" /></strong></label>
													<input type="text" id="formalempnamewhileemployee"  name="formalempnamewhileemployee" class="form-control" maxlength="50"/>
												</div>
												
												
											</div>
											</div>
						           		<!-- End Formal Employee -->
						           		
						           		<!--  Retired Employee -->
										<div class="row">
												<div class="col-sm-9 col-md-9">
													<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
													<input type="radio" id="isAffilated" name="isAffilated" value="3" onclick="showEmpNoDiv('RetiredEmpOfMartin');"/><spring:message code="msgDp_commons62" /> &nbsp;
												</label>
												</div>
											</div>
											
												<div class="row left25">
											<div id="retiredEmployeeStateofFlorida" class="hide">	
												<div id="retiredEmployeeJT" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="dateofretirement" /> </strong></label>
													<input type="text" id="retiredempdateofretirement"  name="retiredempdateofretirement" class="form-control" maxlength="50"/>
												</div>
											</div>
											</div>
											<!-- End Retired Employee -->
									
									<!-- Never Employee Of Martin District -->
									<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
											<input type="radio" id="isAffilated" name="isAffilated" value="0" onclick="showEmpNoDiv('never_emp');"/><spring:message code="msgDp_commons21" /> <span id='displayDistrictNameForExt3'>${districtDName}</span>
										</label>
									</div>
								</div>
								<!-- End  Never Employee Of Martin District-->
								
								
								<!-- Substitute Of Martin -->
								<div class="row">
										<div class="col-sm-9 col-md-9">
											<label class="span15 radio" style="margin-top: 2px;margin-bottom: 2px;">
											<input type="radio" id="isAffilated" value="5" name="isAffilated" onclick="showEmpNoDiv('CurrentSubstituteEmpOfMartin'); "/>I am a current substitute employee for <span id='displayDistrictNameForExt5'>${jobOrder.districtMaster.districtName}</span>
										</label>
									</div>
									
									</div>
									
										<div class="row left25">
											<div id="currentSubstituteEmployee" class="hide">			
												<div id="currentSubsEmpJT" class="col-sm-4 col-md-4">
													<label><strong><spring:message code="lblJT" /> </strong></label>
													<input type="text" id="currentsubstitutejobtitle" class="form-control" maxlength="50"/>
												</div>
												
									</div>
								</div>
								<!-- End Substitute Of Martin -->
								
							</div>
						</c:if>
						<!-- End Martin -->
				   		
				   		
				 	</div>
				 	<div class="modal-footer">
				 		<button class="btn btn-large btn-primary continueBtnNxt" type="submit" onclick="return goNextOrNot();"><strong><spring:message code="btnConti" /> <i class="icon"></i></strong></button>
				 		<button class="btn cvrLtrClose" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose" /></button> 		
				 	</div>
				</div>
				</div>
				</div>
				<input type="hidden" name="kellyJobId" id="kellyJobId" value="${jobOrder.jobId}"/>
				<input type="hidden" name="refType" id="refType" value="${refType}"/>
				<input type="hidden" name="branchId" id="branchId" value="${branchId}"/>
				<input type="hidden" id="dID" name="dID" value="${jobOrder.districtMaster.districtId}"/>
				
				<c:if test="${jobOrder.districtMaster.districtId eq '806810'}">
				<input type="hidden" name="ff_name" id="ff_name"/>
				<input type="hidden" name="ll_name" id="ll_name"/>
				<input type="hidden" name="locationn" id="locationn" value=""/>
				<input type="hidden" name="pposition" id="pposition" value=""/>
				<input type="hidden" name="datee" id="datee" value=""/>
				<input type="hidden" name="nameWhileEmployed1" id="nameWhileEmployed1"/>
				</c:if>
				
				</form>
				<input type="hidden" name="jobId" id="jobId" value="${jobOrder.jobId}"/>
				<input type="hidden" name="isMiami" id="isMiami" value="${isMiami}"/>
				<div class="row">
			   <div class ="col-sm-6 col-md-6"> 
			   
					<!--<button class="btn btn-primary  " type="button" onclick="chkApplyJob('${jobOrder.jobId}') "><strong>Apply <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" id="backbtn">Cancel</a><br>
				-->
				
				<c:choose>
				
				<c:when test='${(jobOrder.districtMaster.jobApplicationCriteriaForProspects ne 0 && empty jobOrder.headQuarterMaster) }'>
		        <button class="btn btn-large btn-primary" type="button" onclick="chkApplyJobNonClient('${jobOrder.jobId}','${jobOrder.districtMaster.jobApplicationCriteriaForProspects}','${jobOrder.exitURL}','${jobOrder.districtMaster.districtId}','');"><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" ><spring:message code="btnClr" /></a><br>
		        </c:when>
		        
		        <c:when  test="${ not empty teacherDetail && not empty teacherId && teacherId ne 0}">
		         <button class="btn btn-primary  " type="button" onclick="chkApplyJobAuthenticate('${jobOrder.jobId}') "><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" id="backbtn"><spring:message code="btnClr" /></a><br>
		       </c:when>
		         <c:otherwise>
			         <c:choose>
				         <c:when test="${not empty kellyUrl && kellyUrl && not empty refType && refType eq 'R'}">
				         <button class="btn btn-primary  " type="button" onclick="applySubmit();"><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" id="backbtn"><spring:message code="btnClr" /></a><br>
				         </c:when>
				         <c:otherwise>
				          <button class="btn btn-primary  " type="button" onclick="chkApplyJob('${jobOrder.jobId}'); "><strong><spring:message code="lblApply1" /> <i class="icon"></i></strong></button>&nbsp;&nbsp;<a href="${redirectURL}" id="backbtn"><spring:message code="btnClr" /></a><br>
				          </c:otherwise>
			          </c:choose>
		       </c:otherwise>
		          
		        </c:choose>
				
				
				</div>
				</div>
			</c:if>						
		</div>		
		<input type="hidden" name="tempjobid" id="tempjobid"/>
		<input type="hidden" name="exitUrl" id="exitUrl"/>
		<input type="hidden" name="district" id="district"/>
		<input type="hidden" name="school" id="school"/>
		<input type="hidden" name="criteria" id="criteria"/>
		<input type="hidden" name="epistatus" id="epistatus"/>
		<input type="hidden" value="${redirectURL}" id="rdURL">
		<input type="hidden" id="ok_cancelflag" name="ok_cancelflag" value="0">
		
		<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
			<div class="modal-dialog">
			<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
				<div class="control-group">
					<div class="" id="divAlertText" >
					</div>
				</div>
		 	</div> 	
		 	<div class="modal-footer"> 	
		 		<button class="btn btn-large btn-primary" aria-hidden="true" onclick="resetjftIsAffilated()" ><strong><spring:message code="lblYes" /> <i class="icon"></i></strong></button>&nbsp;
		 		<span><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" onclick="canceljftIsAffilated()"><strong><spring:message code="lblNo" /> <i class="icon"></i></strong></button></span> 
 				<span><button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClr" /></button></span>		
 			</div>
		</div>
		</div>
		</div>

<div class="modal hide"  id="jobApplyOrNot" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="performAction();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="jobApplyOrNotLabel" class="modal-body"> 	
	<input type="hidden" id="perform" />
	<div class="control-group" id="notApplyMsg">
			<spring:message code="msgYouCantApply" />
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary " data-dismiss="modal" onclick="performAction();"><spring:message code="btnOk" /> <i class="icon"></i></button></span>&nbsp;&nbsp;
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="divJobAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divJobAlertText" >
			<spring:message code="msgThankInterestApplying" /> ${jobOrder.jobTitle}${batchJobOrder.jobTitle} <spring:message code="msgAt" /> <c:choose>
						<c:when test="${not empty schoolMaster}">
							${schoolMaster.schoolName}. <spring:message code="msgOpportunityCurrentlyClosed" /><br/><br/>
						</c:when>
						<c:otherwise>
							<c:choose>
							    <c:when test="${not empty districtMaster.displayName}">
							       ${districtMaster.displayName}
							    </c:when>    
							    <c:otherwise>
							       ${districtMaster.districtName}
							    </c:otherwise>
							</c:choose>. <spring:message code="msgOpportunityCurrentlyClosed" /><br/><br/>
						</c:otherwise>
					</c:choose>

			  <spring:message code="msgRewardingTeachingOpportunities" /> <a target="blank" href="${JobBoardURL}">${JobBoardURL}</a>.
			  <br/><br/><spring:message code="mailAcceotOrDeclineMailHtmal11" /><br/>
			  TeacherMatch Team
					
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 	
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnClose" /></button></span> 		
		</div>
</div>
	</div>
</div>    
			
<div class="modal hide" id="myModalApplyJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="lblSendMessageToAdministrator" /></h3>
	</div>
	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0' frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>

	<div  class="modal-body" style="max-height: 450px;overflow-y:auto">
	
	<form id='frmMessageUpload' enctype='multipart/form-data' method='post' target='uploadFrame'  class="form-inline" onsubmit="return validateMessage();" accept-charset="UTF-8">
		<input type='hidden' id="emailForTeacher" name="emailForTeacher"  value="${emailForTeacher}"/> 
		<div class="control-group">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		<div class="control-group">
			<div class="row">
			<div class="col-sm-12 col-md-12">
		    	<label><strong><spring:message code="lblSub" /></strong><span class="required">*</span></label><br/>
	        	<input id="msgMailSubject" name="msgMailSubject" type="text" class="form-control" maxlength="80" />
			</div>
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="msgMailSpt">
		    	<label><strong><spring:message code="lblMsg" /></strong><span class="required">*</span></label>
		    	<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc" />
		    	 </label>
	        	<textarea rows="5" class="span8" cols="" id="msgMail" name="msgMail" maxlength="1000"></textarea>
	        	<div id='jobFile1' style="padding-top:12px;">
	        	<a href='javascript:void(0);' onclick='addJobFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addJobFileType();'><spring:message code="lnkAttachFile" /></a>
	        	</div>
			</div>
		</div>
		<div class="controls">
				<spring:message code="lblwhatis" /> <img id="imgCaptcha"  height="30px" width="170px" src="verify.png" />
				<img src="images/refresh.png" style="cursor: pointer;" onclick="changeCaptcha('sumOfCaptchaText')"/>
				<div class="row">
				<div class="col-sm-12 col-md-12">
		         <label><strong><spring:message code="msgSumTwoNumber" /></strong></label>
		         <input id="sumOfCaptchaText" type="text"
					class="form-control" 
					 placeholder="sum of number"
					onkeypress="return checkForInt(event);" maxlength="2"/>
				</div>
				</div>	
		 </div>
 		
 		<div id="lodingJobImg" style='display:none;text-align:center;padding-top:4px;'><img src="images/loadingAnimation.gif" /> <spring:message code="msgMsgSending" /></div>
 		</form>
 	</div>
 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClr" /></button>
 		<button class="btn btn-primary" onclick="saveMessage()" ><spring:message code="btnSend" /></button>
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="myModalApplyJob2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='messageshow'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk" /></button>
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<!--<div class="modal-header dragHeader ui-dialog-titlebar ui-widget-header  " >
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick="setZIndexActDiv()">x</button>
		<h3 id="myModalLabel" >TeacherMatch</h3>
	</div>
	-->
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
				<spring:message code="msgJobRequiresAdditionalInf" />
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 		
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexActDiv()"><spring:message code="btnClose" /></button></span> 		
 	</div>
</div>
</div>
</div>
<div class="modal hide" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	 
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='hideDiv();'>
					x
				</button> 		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">	
			<div class='divErrorMsg' id='signUpServerError' style="display: block;margin-bottom: 10px;"></div>			             
			    <div class='divErrorMsg' id='signUpErrordiv' style="display: block;margin-bottom: 10px;"></div>			    			
				<h1 class="font25 top0" style=""><spring:message code="btnSign" /></h1>				
				<div class="row">
					<div class="col-sm-12 col-md-12 ">
					<div class="" style="color: black;"><spring:message code="lblFname" /><span class="required">*</span></div>
		            <input type="text" id="fname" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>		           
					</div>
					</div>
				<div class="row">
					<div class="col-sm-12 col-md-12">
					 <div class="" style="color: black;"><spring:message code="lblLname" /><span class="required">*</span></div>
		            <input type="text" id="lname" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>	          
					</div>
					</div>
						<div class="row">	           		        
			        <div class="col-sm-12 col-md-12 " >
			         <div class="" style="color: black;"><spring:message code="lblEmail" /><span class="required">*</span></div>
			           <input type="text" id="signupEmail" class="form-control" style="height: 40px; width: 100%;padding: 10px;"/>
			        </div>	           							
		 	</div>
		 	</div>		 	
		 	<div class="modal-footer">			 	
		 	<button class="btn btn-primary" type="button" onclick="return signUpTempUser();"><spring:message code="btnSign" /></button>	 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hidePopDiv()"><spring:message code="btnClr" /></button>		 			
		 	</div>
		</div>
	  </div>
</div>
 <div class="modal hide" id="appliedJob" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" style="">		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">				
				<p><spring:message code="msgYuAlreadyAppiedJob" /></p>			
		 	</div> 	
		 	<div class="modal-footer">		 	
		    <button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' onclick="hideDiv()"><spring:message code="btnOk" /></button>		 			
		 	</div>
		</div>
	  </div>
</div>

<div  class="modal hide"  id="applyJobComplete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 99999;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3>Teachermatch</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<!--<span><b><spring:message code="msgSucceApldJob" /></b></span>
				--><span><b><spring:message code="msgThankForInterest" /></b></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideInfo();"><spring:message code="btnOk" /></button> 		
 	</div>
</div>
</div>
</div>
<input type="hidden" id="gridNo" name="gridNo" value="">
<div  class="modal hide" onclick="getSortSecondGrid()" id="myModalforSchhol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 742px;">
	<div class="modal-content">
	<div class="modal-header" id="schoolHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headSchL"/></h3>
	</div>
	<div class="modal-body" id="schoolListDiv">
		
	</div> 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>
</div>
</div>
<script>
function performAction()
{
	checkPopup('http://jobs.dadeschools.net/teachers/Index.asp');
	window.location.href=$("#rdURL").val();
}
function checkPopup(url) {
	  var openWin = window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	  if (!openWin) {
		  	window.location.href=url;
	    } else {
	    window.open(url,"directories=no,height=400,width=600,menubar=no,resizable=no,scrollbars=no,status=no,titlebar=no,top=0,location=no");
	   }
}

try {
   document.getElementById('divDescDM').style.display='inline';
}catch (e) {}
try {
  document.getElementById('divDescSM').style.display='inline';
}catch (e) {}
function openMessageDiv(){
	$('#myModalApplyJob').modal('show');
	$('#errordivMessage').empty();
	$('#msgMailSubject').css("background-color", "");
	$('#msgMail').css("background-color", "");
	$('#jobFile').css("background-color", "");
	$('#msgMailSpt').find(".jqte_editor").css("background-color", "");
	$('#sumOfCaptchaText').css("background-color","");
	document.getElementById('msgMailSubject').value="";
	$('#msgMailSpt').find(".jqte_editor").html("");
	removeJobFile();
}
function saveMessage()
{
	
	if(!validateMessage())
		return;
	
	$('#lodingJobImg').show();
	
	document.getElementById('frmMessageUpload').action="messageUploadServlet.do";
	$('#frmMessageUpload').submit();
	
}
function hideMessage(flg){
	$('#lodingJobImg').hide();
	$('#myModalApplyJob').modal('hide');
	if(flg==1){
		$('#messageshow').html('<spring:message code="msgSuccSentAdministrator" />');
		$('#myModalApplyJob2').modal('show');
	}else{
		alert('<spring:message code="msgSessionExpired" />');  document.location = 'signin.do';
	}
	document.getElementById('msgMailSubject').value="";
	$('#msgMailSpt').find(".jqte_editor").html("");
	removeJobFile();
}
function addJobFileType(){
	$('#jobFile1').empty();
	$('#jobFile1').html("<a href='javascript:void(0);' onclick='removeJobFile();'><img src='images/can-icon.png' title='remove'/></a> <input id='jobFile' name='jobFile' size='20' title='Choose a File' type='file' style='margin-left:20px;margin-top:-15px;'><br>&nbsp;<spring:message code="msgMaximumFileSize2MB" />");
}
function removeJobFile(){
	$('#jobFile1').empty();
	$('#jobFile1').html("<a href='javascript:void(0);' onclick='addJobFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addJobFileType();'><spring:message code="lnkAttachFile" /></a>");
}
function validateMessage()
{
	var sumOfCaptchaText = document.getElementById("sumOfCaptchaText");
	
	$('#errordivMessage').empty();
	$('#msgMailSubject').css("background-color", "");
	$('#msgMail').css("background-color", "");
	$('#jobFile').css("background-color", "");
	$('#msgMailSpt').find(".jqte_editor").css("background-color", "");
	$('#sumOfCaptchaText').css("background-color","");
	var cnt=0;
	var focs=0;
	
	var fileSize=0;		
	if ($.browser.msie==true)
 	{	
	    fileSize = 0;	   
	}
	else
	{	
		if(document.getElementById("jobFile"))
		if(document.getElementById("jobFile").files[0]!=undefined)
			fileSize = document.getElementById("jobFile").files[0].size;
	}
	if(trim($('#msgMailSubject').val())=="")
	{
		$('#errordivMessage').append("&#149; <spring:message code="errMsgPleaseEnterSub" /><br>");
		if(focs==0)
			$('#msgMailSubject').focus();
		$('#msgMailSubject').css("background-color", "#F5E7E1");
		cnt++;focs++;
	}
	
	if ($('#msgMailSpt').find(".jqte_editor").text().trim()=="")
	{
		$('#errordivMessage').append("&#149; <spring:message code="errMsgPleaseEnterMSg" /><br>");
		if(focs==0)
			$('#msgMailSpt').find(".jqte_editor").focus();
		$('#msgMailSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
		cnt++;focs++;
	}else
	{
		var charCount=$('#msgMailSpt').find(".jqte_editor").text().trim();
		var count = charCount.length;
			if(count>1000)
			{
			$('#errordivMessage').append("&#149; <spring:message code="errMsgexceedcharlength" /><br>");
			if(focs==0)
				$('#msgMailSpt').find(".jqte_editor").focus();
			$('#msgMailSpt').find(".jqte_editor").css("background-color", "#F5E7E1");
			cnt++;focs++;
		}
	}
	if(fileSize>=2097152)
	{		
		$('#errordivMessage').append("&#149; <spring:message code="msgFilSizeLess2mb" /><br>");
		if(focs==0)
			$('#jobFile').focus();
		
		$('#jobFile').css("background-color","#F5E7E1");
		cnt++;focs++;	
	}	
	if(trim(sumOfCaptchaText.value)==""){
		$('#errordivMessage').append("&#149; <spring:message code="msgPleaseAddBothNumbers" /><br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}else if(chkCaptcha()){
		$('#errordivMessage').append("&#149; <spring:message code="msgSumBothNumbersWrong" /><br>");
		if(focs==0)
			$('#sumOfCaptchaText').focus();
		
		$('#sumOfCaptchaText').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	

	if(cnt==0)
		return true;
	else
	{
		changeCaptcha('sumOfCaptchaText');
		$('#errordivMessage').show();
		return false;
	}
}
function downloadJobDescription()
{
	DistrictPortfolioConfigAjax.downloadJobDescription(${jobOrder.jobId},{ 
		async: false,
		errorHandler:handleError,
		callback:function(data){
			document.getElementById("hrefJobDesc").href = data; 
		}
	});
}


$(function(){
	try{
		$('#jobDescriptionHTMLConvert').html($('#jobDescriptionHTMLConvert').html().replace(new RegExp("&amp;#x13&amp;#x10;", 'g'), '<br/>'));
	}catch(e){}		
});

</script>
<script>
$("#iconpophover16").tooltip();
</script>


<input type="hidden" id="isJobApproved" name="isJobApproved" value="${jobOrder.approvalBeforeGoLive}"/>
<input type="hidden" id="redirectURL" name="redirectURL" value="${redirectURL}"/>

<div class="modal hide" id="isJobApprovedPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					This Job is not available at this moment.
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="oncancelClick()">Ok <i class="icon"></i></button>&nbsp;&nbsp;
				<!-- <button class="btn" data-dismiss="modal">Cancel</button> &nbsp;&nbsp;  -->
			</div>
		</div>
	</div>
</div>


<div class="modal hide" id="talentAuthenticateModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
			<button type="button" class="close" onclick="hideModalById('talentAuthenticateModel',0)">x</button>
				<h3 id="talentAuthenticateModelLabel">TeacherMatch</h3>
				
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
					You were sent an authentication email to the following email address:( ${teacherDetail.emailAddress} ). Please click the link provided in the email to complete the signup process and log in.  .
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" data-dismiss="modal" onclick="hideModalById('talentAuthenticateModel',1);">Ok <i class="icon"></i></button>&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>


<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'></td></tr>
	</table>
</div>



<script>
function oncancelClick()
{
	var redirectURL = $("#redirectURL").val();
	//alert("redirectURL:- "+redirectURL);
	if(redirectURL!=null & redirectURL!="" & redirectURL!=undefined)
	{
		window.location.assign(redirectURL);
	}
}

$(document).ready(function(){
	var approvalBeofreGoLive = $("#isJobApproved").val();
	if(approvalBeofreGoLive!="1")
	{
		$('#isJobApprovedPopup').modal('show');
		$("#applyJobTopId").hide();
		$("#applyJobBottomId").hide();
	}
}); 

applyScrollOnTb();

</script>
<script type="text/javascript">
	   var cal = Calendar.setup({
	          onSelect: function(cal) { cal.hide() },
	          showTime: true
	      });
	      cal.manageFields("empaproretdatefe6", "empaproretdatefe6", "%m-%d-%Y"); 
	       cal.manageFields("retiredempdateofretirement", "retiredempdateofretirement", "%m-%d-%Y");
	      cal.manageFields("formalempdatesofemployee", "formalempdatesofemployee", "%m-%d-%Y");

</script>
