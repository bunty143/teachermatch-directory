<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/SchoolAjax.js?ver=${resourceMap['SchoolAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/manageschool.js?ver=${resourceMap['js/manageschool.js']}"></script>

<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 

<script>
var $j=jQuery.noConflict();
$j(document).ready(function() {
});
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        
           <c:if test="${userMaster.entityType ne 3}">
          colratio:[237,140,110,110,215,117], // table header width // table header width
        </c:if>
                
        <c:if test="${userMaster.entityType eq 3}">
           colratio:[237,140,120,120,225,50], // table header width
        </c:if>
        
        
        
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

</script>
<style>
	.hide
	{
		display: none;
	}
</style>


<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headManageSchool"/></div>	
         </div>			
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
	
	 <c:if test="${not empty userMaster}">
		<c:if test="${userMaster.entityType eq 3}">	        			
			<c:set var="hide" value=""></c:set>
			<c:set var="hide" value="hide"></c:set>
		</c:if>
	</c:if>
	<div class="row" onkeypress="return chkForEnterRecordsByEntityType(event);" >		
			<form class="bs-docs-example" onsubmit="return false;">
				<div class="mt10 <c:out value="${hide}"/>">
					<div class="col-sm-3 col-md-3">
						<input type="hidden" id="loggedInEntityType" name="loggedInEntityType" class="span1" value="${userMaster.entityType}"/>
						<label><spring:message code="lblEntityType"/></label>
					    <select class="form-control" id="MU_EntityType" name="MU_EntityType" onchange="displayOrHideSearchBox(<c:out value="${userMaster.entityType}"/>)">
						 <c:if test="${not empty userMaster}">
						 	 <c:set var="hide" value=""></c:set>		
						 	 <c:if test="${userMaster.entityType eq 1}">	        			
			         			 <c:set var="hide" value="hide"></c:set>
			         			<option value="2"><spring:message code="optDistrict"/></option>
								<option value="3"><spring:message code="optSchool"/></option>
								<option value="1" selected="selected"><spring:message code="sltTM"/></option>         			
			        		</c:if>	
			        		<c:if test="${userMaster.entityType eq 2}">	
			        			<c:set var="hide" value=""></c:set>
			         			<c:set var="hide" value="hide"></c:set>        			
			         			<option value="2" selected="selected"><spring:message code="optDistrict"/></option>
								<option value="3"><spring:message code="optSchool"/></option>
								<%-- <option value="1" >TM</option>--%>         			
			        		</c:if>
			        		<c:if test="${userMaster.entityType eq 3}">	        			
			         			<c:set var="hide" value=""></c:set>
			         			<c:set var="hide" value="hide"></c:set>
			         			<%--<option value="2">District</option>  --%>
								<option value="3" selected="selected"><spring:message code="optSchool"/></option>
								<%-- <option value="1">TM</option>   --%>         			
			        		</c:if>
			              </c:if>						
						</select>			
	              	</div>
		            <div  id="Searchbox" class="col-sm-5 col-md-5  <c:out value="${hide}"/>">
	         		
		             			<label id="captionDistrictOrSchool"><spring:message code="lblDist/School"/></label>
			             		<input type="text" id="districtORSchoolName" name="districtORSchoolName" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"	/>
								<input type="hidden" id="districtOrSchooHiddenlId"/>
								<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')"  class='result' ></div>	
			           
			             	<input type="hidden" id="userSessionEntityTypeId" value="${userSession.entityType}"/>
							<input type="hidden" id="schoolSessionId" value="${userSession.schoolId.schoolId}"/>
			             	 <input type="hidden" id="districtSessionId" value="${userSession.districtId.districtId}"/>
			             	<input type="hidden" id="schoolHiddenId" value="${userMaster.schoolId.schoolId}"/>
			             	<input type="hidden" id="districtHiddenId" value="${userMaster.districtId.districtId}"/>
		             
	          		</div>
					<div class="col-sm-2 col-md-2 top25-sm">
						<label>&nbsp;</label>
						<c:if test="${fn:indexOf(roleAccess,'|5|')!=-1}">
						<button class="btn btn-primary fl" type="button" onclick="searchSchool()"><spring:message code="btnSearch"/><i class="icon"></i></button>
						</c:if>
					</div>
        	  	</div>
       		 </form>
		</div>            
	
<div  class="modal hide"  id="myModalUpdateMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
 <div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
			<h3 id="myModalLabel"><spring:message code="headManageSchool"/></h3>
		</div>
		<div class="modal-body">		
			<div class="control-group">
				<div class="">
			    	<span id="updateMsg"></span>	        	
				</div>
			</div>
	 	</div>
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 		</div>
	</div>
  </div>
</div>

  <div class="TableContent top15">        	
            <div class="table-responsive" id="divMain">          
                	         
            </div>            
  </div> 

    <br><br>
	<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
<!-- Message Div add by Rahul -->
<div  class="modal hide"  id="myModalactMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='confirmDeactivate'><spring:message code="msgAreYuSuWantToDeActShol"/>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="toggleStatus()"><spring:message code="btnYes"/></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()"><spring:message code="btnNo"/></button>
 	</div>
 	<input type="hidden" id="entitystat" name="entitystat">
 	<input type="hidden" id="actschool" name="actschool">
 	<input type="hidden" id="diststat" name="diststat">
 	
</div> 	
</div>
</div>

<!-- END -->
<script type="text/javascript">
	displaySchoolMasterRecords(<c:out value="${userMaster.entityType}"/>);
	updateMsg('<c:out value="${authKeyVal}"/>',<c:out value="${userMaster.entityType}"/>);
</script>
                 
                 