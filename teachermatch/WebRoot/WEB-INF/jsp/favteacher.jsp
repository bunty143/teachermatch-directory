<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<form:form commandName="favoriteTeacher" onsubmit="return chkFavTeacher()">
<form:hidden path="favTeacherId"/>
<div class="row">
	<div class="span14">
		<div class="span14 centerline offset1 mt30">
			<div class="span1 m0"><img width="41" height="41" src="images/favourite-teacher.png"></div>
			<div class="span7 subheading">Who Was Your Favorite Teacher</div>
		</div>
	</div>
</div>

<div class="row offset1 mt15">
	<div class="row"> 
		<div class="span6">
	    	<div  id='divServerError' class='divErrorMsg' style="display: block;">${msgError}</div>
	    	<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
        </div>	              	
	</div>
	<div class="row">
		<div class="span8">
			<label><strong>Favourite Teacher Name<span class="required">*</span></strong></label>
			<form:input path="favTeacherName" cssClass="span8" maxlength="50"/>
		</div>
	</div>

	<div class="row">
		<div class="span8">
			<label>
				<strong>Favourite Teacher Story<span class="required">*</span></strong>
			</label>
			<form:textarea path="favTeacherStory" cssClass="span8" rows="8" maxlength="750" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></form:textarea>
		</div>
	</div>
	<div class=" mt30">			
		<button class="btn btn-large btn-primary" type="button" onclick="chkFavTeacher()">
			Submit <i class="icon"></i>
		</button>
		&nbsp;&nbsp;&nbsp;&nbsp;<a href="userdashboard.do"><spring:message code="btnClr" /></a>		
		<br><br>
	</div>	
</div>
</form:form>
<script type="text/javascript" src="js/teacher/dashboard.js?ver=${resourceMap['js/teacher/dashboard.js']}">
</script>
<script>
	document.getElementById("favTeacherName").focus();
</script>
