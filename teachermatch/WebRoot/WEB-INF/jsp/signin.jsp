<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet" href="css/dialogbox.css" />

<!-- This script is used to identify that, Is current login user is type 3 and if user is type 3
	then also identify that Is he belongs to multiple schools? If yes then open popup -->
<script  type="text/javascript">
function validateUser()
{
	var flag = loginUserValidate();
  	if(flag==true)
  	{
  		$.ajax(
  		{
  			url: "isSA.do",
  			type: "POST",
  			async: false,
  			data:
  			{
  				emailAddress: $("#emailAddress").val(),
  				password: $("#password").val(),
  			},
  			success:function(result)
  			{
  				var myresult = jQuery.parseJSON(''+result+'');
  				//alert("myresult.isType3User:- "+myresult.isType3User+"\nmyresult.isAuthorizedUser:- "+myresult.isAuthorizedUser+"\myresult.noOfSchools:- "+myresult.noOfSchools+"\nmyresult.noOfSchools>1:- "+(myresult.noOfSchools>1));
  				if(myresult.isType3User=="true" && myresult.isAuthorizedUser=="true" && myresult.noOfSchools>1)
  				{
  					//$("#selectSchoolName").focus();
  					
  					$('#selectSchoolName').find('option').remove();//remove the previous element.
  					$("#modalErrorMsg").text("");//remove the error message.
  					$('#selectSchoolName').append("<option value='none'><spring:message code="msgStSchool"/></option>");
  					$.each(myresult.schoolsDetails, function(index, data)
  					{
  						$('#selectSchoolName').append("<option value="+data.schoolId+">"+data.schoolName+"</option>");
  					});
  					$("#selectSchoolDiv").modal("show");
  					
  				}
  				else if(myresult.isType3User=="false" && myresult.isAuthorizedUser=="true" && myresult.noOfBranches>1)
  				{
  					$('#branchName').find('option').remove();//remove the previous element.
  					$("#modalBranchErrorMsg").text("");//remove the error message.
  					$('#branchName').append("<option value='none'>Select Branch</option>");
  					$.each(myresult.branchDetails, function(index, data)
  					{
  						$('#branchName').append("<option value="+data.branchId+">"+data.branchName+"</option>");
  					});
  					$("#selectBranchDiv").modal("show");
  					
  				}
  				else
  				{
  					try
  					{
  					if(myresult.noOfBranches!="" && myresult.noOfBranches==1)
  					{
  						$.each(myresult.branchDetails, function(index, data)
  						{
  							$('#branchName').append("<option value="+data.branchId+" selected>"+data.branchName+"</option>");
  							$("#branchID").val(data.branchId);
  							$("#brName").val(data.branchName);
  						});
  					}
  					else
  					if(myresult.noOfSchools!="" && myresult.noOfSchools==1)
  					{
  						$.each(myresult.schoolsDetails, function(index, data)
  						{
  							$("#schoolID").val(data.schoolId);
  							$("#schoolName").val(data.schoolName);
  						});
  					}
  					}
  					catch(err){}
  					
  					$("#signinForm").submit();
  				}
  			},
  			error:function(jqXHR, textStatus, errorThrown)
  			{
  				//alert("server error status:- "+textStatus+", error Thrown:- "+errorThrown+", error jqXHR:- "+jqXHR);
  			}
  		});
  	}
  	
  	return false;
}
/*When user click on ok then send the schoolName and school Id to the server*/
function ok()
{
	var schoolID = $("#selectSchoolName").val();
	var schoolName = $("#selectSchoolName option:selected").text();
	
	if(schoolID=="none")
		$("#modalErrorMsg").text("Please select school for login.");
	else
	{
		$("#selectSchoolDiv").modal("hide");	
		$("#schoolName").val(schoolName);
		$("#schoolID").val(schoolID);
		$("#signinForm").submit();
	}
}
function cont()
{
	var branchID = $("#branchName").val();
	var branchName = $("#branchName option:selected").text();
	if(branchID=="none")
		$("#modalBranchErrorMsg").text("Please select branch for login.");
	else
	{
		$("#selectBranchDiv").modal("hide");	
		$("#brName").val(branchName);
		$("#branchID").val(branchID);
		$("#signinForm").submit();
	}
}

function cancel()
{
	$("#selectSchoolDiv").modal("hide");
	$("#emailAddress").val("");
	$("#password").val("");
	$("#emailAddress").focus();
}
</script>

<div id="displayDiv"></div>
<div class="tabletbody">
<!-- <form action="signin.do" method="post" onsubmit="return loginUserValidate();"> -->

<form  action="signin.do" method="post" id="signinForm" >
<div class="row top15">
<div class="col-sm-3 ">
<div class="subheadinglogin "style="margin-bottom: -35px; margin-left: 242px;">Sign&nbsp;In</div>
</div>
</div>
<c:set var="test" value="${fn:escapeXml(param.key)}"/>
<c:choose>
      <c:when test="${fn:contains(test,'script')}">
     
      <input type="hidden" id="val" name="val" value="1">
      <input type="hidden" id="key" name="key" value="">
      </c:when>
      <c:otherwise>
      <input type="hidden" id="key" name="key" value="${fn:escapeXml(param.key)}">
      </c:otherwise>
</c:choose>
<c:set var="test" value="${fn:escapeXml(param.id)}"/>
<c:choose>
      <c:when test="${fn:contains(test,'script')}">
        <input type="hidden" id="val" name="val" value="1">
      <input type="hidden" id="id" name="id" value="">
      </c:when>
      <c:otherwise>
         <input type="hidden" id="id" name="id" value="${fn:escapeXml(param.id)}">
      </c:otherwise>
</c:choose>
<input type="hidden" id="schoolName" name="schoolName" value="">
<input type="hidden" id="schoolID" name="schoolID" value="">


<input type="hidden" id="brName" name="brName" value="">
<input type="hidden" id="branchID" name="branchID" value="">
						
                        <c:if test="${param.y eq 1}">
                        <div class="row"  style='padding-bottom:5px;padding-top:40px;'>
                          <div  class="col-sm-6 col-md-offset-3" style="padding-top: 0px;"  >
                          
                                    <spring:message code="msgContWithAppLogin"/>
                                    
                          </div>
                         </div>
                        </c:if>
                        <div class="row top15">
						  <div class="col-sm-3 col-md-offset-3">
						   	  <div class="subheadinglogin"></div>
						  </div>
						  						  
						  <div class="col-sm-3 col-md-offset-3 systemsetup">						     
						      <a href="systemsetup.do" target="_blank">&nbsp;</a> <button  class="btn fl btn-secondary" type="button" onclick="return testYourSetUp();"><spring:message code="btnTestSetup"/> <i class="icon2"></i></button>						   
						  </div>
						  
						 </div> 

						<!--  scheduled maintenance start -->
						<!-- <div class="row"  style='padding-bottom:20px;'>
						  <div  class="col-sm-6 col-md-offset-3"  style="border: 1px solid;border-color:red;align:center;margin-top:10px;">
						   	  <div>	Dear Partner,</br> 
							  	In an effort to keep your system updated – as well as you informed in the process – TeacherMatch has planned a system maintenance from Monday, August 3, 2015 beginning at 8pm CDT, through Tuesday, August 4, 2015 at 1am CDT.</br>
								We do not anticipate connectivity issues or functionality interruptions outside of this planned time.<br/>
								If you have questions about your system experience, please contact our Partner Success Center at <a href="mailto:partners@teachermatch.org" style="color:red;"><b>partners@teachermatch.org</b></a> or <span style="color:red;"><b>855.980.0545</b></span>.</br>
								Best,</br>
								TeacherMatch Partner Success Team 
								</div>
						  </div>
						 </div> -->
						 <!--  scheduled maintenance end -->

						 <div class="row">  
                         <div class="col-md-offset-3">
   							        <div class="col-sm-6">			                         
									<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
									<div class='divErrorMsg' id='divServerError' style="display: block;">${msgError}</div>
									<div class='divErrorMsg' id='divServerError' style="display: block;">${errorMessage}</div>	
				   					</div>  
                         </div>
                         </div>
                         <div class="row top10">
						 <div class="col-md-offset-3"> 						                     
                                   <div class="col-sm-6">
                                   
                                        <label><spring:message code="lblEmail"/></label>
                                        
                                   	    <input type="text" name="emailAddress" value="${emailAddress}" id="emailAddress" placeholder="<spring:message code="txtEnterEmail"/>" class="form-control"  /> 
                                   </div>  
                          </div>
                          </div>  
                          <div class="row top-sm0">      
                          <div class="col-md-offset-3">            
                                  <div class="col-sm-4" style="width:33%;">
                                        <label><spring:message code="lblPass"/></label>
                                        <input type="password" name="password" id="password" placeholder="<spring:message code="txtEnterPass"/>" class="form-control" maxlength="18" />
                                  </div>
                                  <div class="col-sm-3 top25">
                                  	<button class="btn fl btn-primary" style="padding: 6px 17px;"  type="submit" 
                                  	id="submitLogin" onclick="return validateUser()"><spring:message code="btnLogin"/> <i class="icon"></i></button>
                                  	<!-- <button class="btn fl btn-primary" type="submit" style="padding: 6px 11px;" onclick="return loginUserValidate()">Login <i class="icon"></i></button>  -->
                                  </div>                                                                
                          </div>
                          </div>  
                          <div class="row">                          
						  <div class="col-md-offset-3" >          
                                 <div class="col-sm-3" style="width:17%;padding-right: 0px;">
	                              <div style="float: left;">
	                              	<label class="checkbox" style="    MARGIN-RIGHT: -15px;">
								    	<input type="checkbox" id="txtRememberme" name="txtRememberme"><spring:message code="boxRemeberPass"/>
									</label>								                                                             
	                              </div>                               
                              </div>  
                              <div class="col-sm-1 top5" style="width:1%;padding-right: px;padding-left: 3px;font-size: 20px;     margin-left: 20px;" >
	                                              <span><b>.</b></span>             
                              </div> 
                              <div class="col-sm-3 top10" style="padding-left: 5px;     margin-left: -10px;" >
                                           <a href="forgotpassword.do"><spring:message code="lnkForgotPass"/></a>                   
                                  </div>                                
                         </div> 
                          </div>  
                          <div class="row">                         
                          <div class="col-md-offset-3">                                             
                              <div class="left13 top10">
                              <!-- Are you currently a Teacher looking for opportunities? Please <a href="signup.do">Sign Up</a> --><!--
                              Don't have a login yet, Please <a href="signup.do">Sign Up</a>
                              -->
                              <table><tr><td><spring:message code="msgIfUDontLoginPlzSignUp"/></td> 
                              <td><button class="btn fl btn-primary" type="button" style="padding: 6px 10px;"  
                              	onclick="location.href='signup.do'"><spring:message code="btnSign"/><i class="icon"></i></button></td>
                               </tr>
                               </table> 
                              </div> 
                              
                              <!-- <div class="left15" >
                               Are you a District or School interested in hiring great teachers, please contact us <a href="http://www.teachermatch.org/contact-us/" target="_blank">here</a>
                              </div> 
                               -->
                              <div class="left15" style="margin-top:80px;margin-bottom: -10px;"  >
                               <spring:message code="msgLogSiteTM"/>  <a href="termsofuse.do"><spring:message code="lnkTrmsUse"/></a>        
                              </div>                            
                          </div> 
                     </div>         
                 
</form>


<c:if test="${teacherblockflag eq 1}">
		<div class="modal" id="teacherblockdiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >		
		 <div class="modal-dialog">
          <div class="modal-content">
			<div class="modal-header">	  		
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
			<div class="modal-body">				
				<div class="control-group">
					<div class="" id="ddd">
						<p><spring:message code="msgDueToUnsuccessfullAccClickOnIforgot"/></p>
					</div>
				</div>
		 	</div> 	
		 	<div class="modal-footer">
		 			<span><button class="btn btn-large btn-primary" onclick="hideDive()"><spring:message code="btnOk"/><i class="icon"></i></button></span>
		 	</div>
		</div>
	  </div>
	</div>		
</c:if>
<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 	</table>
</div>
</div>

<!-- -------------Show this popup if user is type 3 and has multiple schools--------------- -->
<div class="modal fade" id="selectSchoolDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content"  style="top:141px;left:-50px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" onclick="cancel()" aria-hidden="true">x</button>
				<h3><spring:message code="headTm"/></h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="modalErrorMsg" class="required" style="display:block;width:100%;height:10px;margin-top:-10px;"></div>
					<div class="row top10">
						<div class="col-sm-offset-0">
							<div class="col-sm-10" >
								<label><spring:message code="lblPlzSltSchool"/></label>
								<select id="selectSchoolName" name="selectSchoolName" class="form-control" style="font-family:'Century Gothic', 'Open Sans', sans-serif;font-size: 11px;line-height: 20px;">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" onclick="ok()"><spring:message code="btnLogin"/> <i class="icon"></i></button>
				<!-- <button class="btn btn-primary" onclick="ok()">Ok</button>  -->
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="cancel()"><spring:message code="btnClr"/></button>
			</div>
		</div>
	</div>
</div>
<!-- --------------------------------End of showing popup-------------------------------- -->


<!-- -------------Show this popup if user is type 6 and has multiple branches--------------- -->
<div class="modal fade" id="selectBranchDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content"  style="top:141px;left:-50px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" onclick="cancel()" aria-hidden="true">x</button>
				<h3>TeacherMatch</h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="modalBranchErrorMsg" class="required" style="display:block;width:100%;height:10px;margin-top:-10px;"></div>
					<div class="row top10">
						<div class="col-sm-offset-0">
							<div class="col-sm-10" >
								<label>Please select branch</label>
								<select id="branchName" name="branchName" class="form-control" style="font-family:'Century Gothic', 'Open Sans', sans-serif;font-size: 11px;line-height: 20px;">
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" type="button" onclick="cont()">Login <i class="icon"></i></button>
				<!-- <button class="btn btn-primary" onclick="ok()">Ok</button>  -->
				<button type="button" class="btn btn-default" data-dismiss="modal" onclick="close()">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!-- --------------------------------End of showing popup-------------------------------- -->

<script  type="text/javascript" language="javascript" src="js/dialogboxforaction.js"></script>
<c:if test="${authmail}">
	<script>
		var divMsg="<div><%=request.getSession()!=null && request.getSession().getAttribute("authmsg")!=null?request.getSession().getAttribute("authmsg"):"" %></div>";
		addDialogContent("am","TeacherMatch",divMsg,"displayDiv");
		showDialog("am",actionForward,"560","200");
		function actionForward(){
			window.location.href="signin.do";
		}
	</script>

</c:if>
<script>
	$("#selectSchoolDiv").hide();
	$("#selectBranchDiv").hide();
	$('#emailAddress').val("");
	$('#loadingDiv').hide();
	$("#teacherblockdiv").draggable();
	
	function divOn(){
		$('#loadingDiv').show();
	}
	document.getElementById("emailAddress").focus();
	function testYourSetUp() 
	{
		window.open("systemsetup.do");
	}
</script>
<script>
//var value=document.getElementById("val").value;
//if(value==1){
//alert("URL is not valid")
//}
</script>