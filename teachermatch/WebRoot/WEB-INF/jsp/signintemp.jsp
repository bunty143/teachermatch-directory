
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link rel="stylesheet" href="css/dialogbox.css" />
<div id="displayDiv"></div>
<div class="tabletbody">
                        <div class="row top15">
						  <div class="col-sm-3 col-md-offset-3">
						   	  <div class="subheadinglogin"></div>
						  </div>
						  						  
						  <div class="col-sm-3 col-md-offset-3 systemsetup">						     
						      						   
						  </div>
						  
						 </div> 
						 <div class="row">  
                         <div class="col-md-offset-3">
   							        <div class="col-sm-6">			                         
									<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
									<div class='divErrorMsg' id='divServerError' style="display: block;">${msgError}</div>			             
				   					</div>  
                         </div>
                         </div>
                         <div class="row top10 " style='text-align:left;padding-left:300px;  '>
						  

						<spring:message code="msgDearVisitorDueToRegularMaintenance"/>
                          </div>  
                          <div class="row top-sm0">      
                        
                          </div>  
                          <div class="row">                          
						  <div class="col-md-offset-3" >          
                                 <div class="col-sm-6">
	                           	                             
	                              <div style="clear: both;"></div>                                    
                              </div>                                  
                         </div> 
                          </div>  
                          <div class="row">                         
                          
                     </div>         
                 

<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 	</table>
</div>
</div>
