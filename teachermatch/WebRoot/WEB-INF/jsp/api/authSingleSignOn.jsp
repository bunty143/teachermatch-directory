<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style>
.bubble{
    background-color: #F2F2F2;
    border-radius: 5px;
    box-shadow: 0px 0px 6px #B2B2B2;
    height: 200px;
    margin: 20px;
    width:  275px;
}

.bubble::after {
    background-color: #F2F2F2;
    box-shadow: -2px 2px 2px 0 rgba( 178, 178, 178, .4 );
    content: "\00a0";
    display: block;
    height: 20px;
    left: -10px;
    position: relative;
    top: 20px;
    transform: rotate( 45deg );
    width:  20px;
}
</style>
<c:choose>
	<c:when test="${msgFlag eq 0}"> <%-- Display Error code with Error Msg --%>
		<div class="row offset4 msgPage">
			<div class="span8 mt30">
				<h3><spring:message code="lblErrorMessage"/>: ${errorMsg}<br/></h3>				 
			<!-- 	<h3>Error Code:    ${errorCode}</h3>  -->
			</div>
		</div>
	</c:when>
	<c:when test="${msgFlag eq 1}"> <%-- Display Error code with Error Msg --%>
		<div class="row offset4 msgPage">
			<div class="span8 mt30">
				<spring:message code="lblYsuccessfullyloggedin"/>
			</div>
		</div>
	</c:when>
</c:choose>
