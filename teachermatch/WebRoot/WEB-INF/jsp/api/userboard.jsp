<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript" src="../dwr/interface/APIAssessmentCampaignAjax.js"></script>
<script type="text/javascript" src="../dwr/engine.js"></script>
<script type='text/javascript' src='../dwr/util.js'></script>
<script type='text/javascript' src='../js/api/api.assessment-campaign.js'></script>
<script type='text/javascript'>
if("${param.w}"!='')
{
	var msg ="<spring:message code='msgApiUserBoard1'/>"
	var nextMsg =" <spring:message code='msgApiUserBoard2'/>";
	if("${param.w}"==1){
		if("${param.attempt}"==1){
			msg = "<b>You Did Not Take Action!</b><br>";
			nextMsg = "You have been returned to your Dashboard because you did not acknowledge your <b>FIRST </b><font color='red'>&#8220;Timed Out&#8221;</font> alert. You have <b>ONE MORE</b> attempt. Be sure to respond to questions within the <font color='red'>75-second</font> time limit or you may get a <font color='red'>&#8220;Timed Out&#8221;</font> message again. When you are ready to complete the <b>EPI</b> without interruption, click <b>&#8220;Resume EPI&#8221;</b> on your Dashboard.";
		}
		else if("${param.attempt}"==2){
			msg = "<b>Are you still here?</b><br>";
			nextMsg = "You have been returned to your Dashboard because you did not acknowledge your <b>SECOND </b><font color='red'>&#8220;Timed Out&#8221;</font> alert. You have <b>ONE FINAL</b> attempt. Be sure to respond to questions within the <font color='red'>75-second</font> time limit or you may get your last <font color='red'>&#8220;Timed Out&#8221;</font> message. After that, you will need to wait 12 months before taking the EPI again. When you are ready to complete the <b>EPI</b> without interruption, click <b>&#8220;Resume EPI&#8221;</b> on your Dashboard.";
		}
	}
	else if("${param.w}"==2){
		nextMsg =" <spring:message code='msgApiUserBoard3'/>";
	}
	showInfo(msg+nextMsg);
	//window.location.href='userdashboard.do';
}
function showInfo(msg)
{
	//$("#myModalv").css({ top: '60%' });
	
	$('#warningImg1').html("<img src='../images/info.png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html("");
	
	var onclick = "onclick=window.location.href='startOrContinueEPI.do'";
	
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
	
}
</script>

<c:if test="${jobOrder.jobId>0}">
<script language="javascript">

var url="${jobOrder.exitURL}";
if(url.indexOf("http")==-1)
url="http://${jobOrder.exitURL}";

var showurl="";
if(url.length>70)
{
  for(var i=0;i<url.length;i=i+70)
  {
	  showurl=showurl+"\n"+url.substring(i, i+70)
  } 
}

var sts = "0";
var mf="${mf}";
//alert(mf);
var divMsg="<spring:message code='msgApiUserBoard4'/>";
if(mf=='n')
{
	divMsg="<spring:message code='msgApiUserBoard5'/>";
	sts = "2";
}

divMsg=divMsg+" the TeacherMatch part of the job application. You will be directed to <a href='"+url+"' target='_blank'>"+showurl+"</a> in a new browser window, however please note that your existing TeacherMatch session is also available in your current browser window.";
showInfoAndRedirect(sts,divMsg,'',url,1);

function redirectTo()
{
	window.location.href='startOrContinueEPI.do';
	window.open(url,"TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
}

function showInfoAndRedirect(sts,msg,nextmsg,url,epiFlag)
{
	//$("#myModalv").css({ top: '60%' });
	//$("#myModalv").css({ width: '50%' });
	var img="info";
	if(sts==1)
		img = "warn";
	else if(sts==2)
		img = "stop";
	
		$('#warningImg1').html("<img src='../images/"+img+".png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html(nextmsg);
	
	var onclick = "";
	if(url!="")
		onclick = "onclick='redirectTo()'";
	
	
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
}
</script>
</c:if>
<c:choose>
	<c:when test="${tCreatedOrExist}">
		<div class="row offset3 ">
			<div class="span9 mt30" style="text-align: justify">
			<!--<div id='stratmsg'>
				<spring:message code="msgEPIBoard1"/> 
				<br/><br/>
				<spring:message code="msgEPIBoard2"/> 
				<br/><br/>
				<spring:message code="msgEPIBoard3"/>   
				<br/><br/>
				<spring:message code="msgEPIBoard4"/>
			</div>-->
				<div class="control-group mt25">
					<div class="controls" style=" text-align:left">	
						<c:choose>
							<c:when test="${baseStatus eq 'comp'}"> 
								<!--<script>document.getElementById('stratmsg').style.display='none'</script>-->
								<center> <img src="../images/360_epi_target.jpg"/>	<br/><br/>
								<h3><b><spring:message code="msgApiUserBoard7"/>.</b></h3></center>
							</c:when>
							<c:when test="${baseStatus eq 'vlt'}">
								<!--<script>document.getElementById('stratmsg').style.display='none'</script>-->
								<center> <img src="../images/360_epi_target.jpg"/>	<br/><br/>
								<h3><b><spring:message code="msgApiUserBoard8"/>.</b></h3></center>
							</c:when>
							<c:when test="${baseStatus eq 'icomp'}">
								${assessmentDetail.splashInstruction1}<br/><br/>
								<div class="col-sm-10 col-md-10 checkbox inline" >
										<input type="checkbox" name="affidavit" id="affidavit" ${affDisable} ${affChk} />
										<label><spring:message code="pConfirm"/></label>
								</div>
								<br/><br/>
								<button class="btn fl btn-primary" type="submit" onclick="saveAffidavit();return checkInventory('0',${epiJobId});"><spring:message code="btnResume"/> <i class="icon"></i></button>
							</c:when>
							<c:when test="${baseStatus eq 'resume'}">
								${assessmentDetail.splashInstruction1}<br/><br/>
								<div class="col-sm-10 col-md-10 checkbox inline" >
										<input type="checkbox" name="affidavit" id="affidavit" ${affDisable} ${affChk} />
										<label><spring:message code="pConfirm"/></label>
								</div>
								<div>
									<br/><br/>
									<button class="btn fl btn-primary" type="submit" onclick="saveAffidavit();return checkInventory('0',${epiJobId});"><spring:message code="btnStartEPI"/> <i class="icon"></i></button>
								</div>
							</c:when>
						</c:choose>			                
					</div>			            
				</div>
			</div>
		</div>
		<div style="display:none;" id="loadingDivInventory">
		     <table  align="left" >
		 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><img src="../images/please.jpg"/></td></tr>
		 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="../images/loadingAnimation.gif"/></td></tr>
		 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'><spring:message code="lblInventBengLoaded"/></td></tr>
			</table>
		</div>
	</c:when>
	<c:otherwise>
		<div class="mt10 span16" >
			<div class="span8 mt30">
				<spring:message code="lblErrorMessage"/>: ${errorMsg}<br/>				 
				<spring:message code="lblErrorCode"/>:    ${errorCode}
			</div>
		</div>
	</c:otherwise>
</c:choose>

