<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="../js/teacher/dashboard.js"></script>
<script type="text/javascript" src="../js/api/apitest.js"></script>



<link rel="stylesheet" type="text/css" href="../css/base.css" />  

<script type="text/javascript">
var $j=jQuery.noConflict();

        $j(document).ready(function() {
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 400,
        width: 973,
        minWidth: null,
        minWidthAuto: false,
        colratio:[50,75,150,175,75,450], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
applyScrollOnTbl();

</script>




<div class="row centerline offset1 mt30">
	<div class="span1 m0"><img width="41" height="41" src="../images/applyfor-job.png"></div>
	<div class="span7 subheading"><spring:message code="lblTeacher_JobOrder"/></div>
</div>

<div class="row offset1">
	<br/>
	<div class="row">
		<div class="span3">
			<label><spring:message code="lblEncrypttoBase64"/></label>			
		</div>
		<div class="span5">			
			<input type="text" id="txtEncData" name="txtEncData" class="span5">			
		</div>
		<div class="span2">
			<input type="button" value="<spring:message code='btnEncrypt'/>" class="btn btn-primary fl" onclick="setEnc()">			
		</div>
		<div class="span4">
			<label id="lblEncTxt"></label>			
		</div>
	</div>
	
	<%--
	<div class="row">
		<div class="span3">
			<label>Decrypt from Base64</label>			
		</div>
		<div class="span5">			
			<input type="text" id="txtDecData" name="txtDecData" class="span5">			
		</div>
		<div class="span2">
			<input type="button" value="Decrypt" class="btn btn-primary fl" onclick="setDec()">			
		</div>
		<div class="span4">
			<label id="lblDecTxt"></label>			
		</div>
	</div>
 	--%>

	<div class="row">
		<div class="span5">
			<label><spring:message code="lblCandidateFirstName"/></label>
			<input type="text" id="candidateFirstName" name="candidateFirstName" class="span5"  maxlength="50" >
		</div>
		<div class="span5">
			<label><spring:message code="lblCandidateLastName"/></label>
			<input type="text" id="candidateLastName" name="candidateLastName" class="span5">			
		</div>
		<div class="span5">
			<label><spring:message code="lblCandidateEmailAddress"/></label>
			<input type="text" id="candidateEmail" name="candidateEmail" class="span5" >			
		</div>
	</div>
	
	<div class="row">		
		<div class="span5">
			<label><spring:message code="lblUserEmailAddress"/></label>
			<input type="text" id="userEmail" name="userEmail" class="span5" >			
		</div>
	</div>
	
	
	<c:set var="authKeyChk" value=""/>
	<c:set var="orgTypeChk" value="D"/>
	<div class="mt10">
	
		<table border="0" class="table table-bordered" id="tblGrid">
			<thead class="bg">
			<tr>			
				<th ><spring:message code="lblSNo"/></th>
				<th ><spring:message code="lblJobId"/></th>
				<th ><spring:message code="lblTitle"/></th>
				<th ><spring:message code="optDistrict"/></th>
				<th ><spring:message code="lblJoTy"/></th>				
				<th ><spring:message code="lblAct"/></th>
			</tr>
			</thead>			
			<c:forEach var="jbOrder" items="${lstJobOrder}" varStatus="status">
			<c:if test="${status.count eq 1}">			
				<c:set var="authKey" value="${jbOrder.districtMaster.authKey}"/>					
			</c:if>
			<tr>
				<td>${status.count}</td>
				<td>${jbOrder.apiJobId}</td>
				<td>${jbOrder.jobTitle}</td>
				<td>${jbOrder.districtMaster.districtName}</td>
				<td>
					<c:choose>
						<c:when test="${jbOrder.createdForEntity eq 2}">
							<c:set var="orgTypeChk" value="D"/>	
							<c:set var="authKeyChk" value="${jbOrder.districtMaster.authKey}"/>
							<spring:message code="optDistrict"/>							
						</c:when>
						<c:when test="${jbOrder.createdForEntity eq 3}">
							<spring:message code="lblSchool"/>
							<c:forEach var="schol" items="${jbOrder.school}">
								<c:set var="authKeyChk" value="${schol.authKey}"/>
								<c:set var="orgTypeChk" value="S"/>	
							</c:forEach>
						</c:when>
					</c:choose>
				</td>
				<td>
					<a href="javascript:void(0)" onclick="applyJB('${authKeyChk}','${orgTypeChk}','${jbOrder.apiJobId}')"><spring:message code="lblapplyjob"/></a>
					&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="applyJBXML('${authKeyChk}','${orgTypeChk}','${jbOrder.apiJobId}')"><spring:message code="lnkapplyjobbyXML"/></a>
					&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="applyJBJSON('${authKeyChk}','${orgTypeChk}','${jbOrder.apiJobId}')"><spring:message code="lnkapplyjobbyJSON"/></a>
					&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="storconEPI('${authKeyChk}','${orgTypeChk}')"><spring:message code="lnkStartContinueEPI1"/></a>
					<br/>
					<a href="javascript:void(0)" onclick="getCGView('${authKeyChk}','${orgTypeChk}','${jbOrder.apiJobId}')"><spring:message code="lnkCGView"/></a>
					
					&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="getAppView('${authKeyChk}','${orgTypeChk}','${jbOrder.apiJobId}')"><spring:message code="lnkApplicantView"/></a>
					&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="epiStatusChkJSON('${authKeyChk}','${orgTypeChk}')"><spring:message code="lnkEPIStatusJSON"/></a>
					&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="epiStatusChkXML('${authKeyChk}','${orgTypeChk}')"><spring:message code="lnkEPIStatusXML"/></a>
					
					<br/>
					<a href="javascript:void(0)" onclick="getCompScorByJbJSON('${authKeyChk}','${orgTypeChk}','${jbOrder.apiJobId}')"><spring:message code="lnkCompositebyjobJSON"/> </a>
					
					&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="getCompScorByJbXML('${authKeyChk}','${orgTypeChk}','${jbOrder.apiJobId}')"><spring:message code="lnkCompositebyjobXML"/></a>
					<br/>
					
					<a href="javascript:void(0)" onclick="getCompScoreJSON('${authKeyChk}','${orgTypeChk}')"><spring:message code="lnkCompositebyCandJSON"/></a>
					&nbsp;&nbsp;<a href="javascript:void(0)" onclick="getCompScoreXML('${authKeyChk}','${orgTypeChk}')"><spring:message code="lnkCompositebyCandXML"/></a>
					<br/>		
										
				</td>
			</tr>				
			</c:forEach>
		</table>
		
		<%--<a href="javascript:void(0)" onclick="applyJBXML('UUk0SEY2TjAyMDEzMDYxMzA3MzMxMw==','S','123456778q8')">apply job by XML</a>--%>
		<div class='net-widget-footer  net-corner-bottom' style="width: 970px;">
			<div class='net-widget' ">
				<table >
					<tr>
						<td>&nbsp;
							<%--<a href="javascript:void(0)" onclick="epiStatusChkJSON('${authKey}','${orgType}')">EPI Status JSON</a>
							&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="epiStatusChkXML('${authKey}','${orgType}')">EPI Status XML</a>
							
							&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="storconEPI('${authKey}','${orgType}')">Start or Continue EPI</a>
							
							&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="getCompScoreJSON('${authKey}','${orgType}')">Composite Score by Candidate JSON</a>
							&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)" onclick="getCompScoreXML('${authKey}','${orgType}')">Composite Score by Candidate XML</a> --%>							
														
						</td>
					</tr>
				</table>
			</div>
		</div>	
	</div>
</div>
<script type="text/javascript">

</script>