<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="../js/jquery.fixheadertable.js"></script>
<script type="text/javascript" src="../js/teacher/dashboard.js"></script>
<script type="text/javascript" src="../js/api/apitest.js"></script>



<link rel="stylesheet" type="text/css" href="../css/base.css" />  

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 973,
        minWidth: null,
        minWidthAuto: false,
        colratio:[500,475], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
applyScrollOnTbl();

</script>




<div class="row centerline offset1 mt30">
	<div class="span1 m0"><img width="41" height="41" src="../images/applyfor-job.png"></div>
	<div class="span7 subheading"><spring:message code="lblTeacher_JobOrder"/></div>
</div>

<div class="row offset1">
	<br/>
	<div class="row">
		<div class="span3">
			<label><spring:message code="lblEncrypttoBase64"/></label>			
		</div>
		<div class="span5">			
			<input type="text" id="txtEncData" name="txtEncData" class="span5">			
		</div>
		<div class="span2">
			<input type="button" value="Encrypt" class="btn btn-primary fl" onclick="setEnc()">			
		</div>
		<div class="span4">
			<label id="lblEncTxt"></label>			
		</div>
	</div>
	
	<%--
	<div class="row">
		<div class="span3">
			<label>Decrypt from Base64</label>			
		</div>
		<div class="span5">			
			<input type="text" id="txtDecData" name="txtDecData" class="span5">			
		</div>
		<div class="span2">
			<input type="button" value="Decrypt" class="btn btn-primary fl" onclick="setDec()">			
		</div>
		<div class="span4">
			<label id="lblDecTxt"></label>			
		</div>
	</div>
 	--%>

	<div class="row">
		<div class="span5">
			<label><spring:message code="lblCandidateFirstName"/></label>
			<input type="text" id="candidateFirstName" name="candidateFirstName" class="span5"  maxlength="50" >
		</div>
		<div class="span5">
			<label><spring:message code="lblCandidateLastName"/></label>
			<input type="text" id="candidateLastName" name="candidateLastName" class="span5">			
		</div>
		<div class="span5">
			<label><spring:message code="lblCandidateEmailAddress"/></label>
			<input type="text" id="candidateEmail" name="candidateEmail" class="span5" >			
		</div>
	</div>
	
	<div class="row">		
		<div class="span5">
			<label><spring:message code="lblUserEmailAddress"/></label>
			<input type="text" id="userEmail" name="userEmail" class="span5" >			
		</div>
	</div>
	
	
	<c:set var="authKeyChk" value=""/>
	<c:set var="orgTypeChk" value="D"/>
	<div class="mt10">
	
		<table border="0" class="table table-bordered" id="tblGrid">
			<thead class="bg">
			<tr>			
				
				<th ><spring:message code="lblDistrict"/></th>
				<th ><spring:message code="lblAct"/></th>
			</tr>
			</thead>			
			<c:forEach var="jbOrder" items="${lstJobOrder}" varStatus="status">
			<c:if test="${status.count eq 1}">			
				<c:set var="authKey" value="${jbOrder.districtMaster.authKey}"/>					
			</c:if>
			<tr>
				
				<td>
					<c:choose>
						<c:when test="${jbOrder.createdForEntity eq 2}">
							${jbOrder.districtMaster.districtName}
							<c:set var="orgTypeChk" value="D"/>	
							<c:set var="authKeyChk" value="${jbOrder.districtMaster.authKey}"/>
							(<spring:message code="lblDistrict"/>)							
						</c:when>
						<c:when test="${jbOrder.createdForEntity eq 3}">							
							<c:forEach var="schol" items="${jbOrder.school}">
								${schol.schoolName}
								<c:set var="authKeyChk" value="${schol.authKey}"/>
								<c:set var="orgTypeChk" value="S"/>	
							</c:forEach>
							(<spring:message code="lblSchool"/>)
						</c:when>
					</c:choose>
				</td>
				<td>
					
					
					<a href="javascript:void(0)" onclick="getCompScoreJSON('${authKeyChk}','${orgTypeChk}')"><spring:message code="lnksetDistrictSchool"/></a>
							
										
				</td>
			</tr>				
			</c:forEach>
		</table>		
	</div>
	
	<br/><br/>
	
	<div class="row" style="padding-left: 20px;">
		<spring:message code="msgEnterJSONtext"/>
		<div id="divJSON">
			<textarea id="txtJSON"  name="txtJSON" rows="7" cols="150">
			</textarea>
		</div>
		<input class ="btn btn-primary fl" type="button" value="Submit  JSON" onclick="addJobByXML()"/>
		<br/>
		<br/>
		<spring:message code="msgEnterXMLtext"/>
		<br/>
		<div id="divXML">
			<textarea id="txtXML"  name="txtXML" rows="7" cols="150"></textarea>
		</div>	
		<input class ="btn btn-primary fl" type="button" value="Submit  XML" onclick="addJobByJSON()"/>				
	</div>
</div>



