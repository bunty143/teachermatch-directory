<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="row offset4 msgPage">
	<div class="span8 mt30">
		${jobOrder.exitMessage} 
		<br/><a href="jobsofinterest.do"><spring:message code="lnkClickHere"/></a><spring:message code="lblToGoBck"/>
	</div>
</div>