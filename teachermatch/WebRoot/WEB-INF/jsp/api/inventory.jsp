<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<script type="text/javascript" src="../dwr/interface/APIAssessmentCampaignAjax.js"></script>
<script type="text/javascript" src="../dwr/engine.js"></script>
<script type='text/javascript' src='../dwr/util.js'></script>
<script type='text/javascript' src='../js/api/api.assessment-campaign.js'></script>

<input type="hidden" id="encryptText" name="encryptText" value="${encryptText}"/>
<script type='text/javascript'>
if("${param.w}"!='')
{
	var msg ="You have been returned to the Dashboard because you did not acknowledge that you timed out. Failure to respond to a question within the time limits again may result in timed out status."
	var nextMsg =" When you are ready to complete EPI without interruption and within the time parameters, click the Resume EPI on the Dashboard.";
	if("${param.w}"==3)
		var nextMsg =" When you are ready to complete Smart Practices without interruption and within the time parameters, click the Resume Smart Practices on the Dashboard.";
	if("${param.w}"==4)
		var nextMsg =" When you are ready to complete IPI without interruption and within the time parameters, click the Resume IPI.";
	//nextMsg =" When you are ready to complete Job Specific Inventory without interruption and within the time parameters, click the Complete Now icon to the right on Job Specific Inventory on the Dashboard. If you have multiple incomplete Job Specific Inventories, it will display you a list of incomplete Job Specific Inventories. Click the Complete Now icon against the Job Title.";
	showInfo(msg+nextMsg);
	//window.location.href='userdashboard.do';
}
function showInfo(msg)
{
//	$("#myModalv").css({ top: '60%' });
	
	$('#warningImg1').html("<img src='../images/info.png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html("");
	
	var onclick = "onclick=window.location.href='inventory.do'";
	if("${param.w}"==4){
		var encryptText = $('#encryptText').val();
		onclick = "onclick=window.location.href='inventory.do?id="+encryptText+"'";
	}		
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
	
}
</script>

<c:if test="${jobOrder.jobId>0}">
<script language="javascript">

var url="${jobOrder.exitURL}";
if(url.indexOf("http")==-1)
url="http://${jobOrder.exitURL}";

var showurl="";
if(url.length>70)
{
  for(var i=0;i<url.length;i=i+70)
  {
	  showurl=showurl+"\n"+url.substring(i, i+70)
  } 
}

var sts = "0";
var mf="${mf}";
//alert(mf);
var divMsg="Thank you for completing";
if(mf=='n')
{
	divMsg="You have not completed";
	sts = "2";
}

divMsg=divMsg+" the TeacherMatch part of the job application. You will be directed to <a href='"+url+"' target='_blank'>"+showurl+"</a> in a new browser window, however please note that your existing TeacherMatch session is also available in your current browser window.";
showInfoAndRedirect(sts,divMsg,'',url,1);

function redirectTo()
{
	window.location.href='inventory.do';
	window.open(url,"TeacherMatch","toolbar=yes,location=yes,scrollbars=yes,menubar=yes");
}

function showInfoAndRedirect(sts,msg,nextmsg,url,epiFlag)
{
	$("#myModalv").css({ top: '60%' });
	$("#myModalv").css({ width: '50%' });
	var img="info";
	if(sts==1)
		img = "warn";
	else if(sts==2)
		img = "stop";
	
		$('#warningImg1').html("<img src='../images/"+img+".png' align='top'>");
	$('#message2showConfirm1').html(msg);
	$('#nextMsg').html(nextmsg);
	
	var onclick = "";
	if(url!="")
		onclick = "onclick='redirectTo()'";
	
	
	$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true' "+onclick+">Ok</button>");
	$('#vcloseBtn').html("<span "+onclick+" >x</span>");
	$('#myModalv').modal('show');
}
</script>
</c:if>
 
<input type="hidden" id="assessmentType" name="assessmentType" value="${assessmentType}"/>
<input type="hidden" id="grpName" name="grpName" value="${grpName}"/>

<c:choose>
	<c:when test="${tCreatedOrExist}">
		<div class="row offset3 ">
			<div class="span9 mt30" style="text-align: justify">
				<div id='stratmsg'>
					<c:choose>
						<c:when test="${assessmentStatus eq 'inbound'}">
							<h3><b>Firstly, complete your SmartPractices Professional Development. Please <u><a href="../userdashboard.do">continue to your dashboard</a></u> to complete your SmartPractices Professional Development.</b></h3>
						</c:when>
						<c:when test="${assessmentStatus eq 'pass'}">
							<h3><b>You have already completed and passed the SmartPractices assessment. Please <u><a href="../userdashboard.do">continue to your dashboard</a></u> to review your certificate.</b></h3>
						</c:when>
						<c:when test="${assessmentStatus eq '1'}">
							<h3><b>The inventory has not been finalized. Please come back later to complete this application.</b></h3>
						</c:when>
						<c:when test="${assessmentStatus eq '8'}">
							<!-- <h3><b>You are not able to attempt the assessment of this Group, Please contact TeacherMatch Administrator.</b></h3> -->
							<h3><b>You have exceeded the maximum number of assessment attempts and are unable to take the assessment again. If you feel you have received this message in error, please utilize the feedback and support option to contact a member of the TeacherMatch support team.</b></h3>
						</c:when>
						<c:when test="${assessmentType eq '3'}">
							<!-- The TeacherMatch Smart Practices is a software-as-a-service tool that uses research-based predictive analytics to provide schools and school districts data about a teacher candidate’s ability to grow students before they are hired.
							<br/><br/>
							Before you start the Smart Practices Inventory, please note the following guidelines. These guidelines have been implemented to ensure that all candidates take the Smart Practices Inventory under equitable conditions. 
							<br/><br/>
							Each item in the Smart Practices Inventory has a stipulated time limit and you must respond to each question within its stipulated time limit. You must answer all of the questions in one sitting. Make sure that you have at least 90 minutes of uninterrupted time to complete the Base Inventory. Make sure that you have a stable and reliable Internet connection. Do not close your browser or hit the "back" button on your browser. You are not able to skip questions.   
							<br/><br/>
							If you do not follow these guidelines when taking the Smart Practices Inventory, your status may become "Timed out" and you will not be able to complete the Smart Practices Inventory. As per the terms of service and under penalty of being excluded from all application processes, candidates are not allowed to create a second account on the TeacherMatch platform. If a candidate's status is "Timed Out" this does not prevent him or her from applying for jobs. It is up to each school and school district to decide how to consider candidates with this designation.
							 -->
							<!--<p>Welcome valuable Kelly Educational Staffing&reg; (KES&reg;) talent. The TeacherMatch&#8482; Smart Practices Assessment tests your knowledge and skills related to the Smart Practices professional development competencies for substitute teachers and paraprofessionals.</p>
							<p><b>Before beginning, please note these IMPORTANT guidelines:</b></p>
							<ul>
								<li>While the average person takes less time to complete, make sure that you have at least 90 minutes of uninterrupted time to complete the Smart Practices Assessment to avoid any “timed out” issues and being unable to complete the assessment.</li>
							 	<li>Make sure that you have a stable and reliable internet connection.</li>
							 	<li>Do not close your browser or hit the "back" button on your browser.</li>
							 	<li>You are not able to skip questions.</li>
							 	<li><b>It is to your advantage to answer each and every item within the allocated time limit, even if you must guess.</b></li>
							</ul>
							<p>Best,<br/>Kelly Educational Staffing</p>-->
							${assessmentDetail.splashInstruction1}
						</c:when>
						<c:when test="${assessmentType eq '4'}">
							<!--The TeacherMatch IPI is a software-as-a-service tool that uses research-based predictive analytics to provide schools and school districts data about a teacher candidate’s ability to grow students before they are hired.
							<br/><br/>
							Before you start the IPI Inventory, please note the following guidelines. These guidelines have been implemented to ensure that all candidates take the IPI Inventory under equitable conditions. 
							<br/><br/>
							Each item in the IPI Inventory has a stipulated time limit and you must respond to each question within its stipulated time limit. You must answer all of the questions in one sitting. Make sure that you have at least 90 minutes of uninterrupted time to complete the IPI Inventory. Make sure that you have a stable and reliable Internet connection. Do not close your browser or hit the "back" button on your browser. You are not able to skip questions.   
							<br/><br/>
							If you do not follow these guidelines when taking the IPI Inventory, your status may become "Timed out" and you will not be able to complete the IPI Inventory. As per the terms of service and under penalty of being excluded from all application processes, candidates are not allowed to create a second account on the TeacherMatch platform. If a candidate's status is "Timed Out" this does not prevent him or her from applying for jobs. It is up to each school and school district to decide how to consider candidates with this designation.-->
							${assessmentDetail.splashInstruction1}
						</c:when>
					</c:choose>
				</div>
				<div class="control-group mt25">
					<div class="controls" style=" text-align:left">	
						<c:choose>
							<c:when test="${assessmentStatus eq 'comp'}"> 
								<script>document.getElementById('stratmsg').style.display='none'</script>
								<center> <img src="../images/360_epi_target.jpg"/>	<br/><br/>
									<c:choose>
										<c:when test="${assessmentType eq '3'}">
											<h3><b>Thank you, Your Smart Practices is Completed.</b></h3>
										</c:when>
										<c:when test="${assessmentType eq '4'}">
											<h3><b>Thank you, Your IPI is Completed.</b></h3>
										</c:when>
									</c:choose>
								</center>
							</c:when>
							<c:when test="${assessmentStatus eq 'vlt'}">
								<script>document.getElementById('stratmsg').style.display='none'</script>
								<center> <img src="../images/360_epi_target.jpg"/>	<br/><br/>
									<c:choose>
										<c:when test="${assessmentType eq '3'}">
											<h3><b>Your Smart Practices is Timed Out.</b></h3>
										</c:when>
										<c:when test="${assessmentType eq '4'}">
											<h3><b>Your IPI is Timed Out.</b></h3>
										</c:when>
									</c:choose>
								</center>
							</c:when>
							<c:when test="${assessmentStatus eq 'icomp'}">
								<c:choose>
									<c:when test="${assessmentType eq '3'}">
										<button class="btn fl btn-primary" type="submit" onclick="return checkInventory('-3',null);">Resume Smart Practices <i class="icon"></i></button>
									</c:when>
									<c:when test="${assessmentType eq '4'}">
										<button class="btn fl btn-primary" type="submit" onclick="return checkInventory('-4',null);">Resume IPI <i class="icon"></i></button> 
									</c:when>
								</c:choose>
							</c:when>
							<c:when test="${assessmentStatus eq 'resume'}">
								<c:choose>
									<c:when test="${assessmentType eq '3'}">
										<button class="btn fl btn-primary" type="submit" onclick="return checkInventory('-3',null);">Start Smart Practices <i class="icon"></i></button>
									</c:when>
									<c:when test="${assessmentType eq '4'}">
										<button class="btn fl btn-primary" type="submit" onclick="return checkInventory('-4',null);">Start IPI <i class="icon"></i></button> 
									</c:when>
								</c:choose>
							</c:when>
						</c:choose>			                
				     </div>			            
				</div>
			</div>
		</div>
				
		<div style="display:none;" id="loadingDivInventory">
		     <table  align="left" >
		 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><img src="../images/please.jpg"/></td></tr>
		 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="../images/loadingAnimation.gif"/></td></tr>
		 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'>The Inventory is being loaded...</td></tr>
			</table>
		</div>
	</c:when>
	<c:otherwise>
		<div class="mt10 span16" >
			<div class="span8 mt30">
				Error Message: ${errorMsg}<br/>				 
				Error Code:    ${errorCode}
			</div>
		</div>
	</c:otherwise>
</c:choose>

