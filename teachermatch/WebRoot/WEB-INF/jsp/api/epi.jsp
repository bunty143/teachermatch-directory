<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>


<script type="text/javascript">

</script>
<script type="text/javascript" src="../dwr/interface/APIAssessmentCampaignAjax.js"></script>
<script type="text/javascript" src="../dwr/engine.js"></script>
<script type='text/javascript' src='../dwr/util.js'></script>
<script type='text/javascript' src='../js/api/api.assessment-campaign.js'></script>


<c:choose>
	<c:when test="${msgFlag eq 0}"> <%-- Display Error code with Error Msg --%>
		<div class="row offset4 msgPage">
			<div class="span8 mt30">
				<spring:message code="lblErrorMessage"/>: ${errorMsg}<br/>				 
				<spring:message code="lblErrorCode"/>  :    ${errorCode}
			</div>
		</div>
	</c:when>
	<c:when test="${msgFlag eq 1}">
		<div class="row offset4 msgPage">
			<div class="span8 mt30">
				1111111
			</div>
		</div>
	</c:when>
	<c:when test="${msgFlag eq 2}">
		<div class="row offset4 msgPage">
			<div class="span8 mt30">				
				<spring:message code="msgYuAlreadyAppiedJob"/>.<br/>				
			</div>
		</div>
	</c:when>
	<c:when test="${msgFlag eq 3}">
		<div class="row offset4 msgPage">
			<div class="span8 mt30">
			<c:if test="${baseStart}">
					<img src="../images/360_epi_target.jpg"/>	<br/>							
			</c:if>	
				
				<spring:message code="msgappliedsuccessfully1"/>. <br/>
				<c:if test="${baseStart}">
					<spring:message code="msgcompletedEPI"/>,<br/>
					<spring:message code="lblPlease"/> <a href="startOrContinueEPI.do"><spring:message code="lnkClickHere1"/></a> <spring:message code="msgtostartcontinueEPI"/>										
				</c:if>				
			</div>			
		</div>
	</c:when>
	<c:when test="${msgFlag eq 4}">
		<div class="row offset4 msgPage">
			<div class="span8 mt30">
				444444444
			</div>
		</div>
	</c:when>
	<c:when test="${msgFlag eq 5}">
		<div class="row offset4 msgPage">
			<div class="span8 mt30">
				555555555
			</div>
		</div>
	</c:when>	
</c:choose>
<div style="display:none;" id="loadingDivInventory">
     <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><img src="../images/please.jpg"/></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="../images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'><td style='padding-top:0px;padding-left:450px;' id='spnMpro' align='center'><spring:message code="msgInventoryisbeingloaded"/>...</td></tr>
	</table>
 </div>