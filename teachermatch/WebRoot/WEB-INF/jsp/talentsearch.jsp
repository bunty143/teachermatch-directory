<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>	
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">			
<div class="container">
<center style="margin-left: 30px;margin-right: 30px;">
<div  style="color:black;font-size: 20px;margin-top: 10px;">
<spring:message code="headExpandYuEducTalentSevh"/>
</div>
<div  style="font-size: 12px;margin-top: 10px;">
<spring:message code="headExpandYuEducTalentSevh2"/></div>

<div class="row" style="margin-top: 15px;">
<div class="col-sm-12 col-md-12">
<div class="col-sm-3 col-md-3" style="width: 20%;"></div>
	<div class="col-sm-3 col-md-3" style="width: 20%;">
		<img src="images/checkq.png" /><br/>
		<b style=" font-size: 12px; color:black;"><spring:message code="msgPostAllJobOpenings"/></b>
	</div>
	<div class="col-sm-3 col-md-3" style="width: 20%;">
		<img src="images/checkq.png"/><br/>
		<b style=" font-size: 12px; color:black;"><spring:message code="msgRecHiQultyCnad"/> </b>
	</div>
	<div class="col-sm-3 col-md-3" style="width: 20%;">
		<img src="images/checkq.png" /><br/>
		<b style=" font-size: 12px; color:black;"><spring:message code="msgEmyFulyAutoRec"/></b>
	</div>
	
</div>
</div>
</div>

<div align="center" style="height: 160px; background-color: #007AB3; margin-top: 10px;">
<br/>
<div style="color: #FFFFFF; margin:15px 0px 10px 0px;font-size: 20px;"><spring:message code="msgMakeConnWithFreeJobPost"/> </div>
<BR/>
<a href="employer-sign-up.do" target="_blank"><button class="signInButtonWhite top10" type="button" style="font-size: 12px; background-color:FFFFFF;margin: auto;color:#474747;width:20%;height: 24%;"><spring:message code="lablSignUpNow"/> </button></a><br/>
</div>
<div class="container">
<div>
<div align="center" style="font-size: 20px;margin-top: 20px;color: black;"><b><spring:message code="lblComingSoon"/> </b></div>
<div align="center"  style="font-size: 12px;margin-top: 10px;">
<spring:message code="msgProfileForSchlAndDist"/> 
</div>
</div>
<div style="margin-top: 10px;margin-left: 10%;">
<div class="col-sm-5 col-md-5" align="right">
<a href="powerprofileindex.do" target="_blank"><img src="images/ppi4.png" style="width:145px;margin-top: 5px;"/></a>
</div>
<div style="text-align: left;" class="col-sm-5 col-md-5">
<div  style="font-size: 12px;margin-top: 25px;">
<spring:message code="msgProfilePowerTracker"/><br><span style="font-size: 20px;"><b><spring:message code="msgProfilePowerTracker2"/></b></span><br><spring:message code="msgProfilePowerTracker3"/>
</div>
</div>
</div>
</center>
</div>
<div align="center" style="background-color: #E7EBEA; min-height: 270px;margin-top: 30px;padding: 10px;">
<!--<video id="Video1" width="50%" height="" autoplay loop none controls>
			   <source src="images/TM-2014 r1c-HD.mp4" type="video/mp4">
			   width="500" height="250"		 			  	 		  
</video>-->
<embed style="width:56%;height: 250px;" src="https://www.youtube.com/v/P0-ytLgDXTs?autoplay=0&loop=1&theme=light&&playlist=P0-ytLgDXTs" frameborder="0" type="application/x-shockwave-flash">

</div>
<div align="center">
<div  style="font-size: 12px;margin-top: 10px;"><b><spring:message code="msgDataDrivenPeplPwrPortfolio"/>
<div style="font-size: 15px;margin-top: 20px;color: black;"><spring:message code="msgLearnMoreAt"/>  <a href="http://www.teachermatch.org" target="_blank"><b>teachermatch.org</b></a></div>
</div>
</div>
	<script type="text/javascript" src="dwr/interface/AssessmentCampaignAjax.js?ver=${resouceMap['AssessmentCampaignAjax.ajax']}"></script>
<script type='text/javascript' src="js/assessment-campaign.js?ver=${resouceMap['js/assessment-campaign.js']}"></script>
<jsp:include page="../tiles/feedbackandsupport3AfterLogin.jsp" />