
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">	

<style>
.imgdiv
{
background-color: #EAEEF1;
}
</style>

<div class=" row col-sm-12 col-md-12" style="margin:auto;">
	<div class="" style="width:40%;float:left;">		
	<script type="text/javascript" 
src="http://www.webestools.com/google_map_gen.js?lati=44.534384&long=-86.800047&zoom=5&width=530&height=600&mapType=normal&map_btn_normal=yes&map_btn_satelite=yes&map_btn_mixte=yes&map_small=yes&marqueur=yes&info_bulle=">
</script>		
	</div>	
	<div class=" row col-sm-6 col-md-6" style="padding: 2px;width:55%;float:right;">
		<div align="center">
			<h1 class="font25">Use our quick search geo-map to pinpoint<br>the locations of schools that are currently featured<br>
			in our <b>Quest</b> jobs listings</h1><br>
		</div>
		<div class=" row col-sm-12 col-md-12">
				<div class=" row col-sm-6 col-md-6" align="center">
					<select name="school" class="form-control">
						<option value=""><spring:message code="optSlt"/></option>
					</select>
						<div class="col-sm-6 col-md-6" align="left">
						<br>
						<b><spring:message code="optionSchoolLevel"/></b><br>
						<input type="checkbox" name="elem" value="elem"><b><spring:message code="optionElementary"/></b><br>
						<input type="checkbox" name="elem" value="middle"><b><spring:message code="optionMiddle"/></b><br>
						<input type="checkbox" name="elem" value="high"><b><spring:message code="optionHigh"/></b>
						</div>
						<div class="col-sm-6 col-md-6">
						<br>
						<b><spring:message code="optionTypeofSchool"/></b>:<br>
						<input type="checkbox" name="elem" value="public"><b><spring:message code="lblPublic"/></b><br>
						<input type="checkbox" name="elem" value="private"><b><spring:message code="lblPrivate"/></b><br>
						<input type="checkbox" name="elem" value="charter"><b><spring:message code="lblCharter"/></b>
						</div>
						</div>
		
				<div class=" row col-sm-6 col-md-6 left10" align="center">
				<select name="type" class="form-control">
                <option value=""><spring:message code="optSlt"/></option>
                </select>
                <br>
                <button onclick="uploadDistFile()" class="btn btn-primary" id="save" style="float: right;"><strong><spring:message code="lblFindSchool"/> <i class="icon"></i></strong></button>
				</div>		
				</div>
				<div class=" row col-sm-12 col-md-12 top10 imgdiv">
				<div class="col-sm-6 col-md-6" align="left">
                   <img src="images/Schoolimg.png" />
                  <b><span class="font15"><spring:message code="lblSchoolName"/></span></b><br>
                    <b><span class="font12"><spring:message code="lblgeomapAddress"/></span></b>
                   </div>
				<div class="col-sm-6 col-md-6" align="left">
                   <img src="images/Schoolimg.png" />
                  <b><span class="font15"><spring:message code="lblSchoolName"/></span></b><br>
                    <b><span class="font12"><spring:message code="lblgeomapAddress"/></span></b>
                   </div>
				</div>
				<div class=" row col-sm-12 col-md-12 imgdiv">
				<div class="col-sm-6 col-md-6" align="left">
                   <img src="images/Schoolimg.png" />
                  <b><span class="font15"><spring:message code="lblSchoolName"/></span></b><br>
                    <b><span class="font12"><spring:message code="lblgeomapAddress"/></span></b>
                   </div>
				<div class="col-sm-6 col-md-6" align="left">
                   <img src="images/Schoolimg.png" />
                  <b><span class="font15"><spring:message code="lblSchoolName"/></span></b><br>
                    <b><span class="font12"><spring:message code="lblgeomapAddress"/></span></b>
                   </div>
				</div>
				<div class=" row col-sm-12 col-md-12 imgdiv">
				<div class="col-sm-6 col-md-6" align="left">
                   <img src="images/Schoolimg.png" />
                  <b><span class="font15"><spring:message code="lblSchoolName"/></span></b><br>
                    <b><span class="font12"><spring:message code="lblgeomapAddress"/></span></b>
                   </div>
				<div class="col-sm-6 col-md-6" align="left">
                   <img src="images/Schoolimg.png" />
                  <b><span class="font15"><spring:message code="lblSchoolName"/></span></b><br>
                    <b><span class="font12"><spring:message code="lblgeomapAddress"/></span></b>
                   </div>
				</div>
				<div class=" row col-sm-12 col-md-12 imgdiv">
				<div class="col-sm-6 col-md-6" align="left">
                   <img src="images/Schoolimg.png" />
                  <b><span class="font15"><spring:message code="lblSchoolName"/></span></b><br>
                    <b><span class="font12"><spring:message code="lblgeomapAddress"/></span></b>
                   </div>
				
				<div class="col-sm-6 col-md-6" align="left">
                   <img src="images/Schoolimg.png" />
                  <b><span class="font15"><spring:message code="lblSchoolName"/></span></b><br>
                    <b><span class="font12"><spring:message code="lblgeomapAddress"/></span></b>
                   </div>
		        </div>
	       

</div>
</div>