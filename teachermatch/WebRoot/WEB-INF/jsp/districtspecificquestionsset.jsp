<!-- @Author: Hanzala 
 * @Discription: view of edit domain Page.
 -->
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<script type='text/javascript' src="js/jobcategory.js?ver=${resourceMap['js/jobcategory.js']}"></script>
<script type="text/javascript" src="dwr/interface/DistrictSpecificQuestionsSetAjax.js?ver=${resourceMap['DistrictSpecificQuestionsSetAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/districtspecificquestionsset.js?ver=${resourceMap['js/referencechkquestionsset.js']}"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script type="text/javascript" src="dwr/interface/ManageJobOrdersAjax.js?ver=${resourceMap['ManageJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/AutoSearchFilterAjax.js?ver=${resourceMap['AutoSearchFilterAjax.Ajax']}"></script>
<script type="text/javascript" src="js/selfServiceCommon.js?ver=${resourceMap['js/selfServiceCommon.js']}"></script>
<style type="text/css">
textarea{
	width:500px;
	height:100px;
   }
   
   .jqte{
   }
   .errMsg {
   color: red;
}
</style>

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
    });
        
function applyScrollOnTbl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
	        $j('#refChkQuesTable').fixheadertable({ //table id 
		        caption: '',
		        showhide: false,
		        theme: 'ui',
		        height: 280,
		     	width: 945,
		        minWidth: null,
		        minWidthAuto: false,
		        colratio:[436,146,120,246], // table header width
		        addTitles: false,
		        zebra: true,
		        zebraClass: 'net-alternative-row',
		        sortable: false,
		        sortedColId: null,
		        //sortType:[],
		        dateFormat: 'd-m-y',
		        pager: false,
		        rowsPerPage: 10,
		        resizeCol: false,
		        minColWidth: 100,
		        wrapper: false
	        });
        });			
}

</script>

<div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         <c:choose>
         <c:when test="${not empty headQuarterId}">
         	<div class="subheading" style="font-size: 13px;">HeadQuarter Specific Questions Set</div>
         </c:when>
         <c:otherwise>
         <div class="subheading" style="font-size: 13px;">District Specific Questions Set</div>
         </c:otherwise>
         </c:choose>
         </div>			
		 <div class="pull-right add-employment1">
			<c:if test="${addQuesFlag}">	
				<a href="javascript:void(0);" onclick="return addNewQuesSet()">+Add Question Set</a>
			</c:if>
		</div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>
<!-- Search  -->
<input type="hidden" id="checkShowDistrict" value="1" />
<div class="row col-sm-12 col-md-12">
		<div class="row">
				<div  id="Searchbox" class="">
				<% String id=session.getAttribute("displayType").toString();%>
				<input type="hidden" id="displayTypeId" value="<%=id%>"/>
				<%if(id.equalsIgnoreCase("0")){ %>
					<c:if test="${entityType eq 1}">	         		 
			           		<div class="col-sm-6 col-md-6">
			           			<label id="captionDistrictOrSchool">District</label>
			            		<input type="text" id="districtNameFilter" name="districtNameFilter" class="form-control"
			            		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowDataFilter', 'districtNameFilter','districtIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'districtIdFilter','divTxtShowDataFilter');"	/>
								<input type="hidden" id="districtIdFilter" value=""/>
								<div id='divTxtShowDataFilter'  onmouseover="mouseOverChk('divTxtShowDataFilter','districtNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
				            </div>
				     </c:if>
				     <%}%>
				     <c:if test="${entityType eq 2}">
	     		   		<div  class="col-sm-6 col-md-6">
					   		<label id="captionDistrictOrSchool">District</label>
		     				<input type="hidden" id="districtIdFilter" value="${districtId}" />
		     				<input type="text"   class="form-control" id="districtNameFilter"  disabled="disabled" value="${districtName}" />
	     				</div>
	     			</c:if>		             
		     	</div>
		  </div>
		<div class="row">
			<div class="mt10">
				 <div class="col-sm-6 col-md-6">
					<label> Question Set</label>
					<input type="text" class="form-control fl" name="quesSetSearchText" id="quesSetSearchText" >
				 </div>
				 <div class="col-sm-2 col-md-2">
					<label> Status</label>
					<select id="refChkSetStatus" name="refChkSetStatus" class="form-control">
						<option value="0" selected="selected">All</option>
						<option value="A">Active</option>
						<option value="I">Inactive</option>
					</select>
			 	</div>
			 	<div class="col-sm-2 col-md-2" style="margin-top:26px;">
			 		<button class="btn btn-primary " type="button" onclick="searchi4Ques()">Search <i class="icon"></i></button>
			 	</div>
			 </div>
		 </div>
		 <%--  
		 <div class="row">
				<div  id="Searchbox" class="">
					<c:if test="${entityType eq 5}">	         		 
			           		<div class="col-sm-5 col-md-5">
		             			<label id="captionDistrictOrBranch">Branch</label>
			             		<input type="text" id="branchNameFilter" name="branchNameFilter" class="form-control"
			             		onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowBranchDataFilter', 'branchNameFilter','branchIdFilter','');"
								onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowBranchDataFilter', 'branchNameFilter','branchIdFilter','');"
								onblur="hideDistrictMasterDiv(this,'branchIdFilter','divTxtShowBranchDataFilter');"/>
								<input type="hidden" id="branchIdFilter" value=""/>
								<div id='divTxtShowBranchDataFilter'  onmouseover="mouseOverChk('divTxtShowBranchDataFilter','branchNameFilter')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>
				     </c:if>
				     <c:if test="${entityType eq 4}">
	     		   		<div  class="col-sm-6 col-md-6">
					   		<label id="captionDistrictOrBranch">Branch</label>
		     				<input type="hidden" id="branchNameFilter" value="" />
		     				<input type="text"   class="form-control" id="branchNameFilter"  disabled="disabled" value="" />
	     				</div>
	     			</c:if>		             
		     	</div>
		  </div>
		  --%>
</div>

<div class="TableContent top15">        	
         <div class="table-responsive" id="refChkQuesSetGrid"></div>            
</div> 

	
<div class="hide" id="addQuesSetDiv">
	
		<div class="row col-sm-12 col-md-12">			                         
		 	<div class='errMsg' id="errQuesSetdiv" ></div>
		</div>
		<div class="row">
		<c:if test="${entityType eq 1}">
			<div  class="col-sm-6 col-md-6">
		        		<label>District</label>
		         		<input type="text" id="districtName" name="districtName"  class="form-control"
			         		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
							onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
							onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
							<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
						<input type="hidden" id="districtId"/>
			</div>
		</c:if>
		<c:if test="${entityType eq 2}">
	     		   	<div  class="col-sm-6 col-md-6">
				   		<label id="captionDistrictOrSchool">District</label>
	     				<input type="hidden" id="districtId" value="${districtId}" />
	     				<input type="text"   class="form-control" id="districtName" disabled="disabled" value="${districtName}" />
	     			</div>
	     </c:if>
		</div>
		<%-- 
		<div class="row">
				<div  id="Searchbox" class="">
					<c:if test="${entityType eq 5}">	         		 
			           		<div class="col-sm-6 col-md-6">
		             			<label id="captionDistrictOrBranch">Branch</label>
			             		<input type="text" id="branchNameForAdd" name="branchNameForAdd" class="form-control"
			             		onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowBranchDataFilter1', 'branchNameForAdd','branchIdForAdd','');"
								onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowBranchDataFilter1', 'branchNameForAdd','branchIdForAdd','');"
								onblur="hideDistrictMasterDiv(this,'branchIdForAdd','divTxtShowBranchDataFilter1');"/>
								<input type="hidden" id="branchIdForAdd" value=""/>
								<div id='divTxtShowBranchDataFilter1'  onmouseover="mouseOverChk('divTxtShowBranchDataFilter1','branchNameForAdd')"
								 style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>
				     </c:if>
				     <c:if test="${entityType eq 4}">
	     		   		<div  class="col-sm-6 col-md-6">
					   		<label id="captionDistrictOrBranch">Branch</label>
		     				<input type="hidden" id="branchIdForAdd" value="" />
		     				<input type="text"   class="form-control" id="branchNameForAdd"  disabled="disabled" value="" />
	     				</div>
	     			</c:if>		             
		     	</div>
		  </div>
		--%>
		
		
		<div class="row">
			 <div class="col-sm-6 col-md-6">
				<div >
					<label><strong>Question Set<span class="required">*</span></strong></label>
				</div>
				<div >
					<input type="hidden" name="quesSetId" id="quesSetId" >
					<input type="hidden" name="quesSetDate" id="quesSetDate" >
					<span id="i4QuesTxt"></span>
					<input type="text" class="form-control fl" maxlength="100" name="quesSetName" id="quesSetName" >
				</div>
			 </div>
		 </div> 
		<div class="row top5">
			<div class="col-sm-2 col-md-2">
                 <label><strong>Status<span class="required">*</span></strong></label>					 
		         <div class="left20">								 
					<label class="radio p0 top5">
						<input type="radio"  value="A" id="quesSetActive"  name="quesSetStatus" checked="checked" > Active
					</label>							
				</div>
							
				  <div style="margin-left:80px;margin-top: -30px;">						
			        <label class="radio p0 top5">
			        	<input type="radio" value="I" id="quesSetInactive" name="quesSetStatus" >Inactive
					</label>  						             
	     	      </div>
			</div>
		</div>
		
			
		 <div class="row" id="divManage">
		  	<div class="col-sm-4 col-md-4">
		  	<c:if test="${entityType ne 6}">
		  			<button onclick="saveQuestion();" class="btn  btn-primary"><strong>Save <i class="icon"></i></strong></button>
		  			</c:if>
		     	 &nbsp;<a href="javascript:void(0);" onclick="clearQues();">Cancel</a>  
		    </div>
		  </div>
</div>
<input type="hidden" id="entityType" value="${entityType}" />
<input type="hidden" id="headQuarterId" value="${headQuarterId}" />
<input type="hidden" id="branchId" value="${branchId}" />
           


<script type="text/javascript">
	var displayID=$("#displayTypeId").val();
	if(displayID!=0){
		setEntityType();
	}
	displaySets();
</script>


<!-- @Author: Hanzala 
 * @Discription: view of edit domain Page.
 -->