<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/UserAjax.js?ver=${resourceMap['UserAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JobDocumentsByDistrictAjax.js?ver=${resourceMap['JobDocumentsByDistrictAjax.Ajax']}"></script>
<script type='text/javascript' src="js/jobdocumentsbydistrict.js?ver=${resourceMap['js/jobdocumentsbydistrict.js']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" /> 
<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<style type="text/css">
.btn-success {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #5bb75b;
  *background-color: #51a351;
  background-image: -moz-linear-gradient(top, #62c462, #51a351);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#62c462), to(#51a351));
  background-image: -webkit-linear-gradient(top, #62c462, #51a351);
  background-image: -o-linear-gradient(top, #62c462, #51a351);
  background-image: linear-gradient(to bottom, #62c462, #51a351);
  background-repeat: repeat-x;
  border-color: #51a351 #51a351 #387038;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff62c462', endColorstr='#ff51a351', GradientType=0);
  filter: progid:DXImageTransform.Microsoft.gradient(enabled=false);
}

.btn-success:hover,
.btn-success:focus,
.btn-success:active,
.btn-success.active,
.btn-success.disabled,
.btn-success[disabled] {
  color: #ffffff;
  background-color: #51a351;
  *background-color: #499249;
}

.btn-success:active,
.btn-success.active {
  background-color: #408140 \9;
}
</style>
<script type="text/javascript">
var $j=jQuery.noConflict();
$j(document).ready(function(){
});
</script>
<script type="text/javascript">
function applyScrollOnTbl()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#jobDocumentsTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',     
        width: 940,    
        minWidth: null,
        minWidthAuto: false,   
        <c:if test="${entityID eq 1}">  
        colratio:[320,454,100,200],
        </c:if>
         <c:if test="${entityID eq 2}">  
        colratio:[220,250,220,130,120],
        </c:if>
	    addTitles: false,
        zebra: true,       
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 20,
        resizeCol: false,
        wrapper: false
        });
        });			
}

function applyScrollOnTblMulti()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#multiDocTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',     
        width: 700,    
        minWidth: null,
        minWidthAuto: false,   
        colratio:[200,200,300],     
	    addTitles: false,
        zebra: true,       
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 20,
        resizeCol: false,
        wrapper: false
        });
        });			
}

function applyScrollOnTblDocs()
{
	    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridDocs').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',     
        width: 700,    
        minWidth: null,
        minWidthAuto: false,   
        colratio:[150,150,150,250],     
	    addTitles: false,
        zebra: true,       
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 20,
        resizeCol: false,
        wrapper: false
        });
        });			
}

</script>
<input type="hidden" id="currentUrl" value="${currentUrl}">
<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/add-editdistrictjob-order.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">
         	<spring:message code="headA/E"/> <span id="titleDistrictOrSchool"><spring:message code="headJobDoc"/></span>
         	</div>	
         </div>
          	<div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="addFlag();openJobDocumentDiv();"><spring:message code="lnkAddJobDoc"/></a>
		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
 </div>
<input type="hidden" value="${entityID}" id="entityType"/>
<input type="hidden" value="${userId}" id="userId"/>
<input type="hidden" value="" id="uploadedFileName"/>
<input type="hidden" value="0" id="addEditFlag"/>
<input type="hidden" value="" id="edJbCateg"/>
<div id="jobDocumentDiv" style="margin-top:10px;display: none;">
<div class="col-sm-9 col-md-9">
	<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
</div>
<c:if test="${entityID eq 1}">
<div class="col-sm-9 col-md-9 schoolClass" id="districtClass" style="margin-top: 10px;width:78%;">
                    	<label><spring:message code="lblDistrict"/><span class="required">*</span></label>
                    	<input class="form-control" type="text" id="onlyDistrictName" name="onlyDistrictName"   value="${districtId}"
                    			onfocus="getOnlyDistrictAutoComp(this, event, 'divTxtDistrictData', 'onlyDistrictName','districtHiddenId','');"
								onkeyup="getOnlyDistrictAutoComp(this, event, 'divTxtDistrictData', 'onlyDistrictName','districtHiddenId','');"
								onblur="hideDistrictDiv(this,'districtHiddenId','divTxtDistrictData');checkDistrict();"	/>
								
								<div id='divTxtDistrictData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtDistrictData','onlyDistrictName')"  class='result' ></div>	
					</div>
		            <div  id="Searchbox" class="<c:out value="${hide}"/>">
		             		<div class="col-sm-5 col-md-5 hide" style="margin-top: 10px;">
		             			<label id="captionDistrictOrSchool"><spring:message code="lblDist/School"/></label>
		             			 <input type="text" id="districtORSchoolName" name="districtORSchoolName" class="form-control"
			             		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtORSchoolName','districtOrSchoolId','');"
								onblur="hideDistrictMasterDiv(this,'districtOrSchoolId','divTxtShowData');"	/>
								
								<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtORSchoolName')" style=' display:none;position:absolute;z-index: 5000' class='result' ></div>	
			             	</div>
			             	
			             	<input type="hidden" id="districtOrSchooHiddenlId" value=""/>
			             	<input type="hidden" id="userSessionEntityTypeId" value="${userMaster.entityType}"/>
							<input type="hidden" id="schoolSessionId" value="${userSession.schoolId.schoolId}"/>
			             	 <input type="hidden" id="districtSessionId" value="${userSession.districtId.districtId}"/>
			             	<input type="hidden" id="schoolHiddenId" value="${userMaster.schoolId.schoolId}"/>
			             	<input type="hidden" id="districtHiddenId" value=""/>
		            
	          		</div>
</c:if>	
<c:if test="${entityID eq 2}">
<div class="col-sm-9 col-md-9" id="districtClass" style="width:78%;">
		<label><spring:message code="lblDistrict"/></label>
		<input class="form-control" readonly="readonly" type="text" id="onlyDistrictName" name="onlyDistrictName"   value="${districtName}"/>
		<div id='divTxtDistrictData' style=' display:none;position:absolute;'  onmouseover="mouseOverChk('divTxtDistrictData','onlyDistrictName')"  class='result' ></div>
		<input type="hidden" id="districtHiddenId" value="${districtId}"/>	
</div>
</c:if>

	<div class="col-sm-11 col-md-11" >	
	 <div class="row">	 
		 <div class="col-sm-5 col-md-5" style="border: 0px solid red;">

		 		<label><spring:message code="optStrJobCat"/></label>
				<select multiple id="avlbList" name="avlbList" class="form-control" style="height: 150px;" > 

		 		<label>Select Job Category</label>
				<select multiple id="avlbList" name="avlbList" class="form-control" style="height: 150px;width:275px;" > 
				   <c:if test="${not empty jobCategoryList}">
				        <c:forEach var="avlst" items="${jobCategoryList}" varStatus="status">
							<option value="${avlst.jobCategoryId }">${avlst.jobCategoryName}</option>
						</c:forEach>
				    </c:if>
				</select>
			</div>		
			<div class="col-sm-1 col-md-1 left20-sm" style="margin-left: 0px;padding-left: 0px;"> 
				<div class="span2"> <span id="addPop" style="cursor:pointer;"><img alt="" src="images/rightarrow.jpg" width="30px"> </span></div>
				<div class="span2"> <span id="removePop" style="cursor:pointer;"><img alt="" src="images/leftarrow.jpg" width="30px"> </span></div>
			</div>
			<div class="col-sm-5 col-md-5" style="border: 0px solid green;">

				<label><spring:message code="lblSltJobCat"/><span class="required">*</span></label>	
				<select multiple class="form-control" id="attachedList" name="attachedList" style="height: 150px;">

				<label>Selected Job Category<span class="required">*</span></label>	
				<select multiple class="form-control" id="attachedList" name="attachedList" style="height: 150px;width:275px;">
				   <c:if test="${not empty attachedList}">
				        <c:forEach var="attlst" items="${attachedList}" varStatus="status">
				        	<c:set var="disable" value=""></c:set>
				        	<c:if test="${attlst.status eq 1}">
				        		<c:set var="disable" value="disabled"></c:set>
				        	</c:if> 
							<option value="${attlst.districtRequisitionNumbers.districtRequisitionId}" ${disable} >${attlst.districtRequisitionNumbers.requisitionNumber}</option>
						</c:forEach>
				    </c:if>
				</select>
			</div>	
			</div>
		  </div>

 <div class="col-sm-11 col-md-11" id="" >
<label><spring:message code="lblSltDefDoc"/><span class="required">*</span></label><br/>
<div class="col-sm-1 col-md-1" style="width:4.333333%;">
	<label class="radio">
<input type="radio" id="defaultDoc" name="defaultDoc" onclick="showOld();"/>
</label>
</div>
<div class="col-sm-4 col-md-4" style="width:30.333333%;">
<select class="form-control" id="jobDocsList" name="jobDocsList" onchange="showNewName();" disabled>
</select>
</div>
<div id="docCaption" class="col-sm-4 col-md-4" style="display: none;margin-top:-25px;">
<label><spring:message code="lbllDocName"/></label><br/>
	<input type="text" name="newName" id="newName" class="form-control" maxlength="50"/>
</div>
<div id="docExpire" class="col-sm-3 col-md-3" style="display: none;margin-top:-25px;">

<label><spring:message code="lblDoesItExp"/><span class="required">*</span></label><br/>
<label class="" style="margin-top: 0px;">
	<input type="radio" id="expire" name="expDoc" title="Yes" value="Yes" onclick="showExpDate('Yes');"/><spring:message code="lblYes"/>

<label>Does it Expire?<span class="required">*</span></label><br/>
<label class="" style="margin-top: 4px;">
	<input type="radio" id="expire" name="expDoc" title="Yes" value="Yes" onclick="showExpDate('Yes');"/>Yes
</label>
<label class="" style="margin-top: 0px;margin-left: 10px;">
	<input type="radio" id="expire" name="expDoc" title="No" value="No" onclick="showExpDate('No');" checked/><spring:message code="lblNo"/>
</label>
</div> 
</div>

<div class="col-sm-11 col-md-11" id="" style="">
<label><spring:message code="lblMyDocName"/><span class="required">*</span></label><br/>
<div class="col-sm-1 col-md-1" style="width:4.333333%;">
 <label class="radio">
	<input type="radio" id="newDoc" name="newDoc" onclick="hideOld();"/>
</label>
</div>
<div id="newCaption" class="col-sm-7 col-md-7" style="width: 63.333333%;" >
	<input type="text" name="newDocName" id="newDocName" class="form-control" disabled="disabled" maxlength="50">
</div>

<div id="expDateDiv" class="col-sm-3 col-md-3" style="display: none;margin-top:-25px;margin-left: 30px;" >
<label><spring:message code="lblDateOfExpire"/><span class="required">*</span></label><br/>
<input type="text" name="expDate" id="expDate" class="form-control" style="width:135px;"/>
</div>  
</div> 
    
<div class="col-sm-9 col-md-9"> 
	<div  id="tempdocDiv" ></div>
</div>  

<div class="col-sm-9 col-md-9" id="uploadedDiv" style="width:78%;">
			<label><spring:message code="lblUploadedDoc"/></label>
				<a href="javascript:void(0);" onclick="showPreviousDocument();"><div id="showPrevDoc"></div></a>
			</div>
			
<div id="fileUploadDiv" style="margin-top: 10px;">
<div class="col-sm-11 col-md-11">
  <span style="color:red;text-align: left;"><spring:message code="msgAcceptDocFormate"/></span>
</div>
<br/>
</div>
<!--<div style="clear:both;"></div>
--><div class="row">
<div class="col-sm-4 col-md-4">
</div>

</div>
<div class="row">
<div class="col-sm-12 col-md-12">
	<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'></iframe>
	<form id='frmDocumentUpload' enctype='multipart/form-data' method='post' target="uploadFrame" action='jobDocumentsByDistrictUploadServlet.do?fileName=${fileName}' class="form-inline">
		<input type="hidden" id="jbDocument" value=""/> 
		<input type="hidden" id="dateTime" name="dateTime"  value="${dateTime}"/>
		<div id="defDoc" class="col-sm-4 col-md-4" style="display: none;">

		<label><spring:message code="lblDefaultDoc"/></label><br/>
		<a href="javascript:void(0);" onclick="showDefaultDocument();"><div id="defaultJobDoc"></div></a>

		<label>Default Document</label><br/>
		<a href="javascript:void(0);" onclick="showDefaultDocument();" id="defdocLink"><div id="defaultJobDoc"></div></a>
		</div>
		
		<div class="col-sm-6 col-md-6">
		       <div class="row">
	    		 <div id="uploadedFiles"></div>
		       </div>
		       <input type=hidden id="hiddenIdDistrict" name="hiddenIdDistrict">
			   <div class="row">
				    <div id="myDoc" class="col-sm-6 col-md-6" style="">
					  <label>Upload My Document</label> 
					   <input name="jobdocument" id="jobdocument" type="file"/>
				    </div>
				    <div id="myDoc2" class="col-sm-6 col-md-6" style="padding-left: 0px;">
					   <br/><button onclick="return uploadFile();" class="btn btn-primary" id="upload">Upload</button>
				    </div>
			  </div>
			  <div class="row">
					 <div id="myDoc" class="col-sm-9 col-md-9" style="">
						  <label>Add Url</label> <a href="#" id="urlInstruction" rel="tooltip" data-original-title="Please Upload URL with http/https."><img width="15" height="15" alt="" src="images/qua-icon.png"></a> 
						   <input name="jobdocumentLink" id="jobdocumentLink" type="text" class="form-control" size="25"/>
					 </div>
					 <div id="myDoc2" class="col-sm-3 col-md-3 top8" style="padding-left: 0px;" >
						   <br/><button onclick="return uploadLink();" class="btn btn-primary" id="upload">Add Url</button>
					 </div>
			   </div>
		</div>
			
	</form>
	
</div>
</div>
<a href="javascript:void(0)" id="hrefDoc" onclick="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;"></a>
<iframe src="" id="ifrmDocument" width="100%" height="480px" style="display: none;"></iframe> 
<div class="row">
<div class="col-sm-10 col-md-10 mt10" id="hideSave">   	
<div class="col-sm-2 col-md-2" id="save" style="padding-right: 0px;"> 	
      	<button onclick="validateAddEditDocument();" class="btn btn-large btn-primary" ><spring:message code="btnDone"/><i class="icon"></i></button>
      	</div>
<div class="col-sm-2 col-md-2" id="update" style="padding-right: 0px;">
      	<button onclick="editJobDocs();" class="btn btn-large btn-primary" ><spring:message code="btnUpdate"/><i class="icon"></i></button>
      	</div>
<div class="col-sm-1 col-md-1 top8" style="padding-left: 0px;">
<a href="javascript:void(0);" onclick="cancelJobDocument();">Cancel</a>
</div>
</div></div>
      </div>
  <div style="clear:both;"></div>   
<div id="documentsData" style="margin-top: 20px;margin-left:15px;">
 	<div class="table-responsive" id="divMain"></div>   
</div>

<div style="display:none;" id="loadingDiv">
     <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>

<div  class="modal hide"  id="EPIMessage" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="EPIMsg"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnOK"/></button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="textMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3><spring:message code="btnOK"/></h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="textMessage"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnOK"/></button> 		
 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="successMessageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="successTextMessage"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"  onclick="hideSuccessModal();"><spring:message code="btnOK"/></button> 		
 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="multipleJobDoc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog"  style="width:760px;">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">	
			<div class="control-group">
				<div class="">
			    	<span id="multipleDocData"></span>	        	
				</div>
			</div>
	 	</div> 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" ><spring:message code="btnOK"/></button> 		
 	</div>
</div>
</div>
</div>
<div  class="modal hide"  id="removeDocModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
		<div class="modal-dialog">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  >x</button>
		<h3><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div class="">
		    	<span id="removeConfirmMessage"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="removeJobDoc();"><spring:message code="btnOk"/></button> 
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideRemoveDocModal();"><spring:message code="btnClr"/></button>		
 	</div>
</div>
</div>
</div>
	<script type="text/javascript">
	//	getZoneNameCheck(0);
				$('#addPop').click(function() {
				if ($('#avlbList option:selected').val() != null) {
					 $('#avlbList option:selected').remove().appendTo('#attachedList');
					 $("#avlbList").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
			         $("#attachedList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			       // removeFromAvailableForSchoolItemList();
			 } else {
			   document.getElementById("textMessage").innerHTML="Please select any job category # before add.";
			   $("#textMessageModal").modal("show");
			 }
			});
			
			$('#removePop').click(function() {
			       if ($('#attachedList option:selected').val() != null) {
			       		 //MultiSelect();	
			       		 //addToAvailableForSchoolItemList();
			             $('#attachedList option:selected').remove().appendTo('#avlbList');
			             sortSelectList();
			             $("#attachedList").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
			             $("#avlbList").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			             $("#avlbList").attr('selectedIndex', '-1').addAttr("selected");
			             
			} else {
			   document.getElementById("textMessage").innerHTML="Please select any job category # before remove.";
			    $("#textMessageModal").modal("show");
			}
			});
			
			function removeFromAvailableForSchoolItemList(){
				var optsDA = document.getElementById('attachedList').options;
				var optsSA = document.getElementById('avlbListSchool').options;
				var tempList = document.getElementById('tempList');
				for(var x = 0;x<optsDA.length;x++){
					for(var i = 0;i<optsSA.length;i++){
						if(optsSA[i].value==optsDA[x].value){
							var x1 = new Option(optsDA[x].text,optsDA[x].value,false,false)
							tempList.options.add(x1);
							optsSA.remove(i);
						}
					}		
			    }
		   }	
		   
		   
		   var arList=new Array();
		   function MultiSelect(){
		   		var optsAttachedDA = document.getElementById('attachedList').options;
		   		var c=0;
				for(var x=0;x<optsAttachedDA.length;x++){
					if(optsAttachedDA[x].selected){
						arList[c++]=optsAttachedDA[x];
					}
				}
		   }
		   function addToAvailableForSchoolItemList(){
		   		var optsAttachedDA = document.getElementById('attachedList').options;
				var optsDA = document.getElementById('avlbList');
				var optsSA = document.getElementById('avlbListSchool');
				var tempList = document.getElementById('tempList').options;
				var count = $('#attachedList option:selected').length;
				if(count>1){
					for(var x = 0;x<arList.length;x++){
						//var x1 = new Option(tempList[arList[x]].text,tempList[arList[x]].value,false,false)
						var x1=arList[x];
						var x2=arList[x];
						optsSA.options.add(x1);
						optsDA.options.add(x2);
					}
					var cnt=0;
					//$('#tempList option').remove();
					//for(var x = 0;x<arList.length;x++){
						//tempList.removeChiled(arList[cnt++]);
					//}
				}else{
					if(optsAttachedDA.length>0){
					for(var x = 0;x<optsAttachedDA.length;x++){
						if(optsAttachedDA[x].selected){
							var x1 = new Option(tempList[x].text,tempList[x].value,false,false)
								optsSA.options.add(x1);
								tempList.remove(x);
							}
						}
					}
			   }
		   }
		   
		   	
		  function sortSelectList(){
		  	var selectListAvlbList = $('#avlbList option');
		  	var selectListavlbListSchool = $('#avlbListSchool option');
			selectListAvlbList.sort(function(a,b){
			    a = a.value;
			    b = b.value;
			    return a-b;
			});
			$('#avlbList').html(selectListAvlbList);
			selectListavlbListSchool.sort(function(a,b){
			    a = a.value;
			    b = b.value;
			    return a-b;
			});
			$('#avlbListSchool').html(selectListavlbListSchool);
		  }
		  
			
		/***********************************************************************************************************************************/
			$('#addPopSchool').click(function() {
			    if ($('#avlbListSchool option:selected').val() != null) {
			         $('#avlbListSchool option:selected').remove().appendTo('#attachedListSchool');
			         $("#avlbListSchool").attr('selectedIndex','-1').find("option:selected").removeAttr("selected");
			         $("#attachedListSchool").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			        // removeFromAvailableForDistrictItemList();
			 } else {
			    document.getElementById("textMessage").innerHTML="Please select any job category # before add.";
			    $("#textMessageModal").modal("show");
			 }
			});
		
			$('#removePopSchool').click(function() {
			       if ($('#attachedListSchool option:selected').val() != null) {
			       		 //MultiSelectSCH();	
			       		 //addToAvailableForDistrictItemList();	
			             $('#attachedListSchool option:selected').remove().appendTo('#avlbListSchool');
			             sortSelectList();
			             $("#attachedListSchool").attr('selectedIndex',  '-1').find("option:selected").removeAttr("selected");
			             $("#avlbListSchool").attr('selectedIndex', '-1').find("option:selected").removeAttr("selected");
			             $("#avlbListSchool").attr('selectedIndex', '-1').addAttr("selected");
			             
			} else {
			   //alert("Please select any position from Attached Requisition # before remove.");
			   document.getElementById("textMessage").innerHTML="Please select any job category # before remove.";
			    $("#textMessageModal").modal("show");
			}
			});
			
			function removeFromAvailableForDistrictItemList(){
				var optsDATCH = document.getElementById('attachedListSchool').options;
				var optsDAAVL = document.getElementById('avlbList').options;
				var tempList = document.getElementById('tempList');
				for(var x = 0;x<optsDAAVL.length;x++){
					//alert(x)
					//alert("1>>> "+optsDAAVL[i].value +" x="+x);
					for(var i = 0;i<optsDATCH.length;i++){
						//alert("2>>> "+optsDATCH[x].value+"  i="+i);
						if(optsDAAVL[x].value==optsDATCH[i].value){
							var x1 = new Option(optsDATCH[x].text,optsDATCH[x].value,false,false)
							tempList.options.add(x1);
							optsDAAVL.remove(i);
						}
					}		
			    }
		   }	
		   
		   var arSCHList=new Array();
		   function MultiSelectSCH(){
		   		var optsAttachedDA = document.getElementById('attachedListSchool').options;
		   		var c=0;
				for(var x=0;x<optsAttachedDA.length;x++){
					if(optsAttachedDA[x].selected){
						arSCHList[c++]=optsAttachedDA[x];
					}
				}
		   }
		   function addToAvailableForDistrictItemList(){
		   		var optsAttachedSA = document.getElementById('attachedListSchool').options;
				var optsDA = document.getElementById('avlbList');
				var optsSA = document.getElementById('avlbListSchool');
				var tempList = document.getElementById('tempList').options;
				var count = $('#attachedListSchool option:selected').length;
				if(count>1){
					for(var x = 0;x<arSCHList.length;x++){
						//var x1 = new Option(tempList[arList[x]].text,tempList[arList[x]].value,false,false)
						var x1=arSCHList[x];
						var x2=arSCHList[x];
						optsDA.options.add(x1);
						optsSA.options.add(x2);
					}
					var cnt=0;
					//$('#tempList option').remove();
					//for(var x = 0;x<arSCHList.length;x++){
						//tempList.remove(arList[cnt++]);
					//}
				}else{
					if(optsAttachedSA.length>0){
					var optsAttachedSA = document.getElementById('attachedListSchool').options;
					for(var x = 0;x<optsAttachedSA.length;x++){
						if(optsAttachedSA[x].selected){
							var x1 = new Option(tempList[x].text,tempList[x].value,false,false)
								optsDA.options.add(x1);
								tempList.remove(x);
							}
						}
					}
			   }
		   }
		  
		/*****************************************************************************************************************************************/	
		</script> 
<input type="hidden" name="jobDocId" id="jobDocId" value=""/>
<input type="hidden" name="docExpFlag" id="docExpFlag"/>
<input type="hidden" id="prevDoc" value=""/>
<input type="hidden" id="defaultDocument" value=""/>
<input type="hidden" id="jobcatIdOnEdit" value=""/>


<script type="text/javascript">
displayJobsDocsRecord();
</script>
<script type="text/javascript">
  var now = new Date();
var calExpDate = Calendar.setup({ 
 	 onSelect: function(cal) { calExpDate.hide() },
 	 min : now,
 	 showTime: true, 
 	 });
  	calExpDate.manageFields("expDate", "expDate", "%m-%d-%Y");
</script>