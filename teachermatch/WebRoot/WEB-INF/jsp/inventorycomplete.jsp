<!-- @author Amit @date 08-May-15 -->
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="row offset4 msgPage">
	<div class="span8 mt30" style="text-align: center;">
		<c:choose>
			<c:when test="${assessmentType eq '3'}">
				<h3><b>Thank you, Your Smart Practices is Completed.</b></h3>
			</c:when>
			<c:when test="${assessmentType eq '4'}">
				<h3><b>Thank you, Your IPI is Completed.</b></h3>
			</c:when>
		</c:choose>
	</div>
</div>