<!-- @Author: Gagan 
 * @Discription: view of edit Competency Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/CompetencyAjax.js?ver=${resourceMap['CompetencyAjax.ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src="js/competency.js?ver=${resourceMap['js/competency.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {
     });
        
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#competencyTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[331,331,68,78,146], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script>
 <div class="row" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="headMagComp"/></div>	
         </div>			
		 <div class="pull-right add-employment1">
		<c:if test="${fn:indexOf(roleAccess,'|1|')!=-1}">
			<a href="javascript:void(0);" onclick="return addCompetency()"><spring:message code="lnkAddComp"/></a>
		</c:if>

		 </div>	
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>


<div class="TableContent top15">        	
         <div class="table-responsive" id="competencyGrid">          
         </div>            
</div> 


	
<div  id="divCompetencyForm" style="display: none;" onkeypress="return chkForEnterSaveCompetency(event);" class="mt10 span16">
<div class="row ">
<div class="col-sm-8 col-md-8">			                         
 			<div class='divErrorMsg' id='errordiv' ></div>
</div>
</div>
	<form  id="frmCompetency" onsubmit="return false;">
		
		<div class="row">
			<div class="col-sm-3 col-md-3">
				<label><spring:message code="lblDomName"/><span class="required">*</span>&nbsp;&nbsp;</label>
				<select id="domainId" name="domainId" class="form-control">
					<option value="0"><spring:message code="StrSltDom"/></option>
					   <c:if test="${not empty beans}">
		                 <c:forEach var="mbean" items="${beans}" varStatus="status">
							<option value="${mbean.domainId}"><c:out value="${mbean.domainName}"/></option>
						</c:forEach>
		               </c:if>
				</select>			
			</div>
	     </div>
		
		<div class="row">
			<div class="col-sm-5 col-md-5"><label><strong><spring:message code="lblCompNm"/><span class="required">*</span></strong></label>
				<input type="hidden" name="competencyId" id="competencyId" >
					<input type="hidden" name="competencyShortName" id="competencyShortName" >
				<input type="text" name="competencyName" id="competencyName" maxlength="100" class="form-control" placeholder="">
			</div>
			<div class="col-sm-2 col-md-2"><label><strong><spring:message code="lblCompetencyUId"/><span class="required">*</span></strong></label>
				<input type="text" name="competencyUId" id="competencyUId" maxlength="2" class="form-control" onKeyPress='return isNumberKey(event)' placeholder="">
			</div>
		</div>
		
		<div class="row">
			<div class="col-sm-1 col-md-1">
				<label><spring:message code="lblRk"/></label>
		    	<input type="text" name="rank" id="rank" maxlength="2" class="form-control" onKeyPress='return isNumberKey(event)' placeholder=""> 			
           </div>
          
           <div class="col-sm-2 col-md-2">
			<label><strong><spring:message code="lblStatus"/><span class="required">*</span></strong></label>
			
      		           	
				    <div class="left20">
					  	<label class=" span1 radio p0 top5">
							<input type="radio"  value="A" id="competencyStatus1"  name="competencyStatus" checked="checked" ><spring:message code="lblActive"/>
						</label>
					</div>
				
				   <div style="margin-left:80px;margin-top: -30px;">
				        <label class=" span2 radio p0 top5">
				        	<input type="radio" value="I" id="competencyStatus2" name="competencyStatus" ><spring:message code="lblInActiv"/>
						</label>
				  </div>          
       	
   	  	    </div>
   	  	</div>
		 <div class="row top20" style="display: none;" id="divManage">
		  	<div class="col-sm-4 col-md-4">
		  		<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
		     		<button onclick="saveCompetency();" class="btn btn-large btn-primary"><strong><spring:message code="btnSave"/> <i class="icon"></i></strong></button>
		     	</c:if>
		     	 &nbsp;<a href="javascript:void(0);" onclick="clearCompetency();"><spring:message code="lnkCancel"/></a>   
		    </div>
		  </div>
		
		<div class="row top15">
			<div class="col-sm-4 col-md-4 idone" id="divDone" style="display: none;">
				<c:if test="${fn:indexOf(roleAccess,'|8|')!=-1}">
					<a href="javascript:void(0);" onclick="saveCompetency();"><spring:message code="lnkImD"/></a>
				</c:if>
				 &nbsp;<a href="javascript:void(0);" onclick="clearCompetency();"><spring:message code="lnkCancel"/></a>
			</div>
		</div>
	</form>
	
</div>

          


<script type="text/javascript">
	displayCompetency();
</script>
                 
   <!-- @Author: Gagan 
 * @Discription: view of edit Competency Page.
 -->              
                 

