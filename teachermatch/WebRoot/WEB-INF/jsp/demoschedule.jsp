<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>

<script type="text/javascript" src="dwr/interface/DemoScheduleAjax.js?ver=${resourceMap['DemoScheduleAjax.Ajax']}"></script>
<script type='text/javascript' src="js/demoschedule.js?ver=${resourceMap['js/demoschedule.js']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>

<script src="calender/js/jscal2.js"></script>
<script src="calender/js/lang/en.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />  
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<link rel="stylesheet" type="text/css" href="css/style.css" /> 
  

<style>
 .icon-question-sign{
 	 font-size: 1.5em;
 }
 .popover{
  width: 400px;
  background-color: #ffffff;
  border: 1px solid #007AB4;
  }
  .circle{
  width: 48px;
  height: 48px;
  margin: 0em auto;
  }
  .popover-title {
  margin: 0;
  padding: 0px 0px 0px 0px;
  font-size: 0px;
  line-height: 0px;
  }
  .popover-content {
   margin: 0;
   padding: 1px;
  }
  .popover.right .arrow:after {
  border-width: 11px 11px 11px 0;
  border-right-color: #007AB4;;
  bottom: -11px;
  left: -1px;
  }
  div.t_fixed_header_main_wrapper {
	position 		: relative; 
	overflow 		: visible; 
  }
 div.t_fixed_header.ui .body th{
	padding: 0px;
  	line-height: 12px;
  	text-align: left;
  	vertical-align: top;
 	border-left-style:none;
 }
.hide
{
	display: none;
}
</style>
<div class="container">
	<div class="row  mt10">
		<div class="span16 centerline">
			<div class="span1 m0"><img src="images/manageusers.png" width="41" height="41"></div>
			<div class="span7 subheading"><spring:message code="headDemoSchedule"/></div>
			<div class="span3 pull-right add-employment1"></div>
		</div>
		<div class="clearfix"></div>
	</div>
	 <c:if test="${not empty userMaster}">
		<c:if test="${userMaster.entityType eq 3 or userMaster.entityType eq 2}">	        			
			<c:set var="hide" value="hide"></c:set>
		</c:if>
	</c:if>
	<div class="row">
       <div class='span10 divErrorMsg' id='errordiv' style="display: block;"></div>
    </div>   
	<div class="row">
		 <div class="span4">
		       <span class=""><label class=""><spring:message code="lblDemoDate"/><span class="required">*</span></label>
		       <input type="text" id="demoDate" name="demoDate" ></span>
	     </div>
	     <div class="span2">
		       <span class=""><label class=""><spring:message code="lblDemoTime"/><span class="required">*</span></label></span>
		       <select id="demoTime" name="demoTime" class="help-inline span2">
			        <option> <spring:message code="lblSltTime"/></option>
			       	<option>12:00</option>
			       	<option>12:30</option>
			       	
			       	<option>01:00</option>
			       	<option>01:30</option>
			       	
			       	<option>02:00</option>
			       	<option>02:30</option>
			       	
			       	<option>03:00</option>
			       	<option>03:30</option>
			       	
			       	<option>04:00</option>
			       	<option>04:30</option>
			       	
			       	<option>05:00</option>
			       	<option>05:30</option>
			       	
			       	<option>06:00</option>
			       	<option>06:30</option>
			       	
			       	<option>07:00</option>
			       	<option>07:30</option>
			       	
			       	<option>08:00</option>
			       	<option>08:30</option>
			       	
			       	<option>09:00</option>
			       	<option>09:30</option>
			       	
			       	<option>10:00</option>
			       	<option>10:30</option>
			       	
			       	<option>11:00</option>
			       	<option>11:30</option>
			       	
			     </select>
	     </div>
	     <div class="span2">
	     <span class=""><label class="mt25"></label></span>
	        <select id="meridiem" name="meridiem" class="help-inline span2">
	        	<option><spring:message code="sltSelectAM/PM"/></option>
		       	<option><spring:message code="optAM"/></option>
		       	<option><spring:message code="optPM"/></option>
		     </select>
	     </div>
	     <div class="span8">
	     <span class=""><label class=""><spring:message code="lblTimeZone"/><span class="required">*</span></label></span>
	       	<select id="timezone" name="timezone" class="help-inline span2">
	       		<option><spring:message code="lblSltTimezone"/></option>
		       	<c:forEach items="${timezone}" var="timezone"> 
					<option value="${timezone.timeZoneId}">${timezone.timeZoneShortName}</option>
				</c:forEach>
		    </select>
	     </div>
	</div>
	
	<div class="row">
		<div class="span16">
		       <span class=""><label class=""><spring:message code="lblLocti"/><span class="required">*</span></label>
		       <input type="text" id="location" name="location"  class="span10 input-small"></span>
	     </div>
	</div>
	
	<div class="row">
		<div class="span16">
		       <span class=""><label class=""><spring:message code="lbldesc/Additional"/></label>
		       <input type="text" id="disSchCmnt" name="disSchCmnt"  class="span10 input-small"></span>
	     </div>
	</div>
	
	<div class="row">
		<div class="span10">
     		<a class="pull-right" href=""><spring:message code="lnkAddAtte"/></a>
	     </div>	
		<div class="span10 table-bordered" style="height: 150px;overflow:auto;" >
	     </div>
	</div>
	
	<div class="row">
		<div class="span16">
		       <span class=""><label class=""><spring:message code="lblDisOrSchoolName"/><span class="required">*</span></label>
		       	   <c:if test="${entityType eq 2}">
			       		<input type="hidden" id="districtId" name="districtId" value="${DistrictId}"/>
			       		<input type="text" id="districtName" name="districtName" value="${DistrictName}"  class="span10 input-small">
			       </c:if>
			       <c:if test="${entityType eq 3}">
			       		<input type="hidden" id="schoolId" name="schoolId" value="${SchoolId}"/>
			       		<input type="text" id="schoolName" name="schoolName" value="${SchoolName}"  class="span10 input-small">
			       </c:if>			
		       </span>
	     </div>
	</div>
	<div class="row">
		<div class="span10">
			<span class=""><label class=""><spring:message code="lblUserName"/><span class="required">*</span></label>
		     <select id="userId" name="userId" class="help-inline span2">
		     	<option><spring:message code="lblSelectUser"/></option>
		       	<c:forEach items="${activeuser}" var="activeuser"> 
					<option value="${activeuser.userId}">${activeuser.firstName}&nbsp;${activeuser.lastName}</option>
				</c:forEach>
		   	 </select>
		   	 </span>
		</div>
	</div>
	<div class="row">
		<div class="span10" >
			<label class="checkbox inline">
		     <input type="checkbox" id="demostatus">&nbsp;<spring:message code="lblDemosWasCom"/>
		     </label>	
		</div>
	</div>
	<div class="row">
		<div class="span10" >
			<span class=""><label class=""></label></span>
			<button class="btn btn-primary" type="button" onclick=""><spring:message code="btnCancelEvent"/></button>
			<button class="btn btn-primary" style="margin-left:320px" type="button" onclick=""><spring:message code="btnClr"/></button>
			<button class="btn btn-primary"  type="button" onclick="saveDemoSchedule();"><spring:message code="btnDone"/></button>
		</div>
	</div>
</div>
<script type="text/javascript">
$('.accordion').on('show hide', function(e){
$(e.target).siblings('.accordion-heading').find('.accordion-toggle i').toggleClass('minus plus', 200);
});
</script>
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("demoDate", "demoDate", "%m-%d-%Y");
    //]]>
</script>

             
<!--==================================================================================================== -->
	
	<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  			<spring:message code="btnLaunchDemo"/>
	</button>

	<!-- Modal -->
	<div class="modalDemoSchedule hide" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        	<h4 class="modal-title" id="myModalLabel"><spring:message code="headDemoSchedule"/></h4>
	      </div>
		      <div class="modal-body">
		      		<div class="row">
						 <div class="span4">
						       <span class=""><label class=""><spring:message code="lblDemoDate"/><span class="required">*</span></label>
						       <input type="text" id="demoDate" name="demoDate" ></span>
					     </div>
					     <div class="span2">
						       <span class=""><label class=""><spring:message code="lblDemoTime"/><span class="required">*</span></label></span>
						       <select id="demoTime" name="demoTime" class="help-inline span2">
							        <option> <spring:message code="lblSltTime"/></option>
							       	<option>12:00</option>
							       	<option>12:30</option>
							       	
							       	<option>01:00</option>
							       	<option>01:30</option>
							       	
							       	<option>02:00</option>
							       	<option>02:30</option>
							       	
							       	<option>03:00</option>
							       	<option>03:30</option>
							       	
							       	<option>04:00</option>
							       	<option>04:30</option>
							       	
							       	<option>05:00</option>
							       	<option>05:30</option>
							       	
							       	<option>06:00</option>
							       	<option>06:30</option>
							       	
							       	<option>07:00</option>
							       	<option>07:30</option>
							       	
							       	<option>08:00</option>
							       	<option>08:30</option>
							       	
							       	<option>09:00</option>
							       	<option>09:30</option>
							       	
							       	<option>10:00</option>
							       	<option>10:30</option>
							       	
							       	<option>11:00</option>
							       	<option>11:30</option>
							       	
							     </select>
					     </div>
					     <div class="span2">
					     <span class=""><label class="mt25"></label></span>
					        <select id="meridiem" name="meridiem" class="help-inline span2">
					        	<option><spring:message code="sltSelectAM/PM"/></option>
						       	<option><spring:message code="optAM"/></option>
						       	<option><spring:message code="optPM"/></option>
						     </select>
					     </div>
					     <div class="span4">
					     <span class=""><label class=""><spring:message code="lblTimeZone"/><span class="required">*</span></label></span>
					       	<select id="timezone" name="timezone" class="help-inline span2">
					       		<option><spring:message code="lblSltTimezone"/></option>
						       	<c:forEach items="${timezone}" var="timezone"> 
									<option value="${timezone.timeZoneId}">${timezone.timeZoneShortName}</option>
								</c:forEach>
						    </select>
					     </div>
				</div>
				
				<div class="row">
					<div class="span16">
					       <span class=""><label class=""><spring:message code="lblLocti"/><span class="required">*</span></label>
					       <input type="text" id="location" name="location"  class="span10 input-small"></span>
				     </div>
				</div>
				
				<div class="row">
					<div class="span16">
					       <span class=""><label class=""><spring:message code="lbldesc/Additional"/></label>
					       <input type="text" id="disSchCmnt" name="disSchCmnt"  class="span10 input-small"></span>
				     </div>
				</div>
				
				<div class="row">
					<div class="span10">
			     		<a class="pull-right" href=""><spring:message code="lnkAddAtte"/></a>
				     </div>	
					<div class="span10 table-bordered" style="height: 150px;overflow:auto;" >
				     </div>
				</div>
				
				<div class="row">
					<div class="span16">
					       <span class=""><label class=""><spring:message code="lblDisOrSchoolName"/><span class="required">*</span></label>
					       	   <c:if test="${entityType eq 2}">
						       		<input type="hidden" id="districtId" name="districtId" value="${DistrictId}"/>
						       		<input type="text" id="districtName" name="districtName" value="${DistrictName}"  class="span10 input-small">
						       </c:if>
						       <c:if test="${entityType eq 3}">
						       		<input type="hidden" id="schoolId" name="schoolId" value="${SchoolId}"/>
						       		<input type="text" id="schoolName" name="schoolName" value="${SchoolName}"  class="span10 input-small">
						       </c:if>			
					       </span>
				     </div>
				</div>
				<div class="row">
					<div class="span10">
						<span class=""><label class=""><spring:message code="lblUserName"/><span class="required">*</span></label>
					     <select id="userId" name="userId" class="help-inline span2">
					     	<option><spring:message code="lblSelectUser"/></option>
					       	<c:forEach items="${activeuser}" var="activeuser"> 
								<option value="${activeuser.userId}">${activeuser.firstName}&nbsp;${activeuser.lastName}</option>
							</c:forEach>
					   	 </select>
					   	 </span>
					</div>
				</div>
				<div class="row">
					<div class="span10" >
						<label class="checkbox inline">
					     <input type="checkbox" id="demostatus">&nbsp;<spring:message code="lblDemosWasCom"/>
					     </label>	
					</div>
				</div>
		      </div>  <!-- /.end body -->
	      <div class="modal-footer">
		      <div class="pull-left">
		      		<button type="button" class="btn btn-primary" ><spring:message code="btnCancelEvent"/></button>
		      </div>
	     	 <div  class="pull-right">
	       		 <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="btnClr"/></button>
	        	<button type="button" class="btn btn-primary" onclick="saveDemoSchedule();"><spring:message code="btnDone"/></button>
	          </div>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
	<!--==================================================================================================== -->                 