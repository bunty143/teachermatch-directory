
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/interface/UserUploadTempAjax.js?ver=${resourceMap['UserUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/JobUploadTempAjax.js?ver=${resourceMap['JobUploadTempAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type="text/javascript" src="js/userupload.js?ver=${resourceMap['js/userupload.js']}"></script>

	<div class="row" style="margin-left: 0px;margin-right: 0px;margin-top:">
         <div style="float: left;">
         	<img src="images/icon1.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;">Import User Details</div>	
         </div>         		
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
     </div>

<div class="row top20">
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-5 col-md-5 importborder" style="margin-left: 15px;margin-right: 15px;">
		<iframe id='uploadFrameID' name='uploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='userUploadServlet' enctype='multipart/form-data' method='post' target='uploadFrame' action='userUploadServlet.do' class="form-inline">
		<table cellpadding="5" align="center" >
		<tr><td colspan="2" style="padding-top:10px;">&nbsp;
			 <div class="control-group">			                         
			 	<div class='divErrorMsg' id='errordiv' ></div>
			</div>
			</td>
		</tr>
		
		  <c:if test="${entityType ne 1}">
		  
		  <c:set var="showOrHide" scope="session" value="none"/>
		  
		  </c:if>
		  <c:if test="${entityType eq null || entityType eq '' || entityType eq 1}">
		     <c:set var="showOrHide" scope="session" value="fixed"/>
		   </c:if>
		  <tr style='display:${showOrHide};'>
			<td colspan="2"><label>District Name<span class="required">*</span></label></td>
			</tr>
			 <tr style='display:${showOrHide};'><td colspan="2">
				 <input type="text" id="districtName" name="districtName" class="help-inline form-control"
				    		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
				             onkeyup="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
				             onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"/>
				 <input type="hidden" id="districtId" value="${districtId}" />
				 	
				 <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
			 </td>
		  </tr>	
		
		<tr><td ><label>User Details<span class="required">*</span></label></td><td><input name="userfile" id="userfile" type="file"></td></tr>	
		<tr><td><button class="btn btn-primary fl" type="button" onclick="return validateUserFile();" style="width: 100px;">Import <i class="icon"></i></button></td></tr>
		</table>
		</form>
	</div>
</div>
<div class="row">	
	<div class="col-sm-3 col-md-3"></div>
	<div class="col-sm-8 col-md-8 mt10" style="margin-left: 5px;margin-right: 5px;">
			<table>
			<tr><td></td><td class="required">NOTE:</td></tr>
			<tr><td valign="top">*</td><td> Make sure that first row of excel file (xls/xlsx) has the column names and subsequent rows has data.</td></tr>
			<tr><td valign="top">*</td><td>Excel file must have following column names -</td></tr>
			
			<tr><td></td><td>user_first_name</td></tr>
			
			<tr><td></td><td>user_last_name</td></tr>
			
			<tr><td></td><td>user_email</td></tr>
			
			<tr><td></td><td>role ( Admin,DA,SA,TM Analyst,District Analyst,School Analyst )</td></tr>
			
			<tr><td valign="top">*</td><td> Column "location" is optional. If it is SA then location is required.</td></tr>
			
			<tr><td valign="top">*</td><td>Column names can be in any order and must match exactly as above.</td></tr>
			<tr><td valign="top">*</td><td>Column names are case insensitive.</td></tr>
		</table>
	</div>
</div>
<div style="display:none;" id="loadingDiv">
	<table  align="center" >
		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div> 
