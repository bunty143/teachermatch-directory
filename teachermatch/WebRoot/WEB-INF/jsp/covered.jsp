<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<style type="text/css">
.a1 {
	position: relative;
	display: inline-block;
}
.a1 > i {
    max-width: 302px;
    z-index: 1030;
	font-style:normal;
	line-height: 1.4;
	color: black;
   	background: white;
	box-shadow: 0 3px 7px rgba(0,0,0,0.3);
	border: 1px solid #D4D6D6;
	text-align:left;
	position: absolute;
	width: 302px;
	height:130px;
	margin-left:-200px;
	padding: 10px 10px 10px 10px;
	bottom: 60%;
	margin-bottom: 22px;
	visibility:hidden;
	opacity:1;
	-webkit-transition: opacity 0.5s linear;
	-moz-transition: opacity 0.5s linear;
	-ms-transition: opacity 0.5s linear;
	-o-transition: opacity 0.5s linear;
	transition: opacity 0.5s linear;
}

.a1 > i:before, .a1 > i:after {
	content: "";
	position: absolute;
	border-left: 20px solid transparent;
	border-right: 20px solid transparent;
	top: 95%;
	left: 45%;
}
.a1 > i:before {
	border-top: 21px solid rgba(0, 0, 0, 0.2);
	margin-top: 7px;
}
.a1 > i:after{
	border-top: 20px solid #fff;
	margin-top: 5px;
}
.a1:hover > i {
	visibility: visible;
	opacity: 1;
}
</style>
<!--<style type="text/css">
.col-sm-2:hover {    
    cursor: pointer;
    -moz-transform:scale(1.1);
    -webkit-transform:scale(1.1);
    -o-transform:scale(1.1);
    transform:scale(1.1);
}
</style>

--><div class="container">
<center style="margin-left: 50px;margin-right: 50px;margin-top: 10px;">
<div style="font-size: 13.5px;color: black;">
<b><spring:message code="msgCovered1" /></b>

</div>
<div style="line-height: 1.4;margin-top: 10px;">
	<spring:message code="msgCovered12_1" /><br> <spring:message code="msgCovered12_2" /><br><spring:message code="msgCovered12_3" />
</div>
<div class="row" style="margin-top: 30px;">
	<div class="col-sm-4 col-md-4">
		<img src="images/covered1.png"/>	
	<div style="color: black;margin-top: 5px;"><span class="headingBlue"><b><spring:message code="msgAcademicJobSearch" /></b></span>
	<br><spring:message code="msgCovered3" /><br> <spring:message code="msgCovered4" /> </div>	
	</div>
	<div class="col-sm-4 col-md-4">
		<img src="images/covered.png"/>
		<div style="color: black;margin-top: 5px;"><span class="headingBlue"><b><spring:message code="msgOnTheMarketStrategies" /></b></span>
		
	<br> <spring:message code="msgCovered16" /> </div>
	</div>
	
	<div class="col-sm-4 col-md-4">
	<br><br><br>
		<img src="images/covered2.png" class="top60"/>
		<div style="color: black;margin-top: 20px;"><span class="headingBlue"><b><br><br><spring:message code="msgCovered5" /> <br><spring:message code="msgCovered6" /></b></span>
	<br><spring:message code="msgCovered7_1" />,<br><spring:message code="msgCovered7_2" /></div>		
	</div>
</div>

<div style="font-size: 13.5px;margin-top: 40px;color: black;">
<b><spring:message code="msgCovered8" /></b>
</div>
<div style="margin-top: 10px;">
	<spring:message code="msgCovered9" />
	</div>
</center>
</div>

<div align="center" style="height: auto; margin-top: 40px;width:100%;position: relative; ">
<div  style="width:100%;">
	<div class="col-sm-2 col-md-2 top60 left10" style="background-color:#E1F4FB;height: 200px;margin-top:15px;">
	<a href="#" class="a1"   ><spring:message code="msgCovered10" /></a><br>	<br>	<br>
		<img src="images/QuestConnect1.png" style="width: 90%;"/><br><br/>	
			<b><p style="margin-top: 6px;"><spring:message code="msgCovered11" /></p></b>
	</div>

	<div class="col-sm-2 col-md-2 top60 left10" style="background-color:#E7EBEA;height: 200px;margin-top:15px;">
	<a href="#" class="a1" ><spring:message code="msgCovered12" /></a>	<br>	<br>
		<img src="images/QuestAcademy1.png" style="width: 90%"/>		
		<b><spring:message code="lblRefAd" /><BR/>
		<spring:message code="lblLearnCentr" /></b>
	</div>
	<div class="col-sm-2 col-md-3 top60 left10" style="background-color: #E1F4FB;height: 240px;">
	<a href="#" class="a1" ><spring:message code="msgCovered13" /></a>	<br>	<br>
		 <img src="images/MeetYourMentor1.png" style="width: 95%"/>	<br>	<br>
		<b><spring:message code="msgCovered14" /></b>
	</div>

	<div class="col-sm-2 col-md-2 top60 left10" style="background-color: #E7EBEA;height: 200px;margin-top:15px;">
		<a href="#" class="a1" ><spring:message code="msgCovered15" /></a><br>	<br>
		<img src="images/ToolKitEssentials1.png" style="width: 90%"/><BR/>	<br>
		<b><spring:message code="lblSmtTp" /><BR/>
		<spring:message code="lblToolsAdAids" /></b>
	</div>

	<div class="col-sm-2 col-md-2 top60 left10" style="background-color: #E1F4FB;height: 200px;margin-top:15px;">
   <a href="#" class="a1" ><b style="margin-left: 20px;"><spring:message code="lblInTheQ" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><i>
   <spring:message code="msgInformedDecisionsAcrossEveryAspect" />
   </i></a><br>	<br>
		<img src="images/IntheQ1.png" style="width: 90%"/>
	<b><p style="margin-top: 7px;"><spring:message code="lblLifeStl" /> <BR/>
		<spring:message code="lblPerspect" /></p></b>
	</div>
</div>
</div>
<!-- 
<script type="text/javascript">
$('#questconnect').tooltip();
$('#questacademy').tooltip();
$('#mentor').tooltip();
$('#totalessentials').tooltip();
$('#intheq').tooltip();
</script> -->
