<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@page import="tm.bean.user.UserMaster"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type='text/javascript' src="js/userhome.js?ver=${resourceMap['js/userhome.js']}"></script>
<script type="text/javascript" src="dwr/interface/UserHomeAjax.js?ver=${resourceMap['UserHomeAjax.ajax']}"></script>
<style type="text/css"><!--
#thin-donut-chart {
    width: 300px;
    height: inherit;   
    margin:auto;     
}
.center-label, .bottom-label {
    text-anchor: middle;
    alignment-baseline: middle;
    pointer-events: none;
    font-size: 60px;
    font-family: 'Century Gothic','Open Sans', sans-serif;
    font-weight: bold;
    color:#fff;
   background-color: #fff;
}
.half-donut .center-label {
    alignment-baseline: initial;
}
.toolTip {
    position: absolute;
    display: none;
    width: auto;
    height: auto;
    background: rgba(0,0,0,0.6);
    border: 0 none;    
    color: white;
    font: 15px sans-serif;
    padding-left:5px;
    padding-right:5px;
    padding-top:5px;
    padding-bottom:5px;
    text-align: center;    
    margin:auto;
  /*background-color:rgb(255,255,255); opacity:0.85;color:#007AB4;*/
  	border-radius: 5px 5px 5px 5px;
  	box-shadow: -3px 3px 15px #888888;
    border:1px solid;
    border-color: rgba(0, 0, 0, 0.8);  
	
}

  --></style> 
<center><br/><br/><b style="font-family: 'Century Gothic','Open Sans', sans-serif;font-size: 20px;"><spring:message code="lblJobPosted"/></b>
<div id="thin-donut-chart">
	<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
	</div>
</div>
</center>

    <br><br>
<script type="text/javascript" src="js/d3.js?ver=${resourceMap['js/d3.js']}"></script>
 <script>
 getQuestUserJob(${userMaster.districtId.districtId});
 </script>
 



