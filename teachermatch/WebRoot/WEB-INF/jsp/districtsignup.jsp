<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="js/quest.js?ver=${resourceMap['js/quest.js']}"></script>
<script type="text/javascript" src="dwr/interface/QuestAjax.js?ver=${resourceMap['QuestAjax.ajax']}"></script>	
<script type="text/javascript" src="js/tmhome.js?ver=${resourceMap['js/tmhome.js']}"></script>	
<link href="css/tmcommon.css?ver=${resourceMap['css/tmcommon.css']}"  rel="stylesheet" type="text/css">
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript'>
    var captchaContainer = null;
    var loadCaptcha = function() {
      captchaContainer = grecaptcha.render('captcha_container', {
        'sitekey' : '6LeMmwATAAAAAGB1iwuORAfQ8z9ZvJAT2Lhad4AT',
        'callback' : function(response) {
          if(response !=null)
          {
          	document.getElementById('captcharesponse').value="success";
          }
          else
          {
          	document.getElementById('captcharesponse').value="failed";
          }
        }
      });
    };
    
    </script>
<script type="text/javascript">
$(document).ready(function(){
$('.popover').hide();
$( "#signupPassword" ).focus(function() {
  $('.popover').show();  
});
$('#signupPassword').focusout(function(){
	var isHoveredOnSeemore = $('#seemore').is(":hover"); // returns true or false If seemore has hover
	var isHoveredOnSeeless = $('#seeless').is(":hover"); // returns true or false If seeless has hover
	
	if( !(isHoveredOnSeemore==true || isHoveredOnSeeless==true) )
		$('.popover').hide();
});
$( "#seemore" ).click(function() {
	$("#signupPassword").focus();
	$('.password-guidelines').show();
	$('#seemore').hide();  
  	$('#seeless').show();    
});
$( "#seeless" ).click(function() {
	$("#signupPassword").focus();
  $('.password-guidelines').hide();
  $('#seemore').show();
  $('#seeless').hide();    
});
  });
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-56937648-1', 'auto');
  ga('send', 'pageview');

</script>

<!-- Start of Async HubSpot Analytics Code -->
  <script type="text/javascript">
    (function(d,s,i,r) {
      if (d.getElementById(i)){return;}
      var n=d.createElement(s),e=d.getElementsByTagName(s)[0];
      n.id=i;n.src='//js.hs-analytics.net/analytics/'+(Math.ceil(new Date()/r)*r)+'/290901.js';
      e.parentNode.insertBefore(n, e);
    })(document,"script","hs-analytics",300000);
  </script>
<!-- End of Async HubSpot Analytics Code -->

<style type="text/css">
.popover-inner {
padding: 3px;
overflow: hidden;
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0,0,0,0.3);
color:black;
}

.popover.left .arrow
{
	width: 20px;
	height: 20px;
	border: none;
	transform: rotate(45deg);
	position: absolute;
	top: 100px;
	left: 262px;
	background-color: white;
	box-shadow: 0 9px 0 0px white, -9px 0 0 0px white, 1px -1px 1px #818181;
}

.popover .arrow {
position: absolute;
width: 0;
height: 0;
}
element.style {
top: 231.5px;
left: 155px;
display: block;
}
.popover.left {
margin-left: -5px;
}
.fade.in {
opacity: 1;
}
.popover {
position: absolute;
top: 0;
left: 0;
z-index: 1010;
display: none;
}
.fade {
opacity: 0;
-webkit-transition: opacity 0.15s linear;
-moz-transition: opacity 0.15s linear;
-ms-transition: opacity 0.15s linear;
-o-transition: opacity 0.15s linear;
transition: opacity 0.15s linear;
}
</style>

<iframe id='useruploadFrameID' name='signupuploadFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	        </iframe>
   <div class="row top15">
   							    <div class="col-sm-3 left250"> 
   							       <div class="subheadinglogin "><spring:message code="btnSign"/></div>
   							   </div> 
   							
   							    <div class="col-sm-4 left-sm70"> 
   							       <spring:message code="msgAlreadyReg"/> <a href="employer-sign-in.do"><spring:message code="lnkLogin"/></a>
   							   </div> 
   							</div>
   					 <form id='districtsignupform' method='post'  class="form-inline">
						 <div class="row col-md-offset-3">
   								<div class="col-sm-8 col-md-8">
                                      <div  id='signUpServerError' class='divErrorMsg' style="display: block;"></div>
						      		  <div  id='signUpErrordiv' class='divErrorMsg' style="display: block;"></div>						      				
								</div>
						</div>
						
					
					<div  id="districtForUser">		
					<div class="row col-md-offset-3 top15">   
					<div class="col-sm-8">		
					<div class="font20" style="color: black;display: block;"><spring:message code="lblDistrict"/><span class="required">*</span><div class="pull-right"><a href="javascript:void(0)" onclick="return showforotherDistrict()" > <spring:message code="lnkMyDistIsNotHere"/></a></div></div>
	           		<input type="text" id="districtName" name="districtName"  type="text"  class="form-control" 
	           		onfocus="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onkeyup="getDistrictMasterAutoComp(this, event, 'divTxtShowData', 'districtName','districtId','');"
					onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
					<div id='divTxtShowData' style=' display:none;position:absolute;z-index:5000;'
					 onmouseover="mouseOverChk('divTxtShowData','districtName')" class='result' ></div>
					<input type="hidden" id="districtId"/>
					<input type="hidden" id="other"/>					
					<input type="text" id="otherdistrictName" class="form-control" style="display: none;">					
					</div>
					</div> 			
					</div>	
						
					   <div class="row col-md-offset-3 top15">                                  
                                   <div class="col-sm-4">
                                        <label><spring:message code="lblFnaem"/><span class="required">*</span></label>
                                       <input type="text" id="fname" class="form-control" maxlength="50"/>
                                   </div>
                                   
                                   <div class="col-sm-4">
                                        <label><spring:message code="lblLname"/><span class="required">*</span></label>
                                         <input type="text" id="lname" class="form-control" maxlength="50"/>
                                   </div>
                       </div>                                   
                      <div class="row col-md-offset-3 top-sm0">             
                                   <div class="col-sm-8 col-md-8">
                                        <label><spring:message code="lblEmail"/><span class="required">*</span></label>
                                      <input type="text" id="signupEmail"  class="form-control" maxlength="75" />
                                   </div>
                      </div>                                   
                      <div class="row col-md-offset-3 top-sm0">                
                                   <div class="col-sm-8 col-md-8">
                                        <label><spring:message code="lblPass"/><span class="required">*</span></label>
                                         <input type="password" id="signupPassword"  class="form-control" maxlength="12"	onkeyup="return PasswordValue()"/></div>
                      </div>
                                   
                         <div class="row col-md-offset-3">
                         
                         <!-- Start of password div -->
                      	<div  class="col-sm-8 col-md-8 top10" style="position: relative;z-index: 10">
                      		<div class="popover fade left in" style="left: -275px; top: -123.5px; display: block;">
                      			<div class="arrow"></div>
                      			<div class="popover-inner">
                      				<div class="popover-title" style="background-color: #007AB4;color:white;"><spring:message code="msgPassReq"/></div>
                      				<div class="popover-content"></div>
                      				<div id="password-rules">
                            			<ol >
                            				<li><spring:message code="liAtLeast6Char"/></li>
                            				<li><spring:message code="liIncludeAtLeast2-3Elt"/>
                            					<ul type="disc">
	                            					<li><spring:message code="liOneCha"/></li>
													<li><spring:message code="liOneDigit"/></li>
													<li><spring:message code="liOneSplSymbol"/></li>
												</ul>
											</li>
											<li><spring:message code="liAtMost12Char"/></li>
										</ol>
									</div>
									
									<div>
									<a href="javascript:void(0);" role="see-more" id="seemore" style="padding-left: 12px;"><spring:message code="lnkMoreInfo"/></a>
									<a href="javascript:void(0);" role="see-more" id="seeless" style="color: red; padding-left: 12px; display: none;"><spring:message code="lnkLessInfo"/></a>
									</div>
									
									<div class="password-guidelines hide" style="padding: 12px 15px; text-align: justify;">
										<span><b><spring:message code="msgPassQuid"/></b></span>
										<ul>
											<li><spring:message code="liEachTimeMakeHardPass"/></li>
											<li><spring:message code="liUseRepeatingOrSequentalCharInPass"/></li>
										</ul>
									</div>
								</div>
							</div>
						</div>

						
                        <!-- End of password div -->
                        <div class="col-sm-8 col-md-8 top10">
                                    <label>
										<strong><spring:message code="lblpassStrength"/>
										</strong>
									</label>									
                                   </div>
                                   
                                   <div class="col-sm-8 col-md-8">
                                   <table cellpadding="0" cellspacing="0" border="0">
									<tr>
										<td colspan="2" align="left" valign="top" nowrap>
											<table cellpadding="0" cellspacing="0"><tr>
												<td>
													<div style="height: 12px; width: 14px;  float: left;  border: thin solid #bcb4a4;">							
														<div id="d4" style="height: 12px; width: 14px; float: left; display: none;">
															&nbsp;
														</div>					
													</div>
												</td>
												<td style="padding-left:10px;">
													<div style="height: 12px; width: 14px;   float: left; border: thin solid #bcb4a4; margin-left: 5%;">
														<div id="d5" style="height: 12px; width: 14px; float: left; display: none;">
															&nbsp;
														</div>
													</div>
												</td>
												<td style="padding-left:10px;">
												<div style="height: 12px; width: 14px; float: left; border:  thin solid #bcb4a4; margin-left: 5%;">
													<div id="d6"					style="height: 12px; width: 14px; float: left; background-color: lightgreen; display: none;">
														&nbsp;
													</div>
												</div>
												</td>
												<td style="padding-left:10px;">
												<div
													style="height: 12px; width: 14px; float: left; border:  thin solid #bcb4a4; margin-left: 5%;">
													<div id="d7" 
														style="height: 12px; width: 14px; float: left; background-color: lightgreen; display: none;">
														&nbsp;
													</div>
												</div>
												
												</td>
												<td><table cellpadding="0" cellspacing="0"><tr><td>
													<div id="d8"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span ><spring:message code="spWeak"/></span>
													</div></td>
												<td>
													<div id="d9"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span ><spring:message code="spNormal"/></span>
													</div></td>
												<td>
													<div id="d10"
														style="padding-left: 10px; height: 10px; width: 14px; float: left; display: none;">
														<span ><spring:message code="spStrong"/></span>
													</div>
											</td>
										
											</tr> </table>
										</td>
									
										</tr></table>
										</td>
									    </tr>
								      </table>
							        </div>
							        	
							       
							        <div class="col-sm-8 col-md-8 top10">
							        <label ><strong><spring:message code="lblPlzStChBox"/></strong><span class="required">*</span></label>
							        </div>
					  </div>         
					  <div class="row col-md-offset-3 top5">        
							         <div class="col-sm-6 col-md-6 top-s110">
							        <div id="captcha_container"></div>							         							        
							        </div>							        
				      </div>   
                            
                      <div class="row col-md-offset-3">	        
							        <div class="col-sm-8 col-md-8 top20" >
							          <button class="btn btn-primary" type="button" onclick="return signUpUser(1);"><spring:message code="btnSign"/> <i class="icon"></i></button>
							        </div>
							        
							        <div class="col-sm-11 col-md-12 top11">
							          <spring:message code="msgClickSignUpUAgreeTm"/> <a href="termsofuse.do"><spring:message code="lnkTermsUse"/></a>
							        </div>
                              
                      </div>
				      </form>  
<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>  

<br/><br/>
<input type="hidden" name="captcharesponse" id="captcharesponse"/>
<div style="display:none;" id="loadingDiv">
    <table  align="center">
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
	</table>
</div>
<div class="modal hide"  id="messageModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	  <div class="modal-content">
	   <div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="return hideDiv()">x</button>
			<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
		</div>
		<div class="modal-body">				
				<p><spring:message code="msgSuccessfullyRegdWithTm"/></p>			
		 	</div>
	 	<div class="modal-footer">
	       <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="return hideDiv()"><spring:message code="btnOk"/></button>
	 	</div>
     </div>
	</div>
</div>
<div style="display:none;" id="loadingDiv">
    <table  align="left" >
 		<tr><td style="padding-top:270px;padding-left:450px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;padding-left:450px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		
	</table>
</div>
<script>
	document.getElementById("districtName").focus();	
</script>
