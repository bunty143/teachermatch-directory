<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">

<script type="text/javascript">

var ids=window.location.hash;
if(ids=="#review")
{
$(function () {
    $('#myTab a:last').tab('show')
   		 $("#id1").hide();    	
		 $("#id11").show();
		 $("#id2").hide();
		 $("#id22").show();
		 $("#id3").show();
		 $("#id33").hide();
  })  		
}

if(ids=="#whatsnew")
{ 
  $(function () {
    $('#myTab a[href="#whatsnew"]').tab('show')
   		 $("#id1").hide();    	
		 $("#id11").show();
		 $("#id2").show();
		 $("#id22").hide();
		 $("#id3").hide();
		 $("#id33").show();
  })  		
}

function ChangeFun(id)
{
	document.getElementById("askrobynmore1").style.display='inline';
	document.getElementById("askrobynmore11").style.display='none';
	document.getElementById("askrobynmore2").style.display='inline';
	document.getElementById("askrobynmore21").style.display='none';
	if(id==1)
	{
		document.getElementById("id1").style.display='inline';
		document.getElementById("id11").style.display='none';
		document.getElementById("id2").style.display='none';
		document.getElementById("id22").style.display='inline';
		document.getElementById("id3").style.display='none';
		document.getElementById("id33").style.display='inline';
	}
	if(id==2)
	{
		document.getElementById("id1").style.display='none';
		document.getElementById("id11").style.display='inline';
		document.getElementById("id2").style.display='inline';
		document.getElementById("id22").style.display='none';
		document.getElementById("id3").style.display='none';
		document.getElementById("id33").style.display='inline';
	}
	if(id==3)
	{	
		document.getElementById("id1").style.display='none';
		document.getElementById("id11").style.display='inline';
		document.getElementById("id2").style.display='none';
		document.getElementById("id22").style.display='inline';
		document.getElementById("id3").style.display='inline';
		document.getElementById("id33").style.display='none';
	}
}
function ChangeMore(id)
{
	document.getElementById(id).style.display='none';
	document.getElementById(id+1).style.display='inline';	
		
}

</script>
<style type="text/css">

.tabwidth
{
width:80px;
height: 35px;
font-size: 9px;
padding: 5px;
vertical-align: top;
font-weight: bold;
}
.nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus
{
color: #555555;
background-color: #FFFFFF;
border: 1px solid #dddddd;
border-bottom-color: transparent;
cursor: default;
}
.nav-tabs > li > a:hover
{
background-color: #FFFFFF;
border: 1px solid #FFFFFF;
border-bottom-color: #dddddd;
}
.active, .active:hover {
font-size: 15px;
border-radius: 4px 0 0 4px;
color: #007AB4!important;
height: 32px;
margin: 0px 0px 0 0px!important;
text-decoration: none;
padding: 0px 0px!important;
}
.imgs
{
padding:5px 2px 2px 2px;

height: 45px;
width: 45px;
}
</style>

<center>
<div class="row">
<div class="col-sm-8 col-md-8">

<div class="row">
<div class="col-sm-12 col-md-12">
<div style=" margin-bottom: 40px;">
<img src="images/mentorLogo.png" width="20%"/><br/>
	<span style="font-size: 22px; color:#007AB5">Meet Your Mentor</span>	
</div>
</div>
</div>


<div role="tabpanel">

  <!-- Nav tabs -->
  <ul id="myTab" class="nav nav-tabs" role="tablist">
    <li class="active"><a onclick="ChangeFun(1);" href="#askrobyn" aria-controls="askrobyn" role="tab" data-toggle="tab" class="tabwidth"><img display="none" id="id1" src="images/word bubblesA.png" align="left" height="25px" width="30px" /> <img id="id11" style="display: none;" src="images/word bubbles.png" align="left" height="25px" width="30px" /> Ask Your Mentor</a></li>
    <li ><a onclick="ChangeFun(2);" href="#whatsnew" aria-controls="whatsnew" data-toggle="tab" class="tabwidth"><img id="id2" src="images/vector smartA.png" align="left" height="20px" width="22px" style="display: none;" /> <img  id="id22" src="images/vector smart.png" align="left" height="20px" width="22px" />What's New ?</a></li>
    <li ><a onclick="ChangeFun(3);" href="#robynsreview" aria-controls="robynsreview"  data-toggle="tab" class="tabwidth"><img id="id3" src="images/vector smarthandA.png" align="left" height="20px" width="22px" style="display: none;" /> <img  id="id33" src="images/vector smarthand.png" align="left" height="20px" width="22px" /> Mentor Review</a></li>   
  </ul>

  <!-- Tab panes -->
  <div class="tab-content" style="margin-top: 30px;">
    <div role="tabpanel" class="tab-pane active" id="askrobyn">
      <div class="row" style="margin-bottom: 15px;">
    	<div class="col-sm-1 col-md-1" style=" padding-top: 10px;">
			<img src="images/questresources.png"  width="40px" height="40px"/>
		</div>
		<div class="col-sm-11 col-md-11" style="padding-top: 10px;  text-align: left; padding-left:40px;">
			<span style="font-size: 11px; color:gray;">Posted January 2015</span> <br/>
			<span style="font-size: 13px; font-weight: bold;">	Which states can I teach in after I graduate?</span> 
		</div>
	  </div>
	  
	  <div class="row">
    	<div class="col-sm-1 col-md-1">
			<img src="images/tquestmentor.png" />
		</div>
		<div class="col-sm-11 col-md-11" style="text-align: left; padding-left:40px;">
			<div style=" height: 74px;  background-color:#8EBAE4; padding: 8px; margin-top:5px; float: left; margin-right:5px;  "><img src="images/usa map.png" align="left" /></div>
			<p style="font-size: 12.5px; color:black; ">After you graduate, you can teach in the state in which you
			hold your new teaching certification. However, if you want to
			teach in another state, you would have to research the
			requirements, as they are all different. All states require a
			bachelor’s degree, a form of teaching certification (traditional
			or alternative) along with a background check.</p>
			<a href="javascript:void(0);" style="font-size: 12px; font-weight: bold;" class="font11" id="askrobynmore1" onclick="ChangeMore('askrobynmore1');">More</a>	
			
			<p style="font-size: 12.5px; color:black; display: none; " id="askrobynmore11">
			You can also look into programs such as National Board Certification and
			American Board for Certification of Teaching Excellence. There is a lot of
			research to do if you want to teach in a different state outside of your native
			certification. I would suggest beginning this research as soon as you think
			you might want to make a change</p>		
		</div>
	  </div>
	  
	    <div class="row" style="margin-bottom: 15px;">
    	<div class="col-sm-1 col-md-1" style=" padding-top: 10px;">
			<img src="images/questresources.png"  width="40px" height="40px"/>
		</div>
		<div class="col-sm-11 col-md-11" style="padding-top: 10px;  text-align: left; padding-left:40px;">
			<span style="font-size: 11px; color:gray;">Posted January 2015</span> <br/>
			<span style="font-size: 13px; font-weight: bold;">Is it okay to ask about extra duties I might have during the interview?</span> 
		</div>
	  </div>
	  
	  <div class="row">
    	<div class="col-sm-1 col-md-1">
			<img src="images/tquestmentor.png" />
		</div>
		<div class="col-sm-11 col-md-11" style="text-align: left; padding-left:40px;">
			<div style=" height: 74px;     float: right; margin-left: 5px;  "><img src="images/askrobynimg2.png" align="left" /></div>
			<p style="font-size: 12.5px; color:black; ">
			Absolutely – don’t hesitate to ask. In all likelihood your
			pro-activeness will be appreciated. You want to make
			sure that this role is everything you want, just as much
			as the school wants to make sure you are a good fit. It
			is important that you are aware of any/all extra requirements during and
			outside of the typical school day.</p>
			<a href="javascript:void(0);" style="font-size: 12px; font-weight: bold;" class="font11"  id="askrobynmore2" onclick="ChangeMore('askrobynmore2');">More</a>	
				<p style="font-size: 12.5px; color:black; display:none; " id="askrobynmore21">
				However, please keep in mind, it is not unusual for administrators or principals
				to ask a teacher to take on extra activities during the school year,
				since new needs arise such as clubs, activities and programs. Changes
				always occur during the course of the school year, so always be prepared
				for the unexpected. For example, a teacher may have to take a leave of
				absence or an influx of students might arrive – which means your role could
				change within the school, as well. As a teacher, flexibility is very important;
				and is something to keep in mind when you evaluate any offers</p>
		</div>
	  </div>
	</div>
    <div role="tabpanel" class="tab-pane" id="whatsnew">
	     <div class="row">
    	<div class="col-sm-1 col-md-1">			
		</div>
		<div class="col-sm-11 col-md-11" style=" text-align: left; border-bottom:2px solid #bcbcbc;; margin-left:10px; padding-bottom:10px; margin-bottom:10px; padding-left:40px;">
		<p style="font-size: 13px; font-weight: bold; margin-bottom: 0px; ">Graduating Mid-Year: Landing a Job is Challenging, Yet Not Impossible</p> 
		<p style="font-size: 11px; color:gray; margin-bottom: 0px;">January 2015</p> <br/>
			
		<div  style="text-align: left; ">
		<div><img src="images/whatsnewimg1.png" align="right" width="191px" height="141px" style="margin-left: 5px;" /></div>
		<p style="font-size: 12.5px; color:black; margin-top:0px; ">
				Landing a teaching job in the middle of the school year
				may appear or sound next to impossible. However, even
				though this time of the year does come with a special
				scope of unique challenges, there are potential opportunities.
				If you understand the reasons why this time can
				be advantageous and take action, odds are you may
				just end up in a classroom much sooner than you
				anticipated.</p>
			<a href="review.do#whatsnew" style="font-size: 12px; font-weight: bold;">See Full Article</a>
		
		</div>
		</div>
	  </div>
	  
	   <div class="row">
    	<div class="col-sm-1 col-md-1">			
		</div>
		<div class="col-sm-11 col-md-11" style=" text-align: left; border-bottom:2px solid #bcbcbc;; margin-left:10px; padding-bottom:10px; margin-bottom:10px; padding-left:40px;">
		<p style="font-size: 13px; font-weight: bold; margin-bottom: 0px;">Clean It Up: Your Digital Footprint Has a Critical Impact on Your Career</p> 
		<p style="font-size: 11px; color:gray; margin-bottom: 0px;">January 2015</p> <br/>
			
		<div  style="text-align: left;">
		<div><img src="images/whatsnewimg2.png" align="right" style="margin-left: 5px;" /></div>
		<p style="font-size: 12.5px; color:Black; margin-top:0px; ">
		<b style="color:#666669;">"If you aren't controlling your footprint, others are."
			Meredith Stuart, History Teacher & Department Chair,
			Duke Law & Divinity School Grad.</b><br/>
			
			The millennial generation, having grown up online as
			digital natives, lives in a unique comfort zone in the
			virtual world. However, if you are living in this comfort
			zone and are looking for employment, it is time for a reality check, a digital clean up,
			and an online plan for the future. This holds especially true for teachers, who are held
			up to higher moral obligations and different requirements, as well as societal expectations
			and limitations on free speech. Our future generation is depending on you, as
			you are a role model for society today. It is your responsibility to uphold a positive and
			professional online presence. Potential employers, students, families, the community
			and the world have access to who you are as a digital person, and that has a lasting
			impact.</p>
			<!--<a href="#" style="font-size: 12px; font-weight: bold;">See Full Article</a>
		
		--></div>
		</div>
	  </div>	  	  
	  
    </div>
    <div role="tabpanel" class="tab-pane" id="robynsreview"  >
     <div class="row" style=" text-align: left; border-bottom:2px solid #bcbcbc; padding-bottom:10px; margin-bottom:10px;">	
    <div class="col-sm-1 col-md-1">
	</div>
	<div class="col-sm-3 col-md-3">
	<div><img src="images/robynreview.png" align="right" /></div>
	</div>
	<div class="col-sm-8 col-md-8" >
		<p style="font-size: 11px; color:gray; margin-bottom: 0px;">Published January 2015</p>
		<p style="font-size: 13px; font-weight: bold; margin-bottom: 0px; ">You’re Hired: Teacher’s Edition</p> 	
			
		<div  style="text-align: left;">
		<p style="font-size: 12.5px; color:black; margin-top:0px; ">
		<i>An Insider's Guide to Show You How To Nail Your Interview
				and Get Hired Now, By Pete H., Amazon Digital Services (Kindle
				Edition), 2014. 24 Pages</i><br/>
				<i><b>You're Hired: Teacher's Edition:</b> An Insider's Guide to Show
				You How To Nail Your Interview and Get Hired Now</i> is a popular
				read for anyone who’s currently interviewing for a
				teaching position, as evidenced by its #1 p
				osition in its category on Kindle’s Bestsellers, as well as the
				numerous thumbs-up online reviews from educators</p>
			<a href="review.do#review" style="font-size: 12px; font-weight: bold;">See Full Review</a>
		
		</div>
		</div>
		</div>
    </div>    
  </div>

</div>
</div>
<div class="col-sm-4 col-md-4" style="background-color: #ECECEC; padding:10px;">
<div style="background-color: #BCBCBC; padding:10px; font-size: 14px; font-weight: bold; width:288px;">What’s happening <img src="images/twitter bird.png"/></div>
<!--<a width="290px" Height="400px" class="twitter-timeline"  href="https://twitter.com/teachermatch" data-widget-id="545623027883327488">Tweets by @Teachermatch</a>
 <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
-->
		 <a width="290px"  class="twitter-timeline" data-dnt="true" href="https://twitter.com/TeacherMatch" data-widget-id="575657425203781632">Tweets by @TeacherMatch</a>
		 <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
			
			<div style=" padding-bottom:10px; margin-top:40px;">
			<a href="https://www.facebook.com/QuestTeacherJobs" target="_blank"><img src="images/squest/f.png" class="imgs" style="padding-left:5px;"/></a>
			<a href="http://www.google.com/+TeachermatchOrganization" target="_blank"><img src="images/squest/g.png" class="imgs"/></a>
			<a href="https://www.linkedin.com/company/teachermatch" target="_blank"><img src="images/squest/l.png" class="imgs"/></a>
			<br/>
			<a href="https://www.youtube.com/channel/UCLi14NFiTU0bf3hTktuTtSw" target="_blank"><img src="images/squest/y.png" class="imgs"/></a>
			<a href="https://twitter.com/EdQuestJobs" target="_blank"><img src="images/squest/t.png" class="imgs"/></a>
			<a href="http://www.pinterest.com/EdQuestJobs/" target="_blank"><img src="images/squest/p.png" class="imgs"/></a>
			
			</div>
</div>

</div>
</center>
