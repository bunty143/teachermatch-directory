<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<link href="css/quest.css?ver=${resourceMap['css/quest.css']}" rel="stylesheet" type="text/css">
<style type="text/css"><!--
#circulardiv
{
border-radius: 50%;
text-align:center;
behavior: url(PIE.htc);
}
#circulardiv b
{
color: #007AB5;
}
.row b
{
font-size: 12px;
}
ul
{
    list-style-type: none;
}
.fontleft
{
text-align: left;
}
.fontleft li
{
margin-bottom:5px;
font-size:12px;
font-weight: bold;
}
.top20
{
margin-top:0px;
padding-top: 10px
}
.divwriter
{
font-size: 12px;
color:#6C6D6F;
padding-bottom: 10px;
padding-top: 10px;
font-weight: bold;
font-style: italic;
}

.divwriter span
{
font-size: 10px;
font-weight: none;
}
ul
{
display: block;
-webkit-margin-before: 1em;
-webkit-margin-after: 1em;
-webkit-padding-start: 0px;
}
h5
{
font-family: Century Gothic;
}
--></style>
<div >
<center>
<div class="container">
<h1 style="color: #007AB5; font-size: 24px; font-family: Century Gothic; ">In the <img src="images/questresources.png" height="60px" width="60px" /></h1>
<div  style=" font-size: 14px; margin-top:8px;">
<p>
It’s time to "Q" up the next chapter — browse our collection<br/>
of insightful articles filled with perspectives that can help you<br/>
set the course for a lifetime of success.</p></div>

<div role="tabpanel" class="row" style="width:100%; ">
<ul role="tablist" > 
 <li>
<div class="col-sm-3 col-md-3" style="padding-left: 0px;">
<div id="circulardiv">
<a href="#career"   data-toggle="tab">
<img src="images/Resources_Career.png"  height="150px" width="225px"/></a>
<br/>
<a href="#career"  data-toggle="tab"><b> <spring:message code="lnkCollegetoCareer"/></b></a>
</div>
</div>
</li>

 <li>
<div class="col-sm-3 col-md-3" style="padding-left: 0px;">
<div id="circulardiv">
<a href="#housing"   data-toggle="tab">
<img src="images/Resources_Housing.png"  height="150px" width="225px"/></a><br/>
<a href="#housing" data-toggle="tab"><b><spring:message code="lnkHousing"/></b></a>
</div>
</div>
</li>

<li>
<div class="col-sm-3 col-md-3" style="padding-left: 0px;">
<div id="circulardiv">
<a href="#finance"   data-toggle="tab">
<img src="images/Resources_Finance.png"  height="150px" width="225px"/></a><br/>
<a href="#finance"   data-toggle="tab"><b><spring:message code="lnkFinance"/></b></a>
</div>
</div>
</li>

<li>
<div class="col-sm-3 col-md-3" style="padding-left: 0px;">
<div id="circulardiv">
<a href="#wardrobe"   data-toggle="tab">
<img src="images/Resources_Wardrobe.png" height="150px" width="225px" /></a><br/>
<a href="#wardrobe"   data-toggle="tab"><b><spring:message code="lnkWardrobe"/></b></a>
</div>
</div>
</li>
</ul>

</div>
</div>
<!--By Default -->
<div  style=" background-color: #E9EDEE;  margin-top: 40px; min-height:300px; padding-bottom:20px; padding-top:20px; ">
<div class="container">
<div class="tab-content" >
<div  class="tab-pane active" style="text-align: right;">

<div class="col-sm-4 col-md-4" >
<div class="divwriter">"<spring:message code="msgDecortingHome_37"/>"<br/>
<span><spring:message code="msgDecortingHome_38"/></span> </div>


<div class="divwriter" style="padding-right: 60px;">
"Focus on building a base wardrobe <br/> of neutral colors and cuts..."<br/>
<span>By Aiden Summer, Demand Media</span></div>

<div class="divwriter" style="width:350px;">
<div style="text-align: left;"><spring:message code="msgDecortingHome_35"/></div>
<span style="padding-right: 60px; text-align: left;"><spring:message code="msgDecortingHome_36"/></span>
</div>
</div>

<div class="col-sm-4 col-md-4" >
<center  style="padding-top:10px; color:#6C6D6F; font-size: 14px;"> 
<img src="images/qicon.png" height="190px" width="200px" /><br/><br/>
<spring:message code="msgLifePers"/>
</center>

</div>

<div class="col-sm-4 col-md-4" >
<div class="divwriter" style="padding-right: 20px;">
<spring:message code="msgDecortingHome_1"/><br/>
<span><spring:message code="msgDecortingHome_2"/></span></div>

<div class="divwriter" style="padding-left: 40px;">
<spring:message code="msgDecortingHome_3"/><br/>
<span><spring:message code="msgDecortingHome_4"/></span> </div>

<div class="divwriter" style="padding-right: 30px;">
<spring:message code="msgDecortingHome_5"/><br/>
<span><spring:message code="msgDecortingHome_6"/></span></div>

</div>

</div>
<!--By Default -->

<!--Ist Tab -->

<div  class="tab-pane" id="career"  >
<div class="col-sm-1 col-md-1" style="text-align: right">
<img src="images/carrericon.png" width="40px" height="40px" /></div>
<div class="col-sm-5 col-md-5" style="color: black; text-align: left">
<h5> <spring:message code="lnkCollegetoCareer"/></h5>
<span style="font-size: 12px; color:#404142; line-height: 23px;">
<spring:message code="msgDecortingHome_7"/></span>

</div>

<div class="col-sm-1 col-md-1" ></div>
<div class="col-sm-5 col-md-5">
<ul class="fontleft">
<li><a href="http://www.huffingtonpost.com/2014/05/02/health-after-college_n_5227100.html" target="_blank"><spring:message code="msgDecortingHome_8"/></a></li>
<li><a href="http://www.levo.com/articles/career-advice/job-search-stress" target="_blank"><spring:message code="msgDecortingHome_9"/></a></li>
<li><a href="http://www.levo.com/articles/career-advice/5-invaluable-lessons-from-working-with-a-career-coach" target="_blank"><spring:message code="msgDecortingHome_10"/></a></li>
<li><a href="http://www.levo.com/articles/career-advice/advice-for-college-graduates" target="_blank"><spring:message code="msgDecortingHome_11"/></a></li>
<li><a href="http://laurenconrad.com/blog/2013/04/ask-lauren-i-m-graduating-now-what-post-grad-life-college-lauren-conrad-april-2013/" target="_blank"><spring:message code="msgDecortingHome_12"/></a></li>
<li><a href="http://mashable.com/2014/05/18/comedian-graduation-advice/" target="_blank"><spring:message code="msgDecortingHome_13"/></a></li>
</ul>
</div>
</div>
<!--Ist Tab -->

<!--IInd Tab -->
<div class="tab-pane" id="housing"  >
<div class="col-sm-1 col-md-1" style="color: black; text-align: right">
<img src="images/housingicon.png" width="40px" height="40px" /></div>
<div class="col-sm-5 col-md-5" style="color: black; text-align: left">
<h5> <spring:message code="lnkHousing"/></h5>
<span style="font-size: 12px; color:#404142; line-height: 23px;">
<spring:message code="msgDecortingHome_14"/>
</span>
</div>


<div class="col-sm-1 col-md-1" ></div>
<div class="col-sm-5 col-md-5" >
<ul class="fontleft">
<li><a href="http://www.domainehome.com/small-apartment-decorating-ideas-spring-2014/" target="_blank"><spring:message code="msgDecortingHome_15"/></a></li>
<li><a href="http://www.buzzfeed.com/alannaokun/things-nobody-tells-you-about-your-first-apartment" target="_blank"><spring:message code="msgDecortingHome_16"/></a></li>
<li><a href="http://www.collegexpress.com/articles-and-advice/grad-school/articles/life-grad-student/10-decorating-tips-make-your-first-apartment-feel-home/" target="_blank"><spring:message code="msgDecortingHome_17"/></a></li>
<li><a href="http://www.buzzfeed.com/peggy/39-easy-diy-ways-to-create-art-for-your-walls" target="_blank"><spring:message code="msgDecortingHome_18"/></a></li>
<li><a href="http://www.buzzfeed.com/peggy/cheap-and-easy-decorating-hacks-that-are-borderline-geniu" target="_blank"><spring:message code="msgDecortingHome_19"/></a></li>
</ul>




</div>
</div>
<!--IInd Tab -->

<!--3rd Tab -->
<div  class="tab-pane" id="finance"  >
<div class="col-sm-1 col-md-1" style="color: black; text-align: right">
<img src="images/financeicon.png" width="40px" height="40px" /></div>
<div class="col-sm-5 col-md-5" style="color: black; text-align: left">

<h5>Finance</h5>
<span style="font-size: 12px; color:#404142; line-height: 23px;">
<spring:message code="msgDecortingHome_20"/></span>
</div>


<div class="col-sm-1 col-md-1"></div>
<div class="col-sm-5 col-md-5">

<ul class="fontleft">
<li><a href="http://www.levo.com/articles/career-advice/3-better-ways-to-manage-your-student-loans" target="_blank"><spring:message code="msgDecortingHome_21"/> </a></li>
<li><a href="http://www.pcmag.com/article2/0,2817,2400562,00.asp" target="_blank"><spring:message code="msgDecortingHome_22"/></a></li>
<li><a href="http://www.levo.com/articles/lifestyle/how-much-your-paycheck-should-you-be-saving" target="_blank"><spring:message code="msgDecortingHome_23"/></a></li>
<li><a href="http://www.forbes.com/sites/nextavenue/2013/05/29/11-essential-money-tips-for-new-college-grads/" target="_blank"><spring:message code="msgDecortingHome_24"/></a></li>
<li><a href="http://www.popsugar.com/smart-living/Best-Grocery-Apps-20133901#photo-20133901"><spring:message code="msgDecortingHome_25"/></a></li>
<li><a href="https://www.tuition.io/blog/2014/08/6-pieces-advice-recent-graduates-get-started-right-financial-footing/" target="_blank"><spring:message code="msgDecortingHome_26"/></a></li>
<li><a href="http://couponing.about.com/cs/aboutcouponing/a/lowerbillpt2.htm" target="_blank"><spring:message code="msgDecortingHome_27"/></a></li>
<li><a href="http://www.myfirstapartment.com/2013/10/living-on-your-own-7-tips-for-avoiding-budget-woes/" target="_blank"><spring:message code="msgDecortingHome_28"/></a></li>
</ul>


</div>
</div>
<!--3rd Tab -->

<!--4th Tab -->
<div  class="tab-pane" id="wardrobe" >
<div class="col-sm-1 col-md-1">
<img src="images/wordrobeicon.png" width="40px" height="40px" /></div>
<div class="col-sm-5 col-md-5" style="color: black; text-align: left">

<h5><spring:message code="lnkWardrobe"/></h5>
<span style="font-size: 12px; color:#404142; line-height: 23px;">
<spring:message code="msgDecortingHome_29"/></span>
</div>


<div class="col-sm-1 col-md-1"></div>
<div class="col-sm-5 col-md-5">

<ul class="fontleft">
<li><a href="https://www.mint.com/blog/consumer-iq/the-7-best-apps-for-shopping-on-a-budget-0413/" target="_blank"><spring:message code="msgDecortingHome_30"/> </a></li>
<li><a href="http://www.huffingtonpost.com/uloop/top-websites-online-shopping_b_4847612.html" target="_blank"><spring:message code="msgDecortingHome_31"/></a></li>
<li><a href="http://www.bustle.com/articles/25514-7-ways-to-make-your-wardrobe-more-grown-up-after-college" target="_blank"><spring:message code="msgDecortingHome_32"/></a></li>
<li><a href="http://www.hercampus.com/style/how-transition-your-wardrobe-college-career" target="_blank"><spring:message code="msgDecortingHome_33"/></a></li>
<li><a href="http://excelle.monster.com/benefits/articles/4469-10-things-every-woman-should-have-in-her-work-wardrobe" target="_blank"><spring:message code="msgDecortingHome_34"/></a></li>
</ul>


</div>
</div>
<!--4th Tab -->
</div>
</div>
</div>
</center>
</div>