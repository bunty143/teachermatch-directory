<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script type="text/javascript" src="js/report/cgSpecificService.js?ver=${resourceMap['js/report/cgSpecificService.js']}"></script>
<script type="text/javascript" src="js/jobapplicationflow/jafAutoCompAcademics.js?ver=${resourceMap['js/jobapplicationflow/jafAutoCompAcademics.js']}"></script>
<!-- -Dhananjay -->
<script type="text/javascript" src="dwr/interface/InviteCandidateToPositionAjax.js?ver=${resourceMap['InviteCandidateToPositionAjax .ajax']}"></script>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />
<style>
.fa-exclamation-circle:before {
    font-size: 1.6em;
}
.phoni{
height: 25px;
width: 20px;
}
.divprofile{
font-weight: bold;
}
</style>

<c:set var="resourceMap"  value="${applicationScope.resourceMap}"/>

<div style="display:none; z-index: 5000;" id="loadingDiv" >
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'>
	 		<td style='padding-top:0px;" id='spnMpro' align='center'>
	 		</td>
 		</tr>
	</table>
</div>

<input type="hidden" id="jobId" name="jobId" value="${param.jobId}"/>
<input type="hidden" id="eRefAccess" name="eRefAccess" value="${eRefAccess}"/>

<script type='text/javascript' src="js/report/candidategridnew.js?ver=${resourceMap['js/report/candidategridnew.js']}"></script>
<script type='text/javascript' src="js/report/sspfCandidategridnew.js?ver=${resourceMap['js/report/sspfCandidategridnew.js']}"></script> 
<script type="text/javascript" src="dwr/interface/CandidateGridAjaxNew.js?ver=${resourceMap['CandidateGridReportAjaxNew.ajax']}"></script>
<script type="text/javascript">
//getCandidateGridLoad();
</script>

<script type="text/javascript" src="dwr/interface/ManageStatusAjax.js?ver=${resourceMap['ManageStatusAjax.ajax']}"></script>
<script type='text/javascript' src="js/report/cgstatusNew.js?ver=${resourceMap['js/report/cgstatusNew.js']}"></script>
<jsp:useBean id="today" class="java.util.Date" scope="page" />
<style>
.tmlogo{
	pointer-events: none;
}
#schoolHeader
{
	height: 40px;
}
</style>   
	
<style>
.btn {
  border-color: #c5c5c5;
  border-color: rgba(0, 0, 0, 0.15) rgba(0, 0, 0, 0.15) rgba(0, 0, 0, 0.25);
}
.btn-primary {
  color: #ffffff;
  text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
  background-color: #9fcf68;
  background-image: -moz-linear-gradient(top, #9fcf68, #9fcf68);
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#9fcf68), to(#9fcf68));
  background-image: -webkit-linear-gradient(top, #9fcf68, #9fcf68);
  background-image: -o-linear-gradient(top, #9fcf68, #9fcf68);
  background-image: linear-gradient(to bottom, #9fcf68, #9fcf68);
  background-repeat: repeat-x;
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ff0088cc', endColorstr='#ff0044cc', GradientType=0);
  border-color: #9fcf68 #9fcf68 #9fcf68;
  border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
  background-color: #9fcf68;
  filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
}
.btn-primary:hover,
.btn-primary:active,
.btn-primary.active,
.btn-primary.disabled,
.btn-primary[disabled] {
  color: #ffffff;
  background-color: #9fcf68;
  /*background-color: #0044cc;
  background-color: #003bb3;*/
}
.btn-primary:active,
.btn-primary.active {
  background-color: #9fcf68 \9;
}
.tooltip-inner {
  max-width: 700px;
  padding: 3px 8px;
  color: #fff;
  text-align: center;
  text-decoration: none;
  background-color: #000;
  border-radius: 4px;
}

 textarea{
 	width:420px;
 	height:100px;
 }
 .table-bordered {
    border-collapse: separate;
    border-color:#cccccc;
 }
 .table-bordered td,th {
  border-left:0px solid #dddddd;
 }
  .tdscore0{
	    width: 1px;
	    height:40px;
	    float: left;
  }
  .tddetails0{
	    width: 1px;
	    height:40px;
	    float: left;
  }
  tr.bgimage {
    background-image : url(images/color-gradients.png);
     background-repeat: no-repeat no-repeat;
 }
 .dropdown-menu{
 	min-width:40px;
 } 
 
  .popover-title {
  margin: 0;
  padding: 0px 0px 0px 0px;
  font-size: 0px;
  line-height: 0px;
  }
  .popover-content {
   margin: 0;
   padding: 0px 0px 0px 0px;
  }
  
  .popover.right .arrow {
  top: 100px;
  left: -10px;
  margin-top: -10px;
  border-width: 10px 10px 10px 0;
  border-right-color: #ffffff;
}

  .popover.right .arrow:after {
  border-width: 11px 11px 11px 0;
  border-right-color: #007AB4;;
  bottom: -11px;
  left: -1px;
	}
	
 .pull-left {
  margin-left:-52px;
  margin-top:7px;
 }	
 .tblborder{

 }
 .nobground2{
	padding: 6px 7px 6px 7px;
	-webkit-border-top-left-radius:30px;border-top-left-radius:30px;-moz-border-radius-topleft:30px;-webkit-border-top-right-radius:30px;border-top-right-radius:30px;-moz-border-radius-topright:30px;-webkit-border-bottom-right-radius:30px;border-bottom-right-radius:30px;-moz-border-radius-bottomright:30px;-webkit-border-bottom-left-radius:30px;border-bottom-left-radius:30px;-moz-border-radius-bottomright:30px;
 }
  .nobground1{
	padding: 6px 10px 6px 10px;
	-webkit-border-top-left-radius:30px;border-top-left-radius:30px;-moz-border-radius-topleft:30px;-webkit-border-top-right-radius:30px;border-top-right-radius:30px;-moz-border-radius-topright:30px;-webkit-border-bottom-right-radius:30px;border-bottom-right-radius:30px;-moz-border-radius-bottomright:30px;-webkit-border-bottom-left-radius:30px;border-bottom-left-radius:30px;-moz-border-radius-bottomright:30px;
 }
  .nobground3{
	padding: 6px 4px 6px 4px;
	-webkit-border-top-left-radius:30px;border-top-left-radius:30px;-moz-border-radius-topleft:30px;-webkit-border-top-right-radius:30px;border-top-right-radius:30px;-moz-border-radius-topright:30px;-webkit-border-bottom-right-radius:30px;border-bottom-right-radius:30px;-moz-border-radius-bottomright:30px;-webkit-border-bottom-left-radius:30px;border-bottom-left-radius:30px;-moz-border-radius-bottomright:30px;
 }
 .icon-ok-circle{
 	font-size: 3em;
	 color:#00FF00;
 }
 .icon-circle{
 	font-size: 3em;
	 color:#E46C0A;
 }
 .icon-remove-circle{
 	font-size: 3em;
	 color:red;
 }
 .icon-circle-blank{
 	 font-size: 3em;
	 color:red;
 }
 .icon-question-sign{
 	 font-size: 1.3em;
 }
 
 .icon-inner{
 	 font-size: 2.3333333333333333em;
 	 font-family: 'Bauhaus 93';
	 color:red;
	 margin-left:-26px;
	 vertical-align:5%;
 }
 .icon-inner2{
 	 font-size: 2.3333333333333333em;
 	 font-family: 'Bauhaus 93';
	 color:red;
	 margin-left:-24px;
	 vertical-align:8%;
 } 
 .circle{
  width: 48px;
  height: 48px;
  margin: 0em auto;
 }
 .subheading{
  font-weight:none; 
 }
  .icon-folder-open,icon-copy,icon-cut,icon-paste,icon-remove-sign,icon-edit
{
	color:#007AB4;
}
.marginleft20
{
	margin-left:-20px;
}

.margintop20
{
	margin-top:-20px;
} 
 
   div.t_fixed_header_main_wrapper {
	position 		: relative; 
	overflow 		: visible; 
  }
  
.modal_header_profile {

  border-bottom: 1px solid #eee;
  background-color: #0078b4;
  text-overflow: ellipsis; 
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  -webkit-border-radius: 6px;
     -moz-border-radius: 6px;
          border-radius: 6px;
  outline: none;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
     -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
          box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
     -moz-background-clip: padding-box;
          background-clip: padding-box;
}


.scrollspy_profile { height:500px; overflow-y:auto; overflow-x:hidden;padding-left:5px;
}


.divwidth
{
	width:728px;
}
.tablewidth
{
width: 900px;
}
.net-widget-footer 
{
	border-bottom: 1px solid #cccccc; 
	border-left: 1px solid #cccccc;
	border-right: 1px solid #cccccc;
	line-height:40px; 
	background-color: #F2FAEF;
	background-image: -moz-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#FFFFFF), to(#FFFFFF));
	background-image: -webkit-linear-gradient(top,  #FFFFFF, #FFFFFF);
	background-image: -o-linear-gradient(top, #FFFFFF, #FFFFFF);
	background-image: linear-gradient(to bottom,#FFFFFF, #FFFFFF);
	background-repeat: repeat-x;
	color:#4D4D4E;
	vertical-align: middle;
	
}
.modaljob {
  position: fixed;
  top: 40%;
  left: 45%;
  z-index: 2000;
  overflow: auto;
  width: 980px;  
  margin: -250px 0 0 -440px;
  background-color: #ffffff;
  border: 1px solid #999;
  border: 1px solid rgba(0, 0, 0, 0.3);
  *border: 1px solid #999;
  /* IE6-7 */

  -webkit-border-radius: 6px;
  -moz-border-radius: 6px;
  border-radius: 6px;
  -webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
  -webkit-background-clip: padding-box;
  -moz-background-clip: padding-box;
  background-clip: padding-box;
}
.net-corner-bottom 
{ 
	-moz-border-radius-bottomleft: 12px; -webkit-border-bottom-left-radius: 12px; border-bottom-left-radius: 12px; -moz-border-radius-bottomright: 12px; -webkit-border-bottom-right-radius: 12px; border-bottom-right-radius: 12px; 
}
.custom-div-border1
{
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-moz-border-radius: 6px;
border-radius: 6px;
}
.modal-border
{
/*border: 1px solid #0A619A;*/
}
.custom-div-border
{
padding:1px;
border-bottom-left-radius:2em;
border-bottom-right-radius:2em;
}
.pdfDivBorder
{
z-index: 5000;
border: 1px solid #999;
border: 1px solid rgba(0, 0, 0, 0.3);
-webkit-border-radius: 6px;
-moz-border-radius: 6px;
border-radius: 6px;
-webkit-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-moz-box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
box-shadow: 0 3px 7px rgba(0, 0, 0, 0.3);
-webkit-background-clip: padding-box;
-moz-background-clip: padding-box;
background-clip: padding-box; 
}
.custom-border
{
margin-left: -32px;
margin-top: 2px;
}


.status-notes-image
{
  margin-left: 40px; 
}
.status-notes-text
{
  margin-top:-20px;
  margin-left: 80px;
}

.profileContent
{
	padding: 0px;
	padding-left: 12px;
	padding-top: 10px;
	color: #474747;
	font-weight: normal;
	font-size: 10px;
}
/* add By Ram Nath */

#divStatusNoteGrid .row, #divStatusNoteGrid .jqte{
	width: 100% !important;
}
.philadelphiaParent{
	padding: 0px 15px 0px 15px !important;
}
.philadelphiaMid{
	padding: 0px 15px 0px 15px;
	background-color:#595959;
	border-radius: 25px;  	
}
.philadelphiaMidGreen{
	padding: 0px 15px 0px 15px;
	background-color:#71C51C;
	border-radius: 25px;
 }
/* end By Ram Nath [25,120,175,100,50,50,100,50]*/
.td1{
width: 25px !important;
}
.td2{
width: 120px !important;
}
.td3{
width: 185px !important;
}
.td4{
width: 100px !important;
}
.td5{
width: 50px !important;
}
.td6{
width: 50px !important;
}
.td8{
width: 50px !important;
}


/* end By Ram Nath */	
</style>
 
<script type="text/javascript">

  function applyScrollOnLEACandidatePorfolio()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLEACandidatePorfolioGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
       <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[200,100,200,150,100,80],
           </c:when>
           <c:otherwise>
             colratio:[200,100,150,150,80,50],
           </c:otherwise>
        </c:choose>
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}


 function applyScrollOnTblLicenseDate()
{	
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#divLicneseGridCertificationsGridD').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
            colratio:[400,220,210],
           </c:when>
           <c:otherwise>
             colratio:[350,220,160],
           </c:otherwise>
        </c:choose>
       	addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });			
}

function applyScrollOnHonors()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j("#honorsGrid").fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[830],
           </c:when>
           <c:otherwise>
             colratio:[730],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}
function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#involvementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        
        height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[245,245,170,170],
           </c:when>
           <c:otherwise>
             colratio:[220,220,145,145],
           </c:otherwise>
        </c:choose>
        
/*		height: 350,
        width: 832,
        minWidth: null,
        minWidthAuto: false,
        colratio:[245,245,170,171], // table header width
*/
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnTb()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#schoolListGrid').fixheadertable({  
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 200,
        width: 680,
        minWidth: null,
        minWidthAuto: false,
        colratio:[350,330],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: true,
        sortedColId: null,
        sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });		
}

function applyScrollOnEducation()
{
var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#gridDataTeacherEducationsForLablel').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3 || entityType==5}">
             colratio:[200,200,220,210],
           </c:when>
           <c:otherwise>
             colratio:[200,170,200,160],
           </c:otherwise>
        </c:choose>
       	addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });		
}

function applyScrollOnTblVerifyTranscript()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#transcriptVerificationTab').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        width: 826,
        minWidth: null,
        minWidthAuto: false,
        colratio:[35,150,180,200,185,76],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
    });	
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}
function showReferencesForm()
{
	 $("#divElectronicReferences").show();
	 $('textarea').jqte();
	 resetReferenceform();
	 document.getElementById("salutation").focus();
}
//======================= Mukesh References ===============================

var countContactFlag=0;
function numericFilter(txb) {  
   txb.value = txb.value.replace(/[^\0-9]/ig, "");  
   if(!txb.value)
   {   
   if(countContactFlag==0)
   $('#errordivElectronicReferences').append("&#149; "+resourceJSON.errorMsgForCandidate+"<br>");
   
   countContactFlag++;
   }
}
function insertOrUpdateElectronicReferencesM(sbtsource)
{
	var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	 var firstName="";
	 var lastName="";
	 		//resetReferenceform();   
	 		var referenceDetails	= trim($('#referenceDetailstext').find(".jqte_editor").html());
			var teacherId		=   trim(document.getElementById("teacherId").value);
			var elerefAutoId	=	document.getElementById("elerefAutoId");
			var salutation		=	document.getElementById("salutation");
			
			if(myfolder==1)
			{
			 firstName		=	trim(document.getElementById("firstName1").value);
			 lastName		= 	trim(document.getElementById("lastName1").value);
			}else{
			 firstName		=	trim(document.getElementById("firstName").value);
			 lastName		= 	trim(document.getElementById("lastName").value);
			}
			var designation		= 	trim(document.getElementById("designation").value);
			
			var organization	=	trim(document.getElementById("organization").value);
			var email			=	trim(document.getElementById("email").value);
			
			var contactnumber	=	trim(document.getElementById("contactnumber").value);
			var longHaveYouKnow	=	trim(document.getElementById("longHaveYouKnow").value);
			var rdcontacted0	=   document.getElementById("rdcontacted0");
			var rdcontacted1	=   document.getElementById("rdcontacted1");
			var pathOfReferencesFile = document.getElementById("pathOfReferenceFile");
			
			var cnt=0;
			var focs=0;	
			
			var fileName = pathOfReferencesFile.value;
			var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
			var fileSize=0;		
			if ($.browser.msie==true){	
			    fileSize = 0;	   
			}else{		
				if(pathOfReferencesFile.files[0]!=undefined)
				fileSize = pathOfReferencesFile.files[0].size;
			}
			$('#errordivElectronicReferences').empty();
			setDefColortoErrorMsgToElectronicReferences();
			if(firstName=="")
			{
				$('#errordivElectronicReferences').append("&#149; Please enter First Name<br>");
				if(focs==0)
				{ 
					if(myfolder==1)
					 $('#firstName1').focus();
					else
					 $('#firstName').focus();
				}
				if(myfolder==1)
				 $('#firstName1').css("background-color",txtBgColor);
				else
				 $('#firstName1').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(lastName=="")
			{
				$('#errordivElectronicReferences').append("&#149; Please enter Last Name <br>");
				if(focs==0){
					if(myfolder==1)
					  $('#lastName1').focus();
					else
					  $('#lastName').focus();
				}
				if(myfolder==1)
				 $('#lastName1').css("background-color",txtBgColor);
				else
				 $('#lastName').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(email=="")
			{
				$('#errordivElectronicReferences').append("&#149; Please enter Email<br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			else if(!isEmailAddress(email))
			{		
				$('#errordivElectronicReferences').append("&#149; Please enter valid Email<br>");
				if(focs==0)
					$('#email').focus();
				
				$('#email').css("background-color",txtBgColor);
				cnt++;focs++;
			}if(contactnumber=="")
			{
				$('#errordivElectronicReferences').append("&#149; Please enter Contact Number<br>");
				if(focs==0)
					$('#contactnumber').focus();
				
				$('#contactnumber').css("background-color",txtBgColor);
				cnt++;focs++;
			}			
			else if(contactnumber.length==10)
			{
				
			}
			else
			{
				$('#errordivElectronicReferences').append("&#149; Please enter exactly (Ten)10 numeric digits<br>");
				if(focs==0)
					$('#contactnumber').focus();

				$('#contactnumber').css("background-color",txtBgColor);
				cnt++;focs++;
			}	
			
			if(designation=="" &&  ((window.location.hostname=="nccloud.teachermatch.org") || (window.location.hostname=="nc.teachermatch.org") || (window.location.hostname=="localhost")))
			{
				$('#errordivElectronicReferences').append("&#149; Please enter Title<br>");
				if(focs==0)
					$('#designation').focus();
				
				$('#designation').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			if(organization=="")
			{
				$('#errordivElectronicReferences').append("&#149; Please enter Organization<br>");
				if(focs==0)
					$('#organization').focus();
				
				$('#organization').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			if(longHaveYouKnow=="" && $("#districtId").val()==4218990)
			{
				$('#errordivElectronicReferences').append("&#149; Please enter How Long Have You Known This Person?<br>");
				if(focs==0)
					$('#longHaveYouKnow').focus();
				
				$('#longHaveYouKnow').css("background-color",txtBgColor);
				cnt++;focs++;
			}
			
			var rdcontacted_value;
			
			if (rdcontacted0.checked) {
				rdcontacted_value = false;
			}
			else if (rdcontacted1.checked) {
				rdcontacted_value = true;
			}
			
			if(ext!=""){
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errordivElectronicReferences').append("&#149; Acceptable file formats include PDF, MS-Word, GIF, PNG, and JPEG files<br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}	
				else if(fileSize>=10485760)
				{
					$('#errordivElectronicReferences').append("&#149; File size must be less than 10mb<br>");
						if(focs==0)
							$('#pathOfReferenceFile').focus();
						
						$('#pathOfReferenceFile').css("background-color",txtBgColor);
						cnt++;focs++;	
						return false;
				}
			}
			

			if(cnt!=0)		
			{
				$('#errordivElectronicReferences').show();
				return false;
			}
			else
			{
				var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null};
				dwr.engine.beginBatch();
				
				dwr.util.getValues(teacherElectronicReferences);
				teacherElectronicReferences.rdcontacted=rdcontacted_value;
				teacherElectronicReferences.salutation=salutation.value;
				teacherElectronicReferences.firstName=firstName;
				teacherElectronicReferences.lastName=lastName;;
				teacherElectronicReferences.designation=designation;
				teacherElectronicReferences.organization=organization;
				teacherElectronicReferences.email=email;
				teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;
				teacherElectronicReferences.referenceDetails=referenceDetails;
				
				var refFile = document.getElementById("pathOfReference").value;
				if(refFile!=""){
					teacherElectronicReferences.pathOfReference=refFile;
				}
				if(fileName!="")
				{	
					
					CGServiceAjax.findDuplicateReferences(teacherElectronicReferences,teacherId,{ 
						async: false,
						errorHandler:handleError,
						callback: function(data)
						{
						if(data=="isDuplicate")
						{
							$('#errordivElectronicReferences').append("&#149; An Electronic Reference has already registered with this Email<br>");
							if(focs==0)
								$('#email').focus();
							$('#email').css("background-color",txtBgColor);
							cnt++;focs++;
							$('#errordivElectronicReferences').show();
							return false;
						}else{
							document.getElementById("frmElectronicReferences").submit();
						}
						}
					});
					
				}else{
					CGServiceAjax.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
						async: true,
						errorHandler:handleError,
						callback: function(data)
						{
							if(data=="isDuplicate")
							{
								$('#errordivElectronicReferences').append("&#149; An Electronic Reference has already registered with the provided Email<br>");
								if(focs==0)
									$('#email').focus();
								$('#email').css("background-color",txtBgColor);
									cnt++;focs++;
								$('#errordivElectronicReferences').show();
								return false;
							}
							hideElectronicReferencesForm();
							getElectronicReferencesGrid_DivProfile(teacherId);
						}
					});
				}
				
				dwr.engine.endBatch();
				return true;
			}
			
}

function setDefColortoErrorMsgToElectronicReferences()
{
	var myfolder=0;
	var sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	$('#salutation').css("background-color","");
	
	if(myfolder==1){
	 	$('#firstName1').css("background-color","");
	 	$('#lastName1').css("background-color","");
	}else{
		$('#firstName').css("background-color","");
	 	$('#lastName').css("background-color","");
	}
	
	$('#designation').css("background-color","");
	$('#organization').css("background-color","");
	$('#email').css("background-color","");
	$('#contactnumber').css("background-color","");
}

function hideElectronicReferencesForm()
{
	resetReferenceform();
	document.getElementById("elerefAutoId").value="";
	document.getElementById("divElectronicReferences").style.display="none";
	//resetReferenceform();
	return false;
}
function resetReferenceform()
{
	var myfolder=0;
	var sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	    document.getElementById("rdcontacted1").checked = true
	 	document.getElementById("salutation").value=0;
	    if(myfolder==1){
			document.getElementById("firstName1").value='';
			document.getElementById("lastName1").value='';
	    }else{
	    	document.getElementById("firstName").value='';
			document.getElementById("lastName").value='';
	    }
		document.getElementById("designation").value='';
		document.getElementById("organization").value='';
		document.getElementById("email").value='';
		document.getElementById("contactnumber").value='';
		$('#referenceDetailstext').find(".jqte_editor").html("");
		document.getElementById("pathOfReferenceFile").value="";
		document.getElementById("pathOfReferenceFile").value=null;
}


function saveNobleReference(fileName)
{
    var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
	    var firstName="";
	    var lastName="";
       var referenceDetailstext	= trim($('#referenceDetailstext').find(".jqte_editor").text());
		var teacherId		=   trim(document.getElementById("teacherId").value);	
	    var elerefAutoId	=	document.getElementById("elerefAutoId");
		var salutation		=	document.getElementById("salutation");
		if(myfolder==1){
		 firstName		=	trim(document.getElementById("firstName1").value);
		 lastName		= 	trim(document.getElementById("lastName1").value);
		}else{
			 firstName		=	trim(document.getElementById("firstName").value);
			 lastName		= 	trim(document.getElementById("lastName").value);
		}
		var designation		= 	trim(document.getElementById("designation").value);
		
		var organization	=	trim(document.getElementById("organization").value);
		var email			=	trim(document.getElementById("email").value);
		
		var contactnumber	=	trim(document.getElementById("contactnumber").value);
		var rdcontacted0	=   document.getElementById("rdcontacted0");
		var rdcontacted1	=   document.getElementById("rdcontacted1");
		var longHaveYouKnow	= trim(document.getElementById("longHaveYouKnow").value);
		
		var cnt=0;
		var focs=0;	
		
		var rdcontacted_value;
		if (rdcontacted0.checked) {
			rdcontacted_value = false;
		}
		else if (rdcontacted1.checked) {
			rdcontacted_value = true;
		}
		
		var teacherElectronicReferences = {elerefAutoId:null,salutation:null, firstName:null, lastName:null, designation:null, organization:null,email:null,contactnumber:null,rdcontacted:null,pathOfReference:null,longHaveYouKnow:null};
		dwr.engine.beginBatch();
	
		dwr.util.getValues(teacherElectronicReferences);
		teacherElectronicReferences.rdcontacted=rdcontacted_value;
		teacherElectronicReferences.salutation=salutation.value;
		teacherElectronicReferences.firstName=firstName;
		teacherElectronicReferences.lastName=lastName;;
		teacherElectronicReferences.designation=designation;
		teacherElectronicReferences.organization=organization;
		teacherElectronicReferences.email=email;
		teacherElectronicReferences.pathOfReference=fileName;
		teacherElectronicReferences.referenceDetails=referenceDetailstext;
		teacherElectronicReferences.longHaveYouKnow=longHaveYouKnow;

		CGServiceAjax.saveUpdateElectronicReferences(teacherElectronicReferences,teacherId,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				if(data=="isDuplicate")
				{
					///updateReturnThreadCount("ref");
					$('#errordivElectronicReferences').append("&#149; An Electronic Reference has already registered with the provided Email<br>");
					if(focs==0)
						$('#email').focus();
					$('#email').css("background-color",txtBgColor);
						cnt++;focs++;
					$('#errordivElectronicReferences').show();
					return false;
				}
				hideElectronicReferencesForm();
				getElectronicReferencesGrid_DivProfile(teacherId);
				
			}
		});
	
		dwr.engine.endBatch();
		return true;
	}
function editFormElectronicReferences(id)
{
	var myfolder=0;
	var	sUrl = window.location.pathname;
	 if(sUrl.indexOf("myfolder") > -1)
		myfolder=1;
$('textarea').jqte();
	 $('#errordivElectronicReferences').empty();
	 setDefColortoErrorMsgToElectronicReferences();
	 CGServiceAjax.editElectronicReferences(id,
		{ 
		  async: false,
		  errorHandler:handleError,
		  callback: function(data)
		  {
	        showReferencesForm();
	        dwr.util.setValues(data);
	      
	        if(sUrl.indexOf("myfolder") > -1){
	    	  document.getElementById("firstName1").value  =data.firstName;
			  document.getElementById("lastName1").value	   =data.lastName;
	        }
		       
			$('#referenceDetailstext').find(".jqte_editor").html(data.referenceDetails);
			if(data.pathOfReference!=null && data.pathOfReference!="")
			{	
				 document.getElementById("pathOfReference").value=data.pathOfReference;
			}else{
				document.getElementById("pathOfReference").value="";
			}
			
			return false;
			}
		});
	return false;
}

function removeReferences()
{
	var teacherId =  document.getElementById("teacherId").value
	var referenceId = document.getElementById("elerefAutoId").value;
	if(window.confirm("Are you sure, you would like to delete the Recommendation Letter."))
	{
		CGServiceAjax.removeReferencesForNoble(referenceId,teacherId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
				getElectronicReferencesGrid_DivProfile(teacherId);
			//	document.getElementById("removeref").style.display="none";
				hideElectronicReferencesForm();
				
			}
		});
	}
}
var eleRefIDForTeacher=0;
var status_eref=0;
function changeStatusElectronicReferences(id,status)
{	
	eleRefIDForTeacher=id;
	status_eref=status;
	if(status==1)
	$('#chgstatusRef1').modal('show');
	else if(status==0)
	 $('#chgstatusRef2').modal('show');	
}
function changeStatusElectronicReferencesConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value
	$('#chgstatusRef1').modal('hide');
	$('#chgstatusRef2').modal('hide');
	
		CGServiceAjax.changeStatusElectronicReferences(eleRefIDForTeacher,status_eref, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			    getElectronicReferencesGrid_DivProfile(teacherId);
				hideElectronicReferencesForm();
				return false;
			}});
		
		eleRefIDForTeacher=0;
}


function setDefColortoErrorMsgToVideoLinksForm()
{
	$('#videourl').css("background-color","");
}
function showVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="block";
	document.getElementById("videourl").focus();
	setDefColortoErrorMsgToVideoLinksForm();
	$('#errordivvideoLinks').empty();
	return false;
}

function hideVideoLinksForm()
{
	document.getElementById("frmvideoLinks").reset();
	document.getElementById("videolinkAutoId").value="";
	document.getElementById("divvideoLinks").style.display="none";
	return false;
}

function insertOrUpdatevideoLinks()
{
	 
	 var teacherId =  document.getElementById("teacherId").value
	var elerefAutoId = document.getElementById("videolinkAutoId");
	var videourl = trim(document.getElementById("videourl").value);
	var cnt=0;
	var focs=0;	
	
	$('#errordivvideoLinks').empty();
	setDefColortoErrorMsgToVideoLinksForm();
	
	if(videourl=="")
	{
		$('#errordivvideoLinks').append("&#149; Please enter Video Link<br>");
		if(videourl==0)
			$('#videourl').focus();
		
		$('#videourl').css("background-color",txtBgColor);
		cnt++;focs++;
	}
	
	if(cnt!=0 )		
	{
		$('#errordivvideoLinks').show();
		return false;
	}
	else
	{	
		var teacherVideoLink = {videolinkAutoId:null,videourl:null, createdDate:null};
		dwr.engine.beginBatch();
		dwr.util.getValues(teacherVideoLink);
		teacherVideoLink.videourl=videourl;
		CGServiceAjax.saveOrUpdateVideoLink(teacherVideoLink,teacherId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				hideVideoLinksForm();
				getVideoLinksGrid_DivProfile(teacherId);
			}
		});
		dwr.engine.endBatch();
		return true;
	}
}


function editFormVideoLink(id)
{
	CGServiceAjax.editVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

function deleteFormVideoLink(id)
{
	CGServiceAjax.deleteVideoLink(id,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				showVideoLinksForm();
				dwr.util.setValues(data);
				return false;
			}
		});
	return false;
}

var videolnkIDForTeacher=0;
function delVideoLink(id)
{	
	videolnkIDForTeacher=id;
	$('#delVideoLnk').modal('show');
}
function delVideoLnkConfirm()
{	
	 var teacherId =  document.getElementById("teacherId").value	
	 $('#delVideoLnk').modal('hide');
		CGServiceAjax.deleteVIdeoLink(videolnkIDForTeacher, { 
			async: true,
			errorHandler:handleError,
			callback:function(data)
			{
			getVideoLinksGrid_DivProfile(teacherId);	
			//getVideoLinksGrid();
				hideVideoLinksForm();
				return false;
			}});
		
		videolnkIDForTeacher=0;
}


var $j=jQuery.noConflict();
        $j(document).ready(function() {
            
        });   
        
        
function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        
         <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[550,280],
           </c:when>
           <c:otherwise>
	         colratio:[500,230],
	       </c:otherwise>
       </c:choose>
        
        
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}      
   
function applyScrollOnTbl()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 250,
        width: 900,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,150,150,150,150,150], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblLanguage()
{
    var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
             width: 730,
           </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
         // table header width
        
         height: 380,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[277,276,276],
           </c:when>
           <c:otherwise>
             colratio:[244,243,243],
           </c:otherwise>
        </c:choose>
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });         
}
applyScrollOnTbl();

function applyScrollOnTranscript()
{

		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTrans').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[150,150,150,150,150,125], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnCertification()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblCert').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 875,
        minWidth: null,
        minWidthAuto: false,
        colratio:[100,50,350,200,175], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnPhone()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
	        $j('#tblPhones').fixheadertable({ //table id
	        caption: '',
	        showhide: false,
	        theme: 'ui',
	        height: 110,
	        width: 615,
	        minWidth: null,
	        minWidthAuto: false,
	        colratio:[400,100,112], // table header width
	        addTitles: false,
	        zebra: true,
	        zebraClass: 'net-alternative-row',
	        sortable: false,
	        sortedColId: null,
	        dateFormat: 'd-m-y',
	        pager: false,
	        rowsPerPage: 10,
	        resizeCol: false,
	        minColWidth: 100,
	        wrapper: false
	        });
        });
	}

// Start Div Grid

function applyScrollOnTblWorkExp()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblWorkExp_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[148,222,111,119,111],
          
           <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[178,298,100,123,131],
           </c:when>
           <c:otherwise>
	         colratio:[148,278,90,103,111],
	       </c:otherwise>
       </c:choose>
          
          
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblEleRef_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleleReferences_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[150,130,100,160,100,80],00
          //colratio:[165,90,95,170,90,70,50],
          //colratio:[118,86,91,242,83,65,45],
          
     <c:choose>
          <c:when test="${(entityType==2 && DistrictId==7800038) || (entityType==3 && DistrictId==7800038)}">
             colratio:[160,130,160,135,90,45,110],
           </c:when>
           <c:when test="${(entityType==2 && DistrictId ne 7800038) || (entityType==3 && DistrictId ne 7800038) }">
             colratio:[110,75,90,150,100,85,65,45,110],
           </c:when>
           <c:otherwise>
	          colratio:[160,130,160,135,90,45,110],
	       </c:otherwise>
       </c:choose>
          
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblSubjectAreaExam()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#subjectAreasGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[148,222,111,119,111],
          
           <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[80,100,200,100,350],
           </c:when>
           <c:otherwise>
	         colratio:[70,100,180,90,290],
	       </c:otherwise>
       </c:choose>
          
          
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblVideoLinks_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblelevideoLink_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
         <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
          //colratio:[600,110],
         
         
         
          <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
              colratio:[620,110,100],
           </c:when>
           <c:otherwise>
	          colratio:[620,110],
	       </c:otherwise>
       </c:choose>
         
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblBackgroundCheck_profile()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tbleBgCheck_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
	    width: 830,
        minWidth: null,
        minWidthAuto: false,
	    colratio:[332,243,255],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblProfileVisitHistory()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblProfileVisitHistory_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        width: 570,
        minWidth: null,
        minWidthAuto: false,
          colratio:[460,110],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function applyScrollOnTblTeacherAcademics_profile()
{
	//alert("Hello")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherAcademics_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
       <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,    
       // colratio:[350,260,60,60],
       <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[225,100,125,400],
           </c:when>
           <c:otherwise>
	        colratio:[410,180,60,60],
	       </c:otherwise>
       </c:choose>
       
        
        
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblTeacherCertifications_profile()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblTeacherCertificates_Profile').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        //colratio:[500,230],
        
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             colratio:[225,125,125,155,255],
           </c:when>
           <c:otherwise>
	         colratio:[200,100,100,130,200],
	       </c:otherwise>
       </c:choose>
        
        
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 20,
        wrapper: false
        });
            
        });			
}

function applyScrollOnInterTrans(){
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblInterTrans').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 470,
        minWidth: null,
        minWidthAuto: false,
        colratio:[120,120,230], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

function applyScrollOnAssessmentDate()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridDate').fixheadertable({ //table id 
        caption: '',
        showhide: false,
      //  theme: 'ui',
        height: 250,
        width: 400,
        minWidth: null,
        minWidthAuto: false,
        colratio:[350,340], // table header width
        addTitles: false,
        zebra: true,
       // zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function deletesavefolder(rootNode)
{
	//alert("cg Jsp 283 tfff"+rootNode);
	// Gagan Setting Key here to hidden because tree object can not be retrieved in iframe
	$('#currentObject').val(rootNode.data.key);
	$('#deleteFolder').modal('show');
	
}
function deleteconfirm()
{
	$('#deleteFolder').modal('hide');
	var rootNodeKey =$('#currentObject').val();
	//alert("Cg Jsp 293 rootNodeKey "+rootNodeKey)
	document.getElementById('iframeSaveCandidate').contentWindow.deleteFolder(rootNodeKey);
}

function applyScrollOnJob()
{ 
    var hig=$(window).height(); 
    hig=hig-350;  
	var $j=jQuery.noConflict();
         $j(document).ready(function() {
         $j('#jobTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: hig,
    
        minWidth: null,
        minWidthAuto: false,
         <c:choose>
    	<c:when test="${entityID==1}">
         colratio:[40,55,205,110,120,80,100,65,70,110], // table header width
        </c:when>
        <c:otherwise>
        <c:choose>
		<c:when test="${prefMap['fitScore'] && prefMap['demoClass']}">
         colratio:[40,55,205,110,130,80,100,65,70,110],
        </c:when>
        <c:when test="${!prefMap['fitScore'] && !prefMap['demoClass']}">
          colratio:[40,55,310,110,130,80,110,120],
        </c:when>
        <c:otherwise>
          colratio:[40,55,250,110,130,80,100,80,110],
        </c:otherwise>
        </c:choose>
        </c:otherwise>
     </c:choose>
     
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_AssessmentDetails()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentDetails').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        //height: 100,
        width: 830,
	    minWidth: null,
        minWidthAuto: false,
        olratio:[230,200,200,200],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnAssessments()
{		
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#assessmentTable').fixheadertable({ 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 150,
        <c:choose>
          <c:when test="${entityType==2 || entityType==3}">
             width: 830,
           </c:when>
           <c:otherwise>
	         width: 730,
	       </c:otherwise>
        </c:choose>
        minWidth: null,
        minWidthAuto: false,
        colratio:[180,120,150,185], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
</script>


<c:set var="jobCatId"  value="${jobCategoryId}"/>
<link rel="stylesheet" type="text/css" href="calender/css/jscal2.css" />
<link rel="stylesheet" type="text/css" href="calender/css/border-radius.css" />
<link rel="stylesheet" type="text/css" href="calender/css/gold/gold.css" />

<div id="tm-root" style='display:run-in'></div>

<input type="hidden"  name="showHideType" id="showHideType" value="0"/>

<input type="hidden"  name="isDistrictWideTags" id="isDistrictWideTags" value="${isDistrictWideTags}"/>
<input type="hidden"  name="isJobSpecificTags" id="isJobSpecificTags" value="${isJobSpecificTags}"/>
<input type="hidden"  name="entityTypeId" id="entityTypeId" value="${entityTypeId}"/>

<input type="hidden"  name="questionSetIdForCg" id="questionSetIdForCg"/>
<input type="hidden"  name="isReqNoForHiring" id="isReqNoForHiring" value="${isReqNoForHiring}"/>
<input type="hidden"  name="jobCategoryDiv" id="jobCategoryDiv" value="2"/>
<input type="hidden"  name="jobCategoryFlag" id="jobCategoryFlag"/>
<input type="hidden"  name="reqNumber" id="reqNumber"/>
<input type="hidden"  name="offerReady" id="offerReady"/>
<input type="hidden"  name="pageFlag" id="pageFlag" value="0"/>
<input type="hidden" id="commDivFlag" name="commDivFlag" value=""/>
<input type="hidden" id="jobForTeacherGId" name="jobForTeacherGId" />
<input type="hidden"  name="currentObject" id="currentObject"/>
<input type="hidden"  name="userFPD" id="userFPD"/>
<input type="hidden"  name="userFPS" id="userFPS"/>
<input type="hidden"  name="userCPD" id="userCPD"/>
<input type="hidden"  name="userCPS" id="userCPS"/>
<input type="hidden"  name="folderId" id="folderId"/>
<input type="hidden"  name="checkboxshowHideFlag" id="checkboxshowHideFlag"/>
<input type="hidden"  name="store" id="store"/>
<input type="hidden"  name="expandchk" id="expandchk" value="0"/>
<input type="hidden"  name="expandremovechk" id="expandremovechk" value="0"/>
<input type="hidden"  name="expandvltchk" id="expandvltchk" value="0"/>
<input type="hidden"  name="expandhiredchk" id="expandhiredchk" value="0"/>
<input type="hidden"  name="expandincompchk" id="expandincompchk" value="0"/>
<input type="hidden"  name="expandinterchk" id="expandinterchk" value="0"/>
<input type="hidden"  name="expandwithdrchk" id="expandwithdrchk" value="0"/>
<input type="hidden"  name="expandhiddenchk" id="expandhiddenchk" value="0"/>
<input type="hidden" id=phoneType name="phoneType" value="0"/>
<input type="hidden" id=msgType name="msgType" value="0"/>		

<input type="hidden" id="noteId" name="noteId" value=""/>
<input type="hidden" id="teacherIdForSchoolSelection" name="teacherIdForAssessmentInvite"/>
<input type="hidden" id="teacherIdForSchoolSelection" name="teacherIdForSchoolSelection"/>
<input type="hidden" id="statusIdForStatusNote" name="statusIdForStatusNote" value="0"/>
<input type="hidden" id="secondaryStatusIdForStatusNote" name="secondaryStatusIdForStatusNote" value="0"/>
<input type="hidden" id="fitScoreForStatusNote" name="fitScoreForStatusNote" value="0"/>
<input type="hidden" id="finalizeStatus" name="finalizeStatus" value="0"/>
<input type="hidden" id="teacherStatusNoteId" name="teacherStatusNoteId" value="0"/>
<input type="hidden" id="statusNoteDeleteType" name="statusNoteDeleteType" value="0"/>
<input type="hidden" id="teacherIdForprofileGrid" name="teacherIdForprofileGrid"/>
<input type="hidden" id="jobForTeacherIdForSNote" name="jobForTeacherIdForSNote"/>
<input type="hidden" id="updateStatusHDRFlag" name="updateStatusHDRFlag"/>
<input type="hidden" id="teacherIdForprofileGridVisitLocation" name="teacherIdForprofileGridVisitLocation" value="CG View" />
<input type="hidden" id="noOfRecordCheck" name="noOfRecordCheck"/>
<input type="hidden" id="doActivity" name="doActivity"/>
<input type="hidden" id="doActivityForOfferReady" name="doActivityForOfferReady"/>
<input type="hidden" id="hireFlag" name="hireFlag" value="0"/>

<input type="hidden" id="offerAccepted" name="offerAccepted" value="1"/>
<input type="hidden" id="headQuarterId" name="headQuarterId" value="${headQuarterId}"/>
<input type="hidden" id="entityType" name="entityType" value="${userMaster.entityType}"/>
<c:choose>
<c:when test="${userMaster.entityType eq 5 || userMaster.entityType eq 6}">
<c:set var="undo" value="Reset"></c:set>
</c:when>
<c:otherwise>
<c:set var="undo" value="Undo"></c:set>
</c:otherwise>
</c:choose>


<!-- candidate profile div -->
<div class="modal hide custom-div-border1" id="cgTeacherDivMaster" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div id="cgTeacherDivBody">
</div>
</div>
<iframe src="" id="ifrmCert" width="100%" height="480px" style="display: none;"></iframe>
<!--@Start
	@Ashish :: Add Qualification Div -->
<div class="modal hide" id="qualificationDiv" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px; ">
	<div class="modal-content">
	<div class="modal-header" id="qualificationDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick=''>x</button>
		<h3 style="padding-left: 5px;">Qualifications</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; padding-top:0px; overflow-y: scroll;">
	<input type="hidden" id="jftIdforPrint" name="jftIdforPrint" />
			<div class='divErrorMsg' id='errorQStatus' style="display: block;"></div>
			<div id="qualificationDivBody"></div>
	</div>
 	<div class="modal-footer">
	 	<table width=470 border=0>
	 		<tr>
		 		<td width=100 nowrap align=left>
		 		<span id="qStatusSave" >
		 		<button class="btn  btn-large btn-orange"  onclick='saveQualificationNote(0)'>In-Progress</button>&nbsp;&nbsp;</span>
		 		</td>
		 		<td width=200  nowrap>
		 	<!-- <button class="btn btn-large" aria-hidden="true" onclick='printQualificationQuestion();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp; -->				
		 	
				<span id="qStatusFinalize" >
				<button class="btn  btn-large btn-primary" onclick='saveQualificationNote(1)'>Finalize <i class="iconlock"></i></button>&nbsp;&nbsp;</span>	
				<button class="btn  btn-large btn-primary" data-dismiss="modal" aria-hidden="true" onclick="printQualificationIssuse();">Print</button>	 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='cancelQualificationIssuse();'>Cancel</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
   </div>
  </div>
 </div>
 
 
 
 
 
 <!--  Display Interview Question Div For Red-->
 <div class="modal hide" id="interviewQuesRed" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px; ">
	<div class="modal-content">
	<div class="modal-header" id="interviewQuesRedeader">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick=''>x</button>
		<h3 style="padding-left: 5px;">Invite for Virtual Video Interview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; padding-top:0px; overflow-y: scroll;">
			<div id="interviewQuesRedBody"></div>
	</div>
 	<div class="modal-footer">
	 	<table width=470 border=0>
	 		<tr>
		 		<td width=200  nowrap>
		 	<!-- <button class="btn btn-large" aria-hidden="true" onclick='printQualificationQuestion();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp; -->				
		 	
				<span id="inviteOK" >
				<button class="btn  btn-large btn-primary" onclick='saveIIStatus();'>OK <i class="iconlock"></i></button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick=''>Cancel</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
   </div>
  </div>
 </div>

<!--  Display Interview Question Div For Gray-->
 <div class="modal hide" id="interviewQuesGrey" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px; ">
	<div class="modal-content">
	<div class="modal-header" id="interviewQuesGreyHeader">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick=''>x</button>
		<h3 style="padding-left: 5px;">Reinvite for Virtual Video Interview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; padding-top:0px; overflow-y: scroll;">
			<div id="interviewQuesGreyBody"></div>
	</div>
 	<div class="modal-footer">
	 	<table width=470 border=0>
	 		<tr>
		 		<td width=200  nowrap>
		 	<!-- <button class="btn btn-large" aria-hidden="true" onclick='printQualificationQuestion();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp; -->				
		 	
				<span id="reInviteOK" >
				<button class="btn  btn-large btn-primary" onclick='saveIIStatusForGrey();'>OK <i class="iconlock"></i></button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick=''>Cancel</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
   </div>
  </div>
 </div>

<!--  Display Interview Question Div For Orange-->
 <div class="modal hide" id="interviewQuesOrange" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px; ">
	<div class="modal-content">
	<div class="modal-header" id="interviewQuesOrangeHeader">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick=''>x</button>
		<h3 style="padding-left: 5px;">Virtual Video Interview started</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; padding-top:0px; overflow-y: scroll;">
			<div id="interviewQuesOrangeBody"></div>
	</div>
 	<div class="modal-footer">
	 	<table width=470 border=0>
	 		<tr>
		 		<td width=200  nowrap>
		 	<!-- <button class="btn btn-large" aria-hidden="true" onclick='printQualificationQuestion();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp; -->				
		 	
				
				<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick=''>OK</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
   </div>
  </div>
 </div>
 
 <!--  Display Interview Question Div For Blue-->
 <div  class="modal hide" id="videoInterViewLinkDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog" style="width: 750px; " >
<div class="modal-content">		
<div class="modal-header">
 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="">x</button>
	<h3 id="myModalLabel">Virtual Video Interview completed</h3>
</div>
<div class="modal-body">		
<div class="control-group">
		<div id="interViewDiv"></div>
</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary" id="saveVVIId" type="button" onclick="saveInterviewValue();">Save <i class="icon"></i></button>
	<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="">Cancel</button> 
</div>
</div>
</div>
</div>
 
<!--  Display Interview Question Div For Red-->
 <div class="modal hide" id="interviewQuesRed" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px; ">
	<div class="modal-content">
	<div class="modal-header" id="interviewQuesRedeader">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick=''>x</button>
		<h3 style="padding-left: 5px;">Invite for Virtual Video Interview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; padding-top:0px; overflow-y: scroll;">
			<div id="interviewQuesRedBody"></div>
	</div>
 	<div class="modal-footer">
	 	<table width=470 border=0>
	 		<tr>
		 		<td width=200  nowrap>
		 	<!-- <button class="btn btn-large" aria-hidden="true" onclick='printQualificationQuestion();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp; -->				
		 	
				<span id="inviteOK" >
				<button class="btn  btn-large btn-primary" onclick='saveIIStatus();'>OK <i class="iconlock"></i></button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick=''>Cancel</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
   </div>
  </div>
 </div>

<!--  Display Interview Question Div For Gray-->
 <div class="modal hide" id="interviewQuesGrey" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px; ">
	<div class="modal-content">
	<div class="modal-header" id="interviewQuesGreyHeader">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick=''>x</button>
		<h3 style="padding-left: 5px;">Reinvite for Virtual Video Interview</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; padding-top:0px; overflow-y: scroll;">
			<div id="interviewQuesGreyBody"></div>
	</div>
 	<div class="modal-footer">
	 	<table width=470 border=0>
	 		<tr>
		 		<td width=200  nowrap>
		 	<!-- <button class="btn btn-large" aria-hidden="true" onclick='printQualificationQuestion();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp; -->				
		 	
				<span id="reInviteOK" >
				<button class="btn  btn-large btn-primary" onclick='saveIIStatusForGrey();'>OK <i class="iconlock"></i></button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick=''>Cancel</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
   </div>
  </div>
 </div>

<!--  Display Interview Question Div For Orange-->
 <div class="modal hide" id="interviewQuesOrange" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px; ">
	<div class="modal-content">
	<div class="modal-header" id="interviewQuesOrangeHeader">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick=''>x</button>
		<h3 style="padding-left: 5px;">Virtual Video Interview started</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; padding-top:0px; overflow-y: scroll;">
			<div id="interviewQuesOrangeBody"></div>
	</div>
 	<div class="modal-footer">
	 	<table width=470 border=0>
	 		<tr>
		 		<td width=200  nowrap>
		 	<!-- <button class="btn btn-large" aria-hidden="true" onclick='printQualificationQuestion();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp; -->				
		 	
				
				<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick=''>OK</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
   </div>
  </div>
 </div>
 
<%-- 
 <div  class="modal hide" id="videoInterViewLinkDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog" >
<div class="modal-content">		
<div class="modal-header">
 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="">x</button>
	<h3 id="myModalLabel">Virtual Video Interview completed</h3>
</div>
<div class="modal-body">		
<div class="control-group">
		<div class="row">
					  <div class="col-sm-12 col-md-12  top10 mt15">
					  	Video
					  	<iframe src="" id="ifrmVideoIIurl" width="100%" height="200px"></iframe> 
					  </div>	
		</div>		
		<div class="row">
				      <div class="col-sm-6 col-md-6  top10">
				       	<iframe id="ifrmForVideoII"  src="slideract.do?name=normScoreFrm&tickInterval=10&max=100&swidth=230&svalue=0" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;margin-top:-11px;"></iframe>
				       </div>
		</div>
</div>
</div>
<div class="modal-footer">
	<button class="btn btn-primary " type="button" onclick="saveInterviewValue();">Save <i class="icon"></i></button>
	<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="">Cancel</button> 
</div>
</div>
</div>
</div>
 
 --%>
 
<!--  Interview  Questions --> 
 
<div class="modal hide" id="offerReadyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog"  style="width: 520px;padding-top:200px; ">
	<div class="modal-content">
	<div class="modal-header" id="offerReadyDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='offerReadyCheck();'>x</button>
		<h3 id="myModalLabel"><span class="left7" id="offer_Ready_title">Offer Ready</span></h3>
	</div>
	<div class="modal-body" style="max-height:370px;overflow-y:auto">
		<div id="offerReadyDiv"  data-spy="scroll" data-offset="50" >
		</div>	
	</div>
	<div class="modal-footer">
       <div  class="pull-right">
      		<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='offerReadyCheck();'>&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
         </div>
     </div>
</div>
	</div>
</div>

<div class="modal hide" id="offerAcceptedModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:720px;">
	<div class="modal-content">
	<div class="modal-header" id="printDataProfileDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='cancelOfferAccepted();'>x</button>
		<h3>Offer Accepted</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div id="offerAcceptDiv"  data-spy="scroll" data-offset="50" >
			Candidate has rejected the conditional offer of employment for this job. Do you still want to continue?
		</div>	
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:500px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='offerAcceptedOK();'>&nbsp;&nbsp;&nbsp;OK&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td   nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='cancelOfferAccepted();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
</div>
	</div>
</div>

<div class="modal hide" id="updateStatusModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:720px;">
	<div class="modal-content">
	<div class="modal-header" id="printDataProfileDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='cancelOfferAccepted();'>x</button>
		<h3>Status</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div id="updateStatusDiv"  data-spy="scroll" data-offset="50" >
			If you set status of this job, it may update the same status of other jobs of this district. We may also have some jobs in which this status has already been set.</br>
			1. Click on button "Overwrite", if you want to overwrite the Score (if exists) of the same status set by you for other jobs (or)</br>
			2. Click on button "Skip", if you do not want to Overwrite the Score of the jobs for which the same status has already been set by you (or)</br>
			3. Click on button "Cancel", if you do not want to set the status and want to stop this process.
		</div>	
	</div>
	<div class="modal-footer">
       <div  class="pull-right">
      		<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='updateStatusOverrideSkip(1);'>&nbsp;&nbsp;&nbsp;Overwrite&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
       	    <button class="btn  btn-large btn-primary" aria-hidden="true" onclick='updateStatusOverrideSkip(0);'>&nbsp;&nbsp;&nbsp;Skip&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
       	    <button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='cancelUpdateStatus();'>Cancel</button>
         </div>
     </div>
</div>
	</div>
</div>


<div class="modal hide" id="outerStatusModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:720px;">
	<div class="modal-content">
	<div class="modal-header" id="printDataProfileDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='outerStatusClose();'>x</button>
		<h3 id="myModalLabel"><span class="left7" id="outer_status_l_title">Status</span></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div id="OuterStatusDiv"  data-spy="scroll" data-offset="50" >
			At this time, your location does not have access to hire from ${recentJobTitle}. Feel free to enter notes on candidates; however, please be aware that status nodes will not turn green. Once you have authorization to hire from this pool, please return and click "Finalize" to complete the process and change the color of the status node (no additional notes will be required at that time).
		</div>	
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:590px;">
	 		<tr>
	 			<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='outerStatusClose();'>&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
	 		</tr>
	 	</table>
	</div>
</div>
	</div>
</div>


<!-- For all data Print -->
<div class="modal hide" id="printDataProfile"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 925px;">
	<div class="modal-content">
	<div class="modal-header" id="printDataProfileDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='canelPrintData();'>x</button>
		<h3>Print Preview</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div id="printDataProfileDiv"  data-spy="scroll" data-offset="50" >
		
		</div>	
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:690px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" id="printButton" onclick='printAllData();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td   nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='canelPrintData();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
	</div>
	</div>
</div>

<!--  Div for Print Selected Data -->
<div class="modal hide" id="selectedprintData"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 550px;">
	<div class="modal-content">
	<div class="modal-header" id="">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='cancelMultiPrint();'>x</button>
		<h3>Select Options</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div class="divErrorMsg" id="errForPrint"></div>
		<div id="selectedprintDataBodyDiv"  data-spy="scroll" data-offset="50" style="padding-bottom: 0px; margin-bottom: -10px;" >
			<label class="checkbox inline">
 				<input type="checkbox" name="cqp" id="chkcl" value="chkcl"/>
 				Cover Letter
 			</label>
 			<label class="checkbox inline">
 				<input type="checkbox" name="cqp" id="chkqq" value="chkqq"/>
 				Qualification Questionnaire
 			</label>
 			
 	<!-- mukesh -->	
 			<label class="checkbox inline jsiView hide">
 				<input type="checkbox" name="cqp" id="chkjsi" value="chkjsi"/>
 				JSI
 			</label>
 			
 			<label class="checkbox inline">
 				<input type="checkbox" name="cqp" id="chkportfolio" value="chkportfolio" onclick="chkportfolioOPtions()"/>
 				Portfolio Item
 			</label>
		</div>	
		<!-- mukesh -->
		<div  class="hide" id="portfolioItmDiv" style="padding-left: 25px;padding-top: 0px; width: 100%;" >
		 <table>
		    <tr>
			    <td width="33%">
				 <label class="checkbox-inline">
			       <input type="checkbox" name="chkportfolioItem" id="pinfo" value="pinfo">
					 Personal Information
			     </label>
			   </td>
			   <td width="33%">
				<label class="checkbox-inline">
			      <input type="checkbox" name="chkportfolioItem" id="academics" value="academics">
			       Academics
			    </label>
			   </td>
			   <td width="33%">
			     <label class="checkbox-inline">
	               <input type="checkbox" name="chkportfolioItem" id="certification" value="certification">
	                 Certification(s)/Licensure(s)
	  		     </label>
	  		    </td>
  		    </tr>
  		  
  		    <tr>
	  		   <td width="33%">
	  		     <label class="checkbox-inline">
	               <input type="checkbox" name="chkportfolioItem" id="references" value="references">
	                 References
	  		     </label>
	  		   </td>
	  		   <td width="33%">
	  		     <label class="checkbox-inline">
	               <input type="checkbox" name="chkportfolioItem" id="experience" value="experience">
	                 Work Experience
	  		     </label>
	  		  </td>
	  		  <td width="33%">
				<label class="checkbox-inline">
			      <input type="checkbox" name="chkportfolioItem" id="resume" value="resume">
			       Resume
			    </label>
			   </td>
		   </tr>
		 
		   <tr>
			 <td width="33%">
	  		   <label class="checkbox-inline">
	             <input type="checkbox" name="chkportfolioItem" id="involvement" value="involvement">
	                Involvement
	  		   </label>
	  		  </td>
	  		 <td width="33%">
	  		  <label class="checkbox-inline">
	            <input type="checkbox" name="chkportfolioItem" id="honors" value="honors">
	                Honors
	  		  </label>
		     </td>
		     <td width="33%">
		      
				<label class="checkbox-inline">
			      <input type="checkbox" name="chkportfolioItem" id="dspqQues" value="dspqQues">
			       District Specific Questions
			    </label> 
			 </td> 
		   </tr>
  		 </table> 
		</div>
		
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:270px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='printMultipleData();'>Print Preview</button>&nbsp;&nbsp;
				</td>
				<td   nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='cancelMultiPrint();'>Cancel</button>
				</td>
	 		</tr>
	 	</table>
	</div>
	</div>
	</div>
</div>

<!--@End
	@Ashish :: Add Qualification Div -->
	


	
<div class="modal hide" id="jWTeacherStatusNotesDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='jWTeacherStatusNotesOnCloseForSLC()'>x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title">Status</span></h3>
		<input type="hidden" id="statusName" name="statusName" value="">
		<input type="hidden" id="teacherIDSp" name="teacherIDSp" value="">
		<input type="hidden" id="statusShortName" name="statusShortName" value="">
		<input type="hidden" id="isEmailTemplateChanged" name="isEmailTemplateChanged" value="0">
		<input type="hidden" id="isEmailTemplateChangedTeacher" name="isEmailTemplateChangedTeacher" value="0">
		
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;">
		<div class="control-group">
			<input type="hidden" name="scoreProvided" id="scoreProvided" />
			<div class='divErrorMsg left2' id='errorStatusNote' style="display: block;"></div>
			<div class='divErrorMsg left2' id='errorStatusNoteRef' style="display: block;"></div>
			<div class="row">
			  <div class="control-group span16">                   
		 			<div id='hiredDateDiv' style="padding-left:15px; padding-top: 10px; display: none;">
		 			<label><strong><span id="hiredtext">Hired Date</span></strong></label><BR/>
						<input type="text" id="setHiredDate" name="setHiredDate" value="${getDateWithoutTime}" maxlength="0" class="help-inline form-control" style="width: 250px;">
		 			</div>
			  </div>
			</div>
			<div id="divStatusNoteGrid" style='width:700px;overflow:hidden;' ></div>
			
			<div id="noteMainDiv" class="hide">
				<%---------- Gourav :Status wise Dynamic Template [Start]  --%>
			<div id="templateDivStatus" style="display: none;">		     	
		    	
		    	
		    	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-8" style="max-width:300px;padding-top:10px;">		    	
			    	<label><strong>Template</strong></label>
		        	<br/>
		        	<select class="form-control" id="statuswisesection" onchange="getsectionwiselist(this.value)">
		        		<option value="0">Select Template</option>
		        	</select>
		         </div>	
			
		    	</div>
		    	
		    	
		    	<div class="row col-md-12" id="showTemplateslist" style="display:none">		    	
		    	<div class="row col-sm-12" style="padding-top:10px;">
		    	<label><strong>Sub Template</strong></label>
		        	<br/>
		        	<span id="addTemplateslist"></span>
		        	
		         </div>		        	
					<button class="btn btn-primary hide top26" id="changeTemplateBody" onclick="getTemplateByLIst()">Apply Template</button>
					<input type="hidden" id="templateId" value=""/>
			</div>
		    </div>
		<%---------- Gourav :Status wise Dynamic Template [END]  --%>
				<div class="row mt5" >
				    	<div class="left16" id='statusNotes' >
				    	<label class="">Note<c:if test="${statusNotes eq true}"><span class="required">*</span></c:if>
				    	</label><br/>
				    		<textarea readonly id="statusNotes" name="statusNotes" class="span6" rows="2"   ></textarea>
				    	</div> 
				</div>  
				<iframe id='uploadStatusFileFrameID' name='frmStatusFileFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form id='frmStatusNote' enctype='multipart/form-data' method='post' target='frmStatusFileFrame' onsubmit="saveStatusNote(0);"  class="form-inline" action='statusNoteUploadServlet.do' accept-charset="UTF-8">
				<input type="hidden" id="statusTeacherId" name="statusTeacherId" value=""/>
				<div style="clear: both"></div>
				<div class="row mt5">  	
						<div class="span4 left16" >
						<div style="clear: both"></div>
				    		<div id='statusNoteFileNames' style="padding-top:12px;">
			        			 <a href='javascript:void(0);' onclick='addStatusNoteFileType();'><img src='images/attach.png'/></a>
			        			 <a href='javascript:void(0);' onclick='addStatusNoteFileType();'>Attach a File</a>
			        		</div> 
				    	</div>
				    	<div class="span4" id="showStatusNoteFile"  style="padding-top:4px;">&nbsp;
			        	</div>   
				</div>
				</form>
				<!--<div class="row mt10">  
						<div class="span10" >
				    		<table>
				    		<tr><td><input type="checkbox" name="emailSentToD"  id="emailSentToD"value="2"></td><td>Sent an email to all the District Admin&nbsp;&nbsp;</td> 
				    		<td><input type="checkbox" name="emailSentToS"  id="emailSentToS" value="3"></td><td>Sent an email to all the School Admin</td></tr> 
				    		</table>
				    	</div> 
				</div>
				-->
				
		 	</div> <!-- Main Div -->
		 	
		 	<div class="row" id="schoolAutoSuggestDivId" style="display: none;">
            	<div class="col-sm-10 col-md-10">
            		<c:if test="${entityType eq 2}">
					  	<label>School Name<span class="required">*</span></label>
		 				<input type="text" id="schoolNameCG" class="form-control" maxlength="100" name="schoolNameCG" style="width: 670px;" placeholder="" onfocus="getSchoolAuto_Hired(this, event, 'divTxtShowData2CG', 'schoolNameCG','schoolIdCG','');"
							onkeyup="getSchoolAuto_Hired(this, event, 'divTxtShowData2CG', 'schoolNameCG','schoolIdCG','');"
							onblur="hideSchoolMasterDiv_djo(this,'schoolIdCG','divTxtShowData2CG'),requisitionNumbers();"	/>
						 <div id='divTxtShowData2CG' style=' display:none;' onmouseover="mouseOverChk_djo('divTxtShowData2CG','schoolNameCG')" class='result' ></div>
				 	</c:if>
				 		<input type="hidden" id="schoolIdCG"/>
				 	
		    	</div>
            </div>
            
            <div class="row" id="requisitionNumbersGrid"  style="display: none;">
            		<c:if test="${entityType ne 1}">
            			<label style='padding-top:10px;'>Position #/Requisition #<span class="required"  id='reqstar' sytle='display:none;'>*</span> (It is used if we are hiring a Candidate. In case of "In-Progress", Position #/Requisition # will not be saved)</label>
            			<c:choose>
				        	<c:when test="${DistrictId==3628590}">
				        		<label class="radio inline" style="margin-top: -5px;">
								<input type="radio" name="rdReqSel"  id="rdReqPre" value="1" checked>Select Position #/Requisition #
								</label>
								<div class="left16">
 							 		<span id="requisitionNumbers"> </span>
 							 	</div>
 							 	<div class="row mt10">
					            	<div class="left16">
					            		<label class="radio inline">
									 	<input type="radio" name="rdReqSel"  id="rdReqPost" value="2" >Enter New Position #/Requisition #
									 	<input  type="text" id="txtNewReqNo" name="txtNewReqNo" class="form-control" maxlength="50" style="width: 350px;" />
					                 	</label>
							    	</div>
					            </div>
				           	</c:when>
				           	<c:when test="${DistrictId ne 3628590}">
 							 	<span id="requisitionNumbers"> </span>
				           	</c:when>
				       </c:choose>
 					</c:if>
            </div>
            
            <!-- Strength & Opportunity Div -->
            <div id="strengthOppDiv" style="display: none;"></div>	
            
            <span id="spanOverride" style='display:none;'>            
        			<label class='checkbox inline'>
        				<input type='checkbox' name='chkOverride' id='chkOverride' value='1'/>
        				Override the Status
        			</label>			    
            </span>
            
            <!-- Shadab Ansari Templates For Teacher -->
            <div class="row hide" id="finelizeSpecificDiv" style="width: 670px;align-items: center;margin: auto;">
				<div class="control-group">
					<div class='divErrorMsg' id='finelizeSpecificErrorDiv'></div>
				</div>
				
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong>Template</strong></label>
					    	<select id="finelizeSpecificTemplatesName" class="form-control"> </select>
					    	<input type="hidden" id="finelizeSpecificTemplatesId" name="finelizeSpecificTemplatesId">
					    </div>
					</div>
					
					<div class="control-group">
						<div class="">
					    	<label><strong>Subject</strong><span class="required">*</span></label>
				        	<input  type="text"  id="finelizeSpecificSubjectLine" name="finelizeSpecificSubjectLine" class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="finelizeSpecificMessage">
					    	<label><strong>Message</strong><span class="required">*</span></label>
				        	<textarea rows="5" class="form-control" cols="" id="finelizeSpecificMessage322" name="finelizeSpecificMessage322" style="margin-top: 0px;"></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<!-- Shadab Ansari Templates For DA/SA -->
		 	<div class="row hide" id="finelizeSpecificDivForDaSa" style="width: 670px;align-items: center;margin: auto;">
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong>Template</strong></label>
					    	<select id="finelizeSpecificTemplatesNameForDaSa" class="form-control"> </select>
					    	<input type="hidden" id="finelizeSpecificTemplatesIdForDaSa" name="finelizeSpecificTemplatesIdForDaSa">
					    </div>
					</div>
					
					<div class="control-group">
						<div class="">
					    	<label><strong>Subject</strong><span class="required">*</span></label>
				        	<input  type="text"  id="finelizeSpecificSubjectLineForDaSa" name="finelizeSpecificSubjectLineForDaSa"class="form-control" maxlength="250" />
				        </div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="finelizeSpecificMessageForDaSa">
					    	<label><strong>Message</strong><span class="required">*</span></label>
				        	<textarea rows="5" class="form-control" cols="" id="" name="" style="margin-top: 0px;"></textarea>
				        </div>
					</div>
		 		</div>
		 		<script>
		 			$(document).ready(function(){
		 				$("#finelizeSpecificTemplatesName").on('change', function() {
		 					getFinalizeSpecificTemplateList(this.value,0);
  						});
  						
  						$("#finelizeSpecificTemplatesNameForDaSa").on('change', function() {
  							getFinalizeSpecificTemplateList(this.value,1);
  						});
					});
		 		</script>
		 	</div>
		 	
		 	<div class="row" id="spSelectDiv">
				<div class="col-sm-12 top5" style="margin-left:-11px;">
					<label class="">Waived Reason<span class="required">*</span></label><br/>
					<select id="waivedReasonNote" name="waivedReasonNote" class="form-control" style="width: 210px;">
				      <option value="0">Select Reasons</option>
					 <c:if test="${not empty reasonsForWaivedList}">
					<c:forEach var="reasonsForWaived" items="${reasonsForWaivedList}" varStatus="reason">
					      <option value="${reasonsForWaived.reasonId}"><c:out value="${reasonsForWaived.reasonName}"/></option>
						</c:forEach>
				     </c:if>
				 </select>
			    </div>     	
			</div>
		 	
		 	
		 	<span id="checkOptions" style='display:none;'>
	            <c:set var="disabledChecked" value=""></c:set>
	            <c:set var="hideView" value=""> </c:set>
			 	<c:if test="${entityType eq 1}">
			 		<c:set var="disabledChecked" value="disabled=\"disabled\""> </c:set>
			 		<c:set var="hideView" value="hide"> </c:set>
			 	</c:if>
			 	<div class="row mt10" id="email_da_sa_div">
	            	<div class="left16">
	            		<c:choose>
	            			<c:when test="${DistrictId == '1200390'}">
	            				<label style="display: none;">
								 	<input type="checkbox" name="email_da_sa" ${disabledChecked} id="email_da_sa"  value="1"/>
				                 	Notify all the associated District and School Admins of the status change	                 	
				                 </label>
				                 <a href='javascript:void(0)' class="${hideView}" style="padding-left: 540px;" onclick="getStatusWiseEmailForAdmin()">View/Edit Message</a>
	            			</c:when>
	            			<c:otherwise>
								<c:if test="${userMaster.entityType eq 5 || userMaster.entityType eq 6}">
								<label>
								 	<input type="hidden" name="email_da_sa" ${disabledChecked} id="email_da_sa"  value="1" /></label>
	            			</c:if>
	            			
	            			<c:if test="${userMaster.entityType ne 5 && userMaster.entityType ne 6}">
	            			<label>
							 		<input type="checkbox" name="email_da_sa" ${disabledChecked} id="email_da_sa"  value="1"/>
			                 		Notify all the associated District and School Admins of the status change	                 	
			                 	</label>
			                 	 <a href='javascript:void(0)' class="${hideView}" style="padding-left: 540px;" onclick="getStatusWiseEmailForAdmin()">View/Edit Message</a>
	            			</c:if>
	            			</c:otherwise>
	            		  </c:choose>	
	            		  <!--</br>
	            		  <c:if test="${DistrictId == '7800040'}">
	            				<label id="email_candi" style="display: none;">
								 	<input type="checkbox" name="email_to_candi" ${disabledChecked} id="email_to_candi"  value="1"/>
				                 	Send email to candidate	                 	
				                 </label>
				                 <a href='javascript:void(0)' class="${hideView}" style="padding-left: 540px;" onclick="getStatusWiseEmailForAdmin()">View/Edit Message</a>
	            			</c:if>  
			    	--></div>
	            </div>
	            
	           <!-- <div class="row left1" id="email_ca_div">
	            	<div>
	            		<label class="checkbox inline">
					 	<input type="checkbox" name="email_ca" ${disabledChecked} id="email_ca"  value="1"/>
	                 	Notify the candidate of the status change
	                 	<a href='javascript:void(0)' class="${hideView}" style="padding-left: 248px;" onclick="getStatusWiseEmailForTeacher()">View/Edit Message</a>
	                 	</label>
	                	
			    	</div>
	            </div>-->
            </span>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=655 border=0>
	 		<tr class="left15">
		 		<td width=118 nowrap align=left>
		 		<span id="statusSave" style='display:none;'><button class="btn  btn-large btn-orange"  onclick='saveStatusNote(0)'>In-Progress</button>&nbsp;</span>
		 		</td>
		 		<td width=122  nowrap align=left>
			 		<span id="unHire" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(2)'>UnHire </button></span>
			 		<span id="unDecline" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(3)'>UnDecline </button></span>
			 		<span id="unRemove" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(4)'>UnReject </button></span>
			 		<span id="unWithdraw" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(8)'>UnWithdraw </button></span>
		 		</td>
		 		<td width=450  nowrap>
		 		<span id="undo" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(5)'>
		 		${undo}
		 		</button>&nbsp;&nbsp;</span>
		 		<span id="resend" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(6)'>Resend</button>&nbsp;&nbsp;</span>
		 		<span id="waived" style='display:none;'><button class="btn  btn-large btn-primary-blue" onclick='saveStatusNote(7)'><spring:message code="lblWaived"/></button>&nbsp;&nbsp;</span>
		 		<input type="hidden" id="statusNotesForStatus" name="statusNotesForStatus" value="${statusNotes}"/>
				<span id="statusFinalize" style='display:none;'><button class="btn  btn-large btn-primary" onclick='saveStatusNote(1)'>Finalize <i class="iconlock"></i></button>&nbsp;&nbsp;</span>
		 		<button id="cancelButtonRefChk" class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='jWTeacherStatusNotesOnCloseForSLC()'>Cancel</button>
		 		<span id="ShowCancelButton"></span>
		 		<input type="hidden" id="referenceValue" value=""/>
		 		</td>
	 		</tr>
	 	</table> 
    </div>
  </div>
 </div>
</div>
<!-- edit message -->
<div class="modal hide" id="myModalEmail" >
	<div class="modal-dialog-for-cgmessage">
        <div class="modal-content">	
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='sendOriginalEmail()'>x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmail'></div>
				</div>
				
			
				
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong>Subject</strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLine" name="subjectLine"class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBody">
					    	<label><strong>Message</strong><span class="required">*</span></label>
				        	<textarea rows="5" class="form-control" cols="" id="" name="" style="margin-top: 0px;"></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn"  onclick='sendOriginalEmail()'>Cancel</button>
		 		<button class="btn btn-primary"  onclick='return sendChangedEmail();' >Ok</button>
		 	</div>
	      </div>
		</div>
	</div>
	

	<div class="modal hide" id="myModalEmailTeacher" >
		<div class="modal-dialog-for-cgmessage">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='sendOriginalEmailTeacher()'>x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmailTeacher'></div>
				</div>
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong>Subject</strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLineTeacher" name="subjectLineTeacher" class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBodyTeacher">
					    	<label><strong>Message</strong><span class="required">*</span></label>
				        	<textarea rows="5" class="form-control" cols="" id="" name=""></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn"  onclick='sendOriginalEmailTeacher()'>Cancel</button>
		 		<button class="btn btn-primary"  onclick='return sendChangedEmailTeacher();' >Ok</button>
		 	</div>
	    </div>
	</div>
</div>


<div class="modal hide" id="myModalReferenceCheck" >
<input type="hidden" name="teacherDetailsForReference" id="teacherDetailsForReference" value="" />
<input type="hidden" name="teacherIdForReference" id="teacherIdForReference" value="" />
		<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(1)'>x</button>
				<h3 id="statusTeacherTitle"></h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
			<div  class="modal-body">
				<div class="" id="referenceRecord">

				</div>
		 	</div>

		 	<div class="modal-footer">
		 		<span id="statusPrintRefChk">&nbsp;&nbsp;</span>
		 		<span id="statusSentRefChk">&nbsp;&nbsp;</span>
		 		<button class="btn"  onclick='closeReferenceCheck(1)' style="height: 34px;">Cancel</button>
		 	</div>
	    </div>
	</div>
</div>

<div class="modal hide" id="myModalReferenceCheckFeedBack">
<input type="hidden" name="teacherDetailsCG" id="teacherDetailsCG" value=""/>
<input type="hidden" name="candidateFullName" id="candidateFullName" value=""/>
<input type="hidden" name="teacherIdCG" id="teacherIdCG" value=""/>
		<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(2)'>x</button>
				<h3 id="statusTeacherTitleReferenceChk"></h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
			<div  class="modal-body">
				<div class="" id="referenceFeedBack">

				</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<span id="statusFinalize" style="display: inline;">
		 		<span id="buttonFinalizeScore">
		 		<button class="btn  btn-large btn-primary" onclick="saveMaxScore('CG')">Finalize <i class="iconlock"></i></button>&nbsp;&nbsp;</span>
		 		</span>
		 		<button class="btn"  onclick='closeReferenceCheck(2)'>Cancel</button>
		 	</div>
	    </div>
	</div>
</div>

<div class="modal hide" id="myModalReferenceCheckFeedBackSLC">
	<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(3)'>x</button>
				<h3 id="statusTeacherTitleReferenceChkSLC"></h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
			<div  class="modal-body">
				<div class="" id="referenceFeedBackSLC">

				</div>
		 	</div>

		 	<div class="modal-footer">
		 		<span id="buttonFinalizeScoreSLC">
		 		<button class="btn  btn-large btn-primary" onclick="saveMaxScoreSLC('SLC')">Finalize <i class="iconlock"></i></button>&nbsp;&nbsp;</span>
		 		</span>
		 		<button class="btn"  onclick='closeReferenceCheck(3)'>Cancel</button>
		 	</div>
	    </div>
	</div>
</div>

<div class="modal hide" id="myModalReferenceCheckConfirm" >
	<div class="modal-dialog" style="width:728px;">
        <div class="modal-content">		
			<div class="modal-header">
				<div id="hideConfirmCross">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(4)'>x</button>
				</div>
				<div id="showConfirmCross" style="display: none;">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(5)'>x</button>
				</div>
				<h3 id="statusTeacherTitleForConfirmation"></h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
			<div  class="modal-body">
				<div class="" id="referenceRecord">
					Score for question is set to be zero<BR/>
				</div>
		 	</div>

		 	<div class="modal-footer" id="hideConfirmButtons">
		 		<button class="btn btn-large btn-primary" type="button" onclick="saveMaxScore('auto');"><strong>Continue <i class="icon"></i></strong></button>
		 		<button type="button" class="btn"  onclick='closeReferenceCheck(4)'>Cancel</button>
		 	</div>
		 	<div class="modal-footer" id="showConfirmButtons" style="display: none;">
		 		<button class="btn btn-large btn-primary" type="button" onclick="saveMaxScoreSLC('auto');"><strong>Continue <i class="icon"></i></strong></button>
		 		<button type="button" class="btn"  onclick='closeReferenceCheck(5)'>Cancel</button>
		 	</div>
	    </div>
	</div>
</div>

<div class="modal hide"  id="modalUpdateStatusNoteMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeStatusNoteMsg(0);'>x</button>
		<h3 id="myModalLabel">Status</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="updateStatusNoteMsg">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeUpdateStatusNoteMsg(1);'>Ok</button> 
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeUpdateStatusNoteMsg(0);'>Cancel</button> 		
 	</div>
   </div>
  </div>
 </div>
 
<div class="modal hide"  id="modalStatusNoteMsg"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeStatusNoteMsg(0);'>x</button>
		<h3 id="myModalLabel">Status</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="statusNoteMsg">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeStatusNoteMsg(1);'>Ok</button> 
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='closeStatusNoteMsg(0);'>Cancel</button> 		
 	</div>
</div>
 	</div>
</div>
 
<div  class="modal hide"  id="myModalProfile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideProfile()">x</button>
		<h3 id="myModalLabel">Profile</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divProfile" class="">
		    		        	<br/>
		    		        	<br/>
		    		        	<br/>
		    		        	<br/>
		    		        	<br/>
		    		        	<br/>
		    		        	<br/>&nbsp;
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideProfile()"">Close</button>	
 	</div>
</div>
</div>
</div>



<div  class="modal hide"  id="myModalTag" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideTag()">x</button>
		<h3 id="myModalLabel">Tag</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divTag" class="">
		    		        	<br/>
		    		        	<br/>
		    		        	<br/>
		    		        
		    		        	<br/>&nbsp;
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideTag()">Close</button>		
 	</div>
</div>
</div>
</div>


<!--<div  class="modal1 hide" style='width: 550px;height: 475px;top: 360px;' id="myModalNorm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
-->
<div  class="modal hide" id="myModalNorm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
   <div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideNorm()">x</button>
		<h3 id="myModalLabel">Norm Distribution</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divNorm" class="" align="center">
		    		        	<br/>
		    		        	<br/>
		    		        	<br/>
		    		        
		    		        	<br/>&nbsp;
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideNorm()">Close</button>		
 	</div>  	
</div>
</div>
</div>

<div  class="modal hide"  id="myModalStatus"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px;">
	<div class="modal-content">		
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideStatus()">x</button>
		<h3 id="myModalLabel"><span class="left7" id="status_l_title">Status</span></h3>
	</div>
	
   <img alt="" class="img-responsive" src="images/headstatus.png">
	<div class="modal-body-cgstatus">		
		<div class="control-group">
			<div id="divStatus">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideStatus()">Done</button>	
 	</div>
</div>
</div>
</div>

<div class="modal hide"  id="saveToFolderDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog" style="width:900px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Save Candidate</h3>
	</div>
	<div class="modal-body" style="max-height: 500px;overflow-y: scroll;"> 		
		<div class="control-group">
		<div  style="border: 0px solid green;width:220px;height:450px;float: left;" class="span5">	
		<div class="span5" id="tree_menu" style=" padding: 8px 0px 10px 2px; " >
			<a data-original-title='Create' rel='tooltip' id='createIcon'><span id="btnAddCode" style="cursor: pointer;" class='icon-folder-open  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Rename' rel='tooltip' id='renameIcon'><span id="renameFolder" class='icon-edit  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Cut' rel='tooltip' id='cutIcon'><span id="cutFolder" class='icon-cut  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Copy' rel='tooltip' id='copyIcon'><span id="copyFolder" class='icon-copy  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Paste' rel='tooltip' id='pasteIcon'><span id="pasteFolder" class='icon-paste  icon-large iconcolor'></span></a>&nbsp;&nbsp;
			<a data-original-title='Delete' rel='tooltip' id='deleteIcon'><span id="deletFolder" class='icon-remove-sign  icon-large iconcolor'></span></a>
		</div>

		
		<span id="treeiFrame">
		<!-- 
		<iframe id="iframeSaveCandidate"  src="tree.do?pageFlag=0" class="pull-left" scrolling="auto"  frameBorder="1" style="border: 0px; padding: 0px; margin: 0px; overflow:visible; text-align: top; vertical-align: top;width:220px;height:420px;"></iframe>
		 -->
		 </span>
	    </div>
	<%-- ======================================== Right Div ========================================================================= --%>			
		<div class="span10" style="padding-top:39px; border: 0px solid green;float: left;">
				<input type="hidden" id="savecandidatearray" name="savecandidatearray">
				<input type="hidden" id="txtoverrideFolderId" name="txtoverrideFolderId">
				<input type="hidden" id="teachetIdFromPoPUp" name="teachetIdFromPoPUp" value="">
				<input type="hidden" id="txtflagpopover" name="txtflagpopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
				<div id="savedCandidateGrid">
				
				</div>
		</div>	
		<div style="clear: both"></div>
		</div>
		
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="saveCandidateToFolderByUser()" >Save <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>
 	</div>
</div>

<%--- ============================================ Gagan: Share Folder Div ======================================================== --%>

<div class="modal hide"  id="shareDiv" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='defaultSaveDiv()'>x</button>
		<h3 id="myModalLabel">Share Candidate</h3>
		<input type="hidden" id="entityType" name="entityType" value="${entityType}">
		<input type="hidden" id="savecandiadetidarray" name="savecandiadetidarray"/>
		<input type="hidden" id="JFTteachetIdFromSharePoPUp" name="JFTteachetIdFromSharePoPUp">
		<input type="hidden" id="txtflagSharepopover" name="txtflagSharepopover" value=""> <%-- If It is 1 -> that means User Clicked on Pop up --%>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">			
			<div class="">
					<div class="row">
					    <div class="col-md-10">
						<div class='divErrorMsg' id='errorinvalidschooldiv' ></div>
					    </div>
					      <div class="col-sm-4 col-md-4">
					       <label>District Name</label> </br>
				            <c:if test="${DistrictName!=null}">
				             	${DistrictName}
				             	<input type="hidden" id="districtId" value="${DistrictId}"/>
				             	<input type="hidden" id="districtName" value="${DistrictName}" name="districtName"/>
				             </c:if>				            
			              <div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
						
					      </div>
					      
					      
					      
					      <div class="col-sm-6 col-md-6">
					        <label>School Name</label>
				          	<c:if test="${SchoolName==null}">
				           	<input type="text" id="schoolName15" maxlength="100" name="schoolName15" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData15', 'schoolName15','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData15', 'schoolName15','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData15');"	/>
							    <input type="hidden" id="schoolId15" value="0"/>
							</c:if>
							  <c:if test="${SchoolName!=null}">
				             	<%-- ${SchoolName}--%>	
				             	<input type="text" id="schoolName15" maxlength="100" name="schoolName15" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData15', 'schoolName15','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowData15', 'schoolName15','districtId','');"
												onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData15');" value="${SchoolName}"	/>
				             	<input type="hidden" id="schoolId15" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolId" value="${SchoolId}"/>
				             	<input type="hidden" id="loggedInschoolName" value="${SchoolName}"/>
				             </c:if>
							 <div id='divTxtShowData15'  onmouseover="mouseOverChk('divTxtShowData15','schoolName15')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
				        
					      </div>
					      <div class="col-md-2 top25">
					        <label>&nbsp;</label>
					        <input type="button" id="" name="" value="Go" onclick="searchUserthroughPopUp(1)" class="btn  btn-primary" >
					      </div>
  					</div>
				</div>
		
		
			<div id="divShareCandidateToUserGrid" style="border: 0px solid green;" class="mt30">
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				<br/>
				
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="shareCandidatethroughPopUp()" >Share <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='defaultSaveDiv()'>Cancel</button> 		
 	</div>
</div>
</div>
</div>

<div  class="modal hide"  id="shareConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="saveAndShareConfirmDiv" class="">
		    	You have successfully shared the Candidates to the selected Users.     	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
  </div>
 </div>
</div>

<div class="modal hide"  id="deleteShareCandidate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Do you really want to delete the selected candidate?
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteCandidate()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
   </div>
  </div>
</div>


<div class="modal hide"  id="duplicatCandidate" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			One or more Candidate already exists in this folder. Click on "Ok" to save other Candidates or click on "Cancel" to cancel the save process. No Candidate will save if you click on "Cancel".
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="saveWithDuplicateRecord()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
   </div>
  </div>
</div>

<div class="modal hide"  id="deleteFolder" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Teachermatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Do you really want to delete this folder? It will delete all the sub folders attached to this folder.
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteconfirm()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
  </div>
 </div>
</div>
<div  class="modal hide"  id="myModalJobList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 980px;">
	<div class="modal-content">		
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setPageFlag()">x</button>
		<h3 id="myModalLabel">Job Detail</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divJob" class="table-responsive" style=" margin-bottom:15px;min-width:920px;overflow-x:hidden;">
		    		  
			
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setPageFlag()">Close</button> 		
 	</div>
</div>
</div></div>
<div  class="moda hide"  id="myModalDesc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"   onclick="setZIndexJobDiv()" >x</button>
		<h3 id="myModalLabel">Job Description</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="">
		    	<span id="description"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="setZIndexJobDiv()">Close</button> 		
 	</div>
   </div>
  </div>
</div>
<div  class="modal hide pdfDivBorder"  id="modalDownloadsCommon" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-contenat" style="background-color: #ffffff;">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">x</button>
		<h3 id="myModalLabelText"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="" style="height: 480px; width: 100%; overflow: auto;'-webkit-overflow-scrolling' : 'touch">
				 <iframe src="" id="ifrmTransCommon" width="100%" height="100%" scrolling="auto">
				 </iframe>  
				<!--<embed  src="" id="ifrmTrans" width="100%" height="100%" scrolling="no"/> -->     	
			    <!--<object data="" id="ifrmTrans" width="100%" height="100%" scrolling="no">
				    <p>Insert your error message here, if the PDF cannot be displayed.</p>
				    </object>
			    -->	  
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexTrans()">Close</button> 		
 	</div>
</div>
 	</div>
</div>

<div  class="modal hide"  id="myModalTranscript" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Transcript</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divTranscript" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
  </div>
 </div>
</div>



<div  class="modal hide"  id="myModalCertification" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Certification/Licensure</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divCertification" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>

<div  class="modal hide"   id="myModalPhone" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   data-backdrop="static">
	<div class='modal-dialog-for-teachercontented'>
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showCommunicationsForPhone();">x</button>
		<h3 id="myModalLabel">Phone</h3>
	</div>	
	<div class="modal-body">			
			<div id="divPhoneGrid"></div>
			<iframe id='uploadPhoneFrameID' name='uploadPhoneFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
			</iframe>
			<form id='frmPhoneUpload' enctype='multipart/form-data' method='post' target='uploadPhoneFrame'  class="form-inline" onsubmit="return savePhone();" action='phoneUploadServlet.do' accept-charset="UTF-8">
			<div class="mt10">
				<div class="span6 hide" id='calldetrailsdiv'>
					<div class='divErrorMsg' id='errordivPhone' style="display: block;margin-bottom: 5px;"></div>
					<label><strong>Enter Call Detail<span class="required">*</span></strong></label>
			    	<div class="span6" id='divTxtPhone' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea readonly id="divTxtPhone" name="divTxtPhone" class="span6" rows="4"></textarea>
			    	</div> 
			    	<input type="hidden" id="teacherIdForPhone" name="teacherIdForPhone" value="">
		    		<input type="hidden"  name="phoneDateTime" id="phoneDateTime" value="${dateTime}"/>
			    	<div id='filePhones' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addPhoneFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addPhoneFileType();'>Attach a File</a>
		        	</div>   
		    	</div>     	
			</div>
			</form>
			
		
 	</div>
 	<c:set var="chkSavePhone" value="inline"/>
		<c:choose>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSavePhone" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSavePhone" value="none" />
			</c:otherwise>
		</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="showCommunicationsForPhone();">Cancel</button>
 		<button class="btn btn-primary hide" style="display: ${chkSavePhone}" id='calldetrailsbtn' onclick="savePhone()" >Save&nbsp;</button>
 	</div>
	</div> 	
  </div> 	
</div>

<div  class="modal hide"  id="myModalCallDetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Call Detail</h3>
	</div>
	<div class="modal-body">	
		<div class="control-group">
			<div id="divCallDetail"></div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
 	</div>
</div>
</div> 	
</div>
	
<div class="modal hide"  id="divAlert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" style="z-index: 5000;" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="divAlertText" >
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer"> 		
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="setZIndexActOtherDiv()">Close</button></span> 		
 	</div>
</div>
</div> 	
</div>


<div class="modal hide"  id="myConfirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='vcloseBtnk'>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="ddd">Are you sure you want to remove this candidate from this CG view?
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span id="spnRemoveTeacher"><button class="btn btn-large btn-primary" >Ok <i class="icon"></i></button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="setZIndexActDiv()">Close</button></span> 		
 	</div>
</div>
</div> 	
</div>

<div class="modal hide"  id="myModalAct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">Take Action</h3>
	</div>
	<div class="modal-body">
		<div class='divErrorMsg' id='errordivAct' style="display: block;"></div>		
		<div class="control-group">
			<div class="" id="divAct">	
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<button class="btn btn-large btn-primary"  onclick="chkActAction()">Save <i class="icon"></i></button>
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>
</div> 	
</div>

<div class="modal hide"  id="myModalCoverLetter" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"   style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog-for-cgcoverletter">
		<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabel">Cover Letter</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="">
		    	<span id="lblCL"></span>	        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
  </div>
 </div>
</div>
<div  class="modal hide"  id="myModalCommunications" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width: 700px;">
	<div class="modal-content">	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForSave();'>x</button>
		<h3 id="myModalLabel">Communications</h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y: auto">		
		<div class="control-group">
			<div class="" id="divCommTxt">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		 <button class="btn btn-primary" aria-hidden="true" onclick='printCommunicationslogs();'>&nbsp;&nbsp;&nbsp;Print&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"   onclick='showCommunicationsDivForSave();'>Close</button> 		
 	</div>
   </div>
 </div>
</div>
<!--===================demo schedule================================================================================= -->
	<div class="modal hide"  id="myModalDemoSchedule" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog-for-cgdemoschedule" style="min-width:470px;"> 
		<div class="modal-content"> 
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">Demo Schedule</h3>
		</div>      
	      
		      <div class="modal-body" style="max-height:450px;overflow-y:scroll;">
		      		<div class="row">
		      			<div class="col-sm-12 col-md-12">
							<div class='divErrorMsg' id='errordiv' style="display: block;"></div>
						</div>
					</div>
		      		<div class="row">
						 <div class="col-sm-2 col-md-2">
						       <span class=""><label class="">Demo Date<span class="required">*</span></label>
						       <input type="text" id="demoDate" name="demoDate" class="help-inline form-control fontsize11"></span>
						       <input type="hidden" id="demoCreatedDate" name="demoCreatedDate" >
					     </div>
					     <div class="col-sm-2 col-md-2">
						       <span class=""><label class="">Demo Time<span class="required">*</span></label></span>
						       <select id="demoTime" name="demoTime" class="help-inline form-control fontsize11">
							        <option> Select Time</option>
							       	<option value="12:00">12:00</option>
							       	<option value="12:30">12:30</option>
							       	
							       	<option value="01:00">01:00</option>
							       	<option value="01:30">01:30</option>
							       	
							       	<option value="02:00">02:00</option>
							       	<option value="02:30">02:30</option>
							       	
							       	<option value="03:00">03:00</option>
							       	<option value="03:30">03:30</option>
							       	
							       	<option value="04:00">04:00</option>
							       	<option value="04:30">04:30</option>
							       	
							       	<option value="05:00">05:00</option>
							       	<option value="05:30">05:30</option>
							       	
							       	<option value="06:00">06:00</option>
							       	<option value="06:30">06:30</option>
							       	
							       	<option value="07:00">07:00</option>
							       	<option value="07:30">07:30</option>
							       	
							       	<option value="08:00">08:00</option>
							       	<option value="08:30">08:30</option>
							       	
							       	<option value="09:00">09:00</option>
							       	<option value="09:30">09:30</option>
							       	
							       	<option value="10:00">10:00</option>
							       	<option value="10:30">10:30</option>
							       	
							       	<option value="11:00">11:00</option>
							       	<option value="11:30">11:30</option>
							       	
							     </select>
					     </div>
					     <div class="col-sm-3 col-md-3">
					     <span class=""><label class="">AM/PM<span class="required">*</span></label></span>
					        <select id="meridiem" name="meridiem" class="help-inline form-control fontsize11">
					        	<option>Select AM/PM</option>
						       	<option value="AM">AM</option>
						       	<option value="PM">PM</option>
						     </select>
					     </div>
					     <div class="col-sm-4 col-md-4">
					     <span class=""><label class="">Time Zone<span class="required">*</span></label></span>
					       	<select id="timezone" name="timezone" class="help-inline form-control fontsize11">
					       		<option>Select Timezone</option>
						       	<c:forEach items="${timezone}" var="timezone"> 
									<option value="${timezone.timeZoneId}">${timezone.timeZoneShortName}&nbsp;(${timezone.timeZoneName})</option>
								</c:forEach>
						    </select>
					     </div>	
					     <div class="col-sm-1 col-md-1 top5-sm">					      
						    <div id="democlassNote">
						    </div>
						 </div>   				    
				</div>
				
				
				<div class="row top10">
					<div class="col-sm-12 col-md-12">
					       <span class=""><label class="">Location<span class="required">*</span></label>
					       <input type="text" id="location" name="location"  class="form-control input-small"></span>
				     </div>
				</div>
				
				<div class="row top10">
					<div class="col-sm-12 col-md-12">
					       <span class=""><label class="">Description/Additional comment</label>
					       <input type="text" id="disSchCmnt" name="disSchCmnt"  class="form-control input-small"></span>
				     </div>
				</div>
				
				<div class="row top10">
					<div class="col-sm-12 col-md-12">
			     		<a class="pull-right" id="addAttHref" href='javascript:void(0);' onclick="showAttendeeDiv();">+Add Attendee</a>
				     <div id="attendeeDiv" class="hide" ></div>
				     </div>	
				    
				</div>				   
				     <div class="row hide top10" id="demoDistrictdiv">
				    	 <div class="col-sm-12 col-md-12">
					          <label>District Name</label></br>
					          <c:if test="${DistrictName!=null}">
					             	${DistrictName}	
					           </c:if>
						</div>
					</div> 
					<div class="row hide top10" id="demoSchooldiv">
				         <div class="col-sm-12 col-md-12">
					        <label>School Name</label>
				          	<c:if test="${SchoolName==null}">
				          	<input type="hidden" name="demoSchoolId" id="demoSchoolId" value=""/>
				           	<input type="text" id="demoSchoolName" maxlength="100" name="demoSchoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowDataDemo', 'demoSchoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowDataDemo', 'demoSchoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'schoolId','divTxtShowDataDemo'); getUsersBySchool('demo');"	/>
							
							
							</c:if>
							  <c:if test="${SchoolName!=null}">
							  <input type="hidden" name="demoSchoolId" id="demoSchoolId" value="${SchoolId}"/>
				             	${SchoolName}<script>getUsersBySchool('demo');</script>
				             </c:if>
							 <div id='divTxtShowDataDemo'  onmouseover="mouseOverChk('divTxtShowDataDemo','demoSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
				        
					      </div>
				     </div>
				     
				     
				     <div class="row hide top10" id="demoUserdiv">
					     <div class="col-sm-12 col-md-12">
							<span class=""><label class="">Attendee Name<span class="required">*</span></label>
							<input type="hidden" id="teacherId" name="teacherId"/>
							<input type="hidden" id="jobId" name="jobId"/>
							<input type="hidden" id="demoId" name="demoId"/>
							<input type='hidden' id="currDate" value="<fmt:formatDate value='${today}' pattern='MM-dd-yyyy' />">
						     <select id="demoUserId" name="demoUserId" class="form-control help-inline" onchange="addAttendee();">
						     	<option>Select Attendee</option>
						     </select>
						   	 </span>
						</div>
				    </div>
				
				<div class="row top10">
					<div class="col-sm-12 col-md-12" >
						<label class="checkbox inline">
					     <input type="checkbox" id="demostatus">&nbsp;Demonstration was completed.
					     </label>	
					</div>
				</div>
		      </div>  <!-- /.end body -->
		      <div class="modal-footer">
			      <div class="pull-left" style='margin-left:2px;margin-top: 0px;'>
			      		<button type="button" class="btn f1 btn-secondary" id="canEvtBtn" onclick="cancelEvent();">Cancel Event</button>
			      </div>
			      
		     	 <div  class="pull-right">
		       		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 
		        	<button type="button" class="btn btn-primary" id="doneBtn" onclick="saveDemoSchedule(false);">Done</button>
		          </div>
		      </div>
	  </div>
</div>
</div>	  
	  
<!--==================================================================================================== -->

<!-- =======================================Demo notes : Start===================================================================== -->

<div  class="modal hide"  id="myModalDemoNotes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width:670px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showDemoNotesDiv();'>x</button>
		<h3 id="myModalLabel">Demo Notes</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" id="demoNotesTxt">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="$('#myModalDemoSchedule').modal('show');">Close</button> 		
 	</div>
   </div>
  </div>
</div>

<div class="modal hide"  id="demoModalDemoNotes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog" style="width:674px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='showDemoNotesDiv();'>x</button>
		<h3>Notes</h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="demodivNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='demoUploadNoteFrameID' name='demouploadNoteFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='demofrmNoteUpload' enctype='multipart/form-data' method='post' target='demouploadNoteFrame'  class="form-inline" onsubmit="return saveDemoNotes();" action='demoNoteUploadServlet.do' accept-charset="UTF-8">
			
		<div class="mt10">
			<div class='span10 divErrorMsg' id='demoerrordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong>Enter Notes<span class="required">*</span></strong></label>
		    	<input type="hidden"  name="noteDateTime" id="noteDateTime" value="${dateTime}"/>
		    	<input type="hidden"  name="demoNoteId" id="demoNoteId"/>
		    	<div class="span10" id='demodivTxtNode' style="padding-left: 0px; margin-left: 0px; " >
		    		<textarea readonly id="demotxtNotes" name="demotxtNotes" class="span10" rows="4"></textarea>
		    	</div>  
		    	<div id='demofileNotes' style="padding-top:10px;">
	        		<a href='javascript:void(0);' onclick='addDemoNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addDemoNoteFileType();'>Attach a File</a>
	        	</div>      	
			</div>						
		</div>
		</form>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="closeDemoNotes();">Cancel</button>&nbsp; 
 		<span id="spnBtnSave"><button class="btn btn-primary"  onclick="saveDemoNotes()">Save<i class="icon"></i></button>&nbsp;</span>
 	</div>
</div>
 	</div>
</div>

<!-- ===========================================Demo notes : End========================================================================= -->

<!--===================panel schedule================================================================================= -->
	<div class="modal hide"  id="myModalPanel" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog-for-cgdemoschedule" style="min-width:670px;">
		<div class="modal-content">
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">Panel</h3>
		</div>      
	      
		      <div class="modal-body" style="max-height: 450px;overflow-y: auto;">
		      		<div class="row">
		      			<div class="col-sm-8 col-md-8">
							<div class='divErrorMsg' id='panelErrordiv' style="display: block;"></div>
						</div>
					</div>
		      		<div class="row">
						 <div class="col-sm-2 col-md-2">
						       <span class=""><label class="">Panel Date<span class="required">*</span></label>
						       <input type="text" id="panelDate" name="panelDate" class="help-inline form-control fontsize11"></span>
						       <input type="hidden" id="panelCreatedDate" name="panelCreatedDate" >
					     </div>
					     <div class="col-sm-2 col-md-2">
						       <span class=""><label class="">Panel Time<span class="required">*</span></label></span>
						       <select id="panelTime" name="panelTime" class="help-inline form-control fontsize11">
							        <option> Select Time</option>
							       	<option value="12:00">12:00</option>
							       	<option value="12:30">12:30</option>
							       	
							       	<option value="01:00">01:00</option>
							       	<option value="01:30">01:30</option>
							       	
							       	<option value="02:00">02:00</option>
							       	<option value="02:30">02:30</option>
							       	
							       	<option value="03:00">03:00</option>
							       	<option value="03:30">03:30</option>
							       	
							       	<option value="04:00">04:00</option>
							       	<option value="04:30">04:30</option>
							       	
							       	<option value="05:00">05:00</option>
							       	<option value="05:30">05:30</option>
							       	
							       	<option value="06:00">06:00</option>
							       	<option value="06:30">06:30</option>
							       	
							       	<option value="07:00">07:00</option>
							       	<option value="07:30">07:30</option>
							       	
							       	<option value="08:00">08:00</option>
							       	<option value="08:30">08:30</option>
							       	
							       	<option value="09:00">09:00</option>
							       	<option value="09:30">09:30</option>
							       	
							       	<option value="10:00">10:00</option>
							       	<option value="10:30">10:30</option>
							       	
							       	<option value="11:00">11:00</option>
							       	<option value="11:30">11:30</option>
							       	
							     </select>
					     </div>
					     <div class="col-sm-3 col-md-3">
					     <span class=""><label class="">AM/PM<span class="required">*</span></label></span>
					        <select id="panelMeridiem" name="panelMeridiem" class="form-control fontsize11">
					        	<option>Select AM/PM</option>
						       	<option value="AM">AM</option>
						       	<option value="PM">PM</option>
						     </select>
					     </div>
					     <div class="col-sm-5 col-md-5">
					     <span class=""><label class="">Time Zone<span class="required">*</span></label></span>
					       	<select id="panelTimezone" name="panelTimezone" class="help-inline form-control fontsize11">
					       		<option>Select Timezone</option>
						       	<c:forEach items="${timezone}" var="timezone"> 
									<option value="${timezone.timeZoneId}">${timezone.timeZoneShortName}&nbsp;(${timezone.timeZoneName})</option>
								</c:forEach>
						    </select>
					     </div>
					     
					     <div id="panelclassNote">
						 </div>
				</div>
				
				<div class="row">
					<div class="col-sm-12 col-md-12">
					       <span class=""><label class="">Location<span class="required">*</span></label>
					       <input type="text" id="panelLocation" name="panelLocation"  class="form-control input-small"></span>
				     </div>
				</div>
				
				<div class="row">
					<div class="col-sm-12 col-md-12">
					       <span class=""><label class="">Description/Additional comment</label>
					       <input type="text" id="panelDisSchCmnt" name="panelDisSchCmnt"  class="form-control input-small"></span>
				     </div>
				</div>
				<div class="row">
				<div class="col-sm-12 col-md-12">							
						<table>
						<tr>
						<td>								
						 <label class="checkbox inline">
					     <input type="checkbox" id="choice1" onclick="showPanels();">&nbsp;Add Panelist From Panel
					     </label>	
						</td>
						<td style="padding-left: 15px;">
					    <label class="checkbox inline">
					     <input type="checkbox" id="choice2" onclick="addNewPanelist();">&nbsp;Add External Panelist
					     </label>
						</td>
						<td style="padding-left: 15px;">
						  <label class="checkbox inline">
					      <input type="checkbox" id="choice3" onclick="showPanelistDiv();">&nbsp;Add Panelist
					     </label>		
						</td>
						</tr>
						</table>
					    
					     				
				</div>
				</div>
				<div class="row hide" id="panelDiv">
					      <div class="col-sm-6 col-md-6">
						      <div class="row">
						        <div class="col-sm-12 col-md-12">
								<label class="">Panel Name</label>
							     <select id="panelsId" name="panelsId" class="form-control help-inline" >
							     	<option value="0">Select Panel</option>
							     </select>
							     &nbsp;
							     </div>
						        <div class="col-sm-12 col-md-12" style="margin-top: -10px;">
						         <a  id="iDone" href='javascript:void(0);' onclick="addAllPanelMebersFromPanel();"><spring:message code="lnkImD"/></a>&nbsp;
								<a  id="iDone" href='javascript:void(0);' onclick="cancelOne();">Cancel</a>
						        </div>
						        </div> 
						   </div>
						    <div class="col-sm-3 col-md-3 top30-sm">  
						     <a  id="addAttHrefshow" href='javascript:void(0);' onclick="viewPanelMembers();">View Panel Members</a>						      
						     </div>
						  
						     
				</div>
				<div class="row" >
					<div class="col-sm-6 col-md-6 left15">
					  <div class='row divErrorMsg'  id='panelAddMemberErrordiv' style="display: block;"></div>
					</div>
				</div>
				<div class="row hide" id="addPanelMemberDiv">
					      <div class="col-sm-4 col-md-4">
					        <div class="row">
							    <div class="col-sm-12 col-md-12">
							      <span class=""><label class="">First Name<span class="required">*</span></label>
							      <input type="text" id="panelMemberFirstName" name="panelMemberFirstName" class="help-inline form-control"></span>
								 </div>
							     <div class="col-sm-12 col-md-12" style="margin-top:10px;">
							       <a  id="iDone1" href='javascript:void(0);' onclick="addNewPanelMember();" ><spring:message code="lnkImD"/></a>&nbsp;
						 		   <a  id="iDone1" href='javascript:void(0);' onclick="cancelTwo();" >Cancel</a>
							     </div>
						     </div>
						   </div>  
						   <div class="col-sm-4 col-md-4">
							   <span class=""><label class="">Last Name<span class="required">*</span></label>
							   <input type="text" id="panelMemberLastName" name="panelMemberLastName" class="help-inline form-control"></span>
						   </div>
						   <div class="col-sm-4 col-md-4">
							   <span class=""><label class="">Email<span class="required">*</span></label>
							   <input type="text" id="panelMemberEmail" name="panelMemberEmail" class="help-inline form-control"></span>
							</div>							
				  </div>
				
				  <div class="row">
					<div class="col-sm-12 col-md-12">
			     		<br/>
				     </div>	
				     <div class="col-sm-12 col-md-12">
				    <div id="panelistDiv" class="span10 hide" ></div>
				    </div>
				  </div>
				
				  <div class="row" style="height:10px;"></div>    
				  <div class="row hide" id="panelDistrictdiv">
				    	 <div class="col-sm-12 col-md-12">
					          <label>District Name</label>
					          <c:if test="${DistrictName!=null}">
					             	${DistrictName}	
					           </c:if>
						</div>
					</div> 
					<div class="row hide" id="panelSchooldiv">
				         <div class="col-sm-12 col-md-12">
					        <label>School Name</label>
				          	<c:if test="${SchoolName==null}">
				           	<input type="text" id="panelSchoolName" maxlength="100" name="panelSchoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowDataPanel', 'panelSchoolName','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowDataPanel', 'panelSchoolName','districtId','');"
												onblur="hideSchoolMasterDiv(this,'panelSchoolId','divTxtShowDataPanel'); getUsersBySchool('panel');"	/>
							</c:if>
							<input type="hidden" id="panelSchoolId"/>
							  <c:if test="${SchoolName!=null}">
				             	${SchoolName}<script>getUsersBySchool('panel');</script>
				             </c:if>
							 <div id='divTxtShowDataPanel'  onmouseover="mouseOverChk('divTxtShowDataPanel','panelSchoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
				        	
					      </div>
				     </div>
				     <div class="row hide" id="panelUserdiv">
					     <div class="col-sm-6 col-md-6">
							<span class=""><label class="">Panelist Name<span class="required">*</span></label>
							<input type="hidden" id="pteacherId" name="pteacherId"/>
							<input type="hidden" id="jobId" name="jobId"/>
							<input type="hidden" id="panelId" name="panelId"/>
							<input type='hidden' id="pcurrDate" value="<fmt:formatDate value='${today}' pattern='MM-dd-yyyy' />">
						     <select id="panelUserId" name="panelUserId" class="span6 form-control" onchange="addPanelist();">
						     	<option>Select Panelist</option>
						     </select>
						   	 </span>
						</div>
				    </div>
				
				<div class="row">
					<div class="col-sm-12 col-md-12" >
						<label class="checkbox inline">
					     <input type="checkbox" id="panelStatus">&nbsp;Panel was completed.
					     </label>	
					</div>
				</div>
		      
		      </div>  <!-- /.end body -->
		      <div class="modal-footer">
			      <div class="pull-left" style='margin-left:2px;margin-top: 0px;'>
			      		<button type="button" class="btn f1 btn-secondary" id="canEvtBtn" onclick="cancelPanelEvent();">Cancel Event</button>
			      </div>
		     	 
		     	 <div  class="pull-right">
		       		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 
		       		<button type="button" class="btn btn-primary hide cmPanel1" id="doneBtn" onclick="saveCommonPanel(false);">Done</button>
		        	<button type="button" class="btn btn-primary cmPanel2" id="doneBtn" onclick="savePanel(false);">Done</button>
		          </div>
		      </div>
	  </div>
  </div>
</div>	  
<!--==================================================================================================== -->
<div  class="modal hide"  id="panelMemberDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style='width:670px;'>
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideMemberPanel();">x</button>
		<h3 id="myModalLabel">Panel Members</h3>
	</div>
	<div class="modal-body" style="height:auto;overflow-y:auto">
	<div class='divErrorMsg'  id='panelMemberErrordiv' style="display: block;"></div>
		<div class="control-group" id='panelMemberData'>
		</div>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideMemberPanel();">Cancel</button> 
		        	<button type="button" class="btn btn-primary" id="doneBtns" onclick="addPanelistFromPanel();" >Add Members</button>
 	</div>
</div>
</div>
</div>

	


<div class="modal hide"  id="addPanelMemberDiv1" style="border: 0px solid blue;width: 600px;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div  class="modal hide"  id="addPanelMemberDiv" style="tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideAddMemberPanel();">x</button>
		<h3 id="myModalLabel">Add Panel Member</h3>
	</div>
	<div class="modal-body">
	<div class='divErrorMsg span8'  id='panelAddMemberErrordiv' style="display: block;"></div>
	<div class="row">
	 <div class="span3">
		   <span class=""><label class="">First Name<span class="required">*</span></label>
		   <input type="text" id="panelMemberFirstName" name="panelMemberFirstName" class="help-inline span3"></span>
	 </div>
	  <div class="span3">
		   <span class=""><label class="">Last Name<span class="required">*</span></label>
		   <input type="text" id="panelMemberLastName" name="panelMemberLastName" class="help-inline span3"></span>
	 </div>
	  <div class="span3">
		   <span class=""><label class="">Email<span class="required">*</span></label>
		   <input type="text" id="panelMemberEmail" name="panelMemberEmail" class="help-inline span3"></span>
	 </div>
	</div>
	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideAddMemberPanel();">Cancel</button> 
		        	<button type="button" class="btn btn-primary" id="doneBtns" onclick="addNewPanelMember();" >Add Member</button>
 	</div>
</div>
</div>
</div>
</div>


<div class="modal hide" id="myModalNotes"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
<input type="hidden" id="teacherId" name="teacherId" value="">
	<div class="modal-dialog" style="width:935px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'>x</button>
		<h3 id="myModalLabel">Notes</h3>
	</div>
	
	<div class="modal-body">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID' name='uploadNoteFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload' enctype='multipart/form-data' method='post' target='uploadNoteFrame'  class="form-inline" onsubmit="return saveNotes();" action='noteUploadServlet.do' accept-charset="UTF-8">
			
		<div class="mt10">
			<div class='span10 divErrorMsg' id='errordivNotes' style="display: block;"></div>
			<div class="span10" >
		    	<label><strong>Enter Notes<span class="required">*</span></strong></label>
		    	<div class="span10" id='divTxtNode' style="padding-left: 0px; margin-left: 0px;" >
		    		<textarea style="width: 100%" readonly id="txtNotes" name="txtNotes" class="span10" rows="4"></textarea>
		    	</div>  
		    	<input type="hidden" id="teacherIdForNote" name="teacherIdForNote" value="">
		    	<input type="hidden"  name="noteDateTime" id="noteDateTime" value="${dateTime}"/>
		    	<div id='fileNotes' style="padding-top:10px;">
	        		<a href='javascript:void(0);' onclick='addNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addNoteFileType();'>Attach a File</a>
	        	</div>      	
			</div>						
		</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<c:set var="chkSaveDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSaveDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSaveDisp" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSaveDisp" value="none" />
			</c:otherwise>
		</c:choose>
 		<!-- <span id="spnBtnCancel" style="display: ${chkSaveDisp}"><a href="#"	onclick="return cancelNotes()">Cancel</a></span> -->
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='showCommunicationsDiv();'>Cancel</button>&nbsp; 
 		<span id="spnBtnSave" style="display: ${chkSaveDisp}"><button class="btn btn-primary"  onclick="saveNotes()">Save <i class="icon"></i></button>&nbsp;</span>
 	</div>
   </div>
</div>
</div>
<!-- ================================================================================================================================ -->
<!--Add message Div by  Sekhar  -->
<div  class="modal hide"  id="myModalMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show'>Your message is successfully sent to the Candidate.
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">Ok</button>
 	</div>
</div> 	
</div>
</div>
<input type="hidden" id="teacherDetailId" name="teacherDetailId">
<div class="modal hide"  id="myModalMessage"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:690px">
	<div class="modal-content">
	<input type="hidden" id="emailId" name="emailId">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>x</button>
		<h3 id="myModalLabel">Post a message to the Candidate</h3>
	</div>
	<iframe id='uploadMessageFrameID' name='uploadMessageFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
	</iframe>
	<form id='frmMessageUpload' enctype='multipart/form-data' method='post' target='uploadMessageFrame'  class="form-inline" onsubmit="return validateMessage();" action='messageCGUploadServlet.do' accept-charset="UTF-8">
	
	<div class="modal-body" style="max-height: 450px;overflow-y:auto;">
		<div class="" id="divMessages">						
		</div>
		<div class="control-group" style="margin-top: 5px;">
			<div class='divErrorMsg' id='errordivMessage' style="display: block;"></div>
		</div>
		
		<%---------- Gagan :District wise Dynamic Template [Start]  --%>
			<div id="templateDiv" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-6" style="max-width:300px;">
			    	<label><strong>Template</strong></label>
		        	<br/>
		        	<select class="form-control" id="districttemplate" onchange="setTemplate()">
		        		<option value="0">Select Template</option>
		        	</select>
		         </div>	
		        	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<button class="btn btn-primary hide top26" id="btnChangetemplatelink" onclick="return getTemplate();">Apply Template</button> <input type="hidden" class="span1" id="confirmFlagforChangeTemplate" name="confirmFlagforChangeTemplate" value="0">
			
		    	</div>
		    </div>
		    
		    <div class="row col-md-12" id="documentdiv">
	    	<div class="row col-sm-6" style="max-width:300px;">
		    	<label><strong>Document</strong></label>
	        	<br/>
	        	<select class="form-control" id="districtDocument" onchange="showDocumentLink()">
	        		<option value="">Select Document</option>
	        	</select>
	        	
		
	         </div>	<br><br>
	         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
	        	<a href="javascript:void(0);" id="viewDoc" class="hide top26" onclick="return vieDistrictFile('ifrmAttachment')">View File</a>
	    	</div>
		<%---------- Gagan :District wise Dynamic Template [END]  --%>
		
		<div class="control-group row" style="padding-left: 15px;padding-top: 5px;">
    		<label><strong>To<br/></strong><span id="emailDiv" style="width:612px;word-wrap:break-word;padding-left:1px;"></span>
   		</div>
		<div id='support' class="row" style="padding-left:15px;" >
		<div class="control-group">
			<div class="">
		    	<label><strong>Subject</strong><span class="required">*</span></label>
	        	<input id="messageSubject" name="messageSubject" type="text" class="form-control" style='width:612px;' maxlength="100" />
			</div>
		</div>
	            
        <div class="control-group">
			<div class="" id="messageSend" style="width: 612px;">
		    	<label><strong>Message</strong><span class="required">*</span></label>
		    	<label class="redtextmsg">(If you are planning to copy and paste text from a web page and/or a Microsoft document,
 				please ensure to paste that text first into a simple text editor such as Notepad to remove the 
 				special characters and then copy and paste it into the field below. Otherwise, the text 
 				directly cut and pasted from a web page and/or a Microsoft document may not display properly to the reader)
 				</label>
	        	<textarea rows="5" class="form-control" cols="" id="msg" name="msg" maxlength="1000"></textarea>
	        </div>
	        <input type="hidden" id="teacherIdForMessage" name="teacherIdForMessage" value="">
    		<input type="hidden"  name="messageDateTime" id="messageDateTime" value="${dateTime}"/>
	    	<div id='fileMessages' style="padding-top:12px;">
        		<a href='javascript:void(0);' onclick='addMessageFileType();'>
        		<img src='images/attach.png'/></a> 
        		<a href='javascript:void(0);' onclick='addMessageFileType();'>Attach a File</a>
        	</div>   
		</div>
 		</div>
 		<div id='lodingImage' style="display: block;text-align:center;padding-top:4px;"></div>
 		<!-- end support div -->
  	</div>
  	
  	</form>
 	<c:set var="chkSendDisp" value="inline"/>
		<c:choose>
			<c:when test="${userMaster.entityType eq 1}">
				<c:set var="chkSendDisp" value="none" />
			</c:when>
			<c:when test="${fn:indexOf(roleAccess,'|8|') ne -1}">
				<c:set var="chkSendDisp" value="inline" />
			</c:when>
			<c:otherwise>			
				<c:set var="chkSendDisp" value="none" />
			</c:otherwise>
	</c:choose>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='showCommunicationsDivForMsg();'>Cancel</button>&nbsp;
 		<button class="btn btn-primary" onclick="validateMessage()" style="display: ${chkSendDisp}">Send</button>&nbsp;
 	</div>
</div>
</div>
</div>

<%-- ---------- GAgan : Apply Template Confirm Pop up [Start] ----------------%>
<div class="modal hide"  id="internalDiv" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog" style="width: 532px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id='' onclick="backInternal();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="internalTxt">
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="backInternal();">OK</button></span> 		
 	</div>
  </div>
</div>
</div>



<div class="modal hide"  id="confirmChangeTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id=''>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="ddd">Do you really want to override the existing template in Message field with
the selected template? You may lose any change done in existing template if
you override it with selected template. If yes, click on "OK" button otherwise
click "Cancel" button.
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span id=""><button class="btn btn-large btn-primary" onclick="confirmChangeTemplate()" >Ok <i class="icon"></i></button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button></span> 		
 	</div>
</div>
</div>
</div>

<%-- ---------- GAgan : Apply Template Confirm Pop up [Start] ----------------%>
<div class="modal hide"  id="confirmChangeTemplate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog-for-cgmessage">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" id=''>x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="ddd">Do you really want to override the existing template in Message field with
the selected template? You may lose any change done in existing template if
you override it with selected template. If yes, click on "OK" button otherwise
click "Cancel" button.
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span id=""><button class="btn btn-large btn-primary" onclick="confirmChangeTemplate()" >Ok <i class="icon"></i></button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button></span> 		
 	</div>
</div>
</div>
</div>
<%------- GAgan : Apply Template Confirm Pop up [END] ---------%>

			
<div  class="modal hide" id="myModalSearch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
<div class="modal-dialog-for-cgsearch" style="min-width: 380px;">
<div class="modal-content">		
<div class="modal-header">
 		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideSearchDiv();">x</button>
	<h3 id="myModalLabel">Search</h3>
</div>
<div class="modal-body">		
<div class="control-group">		
		<div class="row">
			<div onkeypress="return chkForEnterSearchTeacher(event);">
				<form>
			      <div class="row" style="margin-right: 0px;margin-left: 0px;">
			      
				       <div class="col-sm-6 col-md-6">
				        <span class=""><label class="">First Name</label><input type="text" id="firstName" name="firstName"  maxlength="50"  class="help-inline form-control"></span>
				       </div>
				       
				       <div class="col-sm-6 col-md-6">					      
					       	<span class=""><label class="">Last Name</label><input type="text" id="lastName" name="lastName"  maxlength="50"  class="form-control"></span>					      
				       </div>
				       
				       <div class="col-sm-6 col-md-6">
					       <span><label class="">Email Address</label><input type="text" id="emailAddress" name="emailAddress"  maxlength="75"  class="form-control"></span>					      
				       </div>
				       
				       <div class="col-sm-6 col-md-6">					      
					       	<span class=""><label class="">SSN</label><input type="text" id="ssn" name="ssn"  maxlength="50"  class="form-control"></span>					      
				       </div>
				       
				       <div class="col-sm-6 col-md-6">
							<label class="checkbox" style="margin-bottom: 0px;">
							 <input id="notReviewedFlag" name="notReviewedFlag" type="checkbox" value="1">
							 <spring:message code="msgCandidateNotReviewed" /> <a href="#" id="notReviewedToolTip" rel="tooltip" data-original-title="<spring:message code="msgCandidateNotReviewedToolTip"/>"><img src="images/qua-icon.png" width="15" height="15" alt=""></a> 
							</label>
					  </div>
			      </div>
			      
			      
				<div id="searchLinkDiv" class="row">
				<div class="left30 top5">
		       		<a href="javascript:void:(0);" onclick="displayAdvanceSearch()">Advanced Search</a>
		       </div>
				</div>		
				<div id="advanceSearchDiv" class="hide">
				  	<div class="row top10" id="divCertificate" style="margin-right: 0px;margin-left: 0px;"></div>
			      <div class="row" style="margin-right: 0px;margin-left: 0px;"> 
			      	    <div class="col-sm-6 col-md-6  top10">
					       	<span class=""><label class="">Certification/Licensure State</label>
					       	<select class="form-control " id="stateId" name="stateId" onchange="activecityType();" >
							 <option value="0">All Certification/Licensure State</option>  
							 <c:if test="${not empty lstStateMaster}">
							 	<c:forEach var="stateObj" items="${lstStateMaster}">
									<option value="${stateObj.stateId}">${stateObj.stateName}</option>
								</c:forEach>	
				        	 </c:if>
							</select>	
					       	</span>
				       </div>
				       
				      
				       
				       <div class="col-sm-6 col-md-6  top10">
					       <span><label class="">Certification/Licensure Name</label>
					       </span>
					       <div>
							<input type="hidden" id="certificateTypeMaster" value="0">
							<input type="text" 
								class="form-control"
								maxlength="200"
								id="certType" 
								value=""
								name="certType" 
								onfocus="getFilterCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');"
								onkeyup="getFilterCertificateTypeAutoComp(this, event, 'divTxtCertTypeData', 'certType','certificateTypeMaster','');" 
								onblur="hideFilterCertificateTypeDiv(this,'certificateTypeMaster', 'divTxtCertTypeData');"/>
							<div id='divTxtCertTypeData' style=' display:none;position:absolute;' class='result' onmouseover="mouseOverChk('divTxtCertTypeData','certType')"></div>
						</span>
						</div>       
				      </div>
				      
				      <div class="col-sm-6 col-md-6 top10" id="addCertificate"><a href="javascript:void(0);" onclick="addCertificate(1)" >+ Add Certification/Licensure Name</a></div>
		         </div>
				   
				      
				      <div class="col-sm-12 col-md-12  top10">
				       <span class=""><label class="">Region</label>
					       	<input  type="hidden" id="regionId" value="0">
							<input  type="text" 
								class="form-control"
								maxlength="100"
								id="regionName" 
								value=""
								name="regionName" 
								onfocus="getRegionAutoComp(this, event, 'divTxtRegionData', 'regionName','regionId','');"
								onkeyup="getRegionAutoComp(this, event, 'divTxtRegionData', 'regionName','regionId','');" 
								onblur="hideRegionDiv(this,'regionId','divTxtRegionData');"/>
							<div id='divTxtRegionData' style=' display:none;position:absolute;' 
							onmouseover="mouseOverChk('divTxtRegionData','regionName')" class='result' ></div>
				       </span>
				       </div>
				       
				      <div class="col-sm-6 col-md-6  top10">
				       		<label class="">Highest Degree Attained</label>
							<input  type="text"
							maxlength="50" 
							class="form-control input-small"
							id="degreeName" 
							autocomplete="off"
							value=""
							name="degreeName" 
							onfocus="getDegreeMasterAutoComp(this, event, 'divTxtShowEducation', 'degreeName','degreeId','');"
							onkeyup="getDegreeMasterAutoComp(this, event, 'divTxtShowEducation', 'degreeName','degreeId','');" 
							onblur="hideDegreeMasterDiv(this,'degreeId','divTxtShowEducation');"/>
							<div id='divTxtShowEducation' style=' display:none;position:absolute;' class='result'  
							onmouseover="mouseOverChk('divTxtShowEducation','degreeName')"></div>	
							<input  type="hidden" id="degreeId" value="0">
							<input  type="hidden" id="degreeType" value="">		
								
					   </div>
				       <div class="col-sm-6 col-md-6  top10">
					       	<span class=""><label class="">College Attended</label>
					       	<input  type="hidden" id="universityId" value="0">
							<input  type="text" 
								class="form-control"
								maxlength="100"
								id="universityName" 
								autocomplete="off"
								value=""
								name="universityName" 
								onfocus="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');"
								onkeyup="getUniversityAutoComp(this, event, 'divTxtUniversityData', 'universityName','universityId','');" 
								onblur="hideUniversityDiv(this,'universityId','divTxtUniversityData');"/>
							<div id='divTxtUniversityData' style=' display:none;position:absolute;' 
							onmouseover="mouseOverChk('divTxtUniversityData','universityName')" class='result' ></div>
						</span>
				    </div>
				    
				      <div class="col-sm-6 col-md-6  top10">
				       <span class=""><label class="">Norm Score</label>
				       <table><tr><td>
				       <select style='width:70px;' id="normScoreSelectVal" name="contacted" class="form-control" >
				       			<option value="5" selected="selected">>=</option>
								<option value="1" >=</option>
								<option value="2"><</option>
								<option value="3"><=</option>
								<option value="4">></option>
						</select>
				       </td><td style='padding-left:5px;' id="sliderNormScore">
				       
				       <!-- 
				       <iframe id="ifrmNorm"  src="slideract.do?name=normScoreFrm&tickInterval=10&max=100&swidth=230&svalue=0" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;margin-top:-11px;"></iframe>
				       -->
				       </td></tr></table>
				       <input type="hidden" name="normScore" id="normScore" />
				       </div>
				       <div class="col-sm-6 col-md-6  top10">
					       	<span class=""><label class="">CGPA </label>
					       	<table><tr><td>
					       	<select style='width:70px;' id="CGPASelectVal" name="contacted" class="form-control">
					       		<option value="5" selected="selected">>=</option>
								<option value="1">=</option>
								<option value="2"><</option>
								<option value="3"><=</option>
								<option value="4">></option>
								
							</select>	
							</td><td style='padding-left:5px;' id="sliderCGPA">
							<!-- 
					       <iframe id="ifrmCGPA"  src="slideract.do?name=CGPAFrm&tickInterval=1&max=5&swidth=230&svalue=0" scrolling="no" frameBorder="0" style="border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:280px;margin-top:-11px;"></iframe>
					         -->
					       </td></tr></table>
					       <input type="hidden" name="CGPA" id="CGPA" />
					       </span>
				       </div>
				       
			     
			      	  <div class="col-sm-6 col-md-6  top10">
					       	  <span class=""><label class="">Contacted</label>
					       	<input type="hidden" name="contacted" id="contacted" value="0">
					       <table><tr><td><input type="radio" name="contactedRd"  onclick="checkValue(1);"  id="contactedRd1"   value="1"></td><td>Yes</td><td  style='padding-left:20px;'><input type="radio" name="contactedRd"  onclick="checkValue(2);"  id="contactedRd2"  value="2"></td><td>No</td><td  style='padding-left:20px;'><input type="radio" name="contactedRd"  onclick="checkValue(0);"  id="contactedRd0" value="0" checked></td><td>All</td></tr></table>
				       </div>
			      	 
			      	    <div class="col-sm-6 col-md-6  top10">
					       	<span class=""><label class="">Candidate Status</label>
					       	  	<input type="hidden" name="candidateStatus" id="candidateStatus" value="0">
					       <table><tr><td><input type="radio" name="candidateStatusRd"  onclick="checkStatusValue('hird');"  id="candidateStatusRd1"   value="hird"></td><td>
					       <c:choose>
					       <c:when test="${entityType eq 5 || entityType eq 6}">
					       	Talent Type Hired Candidates
					       </c:when>
					       <c:otherwise>Hired Candidates</c:otherwise></c:choose>					       
					       </td><td  style='padding-left:20px;'><input type="radio" name="candidateStatusRd"  onclick="checkStatusValue(0);"  id="candidateStatusRd0" value="0" checked></td><td>All Candidates</td></tr></table>
					      </span>
				       </div>
				         
				       <div class="col-sm-6 col-md-6  top10">
					       	<span class=""><label class="">Tags </label>
					       	     <select  id="tagsearchId" name="tagsearchId" class="form-control">
					       		       ${tagslist}
							     </select>	
					       </span>
				       </div>
				       
				       <div class="col-sm-6 col-md-6  top10">
						<div class=""><label class="">Year of Teaching experience</label>
							<table><tr><td>
											<select style='width: 70px;' id="YOTESelectVal"	name="YOTESelectVal" class="form-control">
												<option value="5" selected="selected">>=</option>
												<option value="1">=</option>
												<option value="2"><</option>
												<option value="3"><=</option>
												<option value="4">></option>
											</select>
										</td>
										<td style='padding-left:5px;' id="sliderYOTETP"></td>
										<input type="hidden" name="YOTE" id="YOTE" />
									</tr>
							</table>
						</div>
					   </div>
				       
				       <div class="col-sm-6 col-md-6 top10">
				       		<span class=""><label class="">State Name</label>
								<select class="form-control" id="stateId2" name="stateId2" onchange="activecityType(0);">
									<option value="0">All State</option>
									<c:if test="${not empty lstStateMaster}">
										<c:forEach var="stateObj" items="${lstStateMaster}">
											<option value="${stateObj.stateId}">${stateObj.stateName}</option>
										</c:forEach>
									</c:if>
								</select>
							</span>
					   </div>
					    
					   <div class="col-sm-6 col-md-6 top10">
							<span class=""><label class="">Zip Code</label>
								<input id="zipCode" name="" maxlength="10" class="help-inline form-control" type="text" onkeypress="return checkForInt(event);">	
					    	</span>
					   </div>
					   
					    <div class="col-sm-6 col-md-6">
						<div class="row">
							<div class="col-sm-6 col-md-6">
								<span class=""><label class="">EPI Date From</label>
									<input type="text" id="epiFromDate" name="fromDate" class="help-inline form-control">
								</span>
							</div>
							<div class="col-sm-6 col-md-6">
								<span class=""><label class="">EPI Date To</label>
									<input type="text" id="epiToDate" name="toDate"	class="help-inline form-control">
								</span>
							</div>
						</div>
					</div>
					   
			   
			     </div> 
				</form>
			</div>            
		</div>
			
</div>
</div>
<div class="modal-footer">
	<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick="hideSearchDiv();">Close</button> 
<button class="btn btn-primary " type="button" onclick="searchCG()">Search <i class="icon"></i></button>
</div>
</div>
</div>
</div>	
<div class="tableContentJobHeader">
</div>
<div class="TableContent">  
   	
            <div class="table-responsive" id="divReportGrid">          

            </div>            
</div> 

 
<div style="display:none; z-index: 5000;" id="loadingDivReference" >
    <table  align="center" >
 		<tr><td style="padding-top:270px;" align="center"><!-- <img src="images/please.jpg"/> --></td></tr>
 		<tr><td style="padding-top:0px;"  align="center"><img src="images/loadingAnimation.gif"/></td></tr>
 		<tr id='paymentMessage'>
	 		<td style='padding-top:0px;" id='spnMpro' align='center'>
	 		</td>
 		</tr>
	</table>
</div>
 
 
<div class="modal hide"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='blockMessage'>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
 	</div>
</div>
</div>
</div>
<!-- candidate phone div -->
<div  class="modal hide"  id="myModalPhoneShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="max-width:370px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel">Phone</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="divPhoneByPro" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">Close</button> 		
   </div>
  </div>
 </div>
</div>
<!-- Start Teacher Profile -->
<div  class="modal hide" id="myModalProfileVisitHistoryShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-dialog" style="width:100px;" id="mydiv">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel">Teacher Profile Visit History</h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y: scroll;padding-right: 18px;">		
		<div class="control-group">
			<div id="divteacherprofilevisithistory" class="">
		    		        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">Close</button> 		
 	</div>
  </div>
 </div>
</div>
<div  class="modal hide"  id="myModalReferenceNoteView"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog' style="width: 520px;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel">Reference Notes</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" id="divRefNotesInner">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">Close</button> 		
 	</div>
  </div>
 </div>
</div>

<div  class="modal hide"  id="myModalReferenceNoteViewNew"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog' style="width: 520px;">
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopoverNew()">x</button>
		<h3 id="myModalLabelNew">Reference Notes</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div class="" id="divRefNotesInnerNew">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopoverNew()">Close</button> 		
 	</div>
  </div>
 </div>
</div>

<!-- Ref Note -->
<div class="modal hide"  id="myModalReferenceNotesEditor"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog-for-cgreference">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'>x</button>
		<h3 id="myModalLabel">Notes</h3>
	</div>
	
	<div class="modal-body"  style="margin:10px;">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID_ref' name='uploadNoteFrame_ref' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload_ref' enctype='multipart/form-data' method='post' target='uploadNoteFrame_ref'  class="form-inline" onsubmit="return saveReferenceNotes();" action='referenceNoteUploadServlet.do' accept-charset="UTF-8">
		
			<input type="hidden" name="eleRefId" id="eleRefId">
			<input type="hidden" id="teacherIdForNote_ref" name="teacherIdForNote_ref">
			
			<div class="row mt10">
				<div class='span10 divErrorMsg' id='errordivNotes_ref' style="display: block;"></div>
				<div class="span10" >
			    	<label><strong>Enter Notes<span class="required">*</span></strong></label>
			    	<div class="span10" id='divTxtNode_ref' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea readonly id="txtNotes_ref" name="txtNotes_ref" class="span10" rows="4"   ></textarea>
			    	</div>  
			    	<div id='fileRefNotes' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>Attach a File</a>
		        	</div>      	
				</div>						
			</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'>Cancel</button>&nbsp; 
 		<span id="spnBtnSave" style="display:inner"><button class="btn btn-primary"  onclick="saveReferenceNotes()">Save <i class="icon"></i></button>&nbsp;</span>
 	</div>
  </div>
 </div>
</div>
<!-- End Ref Note -->
<!-- Ref Note New -->
<div class="modal hide"  id="myModalReferenceNotesEditorNew"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-dialog-for-cgreference">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditor();'>x</button>
		<h3 id="myModalLabel">Notes</h3>
	</div>
	
	<div class="modal-body"  style="margin:10px;">	
		<div class="row" id="divNotes" style="padding-left: 15px; ">						
		</div>
		<iframe id='uploadNoteFrameID_ref' name='uploadNoteFrame_ref' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
		</iframe>
		<form id='frmNoteUpload_ref' enctype='multipart/form-data' method='post' target='uploadNoteFrame_ref'  class="form-inline" onsubmit="return saveReferenceNotes();" action='referenceNoteUploadServlet.do' accept-charset="UTF-8">
		
			<input type="hidden" name="eleRefId" id="eleRefId">
			<input type="hidden" id="teacherIdForNote_ref" name="teacherIdForNote_ref">
			
			<div class="row mt10">
				<div class='span10 divErrorMsg' id='errordivNotes_refNew' style="display: block;"></div>
				<div class="span10" >
			    	<label><strong>Enter Notes<span class="required">*</span></strong></label>
			    	<div class="span10" id='divTxtNode_refNew' style="padding-left: 0px; margin-left: 0px; " >
			    		<textarea readonly id="txtNotes_refNew" name="txtNotes_ref" class="span10" rows="4"   ></textarea>
			    	</div>  
			    	<div id='fileRefNotesNew' style="padding-top:10px;">
		        		<a href='javascript:void(0);' onclick='addRefeNoteFileType();'><img src='images/attach.png'/></a> <a href='javascript:void(0);' onclick='addRefeNoteFileType();'>Attach a File</a>
		        	</div>      	
				</div>						
			</div>
		</form>
 	</div>
 	 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeRefNotesDivEditorNew();'>Cancel</button>&nbsp; 
 		<span id="spnBtnSave" style="display:inner"><button class="btn btn-primary"  onclick="saveReferenceNotesNew()">Save <i class="icon"></i></button>&nbsp;</span>
 	</div>
  </div>
 </div>
</div>
<!-- End Ref Note New -->
<!-- End Teacher Profile -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadPDR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">x</button>
		<h3 id="myModalLabel">PD Report</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		
		  <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   <iframe id="ifrmPDR" width="100%" height="480px" src=""></iframe>		  
		  </div>   
		  
		   
		
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showProfilePopover()">Close</button> 		
   </div>
  </div>
 </div>
</div>
<div  class="modal hide pdfDivBorder"  id="modalDownloadCGR" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabel">CG Report</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		
		  <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   <iframe id="ifrmCGR" width="100%" height="480px" src=""></iframe>		  
		  </div>   
		  
		   
		
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
   </div>
  </div>
 </div>
</div>
      
<!-- Send Message -->

<div  class="modal hide"  id="myModalMsgShow_SendMessage"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='message2show_SendMessage'>Your message is successfully sent to the Candidate.
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button>
 	</div>
   </div>
  </div>
</div>
<div class="modal hide"  id="myModalQAEXEditor"   tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showPreviousModel();">x</button>
		<h3>Explaination <a href="#" id="iconpophoverQE" rel="tooltip" data-original-title="Click here to view the explaination."><img src="images/qua-icon.png" width="15" height="15" alt=""></a></h3>
	</div>
	<div class="modal-body">	
			<div class="row mt10">
				<div class="span10" >
			    	<div id='divExplain' style="margin-top:-15px;">
		        	</div>      	
				</div>						
			</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showPreviousModel();">Cancel</button>
 	</div>
</div>

<div  class="modal hide" id="modalDownloadIAFN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;">
	<div class="modal-dialog-for-cgdemoschedule">
	<div class="modal-content">
	<div class="modal-header" id="modalDownloadIAFNMove" style="cursor: move;">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelInstructionHeader"></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="" class="">
				<iframe src="" id="ifrmAttachInstructionFileName" width="100%" height="480px">
				 </iframe>        	
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button> 		
 	</div>
</div>
</div>
</div>
<iframe src="" id="ifrmRef" width="100%" height="480px" style="display: none;"></iframe>
<iframe src="" id="ifrmTrans" width="100%" height="480px" style="display: none;"></iframe>


<div  class="modal hide"  id="myModalViewCompleteTeacherStatusNotes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="viewCompleteTeacherStatusNotesClose()">x</button>
		<h3 id="myModalLabel">Status Notes</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="myModalViewCompleteTeacherStatusNotesInnerText" style="max-height:350px;">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="viewCompleteTeacherStatusNotesClose()">Close</button> 		
 	</div>
  </div>
</div>
</div>
 
<div  class="modal hide"  id="myJobWiseStatusDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog'>
	<div class='modal-content'>
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelForPanel">Job Wise Panel Status</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" >
		<div class='divErrorMsg' id='errordivStauts'>
		</div>
			<div id="panelStatusData" >
				<input type="hidden" id="jPanelStauts" value="">
				<select id="jobPanelStatusId" name="jobPanelStatusId" class="form-control help-inline" >
					<option>Select Status</option>
				</select>
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn btn-primary"  aria-hidden="true" onclick="showPanel();">Ok</button>&nbsp;<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Close</button>
 	</div>
   </div>
 </div>
</div>

<style>
.large.tooltip-inner {max-width: 350px;width:350px;}
</style>
<script type="text/javascript">
getJobInformationHeaderCG();
getCandidateGrid();
//$("#myModalStatus").draggable();
//$("#myModalSearch").draggable();
$('#myModal').modal('hide');
$('#myModalCoverLetter').modal('hide');
$('#tpViewPDF').tooltip();
$('#tpSearch').tooltip();
$('#tpLegend').tooltip();
$('#normCurve').tooltip();
$('#normCurve').tooltip();
$('#createIcon').tooltip();
$('#renameIcon').tooltip();
$('#cutIcon').tooltip();
$('#copyIcon').tooltip();
$('#pasteIcon').tooltip();
$('#deleteIcon').tooltip();
$('#iconpophoverQE').tooltip();
$('#iconpophover7').tooltip();
$('#notReviewedToolTip').tooltip({
 template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner large"></div></div>'
});

$(document).ready(function(){
$('#demodivTxtNode').find(".jqte").width(612);
$('#divTxtNode').find(".jqte").width(875);
$('#messageSend').find(".jqte").width(612);
$('#statusNotes').find(".jqte").width(670);
});


</script>
<script>
$(document).ready(function(){
	/* ---------------------- Create Folder Method Start here --------------- */
	$("#btnAddCode").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.createUserFolder();
	});
/*---------------- ------------- Create Folder Method End  here --------------- */
/*-----------------  Rename Folder ------------------------*/
	$("#renameFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.renameFolder();
	});
/*-----------------  Cut Copy Paste Folder ------------------------*/
	$("#cutFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.cutFolder();	
	});

	$("#copyFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.copyFolder();		
	});

	$("#pasteFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.pasteFolder();	
	});
		
	$("#deletFolder").click(function(){
		document.getElementById('iframeSaveCandidate').contentWindow.deletFolder();
	});	
	 $('#divTxtPhone').find(".jqte");
}) 
</script>

<script type="text/javascript">//<![CDATA[
   
      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
     cal.manageFields("demoDate", "demoDate", "%m-%d-%Y");
     cal.manageFields("panelDate", "panelDate", "%m-%d-%Y");
    //]]>
</script>

<script type="text/javascript">//<![CDATA[ 
  var now = new Date();
  var calHired = Calendar.setup({ 
 	 onSelect: function(cal) { calHired.hide() },
 	 max : now,
 	 showTime: true
 	 
 	 });
  	calHired.manageFields("setHiredDate", "setHiredDate", "%m-%d-%Y");
  	
  	
  	
 //]]>
$(document).mouseup(function (e)
{
    var container = $(".DynarchCalendar-topCont");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
}); 
</script>

<!--Start : @ashish :: for CG Teacher Profile {draggableDiv} -->

<script>
$(function()
 {        
 /*
 
$("#myModalStatus").dialog
({
        autoOpen: false,
        modal: false,      
        resizable: false,
  
});
        
	    $( "#cgTeacherDivMaster" ).draggable({
		handle:'#cgTeacherDivHeader', 
		containment:'window',
		revert: function(){
			var $this = $(this);
			var thisPos = $this.position();
			var parentPos = $this.parent().position();
			var x = thisPos.left - parentPos.left;
			var y = thisPos.top - parentPos.top;
			if(x<0 || y<180)
			{
				return true; 
			}else{
				return false;
			}
		}
	});
	*/
/*	$( "#qualificationDiv" ).draggable({
		cursor:'move',
		handle:'#qualificationDivHeader', 
		containment:'window',
		revert: function(){
			var $this = $(this);
			var thisPos = $this.position();
			var parentPos = $this.parent().position();
			var x = thisPos.left - parentPos.left;
			var y = thisPos.top - parentPos.top;
			if(x<0 || y<180)
			{
				return true; 
			}
			else{
				return false;
			}
		}
	});
});*/
	
	$( "#printDataProfile" ).draggable({
		cursor:'move',
		handle:'#printDataProfileDivHeader', 
		containment:'window',
		revert: function(){
			var $this = $(this);
			var thisPos = $this.position();
			var parentPos = $this.parent().position();
			var x = thisPos.left - parentPos.left;
			var y = thisPos.top - parentPos.top;
			if(x<0 || y<180)
			{
				return true; 
			}else{
				return false;
			}
		}
	});
	
});
</script>

<!--End : @ashish :: for CG Teacher Profile {draggableDiv} -->

<!-- CG Mass Update --> 
<input type="hidden"  name="teacherIdsOSCEOLA_mass" id="teacherIdsOSCEOLA_mass"/>
<input type="hidden"  name="jobForTeacherIds_mass" id="jobForTeacherIds_mass"/>
<input type="hidden"  name="teacherIds_mass" id="teacherIds_mass"/>
<input type="hidden"  name="statusId_mass" id="statusId_mass"/>
<input type="hidden"  name="secondaryStatusId_mass" id="secondaryStatusId_mass"/>
<input type="hidden"  name="txtschoolCount_mass" id="txtschoolCount_mass"/>
<input type="hidden"  name="dispalyStatusName_mass" id="dispalyStatusName_mass"/>

<input type="hidden"  name="statusnoteFileNameCGMassTemp" id="statusnoteFileNameCGMassTemp"/>

<input type="hidden"  name="isFinalizeCGMass" id="isFinalizeCGMass"/>
<input type="hidden"  name="userActionCGMass" id="userActionCGMass" value="0" />

<input type="hidden"  name="refreshGridCGMass" id="refreshGridCGMass" value="0" />

<div  class="modal hide"  id="myModalStatus_mass"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 520px;">
	<div class="modal-content">		
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideStatus_mass()">x</button>
		<h3 id="myModalLabel_mass"><span class="left7" id="status_l_title_mass">Status</span></h3>
	</div>
	
   <img alt="" class="img-responsive" src="images/headstatus.png">
	<div class="modal-body-cgstatus">		
		<div class="control-group">
			<div id="divStatus_mass">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideStatus_mass()">Done</button>	
 	</div>
</div>
</div>
</div>

<!-- 2nd  -->
<div class="modal hide" id="myModalStatusInfo_mass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showStatusDetailsOnClose()'>x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title_mass">Status</span></h3>
		<input type="hidden" id="statusName_mass" name="statusName_mass" value="">
		<input type="hidden" id="statusShortName_mass" name="statusShortName_mass" value="">
		<input type="hidden" id="isEmailTemplateChanged_mass" name="isEmailTemplateChanged_mass" value="0">
		<input type="hidden" id="isEmailTemplateChangedTeacher_mass" name="isEmailTemplateChangedTeacher_mass" value="0">
		
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;">
		<div class="control-group">
			<input type="hidden" name="scoreProvided_CGMass" id="scoreProvided_CGMass" />
			<div class='divErrorMsg left2' id='errorStatusNote_mass' style="display: block;"></div>
			
			<div id="divStatusNoteGrid_mass" style='width:700px;overflow:hidden;'></div>
			
			<div id="noteMainDiv_mass" class="hide">
				<div id="templateDivStatus_mass" style="display: none;">		     	
			    	<div class="row col-md-12" >
			    	<div class="row col-sm-8" style="max-width:300px;padding-top:10px;">		    	
				    	<label><strong>Template</strong></label>
			        	<br/>
			        	<select class="form-control" id="statuswisesection_mass" onchange="getsectionwiselist_mass(this.value)">
			        		<option value="0">Select Template</option>
			        	</select>
			         </div>	
				
			    	</div>
			    	
			    	
			    	<div class="row col-md-12" id="showTemplateslist_mass" style="display:none">		    	
			    	<div class="row col-sm-12" style="padding-top:10px;">
			    	<label><strong>Sub Template</strong></label>
			        	<br/>
			        	<span id="addTemplateslist_mass"></span>
			        	
			         </div>		        	
						<button class="btn btn-primary hide top26" id="changeTemplateBody_mass" onclick="getTemplateByLIst()">Apply Template</button>
						<input type="hidden" id="templateId_mass" value=""/>
				</div>
			    </div>
			
				<div class="row mt5" >
			    	<div class="left16" id="statusNotes_mass" >
			    	<label class="">Note<c:if test="${statusNotes eq true}"><span class="required">*</span></c:if>
			    	</label>
			    		<textarea readonly id="statusNotes_mass" name="statusNotes_mass" class="span6" rows="2"   ></textarea>
			    	</div> 
				</div>  
				<iframe id='uploadStatusFileFrameID_mass' name='frmStatusFileFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form id='frmStatusNote_mass' enctype='multipart/form-data' method='post' target='frmStatusFileFrame' onsubmit="saveStatusNote(0);"  class="form-inline" action='statusNoteUploadServlet_CGMass.do' accept-charset="UTF-8">
				<input type="hidden" id="statusNotesTeacherIds_CGMass" name="statusNotesTeacherIds_CGMass" value=""/>
				<input type="hidden" id="statusNotesJobId_CGMass" name="statusNotesJobId_CGMass" value=""/>
				<div style="clear: both"></div>
				<div class="row mt5">  	
					<div class="span4 left16" >
					<div style="clear: both"></div>
			    		<div id='statusNoteFileNames_mass' style="padding-top:12px;">
		        			 <a href='javascript:void(0);' onclick='addStatusNoteFileType();'><img src='images/attach.png'/></a>
		        			 <a href='javascript:void(0);' onclick='addStatusNoteFileType();'>Attach a File</a>
		        		</div> 
			    	</div>
			    	<div class="span4" id="showStatusNoteFile_mass"  style="padding-top:4px;">&nbsp;
		        	</div>   
				</div>
				</form>
				
		 	</div> <!-- Main Div -->
		 	
		 	<div class="row" id="schoolAutoSuggestDivId_mass" style="display: none;">
            	<div class="col-sm-8 col-md-8">
            		<c:if test="${entityType eq 2}">
					  	<label>School Name<span class="required">*</span></label>
					  	<input type="text" id="schoolName_mass" class="form-control" maxlength="100" name="schoolName_mass" style="width: 657px;" placeholder="" onfocus="getSchoolAuto_djo(this, event, 'divTxtShowData2_mass', 'schoolName_mass','districtOrSchoolId','');"
							onkeyup="getSchoolAuto_djo(this, event, 'divTxtShowData2_mass', 'schoolName_mass','districtOrSchoolId','');"
							onblur="hideSchoolMasterDiv_djo(this,'districtOrSchoolId','divTxtShowData2_mass'),requisitionNumbers_mass();"	/>
		 				<input style='display:none;width:657px' type="text" id="schoolName_mass1" class="form-control" maxlength="100" name="schoolName_mass1"  placeholder="" onfocus="getSchoolAuto_djo(this, event, 'divTxtShowData2_mass', 'schoolName_mass1','schoolId_mass','');"
							onkeyup="getSchoolAuto_djo(this, event, 'divTxtShowData2_mass', 'schoolName_mass1','schoolId_mass','');"
							onblur="hideSchoolMasterDiv_djo(this,'schoolId_mass','divTxtShowData2_mass'),requisitionNumbers_mass();"	/>
							<!-- <input type="hidden" id="schoolId"/> -->
					 	  <input type="hidden" name="schoolId_mass" id="schoolId_mass" value=""/>  
						 <div id='divTxtShowData2_mass' style=' display:none;' onmouseover="mouseOverChk_djo('divTxtShowData2_mass','schoolName_mass')" class='result' ></div>
				 	</c:if>
		    	</div>
            </div>
            
            <div class="row" id="requisitionNumbersGrid_mass"  style="display: none;">
           		<c:if test="${entityType ne 1}">
					<label style='padding-top:10px;'>Position #/Requisition #<span class="required"  id='reqmassstar' sytle='display:none;'>*</span> (It is used if we are hiring a Candidate. In case of "In-Progress", Position #/Requisition # will not be saved)</label>
					<span id="requisitionNumbers_mass"> </span>
				</c:if>
            </div>
            <span id="spanOverride11_mass" style='display:none;'>            
         		<label class='checkbox inline'>
         			<input type='checkbox' name='chkOverride' id='chkOverride_mass' value='1'/>
         			Override the Status
         		</label>			    
            </span>
            
		 	<span id="checkOptions_mass" style='display:none;'>
	            <c:set var="disabledChecked" value=""></c:set>
	            <c:set var="hideView" value=""> </c:set>
			 	<c:if test="${entityType eq 1}">
			 		<c:set var="disabledChecked" value="disabled=\"disabled\""> </c:set>
			 		<c:set var="hideView" value="hide"> </c:set>
			 	</c:if>
			 	<div class="row mt10" id="email_da_sa_div">
	            	<div class="left16">
		            	<c:choose>
						      <c:when test="${DistrictId == '1200390'}">
						      	<label style="display: none;">
							 		<input type="checkbox" name="email_da_sa_mass" ${disabledChecked} id="email_da_sa_mass"  value="1"/>
			                 		Notify all the associated District and School Admins of the status change	                 	
			                 	</label>
			                 	<a href='javascript:void(0)' class="${hideView}" style="padding-left: 540px;" onclick="getStatusWiseEmailForAdmin_mass()">View/Edit Message</a>
						      </c:when>
						      <c:otherwise>
								<c:if test="${userMaster.entityType eq 5 || userMaster.entityType eq 6}">
								<label>
								 	<input type="hidden" name="email_da_sa" ${disabledChecked} id="email_da_sa"  value="1" /><!--
				                 	Notify all the associated District and School Admins of the status change	                 	
				                 --></label>
				                 <!--<a href='javascript:void(0)' class="${hideView}" style="padding-left: 90px;" onclick="getStatusWiseEmailForAdmin()">View/Edit Message</a>
	            			-->
	            			</c:if>
	            			
	            			<c:if test="${userMaster.entityType ne 5 && userMaster.entityType ne 6}">
	            			<label>
							 		<input type="checkbox" name="email_da_sa_mass" ${disabledChecked} id="email_da_sa_mass"  value="1"/>
			                 		Notify all the associated District and School Admins of the status change	                 	
			                 	</label>
			                 	<a href='javascript:void(0)' class="${hideView}" style="padding-left: 90px;" onclick="getStatusWiseEmailForAdmin_mass()">View/Edit Message</a>
	            			</c:if>
	            			</c:otherwise>
						</c:choose>
			    	</div>
	            </div>
            </span>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=655 border=0>
	 		<tr class="left15">
		 		<td width=115 nowrap align=left>
		 		<span id="statusSave_mass" style='display:none;'><button class="btn  btn-large btn-orange"  onclick='validateStatusInfo_cgmass(0)'>In-Progress</button>&nbsp;&nbsp;</span>
		 		</td>
		 		<td width=125  nowrap align=left>
			 		<span id="unHire_mass" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(2)'>UnHire </button></span>
			 		<span id="unDecline_mass" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(3)'>UnDecline </button></span>
			 		<span id="unRemove_mass" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(4)'>UnReject </button></span>
		 		</td>
		 		<td width=450  nowrap>
		 		<input type="hidden" id="statusNotesForStatus_mass" name="statusNotesForStatus" value="${statusNotes}"/>
				<span id="statusFinalize_mass" style='display:none;'><button class="btn  btn-large btn-primary" onclick='validateStatusInfo_cgmass(1)'>Finalize <i class="iconlock"></i></button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='showStatusDetailsOnClose()'>Cancel</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>

<!-- 3rd -->

<div class="modal hide" id="myModalEmail_mass" >
	<div class="modal-dialog-for-cgmessage">
        <div class="modal-content">	
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='sendOriginalEmail_mass()'>x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmail_mass'></div>
				</div>
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong>Subject</strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLine_mass" name="subjectLine_mass"class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBody_mass">
					    	<label><strong>Message</strong><span class="required">*</span></label>
				        	<textarea rows="5" class="form-control" cols="" id="" name="" style="margin-top: 0px;"></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn"  onclick='sendOriginalEmail_mass()'>Cancel</button>
		 		<button class="btn btn-primary"  onclick='return sendChangedEmail_mass();' >Ok</button>
		 	</div>
	      </div>
		</div>
	</div>
<!-- 4th -->

<div class="modal hide" id="myModalPreCheckInfoCGMass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog" style="width: 600px;">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="preCheckClose_mass();">x</button>
				<h3 id="myModalPreCheckInfoCGMassLabel"></h3>
			</div>
			<div class="modal-body">
				<div class='divErrorMsg' id='errordivCGMass' style="display: block;"></div>		
		 		</div> 	
		 	<div class="modal-footer">
		 		<button class="btn btn-large btn-primary" id="mass_cgInfo_Continue" style="display: none;" onclick="showStatusInfo_CGMass();">Continue <i class="icon"></i></button>
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="preCheckClose_mass();">Close</button> 		
		 	</div>
		</div>
	</div> 	
</div>

<!-- 5th -->
<div class="modal hide" id="myModalConfirmInfoCGMass" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog" style="width: 600px;">
		<div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="statusDetailsClose_mass();">x</button>
				<h3 id="myModalConfirmInfoCGMassLabel">Status</h3>
			</div>
			<div class="modal-body">
				<div class='divErrorMsg' id='errordivConfirmCGMass' style="display: block;"></div>		
		 		</div>
		 		<div class="control-group"></div>
		 	<div class="modal-footer">
		 		<button class="btn btn-large btn-primary" id="mass_cgInfo_Override" style="display: none;" onclick="userConfirmationCGMss(1);">Override <i class="icon"></i></button>
		 		<button class="btn btn-large btn-primary" id="mass_cgInfo_Skip" style="display: none;" onclick="userConfirmationCGMss(2);">Skip <i class="icon"></i></button>
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="statusDetailsClose_mass();">Close</button> 		
		 	</div>
		</div>
	</div> 	
</div>

<!-- 6 -->
<div  class="modal hide"  id="myModalMsgShowSuccessCGMass"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog' style="width: 600px;">
		<div class='modal-content'>
			<div class="modal-header">
	  			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="successClose_mass();">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='successMessageCGMass'>
					You have successfully changed status to the selected Candidates.
				</div>
 			</div>
	 		<div class="modal-footer">
	 			<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="successClose_mass()" >Ok</button>
	 		</div>
   		</div>
  	</div>
</div>

<div class="modal hide" id="tfaMsgShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'>
					x
				</button>
				<h3 id="myModalLabel">
					TeacherMatch
				</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='showTfaMsgBody'>
					You are attempting to change candidate portfolio information. This means you are altering the candidates application, which should only be done when there is confirmation that the candidate has included inaccurate information. The candidate will be alerted you are making this change. Do you really want to change this candidate's TFA status? 
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary" onclick="saveTFAbyUser();">Save<i class="icon"></i></button>
				<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeTFAMsgBox();'>Cancel</button>
			</div>
		</div>
	</div>
</div>

<div  class="modal hide distAttachmentShowDiv"  id="distAttachmentShowDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDiv();">x</button>
				<h3 id="UserProfileNameEEOC11">Attachment</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class="" id="divPnrAttachment">
					 
					</div>
					<iframe src="" id="ifrmAttachment" width="100%" height="480px"> </iframe>   
				</div>
				
		 	</div>
		 	<input type="hidden" id="eligibilityMasterId" value=""/>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv1()" >Close</button> 		
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<!-- Popup window -->
<div class="modal hide"  id="chgstatusRef1"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			Do you really want to activate the reference?.
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="chgstatusRef2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div id="deact" class="modal-body"> 		
		<div class="control-group">
			Do you really want to deactivate the reference?.
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="changeStatusElectronicReferencesConfirm()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide"  id="delVideoLnk" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Do you really want to delete Video Link?
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn  btn-primary" onclick="delVideoLnkConfirm()" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button> 		
 	</div>
</div>
	</div>
</div>

<input type="hidden" name="teacherIds" id="teacherIds" value="">
<div class="modal hide" id="tagsForMultipleCandidateDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 505px;">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> x </button>
				<h3> Apply Tags </h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div id="applyTagsId" name="applyTagsId"></div>
				</div>
			</div>
			
			<div class="modal-footer">
				<table>
				<tr>
					<td> <input type="radio" name="jobSpecificOrDistrictSpecific" checked="checked" value="job"> </td>
					<td style="text-align: left; padding-left: 5px;"> This tag applies to this job only. </td>
				</tr>
				
				<tr>
					<td> <input type="radio" name="jobSpecificOrDistrictSpecific" checked="checked" value="district"> </td>
					<td style="text-align: left; padding-left: 5px;"> This tag applies to the candidate and all jobs that this candidate has applied to. </td>
				</tr>
				
				<tr>
					<td colspan="2">
						<button class="btn" data-dismiss="modal" aria-hidden="true" style="float:right; margin-right:5px"> Close </button>
						<button class="btn btn-primary" type="button" onclick="saveApplyTags()" style="float:right; margin-right:5px"> Apply Tags&nbsp;<i class="icon"></i> </button>
					</td>
				</tr>
				</table>
			</div>
		</div>
	</div>
</div>

<div class="modal hide"  id="saveDataLoading" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			Data has been saved successfully. 
		</div>
 	</div>
 	<div class="modal-footer">
 	<button class="btn  btn-primary" data-dismiss="modal" aria-hidden="true">ok</button> 		
 	</div>
</div>
	</div>
</div>
<!-- new CG code -->

<div  class="modal hide"  id="divToggel"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog' style="width: 900px;">
		<div class='modal-content'>
			<div class="modal-header">
	  			<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="divToggelClose();">x</button>
				<h3 id="myModalLabel">Competency Domain Definitions</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id='successMessageCGMass'>
					
					
					<!--
					<table class='table-bordered'>
					<tr class='bg'>
					<td style='vertical-align: top; text-align: left; padding-left: 10px;padding-top:4px;' width='30%'  class='net-header-text'>
					Competency Domain Definitions
					</td>
					<td style='vertical-align: top; text-align: right; padding-right: 10px;' width='30%' height="54">
					<b><button onclick="divToggelClose();" type="button" class="close" data-dismiss="modal" aria-hidden="true"> <span   class='net-header-text'>x</span></button><br/>
					</td>
					</tr>
					-->
					<tr>
					<td colspan='2' style='text-align: left; padding-left: 10px;padding-right:10px;padding-top:3px;background-color:white;' width='30%'>
					<b>Candidate Grid Key</b>
					<p>This report shows candidates in rank order by <b>job</b> and <b>national</b> composite score across each EPI domain.</p>
					<p>The basis of EPI scores is the t-score, which for 99% of candidates will range from 20 - 80, has an average (<i>mean</i>) of 50, and a standard deviation of 10.</p>
					
					Candidate Pool And It's Predicted Performance
					<p><img src='images/legend_image.png'></p>
					<p>The advantage of reporting t-scores is that decision makers can interpret test scores across each EPI domain, as well as across the entire candidate group. For example, a proper interpretation for a national t-score of 60 is, "this candidate scored one standard deviation unit above the mean."</p>
					
					<b>Interpretation</b>
					<p>Candidates exhibiting higher scores are more likely to generate higher levels of student growth when compared to candidates with lower scores. A fair interpretation for a score of 60 (one standard deviation unit above the mean) is, "Analytics indicate this candidate is <b>extremely</b> likely to grow students faster, by one standard deviation unit, when compared to all other candidates in the comparison group."</p>
					
					<b>Definitions</b>
					<p><u>Job Score</u>:&nbsp;The job score represents a candidate's score within a <b>job</b>; use this score to compare a candidate's score to other candidates <i>applying for</i> the same job.</p>
					<p><u>National Score</u>:&nbsp;Scores in the <b>national</b> column are color-coded by decile ranking (each color signifies a decile), and represent the ranking for each candidate in the national group. Use this score to compare a candidate's score across all other candidates <i>in the nation</i>.</p>
					<p><u>Decile</u>:&nbsp;The decile rank (national column) represents which 10<sup>th</sup> percentile a score falls within. The deciles are the nine values that divide the data set into ten equal parts. The deciles determine the values for 10%, 20%... and 90% of the data.
					
					<div style='margin-left:30px;'><table>
					<tr><td>D9 = 90<sup>th</sup> percentile</td></tr>
					<tr><td>D8 = 80<sup>th</sup> percentile</td></tr>
					<tr><td>D7 = 70<sup>th</sup> percentile</td></tr>
					<tr><td>D6 = 60<sup>th</sup> percentile</td></tr>
					<tr><td>D5 = 50<sup>th</sup> percentile</td></tr>
					<tr><td>D4 = 40<sup>th</sup> percentile</td></tr>
					<tr><td>D3 = 30<sup>th</sup> percentile</td></tr>
					<tr><td>D2 = 20<sup>th</sup> percentile</td></tr>
					<tr><td>D1 = 10<sup>th</sup> percentile</td></tr>
					</table></div></p>
					
					<p>A teacher candidate's decile rank indicates how well that particular teacher scored on the EPI in relation to all of the teachers who have ever completed an EPI within the TeacherMatch system. Teachers whose scores place them at the upper percentiles are much more likely to catalyze higher levels of student growth than the teachers whose EPI scores place them at the lower percentiles of TeacherMatch's National Norm Ranking, as demonstrated by the following illustration:</p>
					<b>EPI Domains</b>	
					<p><u>Attitudinal</u>:&nbsp;This score represents the attitudinal factors affecting a candidate's ability to remain highly effective teachers. Competencies include, persistence in the face of adversity, maintaining a positive attitude, and motivation to succeed.</p>
					<p><u>Cognitive</u>:&nbsp;This score represents candidate's scores across three broad domains of general cognitive ability. Competencies include verbal ability, quantitative ability, and analytical reasoning</p>
					<p><u>Teaching Skills</u>:&nbsp;This score represents candidate's scores across four key dimensions of effective teaching, including, instructing, creating a learning environment, planning for successful outcomes, and analyzing and adjusting.</p>
					
					<!--
					</td>
					</tr>
					</table>
					-->
					
				</div>
 			</div>
	 		<div class="modal-footer">
	 			<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="divToggelClose()" >Ok</button>
	 		</div>
   		</div>
  	</div>
</div>

<input type="hidden" id="teacherJobId" value="">
<div class='modal hide'  id='tagDiv' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'></div>
<div id="disSpeDiv"></div>

<div class="modal hide" id="certTextDivDSPQ" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="certTextDivClose();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body" style="max-height: 400px;overflow-y:auto;"> 		
		<div class="control-group certTextContent">
			
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn" data-dismiss="modal" onclick="certTextDivClose();" aria-hidden="true">Close</button></span>&nbsp;&nbsp; 		
 	</div>
</div>
	</div>
</div>
<div class="modal hide"  id="myModal3"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">TeacherMatch</h3>
		</div>
		 <div  class="modal-body"> 		
			<div class="control-group" id='message2showConfirm'>
			</div>
	 	 </div>
	 	<div class="modal-footer" id='footerbtn'>
	       <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
	 	</div>
     </div>
	</div>
</div>
<div class="modal hide" id="onlineActivityDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='onlineActivityOnClose()'>x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title">Status</span></h3>
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;">
		<div class="row">
		<div class="col-sm-6 col-md-6">
			 <div class='divErrorMsg' id='errordivnlineActivityquestion' style="display: block;"></div>
		</div>
		</div>
		<div class="control-group">
			<div id="onlineActivityGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=680 border=0>
	 		<tr class="left15">
		 		<td width=680 align=right  nowrap>
				<span id="onlineActivityFinalize" style='display:inline;'><button class="btn  btn-large btn-primary" onclick='saveOnlineActivity(1)'>Save</button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='onlineActivityOnClose()'>Cancel</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>
<div class="modal hide" id="onlineActivityMSGDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='onlineActivityMSGOnClose()'>x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title">Status</span></h3>
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;">
		<div class="row">
		<div class="col-sm-12 col-md-12">
			We found that Score for one or more question(s) is 0 (zero). Please click on "Ok" button if you intentionally given that score or click on "Cancel" button if you want to change that score.
		</div>
		</div>
		<div class="control-group">
			<div id="onlineActivityGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=680 border=0>
	 		<tr class="left15">
		 		<td width=680 align=right  nowrap>
				<span id="onlineActivityFinalize" style='display:inline;'><button class="btn  btn-large btn-primary" onclick='saveOnlineActivity(2)'>Ok</button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='onlineActivityMSGOnClose()'>Cancel</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>
<div class="modal hide" id="schoolSelectionDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title">School Selection</span></h3>
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;">
		<div class="control-group">
			<div id="schoolSelectionDivGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=680 border=0>
	 		<tr class="left15">
		 		<td width=680 align=right  nowrap>
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" >Close</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>
<div class="modal hide" id="modalAssessmentInvite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title">Assessment Details</span></h3>
	</div>
	<div class="modal-body" style=" max-height: 400px;padding-left: 33px;">
		<div class="control-group" >
		<div id="focus"></div>
			<div id="assessmentDivGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=680 border=0>
	 		<tr class="left15">
		 		<td width=680 align=right  nowrap>
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" >Close</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>
<div class="modal hide" id="modalAssessmentDetails" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:1050px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeAssessmentDetails();">x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title">Assessment Details</span></h3>
	</div>
	<div class="modal-body" style=" max-height: 450px;overflow-y: auto;">
		<div class="control-group">
			<div id="assessmentDetailsDivGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=990 border=0>
	 		<tr class="left15">
	 		<td align=right style="width:92%">
	 		<button class="btn btn-large btn-primary" id="saveScore" type="button" onclick="setScoreForQuestion();" style="display:none;"><strong>Save <i class="icon"></i></strong></button>
	 		</td>
		 		<td align=right  nowrap>
		 		<button class="btn"  onclick="closeAssessmentDetails();">Close</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>

<div class="modal hide"  id="successInvite"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5100;" data-backdrop="static">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeSuccessInvite();">x</button>
			<h3 id="myModalLabel">TeacherMatch</h3>
		</div>
		 <div  class="modal-body"> 		
			<span><b>Invitation Sent Successfully.</b></span>
	 	 </div>
	 	<div class="modal-footer" id='footerbtn'>
	       <button class="btn btn-primary" onclick="closeSuccessInvite();">Ok</button>
	 	</div>
     </div>
	</div>
</div>

<div class="modal hide"  id="successMultiInvite"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5100;" data-backdrop="static">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeSuccessMultiInvite();">x</button>
			<h3 id="myModalLabel">TeacherMatch</h3>
		</div>
		 <div  class="modal-body"> 		
			<span><b>Invitation Sent Successfully.</b></span>
	 	 </div>
	 	<div class="modal-footer" id='footerbtn'>
	       <button class="btn btn-primary" onclick="closeSuccessMultiInvite();">Ok</button>
	 	</div>
     </div>
	</div>
</div>


<!--=========================================Phone Interview=========================================================== -->
	<div class="modal hide"  id="myModalPhoneInterview" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog-for-cgdemoschedule" style="width:90%;"> 
		<div class="modal-content"> 
		<div class="modal-header">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel">Phone Interview</h3>
		</div>      
	      
		      <div class="modal-body" style="max-height:450px;overflow-y:scroll;">
		      		<div class="row">
		      			<div class="col-sm-12 col-md-12">
							<div class='divErrorMsg' id='eventArrordiv' style="display: block;"></div>
						</div>
					</div>
		      		<input type="hidden" id="eventId" name="eventId" />
					<input type="hidden" id="districtIdforEvent" value="${districtIdforEvent}" />
					<input type="hidden" id="teacherIdforEvent" name="teacherIdforEvent"/>
					<input type="hidden" id="chkDATFlag" name="chkDATFlag">
					<input type="hidden" id="inputDivCount" name="inputDivCount" value="1">
					<input type="hidden" id="deleteScheduleId" name="deleteScheduleId" >
			   <div id="addEditEventDiv">
				 <div class="row">
					<div class="col-sm-8 col-md-8">
						<label>
							Event Name<span class="required">*</span>
						</label>
						<input class="form-control" type="text" id="eventName" name="eventName" maxlength="200" />
					</div>
				</div>

			<div class="row" id="scheduleTypeDiv">
				<div class="col-sm-6 col-md-6">
					<label>
						Schedule Type<span class="required">*</span> <a href="#"  id="evtsch" rel="tooltip" data-original-title="Event Schedule"><img width="15" height="15" alt="" src="images/qua-icon.png"></a>
					</label>
					  <span id="intschedule">
					  </span>
					
					</div>
			</div>

		  <div class="row hide" id="desDiv">
				<div class="control-group col-sm-8 col-md-8" id="eventDescriptionCG">
						<label>
							<strong> Description</strong><span class="required">*</span>
						</label>
						<textarea rows="8" class="form-control" cols="" id="eventDescription"
							name="eventDescription" maxlength="1000"></textarea>
				</div>
			</div>


			<div class="row" id="messagetoprincipleDiv">
				<div class="col-sm-4 col-md-4" id="">
					<label>
						Message Template for Candidate(s)
					</label>
					<select class="form-control " id="messagetoprinciple"
						name="messagetoprinciple" class="span3"
						onchange="getEmailDescription()">
						<option value="" selected="selected">
							Select Message
						</option>
					</select>
				</div>
			</div>

			<div class="row" id="subjectDiv">
				<div class="col-sm-8 col-md-8" id="">
					<label>
						Subject<span class="required">*</span>
					</label>
					<input type="text" id="subjectforParticipants"
						name="subjectforParticipants" maxlength="100" class="form-control" />
				</div>
			</div>
	
			<div class="row" id="msgtoparticipantdiv">
				<div class="control-group col-sm-8 col-md-8" id="msgforParticipantsCG">
						<label>
							<strong>Message to Candidate(s)<span class="required">*</span>
							</strong>
						</label>
						<textarea rows="8" class="form-control" cols="" id="msgtoparticipants"
							name="msgtoparticipants" maxlength="1000"></textarea>
		 		</div>
			</div>

				<!-- Msg to Facilatator Div Start -->
				<div class="row" id="msgtofacilitatorsDiv1">
					<div class="col-sm-4 col-md-4" id="">
						<label>
							Message Template for Facilitator(s)
						</label>
						<select class="form-control " id="msgtofacilitatorss"
							name="msgtofacilitatorss" class="span3"
							onchange="getEmailDescription1()">
							<option value="" selected="selected">
								Select Template
							</option>
						</select>
					</div>
				</div>
			
				<div class="row" id="subjectforFacilatorDiv">
				   <div class="col-sm-8 col-md-8" id="">
						<label>
							Subject<span class="required">*</span>
						</label>
						<input type="text" id="subjectforFacilator" name="subjectforFacilator" maxlength="100" class="form-control" />
				   </div>
				</div>
		
			<div class="row" id="msgtofacilitatorsDiv2">
				<div class="control-group col-sm-8 col-md-8" id="messtoFaciliatatorCG">
						<label>
							<strong>Message to Facilitator(s)<span class="required">*</span>
							</strong>
						</label>
						<textarea rows="8" class="form-control" cols="" id="msgtofacilitators"
							name="msgtofacilitators" maxlength="1000"></textarea>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-8 col-md-8 top15">
					<div class="row">
						<div class="col-sm-6 col-md-6">
							<!--<button onclick="savePhoneEvent()" class="btn btn-warningevents" id="save"><strong>Save<i class="icon"></i></strong></button>-->
						</div>	
						<div class="col-sm-6 col-md-6" style="text-align: right;">
						     <button onclick="savePhoneEvent()" class="btn btn-primary" id="save"><strong>Next <i class="icon"></i></strong></button>
						</div>
						</div>
					</div>
				</div>
           </div><%--Event Creation Body Closed --%>
			
		   <%--Event Schedule  div start --%>
			
		   <div id="facilitatorAndScheduleDiv" class="hide">
		      	<div class="pull-right add-employment1" id="addScheduleLink">
					<a href="javascript:void(0);" onclick="return addSchedule()">+Add Schedule</a>
				</div>
		      <div id="eventScheduleDivMain" class="top10">
			  </div>
			  <div class="row" id="addScheduleLinkForSlots">
				<div class="col-sm-6 col-md-6 top15">
						<a href="javascript:void(0);" onclick="return addSchedule()">+Add Slot</a>
				</div>
			 </div>	
			   <div class="row col-sm-12 col-md-12 top10" >
			        <div id="facilitatorNewDivCG"  class="row">
	                                       
	                </div>
			   </div>
			
			   <div class="row top10" >
				     <div class="col-sm-12 col-md-12">
						<span class=""><label class="">Facilitator Name<span class="required">*</span></label>
					     <select id="facilitatorId" name="FacilitatorId" class="form-control help-inline" onchange="addFacilitatorCG();">
					     	<option value="">Select Facilitator(s)</option>
					     </select>
					   	 </span>
					</div>
			    </div>
			 <div class="row top10">
	 			<div class="col-sm-6 col-md-6">
			       <button class="btn btn-primary" type="button" onclick="saveSchedule()">Send Invite&nbsp;<i class="icon"></i></button>
			   </div>
			   <div class="col-sm-6 col-md-6" style="text-align: right;">
			       <button class="btn btn-primary" type="button" onclick="showAddEditPhoneEvents()"><i class="backicon"></i> Back &nbsp;</button>
			   </div>
			 </div> 
		   </div>
			
			
		      </div>  <!-- /.end body -->
		      <div class="modal-footer">
			      <!--<div class="pull-left" style='margin-left:2px;margin-top: 0px;'>
			      		<button type="button" class="btn f1 btn-secondary" id="canEvtBtn" onclick="cancelEvent();">Cancel Event</button>
			      </div>
			      
		     	 --><div  class="pull-right">
		       		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="return exitfrompageforEventOnCG();">Close</button> 
		        	<!--<button type="button" class="btn btn-primary" id="doneBtn" onclick="saveDemoSchedule(false);">Done</button>
		          --></div>
		      </div>
	  </div>
</div>
</div>	  
	  
<!--==================================================================================================== -->
<div  class="modal hide"  id="myModalactMsgShow"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showMessageDiv()">x</button>
		<h3 id="myModalLabel">Teacher Match</h3>
	</div>
	<div class="modal-body">
		<div class="control-group" id='confirmDeactivate'>
		Do you really want to remove the  facilitator from this event?
		</div>
 	</div>
 	<div class="modal-footer">
 		<span id=""><button class="btn  btn-primary" onclick="removeFacilitatorById();">Yes <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideMessageDiv()">No</button>
 	</div>
 	<input type="hidden" id="facilitatordelId" name="facilitatordelId">
 </div> 	
</div>
</div>



<!-- modal divs end -->
<div class="modal hide"  id="sendEmailtoParticipants" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="return refreshCGonEvent();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group tempMsg" id="succesEmailMsg">
			Great! Event has been scheduled
		</div>
 	</div>
 	<div class="modal-footer" id="popupFooter">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="return refreshCGonEvent();">Close</button> 		
 	</div>
</div>
	</div>
</div>
<div style="clear: both;"></div>

<div class="modal hide" id="multiInviteAssessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title">Assessment Invite</span></h3>
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;padding-left: 67px;">
		<div class="control-group">
		    <div class="row">
		    <div class="col-sm-12 col-md-12">
		    	<div class='divErrorMsg' id='multiInviteErrorDiv' style="display:none;"></div>
		    </div>
			</div>
			<div class="row">
			<div class="col-sm-12 col-md-12">
				<div id="inviteGridData"></div>
				</div>
			</div>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=680 border=0>
	 		<tr class="">
	 			<td width=680 align=right  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" >Close</button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>

	 <iframe src=""  style="display: none;" id="ifrmletterOfIntent" width="100%" height="100%" scrolling="no">
			 </iframe>    

<div class="modal hide" id="scoreUpdateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="return hideScoreUpdateDiv();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group" id="successScoringMsg">
			You have successfully scored the Candidate.
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="return hideScoreUpdateDiv();">Close</button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide" id="scoreUpdateErrorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5999;"   data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="getFocus();">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group" id="errorScoringMsg">
			Please suggest the reason of updating the score in Note field.
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="getFocus();">Ok</button> 		
 	</div>
</div>
	</div>
</div>

<div class="modal hide" id="myModalReferenceCheckForPrint">
		<div class="modal-dialog" style="width:925px;">
        <div class="modal-content">
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeReferenceCheck(6)'>x</button>
				<h3 id="statusTeacherTitleReferenceChk_1">e-Reference</h3>
			</div>
			<div class='divErrorMsg left2' id='errorStatusReferenceCheck' style="display: block; padding-left: 15px;"></div>
			<div  class="modal-body" >
			
			<div class="" style="height:450px; overflow-y:scroll;">
				<div class="" id="referencePrintData">

				</div>
				</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<span id="statusFinalize" style="display: inline;">
		 		<button class="btn  btn-large btn-primary" onclick="printReferenceDataToWindow()">Print <i class="iconlock"></i></button>&nbsp;&nbsp;</span>
		 		<button class="btn"  onclick='closeReferenceCheck(6)'>Cancel</button>
		 	</div>
	    </div>
	</div>
</div>
</div>

<div class="modal hide" id="myModal222" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog-for-cgsearch" style="width: 380px;">
		<div class="modal-content">		
			<div class="modal-header">
 				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="showAndHideModal();">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body"> 		
				<div id='msg2show'>This Certificate is already selected.</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary"" data-dismiss="modal" aria-hidden="true"  onclick="showAndHideModal();">Ok</button> 
			</div>
		</div>
	</div>
</div>

<div class="modal hide"  id="printmessage1"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
	   <h3 id="myModalLabel">Teachermatch</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" >
			 Please make sure that pop ups are allowed on your browser.
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true">Ok</button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>


<%--Mukesh Applying Multiple jobs --%>

<div class="modal hide" id="myJobDetailListFromCG" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog' style="width: 1012px">
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3>Apply/Add Candidate(s) to Other Job(s)</h3>
			</div>
			
			<div class="modal-body">
				<div class="control-group">
					<div class="row">
						<div class="col-sm-8 col-md-8">
							<div class="row">
								<div class="col-sm-6 col-md-6">
									<c:if test="${DistrictName!=null}">
									<label><strong>District</strong></label>									
										<input type="text" class="form-control" value="${DistrictName}" disabled="disabled" />
									</c:if>
									<c:if test="${not empty HeadQuarterName}">
									<label><strong>Headquarter</strong></label>									
										<input type="text" class="form-control" value="${HeadQuarterName}" disabled="disabled" />
									</c:if>
								</div>
								<%-- 12-6 for open school filter--%>
								<c:if test="${entityType ne 5 && entityType ne 6}">
								<div class="col-sm-6 col-md-6">
									<c:choose>
										<c:when test="${schoolName==null}">
											<label>School Name</label>
											<input type="text" id="schoolNameApply" maxlength="100"
												name="schoolName1" class="form-control" placeholder=""
												onfocus="getSchoolAuto(this, event, 'divTxtShowDataApply', 'schoolNameApply','districtId','');"
												onkeyup="getSchoolAuto(this, event, 'divTxtShowDataApply', 'schoolNameApply','districtId','');"
												onblur="hideSchoolMasterDiv(this,'schoolIdApply','divTxtShowDataApply');" />
											<input type="hidden" id="schoolIdApply" value="0" />
											<div class="span5" id='divTxtShowDataApply'
												onmouseover="mouseOverChk('divTxtShowDataApply','schoolNameApply')"
												style='display: none; position: absolute; z-index: 5000; margin: 0px;'
												class='result'></div>
										</c:when>
										<c:otherwise>
											<label>
												School Name
											</label>
											<input type="text" class="form-control" id="schoolName1"
												value="${schoolName}" disabled="disabled" />
											<input type="hidden" id="schoolIdApply" value="${SchoolId}" />
										</c:otherwise>
									</c:choose>
								</div>
								</c:if>
								
								<c:if test="${entityType eq 5 || entityType eq 6}">
								<c:set var="disab" value=""></c:set>
								<c:if test="${entityType eq 6}">
									<c:set var="disab" value="readonly"></c:set>
								</c:if>
									<div class="col-sm-6 col-md-6">
										<label class="">Branch Name</label>
										<input type="text" id="branchName" name="branchName"  class="form-control" value="${BranchName}" 
					       					onfocus="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchSearchId','');"
											onkeyup="getBranchMasterAutoComp(this, event, 'divTxtShowDataBranch', 'branchName','branchSearchId','');"
											onblur="hideBranchMasterDiv(this,'branchSearchId','divTxtShowDataBranch');" ${disab}/>
											<div id='divTxtShowDataBranch' style='display:none;position:absolute;z-index:5000;' onmouseover="mouseOverChk('divTxtShowDataBranch','branchName')" class='result' ></div>
											<input  type="hidden" id="branchSearchId" name="branchSearchId" value="${branchId}" />								      
									</div>
								</c:if>
								
								<div class="col-sm-6 col-md-6">
									<span class=""><label class="">Job Category</label>
										 <select class="form-control " id="jobCategoryIdSearch" name="jobCategoryIdSearch" onchange="activecityType();">
											<option value="0">All job category</option>
											  <c:if test="${not empty lstJobCategoryMasters}">
												<c:forEach var="jobCateObj" items="${lstJobCategoryMasters}">
												 	<c:choose>
													    <c:when test="${jobCatId == jobCateObj.jobCategoryId}">
															<option value="${jobCateObj.jobCategoryId}" selected>
																${jobCateObj.jobCategoryName}
															</option>
													    </c:when>
														<c:otherwise>
															<option value="${jobCateObj.jobCategoryId}">
																${jobCateObj.jobCategoryName}
															</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:if>
										</select> 
									</span>
								</div>
								<div class="col-sm-6 col-md-6">
									<label>Job Id</label>
									<input class="form-control" type="text" name="jobIdSearch" id="jobIdSearch" onkeypress="return checkForInt(event);" />
								</div>
							</div>
						</div>
						
						<div class="col-sm-4 col-md-4">
							<label>Subject</label>
							<select class="form-control" multiple="multiple" name="subjects1" id="subjects1" style="height: 90px;">
								<option value="0">All</option>
								<c:if test="${not empty subjectMasters}">
									<c:forEach var="subjectMaster" items="${subjectMasters}">
										<option value="${subjectMaster.subjectId}">
											${subjectMaster.subjectName}
										</option>
									</c:forEach>
								</c:if>
							</select>
						</div>
					</div>

					<div class="row top15" style="margin-top: 0px;">
						<div class="col-sm-4 col-md-4">
							<label>Zone</label>
								<select class="form-control" name="zone" id="zone">
								<option value="0">All</option>
									<c:if test="${not empty listGeoZoneMasters}">
											<c:forEach var="geoZoneObj" items="${listGeoZoneMasters}">
												<option value="${geoZoneObj.geoZoneId}">
													${geoZoneObj.geoZoneName}
												</option>
											</c:forEach>
									</c:if>
								
								</select>
						</div>
						<div class="col-sm-3 col-md-3">
							<button class="btn  btn-primary top25-sm" onclick="getJobDetailListFromCGBySearch();">Search<i class="icon"></i></button>
						</div>
					</div>

					<div id="divJobListOnCG" class="top15">

					</div>
				</div>
			</div>
			<div class="modal-footer">
				<div class="col-sm-3 col-md-3" align="left">
					<label class="checkbox">
					    <input type="checkbox" name="sendNotificationOnApply" id="sendNotificationOnApply" value="1">
					    Auto Notify Candidate(s)
                    </label>
				</div>
			
				 <span id="">
				   <button class="btn btn-primary" onclick="applyJobOnCG()">Apply to Job(s)<i class="icon"></i></button>
				</span>&nbsp;&nbsp;
				<button class="btn" data-dismiss="modal" aria-hidden="true">
				   Cancel
				</button>
			</div>
		</div>
	</div>
</div>


<div class="modal hide"  id="alertMsgForApply"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:450px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
	   <h3 id="myModalLabel">Teachermatch</h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" id="alertmsgToShow">
				
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true">Ok</button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>


<div class="modal hide"  id="octContentDiv"  tabindex="-1" role="dialog" data-keyboard="false" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static" >
	<div class="modal-dialog" style="width:950px; margin-top: 0px;">
	<div class="modal-content">
	<div class="modal-header">
	   	<button type="button" class="close" data-dismiss="modal" onclick="showPrevProfile();" aria-hidden="true">x</button>
	   <h3 id="myModalLabel" ><span id="changeByName"></span></h3>
	</div>
	
	<div class="modal-body" style="max-height: 400px; overflow-y: auto;"> 		
			<div class="control-group">
			<div class="" id="octmsgToShow">
				
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
	 	<table border=0 align="right">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" onclick="showPrevProfile();" aria-hidden="true">Ok</button>
				</td>
				
	 		</tr>
	 	</table>
	</div>
  </div>
 </div>
</div>



<div class='modal hide in' id='eRef' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
			<div class='modal-dialog' style='width:1000px;'>
			<div class='modal-content'>
			<div class='modal-header' style="height:50px;">
	        <button type='button' class='close confirmFalse' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
	        
	        
				       		 <!-- /****************************************heading***************************************/ -->
							  <div class="row" style="margin:0px;color:white;">
							      <div style="float: left;">
							       	<img src="images/manageusers.png" width="41" height="41" alt="">
							       </div>        
									<div style="float: left;">
										<div class="subheading" style="font-size: 13px;color:white;">e-References</div>	
									</div>
								<c:if test="${districtMaster ne null}">
									<c:choose>
										<c:when test="${districtMaster.displayName eq null}">
											<div class="pull-right add-employment1" style="font-size: 13px;color:white;margin-top:-.7%;">${districtMaster.districtName}</div>							
										</c:when>
										<c:when test="${districtMaster.displayName eq ''}">
											<div class="pull-right add-employment1" style="font-size: 13px;color:white;margin-top:-.7%;">${districtMaster.districtName}</div>							
										</c:when>
										<c:otherwise>
											<div class="pull-right add-employment1" style="font-size: 13px;color:white;margin-top:-.7%;">${districtMaster.displayName}</div>	
										</c:otherwise>
									</c:choose>
								</c:if>
								 		
								<div style="clear: both;"></div>
							   	<div class="centerline"></div>
							</div>
							<!-- /****************************************heading***************************************/ -->
	
	        <!--<h4 class='modal-title' id='myModalLabel' style='color:white;float:left;'>e-References</h4><!-- <h4 class='modal-title' id='myModalLabel1' style='color:white;float:right;'>e-References</h4>-->
	      </div>
	      <div class='modal-body' id='confirmMessage'>
				<div>
				<input type='hidden' id='elerefAutoId' value='' name='elerefAutoId' />
				<input type='hidden' id='districtId' value='' name='districtId' />
				<input type='hidden' id='jobId' value='' name='jobId' />
				<input type='hidden' id='teacherId' value='' name='teacherId' />  
				<input type='hidden' id='userId' value='' name='userId' />
				
					<div class='divErrorMsg' id='errordiv4question'	style="display: block; padding-bottom: 7px;"></div>
					<table width="100%" border="0" class="table table-bordered table-striped" id="tblGrid_eref">

					</table>

					<!--<button class="btn btn-large btn-primary" type="button" onclick="setDistrictQuestions1('dashboard');">
						<strong>Continue <i class="icon"></i></strong>
					</button>-->

				</div>
			</div>
	      <div class='modal-footer'>
	      <button type='button' class='btn btn-primary' id='confirmTrue' onclick="setDistrictQuestions1('dashboard');"><strong>Continue <i class="icon"></i></strong></button>
	        <button type='button' class='btn btn-default confirmFalse' >Cancel</button>	        
	      </div>
	    </div>
	  </div>
	</div>
	
	
	<div class="modal hide" id="myModalReferenceCheckConfirm1" >
		<div class="modal-dialog" style="width:728px;">
	        <div class="modal-content">		
				<div class="modal-header" style="height:50px;">
			  		<button type="button" class="close cancelERef" data-dismiss="modal" aria-hidden="true" >x</button>
					<h3 id="statusTeacherTitle">e-References Confirmation</h3>
				</div>
				<div class='divErrorMsg left2' id='errorStatusReferenceCheck1' style="display: block; padding-left: 15px;"></div>
				<div  class="modal-body">
					<div class="" id="referenceRecord">
						One or more scores has the value of zero.  If this is not intended, please review your entry.  To confirm that the scores should remain zero, press continue<BR/>
					</div>
			 	</div>

			 	<div class="modal-footer">
			 		<button class="btn btn-large btn-primary" type="button" onclick="setDistrictQuestions1('auto');"><strong>Continue <i class="icon"></i></strong></button>
			 		<button type="button" class="btn cancelERef"  >Cancel</button>
			 	</div>
		    </div>
		</div>
	</div>
	
	<div class="modal hide" id="confirmationMessageForPrintResume" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog-for-cgcoverletter" style='width: 35%;'>
		<div class="modal-content">
			<div class="modal-header">
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body" style="max-height: 400px;overflow-y:auto;">
				<div class="control-group">
				</div>
			</div>
			<div class="modal-footer">
				<span id=""><button class="btn btn-primary" data-dismiss="modal" onclick="showConfirmationMessageForPrintResumeOk()">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
			</div>
		</div>
	</div>
	</div>


<!-- SLC Report -->
<div  class="modal hide pdfDivBorder"  id="modalDownloadCGSLCReport" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
	<div class="modal-content"  style="background-color: #ffffff;">
	
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" >x</button>
		<h3 id="myModalLabelSLCNotes">SLC Notes</h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
		
		  <div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   <iframe id="ifrmCGSLCR" width="100%" height="480px" src=""></iframe>		  
		  </div>   
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" >Close</button> 		
   </div>
  </div>
 </div>
</div>
	
<script type="text/javascript">//<![CDATA[

      var cal = Calendar.setup({
          onSelect: function(cal) { cal.hide() },
          showTime: true
      });
    
     cal.manageFields("epiFromDate", "epiFromDate", "%m-%d-%Y");
     cal.manageFields("epiToDate", "epiToDate", "%m-%d-%Y");
     
    //]]>
</script>

<!-- Smart Practices Certificate modal div start -->
<div class="modal hide pdfDivBorder" id="modalDownloadSPC" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog-for-cgpdfreport">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
				<h3 id="myModalLabel">Smart Practices Certificate</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
		  			<div style="width: 100%; height: 100%; -webkit-overflow-scrolling: touch;">
		   				<iframe id="ifrmSPC" width="100%" height="480px" src=""></iframe>
		  			</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
   			</div>
  		</div>
	</div>
</div>
<!-- Smart Practices Certificate modal div end -->

<input type="hidden" id="gridNo" name="gridNo" value="">
<div  class="modal hide" onclick="getSortSecondGrid()" id="myModalforSchhol" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog" style="width: 742px;">
	<div class="modal-content">
	<div class="modal-header" id="schoolHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel"><spring:message code="headSchL"/></h3>
	</div>
	<div class="modal-body" id="schoolListDiv">
		
	</div> 	
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
 	</div>
</div>
</div>
</div>

<!--  Gaurav Kumar -->

<div class="modal hide" id="eeocSupportModal" style="border: 0px solid blue;" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width:720px; ">
	<div class="modal-content">
	<div class="modal-header" id="qualificationDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='showProfilePopup();'>x</button>
		<h3 style="padding-left: 5px;"><spring:message code="lblEeocSupport"/></h3>
	</div>
	
	<div id="eeocSupportDiv" class="modal-body">
	
	</div>
 	<div class="modal-footer">
	<button type="button" class="btn" data-dismiss="modal" aria-hidden="true"  onclick='showProfilePopup();'><spring:message code="btnClsoe"/></button>
    </div>
   </div>
  </div>
 </div>
 
 <style>
 .profileContent input {
    float: left;
}

.eeoc-label {
    margin-left: 3px;
}
 </style>

<script>
//alert($('#myModalforSchhol').is(':visible'));

$('#myModalforSchhol').on('hide', function () {
  
 // getSortFirstGrid();
});
$( document.body ).click(function() {
    if($('#myModalforSchhol').is(':visible'))
    {
    	
    	$('#gridNo').val("2");
    }
    else
    {
    	$('#gridNo').val("1");
    }
    
});

</script>

<!-- Smart Practices Reset modal div start -->
<div class="modal hide" id="modalResetSp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<input type="hidden" id="teacherIdForResetSp" name="teacherIdForResetSp" value="">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeResetSp();">x</button>
				<h3 id="myModalLabel">Smart Practices Reset</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivResetSp' style="display: block;"></div>
					<div  id="resetSpDescription"><label><strong><spring:message code="lblResetSp"/><span class="required">*</span></strong></label><br/><br/>
<!--						<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>-->
						<textarea class="form-control" name="msgResetSp" id="msgResetSp" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnResetSp" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" onclick="closeResetSp();">Cancel</button>
   			</div>
  		</div>
	</div>
</div>

<div class="modal hide" id="modalResetSpMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeResetSp()">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id="resetMsg2Show"></div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"	onclick="closeResetSp()">Ok</button>
			</div>
		</div>
	</div>
</div>

<div class="modal" id="myModal23" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
      </div>
      <div class="modal-body">
       <div class="control-group" id='message23show'></div>
      </div>
      <div class="modal-footer">
        	<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
      </div>
    </div>
  </div>
</div>
<!-- Smart Practices Reset modal div end -->

<div class="modal hide"  id="hideTalentDiv" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog" style="width: 532px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="internalTxt">
			   Do you want to hide these talent?
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideTheseTalents();">Yes</button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="cancelPopUpHideTalent();">Cancel</button></span> 		
 	</div>
  </div>
</div>
</div>
<div class="modal hide"  id="hideTalentSuccessDiv" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog" style="width: 532px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="internalTxt">
			   Talent hidden successfully.
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button></span>		
 	</div>
  </div>
</div>
</div>
<div class="modal hide"  id="unhideTalentDiv" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog" style="width: 532px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="internalTxt">
			   Do you want to unhide these talent?
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="unhideTheseTalents(0);">Yes</button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="cancelPopUpUnhideTalent();">Cancel</button></span> 		
 	</div>
  </div>
</div>
</div>
<div class="modal hide"  id="unhideTalentMessageDiv" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog" style="width: 532px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="internalTxt">
			   You have selected other than Hidden talents. Do you want to unhide these talents?
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="unhideTheseTalents(1);">Yes</button></span>
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true" onclick="cancelPopUpUnhideTalentMessage();">Cancel</button></span> 		
 	</div>
  </div>
</div>
</div>
<div class="modal hide"  id="unhideTalentSuccessDiv" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" aria-hidden="true"  data-backdrop="static" >
	<div class="modal-dialog" style="width: 532px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabel">TeacherMatch</h3>
	</div>
	<div class="modal-body">				
		<div class="control-group">
			<div class="" id="internalTxt">
			   Talent Unhidden successfully.
			</div>
		</div>
 	</div> 	
 	<div class="modal-footer">
 		<span><button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button></span>		
 	</div>
  </div>
</div>
</div>

<!-- Assessment Reset modal div start -->
<div class="modal hide" id="modalResetAssessment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<input type="hidden" id="teacherIdForResetAssessment" name="teacherIdForResetAssessment" value="">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeAssessmentResetDiv();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivResetAssessment' style="display: block;"></div>
					<div  id="resetAssessmentDescription"><label><strong><spring:message code="lblResetSp"/><span class="required">*</span></strong></label><br/><br/>
<!--						<label class="redtextmsg"><spring:message code="msgHowToCopyPastCutDouc"/></label>-->
						<textarea class="form-control" name="msgResetAssessment" id="msgResetAssessment" maxlength="750" rows="4" onkeyup="return chkLenOfTextArea(this,750);" onblur="return chkLenOfTextArea(this,750);"></textarea>
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnResetAssessment" >Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" onclick="closeAssessmentResetDiv();">Cancel</button>
   			</div>
  		</div>
	</div>
</div>

<div class="modal hide" id="modalResetAssessmentMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class='modal-dialog'>
		<div class='modal-content'>
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="closeAssessmentResetDiv()">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group" id="resetAssessmentMsg2Show"></div>
			</div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"	onclick="closeAssessmentResetDiv()">Ok</button>
			</div>
		</div>
	</div>
</div>
<!-- Assessment Reset modal div end -->

<div class="modal hide verifyTranscriptDiv" id="verifyTranscriptDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="table-responsive">
		<div class='onr-dialog'>
			<div class='modal-content'>
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideDivTranscript(1);">x</button>
				<h3><span id="myModalLabelTrans"></span></h3></h3>
			</div>
			 <div class="modal-body">
			 	<div class='divErrorMsg left2' id='errorStatusVerifyTranscript' style="display: block;"></div>
				<div class="control-group">
					<div class="" id="divVerifyTrans">

					</div>
				</div>
			  </div>
		 	<div class="modal-footer">
		 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="hideDivTranscript(1);"><spring:message code="btnClose"/></button>
		 	</div>
		  </div>
		 </div>
	</div>
</div>

<div class="modal" id="myModaTranscript" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel">TeacherMatch</h3>
				</div>
				<div class="modal-body">
					<div class="control-group" id="messageTranscriptShow"></div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="hideDivTranscript(2);"><spring:message code="btnOk"/></button>
				</div>
			</div>
		</div>
</div>

<input type="hidden" id="jobIdSchool" name="jobIdSchool" value=""/>
<div  class="modal hide"  id="myModalforSchholList" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" style="width: 742px;">
	<div class="modal-content">
		<div class="modal-header" id="schoolHeader">
	  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
			<h3 id="myModalLabel"><spring:message code="headSchL"/></h3>
		</div>
		<div class="modal-body" id="schoolListDivNew">
			
		</div> 	
	 	<div class="modal-footer">
	 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnOk"/></button>
	 	</div>
    </div>
  </div>
</div>

<div class="modal hide" id="disconnectResponse" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="myModalLabel">TeacherMatch</h3>
				</div>
				<div class="modal-body">
					<div class="control-group" id="disconnectRespMsg"></div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="hideDisconnectResponse();"><spring:message code="btnOk"/></button>
				</div>
			</div>
		</div>
</div>

<!--by brajesh-->
<div class="modal hide" id="confrmmodalKSNId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"  aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick='closeDeclineTalentDiv();'>x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="internalTxt">
			  <center><spring:message code="lblDismissTxt"/></center>
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary"  data-dismiss="modal" aria-hidden="true" id="btnKSNButtonId" onclick="declinedKsnIds();"><spring:message code="lblCreate"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" data-dismiss="modal" aria-hidden="true"  id="clnId"><spring:message code="btnClr"/> </button>
   			</div>
  		</div>
	</div>
</div>

<div class="modal hide" id="confrmteacheridTmTlntId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeTalentDetailListDiv();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="internalTxt">
			  <center><spring:message code="lblConfrmMSGT"/></center>
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary"  data-dismiss="modal" aria-hidden="true" id="btnKSNAPIButtonId">Ok</i></button></span>&nbsp;&nbsp;
 				<!--<button class="btn" data-dismiss="modal" aria-hidden="true"  id="clnKSNId"><spring:message code="btnClr"/> --></button>
   			</div>
  		</div>
	</div>
</div>

<div class="modal hide" id="modalSelectKSNId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeTalentDiv();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="internalTxt">
			  <center>Please select a candidate from the list to "Link to KSN".</center>
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" data-dismiss="modal"  id="oktochoseKSNNO"  onclick='closeTalentDiv();'>Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				
   			</div>
  		</div>
	</div>
</div>

<div class="modal hide" id="modalsuccessKSNId" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick='closeTalentSuccessDiv();' >x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="internalTxt1">Successfully Changed</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" id="oktochoseKSNNOo" onclick='closeTalentSuccessDiv();'>Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				
   			</div>
  		</div>
	</div>
</div>


<div class="modal hide" id="modalErr100" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeErr100();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="err100msg">
				
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" onclick="closeErr100();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				
   			</div>
  		</div>
	</div>
</div>

<!--........-->


<!-- --sandeep  start 17-11-15 -->


<div class="modal hide" id="modalErrContinue" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeContinue();">x</button>
				<h3 id="myModalLabel">Teachermatch</h3>
			</div>
			<div class="modal-body">
			<div class="" id="errContinuemsg">
				
			</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" data-dismiss="modal" aria-hidden="true" onclick="closeContinue();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				
   			</div>
  		</div>
	</div>
</div>
<style>
#content_del{
	table-layout:fixed; 
	overflow:auto;
	word-break: break-all;
}
#content_del td{
	vertical-align:top;
}
</style>
<div  class="modal hide"  id="hv_integration"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 650px;">
	<div class="modal-content">		
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hide_hireVue()">x</button>
		<h3 id="myModalLabel"><span class="left7">HireVue Integration</span></h3>
	</div>	
	<div class="modal-body">		
		<div class="control-group">
			<div id="errMsgsId" class="" style="color: red"></div>
			<div class="table-responsive">

				<div id="hireVuePositionId" class=""></div>
			
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button type="button" class="btn btn-primary" onclick="addTocg()">Apply for Interview</button>
 		<button class="btn btn-default" data-dismiss="modal" aria-hidden="true" onclick="">Cancel</button>	
 	</div>
</div>
</div>
</div>

<div class="modal" id="myModalCommon" style="display: none;z-index: 5000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
  <div class="modal-dialog" style="width: 500px;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		<h3 id="myModalLabelCommon">TeacherMatch</h3>
      </div>
      <div class="modal-body">
       <div class="control-group" id='message2showCommon'>
		</div>
      </div>
      <div class="modal-footer">
        	<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
      </div>
    </div>
  </div>
</div>

<jsp:include page="selfserviceapplicantprofilecommon.jsp"></jsp:include>

<!--   Add district list in popup starat ----->
<div class="modal hide" id="selectDistrictDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 40%;">
		<div class="modal-content"  style="background-color: #ffffff;">
			<div class="modal-header">
  				<button type="button" class="close" onclick="closeDistrictlist();">x</button>
				<h3 id="myModalLabel">TeacherMatch</h3>
			</div>
			<div class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errorDivDistrictlist' style="display: block;"></div>
					<div id="districtlistdiv">
						<label><strong><spring:message code="msgSelectDistrict"/><span class="required">*</span></strong></label>
						<select name="modalDistrictDetails" id="modalDistrictDetails" class="form-control"></select>
	              		<input type="hidden" name="modeldistrictid" id="modeldistrictid" value="">
					</div>
				</div>
 			</div>
 			<div class="modal-footer">
 				<span id=""><button class="btn btn-large btn-primary" id="btnDefaultEPIGroup" onclick="openDistrictlist();">Ok <i class="icon"></i></button></span>&nbsp;&nbsp;
 				<button class="btn" onclick="closeDistrictlist();">Cancel</button>
   			</div>
   			<input type="hidden" name="districtlistdivteacherid" id="districtlistdivteacherid" value="">
  		</div>
	</div>
</div>
<!--   Add district list in popup end ----->


<script>
function closeDistrictlist()
{
	$("#selectDistrictDiv").modal("hide");
}

function hide_hireVue(){
	try { $('#hv_integration').modal('hide'); } catch (e) {}
}

</script>
<!-- --sandeep  end 17-11-15 -->

<script><!--<!--
function applyScrollOnStatusNote(id)
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#'+id).fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 670,
        minWidth: null,
        minWidthAuto: false,
        colratio:[80,110,160,220,100], // table header width
        // colratio:[220,100,120,120,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnStatusNotes(id)
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#'+id).fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 100,
        width: 670,
        minWidth: null,
        minWidthAuto: false,
        colratio:[250,110,110,120,500], // table header width
        // colratio:[220,100,120,120,130], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}

/****TPL-4833  Configuration and Logic for Candidates Not Reviewed starts*******************/
var candidateNotReviewedUtil = {
	teacherId:null,
	jobId:null,
	event:null,
	setNotReviewedFlag:function fNotReviewedFlag(i,e){
		this.setJobId();
		this.teacherId = i;
		this.event = e;
		CandidateGridAjaxNew.updateCandidateNotReviewed(this.teacherId,this.jobId,this.event,{
			async: false,
			errorHandler:handleError,
			callback:function(data){
				if(e=="CNR_PAA"||e=="CNR_IOEA"||e=="addMessage2"||e=="ApplyGlobalTag2"){	
					var tteacherIds  = i.split("##");
					for(var x=0; x<tteacherIds.length; x++){
						if(data == "1")
							if(tteacherIds[x]!="" && tteacherIds[x]!=null)
								$("#cndNotReviewed"+$("#hidCndNotReviewed"+tteacherIds[x]).val()).hide();
						else
							if(tteacherIds[x]!="" && tteacherIds[x]!=null)
								$("#cndNotReviewed"+$("#hidCndNotReviewed"+tteacherIds[x]).val()).show();
					}
				}else
					if(data == "1"){
						$("#cndNotReviewed"+$("#hidCndNotReviewed"+i).val()).hide();
					}else{
						$("#cndNotReviewed"+$("#hidCndNotReviewed"+i).val()).show();
					}
			}
		});
	},
	setJobId:function fJobId(){
		var url = window.location.href;
		url = url.split('?')[1];
		url = url.split('&')[0];
		this.jobId = url.split('=')[1];
	}
};
/****TPL-4833  Configuration and Logic for Candidates Not Reviewed ends*******************/
$(document).ready(function(){
	$(".profile").click(function(){
	});
});
</script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">