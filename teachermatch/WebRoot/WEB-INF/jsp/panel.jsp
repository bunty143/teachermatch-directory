<!-- @Author: Gagan 
 * @Discription: view of edit panel Page.
 -->
<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:set var="resourceMap"  value="${applicationScope.resouceMap}"/>
<script type="text/javascript" src="dwr/engine.js"></script>
<script type='text/javascript' src='dwr/util.js'></script>
<script type='text/javascript' src='js/panel.js?ver=${resourceMap["js/panel.js"]}'></script>
<script type="text/javascript" src="dwr/interface/PanelAjax.js?ver=${resourceMap['PanelAjax.Ajax']}"></script>
<script type="text/javascript" src="dwr/interface/BatchJobOrdersAjax.js?ver=${resourceMap['BatchJobOrdersAjax.Ajax']}"></script>
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.fixheadertable.js"></script>
<link rel="stylesheet" type="text/css" href="css/base.css?ver=${resourceMap['css/base.css']}" />

<c:set var="notShow" />
<c:if test="${userSession.entityType==1}"> 
<c:set var="notShow" value="display:none;"/>

<div class="row col-sm-offset-2 col-md-offset-2 col-sm-8 col-md-8 modalTM1" id='searchItem' style="margin-top: 15px;padding-top: 15px;padding-bottom:15px;">	
     <div class='divErrorMsg' id='errordiv' style="padding-left: 15px;"></div>
        <div class="col-sm-10 col-md-10" style="max-width: 89%">
           <label><spring:message code="lblDistrictName"/></label>
           <span>
           <input type="text" id="districtName" autocomplete="off" maxlength="100"  name="districtName" class="help-inline form-control"
          		 onfocus="getDistrictAuto(this, event, 'divTxtShowData', 'districtName','districtId','');"
						onkeyup="getDistrictAuto(this,event,'divTxtShowData', 'districtName','districtId','');"
						onblur="hideDistrictMasterDiv(this,'districtId','divTxtShowData');"	/>
     	   	</span>
         	<input type="hidden" id="districtId" value="0"/>
         	<div id='divTxtShowData'  onmouseover="mouseOverChk('divTxtShowData','districtName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>	
         </div>
 
	      <div class="col-sm-9 col-md-9" style="display: none;">
	        <label><spring:message code="lblSchoolName"/></label>        	
	         	<input type="text" id="schoolName" autocomplete="off" maxlength="100" name="schoolName" class="form-control" placeholder="" onfocus="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
							onkeyup="getSchoolAuto(this, event, 'divTxtShowData2', 'schoolName','districtId','');"
							onblur="hideSchoolMasterDiv(this,'districtOrSchoolId','divTxtShowData2');"	/>
		    				<input type="hidden" id="schoolId" value="0"/>
	           				<input type="hidden" id="schoolName" value="" name="schoolName"/>
	           
		   <div id='divTxtShowData2'  onmouseover="mouseOverChk('divTxtShowData2','schoolName')" style=' display:none;position:absolute;z-index:5000;' class='result' ></div>
	      </div>
      <div class="col-sm-1 col-md-1">
        <label  style="float: right;margin-right:-50px;" id='closePan'>&nbsp;</label>
       
		<button class="btn btn-primary top25-sm left25-sm" style="width: 90px;" type="button" onclick="searchData();"><spring:message code="btnSearch"/><i class="icon"></i></button>         	
      </div>     
 </div>

 
   <div class="modalsa" style="display: none;" id="sa">
	<div class="" style="padding-top: 5px;">
	<a href='javascript:void(0);' onclick="getSearchPan()"><span class='icon-search icon-large iconcolor' style="font-size: 0.99em;"></span><b>&nbsp;<spring:message code="lnkSearchAg"/><img src="images/arrow_left_animated.gif"/></b></a>
	</div>
	</div>
</c:if>	

<script type="text/javascript">
var $j=jQuery.noConflict();
        $j(document).ready(function() {       
            
        });
        
function applyScrollOnTbl()
{
	//alert("Hi");
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#panelTable').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 280,
        width: 945,
        minWidth: null,
        minWidthAuto: false,
        colratio:[127,612,230], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

 </script>

<div id="panelDiv" class="hide" >
<div class="row top10forpanel" style="margin:0px;">
         <div style="float: left;">
         	<img src="images/manageusers.png" width="41" height="41" alt="">
         </div>        
         <div style="float: left;">
         	<div class="subheading" style="font-size: 13px;"><spring:message code="lblManagePanel"/></div>	
         </div>			
		<div class="pull-right add-employment1">
			<a href="javascript:void(0);" onclick="return addNewPanel()"><spring:message code="lnkAddPanel"/></a>
		</div>
		<div style="clear: both;"></div>	
	    <div class="centerline"></div>
</div>

<!-- Div For Panel Grid   -->

<div class="TableContent top15">        	
            <div class="table-responsive" id="panelGrid">          
                	         
            </div>            
</div> 	
<div  id="divPanelForm" style="display: none;" class="mt10 span16" onkeypress="return chkForEnterSavePanel(event);" >
	 <div class="row">
			<div class="col-sm-4 col-md-4">		                         
	 	    <div class='divErrorMsg' id='errorpaneldiv' ></div>
	        </div>
	</div>
	<form  id="frmDomin" onsubmit="return false;">
		<div class="row">
			<div class="col-sm-4 col-md-4"><label><strong><spring:message code="lblDistrictName"/><span class="required">*</span></strong></label><br/>
				<input type="hidden" name="panelId" id="panelId" >
				<c:if test="${userSession.entityType==1}"> 
					<input type="text" name="distName" id="distName" maxlength="100" class="form-control" placeholder="">
				</c:if>
				
				<c:if test="${DistrictName!=null}">
	             	${DistrictName}
	             	<input type="hidden" id="districtId" value="${DistrictId}"/>
	             	<input type="hidden" id="districtName" value="${DistrictName}" name="districtName"/>
	             </c:if>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-sm-4 col-md-4"><label><strong><spring:message code="lblPanelName"/><span class="required">*</span></strong></label>
				<input type="text" name="panelName" id="panelName" maxlength="100" class="form-control" placeholder="">
			</div>
		</div>
		
		
		
		 <div class="row mb30" style="display: none;" id="divManage">
			  	<div class="col-sm-4 col-md-4 mt30">
			     		<button onclick="savePanel();" class="btn btn-large btn-primary"><strong><spring:message code="btnSave"/><i class="icon"></i></strong></button>
			     	 &nbsp;<a href="javascript:void(0);" onclick="clearPanel();"><spring:message code="lnkCancel"/></a>  
			    </div>
		</div>
		  <br/>
		  
		<div class="row">
			<div class="col-sm-4 col-md-4 idone" id="divDone" style="display: none;">
				<a href="javascript:void(0);" onclick="savePanel();"><spring:message code="lnkImD"/></a>
				 &nbsp;<a href="javascript:void(0);" onclick="clearPanel();"><spring:message code="lnkCancel"/></a> 
			</div>
		</div>
		 <br><br>
	</form>
</div>

<br><br>
</div>


<div class="modal hide"  id="deletePanelDiv"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><spring:message code="btnX"/></button>
		<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
	</div>
	<div class="modal-body"> 		
		<div class="control-group">
			<spring:message code="msgWantToDeletePanalAll"/>
		
		</div>
 	</div>
 	<div class="modal-footer">
 	<span id=""><button class="btn btn-primary" onclick="deleteconfirm()" ><spring:message code="btnOk"/> <i class="icon"></i></button></span>&nbsp;&nbsp;
 		<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="btnClose"/></button> 		
 	</div>
  </div>
 </div>
</div>

<c:if test="${userSession.entityType ne 1}">
<script type="text/javascript">
	displayPanel();
</script>
</c:if>