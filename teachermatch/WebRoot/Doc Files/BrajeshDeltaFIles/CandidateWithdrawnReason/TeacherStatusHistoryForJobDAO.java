package tm.dao;
import static tm.services.district.GlobalServices.println;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictSpecificReason;
import tm.bean.DistrictSpecificTiming;
import tm.bean.JobOrder;
import tm.bean.SapCandidateDetails;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.master.DistrictMaster;
import tm.bean.master.EligibilityMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.district.GlobalServices;
import tm.services.district.PrintOnConsole;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;

public class TeacherStatusHistoryForJobDAO extends GenericHibernateDAO<TeacherStatusHistoryForJob, Long>{

	public TeacherStatusHistoryForJobDAO() {
		super(TeacherStatusHistoryForJob.class);
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob findByTeacherStatusHistoryForJob(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,String status)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         Criterion criterion4 = Restrictions.eq("status",status);
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterion4);
	         if(lstTeacherStatusHistoryForJob!=null && lstTeacherStatusHistoryForJob.size()>0){
	        	 teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
	         }
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob>    findByTeacherStatusHistoryForJobNew(List<TeacherDetail> teacherDetail,List<JobOrder> jobOrder,StatusMaster statusMaster,String status)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	         Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.in("jobOrder",jobOrder);
	         Criterion criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         Criterion criterion4 = Restrictions.eq("status",status);
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterion4);
	         
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatusHistoryForJob(TeacherDetail teacherDetail,JobOrder jobOrder,UserMaster userMaster)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	        Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	        Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	     	Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			if(jobOrder==null){
				if(userMaster.getEntityType()==2){
					Criteria cc = criteria.createCriteria("userMaster");	
					cc.add(Restrictions.eq("districtId",userMaster.getDistrictId()));
				}if(userMaster.getEntityType()==3){
					Criteria cc = criteria.createCriteria("userMaster");	
					cc.add(Restrictions.eq("schoolId",userMaster.getSchoolId()));
				}
			}else{
				criteria.add(criterion2);
			}
			lstTeacherStatusHistoryForJob = criteria.list();
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatusHistoryActive(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			lstTeacherStatusHistoryForJob = criteria.list();
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatusHistorySelected(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status","S");
			Criterion criterion4 = Restrictions.eq("status","W");
			Criterion criterion5 = Restrictions.or(criterion3, criterion4);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion5);
			criteria.addOrder(Order.desc("statusMaster"));
			lstTeacherStatusHistoryForJob = criteria.list();
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatusHistorySelectedStatus(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		System.out.println("::::::::::::::::findByTeacherStatusHistorySelectedStatus:::::::::::::::::::::::::::::;;:");
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status","S");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.addOrder(Order.desc("statusMaster"));
			lstTeacherStatusHistoryForJob = criteria.list();
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob findByTeacherStatusHistoryLastSelected(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		TeacherStatusHistoryForJob teacherStatusHistoryForJob =null;
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("status","S");
			Criterion criterion4 = Restrictions.isNotNull("statusMaster");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.addOrder(Order.desc("statusMaster"));
			lstTeacherStatusHistoryForJob = criteria.list();
			
			if(lstTeacherStatusHistoryForJob.size()>0){
				teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
			}
			
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob findByTeacherStatus(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterion4 = Restrictions.eq("status","A");
	         Criterion criterion5 = Restrictions.eq("status","S");
	         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
	         Criterion criterion3 = null;
	         if(statusMaster!=null){
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterion6);
	         if(lstTeacherStatusHistoryForJob!=null && lstTeacherStatusHistoryForJob.size()>0){
	        	 teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
	         }
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherDetail> findTeacherListByStatusAndSecStatus(JobOrder jobOrder,SecondaryStatus secondaryStatus)
	{
        List <TeacherDetail> lstTeacherDetails =null;
        try{
        	Criterion criterion1 =null;
        	if(jobOrder.getDistrictMaster().getStatusMaster()!=null){
				criterion1=Restrictions.eq("statusMaster",jobOrder.getDistrictMaster().getStatusMaster());
        	}else if(jobOrder.getDistrictMaster().getSecondaryStatus()!=null){
				criterion1=Restrictions.eq("secondaryStatus",secondaryStatus);
			}
        	if(criterion1!=null){
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion4 = Restrictions.eq("status", "S");
				Criterion criterion5 = Restrictions.or(criterion3,criterion4);
			  	Criterion criterionW = Restrictions.eq("status","W");
				Criterion criterionORW = Restrictions.or(criterionW, criterion5);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterionORW);
				criteria.setProjection(Projections.groupProperty("teacherDetail"));
				lstTeacherDetails = criteria.list();
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherDetails;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findStatusAndSecStatusByTeacherList(List<TeacherDetail> teacherDetails)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	if(teacherDetails.size()>0){
	        	Criterion criterion1 = Restrictions.in("teacherDetail", teacherDetails);
	        	Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion4 = Restrictions.eq("status", "S");
				Criterion criterion5 = Restrictions.or(criterion3,criterion4);
			  	Criterion criterionW = Restrictions.eq("status","W");
				Criterion criterionORW = Restrictions.or(criterionW, criterion5);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterionORW);
				lstTeacherStatusHistoryForJob = criteria.list();
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob getOverride(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion3 = Restrictions.eq("override", true);
			Criterion criterion4 = null;
	         if(statusMaster!=null){
	        	 criterion4 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion4 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
			lstTeacherStatusHistoryForJob=findByCriteria(criterion1,criterion2,criterion3,criterion4);
        }catch(Exception e){
        	e.printStackTrace();
        }
        if(lstTeacherStatusHistoryForJob.size() ==1)
        	teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
        else if(lstTeacherStatusHistoryForJob.size() > 1)
        	System.out.println("lstTeacherStatusHistoryForJob size should be 1 but ... Now size is "+lstTeacherStatusHistoryForJob.size()+" ... We need to verify at database level ..");
        
        return teacherStatusHistoryForJob;
	}
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> getTeacherStatusHistoryByJobList(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try
        {
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.in("jobOrder", lstJobOrder);
			Criterion criterion3 = Restrictions.eq("status","S");
		  	Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion3);
			lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterionORW);
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatusHistoryActive_msu(TeacherDetail teacherDetail,List<JobOrder> LstJobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.in("jobOrder", LstJobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatusHistoryActiveByStatus_msu(TeacherDetail teacherDetail,List<JobOrder> LstJobOrder,StatusMaster statusMaster)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.in("jobOrder", LstJobOrder);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("statusMaster", statusMaster);
		  	Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion3);
			lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterionORW,criterion4);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatusHistorySelected_msu(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.in("jobOrder", lstJobOrder);
			Criterion criterion3 = Restrictions.eq("status","S");
			Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion3);
			lstTeacherStatusHistoryForJob = findByCriteria(Order.desc("statusMaster"), criterion1,criterion2,criterionORW);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatus_msu(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try
        {
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.in("jobOrder",lstJobOrder);
	         Criterion criterion4 = Restrictions.eq("status","A");
	         Criterion criterion5 = Restrictions.eq("status","S");
	         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
			 Criterion criterionW = Restrictions.eq("status","W");
			 Criterion criterionORW = Restrictions.or(criterionW, criterion6);
	         Criterion criterion3 = null;
	         if(statusMaster!=null)
	         {
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         }
	         else
	         {
	        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterionORW);
        }
        catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List <TeacherStatusHistoryForJob> findByTID_JID_Status(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,List<UserMaster> userMasters)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterion4 = Restrictions.eq("status","A");
	         Criterion criterion5 = Restrictions.eq("status","S");
	         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
			 Criterion criterionW = Restrictions.eq("status","W");
			 Criterion criterionORW = Restrictions.or(criterionW, criterion6);
	         Criterion criterion3 = null;
	         if(statusMaster!=null){
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         Criterion criterion7 = Restrictions.in("userMaster",userMasters);
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterionORW,criterion7);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findHireTeachersWithDistrictListAndTeacherList(List<TeacherDetail> teachersList,DistrictMaster districtMasters) 
	{	
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			if(teachersList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teachersList);
				Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);
				Criterion criterion3 = Restrictions.eq("status", "A");
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMasters));
				hiredTeacherList = criteria.list();
				System.out.println(" hiredTeacherList :: "+hiredTeacherList.size());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
	
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob findByTeacherStatusHistoryLastSelectedByTeacher(TeacherDetail teacherDetail)
	{
		TeacherStatusHistoryForJob teacherStatusHistoryForJob =null;
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion3 = Restrictions.eq("status","S");
			Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion3);

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterionORW);
			criteria.addOrder(Order.desc("teacherStatusHistoryForJobId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			lstTeacherStatusHistoryForJob = criteria.list();
			if(lstTeacherStatusHistoryForJob.size()>0){
				teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
			}
			
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob getCreatedById(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		TeacherStatusHistoryForJob teacherStatusHistoryForJob =null;
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
			Criterion criterion4 = null;
	         if(statusMaster!=null){
	        	 criterion4 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion4 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion4);
			criteria.addOrder(Order.desc("teacherStatusHistoryForJobId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			lstTeacherStatusHistoryForJob = criteria.list();
			if(lstTeacherStatusHistoryForJob.size()>0){
				teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
			}
			
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob getCreatedByTeacher(TeacherDetail teacherDetail,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
		TeacherStatusHistoryForJob teacherStatusHistoryForJob =null;
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion4 = null;
	         if(statusMaster!=null){
	        	 criterion4 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion4 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }

			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion4);
			criteria.addOrder(Order.desc("teacherStatusHistoryForJobId"));
			criteria.setFirstResult(0);
			criteria.setMaxResults(1);
			lstTeacherStatusHistoryForJob = criteria.list();
			if(lstTeacherStatusHistoryForJob.size()>0){
				teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
			}
			
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findTeacherStatusByJobOrderAndUser(UserMaster userMaster,JobOrder jobOrder) 
	{	
		List<TeacherStatusHistoryForJob> teacherList = new ArrayList<TeacherStatusHistoryForJob>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("userMaster",userMaster);
			Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
		    Criterion criterion4 = Restrictions.eq("status","A");
	        Criterion criterion5 = Restrictions.eq("status","S");
	        Criterion criterion6 = Restrictions.or(criterion4,criterion5);
	        Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion6);
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterionORW);
			teacherList = criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	@Transactional(readOnly=true)
	public List <TeacherStatusHistoryForJob> findByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterion3 = Restrictions.eq("status","A");
	         Criterion criterion4 = Restrictions.eq("status","S");
	         Criterion criterion5 = Restrictions.or(criterion3,criterion4);
			 Criterion criterionW = Restrictions.eq("status","W");
			 Criterion criterionORW = Restrictions.or(criterionW, criterion5);
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterionORW);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public List <TeacherStatusHistoryForJob> getStatus(List<TeacherDetail> lstTeacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	if(lstTeacherDetail.size()>0){
		         Criterion criterion1 = Restrictions.in("teacherDetail",lstTeacherDetail);
		         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
		         Criterion criterion4 = Restrictions.eq("status","A");
		         Criterion criterion5 = Restrictions.eq("status","S");
		         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
				 Criterion criterionW = Restrictions.eq("status","W");
				 Criterion criterionORW = Restrictions.or(criterionW, criterion6);
		         Criterion criterion3 = null;
		         if(statusMaster!=null)
		        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
		         else
		        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
		         
		         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterionORW);
	        }
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> massStatusSelected(List<TeacherDetail> lstTeacherDetail,JobOrder jobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	if(lstTeacherDetail.size()>0){
	        	Criterion criterion1 = Restrictions.in("teacherDetail", lstTeacherDetail);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("status","S");
				Criterion criterionW = Restrictions.eq("status","W");
				Criterion criterionORW = Restrictions.or(criterionW, criterion3);

				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterionORW);
				criteria.addOrder(Order.desc("statusMaster"));
				lstTeacherStatusHistoryForJob = criteria.list();
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatus_CGMass(List<TeacherDetail> lstTeacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	if(lstTeacherDetail.size()>0){
		         Criterion criterion1 = Restrictions.in("teacherDetail",lstTeacherDetail);
		         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
		         Criterion criterion4 = Restrictions.eq("status","A");
		         Criterion criterion5 = Restrictions.eq("status","S");
		         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
				 Criterion criterionW = Restrictions.eq("status","W");
				 Criterion criterionORW = Restrictions.or(criterionW, criterion6);
		         Criterion criterion3 = null;
		         if(statusMaster!=null)
		        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
		         else
		        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
		         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterionORW);
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob findByTeacherStatusAndUserMaster(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterionUser = Restrictions.eq("userMaster",userMaster);
	         Criterion criterion4 = Restrictions.eq("status","A");
	         Criterion criterion5 = Restrictions.eq("status","S");
	         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
	         Criterion criterion3 = null;
	         if(statusMaster!=null){
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterion6,criterionUser);
	         if(lstTeacherStatusHistoryForJob!=null && lstTeacherStatusHistoryForJob.size()>0){
	        	 teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
	         }
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findHireTeachersWithDistrictAndTeacher(TeacherDetail teacherDetail,DistrictMaster districtMasters) 
	{	
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);
			Criterion criterion3 = Restrictions.eq("status", "A");
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMasters));
			hiredTeacherList = criteria.list();
			
			System.out.println(" hiredTeacherList >>>:: "+hiredTeacherList.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findHireTeachersWithDistrictListWithoutAndTeacherList(Order sortOrderStrVal,DistrictMaster districtMasters,List<TeacherDetail> teachersList,List<JobOrder> lstjJobOrders) 
	{	
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(teachersList!=null && teachersList.size()>0){
				Criterion criterion1 = Restrictions.in("teacherDetail",teachersList);
				criteria.add(criterion1);
			}
			
			if(lstjJobOrders!=null && lstjJobOrders.size()>0){
				Criterion jobcriterion = Restrictions.in("jobOrder",lstjJobOrders);
				criteria.add(jobcriterion);
			}
			
			Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);
			Criterion criterion3 = Restrictions.eq("status", "A");
			
			criteria.add(criterion2);
			criteria.add(criterion3);
			String str = sortOrderStrVal.toString();
			
			
			if(districtMasters!=null)
			 criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMasters));
			
			hiredTeacherList = criteria.list();
			
			System.out.println(" hiredTeacherList :: "+hiredTeacherList.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
   	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findByTeacherIdAndJObIds(DistrictMaster districtMasters,List<TeacherDetail> teachersList,List<JobOrder> lstjJobOrders) 
	{	
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
		
			Criterion criterion1 = Restrictions.in("teacherDetail",teachersList);
			criteria.add(criterion1);
	
			Criterion jobcriterion = Restrictions.in("jobOrder",lstjJobOrders);
			criteria.add(jobcriterion);
			
			
			Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);
			Criterion criterion3 = Restrictions.eq("status", "A");
			
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			if(districtMasters!=null)
			 criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMasters));
			
			hiredTeacherList = criteria.list();
			
			System.out.println(" hiredTeacherList :: "+hiredTeacherList.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
   	
	
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findByHIredDAte(DistrictMaster districtMasters,String hiredfromDate, String hiredtoDate) 
	{	
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Date hirefdate= null;
			Date hiretdate=null;
			
			if(!hiredfromDate.equals("")){
				hirefdate=Utility.getCurrentDateFormart(hiredfromDate);
			}
			
			if(!hiredtoDate.equals("")){
				hiretdate =Utility.getCurrentDateFormart(hiredtoDate);
			}
			
			Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);
			Criterion criterion3 = Restrictions.eq("status", "A");
			
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			if(districtMasters!=null)
			 criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMasters));
			
			
			if(hirefdate!=null && hiretdate!=null){
			criteria.add(Restrictions.ge("hiredByDate",hirefdate)).add(Restrictions.le("hiredByDate",hiretdate));
			}else if(hirefdate!=null && hiretdate==null){
			criteria.add(Restrictions.ge("hiredByDate",hirefdate));
			}else if(hirefdate==null && hiretdate!=null){
			criteria.add(Restrictions.le("hiredByDate",hiretdate));
		    }
			
		
			
			hiredTeacherList = criteria.list();
			
			System.out.println(" filter hiredTeacherListhiredTeacherList :: "+hiredTeacherList.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
   	
	
	@Transactional(readOnly=true)
	public List <TeacherStatusHistoryForJob> findByTeachersAndJobs(List<TeacherDetail> teacherDetails,List<JobOrder> jobOrders)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	if(teacherDetails.size()>0){
		         Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
		         Criterion criterion2 = Restrictions.in("jobOrder",jobOrders);
		         Criterion criterion3 = Restrictions.eq("status","A");
		         Criterion criterion4 = Restrictions.eq("status","S");
		         Criterion criterion5 = Restrictions.or(criterion3,criterion4);
				 Criterion criterionW = Restrictions.eq("status","W");
				 Criterion criterionORW = Restrictions.or(criterionW, criterion5);
		         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterionORW);
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findJobByTeachersWithDistrictAndCategory(TeacherDetail teacherDetail,JobOrder jobOrder) 
	{	
		List<TeacherStatusHistoryForJob> teacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("status","S");
			Criterion criterion5 = Restrictions.or(criterion3,criterion4);
			 Criterion criterionW = Restrictions.eq("status","W");
			 Criterion criterionORW = Restrictions.or(criterionW, criterion5);
			criteria.add(criterion1);
			criteria.add(criterionORW);
			criteria.createCriteria("jobOrder").addOrder(Order.desc("createdDateTime")).add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster())).add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			teacherList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherandJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	     	Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(teacherDetail!=null){
				if(jobOrder!=null && teacherDetail!=null){
					Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
					criteria.add(criterion1);
					criteria.add(criterion2);
				}else if(teacherDetail!=null){
					Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
					criteria.add(criterion1);
				}
				lstTeacherStatusHistoryForJob = criteria.list();
			}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findHistory(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try
        {
        	Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         criteria.add(criterion1);
	         Criterion criterion2 = Restrictions.in("jobOrder",lstJobOrder);
	         criteria.add(criterion2);
	         Criterion criterion4 = Restrictions.eq("status","A");
	         Criterion criterion5 = Restrictions.eq("status","S");
	         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
			 Criterion criterionW = Restrictions.eq("status","W");
			 Criterion criterionORW = Restrictions.or(criterionW, criterion6);
	         criteria.add(criterionORW);
	         Criterion criterion3 = null;
	         if(statusMaster!=null)
	         {
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	        	 criteria.add(criterion3);
	         }
	         else
	         {
	        	 criterion3 = Restrictions.eq("secondaryStatus_copy",secondaryStatus);
	        	 criteria.createCriteria("secondaryStatus").add(criterion3);
	         }
	         lstTeacherStatusHistoryForJob =  criteria.list();
        }
        catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	
	/*Hired Candidates with and without District*/
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> hiredCandidateWithAndWithoutDistrict(DistrictMaster districtMasters, int sortingcheck, Order sortOrderStrVal) 
	{	
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
					
			Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);
			Criterion criterion3 = Restrictions.eq("status", "A");
			
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			
			Criteria c1 = null;
			Criteria c2 = null;
			
			c1 = criteria.createCriteria("jobOrder");
			c2 = criteria.createCriteria("teacherDetail");
			if(districtMasters!=null)
			{	
			 c1.add(Restrictions.eq("districtMaster",districtMasters));
			}
			
			//teacherDetail
			if(sortingcheck==1){
				System.out.println("lastName lastNamelastName sorting ");
				if(c2!=null)
				c2.addOrder(sortOrderStrVal);
			}
			else if(sortingcheck==2){
				System.out.println("jobTitle jobTitlejobTitle sorting");
				if(c1!=null)
				c1.addOrder(sortOrderStrVal);
			}
			
			hiredTeacherList = criteria.list();
			
			System.out.println(" hiredTeacherList :: "+hiredTeacherList.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
   	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findJobByTeachersWithDistrict(TeacherDetail teacherDetail,JobOrder jobOrder) 
	{	
		List<TeacherStatusHistoryForJob> teacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("status","S");
			Criterion criterion5 = Restrictions.or(criterion3,criterion4);
    		Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion5);

			criteria.add(criterion1);
			criteria.add(criterionORW);
			criteria.addOrder(Order.desc("createdDateTime"));
			criteria.createCriteria("jobOrder").addOrder(Order.desc("createdDateTime")).add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			teacherList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob findByTeacherStatusHistory(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterion4 = Restrictions.eq("status","A");
	         Criterion criterion5 = Restrictions.eq("status","S");
	         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
			 Criterion criterionW = Restrictions.eq("status","W");
			 Criterion criterionORW = Restrictions.or(criterionW, criterion6);

	         Criterion criterion3 = null;
	         if(statusMaster!=null){
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         lstTeacherStatusHistoryForJob = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion2,criterion3,criterionORW);
	         if(lstTeacherStatusHistoryForJob!=null && lstTeacherStatusHistoryForJob.size()>0){
	        	 teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
	         }
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public List <TeacherStatusHistoryForJob> findByTeachersAndJobsStatus(List<TeacherDetail> teacherDetails,List<JobOrder> jobOrders)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	if(teacherDetails.size()>0){
		         Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
		         Criterion criterion2 = Restrictions.in("jobOrder",jobOrders);
		         Criterion criterion3 = Restrictions.eq("status","A");
		         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3);
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findHireAndVcompTeacherList(List<TeacherDetail> teachersList,DistrictMaster districtMaster,StatusMaster statusMaster,StatusMaster secondaryStatus) 
	{	
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		try 
		{
			if(teachersList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teachersList);
				Criterion criterion2 = Restrictions.eq("statusMaster", statusMaster);
				Criterion criterion3 = Restrictions.eq("status", "A");
				if(districtMaster!=null && districtMaster.getDistrictId()==1200390){
					Criterion criterion4 = Restrictions.and(criterion2,criterion3);
					Criterion criterion5 = Restrictions.eq("statusMaster", secondaryStatus);
					Criterion criterion6 = Restrictions.eq("status", "S");
					Criterion criterion7 = Restrictions.and(criterion5,criterion6);
					Criterion criterion8 = Restrictions.or(criterion4,criterion7);
					criteria.add(criterion1);
					criteria.add(criterion8);
				}else{
					criteria.add(criterion1);
					criteria.add(criterion2);
					criteria.add(criterion3);
				}
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMaster));
				hiredTeacherList = criteria.list();
				System.out.println(" hiredTeacherList :: "+hiredTeacherList.size());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findHireTeacherList(List<TeacherDetail> teachersList,DistrictMaster districtMaster,Map<String,StatusMaster> statusMap) 
	{	
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		try 
		{
			if(teachersList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teachersList);
				Criterion criterion2 = Restrictions.eq("statusMaster", statusMap.get("hird"));
				Criterion criterion3 = Restrictions.eq("status", "A");
				
				Criterion criterionR = Restrictions.eq("statusMaster", statusMap.get("rem"));
				Criterion criterionRA = Restrictions.eq("status", "A");
				Criterion criterionRM = Restrictions.and(criterionR,criterionRA);
				
				Criterion criterionD = Restrictions.eq("statusMaster", statusMap.get("dcln"));
				Criterion criterionDA = Restrictions.eq("status", "A");
				Criterion criterionDM = Restrictions.and(criterionD,criterionDA);
				
				Criterion criterionW = Restrictions.eq("statusMaster", statusMap.get("widrw"));
				Criterion criterionWA = Restrictions.eq("status", "A");
				Criterion criterionWM = Restrictions.and(criterionW,criterionWA);
				
				if(districtMaster!=null && districtMaster.getDistrictId()==1200390){
					Criterion criterion4 = Restrictions.and(criterion2,criterion3);
					Criterion criterion5 = Restrictions.eq("statusMaster", statusMap.get("vcomp"));
					Criterion criterion6 = Restrictions.eq("status", "S");
					Criterion criterion7 = Restrictions.and(criterion5,criterion6);
					Criterion criterion8 = Restrictions.or(criterion4,criterion7);
					Criterion criterion9 = Restrictions.or(criterionRM,criterionDM);
					Criterion criterion10 = Restrictions.or(criterionWM,criterion8);
					Criterion criterion11 = Restrictions.or(criterion10,criterion9);
					criteria.add(criterion1);
					criteria.add(criterion11);
				}else{
					criteria.add(criterion1);
					criteria.add(criterion2);
					criteria.add(criterion3);
				}
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMaster));
				hiredTeacherList = criteria.list();
				System.out.println(" hiredTeacherList :: "+hiredTeacherList.size());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherStatusForUndo(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	          Criterion criterion4 = Restrictions.eq("status","A");
	         Criterion criterion5 = Restrictions.eq("status","S");
	         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
			 Criterion criterionW = Restrictions.eq("status","W");
			 Criterion criterionORW = Restrictions.or(criterionW, criterion6);
	         Criterion criterion3 = null;
	         if(statusMaster!=null){
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterionORW);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> getTSHDelta(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,List<JobOrder> lstJobOrder) 
	{	
		List<TeacherStatusHistoryForJob> teacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			if(teacherDetails!=null && teacherDetails.size()>0)
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				//Criterion criterion2 = Restrictions.eq("status", "A");
				//Criterion criterion3 = Restrictions.eq("status","S");
				//Criterion criterion4 = Restrictions.or(criterion2,criterion3);
				
				criteria.add(criterion1);
				//criteria.add(criterion4);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMaster));
				
				if(lstJobOrder!=null && lstJobOrder.size()>0)
				{
					Criterion criterion5 = Restrictions.in("jobOrder",lstJobOrder);
					criteria.add(criterion5);
				}
				teacherList = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> getAllTSHDelta(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster, List<SecondaryStatus> lstSecondaryStatus) 
	{	
		List<TeacherStatusHistoryForJob> teacherList = new ArrayList<TeacherStatusHistoryForJob>();
		try 
		{
			if(lstSecondaryStatus!=null && lstSecondaryStatus.size() >0)
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
				Criterion criterion2 = Restrictions.eq("status", "S");
				Criterion criterion3 = Restrictions.in("secondaryStatus",lstSecondaryStatus);
				Criterion criterionW = Restrictions.eq("status","W");
				Criterion criterionORW = Restrictions.or(criterionW, criterion2);
				criteria.add(criterion1);
				criteria.add(criterionORW);
				criteria.add(criterion3);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("districtMaster",districtMaster));
				teacherList = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return teacherList;
	}
	
	@Transactional(readOnly=true)
	public List <TeacherStatusHistoryForJob> findByTeacherStatusHistoryByStatus(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterion3 = null;
	         
	         if(statusMaster!=null){
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public  List<TeacherStatusHistoryForJob> getScreningCompleteJobsByJobsAndTeacher(TeacherDetail teacherDetail,HashSet<JobOrder> masterJOb,StatusMaster statusMaster)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.in("jobOrder",masterJOb);
	         Criterion criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3);
	         
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public List<TeacherDetail> findTeacherListByStatusAndSecStatusHBD(JobOrder jobOrder,SecondaryStatus secondaryStatus)
	{
		System.out.println("::::::::::::findTeacherListByStatusAndSecStatusHBD::::::");
        List <TeacherDetail> lstTeacherDetails =null;
        try{
        	Criterion criterion1 =null;
        	if(jobOrder.getDistrictMaster()!=null){
	        	if(jobOrder.getDistrictMaster().getStatusMaster()!=null){
					criterion1=Restrictions.eq("statusMaster",jobOrder.getDistrictMaster().getStatusMaster());
	        	}else if(jobOrder.getDistrictMaster().getSecondaryStatus()!=null){
					criterion1=Restrictions.eq("secondaryStatus",secondaryStatus);
				}
        	}else if(jobOrder.getBranchMaster()!=null){
	        	
        	}else if(jobOrder.getHeadQuarterMaster()!=null){
	        	if(jobOrder.getHeadQuarterMaster().getStatusMaster()!=null){
					criterion1=Restrictions.eq("statusMaster",jobOrder.getHeadQuarterMaster().getStatusMaster());
	        	}else if(jobOrder.getHeadQuarterMaster().getSecondaryStatus()!=null){
					criterion1=Restrictions.eq("secondaryStatus",secondaryStatus);
				}
        	}
        	if(criterion1!=null){
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion4 = Restrictions.eq("status", "S");
				Criterion criterion5 = Restrictions.or(criterion3,criterion4);
				Criterion criterionW = Restrictions.eq("status","W");
				Criterion criterionORW = Restrictions.or(criterionW, criterion5);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterionORW);
				criteria.setProjection(Projections.groupProperty("teacherDetail"));
				lstTeacherDetails = criteria.list();
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherDetails;
	}
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findHireTeacherListByHBD(List<TeacherDetail> teachersList,JobOrder jobOrder,Map<String,StatusMaster> statusMap) 
	{	
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		try 
		{
			if(teachersList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.in("teacherDetail",teachersList);
				Criterion criterion2 = Restrictions.eq("statusMaster", statusMap.get("hird"));
				Criterion criterion3 = Restrictions.eq("status", "A");
				
				Criterion criterionR = Restrictions.eq("statusMaster", statusMap.get("rem"));
				Criterion criterionRA = Restrictions.eq("status", "A");
				Criterion criterionRM = Restrictions.and(criterionR,criterionRA);
				
				Criterion criterionD = Restrictions.eq("statusMaster", statusMap.get("dcln"));
				Criterion criterionDA = Restrictions.eq("status", "A");
				Criterion criterionDM = Restrictions.and(criterionD,criterionDA);
				
				Criterion criterionW = Restrictions.eq("statusMaster", statusMap.get("widrw"));
				Criterion criterionWA = Restrictions.eq("status", "A");
				Criterion criterionWM = Restrictions.and(criterionW,criterionWA);
				
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390){
					Criterion criterion4 = Restrictions.and(criterion2,criterion3);
					Criterion criterion5 = Restrictions.eq("statusMaster", statusMap.get("vcomp"));
					Criterion criterionS5 = Restrictions.eq("status", "S");
					Criterion criterionW5 = Restrictions.eq("status","W");
					Criterion criterionORW5 = Restrictions.or(criterionW5, criterionS5);
					Criterion criterion7 = Restrictions.and(criterion5,criterionORW5);
					Criterion criterion8 = Restrictions.or(criterion4,criterion7);
					Criterion criterion9 = Restrictions.or(criterionRM,criterionDM);
					Criterion criterion10 = Restrictions.or(criterionWM,criterion8);
					Criterion criterion11 = Restrictions.or(criterion10,criterion9);
					criteria.add(criterion1);
					criteria.add(criterion11);
				}else{
					criteria.add(criterion1);
					criteria.add(criterion2);
					criteria.add(criterion3);
				}
				Criteria jobOrderSet= criteria.createCriteria("jobOrder");
				if(jobOrder.getDistrictMaster()!=null){
					jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
				}else{
					jobOrderSet.add(Restrictions.isNull("districtMaster"));
				}
				if(jobOrder.getBranchMaster()!=null){
					jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
				}else{
					jobOrderSet.add(Restrictions.isNull("branchMaster"));
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
				}else{
					jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
				}
				hiredTeacherList = criteria.list();
				System.out.println(" hiredTeacherList :: "+hiredTeacherList.size());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
	 
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> hiredteachers(Date lastUpdate, Integer statusId,Integer districtId) 
	{		
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion createdDateTime = Restrictions.gt("createdDateTime", lastUpdate);
			Criterion statusIdCriteria =Restrictions.eq("statusMaster.statusId", statusId); 
			Criterion statusCriteria =Restrictions.eq("status", "A");
//			Criterion distIdCriteria =Restrictions.eq("jobOrder.districtMaster.districtId", districtId); 
			criteria.add(createdDateTime); criteria.add(statusCriteria); criteria.add(statusIdCriteria); 
			criteria = criteria.createCriteria("jobOrder").createCriteria("districtMaster").add(Restrictions.eq("hrIntegrated",true));
 			hiredTeacherList=(List<TeacherStatusHistoryForJob>)criteria.list();
			

			}
		
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
	
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> hiredteachersFromHQ(Date lastUpdate, Integer statusId,Integer hqId) 
	{	
		List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion createdDateTime = Restrictions.gt("createdDateTime", lastUpdate);
			Criterion statusIdCriteria =Restrictions.eq("statusMaster.statusId", statusId); 
			Criterion statusCriteria =Restrictions.eq("status", "A");
			Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, statusCriteria);
			criteria.add(createdDateTime);
			criteria.add(criterionORW); 
			criteria.add(statusIdCriteria); 
			criteria = criteria.createCriteria("jobOrder").createCriteria("headQuarterMaster").add(Restrictions.eq("headQuarterId",hqId));
 			hiredTeacherList=(List<TeacherStatusHistoryForJob>)criteria.list();
			

			}
		
		catch (Exception e) {
			e.printStackTrace();
		}		
		return hiredTeacherList;
	}
	
	@Transactional(readOnly=true)
	public List <TeacherStatusHistoryForJob> findHistoryByTeachersAndJobs(List<TeacherDetail> teacherDetails,List<JobOrder> jobOrders)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	if(teacherDetails.size()>0){
		         Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
		         Criterion criterion2 = Restrictions.in("jobOrder",jobOrders);
		         Criterion criterion4 = Restrictions.eq("status","S");
		         Criterion criterion5 = Restrictions.eq("status","W");
		         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
		         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion6);
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=true)
	public List <TeacherStatusHistoryForJob> findHistory(ArrayList<Criterion> lstCriterion,int flag)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        
    	lstCriterion.add(Restrictions.eq("status","A"));
		lstCriterion.add(Restrictions.eq("status","S"));
		lstCriterion.add(Restrictions.eq("status","W"));
        
        Criterion [] criterionList = lstCriterion.toArray(new Criterion[lstCriterion.size()]);
        try{
        	if(criterionList.length>0){
        		
		         lstTeacherStatusHistoryForJob = findByCriteria(criterionList);
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	public List<StatusMaster> getMainStatusMaster(){
		List<StatusMaster> statusMasters=new ArrayList<StatusMaster>();
		try{
			StatusMaster statusMaster=new StatusMaster();
			statusMaster.setStatusId(6);
			statusMasters.add(statusMaster);
			
			statusMaster=new StatusMaster();
			statusMaster.setStatusId(10);
			statusMasters.add(statusMaster);
			
			statusMaster=new StatusMaster();
			statusMaster.setStatusId(19);
			statusMasters.add(statusMaster);
		
			statusMaster=new StatusMaster();
			statusMaster.setStatusId(7);
			statusMasters.add(statusMaster);
		}catch(Exception e){
			e.printStackTrace();
		}
		return statusMasters;
	}
	@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob findWaivedStatusByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterion5 = Restrictions.eq("status","W");
	         Criterion criterion3 = null;
	         if(statusMaster!=null){
	        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterion5);
	         if(lstTeacherStatusHistoryForJob!=null && lstTeacherStatusHistoryForJob.size()>0){
	        	 teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
	         }
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
	@Transactional(readOnly=true)
	public List<String []> hiredCandidateByDistrictId(Map<String,String> allMap,UserMaster userMaster,boolean report,String STATUS)
	{
		System.out.println("start date==================");
		List<String []> lstHiredTeacher=new ArrayList<String []>();
		try{
			//________________________________________________________________________________________________________________________________________________
			
			for(Map.Entry<String, String> entry:allMap.entrySet())
				println("key======"+entry.getKey()+" value========"+entry.getValue());
			Integer districtId=Integer.parseInt(allMap.get("districtId")!=null?(allMap.get("districtId").trim().equals("")?"0":allMap.get("districtId")):"0");
			Integer hdId=Integer.parseInt(allMap.get("hdId")!=null?(allMap.get("hdId").trim().equals("")?"0":allMap.get("hdId")):"0");
			Integer sortingFlag=allMap.get("sortingFlag_ZeroAsc_OneDesc")!=null?(allMap.get("sortingFlag_ZeroAsc_OneDesc").trim().equals("")?0:Integer.parseInt(allMap.get("sortingFlag_ZeroAsc_OneDesc"))):0;
			String sortingField=allMap.get("sortingField");
			Integer pageNo=allMap.get("pageNo")!=null?(allMap.get("pageNo").trim().equals("")?0:Integer.parseInt(allMap.get("pageNo"))):0;
			Integer noOfRow=allMap.get("noOfRow")!=null?(allMap.get("noOfRow").trim().equals("")?10:Integer.parseInt(allMap.get("noOfRow"))):10;
			String filterFirstName=allMap.get("firstName");
			String filterLastName=allMap.get("lastName");
			String filterPostion=allMap.get("position");
			String filterSchoolId=allMap.get("schoolId");
			Integer filterEstechId=Integer.parseInt(allMap.get("estechId"));
			if(sortingField.trim().equals("")){
				sortingField="firstName";
				sortingFlag=0;
			}
			int start=((pageNo-1)*noOfRow);
			StatusMaster sm=statusMasterDAO.findStatusByShortNameHired("hird");
			//String candName="",jobId="",teacherId="",teacherEmail="",postionNumber="",hiredDate="",teacherHistoryId="";
			List<Criterion> listRestriction=new ArrayList<Criterion>();
			
			if(districtId!=null && districtId!=0)
				listRestriction.add(Restrictions.eq("dm.districtId", districtId));
			else
				listRestriction.add(Restrictions.eq("dm.headQuarterMaster.headQuarterId", hdId));
				
			if(!filterFirstName.trim().equals(""))
				listRestriction.add(Restrictions.ilike("td.firstName", "%"+filterFirstName.trim().toLowerCase()+"%"));
			if(!filterLastName.trim().equals(""))
				listRestriction.add(Restrictions.ilike("td.lastName", "%"+filterLastName.trim().toLowerCase()+"%"));
			if(filterPostion!=null && !filterPostion.trim().equals(""))
				listRestriction.add(Restrictions.ilike("jo.requisitionNumber", "%"+filterPostion.trim()+"%"));
			if(filterEstechId!=null && filterEstechId!=0)
				listRestriction.add(Restrictions.eq("est.employmentservicestechnicianId", filterEstechId));
			//_______________________________________________________________________________________________________________________________________________
			
			
			
			List<Integer> listJobIds=null;
			if(userMaster.getEntityType()==3){
				listJobIds=new ArrayList<Integer>();
				listJobIds=schoolInJobOrderDAO.findJobIdBySchool(userMaster.getSchoolId(),districtId);
				listJobIds.add(0);
			}
			String eligibilityIds=getColumnId(districtId);
			System.out.println("eligibilityIds===="+eligibilityIds);
			//SapCandidateDetails ss;
			String multipleQuery="" ;
			int countQuery=1;
			for(String eligibilityId: eligibilityIds.split(",")){
				if(countQuery==1)
				multipleQuery="IFNULL((select ESM.statusColorCode from eligibilityverificationhistroy EVH, eligibilitystatusmaster ESM where EVH.jobId=this_.jobId and EVH.districtId=jo1_.districtId and EVH.teacherId=this_.teacherId and EVH.eligibilityStatusId=ESM.eligibilityStatusId and EVH.eligibilityId='"+eligibilityId+"' order by createdDateTime desc limit 0,1),'0000CC')  as EVH"+countQuery;
				else
				multipleQuery+="@#@IFNULL((select ESM.statusColorCode from eligibilityverificationhistroy EVH, eligibilitystatusmaster ESM where EVH.jobId=this_.jobId and EVH.districtId=jo1_.districtId and EVH.teacherId=this_.teacherId and EVH.eligibilityStatusId=ESM.eligibilityStatusId and EVH.eligibilityId='"+eligibilityId+"' order by createdDateTime desc limit 0,1),'0000CC')  as EVH"+countQuery;	
					countQuery++;
			}
				String schoolName= "IFNULL((select concat(sm.schoolName,'\n(',sm.locationCode,')')  from jobrequisitionnumbers jrn, schoolmaster sm, jobforteacher jft where jrn.schoolId = sm.schoolId and jrn.jobId = jo1_.jobId and jft.jobId=jrn.jobId and jft.hiredBySchool=sm.schoolId and jft.teacherId=td2_.teacherId and jrn.districtRequisitionId in (select drq.districtRequisitionId from districtrequisitionnumbers drq where drq.requisitionNumber =jo1_.requisitionNumber)),'') as schoolName";
				String schoolNameCondition= "IFNULL((select sm.schoolId from jobrequisitionnumbers jrn, schoolmaster sm, jobforteacher jft where jrn.schoolId = sm.schoolId and jrn.jobId = jo1_.jobId and jft.jobId=jrn.jobId and jft.hiredBySchool=sm.schoolId and jft.teacherId=td2_.teacherId and jrn.districtRequisitionId in (select drq.districtRequisitionId from districtrequisitionnumbers drq where drq.requisitionNumber =jo1_.requisitionNumber)),'')="+filterSchoolId+"";
				String status="IFNULL((/* criteria query */ select case when scd.sapStatus='S' then 'Send to PeopleSoft' when scd.sapStatus='Q' then 'Queue' when scd.sapStatus='F' then 'Failed' else 'New' END  from  sapcandidatedetails scd  where scd.teacherId=this_.teacherId and scd.jobId=this_.jobId order by scd.sapCandidateId desc limit 0,1 ),'New') as sapToPeopleSoft";
				String statuscondition="IFNULL((/* criteria query */ select case when scd.sapStatus='S' then 'Send to PeopleSoft' when scd.sapStatus='Q' then 'Queue' when scd.sapStatus='F' then 'Failed' else 'New' END  from  sapcandidatedetails scd  where scd.teacherId=this_.teacherId and scd.jobId=this_.jobId order by scd.sapCandidateId desc limit 0,1 ),'New') = '"+STATUS+"'";
				String statusconditionNOTSAP="IFNULL((/* criteria query */ select case when scd.sapStatus='S' then 'Send to PeopleSoft' when scd.sapStatus='Q' then 'Queue' when scd.sapStatus='F' then 'Failed' else 'New' END  from  sapcandidatedetails scd  where scd.teacherId=this_.teacherId and scd.jobId=this_.jobId order by scd.sapCandidateId desc limit 0,1 ),'New') != 'Send to PeopleSoft'";
				String eligibilityId="(/* criteria query */ select group_concat(eligibilityId) from  eligibilitymaster em  where em.districtId=jo1_.districtId and eligibilityCode!='OS') eligibilityId";
					Session session = getSession();				
					Criteria criteria = session.createCriteria(TeacherStatusHistoryForJob.class,"tshj");
					criteria.createAlias("jobOrder", "jo").createAlias("teacherDetail", "td").createAlias("jo.districtMaster", "dm").createAlias("statusMaster", "sm").createAlias("jo.employmentServicesTechnician", "est")//.setFetchMode("sapCandidateDetails", FetchMode.JOIN).createAlias("sapCandidateDetails", "scd",CriteriaSpecification.LEFT_JOIN)
				    .setProjection(Projections.distinct(Projections.projectionList()
				        .add( Projections.property("td.firstName"), "firstName" )
				        .add( Projections.property("td.lastName"), "lastName" )
				        .add( Projections.property("td.emailAddress"), "emailAddress" )
				        .add( Projections.property("jo.requisitionNumber"), "requisitionNumber" )
				        .add( Projections.property("hiredByDate"), "hiredByDate" )
				        .add( Projections.property("createdDateTime"), "createdDateTime" )
				        .add( Projections.property("teacherStatusHistoryForJobId"), "teacherStatusHistoryForJobId")
				        .add( Projections.groupProperty("td.teacherId"), "teacherId")
				        .add( Projections.groupProperty("jo.jobId"), "jobId")
				        .add(Projections.sqlProjection(status, new String[]{"sapToPeopleSoft"}, new Type[]{ Hibernate.STRING}),"sapToPeopleSoft")
				        .add(Projections.sqlProjection("\'"+eligibilityIds+"\' as eligibilityIds", new String[]{"eligibilityIds"}, new Type[]{ Hibernate.STRING}),"eligibilityIds")
				        .add(Projections.sqlProjection(schoolName, new String[]{"schoolName"}, new Type[]{ Hibernate.STRING}),"schoolName")
				        .add(Projections.sqlProjection(multipleQuery.split("@#@")[0], new String[]{"EVH1"}, new Type[]{ Hibernate.STRING}),"EVH1")
				        .add(Projections.sqlProjection(multipleQuery.split("@#@")[1], new String[]{"EVH2"}, new Type[]{ Hibernate.STRING}),"EVH2")
				        .add(Projections.sqlProjection(multipleQuery.split("@#@")[2], new String[]{"EVH3"}, new Type[]{ Hibernate.STRING}),"EVH3")
				        .add(Projections.sqlProjection(multipleQuery.split("@#@")[3], new String[]{"EVH4"}, new Type[]{ Hibernate.STRING}),"EVH4")
				        .add(Projections.sqlProjection(multipleQuery.split("@#@")[4], new String[]{"EVH5"}, new Type[]{ Hibernate.STRING}),"EVH5")
				        //.add( Projections.property("11,12"), "eligibilityIds")
				        //.add(Projections.sqlProjection(eligibilityId, new String[]{"eligibilityId"}, new Type[]{ Hibernate.STRING}),"eligibilityId")
				    ));
					Date restrictionDate=GlobalServices.DATE_FORMAT.parse("february 09, 2016");
					criteria.add(Restrictions.eq("sm.statusId",sm.getStatusId()));
					criteria.add(Restrictions.eq("status","A"));
					criteria.add(Restrictions.ge("tshj.hiredByDate",restrictionDate));
					criteria.add(Restrictions.sqlRestriction(statusconditionNOTSAP));
					if(!STATUS.trim().equals("All"))
					criteria.add(Restrictions.sqlRestriction(statuscondition));
					if(filterSchoolId!=null && !filterSchoolId.trim().equalsIgnoreCase("0"))
						criteria.add(Restrictions.sqlRestriction(schoolNameCondition));
					//criteria.add(Restrictions.eq("sapToPeopleSoft", "New"));
					if(listJobIds!=null){
						criteria.add(Restrictions.in("jo.jobId", listJobIds));
					}
					for(Criterion restriction:listRestriction)
					criteria.add(restriction);
					
					
					if(!report){
					criteria.setFirstResult(start);
					criteria.setMaxResults(noOfRow);
					}
					
					if(sortingField!=null && !sortingField.equals(""))
						if(sortingFlag==0)
							criteria.addOrder(Order.asc(sortingField));
						else if(sortingFlag==1)
							criteria.addOrder(Order.desc(sortingField));
					
					long a = System.currentTimeMillis();
					//criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
					lstHiredTeacher = criteria.list() ;			
					long b = System.currentTimeMillis();
					System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + lstHiredTeacher.size() );
					
			} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstHiredTeacher;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByJobAndTeacher(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	     	Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(teacherDetail!=null){
				if(jobOrder!=null && teacherDetail!=null){
					Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
					Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
					criteria.add(criterion1);
					criteria.add(criterion2);
				}else if(teacherDetail!=null){
					Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
					criteria.add(criterion1);
				}
				criteria.addOrder(Order.desc("statusMaster"));
				lstTeacherStatusHistoryForJob = criteria.list();
			}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}

	
	private String getColumnId(Integer districtId){
		List<Integer> listColoumn=new ArrayList<Integer>();
		Session session=getSession();
		Criteria criteria = session.createCriteria(EligibilityMaster.class)
	    .setProjection( Projections.projectionList()
	        .add( Projections.property("eligibilityId"), "eligibilityId" )
	    ).add(Restrictions.eq("districtId", districtId)).add(Restrictions.ne("eligibilityCode", "OS")).add(Restrictions.eq("status", "A"));
		listColoumn=criteria.list();
		String column="";
		int countColumn=0;
		for(Integer columnId:listColoumn){
			System.out.println("id------>"+columnId);
			if(countColumn==0)
				column=""+columnId;
			else
				column+=","+columnId;
			countColumn++;
		}
		return column;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findJobByTeachersWithHeadQuarter(TeacherDetail teacherDetail,List<JobOrder> jobOrder) 
	{	
		List<TeacherStatusHistoryForJob> teacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("status","S");
			Criterion criterion5 = Restrictions.or(criterion3,criterion4);
    		Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion5);

			criteria.add(criterion1);
			criteria.add(criterionORW);
			criteria.addOrder(Order.desc("createdDateTime"));
			if(jobOrder!=null && jobOrder.size()>0)
			criteria.add(Restrictions.in("jobOrder",jobOrder));
			teacherList = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherSecondaryStatus(List<TeacherDetail> teacherDetailList,List<SecondaryStatus> secondaryStatusList)
	{
        List <TeacherStatusHistoryForJob> teachHJList =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	if(teacherDetailList.size()>0){
        		 Session session 		= 	getSession();
    			 Criteria criteria 		= 	session.createCriteria(getPersistentClass());
        		 Criterion criterion1   = 	Restrictions.in("teacherDetail",teacherDetailList);
		         Criterion criterion2   = 	Restrictions.in("secondaryStatus",secondaryStatusList);
		         criteria.add(criterion1);
				 criteria.add(criterion2);
				 criteria.addOrder(Order.asc("teacherStatusHistoryForJobId"));
				 teachHJList = criteria.list();
        	}
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teachHJList;
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> FindStatusForLastActivity(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
        try{
        	StatusMaster statusMasterObject = WorkThreadServlet.statusIdMap.get(7);
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);         
	         Criterion  criterion3 = Restrictions.eq("statusMaster",statusMasterObject);
	        
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3);
	        
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestcandidate(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,int districtID,int jobcategoryID, int jobID) 
	{			
		System.out.println("sortColomnName:::"+sortColomnName);
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);			
           if(sortColomnName.equalsIgnoreCase("district"))
				sortColomnName = "District";
			else if(sortColomnName.equalsIgnoreCase("jobid"))
				sortColomnName = "Job_ID";
			else if(sortColomnName.equalsIgnoreCase("JobTitle"))
				sortColomnName = "Job_Title";
			else if(sortColomnName.equalsIgnoreCase("JobCategory"))
				sortColomnName = "Job_Category";
			else if(sortColomnName.equalsIgnoreCase("NumberOfPositions"))
				sortColomnName = "Number_of_Positions";
			else if(sortColomnName.equalsIgnoreCase("NApplications"))
				sortColomnName = "N_Applications";
			else if(sortColomnName.equalsIgnoreCase("NAvailableCandidates"))
				sortColomnName = "N_Available_Candidates";
			else if(sortColomnName.equalsIgnoreCase("NHired"))
				sortColomnName = "N_Hired";
			else if(sortColomnName.equalsIgnoreCase("AverageHireTime"))
				sortColomnName = "Average_Hire_Time";           
			else if(sortColomnName.equalsIgnoreCase("AverageApplyTime"))
				sortColomnName = "Average_Apply_Time";           
			else if(sortColomnName.equalsIgnoreCase("averageScreenTime"))
				sortColomnName = "average_Screen_Time";
			else if(sortColomnName.equalsIgnoreCase("AverageEvalTime"))
				sortColomnName = "Average_Eval_Time";
			else if(sortColomnName.equalsIgnoreCase("AverageVetTime"))
				sortColomnName = "Average_Vet_Time";
			
          
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal  ;
								
			sql=" Select  "+
			 " districtname District, "+
			 " job.jobid Job_ID, "+
			 " jobtitle Job_Title, "+
			 " jobcategoryname Job_Category, "+
			 " ifnull(Number_of_Positions,0) Number_of_Positions, "+
			 " ifnull(N_Applications,0) N_Applications, "+
			 " ifnull(N_Available,0) as N_Available_Candidates, "+
			 " ifnull(N_Hired,0) N_Hired, "+
			 " Average_Hire_Time, "+
			 " Average_Apply_Time, "+
			 " average_Screen_Time, "+
			 " Average_Eval_Time, "+
			 " Average_Vet_Time "+
			 " from  "+
			 " ( "+
			 " select  "+
			 " jd.districtid, "+
			 " districtname, "+
			 " jd.jobid, "+
			 " jobtitle, "+ 
			 " jd.jobcategoryid, "+
			 " jobcategoryname, "+
			 " noofhires, "+
			 " count(*) N_Applications, "+
			 " ifnull(round(avg(hiretime),1),0) Average_Hire_Time, "+
			 " ifnull(round(avg(applytime),1),0) Average_Apply_Time, "+
			 " ifnull(round(avg(screentime),1),0) Average_Screen_Time, "+
			 " ifnull(round(avg(evaltime),1),0) Average_Eval_Time, "+
			 " ifnull(round(avg(vettime),1),0) Average_Vet_Time "+
			 " from "+ 
			 " ( "+
			 " select "+
			 " jft.districtid, "+
			 " jft.teacherid, "+
			 " jo.jobcategoryid, "+
			 " jft.jobid, "+ 
			 " jo.jobtitle, "+
			 " jft.status, "+
			 " jo.noofhires, "+
			 " tshfj6.statusid hired, "+
			 " tshfj16.statusid screened, "+
			 " tshfj17.statusid evaluated, "+
			 " tshfj18.statusid vetted, "+
			 " jft.createddatetime applicationdate, "+
			 " jft.jobcompletedate applcompletedate, "+
			 " tshfj16.createddatetime screeningdate, "+
			 " tshfj17.createddatetime evaluationdate, "+
			 " tshfj18.createddatetime vettingdate, "+
			 " case when tshfj6.hiredbydate is null and jft.status=6 then jft.lastactivitydate "+
			 " when tshfj6.hiredbydate is null and jft.status<>6 then null "+
			 " else tshfj6.hiredbydate end `hireddate`, "+
			 " datediff(case "+ 
			 " when tshfj6.hiredbydate is null and jft.status=6 "+
			 " then jft.lastactivitydate "+
			 " when tshfj6.hiredbydate is null and jft.status<>6 "+
			 " then null "+
			 " else tshfj6.hiredbydate "+
			 " end, jft.createddatetime) hiretime, "+
			 " datediff(jft.jobcompletedate, jft.createddatetime) applytime, "+
			 " datediff(tshfj16.createddatetime, jft.createddatetime) screentime, "+
			 " datediff(tshfj17.createddatetime, jft.createddatetime) evaltime, "+
			 " datediff(tshfj18.createddatetime, jft.createddatetime) vettime "+
			 " from jobforteacher jft "+
			 " left join teacherstatushistoryforjob tshfj6 on tshfj6.teacherid=jft.teacherid "+
			 " and tshfj6.jobid=jft.jobid "+
			 " and tshfj6.statusid=6 "+
			 " left join teacherstatushistoryforjob tshfj16 on tshfj16.teacherid=jft.teacherid "+
			 " and tshfj16.jobid=jft.jobid "+
			 " and tshfj16.statusid=16 "+
			 " left join teacherstatushistoryforjob tshfj17 on tshfj17.teacherid=jft.teacherid "+
			 " and tshfj17.jobid=jft.jobid "+
			 " and tshfj17.statusid=17 "+
			 " left join teacherstatushistoryforjob tshfj18 on tshfj18.teacherid=jft.teacherid "+
			 " and tshfj18.jobid=jft.jobid "+
			 " and tshfj18.statusid=18 "+
			 " inner join joborder jo on jft.jobid=jo.jobid "+
			 " where jft.status<>8 "+
			 " ) jd "+
			 " left join districtmaster dm on dm.districtid=jd.districtId "+
			 " left join jobcategorymaster jcm on jcm.jobCategoryId=jd.jobCategoryId "+
			 " group by jobid "+ 
			 " ) job "+
			 " left join	( "+
			 " select "+ 
			 " sij.schoolId as Internal_School_ID, "+ 
			 " sij.jobid as Internal_Job_ID, "+ 
			 " sum(sij.noOfSchoolExpHires) as Number_of_Positions "+
			 " from schoolinjoborder sij "+
			 " inner join joborder jo on sij.jobid=jo.jobid "+
			 " group by jo.districtid, sij.jobid "+
			 " ) pos on job.jobid=pos.Internal_Job_ID "+
			 " left join "+
			 " ( "+
			 " select jft.jobid, count(*) N_Available "+
			 " from jobforteacher jft "+
			 " inner join joborder jo on jft.jobid=jo.jobid "+
			 " left join statusmaster sm on sm.statusid=jft.status "+
			 " where jft.status not in (3,7,8,9) "+
			 " group by jft.jobid "+
			 " ) Ava on Ava.jobid=job.jobid "+
			 " left join "+
			 " ( "+
			 " select jft.jobid, count(*) N_hired "+
			 " from jobforteacher jft "+
			 " inner join joborder jo on jft.jobid=jo.jobid "+
			 " left join teacherstatushistoryforjob tshfj6 on tshfj6.teacherid=jft.teacherid "+
			 " and tshfj6.jobid=jft.jobid "+
			 " and tshfj6.statusid=6 "+
			 " left join statusmaster sm on sm.statusid=jft.status "+
			 " where (jft.status=6 or tshfj6.statusid=6) "+
			 " group by jft.jobid "+
			 " ) hired on hired.jobid=job.jobid "+
			 " where if ("+districtID+"=0, job.districtid<>0, job.districtid="+districtID+") "+
			 " and if ("+jobcategoryID+"=0, job.jobcategoryid<>0, job.jobcategoryid="+jobcategoryID+") "+
			 " and if ("+jobID+"=0, job.jobid<>0, job.jobid="+jobID+") "+orderby;
		 
		 
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);	
		
		ResultSet rs=ps.executeQuery();			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}

	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findJobByTeachersWithHeadQuater(TeacherDetail teacherDetail,JobOrder jobOrder) 
	{	
		List<TeacherStatusHistoryForJob> teacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("status","S");
			Criterion criterion5 = Restrictions.or(criterion3,criterion4);
    		Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion5);

			criteria.add(criterion1);
			criteria.add(criterionORW);
			criteria.addOrder(Order.desc("createdDateTime"));
			criteria.createCriteria("jobOrder").addOrder(Order.desc("createdDateTime")).add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			teacherList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
	
	@Transactional(readOnly=false)
	 @SuppressWarnings("unchecked")
	 public  List<String[]> jobPDPReportforNCGeneral(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,int districtID,String jobcategoryname,String jobTitle,int jobstatusId) 
	 {
	  PrintOnConsole.getJFTPrint("JFT:101");
	  List<String[]> lst=new ArrayList<String[]>(); 
	  Session session = getSession();
	  String sql = "";
	   Connection connection =null;
	  try {
	      SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
	      ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
	        connection = connectionProvider.getConnection();
	   
	   
	            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
	          
	   
	           if(sortColomnName.equalsIgnoreCase("districtname"))
	    sortColomnName = "District_Name";
	   else if(sortColomnName.equalsIgnoreCase("jobtitle"))
	    sortColomnName = "Job_Title";
	   else if(sortColomnName.equalsIgnoreCase("planning"))
	    sortColomnName = "Planning";
	   else if(sortColomnName.equalsIgnoreCase("lerningEnvrmnt"))
	    sortColomnName = "Learning_Environment";
	   else if(sortColomnName.equalsIgnoreCase("linstruct"))
	    sortColomnName = "Instruct";
	   else if(sortColomnName.equalsIgnoreCase("analyzeandAdjust"))
	    sortColomnName = "Analyze_and_Adjust";
	   
	           String orderby = " order by "+sortColomnName+" "+sortOrderStrVal  ;
	           
	          String jobTle="";
	           String jobcategrnm="";
	           if(jobTitle.equalsIgnoreCase("0"))
	            jobTle="and if('' ='',jo.jobtitle is not null, jo.jobtitle like '%%')";
	           else
	            jobTle="and if('"+jobTitle+"' ='',jo.jobtitle is not null, jo.jobtitle like '%"+jobTitle+"%')";
	           
	           if(jobcategoryname.equalsIgnoreCase("0"))
	            jobcategrnm="and if(''='',jcm.jobCategoryName is not null, jcm.jobCategoryName like '%%')";
	           else
	            jobcategrnm="and if('"+jobcategoryname+"' ='',jcm.jobCategoryName is not null, jcm.jobCategoryName like '%"+jobcategoryname+"%')";;
	           	           
	           sql=" select "+ 
	           " `District_Name`, "+ 
	           " `Job_Title`, "+ 
	           " round(avg(`Planning`),2) as `Planning`, "+
	           " round(avg(`Learning`),2) as `Learning_Environment`, "+
	           " round(avg(`Instruct`),2) as `Instruct`, "+
	           " round(avg(`AandA`),2) as `Analyze_and_Adjust` "+
	           " from ( "+
	           " Select "+
	           " districtname `District_Name`, "+ 
	           " firstname `Candidate First Name`, "+ 
	           " lastname `Candidate Last Name`, "+
	           " emailaddress `Candidate Email`, "+
	           " jobtitle `Job_Title`, "+
	           " cast(ifnull(avg(if(competencyId=9,competencynormscore,null)),'') as char) as `Planning`, "+
	           " cast(ifnull(avg(if(competencyId=10,competencynormscore,null)),'') as char) as `Learning`, "+
	           " cast(ifnull(avg(if(competencyId=11,competencynormscore,null)),'') as char) as `Instruct`, "+
	           " cast(ifnull(avg(if(competencyId=12,competencynormscore,null)),'') as char) as `AandA` "+
	           " from "+
	           " ( "+
	           " select "+ 
	           " td.teacherid, "+
	           " td.emailaddress, "+ 
	           " td.firstname, "+ 
	           " td.lastname, "+
	           " epi.teachernormscore, "+
	           " ads.domainid, "+
	           " ads.normscore as domainnormscore, "+
	           " acs.competencyid, "+
	           " acs.normscore as competencynormscore, "+
	           " dm.districtname, "+ 
	           " jo.jobid, "+
	           " jo.jobtitle, "+
	           " sm.status "+
	           " from jobforteacher jft "+
	           " inner join teacherdetail td on jft.teacherid=td.teacherid "+
	           " inner join joborder jo on jft.jobid=jo.jobid "+
	           " inner join jobcategorymaster jcm on jcm.jobcategoryid=jo.jobcategoryid "+
	           " inner join districtmaster dm on jo.districtid=dm.districtid "+
	           " inner join statusmaster sm on sm.statusid=jft.status "+
	           " inner join teachernormscore epi on td.teacherid=epi.teacherid "+
	           " inner join assessmentdomainscore ads on epi.teacherid=ads.teacherid "+
	           " left join assessmentcompetencyscore acs on ads.teacherid=acs.teacherid and ads.domainid=3 "+
	           " where "+ 
	           " epi.teachernormscore is not null "+
	           " and if("+districtID+"=0,jo.districtid is not null,jo.districtid="+districtID+") "+
	           " and if("+jobstatusId+"=0, jft.status is not null, jft.status ="+jobstatusId+") "+
	           " "+jobTle+" "+
	           " "+jobcategrnm+" "+
	           " ) as pdp "+
	           " group by teacherid ) as teacherpdp "+
	           " Group by `District_Name`, `Job_Title` "+orderby;
	   
	  PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE); 
	 
	  ResultSet rs=ps.executeQuery();
	 
	   if(rs.next()){    
	    do{
	     final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
	     for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
	        {
	      allInfo[i-1]=rs.getString(i);
	        }
	     lst.add(allInfo);
	     
	    }while(rs.next());
	   }
	   else{
	    System.out.println("Record not found.");
	   }
	   
	   return lst;
	  } catch (Exception e) {
	   e.printStackTrace();
	  }finally{
	   if(connection!=null)
	    try {
	     connection.close();
	    } catch (SQLException e) {
	     // TODO Auto-generated catch block
	     e.printStackTrace();
	    }
	  }
	  
	  
	  
	  return null;
	 
	 }
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> findByTeacherListJobList(List<TeacherDetail> teacherDetail,List<JobOrder> jobOrder,StatusMaster statusMaster)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
	         Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.in("jobOrder",jobOrder);
	         Criterion criterion3 = Restrictions.eq("statusMaster",statusMaster);
	         Criterion criterion4 = Restrictions.eq("status", "A");
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterion4);
        }catch(Exception e){
        	e.printStackTrace();
        }
		return lstTeacherStatusHistoryForJob;
	}

	
//	Jai Shri Ram!
	
	@Transactional(readOnly=false)
	 @SuppressWarnings("unchecked")
	 public  List<String[]> getHiredPDPReport(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,int districtID,String jobcategoryname,String jobTitle,int jobstatusId,int tchid,String fromdate,String endDate) 
	 {
		 PrintOnConsole.getJFTPrint("JFT:101");
	  List<String[]> lst=new ArrayList<String[]>(); 
	  Session session = getSession();
	  String sql = "";
	   Connection connection =null;
	  try {
	      SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
	      ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
	        connection = connectionProvider.getConnection();

	        if(sortColomnName.equalsIgnoreCase("districtname"))
	        	   sortColomnName = "District_Name";
	           else if(sortColomnName.equalsIgnoreCase("firstname"))
	   		    sortColomnName = "Candidate_First_Name";
	           else if(sortColomnName.equalsIgnoreCase("lastname"))
				    sortColomnName = "Candidate_Last_Name";
	           else if(sortColomnName.equalsIgnoreCase("emailaddress"))
				    sortColomnName = "Candidate_Email";
	           else if(sortColomnName.equalsIgnoreCase("planning"))
				    sortColomnName = "Planning";
	           else if(sortColomnName.equalsIgnoreCase("lerningEnvrmnt"))
				    sortColomnName = "Learning_Environment";
	           else if(sortColomnName.equalsIgnoreCase("linstruct"))
				    sortColomnName = "Instruct";
	           else if(sortColomnName.equalsIgnoreCase("analyzeandAdjust"))
				    sortColomnName = "Analyze_and_Adjust";
	           
			  /* else if(sortColomnName.equalsIgnoreCase("jobtitle"))
			    sortColomnName = "Job_Title";*/
	   
	           String orderby = " order by "+sortColomnName+" "+sortOrderStrVal  ;
	           
	           String jobTle="";
	           String jobcategrnm="";
	           if(jobTitle.equalsIgnoreCase("0"))
	            jobTle="and if('' ='',jo.jobtitle is not null, jo.jobtitle like '%%')";
	           else
	            jobTle="and if('"+jobTitle+"' ='',jo.jobtitle is not null, jo.jobtitle like '%"+jobTitle+"%')";
	           
	           if(jobcategoryname.equalsIgnoreCase("0"))
	            jobcategrnm="and if(''='',jcm.jobCategoryName is not null, jcm.jobCategoryName like '%%')";
	           else
	            jobcategrnm="and if('"+jobcategoryname+"' ='',jcm.jobCategoryName is not null, jcm.jobCategoryName like '%"+jobcategoryname+"%')";;
	            
	            String dateformate="";
	            System.out.println(fromdate);
	            System.out.println(endDate);
	            if(!fromdate.equalsIgnoreCase("0") && endDate.equalsIgnoreCase("0"))
	            		dateformate=	"and (jft.hiredbydate >='"+fromdate+" 00:00:01')";
	            else if(fromdate.equalsIgnoreCase("0") && !endDate.equalsIgnoreCase("0"))
	            	dateformate=	" and jft.hiredbydate <='"+endDate+" 23:59:59')";
	            else if(!fromdate.equalsIgnoreCase("0") && !endDate.equalsIgnoreCase("0"))
			            	dateformate=" and (jft.hiredbydate >='"+fromdate+" 00:00:01' and jft.hiredbydate <='"+endDate+" 23:59:59')";
			            
			    //shriram////////////////////////
	            System.out.println("districtID====="+districtID);
	          
	            	
	          sql=" Select "+
	          " districtname `District_Name`, "+  
	          " firstname `Candidate_First_Name`, "+ 
	          " lastname `Candidate_Last_Name`, "+
	          " emailaddress `Candidate_Email`, "+ 
	          " cast(ifnull(avg(if(competencyId=9,competencynormscore,null)),'') as char) as `Planning`, "+
	          " cast(ifnull(avg(if(competencyId=10,competencynormscore,null)),'') as char) as `Learning_Environment`, "+
	          " cast(ifnull(avg(if(competencyId=11,competencynormscore,null)),'') as char) as `Instruct`, "+
	          " cast(ifnull(avg(if(competencyId=12,competencynormscore,null)),'') as char) as `Analyze_and_Adjust` "+
	          "  from "+
	          "  ( "+
	          " select "+ 
	          " td.teacherid, "+
	       		" 	td.emailaddress, "+
	          " td.firstname, "+ 
	          " td.lastname, "+
	          " epi.teachernormscore, "+
	          " ads.domainid, "+
	          " ads.normscore as domainnormscore, "+
	          " acs.competencyid, "+
	          " acs.normscore as competencynormscore, "+
	          " dm.districtname, "+
	          " sm.status "+
	          " from jobforteacher jft "+
	          " inner join teacherdetail td on jft.teacherid=td.teacherid "+
	          " inner join joborder jo on jft.jobid=jo.jobid "+
	          " inner join jobcategorymaster jcm on jcm.jobcategoryid=jo.jobcategoryid "+
	          " inner join districtmaster dm on jo.districtid=dm.districtid "+
	          " inner join statusmaster sm on sm.statusid=jft.status "+
	          " inner join teachernormscore epi on td.teacherid=epi.teacherid "+
	          " inner join assessmentdomainscore ads on epi.teacherid=ads.teacherid "+
	          " left join assessmentcompetencyscore acs on ads.teacherid=acs.teacherid and ads.domainid=3 "+
	          " where "+ 
	          " epi.teachernormscore is not null "+
	          " and if("+districtID+"=0,jo.districtid is not null,jo.districtid="+districtID+") "+
	        /*  " and jft.status = "+jobstatusId+*/
	         // " and if("+jobstatusId+"=0, jft.status is not null, jft.status ="+jobstatusId+") "+
             " and jft.status = '6' "+
	          " "+jobTle+" "+
	          " "+jobcategrnm+" "+
	          " "+dateformate+" "+
	          " ) as pdp "+
	          " group by teacherid "+orderby;
	          
	          System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::"+sql);
	      
	  PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE); 
	 System.out.println(":::::::::::befor the query:::::::::::::::::::::::::::::::::::::::::::::::::::::");
	
	 ResultSet rs=ps.executeQuery();

	  System.out.println(":::::::::::::::::;after the query:::::::::::::::::::::::::::::::::::::::::::::::::::::"+jobstatusId);
	   if(rs.next()){    
	    do{
	     final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
	     for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
	        {
	      allInfo[i-1]=rs.getString(i);
	        }
	     lst.add(allInfo);
	     
	    }while(rs.next());
	   }
	   else{
	    System.out.println("Record not found.");
	   }
	   
	   return lst;
	  } catch (Exception e) {
	   e.printStackTrace();
	  }finally{
	   if(connection!=null)
	    try {
	     connection.close();
	    } catch (SQLException e) {
	     // TODO Auto-generated catch block
	     e.printStackTrace();
	    }
	  }
	  
	  
	  return null;
	 
	 }
	
	@Transactional(readOnly=true)
	public List<TeacherDetail> getTeacherDetailByHireddDate(DistrictMaster districtId,String date)
	{
		Session session=getSession();
		StatusMaster statusMaster=new StatusMaster();
		statusMaster.setStatusId(6);
		try
		{
			DetachedCriteria subQuery=DetachedCriteria.forClass(TeacherPersonalInfo.class,"personal")
			.add(Restrictions.eq("teacherId", "history.teacherDetail"))
			.setProjection(Projections.projectionList().add(Projections.property("teacherId")));
			return session.createCriteria(getPersistentClass(),"history")
			.add(Restrictions.between("hiredByDate", Utility.getLowDate(date), Utility.getHighDate(date)))
			.add(Restrictions.eq("statusMaster", statusMaster))
			.createCriteria("jobOrder", "jobOrder")
			.add(Restrictions.eq("jobOrder.districtMaster", districtId))
			.setProjection(Projections.projectionList().add(Projections.groupProperty("history.teacherDetail")))
			.list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
		
		
	}
	
	@Transactional(readOnly=true)
	public List getTeachersByHireddDate(DistrictMaster districtId,String date,List<StatusMaster> lstStatusMasters,List<TeacherDetail> listSentSapCandidateDetails)
	{
		Session session=getSession();
		/*StatusMaster statusMaster=new StatusMaster();
		statusMaster.setStatusId(6);*/
		
		try
		{
			Criteria criteria = session.createCriteria(getPersistentClass(),"history");
			if(date!=null && !date.isEmpty())
			criteria.add(Restrictions.between("hiredByDate", Utility.getLowDate(date), Utility.getHighDate(date)));
			criteria.add(Restrictions.in("statusMaster", lstStatusMasters));
			
			Criteria c1 = criteria.createCriteria("jobOrder", "jobOrder");
			c1.add(Restrictions.eq("jobOrder.districtMaster", districtId));
	//		System.out.println("listSentSapCandidateDetails size:::"+listSentSapCandidateDetails.size());
			criteria.add(Restrictions.not(Restrictions.in("teacherDetail", listSentSapCandidateDetails)));
			c1.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("history.teacherDetail"))
					.add(Projections.groupProperty("history.jobOrder"))
					);
			return criteria.list();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}
	}
	@Transactional(readOnly=true)
	public List<TeacherStatusHistoryForJob> getHistoryStatusList(TeacherDetail teacherDetail,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        try{
        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("status", "S");
			Criterion criterion5 = Restrictions.eq("status", "W");
			Criterion criterion6 = Restrictions.or(criterion3, criterion4);
			Criterion criterion7 = Restrictions.or(criterion6, criterion5);
			Criterion criterion8 = null;
	         if(statusMaster!=null){
	        	 criterion8 = Restrictions.eq("statusMaster",statusMaster);
	         }else{
	        	 criterion8 = Restrictions.eq("secondaryStatus",secondaryStatus);
	         }
			lstTeacherStatusHistoryForJob=findByCriteria(criterion1,criterion7,criterion8);
        }catch(Exception e){
        	e.printStackTrace();
        }
        return lstTeacherStatusHistoryForJob;
	}
	
	@Transactional(readOnly=false)	
	public List<TeacherStatusHistoryForJob> findJobsHistoryByTeachers(TeacherDetail teacherDetail) 
	{	
		List<TeacherStatusHistoryForJob> teacherList = new ArrayList<TeacherStatusHistoryForJob>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
			Criterion criterion3 = Restrictions.eq("status", "A");
			Criterion criterion4 = Restrictions.eq("status","S");
			Criterion criterion5 = Restrictions.or(criterion3,criterion4);
    		Criterion criterionW = Restrictions.eq("status","W");
			Criterion criterionORW = Restrictions.or(criterionW, criterion5);

			criteria.add(criterion1);
			criteria.add(criterionORW);
			criteria.addOrder(Order.desc("createdDateTime"));
			teacherList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherList;
	}
		@Transactional(readOnly=true)
	public TeacherStatusHistoryForJob findWaivedTeacherStatusHistory(TeacherDetail teacherDetail,JobOrder jobOrder,String status)
	{
        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
        TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
        try{
	         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
	         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
	         Criterion criterion4 = Restrictions.eq("status",status);
	         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion4);
	         if(lstTeacherStatusHistoryForJob!=null && lstTeacherStatusHistoryForJob.size()>0){
	        	 teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
	         }
        }catch(Exception e){
        	e.printStackTrace();
        }
		return teacherStatusHistoryForJob;
	}
		
		@Transactional(readOnly=true)
		public List<TeacherStatusHistoryForJob> findByTeacherStatusHistorySelectedOp(TeacherDetail teacherDetail,JobOrder jobOrder)
		{
	        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
	        try{
	        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("status","S");
				Criterion criterion4 = Restrictions.eq("status","W");
				Criterion criterion5 = Restrictions.or(criterion3, criterion4);
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion5);
				criteria.addOrder(Order.desc("statusMaster"));
				
				criteria.createAlias("secondaryStatus", "secondaryStatus",Criteria.LEFT_JOIN).createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN)
				.createAlias("userMaster", "usermaster",Criteria.LEFT_JOIN).createAlias("usermaster.schoolId", "schoolId",Criteria.LEFT_JOIN)
				.setProjection(Projections.projectionList()
						.add( Projections.property("teacherStatusHistoryForJobId"))
						.add( Projections.property("statusmaster.statusId"), "statusId" )
						.add( Projections.property("secondaryStatus.secondaryStatusId"), "secondaryStatusId" )
						.add( Projections.property("status"))
						.add( Projections.property("override")) //boolean		
						.add( Projections.property("usermaster.userId"), "userId" )
			    		.add( Projections.property("usermaster.entityType"), "entityType" )
			    		.add( Projections.property("schoolId.schoolId"), "schoolId" )
			    		.add( Projections.property("statusmaster.statusShortName"), "statusShortName" )
			    		.add( Projections.property("statusmaster.status"))
			    		.add( Projections.property("secondaryStatus.secondaryStatusName"))
			    		);
				List<String []> lstTeacherStatusHistoryForJobTemp=new ArrayList<String []>();
				
				lstTeacherStatusHistoryForJobTemp = criteria.list();
				
				for (Iterator it = lstTeacherStatusHistoryForJobTemp.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();    				
					if(row[0]!=null){
						TeacherStatusHistoryForJob teacherStatusHistoryForJob = new TeacherStatusHistoryForJob();
						teacherStatusHistoryForJob.setTeacherStatusHistoryForJobId(Long.parseLong(row[0].toString()));
						
						if(row[1]!=null){
							StatusMaster statusMaster = new StatusMaster();
							statusMaster.setStatusId(Integer.parseInt(row[1].toString()));
							statusMaster.setStatusShortName(row[8].toString());
							statusMaster.setStatus(row[9].toString());
							teacherStatusHistoryForJob.setStatusMaster(statusMaster);
						}
						
						if(row[2]!=null){
							SecondaryStatus secondaryStatus = new SecondaryStatus();
							secondaryStatus.setSecondaryStatusId(Integer.parseInt(row[2].toString()));
							secondaryStatus.setSecondaryStatusName(row[10].toString());
							teacherStatusHistoryForJob.setSecondaryStatus(secondaryStatus);
						}
						if(row[3]!=null){
							teacherStatusHistoryForJob.setStatus(row[3].toString());
						}						
						if(row[4]!=null){
							teacherStatusHistoryForJob.setOverride(Boolean.parseBoolean(row[4].toString()));
						}
						if(row[5]!=null){
							UserMaster userMaster = new UserMaster();
							userMaster.setUserId(Integer.parseInt(row[5].toString()));
							userMaster.setEntityType(Integer.parseInt(row[6].toString()));
							
							if(row[7]!=null){
								SchoolMaster schoolMaster = new SchoolMaster();
								schoolMaster.setSchoolId(Long.parseLong(row[7].toString()));
								userMaster.setSchoolId(schoolMaster);	
							}
							teacherStatusHistoryForJob.setUserMaster(userMaster);
						}
						teacherStatusHistoryForJob.setJobOrder(jobOrder);
						teacherStatusHistoryForJob.setTeacherDetail(teacherDetail);
						lstTeacherStatusHistoryForJob.add(teacherStatusHistoryForJob);
					}
				}
				
				/*for (TeacherStatusHistoryForJob stringstemp : lstTeacherStatusHistoryForJob) {
					System.out.println(" id : "+stringstemp.getTeacherStatusHistoryForJobId());
				}*/
				
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			return lstTeacherStatusHistoryForJob;
		}
		
		@Transactional(readOnly=false)	
		public List<TeacherStatusHistoryForJob> findHireTeacherListByHBD_OP(List<TeacherDetail> teachersList,JobOrder jobOrder,Map<String,StatusMaster> statusMap) 
		{	
			List<TeacherStatusHistoryForJob> hiredTeacherList = new ArrayList<TeacherStatusHistoryForJob>();
			try 
			{
				if(teachersList.size()>0){
					Session session = getSession();
					Criteria criteria = session.createCriteria(getPersistentClass());
					Criterion criterion1 = Restrictions.in("teacherDetail",teachersList);
					Criterion criterion2 = Restrictions.eq("statusMaster", statusMap.get("hird"));
					Criterion criterion3 = Restrictions.eq("status", "A");
					
					Criterion criterionR = Restrictions.eq("statusMaster", statusMap.get("rem"));
					Criterion criterionRA = Restrictions.eq("status", "A");
					Criterion criterionRM = Restrictions.and(criterionR,criterionRA);
					
					Criterion criterionD = Restrictions.eq("statusMaster", statusMap.get("dcln"));
					Criterion criterionDA = Restrictions.eq("status", "A");
					Criterion criterionDM = Restrictions.and(criterionD,criterionDA);
					
					Criterion criterionW = Restrictions.eq("statusMaster", statusMap.get("widrw"));
					Criterion criterionWA = Restrictions.eq("status", "A");
					Criterion criterionWM = Restrictions.and(criterionW,criterionWA);
					
					if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390){
						Criterion criterion4 = Restrictions.and(criterion2,criterion3);
						Criterion criterion5 = Restrictions.eq("statusMaster", statusMap.get("vcomp"));
						Criterion criterionS5 = Restrictions.eq("status", "S");
						Criterion criterionW5 = Restrictions.eq("status","W");
						Criterion criterionORW5 = Restrictions.or(criterionW5, criterionS5);
						Criterion criterion7 = Restrictions.and(criterion5,criterionORW5);
						Criterion criterion8 = Restrictions.or(criterion4,criterion7);
						Criterion criterion9 = Restrictions.or(criterionRM,criterionDM);
						Criterion criterion10 = Restrictions.or(criterionWM,criterion8);
						Criterion criterion11 = Restrictions.or(criterion10,criterion9);
						criteria.add(criterion1);
						criteria.add(criterion11);
					}else{
						criteria.add(criterion1);
						criteria.add(criterion2);
						criteria.add(criterion3);
					}
					//Criteria jobOrderSet= criteria.createCriteria("jobOrder");
					if(jobOrder.getDistrictMaster()!=null){
						criteria.add(Restrictions.eq("jo.districtMaster",jobOrder.getDistrictMaster()));
					}else{
						criteria.add(Restrictions.isNull("jo.districtMaster"));
					}
					if(jobOrder.getBranchMaster()!=null){
						criteria.add(Restrictions.eq("jo.branchMaster",jobOrder.getBranchMaster()));
					}else{
						criteria.add(Restrictions.isNull("jo.branchMaster"));
					}
					if(jobOrder.getHeadQuarterMaster()!=null){
						criteria.add(Restrictions.eq("jo.headQuarterMaster",jobOrder.getHeadQuarterMaster()));
					}else{
						criteria.add(Restrictions.isNull("jo.headQuarterMaster"));
					}
					criteria.createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN).createAlias("teacherDetail", "td")
					.createAlias("jobOrder", "jo")
					.setProjection(Projections.projectionList()
							.add( Projections.property("teacherStatusHistoryForJobId"))
							.add( Projections.property("statusmaster.statusId"))
				    		.add( Projections.property("statusmaster.statusShortName"))
				    		.add( Projections.property("td.teacherId"))
				    		.add( Projections.property("jo.jobId"))
				    		.add( Projections.property("jo.jobTitle"))
				    		.add( Projections.property("hiredByDate"))
				    		);
					List<String []> lstTeacherStatusHistoryForJobTemp=new ArrayList<String []>();
					lstTeacherStatusHistoryForJobTemp = criteria.list();

					for (Iterator it = lstTeacherStatusHistoryForJobTemp.iterator(); it.hasNext();)
					{
						Object[] row = (Object[]) it.next();    				
						if(row[0]!=null){
							TeacherStatusHistoryForJob teacherStatusHistoryForJob = new TeacherStatusHistoryForJob();
							teacherStatusHistoryForJob.setTeacherStatusHistoryForJobId(Long.parseLong(row[0].toString()));
							
							if(row[1]!=null){
								StatusMaster statusMaster = new StatusMaster();
								statusMaster.setStatusId(Integer.parseInt(row[1].toString()));
								statusMaster.setStatusShortName(row[2].toString());
								teacherStatusHistoryForJob.setStatusMaster(statusMaster);
							}
							if(row[3]!=null){
								TeacherDetail td = new TeacherDetail();
								td.setTeacherId(Integer.parseInt(row[3].toString()));
								teacherStatusHistoryForJob.setTeacherDetail(td);
							}
							if(row[4]!=null){
								JobOrder jo = new JobOrder();
								jo.setJobId(Integer.parseInt(row[4].toString()));
								jo.setJobTitle(row[5].toString());
								teacherStatusHistoryForJob.setJobOrder(jo);
							}
							
							
							try {
	    						if(row[6]!=null){
	    							DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		        					Date hiredByDate = df.parse(row[6].toString());
		        					teacherStatusHistoryForJob.setHiredByDate(hiredByDate);
	    						}
							} catch (Exception e) {}
							
							hiredTeacherList.add(teacherStatusHistoryForJob);
						}
						}
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return hiredTeacherList;
		}
		//SWADESH
		@Transactional(readOnly=false)	
		public List<TeacherStatusHistoryForJob> findReasonAndTiming(List<TeacherDetail> teacherDetail,List<JobOrder> jobOrder,String reason,String timing,String searchbystatus) 
		{	
			List<TeacherStatusHistoryForJob> teacherStatusHistoryForJob = new ArrayList<TeacherStatusHistoryForJob>();
			StatusMaster statusMaster=new StatusMaster();
    		StatusMaster statusMaster1=new StatusMaster();
    		DistrictSpecificReason districtSpecificReason=new DistrictSpecificReason();
    		DistrictSpecificTiming districtSpecificTiming=new DistrictSpecificTiming();
    		if(!reason.equals("0"))
    		districtSpecificReason.setReasonId(Integer.parseInt(reason));
    		if(!timing.equals("0"))
    		districtSpecificTiming.setTimingId(Integer.parseInt(timing));
			try 
			{
				Session session = getSession();
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion=null;
				Criterion criterion4=null;
				Criterion criterion1=null;
				statusMaster= WorkThreadServlet.statusMap.get("widrw");
	    		statusMaster1=WorkThreadServlet.statusMap.get("dcln");
	    		if(teacherDetail.size()>0)
				criterion=Restrictions.in("teacherDetail", teacherDetail);
	    		if(searchbystatus.equals("0"))
				criterion1=Restrictions.or(Restrictions.eq("statusMaster",statusMaster), Restrictions.eq("statusMaster",statusMaster1));
	    		else if(searchbystatus.equals("7"))
	    		criterion1=Restrictions.eq("statusMaster",statusMaster);
	    		else if(searchbystatus.equals("19"))
	    		criterion1=Restrictions.eq("statusMaster",statusMaster1);
				Criterion criterion2=Restrictions.isNotNull("timingforDclWrdw");
				Criterion criterion3= Restrictions.isNotNull("reasonforDclWrdw");
				Criterion criterion7= Restrictions.eq("timingforDclWrdw", districtSpecificTiming);
				Criterion criterion8= Restrictions.eq("reasonforDclWrdw", districtSpecificReason);
				if(jobOrder.size()>0)
				criterion4=Restrictions.in("jobOrder", jobOrder);
				Criterion criterion5=Restrictions.sqlRestriction("{alias}.timingforDclWrdw !=''");
				Criterion criterion6=Restrictions.sqlRestriction("{alias}.reasonforDclWrdw !=''");
				if(teacherDetail.size()>0)
				criteria.add(criterion);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				if(jobOrder.size()>0)
				criteria.add(criterion4);
				criteria.add(criterion5);
				criteria.add(criterion6);
				if(!timing.equals("0"))
				criteria.add(criterion7);
				if(!reason.equals("0"))
				criteria.add(criterion8);
				criteria.addOrder(Order.asc("teacherDetail"));
				teacherStatusHistoryForJob = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return teacherStatusHistoryForJob;
		}
		
		
		
		/**
		 * @author Ankit Sharma
		 * @param teacherDetailList
		 * @param secondaryStatusList
		 * @return
		 * @implemented for KellyONRAjax
		 */
		@Transactional(readOnly=true)
		public List<TeacherStatusHistoryForJob> findByTeacherSecondaryStatus_Op(List<TeacherDetail> teacherDetailList,List<SecondaryStatus> secondaryStatusList)
		{
	        List <TeacherStatusHistoryForJob> teachHJList =new ArrayList<TeacherStatusHistoryForJob>();
	        try{
	        	if(teacherDetailList.size()>0){
	        		 Session session 		= 	getSession();
	    			 Criteria criteria 		= 	session.createCriteria(getPersistentClass());
	        		 Criterion criterion1   = 	Restrictions.in("teacherDetail",teacherDetailList);
			         Criterion criterion2   = 	Restrictions.in("secondaryStatus",secondaryStatusList);
			         criteria.add(criterion1);
					 criteria.add(criterion2);
					 criteria.addOrder(Order.asc("teacherStatusHistoryForJobId"));
					 criteria.createAlias("secondaryStatus", "secondaryst",Criteria.LEFT_JOIN)
					 .setProjection(Projections.projectionList().add( Projections.property("teacherStatusHistoryForJobId"))
							 .add(Projections.property("teacherDetail.teacherId"))
							 .add(Projections.property("jobOrder.jobId"))
							 .add(Projections.property("secondaryst.secondaryStatusName")));
					 
					 List<String []> lstTeacherStatusHistoryForJobTemp=new ArrayList<String []>();
					 lstTeacherStatusHistoryForJobTemp = criteria.list();
					 
					 for (Iterator it = lstTeacherStatusHistoryForJobTemp.iterator(); it.hasNext();)
						{
						 TeacherStatusHistoryForJob teacherStatusHistoryForJob = new TeacherStatusHistoryForJob();
						 Object[] row = (Object[]) it.next();    				
							if(row[0]!=null){
									teacherStatusHistoryForJob.setTeacherStatusHistoryForJobId(Long.parseLong(row[0].toString()));
							}
							
							if(row[1]!=null){
								TeacherDetail teacherDetail = new TeacherDetail();
								teacherDetail.setTeacherId(Integer.parseInt(row[1].toString()));
								teacherStatusHistoryForJob.setTeacherDetail(teacherDetail);
							}
							
							if(row[2]!=null){
								JobOrder jobOrder = new JobOrder();
								jobOrder.setJobId(Integer.parseInt(row[2].toString()));
								teacherStatusHistoryForJob.setJobOrder(jobOrder);
							}
							
							if(row[3]!=null){
								SecondaryStatus secondaryStatus = new SecondaryStatus();
								secondaryStatus.setSecondaryStatusName(row[3].toString());
								teacherStatusHistoryForJob.setSecondaryStatus(secondaryStatus);
							}
							
							teachHJList.add(teacherStatusHistoryForJob);
						}
					 
	        	}
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			return teachHJList;
		}
		
		
		/**
		 * @author Ankit Sharma
		 * @param teacherDetails
		 * @param jobOrders
		 * @return
		 * @implemented for KellyONRAjax
		 */
		@Transactional(readOnly=true)
		public List <TeacherStatusHistoryForJob> findHistoryByTeachersAndJobs_Op(List<TeacherDetail> teacherDetails,List<JobOrder> jobOrders)
		{
	        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
	        try{
	        	if(teacherDetails.size()>0){
	        		Session session 		= 	getSession();
	    			 Criteria criteria 		= 	session.createCriteria(getPersistentClass());
	    			 
			         Criterion criterion1 = Restrictions.in("teacherDetail",teacherDetails);
			         Criterion criterion2 = Restrictions.in("jobOrder",jobOrders);
			         Criterion criterion4 = Restrictions.eq("status","S");
			         Criterion criterion5 = Restrictions.eq("status","W");
			         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
			         criteria.add(criterion1);
			         criteria.add(criterion2);
			         criteria.add(criterion6);
			         criteria.setProjection(Projections.projectionList()
							 .add(Projections.property("teacherDetail.teacherId"))
							 .add(Projections.property("jobOrder.jobId"))
							 .add(Projections.property("secondaryStatus.secondaryStatusId"))
							 .add(Projections.property("statusMaster.statusId"))
							 .add(Projections.property("status")));
			         
			         List<String []> lstTeacherStatusHistoryForJobTemp=new ArrayList<String []>();
			         lstTeacherStatusHistoryForJobTemp = criteria.list();
			         if(lstTeacherStatusHistoryForJobTemp.size()>0){
			        	 for(Iterator it = lstTeacherStatusHistoryForJobTemp.iterator(); it.hasNext();){
			        		 TeacherStatusHistoryForJob teacherStatusHistoryForJob = new TeacherStatusHistoryForJob();
							 	Object[] row = (Object[]) it.next();    				
							 	
							 	if(row[0]!=null){
									TeacherDetail teacherDetail = new TeacherDetail();
									teacherDetail.setTeacherId(Integer.parseInt(row[0].toString()));
									teacherStatusHistoryForJob.setTeacherDetail(teacherDetail);
								}
								
								if(row[1]!=null){
									JobOrder jobOrder = new JobOrder();
									jobOrder.setJobId(Integer.parseInt(row[1].toString()));
									teacherStatusHistoryForJob.setJobOrder(jobOrder);
								}
								
								if(row[2]!=null){
									SecondaryStatus secondaryStatus = new SecondaryStatus();
									secondaryStatus.setSecondaryStatusId(Integer.parseInt(row[2].toString()));
									teacherStatusHistoryForJob.setSecondaryStatus(secondaryStatus);
								}
								
								if(row[3]!=null){
									StatusMaster statusMaster = new StatusMaster();
									statusMaster.setStatusId(Integer.parseInt(row[3].toString()));
									teacherStatusHistoryForJob.setStatusMaster(statusMaster);
								}
								
								if(row[4]!=null){
									teacherStatusHistoryForJob.setStatus(row[4].toString());
								}
								
								lstTeacherStatusHistoryForJob.add(teacherStatusHistoryForJob);
			        	 }
			         }
	        	}
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			return lstTeacherStatusHistoryForJob;
		}
		@Transactional(readOnly=true)
		public List <TeacherStatusHistoryForJob> findByTeacherAndJob_OP(TeacherDetail teacherDetail,JobOrder jobOrder)
		{
	        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
	        try{

        		Session session 		= 	getSession();
    			 Criteria criteria 		= 	session.createCriteria(getPersistentClass());
    			 
    			 Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
    			 Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
    			 Criterion criterion3 = Restrictions.eq("status","A");
    			 Criterion criterion4 = Restrictions.eq("status","S");
    			 Criterion criterion5 = Restrictions.or(criterion3,criterion4);
    			 Criterion criterionW = Restrictions.eq("status","W");
    			 Criterion criterionORW = Restrictions.or(criterionW, criterion5);
    			 
    			 
		         criteria.add(criterion1);
		         criteria.add(criterion2);
		         criteria.add(criterionORW);
		         criteria.createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN)
		         .setProjection(Projections.projectionList()
						 .add(Projections.property("teacherDetail.teacherId"))
						 .add(Projections.property("jobOrder.jobId"))
						 .add(Projections.property("secondaryStatus.secondaryStatusId"))
						 .add(Projections.property("statusMaster.statusId"))
						 .add(Projections.property("status"))
						 .add( Projections.property("statusmaster.statusShortName"))
						 );
		         
		         
		         List<String []> lstTeacherStatusHistoryForJobTemp=new ArrayList<String []>();
		         lstTeacherStatusHistoryForJobTemp = criteria.list();
		         if(lstTeacherStatusHistoryForJobTemp.size()>0){
		        	 for(Iterator it = lstTeacherStatusHistoryForJobTemp.iterator(); it.hasNext();){
		        		 TeacherStatusHistoryForJob teacherStatusHistoryForJob = new TeacherStatusHistoryForJob();
						 	Object[] row = (Object[]) it.next();    				
						 	
						 	if(row[0]!=null){								
								teacherStatusHistoryForJob.setTeacherDetail(teacherDetail);
							}
							
							if(row[1]!=null){							
								teacherStatusHistoryForJob.setJobOrder(jobOrder);
							}
							
							if(row[2]!=null){
								SecondaryStatus secondaryStatus = new SecondaryStatus();
								secondaryStatus.setSecondaryStatusId(Integer.parseInt(row[2].toString()));
								teacherStatusHistoryForJob.setSecondaryStatus(secondaryStatus);
							}
							
							if(row[3]!=null){
								StatusMaster statusMaster = new StatusMaster();
								statusMaster.setStatusId(Integer.parseInt(row[3].toString()));
								statusMaster.setStatusShortName(row[5].toString());
								teacherStatusHistoryForJob.setStatusMaster(statusMaster);
							}
							
							if(row[4]!=null){
								teacherStatusHistoryForJob.setStatus(row[4].toString());
							}
							
							lstTeacherStatusHistoryForJob.add(teacherStatusHistoryForJob);
		        	 }
		         }
        	
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			return lstTeacherStatusHistoryForJob;
		}
		
		public int getTotalHireInJobOrder(JobOrder jobOrder){
			int totalHire=0;
			try{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(TeacherStatusHistoryForJob.class);
			criteria.createAlias("jobOrder", "jo").createAlias("statusMaster", "sm")
		    .setProjection(Projections.rowCount());
			criteria.add(Restrictions.eq("sm.statusId",6));
			criteria.add(Restrictions.eq("status","A"));
			criteria.add(Restrictions.eq("jo.jobId", jobOrder.getJobId()));
			totalHire=((Long) criteria.uniqueResult()).intValue();
			}catch (Exception e) {
				e.printStackTrace();
			}
			return totalHire;
		}
		
		@Transactional(readOnly=true)
		public List<TeacherStatusHistoryForJob> findByTeacherStatusHistoryActive_Op(TeacherDetail teacherDetail,JobOrder jobOrder)
		{
	        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
	        try{
	        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobOrder);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				 criteria.createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN)
		         .setProjection(Projections.projectionList()
						 .add(Projections.property("teacherDetail.teacherId"))
						 .add(Projections.property("jobOrder.jobId"))
						 .add(Projections.property("secondaryStatus.secondaryStatusId"))
						 .add(Projections.property("statusMaster.statusId"))
						 .add(Projections.property("status"))
						 .add( Projections.property("statusmaster.statusShortName"))
						 );
		         
		         
		         List<String []> lstTeacherStatusHistoryForJobTemp=new ArrayList<String []>();
		         lstTeacherStatusHistoryForJobTemp = criteria.list();
		         if(lstTeacherStatusHistoryForJobTemp.size()>0){
		        	 for(Iterator it = lstTeacherStatusHistoryForJobTemp.iterator(); it.hasNext();){
		        		 TeacherStatusHistoryForJob teacherStatusHistoryForJob = new TeacherStatusHistoryForJob();
						 	Object[] row = (Object[]) it.next();    				
						 	
						 	if(row[0]!=null){								
								teacherStatusHistoryForJob.setTeacherDetail(teacherDetail);
							}
							
							if(row[1]!=null){							
								teacherStatusHistoryForJob.setJobOrder(jobOrder);
							}
							
							if(row[2]!=null){
								SecondaryStatus secondaryStatus = new SecondaryStatus();
								secondaryStatus.setSecondaryStatusId(Integer.parseInt(row[2].toString()));
								teacherStatusHistoryForJob.setSecondaryStatus(secondaryStatus);
							}
							
							if(row[3]!=null){
								StatusMaster statusMaster = new StatusMaster();
								statusMaster.setStatusId(Integer.parseInt(row[3].toString()));
								statusMaster.setStatusShortName(row[5].toString());
								teacherStatusHistoryForJob.setStatusMaster(statusMaster);
							}
							
							if(row[4]!=null){
								teacherStatusHistoryForJob.setStatus(row[4].toString());
							}
							
							lstTeacherStatusHistoryForJob.add(teacherStatusHistoryForJob);
		        	 }
		         }
        	
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			return lstTeacherStatusHistoryForJob;
		}
		
		@Transactional(readOnly=true)
		public TeacherStatusHistoryForJob findByTeacherStatus_op(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
		{
			TeacherStatusHistoryForJob teacherStatusHistoryForJob = new TeacherStatusHistoryForJob();
	        
	        Criteria criteria = session.createCriteria(getPersistentClass());
	        try{
		         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
		         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
		         Criterion criterion4 = Restrictions.eq("status","A");
		         Criterion criterion5 = Restrictions.eq("status","S");
		         Criterion criterion6 = Restrictions.or(criterion4,criterion5);
		         Criterion criterion3 = null;
		         if(statusMaster!=null){
		        	 criterion3 = Restrictions.eq("statusMaster",statusMaster);
		         }else{
		        	 criterion3 = Restrictions.eq("secondaryStatus",secondaryStatus);
		         }
		         criteria.createAlias("statusMaster", "statusmaster",Criteria.LEFT_JOIN)
		         .setProjection(Projections.projectionList()
						 .add(Projections.property("teacherDetail.teacherId"))
						 .add(Projections.property("jobOrder.jobId"))
						 .add(Projections.property("secondaryStatus.secondaryStatusId"))
						 .add(Projections.property("statusMaster.statusId"))
						 .add(Projections.property("status"))
						 .add( Projections.property("statusmaster.statusShortName"))
						 .add(Projections.property("teacherStatusHistoryForJobId"))
						 );
		         criteria.add(criterion1).add(criterion2).add(criterion3).add(criterion6);
		         criteria.setMaxResults(1);
		         List<String []> lstTeacherStatusHistoryForJobTemp=new ArrayList<String []>();
		         lstTeacherStatusHistoryForJobTemp = criteria.list();
		         if(lstTeacherStatusHistoryForJobTemp.size()>0){
		        	 //for(Iterator it = lstTeacherStatusHistoryForJobTemp.iterator(); it.hasNext();){
						 	Object[] row = (Object[]) lstTeacherStatusHistoryForJobTemp.get(0);    				
						 	
						 	if(row[6]!=null){								
								teacherStatusHistoryForJob.setTeacherStatusHistoryForJobId(Long.parseLong(row[6].toString()));
							}
							
						 	
						 	if(row[0]!=null){								
								teacherStatusHistoryForJob.setTeacherDetail(teacherDetail);
							}
							
							if(row[1]!=null){							
								teacherStatusHistoryForJob.setJobOrder(jobOrder);
							}
							
							if(row[2]!=null){								
								teacherStatusHistoryForJob.setSecondaryStatus(secondaryStatus);
							}
							
							if(row[3]!=null){							
								teacherStatusHistoryForJob.setStatusMaster(statusMaster);
							}
							
							if(row[4]!=null){
								teacherStatusHistoryForJob.setStatus(row[4].toString());
							}
		        	 }
		       //  }
		         /*lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterion6);
		         if(lstTeacherStatusHistoryForJob!=null && lstTeacherStatusHistoryForJob.size()>0){
		        	 teacherStatusHistoryForJob=lstTeacherStatusHistoryForJob.get(0);
		         }*/
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			return teacherStatusHistoryForJob;
		}
		@Transactional(readOnly=true)
		public List<TeacherStatusHistoryForJob> findByTeacherStatusListForUndo(TeacherDetail teacherDetail,JobOrder jobOrder,List<StatusMaster> statusMasterList,List<SecondaryStatus> secondaryStatusList)
		{
			System.out.println(":::::::::::findByTeacherStatusListForUndo:::::::::::::");
	        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
	        try{
		         Criterion criterion1 = Restrictions.eq("teacherDetail",teacherDetail);
		         Criterion criterion2 = Restrictions.eq("jobOrder",jobOrder);
		         Criterion criterion4 = Restrictions.not(Restrictions.eq("status","I"));
		         Criterion criterion3 = null;
		         Criterion criterionStatus = Restrictions.in("statusMaster",statusMasterList);
		         Criterion criterionSecStatus = Restrictions.in("secondaryStatus",secondaryStatusList);
		         criterion3 = Restrictions.or(criterionStatus,criterionSecStatus);
		         lstTeacherStatusHistoryForJob = findByCriteria(criterion1,criterion2,criterion3,criterion4);
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
			return lstTeacherStatusHistoryForJob;
		}
		
		@Transactional(readOnly=true)
		public List<TeacherStatusHistoryForJob> getHistoryStatusList_OP(TeacherDetail teacherDetail,StatusMaster statusMaster,SecondaryStatus secondaryStatus)
		{
	        List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
	        try{
	        	Session session = getSession();
	        	Criteria criteria = session.createCriteria(getPersistentClass());
	        	Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion3 = Restrictions.eq("status", "A");
				Criterion criterion4 = Restrictions.eq("status", "S");
				Criterion criterion5 = Restrictions.eq("status", "W");
				Criterion criterion6 = Restrictions.or(criterion3, criterion4);
				Criterion criterion7 = Restrictions.or(criterion6, criterion5);
				Criterion criterion8 = null;
		         if(statusMaster!=null){
		        	 criterion8 = Restrictions.eq("statusMaster",statusMaster);
		         }else{
		        	 criterion8 = Restrictions.eq("secondaryStatus",secondaryStatus);
		         }
		         
		         criteria.add(criterion1);
		         criteria.add(criterion7);
		         criteria.add(criterion8);
		         criteria.setProjection( Projections.projectionList()
 						.add(Projections.property("teacherStatusHistoryForJobId"))
 						.add(Projections.property("status"))
     					);
		         
		         List results = criteria.list();
 				if(results!=null && results.size()>0){
 					for(Iterator it =results.iterator();it.hasNext();){
 						TeacherStatusHistoryForJob obj = new TeacherStatusHistoryForJob();
 						Object[] row = (Object[]) it.next();
 						if(row[0]!=null){
 							obj.setTeacherStatusHistoryForJobId(Long.parseLong(row[0].toString()));
 						}
 						if(row[1]!=null){
 							obj.setStatus(row[1].toString());
 						}
 						lstTeacherStatusHistoryForJob.add(obj);
 					}
 				}
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
	        return lstTeacherStatusHistoryForJob;
		}
		
		//brajesh
		
		@Transactional(readOnly=false)
		 @SuppressWarnings("unchecked")
		 public  List<String[]> getCandidateWithdrawnReason(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,
				 String sortColomnName,int districtID,String job,String WithdrawFromDate,String WithdrawToDate,String districtname) 
		 {
			 PrintOnConsole.getJFTPrint("JFT:101");
		  List<String[]> lst=new ArrayList<String[]>(); 
		  Session session = getSession();
		  String sql = "";
		   Connection connection =null;
		  try {
		      SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		      ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		        connection = connectionProvider.getConnection();

		        if(sortColomnName.equalsIgnoreCase("districtname"))
		        	   sortColomnName = "districtname";
		           else if(sortColomnName.equalsIgnoreCase("jobtitle"))
		   		    sortColomnName = "jobtitle";
		           else if(sortColomnName.equalsIgnoreCase("WithdrawnBy"))
					    sortColomnName = "Withdrawn_By";
		           else if(sortColomnName.equalsIgnoreCase("WithdrawTime"))
					    sortColomnName = "Withdraw_Time";
		           else if(sortColomnName.equalsIgnoreCase("WithdrawnReason"))
					    sortColomnName = "Withdrawn_Reason";
		           else if(sortColomnName.equalsIgnoreCase("NOfWithdraws"))
					    sortColomnName = "N_Of_Withdraws";
		         
		           
				  /* else if(sortColomnName.equalsIgnoreCase("jobtitle"))
				    sortColomnName = "Job_Title";*/
		   
		           String orderby = " order by "+sortColomnName+" "+sortOrderStrVal  ;
		           
		           String jobTle="";
		           String districtidforquery="";
		          
		        String districtnamequery="";
		           if(job.equalsIgnoreCase(""))
		            jobTle="and if('' ='',jobtitle is not null, jobtitle like '%%')";
		           else
		            jobTle="and if('"+job+"' ='',jobtitle is not null, jobtitle like '%"+job+"%')";
		           
		           
		          /* if(districtname.equalsIgnoreCase("0"))
		        	   	districtnamequery="and if(''='',districtname is not null, districtname like '%%')";
			           else*/
		           if(districtID!=0)
		        	   districtidforquery=districtID+"";
			        	   districtnamequery=" and if('"+districtidforquery+"' ='',districtname is not null, districtname like '%"+districtidforquery+"%')";
		            
		            String dateformate="";
		            System.out.println(WithdrawFromDate);
		            System.out.println(WithdrawToDate);
		            WithdrawToDate=new Date().toString();
		            if(!WithdrawFromDate.equalsIgnoreCase("0") /*&& WithdrawToDate.equalsIgnoreCase("0")*/)
		            		dateformate=	"and (withdrawndatetime >='"+WithdrawFromDate+" 00:00:01')";
		            else if(WithdrawFromDate.equalsIgnoreCase("0") /*&& !WithdrawToDate.equalsIgnoreCase("0")*/)
		            	dateformate=	" and withdrawndatetime <='"+WithdrawToDate+" 23:59:59')";
		            else if(!WithdrawFromDate.equalsIgnoreCase("0") /*&& !WithdrawToDate.equalsIgnoreCase("0")*/)
				            	dateformate=" and (withdrawndatetime >='"+WithdrawFromDate+" 00:00:01' and withdrawndatetime <='"+WithdrawToDate+" 23:59:59')";
				            
				    //shriram////////////////////////
		            System.out.println("districtID====="+districtID);
		          
		            sql = "select w.districtname District, jobtitle Job, "+ 
							" withdrawnBy Withdrawn_By, WithdrawTime Withdraw_Time, "+ 
							" withdrawnReason Withdrawn_Reason, count(*) N_Of_Withdraws "+
							" from ( "+ 
							" select dm.districtname, jo.jobtitle, jft.teacherid, jft.districtid, "+ 
							" jft.jobid, sm.status, jft.withdrawndatetime, "+
							" case when ActiontakenByCandidate=1 then 'Candidate' else rm.rolename end `withdrawnBy`, "+
							" tshfj.ActiontakenByCandidate, "+ 
							" case when tshfj.timingforDclwrdw is null then 'Candidate Withdraw' else dst.timing end WithdrawTime, "+ 
							" case when ActiontakenByCandidate=1 then wrm.reason "+
							" else dsr.reason end `withdrawnReason` "+
							" from jobforteacher jft "+
							" left join teacherstatushistoryforjob tshfj on jft.teacherid=tshfj.teacherid "+
							" and jft.status=tshfj.statusid and jft.jobid=tshfj.jobid "+
							" left join statusmaster sm on sm.statusid=jft.status "+
							" left join withdrawnreasonmaster wrm on wrm.withdrawnreasonmasterid=tshfj.withdrawnreasonmasterid "+
							" and wrm.districtid=jft.districtid "+
							" left join districtmaster dm on dm.districtid=jft.districtid "+
							" left join joborder jo on jo.jobid=jft.jobid "+
							" left join usermaster um on um.userid=tshfj.createdBy "+
							" left join rolemaster rm on rm.roleid=um.roleid "+
							" left join districtspecificreason dsr on dsr.reasonid=tshfj.reasonforDclWrdw "+
							" left join districtspecifictiming dst on dst.timingid=tshfj.timingforDclwrdw "+
							" where jft.status=7 and jft.withdrawndatetime "+
							" ) w "+
							" where "+
							//" "+districtnamequery+" "+
							" "+jobTle+" "+
							//" "+dateformate+" "+
							" group by w.districtname, jobid, withdrawnBy, WithdrawTime, withdrawnreason "+
							""+orderby;
		            
		       
		          
		          System.out.println(":::::::::::::::"+sql);
		      
		  PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE); 
		
		 ResultSet rs=ps.executeQuery();

		   if(rs.next()){    
		    do{
		     final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
		     for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
		        {
		      allInfo[i-1]=rs.getString(i);
		        }
		     lst.add(allInfo);
		    }while(rs.next());
		   }
		   else{
		    System.out.println("Record not found.");
		   }
		   
		   return lst;
		  } catch (Exception e) {
		   e.printStackTrace();
		  }finally{
		   if(connection!=null)
		    try {
		     connection.close();
		    } catch (SQLException e) {
		     // TODO Auto-generated catch block
		     e.printStackTrace();
		    }
		  }
		  
		  
		  return null;
		 
		 }
		
		
		
}
