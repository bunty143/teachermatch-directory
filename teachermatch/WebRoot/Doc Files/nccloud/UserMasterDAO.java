package tm.dao.user;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.EmployeeMaster;
import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
/* @Author: Vishwanath Kumar
 * @Discription: UserMaster DAO.
 */
public class UserMasterDAO extends GenericHibernateDAO<UserMaster, Integer>{

	public UserMasterDAO() 
	{
		super(UserMaster.class);
	}
	
	@Autowired
	private RoleMasterDAO roleMasterDAO;
	
	
	@Transactional(readOnly=true)
	public List<UserMaster> getLogin(String emailAddress, String password)
	{
	         List <UserMaster> lstUsers = null;
			 Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
			 Criterion criterion2 = Restrictions.eq("password", password);
			 Criterion criterion3=  Restrictions.and(criterion1, criterion2);
			 lstUsers = findByCriteria(criterion3);  
			 return lstUsers;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> checkUserEmail(String emailAddress)
	{
	         List <UserMaster> lstUsers = null;
			 Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
			 lstUsers = findByCriteria(criterion1);  
			 return lstUsers;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByKeyAndID(String key, Integer id)
	{
		List <UserMaster> lstUserMaster= null;
		try 
		{	         
			 Criterion criterion1 = Restrictions.eq("userId",id);
			 Criterion criterion2 = Restrictions.eq("verificationCode",key );
			 Criterion criterion3=  Restrictions.and(criterion1, criterion2);
			 lstUserMaster = findByCriteria(criterion3);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> findByEmail(String emailAddress)
	{
	         List <UserMaster> lstUserMasters = null;
			 Criterion criterion = Restrictions.eq("emailAddress",emailAddress);
			 lstUserMasters = findByCriteria(criterion);  
			 
			 return lstUserMasters;
	}
	/* @Author: Gagan */
	/*========== Check up Duplicate UserEmail ================*/
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<UserMaster> checkDuplicateUserEmail(Integer userId,String emailAddress) 
	{
		Session session = getSession();
		if(userId!=null)
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.not(Restrictions.in("userId",new Integer[]{userId}))) 
			.add(Restrictions.eq("emailAddress", emailAddress)) 
			.list();
			return result;
		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("emailAddress", emailAddress)) 
			.list();
			return result;
		}
	}
	
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUserBySchool(SchoolMaster schoolMaster)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			lstUserMaster = findByCriteria(Order.asc("firstName"),criterion1,criterion2);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=true)
	public List<UserMaster> getUserBySchoolWithoutAnalist(SchoolMaster schoolMaster)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",new Integer(3));
			RoleMaster rm = new RoleMaster();
			rm.setRoleId(6);
			Criterion criterion4 = Restrictions.ne("roleId",rm);
			lstUserMaster = findByCriteria(Order.asc("firstName"),criterion1,criterion2,criterion3,criterion4);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	/*
	 *  
	 */
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByDistrict(DistrictMaster districtMaster)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",new Integer(2));
			 Criterion criterion3 = Restrictions.eq("status","A");
			 lstUserMaster = findByCriteria(Order.asc("firstName"),criterion1, criterion2,criterion3);
			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByDistrictWithoutAnalist(DistrictMaster districtMaster)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",new Integer(2));
			 Criterion criterion3 = Restrictions.eq("status","A");
			 RoleMaster rm = new RoleMaster();
			 rm.setRoleId(5);
			 Criterion criterion4 = Restrictions.ne("roleId",rm);
			 lstUserMaster = findByCriteria(Order.asc("firstName"),criterion1, criterion2,criterion3,criterion4);
			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=true)
	public List<UserMaster> getUserBySchool(Order order,int startPos,int limit,SchoolMaster schoolMaster)
	{
	
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			lstUserMaster = criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=true)
	public int getUserBySchoolTotal(SchoolMaster schoolMaster)
	{
	
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			lstUserMaster = criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster.size();
	}
	@Transactional(readOnly=true)
	public int getUserByDistrictTotal(DistrictMaster districtMaster)
	{
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
		    Criterion criterion2 = Restrictions.eq("entityType",new Integer(2));
			Criterion criterion3 = Restrictions.eq("status","A");
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			lstUserMaster = criteria.list();
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster.size();
	}
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByDistrict(Order order,int startPos,int limit,DistrictMaster districtMaster)
	{
		List<UserMaster> lstUserMaster = null;
		try{
			Session session = getSession();
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
		    Criterion criterion2 = Restrictions.eq("entityType",new Integer(2));
			Criterion criterion3 = Restrictions.eq("status","A");
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.addOrder(order);
			lstUserMaster = criteria.list();
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster;
	}
	@Transactional(readOnly=true)
	public List<UserMaster> findByUserByRole(Order order,int startPos,int limit,Criterion... criterion)
	{
		List<UserMaster> lstUserMaster = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
	
			Criteria cc = criteria.createCriteria("roleId");			
			if(order!=null && order.toString().contains("roleName")){	
				cc.addOrder(order);
			}
			else if(order != null){
				criteria.addOrder(order);
			}
			lstUserMaster = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUserMaster;
	}
	@Transactional(readOnly=true)
	public int getRowCountUserByRole(Criterion... criterion)
	{
		List<UserMaster> lstUserMaster = null;
		try{
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			for (Criterion c : criterion) {
				criteria.add(c);
			}
		
			Criteria cc = criteria.createCriteria("roleId");			
		   lstUserMaster = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUserMaster.size();
	}
	@Transactional(readOnly=true)
	public List<UserMaster> findByAllUserMaster()
	{
		List<UserMaster> lstUserMaster = null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			lstUserMaster = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public int getUserId(String emailAddress)
	{		
	         List <UserMaster> lstUsers = null;
	         Integer userId=0;
	         try{
				 Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
				 lstUsers = findByCriteria(criterion1);
				 userId=lstUsers.get(0).getUserId();
	         }catch(Exception e){
	        	 
	         }
			 return userId;
	}
	@Transactional(readOnly=true)
	public String getEmailId(String userId)
	{		
         List <UserMaster> lstUsers = null;
         String email="";
         try{
			 Criterion criterion1 = Restrictions.eq("userId",Integer.parseInt(userId));
			 lstUsers = findByCriteria(criterion1);
			 email=lstUsers.get(0).getEmailAddress();
         }catch(Exception e){
         }
		 return email;
	}

	/* ============ Gagan: For getting UserList by UserId array============== */
	@Transactional(readOnly=false)
	public List<UserMaster> getUserlistByUserIdArray(List  lstUserIdArray)
	{
		List<UserMaster> lstUser= new ArrayList<UserMaster>();
		try 
		{
			if(lstUserIdArray.size()>0){
				Criterion criterion1 	= 	Restrictions.in("userId",lstUserIdArray);
				Criterion criterion2 	= 	Restrictions.eq("status","A");
				lstUser =findByCriteria(criterion1,criterion2);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUser;
	}
	

	@Transactional(readOnly=true)
	public List<UserMaster> getUserByOnlySchool(SchoolMaster schoolMaster)
	{
	
		List <UserMaster> lstUserMaster= null;
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",3);
			lstUserMaster = findByCriteria(Order.asc("firstName"),criterion1,criterion2,criterion3);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByDistrictAndSchoolList(DistrictMaster districtMaster,List<SchoolMaster> schoolMasters)	{
	
		List <UserMaster> lstUserMaster= null;
		try 
		{	 
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 
			 Criterion criterion3 = Restrictions.in("schoolId",schoolMasters);
			 Criterion criterion4 = Restrictions.eq("entityType",3);
			 
			 Criterion criterion5 = Restrictions.and(criterion1,criterion2);
			 Criterion criterion6 = Restrictions.and(criterion3,criterion4);
			 
			 Criterion  criterion7=Restrictions.or(criterion5,criterion6);
			 Criterion criterion8 = Restrictions.eq("status","A");
			 
			 lstUserMaster = findByCriteria(Order.asc("firstName"),criterion7,criterion8);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByDistrictListOnlySchool(DistrictMaster districtMaster,SchoolMaster schoolMaster)	{
	
		List <UserMaster> lstUserMaster= null;
		try 
		{	 
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 
			 Criterion criterion3 = Restrictions.eq("schoolId",schoolMaster);
			 Criterion criterion4 = Restrictions.eq("entityType",3);
			 
			 Criterion criterion5 = Restrictions.and(criterion1,criterion2);
			 Criterion criterion6 = Restrictions.and(criterion3,criterion4);
			 
			 Criterion  criterion7=Restrictions.or(criterion5,criterion6);
			 Criterion criterion8 = Restrictions.eq("status","A");
			 
			 lstUserMaster = findByCriteria(Order.asc("firstName"),criterion7,criterion8);
			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	
	@Transactional(readOnly=true)
	public List<UserMaster> getActiveUserByDistrict(DistrictMaster districtMaster)
	{
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			lstUserMaster = findByCriteria(criterion1,criterion2);
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster;
	}
	
	
	@Transactional(readOnly=true)
	public List<UserMaster> getActiveDAUserByDistrict(DistrictMaster districtMaster)
	{
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		
		RoleMaster roleMaster = roleMasterDAO.findById(2, false, false);		
		try{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",2);
			Criterion criterion4 = Restrictions.eq("roleId",roleMaster);
			lstUserMaster = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster;
	}
	
	
	@Transactional(readOnly=true)
	public List<UserMaster> getAllDistrictUsers(DistrictMaster districtMaster)
	{
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try{
			Criterion criterion = Restrictions.eq("districtId",districtMaster);
			lstUserMaster = findByCriteria(criterion);
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getAllSchoolUsersForSchool(SchoolMaster schoolMaster)
	{
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try
		{
			Criterion criterion = Restrictions.eq("schoolId",schoolMaster);
			lstUserMaster = findByCriteria(criterion);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getAllDistrictAndAllSAForSchool(DistrictMaster districtMaster,SchoolMaster schoolMaster)	{
	
		List <UserMaster> lstUserMaster= null;
		try 
		{	 
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 
			 Criterion criterion3 = Restrictions.eq("schoolId",schoolMaster);
			 Criterion criterion4 = Restrictions.eq("entityType",3);
			 
			 Criterion criterion5 = Restrictions.and(criterion1,criterion2);
			 Criterion criterion6 = Restrictions.and(criterion3,criterion4);
			 
			 Criterion  criterion7=Restrictions.or(criterion5,criterion6);
			 
			 
			 lstUserMaster = findByCriteria(criterion7);
			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByEmployeeCode(DistrictMaster districtMaster,SchoolMaster schoolMaster,EmployeeMaster employeeMaster)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("employeeMaster",employeeMaster);
			Criterion criterion4 = Restrictions.eq("districtId",districtMaster);
			if(schoolMaster!=null)
				lstUserMaster = findByCriteria(criterion1,criterion2,criterion3,criterion4);
			else
				lstUserMaster = findByCriteria(criterion2,criterion3,criterion4);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByEmployeeCode(DistrictMaster districtMaster,EmployeeMaster employeeMaster)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("employeeMaster",employeeMaster);
			Criterion criterion4 = Restrictions.eq("districtId",districtMaster);
			
			lstUserMaster = findByCriteria(criterion2,criterion3,criterion4);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getSchoolAdminList(List<SchoolMaster> schoolList)
	{
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		 RoleMaster rm = new RoleMaster();
		 rm.setRoleId(3);
		
		try 
		{	         
			Criterion criterion1 = Restrictions.in("schoolId",schoolList);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",3);
			Criterion criterion4 = Restrictions.eq("roleId",rm);
			lstUserMaster = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getSchoolAdmin(SchoolMaster schoolMaster)
	{
		System.out.println(" ============================getSchoolAdmin================= :: "+schoolMaster.getSchoolId());
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		 RoleMaster rm = new RoleMaster();
		 rm.setRoleId(3);
		
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",3);
			Criterion criterion4 = Restrictions.eq("roleId",rm);
			lstUserMaster = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		if(lstUserMaster!=null && lstUserMaster.size()>0)
			return lstUserMaster;
		else
			return null;
	}
	@Transactional(readOnly=true)
	public List<UserMaster> findByEmailAndDate(String emailAddress)
	{
	         List <UserMaster> lstUserMasters = null;
			 Criterion criterion = Restrictions.eq("emailAddress",emailAddress);
			 lstUserMasters = findByCriteria(Order.desc("createdDateTime"),criterion);  
			 
			 return lstUserMasters;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUserBySchoolUnique(SchoolMaster schoolMaster)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion1 = Restrictions.eq("schoolId",schoolMaster);
			Criterion criterion2 = Restrictions.eq("entityType",3);
			Criterion criterion3 = Restrictions.eq("status","A");
			lstUserMaster = findByCriteria(Order.desc("userId"),criterion1,criterion2,criterion3);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=false)
	public List<UserMaster> findByUserMasters(String ufname,String ulname,String uemail,String uid,DistrictMaster districtMaster,SchoolMaster schoolMaster)
	{
		
		List<UserMaster> lstUserMasters = new ArrayList<UserMaster>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    criteria.setMaxResults(100);
		    if(ufname!=null){
		    	criteria.add(Restrictions.like("firstName", ufname.trim(),MatchMode.ANYWHERE));
		    }
		    if(ulname!=null){
		    	criteria.add(Restrictions.like("lastName", ulname.trim(),MatchMode.ANYWHERE));
		    }
		    if(uemail!=null){
		    	criteria.add(Restrictions.like("emailAddress", uemail.trim(),MatchMode.START));
		    }
		    if(districtMaster!=null){
		    	criteria.add(Restrictions.eq("districtId",districtMaster));
		    }
		    if(schoolMaster!=null){
		    	criteria.add(Restrictions.eq("schoolId",schoolMaster));
		    }
		    try{
		    	if(uid!=null){
		    		criteria.add(Restrictions.eq("userId",Integer.parseInt(uid)));
		    	}
		    }catch(Exception e){
		    	e.printStackTrace();
		    }
			lstUserMasters =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUserMasters;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUserIDSBySchools(List<SchoolMaster> schoolMasters)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion1 = Restrictions.in("schoolId",schoolMasters);
			Criterion criterion2 = Restrictions.eq("entityType",3);
			Criterion criterion3 = Restrictions.eq("status","A");
			lstUserMaster = findByCriteria(criterion1,criterion2,criterion3);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUsersByUserIds(List<Integer> userIds)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion1 = Restrictions.in("userId",userIds);
			Criterion criterion2 = Restrictions.eq("status","A");
			lstUserMaster = findByCriteria(criterion1,criterion2);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=true)
	public List<UserMaster> getUserIDSBySchoolsAndDistrict(List<SchoolMaster> schoolMasters,List userIds)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion1 = Restrictions.in("schoolId",schoolMasters);
			Criterion criterion2 = Restrictions.eq("entityType",3);
			Criterion criterion4 = Restrictions.and(criterion1, criterion2);
			Criterion criterion5 = Restrictions.in("userId",userIds);
			Criterion criterion6 = Restrictions.or(criterion4,criterion5);
			Criterion criterion3 = Restrictions.eq("status","A");
			lstUserMaster = findByCriteria(criterion6,criterion3);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUsersByUserIdLst(List<Integer> userIds)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	         
			Criterion criterion1 = Restrictions.in("userId",userIds);			
			lstUserMaster = findByCriteria(criterion1);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}

	//User By District added by Mukesh 	
	@Transactional(readOnly=true)
	public List<UserMaster> getUsersByDistrict(DistrictMaster districtId,List<Criterion>firstlastEmailCri)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		try 
		{	
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    criteria.add(Restrictions.eq("districtId",districtId));
		   
		    if(firstlastEmailCri!=null && firstlastEmailCri.size()>0){
				for(Criterion cri: firstlastEmailCri)
					criteria.add(cri);
			}
		    lstUserMaster=criteria.list();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}

	@Transactional(readOnly=true)
	public List<UserMaster> checkDAUserEmail(String emailAddress)
	{
	         List <UserMaster> lstUsers = null;
			 Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
			 Criterion criterion2 = Restrictions.eq("entityType",2);
			 lstUsers = findByCriteria(criterion1,criterion2);  
			 return lstUsers;
	}
	
	@Transactional(readOnly=false)
	public void actDeactUsers(List<UserMaster> userMasters,String status) 
	{
		Session session = getSession();
		String sql = "";
		sql = "update usermaster set status = '"+status+"' WHERE  `userId` IN (:userMasters)";
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameterList("userMasters", userMasters );
			query.executeUpdate();
		  } catch (HibernateException e) {
			e.printStackTrace();
		  }
	}
	
	@Transactional(readOnly=true)
	public boolean checkTeacherAlsoSA(EmployeeMaster employeeMaster)
	{
	         List <UserMaster> lstUsers = null;
	         boolean result =false;
	         try {
		         DistrictMaster districtId = new DistrictMaster();
		         RoleMaster roleMaster = new RoleMaster();
		         roleMaster.setRoleId(3);
		         districtId.setDistrictId(4218990);
				 Criterion criterion1 = Restrictions.eq("employeeMaster",employeeMaster);
				 Criterion criterion2 = Restrictions.eq("districtId",districtId);
				 Criterion criterion3 = Restrictions.eq("status","A");
				 Criterion criterion4 = Restrictions.eq("roleId",roleMaster);
				 lstUsers = findByCriteria(criterion1,criterion2,criterion3,criterion4);
				 if(lstUsers.size()>0){
					 result=true;
				 }
	         } catch (Exception e) {
			}
			 return result;
	}
	

	@Transactional(readOnly=true)
	public List<UserMaster> getActiveDAAndSAByDistrict(DistrictMaster districtMaster,List<RoleMaster>  roleMaster)
	{
		List<UserMaster> userMastersList = new ArrayList<UserMaster>();
		try
		{
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.in("roleId",roleMaster);
			userMastersList = findByCriteria(criterion1,criterion2,criterion3);
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return userMastersList;
	}
	
	/**
	 * Added by Amit
	 * @param districtMaster
	 * @param emailAddress
	 * @return userMasterList
	 * @date 11-Mar-15
	 */
	@Transactional(readOnly=true)
	public List<UserMaster> findByByDistrictAndEmail(DistrictMaster districtMaster,String emailAddress)
	{
		List <UserMaster> userMasterList = null;
		Criterion criterion1 = Restrictions.eq("districtId",districtMaster);
		Criterion criterion2 = Restrictions.eq("emailAddress",emailAddress);
		Criterion criterion3 = Restrictions.eq("status","A");
		userMasterList = findByCriteria(criterion1,criterion2,criterion3);  
		return userMasterList;
	}
	
	
	@Transactional(readOnly=true)
	public List<RoleMaster> findByRoleMaterByEmail(String emailAddress)
	{
		List<RoleMaster> rolemaMasters=new ArrayList<RoleMaster>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("emailAddress",emailAddress));
			
			 criteria.setProjection(Projections.groupProperty("roleId"));
			rolemaMasters =  criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		return rolemaMasters;
	}
	



  @Transactional(readOnly=true)
  public List<UserMaster> checkDuplicateEmailAddersAndEntityType3(String emailAddress,String districtId )
  {
	List<UserMaster> districtEmailAdderss1 = null;
	try{
		 Integer districtIdInintger = Integer.parseInt(districtId);
		Criterion criterion1 = Restrictions.eq("districtId.districtId",districtIdInintger);
		Criterion criterion2 = Restrictions.eq("entityType",3);
		Criterion criterion3 = Restrictions.eq("emailAddress", emailAddress);
		 districtEmailAdderss1 = findByCriteria(criterion1,criterion2,criterion3);
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return districtEmailAdderss1;
 }
  
  @Transactional(readOnly=true)
  public List<UserMaster> checkDuplicateEmailAddersAndEntityType2(String emailAddress,String districtId,int entityType )
  {
	List<UserMaster> districtEmailAdderss2=null;
	try{
		 Integer districtIdInintger = Integer.parseInt(districtId);
		Criterion criterion1 = Restrictions.eq("districtId.districtId",districtIdInintger);
		Criterion criterion2 = Restrictions.eq("entityType",entityType);
		Criterion criterion3 = Restrictions.eq("emailAddress", emailAddress);
		 districtEmailAdderss2 = findByCriteria(criterion1,criterion2,criterion3);
		
	}catch (Exception e) {
		e.printStackTrace();
	}
	return districtEmailAdderss2;
 }
  @Transactional(readOnly=true)
  public List<UserMaster> getAllUserByDistrictAndSchool(DistrictMaster districtMaster,SchoolMaster schoolMaster)	{
	
		List <UserMaster> lstUserMaster= null;
		try 
		{	 
			 Criterion criterion1 = Restrictions.eq("districtId",districtMaster);			 
			 Criterion criterion2 = Restrictions.eq("schoolId",schoolMaster);
			 Criterion criterion3 = Restrictions.eq("entityType",3);					 
			 Criterion criterion4 = Restrictions.eq("status","A");			 
			 lstUserMaster = findByCriteria(Order.asc("firstName"),criterion1,criterion2,criterion3,criterion4);			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
  
  	@Transactional(readOnly=true)
	public List<UserMaster> getActiveDAUserByUserIds(List<Integer> usreIdList)
	{
  		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
  		
  		RoleMaster roleMaster = roleMasterDAO.findById(2, false, false);		
		try{
			Criterion criterion1 = Restrictions.in("userId",usreIdList);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",2);
			Criterion criterion4 = Restrictions.eq("roleId",roleMaster);
			lstUserMaster = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster;
	}
  	
  	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<UserMaster> checkDuplicateEmailAddress(String emailAddress,List<UserMaster> userId) 
	{
		Session session = getSession();
		Integer userId1 = null;

		if(userId!=null)
		{	
			UserMaster userOBJ = userId.get(0);
			if(userOBJ!=null){
			 userId1 = userOBJ.getUserId();
			}
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("emailAddress", emailAddress)) 
			.add(Restrictions.not(Restrictions.in("userId",new Integer []{userId1})))
			.list();
			return result;

		}
		else
		{
			List result = session.createCriteria(getPersistentClass())       
			.add(Restrictions.eq("emailAddress", emailAddress)) 
			.list();
			return result;
		}
	}
  	@Transactional(readOnly=false)
	public List<UserMaster> findAllUserMaster(List emails){
		List<UserMaster> userMasterList= new ArrayList<UserMaster>();
		try 
		{
			Criterion criterion = Restrictions.eq("status","A");
			Criterion criterion1 = Restrictions.in("emailAddress",emails);	
			userMasterList =	findByCriteria(criterion,criterion1);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return userMasterList;
	}
  	

	@Transactional(readOnly=true)
	public List<UserMaster> getActiveDAUserByDistrictHBD(JobOrder jobOrder)
	{
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		
		RoleMaster roleMaster = roleMasterDAO.findById(2, false, false);		
		try{
			Criterion criterion3 = Restrictions.eq("entityType",2);
			Criterion criterion4 = Restrictions.eq("roleId",roleMaster);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtId",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtId"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				try{
					if(jobOrder.getDistrictMaster()!=null){
						if(jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2){
							criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getDistrictMaster().getHeadQuarterMaster()));
						}else{
							criteria.add(Restrictions.isNull("headQuarterMaster"));
						}
					}else{
						criteria.add(Restrictions.isNull("headQuarterMaster"));
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			criteria.add(criterion3);
			criteria.add(criterion4);
			criteria.add(Restrictions.eq("status","A"));
			lstUserMaster = criteria.list();
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster;
	}
	
	// findByHQBRAndEmail
	@Transactional(readOnly=true)
	public List<UserMaster> findByHQBRAndEmail(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,String emailAddress,int entityId)
	{
		List <UserMaster> userMasterList = null;
		
		Session session = getSession();
	    Criteria criteria = session.createCriteria(getPersistentClass());
	    if(entityId!=6){
	    if(headQuarterMaster!=null)
	    	criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
	    if(branchMaster!=null)
	    	criteria.add(Restrictions.eq("branchMaster",branchMaster));
	    }
		criteria.add(Restrictions.eq("emailAddress",emailAddress));
		criteria.add(Restrictions.eq("status","A"));
		userMasterList = criteria.list();  
		return userMasterList;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getAllHQBRUsers(HeadQuarterMaster headQuarterMaster , BranchMaster branchMaster)
	{
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try{
			Criterion criterion  = null;
			if(headQuarterMaster!=null){
				criterion	= Restrictions.eq("headQuarterMaster",headQuarterMaster);
			lstUserMaster = findByCriteria(criterion);
			}
			else if(branchMaster!=null){
				criterion	= Restrictions.eq("branchMaster",branchMaster);
				lstUserMaster = findByCriteria(criterion);
			}
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster;
	}
	
	@Transactional(readOnly=true)
	public List<UserMaster> getUserByHqBr(HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster, int entityId)
	{
	
		List <UserMaster> lstUserMaster= new ArrayList<UserMaster>();
		Session session = getSession();
	    Criteria criteria = session.createCriteria(getPersistentClass());
		try 
		{	         
			if(headQuarterMaster!=null)
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			if(branchMaster!=null)
				criteria.add(Restrictions.eq("branchMaster",branchMaster));
			criteria.add(Restrictions.eq("entityType",entityId));
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.asc("firstName"));
			 lstUserMaster = criteria.list();
			 
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	@Transactional(readOnly=false)
	public List<UserMaster> getUserlistAllByUserIdArray(List  lstUserIdArray)
	{
		List<UserMaster> lstUser= new ArrayList<UserMaster>();
		try 
		{
			if(lstUserIdArray.size()>0){
				Criterion criterion1 	= 	Restrictions.in("userId",lstUserIdArray);
				lstUser =findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUser;
	}
	
	public List<UserMaster> getAllBranchAdmin(List<BranchMaster> branchList){
		List<UserMaster> lstUser= new ArrayList<UserMaster>();
		try 
		{
			RoleMaster master=new RoleMaster();
			master.setRoleId(11);
			Criterion criterion1 	= 	Restrictions.in("branchMaster",branchList);
			Criterion criterion2 	= 	Restrictions.eq("entityType",6);
			Criterion criterion3 	= 	Restrictions.eq("roleId",master);
			Criterion criterion4 	= 	Restrictions.eq("status","A");
			lstUser =findByCriteria(criterion1,criterion2,criterion3,criterion4);
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return lstUser;
	}
	
	/**
	 * 
	 * @param headQuarterMaster
	 * @return
	 */
	@Transactional(readOnly=true)
	public List<UserMaster> getActiveDAUserByHeadquarter(HeadQuarterMaster headQuarterMaster)
	{
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		
		RoleMaster roleMaster = roleMasterDAO.findById(10, false, false);		
		try{
			Criterion criterion1 = Restrictions.eq("headQuarterMaster",headQuarterMaster);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",5);
			Criterion criterion4 = Restrictions.eq("roleId",roleMaster);
			lstUserMaster = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		}catch (Exception e) 
		{
			e.printStackTrace();
		} 
		return lstUserMaster;
	}
	
	/**
	 * Get All active headquarter admins
	 * @return
	 */
	@Transactional(readOnly=true)
	public List<UserMaster> getAllHeadquarterAdmins(){
		
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try {
			RoleMaster roleMaster = roleMasterDAO.findById(10, false, false);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",5);
			Criterion criterion4 = Restrictions.eq("roleId",roleMaster);
			lstUserMaster = findByCriteria(criterion2,criterion3,criterion4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
	
	/**
	 * Get All active branch admins
	 * @return
	 */
	@Transactional(readOnly=true)
	public List<UserMaster> getAllBranchAdmins(){
		
		List<UserMaster> lstUserMaster = new ArrayList<UserMaster>();
		try {
			RoleMaster roleMaster = roleMasterDAO.findById(11, false, false);
			Criterion criterion2 = Restrictions.eq("status","A");
			Criterion criterion3 = Restrictions.eq("entityType",6);
			Criterion criterion4 = Restrictions.eq("roleId",roleMaster);
			lstUserMaster = findByCriteria(criterion2,criterion3,criterion4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lstUserMaster;
	}
}
