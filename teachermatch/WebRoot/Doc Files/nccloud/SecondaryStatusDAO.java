package tm.dao.master;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.JobOrder;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusNodeMaster;
import tm.bean.user.UserMaster;
import tm.dao.generic.GenericHibernateDAO;
import tm.utility.Utility;

public class SecondaryStatusDAO extends GenericHibernateDAO<SecondaryStatus, Integer>
{
	public SecondaryStatusDAO()
	{
		super(SecondaryStatus.class);
	}
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Transactional(readOnly=false)
	public SecondaryStatus findSecondaryStatusObjByJobCategory(JobOrder jobOrder)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		SecondaryStatus secondaryStatus=null;
		
		try 
		{
			Criterion criterion2				=	Restrictions.eq("status","A");
			Criterion criterion1 =	Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			Criterion criterion3 =	Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
			Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
			if(jobOrder.getDistrictMaster().getSecondaryStatus()!=null){
				Criterion criterion6 =	Restrictions.eq("secondaryStatus_copy",jobOrder.getDistrictMaster().getSecondaryStatus());
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion6);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus.size()>0){
			secondaryStatus=lstStatus.get(0);
		}
		return secondaryStatus;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int checkSecondaryStatus(DistrictMaster districtMaster)
	{
		int iReturnValue=0;
		Session session = getSession();
		String sql =" SELECT COUNT(*) from secondarystatus  where districtId = "+districtMaster.getDistrictId()+" and status='A'" ;
		Query query = session.createSQLQuery(sql);
		List<BigInteger> rowCount = query.list();
		if(rowCount!=null && rowCount.size()==1)
			iReturnValue=rowCount.get(0).intValue();
		return iReturnValue;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatus(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= null;
		try 
		{
			Criterion criterion2				=	Restrictions.eq("status","A");
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecStatusByStatusIdList(List secSstatusIdList)
	{
		List<SecondaryStatus> lstStatus= null;
		try 
		{
			Criterion criterion2				=	Restrictions.eq("status","A");
			Criterion criterion1 = Restrictions.in("secondaryStatusId",secSstatusIdList);
			lstStatus = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<Integer> getDistrictId()
	{
		List<Integer> districtMasters = new ArrayList<Integer>();
		DistrictMaster districtMaster = null;
		try{
			Session session = getSession();
			List result = session.createCriteria(getPersistentClass()) 
			.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("districtMaster"))
					.add(Projections.property("districtMaster"))  
			).list();

			Iterator itr = result.iterator();
			while(itr.hasNext())
			{
				Object[] obj = (Object[]) itr.next();
				districtMaster=(DistrictMaster)obj[0];
				districtMasters.add(districtMaster.getDistrictId());
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		return districtMasters;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusWithDistrictList(List<DistrictMaster> districtMasters)
	{
		List<SecondaryStatus> lstStatus= null;
		try 
		{
			if(districtMasters!=null && districtMasters.size() >0)
			{
				Criterion criterion1 =	Restrictions.in("districtMaster", districtMasters);
				Criterion criterion2				=	Restrictions.eq("status","A");
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2);
			}
					
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusOnlyStatus(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= null;
		try 
		{
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 =	Restrictions.isNotNull("statusMaster");
			Criterion criterion3				=	Restrictions.eq("status","A");
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusOnly(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= null;
		try 
		{
			Criterion criterion1				=	Restrictions.isNull("statusMaster");
			Criterion criterion2				=	Restrictions.isNull("statusNodeMaster");
			Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
			Criterion criterion3				=	Restrictions.eq("status","A");
			Criterion criterion4 =	Restrictions.eq("districtMaster", districtMaster);
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion5);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusOnlyWithSecStatusIds(DistrictMaster districtMaster,List secStatusIds)
	{
		List<SecondaryStatus> lstStatus= null;
		try 
		{
			Criterion criterion1				=	Restrictions.isNull("statusMaster");
			Criterion criterion2				=	Restrictions.isNull("statusNodeMaster");
			Criterion criterion6= Restrictions.isNull("jobCategoryMaster");
			Criterion criterion3				=	Restrictions.eq("status","A");
			Criterion criterion4 =	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion5 =	Restrictions.not(Restrictions.in("secondaryStatusId",secStatusIds));
			if(secStatusIds.size()>0){
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion5,criterion6);
			}else{
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion6);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusForInactive(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= null;
		try 
		{
			Criterion criterion1				=	Restrictions.isNull("statusMaster");
			Criterion criterion2				=	Restrictions.isNull("statusNodeMaster");
			Criterion criterion3				=	Restrictions.eq("status","I");
			Criterion criterion4 =	Restrictions.eq("districtMaster", districtMaster);
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategory(JobOrder jobOrder)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		
		boolean jobSubCateFlag = false;
		boolean jobSubCateSLCFlag = false;
		JobCategoryMaster parentJobCategory = null;
		
		try 
		{
			Criterion criterion2				=	Restrictions.eq("status","A");
			Criterion criterion1 =	Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
			Criterion criterion3 =	null;//Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
			Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
			Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
			
			
			
			try{
				if(jobOrder!=null){
				 	
					if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()>0){
						System.out.println("01");
						jobSubCateFlag = true;
						if(jobOrder.getJobCategoryMaster().getAttachSLCFromJC()!=null && jobOrder.getJobCategoryMaster().getAttachSLCFromJC()){
							System.out.println("02");
							parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
							jobSubCateSLCFlag = true; //DSPQ display By Parent Job category
						}
						else if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
							System.out.println("04");
							parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
							jobSubCateSLCFlag = true;
						}
						else{
							System.out.println("03");
							jobSubCateSLCFlag = false; //DSPQ display By Sub Job category
						}
					}
					
					 if(jobSubCateFlag && jobSubCateSLCFlag){
						 System.out.println(" parentJobCategory "+parentJobCategory.getJobCategoryId());
						 criterion3 = Restrictions.eq("jobCategoryMaster",parentJobCategory);
					 }else{
						 criterion3 = Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
					 }
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4);
			if(lstStatus.size()==0){
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion5);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByDistrictAndJobCategory(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion_status=Restrictions.eq("status","A");
			Criterion criterion_district=Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion_jobCategory=null;//Restrictions.eq("jobCategoryMaster", jobCategoryMaster);
			Criterion criterion_jobCategory_IsNull=Restrictions.isNull("jobCategoryMaster");
			if(districtMaster!=null && jobCategoryMaster!=null)
			{
				boolean jobSubCateFlag = false;
				boolean jobSubCateSLCFlag = false;
				JobCategoryMaster parentJobCategory = null;
				
				try{
					 if(jobCategoryMaster!=null){
						 	if(jobCategoryMaster.getParentJobCategoryId()!=null){
								jobSubCateFlag = true;
								if(jobCategoryMaster.getAttachSLCFromJC()!=null && jobCategoryMaster.getAttachSLCFromJC()){	
									parentJobCategory = jobCategoryMaster.getParentJobCategoryId();
									jobSubCateSLCFlag = true; //DSPQ display By Parent Job category
								}else{
									jobSubCateSLCFlag = false; //DSPQ display By Sub Job category
								}
							}
						}
					 
					 if(jobSubCateFlag && jobSubCateSLCFlag){
						 System.out.println(" 06::::::::");
						 criterion_jobCategory = Restrictions.eq("jobCategoryMaster",parentJobCategory);//criteria.add(Restrictions.eq("jobCategoryMaster",parentJobCategory));
					 }else{
						 System.out.println(" 07::::::::");
						 criterion_jobCategory = Restrictions.eq("jobCategoryMaster",jobCategoryMaster);//criteria.add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster));
					 }
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
				lstSecondaryStatus = findByCriteria(Order.asc("orderNumber"),criterion_district,criterion_jobCategory,criterion_status);
				//System.out.println("DAO Dist and Job Cate");
				//System.out.println("lstSecondaryStatus Size "+lstSecondaryStatus.size());
			}
			else
			{
				
				lstSecondaryStatus = findByCriteria(Order.asc("orderNumber"),criterion_district,criterion_jobCategory_IsNull,criterion_status);
				//System.out.println("DAO Dist");
				//System.out.println("lstSecondaryStatus Size "+lstSecondaryStatus.size());
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstSecondaryStatus;
		}
		
	}
	@Transactional(readOnly=false)
	public boolean copyDataFromDistrictToJobCategory(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,UserMaster userMaster)
	{
		
		System.out.println(":::::::::::: DAO copyDataFromDistrictToJobCategory::::::::::::::::::::::::");
		boolean isReturn=true;
		List<SecondaryStatus> lstSecondaryStatusSource= new ArrayList<SecondaryStatus>();
		List<SecondaryStatus> lstSecondaryStatusIsNull= new ArrayList<SecondaryStatus>();
		
		Map<Integer , SecondaryStatus> map=new HashMap<Integer, SecondaryStatus>();
		
		try 
		{
			Criterion criterion_status=Restrictions.eq("status","A");
			Criterion criterion_district=Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion_jobCategory_IsNull=Restrictions.isNull("jobCategoryMaster");
			
			Criterion criterion_secondaryStatus_IsNull=Restrictions.isNull("secondaryStatus");
			
			Criterion criterion_secondaryStatus_IsNotNull=Restrictions.isNotNull("secondaryStatus");
			
			lstSecondaryStatusIsNull= findByCriteria(Order.asc("secondaryStatusId"),criterion_district,criterion_jobCategory_IsNull,criterion_secondaryStatus_IsNull,criterion_status);
			 SessionFactory sessionFactory=getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
		
			
			if(lstSecondaryStatusIsNull.size()>0)
			{	
				for(SecondaryStatus secondaryStatus:lstSecondaryStatusIsNull)
				{
					SecondaryStatus status=new SecondaryStatus();
					status.setDistrictMaster(districtMaster);
					status.setSchoolMaster(secondaryStatus.getSchoolMaster());
					status.setJobCategoryMaster(jobCategoryMaster);
					status.setSecondaryStatusName(secondaryStatus.getSecondaryStatusName());
					status.setStatusMaster(secondaryStatus.getStatusMaster());
					status.setSecondaryStatus(secondaryStatus.getSecondaryStatus());
					status.setStatusNodeMaster(secondaryStatus.getStatusNodeMaster());
					status.setOrderNumber(secondaryStatus.getOrderNumber());
					status.setStatus(secondaryStatus.getStatus());
					status.setUsermaster(userMaster);
					status.setSecondaryStatus_copy(secondaryStatus);
					status.setCreatedDateTime(secondaryStatus.getCreatedDateTime());
					statelesSsession.insert(status);
					map.put(secondaryStatus.getSecondaryStatusId(),status);
				}
			}
			txOpen.commit();
	       	statelesSsession.close();
	       	
	       	sessionFactory=getSessionFactory();
			statelesSsession = sessionFactory.openStatelessSession();
			txOpen =statelesSsession.beginTransaction();
			
			lstSecondaryStatusSource = findByCriteria(Order.asc("secondaryStatusId"),criterion_district,criterion_jobCategory_IsNull,criterion_secondaryStatus_IsNotNull,criterion_status);
			if(lstSecondaryStatusSource.size()>0)
			{
				
				int i=0;
				for(SecondaryStatus secondaryStatus:lstSecondaryStatusSource)
				{
					SecondaryStatus status=new SecondaryStatus();
					++i;
					
					status.setDistrictMaster(districtMaster);
					status.setSchoolMaster(secondaryStatus.getSchoolMaster());
					status.setJobCategoryMaster(jobCategoryMaster);
					status.setSecondaryStatusName(secondaryStatus.getSecondaryStatusName());
					status.setStatusMaster(secondaryStatus.getStatusMaster());
					status.setSecondaryStatus(map.get(secondaryStatus.getSecondaryStatus().getSecondaryStatusId()));
					status.setStatusNodeMaster(secondaryStatus.getStatusNodeMaster());
					status.setOrderNumber(secondaryStatus.getOrderNumber());
					status.setStatus(secondaryStatus.getStatus());
					status.setUsermaster(userMaster);
					status.setSecondaryStatus_copy(secondaryStatus);
					status.setCreatedDateTime(secondaryStatus.getCreatedDateTime());
					statelesSsession.insert(status);
				}
			}
			txOpen.commit();
	       	statelesSsession.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
			isReturn=false;
		}
		finally
		{
			return isReturn;
		}
	}
	
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSSByDistrictPlus(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion_district=Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion_jcNotNull=Restrictions.isNotNull("jobCategoryMaster");
			if(districtMaster!=null)
				lstSecondaryStatus = findByCriteria(Order.asc("orderNumber"),criterion_district,criterion_jcNotNull);
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstSecondaryStatus;
		}
		
	}

	@Transactional(readOnly=false)
	public Map<SecondaryStatus,JobCategoryMaster> findSSByDistrictPlusByMap(DistrictMaster districtMaster,SecondaryStatus secondaryStatusObj)
	{
		Map<SecondaryStatus, JobCategoryMaster> map=new HashMap<SecondaryStatus, JobCategoryMaster>();
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion_district=Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion_jcNotNull=Restrictions.isNotNull("jobCategoryMaster");
			Criterion criterion_ssIsNull=Restrictions.isNull("secondaryStatus");
			Criterion criterion_ssName=Restrictions.eq("secondaryStatusName",secondaryStatusObj.getSecondaryStatusName());
			
			if(districtMaster!=null)
				lstSecondaryStatus = findByCriteria(Order.asc("orderNumber"),criterion_district,criterion_jcNotNull,criterion_ssIsNull,criterion_ssName);
			
			if(lstSecondaryStatus.size()>0)
			{
				for(SecondaryStatus pojo:lstSecondaryStatus)
				{
					if(pojo!=null && pojo.getJobCategoryMaster()!=null)
					{
						if(map.get(pojo)==null)
						{
							map.put(pojo, pojo.getJobCategoryMaster());
						}
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return map;
		}
		
	}
	
	@Transactional(readOnly=false)
	public boolean isFoundChildNodes(SecondaryStatus secondaryStatus)
	{
		boolean bReturnValue=false;
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion_SSID=Restrictions.eq("secondaryStatus_copy", secondaryStatus);
			Criterion criterion_status=Restrictions.eq("status", "A");
			
			lstSecondaryStatus=findByCriteria(criterion_SSID,criterion_status);
			if(lstSecondaryStatus.size()>0)
				bReturnValue=true;
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return bReturnValue;
		}
		
	}
	
	
	@Transactional(readOnly=false)
	public int deleteChildNodes(SecondaryStatus secondaryStatus)
	{
		int iCount=0;
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion_SSID=Restrictions.eq("secondaryStatus_copy", secondaryStatus);
			Criterion criterion_status=Restrictions.eq("status", "A");
			
			lstSecondaryStatus=findByCriteria(criterion_SSID,criterion_status);
			if(lstSecondaryStatus.size()>0)
			{
				for(SecondaryStatus pojo:lstSecondaryStatus)
				{
					if(pojo!=null)
					{
						pojo.setStatus("I");
						makePersistent(pojo);
						flush();
						clear();
						iCount++;
					}
				}
			}
			
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return iCount;
		}
		
	}
	
	
	@Transactional(readOnly=false)
	public int updateChildNodes(Map<Integer,JobCategoryMaster> jCM,SecondaryStatus secondaryStatus,String strNewFolderName,String strOldStatusName)
	{
		int iCount=0;
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try{
			Criterion criterion_SSID=Restrictions.eq("secondaryStatus_copy", secondaryStatus);
			Criterion criterion_status=Restrictions.eq("status", "A");
			Criterion criterion_secondaryStatusName=Restrictions.eq("secondaryStatusName", strOldStatusName);
			
			lstSecondaryStatus=findByCriteria(criterion_SSID,criterion_status,criterion_secondaryStatusName);
			 SessionFactory sessionFactory=getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
		
			if(lstSecondaryStatus.size()>0){
				for(SecondaryStatus pojo:lstSecondaryStatus){
					if(pojo!=null){
						JobCategoryMaster jc=null;
						if(strNewFolderName.trim().equalsIgnoreCase("jsi")){
							if(pojo.getJobCategoryMaster()!=null){
								
								jc=jCM.get(pojo.getJobCategoryMaster().getJobCategoryId());
								
								if(jc!=null){
									System.out.println(jc.getJobCategoryName()+"::::<><>||<><>::::"+pojo.getJobCategoryMaster().getJobCategoryId());
									pojo.setSecondaryStatusName(strNewFolderName.trim());
									statelesSsession.update(pojo);
									iCount++;
								}
							}
						}else{
							pojo.setSecondaryStatusName(strNewFolderName.trim());
							statelesSsession.update(pojo);
							iCount++;
						}
					}
				}
			}
			txOpen.commit();
	       	statelesSsession.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return iCount;
		}
		
	}
	@Transactional(readOnly=false)
	public String  findSecondaryStatusExist(DistrictMaster districtMaster,String secStatus)
	{
		List<SecondaryStatus> secondaryStatusList= new ArrayList<SecondaryStatus>();
		String secondaryStatusName="";
		try 
		{
			Criterion criterion=Restrictions.ilike("secondaryStatusName",secStatus+"%");
			Criterion criterion1=Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2=Restrictions.eq("status","A");
			secondaryStatusList=findByCriteria(Order.desc("secondaryStatusId"),criterion,criterion1,criterion2);
			if(secondaryStatusList.size()>0){
				String secStatusNamePre=secondaryStatusList.get(0).getSecondaryStatusName().replace(secStatus+"1.","");
				int secStatusName=1;
				if(secStatusNamePre.length()>0){
					try{
						secStatusName+=Integer.parseInt(secStatusNamePre);
					}catch(Exception e){ //e.printStackTrace();
					}
					secondaryStatusName=secStatus+"1."+secStatusName;	
				}else{
					secondaryStatusName=secStatus+"1.1";
				}
			}else{
				secondaryStatusName=secStatus;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return secondaryStatusName;
	}

	
	@Transactional(readOnly=false)
	public Map<JobCategoryMaster,SecondaryStatus> findParentIdByMap(DistrictMaster districtMaster,SecondaryStatus secondaryStatusObj)
	{
		Map<JobCategoryMaster,SecondaryStatus> map=new HashMap<JobCategoryMaster,SecondaryStatus>();
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion_district=Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion_jcNotNull=Restrictions.isNotNull("jobCategoryMaster");
			Criterion criterion_ssIsNull=Restrictions.isNull("secondaryStatus");
			Criterion criterion_ssName=Restrictions.eq("secondaryStatusName",secondaryStatusObj.getSecondaryStatusName());
			
			if(districtMaster!=null)
				lstSecondaryStatus = findByCriteria(Order.asc("orderNumber"),criterion_district,criterion_jcNotNull,criterion_ssIsNull,criterion_ssName);
			
			if(lstSecondaryStatus.size()>0)
				for(SecondaryStatus pojo:lstSecondaryStatus)
					if(pojo!=null && pojo.getJobCategoryMaster()!=null)
						if(map.get(pojo.getJobCategoryMaster())==null)
							map.put(pojo.getJobCategoryMaster(),pojo);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return map;
		}
		
	}
	
	
	@Transactional(readOnly=false)
	public int cutAndPasteForJobCategory(SecondaryStatus statusParent,SecondaryStatus status_Child, Integer jobCategory,DistrictMaster districtMaster,String sourceParetnName)
	{
		int iCount=0;
		try 
		{
			List<SecondaryStatus> lstSecondaryStatusChild= new ArrayList<SecondaryStatus>();
			Criterion criterion_SSID=Restrictions.eq("secondaryStatus_copy", status_Child);
			Criterion criterion_status=Restrictions.eq("status", "A");
			lstSecondaryStatusChild=findByCriteria(criterion_SSID,criterion_status);
			
			Map<JobCategoryMaster,SecondaryStatus> mapParetntId=new HashMap<JobCategoryMaster,SecondaryStatus>();
			if(jobCategory==null || jobCategory==0)
				mapParetntId=findParentIdByMap(districtMaster, statusParent);
			
			 SessionFactory sessionFactory=getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();

			if(lstSecondaryStatusChild.size()>0)
			{
				for(SecondaryStatus pojo :lstSecondaryStatusChild)
				{
					if(pojo.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase(sourceParetnName))
					{
						SecondaryStatus secondaryStatus_parent=mapParetntId.get(pojo.getJobCategoryMaster());
						if(secondaryStatus_parent!=null)
							pojo.setSecondaryStatus(secondaryStatus_parent);
						pojo.setSecondaryStatus_copy(status_Child);
						statelesSsession.update(pojo);
						iCount++;
					}
				}
			}
			txOpen.commit();
	       	statelesSsession.close();
			
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		finally
		{
		  return iCount;	
		}
	}
	@Transactional(readOnly=false)
	public int findSecondaryStatusOrderNumber(DistrictMaster districtMaster,SecondaryStatus secondaryStatus)
	{
		int orderNumber=0;
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion2				=	Restrictions.eq("status","A");
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion3 =	Restrictions.eq("secondaryStatus", secondaryStatus);
			lstStatus = findByCriteria(Order.desc("orderNumber"),criterion1,criterion2,criterion3);		
			if(lstStatus.size()>0){
				orderNumber=lstStatus.get(0).getOrderNumber();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return ++orderNumber;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryWithStatus(JobOrder jobOrder,StatusMaster statusMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(statusMaster!=null){
				Criterion criterion2				=	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
				Criterion criterion3 =	Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
				
				Criterion criterion6				=	Restrictions.eq("statusNodeMaster",statusMaster.getStatusNodeMaster());
				
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion6);
				if(lstStatus.size()==0){
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion5,criterion6);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryWithOutStatus(JobOrder jobOrder)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
				Criterion criterion2				=	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
				Criterion criterion3 =	Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
				Criterion criterion7				=	Restrictions.isNull("statusNodeMaster");
				
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion7);
				if(lstStatus.size()==0){
						lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion5,criterion7);
				}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusOnlyStatusForDPoints(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 =	Restrictions.isNotNull("statusMaster");
			Criterion criterion3				=	Restrictions.eq("status","A");
			Criterion criterion4=	Restrictions.isNull("jobCategoryMaster");
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}

	@Transactional(readOnly=false)
	public SecondaryStatus findSecondaryStatusByDistrict(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 =	Restrictions.isNotNull("statusMaster");
			Criterion criterion3				=	Restrictions.eq("status","A");
			Criterion criterion4=	Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion5=	Restrictions.eq("secondaryStatusName","JSI");
			lstStatus = findByCriteria(criterion1,criterion3,criterion4,criterion5);	
			if(lstStatus.size()>0)
				return lstStatus.get(0);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategory(DistrictMaster districtMaster, JobCategoryMaster jobCategoryMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
			Criterion criterion1 =	Restrictions.eq("status","A");
			Criterion criterion2 =	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion3 =	Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion4 =	Restrictions.isNull("statusNodeMaster");
			lstStatus = findByCriteria(criterion1,criterion2,criterion3,criterion4);	
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryWithStatus_msu(JobCategoryMaster jobCategoryMaster,DistrictMaster districtMaster,StatusMaster statusMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(statusMaster!=null){
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 =	Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion5 =	Restrictions.isNull("jobCategoryMaster");
				Criterion criterion6 =	Restrictions.eq("statusNodeMaster",statusMaster.getStatusNodeMaster());
				
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion6);
				if(lstStatus.size()==0){
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion5,criterion6);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> getSecondaryStatusForPanel(DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,StatusMaster statusMaster)
	{
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1 =	Restrictions.eq("statusMaster",statusMaster);
			Criterion criterion2 =	Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
			Criterion criterion3 =	Restrictions.eq("status","A");
			Criterion criterion4 =	Restrictions.eq("districtMaster", districtMaster);
			lstSecondaryStatus = findByCriteria(criterion1,criterion2,criterion3,criterion4);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return lstSecondaryStatus;
	}
	
	
	@Transactional(readOnly=false)
	public SecondaryStatus findSecondaryStatusByJobOrder(JobOrder jobOrder,String secondaryStatusName)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		SecondaryStatus secondaryStatus=null;
		try 
		{
			if(secondaryStatusName!=null){
				Criterion criterion2				=	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
				Criterion criterion3 =	Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion6=Restrictions.ilike("secondaryStatusName",secondaryStatusName+"%");
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion6);
				if(lstStatus.size()>0){
					secondaryStatus=lstStatus.get(0);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return secondaryStatus;
	}
	
	
	@Transactional(readOnly=false)
	public SecondaryStatus findSSByDID(DistrictMaster districtMaster,String secondaryStatusName)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		SecondaryStatus secondaryStatus=null;
		try 
		{
			if(secondaryStatusName!=null){
				Criterion criterion2				=	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
				Criterion criterion6=Restrictions.ilike("secondaryStatusName",secondaryStatusName+"%");
				
				lstStatus = findByCriteria(criterion1,criterion2,criterion4,criterion6);
				if(lstStatus.size()==0){
					lstStatus = findByCriteria(criterion1,criterion2,criterion5,criterion6);
				}
				if(lstStatus.size()>0){
					secondaryStatus=lstStatus.get(0);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return secondaryStatus;
	}
	
	
	@Transactional(readOnly=false)
	public List<Integer> findBySecondryStatusName(DistrictMaster districtMaster, Integer ssId)
	{
		List<Integer> lstsStatusIDs= new ArrayList<Integer>();
		String ssName =null;
		try 
		{ 
			    Criterion criterion1    =	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion2	=	Restrictions.eq("status","A");
			    Criterion criterion4    =	Restrictions.eq("secondaryStatusId",ssId);
				
				List<SecondaryStatus> listsSecondaryStatus = findByCriteria(criterion1,criterion2,criterion4);
				if(listsSecondaryStatus.size()>0)
					ssName	= listsSecondaryStatus.get(0).getSecondaryStatusName();
				if(ssName !=null){
					Criterion criterion5    =  Restrictions.ilike("secondaryStatusName",ssName+"%");
					List<SecondaryStatus> listsNameofsecoundryStatus = findByCriteria(criterion1, criterion2 ,criterion5);
					for(SecondaryStatus ss :listsNameofsecoundryStatus ){
						lstsStatusIDs.add(ss.getSecondaryStatusId());
					}
				}	
			
	  }
		 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstsStatusIDs;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryAndDistrict(List<JobCategoryMaster> jobCategoryMasterList,DistrictMaster districtMaster,SecondaryStatus secondaryStatus)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
			if(jobCategoryMasterList.size()>0){
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 =	Restrictions.in("jobCategoryMaster",jobCategoryMasterList);
				Criterion criterion6 =	Restrictions.eq("secondaryStatusName",secondaryStatus.getSecondaryStatusName());
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion6);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryList(DistrictMaster districtMaster,List<JobCategoryMaster> jobCategoryMasterList)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(jobCategoryMasterList.size()>0)
			{
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 =	Restrictions.in("jobCategoryMaster",jobCategoryMasterList);
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4);
				if(lstStatus.size()==0){
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion5);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		System.out.println("lstStatus :: "+lstStatus.size());
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondryStatusByDistrict(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstSecondaryStatus=new ArrayList<SecondaryStatus>();
		
		try 
		{
			Criterion criterion1 =	Restrictions.eq("status","A");
			Criterion criterion2=Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion3=Restrictions.isNull("statusNodeMaster");
			
			
			if(districtMaster!=null)
				lstSecondaryStatus = findByCriteria(Order.asc("secondaryStatusName"),criterion1,criterion2,criterion3);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	return lstSecondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findByStatusName(DistrictMaster districtMaster, String ssId)
	{
		List<SecondaryStatus> listsSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{ 
			    Criterion criterion1    =	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion2	=	Restrictions.eq("status","A");
			    Criterion criterion4    =	Restrictions.ilike("secondaryStatusName",ssId+"%");
				listsSecondaryStatus = findByCriteria(criterion1,criterion2,criterion4);
				
	  }
		 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listsSecondaryStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryLists(List<DistrictMaster> districtMasters,List<JobCategoryMaster> jobCategoryMasterList)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(jobCategoryMasterList.size()>0)
			{
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.in("districtMaster",districtMasters);
				Criterion criterion3 =	Restrictions.in("jobCategoryMaster",jobCategoryMasterList);
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4);
				if(lstStatus.size()==0){
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion5);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		System.out.println("lstStatusdddddd :: "+lstStatus.size());
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findByMultipleStatusName(DistrictMaster districtMaster, String ssId[])
	{
		List<SecondaryStatus> listsSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
		    Criterion criterion1    =	Restrictions.eq("districtMaster",districtMaster);
			Criterion criterion2	=	Restrictions.eq("status","A");

			Criterion groupCriterion=null;
			groupCriterion    =	Restrictions.eq("secondaryStatusName",ssId[0]);
				for (int i = 1; i < ssId.length; i++)
				{
					Criterion nextcriteria    =	Restrictions.eq("secondaryStatusName",ssId[i]);
					groupCriterion= Restrictions.or(groupCriterion, nextcriteria);
				}
			listsSecondaryStatus = findByCriteria(criterion1,criterion2,groupCriterion);
				
	  }
		 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return listsSecondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByName(DistrictMaster districtMaster,String statusName)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2				=	Restrictions.eq("status","A");
			Criterion criterion3=	Restrictions.eq("secondaryStatusName",statusName);
			lstStatus = findByCriteria(criterion1,criterion2,criterion3);	
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusforSpecificStatusList(DistrictMaster districtMaster,List<StatusMaster> lstStatusMasters)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1=	Restrictions.in("statusMaster",lstStatusMasters);
			Criterion criterion2=	Restrictions.isNull("jobCategoryMaster");
			Criterion criterion3=	Restrictions.eq("status","A");
			Criterion criterion4 =	Restrictions.eq("districtMaster", districtMaster);
			
			lstStatus = findByCriteria(criterion1,criterion2,criterion3,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusforSpecificStatusListBYDistricts(List<DistrictMaster> districtMasterList,List<StatusMaster> lstStatusMasters)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1=	Restrictions.in("statusMaster",lstStatusMasters);
			Criterion criterion2=	Restrictions.isNull("jobCategoryMaster");
			Criterion criterion3=	Restrictions.eq("status","A");
			Criterion criterion4 =	Restrictions.in("districtMaster", districtMasterList);
			
			lstStatus = findByCriteria(criterion1,criterion2,criterion3,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
	
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryWithStatuss(JobOrder jobOrder,Map<String, StatusMaster> mapStatus)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			List<StatusNodeMaster> statusNodeMasters=new ArrayList<StatusNodeMaster>();
			
			statusNodeMasters.add(mapStatus.get("scomp").getStatusNodeMaster());
			statusNodeMasters.add(mapStatus.get("ecomp").getStatusNodeMaster());
			statusNodeMasters.add(mapStatus.get("vcomp").getStatusNodeMaster());
			
			if(mapStatus!=null && mapStatus.size()>0){
				Criterion criterion2				=	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",jobOrder.getDistrictMaster());
				Criterion criterion3 =	Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster());
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
				Criterion criterion6				=	Restrictions.in("statusNodeMaster",statusNodeMasters);
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion6);
				if(lstStatus.size()==0){
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion5,criterion6);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> getSSDelta(DistrictMaster districtMaster,String secondaryStatusName)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(secondaryStatusName!=null){
				Criterion criterion1 =	Restrictions.eq("status","A");
				Criterion criterion2 =	Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion3 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion4 =  Restrictions.ilike("secondaryStatusName",secondaryStatusName+"%");
				lstStatus = findByCriteria(criterion1,criterion2,criterion3,criterion4);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondryStatusByJobCategory(JobOrder jobOrder)
	{
		List<SecondaryStatus> lstSecondaryStatus=new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1 =	Restrictions.eq("status","A");
			Criterion criterion2=Restrictions.eq("jobCategoryMaster", jobOrder.getJobCategoryMaster());
			Criterion criterion3=Restrictions.isNull("statusNodeMaster");
			Criterion criterion4=Restrictions.eq("districtMaster", jobOrder.getDistrictMaster());
		    lstSecondaryStatus = findByCriteria(Order.asc("secondaryStatusName"),criterion1,criterion2,criterion3,criterion4);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	return lstSecondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusListByStatusName(String secondaryStatusName,DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(secondaryStatusName!=null){
				Criterion criterion2				=	Restrictions.eq("status","A");
				Criterion criterion4 =	Restrictions.isNotNull("jobCategoryMaster");
				Criterion criterion5=	Restrictions.isNull("jobCategoryMaster");
				Criterion criterion6=Restrictions.ilike("secondaryStatusName",secondaryStatusName+"%");
				Criterion criterion7=null;
				if(districtMaster!=null){
					criterion7=Restrictions.eq("districtMaster", districtMaster);
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion2,criterion4,criterion6,criterion7);
				} else {
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion2,criterion4,criterion6);
				}
				if(lstStatus.size()==0){
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion2,criterion5,criterion6,criterion7);
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByDistrictOnly(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
				Criterion criterion2				=	Restrictions.eq("status","A");
				Criterion criterion7=Restrictions.eq("districtMaster", districtMaster);
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion2,criterion7);
				if(lstStatus.size()==0){
					lstStatus = findByCriteria(Order.asc("orderNumber"),criterion2,criterion7);
				}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByDistrictAndJobCategoryAndHead(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster)
	{
		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::>>>>>>>>>>>>>>>>>>>>>.:findSecondaryStatusByDistrictAndJobCategoryAndHead::::::::::::::::::::::::::::::::::::");
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtMaster",districtMaster));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			/*if(branchMaster!=null){
				criteria.add(Restrictions.eq("branchMaster",branchMaster));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}*/
			if(headQuarterMaster!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			}
			if(jobCategoryMaster!=null){
//////////////////////////Sub Job category Logic//////////////////////////////////////
				boolean jobSubCateFlag = false;
				boolean jobSubCateSLCFlag = false;
				JobCategoryMaster parentJobCategory = null;
				
				
				try
				{
					 if(jobCategoryMaster!=null){
						 	System.out.println(" 01::::::::");
							if(jobCategoryMaster.getParentJobCategoryId()!=null){
								jobSubCateFlag = true;
								if(jobCategoryMaster.getAttachSLCFromJC()!=null && jobCategoryMaster.getAttachSLCFromJC()){	
									System.out.println(" 02");
									parentJobCategory = jobCategoryMaster.getParentJobCategoryId();
									jobSubCateSLCFlag = true; //DSPQ display By Parent Job category
								}
								else if(jobCategoryMaster.getHeadQuarterMaster()!=null && jobCategoryMaster.getParentJobCategoryId()!=null){
									System.out.println("04");
									parentJobCategory = jobCategoryMaster.getParentJobCategoryId();
									jobSubCateSLCFlag = true; 
								}
								else{
									System.out.println(" 03");
									jobSubCateSLCFlag = false; //DSPQ display By Sub Job category
								}
							}
						}
					 
					 if(jobSubCateFlag && jobSubCateSLCFlag){
						 System.out.println(" 06::::::::");
						 criteria.add(Restrictions.eq("jobCategoryMaster",parentJobCategory));
					 }else{
						 System.out.println(" 07::::::::");
						 criteria.add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster));
					 }
				//	criteria.add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster));
				}catch(Exception e){
					e.printStackTrace();
				}
				/////////////////////////////////////////////////////////////////////////////////////////
			}else{
				criteria.add(Restrictions.isNull("jobCategoryMaster"));
			}
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.asc("orderNumber"));
			lstSecondaryStatus = criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstSecondaryStatus;
		}
	}
	@Transactional(readOnly=false)
	public boolean copyDataFromDistrictToJobCategoryBanchAndHead(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,UserMaster userMaster)
	{
		
		System.out.println("::::::::::::Yes Newwwwwwwwww copyDataFromDistrictToJobCategoryBanchAndHead::::::::::::::::::::::::");
		boolean isReturn=true;
		List<SecondaryStatus> lstSecondaryStatusSource= new ArrayList<SecondaryStatus>();
		List<SecondaryStatus> lstSecondaryStatusIsNull= new ArrayList<SecondaryStatus>();
		
		Map<Integer , SecondaryStatus> map=new HashMap<Integer, SecondaryStatus>();
		
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("jobCategoryMaster"));
			criteria.add(Restrictions.isNull("secondaryStatus"));
			criteria.add(Restrictions.isNull("branchMaster"));
			if(userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6){
				if(districtMaster!=null){
					System.out.println("1111111111 11");
					criteria.add(Restrictions.eq("districtMaster",districtMaster));
				}
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			
			if(userMaster.getEntityType()==5){
				if(headQuarterMaster!=null){
					System.out.println("1111111111 33");
					criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				}
			}
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.asc("orderNumber"));
			lstSecondaryStatusIsNull = criteria.list();
			
			System.out.println("lstSecondaryStatusIsNull>>>>>>>>>:::::::::::::"+lstSecondaryStatusIsNull.size());
			
			for(SecondaryStatus ss:lstSecondaryStatusIsNull){
				System.out.println(" mmmmmmmmmmmm>mmmm>>>>>>> "+ss.getSecondaryStatusName());
			}
			
			
			SessionFactory sessionFactory=getSessionFactory();
			 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
        	 Transaction txOpen =statelesSsession.beginTransaction();
		
			
			if(lstSecondaryStatusIsNull.size()>0)
			{
				for(SecondaryStatus secondaryStatus:lstSecondaryStatusIsNull)
				{
					SecondaryStatus status=new SecondaryStatus();
					if(districtMaster!=null){
						status.setDistrictMaster(districtMaster);
					}
					if(branchMaster!=null){
						status.setBranchMaster(branchMaster);
					}
					if(headQuarterMaster!=null){
						status.setHeadQuarterMaster(headQuarterMaster);
					}
					status.setSchoolMaster(secondaryStatus.getSchoolMaster());
					status.setJobCategoryMaster(jobCategoryMaster);
					status.setSecondaryStatusName(secondaryStatus.getSecondaryStatusName());
					status.setStatusMaster(secondaryStatus.getStatusMaster());
					status.setSecondaryStatus(secondaryStatus.getSecondaryStatus());
					status.setStatusNodeMaster(secondaryStatus.getStatusNodeMaster());
					status.setOrderNumber(secondaryStatus.getOrderNumber());
					status.setStatus(secondaryStatus.getStatus());
					status.setUsermaster(userMaster);
					status.setSecondaryStatus_copy(secondaryStatus);
					status.setCreatedDateTime(secondaryStatus.getCreatedDateTime());
					statelesSsession.insert(status);
					map.put(secondaryStatus.getSecondaryStatusId(),status);
				}
			}
			txOpen.commit();
	       	statelesSsession.close();
	       	
	       	sessionFactory=getSessionFactory();
			statelesSsession = sessionFactory.openStatelessSession();
			txOpen =statelesSsession.beginTransaction();
			
			
			Session sessionNew = getSession();
			Criteria criteriaNew = sessionNew.createCriteria(getPersistentClass());
			criteriaNew.add(Restrictions.isNull("jobCategoryMaster"));
			criteriaNew.add(Restrictions.isNotNull("secondaryStatus"));
			criteriaNew.add(Restrictions.isNull("branchMaster"));
			if(userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6){
				if(districtMaster!=null){
					System.out.println("1111111111 1");
					criteriaNew.add(Restrictions.eq("districtMaster",districtMaster));
				}
			}else{
				criteriaNew.add(Restrictions.isNull("districtMaster"));
			}
			if(userMaster.getEntityType()==5){
				if(headQuarterMaster!=null){
					System.out.println("1111111111 3");
					criteriaNew.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
				}
			}
			criteriaNew.add(Restrictions.eq("status","A"));
			criteriaNew.addOrder(Order.asc("orderNumber"));
			lstSecondaryStatusSource = criteriaNew.list();
			
			System.out.println("lstSecondaryStatusSource:::::::::::::::"+lstSecondaryStatusSource.size());
			if(lstSecondaryStatusSource.size()>0)
			{
				
				int i=0;
				for(SecondaryStatus secondaryStatus:lstSecondaryStatusSource)
				{
					SecondaryStatus status=new SecondaryStatus();
					++i;
					
					if(districtMaster!=null){
						status.setDistrictMaster(districtMaster);
					}
					if(branchMaster!=null){
						status.setBranchMaster(branchMaster);
					}
					if(headQuarterMaster!=null){
						status.setHeadQuarterMaster(headQuarterMaster);
					}
					status.setSchoolMaster(secondaryStatus.getSchoolMaster());
					status.setJobCategoryMaster(jobCategoryMaster);
					status.setSecondaryStatusName(secondaryStatus.getSecondaryStatusName());
					status.setStatusMaster(secondaryStatus.getStatusMaster());
					status.setSecondaryStatus(map.get(secondaryStatus.getSecondaryStatus().getSecondaryStatusId()));
					status.setStatusNodeMaster(secondaryStatus.getStatusNodeMaster());
					status.setOrderNumber(secondaryStatus.getOrderNumber());
					status.setStatus(secondaryStatus.getStatus());
					status.setUsermaster(userMaster);
					status.setSecondaryStatus_copy(secondaryStatus);
					status.setCreatedDateTime(secondaryStatus.getCreatedDateTime());
					statelesSsession.insert(status);
				}
			}
			txOpen.commit();
	       	statelesSsession.close();
		} 
		catch (Exception e) {
			e.printStackTrace();
			isReturn=false;
		}
		finally
		{
			return isReturn;
		}
	}
	@Transactional(readOnly=false)
	public String  findSecondaryStatusExistForHeadBranch(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String secStatus)
	{
		System.out.println("  findSecondaryStatusExistForHeadBranch  :: "+secStatus);
		List<SecondaryStatus> secondaryStatusList= new ArrayList<SecondaryStatus>();
		String secondaryStatusName="";
		try 
		{
			Criterion criterion=Restrictions.ilike("secondaryStatusName",secStatus+"%");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtMaster",districtMaster));
			}
			if(branchMaster!=null){
				criteria.add(Restrictions.eq("branchMaster",branchMaster));
			}
			if(headQuarterMaster!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
			}
			criteria.add(criterion);
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.desc("secondaryStatusId"));
			secondaryStatusList = criteria.list();
			if(secondaryStatusList.size()>0){
				String secStatusNamePre=secondaryStatusList.get(0).getSecondaryStatusName().replace(secStatus+"1.","");
				int secStatusName=1;
				if(secStatusNamePre.length()>0){
					try{
						secStatusName+=Integer.parseInt(secStatusNamePre);
					}catch(Exception e){ //e.printStackTrace();
					}
					secondaryStatusName=secStatus+"1."+secStatusName;	
				}else{
					secondaryStatusName=secStatus+"1.1";
				}
			}else{
				secondaryStatusName=secStatus;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return secondaryStatusName;
	}
	@Transactional(readOnly=false)
	public Map<SecondaryStatus,JobCategoryMaster> findSSByDistrictPlusByMapHeadBranch(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,SecondaryStatus secondaryStatusObj)
	{
		System.out.println("::::::::::::::::::::findSSByDistrictPlusByMapHeadBranch:::::::::::::::::::::");
		Map<SecondaryStatus, JobCategoryMaster> map=new HashMap<SecondaryStatus, JobCategoryMaster>();
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion=null;
			if(headQuarterMaster!=null){
				criterion=Restrictions.eq("headQuarterMaster", headQuarterMaster);
			}else{
				criterion=Restrictions.eq("districtMaster", districtMaster);
			}
			Criterion criterion_jcNotNull=Restrictions.isNotNull("jobCategoryMaster");
			Criterion criterion_ssIsNull=Restrictions.isNull("secondaryStatus");
			Criterion criterion_ssName=Restrictions.eq("secondaryStatusName",secondaryStatusObj.getSecondaryStatusName());
			
			if(headQuarterMaster!=null && branchMaster==null){
				System.out.println("::::::::::head:::::::::::::::");
				lstSecondaryStatus = findByCriteria(Order.asc("orderNumber"),criterion,criterion_jcNotNull,criterion_ssIsNull,criterion_ssName,Restrictions.isNull("branchMaster"),Restrictions.isNull("districtMaster"));
			}
			else if(branchMaster!=null)
			{
				System.out.println("::::::::::branch:::::::::::::::");
				lstSecondaryStatus = findByCriteria(Order.asc("orderNumber"),criterion,criterion_jcNotNull,criterion_ssIsNull,criterion_ssName,Restrictions.eq("branchMaster",branchMaster),Restrictions.isNull("districtMaster"));

			}
			else{
				System.out.println("::::::::::head Else:::::::::::::::");
				lstSecondaryStatus = findByCriteria(Order.asc("orderNumber"),criterion,criterion_jcNotNull,criterion_ssIsNull,criterion_ssName,Restrictions.isNull("branchMaster"),Restrictions.isNull("headQuarterMaster"));
			}
			
			if(lstSecondaryStatus.size()>0)
			{
				for(SecondaryStatus pojo:lstSecondaryStatus)
				{
					if(pojo!=null && pojo.getJobCategoryMaster()!=null)
					{
						if(map.get(pojo)==null)
						{
							map.put(pojo, pojo.getJobCategoryMaster());
						}
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return map;
		}
		
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusHeadQuarterBranchOnly(HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(headQuarterMaster!=null)
			{
				criteria.add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}
			if(branchMaster!=null)
			{
				criteria.add(Restrictions.eq("branchMaster", branchMaster));
			}else
			{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(districtMaster!=null)
			{
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}
			else
			{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			criteria.add(Restrictions.isNull("jobCategoryMaster"));
			criteria.add(Restrictions.eq("status", "A"));
			criteria.add(Restrictions.isNotNull("statusMaster"));
			lstStatus = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusOnlyByHeadBranch(HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(headQuarterMaster!=null)
			{
				criteria.add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}
			if(branchMaster!=null)
			{
				criteria.add(Restrictions.eq("branchMaster", branchMaster));
			}else
			{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(districtMaster!=null)
			{
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}
			else
			{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			criteria.add(Restrictions.isNull("jobCategoryMaster"));
			criteria.add(Restrictions.eq("status", "A"));
			criteria.add(Restrictions.isNull("statusMaster"));
			criteria.addOrder(Order.asc("orderNumber"));
			criteria.add(Restrictions.isNull("statusNodeMaster"));
			lstStatus = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus==null || lstStatus.size()==0)
		return null;
		else
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findHBDSecondaryStatusOnlyStatusForDPoints(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 =	Restrictions.isNotNull("statusMaster");
			Criterion criterion3 =	Restrictions.eq("status","A");
			Criterion criterion4=	Restrictions.isNull("jobCategoryMaster");
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusOnlyForHeadQuarter(HeadQuarterMaster headQuarterMaster)
	{
		System.out.println("findSecondaryStatusOnlyForHeadQuarter::::::::::::::::::");
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Criterion criterion1	=	Restrictions.isNull("statusMaster");
			Criterion criterion2	=	Restrictions.isNull("statusNodeMaster");
			Criterion criterion5	=	Restrictions.isNull("jobCategoryMaster");
			Criterion criterion6	=	Restrictions.isNull("branchMaster");
			Criterion criterion7	=	Restrictions.isNull("districtMaster");
			Criterion criterion3	=	Restrictions.eq("status","A");
			Criterion criterion4 	=	Restrictions.eq("headQuarterMaster", headQuarterMaster);
			lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion4,criterion5,criterion6,criterion7);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryHeadAndBranch(JobOrder jobOrder){
		System.out.println(":::::::::::findSecondaryStatusByJobCategoryHeadAndBranch:::::::::::::"+jobOrder.getJobId());
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			/*if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}*/
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			
			//////////////////////////Sub Job category Logic//////////////////////////////////////
			boolean jobSubCateFlag = false;
			boolean jobSubCateSLCFlag = false;
			JobCategoryMaster parentJobCategory = null;
			
			//System.out.println("jobOrder:::::::::Category==============>>>>>>>>>>>>>========================================="+jobOrder.getJobCategoryMaster().getJobCategoryId());
			
			try
			{
				 if(jobOrder!=null){
					 	
						if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()>0){
							System.out.println("01");
							jobSubCateFlag = true;
							if(jobOrder.getJobCategoryMaster().getAttachSLCFromJC()!=null && jobOrder.getJobCategoryMaster().getAttachSLCFromJC()){
								System.out.println("02");
								parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								jobSubCateSLCFlag = true; //DSPQ display By Parent Job category
							}
							else if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
								System.out.println("04");
								parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								jobSubCateSLCFlag = true;
							//	criteria.add(Restrictions.isNull("branchMaster"));
							}
							else{
								System.out.println("03");
								jobSubCateSLCFlag = false; //DSPQ display By Sub Job category
							}
						}
					}
				 
				 if(jobSubCateFlag && jobSubCateSLCFlag){
					 System.out.println(" parentJobCategory "+parentJobCategory.getJobCategoryId());
					 criteria.add(Restrictions.eq("jobCategoryMaster",parentJobCategory));
				 }else{
					 criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
				 }
			}catch(Exception e){
				e.printStackTrace();
			}
			/////////////////////////////////////////////////////////////////////////////////////////
	
			criteria.add(Restrictions.isNotNull("jobCategoryMaster"));
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.asc("orderNumber"));
			lstStatus = criteria.list();
			System.out.println(" lstStatus :: "+lstStatus.size());
			
			if(lstStatus.size()==0){
				Criteria criteriaNew = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
					criteriaNew.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("districtMaster"));
					criteriaNew.add(Restrictions.isNull("branchMaster"));
				}
				/*if(jobOrder.getBranchMaster()!=null){
					criteriaNew.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("branchMaster"));
				}*/
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteriaNew.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("headQuarterMaster"));
				}
				criteriaNew.add(Restrictions.isNull("jobCategoryMaster"));
				criteriaNew.add(Restrictions.eq("status","A"));
				criteriaNew.addOrder(Order.asc("orderNumber"));
				lstStatus = criteriaNew.list();
			}
			System.out.println("lstStatus::::::::>>New::::::::"+lstStatus.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryWithOutStatusAndHB(JobOrder jobOrder)
	{
		System.out.println(" =================== findSecondaryStatusByJobCategoryWithOutStatusAndHB =======");
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
			criteria.add(Restrictions.isNotNull("jobCategoryMaster"));
			criteria.add(Restrictions.isNull("statusNodeMaster"));
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.asc("orderNumber"));
			lstStatus = criteria.list();
			if(lstStatus.size()==0){
				
				Criteria criteriaNew = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
					criteriaNew.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("districtMaster"));
				}
				if(jobOrder.getBranchMaster()!=null){
					criteriaNew.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("branchMaster"));
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteriaNew.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("headQuarterMaster"));
				}
				criteriaNew.add(Restrictions.isNull("jobCategoryMaster"));
				criteriaNew.add(Restrictions.isNull("statusNodeMaster"));
				criteriaNew.add(Restrictions.eq("status","A"));
				criteriaNew.addOrder(Order.asc("orderNumber"));
				lstStatus = criteriaNew.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryWithStatusAndHBD(JobOrder jobOrder,StatusMaster statusMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(statusMaster!=null){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
					criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
				}else{
					criteria.add(Restrictions.isNull("districtMaster"));
				}
				if(jobOrder.getBranchMaster()!=null){
					criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
				}else{
					criteria.add(Restrictions.isNull("branchMaster"));
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
				}else{
					criteria.add(Restrictions.isNull("headQuarterMaster"));
				}
				criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
				criteria.add(Restrictions.isNotNull("jobCategoryMaster"));
				criteria.add(Restrictions.eq("statusNodeMaster",statusMaster.getStatusNodeMaster()));
				criteria.add(Restrictions.eq("status","A"));
				criteria.addOrder(Order.asc("orderNumber"));
				lstStatus = criteria.list();
				if(lstStatus.size()==0){
					
					Criteria criteriaNew = session.createCriteria(getPersistentClass());
					if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
						criteriaNew.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
					}else{
						criteriaNew.add(Restrictions.isNull("districtMaster"));
					}
					if(jobOrder.getBranchMaster()!=null){
						criteriaNew.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
					}else{
						criteriaNew.add(Restrictions.isNull("branchMaster"));
					}
					if(jobOrder.getHeadQuarterMaster()!=null){
						criteriaNew.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
					}else{
						criteriaNew.add(Restrictions.isNull("headQuarterMaster"));
					}
					criteriaNew.add(Restrictions.isNull("jobCategoryMaster"));
					criteria.add(Restrictions.eq("statusNodeMaster",statusMaster.getStatusNodeMaster()));
					criteriaNew.add(Restrictions.eq("status","A"));
					criteriaNew.addOrder(Order.asc("orderNumber"));
					lstStatus = criteriaNew.list();
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategoryAndDistrictHBD(List<JobCategoryMaster> jobCategoryMasterList,JobOrder jobOrder,SecondaryStatus secondaryStatus)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
			if(jobCategoryMasterList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
					criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
				}else{
					criteria.add(Restrictions.isNull("districtMaster"));
				}
				if(jobOrder.getBranchMaster()!=null){
					criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
				}else{
					criteria.add(Restrictions.isNull("branchMaster"));
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
				}else{
					criteria.add(Restrictions.isNull("headQuarterMaster"));
				}
				criteria.add(Restrictions.in("jobCategoryMaster",jobCategoryMasterList));
				criteria.add(Restrictions.eq("secondaryStatusName",secondaryStatus.getSecondaryStatusName()));
				criteria.add(Restrictions.eq("status","A"));
				criteria.addOrder(Order.asc("orderNumber"));
				lstStatus = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> getSecondaryStatusForPanelByHBD(JobOrder jobOrder,StatusMaster statusMaster)
	{
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			
			//////////////////////////Sub Job category Logic//////////////////////////////////////
			boolean jobSubCateFlag = false;
			boolean jobSubCateSLCFlag = false;
			JobCategoryMaster parentJobCategory = null;
			
			System.out.println("jobOrder:::::::::Category==============>>>>>>>>>>>>>========================================="+jobOrder.getJobCategoryMaster().getJobCategoryId());
			
			try
			{
				 if(jobOrder!=null){
					 	
					 if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()>0){
							System.out.println("01");
							jobSubCateFlag = true;
							if(jobOrder.getJobCategoryMaster().getAttachSLCFromJC()!=null && jobOrder.getJobCategoryMaster().getAttachSLCFromJC()){
								System.out.println("02");
								parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								jobSubCateSLCFlag = true; //DSPQ display By Parent Job category
							}
							else if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
								System.out.println("04");
								parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								jobSubCateSLCFlag = true;
							}
							else{
								System.out.println("03");
								jobSubCateSLCFlag = false; //DSPQ display By Sub Job category
							}
						}
					}
				 
				 if(jobSubCateFlag && jobSubCateSLCFlag){
					 System.out.println(" parentJobCategory "+parentJobCategory.getJobCategoryId());
					 criteria.add(Restrictions.eq("jobCategoryMaster",parentJobCategory));
				 }else{
					 criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
				 }
			}catch(Exception e){
				e.printStackTrace();
			}
			/////////////////////////////////////////////////////////////////////////////////////////
			
			//criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
			criteria.add(Restrictions.eq("statusMaster",statusMaster));
			criteria.add(Restrictions.eq("status","A"));
			lstSecondaryStatus = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return lstSecondaryStatus;
	}
	@Transactional(readOnly=false)
	public SecondaryStatus findSecondaryStatusObjByJobCategoryAndHBD(JobOrder jobOrder)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		SecondaryStatus secondaryStatus=null;
		try 
		{
			if(jobOrder.getBranchMaster()==null)
			if((jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getSecondaryStatus()!=null) || (jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getSecondaryStatus()!=null)){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
					criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
					criteria.add(Restrictions.eq("secondaryStatus_copy",jobOrder.getDistrictMaster().getSecondaryStatus()));
				}else{
					criteria.add(Restrictions.isNull("districtMaster"));
				}
				if(jobOrder.getBranchMaster()!=null){
					//criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
					//criteria.add(Restrictions.eq("secondaryStatus_copy",jobOrder.getBranchMaster().getSecondaryStatus()));
				}else{
					criteria.add(Restrictions.isNull("branchMaster"));
				}
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
					criteria.add(Restrictions.eq("secondaryStatus_copy",jobOrder.getHeadQuarterMaster().getSecondaryStatus()));
				}else{
					criteria.add(Restrictions.isNull("headQuarterMaster"));
				}
				criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
				criteria.add(Restrictions.isNotNull("jobCategoryMaster"));
				criteria.add(Restrictions.eq("status","A"));
				criteria.addOrder(Order.asc("orderNumber"));
				lstStatus = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstStatus.size()>0){
			secondaryStatus=lstStatus.get(0);
		}
		return secondaryStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> getSecondaryStatusForRenameByHBD(SecondaryStatus secondaryStatus,String folderName)
	{
		System.out.println(secondaryStatus.getSecondaryStatusId()+":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::getSecondaryStatusForRenameByHBD::::::::::::::::::::::::::::::::::::::::::::::::::::"+folderName);
		List<SecondaryStatus> lstSecondaryStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(secondaryStatus.getDistrictMaster()!=null){
				criteria.add(Restrictions.eq("districtMaster",secondaryStatus.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
			}
			if(secondaryStatus.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",secondaryStatus.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			if(secondaryStatus.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",secondaryStatus.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			if(secondaryStatus.getJobCategoryMaster()!=null){
				criteria.add(Restrictions.eq("jobCategoryMaster",secondaryStatus.getJobCategoryMaster()));
			}else{
				criteria.add(Restrictions.isNull("jobCategoryMaster"));
			}
			criteria.add(Restrictions.ne("secondaryStatusId",secondaryStatus.getSecondaryStatusId()));
			criteria.add(Restrictions.like("secondaryStatusName",folderName.trim()));
			criteria.add(Restrictions.eq("status","A"));
			lstSecondaryStatus = criteria.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}		
		return lstSecondaryStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategory(List<JobCategoryMaster> jobCategoryMasterList,String statusName)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
			if(jobCategoryMasterList.size()>0){
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.isNotNull("headQuarterMaster");
				Criterion criterion3 =	Restrictions.in("jobCategoryMaster",jobCategoryMasterList);
				Criterion criterion6 =	Restrictions.eq("secondaryStatusName",statusName);
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2,criterion3,criterion6);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByNames(List<JobCategoryMaster> jobCategoryMasterList,List<String> statusNames)
	{
		System.out.println(":::::::::::findSecondaryStatusByNames::::::::::>"+jobCategoryMasterList.size());
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(jobCategoryMasterList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.in("jobCategoryMaster",jobCategoryMasterList));
				criteria.add(Restrictions.in("secondaryStatusName",statusNames));
				criteria.add(Restrictions.eq("status","A"));
				lstStatus = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByNamesWithDefault(List<String> statusNames,BranchMaster branchMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.isNull("jobCategoryMaster"));
			criteria.add(Restrictions.isNotNull("headQuarterMaster"));
			criteria.add(Restrictions.eq("branchMaster",branchMaster));
			criteria.add(Restrictions.in("secondaryStatusName",statusNames));
			criteria.add(Restrictions.eq("status","A"));
			lstStatus = criteria.list();
			if(lstStatus.size()==0){
				Criteria criteria2 = session.createCriteria(getPersistentClass());
				criteria2.add(Restrictions.isNull("jobCategoryMaster"));
				criteria2.add(Restrictions.isNotNull("headQuarterMaster"));
				criteria2.add(Restrictions.isNull("branchMaster"));
				criteria2.add(Restrictions.in("secondaryStatusName",statusNames));
				criteria2.add(Restrictions.eq("status","A"));
				lstStatus = criteria2.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<String> findSLCByHeadQuarter(HeadQuarterMaster headQuarterMaster)
	{
		List<String> lstStatus= new ArrayList<String>();
		try{
			
			if(headQuarterMaster!=null){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("headQuarterMaster",headQuarterMaster);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(Restrictions.isNull("branchMaster"));
				criteria.add(Restrictions.isNull("districtMaster"));
				
				lstStatus =  criteria.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("slcName"))
				).list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByHeadQuarter(HeadQuarterMaster headQuarterMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("headQuarterMaster",headQuarterMaster);
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByDistrict(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("districtMaster",districtMaster);
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategory(JobCategoryMaster jobCategoryMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.eq("jobCategoryMaster",jobCategoryMaster);
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusByJobCategory(List<JobCategoryMaster> jobCategoryMasterList)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try{
				Criterion criterion2 =	Restrictions.eq("status","A");
				Criterion criterion1 =	Restrictions.in("jobCategoryMaster",jobCategoryMasterList);
				lstStatus = findByCriteria(Order.asc("orderNumber"),criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findOrderedSecondaryStatusByJobCategory(List<JobCategoryMaster> jobCategoryMasterList)
	{
		System.out.println(":::::::::::findSecondaryStatusByNames::::::::::>"+jobCategoryMasterList.size());
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(jobCategoryMasterList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.in("jobCategoryMaster",jobCategoryMasterList));
				criteria.addOrder(Order.asc("secondaryStatusId"));
				criteria.add(Restrictions.eq("status","A"));
				lstStatus = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findOrderedSecondaryStatusByJobCategoryForHead(List<JobCategoryMaster> jobCategoryMasterList)
	{
		System.out.println(":::::::::::findSecondaryStatusByNames::::::::::>"+jobCategoryMasterList.size());
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			if(jobCategoryMasterList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.isNotNull("headQuarterMaster"));
				criteria.add(Restrictions.isNull("branchMaster"));
				criteria.add(Restrictions.isNull("districtMaster"));
				criteria.add(Restrictions.in("jobCategoryMaster",jobCategoryMasterList));
				//criteria.addOrder(Order.asc("secondaryStatusId"));
				criteria.add(Restrictions.eq("status","A"));
				lstStatus = criteria.list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> getOfferReadyByDistrict(DistrictMaster districtMaster)
	{
		System.out.println(":::::::::::getOfferReadyByDistrict::::::::::>"+districtMaster);
		List<SecondaryStatus> lstStatus = new ArrayList<SecondaryStatus>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtMaster", districtMaster));
			}
			Criterion criterion1 = Restrictions.eq("secondaryStatusName","Offer Ready");
			criteria.add(criterion1);
			lstStatus = criteria.list();
			System.out.println("lstStatus :: "+lstStatus.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	@Transactional(readOnly=false)
	public SecondaryStatus findSecondaryStatusObjByJobCategoryHeadAndBranch(JobOrder jobOrder,String secondaryStatusName){
		System.out.println(":::::::::::findSecondaryStatusByJobCategoryHeadAndBranch:::::::::::::"+jobOrder.getJobId());
		SecondaryStatus secondaryStatus=null;
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			/*if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}*/
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			
			//////////////////////////Sub Job category Logic//////////////////////////////////////
			boolean jobSubCateFlag = false;
			boolean jobSubCateSLCFlag = false;
			JobCategoryMaster parentJobCategory = null;
			
			//System.out.println("jobOrder:::::::::Category==============>>>>>>>>>>>>>========================================="+jobOrder.getJobCategoryMaster().getJobCategoryId());
			
			try
			{
				 if(jobOrder!=null){
					 	
						if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()>0){
							System.out.println("01");
							jobSubCateFlag = true;
							if(jobOrder.getJobCategoryMaster().getAttachSLCFromJC()!=null && jobOrder.getJobCategoryMaster().getAttachSLCFromJC()){
								System.out.println("02");
								parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								jobSubCateSLCFlag = true; //DSPQ display By Parent Job category
							}
							else if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
								System.out.println("04");
								parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								jobSubCateSLCFlag = true;
							//	criteria.add(Restrictions.isNull("branchMaster"));
							}
							else{
								System.out.println("03");
								jobSubCateSLCFlag = false; //DSPQ display By Sub Job category
							}
						}
					}
				 
				 if(jobSubCateFlag && jobSubCateSLCFlag){
					 System.out.println(" parentJobCategory "+parentJobCategory.getJobCategoryId());
					 criteria.add(Restrictions.eq("jobCategoryMaster",parentJobCategory));
				 }else{
					 criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
				 }
			}catch(Exception e){
				e.printStackTrace();
			}
			/////////////////////////////////////////////////////////////////////////////////////////
	
			criteria.add(Restrictions.isNotNull("jobCategoryMaster"));
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.asc("orderNumber"));
			lstStatus = criteria.list();
			System.out.println(" lstStatus :: "+lstStatus.size());
			
			if(lstStatus.size()==0){
				Criteria criteriaNew = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
					criteriaNew.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("districtMaster"));
					criteriaNew.add(Restrictions.isNull("branchMaster"));
				}
				/*if(jobOrder.getBranchMaster()!=null){
					criteriaNew.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("branchMaster"));
				}*/
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteriaNew.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("headQuarterMaster"));
				}
				criteriaNew.add(Restrictions.isNull("jobCategoryMaster"));
				criteriaNew.add(Restrictions.eq("status","A"));
				criteriaNew.addOrder(Order.asc("orderNumber"));
				lstStatus = criteriaNew.list();
			}
			System.out.println("lstStatus::::::::>>New::::::::"+lstStatus.size());
			if(lstStatus.size()>0){
				for (SecondaryStatus secondaryStatus2 : lstStatus) {
					if(secondaryStatus2.getSecondaryStatusName().equalsIgnoreCase(secondaryStatusName)){
						secondaryStatus=secondaryStatus2;
					}
				}
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return secondaryStatus;
	}
	
	
	@Transactional(readOnly=false)
	public List<String[]> findSecondaryStatusByJobCategoryHeadAndBranchOp(JobOrder jobOrder){
		System.out.println(":::::::::::findSecondaryStatusByJobCategoryHeadAndBranch:::::::::::::"+jobOrder.getJobId());
		List<String[]> lstStatus= new ArrayList<String[]>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
				criteria.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				criteria.add(Restrictions.isNull("districtMaster"));
				criteria.add(Restrictions.isNull("branchMaster"));
			}
			/*if(jobOrder.getBranchMaster()!=null){
				criteria.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				criteria.add(Restrictions.isNull("branchMaster"));
			}*/
			if(jobOrder.getHeadQuarterMaster()!=null){
				criteria.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				criteria.add(Restrictions.isNull("headQuarterMaster"));
			}
			
			//////////////////////////Sub Job category Logic//////////////////////////////////////
			boolean jobSubCateFlag = false;
			boolean jobSubCateSLCFlag = false;
			JobCategoryMaster parentJobCategory = null;
			
			//System.out.println("jobOrder:::::::::Category==============>>>>>>>>>>>>>========================================="+jobOrder.getJobCategoryMaster().getJobCategoryId());
			
			try
			{
				 if(jobOrder!=null){
					 	
						if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()>0){
							System.out.println("01");
							jobSubCateFlag = true;
							if(jobOrder.getJobCategoryMaster().getAttachSLCFromJC()!=null && jobOrder.getJobCategoryMaster().getAttachSLCFromJC()){
								System.out.println("02");
								parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								jobSubCateSLCFlag = true; //DSPQ display By Parent Job category
							}
							else if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
								System.out.println("04");
								parentJobCategory = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
								jobSubCateSLCFlag = true;
							//	criteria.add(Restrictions.isNull("branchMaster"));
							}
							else{
								System.out.println("03");
								jobSubCateSLCFlag = false; //DSPQ display By Sub Job category
							}
						}
					}
				 
				 if(jobSubCateFlag && jobSubCateSLCFlag){
					 System.out.println(" parentJobCategory "+parentJobCategory.getJobCategoryId());
					 criteria.add(Restrictions.eq("jobCategoryMaster",parentJobCategory));
				 }else{
					 criteria.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
				 }
			}catch(Exception e){
				e.printStackTrace();
			}
			/////////////////////////////////////////////////////////////////////////////////////////
	
			criteria.add(Restrictions.isNotNull("jobCategoryMaster"));
			criteria.add(Restrictions.eq("status","A"));
			criteria.addOrder(Order.asc("orderNumber"));
			
			criteria.setProjection( Projections.projectionList()
                    .add( Projections.property("statusMaster.statusId"), "statusId" )
                    .add(Projections.property("secondaryStatusName"), "secondaryStatusName" )              
                    
                );
			
			
			lstStatus = criteria.list();
			System.out.println(" lstStatus :: "+lstStatus.size());
			
			if(lstStatus.size()==0){
				Criteria criteriaNew = session.createCriteria(getPersistentClass());
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getHeadQuarterMaster()==null){
					criteriaNew.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("districtMaster"));
					criteriaNew.add(Restrictions.isNull("branchMaster"));
				}
				/*if(jobOrder.getBranchMaster()!=null){
					criteriaNew.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("branchMaster"));
				}*/
				if(jobOrder.getHeadQuarterMaster()!=null){
					criteriaNew.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
				}else{
					criteriaNew.add(Restrictions.isNull("headQuarterMaster"));
				}
				criteriaNew.add(Restrictions.isNull("jobCategoryMaster"));
				criteriaNew.add(Restrictions.eq("status","A"));
				criteriaNew.addOrder(Order.asc("orderNumber"));
				
				criteria.setProjection( Projections.projectionList()
	                    .add( Projections.property("statusMaster.statusId"), "statusId" )
	                    .add(Projections.property("secondaryStatusName"), "secondaryStatusName" )              
	                    
	                );
				
				lstStatus = criteriaNew.list();
			}
			System.out.println("lstStatus::::::::>>New::::::::"+lstStatus.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	
	
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findByNameAndJobCats(String secondaryStatusName, List<JobCategoryMaster> jobCats)
	{
		System.out.println(":::::::::::findByNameAndHQ::::::::::>");
		List<SecondaryStatus> lstStatus = null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(jobCats!=null && jobCats.size()>0){
				criteria.add(Restrictions.in("jobCategoryMaster", jobCats));
			}
			Criterion criterion1 = Restrictions.eq("secondaryStatusName",secondaryStatusName);
			criteria.add(criterion1);
			lstStatus = criteria.list();
			System.out.println("lstStatus :: "+lstStatus.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	/*
	 * @Deepak 
	 * For the job Category
	 */
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findSecondaryStatusOp(DistrictMaster districtMaster)
	{
		List<SecondaryStatus> lstStatus= new ArrayList<SecondaryStatus>();
		Session session = getSession();
		try 
		{
			Criterion criterion2				=	Restrictions.eq("status","A");
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			Criteria criteria = session.createCriteria(this.getPersistentClass());
			criteria.add(criterion1).add(criterion2).addOrder(Order.asc("orderNumber"));
			
			
			criteria.setProjection( Projections.projectionList()
                    .add( Projections.property("secondaryStatusId"), "secondaryStatusId" )
                    .add(Projections.property("secondaryStatusName"), "secondaryStatusName" )              
                    .add(Projections.property("statusMaster"),"statusMaster")
                    .add(Projections.property("statusNodeMaster"),"statusNodeMaster")
                    .add(Projections.property("jobCategoryMaster"),"jobCategoryMaster")
                );
			
			lstStatus = criteria.setResultTransformer( new AliasToBeanResultTransformer(this.getPersistentClass())).list();  // findByCriteria(Order.asc("orderNumber"),criterion1,criterion2);		
			 
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstStatus;
	}
	
	// @Anurag for Optimization
	
	@Transactional(readOnly=false)
	public Map<Integer, String> findSecondaryStatusOnlyStatusOp(DistrictMaster districtMaster)
	{
		List<Object> lstStatus= new ArrayList<Object>();
		Map<Integer, String> resultMap = new HashMap<Integer, String>();
		Session session = getSession();
		try 
		{
	
			Criterion criterion1 =	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion2 =	Restrictions.isNotNull("statusMaster");
			Criterion criterion3	=	Restrictions.eq("status","A");
			Criteria criteria = session.createCriteria(this.getPersistentClass());
			criteria.add(criterion1).add(criterion2).add(criterion3).addOrder(Order.asc("orderNumber"));
			criteria.add(Restrictions.isNotNull("statusMaster.statusId"));
			
			criteria.setProjection( Projections.projectionList()
                    .add( Projections.property("secondaryStatusId"), "secondaryStatusId" )
                    .add(Projections.property("secondaryStatusName"), "secondaryStatusName" )              
                    .add(Projections.property("statusMaster.statusId"))
                
                );
			System.out.println("---------------------------  Query-------------------------");
			lstStatus = criteria.list();
			
			 if(lstStatus.size()>0){
                 for (Iterator it = lstStatus.iterator(); it.hasNext();) {
                 Object[] row = (Object[]) it.next()       ;                                                                 
                 resultMap.put(Integer.parseInt(row[2].toString()),row[1].toString());
             }               
            }
			
			
			//lstStatus = criteria.setResultTransformer( new AliasToBeanResultTransformer(this.getPersistentClass())).list();  // findByCriteria(Order.asc("orderNumber"),criterion1,criterion2);		
			System.out.println("---------------------------  Query-------------------------"+lstStatus.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return resultMap;
	}
	@Transactional(readOnly=false)
	public List<SecondaryStatus> findByNameListAndJobCats(List<String> secondaryStatusName, List<JobCategoryMaster> jobCats)
	{
		System.out.println(":::::::::::findByNameAndHQ::::::::::>");
		List<SecondaryStatus> lstStatus = null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(jobCats!=null && jobCats.size()>0){
				criteria.add(Restrictions.in("jobCategoryMaster", jobCats));
			}
			Criterion criterion1 = Restrictions.in("secondaryStatusName",secondaryStatusName);
			criteria.add(criterion1);
			lstStatus = criteria.list();
			System.out.println("lstStatus :: "+lstStatus.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstStatus;
	}
	

	// Optimization Anurag replacement of findSecStatusByStatusIdList	
	
	@Transactional(readOnly=false)
	public List<Integer> findSecStatusByStatusIdListOp(List secSstatusIdList)
	{
		List<Integer> lstStatus= new ArrayList<Integer>();
		try 
		{
			Criterion criterion2				=	Restrictions.eq("status","A");
			Criterion criterion1 = Restrictions.in("secondaryStatusId",secSstatusIdList);
			Criteria criteria = getSession().createCriteria(this.getPersistentClass());
			criteria.add(criterion1).add(criterion2);
			criteria.setProjection(Projections.property("secondaryStatusId"));
			lstStatus = criteria.list();		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstStatus;
	}
	
}
