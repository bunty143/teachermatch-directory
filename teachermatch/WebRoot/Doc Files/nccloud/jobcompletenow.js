function completeNow(prefStatus, portfolioStatus,jobId,inventory,isJobAssessment)
{
	document.getElementById("jobId").value=jobId;
	document.getElementById("savejobFlag").value=1;
	document.getElementById("completeNow").value=1;
	checkTeacherCriteria(prefStatus, portfolioStatus,jobId,inventory,isJobAssessment);
}
function resetCompleteVars()
{
	document.getElementById("prefStatus").value="";
	document.getElementById("portfolioStatus").value="";
	document.getElementById("inventory").value="";
	document.getElementById("jobId").value=0;
	document.getElementById("savejobFlag").value=0;
	document.getElementById("completeNow").value=0;
	document.getElementById("isJobAssessment").value="";
}
function checkTeacherCriteria(prefStatus, portfolioStatus,jobId,inventory,isJobAssessment){
	
	document.getElementById("prefStatus").value=prefStatus;
	document.getElementById("portfolioStatus").value=portfolioStatus;
	document.getElementById("inventory").value=inventory;
	document.getElementById("isJobAssessment").value=isJobAssessment;
	
	var jobOrder = {jobId:jobId}
	AssessmentCampaignAjax.checkTeacherCriteria(jobOrder,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data.length>0)
			{
				var qqTaken=data[0];
				var qq=data[3];
				var dsqq=data[1];
				var isAffi=data[2];
					//Miami check
				if(data[4]=="true"){
					document.getElementById("isMiami").value=true;
				}else{
					document.getElementById("isMiami").value=false;
				}
				if($("#qqFlag").length > 0){
					$("#qqFlag").val(qq);
				}
				if(dsqq=="false" && qq=="true"){
					getDistrictQuestionsSet(jobId);
				}else if(dsqq=="true" || qq=="true"){
					portfolioCompleteNow(jobId,isAffi,qq);
				}else{
					continueCompleteNow();
				}
			}
		}
	});	
}

function portfolioCompleteNow(jobId,isAffi,qq){
	try{document.getElementById("txtDistrictPortfolioConfig").value=0;}catch(e){}
	if(jobId > 0)
	{
		var candidateType = "E";
		if(isAffi==1)
			candidateType = "I";
		$("#txtCandidateType").val(isAffi);
		DistrictPortfolioConfigAjax.getPortfolioConfigByJobId(jobId,candidateType,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
			
			if(data!=null)
			{
				$("#StudentTeacherDiv").hide();
				$('#languageDiv').hide();
				$("#dspqQuestionHeading").html("District Specific Questions");
				if(data.headQuarterMaster!=null){
					$("#headQuaterIdForDspq").val(data.headQuarterMaster.headQuarterId);
					if(data.headQuarterMaster.headQuarterId==1){
						data.coverLetter=0;
						$("#forKellyCvrLtr").hide();
						$("#kellYcvRLtrMain").show();
						$("#kellYnxtInst").hide();
						$("#kellYnxtDisDivInf").hide();
						$("#forAllDistCvrLtr").show();
						$("#workForKelly2").prop("checked",false);
						$("#workForKelly1").prop("checked",false);
						$("#contactedKelly1").prop("checked",false);
						$("#contactedKelly2").prop("checked",false);
						$(".forAllDistCvrLtr").hide();
						$(".forKellyCvrLtr").show();										
						$(".continueBtnNxt").hide();
						$("#headQuaterIdForDspq").val(data.headQuarterMaster.headQuarterId);
						$("#dspqQuestionHeading").html("Application Questions");
					}
				}
				$("#tfaOptional").val(data.tfaOptional);
				$("#nationalBoardOptional").val(data.nationalBoardOptional);
				$("#certfiedTeachingExpOptional").val(data.certfiedTeachingExpOptional);
				$("#substituteOptional").val(data.substituteOptional);
				$("#eEocOptional").val(data.eEOCOptional);
				$("#videoSecOptional").val(data.videoSecOptional);
				$("#gpaOptional").val(data.gpaOptional);
				$("#empSecSalaryOptional").val(data.empSecSalaryOptional);
				$("#empSecRoleOptional").val(data.empSecRoleOptional);
				$("#empSecPrirOptional").val(data.empSecPrirOptional);
				$("#empSecMscrOptional").val(data.empSecMscrOptional);
				$("#ressumeOptional").val(data.ressumeOptional);
				$("#addressOptional").val(data.addressOptional);
				$("#expectedSalarySection").val(data.expectedSalary);
				$("#certificationUrl").val(data.certificationUrl);
				$("#certificationDoeNumber").val(data.certificationDoeNumber);
				$("#ssnOptional").val(data.ssnOptional);
				$("#academicsDatesOptional").val(data.academicsDatesOptional);
				$("#empDatesOptional").val(data.empDatesOptional);
				$("#certiDatesOptional").val(data.certiDatesOptional);
				$("#certiGrades").val(data.certiGrades);
				$("#empSecReasonForLeavOptional").val(data.empSecReasonForLeavOptional);
				$("#transcriptOptional").val(data.academicTranscript);
                $("#degreeOptional").val(data.degreeOptional);
				$("#schoolOptional").val(data.schoolOptional);
				$("#fieldOfStudyOptional").val(data.fieldOfStudyOptional);
				$("#empPositionOptional").val(data.empPositionOptional);
				$("#empOrganizationOptional").val(data.empOrganizationOptional);
				$("#empCityOptional").val(data.empCityOptional);
				$("#empStateOptional").val(data.empStateOptional);
				$("#licenseLetterOptional").val(data.licenseLetterOptional);
				if(data.districtMaster!=null && data.districtMaster.districtId==7800040)
				{	data.coverLetter=0;
					$("#expectedSalaryDiv").show();
					$(".expSalaryCss").show();
					$("#languageDiv").show();
					checkKnowLanguage();
				}
				else
				{
					$("#expectedSalaryDiv").hide();
					$(".expSalaryCss").hide();
				}
				if(data.districtMaster!=null && data.districtMaster.districtId==3700112){
					data.coverLetter=0;
					$("#expectedSalaryDiv").show();
				}
				$("#jeffcoSeachDiv").hide();
				if(data.districtMaster!=null && data.districtMaster.districtId==804800){
					$("#jeffcoSeachDiv").show();
				}
				if(data.districtMaster!=null && data.districtMaster.districtId==3703120 && data.districtMaster.districtId==3700690)
				{											
					$("#expectedSalaryDiv").show();
					if(data.districtMaster.districtId==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Licensed") !=-1)
					$(".expSalaryCss").show();
				}
				if(data.districtMaster!=null && data.districtMaster.districtId==614730 && data.jobCategoryName=="School Nurse"){
					data.tfaAffiliate=0;
				}
				
				if(data.districtMaster!=null && data.districtMaster.districtId==804800 && (data.jobCategoryName.indexOf("Hourly") !=-1 || data.jobCategoryName=="Substitute Teacher"))
				{
					data.coverLetter=0;
				}
				
				if(data.districtMaster!=null && data.districtMaster.districtId==804800 && data.jobCategoryName=="Administrator/Protech")
				{
					$("#expectedSalaryDiv").show();
				}
				$(".pritr").hide();
				$(".mscitr").hide();
				$("#crequired").css("color", "red");
				$("#crequired2").css("color", "red");
				
				if(data.districtMaster!=null && data.districtMaster.districtId==3904380){
					$("#crequired").css("color", "white");
					$("#crequired2").css("color", "white");					
					if(data.isNonTeacher==true){						
						data.additionalDocuments=false;
					}
					
					
				}
				if(data.districtMaster!=null && data.districtMaster.districtId==3628590)
				{
					data.coverLetter=0;
				}
				$('#jobcategoryDsp').val(data.jobCategoryName);
				if(data.districtMaster!=null && data.districtMaster.districtId==4218990){
					$("#languageDiv").show();
					checkKnowLanguage();
					$(".optionalCss").hide();
					$('#jobcategoryDsp').val(data.jobCategoryName);
					$(".portfolio_Subheading:contains('"+resourceJSON.msgEmploymentHistory+"')").html(resourceJSON.msgEmployment);
					$("#expectedSalaryDiv").show();
					$(".pritr").show();
					$(".mscitr").show();
				}
				if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isSchoolSupportPhiladelphia==false && data.isNonTeacher==false){
					$('#divStdTchrExp').show();											
					displayStdTchrExp();
				}
				if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isNonTeacher==true && data.coverLetter==1){
					$('#cvrltrTxt').hide();
					$('.philNT').hide();
					$('.philadelphiaNTCss').show();
					$('#isnontj').val(true);									
				
				}else if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isSchoolSupportPhiladelphia==true && data.coverLetter==1){					
					data.coverLetter=0;
					data.videoLink=false;
					data.expCertTeacherTraining=false;
					data.academicTranscript=0;
					data.nationalBoardCert=false;
					data.willingAsSubstituteTeacher=false;
					data.tfaAffiliate=false;
					data.certification=0;
					data.proofOfCertification=0;
					data.additionalDocuments=false;
					data.dateOfBirth=false;
					data.reference=2;
					data.referenceLettersOfRecommendation=0;
					//$("#StudentTeacherDiv").show();
					$('#isSchoolSupportPhiladelphia').val("1");
					$('#cvrltrTxt').hide();
					$('.philNT').hide();
					$('.philadelphiaNTCss').show();			
					$("#crequired").css("color", "white");
					$("#crequired2").css("color", "white");
					$(".philadelphiaCss:contains('"+resourceJSON.msgschooldistrictreferences+"')").html(resourceJSON.msgPhiladelphiawith3Ref);
																		
				}else{
					$('#cvrltrTxt').hide();
				}
				if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && (data.isPrinciplePhiladelphia==true || $('input:radio[name=staffType]').is(":checked") || $("#staffTypeSession").val()=="I"))//for active principal(SA) & principal jobcategory 
				{
					$("#isPrinciplePhiladelphia").val("1");
					$(".philadelphiaCss:contains('"+resourceJSON.msgschooldistrictreferences+"')").html(resourceJSON.msgPhiladelphiawith3Ref);
					data.reference=2;
					data.referenceLettersOfRecommendation=0;
					data.coverLetter=0;
					////////////////////
					data.academic=0;
					data.academicTranscript=0;
					data.employment=0;
					data.expCertTeacherTraining=0;
					data.tfaAffiliate=0;
					data.willingAsSubstituteTeacher=0;
					data.resume=0;
					$("#tfaDistSpecificoption").hide();
					data.dateOfBirth=0;
					//	data.districtSpecificPortfolioQuestions=0;
					////////////////////
					$('.philadelphiaNTCss1').show();
					$('.philadelphiaNTCss').hide();
					$('#expectedSalaryDiv').hide();
				}
				if(data.expectedSalary==1){
					$("#expectedSalaryDiv").show();
					$(".expSalaryCss").hide();
				}else if(data.expectedSalary==2){
					$("#expectedSalaryDiv").show();
					$(".expSalaryCss").show();
				}
				resetSBTNSource();
				document.getElementById("IsSIForMiami").value=data.isSubstituteInstructionalForMiami;
				document.getElementById("isItvtForMiami").value=data.isInterventionistsForMiami;
				document.getElementById("jobTitleFeild").value=data.jobTitle;
				displayGKAndSubject();
				
				if(data.districtSpecificPortfolioQuestions==0 &&  data.academic ==0 && data.academicTranscript ==0 && data.certification ==0 && data.proofOfCertification ==0 && data.reference ==0 && data.referenceLettersOfRecommendation ==0 && data.resume==0 && data.tfaAffiliate==0 && data.willingAsSubstituteTeacher==0 && data.phoneNumber==0 && data.personalinfo==0 && data.ssn==0 && data.race==0 && data.formeremployee==0 && data.honors==0 && data.involvement==0 && data.additionalDocuments==0)
					document.getElementById("txtDistrictPortfolioConfig").value=0;
				else
					document.getElementById("txtDistrictPortfolioConfig").value=data.districtPortfolioConfigId;
				
				
				var isDspqReqForKelly=document.getElementById("isDspqReqForKelly").value;	
				var isKelly=document.getElementById("isKelly").value;
				//alert("isKelly "+isKelly +" isDspqReqForKelly "+isDspqReqForKelly);
				if(isKelly=="true" && isDspqReqForKelly=="false")
				{
					getDistrictQuestionsSet(jobId);
				}
				else
				{
					if(data.academic > 0 || data.academicTranscript > 0 || data.certification > 0 || data.proofOfCertification > 0 || data.reference > 0 || data.referenceLettersOfRecommendation > 0 || data.resume==1 || data.tfaAffiliate==1 || data.willingAsSubstituteTeacher==1 || data.phoneNumber==1 || data.personalinfo==1 || data.ssn==1 || data.race==1 || data.formeremployee==1 || data.videoLink==1 || data.districtSpecificPortfolioQuestions==1 || data.honors==1 || data.involvement==1 || data.additionalDocuments==1)
					{
						//alert("try to show loadingDiv_dspq_ie from Incomplete");
						$('#loadingDiv_dspq_ie').show();
						$('#myModalDymanicPortfolio').modal('show');
						//Empty All error message div
						$('#divErrorMsg_dynamicPortfolio').empty();

						$('#errordiv_AcademicForm').empty();
						resetUniversityForm();
						setDefColortoErrorMsg_Academic();
						$('#errordiv_Certification').empty();
						hideForm_Certification();

						$('#errordivElectronicReferences').empty();
						setDefColortoErrorMsgToElectronicReferences();
						hideElectronicReferencesForm();

						$('#errorDivResume').empty();

						var countConfig_Academic=0;
						var countConfig_AcademicTranscript=0;
						var countConfig_Certification=0;
						var countConfig_ProofOfCertification=0;
						var countConfig_Reference=0;
						var countConfig_ReferenceLettersOfRecommendation=0;
						var resume_config=false;
						var tfaAffiliate_config=false;
						var willingAsSubstituteTeacher_config=false;
						var phoneNumber_config=false;
						var address_config = false;
						var exp_config=false;
						var nbc_config=false;
						var affidavit_config=false;
						
						var personalinfo_config=false;
						var dateOfBirth_config=false;
						var involvement_config=false;
						var honors_config=false;
						var ssn_config=false;
						var race_config=false;
						var formeremployee_config=false;
						var veteran_config=false;
						var ethnicOrigin_config=false;
						var ethinicity_config=false;
						var employment_config=false;
						var countConfig_Residency=0;
						var generalKnowledge_config=false;
						var subjectAreaExam_config=false;
						var additionalDocuments_config=false;
						var gender_config=false;
						var retireNo_config=false;
						
						countConfig_Academic=data.academic;
						countConfig_AcademicTranscript=data.academicTranscript;
						countConfig_Certification=data.certification;
						countConfig_ProofOfCertification=data.proofOfCertification;
						countConfig_Reference=data.reference;
						countConfig_ReferenceLettersOfRecommendation=data.referenceLettersOfRecommendation;
						resume_config=data.resume;
						tfaAffiliate_config=data.tfaAffiliate;
						willingAsSubstituteTeacher_config=data.willingAsSubstituteTeacher;
						phoneNumber_config=data.phoneNumber;
						address_config = data.address;
						exp_config = data.expCertTeacherTraining;
						nbc_config = data.nationalBoardCert;
						affidavit_config = data.affidavit;
						countConfig_Residency=data.residency;
						personalinfo_config = data.personalinfo;
						dateOfBirth_config = data.dateOfBirth;
						involvement_config=data.involvement;
						honors_config=data.honors;
						ssn_config = data.ssn;
						race_config = data.race;
						formeremployee_config = data.formeremployee;
						veteran_config = data.veteran;
						ethnicOrigin_config = data.ethnicorigin;
						ethinicity_config =  data.ethinicity;
						employment_config = data.employment;
						gender_config = data.genderId;
						
						
						generalKnowledge_config = data.generalKnowledgeExam;
						subjectAreaExam_config = data.subjectAreaExam;
						additionalDocuments_config = data.additionalDocuments;
						retireNo_config = data.retirementnumber;
						if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isNonTeacher==true){						
							videoLink_config=false;
						}else{
							videoLink_config=data.videoLink;
						}
						dSPQuestions_config = data.districtSpecificPortfolioQuestions;
						
						var IsSIForMiami=data.isSubstituteInstructionalForMiami;
						var isItvtForMiami=data.isInterventionistsForMiami;
						
						validateDynamicPortfolio(data.candidateType,countConfig_Academic,countConfig_AcademicTranscript,countConfig_Certification,countConfig_ProofOfCertification,countConfig_Reference,countConfig_ReferenceLettersOfRecommendation,resume_config,tfaAffiliate_config,willingAsSubstituteTeacher_config,phoneNumber_config,'level1',address_config,exp_config,nbc_config,affidavit_config,personalinfo_config,ssn_config,race_config,formeremployee_config,generalKnowledge_config,subjectAreaExam_config,additionalDocuments_config,veteran_config,ethnicOrigin_config,ethinicity_config,employment_config,gender_config,IsSIForMiami,isItvtForMiami,retireNo_config,videoLink_config,dSPQuestions_config,dateOfBirth_config,involvement_config,honors_config,countConfig_Residency,data);
					}
					else
					{
						getDistrictQuestionsSet(jobId);
					}
				}
				
				
			}else if(data==null && qq=="true")
			{
				getDistrictQuestionsSet(jobId);
			}
			else // Call default Rule
			{
				continueCompleteNow();
			}
			}
		});
	}
}

function continueCompleteNow()
{
	$('#loadingDiv_dspq_ie').hide();
	var prefStatus=document.getElementById("prefStatus").value;
	var portfolioStatus = document.getElementById("portfolioStatus").value;
	var inventory = document.getElementById("inventory").value;
	if($("#isPrinciplePhiladelphia").val()=="1"){
		var inventory = "done";
	}
	//var inventory = "done";
	var jobId = document.getElementById("jobId").value;
	var isaffilatedstatus=document.getElementById("isaffilatedstatushiddenId").value;
	var isJobAssessment = document.getElementById("isJobAssessment").value;
	//alert("continueCompleteNow() prefStatus : "+prefStatus+" portfolioStatus : "+portfolioStatus+" inventory : "+inventory+" jobId : "+jobId);
	resetCompleteVars();
	if(prefStatus=='false')
	{
		try{$('#redirectToPreference').modal('show');}catch(err){}
	}
	else if(portfolioStatus=='false')
	{
		try{$('#redirectToPortfolio').modal('show');}catch(err){}
		
	}else if(inventory=='epi')
	{	
		checkInventory('0',jobId);
	}else if(inventory=='jsi')
	{
		checkInventory(jobId,null);
	}else if(inventory=='done')
	{
		
		$('#redirectToDashboard').modal('show');
		getTeacherJobDone(jobId);
	}
}

function getTeacherCriteria(jobId)
{
	$('#loadingDiv_dspq_ie').show();
	if(document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nc.teachermatch.org")						
	{
		var buttonSSNClick=document.getElementById("submitSSN").value;
		if(buttonSSNClick!='ButtonClick')
		{			
			$('#normalTeacherCerti').show();
		}		
	$('#showNCDPIOnlySsn').show();	
	$("#reqReferenceAstrick").show();
	
	}
	
	
	DSPQServiceAjax.callNewSelfServiceApplicationFlow(jobId,'E',{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			if(data!='' && data!="DSPQNC" && data!="OldProcess" && data!="AlreadyCompleted")
			{
				window.location.href="./"+data;
			}
			else if(data!='' && data=="DSPQNC")
			{
				$('#DSPQNCDivNotification').modal('show');
			}
			else if(data!='' && data=="AlreadyCompleted")
			{
				$('#DSPQCompletedDivNotification').modal('show');
			}
			else
			{
				//alert("JCN Call Old Process");
				//Start ... Call Old Process
				DashboardAjax.getTeacherCriteria(jobId,{ 
					async: true,
					errorHandler:handleError,
					callback: function(data)
					{
						if(data!="")
						{
							//$('#loadingDiv_dspq_ie').hide();
							$('.nobleCssShow').hide();
							$('.newBerlinCss').hide();
							$("#crequired").css("color", "red");
							$("#crequired2").css("color", "red");
							try{
								if(data[3]=='epi')
								{
									$('#loadingDiv_dspq_ie').hide();
									try {
										$('#myModalDymanicPortfolio').modal('hide');
									} catch (e) {}
									
									checkInventory(0,jobId);
									return false;
								}
								
								document.getElementById('districtIdForDSPQ').value=data[7];
								if(data[7]!=1200390)
								{
									$('#transcriptDiv').show();
									$('#partInsEmp').hide();
								}
								else
								{
									$('#transcriptDiv').hide();
									$('#partInsEmp').show();
								}
								
								var districtIdForDSPQ="";	
								if ($('#districtIdForDSPQ').length > 0) {
									$('#districtIdForDSPQ').val(data[7]);
								}
								
								if(data[7]==7800038)
								{							
									$('.nobleCssHide').hide();
									$('.nobleCssShow').show();
									$('.#transcriptDiv').hide();
									$('#partInsEmp').show();					
								}
								
								if(data[7]==5510470)
								{
									$('.newBerlinCss').show();
								}
								
								if(data[7]==4218990){
									$(".optionalCss").hide();
									$(".philadelphiaCss").show();
									$('#expectedSalaryDiv').show();
									$(".pritr").show();
									$(".mscitr").show();						
								}
								
								if(data[7]==4218990 && data[9]=="true"){						
									$('.philNT').hide();
									$("#tfarequired").hide();
									$("#tfarequired").html("");
									$("#sSubTrequired").html("");
									$('.philadelphiaNTCss').show();
									$('#isnontj').val(true);									
								
								}else if(data[7]==4218990 && data[10]=="true"){						
									$('#isSchoolSupportPhiladelphia').val("1");						
									$('#cvrltrTxt').hide();
									$('.philNT').hide();
									$('.philadelphiaNTCss').show();
									$("#crequired").css("color", "white");
									$("#crequired2").css("color", "white");
									$(".philadelphiaCss:contains('"+resourceJSON.msgschooldistrictreferences+"')").html(resourceJSON.msgPhiladelphiawith3Ref);																					
								}
							}catch(err){}
							try{
								document.getElementById('teacherIdForDSPQ').value=data[8];
							}catch(err){}
							completeNow(data[0],data[1],data[2],data[3],data[5]);
							if(document.getElementById('isaffilatedstatushiddenId'))
							{
								try{
									document.getElementById("isDspqReqForKelly").value = data[11];	
									document.getElementById("isKelly").value=data[12];
								}catch(e){
									document.getElementById("isDspqReqForKelly").value = "false";	
									document.getElementById("isKelly").value= "false";
								}
								document.getElementById('isaffilatedstatushiddenId').value=data[4];
								$('#myModalLabelDynamicPortfolio').html(resourceJSON.msgRequiredApplicationItemsfor+data[6]);
								$('#displayDistrictNameForExt1').html(data[6]);
								$('#displayDistrictNameForExt2').html(data[6]);
								$('#displayDistrictNameForExt3').html(data[6]);
								
								try{$('#myModalCL').modal('hide');}catch(e){}
							}
							else{
								try{
									document.getElementById("isDspqReqForKelly").value = data[11];	
									document.getElementById("isKelly").value=data[12];
								}catch(e){
									document.getElementById("isDspqReqForKelly").value = "false";	
									document.getElementById("isKelly").value= "false";
								}
							}
							callForMiamiDistrict();
						}
					}
				});	
				
				//End ... Call Old Process
			}
			
		}});
	
	
}
///// applyjob change //////////////////
function getTeacherCriteriaStatus(jobId)
{
	DashboardAjax.getTeacherCriteria(jobId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!="")
			{
				try{
					document.getElementById('districtIdForDSPQ').value=data[7];
				}catch(err){}
				try{
					document.getElementById('teacherIdForDSPQ').value=data[8];
				}catch(err){}
				completeNow(data[0],data[1],data[2],data[3],data[5]);
				if(document.getElementById('isaffilatedstatushiddenId'))
				{
					document.getElementById('isaffilatedstatushiddenId').value=data[4];
					$('#myModalLabelDynamicPortfolio').html(resourceJSON.msgRequiredApplicationItemsfor+data[6]);
					try{$('#myModalCL').modal('hide');}catch(e){}
				}
			}
		}
	});	
}

function getTeacherJobDone(jobId)
{
	DashboardAjax.getTeacherJobDone(jobId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null)
			{
				try{
					var mf=data[2];
					if(mf!=null && mf=='y'){
						if(data[0]!=null && data[0]!="")
						{
							$('#redirectToDashboard').modal('hide');
							var url=data[0];
							if(url.indexOf("http")==-1)
							url="http://"+url;
							
							var showurl="";
							if(url.length>70)
							{
							  for(var i=0;i<url.length;i=i+70)
							  {
								  showurl=showurl+"\n"+url.substring(i, i+70)
							  } 
							}
							
							
							var sts = "0";
						
							
							var divMsg=resourceJSON.msgThankforcompleting;
							if(mf=='n')
							{
								divMsg=resourceJSON.msghavenotcompleted;
								sts = "2";
							}
					
							divMsg=divMsg+" "+resourceJSON.msgteachermatchpart+" <a href='"+url+"' target='_blank'>"+showurl+"</a>"+resourceJSON.msgcurrentbrowserclose;
					
							showInfoAndRedirect(sts,divMsg,'',url,2);
						}else{
							$('#redirectToDashboard').modal('hide');
						    window.location.href='thankyoumessage.do?jobId='+jobId;
						}
					}else
					{
						getTeacherCriteriaExternal(jobId);
					}
				}catch(err){}
			}
		}
	});	
}

function getTeacherCriteriaExternal(jobId)
{
	DashboardAjax.getTeacherCriteria(jobId,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!="")
			{
				try{
					document.getElementById('districtIdForDSPQ').value=data[7];
				}catch(err){}
				try{
					document.getElementById('teacherIdForDSPQ').value=data[8];
				}catch(err){}
				document.getElementById("prefStatus").value=data[0];
				document.getElementById("portfolioStatus").value=data[1];
				document.getElementById("jobId").value=data[2];
				document.getElementById("inventory").value=data[3];
				document.getElementById("savejobFlag").value=1;
				document.getElementById("completeNow").value=1;
				document.getElementById("isJobAssessment").value=data[5];
				
				if(document.getElementById('isaffilatedstatushiddenId'))
				{
					document.getElementById('isaffilatedstatushiddenId').value=data[4];
					$('#myModalLabelDynamicPortfolio').html(resourceJSON.msgRequiredApplicationItemsfor+" "+data[6]);
					$('#displayDistrictNameForExt1').html(data[6]);
					$('#displayDistrictNameForExt2').html(data[6]);
					$('#displayDistrictNameForExt3').html(data[6]);
					
					try{$('#myModalCL').modal('hide');}catch(e){}
				}
				
				continueCompleteNow();
				try{
					if(document.getElementById("dashbd"))
						{
							//alert("completeJJJ");
							//alert("updateFlag: "+document.getElementById("updateFlag").value);
							//alert("isAffilatedValue:: "+$('#isAffilatedValue').val());
							if(document.getElementById("updateFlag").value=="y")
							{
								AssessmentCampaignAjax.resetjftIsAffilated(jobId,$('#isAffilatedValue').val(),{ 
									async: false,
									errorHandler:handleError,
									callback: function(data)
									{
										document.getElementById("updateFlag").value==""
										getDashboardJobsofIntrest();
									}
								});
							}else
								getDashboardJobsofIntrest();
						}
				}catch(err){}
				
			}
		}
	});	
}
function checkAffiBeforeaAplyJob(savejobFlag)
{
	//alert('enter ---------->>'+$('[name="dob"]').val()+"  >>"+$('[name="last4SSN"]').val());
	if(messageShowOrNot($('[name="dob"]').val(),$('[name="last4SSN"]').val())){
		return false;
	}
	//alert('enter----');
	var jobId=document.getElementById("jobId").value;
	var ok_cancelflag=document.getElementById("ok_cancelflag").value;
	
	//alert("ok_cancelflag:: "+ok_cancelflag);
	$('#loadingDiv_dspq_ie').show();
	var isAffilated = ok_cancelflag;
	AssessmentCampaignAjax.checkIsAffilated(jobId,isAffilated,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data){
		$('#loadingDiv_dspq_ie').hide();
		if(data!=0 && ( (data==1 && isAffilated==0) ||(data==1 && isAffilated==1)) )
		{
			var appender = "<br/><br/>"+resourceJSON.msgyesupdateyourstatus;
			appender+="<br/><br/>"+resourceJSON.msgnocurrentupdatestatus;
			appender+="<br/><br/>"+resourceJSON.msgcancelbackstart;
			if(isAffilated==1) // Teacher has checked isAffilated check curent job and latest previous job's isAffilated status 0 means NOT checked 
			{
				document.getElementById("divAlertText").innerHTML=resourceJSON.msgdeclearyourself+" "+appender;			
			}
			else
			{
				if(isAffilated==0)// Teacher has NOT checked isAffilated check curent job and latest previous job's isAffilated status 1 means Checked.
					document.getElementById("divAlertText").innerHTML=resourceJSON.msgrectifydeclearyourself+" "+appender;
			}
			$('#divAlert').modal('show');
			document.getElementById("ok_cancelflag").value=isAffilated;
			
			$('#isresetStatus').val(1);
			$('#isAffilatedValue').val(isAffilated);
		}
		else
		{
			callDynamicPortfolio_externalJobApply(savejobFlag);
		}
	}
	});	
	
}

function resetAffilatedAndContinue(savejobFlag)
{
	$('#loadingDiv_dspq_ie').show();
	document.getElementById("updateFlag").value="y";
	$('#divAlert').modal('hide');
	callDynamicPortfolio_externalJobApply(savejobFlag);
}
function doNotResetAffilatedAndContinue(savejobFlag)
{
	$('#loadingDiv_dspq_ie').show();
	document.getElementById("updateFlag").value="n";
	$('#divAlert').modal('hide');
	callDynamicPortfolio_externalJobApply(savejobFlag);
}

// mukesh.........
function checkInvitation()
{
	//alert("call")
	$('#loadingDivInventory').fadeIn();
	var jobId=document.getElementById("jobId").value;
	//alert("jobId :: "+jobId);
	
	StateAjax.checkcandidateInvitation(jobId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			
		//alert("data"+data)
		 if(data=="0")
		 {
			 $('#checkInvitationDiv').modal('show');
			 
			 document.getElementById("InvitationErrMsgDiv").innerHTML=resourceJSON.msgdistricthumanresoursedevelopment;
		 }
		}
	});
	$('#loadingDivInventory').hide();	
}

function redirectnonInvitCandURl()
{
	window.location.href="userdashboard.do";	
}

