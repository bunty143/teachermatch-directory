package tm.controller.teacher;

import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.InternalTransferCandidates;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAnswerDetail;
import tm.bean.TeacherAssessmentAttempt;
import tm.bean.TeacherAssessmentdetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStrikeLog;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.districtassessment.DistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentAnswerDetail;
import tm.bean.districtassessment.TeacherDistrictAssessmentAttempt;
import tm.bean.districtassessment.TeacherDistrictAssessmentDetail;
import tm.bean.districtassessment.TeacherDistrictSectionDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.user.UserMaster;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherAssessmentAttemptDAO;
import tm.dao.TeacherAssessmentDetailDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStrikeLogDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentAttemptDAO;
import tm.dao.districtassessment.TeacherDistrictAssessmentDetailDAO;
import tm.dao.districtassessment.TeacherDistrictSectionDetailDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.utility.Utility;

/*
 * Manage Teacher related url
 * setting, changepassword,
 */
@Controller
public class TMController 
{
	String locale = Utility.getValueOfPropByKey("locale");
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(TeacherPersonalInfoDAO teacherPersonalInfoDAO) 
	{
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;

	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	public void setAssessmentDetailDAO(AssessmentDetailDAO assessmentDetailDAO) {
		this.assessmentDetailDAO = assessmentDetailDAO;
	}

	@Autowired
	private TeacherAssessmentDetailDAO teacherAssessmentDetailDAO;
	public void setTeacherAssessmentDetailDAO(TeacherAssessmentDetailDAO teacherAssessmentDetailDAO) {
		this.teacherAssessmentDetailDAO = teacherAssessmentDetailDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherAssessmentAttemptDAO assessmentAttemptDAO;
	public void setAssessmentAttemptDAO(TeacherAssessmentAttemptDAO assessmentAttemptDAO) {
		this.assessmentAttemptDAO = assessmentAttemptDAO;
	}
	
	@Autowired
	private TeacherStrikeLogDAO teacherStrikeLogDAO;
	public void setTeacherStrikeLogDAO(TeacherStrikeLogDAO teacherStrikeLogDAO) {
		this.teacherStrikeLogDAO = teacherStrikeLogDAO;
	}
	
	@Autowired
	private TeacherDistrictAssessmentAttemptDAO teacherDistrictAssessmentAttemptDAO;
	
	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	@Autowired
	private TeacherDistrictAssessmentDetailDAO teacherDistrictAssessmentDetailDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherDistrictSectionDetailDAO teacherDistrictSectionDetailDAO;
	
	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	
	@RequestMapping(value="/settings.do", method=RequestMethod.GET)
	public String doPersonalInfoGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		map.addAttribute("teacherDetail", teacherDetail);

		
		return "settings";
	}

	@RequestMapping(value="/settings.do", method=RequestMethod.POST)
	//public String doPersonalInfoPOST(@ModelAttribute(value="teacherDetail") TeacherDetail teacherDetail,BindingResult result,ModelMap map,HttpServletRequest request)
	public String doPersonalInfoPOST(@ModelAttribute(value="teacherDetail") TeacherDetail teacherDetail,BindingResult result,ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) {
			return "redirect:index.jsp";
		}

		try 
		{
			boolean sendMailEmailChange = false;
			
			String oldEmail=null;
			TeacherDetail tDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			
			TeacherPersonalInfo teacherPersonalInfo		 =	teacherPersonalInfoDAO.findById(tDetail.getTeacherId(), false, false);

			oldEmail=tDetail.getEmailAddress();
			if(!teacherDetail.getEmailAddress().trim().equalsIgnoreCase(tDetail.getEmailAddress().trim())) {
				sendMailEmailChange=true;
			}

			tDetail.setFirstName(teacherDetail.getFirstName());
			tDetail.setLastName(teacherDetail.getLastName());
			tDetail.setEmailAddress(teacherDetail.getEmailAddress());
			if(teacherPersonalInfo!=null){
				teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
				teacherPersonalInfo.setLastName(teacherDetail.getLastName());
			}else{
				teacherPersonalInfo=new TeacherPersonalInfo();
				teacherPersonalInfo.setTeacherId(tDetail.getTeacherId());
				teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
				teacherPersonalInfo.setLastName(teacherDetail.getLastName());
				teacherPersonalInfo.setIsDone(false);
				teacherPersonalInfo.setCreatedDateTime(new Date());
			}
			
			TeacherDetail teacherDetailPic=teacherDetailDAO.findById(tDetail.getTeacherId(), false,false);
			if(teacherDetailPic.getIdentityVerificationPicture()!=null)
				tDetail.setIdentityVerificationPicture(teacherDetailPic.getIdentityVerificationPicture());
				
				teacherDetailDAO.makePersistent(tDetail);
				teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
			

			if(sendMailEmailChange) {
				emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Your TeacherMatch email has been changed",MailText.getSettingEmailChangeMailText(request,teacherDetail,oldEmail));
				emailerService.sendMailAsHTMLText(oldEmail, "Your TeacherMatch email has been changed",MailText.getSettingEmailChangeMailText(request,teacherDetail,oldEmail));
			}
			
		}		
		catch (Exception e) {
			e.printStackTrace();
		}



		return "redirect:userdashboard.do";
	}
	@RequestMapping(value="/changepwd.do", method=RequestMethod.POST)
	public String changePWD(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		try 
		{
			HttpSession session = request.getSession();
			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{	
				out.write("100001");
				return null;
			}
			
			response.setContentType("text/html");
			String password = request.getParameter("password");
			
			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			teacherDetail.setPassword(MD5Encryption.toMD5(password));
			teacherDetailDAO.makePersistent(teacherDetail);
			emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Your TeacherMatch password has been changed",MailText.getSettingPWDChangeMailText(request,teacherDetail));
			
			out.write("");
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	

	@RequestMapping(value="/chksettingemailexist.do", method=RequestMethod.POST)
	public String isEmailExist(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		try 
		{
			HttpSession session = request.getSession(false);

			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{	
				out.write("100001");
				return null;
			}


			
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");

			
			String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
			
			List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
			List<UserMaster> lstUsermaMasters= userMasterDAO.findByEmail(emailAddress);


			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();


			if((  (lstTeacherDetails!=null && lstTeacherDetails.size()>0) || (lstUsermaMasters!=null && lstUsermaMasters.size()>0)   )&& !emailAddress.equals(teacherDetail.getEmailAddress().trim()))
			{
				pw.println("1");
			}
			else
			{
				pw.println("0");
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	

	@RequestMapping(value="/chkpwdexist.do", method=RequestMethod.POST)
	public String isPasswordExistPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		try 
		{
			HttpSession session = request.getSession();
			PrintWriter out = response.getWriter();
			if (session == null || session.getAttribute("teacherDetail") == null) 
			{	
				out.write("100001");
				return null;
			}

			String encryptedPWD=null;			
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");			
			String password = request.getParameter("password")==null?"":request.getParameter("password").trim();
			encryptedPWD=MD5Encryption.toMD5(password.trim());
			response.setContentType("text/html");
			PrintWriter pw = response.getWriter();
			if(teacherDetail.getPassword().equals(encryptedPWD))
			{
				pw.println("1");
			}
			else
			{
				pw.println("0");
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to run assessment.
	 */
	@RequestMapping(value="/runAssessment.do", method=RequestMethod.GET)
	public String runAssessment(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		
		map.addAttribute("teacherDetail", teacherDetail);
		
		String assessmentId = request.getParameter("assessmentId");
		String jobId = request.getParameter("jobId");
		String newJobId = request.getParameter("newJobId");
		AssessmentDetail assessmentDetail = null;
		TeacherAssessmentdetail teacherAssessmentdetail = null;
		int remainingSessionTime = 0;
		JobOrder jobOrder = null;
		int attemptId = 0;
		int totalAttempted = 0;
		int assessmentType = 1;
		try 
		{
			if(assessmentId!=null && assessmentId.trim().length()>0)
			{
				assessmentDetail = assessmentDetailDAO.findById(Integer.parseInt(assessmentId), false, false);
				List<TeacherAssessmentdetail> teacherAssessmentdetails = null;
				assessmentType = assessmentDetail.getAssessmentType();
				teacherAssessmentdetails=teacherAssessmentDetailDAO.getTeacherAssessmentdetails(assessmentDetail, teacherDetail);
				if(teacherAssessmentdetails.size()!=0)
					teacherAssessmentdetail = teacherAssessmentdetails.get(0);
				
				int totalAssessmentSessionTime = teacherAssessmentdetail.getAssessmentSessionTime()==null?0:teacherAssessmentdetail.getAssessmentSessionTime();
				int consumedSessionTime = 0;
				TeacherAssessmentAttempt teacherAssessmentAttempt = null;
				List<TeacherAssessmentAttempt> teacherAssessmentAttempts = assessmentAttemptDAO.findNoOfAttempts(assessmentDetail, teacherDetail, teacherAssessmentdetail.getAssessmentTakenCount());
				
				if (!teacherAssessmentAttempts.isEmpty()) {
					{
						for (TeacherAssessmentAttempt teacherAssessmentAttempt1 : teacherAssessmentAttempts) {
							consumedSessionTime+=teacherAssessmentAttempt1.getAssessmentSessionTime();
							
							if(!teacherAssessmentAttempt1.getIsForced())
								totalAttempted++;
							
							//totalAttempted++;
						}
						
						teacherAssessmentAttempt = teacherAssessmentAttempts.get(teacherAssessmentAttempts.size()-1);
						attemptId = teacherAssessmentAttempt.getAttemptId();
						
						/// new changes /////////////
						/*if(teacherAssessmentAttempt.getIsForced())
							totalAttempted = totalAttempted - 1;*/
						///////////////////////////
						
					}
				}
				
				remainingSessionTime = totalAssessmentSessionTime-consumedSessionTime;
				
			}
			if(jobId!=null && jobId.trim().length()>0)
			{
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
			}


			List<TeacherAnswerDetail> teacherAnswerDetails = null;
			/*teacherAnswerDetails = teacherAnswerDetailDAO.findNoOfSkippedQuestion(assessmentDetail, teacherDetail, teacherAssessmentdetail);
			System.out.println(teacherAnswerDetails.size());*/
			
			List<TeacherStrikeLog> teacherStrikeLogList= null;
			teacherStrikeLogList = teacherStrikeLogDAO.getTotalStrikesByTeacher(teacherDetail, teacherAssessmentdetail);
			//System.out.println("teacherStrikeLogList.size()::::::::::::::: "+teacherAssessmentdetail.getTeacherAssessmentId());
			//System.out.println("teacherStrikeLogList.size()::::::::::::::: "+teacherDetail.getTeacherId());

			map.addAttribute("assessmentDetail", assessmentDetail);	
			map.addAttribute("teacherAssessmentdetail", teacherAssessmentdetail);
			map.addAttribute("jobOrder", jobOrder);
			map.addAttribute("remainingSessionTime", remainingSessionTime);
			map.addAttribute("attemptId", attemptId);
			map.addAttribute("totalAttempted",totalAttempted);
			//map.addAttribute("totalSkippedQuestions", teacherAnswerDetails.size());
			map.addAttribute("totalSkippedQuestions", 0);
			map.addAttribute("newJobId", newJobId);
			map.addAttribute("isCompleted", true);
			int teacherStrikeLogListSize=0;
			if(teacherStrikeLogList!=null){
				teacherStrikeLogListSize=teacherStrikeLogList.size();
			}
			map.addAttribute("totalStrikes",teacherStrikeLogListSize);
			String rURL = "userdashboard.do";
			boolean epiStandalone = teacherDetail.getIsPortfolioNeeded();
			System.out.println("epiStandalone:::: "+epiStandalone);
			if(!epiStandalone)
				rURL = "portaldashboard.do";
			
			if(assessmentType==1)
			{
				List<JobForTeacher> lstJobForTeacher = jobForTeacherDAO.findMostRecentJobByTeacher(teacherDetail);
				if(lstJobForTeacher.size()>0)
				{
					boolean onDashboard = false;
					jobOrder = lstJobForTeacher.get(0).getJobId();
					int flagForURL = jobOrder.getFlagForURL();
					int flagForMessage  = jobOrder.getFlagForMessage();
					String exitMessage = jobOrder.getExitMessage();
					String exitURL = jobOrder.getExitURL();
					
					if(exitMessage!=null && !exitMessage.trim().equals("") && exitURL!=null && !exitURL.trim().equals(""))
					{
						onDashboard = true;
					}else if(flagForMessage==1)
					{
						onDashboard = false;
						rURL = "thankyoumessage.do?jobId="+jobOrder.getJobId();
						rURL=getURL(teacherDetail, jobOrder,rURL);
						//sb.append("window.location.href='thankyoumessage.do?jobId="+jobOrder.getJobId()+"'");

					}else if(flagForURL==1)
					{
						if(!epiStandalone)
							rURL = "portaldashboard.do?jobId="+jobOrder.getJobId()+"&es=1";
							//rURL=exitURL;
						else
						{
							rURL = "userdashboard.do?jobId="+jobOrder.getJobId()+"&es=1";;
							rURL=getURL(teacherDetail, jobOrder,rURL);
						}
							//rURL=exitURL;
						
					}
					
					/*if(rURL.indexOf("http")==-1)
						rURL="http://"+rURL;*/
				}
			}
			System.out.println("rURL::: "+rURL);
			map.addAttribute("rURL",rURL);
			
			map.addAttribute("EPITeacherBaseInventory1",Utility.getLocaleValuePropByKey("EPITeacherBaseInventory", locale));
	        
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "runAssessment";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to redirect to thankyou page.
	 */
	@RequestMapping(value="/thankyoumessage.do", method=RequestMethod.GET)
	public String thankYouMsgGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		map.addAttribute("teacherDetail", teacherDetail);
		String jobId = request.getParameter("jobId");
		JobOrder jobOrder  = null;

		try 
		{
			if(jobId!=null && jobId.trim().length()>0)
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null &&  jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId().equals(2)){
					JobForTeacher jft=jobForTeacherDAO.getJFTByTeacherIdAndJobId(teacherDetail.getTeacherId(), jobOrder.getJobId());
					if(jft!=null && jft.getjFTWiseDetails()==null || (jft.getjFTWiseDetails()!=null && (jft.getjFTWiseDetails().getAffidavit()==null || !jft.getjFTWiseDetails().getAffidavit())))
						return BasicController.getAffidavitReturnUrl(map, statusMasterDAO, jobForTeacherDAO, jft.getJobId().getJobId(), jft.getTeacherId().getTeacherId());

				}

			map.addAttribute("jobOrder", jobOrder);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "thankyoumessage";
	}
	
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to run assessment.
	 */
	@RequestMapping(value="/runDistrictAssessment.do", method=RequestMethod.GET)
	public String runDistrictAssessment(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		map.addAttribute("teacherDetail", teacherDetail);
		/*TeacherDetail teacherDetail = teacherDetailDAO.findById(Integer.parseInt(request.getParameter("td")), false, false);
		map.addAttribute("teacherDetail", teacherDetail);*/
		
		String distAssessmentId = request.getParameter("distAssessmentId");
		String jobId = request.getParameter("jobId");
		String newJobId = request.getParameter("newJobId");
		DistrictAssessmentDetail districtAssessmentDetail = null;
		TeacherDistrictAssessmentDetail teacherDistrictAssessmentdetail = null;
		int remainingSessionTime = 0;
		JobOrder jobOrder = null;
		int attemptId = 0;
		int totalAttempted = 0;
		try 
		{
			if(distAssessmentId!=null && distAssessmentId.trim().length()>0)
			{
				districtAssessmentDetail = districtAssessmentDetailDAO.findById(Integer.parseInt(distAssessmentId), false, false);
				List<TeacherDistrictAssessmentDetail> teacherDistrictAssessmentdetails = null;
				teacherDistrictAssessmentdetails=teacherDistrictAssessmentDetailDAO.getTeacherDistrictAssessmentdetails(districtAssessmentDetail, teacherDetail);
				if(teacherDistrictAssessmentdetails.size()!=0)
					teacherDistrictAssessmentdetail = teacherDistrictAssessmentdetails.get(0);
				
				int totalAssessmentSessionTime = teacherDistrictAssessmentdetail.getAssessmentSessionTime()==null?0:teacherDistrictAssessmentdetail.getAssessmentSessionTime();
				int consumedSessionTime = 0;
				TeacherDistrictAssessmentAttempt teacherDistrictAssessmentAttempt = null;
				List<TeacherDistrictAssessmentAttempt> teacherDistrictAssessmentAttempts = teacherDistrictAssessmentAttemptDAO.findNoOfAttempts(districtAssessmentDetail, teacherDetail);
				
				if (!teacherDistrictAssessmentAttempts.isEmpty()) {
					{
						for (TeacherDistrictAssessmentAttempt teacherAssessmentAttempt1 : teacherDistrictAssessmentAttempts) {
							consumedSessionTime+=teacherAssessmentAttempt1.getDistrictAssessmentSessionTime();
							
							if(!teacherAssessmentAttempt1.getIsForced())
								totalAttempted++;
							
						}
						
						teacherDistrictAssessmentAttempt = teacherDistrictAssessmentAttempts.get(teacherDistrictAssessmentAttempts.size()-1);
						attemptId = teacherDistrictAssessmentAttempt.getAttemptId();
						
						/// new changes /////////////
						/*if(teacherAssessmentAttempt.getIsForced())
							totalAttempted = totalAttempted - 1;*/
						///////////////////////////
						
					}
				}
				
				remainingSessionTime = totalAssessmentSessionTime-consumedSessionTime;
				
			}
			if(jobId!=null && jobId.trim().length()>0)
			{
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
			}


			List<TeacherDistrictAssessmentAnswerDetail> districtAssessmentAnswerDetails = null;
			/*teacherAnswerDetails = teacherAnswerDetailDAO.findNoOfSkippedQuestion(assessmentDetail, teacherDetail, teacherAssessmentdetail);
			System.out.println(teacherAnswerDetails.size());*/
			
		
			//System.out.println("teacherStrikeLogList.size()::::::::::::::: "+teacherAssessmentdetail.getTeacherAssessmentId());
			//System.out.println("teacherStrikeLogList.size()::::::::::::::: "+teacherDetail.getTeacherId());
			List<TeacherDistrictSectionDetail> teacherDistrictSectionDetailList = null;

            if(teacherDistrictAssessmentdetail!=null)

                  teacherDistrictSectionDetailList = teacherDistrictSectionDetailDAO.getSectionsByTeacherDistrictAssessmentId(teacherDistrictAssessmentdetail);

            String collapseIds="";


            if(teacherDistrictSectionDetailList.size()>0)
            {
                  for(int i=0;i<teacherDistrictSectionDetailList.size();i++)
                  {
                        if(collapseIds.trim()=="")
                              collapseIds+="#id"+i;
                        else if(collapseIds.trim()!="")
                              collapseIds+=","+"#id"+i;
                  }
            }

            map.addAttribute("collapseIds", collapseIds);
			map.addAttribute("districtAssessmentDetail", districtAssessmentDetail);	
			map.addAttribute("teacherDistrictAssessmentDetail", teacherDistrictAssessmentdetail);
			map.addAttribute("jobOrder", jobOrder);
			map.addAttribute("remainingSessionTime", remainingSessionTime);
			map.addAttribute("attemptId", attemptId);
			map.addAttribute("totalAttempted",totalAttempted);
			//map.addAttribute("totalSkippedQuestions", teacherAnswerDetails.size());
			map.addAttribute("totalSkippedQuestions", 0);
			map.addAttribute("newJobId", newJobId);
			map.addAttribute("isCompleted", true);
			int teacherStrikeLogListSize=0;
			/*if(teacherStrikeLogList!=null){
				teacherStrikeLogListSize=teacherStrikeLogList.size();
			}*/
			map.addAttribute("totalStrikes",teacherStrikeLogListSize);
			String rURL = "userdashboard.do";
			boolean epiStandalone = teacherDetail.getIsPortfolioNeeded();
			System.out.println("epiStandalone:::: "+epiStandalone);
			if(!epiStandalone)
				rURL = "portaldashboard.do";
			
			
			System.out.println("rURL::: "+rURL);
			map.addAttribute("rURL",rURL);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "runDistrictAssessment";
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to redirect to thankyou page.
	 */
	@RequestMapping(value="/thank-you.do", method=RequestMethod.GET)
	public String thankYouMsgDistrictGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("teacherDetail") == null) 
		{
			return "redirect:index.jsp";
		}

		TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
		map.addAttribute("teacherDetail", teacherDetail);
		String jobId = request.getParameter("jobId");
		JobOrder jobOrder  = null;

		try 
		{
			if(jobId!=null && jobId.trim().length()>0)
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);

			map.addAttribute("jobOrder", jobOrder);	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "thankyoumessage";
	}
	
	
	public String getURL(TeacherDetail teacherDetail, JobOrder jobOrder,String rURL)
	{
		
		int IsAffilated=0;
		JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
		DistrictMaster districtMaster=jobOrder.getDistrictMaster();
		
		List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
		boolean internalFlag=false;
		boolean offerJSI =false;
		if(itcList.size()>0)
			internalFlag=true;
		
		if(jobOrder.getDistrictMaster()!=null && internalFlag)
		{
			districtMaster = jobOrder.getDistrictMaster();
			if(districtMaster.getOfferJSI())
				offerJSI=true;
		}
		try{
			
			 if(jobForTeacher!=null && jobForTeacher.getIsAffilated()!=null)
			 {
				 if(jobForTeacher.getIsAffilated()==1)
					 IsAffilated=1;
			 }
		    if(jobOrder.getJobCategoryMaster()!=null && IsAffilated==1 && internalFlag==false)
		    {
				if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0)
				{
					offerJSI=true;
				}
				else
				{
					offerJSI=false;
				}
			 }
		}catch(Exception e){
			e.printStackTrace();
		}
		if(jobOrder.getJobAssessmentStatus()==1 && ((offerJSI && internalFlag )||(internalFlag==false && IsAffilated==0) ||(internalFlag==false && IsAffilated==1 && offerJSI)))
			if(jobOrder.getIsJobAssessment()!=null &&  jobOrder.getIsJobAssessment()==true)
				if((jobForTeacher!=null) && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp"))
					rURL = "applyjob.do?jobId="+jobOrder.getJobId();
		
		return rURL;
	}
}
