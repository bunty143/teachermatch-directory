package tm.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolKeyContact;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.report.CGInviteInterviewAjax;
import tm.services.report.CandidateGridService;
import tm.utility.StringKey;
import tm.utility.Utility;

public class CommonService {

	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	
	@Autowired 
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private TeacherStatusScoresDAO teacherStatusScoresDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	CGInviteInterviewAjax cGInviteInterviewAjax;
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	/* @Author: Gagan 
	 * @Discription: It is used to get Hr information for Users that will used in Forgot Password email footer.
	 */
	@Transactional()
	public String[] getHrDetailToTeacher(HttpServletRequest request,JobOrder jobOrder)
	{
		System.out.println("\n Method getHrDetail in CommonService ");
		//HttpSession session = request.getSession(false);
		String schoolDistrictName =	"";
		int isHrContactExist = 0;
		String hrContactFirstName = "";
		String hrContactLastName = "";
		String hrContactEmailAddress = "";
		String title 			 = "";
		String phoneNumber		 = "";
		String dmName 			 = "";
		String dmEmailAddress = "";
		String dmPhoneNumber	 = "";
	
		String jobordertype		="";
		String joborderwithSingleSchoolAttachFlag ="";
		String attachSchoolName	="";
		String districtNameforSJO		="";
		String[] arrHrDetail	= new String[20]; 
		
		List<DistrictKeyContact> dkclist = new ArrayList<DistrictKeyContact>();
		List<SchoolKeyContact> skclist = new ArrayList<SchoolKeyContact>();
		
		ContactTypeMaster ctm 	= new ContactTypeMaster();
		
		try{
			ctm.setContactTypeId(1);
			Criterion criterion1 = Restrictions.eq("keyContactTypeId", ctm);
			Criterion criterion2 = null;
			
			jobordertype=""+jobOrder.getCreatedForEntity();
			if(jobOrder.getCreatedForEntity()==2 && jobOrder.getSchool().size()==1)
			{
				joborderwithSingleSchoolAttachFlag	="1";
				if(jobOrder.getSchool().get(0).getDisplayName()!=null && (!jobOrder.getSchool().get(0).getDisplayName().trim().equals("")))
					attachSchoolName	=	jobOrder.getSchool().get(0).getDisplayName();
				else
					attachSchoolName	=	jobOrder.getSchool().get(0).getSchoolName()==null?"":jobOrder.getSchool().get(0).getSchoolName();
			}
			else
			{
				/* ==== if job order is SJO in this case */
				if(jobOrder.getCreatedForEntity()==3)
				{
					if(jobOrder.getDistrictMaster().getDisplayName()!=null && (!jobOrder.getDistrictMaster().getDisplayName().trim().equals("")))
						districtNameforSJO	=	jobOrder.getDistrictMaster().getDisplayName();
					else
						districtNameforSJO		=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
				}
				joborderwithSingleSchoolAttachFlag	="0";
			}
			
			if(jobOrder.getCreatedForEntity()==2)
			{
				if(jobOrder.getDistrictMaster().getDisplayName()!=null && (!jobOrder.getDistrictMaster().getDisplayName().trim().equals("")))
					schoolDistrictName	=	jobOrder.getDistrictMaster().getDisplayName();
				else
					schoolDistrictName		=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
				
				criterion2				=	Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				dkclist					=	districtKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);
				
				if(dkclist.size()>0)
				{
					isHrContactExist = 1;
					if(dkclist.get(0).getKeyContactFirstName()!=null)
						hrContactFirstName = dkclist.get(0).getKeyContactFirstName();
					
					if(dkclist.get(0).getKeyContactLastName()!=null)
						hrContactLastName = dkclist.get(0).getKeyContactLastName();
					
					if(dkclist.get(0).getKeyContactEmailAddress()!=null)
						hrContactEmailAddress = dkclist.get(0).getKeyContactEmailAddress();
					
					if(dkclist.get(0).getKeyContactPhoneNumber()!=null)
						phoneNumber	= dkclist.get(0).getKeyContactPhoneNumber();
					
					if(dkclist.get(0).getKeyContactTitle()!=null)
						title = dkclist.get(0).getKeyContactTitle();
						
				}
				else
				{
					System.out.println(" [ CommonService ] DA Final Decision Maker Information [ If Hr is not Available ]");
					if(jobOrder.getDistrictMaster().getDmName()!=null)
					{
						isHrContactExist=2;
						dmName	=	jobOrder.getDistrictMaster().getDmName();
						
						if(jobOrder.getDistrictMaster().getDmEmailAddress()!=null)
							dmEmailAddress	=	jobOrder.getDistrictMaster().getDmEmailAddress();
						
						if(jobOrder.getDistrictMaster().getDmPhoneNumber()!=null)
							dmPhoneNumber	=	jobOrder.getDistrictMaster().getDmPhoneNumber();
					}
				}
			}
			else
			{
				if(jobOrder.getCreatedForEntity()==3)
				{
					if(jobOrder.getSchool().get(0).getDisplayName()!=null && (!jobOrder.getSchool().get(0).getDisplayName().trim().equals("")))
						schoolDistrictName	=	jobOrder.getSchool().get(0).getDisplayName();
					else
						schoolDistrictName	=	jobOrder.getSchool().get(0).getSchoolName()==null?"":jobOrder.getSchool().get(0).getSchoolName();
					
					criterion2				=	Restrictions.eq("schoolId", jobOrder.getSchool().get(0).getSchoolId());
					skclist					=	schoolKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);
					
					if(skclist.size()>0)
					{
						isHrContactExist = 1;
						if(skclist.get(0).getKeyContactFirstName()!=null)
							hrContactFirstName = skclist.get(0).getKeyContactFirstName();
						
						if(skclist.get(0).getKeyContactLastName()!=null)
							hrContactLastName = skclist.get(0).getKeyContactLastName();
						
						if(skclist.get(0).getKeyContactEmailAddress()!=null)
							hrContactEmailAddress = skclist.get(0).getKeyContactEmailAddress();
						
						if(skclist.get(0).getKeyContactPhoneNumber()!=null)
							phoneNumber	= skclist.get(0).getKeyContactPhoneNumber();
						
						if(skclist.get(0).getKeyContactTitle()!=null)
							title = skclist.get(0).getKeyContactTitle();
					}
				}
				else
				{
					System.out.println(" [CommonService  SA Final Decision Maker Information [ If Hr is not Available ]");
					if(jobOrder.getSchool()!=null && jobOrder.getSchool().size()>0 && jobOrder.getSchool().get(0).getDmName()!=null)
					{
						isHrContactExist=2;
						dmName	=	jobOrder.getSchool().get(0).getDmName();
						
						if(jobOrder.getSchool().get(0).getDmEmailAddress()!=null)
							dmEmailAddress	=	jobOrder.getSchool().get(0).getDmEmailAddress();
						
						if(jobOrder.getSchool().get(0).getDmPhoneNumber()!=null)
							dmPhoneNumber	=	jobOrder.getSchool().get(0).getDmPhoneNumber();
					}
				}
			}
			
			arrHrDetail[0]	= hrContactFirstName;
			arrHrDetail[1]	= hrContactLastName;
			arrHrDetail[2]	= hrContactEmailAddress;	
			arrHrDetail[3]	= phoneNumber;
			arrHrDetail[4]	= title;
			arrHrDetail[5]	= ""+isHrContactExist;
			arrHrDetail[6]	= schoolDistrictName;
			arrHrDetail[7]	= dmName;
			arrHrDetail[8]	= dmEmailAddress;
			arrHrDetail[9]	= dmPhoneNumber;
			
			arrHrDetail[16]	= jobordertype;
			arrHrDetail[17]	= joborderwithSingleSchoolAttachFlag;
			arrHrDetail[18]	= attachSchoolName;
			arrHrDetail[19]	= districtNameforSJO;


		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return arrHrDetail;
	}
	
	public String[] getHrDetailToUser(HttpServletRequest request,UserMaster userMaster)
	{
		System.out.println("\n Method getHrDetail in CommonService ");
		String schoolDistrictName =	"";
		int isHrContactExist = 0;
		String hrContactFirstName = "";
		String hrContactLastName = "";
		String hrContactEmailAddress = "";
		String title 			 = "";
		String phoneNumber		 = "";
		String dmName 			 = "";
		String dmEmailAddress = "";
		String dmPhoneNumber	 = "";
		String[] arrHrDetail	= new String[15]; 
		
		List<DistrictKeyContact> dkclist = new ArrayList<DistrictKeyContact>();
		List<SchoolKeyContact> skclist = new ArrayList<SchoolKeyContact>();
		
		ContactTypeMaster ctm 	= new ContactTypeMaster();
		
		try{
			//ctm	=	contactTypeMasterDAO.findById(1, false, false);
			ctm.setContactTypeId(1);
			Criterion criterion1 = Restrictions.eq("keyContactTypeId", ctm);
			Criterion criterion2 = null;
			
			
			if(userMaster.getEntityType()==2)
			{
				schoolDistrictName		=	userMaster.getDistrictId().getDistrictName()==null?"":userMaster.getDistrictId().getDistrictName();
				criterion2				=	Restrictions.eq("districtId", userMaster.getDistrictId().getDistrictId());
				dkclist					=	districtKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);
				
				if(dkclist.size()>0)
				{
					isHrContactExist = 1;
					if(dkclist.get(0).getKeyContactFirstName()!=null)
						hrContactFirstName = dkclist.get(0).getKeyContactFirstName();
					
					if(dkclist.get(0).getKeyContactLastName()!=null)
						hrContactLastName = dkclist.get(0).getKeyContactLastName();
					
					if(dkclist.get(0).getKeyContactEmailAddress()!=null)
						hrContactEmailAddress = dkclist.get(0).getKeyContactEmailAddress();
					
					if(dkclist.get(0).getKeyContactPhoneNumber()!=null)
						phoneNumber	= dkclist.get(0).getKeyContactPhoneNumber();
					
					if(dkclist.get(0).getKeyContactTitle()!=null)
						title = dkclist.get(0).getKeyContactTitle();
						
				}
				else
				{
					System.out.println(" [ CommonService ] DA Final Decision Maker Information [ If Hr is not Available ]");
					if(userMaster.getDistrictId().getDmName()!=null)
					{
						isHrContactExist=2;
						dmName	=	userMaster.getDistrictId().getDmName();
						
						if(userMaster.getDistrictId().getDmEmailAddress()!=null)
							dmEmailAddress	=	userMaster.getDistrictId().getDmEmailAddress();
						
						if(userMaster.getDistrictId().getDmPhoneNumber()!=null)
							dmPhoneNumber	=	userMaster.getDistrictId().getDmPhoneNumber();
					}
				}
			}
			else
			{
				if(userMaster.getEntityType()==3)
				{
					schoolDistrictName		=	userMaster.getSchoolId().getSchoolName()==null?"":userMaster.getSchoolId().getSchoolName();
					criterion2				=	Restrictions.eq("schoolId", userMaster.getSchoolId().getSchoolId());
					skclist					=	schoolKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);
					
					if(skclist.size()>0)
					{
						isHrContactExist = 1;
						if(skclist.get(0).getKeyContactFirstName()!=null)
							hrContactFirstName = skclist.get(0).getKeyContactFirstName();
						
						if(skclist.get(0).getKeyContactLastName()!=null)
							hrContactLastName = skclist.get(0).getKeyContactLastName();
						
						if(skclist.get(0).getKeyContactEmailAddress()!=null)
							hrContactEmailAddress = skclist.get(0).getKeyContactEmailAddress();
						
						if(skclist.get(0).getKeyContactPhoneNumber()!=null)
							phoneNumber	= skclist.get(0).getKeyContactPhoneNumber();
						
						if(skclist.get(0).getKeyContactTitle()!=null)
							title = skclist.get(0).getKeyContactTitle();
					}
					else
					{
						System.out.println(" [CommonService  SA Final Decision Maker Information [ If Hr is not Available ]");
						if(userMaster.getSchoolId().getDmName()!=null)
						{
							isHrContactExist=2;
							dmName	=	userMaster.getSchoolId().getDmName();
							
							if(userMaster.getSchoolId().getDmEmailAddress()!=null)
								dmEmailAddress	=	userMaster.getSchoolId().getDmEmailAddress();
							
							if(userMaster.getSchoolId().getDmPhoneNumber()!=null)
								dmPhoneNumber	=	userMaster.getSchoolId().getDmPhoneNumber();
						}
					}
				}
			}
			
			arrHrDetail[0]	= hrContactFirstName;
			arrHrDetail[1]	= hrContactLastName;
			arrHrDetail[2]	= hrContactEmailAddress;	
			arrHrDetail[3]	= phoneNumber;
			arrHrDetail[4]	= title;
			arrHrDetail[5]	= ""+isHrContactExist;
			arrHrDetail[6]	= schoolDistrictName;
			arrHrDetail[7]	= dmName;
			arrHrDetail[8]	= dmEmailAddress;
			arrHrDetail[9]	= dmPhoneNumber;
			

		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return arrHrDetail;
	}
	
	@Transactional()
	public String[] getHrDetailToTeacherNow(HttpServletRequest request,JobOrder jobOrder,Map<Integer,List<SchoolMaster>> schoolMap)
	{

		System.out.println("\n Method getHrDetail in CommonService ");
		//HttpSession session = request.getSession(false);
		String schoolDistrictName =	"";
		int isHrContactExist = 0;
		String hrContactFirstName = "";
		String hrContactLastName = "";
		String hrContactEmailAddress = "";
		String title 			 = "";
		String phoneNumber		 = "";
		String dmName 			 = "";
		String dmEmailAddress = "";
		String dmPhoneNumber	 = "";
	
		String jobordertype		="";
		String joborderwithSingleSchoolAttachFlag ="";
		String attachSchoolName	="";
		String districtNameforSJO		="";
		String[] arrHrDetail	= new String[20]; 
		
		List<DistrictKeyContact> dkclist = new ArrayList<DistrictKeyContact>();
		List<SchoolKeyContact> skclist = new ArrayList<SchoolKeyContact>();
		
		ContactTypeMaster ctm 	= new ContactTypeMaster();
		
		try{
			ctm.setContactTypeId(1);
			Criterion criterion1 = Restrictions.eq("keyContactTypeId", ctm);
			Criterion criterion2 = null;
			
			jobordertype=""+jobOrder.getCreatedForEntity();
			List<SchoolMaster> schools = schoolMap.get(jobOrder.getJobId());
			if(schools==null)
				schools = new ArrayList<SchoolMaster>();
			
			if(jobOrder.getCreatedForEntity()==2 && schools.size()==1)
			{
				joborderwithSingleSchoolAttachFlag	="1";
				if(schools.get(0).getDisplayName()!=null && (!schools.get(0).getDisplayName().trim().equals("")))
					attachSchoolName	=	schools.get(0).getDisplayName();
				else
					attachSchoolName	=	schools.get(0).getSchoolName()==null?"":schools.get(0).getSchoolName();
			}
			else
			{
				/* ==== if job order is SJO in this case */
				if(jobOrder.getCreatedForEntity()==3)
				{
					if(jobOrder.getDistrictMaster().getDisplayName()!=null && (!jobOrder.getDistrictMaster().getDisplayName().trim().equals("")))
						districtNameforSJO	=	jobOrder.getDistrictMaster().getDisplayName();
					else
						districtNameforSJO		=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
				}
				joborderwithSingleSchoolAttachFlag	="0";
			}
			
			if(jobOrder.getCreatedForEntity()==2)
			{
				if(jobOrder.getDistrictMaster().getDisplayName()!=null && (!jobOrder.getDistrictMaster().getDisplayName().trim().equals("")))
					schoolDistrictName	=	jobOrder.getDistrictMaster().getDisplayName();
				else
					schoolDistrictName		=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
				
				criterion2				=	Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
				dkclist					=	districtKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);
				
				if(dkclist.size()>0)
				{
					isHrContactExist = 1;
					if(dkclist.get(0).getKeyContactFirstName()!=null)
						hrContactFirstName = dkclist.get(0).getKeyContactFirstName();
					
					if(dkclist.get(0).getKeyContactLastName()!=null)
						hrContactLastName = dkclist.get(0).getKeyContactLastName();
					
					if(dkclist.get(0).getKeyContactEmailAddress()!=null)
						hrContactEmailAddress = dkclist.get(0).getKeyContactEmailAddress();
					
					if(dkclist.get(0).getKeyContactPhoneNumber()!=null)
						phoneNumber	= dkclist.get(0).getKeyContactPhoneNumber();
					
					if(dkclist.get(0).getKeyContactTitle()!=null)
						title = dkclist.get(0).getKeyContactTitle();
						
				}
				else
				{
					System.out.println(" [ CommonService ] DA Final Decision Maker Information [ If Hr is not Available ]");
					if(jobOrder.getDistrictMaster().getDmName()!=null)
					{
						isHrContactExist=2;
						dmName	=	jobOrder.getDistrictMaster().getDmName();
						
						if(jobOrder.getDistrictMaster().getDmEmailAddress()!=null)
							dmEmailAddress	=	jobOrder.getDistrictMaster().getDmEmailAddress();
						
						if(jobOrder.getDistrictMaster().getDmPhoneNumber()!=null)
							dmPhoneNumber	=	jobOrder.getDistrictMaster().getDmPhoneNumber();
					}
				}
			}
			else
			{
				if(jobOrder.getCreatedForEntity()==3)
				{
					if(schools.get(0).getDisplayName()!=null && (!schools.get(0).getDisplayName().trim().equals("")))
						schoolDistrictName	=	schools.get(0).getDisplayName();
					else
						schoolDistrictName	=	schools.get(0).getSchoolName()==null?"":schools.get(0).getSchoolName();
					
					criterion2				=	Restrictions.eq("schoolId", schools.get(0).getSchoolId());
					skclist					=	schoolKeyContactDAO.findByCriteria(Order.asc("keyContactId"),criterion1,criterion2);
					
					if(skclist.size()>0)
					{
						isHrContactExist = 1;
						if(skclist.get(0).getKeyContactFirstName()!=null)
							hrContactFirstName = skclist.get(0).getKeyContactFirstName();
						
						if(skclist.get(0).getKeyContactLastName()!=null)
							hrContactLastName = skclist.get(0).getKeyContactLastName();
						
						if(dkclist.get(0).getKeyContactEmailAddress()!=null)
							hrContactEmailAddress = dkclist.get(0).getKeyContactEmailAddress();
						
						if(skclist.get(0).getKeyContactPhoneNumber()!=null)
							phoneNumber	= skclist.get(0).getKeyContactPhoneNumber();
						
						if(skclist.get(0).getKeyContactTitle()!=null)
							title = skclist.get(0).getKeyContactTitle();
					}
				}
				else
				{
					System.out.println(" [CommonService  SA Final Decision Maker Information [ If Hr is not Available ]");
					if(schools.get(0).getDmName()!=null)
					{
						isHrContactExist=2;
						dmName	=	schools.get(0).getDmName();
						
						if(schools.get(0).getDmEmailAddress()!=null)
							dmEmailAddress	=	schools.get(0).getDmEmailAddress();
						
						if(schools.get(0).getDmPhoneNumber()!=null)
							dmPhoneNumber	=	schools.get(0).getDmPhoneNumber();
					}
				}
			}
			
			arrHrDetail[0]	= hrContactFirstName;
			arrHrDetail[1]	= hrContactLastName;
			arrHrDetail[2]	= hrContactEmailAddress;	
			arrHrDetail[3]	= phoneNumber;
			arrHrDetail[4]	= title;
			arrHrDetail[5]	= ""+isHrContactExist;
			arrHrDetail[6]	= schoolDistrictName;
			arrHrDetail[7]	= dmName;
			arrHrDetail[8]	= dmEmailAddress;
			arrHrDetail[9]	= dmPhoneNumber;
			
			arrHrDetail[16]	= jobordertype;
			arrHrDetail[17]	= joborderwithSingleSchoolAttachFlag;
			arrHrDetail[18]	= attachSchoolName;
			arrHrDetail[19]	= districtNameforSJO;


		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return arrHrDetail;
	
	}
	
	@Transactional
	public String futureJobStatus(JobOrder jobOrder,TeacherDetail teacherDetail,JobForTeacher jobForTeacher){
		
		
		try{
			DistrictMaster districtMaster=jobOrder.getDistrictMaster();
			System.out.println("futureJobStatus jobForTeacher:::::::"+jobForTeacher.getJobForTeacherId());
			System.out.println(teacherDetail.getTeacherId()+"<<<<::: Future Job :::>>>>"+jobOrder.getJobId());
			//System.out.println(jobOrder.getJobCategoryMaster().getJobCategoryId()+"<<<>>futureJob:::::"+districtMaster.getDistrictId());
			List<JobForTeacher> jobForTeacherList = new ArrayList<JobForTeacher>();
			try{
				jobForTeacherList = jobForTeacherDAO.findAllTeacherByDistrictId(teacherDetail,districtMaster);
				System.out.println(":::::::: jobForTeacherList.size() :::::::::::"+jobForTeacherList.size());
				if(jobForTeacherList!=null && jobForTeacherList.size()==1 && jobForTeacherList.get(0).getJobId().getJobId().equals(jobOrder.getJobId())){
					System.out.println(":::::::::: First Time Job Apply ::::::::::");
					firstTimeJobApply(jobOrder,teacherDetail,jobForTeacher);
				}
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges= new ArrayList<JobCategoryWiseStatusPrivilege>();
			try{
				jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.findStatusPrivilegeByJob(jobOrder);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println(":::jobCategoryWiseStatusPrivileges::::::::::::::::::::::>>>>"+jobCategoryWiseStatusPrivileges.size());
			if(jobCategoryWiseStatusPrivileges.size()>0){
				Map<String,String> statusSecMap=new HashMap<String,String>();
				Map<String,String> statusSecUpdateStatusMap=new HashMap<String,String>();
				
				for (JobCategoryWiseStatusPrivilege jobPri : jobCategoryWiseStatusPrivileges){
					if(jobPri.getStatusMaster()!=null){
						statusSecUpdateStatusMap.put(jobPri.getStatusMaster().getStatusId()+"##0", jobPri.getUpdateStatusOption()+"");
						statusSecMap.put(jobPri.getStatusMaster().getStatusId()+"##0",jobPri.getStatusMaster().getStatus());
					}else{
						statusSecUpdateStatusMap.put("0##"+jobPri.getSecondaryStatus().getSecondaryStatusId(), jobPri.getUpdateStatusOption()+"");
						statusSecMap.put("0##"+jobPri.getSecondaryStatus().getSecondaryStatusId(),jobPri.getSecondaryStatus().getSecondaryStatusName());
					}
				}
				System.out.println("statusSecUpdateStatusMap::::::::::"+statusSecUpdateStatusMap.size());
				
				Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
				Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
				CandidateGridService cgService = new CandidateGridService();
				List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
				LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
				Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
				try{
					int counter=0; 
					for(SecondaryStatus tree: lstTreeStructure)
					{
						if(tree.getSecondaryStatus()==null)
						{
							if(tree.getChildren().size()>0){
								counter=cgService.getAllStatusForHistoryStatus(statusSecMap,statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
							}
						}
					}
				}catch(Exception e){}
				
		        Map<String,List<TeacherStatusHistoryForJob>> statusSecJFTMap=new HashMap<String,List<TeacherStatusHistoryForJob>>();
				List<TeacherStatusHistoryForJob> historyForJobs=teacherStatusHistoryForJobDAO.findJobByTeachersWithDistrict(teacherDetail,jobOrder);
				
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobs){
					if(teacherStatusHistoryForJob.getStatusMaster()!=null){
						String key=teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0";
						List<TeacherStatusHistoryForJob> jobsHAll=statusSecJFTMap.get(key);
						if(jobsHAll==null){
							List<TeacherStatusHistoryForJob> jobsH= new ArrayList<TeacherStatusHistoryForJob>();
							jobsH.add(teacherStatusHistoryForJob);
							statusSecJFTMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",jobsH);
						}else{
							jobsHAll.add(teacherStatusHistoryForJob);
							statusSecJFTMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",jobsHAll);
						}
					}else{
						String key="0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId();
						List<TeacherStatusHistoryForJob> jobsHAll=statusSecJFTMap.get(key);
						if(jobsHAll==null){
							List<TeacherStatusHistoryForJob> jobsH= new ArrayList<TeacherStatusHistoryForJob>();
							jobsH.add(teacherStatusHistoryForJob);
							statusSecJFTMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),jobsH);
						}else{
							jobsHAll.add(teacherStatusHistoryForJob);
							statusSecJFTMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),jobsHAll);
						}
					}
				}
				System.out.println("statusMasterMap::::::::::"+statusMasterMap.size());
				System.out.println("secondaryStatusMap::::::::::"+secondaryStatusMap.size());
				System.out.println("statusSecJFTMap::::::::::"+statusSecJFTMap.size());
				
				boolean existJobOrder=false;
				StatusMaster statusMasterForJFT=null;
				SecondaryStatus secondaryStatusForJFT=null;
				ArrayList<String> keys = new ArrayList<String>(statusNameMap.keySet());
				List<TeacherStatusHistoryForJob> isertJobHList=new ArrayList<TeacherStatusHistoryForJob>();
		        for(int i=keys.size()-1; i>=0;i--){
		        	String autoStatus=statusSecUpdateStatusMap.get(keys.get(i));
	        		System.out.println("autoStatus:::"+autoStatus);
	        		
			        	if(statusMasterMap.get(keys.get(i))!=null){
			        		System.out.println("::::::::::::::S:::::::::::::::"+keys.get(i));
			        		List<TeacherStatusHistoryForJob> jobs=statusSecJFTMap.get(keys.get(i));
			        		if(jobs!=null){
			        		for (TeacherStatusHistoryForJob jobs2 : jobs) {
			        			if(autoStatus!=null && !autoStatus.equals("")){
			        				if(autoStatus.equals("0")){
			        					if(jobs2.getJobOrder().getJobCategoryMaster()!=null){
			        						if(jobs2.getJobOrder().getJobCategoryMaster().getJobCategoryId().equals(jobOrder.getJobCategoryMaster().getJobCategoryId())){
			        							isertJobHList.add(jobs2);
			        							System.out.println("C 1");
			        							if(statusMasterForJFT==null){
			        								existJobOrder=true;
			        								System.out.println("S 1");
				        							statusMasterForJFT=statusMasterMap.get(keys.get(i));
			        							}
			        							break;
			        						}
			        					}
			        				}else if(autoStatus.equals("1")){
			        					isertJobHList.add(jobs2);
			        					System.out.println("C 2");
		    							if(statusMasterForJFT==null){
		    								existJobOrder=true;
		    								System.out.println("S 2");
		        							statusMasterForJFT=statusMasterMap.get(keys.get(i));
		    							}
		    							break;
			        				}else if(autoStatus.equals("2") && jobOrder.getGeoZoneMaster()!=null){
			        					String jobTitle=jobOrder.getJobTitle();
			        					try{
											if(jobTitle.indexOf("(")>0){
												jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
												System.out.println("jobTitle S:::::::::"+jobTitle);
												if(jobs2.getJobOrder().getJobTitle().contains(jobTitle)){
													isertJobHList.add(jobs2);
													System.out.println("C 3");
				        							if(statusMasterForJFT==null){
				        								existJobOrder=true;
				        								System.out.println("S 3");
					        							statusMasterForJFT=statusMasterMap.get(keys.get(i));
				        							}
				        							break;
												}
											}
			        					}catch(Exception e){
			        						e.printStackTrace();
			        					}
			        				}
			        			}
							}	
			        		
			        	} else{
			        		// Get Status For First Time Job Apply
			        		if(autoStatus!=null && !autoStatus.equals("")){
		        				if(autoStatus.equals("0")){
        							if(statusMasterForJFT==null){
	        							statusMasterForJFT=statusMasterMap.get(keys.get(i));
        							}
        							break;
		        				}else if(autoStatus.equals("1")){
	    							if(statusMasterForJFT==null){
	        							statusMasterForJFT=statusMasterMap.get(keys.get(i));
	    							}
	    							break;
		        				}else if(autoStatus.equals("2") && jobOrder.getGeoZoneMaster()!=null){
		        					String jobTitle=jobOrder.getJobTitle();
		        					try{
										if(jobTitle.indexOf("(")>0){
											jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
		        							if(statusMasterForJFT==null){
			        							statusMasterForJFT=statusMasterMap.get(keys.get(i));
		        							}
		        							break;
										}
		        					}catch(Exception e){
		        						e.printStackTrace();
		        					}
		        				}
		        			}
			        	}
		        	} else if(secondaryStatusMap.get(keys.get(i))!=null){
		        		System.out.println("SS:::::::::::::::"+keys.get(i));
		        		List<TeacherStatusHistoryForJob> jobs=statusSecJFTMap.get(keys.get(i));
		        		if(jobs!=null){
		        		for (TeacherStatusHistoryForJob jobs2 : jobs) {
		        			if(autoStatus!=null && !autoStatus.equals("")){
		        				if(autoStatus.equals("0")){
		        					if(jobs2.getJobOrder().getJobCategoryMaster()!=null){
		        						if(jobs2.getJobOrder().getJobCategoryMaster().getJobCategoryId().equals(jobOrder.getJobCategoryMaster().getJobCategoryId())){
		        							isertJobHList.add(jobs2);
		        							System.out.println("C 4");
		        							if(secondaryStatusForJFT==null){
		        								existJobOrder=true;
		        								System.out.println("S 4");
		        								secondaryStatusForJFT=secondaryStatusMap.get(keys.get(i));
		        							}
		        							break;
		        						}
		        					}
		        				}else if(autoStatus.equals("1")){
		        					isertJobHList.add(jobs2);
		        					System.out.println("C 5");
	    							if(secondaryStatusForJFT==null){
	    								existJobOrder=true;
	    								secondaryStatusForJFT=secondaryStatusMap.get(keys.get(i));
	        							System.out.println("S 5");
	    							}
	    							break;
		        				}else if(autoStatus.equals("2") && jobOrder.getGeoZoneMaster()!=null){
		        					String jobTitle=jobOrder.getJobTitle();
		        					try{
										if(jobTitle.indexOf("(")>0){
											jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
										}
										System.out.println("jobTitle SS:::::::::"+jobTitle);
										if(jobs2.getJobOrder().getJobTitle().contains(jobTitle)){
											isertJobHList.add(jobs2);
											System.out.println("C 6");
		        							if(secondaryStatusForJFT==null){
		        								existJobOrder=true;
		        								System.out.println("S 6");
		        								secondaryStatusForJFT=secondaryStatusMap.get(keys.get(i));
		        							}
		        							break;
										}
		        					}catch(Exception e){
		        						e.printStackTrace();
		        					}
		        				}
		        			}
						}
		        	 } else { // Get Secondary Status For First Time Job Apply
		        			if(autoStatus!=null && !autoStatus.equals("")){
		        				if(autoStatus.equals("0")){
        							if(secondaryStatusForJFT==null){
        								secondaryStatusForJFT=secondaryStatusMap.get(keys.get(i));
        							}
        							break;
		        				}else if(autoStatus.equals("1")){
	    							if(secondaryStatusForJFT==null){
	    								secondaryStatusForJFT=secondaryStatusMap.get(keys.get(i));
	    							}
	    							break;
		        				}else if(autoStatus.equals("2") && jobOrder.getGeoZoneMaster()!=null){
		        					String jobTitle=jobOrder.getJobTitle();
		        					try{
										if(jobTitle.indexOf("(")>0){
											jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
										}
		        							if(secondaryStatusForJFT==null){
		        								secondaryStatusForJFT=secondaryStatusMap.get(keys.get(i));
		        							}
		        							break;
		        					}catch(Exception e){
		        						e.printStackTrace();
		        					}
		        				}
		        			}
		        	    }
		        	 }
		        	System.out.println("");
		        	System.out.println("==========================================================================="+existJobOrder);
		        	System.out.println("");
		        }
		        Map<String,TeacherStatusHistoryForJob> filterMap=new HashMap<String, TeacherStatusHistoryForJob>();
		        if(isertJobHList.size()>0)
		        for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : isertJobHList) {
		        	String key="";
		        	if(teacherStatusHistoryForJob.getStatusMaster()!=null){
						key=teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"#"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##0";
		        	}else{
		        		key="0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"#"+teacherStatusHistoryForJob.getJobOrder().getJobId();
		        	}
		        	filterMap.put(key,teacherStatusHistoryForJob);
				}
		        
		        List<TeacherStatusNotes> notesForJobs=teacherStatusNotesDAO.findJobByTeachersWithDistrict(teacherDetail,jobOrder);
				
				List<TeacherStatusNotes> tsNotes=new ArrayList<TeacherStatusNotes>();
				if(notesForJobs.size()>0)
				for (TeacherStatusNotes teacherStatusNotes : notesForJobs) {
					String key="";
		        	if(teacherStatusNotes.getStatusMaster()!=null){
						key=teacherStatusNotes.getStatusMaster().getStatusId()+"#"+teacherStatusNotes.getTeacherDetail().getTeacherId()+"#"+teacherStatusNotes.getJobOrder().getJobId()+"##0";
		        	}else{
		        		key="0##"+teacherStatusNotes.getSecondaryStatus().getSecondaryStatusId()+"#"+teacherStatusNotes.getTeacherDetail().getTeacherId()+"#"+teacherStatusNotes.getJobOrder().getJobId();
		        	}
		        	if(filterMap.get(key)!=null){
		        		System.out.println("Key Notes::::::::::::::::"+key);
		        		tsNotes.add(teacherStatusNotes);
		        	}
				}
		        
			    List<TeacherStatusScores> scoreForJobs=teacherStatusScoresDAO.findJobByTeachersWithDistrict(teacherDetail,jobOrder);
				List<TeacherStatusScores> tsScores=new ArrayList<TeacherStatusScores>();
				if(scoreForJobs.size()>0)
				for (TeacherStatusScores teacherStatusScores : scoreForJobs) {
					String key="";
		        	if(teacherStatusScores.getStatusMaster()!=null){
						key=teacherStatusScores.getStatusMaster().getStatusId()+"#"+teacherStatusScores.getTeacherDetail().getTeacherId()+"#"+teacherStatusScores.getJobOrder().getJobId()+"##0";
		        	}else{
		        		key="0##"+teacherStatusScores.getSecondaryStatus().getSecondaryStatusId()+"#"+teacherStatusScores.getTeacherDetail().getTeacherId()+"#"+teacherStatusScores.getJobOrder().getJobId();
		        	}
		        	if(filterMap.get(key)!=null){
		        		System.out.println("Key Score::::::::::::::::"+key);
		        		tsScores.add(teacherStatusScores);
		        	}
				}
		    	System.out.println("existJobOrder>>>:::::::::::"+existJobOrder);
		    	
		    	System.out.println("All notesForJobs::::::::"+notesForJobs.size());
			    System.out.println("All scoreForJobs::::::::"+scoreForJobs.size());
				System.out.println("All historyForJobs::::::::"+historyForJobs.size());
					
		        if(existJobOrder){ System.out.println("::::::::: existJobOrder :::::::::::::::::"+existJobOrder);
					SessionFactory sessionFactory=teacherStatusHistoryForJobDAO.getSessionFactory();
					StatelessSession statelesSsession = sessionFactory.openStatelessSession();
					Transaction txOpen =statelesSsession.beginTransaction();
		        	System.out.println("::::::::::: Inside existJobOrder:::::::::::");
		        	
		        	System.out.println("tsNotes:::"+tsNotes.size());
		        	System.out.println("tsScores:::"+tsScores.size());
		        	System.out.println("isertJobHList:::"+isertJobHList.size());
		        	if(isertJobHList.size()>0){
			        	for (TeacherStatusHistoryForJob tsHForJob : isertJobHList) {
			        		boolean insertFlag=false;
			        		if(tsHForJob.getStatusMaster()!=null){
			        			if(statusSecMap.get(tsHForJob.getStatusMaster().getStatusId()+"##0")!=null){
			        				insertFlag=true;
			        				System.out.println("Histrory Status:::"+tsHForJob.getStatusMaster().getStatusId());
				        		}
			        		}else if(statusSecMap.get("0##"+tsHForJob.getSecondaryStatus().getSecondaryStatusId())!=null){
			        			insertFlag=true;
			        			System.out.println("Histrory Sec Status:::"+tsHForJob.getSecondaryStatus().getSecondaryStatusId());
			        		}
			        		if(insertFlag){
			            		try {
			            			Calendar c = Calendar.getInstance();
									c.add(Calendar.SECOND,01);
									Date date = c.getTime();
			            			TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
									tSHJ.setTeacherDetail(teacherDetail);
									tSHJ.setJobOrder(jobOrder);
									if(tsHForJob.getStatusMaster()!=null){
										tSHJ.setStatusMaster(tsHForJob.getStatusMaster());
									}
									if(tsHForJob.getSecondaryStatus()!=null){
										tSHJ.setSecondaryStatus(tsHForJob.getSecondaryStatus());
									}
									tSHJ.setStatus(tsHForJob.getStatus());
									tSHJ.setCreatedDateTime(date);
									tSHJ.setUserMaster(tsHForJob.getUserMaster());
									tSHJ.setOverride(tsHForJob.getOverride());
									tSHJ.setOverrideBy(tsHForJob.getOverrideBy());
									//teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									statelesSsession.insert(tSHJ);
								} catch (Exception e) {
									// TODO: handle exception
								}
		            		}
						}
		        	}
		        	
		        	if(tsNotes.size()>0){
		            	for (TeacherStatusNotes tsNote : tsNotes) {
		            		boolean insertFlag=false;
		            		if(tsNote.getStatusMaster()!=null){
		            			if(statusSecMap.get(tsNote.getStatusMaster().getStatusId()+"##0")!=null){
		            				insertFlag=true;
		            				System.out.println("Notes Status-:::"+tsNote.getStatusMaster().getStatusId());
		    	        		}
		            		}else if(statusSecMap.get("0##"+tsNote.getSecondaryStatus().getSecondaryStatusId())!=null){
		            			insertFlag=true;
		            			System.out.println("Notes Sec Status:::"+tsNote.getSecondaryStatus().getSecondaryStatusId());
		            		}
		            		if(insertFlag){
			            		try {
			    					TeacherStatusNotes teacherObj = new TeacherStatusNotes();
									
			    					teacherObj.setTeacherDetail(teacherDetail);
			    					teacherObj.setUserMaster(tsNote.getUserMaster());
			    					teacherObj.setJobOrder(jobOrder);
			    					teacherObj.setDistrictId(tsNote.getDistrictId());
			    					if(tsNote.getSchoolId()!=null){
			    						teacherObj.setSchoolId(tsNote.getSchoolId());
			    					}else{
			    						teacherObj.setSchoolId(null);
			    					}
			    					teacherObj.setEmailSentTo(tsNote.getEmailSentTo());
									teacherObj.setFinalizeStatus(tsNote.isFinalizeStatus());
									if(tsNote.getStatusMaster()!=null)
										teacherObj.setStatusMaster(tsNote.getStatusMaster());
				
									if(tsNote.getSecondaryStatus()!=null)
										teacherObj.setSecondaryStatus(tsNote.getSecondaryStatus());
									
									teacherObj.setStatusNotes(tsNote.getStatusNotes());
									
									if(tsNote.getStatusNoteFileName()!=null)
										teacherObj.setStatusNoteFileName(tsNote.getStatusNoteFileName());
									
									//teacherStatusNotesDAO.makePersistent(teacherObj);
									statelesSsession.insert(teacherObj);
									
								} catch (Exception e) {
									// TODO: handle exception
								}
		            		}
		    			}
		        	}
		        	if(tsScores.size()>0){
		            	for (TeacherStatusScores tsScore : tsScores){
		            		boolean insertFlag=false;
		            		if(tsScore.getStatusMaster()!=null){
		            			if(statusSecMap.get(tsScore.getStatusMaster().getStatusId()+"##0")!=null){
		            				insertFlag=true;
		            				System.out.println("Score Status:>>::"+tsScore.getStatusMaster().getStatusId());
		    	        		}
		            		}else if(statusSecMap.get("0##"+tsScore.getSecondaryStatus().getSecondaryStatusId())!=null){
		            			insertFlag=true;
		            			System.out.println("Score Sec Status:::"+tsScore.getSecondaryStatus().getSecondaryStatusId());
		            		}
		            		if(insertFlag){
			            		try {
			            			int fStatus=0;
			            			StatusMaster statusMaster=null;
			            			SecondaryStatus secondaryStatus=null;
			            			if(tsScore.getStatusMaster()!=null){
			            				statusMaster=tsScore.getStatusMaster();
			            			}
			            			if(tsScore.getSecondaryStatus()!=null){
			            				secondaryStatus=tsScore.getSecondaryStatus();
			            			}
			            			
			    					List<TeacherStatusScores> teacherStatusScoresList= teacherStatusScoresDAO.getStatusScore(teacherDetail,jobOrder,statusMaster,secondaryStatus,tsScore.getUserMaster());
			    					TeacherStatusScores statusScores = new TeacherStatusScores();
			    					
			    					if(teacherStatusScoresList.size()>0){
			    						statusScores =teacherStatusScoresList.get(0);
			    						if(statusScores.isFinalizeStatus())
			    							fStatus=1;
			    					}
			    					if(fStatus==0){
			    						System.out.println("::::::::::::::Score Insert >::::::::::::::::::");
			    						statusScores.setTeacherDetail(teacherDetail);
			    						statusScores.setUserMaster(tsScore.getUserMaster());
			    						statusScores.setJobOrder(jobOrder);
			    						statusScores.setDistrictId(tsScore.getDistrictId());
			    						if(tsScore.getStatusMaster()!=null){
			    							statusScores.setStatusMaster(tsScore.getStatusMaster());
			    						}
			    						if(tsScore.getSecondaryStatus()!=null){
			    							statusScores.setSecondaryStatus(tsScore.getSecondaryStatus());
			    						}
		
			    						if(tsScore.getSchoolId()!=null){
			    							statusScores.setSchoolId(tsScore.getSchoolId());	
			    						}else{
			    							statusScores.setSchoolId(null);
			    						}
			    						statusScores.setEmailSentTo(tsScore.getEmailSentTo());
			    						//maxScore=tsScore.getMaxScore();
			    						//scoreProvided=tsScore.getScoreProvided();
			    						statusScores.setScoreProvided(tsScore.getScoreProvided());
			    						statusScores.setMaxScore(tsScore.getMaxScore());
			    						statusScores.setFinalizeStatus(tsScore.isFinalizeStatus());
			    						//teacherStatusScoresDAO.makePersistent(statusScores);
			    						statelesSsession.insert(statusScores);
			    					}
									
								} catch (Exception e) {
									// TODO: handle exception
								}
								//////////////////////////////////////////////////////////////////////////
								
								Double jobWiseConsolidatedScore=0.0;
								Double jobWiseMaxScore=0.0;
								
								try
								{
									Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobOrder);
									jobWiseConsolidatedScore=teacherStatusScoresAvg[0];
									jobWiseMaxScore=teacherStatusScoresAvg[1];
								}
								catch(Exception e)
								{ 
									e.printStackTrace();
								}
								
								System.out.println("JID "+jobOrder.getJobId()+" TID "+teacherDetail.getTeacherId() +" Score "+jobWiseConsolidatedScore +" Max "+jobWiseMaxScore);
								try
								{
									if(jobWiseConsolidatedScore > 0 && jobWiseMaxScore > 0.0){
										JobWiseConsolidatedTeacherScore jWScore=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore(teacherDetail, jobOrder);
										if(jWScore==null){
											jWScore=new JobWiseConsolidatedTeacherScore();
											jWScore.setTeacherDetail(teacherDetail);
											jWScore.setJobOrder(jobOrder);
											jWScore.setJobWiseConsolidatedScore(jobWiseConsolidatedScore);
											jWScore.setJobWiseMaxScore(jobWiseMaxScore);
											jobWiseConsolidatedTeacherScoreDAO.makePersistent(jWScore);
										}else{
											jWScore.setJobWiseConsolidatedScore(jobWiseConsolidatedScore);
											jWScore.setJobWiseMaxScore(jobWiseMaxScore);
											jobWiseConsolidatedTeacherScoreDAO.makePersistent(jWScore);
										}
									}
								}catch(Exception e){}
								////////////////////////////////////////////////////////////////////////
		            		}
		    			}
		        	}
		        	/////////////////////////Status update for jobForTeacher /////////////////////
		        	try{
		        		if(secondaryStatusForJFT!=null || statusMasterForJFT!=null){
		        			System.out.println("::::::::::::::::::::::::JobForTeacher Update Here :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
				        	if((secondaryStatusForJFT!=null && statusMasterForJFT==null) || (statusMasterForJFT!=null && secondaryStatusForJFT!=null && cgService.priSecondaryStatusCheck(statusMasterForJFT,secondaryStatusForJFT))){
								jobForTeacher.setStatusMaster(null);
								jobForTeacher.setSecondaryStatus(secondaryStatusForJFT);
								jobForTeacher.setLastActivity(secondaryStatusForJFT.getSecondaryStatusName());
								jobForTeacher.setLastActivityDate(new Date());
							}else{
								jobForTeacher.setStatus(statusMasterForJFT);
								jobForTeacher.setStatusMaster(statusMasterForJFT);
								jobForTeacher.setStatusMaster(statusMasterForJFT);
								jobForTeacher.setLastActivity(statusMasterForJFT.getStatus());
							}
				        	//jobForTeacherDAO.updatePersistent(jobForTeacher);
				        	statelesSsession.update(jobForTeacher);
		        		}
		        	}catch(Exception e){
		        		e.printStackTrace();
		        	}
		        	txOpen.commit();
		        	statelesSsession.close();
					///////////////////////////////////////////////////////////////////////////////////////////
		        }
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	public String undoJobStatus(JobForTeacher jobForTeacher){
  
  try{
   System.out.println("undoJobStatus jobForTeacher:::::::"+jobForTeacher.getJobForTeacherId());
    JobOrder jobOrder=jobForTeacher.getJobId();
    TeacherDetail teacherDetail=jobForTeacher.getTeacherId();
    CandidateGridService cgService = new CandidateGridService();
    List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
    Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
    try{
     int counter=0; 
     for(SecondaryStatus tree: lstTreeStructure)
     {
      if(tree.getSecondaryStatus()==null)
      {
       if(tree.getChildren().size()>0){
        counter=cgService.getStatusForUndoHistoryStatus(tree.getChildren(),statusHistMap,counter);
       }
      }
     }
    }catch(Exception e){}
    System.out.println("statusHistMap:::::::"+statusHistMap.size());
    List<TeacherStatusHistoryForJob> historyForJobs=teacherStatusHistoryForJobDAO.findByTeacherAndJob(teacherDetail,jobOrder);
    System.out.println("historyForJobs::::::::"+historyForJobs.size());
    int count=0;
    StatusMaster statusMasterForJFT=null;
    SecondaryStatus secondaryStatusForJFT=null;
    boolean masterStatus=false;
    for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobs){
     
     if(teacherStatusHistoryForJob.getStatusMaster()!=null){
      String key=teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0";
      int tempCount=0;
      if(statusHistMap.get(key)!=null){
       tempCount=statusHistMap.get(key);
      }
      if(count==0 || count<tempCount){
       count=tempCount;
       statusMasterForJFT=teacherStatusHistoryForJob.getStatusMaster();
       masterStatus=true;
       secondaryStatusForJFT=null;
      }
     }else if(teacherStatusHistoryForJob.getSecondaryStatus()!=null){
      String key="0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId();
      int tempCount=0;
      if(statusHistMap.get(key)!=null){
       tempCount=statusHistMap.get(key);
      }
      if(count==0 || count<tempCount){
       count=tempCount;
       statusMasterForJFT=null;
       secondaryStatusForJFT=teacherStatusHistoryForJob.getSecondaryStatus();
      }
     }
     
    }
    try{
     if(historyForJobs.size()==0){
      System.out.println("-------No Status Abl--------------->");
         int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
         statusMasterForJFT = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
        }
    }catch (Exception e) {
     // TODO: handle exception
     e.printStackTrace();
    }
    try{
     System.out.println("Before set ststus if in side :::::::::::::::::::::::::::::::::::::::");
           if(secondaryStatusForJFT!=null || statusMasterForJFT!=null){
            System.out.println("Before set ststus if in side *************************************************");
            if((secondaryStatusForJFT!=null && statusMasterForJFT==null) || (statusMasterForJFT!=null && secondaryStatusForJFT!=null && cgService.priSecondaryStatusCheck(statusMasterForJFT,secondaryStatusForJFT))){
             System.out.println("Before set ststus if in side /////////////////////////////////////////////////");
             if(masterStatus==false && historyForJobs.size()!=0){
              System.out.println("Before set ststus if in side ++++++++++++++++++++++++++++++++++++++++++++++++++++++");
              int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
              System.out.println("Flag List print here here****************"+flagList);
           statusMasterForJFT = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
                  jobForTeacher.setStatus(statusMasterForJFT);
           System.out.println(" commoms file "+jobForTeacher.getStatus());
              if(statusMasterForJFT!=null){
               System.out.println("Before set ststus if in side --------------------------------------------------------------");
         //jobForTeacher.setApplicationStatus(statusMasterForJFT.getStatusId());
        }
             }
             jobForTeacher.setStatusMaster(null);
       jobForTeacher.setSecondaryStatus(secondaryStatusForJFT);
       jobForTeacher.setLastActivity(secondaryStatusForJFT.getSecondaryStatusName());
       jobForTeacher.setLastActivityDate(new Date());
       System.out.println("secondaryStatusForJFT::::::"+secondaryStatusForJFT.getSecondaryStatusName());
      }else{
       System.out.println("Before set ststus if in side 111111111111111111111111111111111111111111111111");
       System.out.println("statusMasterForJFT::::::"+statusMasterForJFT.getStatusShortName());
       jobForTeacher.setSecondaryStatus(null);
            jobForTeacher.setStatus(statusMasterForJFT);
       System.out.println("############## commoms file ###############"+jobForTeacher.getStatus());
       if(statusMasterForJFT!=null){
        System.out.println("Before set ststus if in side 222222222222222222222222222222222222222222222222222");
        //jobForTeacher.setApplicationStatus(statusMasterForJFT.getStatusId());
       }
       jobForTeacher.setStatusMaster(statusMasterForJFT);
       jobForTeacher.setLastActivity(statusMasterForJFT.getStatus());
       jobForTeacher.setLastActivityDate(new Date());
      }
      jobForTeacherDAO.makePersistent(jobForTeacher);
           }
          }catch(Exception e){
           e.printStackTrace();
          }
    
    
  }catch(Exception e){
   e.printStackTrace();
  }
  return null;
 }
	
	@Transactional
	public String autoStatusForSPJobs(JobOrder jobOrder,TeacherDetail teacherDetail,JobForTeacher jobForTeacher){
		System.out.println(" auto status pre-hire ::: "+teacherDetail.getTeacherId());
		String statusName = Utility.getLocaleValuePropByKey("lblSPStatus", locale);
		try {
			List<JobForTeacher> appliedJobByTeacher = jobForTeacherDAO.findByTeacherIdDistrictId(teacherDetail, null);
			List<JobCategoryMaster> jobCatList = new ArrayList<JobCategoryMaster>();
			List<JobOrder> jobOrderList = new ArrayList<JobOrder>();
			List<SecondaryStatus> sortedSecondaryStatus = null;
			List<SecondaryStatus> secondaryStatusList = null;
			Map<Integer,SecondaryStatus> secondarysMap = new HashMap<Integer, SecondaryStatus>();
			List<TeacherStatusHistoryForJob> teacherStatusForJob = null;
			Map<Integer,TeacherStatusHistoryForJob> teacherStatusJob = new HashMap<Integer, TeacherStatusHistoryForJob>();
			Map<Integer,List<String>> branchAdminEmails = new HashMap<Integer, List<String>>();
			List<BranchMaster> listBranches = new ArrayList<BranchMaster>();
			List<UserMaster> listUsers = null;
			if(appliedJobByTeacher!=null && appliedJobByTeacher.size()>0){
				for(JobForTeacher jft : appliedJobByTeacher){
					if(jft.getJobId().getJobCategoryMaster().getPreHireSmartPractices()!=null && jft.getJobId().getJobCategoryMaster().getPreHireSmartPractices()){
						jobOrderList.add(jft.getJobId());
						listBranches.add(jft.getJobId().getBranchMaster());
						if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
							jobCatList.add(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
						}
					}
				}
				listUsers = userMasterDAO.getAllBranchAdmin(listBranches);
				if(listUsers!=null && listUsers.size()>0){
					List<String> temp = null;
					for(UserMaster userMaster : listUsers){
						if(branchAdminEmails.get(userMaster.getBranchMaster().getBranchId())!=null){
							temp = branchAdminEmails.get(userMaster.getBranchMaster().getBranchId());
							temp.add(userMaster.getEmailAddress());
							branchAdminEmails.put(userMaster.getBranchMaster().getBranchId(), temp);
						}
						else{
							temp = new ArrayList<String>();
							temp.add(userMaster.getEmailAddress());
							branchAdminEmails.put(userMaster.getBranchMaster().getBranchId(), temp);
						}
					}
				}
				Map<String,String> statusSecMap=new HashMap<String,String>();

				teacherStatusForJob = teacherStatusHistoryForJobDAO.getTeacherStatusHistoryByJobList(teacherDetail, jobOrderList);
				if(teacherStatusForJob!=null && teacherStatusForJob.size()>0){
					for(TeacherStatusHistoryForJob tstatus : teacherStatusForJob){
						teacherStatusJob.put(tstatus.getJobOrder().getJobId(), tstatus);
					}
				}
				secondaryStatusList = secondaryStatusDAO.findSecondaryStatusByJobCategory(jobCatList, Utility.getLocaleValuePropByKey("lblSPStatus", locale));
				sortedSecondaryStatus = secondaryStatusDAO.findOrderedSecondaryStatusByJobCategoryForHead(jobCatList);
				if(secondaryStatusList!=null && secondaryStatusList.size()>0){
					for(SecondaryStatus secStatus : secondaryStatusList){
						secondarysMap.put(secStatus.getJobCategoryMaster().getJobCategoryId(), secStatus);

						if(secStatus.getStatusMaster()!=null){
							statusSecMap.put(secStatus.getStatusMaster().getStatusId()+"##0",secStatus.getStatusMaster().getStatus());
						}else{
							statusSecMap.put("0##"+secStatus.getSecondaryStatus().getSecondaryStatusId(),secStatus.getSecondaryStatus().getSecondaryStatusName());
						}
					}
					
					boolean mainSatus = false;
					int parentCategoryId = 0;
					Map<String,List<TeacherStatusHistoryForJob>> statusSecJFTMap=new HashMap<String,List<TeacherStatusHistoryForJob>>();
					List<TeacherStatusHistoryForJob> historyForJobs=teacherStatusHistoryForJobDAO.findJobByTeachersWithHeadQuarter(teacherDetail,jobOrderList);
					
					Map<String,TeacherStatusHistoryForJob> historyMap=new HashMap<String, TeacherStatusHistoryForJob>();
					
					for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobs) {
						String key="";
						if(teacherStatusHistoryForJob.getStatusMaster()!=null){
							key=teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0";
						}else{
							key=teacherStatusHistoryForJob.getJobOrder().getJobId()+"##0"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId();
						}
						historyMap.put(key, teacherStatusHistoryForJob);
					}
					
					
					for(JobForTeacher jft : appliedJobByTeacher)
					{
							boolean mailFlag=false;
							statusSecJFTMap=new HashMap<String,List<TeacherStatusHistoryForJob>>();
							if(jobOrderList.contains(jft.getJobId()) && !(jft.getStatus().getStatusShortName().equalsIgnoreCase("hird"))){
	
								try{
								parentCategoryId = jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();
								}
								catch(Exception ex){
									parentCategoryId = 0;
								}
								SecondaryStatus secSt = secondarysMap.get(parentCategoryId);
								//////////////////////////////////////////////////////////////////////////////////////////////
	
								Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
								Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
								CandidateGridService cgService = new CandidateGridService();
								LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
								Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								try{
									int counter=0;
									for(SecondaryStatus tree: sortedSecondaryStatus)
									{
										if(tree.getSecondaryStatus()==null)
										{
											if(tree.getChildren().size()>0){
												counter=cgService.getAllStatusForHistoryStatus(statusSecMap,statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
											}
										}
									}
								}catch(Exception e){}
								for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyForJobs){
									if(jft.getJobId().getJobId()==teacherStatusHistoryForJob.getJobOrder().getJobId()){
										if(teacherStatusHistoryForJob.getStatusMaster()!=null){
											String key=teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0";
											List<TeacherStatusHistoryForJob> jobsHAll=statusSecJFTMap.get(key);
											if(jobsHAll==null){
												List<TeacherStatusHistoryForJob> jobsH= new ArrayList<TeacherStatusHistoryForJob>();
												jobsH.add(teacherStatusHistoryForJob);
												statusSecJFTMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",jobsH);
											}else{
												jobsHAll.add(teacherStatusHistoryForJob);
												statusSecJFTMap.put(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0",jobsHAll);
											}
										}else{
											String key="0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId();
											List<TeacherStatusHistoryForJob> jobsHAll=statusSecJFTMap.get(key);
											if(jobsHAll==null){
												List<TeacherStatusHistoryForJob> jobsH= new ArrayList<TeacherStatusHistoryForJob>();
												jobsH.add(teacherStatusHistoryForJob);
												statusSecJFTMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),jobsH);
											}else{
												jobsHAll.add(teacherStatusHistoryForJob);
												statusSecJFTMap.put("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId(),jobsHAll);
											}
										}
									}
								}
								System.out.println("statusMasterMap::::::::::"+statusMasterMap.size());
								System.out.println("secondaryStatusMap::::::::::"+secondaryStatusMap.size());
								System.out.println("statusSecJFTMap::::::::::"+statusSecJFTMap.size());
	
								boolean existJobOrder=false;
								StatusMaster statusMasterForJFT=null;
								SecondaryStatus secondaryStatusForJFT=null;
	
								ArrayList<String> keys = new ArrayList<String>(statusNameMap.keySet());
								List<TeacherStatusHistoryForJob> isertJobHList=new ArrayList<TeacherStatusHistoryForJob>();
								for(int index=keys.size()-1; index>=0;index--){
									if(secondaryStatusMap.get(keys.get(index))!=null){
										List<TeacherStatusHistoryForJob> jobsHist=statusSecJFTMap.get(keys.get(index));
										if(jobsHist!=null)
											for (TeacherStatusHistoryForJob jobs2 : jobsHist) {
														isertJobHList.add(jobs2);
														if(secondaryStatusForJFT==null){
															existJobOrder=true;
															secondaryStatusForJFT=secondaryStatusMap.get(keys.get(index));
															System.out.println("secondaryStatusForJFT::::::::::::"+secondaryStatusForJFT.getSecondaryStatusId());
														}
														break;
											}	
									}
									else if(statusMasterMap.get(keys.get(index))!=null){
										List<TeacherStatusHistoryForJob> jobsHist=statusSecJFTMap.get(keys.get(index));
										if(jobsHist!=null)
											for (TeacherStatusHistoryForJob jobs2 : jobsHist) {
													isertJobHList.add(jobs2);
													System.out.println("C 2");
													if(statusMasterForJFT==null){
														existJobOrder=true;
														System.out.println("S 2");
														statusMasterForJFT=statusMasterMap.get(keys.get(index));
														System.out.println("statusMasterForJFT::::::::::::::::::::::"+statusMasterForJFT.getStatusId());
													}
													break;
											}
	
									}
								}
								
								Map<String,TeacherStatusHistoryForJob> filterMap=new HashMap<String, TeacherStatusHistoryForJob>();
								if(isertJobHList.size()>0)
									for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : isertJobHList) {
										String key="";
										if(teacherStatusHistoryForJob.getStatusMaster()!=null){
											key=teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"#"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##0";
										}else{
											key="0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId()+"#"+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"#"+teacherStatusHistoryForJob.getJobOrder().getJobId();
										}
										filterMap.put(key,teacherStatusHistoryForJob);
									}
									
									if(existJobOrder){
							        	if(isertJobHList.size()>0){
								        	for (TeacherStatusHistoryForJob tsHForJob : isertJobHList) {
								        		boolean insertFlag=false;
								        		
								        		String key="";
												if(tsHForJob.getStatusMaster()!=null){
													key=tsHForJob.getJobOrder().getJobId()+"##"+tsHForJob.getStatusMaster().getStatusId()+"##0";
												}else{
													key=tsHForJob.getJobOrder().getJobId()+"##0"+tsHForJob.getSecondaryStatus().getSecondaryStatusId();
												}
												if(historyMap.get(key)==null){
									        		if(tsHForJob.getStatusMaster()!=null){
									        			
									        			if(statusSecMap.get(tsHForJob.getStatusMaster().getStatusId()+"##0")!=null){
									        				insertFlag=true;
										        		}
									        		}else if(statusSecMap.get("0##"+tsHForJob.getSecondaryStatus().getSecondaryStatusId())!=null){
									        			insertFlag=true;
									        		}
												}
								        		
								        		if(insertFlag){
								            		try {
								            			Calendar c = Calendar.getInstance();
														c.add(Calendar.SECOND,01);
														Date date = c.getTime();
								            			TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
														tSHJ.setTeacherDetail(teacherDetail);
														tSHJ.setJobOrder(jft.getJobId());
														if(tsHForJob.getStatusMaster()!=null){
															tSHJ.setStatusMaster(tsHForJob.getStatusMaster());
														}
														if(tsHForJob.getSecondaryStatus()!=null){
															tSHJ.setSecondaryStatus(tsHForJob.getSecondaryStatus());
														}
														tSHJ.setStatus(tsHForJob.getStatus());
														tSHJ.setCreatedDateTime(date);
														tSHJ.setUserMaster(tsHForJob.getUserMaster());
														tSHJ.setOverride(tsHForJob.getOverride());
														tSHJ.setOverrideBy(tsHForJob.getOverrideBy());
														teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
														mailFlag = true;
														//statelesSsession.insert(tSHJ);
														//txOpen.commit();
													} catch (Exception e) {
														e.printStackTrace();
														// TODO: handle exception
													}
							            		}
											}
							        	}
								 }
								 try{
										if(existJobOrder){	
										 	boolean selectedstatus=false;
											boolean selectedSecondaryStatus=false;
											boolean []status=getStatusJFT(statusMasterForJFT,secondaryStatusForJFT,jft);
											selectedstatus=status[0];
											selectedSecondaryStatus=status[1];
											System.out.println("::::::selectedstatus::::::::;;>>>>>>>>>>>>>>>>>>>>>>>>>>>;>>"+selectedstatus);
											System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);
											if(selectedstatus || selectedSecondaryStatus){
								        		if(secondaryStatusForJFT!=null || statusMasterForJFT!=null){
										        	if((secondaryStatusForJFT!=null && statusMasterForJFT==null) || (statusMasterForJFT!=null && secondaryStatusForJFT!=null && cgService.priSecondaryStatusCheck(statusMasterForJFT,secondaryStatusForJFT))){
														jft.setStatusMaster(null);
														jft.setSecondaryStatus(secondaryStatusForJFT);
														statusName = secondaryStatusForJFT.getSecondaryStatusName();
														jft.setLastActivity(statusName);
														jft.setLastActivityDate(new Date());
													}else{
														jft.setStatus(statusMasterForJFT);
														jft.setStatusMaster(statusMasterForJFT);
														statusName = Utility.getLocaleValuePropByKey("lblSPStatus", locale);
														jft.setLastActivity(statusMasterForJFT.getStatus());
														jft.setLastActivityDate(new Date());
													}
										        	jobForTeacherDAO.updatePersistent(jft);
								        		}
											}
										 }else if(!existJobOrder){
								        		if(secondaryStatusForJFT!=null || statusMasterForJFT!=null){
										        	if((secondaryStatusForJFT!=null && statusMasterForJFT==null) || (statusMasterForJFT!=null && secondaryStatusForJFT!=null && cgService.priSecondaryStatusCheck(statusMasterForJFT,secondaryStatusForJFT))){
														jft.setStatusMaster(null);
														jft.setSecondaryStatus(secondaryStatusForJFT);
														statusName = secondaryStatusForJFT.getSecondaryStatusName();
													}else{
														jft.setStatus(statusMasterForJFT);
														jft.setStatusMaster(statusMasterForJFT);
														statusName = Utility.getLocaleValuePropByKey("lblSPStatus", locale);
													}
										        	jft.setLastActivity(statusName);
													jft.setLastActivityDate(new Date());
										        	jobForTeacherDAO.updatePersistent(jft);
								        		}else if(secSt!=null){
								        		 	boolean selectedstatus=false;
													boolean selectedSecondaryStatus=false;
													boolean []status=getStatusJFT(secSt.getStatusMaster(),secSt,jft);
													selectedstatus=status[0];
													selectedSecondaryStatus=status[1];
													System.out.println("::::::selectedstatus::::::::;;>>>>>>>>>>>>>>>>>>>>>>>>>>>;>>"+selectedstatus);
													System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);
													if(selectedstatus){
														 System.out.println("Else Here Double :::::::::::::::::::");
														 jft.setStatus(secSt.getStatusMaster());
														 jft.setStatusMaster(secSt.getStatusMaster());
														 jft.setLastActivity(secSt.getStatusMaster().getStatus());
														 jft.setLastActivityDate(new Date());
														 jobForTeacherDAO.updatePersistent(jft);
													}
											 }
										 }
						        	}catch(Exception e){
						        		e.printStackTrace();
						        	}
						        	
						        	if((!existJobOrder) && secondaryStatusForJFT==null && statusMasterForJFT==null){
						        		if(secondarysMap.containsKey(parentCategoryId)
						                        && secSt!=null && secSt.getStatusMaster()!=null
						                        && (secSt.getStatusMaster().getStatusShortName().equalsIgnoreCase("scomp")
						                          || secSt.getStatusMaster().getStatusShortName().equalsIgnoreCase("ecomp")
						                          || secSt.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp")
						                          || secSt.getStatusMaster().getStatusShortName().equalsIgnoreCase("hird")
						                          || secSt.getStatusMaster().getStatusShortName().equalsIgnoreCase("rem")
						                          || secSt.getStatusMaster().getStatusShortName().equalsIgnoreCase("widrw"))){
								                       statusName = Utility.getLocaleValuePropByKey("lblSPStatus", locale); 
								                       mainSatus = true;
						                      }else if(secondarysMap.containsKey(parentCategoryId)){
						                    	  statusName = secSt.getSecondaryStatusName();
						                      }
						                      if(secSt!=null){
							                       TeacherStatusHistoryForJob teacherStatusHistoryForJob = new TeacherStatusHistoryForJob();
							                       teacherStatusHistoryForJob.setJobOrder(jft.getJobId());
							                       teacherStatusHistoryForJob.setCreatedDateTime(new Date());
							                       teacherStatusHistoryForJob.setTeacherDetail(teacherDetail);
							                       if(mainSatus){
							                        teacherStatusHistoryForJob.setStatusMaster(secSt.getStatusMaster());
							                        teacherStatusHistoryForJob.setSecondaryStatus(null);
							                       }else{
							                        teacherStatusHistoryForJob.setStatusMaster(null);
							                        teacherStatusHistoryForJob.setSecondaryStatus(secSt);
							                       }
							                       teacherStatusHistoryForJob.setStatus("S");
							                       	UserMaster userMaster = new UserMaster();
							                       	try {
														userMaster.setUserId(jft.getJobId().getCreatedBy());
													} catch (Exception e) {
														e.printStackTrace();
													}
							                       teacherStatusHistoryForJob.setUserMaster(userMaster);
							                       teacherStatusHistoryForJobDAO.makePersistent(teacherStatusHistoryForJob);
							                       mailFlag = true;
							                       //statelesSsession.insert(teacherStatusHistoryForJob);
						                      }
	
						        	}
								//txOpen.commit();
							}
							
							try 
							{/*
								if(mailFlag){
								String sEmailBodyText = MailText.statusNoteSendToBranchAdmin(jft.getJobId(),teacherDetail,statusName);
								String sEmailSubject="Status Change";
								String toEmail ="";
								List<String> emailList = branchAdminEmails.get(jft.getJobId().getBranchMaster().getBranchId());
								if(emailList!=null && emailList.size()>0){
									for(String s : emailList){
										DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
										dsmt.setEmailerService(emailerService);
										dsmt.setMailfrom(Utility.getValueOfPropByKey("smtphost.noreplyusername"));
										dsmt.setMailto(s);
										dsmt.setMailsubject(sEmailSubject);
										dsmt.setMailcontent(sEmailBodyText);
	
										try {
											dsmt.start();	
										} catch (Exception e) {}
									}
								}
								}
							 */}
							catch(Exception e){e.printStackTrace();}
					}
					//statelesSsession.close();
					teacherStatusJob.clear();
					appliedJobByTeacher.clear();
					jobCatList.clear();
					jobOrderList.clear();
					sortedSecondaryStatus.clear();
					secondaryStatusList.clear();
					listBranches.clear();
					listUsers.clear();
					secondarysMap.clear();
				}

			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		//Auto-Status Pre-Hire Smart Practices End
		return null;
	}
	public boolean[] getStatusJFT(StatusMaster statusMasterForJFT,SecondaryStatus secondaryStatusForJFT,JobForTeacher jft){
		boolean returnVal[]=new boolean[2];
		CandidateGridService cgService = new CandidateGridService();
		boolean selectedstatus=false;
		boolean selectedSecondaryStatus=false;
		try{
			if(jft.getStatus()!=null && statusMasterForJFT!=null){
				selectedstatus=cgService.selectedStatusCheck(statusMasterForJFT,jft.getStatus());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		try{
			if(secondaryStatusForJFT!=null && jft.getStatus()!=null &&  statusMasterForJFT!=null){
				if(jft.getSecondaryStatus()!=null){
					selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatusForJFT,jft.getSecondaryStatus());
					if(selectedSecondaryStatus==false){
						selectedstatus=false;
					}
				}
			}else if(jft.getSecondaryStatus()!=null && secondaryStatusForJFT!=null && statusMasterForJFT==null){
				if(jft.getStatus()!=null){
					if(cgService.priSecondaryStatusCheck(jft.getStatus(),secondaryStatusForJFT)){
						selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatusForJFT,jft.getSecondaryStatus());
					}
				}
			}else if(jft.getSecondaryStatus()==null && secondaryStatusForJFT!=null && statusMasterForJFT==null){
				if(jft.getStatus()!=null){
					if(cgService.priSecondaryStatusCheck(jft.getStatus(),secondaryStatusForJFT)){
						selectedSecondaryStatus=true;
					}
				}
			}
			returnVal[0]=selectedstatus;
			returnVal[1]=selectedSecondaryStatus;
		}catch(Exception e){
			e.printStackTrace();
		}
		return returnVal;
	}
	
	@Transactional
	public String firstTimeJobApply(JobOrder jobOrder,TeacherDetail teacherDetail,JobForTeacher jobForTeacher){
		System.out.println(":::::::::::::: First Time Job Apply ::::::::::::::");
		try{
			DistrictMaster districtMaster=jobOrder.getDistrictMaster();
			List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges= new ArrayList<JobCategoryWiseStatusPrivilege>();
			
			try{
				jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.findstatusForFirstTimeJobApply(jobOrder);
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println(":::jobCategoryWiseStatusPrivileges::::::::::::::::::::::>>>>"+jobCategoryWiseStatusPrivileges.size());
			if(jobCategoryWiseStatusPrivileges.size()>0){
				Map<String,String> statusSecMap=new HashMap<String,String>();
				Map<String,String> statusSecUpdateStatusMap=new HashMap<String,String>();
				
				for (JobCategoryWiseStatusPrivilege jobPri : jobCategoryWiseStatusPrivileges){
					if(jobPri.getStatusMaster()!=null){
						statusSecUpdateStatusMap.put(jobPri.getStatusMaster().getStatusId()+"##0", jobPri.getStatusForFirstTimeJobApply()+"");
						statusSecMap.put(jobPri.getStatusMaster().getStatusId()+"##0",jobPri.getStatusMaster().getStatus());
					}else{
						statusSecUpdateStatusMap.put("0##"+jobPri.getSecondaryStatus().getSecondaryStatusId(), jobPri.getStatusForFirstTimeJobApply()+"");
						statusSecMap.put("0##"+jobPri.getSecondaryStatus().getSecondaryStatusId(),jobPri.getSecondaryStatus().getSecondaryStatusName());
					}
				}
				
				System.out.println("::::::::::::statusSecUpdateStatusMap::::::::::"+statusSecUpdateStatusMap.size());
				Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
				Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
				CandidateGridService cgService = new CandidateGridService();
				List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
				LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
				Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
				
				try{
					int counter=0; 
					for(SecondaryStatus tree: lstTreeStructure)
					{
						if(tree.getSecondaryStatus()==null)
						{
							if(tree.getChildren().size()>0){
								counter=cgService.getAllStatusForHistoryStatus(statusSecMap,statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
							}
						}
					}
				}catch(Exception e){}
				
				System.out.println("statusMasterMap 1111::::::::::"+statusMasterMap.size());
				System.out.println("secondaryStatusMap 1111::::::::::"+secondaryStatusMap.size());
				
				boolean existJobOrder=false;
				ArrayList<String> keys = new ArrayList<String>(statusNameMap.keySet());
				System.out.println(":::::: keys.size() ::::::::"+keys.size());
				int counter = 0;
				for(int i=keys.size()-1; i>=0;i--){
					StatusMaster statusMasterForJFT=null;
					SecondaryStatus secondaryStatusForJFT=null;
		        	String firstTimeApply=statusSecUpdateStatusMap.get(keys.get(i));
	        		System.out.println("firstTimeApply:::"+firstTimeApply);
			        	if(statusMasterMap.get(keys.get(i))!=null){
			        		counter = counter+1;
			        		System.out.println("::::::::::::::S:::::::::::::::"+keys.get(i));
			        		// Get Status For First Time Job Apply
			        		if(firstTimeApply!=null && !firstTimeApply.equals("")){
		        				if(firstTimeApply.equals("0")){
        							if(statusMasterForJFT==null){
	        							statusMasterForJFT=statusMasterMap.get(keys.get(i));
        							}
        							//break;
		        				}else if(firstTimeApply.equals("1")){
	    							if(statusMasterForJFT==null){
	        							statusMasterForJFT=statusMasterMap.get(keys.get(i));
	    							}
	    							//break;
		        				}
		        			}
			        	
		        	} else if(secondaryStatusMap.get(keys.get(i))!=null){
		        		System.out.println("SS:::::::::::::::"+keys.get(i));
		        		counter = counter+1;
	        			// Get Secondary Status For First Time Job Apply
	        			if(firstTimeApply!=null && !firstTimeApply.equals("")){
	        				if(firstTimeApply.equals("0")){
    							if(secondaryStatusForJFT==null){
    								secondaryStatusForJFT=secondaryStatusMap.get(keys.get(i));
    							}
    							//break;
	        				}else if(firstTimeApply.equals("1")){
    							if(secondaryStatusForJFT==null){
    								secondaryStatusForJFT=secondaryStatusMap.get(keys.get(i));
    							}
    							//break;
	        				}
	        			}
		        	 }
			        	
			        	//if(districtMaster!=null && districtMaster.getDistrictId()==1200390)
			        	{
			        		System.out.println("::::::::::::DistrictId AutoStatus:::::::::"+districtMaster.getDistrictId());
			        		SessionFactory sessionFactory=teacherStatusHistoryForJobDAO.getSessionFactory();
							StatelessSession statelesSsession = sessionFactory.openStatelessSession();
							Transaction txOpen =statelesSsession.beginTransaction();
				        	try {
		            			Calendar c = Calendar.getInstance();
								c.add(Calendar.SECOND,01);
								Date date = c.getTime();
		            			TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
								tSHJ.setTeacherDetail(teacherDetail);
								tSHJ.setJobOrder(jobOrder);
								tSHJ.setStatusMaster(statusMasterForJFT);
								tSHJ.setSecondaryStatus(secondaryStatusForJFT);
								tSHJ.setStatus("S");
								tSHJ.setCreatedDateTime(new Date());
							 	UserMaster userMaster = new UserMaster();
		                       	try {
									userMaster.setUserId(jobOrder.getCreatedBy());
								} catch (Exception e) {
									e.printStackTrace();
								}
								tSHJ.setUserMaster(userMaster);
								teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
								//statelesSsession.insert(tSHJ);
							} catch (Exception e) {
								// TODO: handle exception
								e.printStackTrace();
							}finally{
								System.out.println(" KKKKKKKKKKK firstTimeApply ::::::::: "+firstTimeApply);
					        	
					        	//For vvi email when auto status on
					        	if(firstTimeApply!=null && !firstTimeApply.equals("")){
					        		if(firstTimeApply.equals("1")){
										try{
											System.out.println(" ::::::::: first Time Apply ::::::::: "+firstTimeApply);
											ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
											cGInviteInterviewAjax = (CGInviteInterviewAjax)context0.getBean("cGInviteInterviewAjax");
											try{
												System.out.println(" 01 cGInviteInterviewAjax :::: "+cGInviteInterviewAjax);	
											}catch(Exception e){e.printStackTrace();}
											if(jobOrder!=null && jobOrder.getI4QuestionSets()!=null && jobOrder.getI4QuestionSets().getID()!=null)
											cGInviteInterviewAjax.saveIIStatus(jobOrder.getJobId().toString(), teacherDetail.getTeacherId().toString(), jobOrder.getI4QuestionSets().getID().toString());
										}catch(Exception e){
											e.printStackTrace();
										}
						        	}
					        	}
							}

							if(counter==1){
								JobForTeacher jobForTeacherNew = jobForTeacherDAO.findById(jobForTeacher.getJobForTeacherId(), false, false);
								try{
					        		if(secondaryStatusForJFT!=null || statusMasterForJFT!=null){
							        	if((secondaryStatusForJFT!=null && statusMasterForJFT==null) || (statusMasterForJFT!=null && secondaryStatusForJFT!=null && cgService.priSecondaryStatusCheck(statusMasterForJFT,secondaryStatusForJFT))){
							        		jobForTeacherNew.setStatusMaster(null);
							        		jobForTeacherNew.setSecondaryStatus(secondaryStatusForJFT);
										}else{
											jobForTeacherNew.setStatus(statusMasterForJFT);
											jobForTeacherNew.setStatusMaster(statusMasterForJFT);
										}
							        	jobForTeacherDAO.updatePersistent(jobForTeacherNew);
							        	//statelesSsession.update(jobForTeacher);
							        }
					        	}catch(Exception e){
					        		e.printStackTrace();
					        	}
							  }
			        	}
 				   }
			     }
		}catch(Exception exception){
			exception.printStackTrace();
		}
		
		return null;
	}
	
	public void statusUpdate(JobForTeacher jft,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster){
		
		System.out.println(":::::::::::::::::::::statusUpdate::::::::::::::::::");
		CandidateGridService cgService=new CandidateGridService();
		 List<TeacherStatusHistoryForJob> historyForJoblist=null;
		 TeacherStatusHistoryForJob historyForJob=null;
		 TeacherDetail teacherDetail=jft.getTeacherId();
		 JobOrder jobOrder=jft.getJobId();
		 try{
			    boolean selectedstatus=false;
			    boolean selectedSecondaryStatus=false;
			    try{
			        if(jft.getStatus()!=null && statusMaster!=null){
			            selectedstatus=cgService.selectedStatusCheck(statusMaster,jft.getStatus());
			        }
			    }catch(Exception e){
			        e.printStackTrace();
			    }
			    try{
			        if(secondaryStatus!=null && jft.getStatus()!=null &&  statusMaster!=null){// && (jobForTeacherObj.getStatus().getStatusShortName().equals("scomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("ecomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("vcomp"))
			        if(jft.getSecondaryStatus()!=null){
			            selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatus,jft.getSecondaryStatus());
			            if(selectedSecondaryStatus==false){
			                selectedstatus=false;
			            }
			        }
			    }else if(jft.getSecondaryStatus()!=null && secondaryStatus!=null && statusMaster==null){
			        if(jft.getStatus()!=null){
			            if(cgService.priSecondaryStatusCheck(jft.getStatus(),secondaryStatus)){
			                selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jft.getSecondaryStatus());
			            }
			        }
			    }else if(jft.getSecondaryStatus()==null && secondaryStatus!=null && statusMaster==null){
			        if(jft.getStatus()!=null){
			            if(cgService.priSecondaryStatusCheck(jft.getStatus(),secondaryStatus)){
			                selectedSecondaryStatus=true;
			            }
			        }
			    }
			}catch(Exception e){
			    e.printStackTrace();
			}
			System.out.println("::::::selectedstatus::::::::;;>>>>>>>>>>>>>>>>>>>>>>>>>>>;>>"+selectedstatus);
			System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);
			
		    historyForJoblist=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryByStatus(jft.getTeacherId(),jft.getJobId(),statusMaster,secondaryStatus);
		    System.out.println("historyForJoblist::::"+historyForJoblist.size());
			if(statusMaster!=null){
				if(selectedstatus){
					jft.setStatusMaster(statusMaster);
					jft.setStatus(statusMaster);
					jobForTeacherDAO.updatePersistent(jft);
				}
					    //updating History Table	
				if(historyForJoblist!=null && historyForJoblist.size()>0){
				}else{
					historyForJob=new TeacherStatusHistoryForJob();
					historyForJob.setJobOrder(jobOrder);
					historyForJob.setTeacherDetail(teacherDetail);
					historyForJob.setStatus("S");
					historyForJob.setSecondaryStatus(secondaryStatus);
					historyForJob.setUserMaster(userMaster);
					historyForJob.setCreatedDateTime(new Date());
					teacherStatusHistoryForJobDAO.makePersistent(historyForJob);
				}	
			}
				
		    if(secondaryStatus!=null){
				if(selectedSecondaryStatus){
					jft.setStatusMaster(null);
					jft.setSecondaryStatus(secondaryStatus);
					jobForTeacherDAO.updatePersistent(jft);
				}
				//updating History Table	
				if(historyForJoblist!=null && historyForJoblist.size()>0){
				}else{
					historyForJob=new TeacherStatusHistoryForJob();
					historyForJob.setJobOrder(jobOrder);
					historyForJob.setTeacherDetail(teacherDetail);
					historyForJob.setStatus("S");
					historyForJob.setSecondaryStatus(secondaryStatus);
					historyForJob.setUserMaster(userMaster);
					historyForJob.setCreatedDateTime(new Date());
					teacherStatusHistoryForJobDAO.makePersistent(historyForJob);
			  }	
		 }
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	}
	
	public static int getRequiredReferenceCount(JobForTeacher jobForTeacher,Map<String,Integer> referenceMap)
	{
		try
		{
			if(jobForTeacher.getIsAffilated()!=null && jobForTeacher.getIsAffilated()==1)  // 1=Internal Candidate
			{
				if(referenceMap.get(StringKey.internal)!=null)
					return referenceMap.get(StringKey.internal);
				else
					return referenceMap.get(StringKey.districtInternal);
			}
			else   //external
			{
				if(referenceMap.get(StringKey.external)!=null)
					return referenceMap.get(StringKey.external);
				else
					return referenceMap.get(StringKey.districtExternal);
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return 0;
	}
	

}
