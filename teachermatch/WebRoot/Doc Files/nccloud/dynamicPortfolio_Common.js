var digitToString=new Array("","one","two","three","four","five","six","seven","eight","nine","ten");
var dspqCalInstance=null;
var isJobAplied=false;
var jeffcoNotApplied =true;

/*========= Start ... Paging and Sorting ===========*/
var dp_Academics_page = 1;
var dp_Academics_noOfRows = 100;
var dp_Academics_sortOrderStr="";
var dp_Academics_sortOrderType="";

var dp_Certification_page = 1;
var dp_Certification_noOfRows = 10;
var dp_Certification_sortOrderStr="";
var dp_Certification_sortOrderType="";

var dp_Reference_page = 1;
var dp_Reference_noOfRows = 10;
var dp_Reference_sortOrderStr="";
var dp_Reference_sortOrderType="";

var dp_AdditionalDocuments_page = 1;
var dp_AdditionalDocuments_noOfRows = 10;
var dp_AdditionalDocuments_sortOrderStr="";
var dp_AdditionalDocuments_sortOrderType="";

var dp_Employment_page=1;
var dp_Employment_noOfRows=100;
var dp_Employment_sortOrderStr="";
var dp_Employment_sortOrderType="";

var dp_SubjectArea_page = 1;
var dp_SubjectArea_noOfRows = 10;
var dp_SubjectArea_sortOrderStr="";
var dp_SubjectArea_sortOrderType="";

var dp_VideoLink_Rows=10
var dp_VideoLink_page=1
var dp_VideoLink_sortOrderStr=""
var dp_VideoLink_sortOrderType=""

var dp_StdTchrExp_Rows=10;
var dp_StdTchrExp_page=1;
var dp_StdTchrExp_sortOrderStr="";
var dp_StdTchrExp_sortOrderType="";

var dp_Residency_Rows=10;
var dp_Residency_page=1;
var dp_Residency_sortOrderStr="";
var dp_Residency_sortOrderType="";

var dp_Involvement_Rows=10;
var dp_Involvement_page=1;
var dp_Involvement_sortOrderStr="";
var dp_Involvement_sortOrderType="";

var dp_TchrLang_Rows=10;
var dp_TchrLang_page=1;
var dp_TchrLang_sortOrderStr="";
var dp_TchrLang_sortOrderType="";
var txtBgColor="#F5E7E1";

function getPaging(pageno)
{
	var gridNameFlag = document.getElementById("gridNameFlag").value;
	
	if(gridNameFlag=="subjectAreas"){
		if(pageno!='')
		{
			dp_SubjectArea_page=pageno;	
		}
		else
		{
			dp_SubjectArea_page=1;
		}
		dp_SubjectArea_noOfRows = document.getElementById("pageSize").value;
		showGridSubjectAreasGrid();
	}
	else if(gridNameFlag=="additionalDocuments"){
		if(pageno!='')
		{
			dp_AdditionalDocuments_page=pageno;	
		}
		else
		{
			dp_AdditionalDocuments_page=1;
		}
		dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize").value;
		showGridAdditionalDocuments();
	}else if(gridNameFlag=="academics"){
		if(pageno!='')
		{
			dp_Academics_page=pageno;	
		}
		else
		{
			dp_Academics_page=1;
		}
		dp_Academics_noOfRows = document.getElementById("pageSize").value;
		showGridAcademics();
	}else if(gridNameFlag=="certification"){
		if(pageno!='')
		{
			dp_Certification_page=pageno;	
		}
		else
		{
			dp_Certification_page=1;
		}
		dp_Certification_noOfRows = document.getElementById("pageSize3").value;
		showGridCertifications();
	}else if(gridNameFlag=="reference"){
		if(pageno!='')
		{
			dp_Reference_page=pageno;	
		}
		else
		{
			dp_Reference_page=1;
		}
		dp_Reference_noOfRows = document.getElementById("pageSize").value;
		getElectronicReferencesGrid();
	}else if(gridNameFlag=="workExperience"){
		if(pageno!='')
		{
			dp_Employment_page=pageno;	
		}
		else
		{
			dp_Employment_page=1;
		}
		dp_Employment_noOfRows = document.getElementById("pageSize").value;
		getPFEmploymentDataGrid();
	}else if(gridNameFlag=="videolinks"){
		if(pageno!='')
		{
			dp_VideoLink_page=pageno;	
		}
		else
		{
			dp_VideoLink_page=1;
		}
		dp_VideoLink_Rows = document.getElementById("pageSize").value;
		getVideoLinksGrid();
	}else if(gridNameFlag=="stdTchrGrid"){
		if(pageno!='')
		{
			dp_StdTchrExp_page=pageno;	
		}
		else
		{
			dp_StdTchrExp_page=1;
		}
		dp_StdTchrExp_Rows = document.getElementById("pageSize").value;
		displayStdTchrExp();
	}
	else if(gridNameFlag=="involvement"){
		if(pageno!='')
		{
			dp_Involvement_page=pageno;	
		}
		else
		{
			dp_Involvement_page=1;
		}
		dp_Involvement_Rows = document.getElementById("pageSize").value;
		getInvolvementGrid();
	}else if(gridNameFlag=="language"){
		if(pageno!='')
		{
			dp_TchrLang_page=pageno;	
		}
		else
		{
			dp_TchrLang_page=1;
		}
		dp_TchrLang_Rows = document.getElementById("pageSize").value;
		displayTchrLanguage();
	}else if(gridNameFlag=="residency"){
		if(pageno!='')
		{
			dp_Residency_page=pageno;	
		}
		else
		{
			dp_Residency_page=1;
		}
		dp_Residency_Rows = document.getElementById("residencyGridPager").value;
		getResidencyGrid();
	}
	
	//videolinks
	
}

function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	var gridNameFlag = document.getElementById("gridNameFlag").value;
	if(gridNameFlag=="subjectAreas")
	{
		if(pageno!=''){
			dp_SubjectArea_page=pageno;	
		}else{
			dp_SubjectArea_page=1;
		}
		dp_SubjectArea_sortOrderStr	=	sortOrder;
		dp_SubjectArea_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_SubjectArea_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_SubjectArea_noOfRows=10;
		}
		showGridSubjectAreasGrid();
	}
	else if(gridNameFlag=="additionalDocuments"){
		if(pageno!=''){
			dp_AdditionalDocuments_page=pageno;	
		}else{
			dp_AdditionalDocuments_page=1;
		}
		dp_AdditionalDocuments_sortOrderStr	=	sortOrder;
		dp_AdditionalDocuments_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_AdditionalDocuments_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_AdditionalDocuments_noOfRows=10;
		}
		showGridAdditionalDocuments();
	}else if(gridNameFlag=="academics"){
		if(pageno!=''){
			dp_Academics_page=pageno;	
		}else{
			dp_Academics_page=1;
		}
		dp_Academics_sortOrderStr	=	sortOrder;
		dp_Academics_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Academics_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Academics_noOfRows=10;
		}
		showGridAcademics();
	}else if(gridNameFlag=="certification"){
		if(pageno!=''){
			dp_Certification_page=pageno;	
		}else{
			dp_Certification_page=1;
		}
		dp_Certification_sortOrderStr	=	sortOrder;
		dp_Certification_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize3")!=null){
			dp_Certification_noOfRows = document.getElementById("pageSize3").value;
		}else{
			dp_Certification_noOfRows=10;
		}
		showGridCertifications();
	}else if(gridNameFlag=="reference"){
		if(pageno!=''){
			dp_Reference_page=pageno;	
		}else{
			dp_Reference_page=1;
		}
		dp_Reference_sortOrderStr	=	sortOrder;
		dp_Reference_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Reference_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Reference_noOfRows=10;
		}
		getElectronicReferencesGrid();
	}else if(gridNameFlag=="workExperience"){
		if(pageno!=''){
			dp_Employment_page=pageno;	
		}else{
			dp_Employment_page=1;
		}
		dp_Employment_sortOrderStr	=	sortOrder;
		dp_Employment_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Employment_noOfRows = document.getElementById("pageSize1").value;
		}else{
			dp_Employment_noOfRows=10;
		}
		getPFEmploymentDataGrid();
	}else if(gridNameFlag=="videolinks"){

		if(pageno!=''){
			dp_VideoLink_page=pageno;	
		}else{
			dp_VideoLink_page=1;
		}
		dp_VideoLink_sortOrderStr	=	sortOrder;
		dp_VideoLink_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_VideoLink_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_VideoLink_Rows=10;
		}
		getVideoLinksGrid();
	
	}else if(gridNameFlag=="stdTchrGrid"){

		if(pageno!=''){
			dp_StdTchrExp_page=pageno;	
		}else{
			dp_StdTchrExp_page=1;
		}
		dp_StdTchrExp_sortOrderStr	=	sortOrder;
		dp_StdTchrExp_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_StdTchrExp_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_StdTchrExp_Rows=10;
		}
		displayStdTchrExp();
	
	}
	else if(gridNameFlag=="involvement"){
		if(pageno!=''){
			dp_Involvement_page=pageno;	
		}else{
			dp_Involvement_page=1;
		}
		dp_Involvement_sortOrderStr	=	sortOrder;
		dp_Involvement_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_Involvement_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_Involvement_Rows=10;
		}
		getInvolvementGrid();	
	}else if(gridNameFlag=="language"){

		if(pageno!=''){
			dp_TchrLang_page=pageno;	
		}else{
			dp_TchrLang_page=1;
		}
		dp_TchrLang_sortOrderStr	=	sortOrder;
		dp_TchrLang_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("pageSize1")!=null){
			dp_TchrLang_Rows = document.getElementById("pageSize1").value;
		}else{
			dp_TchrLang_Rows=10;
		}
		displayTchrLanguage();
	
	}else if(gridNameFlag=="residency"){

		if(pageno!=''){
			dp_Residency_page=pageno;	
		}else{
			dp_Residency_page=1;
		}
		dp_Residency_sortOrderStr	=	sortOrder;
		dp_Residency_sortOrderType	=	sortOrderTyp;
		if(document.getElementById("residencyGridPager")!=null){
			dp_Residency_Rows = document.getElementById("residencyGridPager").value;
		}else{
			dp_Residency_Rows=10;
		}
		getResidencyGrid();
	
	}
}

/*function getPagingAndSorting(pageno,sortOrder,sortOrderTyp)
{
	if(pageno!=''){
		dynamicPortfolio_Academics_page=pageno;	
	}else{
		dynamicPortfolio_Academics_page=1;
	}
	dp_Academics_sortOrderStr=sortOrder;
	dp_Academics_sortOrderType=sortOrderTyp;
	if(document.getElementById("pageSize")!=null){
		dp_Academics_noOfRows = document.getElementById("pageSize").value;
	}else{
		dp_Academics_noOfRows=100;
	}
	showGridAcademics();
}*/

/*========= End ... Paging and Sorting ===========*/

function handleError(message, exception)
{
	if(exception.javaClassName=="java.lang.IllegalStateException")
	{alert(resourceJSON.oops);  document.location = 'signin.do';}
	else{alert("Server Error: "+exception.javaClassName);}
}
function trim(s)
{
	while ((s.substring(0,1) == ' ') || (s.substring(0,1) == '\n') || (s.substring(0,1) == '\r')){
		s = s.substring(1,s.length);
	}
	while ((s.substring(s.length-1,s.length) == ' ') || (s.substring(s.length-1,s.length)	== '\n') || (s.substring(s.length-1,s.length) == '\r')){
		s = s.substring(0,s.length-1);
	}
	return s;
}
function isNumber(field) {
	var re = /^[0-9-'.'-',']*$/;
	if (!re.test(field.value)) {
		field.value = field.value.replace(/[^0-9-'.'-',']/g,"");
	}
}
function checkForInt(evt) {

	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
}

function checkForDecimalTwo(evt) {

	var count=true;
	var str=evt.srcElement.value;
	if (str.lastIndexOf('.') == -1) str += ".";
	var decNum = str.substring(str.lastIndexOf('.')+1, str.length);
	var charCode = ( evt.which ) ? evt.which : event.keyCode;
	if(charCode==8)
		return true;

	var parts = evt.srcElement.value.split('.');
    if(parts.length > 1 && charCode==46)
        return false;
    
	return( ((charCode >= 48 || charCode==46)&& count && charCode <= 57)||(charCode==13) );
}

function setFocus(id)
{
	if(id!="")
		document.getElementById(id).focus();
}

function showForm(divFormGrid)
{
	document.getElementById(divFormGrid).style.display="block";
}

function setGridNameFlag(gridName)
{
	document.getElementById("gridNameFlag").value=gridName;
}

function isEmailAddress(str) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
	return emailPattern.test(str);
}

function applyScrollOnTbl_EmployeementHistory()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#employeementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
          colratio:[200,200,150,110,200,105],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTbl_Academic()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#academicGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
        colratio:[185,130,136,144,136,125,100], //144,130,136,185,136,146,97
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
        colratio:[440,416,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}
function applyScrollOnTbl_Certifications()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[300,115,100,200,145,100], //125,90,400,120,80
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblEleRef()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#eleReferencesGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
          colratio:[150,90,120,200,80,90,100,127], //170,100,120,180,80,70,110
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}


function applyScrollOnTbl_AdditionalDocuments()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#additionalDocumentsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
        colratio:[440,416,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}


function applyScrollOnTbl_SubjectAreas()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#subjectAreasGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 957,
        minWidth: null,
        minWidthAuto: false,
        colratio:[200,200,300,157,100],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
        });			
}

function showCLOnCloseOfDP()
{
	if(document.getElementById("noCl"))
	{
		
	}else
	{
		if(isCoverLetterNeeded)
		{
			try { $('#myModalCL').modal('show'); } catch (e) {}
		}
	}
	
	closeDSPQCal();
	$('#iconpophover101').tooltip();
}
var applyFlag=true;
function saveAndContinueDynamicPortfolio(){

	/*if(messageShowOrNot())
	return false;*/
/*if(document.location.hostname=="nccloud.teachermatch.org" || document.location.hostname=="nc.teachermatch.org")	
	{
	var buttonSSNClick=document.getElementById("submitSSN").value;
	if(buttonSSNClick!='ButtonClick')
	{
		
		ssnButtonNotClick("Please click Submit SSN above to ensure that your Licensure and Education information is populated accurately on your application.");
		return false;
	}
	}*/

	
	resetSBTNSource();
	closeDSPQCal();
	$('#loadingDiv_dspq_ie').show();	
	var sbtsource=1;
	var isValidate=true;
	var count = 0;
		

	if($("#residencyStreetAddress").is(':visible'))
	{
		isValidate=false;
		insertOrUpdate_Residency();
	}
	if($("#degreeName").is(':visible'))
	{
		document.getElementById("sbtsource_aca").value=1;
		isValidate=false;
		insertOrUpdate_Academic(sbtsource);
	}	
	if($("#stateMaster").is(':visible'))
	{
		document.getElementById("sbtsource_cert").value=1;
		isValidate=false;
		insertOrUpdate_Certification(sbtsource);
	}
	else
	{
		displayGKAndSubject();
	}
	
	if($("#salutation").is(':visible'))
	{
		document.getElementById("sbtsource_ref").value=1;
		isValidate=false;
		insertOrUpdateElectronicReferences(sbtsource);
	}
	
	if($("#role").is(':visible'))
	{
		document.getElementById("sbtsource_emp").value=1;
		isValidate=false;
		insertOrUpdateEmployment(sbtsource);
	}
	
	
	if($("#examStatus").is(':visible'))
	{
		document.getElementById("sbtsource_subArea").value=1;
		isValidate=false;
		saveSubjectAreas(sbtsource);
	}
	
	
	if($("#documentName").is(':visible'))
	{
		document.getElementById("sbtsource_aadDoc").value=1;
		isValidate=false;
		insertOrUpdate_AdditionalDocuments(sbtsource);
	}
	
	if($("#videourl").is(':visible'))
	{
		document.getElementById("sbtsource_videoLink").value=1;
		isValidate=false;
		insertOrUpdatevideoLinks();
	}
	else
	{
		getVideoLinksGrid();
	}
	
	if($("#schoolNameStdTch").is(':visible'))
	{		
		isValidate=false;
		insertOrUpdateStdTchrExp();
	}
	if($("#languageText").is(':visible'))
	{		
		isValidate=false;
		insertOrUpdateTchrLang();
	}
	if($("#organizationInv").is(':visible'))
	{	isValidate=false;	
		saveOrUpdateInvolvement();
	}
	if($("#honor").is(':visible'))
	{	isValidate=false;	
		saveOrUpdateHonors();
	}
		
	if($("#octCanadaDiv").is(':visible'))
	{
		saveOctUpload();
	}
	if($("#senNumDiv").is(':visible'))
	{
		//Seniority number
		saveSeniorityNumber();
	}
	if($("#languageDiv").is(':visible'))
	{
		try {
			updateLangeuage();
		} catch (e) {
			// TODO: handle exception
		}
		
	}
	saveAndContinueBottomPart();
	
	// validate Dynamic Portfolio
	if(isValidate)
	{
		validatePortfolioErrorMessageAndGridData('level2');
	}

}

function validatePortfolioErrorMessageAndGridData(source){
	
	if(!jeffcoNotApplied){
		$('#loadingDiv_dspq_ie').hide();
		return false;
	}	
	if($("#allowNext").val()=="1")
	{
		var txtDistrictPortfolioConfig=document.getElementById("txtDistrictPortfolioConfig").value
		var isAffilated=0;
		var candidateType="";
		
		try{
			if(document.getElementById("isAffilated")!=null && document.getElementById("isAffilated").checked)
				isAffilated=1;
			else
				isAffilated=document.getElementById("txtCandidateType").value;
		}catch(err){}
		
		if(isAffilated==1)
			candidateType="I";
		else
			candidateType="E";
		
		if(txtDistrictPortfolioConfig > 0)
		{
			if(source=='level1')
			{
				//alert("try to show loadingDiv_dspq_ie from common Internal Job");
				$('#loadingDiv_dspq_ie').show();				
				$('#myModalDymanicPortfolio').modal('show');
			}
			$(".certiGuiText").hide();
			var jobId=document.getElementById("jobId").value;
			DistrictPortfolioConfigAjax.getPortfolioConfigByJobId(jobId,candidateType,{ 
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					if(data!=null)
					{
						if(data.districtMaster!=null && data.districtMaster.districtId==5510470){
							$('.newBerlinCss').show();
						}
						$('#jobcategoryDsp').val(data.jobCategoryName);
						if(data.districtMaster!=null && data.districtMaster.districtId==4218990){
							
							if($('#commonTextVideo').length>0){
								if(data.isNonTeacher==false && data.isSchoolSupportPhiladelphia==false)
									$('#commonTextVideo').html(resourceJSON.msgIncludeURLVideos);
								else
									$('#commonTextVideo').html(resourceJSON.msgIncludeURLVideos);
								
							}
							$(".portfolio_Subheading:contains("+resourceJSON.msgEmploymentHistory+")").html(resourceJSON.msgEmployment);
							if(data.isNonTeacher==true && data.coverLetter==1){
								$('#cvrltrTxt').hide();
								$('.philNT').hide();
								$('.philadelphiaNTCss').show();
								$('#isnontj').val(true);								
							}else if(data.isSchoolSupportPhiladelphia==true && data.coverLetter==1){
								data.coverLetter=0;
								data.coverLetter=0;
								data.videoLink=false;
								data.expCertTeacherTraining=false;
								data.academicTranscript=0;
								data.nationalBoardCert=false;
								data.willingAsSubstituteTeacher=false;
								data.tfaAffiliate=false;
								data.certification=0;
								data.proofOfCertification=0;
								data.additionalDocuments=false;
								data.dateOfBirth=false;
								data.reference=2;
								data.referenceLettersOfRecommendation=0;
								$('#isSchoolSupportPhiladelphia').val("1");
								$('#cvrltrTxt').hide();
								$('.philNT').hide();																
								$(".philadelphiaCss:contains('"+resourceJSON.msgschooldistrictreferences+"')").html(resourceJSON.msgPhiladelphiawith3Ref);									
							}
							
							if(data.isPrinciplePhiladelphia==true || $('input:radio[name=staffType]').is(":checked") || $("#staffTypeSession").val()=="I")//for active principal(SA) & principal jobcategory
							{
								if($('input:radio[name=staffType]').is(":checked") || $("#staffTypeSession").val()=="I"){
									$('#fe2').prop('checked', true);
									$("#fe2Div").show();									
								}
								$(".philadelphiaCss:contains('"+resourceJSON.msgschooldistrictreferences+"')").html(resourceJSON.msgPhiladelphiawith3Ref);

								$("#isPrinciplePhiladelphia").val("1");
								data.reference=2;
								data.referenceLettersOfRecommendation=0;
								data.coverLetter=0;
								////////////////////
								data.academic=0;
								data.academicTranscript=0;
								data.dateOfBirth=0;
								data.employment=0;
								data.expCertTeacherTraining=0;
								data.tfaAffiliate=0;
								data.willingAsSubstituteTeacher=0;
								data.resume=0;
								$("#tfaDistSpecificoption").hide();
							//	data.districtSpecificPortfolioQuestions=0;
								////////////////////
								$('.philadelphiaNTCss1').show();
								$('.philadelphiaNTCss').hide();
								$('#expectedSalaryDiv').hide();
							}
							if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isSchoolSupportPhiladelphia==false && data.isNonTeacher==false){
								$('#divStdTchrExp').show();											
								displayStdTchrExp();
							}
							$('.philadelphiaCss').show();								
							$(".portfolio_Subheading:contains('"+resourceJSON.msgEmploymentHistory+"')").html(resourceJSON.msgEmployment);
							
						}else if(data.districtMaster!=null && data.districtMaster.districtId==3904380){						
							$("#crequired").css("color", "red");
							$("#crequired2").css("color", "red");
							if(data.isNonTeacher==true){
								data.additionalDocuments=false;
							}
						}
						
						if(data.districtMaster!=null && data.districtMaster.districtId==804800){
							$(".additionalDocumentsHeaderText").html(resourceJSON.msgIncludePhilosophyStatement);
							$(".additionalDocumentsHeaderText").show();
						}
						if(data.districtMaster!=null && data.districtMaster.districtId==614730 && data.jobCategoryName=="School Nurse"){
							data.tfaAffiliate= false;
						}
						if(data.academic > 0 || data.academicTranscript > 0 || data.certification > 0 || data.proofOfCertification > 0 || data.reference > 0 || data.referenceLettersOfRecommendation > 0 || data.resume==1 || data.tfaAffiliate==1 || data.willingAsSubstituteTeacher==1 || data.phoneNumber==1 || data.personalinfo==1 || data.dateOfBirth==1 || data.ssn==1 || data.race==1 || data.formeremployee==1 || data.videoLink==1 || data.districtSpecificPortfolioQuestions==1 || data.honors==1 || data.involvement==1 || data.additionalDocuments==1)
						{
							var countConfig_Residency=0;
							var countConfig_Academic=0;
							var countConfig_AcademicTranscript=0;
							var countConfig_Certification=0;
							var countConfig_ProofOfCertification=0;
							var countConfig_Reference=0;
							var countConfig_ReferenceLettersOfRecommendation=0;
							var resume_config=false;
							var tfaAffiliate_config=false;
							var willingAsSubstituteTeacher_config=false;
							var phoneNumber_config=false;
							var address_config=false;
							var exp_config=false;
							var nbc_config=false;
							var affidavit_config=false;
							
							var personalinfo_config=false;
							var dateOfBirth_config=false;
							var ssn_config=false;
							var race_config=false;
							var formeremployee_config=false;
							
							var generalKnowledge_config=false;
							var subjectAreaExam_config=false;
							var additionalDocuments_config=false;
							var veteran_config=false;
							var ethnicOrigin_config=false;
							var ethinicity_config=false;
							var employment_config=false;
							var gender_config=false;
							var retireNo_config=false;
							var videoLink_config=false;
							var dSPQuestions_config=false;
							var involvement_config=false;
							var honors_config=false;
							
							countConfig_Residency=data.residency;
							countConfig_Academic=data.academic;
							countConfig_AcademicTranscript=data.academicTranscript;
							countConfig_Certification=data.certification;
							countConfig_ProofOfCertification=data.proofOfCertification;
							countConfig_Reference=data.reference;
							countConfig_ReferenceLettersOfRecommendation=data.referenceLettersOfRecommendation;
							resume_config=data.resume;
							tfaAffiliate_config=data.tfaAffiliate;
							willingAsSubstituteTeacher_config=data.willingAsSubstituteTeacher;
							phoneNumber_config=data.phoneNumber;
							address_config = data.address;
							exp_config = data.expCertTeacherTraining;
							nbc_config = data.nationalBoardCert;
							affidavit_config = data.affidavit;
							
							personalinfo_config = data.personalinfo;
							dateOfBirth_config = data.dateOfBirth;
							ssn_config = data.ssn;
							race_config = data.race;
							formeremployee_config = data.formeremployee;
							veteran_config = data.veteran;
							ethnicOrigin_config = data.ethnicorigin;
							ethinicity_config = data.ethinicity;
							employment_config = data.employment;
							gender_config = data.genderId;
							
							generalKnowledge_config = data.generalKnowledgeExam;
							subjectAreaExam_config = data.subjectAreaExam;
							additionalDocuments_config = data.additionalDocuments;
							retireNo_config = data.retirementnumber;
							if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isNonTeacher==true){								
								videoLink_config=false;
							}else{
								videoLink_config=data.videoLink;
							}
							
							involvement_config=data.involvement;
							honors_config=data.honors;
							dSPQuestions_config = data.districtSpecificPortfolioQuestions;							
							var IsSIForMiami=data.isSubstituteInstructionalForMiami;
							var isItvtForMiami=data.isInterventionistsForMiami;
							var candidateType  = data.candidateType;
							validateDynamicPortfolio(candidateType,countConfig_Academic,countConfig_AcademicTranscript,countConfig_Certification,countConfig_ProofOfCertification,countConfig_Reference,countConfig_ReferenceLettersOfRecommendation,resume_config,tfaAffiliate_config,willingAsSubstituteTeacher_config,phoneNumber_config,source,address_config,exp_config,nbc_config,affidavit_config,personalinfo_config,ssn_config,race_config,formeremployee_config,generalKnowledge_config,subjectAreaExam_config,additionalDocuments_config,veteran_config,ethnicOrigin_config,ethinicity_config,employment_config,gender_config,IsSIForMiami,isItvtForMiami,retireNo_config,videoLink_config,dSPQuestions_config,dateOfBirth_config,involvement_config,honors_config,countConfig_Residency,data);
							
						}
					}
				}
			});
			
		}
		else
		{
			if(source=='level1')
			{
				hideLoadingDiv_DSPQ();
			}
			$('#loadingDiv_dspq_ie').hide();
			$('#myModalDASpecificQuestions').modal('show');
		}
	}else{
		$("#allowNext").val("1");
	}
}

function saveAndContinueBottomPart()
{
	console.log('saveAndContinueBottomPart');//sekhar
	jeffcoNotApplied=true;
	$('#errordivspecificquestion').empty();
	$('#errordiv_bottomPart_TFA').empty();
	if($('#errordiv_bottomPart_tfaOptions').length>0){
		$('#errordiv_bottomPart_tfaOptions').empty();
	}
	$('#errordiv_bottomPart_divstdTch').empty();
	$('#errordiv_bottomPart_wst').empty();
	$('#errordiv_bottomPart_resume').empty();
	$('#errordiv_bottomPart_phone').empty();
	$('#errAddress1').empty();
//	$('#multyErrDiv').empty();
	$('#errAddressPr').empty();
	$('#errCountry').empty();
	$('#errZip').empty();
	$('#errState').empty();
	$('#errCity').empty();
	
	$('#errCountryPr').empty();
	$('#errZipPr').empty();
	$('#errStatePr').empty();
	$('#errCityPr').empty();
	
	$('#errExpCTT').empty();
	$('#errNBCY').empty();
	$('#errAffidavit').empty();
	$('#errPersonalInfoAndSSN').empty();
	$('#errFormerEmployee').empty();
	$('#errRace').empty();
	$('#errGeneralKnowledge').empty();
	//$('#errSubjectArea').empty();
	$('#errAdditionalDocuments').empty();
	$('#errEthnicOrigin').empty();
	$('#errEthinicity').empty();
	$('#errGender').empty();
	$('#errDOB').empty();
	$('#errRetireNo').empty();
	$('#errordivvideoLinks').empty();
	$('#errExpSalary').empty();
	
	
	var tFA_config=document.getElementById("tFA_config").value;
	var wst_config=document.getElementById("wst_config").value;
	var resume_config=document.getElementById("resume_config").value;
	var phone_config=document.getElementById("phone_config").value;
	var address_config=document.getElementById("address_config").value;
	var exp_config=document.getElementById("exp_config").value;
	var nbc_config=document.getElementById("nbc_config").value;
	var affidavit_config=document.getElementById("affidavit_config").value;
	
	var personalinfo_config=document.getElementById("personalinfo_config").value;
	var dateOfBirth_config=document.getElementById("dateOfBirth_config").value;
	var ssn_config=document.getElementById("ssn_config").value;
	var race_config=document.getElementById("race_config").value;
	var formeremployee_config=document.getElementById("formeremployee_config").value;
	
	var generalKnowledge_config=document.getElementById("generalKnowledge_config").value;
	var subjectAreaExam_config=document.getElementById("subjectAreaExam_config").value;
	var additionalDocuments_config=document.getElementById("additionalDocuments_config").value;
	var veteran_config=document.getElementById("veteran_config").value;
	var ethnicOrigin_config=document.getElementById("ethnicOrigin_config").value;
	var ethinicity_config=document.getElementById("ethinicity_config").value;
	var employment_config=document.getElementById("employment_config").value;
	var gender_config=document.getElementById("gender_config").value;
	var retireNo_config=document.getElementById("retireNo_config").value;
	var videoLink_config=document.getElementById("videoLink_config").value;
	var dSPQuestions_config=document.getElementById("dSPQuestions_config").value;
	
	var affidavit=document.getElementsByName("affidavit");
	var affflag;
	
	if(affidavit[0].checked)
		affflag=true;
	else 
		affflag=false;
	
	var tfaAffiliate 	= 	document.getElementById("tfaAffiliate").value;
	var corpsYear 		= 	document.getElementById("corpsYear").value;
	var tfaRegion 		= 	document.getElementById("tfaRegion").value;
	
	var phoneNumber=""; //document.getElementById("phoneNumber").value;
	
	var phoneNumber1=document.getElementById("phoneNumber1").value;
	var phoneNumber2=document.getElementById("phoneNumber2").value;
	var phoneNumber3=document.getElementById("phoneNumber3").value;
	
	if(phoneNumber1!="" && phoneNumber2!="" && phoneNumber3!="")
		if(phoneNumber1.length==3 && phoneNumber2.length==3 && phoneNumber3.length==4)
			phoneNumber=phoneNumber1+phoneNumber2+phoneNumber3;
	
	var hdnResume = document.getElementById("hdnResume").value;
	var resumeFile = document.getElementById("resume");
	var addressLine1 = document.getElementById("addressLine1");
	var addressLine2 = document.getElementById("addressLine2");
	var zipCode = document.getElementById("zipCode");
	
	var stateIdForDSPQ = "";
	var cityIdForDSPQ = "";
	var countryId = document.getElementById("countryId").value;
	
	/*******************present address detail******************/
	
		var praddressLine1 = document.getElementById("addressLinePr");
		var prpraddressLine2 = document.getElementById("addressLine2Pr");
		var przipCode = document.getElementById("zipCodePr");
		
		var prstateIdForDSPQ = "";
		var prcityIdForDSPQ = "";
		var prcountryId = document.getElementById("countryIdPr").value;
	/*******************end present address detail**************/

	var districtIdForDSPQ = $("#districtIdForDSPQ").val();
	
	/*if(countryId==223)
	{
		stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
		cityIdForDSPQ = document.getElementById("cityIdForDSPQ").value;
	}
	else
	{
		stateIdForDSPQ = document.getElementById("otherState").value;
		cityIdForDSPQ = document.getElementById("otherCity").value;
	}*/

	if(countryId!="")
	{
		if(countryId==223)
		{
			stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
			cityIdForDSPQ = document.getElementById("cityIdForDSPQ").value;
		}
		else
		{
			if(document.getElementById("countryCheck").value==1)
			{
				stateIdForDSPQ = document.getElementById("stateIdForDSPQ").value;
				cityIdForDSPQ = document.getElementById("otherCity").value;
			}
			else
			{
				stateIdForDSPQ = document.getElementById("otherState").value;
				cityIdForDSPQ = document.getElementById("otherCity").value;
			}
		}
	}
	
	if(prcountryId!="")
	{
		if(prcountryId==223)
		{
			prstateIdForDSPQ = document.getElementById("stateIdForDSPQPr").value;
			prcityIdForDSPQ = document.getElementById("cityIdForDSPQPr").value;
		}
		else
		{
			if(document.getElementById("countryCheckPr").value==1)
			{
				prstateIdForDSPQ = document.getElementById("stateIdForDSPQPr").value;
				prcityIdForDSPQ = document.getElementById("otherCityPr").value;
			}
			else
			{
				prstateIdForDSPQ = document.getElementById("otherStatePr").value;
				prcityIdForDSPQ = document.getElementById("otherCityPr").value;
			}
		}
	}
	
	var expCertTeacherTraining = document.getElementById("expCertTeacherTraining").value;
	
	if(districtIdForDSPQ!="" && districtIdForDSPQ=="7800047"){
		expCertTeacherTraining = $('input[name=expTchRadio]:checked').val();
	}
	
	var nbc1 = document.getElementById("nbc1");
	var nbc2 = document.getElementById("nbc2");
	var nationalBoardCertYear = document.getElementById("nationalBoardCertYear").value;
	
	var isNonTeacher = document.getElementById("isNonTeacher").checked;
	var salutation_pi=$("#salutation_pi").val();
	var firstName_pi=$("#firstName_pi").val();
	var middleName_pi=$("#middleName_pi").val();
	var lastName_pi=$("#lastName_pi").val();
	var anotherName_pi=$("#anotherName").val();
	var ssn_pi=$("#ssn_pi").val();
	var expectedSalary_pi=$("#expectedSalary").val();
	var dobMonth=$("#dobMonth").val();
	var dobDay=$("#dobDay").val();
	var dobYear=$("#dobYear").val();
	var drivingLicState=$("#drivingLicState").val();
	var drivingLicNum=$("#drivingLicNum").val();
	var dob="";
	
	var vt1=$("#vt1").val();
	var vt2=$("#vt2").val();
	var veteranValue="";
	
	if (document.getElementById('vt1').checked) {
		veteranValue = document.getElementById('vt1').value;
	}else if (document.getElementById('vt2').checked) {
		veteranValue = document.getElementById('vt2').value;
	}
	
	
	
	var rtDate=$("#rtDate").val();
	var wdDate=$("#wdDate").val();
	var retireNo=$("#retireNo").val();
	var stMForretire=$("#stMForretire").val();
	var distForRetire=$("#retireddistrictId").val();
	
	var employeeType="";
	var formerEmployeeNo="";;
	var currentEmployeeNo="";
	var noLongerEmployed="";
	
	var isCurrentFullTimeTeacher="";
	
	var fileName = resumeFile.value;
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	var fileSize=0;		
	if ($.browser.msie==true){	
	    fileSize = 0;	   
	}else{		
		if(resumeFile.files[0]!=undefined)
		fileSize = resumeFile.files[0].size;
	}
	
	
	var canServeAsSubTeacher=2;
	try{
		if (document.getElementById('canServeAsSubTeacher0').checked) {
			canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher0').value;
		}else if (document.getElementById('canServeAsSubTeacher1').checked) {
			canServeAsSubTeacher = document.getElementById('canServeAsSubTeacher1').value;
		}
	}catch(err){alert(err);}
	
	var cnt_resume=0;
	var cnt_tfa=0;
	var cnt_wst=0;
	var cnt_ph=0;
	var cnt_address1=0;
	var cnt_zip=0;
	var cnt_state=0;
	var cnt_city=0;
	var cnt_Country=0;
	var cnt_ectt=0;
	var cnt_nbcy=0;
	var cnt_affdt=0;
	var focs=0;	

	var cnt_address1pr=0;
	var cnt_zippr=0;
	var cnt_statepr=0;
	var cnt_citypr=0;
	var cnt_Countrypr=0;
	
	var cnt_PersonalInfo=0;
	var cnt_SSN=0;
	var cnt_Race=0;
	var cnt_EthnicOrigin=0;
	var cnt_Ethinicity=0;
	var cnt_FormerEmployee=0;
	var cnt_Veteran=0;
	var cnt_Gender=0;
	var cnt_ExpSalary=0;
	
	
	
	var cnt_GeneralKnowledge=0;
	var cnt_SubjectAreaExam=0;
	var cnt_AdditionalDocuments=0;
	var cnt_RetireNo=0;
	//////////////////////////////////////////////////////////////////////
	if(dSPQuestions_config==true){	
		$('#errordivspecificquestion').empty();
		var arr =[];
		var jobOrder = {jobId:document.getElementById("jobId").value};
		var dspqType=document.getElementById("dspqType").value;
		var isRequiredCount=0;
		var ansRequiredCount=0;
		for(i=1;i<=totalQuestionsList;i++)
		{   
			var schoolMaster="";
			var isRequiredAns=0;
			var isRequired = dwr.util.getValue("QS"+i+"isRequired");
			if(isRequired==1){
				isRequiredCount++;
			}			
			var districtSpecificQuestion = {questionId:dwr.util.getValue("QS"+i+"questionId")};
			var questionTypeShortName = dwr.util.getValue("QS"+i+"questionTypeShortName");
			var questionTypeMaster = {questionTypeId:dwr.util.getValue("QS"+i+"questionTypeId")};			
			var qType = dwr.util.getValue("QS"+i+"questionTypeShortName");
			var o_maxMarks = dwr.util.getValue("o_maxMarksS");
			if(qType=='tf' || qType=='slsel' ||  qType=='slsel')
			{
				var optId="";
				var errorFlag=1;
				var isValidAnswer=false;
				if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
				{
					 errorFlag=0;
					optId=$("input[name=QS"+i+"opt]:radio:checked").val();
				}else if(isRequired==1){
					$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
					dSPQuestionsErrorCount=1;
					errorFlag=1;
				}

				isValidAnswer =dwr.util.getValue("QS"+optId+"validQuestion")
				if(dwr.util.getValue("QS"+i+"question").indexOf(resourceJSON.msgPartTimeTeachingPos) >= 0 && document.getElementById("districtIdForDSPQ").value==614730){
					var nextQ = i+1;
					document.getElementById("QS"+nextQ+"isRequired").value=0;
					if(isValidAnswer==true){
						document.getElementById("QS"+nextQ+"isRequired").value=1;
					}
				}else if(dwr.util.getValue("QS"+i+"question").indexOf("Have you ever applied or worked for Kelly Services?") >= 0 && (document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==100006)){
							var nextQ = i+1;
							document.getElementById("QS"+nextQ+"isRequired").value=0;
							if(isValidAnswer==true){
								document.getElementById("QS"+nextQ+"isRequired").value=1;
							}
						}
						else if(dwr.util.getValue("QS"+i+"question").indexOf("Have you ever applied for work in any public or private schools?") >= 0 && ( document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==100006)){
							var nextQ = i+1;
							document.getElementById("QS"+nextQ+"isRequired").value=0;
							if(isValidAnswer==true){
								document.getElementById("QS"+nextQ+"isRequired").value=1;
							}
					}else if(dwr.util.getValue("QS"+i+"question").indexOf("Have you ever been an intern or volunteer at any public or private schools?") >= 0 && ( document.getElementById("districtIdForDSPQ").value==614730 || document.getElementById("districtIdForDSPQ").value==100006)){
							var nextQ = i+1;
							document.getElementById("QS"+nextQ+"isRequired").value=0;
							if(isValidAnswer==true){
								document.getElementById("QS"+nextQ+"isRequired").value=1;
							}
						}
				
				if(errorFlag==0){
					if(isRequired==1){
						isRequiredAns=1;
					}
					var qId = dwr.util.getValue("QS"+i+"questionId");
					var selectValueForColumbus="";
					console.log(dwr.util.getValue("QS"+i+"question"));
					if(dwr.util.getValue("QS"+i+"question").indexOf("Have you ever worked for Columbus City Schools?") >= 0 && (document.getElementById("districtIdForDSPQ").value==3904380)){
						var columbusSetting = dwr.util.getValue("QS"+qId+"columbus");
						//arr.push({ 
						//	"insertedText"  : $("#columbus_setting_value").val()
						//});
						arr.push({ 
							"insertedText":columbusSetting,
							"selectedOptions"  : optId,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : dwr.util.getValue("QS"+optId+"validQuestion"),
							"jobOrder" : jobOrder						
						});
						console.log(arr);
					}else{
						arr.push({ 
							"selectedOptions"  : optId,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : dwr.util.getValue("QS"+optId+"validQuestion"),
							"jobOrder" : jobOrder						
						});
					}
					
				}
	
			}else if(qType=='ml' || qType=='sl')
			{
				var insertedText = dwr.util.getValue("QS"+i+"opt");
				if((insertedText!=null && insertedText!="") || isRequired==0)
				{
					if(isRequired==1){
						isRequiredAns=1;
					}
					
					if($(".school"+i).length>0){
						schoolMaster=$(".school"+i).val();
					}
					
					arr.push({ 
						"insertedText"    : insertedText,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"jobOrder" : jobOrder,
						"schoolIdTemp" : schoolMaster
					});
				}else
				{
					if(isRequired==1){
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
			}else if(qType=='et' || qType=='sswc'){
				var optId="";
				var errorFlag=1;
				if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
				{
					errorFlag=0;
					optId=$("input[name=QS"+i+"opt]:radio:checked").val();
				}else
				{
					if(isRequired==1){
						errorFlag=1;
						$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
						dSPQuestionsErrorCount=1;
					}
				}
				if($("input[name=QS"+i+"opt]:radio").length==0)
					errorFlag=0;
				
				var insertedText = "";
				var isValidAnswer = dwr.util.getValue("QS"+optId+"validQuestion");
				
				if(dwr.util.getValue("QS"+i+"question")==resourceJSON.msgpreviouslyworkedforUNO){
					if(isValidAnswer==true && (dwr.util.getValue("QS"+i+"optet1").trim()=="" || dwr.util.getValue("QS"+i+"optet2").trim()=="")){						
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
							errorFlag=1;
						}
					}
					insertedText=dwr.util.getValue("QS"+i+"optet1")+"##"+dwr.util.getValue("QS"+i+"optet2");
					
				}else{
					insertedText=dwr.util.getValue("QS"+i+"optet");
					if(isValidAnswer==true && insertedText.trim()==""){
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
							errorFlag=1;
						}
					}
				}
				if(errorFlag==0){
					if(isRequired==1){
						isRequiredAns=1;
					}
					arr.push({ 
						"selectedOptions"  : optId,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"insertedText"    : insertedText,
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"questionOption" : dwr.util.getValue("qOptS"+optId),
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"isValidAnswer" : isValidAnswer,
						"jobOrder" : jobOrder
					});
				}
			}else if(qType=='mlsel'){
				try{
					 var multiSelectArray="";
					 var inputs = document.getElementsByName("multiSelect"+i); 
					 for (var j = 0; j < inputs.length; j++) {
					        if (inputs[j].type === 'checkbox') {
					        	if(inputs[j].checked){
					        		multiSelectArray+=	inputs[j].value+"|";
					            }
					        }
					} 
					if(multiSelectArray!=""  || isRequired==0){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							//"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
						}
					}
				}catch(err){}
			}if(qType=='mloet'){
				try{
					 var multiSelectArray="";
					 var inputs = document.getElementsByName("multiSelect"+i); 
					 for (var j = 0; j < inputs.length; j++) {
					        if (inputs[j].type === 'checkbox') {
					        	if(inputs[j].checked){
					        		multiSelectArray+=	inputs[j].value+"|";
					            }
					        }
					} 
					var insertedText = dwr.util.getValue("QS"+i+"optmloet"); 
					if(multiSelectArray!="" || isRequired==0){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"insertedText"    : insertedText,
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							//"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
						}
					}
				}catch(err){}
			}else if(qType=='rt'){
				var optId="";
				var score="";
				var rank="";
				var scoreRank=0;
				var opts = document.getElementsByName("optS");
				var scores = document.getElementsByName("scoreS");
				var ranks = document.getElementsByName("rankS");
				var o_ranks = document.getElementsByName("o_rankS");
				
				var tt=0;
				var uniqueflag=false;
				for(var i = 0; i < opts.length; i++) {
					optId += opts[i].value+"|";
					score += scores[i].value+"|";
					rank += ranks[i].value+"|";
					if(checkUniqueRankForPortfolio(ranks[i]))
					{
						uniqueflag=true;
						break;
					}
	
					if(ranks[i].value==o_ranks[i].value)
					{
						scoreRank+=parseInt(scores[i].value);
					}
					if(ranks[i].value=="")
					{
						tt++;
					}
				}
				if(uniqueflag)
					return;
	
				if(tt!=0)
					optId=""; 
	
				if(optId=="")
				{
					//totalSkippedQuestions++;
					//strike checking
				}
				var totalScore = scoreRank;
				if(isRequired==1){
					isRequiredAns=1;
				} 
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : isValidAnswer,
					"jobOrder" : jobOrder,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"insertedRanks"    : rank,
					"maxMarks" :o_maxMarks
				});
			}else if(qType=='OSONP'){
			//	alert("hello "+qType);
				try{
					 var multiSelectArray="";
					 var inputs = document.getElementsByClassName("OSONP"+i); 
					 for (var j = 0; j < inputs.length; j++) {
					        if (inputs[j].type === 'radio') {
					        	if(inputs[j].checked){
					        		multiSelectArray+=	inputs[j].value+"|";
					            }
					        }
					} 
					var insertedText = dwr.util.getValue("QS"+i+"OSONP");
					var isValidAnswer = dwr.util.getValue("QS"+multiSelectArray+"validQuestion");							
					if(isValidAnswer==true && insertedText.trim()==""){
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							if(errorFlagCheck==0){
								errorFlagCheck=1;
								$("#divErrorMsg_dynamicPortfolio").append("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							}
							iErrorCount++;
							document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
							dSPQuestionsErrorCount=1;
						}
					}
					
					if(multiSelectArray!="" || isRequired==0){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"insertedText"    : insertedText,
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							//"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
						}
					}
				}catch(err){}
			}else if(qType=='DD'){
				try{
					
					 var multiSelectArray=$("#dropdown"+i+" :selected").val() ;				
				
					if(multiSelectArray!=""  || isRequired==0){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							//"questionOption" : dwr.util.getValue("qOptS"+optId),
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							dSPQuestionsErrorCount=1;
						}
					}
				}catch(err){}
			}
			if(qType=='sscb'){
				try{
					var isValidAnswer = false;
					var insertedText = dwr.util.getValue("QS"+i+"multiselectText");
					 var multiSelectArray="";
					 var inputs = document.getElementsByName("multiSelect"+i); 
					 for (var j = 0; j < inputs.length; j++) {
					        if (inputs[j].type === 'checkbox') {
					        	if(inputs[j].checked){
					        		multiSelectArray+=	inputs[j].value+"|";
					        		if(isValidAnswer==false)
					        			isValidAnswer = dwr.util.getValue("QS"+inputs[j].value+"validQuestion")
					            }
					        }
					} 
					 
					 if($(".school"+i).length>0){
							schoolMaster=$(".school"+i).val();
						}
					 
				
					 if(isValidAnswer==true && insertedText.trim()==""){							
								$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");								
								dSPQuestionsErrorCount=1;
								errorFlag=1;					
								
								
						}else{
							var sectCnt = $(".secHeading").length;
							var showsecerror=false;
								for ( var int = 0; int < sectCnt; int++) {			
									var cntSec= int+1;
									//alert($(".sectionCnt"+cntSec+":checked").length);
									var atLeastOneIsCheckedInsec = $(".sectionCnt"+cntSec+":checked").length;
									if(atLeastOneIsCheckedInsec==0){
										showsecerror=true;
									}
								}
								if(showsecerror){/*
									$("#divErrorMsg_dynamicPortfolio").append("&#149; Please provide response for each section in Question "+i);
									iErrorCount++;
									document.getElementById("modalDymanicPortfolio_modal_body").scrollTop=0;
									dSPQuestionsErrorCount=1;
									//$("#errordivspecificquestionUpload").show();
								*/}
							}
					 
				if(errorFlag==0){
					if(multiSelectArray!=""){
						if(isRequired==1){
							isRequiredAns=1;
						} 
						arr.push({ 
							"selectedOptions"  : multiSelectArray,
							"question"  : dwr.util.getValue("QS"+i+"question"),
							"insertedText"    : insertedText,
							"questionTypeMaster" : questionTypeMaster,
							"questionType" : questionTypeShortName,
							"schoolIdTemp" : schoolMaster,
							"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
							"isValidAnswer" : isValidAnswer,
							"jobOrder" : jobOrder
						});
					}else{
						if(isRequired==1){
							$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
							
							dSPQuestionsErrorCount=1;
						}
					}
				}
				}catch(err){}
			}
			if(qType=='UAT'){
							
				try{
					var errorFlag=0;
					
					var cntErr=0;
					var fileNameQuestion = dwr.util.getValue("QS"+i+"File").value;
					var ext = fileNameQuestion.substr(fileNameQuestion.lastIndexOf('.') + 1).toLowerCase();

					if(isRequired==1 && fileNameQuestion=="")
					{
						$('#errordivspecificquestionUpload').html("&#149; "+resourceJSON.msgPleaseupload+" "+dwr.util.getValue("QS"+i+"question")+".</BR>");
						errorFlag=1;
						cntErr++;
					}
					if(ext!="")
					{
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
						{
							$('#errordivspecificquestionUpload').html("&#149; "+resourceJSON.msgselectAcceptable+" "+dwr.util.getValue("QS"+i+"question")+" "+resourceJSON.msgformatswhichincludePDF+"</BR>");
							errorFlag=1;
							cntErr++;
						}
					}
					
					if(cntErr==0 && fileNameQuestion!=""){
						var newName="proof_of_highly_qualified_"+fileNameQuestion.substr(fileNameQuestion.lastIndexOf('\\') + 1).toLowerCase()						
						$('#answerFileName').val(newName);
						document.getElementById('frmAnswerUpload').submit();						
					}else{
						$("#errordivspecificquestionUpload").show();
					}
					
					var isValidAnswer = false;
					var insertedText = dwr.util.getValue("QS"+i+"Text");
					var file = dwr.util.getValue("answerFileName");
					var multiSelectArray=$("#dropdown"+i+" :selected").val() ;

					if(cntErr==0 && errorFlag==0){
						if(multiSelectArray!=""){
							if(isRequired==1){
								isRequiredAns=1;
							} 
							if(fileNameQuestion==""){
								file=dwr.util.getValue("editCaseFile");
							}

							arr.push({ 
								"selectedOptions"  : multiSelectArray,
								"question"  : dwr.util.getValue("QS"+i+"question"),
								"insertedText"    : insertedText,
								"fileName"    : file,
								"questionTypeMaster" : questionTypeMaster,
								"questionType" : questionTypeShortName,
								//"schoolIdTemp" : schoolMaster,
								"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
								"isValidAnswer" : isValidAnswer,
								"jobOrder" : jobOrder
							});
						}else{
							if(isRequired==1){
								$("#errordivspecificquestion").html("&#149; "+resourceJSON.msgProvideResToAllQ+"<br>");
								dSPQuestionsErrorCount=1;
							}
						}
					}
				}catch(err){}
			}
			if(isRequiredAns==1){
				ansRequiredCount++;
			}
		}
		
		//alert('isRequiredCount:::'+isRequiredCount);
		//alert('ansRequiredCount:::'+ansRequiredCount);
		/*if(isRequiredCount==ansRequiredCount) 
		{*/
			PFCertifications.setDistrictQPortfoliouestions(arr,dspqType,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data){
				if(data!=null)
				{
					arr =[];
				}
			}
			});	
		/*}else
		{
			$("#errordivspecificquestion").html("&#149; Please provide responses to all the questions since they are required.<br>");
			dSPQuestionsErrorCount=1;
		}*/
	}
	
	///////////////////////////////////////////////////////////////////////////////
	var cnt_videoLink=0;
	
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	$('.tfaOptId').css("background-color","");
	
	$('#resume').css("background-color","");
	
	//$('#phoneNumber').css("background-color","");
	$('#phoneNumber1').css("background-color","");
	$('#phoneNumber2').css("background-color","");
	$('#phoneNumber3').css("background-color","");
	$('#addressLine1').css("background-color","");	
	$('#zipCode').css("background-color","");
	$('#stateIdForDSPQ').css("background-color","");
	$('#cityIdForDSPQ').css("background-color","");
	$('#otherState').css("background-color","");
	$('#otherCity').css("background-color","");
	$('#countryId').css("background-color","");
	
	$('#addressLine1Pr').css("background-color","");
	$('#zipCodePr').css("background-color","");
	$('#stateIdForDSPQPr').css("background-color","");
	$('#cityIdForDSPQPr').css("background-color","");
	$('#otherStatePr').css("background-color","");
	$('#otherCityPr').css("background-color","");
	$('#countryIdPr').css("background-color","");
	
	$('#salutation_pi').css("background-color","");
	$('#firstName_pi').css("background-color","");
	$('#middleName_pi').css("background-color","");
	$('#lastName_pi').css("background-color","");
	$('#ssn_pi').css("background-color","");
	
	$('#dobMonth').css("background-color","");
	$('#dobDay').css("background-color","");
	$('#dobYear').css("background-color","");
	
	$('#rtDate').css("background-color","");
	$('#wdDate').css("background-color","");
	$('#retireNo').css("background-color","");
	
	try{
		$('#generalKnowledgeExamStatus').css("background-color","");
		$('#generalKnowledgeExamDate').css("background-color","");
		$('#generalKnowledgeScoreReport').css("background-color","");
	}catch(err){}
	try{
		$('#examStatus').css("background-color","");
		$('#examDate').css("background-color","");
		$('#subjectIdforDSPQ').css("background-color","");
		$('#scoreReport').css("background-color","");
	}catch(err){}
	
	if(personalinfo_config==1)
	{
		if(trim(firstName_pi)=="")
		{
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
			$('#firstName_pi').css("background-color",txtBgColor);
			cnt_PersonalInfo++;focs++;
		}
		
		if(trim(lastName_pi)=="")
		{
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.PlzEtrFirstName+"<br>");
			$('#lastName_pi').css("background-color",txtBgColor);
			cnt_PersonalInfo++;focs++;
		}			
		
	}
	
	$('#errDOB').hide();
	$('#errDOB').empty();
	
	if(document.getElementById("districtIdForDSPQ").value==4218990 || document.getElementById("districtIdForDSPQ").value==1302010){
		var idobYear = new String(parseInt(trim(dobYear)));
		var currentFullYear = new Date().getFullYear();
		currentFullYear=currentFullYear-1;
		
		if(trim(dobMonth)!="0"){
			if(trim(dobDay)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
				$('#dobDay').css("background-color",txtBgColor);
				cnt_PersonalInfo++;focs++;
			}
			
			if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
				$('#dobYear').css("background-color",txtBgColor);
				$('#errDOB').show();
				cnt_PersonalInfo++;focs++;
			}
		
		}else if(trim(dobDay)!="0" ){	
			if(trim(dobMonth)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
				$('#dobMonth').css("background-color",txtBgColor);
				cnt_PersonalInfo++;focs++;
			}
			
			if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
				$('#dobYear').css("background-color",txtBgColor);
				$('#errDOB').show();
				cnt_PersonalInfo++;focs++;
			}
		}else if((dobYear!="") || ( dobYear!=0 || idobYear!="NaN") || (idobYear < currentFullYear || idobYear > 1931)){
			if(trim(dobMonth)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
				$('#dobMonth').css("background-color",txtBgColor);
				cnt_PersonalInfo++;focs++;
			}
			
			if(trim(dobDay)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
				$('#dobDay').css("background-color",txtBgColor);
				cnt_PersonalInfo++;focs++;
			}
		}

		if(trim(dobMonth)> 0 && trim(dobDay) > 0 && trim(dobYear) > 0)
	{
		dob=trim(dobMonth)+"-"+trim(dobDay)+"-"+trim(dobYear);
		document.getElementById("dob").value=dob;
	}
	$('#errDOB').show();
	}
	else if(dateOfBirth_config==1){
		if(trim(dobMonth)=="0")
		{
			$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
			$('#dobMonth').css("background-color",txtBgColor);
			cnt_PersonalInfo++;focs++;
		}
		
		if(trim(dobDay)=="0")
		{
			$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
			$('#dobDay').css("background-color",txtBgColor);
			cnt_PersonalInfo++;focs++;
		}
		
		var idobYear = new String(parseInt(trim(dobYear)));
		var currentFullYear = new Date().getFullYear();
		currentFullYear=currentFullYear-1;
		
		if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
		{
			$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
			$('#dobYear').css("background-color",txtBgColor);
			$('#errDOB').show();
			cnt_PersonalInfo++;focs++;
		}

		if(trim(dobMonth)> 0 && trim(dobDay) > 0 && trim(dobYear) > 0)
	{
		dob=trim(dobMonth)+"-"+trim(dobDay)+"-"+trim(dobYear);
		document.getElementById("dob").value=dob;
	}
	$('#errDOB').show();
	}
	if($("#ssnOptional").val()=="false"){
		ssn_config=false;
		try
		{		
		if(document.getElementById("isAffilated").checked && trim(ssn_pi)=="" && document.getElementById("headQuaterIdForDspq").value==2)
		{			
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgSocialSecurityNumber+"<br>");
			$('#ssn_pi').css("background-color",txtBgColor);
			cnt_SSN++;focs++;
		}
		}catch(err){}
	}
	if(ssn_config==1)
	{
		if(trim(ssn_pi)=="")
		{
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgSocialSecurityNumber+"<br>");
			$('#ssn_pi').css("background-color",txtBgColor);
			cnt_SSN++;focs++;
		}
		/*else if(trim(ssn_pi).length < 4)
		{
			$('#errPersonalInfoAndSSN').append("&#149; Please enter 4 digits SSN.<br>");
			$('#ssn_pi').css("background-color",txtBgColor);
			cnt_SSN++;focs++;
		}*/
	}
	
	if(veteran_config==1)
	{
		if(trim(veteranValue)=="")
		{
			$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgVeteran+"<br>");			
			cnt_Veteran++;focs++;
		}
		
		if(districtIdForDSPQ==1201470 && trim(veteranValue)=="1"){
			if($("#veteranOptS:checked").length==0){
				$('#errPersonalInfoAndSSN').append("&#149; "+resourceJSON.msgPleaseVeteranPrefrence+"<br>");			
				cnt_Veteran++;focs++;
				
			}else{
				setDistrictSpecificVeteranValues();
			}

		}
	}
	
	
	
	if(formeremployee_config==1)
	{
		if($('#fe2').is(':checked') || $('#fe1').is(':checked') || $('#fe3').is(':checked'))
		{
			
		}
		else
		{
			$('#errFormerEmployee').append("&#149; "+resourceJSON.msgCurrentEmployment1+"<br>");
			cnt_FormerEmployee++;focs++;
		}
		
		if($('#fe2').is(':checked'))
		{
			employeeType="1";
			currentEmployeeNo=$("#empfe2").val();
			if($('#rdCEmp1').is(':checked'))
				isCurrentFullTimeTeacher="1";
			else if($('#rdCEmp2').is(':checked'))
				isCurrentFullTimeTeacher="0";
			else if($('#rdCEmp3').is(':checked'))
				isCurrentFullTimeTeacher="2";
			
			if(currentEmployeeNo==null || trim(currentEmployeeNo)=='')
			{
				$('#errFormerEmployee').append("&#149; "+resourceJSON.msgEnterEmployeeNo+"</BR>");
				$('#empfe2').css("background-color","#F5E7E1");
				cnt_FormerEmployee++;focs++;
			}else if(document.getElementById("districtIdForDSPQ").value==1200390 && (currentEmployeeNo!=null || trim(currentEmployeeNo)!='') && isNaN(currentEmployeeNo)){
				$('#errFormerEmployee').append("&#149; Employee Number is only a numerical value. Please enter the correct information, or contact MDCPS for your employee number</BR>");
				cnt_FormerEmployee++;focs++;
			}
			else
			{
				if(document.getElementById("districtIdForDSPQ").value!=4218990){
				$('#empfe2').css("background-color","");
				if(!$('input[name=rdCEmp]').is(":checked"))
				{
					$('#errFormerEmployee').append("&#149; "+resourceJSON.msgStaffMemtype+"</BR>");
					cnt_FormerEmployee++;focs++;
				}
				}
			}
			
		}
		else if($('#fe1').is(':checked'))
		{
			employeeType="0";
			formerEmployeeNo=$("#empfe1").val();
			if(districtIdForDSPQ==1302010)
				noLongerEmployed=$('#empfewDiv').find(".jqte_editor").text().trim();
				
			
			/*if($('#empchk11').is(':checked'))
			{
				if(trim(rtDate)=="")
				{
					$('#errFormerEmployee').append("&#149; Please enter Retirement Date<br>");
					$('#rtDate').css("background-color",txtBgColor);
					cnt_FormerEmployee++;focs++;
				}
				
			}	
			
			if($('#empchk12').is(':checked'))
			{
				if(trim(wdDate)=="")
				{
					$('#errFormerEmployee').append("&#149; Please enter money Withdrawl Date<br>");
					$('#wdDate').css("background-color",txtBgColor);
					cnt_FormerEmployee++;focs++;
				}
			}*/
		}
		else if($('#fe3').is(':checked'))	
		{
			employeeType="2";
		}
	}
	
	
	if(retireNo_config==1)
	{
		if(document.getElementById("isretired").checked==true)
		{
			if(trim(retireNo)=="")
			{
				$('#errRetireNo').append("&#149; "+resourceJSON.msgTeacherRetirementNumber+"<br>");
				cnt_RetireNo++;focs++;
			}
			if(distForRetire=="")
			{
				$('#errRetireNo').append("&#149; "+resourceJSON.msgRetiredFromDistrict+"<br>");
				cnt_RetireNo++;focs++;
			}
			if(stMForretire=="")
			{
				$('#errRetireNo').append("&#149; "+resourceJSON.msgRetiredFromState+"<br>");
				cnt_RetireNo++;focs++;
			}
		}
	}
	
	
	var isMiamiChk=document.getElementById("isMiami").value;
	var displayGKAndSubject=document.getElementById("displayGKAndSubject").value;
	var displayPassFailGK1=document.getElementById("displayPassFailGK").value;
	var IsSIForMiami1=document.getElementById("IsSIForMiami").value;
	var jobTitle=document.getElementById("jobTitleFeild").value;
	var isItvtForMiami=document.getElementById("isItvtForMiami").value;
	
	//if(generalKnowledge_config==1 && displayGKAndSubject=="true" && isMiamiChk=="true")
	var generalKnowledgeExamNote = $("#generalExamNote").find(".jqte_editor").html();	
	$('[name="generalExamNote"]').text(generalKnowledgeExamNote);
	
	var subjectExamTextarea = $("#subjectExamTextarea").find(".jqte_editor").html();	
	$('[name="subjectExamTextarea"]').text(subjectExamTextarea);
	//subjectExamTextarea
	
	if(isItvtForMiami=='false') // && generalKnowledge_source==0
	{
		if((generalKnowledge_config==1 && displayGKAndSubject=='true' && isMiamiChk=="true" && IsSIForMiami1=="false") || (generalKnowledge_config==1 && isMiamiChk=="true" && IsSIForMiami1=="true" && displayPassFailGK1=="false") || (displayGKAndSubject=='true' && document.getElementById("districtIdForDSPQ").value=="614730") || (displayGKAndSubject=='true' && document.getElementById("districtIdForDSPQ").value=="1302010")) // && generalKnowledge_source==0
		{
			var generalKnowledgeExamStatus = document.getElementById("generalKnowledgeExamStatus").value;
			var generalKnowledgeExamDate = document.getElementById("generalKnowledgeExamDate").value;
			var generalKnowledgeScoreReport = document.getElementById("generalKnowledgeScoreReport").value;
			var generalKnowledgeScoreReportHidden = document.getElementById("generalKnowledgeScoreReportHidden").value;
			
			
			if(jobTitle=="Teach For America 2015-2016"){

				if(generalKnowledgeExamStatus!=0 || generalKnowledgeExamDate!="" || trim(generalKnowledgeScoreReport)!="" && trim(generalKnowledgeScoreReportHidden)!=""){
				if(trim(generalKnowledgeExamStatus)=="0"){
					$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgExamStatus+"</BR>");
					$('#generalKnowledgeExamStatus').css("background-color",txtBgColor);
					cnt_GeneralKnowledge++;focs++;
				}
				
				
				if(trim(generalKnowledgeExamDate)==""){
					$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgExamDate+"</BR>");
					$('#generalKnowledgeExamDate').css("background-color",txtBgColor);
					cnt_GeneralKnowledge++;focs++;
				}
				
				if(trim(generalKnowledgeScoreReport)=="" && trim(generalKnowledgeScoreReportHidden)==""){
					$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgUploadScoreReport+"</BR>");
					$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
					cnt_GeneralKnowledge++;focs++;
				}else{
					if(trim(generalKnowledgeScoreReport)!=""){
						var ext = generalKnowledgeScoreReport.substr(generalKnowledgeScoreReport.lastIndexOf('.') + 1).toLowerCase();	
						
						var fileSize = 0;
						if ($.browser.msie==true)
					 	{	
						    fileSize = 0;	   
						}
						else
						{
							if(document.getElementById("generalKnowledgeScoreReport").files[0]!=undefined)
							{
								fileSize = document.getElementById("generalKnowledgeScoreReport").files[0].size;
							}
						}
						
						if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
						{
							$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgAcceptableScoreReportformats+"</BR>");
							$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
							cnt_GeneralKnowledge++;focs++;
						}else if(fileSize>=10485760){
							$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgScoreReportFilesize+".<br>");
							$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
							cnt_GeneralKnowledge++;focs++;
						}
					}
				}
			
				}
				
				
				
			}
			else{
			if(trim(generalKnowledgeExamStatus)=="0"){
				$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgExamStatus+"</BR>");
				$('#generalKnowledgeExamStatus').css("background-color",txtBgColor);
				cnt_GeneralKnowledge++;focs++;
			}
			
			
			if(trim(generalKnowledgeExamDate)==""){
				$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgExamDate+"</BR>");
				$('#generalKnowledgeExamDate').css("background-color",txtBgColor);
				cnt_GeneralKnowledge++;focs++;
			}
			
			if(trim(generalKnowledgeScoreReport)=="" && trim(generalKnowledgeScoreReportHidden)==""){
				$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgUploadScoreReport+"</BR>");
				$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
				cnt_GeneralKnowledge++;focs++;
			}else{
				if(trim(generalKnowledgeScoreReport)!=""){
					var ext = generalKnowledgeScoreReport.substr(generalKnowledgeScoreReport.lastIndexOf('.') + 1).toLowerCase();	
					
					var fileSize = 0;
					if ($.browser.msie==true)
				 	{	
					    fileSize = 0;	   
					}
					else
					{
						if(document.getElementById("generalKnowledgeScoreReport").files[0]!=undefined)
						{
							fileSize = document.getElementById("generalKnowledgeScoreReport").files[0].size;
						}
					}
					
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
					{
						$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgAcceptableScoreReportformats+"</BR>");
						$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
						cnt_GeneralKnowledge++;focs++;
					}else if(fileSize>=10485760){
						$('#errGeneralKnowledge').append("&#149; "+resourceJSON.msgScoreReportFilesize+"<br>");
						$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
						cnt_GeneralKnowledge++;focs++;
					}
				}
			}
		}
		}
	}
	
	if(subjectAreaExam_config==1)
	{/*
		var examStatus = document.getElementById("examStatus").value;
		var examDate = document.getElementById("examDate").value;
		var subjectIdforDSPQ = document.getElementById("subjectIdforDSPQ").value;
		var scoreReport = document.getElementById("scoreReport").value;
		var scoreReportHidden = document.getElementById("scoreReportHidden").value;
		
		if(trim(examStatus)=="0" && trim(examDate)=="" && trim(subjectIdforDSPQ)==0 && trim(scoreReport)=="" && trim(scoreReportHidden)=="" && displayGKAndSubject=='true')
		{
			//alert("T SB");
		}
		else
		{
			//alert("SB");
			if(trim(examStatus)=="0"){
				$('#errSubjectArea').append("&#149; Please select Exam Status.</BR>");
				$('#examStatus').css("background-color",txtBgColor);
				cnt_SubjectAreaExam++;focs++;
			}
			
			
			if(trim(examDate)==""){
				$('#errSubjectArea').append("&#149; Please enter Exam Date.</BR>");
				$('#examDate').css("background-color",txtBgColor);
				cnt_SubjectAreaExam++;focs++;
				
			}
			
			if(trim(subjectIdforDSPQ)==0){
				$('#errSubjectArea').append("&#149; Please select Subject.</BR>");
				$('#subjectIdforDSPQ').css("background-color",txtBgColor);
				cnt_SubjectAreaExam++;focs++;
			}
			
			
			
			if(trim(scoreReport)=="" && trim(scoreReportHidden)==""){
				$('#errSubjectArea').append("&#149; Please upload Score Report.</BR>");
				$('#scoreReport').css("background-color",txtBgColor);
				cnt_SubjectAreaExam++;focs++;
			}else{
				if(trim(scoreReport)!=""){
					var ext = scoreReport.substr(scoreReport.lastIndexOf('.') + 1).toLowerCase();	
					
					var fileSize = 0;
					if ($.browser.msie==true)
				 	{	
					    fileSize = 0;	   
					}
					else
					{
						if(document.getElementById("scoreReport").files[0]!=undefined)
						{
							fileSize = document.getElementById("scoreReport").files[0].size;
						}
					}
					
					if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
					{
						$('#errSubjectArea').append("&#149; Please select Acceptable Score Report formats which include PDF, MS-Word, GIF, PNG, and JPEG  files.</BR>");
						$('#scoreReport').css("background-color",txtBgColor);
						cnt_SubjectAreaExam++;focs++;
					}else if(fileSize>=10485760){
						$('#errSubjectArea').append("&#149; Score Report File size must be less than 10mb.<br>");
						$('#scoreReport').css("background-color",txtBgColor);
						cnt_SubjectAreaExam++;focs++;
					}
				}
			}
		}
		
		
	*/}
	
	
		/*if(additionalDocuments_config==1){
		var documentName = document.getElementById("documentName").value;
		if(trim(documentName)==""){
			$('#errAdditionalDocuments').append("&#149; Please enter Document Name.</BR>");
			$('#documentName').css("background-color",txtBgColor);
			cnt_AdditionalDocuments++;focs++;
		}
		
		var uploadedDocument = document.getElementById("uploadedDocument").value;
		var uploadedDocumentHidden = document.getElementById("uploadedDocumentHidden").value;
		
		if(trim(uploadedDocument)=="" && trim(uploadedDocumentHidden)==""){
			$('#errAdditionalDocuments').append("&#149; Please upload Document.</BR>");
			$('#uploadedDocument').css("background-color",txtBgColor);
			cnt_AdditionalDocuments++;focs++;
		}else{
			if(trim(scoreReport)!=""){
				var ext = uploadedDocument.substr(uploadedDocument.lastIndexOf('.') + 1).toLowerCase();	
				
				var fileSize = 0;
				if ($.browser.msie==true)
			 	{	
				    fileSize = 0;	   
				}
				else
				{
					if(document.getElementById("uploadedDocument").files[0]!=undefined)
					{
						fileSize = document.getElementById("uploadedDocument").files[0].size;
					}
				}
				if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
				{
					$('#errAdditionalDocuments').append("&#149; Please select Acceptable Document formats which include PDF, MS-Word, GIF, PNG, and JPEG  files.</BR>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt_AdditionalDocuments++;focs++;
				}else if(fileSize>=10485760){
					$('#errAdditionalDocuments').append("&#149; Document File size must be less than 10mb.<br>");
					$('#uploadedDocument').css("background-color",txtBgColor);
					cnt_AdditionalDocuments++;focs++;
				}
			}
		}
	}*/
	
	// For EthnicOrigin value
	var ethnicOriginValue=-1;
//	if(ethnicOrigin_config==1)
//	{
		var elements = document.getElementsByName('ethnicOriginId');
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  ethnicOriginValue=elements[i].value;
		  }
		}
		
		if($("#eEocOptional").val()=="false"){}
		else if(districtIdForDSPQ==7800038){}
		else if(districtIdForDSPQ==4218990){}
		else if(districtIdForDSPQ==3904380){}
		else if(districtIdForDSPQ==1201470){}
		else if(districtIdForDSPQ==7800040){}
		else if(districtIdForDSPQ==7800047){}
		else if(districtIdForDSPQ==614730){}
		else if(districtIdForDSPQ==5304860){}
		else if(districtIdForDSPQ==804800){}
		else if(districtIdForDSPQ==1302010){}
		else if(districtIdForDSPQ==3700690){}
		else if(districtIdForDSPQ==3703120){}
		else if(districtIdForDSPQ==3700112){}
		else if(districtIdForDSPQ==3702040){}
		else if(districtIdForDSPQ==3702640){}
		else if($("#headQuaterIdForDspq").val()==2){}
		else{
			if(ethnicOriginValue==-1)
			{
				$('#errEthnicOrigin').append("&#149; "+resourceJSON.msgSelectEthnicOrigin+"<br>");
				//$('#phoneNumber').css("background-color",txtBgColor);
				cnt_EthnicOrigin++;focs++;
			}
		}
		
		//alert(" ethnicOriginValue "+ethnicOriginValue);
//	}
	
	//For Ethinicity value
	var ethinicityValue=-1;
//	if(ethinicity_config==1)
//	{
		var elements = document.getElementsByName('ethinicityId');
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  ethinicityValue=elements[i].value;
		  }
		}
		
		//alert(" ethinicityValue "+ethinicityValue);
		if($("#eEocOptional").val()=="false"){}
		else if(districtIdForDSPQ==7800038){}
		else if(districtIdForDSPQ==4218990){}
		else if(districtIdForDSPQ==3904380){}
		else if(districtIdForDSPQ==1201470){}
		else if(districtIdForDSPQ==7800040){}
		else if(districtIdForDSPQ==7800047){}
		else if(districtIdForDSPQ==614730){}
		else if(districtIdForDSPQ==5304860){}
		else if(districtIdForDSPQ==804800){}
		else if(districtIdForDSPQ==1302010){}
		else if(districtIdForDSPQ==3700690){}
		else if(districtIdForDSPQ==3703120){}
		else if(districtIdForDSPQ==3700112){}
		else if(districtIdForDSPQ==3702040){}
		else if(districtIdForDSPQ==3702640){}
		else if($("#headQuaterIdForDspq").val()==2){}
		else{
			if(ethinicityValue==-1)
			{
				$('#errEthinicity').append("&#149; "+resourceJSON.msgSelectEthnicity1+"<br>");
				//$('#phoneNumber').css("background-color",txtBgColor);
				cnt_Ethinicity++;focs++;
			}
		}
//	}
	
		
		//For Employment
		/*if(employment_config==1)
		{
			
		}*/
	
	//For Race value
	if(race_config==1)
	{
		var raceValue="";
		var elements = document.getElementsByName('raceId');
		
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  raceValue+=elements[i].value+",";
		  }
		}
		/*var elements = document.getElementsByName('raceId');
		for (i=0;i<elements.length;i++) 
		{
		  if(elements[i].checked) 
		  {
			  raceValue=elements[i].value;
		  }
		}*/
		
		if($("#eEocOptional").val()=="false"){}
		else if(districtIdForDSPQ==7800038){}
		else if(districtIdForDSPQ==4218990){}
		else if(districtIdForDSPQ==3904380){}
		else if(districtIdForDSPQ==1201470){}
		else if(districtIdForDSPQ==7800040){}
		else if(districtIdForDSPQ==7800047){}
		else if(districtIdForDSPQ==614730){}
		else if(districtIdForDSPQ==5304860){}
		else if(districtIdForDSPQ==804800){}
		else if(districtIdForDSPQ==1302010){}
		else if(districtIdForDSPQ==3700690){}
		else if(districtIdForDSPQ==3703120){}
		else if(districtIdForDSPQ==3700112){}
		else if(districtIdForDSPQ==3702040){}
		else if(districtIdForDSPQ==3702640){}
		else if($("#headQuaterIdForDspq").val()==2){}
		else{
			if(raceValue=="")
			{
				$('#errRace').append("&#149; "+resourceJSON.msgSelectRace+"<br>");
				//$('#phoneNumber').css("background-color",txtBgColor);
				cnt_Race++;focs++;
			}
		}
	}
	
	
	
	var miami = document.getElementById("isMiami").value;
	var genderValue=-1;
	if(districtIdForDSPQ==614730){}
	else if(districtIdForDSPQ==5304860){}
	else if(districtIdForDSPQ==1302010){}
	else if($("#headQuaterIdForDspq").val()==2){}
	else if(gender_config==1)
	{
		var genderElements = document.getElementsByName('genderId');
		
		for (i=0;i<genderElements.length;i++) 
		{
			/*if(miami==false)
			{
				if(genderElements[i].value==1 || genderElements[i].value==2)
					if(genderElements[i].checked) 
						  genderValue=genderElements[i].value;
			}
			else 
			{*/
				if(genderElements[i].checked) 
					  genderValue=genderElements[i].value;
			/*}*/
		}
		
		if(genderValue==-1)
		{
			$('#errGender').append("&#149; "+resourceJSON.msgSelectGender+"<br>");
			//$('#phoneNumber').css("background-color",txtBgColor);
			cnt_Gender++;focs++;
		}
	}
	
	if(document.getElementById("districtIdForDSPQ").value==7800040 || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Licensed") !=-1))
	{
		if(expectedSalary_pi=="")
		{
			$('#errExpSalary').append("&#149; "+resourceJSON.msgExpectedSalary+"<br>");
			cnt_ExpSalary++;focs++;
		}
	}
	
	if(document.getElementById("districtIdForDSPQ").value==804800 && $('#jobcategoryDsp').val().trim()=="Licensed"){
		wst_config=0;
		$("#sSubTrequired").hide();
	}else if(document.getElementById("districtIdForDSPQ").value==3700690 && ($('#jobcategoryDsp').val().trim().indexOf("Classified") !=-1 || $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1)){
		wst_config=0;
		$("#sSubTrequired").hide();
	}else if(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
		wst_config=0;
		$("#sSubTrequired").hide();
	}else if(document.getElementById("districtIdForDSPQ").value==3702040){
		wst_config=0;
		$("#sSubTrequired").hide();
	}
	
	if(($("#headQuaterIdForDspq").val()==2 && ($("#dspqName").val()=="Option A"|| $("#dspqName").val()=="Option C")) || document.getElementById("districtIdForDSPQ").value==804800 && ($('#jobcategoryDsp').val()=="Teacher, Licensed" || $('#jobcategoryDsp').val()=="Digital Teacher Librarian" || $('#jobcategoryDsp').val()=="Special Education Teacher")){
		wst_config=0;
		$("#sSubTrequired").hide();
	}
	if( $("#substituteOptional").val()=="false"){
		wst_config=0;
	}
	if(document.getElementById("districtIdForDSPQ").value==4218990){
		var nonteacherFlag=$("#isnontj").val();
		if(nonteacherFlag=="" || nonteacherFlag!="true"){
			if(wst_config==1 && canServeAsSubTeacher==2)
			{
				$('#errordiv_bottomPart_wst').append("&#149; "+resourceJSON.msgSubstituteTeacher+"<br>");
				cnt_wst++;focs++;
			}
		}
	}
	else{
		if(wst_config==1 && canServeAsSubTeacher==2)
		{
			$('#errordiv_bottomPart_wst').append("&#149; "+resourceJSON.msgSubstituteTeacher+"<br>");
			cnt_wst++;focs++;
		}
	}
	
	if($("#jeFFcoEmployeeInf").is(':visible')){
		phone_config=0;
	}
	if(phone_config==1 && trim(phoneNumber)=="")
	{
		if(phoneNumber1=="" && phoneNumber2=="" && phoneNumber3=="")
			$('#errordiv_bottomPart_phone').append("&#149; "+resourceJSON.msgPhoneNumber+"<br>");
		else if(phoneNumber1.length!=3 || phoneNumber2.length!=3 || phoneNumber3.length!=4)
			$('#errordiv_bottomPart_phone').append("&#149; "+resourceJSON.msgValidPhoneNumber+"<br>");
		
		$('#phoneNumber1').css("background-color",txtBgColor);
		$('#phoneNumber2').css("background-color",txtBgColor);
		$('#phoneNumber3').css("background-color",txtBgColor);
		cnt_ph++;focs++;
	}
	
	
	if(document.getElementById("districtIdForDSPQ").value==804800 && ( $('#jobcategoryDsp').val().trim()=="Licensed" || $('#jobcategoryDsp').val()=="Substitute Teachers" )){
		exp_config=0;
	} else if(document.getElementById("districtIdForDSPQ").value==1302010 && ($('#jobcategoryDsp').val().trim()=="Classified" || $('#jobcategoryDsp').val().trim()=="Substitutes")){
		exp_config=0;
	}else if((document.getElementById("districtIdForDSPQ").value==7800049  || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053) && candidateType=="I" && formeremployee_config==true && $('#jobcategoryDsp').val().trim()=="Personnel enseignant"){
		exp_config=0;
	}else if(document.getElementById("districtIdForDSPQ").value==3700690 && ($('#jobcategoryDsp').val().trim().indexOf("Classified") !=-1 || $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1)){
		exp_config=0;
	}else if(document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Bus Driver") !=-1){
		exp_config=0;
	}

	if($("#certfiedTeachingExpOptional").val()!="" && $("#certfiedTeachingExpOptional").val()=="false"){
		exp_config=0;
	}
	if(exp_config==1)
	{
		if(trim(expCertTeacherTraining)=="")
		{
			$('#errExpCTT').append("&#149; "+resourceJSON.msgteachingcertificate+"<br>");
			$('#expCertTeacherTraining').css("background-color",txtBgColor);
			cnt_ectt++;focs++;
		}
	}
	
	if(nbc_config==1)
	{
		if(nbc1.checked && trim(nationalBoardCertYear)=="")
		{
			$('#errNBCY').append("&#149; "+resourceJSON.msgnationalboard+"<br>");
			$('#nationalBoardCertYear').css("background-color",txtBgColor);
			cnt_nbcy++;focs++;
		}
	}
	
	if(affidavit_config==1)
	{
		if(affflag==false)
		{
			$('#errAffidavit').append("&#149; "+resourceJSON.msgSelectAffidavit+"<br>");
			$('#affidavit').css("background-color",txtBgColor);
			cnt_affdt++;focs++;
		}
	}
	if($("#tfaOptional").val()=="false" || districtIdForDSPQ==7800056 || districtIdForDSPQ==1201470 || districtIdForDSPQ==3700112 || districtIdForDSPQ==614730 || districtIdForDSPQ==1302010 || document.getElementById("districtIdForDSPQ").value==3700690 || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Classified") !=-1) || (document.getElementById("districtIdForDSPQ").value==3703120 && $('#jobcategoryDsp').val().trim().indexOf("Substitute") !=-1)  || ($("#headQuaterIdForDspq").val()==2 && $("#dspqName").val()=="Option A")){}
	else if(districtIdForDSPQ==4218990){
		var nonteacherFlag=$("#isnontj").val();
		if(nonteacherFlag=="" || nonteacherFlag!="true"){
			
			if(tfaAffiliate!="3" && tfaAffiliate!="")
			{
				if(trim(corpsYear)=="")
				{
					$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgcorpsyear+"<br>");
					if(focs==0)
						$('#corpsYear').focus();
					
					$('#corpsYear').css("background-color",txtBgColor);
					cnt_tfa++;focs++;
				}
				
				if(trim(tfaRegion)=="")
				{
					$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgtfaregion+"<br>");
					if(focs==0)
						$('#tfaRegion').focus();
					
					$('#tfaRegion').css("background-color",txtBgColor);
					cnt_tfa++;focs++;
				}
			}
			
		}
	}
	else{		
	if(trim(tfaAffiliate)=="")
	{
		$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgTeachForAmericaAffiliate+"<br>");
		if(focs==0)
			$('#tfaAffiliate').focus();
		
		$('#tfaAffiliate').css("background-color",txtBgColor);
		cnt_tfa++;focs++;
	}
	
	if(tfaAffiliate!="3" && tfaAffiliate!="")
	{
		if(trim(corpsYear)=="")
		{
			$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgcorpsyear+"<br>");
			if(focs==0)
				$('#corpsYear').focus();
			
			$('#corpsYear').css("background-color",txtBgColor);
			cnt_tfa++;focs++;
		}
		
		if(trim(tfaRegion)=="")
		{
			$('#errordiv_bottomPart_TFA').append("&#149; "+resourceJSON.msgtfaregion+"<br>");
			if(focs==0)
				$('#tfaRegion').focus();
			
			$('#tfaRegion').css("background-color",txtBgColor);
			cnt_tfa++;focs++;
		}
	}
	}
	
	if(districtIdForDSPQ==4218990){		
		
		var nonteacherFlag=$("#isnontj").val();
		var isSchoolsupport=$("#isSchoolSupportPhiladelphia").val();		
		var atLeastOneIsChecked = $('.tfaOptId:checked').length > 0;
		
		if((isSchoolsupport=="" || isSchoolsupport=="0") && (nonteacherFlag=="" || nonteacherFlag=="false")){
			if(document.getElementById('StuTchrChk').checked==true && $('#ttlRecStdExp').val()==0){
				$('#errordiv_bottomPart_divstdTch').html("&#149; "+resourceJSON.msgStdTeachingExp+"");
			}
		}
		
		/*if(nonteacherFlag=="" || nonteacherFlag!="true"){
			if(atLeastOneIsChecked==false){
				$('#errordiv_bottomPart_divstdTch').html("&#149; PLEASE SELECT ONE OR PROGRAM YOU'VE BEEN INVOLVED IN");
				$('#errordiv_bottomPart_tfaOptions').show();
				if(focs==0)
					$('#errordiv_bottomPart_tfaOptions').focus();
				
				$('.tfaOptId').css("background-color",txtBgColor);
				cnt_tfa++;focs++;
			}else{
				//setDistrictSpecificTFAValues();				
				setDistrictSpecificTFAValues();
			}
		}else{*/
			if(atLeastOneIsChecked==true){
				
				setDistrictSpecificTFAValues();
			//}
		}
	}
	if(document.getElementById("districtIdForDSPQ").value=="804800" && $('#jobcategoryDsp').val().trim().indexOf("Hourly") !=-1){
		resume_config=0;
		$("#requiredRessume").hide();
	}else if((document.getElementById("districtIdForDSPQ").value==7800049 || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053) && candidateType=="I" && formeremployee_config==true && $('#jobcategoryDsp').val().trim()=="Personnel enseignant"){
		resume_config=0;
		$("#requiredRessume").hide();
	}
	
	if($("#ressumeOptional").val()=="false"){
		resume_config=0;
	}
	if(ext!="")
	{
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errordiv_bottomPart_resume').append("&#149; "+resourceJSON.msgacceptableresume+"<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt_resume++;focs++;	

		}
		else if(fileSize>=10485760)
		{		
			$('#errordiv_bottomPart_resume').append("&#149; "+resourceJSON.msgfilesize+"<br>");
			if(focs==0)
				$('#resume').focus();

			$('#resume').css("background-color",txtBgColor);
			cnt_resume++;focs++;	

		}
		else
		{	
			if(cnt_resume > 0)
				$('#errordiv_bottomPart_resume').show();
			//document.getElementById("frmExpResumeUpload").submit();
		}
	}
	else if(resume_config==1 && ext=="" && hdnResume=="")
	{
		$('#errordiv_bottomPart_resume').append("&#149; "+resourceJSON.msgresumr+"<br>");
		cnt_resume++;
		$('#errordiv_bottomPart_resume').show();
	}
	
	 if($("#addressOptional").val()=="false"){
		address_config=0;
	}
	if(address_config==1)
	{
		if(trim(addressLine1.value)=="")
		{
			$('#errAddress1').append("&#149; "+resourceJSON.msgaddresslinefirst+"<br>");
			$('#addressLine1').css("background-color",txtBgColor);
			cnt_address1++;focs++;
		}	
		if(trim(zipCode.value)=="")
		{
			$('#errZip').append("&#149; "+resourceJSON.msgenterzipcode+"<br>");
			$('#zipCode').css("background-color",txtBgColor);
			cnt_zip++;focs++;
		}
		else if(resourceJSON.locale=="en")
		{
			if (!/^\d{5}$/.test(zipCode.value))
			{			
				$('#errZip').append("&#149; "+resourceJSON.msgvalidzipcode+"<br>");			
				$('#zipCode').css("background-color",txtBgColor);
				cnt_zip++;focs++;
			}
		}
		
		//------ Start:: Country ,State, City----------
		if(countryId=="")
		{
			$('#errCountry').append("&#149; "+resourceJSON.msgPleaseaCountry+"<br>");
			$('#cnt_Country').css("background-color",txtBgColor);
			cnt_Country++;focs++;
		}
		else
		{
			if(trim(stateIdForDSPQ)=="")
			{
				$('#errState').append("&#149; "+resourceJSON.msgPleaseselectaState+"<br>");
				
				if(countryId==223)
				{
					$('#stateIdForDSPQ').css("background-color",txtBgColor);
					cnt_state++;focs++;
				}
				else
				{
					if(document.getElementById("countryCheck").value==1)
					{
						$('#stateIdForDSPQ').css("background-color",txtBgColor);
						cnt_state++;focs++;
					}
					else if(document.getElementById("countryCheck").value==0)
					{
						$('#otherState').css("background-color",txtBgColor);
						cnt_state++;focs++;
					}
				}
			}
			
			if(trim(cityIdForDSPQ)=="")
			{
				$('#errCity').append("&#149; "+resourceJSON.msgPleaseselectaCity+"<br>");
				
				if(countryId==223)
				{
					$('#cityIdForDSPQ').css("background-color",txtBgColor);
					cnt_city++;focs++;
				}
				else
				{
					if(document.getElementById("countryCheck").value==1)
					{
						$('#otherCity').css("background-color",txtBgColor);
						cnt_city++;focs++;
					}
					else if(document.getElementById("countryCheck").value==0)
					{
						$('#otherCity').css("background-color",txtBgColor);
						cnt_city++;focs++;
					}
				}
			}
			
			
			
			/*if(document.getElementById("countryCheck").value==1)
			{
				if(trim(stateIdForDSPQ)=="")
				{
					$('#errState').append("&#149; Please select a State<br>");
					$('#stateIdForDSPQ').css("background-color",txtBgColor);
					cnt_state++;focs++;
				}
				if(trim(cityIdForDSPQ)=="")
				{
					$('#errCity').append("&#149; Please select a City<br>");
					$('#cityIdForDSPQ').css("background-color",txtBgColor);
					cnt_city++;focs++;
				}
			}
			else if(document.getElementById("countryCheck").value==0)
			{
				if(trim(stateIdForDSPQ)=="")
				{
					$('#errState').append("&#149; Please select a State<br>");
					$('#otherState').css("background-color",txtBgColor);
					cnt_state++;focs++;
				}
				if(trim(cityIdForDSPQ)=="")
				{
					$('#errCity').append("&#149; Please select a City<br>");
					$('#otherCity').css("background-color",txtBgColor);
					cnt_city++;focs++;
				}
			}*/
		}
		//------ End----------
		
		
		//present address error
		if(districtIdForDSPQ==4503810){
		if(trim(praddressLine1.value)=="")
		{
			$('#errAddressPr').append("&#149; Please enter Address Line 1<br>");
			$('#addressLinePr').css("background-color",txtBgColor);
			cnt_address1pr++;focs++;
		}	
		if(trim(przipCode.value)=="")
		{
			$('#errZipPr').append("&#149; Please enter Zip Code<br>");
			$('#zipCodePr').css("background-color",txtBgColor);
			cnt_zippr++;focs++;
		}
		
		//------ Start:: Country ,State, City----------
		if(prcountryId=="")
		{
			$('#errCountryPr').append("&#149; Please select a Country<br>");
			$('#countryIdPr').css("background-color",txtBgColor);
			cnt_Countrypr++;focs++;
		}
		else
		{
			if(trim(prstateIdForDSPQ)=="")
			{
				$('#errStatePr').append("&#149; Please select a State<br>");
				
				if(prcountryId==223)
				{
					$('#stateIdForDSPQPr').css("background-color",txtBgColor);
					cnt_statepr++;focs++;
				}
				else
				{
					if(document.getElementById("countryCheckPr").value==1)
					{
						$('#stateIdForDSPQPr').css("background-color",txtBgColor);
						cnt_statepr++;focs++;
					}
					else if(document.getElementById("countryCheckPr").value==0)
					{
						$('#otherStatePr').css("background-color",txtBgColor);
						cnt_statepr++;focs++;
					}
				}
			}
			
			if(trim(prcityIdForDSPQ)=="")
			{
				$('#errCityPr').append("&#149; Please select a City<br>");
				
				if(prcountryId==223)
				{
					$('#cityIdForDSPQPr').css("background-color",txtBgColor);
					cnt_citypr++;focs++;
				}
				else
				{
					if(document.getElementById("countryCheckPr").value==1)
					{
						$('#otherCityPr').css("background-color",txtBgColor);
						cnt_citypr++;focs++;
					}
					else if(document.getElementById("countryCheckPr").value==0)
					{
						$('#otherCityPr').css("background-color",txtBgColor);
						cnt_citypr++;focs++;
					}
				}
			}
			
			
			
			/*if(document.getElementById("countryCheck").value==1)
			{
				if(trim(stateIdForDSPQ)=="")
				{
					$('#errState').append("&#149; Please select a State<br>");
					$('#stateIdForDSPQ').css("background-color",txtBgColor);
					cnt_state++;focs++;
				}
				if(trim(cityIdForDSPQ)=="")
				{
					$('#errCity').append("&#149; Please select a City<br>");
					$('#cityIdForDSPQ').css("background-color",txtBgColor);
					cnt_city++;focs++;
				}
			}
			else if(document.getElementById("countryCheck").value==0)
			{
				if(trim(stateIdForDSPQ)=="")
				{
					$('#errState').append("&#149; Please select a State<br>");
					$('#otherState').css("background-color",txtBgColor);
					cnt_state++;focs++;
				}
				if(trim(cityIdForDSPQ)=="")
				{
					$('#errCity').append("&#149; Please select a City<br>");
					$('#otherCity').css("background-color",txtBgColor);
					cnt_city++;focs++;
				}
			}*/
		}
		}
		//------ End----------
	
	}	
	
	if(cnt_PersonalInfo >= 1 || cnt_SSN >=1)
	{
		$('#errPersonalInfoAndSSN').show();
	}
	
	if(cnt_PersonalInfo >= 1 || cnt_Veteran >=1)
	{
		$('#errPersonalInfoAndSSN').show();
	}
	
	if(cnt_FormerEmployee >= 1)
	{
		$('#errFormerEmployee').show();
	}
	
	if(cnt_GeneralKnowledge >= 1){
		$('#errGeneralKnowledge').show();
	}
	else
	{
		$('#errGeneralKnowledge').hide();
	}
	
	if(cnt_SubjectAreaExam >= 1){
		//$('#errSubjectArea').show();
	}
	
	if(cnt_Race >=1)
	{
		$('#errRace').show();
	}
	if(cnt_Gender >=1)
	{
		$('#errGender').show();
	}
	if(cnt_ExpSalary >=1)
	{
		$('#errExpSalary').show();
	}
	if(cnt_EthnicOrigin >=1)
	{
		$('#errEthnicOrigin').show();
	}
	if(cnt_Ethinicity >=1)
	{
		$('#errEthinicity').show();
	}
	
	if(cnt_tfa!=0 && tFA_config==1)		
	{
		$('#errordiv_bottomPart_TFA').show();
	}
	else if(cnt_tfa!=0 && tfaAffiliate!="3" && tfaAffiliate!="")		
	{
		$('#errordiv_bottomPart_TFA').show();
	}
	
	if(cnt_wst==1)
	{
		$('#errordiv_bottomPart_wst').show();
	}
	
	if(cnt_ph==1)
	{
		$('#errordiv_bottomPart_phone').show();
	}
	
	if(cnt_address1==1)
	{
		$('#errAddress1').show();
	}
	if(cnt_address1pr==1)
	{
		$('#errAddressPr').show();
	}
	if(cnt_zippr==1)
	{
		$('#multyErrDivPr').show();
		$('#errZipPr').show();
	}
	if(cnt_zip==1)
	{
		$('#multyErrDiv').show();
		$('#errZip').show();
	}
	if(cnt_state==1)
	{
		$('#multyErrDiv').show();
		$('#errZip').show();
		$('#errState').show();
	}
	if(cnt_statepr==1)
	{
		$('#multyErrDivPr').show();
		$('#errZipPr').show();
		$('#errStatePr').show();
	}
	if(cnt_city==1)
	{
		$('#multyErrDiv').show();
		$('#errZip').show();
		$('#errState').show();
		$('#errCity').show();
	}
	if(cnt_citypr==1)
	{
		$('#multyErrDivPr').show();
		$('#errZipPr').show();
		$('#errStatePr').show();
		$('#errCityPr').show();
	}
	if(cnt_Country==1)
	{
		$('#errCountry').show();
	}
	if(cnt_Countrypr==1)
	{
		$('#errCountryPr').show();
	}
	if(cnt_ectt==1)
	{
		$('#errExpCTT').show();
	}
	if(cnt_nbcy==1)
	{
		$('#errNBCY').show();
	}
	if(cnt_affdt==1)
	{
		$('#errAffidavit').show();
	}
	if(cnt_RetireNo>0)
	{
		$('#errRetireNo').show();
	}
	
	if(((generalKnowledge_config==1 || subjectAreaExam_config || additionalDocuments_config) && cnt_AdditionalDocuments==0 && cnt_GeneralKnowledge==0 && cnt_SubjectAreaExam==0)){
		try{
			document.getElementById("multifileuploadform").submit();
		}catch(err){}
	}
	var isContinue=true;
	if(cnt_tfa==0 || cnt_wst==0 || cnt_resume==0 || cnt_ph==0 || cnt_address1==0 || cnt_zip==0 || cnt_state==0 || cnt_city==0 || cnt_ectt==0 ||	cnt_nbcy==0 || cnt_affdt==0 || cnt_PersonalInfo==0 || cnt_SSN==0 || cnt_FormerEmployee==0 || cnt_Race==0 || cnt_GeneralKnowledge==0 || cnt_SubjectAreaExam==0|| cnt_AdditionalDocuments==0 || cnt_Veteran==0 || cnt_EthnicOrigin==0 || cnt_Ethinicity==0 ||cnt_Country==0 || cnt_Gender || cnt_RetireNo==0 || cnt_ExpSalary==0)		
	{
		if(tfaAffiliate==null || tfaAffiliate=='')
			tfaAffiliate =0;
		if(corpsYear==null || corpsYear=='')
			corpsYear =0;
		if(tfaRegion==null || tfaRegion=='')
			tfaRegion =0;
		
		
		//------------------------Testing for flag----------------------
		var isAffilated=0;
		var candidateType="";
		
		try{
			if(document.getElementById("isAffilated")!=null && document.getElementById("isAffilated").checked)
				isAffilated=1;
			else
				isAffilated=document.getElementById("txtCandidateType").value;
		}catch(err){}
		
		if(isAffilated==1)
			candidateType="I";
		else
			candidateType="E";
		
		
		//--------------------------------------------------------------
		var jobId=document.getElementById("jobId").value;
		var portfolioStatus = document.getElementById("portfolioStatus").value;
		PFExperiences.insertOrUpdateTeacherTFA(tfaAffiliate,corpsYear,tfaRegion,canServeAsSubTeacher,phoneNumber,addressLine1.value,addressLine2.value,zipCode.value,stateIdForDSPQ,cityIdForDSPQ,expCertTeacherTraining,nationalBoardCertYear,affflag,salutation_pi,firstName_pi,middleName_pi,lastName_pi,ssn_pi,dob,employeeType,formerEmployeeNo,currentEmployeeNo,isCurrentFullTimeTeacher,raceValue,rtDate,wdDate,veteranValue,districtIdForDSPQ,ethnicOriginValue,ethinicityValue,countryId,genderValue,candidateType,portfolioStatus,jobId,isNonTeacher,retireNo,distForRetire,stMForretire,expectedSalary_pi,anotherName_pi,noLongerEmployed,drivingLicState,drivingLicNum,praddressLine1.value,prpraddressLine2.value,przipCode.value,prstateIdForDSPQ,prcityIdForDSPQ,prcountryId,
		{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{	
			
			/*if(candidateType!="I" && cnt_PersonalInfo==0 && cnt_SSN==0 && document.getElementById("districtIdForDSPQ").value==804800){	
				if(messageShowOrNot()){
					jeffcoNotApplied=false;
				}
			}*/
			if(data[3]==1)
			{
				$("#loadingDiv_dspq_ie").hide();
				isContinue=false;
				$("#allowNext").val("0");
				
				var msg=resourceJSON.msgUserNameOrPass+"<a href='https://platform.teachermatch.org/forgotpassword.do' target='_blank'>"+resourceJSON.BtnClick+"</a>. "+resourceJSON.msgContectHelpDesk+" clientservices@teachermatch.net "+resourceJSON.msgor+" 855-980-0511.";
				$("#chkDupCandidateBody").html(msg);
				//$("#perform").val("RD");
				try{$('#chkDupCandidate').modal('show');}catch(e){}
			}
			else
			{
			   if(data[0]==1)
				{
				   //alert('4444444 data[1]:'+data[1]);//sekhar
				   
				   //alert('document.getElementById("portfolioStatus").value::::'+document.getElementById("portfolioStatus").value);//sekhar
					isContinue=true;
					if(data[1]==1 && document.getElementById("portfolioStatus").value=="false")
					{
						document.getElementById("portfolioStatus").value="true";
						if(data[2]!="")
						{
							if(data[2]==0)
								document.getElementById("inventory").value="''";
							else if(data[2]==1)
								document.getElementById("inventory").value="epi";
							if(data[2]==2)
								document.getElementById("inventory").value="jsi";
							if(data[2]==3)
								document.getElementById("inventory").value="done";
						}
					}
					if(fileName!="")
					{
						document.getElementById("frmExpResumeUpload").submit();
					}
					else
					{
						validatePortfolioErrorMessageAndGridData('level2');
					}
				}else
				{
					//alert('5555555 data[1]:'+data[1]);//sekhar
					$("#loadingDiv_dspq_ie").hide();
					isContinue=false;
					$("#allowNext").val("0");
					
					if(data[1]==0)
					{
						var msg=resourceJSON.msgEmployeeOfMiami+"<a href='http://jobs.dadeschools.net/teachers/Index.asp'>http://jobs.dadeschools.net/teachers/Index.asp</a>";
						$("#notApplyMsg").html(msg);
						$("#perform").val("RD");
						try{$('#jobApplyOrNot').modal('show');}catch(e){}
						
					}
					else if(data[1]==3) //Philadaphia check
					{
						/*var msg="Dear Principal,<br><br>Thank you for your interest in applying to lead a different school for the 2015-2016 school year. At this time, the principal application is only open to external and new-to-principalship candidates. " +
								"The application for current SDP principals will open on Monday, February 23. In preparation for applying, please ensure you have spoken with your Assistant Superintendent about your interest. " +
								//"Please do not reply to this e-mail. If you need assistance, please contact <a href='mailto:principalschoosephilly@philasd.org' target='_top'>principalschoosephilly@philasd.org</a>." +
								"If you need assistance, please contact <a href='mailto:principalschoosephilly@philasd.org' target='_top'>principalschoosephilly@philasd.org</a>." +
								"<br><br>Best regards,<br>" +
								"The Office of Talent" +
								"<br>The School District of Philadelphia";*/
								var msg=resourceJSON.msgDearApplicant+"<br><br>"+resourceJSON.msgIntrestOfTeching+" " +
								""+resourceJSON.msgadditionalquestions+"<a href='mailto:recruitment@philasd.org' target='_top'>recruitment@philasd.org</a>." +
								"<br><br>"+resourceJSON.msgSincerely+",<br>" +
								resourceJSON.msgOfficeofTalent;
						
						$('#message2showConfirm').html(msg);
						try{ $('#myModal3').modal('show'); }catch(ee){}
					}
					else if(data[1]==2) //DrugFailed
					{
						$('#message2showConfirm').html(resourceJSON.msgPositivedrugreport);
						try{ $('#myModal3').modal('show'); }catch(ee){}
					}else 
					{
						$('#message2showConfirm').html(resourceJSON.msgCurrentEmploymentsection);
						try{ $('#myModal3').modal('show'); }catch(ee){}
					}
				}
			}
			}
		});
	}
	else
	{
		return false;
	}
	
	
	if(!isContinue)
		return;
		
		////////////for miami
		var jobId = document.getElementById("jobId").value;
		DistrictPortfolioConfigAjax.checkEmployeeDetail(jobId,{ 
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
			
			if($('#jobcategoryDsp').val()=="Aspiring Assistant Principal" && data[1]=="RD")
				data[0]="1";
			
				if(data[0]=="0")
				{
					$("#loadingDiv_dspq_ie").hide();
					//alert("Do not Apply");
					/*
					  R - Restricted (PC)
					  A - Allowed
					  NFP - Not Fulltime nor Parttime
					  P - Parttime
					  F - Fulltime
					  RD - Redirect
					 */
					applyFlag=false;
					var retVal = data[1];
					//alert(data[0]+" " +retVal);
					var msg = "";
					if(retVal=="R")//PC
						msg=resourceJSON.msgOfficeProfessionalStandards;
					if(retVal=="RD")
						msg=resourceJSON.msgCurrEmpOfMiami+" <a href='http://jobs.dadeschools.net/teachers/Index.asp'>http://jobs.dadeschools.net/teachers/Index.asp</a>";
					if(retVal=="NFP")
						msg=resourceJSON.msgRecordsIndicatePosition;
					if(retVal=="P")
					{
						//alert("data");
						//msg="Our records indicate the position you are applying for is restricted and/or not within the scope of the position you currently hold.";
						msg=resourceJSON.msgParttimeemploymentwithMiamiDade+"<a href='mailto:employeeservices@dadeschools.net'>employeeservices@dadeschools.net</a>. ";
						try{$('#jobApplyOrNot').modal('show');}catch(e){}
						//try{$('#jobApplyOrNot').modal('hide');}catch(e){}
						//notifyUsers(retVal);
					}
					if(retVal=="F")
						msg=resourceJSON.msgRecordsIndicatePosition;
					
					$("#notApplyMsg").html(msg);
						$("#perform").val(retVal);
					
					return;
				}else
					applyFlag=true;
			}
		});
		
	//}
	
}

function performAction()
{
	var retVal= $("#perform").val();
	var msg="";
	if(retVal=="R")//PC
		notifyUsers(retVal);
	else if(retVal=="RD")
	{
		checkPopup('http://jobs.dadeschools.net/teachers/Index.asp');
		notifyUsers(retVal);
	}else if(retVal=="NFP")
		//msg="You cannot apply either Full Time job or Part Time job.";
		notifyUsers(retVal);
	else if(retVal=="P")
		//msg="You are a former employee of Miami Dade but not eligible for applying this job as it is a part time job.";
		notifyUsers(retVal);
	else if(retVal=="F")
		notifyUsers(retVal);
}
function notifyUsers(retVal)
{
	var jobId=document.getElementById("jobId").value;
	
	if(jobId > 0 )
	{
		DistrictPortfolioConfigAjax.notifyUsers(jobId,retVal,{ 
			async: true,
			errorHandler:handleError,
			callback: function(data)
			{
				//alert("data: "+retVal);
				window.location.href="userdashboard.do";
			}
			});
	}
}


function validateCandidateType()
{
	var jobId=document.getElementById("jobId").value;
	var isAffilated=0;
	var candidateType="";
	
	if(document.getElementById("districtIdForDSPQ").value==4218990){
		
	}else{
	
		
		if(document.getElementById("isAffilated").checked)
		{
			isAffilated=1;
			$("#inst").show();
			$("#noninst").show();
			try{if(document.getElementById("isMiami").value=="true")
			 $("#partinst").show();}catch(e){}
			 
			 if(document.getElementById("districtIdForDSPQ").value==5304860 || document.getElementById("districtIdForDSPQ").value==614730)
				{
					var tempStr=$("#partinst").html();
					var chngdd=tempStr.replace(resourceJSON.msgCurrentlyinstructional+"", ""+resourceJSON.msgCurrentsubstitute);
					$("#partinst").html(chngdd);
					$("#partinst").show();
				}
			 if(document.getElementById("districtIdForDSPQ").value==7800049 || document.getElementById("districtIdForDSPQ").value==7800048 || document.getElementById("districtIdForDSPQ").value==7800050 || document.getElementById("districtIdForDSPQ").value==7800051 || document.getElementById("districtIdForDSPQ").value==7800053)
				{
					var tempStr=$("#partinst").html();
					var chngdd=tempStr.replace("I am currently a part-time instructional employee (Teacher)","I am currently a part-time, substitute, LTO, etc employee.");
					$("#partinst").html(chngdd);
					$("#partinst").show();
				}
			 if(document.getElementById("districtIdForDSPQ").value==804800){
					$(".empCvrrLtrDiv").show();
				}
		}else
		{
			$("#inst").hide();
			$("#noninst").hide();
			$("#partinst").hide();
			$(".empCvrrLtrDiv").hide();
	}
	}
	if(isAffilated==1)
		candidateType="I";
	else
		candidateType="E";
	
	if(jobId > 0 )
	{
		DistrictPortfolioConfigAjax.getPortfolioConfigByJobId(jobId,candidateType,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null)
			{
				//setCoverLetter();
				if(data.coverLetter!=null)
				{
					if(data.districtMaster!=null && data.districtMaster.districtId==7800040){
						data.coverLetter=0;
					}
					if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isNonTeacher==true && data.coverLetter==1){
						$('#cvrltrTxt').hide();
						$('.philNT').hide();
						$('.philadelphiaNTCss').show();
						$('#isnontj').val(true);										
						//data.coverLetter=0;
					}else{
						$('#cvrltrTxt').hide();
					}
					
					//tpl-1647 for custom configuration
					if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isPrinciplePhiladelphia==true)//for active principal(SA) & principal jobcategory 
					{
						$("#isPrinciplePhiladelphia").val("1");		
						data.coverLetter=0;						
					}
					
					if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.jobCategoryName=="Principal")//for active principal(SA) & principal jobcategory 
					{						
						$("#inst").show();					
						var tempStr=$("#inst").html();
						var chngdd=tempStr.replace(resourceJSON.msgFullTimeinstructional+"", ""+resourceJSON.msgCurrentprincipal);
						$("#inst").html(chngdd);						
					}
									
					
					if(data.coverLetter==1)
						document.getElementById("noCoverLetter").style.display="none";
					else
						document.getElementById("noCoverLetter").style.display="block";
				}
				
				if(data.districtSpecificPortfolioQuestions==0 && data.academic ==0 && data.academicTranscript ==0 && data.certification ==0 && data.proofOfCertification ==0 && data.reference ==0 && data.referenceLettersOfRecommendation ==0 && data.resume==0 && data.tfaAffiliate==0 && data.willingAsSubstituteTeacher==0 && data.phoneNumber==0 && data.personalinfo==0 && data.ssn==0 && data.race==0 && data.formeremployee==0 && data.honors==0 && data.involvement==0 && data.additionalDocuments==0)
					document.getElementById("txtDistrictPortfolioConfig").value=0;
				else
					document.getElementById("txtDistrictPortfolioConfig").value=data.districtPortfolioConfigId;
			}
			else
			{
				document.getElementById("txtDistrictPortfolioConfig").value=0;
			}
		}
		});
	}
}


function validateCandidateTypeForExternalJobApply()
{
	var jobId=document.getElementById("jobId").value;
	var isAffilated=0;
	var candidateType="";
	if(document.getElementById("isAffilated").checked)
	{
			isAffilated=1;
			$("#inst").show();
			$("#noninst").show();
			try{
			if(document.getElementById("isMiami").value=="true")
			{
				$("#partinst").show();
			}
			}catch(e){}
			
			if((document.getElementById("districtIdForDSPQ") && document.getElementById("districtIdForDSPQ").value==5304860) || $('#districtId').val()==5304860)
			{
				var tempStr=$("#partinst").html();
				var chngdd=tempStr.replace(resourceJSON.msgCurrentlyinstructional+"", ""+resourceJSON.msgCurrentsubstitute);
				$("#partinst").html(chngdd);
				$("#partinst").show();
			}
			
			if(document.getElementById("districtId") && document.getElementById("districtId").value==7800049 || document.getElementById("districtId").value==7800048 || document.getElementById("districtId").value==7800050 || document.getElementById("districtId").value==7800051 || document.getElementById("districtId").value==7800053 || $('#districtId').val()==7800049)
			{
				var tempStr=$("#partinst").html();
				var chngdd=tempStr.replace("I am currently a part-time instructional employee (Teacher)", "I am currently a part-time, substitute, LTO, etc employee.");
				$("#partinst").html(chngdd);
				$("#partinst").show();
			}
			
			if((document.getElementById("districtIdForDSPQ") && document.getElementById("districtIdForDSPQ").value==804800) || $('#districtId').val()==804800){
				$(".empCvrrLtrDiv").show();				
			}
	}else
	{
		$("#inst").hide();
		$("#noninst").hide();
		$("#partinst").hide();
		$(".empCvrrLtrDiv").hide();
	}
	
	if($('#districtId').val()==4218990){
		$("#inst").hide();
		$("#noninst").hide();
		$("#partinst").hide();
	}
	
	if(isAffilated==1)
		candidateType="I";
	else
		candidateType="E";
	
	if(jobId > 0 )
	{
		DistrictPortfolioConfigAjax.getPortfolioConfigByJobIdForExternal(jobId,candidateType,{ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null)
			{
				//setCoverLetter();
				if(data.coverLetter!=null)
				{
					if(data.districtMaster!=null && data.districtMaster.districtId==4218990){
						
						$("#inst").hide();
						$("#noninst").hide();
						$("#partinst").hide();
					}
					if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.isNonTeacher==true && data.coverLetter==1){
						$('#cvrltrTxt').hide();
						$('.philNT').hide();
						$('.philadelphiaNTCss').show();
						$('#isnontj').val(true);										
						//data.coverLetter=0;
					}else{
						$('#cvrltrTxt').hide();
					}
					
					if(data.districtMaster!=null && data.districtMaster.districtId==4218990 && data.jobCategoryName=="Principal")//for active principal(SA) & principal jobcategory 
					{						
						$("#inst").show();					
						var tempStr=$("#inst").html();
						var chngdd=tempStr.replace(resourceJSON.msgFullTimeinstructional+"", ""+resourceJSON.msgCurrentprincipal);
						$("#inst").html(chngdd);						
					}					
					if(data.coverLetter==1)
						document.getElementById("noCoverLetter").style.display="none";
					else
						document.getElementById("noCoverLetter").style.display="block";
				}
			}
		}
		});
	}
}

function resetBottomPart()
{
	$('#tfaAffiliate').css("background-color","");
	$('#corpsYear').css("background-color","");
	$('#tfaRegion').css("background-color","");
	$('#resume').css("background-color","");
//	$('#phoneNumber').css("background-color","");
	$('#phoneNumber1').css("background-color","");
	$('#phoneNumber2').css("background-color","");
	$('#phoneNumber3').css("background-color","");
	$('#rtDate').css("background-color","");
	$('#wdDate').css("background-color","");
}


/*function getTeacherDetails()
{
	DistrictPortfolioConfigAjax.getTeacherDetails({ 
		async: true,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null)
			{
				if(data.emailAddress!=null && data.emailAddress!="")
				{
					document.getElementById("email_pi").value=data.emailAddress;
				}
				else
				{
					document.getElementById("email_pi").value="";
				}
			}
		}
		});
}*/


function displayGKAndSubject()
{
	//alert("Calling displayGKAndSubject.....1");
	DistrictPortfolioConfigAjax.getCurrentStatusCertificate({ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null)
			{
				var res = data.split("#");				
				document.getElementById("displayGKAndSubject").value=res[0];
				document.getElementById("displayPassFailGK").value=res[1];
				showHideGK_SubjectArea();
			}
		}
		});
}


function validateDOBYearForDSPQ()
{
	$('#errDOB').hide();
	$('#errDOB').empty();
	
	$('#dobYear').css("background-color","");
	$('#dobMonth').css("background-color","");
	$('#dobDay').css("background-color","");
	
	var dobYear=$("#dobYear").val();
	dobYear=trim(dobYear);
	var idobYear = new String(parseInt(dobYear));
	var currentFullYear = new Date().getFullYear();
	currentFullYear=currentFullYear-1;
	
	var dobMonth=$("#dobMonth").val();
	var dobDay=$("#dobDay").val();
	if(document.getElementById("districtIdForDSPQ").value==4218990){		
		
		if(trim(dobMonth)!="0"){
			if(trim(dobDay)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
				$('#dobDay').css("background-color",txtBgColor);
				$('#errDOB').show();
			}
			
			if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
				$('#dobYear').css("background-color",txtBgColor);
				$('#errDOB').show();
				return false;
			}
		
		}else if(trim(dobDay)!="0" ){	
			if(trim(dobMonth)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgMonthOfbirth+"<br>");
				$('#dobMonth').css("background-color",txtBgColor);
				$('#errDOB').show();
			}
			
			if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
				$('#dobYear').css("background-color",txtBgColor);
				$('#errDOB').show();
				return false;
			}
		}else if((dobYear!="") || ( dobYear!=0 || idobYear!="NaN") || (idobYear < currentFullYear || idobYear > 1931)){
			if(trim(dobMonth)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
				$('#dobMonth').css("background-color",txtBgColor);
				$('#errDOB').show();
			}
			
			if(trim(dobDay)=="0")
			{
				$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
				$('#dobDay').css("background-color",txtBgColor);
				$('#errDOB').show();
			}
		}
	}
	else{
	if(trim(dobMonth)=="0")
	{
		$('#errDOB').append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
		$('#dobMonth').css("background-color",txtBgColor);
		$('#errDOB').show();
		//return false;
	}
	
	if(trim(dobDay)=="0")
	{
		$('#errDOB').append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
		$('#dobDay').css("background-color",txtBgColor);
		$('#errDOB').show();
		//return false;
	}
	
	if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
	{
		$('#errDOB').append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
		$('#dobYear').css("background-color",txtBgColor);
		$('#errDOB').show();
		return false;
	}
	
}
	/*else if(dobYear==0 || idobYear=="NaN")
	{
		$('#errDOB').append("&#149; Please enter a valid year of birth.<br>");
		$('#dobYear').css("background-color",txtBgColor);
		$('#errDOB').show();
		return false;
	}
	else if(idobYear > currentFullYear || idobYear < 1931)
	{
		$('#errDOB').append("&#149; Please enter a valid year of birth.<br>");
		$('#dobYear').css("background-color",txtBgColor);
		$('#errDOB').show();
		return false;
	}*/
	return true;
}


function hideAllPortfolioModel()
{
	try { $('#myModalCL').modal('hide'); } catch (e) {}
	try { $('#myModalDymanicPortfolio').modal('hide'); } catch (e) {}
	try { $('#topErrorMessageDSPQ').modal('hide'); } catch (e) {}
}

function showDSPQDiv()
{
	try { $('#topErrorMessageDSPQ').modal('hide'); } catch (e) {}
	try { $('#myModalDymanicPortfolio').modal('show'); } catch (e) {}
}

//Get State by Country
function getStateByCountryForDspq(stateModuleFlag)
{
	var countryId = document.getElementById("countryId").value;
	
	if(countryId!='')
	{
		StateAjax.getStateByCountry(countryId,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQ").html(data[0]);
						if(!(countryId==data[3]))
						{
							document.getElementById("slst").selected=true;
							document.getElementById("slcty").selected=true;
						}
							
					}
					else if(stateModuleFlag=="portfolio")
					{
						$("#stateId").html(data[0]);
						if(!(countryId==data[3]))
						{
							document.getElementById("slst").selected=true;
							document.getElementById("slcty").selected=true;
						}
							
					}
					document.getElementById("divUSAState").style.display="inline";
					document.getElementById("divotherstate").style.display="none";
					if(countryId!=223)
					{
						document.getElementById("divUSACity").style.display="none";
						document.getElementById("divothercity").style.display="inline";
						
						if(countryId==data[3])
							document.getElementById("otherCity").value=data[2];
						else
							document.getElementById("otherCity").value="";
					}
					else
					{
						document.getElementById("divUSACity").style.display="inline";
						document.getElementById("divothercity").style.display="none";
						getCityListByStateSetForDSPQ(data[0]);
						
						if(countryId==data[3])
						{
							if(data[4]!=null && data[4]!="")
								document.getElementById("ct"+data[4]).selected=true;
							else
								document.getElementById("slcty").selected=true;
						}
					}
				}
				else
				{
					document.getElementById("countryCheck").value=0;
					document.getElementById("divUSAState").style.display="none";
					document.getElementById("divUSACity").style.display="none";
					
					if(countryId==data[3])
					{
						document.getElementById("otherState").value=data[1];
						document.getElementById("otherCity").value=data[2];
					}
					else
					{
						document.getElementById("otherState").value="";
						document.getElementById("otherCity").value="";
					}
					document.getElementById("divotherstate").style.display="inline";
					document.getElementById("divothercity").style.display="inline";
				}
			},
		});
	}
	
	if(countryId=='')
	{
		if(resourceJSON.locale=="fr")
		{
			countryId='38';
			document.getElementById("ctry38").selected=true;
		}else
		{
			countryId='223';
			document.getElementById("ctry223").selected=true;
		}
	
		
		StateAjax.getStateByCountry(countryId,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					document.getElementById("countryCheck").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQ").html(data[0]);
			
						if(!(countryId==data[3]))
							document.getElementById("slst").selected=true;
					}
					else if(stateModuleFlag=="portfolio")
					{
						$("#stateId").html(data[0]);
						if(!(countryId==data[3]))
							document.getElementById("slcty").selected=true;
					}
					document.getElementById("divUSAState").style.display="inline";
					document.getElementById("divUSACity").style.display="inline";
					document.getElementById("divotherstate").style.display="none";
					document.getElementById("divothercity").style.display="none";
				}
				else
				{
					document.getElementById("countryCheck").value=0;
					document.getElementById("divUSAState").style.display="none";
					document.getElementById("divUSACity").style.display="none";
					
					if(countryId==data[3])
					{
						document.getElementById("otherState").value=data[1];
						document.getElementById("otherCity").value=data[2];
					}
					else
					{
						document.getElementById("otherState").value="";
						document.getElementById("otherCity").value="";
					}
					document.getElementById("divotherstate").style.display="inline";
					document.getElementById("divothercity").style.display="inline";
				}
			},
		});
	}
	
	
	/*if(countryId=='223')
	{
		document.getElementById("divUSAState").style.display="inline";
		document.getElementById("divUSACity").style.display="inline";
		
		document.getElementById("divotherstate").style.display="none";
		document.getElementById("divothercity").style.display="none";
	}
	else
	{
		//alert("Other countryId "+countryId);
		document.getElementById("divUSAState").style.display="none";
		document.getElementById("divUSACity").style.display="none";
		
		document.getElementById("divotherstate").style.display="inline";
		document.getElementById("divothercity").style.display="inline";
	}*/
}

function getStateByCountryForDspqPr(stateModuleFlag)
{
	var countryId = document.getElementById("countryIdPr").value;
	if(countryId!='')
	{
		StateAjax.getStateByCountryPresent(countryId,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					document.getElementById("countryCheckPr").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQPr").html(data[0]);
						if(!(countryId==data[3]))
						{
							try{document.getElementById("slstPr").selected=true; } catch (e) {}
							document.getElementById("slctyPr").selected=true;
						}
					}
					
					document.getElementById("divUSAStatePr").style.display="inline";
					document.getElementById("divotherstatePr").style.display="none";
					if(countryId!=223)
					{
						document.getElementById("divUSACityPr").style.display="none";
						document.getElementById("divothercityPr").style.display="inline";
						
						if(countryId==data[3])
							document.getElementById("otherCityPr").value=data[2];
						else
							document.getElementById("otherCityPr").value="";
					}
					else
					{
						document.getElementById("divUSACityPr").style.display="inline";
						document.getElementById("divothercityPr").style.display="none";
						getCityListByStateSetForDSPQPr(data[0]);
						if(countryId==data[3])
						{
							if(data[4]!=null && data[4]!="")
								document.getElementById("ctPr"+data[4]).selected=true;
							else
								document.getElementById("slctyPr").selected=true;
						}
					}
				}
				else
				{
					document.getElementById("countryCheckPr").value=0;
					document.getElementById("divUSAStatePr").style.display="none";
					document.getElementById("divUSACityPr").style.display="none";
					
					if(countryId==data[3])
					{
						document.getElementById("otherStatePr").value=data[1];
						document.getElementById("otherCityPr").value=data[2];
					}
					else
					{
						document.getElementById("otherStatePr").value="";
						document.getElementById("otherCityPr").value="";
					}
					document.getElementById("divotherstatePr").style.display="inline";
					document.getElementById("divothercityPr").style.display="inline";
				}
			},
		});
	}
	
	if(countryId=='')
	{
		if(resourceJSON.locale=="fr")
		{
			countryId='38';
			document.getElementById("ctry38").selected=true;
		}else
		{
			countryId='223';
			document.getElementById("ctry223").selected=true;
		}
	
		
		StateAjax.getStateByCountryPresent(countryId,stateModuleFlag,{ 
			async: true,
			callback: function(data)
			{
				if(data[0]!="")
				{
					document.getElementById("countryCheckPr").value=1;
					if(stateModuleFlag=="dspq")
					{
						$("#stateIdForDSPQPr").html(data[0]);
			
						if(!(countryId==data[3]))
							document.getElementById("slstPr").selected=true;
					}
					else if(stateModuleFlag=="portfolio")
					{
						$("#stateId").html(data[0]);
						if(!(countryId==data[3]))
							document.getElementById("slctyPr").selected=true;
					}
					document.getElementById("divUSAStatePr").style.display="inline";
					document.getElementById("divUSACityPr").style.display="inline";
					document.getElementById("divotherstatePr").style.display="none";
					document.getElementById("divothercityPr").style.display="none";
				}
				else
				{
					document.getElementById("countryCheckPr").value=0;
					document.getElementById("divUSAStatePr").style.display="none";
					document.getElementById("divUSACityPr").style.display="none";
					
					if(countryId==data[3])
					{
						document.getElementById("otherStatePr").value=data[1];
						document.getElementById("otherCityPr").value=data[2];
					}
					else
					{
						document.getElementById("otherStatePr").value="";
						document.getElementById("otherCityPr").value="";
					}
					document.getElementById("divotherstatePr").style.display="inline";
					document.getElementById("divothercityPr").style.display="inline";
				}
			},
		});
	}
	
	
	/*if(countryId=='223')
	{
		document.getElementById("divUSAState").style.display="inline";
		document.getElementById("divUSACity").style.display="inline";
		
		document.getElementById("divotherstate").style.display="none";
		document.getElementById("divothercity").style.display="none";
	}
	else
	{
		//alert("Other countryId "+countryId);
		document.getElementById("divUSAState").style.display="none";
		document.getElementById("divUSACity").style.display="none";
		
		document.getElementById("divotherstate").style.display="inline";
		document.getElementById("divothercity").style.display="inline";
	}*/
}



function saveAllDSPQ(){
	
	var conti=false;
	var sbtsource=1;
	if($("#degreeName").is(':visible'))
	{
		document.getElementById("sbtsource_aca").value=1;
		conti=true;
		insertOrUpdate_Academic(sbtsource);
	}
	if($("#stateMaster").is(':visible'))
	{
		document.getElementById("sbtsource_cert").value=1;
		conti=true;
		insertOrUpdate_Certification(sbtsource);
	}
	else
	{
		displayGKAndSubject();
	}
	if($("#salutation").is(':visible'))
	{
		document.getElementById("sbtsource_ref").value=1;
		conti=true;
		insertOrUpdateElectronicReferences(sbtsource);
	}
	if($("#role").is(':visible'))
	{
		document.getElementById("sbtsource_emp").value=1;
		conti=true;
		insertOrUpdateEmployment(sbtsource);
	}
	if($("#examStatus").is(':visible'))
	{
		document.getElementById("sbtsource_subArea").value=1;
		conti=true;
		saveSubjectAreas(sbtsource);
	}
	if($("#documentName").is(':visible'))
	{
		document.getElementById("sbtsource_aadDoc").value=1;
		conti=true;
		insertOrUpdate_AdditionalDocuments(sbtsource);
	}
	
	if(conti)
	{
		validatePortfolioErrorMessageAndGridData('level2');
	}
}



function showHideGK_SubjectArea()
{
	//alert("Calling showHideGK_SubjectArea");
	var isMiamiChk=document.getElementById("isMiami").value;
	var displayGKAndSubject1=document.getElementById("displayGKAndSubject").value;
	
	var generalKnowledge_config=document.getElementById("generalKnowledge_config").value;
	var subjectAreaExam_config=document.getElementById("subjectAreaExam_config").value;
	
	var IsSIForMiami=document.getElementById("IsSIForMiami").value;
	var displayPassFailGK1=document.getElementById("displayPassFailGK").value;
	
	var isItvtForMiami = document.getElementById("isItvtForMiami").value;
	var districtIdForDSPQ = $("#districtIdForDSPQ").val();
	
	if(document.getElementById("districtIdForDSPQ").value=="614730" || document.getElementById("districtIdForDSPQ").value=="1302010"){
		if(generalKnowledge_config==1)
		{
			document.getElementById("generalKnowledgeDiv").style.display="block";
		}
		else
		{
			document.getElementById("generalKnowledgeDiv").style.display="none";
		}
		if(subjectAreaExam_config==1)
		{
			document.getElementById("subjectAreaDiv").style.display="block";
		}
		else
		{
			document.getElementById("subjectAreaDiv").style.display="none";
		}
	}	
	else if(isItvtForMiami=='false')
	{
		if(displayGKAndSubject1=="true" && isMiamiChk=="true" && IsSIForMiami=="false")
		{
			//alert("1");
			if(generalKnowledge_config==1)
			{
				document.getElementById("generalKnowledgeDiv").style.display="block";
			}
			else
			{
				document.getElementById("generalKnowledgeDiv").style.display="none";
			}
			if(subjectAreaExam_config==1)
			{
				document.getElementById("subjectAreaDiv").style.display="block";
			}
			else
			{
				document.getElementById("subjectAreaDiv").style.display="none";
			}
		}
		else if(isMiamiChk=="true" && IsSIForMiami=="true" && displayPassFailGK1=="false")
		{
			//alert("2");
			if(generalKnowledge_config==1)
			{
				document.getElementById("generalKnowledgeDiv").style.display="block";
			}
			else
			{
				document.getElementById("generalKnowledgeDiv").style.display="none";
			}
			document.getElementById("subjectAreaDiv").style.display="none";
		}
		else
		{
			//alert("3");
			document.getElementById("generalKnowledgeDiv").style.display="none";
			document.getElementById("subjectAreaDiv").style.display="none";
		}
	}
	else
	{
		try
		{
			if(isMiamiChk=="true")
			{
				document.getElementById("generalKnowledgeDiv").style.display="none";
				document.getElementById("subjectAreaDiv").style.display="none";
			}
		}catch(e){}
	}
	
}

function hideLoadingDiv_DSPQ()
{
	try
	{
		$('#loadingDiv_dspq_ie').fadeOut();
	}
	catch(e){}
}

function resetSBTNSource()
{
	
	try
	{
		document.getElementById("threadCount").value=0;
	}
	catch(e){}
	
	try
	{
		document.getElementById("returnThreadCount").value=0;
	}
	catch(e){}
	
	try
	{
		document.getElementById("callForwardCount").value=-2;
	}
	catch(e){}
	
	
	/////////////////
	
	try
	{
		document.getElementById("sbtsource_aca").value=0;
	}
	catch(e){}
	
	
	try
	{
		document.getElementById("sbtsource_cert").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("sbtsource_ref").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("sbtsource_emp").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("sbtsource_subArea").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("sbtsource_aadDoc").value=0;
	}
	catch(e){}
	
	///////////////////////////////////////////////
	
	try
	{
		document.getElementById("threadCount_aca").value=0;
		//alert("aca :: "+document.getElementById("threadCount_aca").value);
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_cert").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_emp").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_ref").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_subarea").value=0;
		
	}
	catch(e){}
	
	try
	{
		document.getElementById("threadCount_adddoc").value=0;
	}
	catch(e){}
	
	
}

function updateThreadCount(section)
{
	try
	{
		var threadCount=document.getElementById("threadCount").value;
		var iThreadCount = parseInt(threadCount);
		iThreadCount=iThreadCount+1;
		document.getElementById("threadCount").value=iThreadCount;
		//alert(section+" iThreadCount "+iThreadCount);
	}
	catch(e){}
}

function updateReturnThreadCount(section)
{
	try
	{
		var returnThreadCount=document.getElementById("returnThreadCount").value;
		var iReturnThreadCount = parseInt(returnThreadCount);
		iReturnThreadCount=iReturnThreadCount+1;
		document.getElementById("returnThreadCount").value=iReturnThreadCount;
		//alert(section+" iReturnThreadCount "+iReturnThreadCount);
	}
	catch(e){}
}


function resetTopAndSubDivErrors()
{
	$('divErrorMsg_top_pInfo').empty();
	$('divErrorMsg_top_pInfo').hide();
	$('divErrorMsg_top_pInfo_header').hide();

	$('#divErrorMsg_top_Address').empty();
	$('#divErrorMsg_top_Address').hide();
	$('#divErrorMsg_top_Address_pInfo_header').hide();

	$('#divErrorMsg_top_preAddress').empty();
	$('#divErrorMsg_top_preAddress').hide();
	$('#divErrorMsg_top_preAddress_pInfo_header').hide();
	
	$('#divErrorMsg_top_Curr_Emp').empty();
	$('#divErrorMsg_top_Curr_Emp').hide();
	$('#divErrorMsg_top_Curr_Emp_header').hide();

	$('#divErrorMsg_top_aca').empty();
	$('#divErrorMsg_top_aca').hide();
	$('#divErrorMsg_top_aca_header').hide();

	$('#divErrorMsg_top_Residency').empty();
	$('#divErrorMsg_top_Residency').hide();
	$('#divErrorMsg_top_Residency_header').hide();

	$('#divErrorMsg_top_certi').empty();
	$('#divErrorMsg_top_certi').hide();
	$('#divErrorMsg_top_certi_header').hide();

	$('#divErrorMsg_top_gk').empty();
	$('#divErrorMsg_top_gk').hide();
	$('#divErrorMsg_top_gk_header').hide();

	$('#divErrorMsg_top_subarea').empty();
	$('#divErrorMsg_top_subarea').hide();
	$('#divErrorMsg_top_subarea_header').hide();

	$('#divErrorMsg_top_emphis').empty();
	$('#divErrorMsg_top_emphis').hide();
	$('#divErrorMsg_top_emphis_header').hide();

	$('#divErrorMsg_top_AddDoc').empty();
	$('#divErrorMsg_top_AddDoc').hide();
	$('#divErrorMsg_top_AddDoc_header').hide();

	$('#divErrorMsg_top_ref').empty();
	$('#divErrorMsg_top_ref').hide();
	$('#divErrorMsg_top_ref_header').hide();
	
	$('#divErrorMsg_top_StdTchExp').empty();
	$('#divErrorMsg_top_StdTchExp').hide();
	$('#divErrorMsg_top_StdTchExp_header').hide();

	$('#divErrorMsg_top_TFA').empty();
	$('#divErrorMsg_top_TFA').hide();
	$('#divErrorMsg_top_TFA_header').hide();
	
	$('#divErrorMsg_top_currMemIn').empty();
	$('#divErrorMsg_top_currMemIn_header').hide();
}

function closeDSPQCal()
{
	try
	{
		if(dspqCalInstance!=null)
		{
			dspqCalInstance.hide();
		}
	}
	catch(e){}
}

function chkNonTeacher(){
	if(document.getElementById("isNonTeacher").checked){		
		document.getElementById("expCertTeacherTraining").disabled=true;
		$(".expDiv").hide();
		document.getElementById("expCertTeacherTraining").value="0.0";
	}else{
		document.getElementById("expCertTeacherTraining").disabled=false;
		document.getElementById("expCertTeacherTraining").value="";
		$(".expDiv").show();
	}
}

//Check Duplicate Candidate
function chkDupCandidate()
{
	var teacherId = trim(document.getElementById("teacherIdForDSPQ").value);
	var jobId	=	trim(document.getElementById("jobId").value);
	
	PFExperiences.chkDupCandidate(teacherId,jobId,{ 
		async: true,
		callback: function(data)
		{
			//alert("SUCCESS");
		}
	});
}


function chkDistrict()
{
var districtIdForDSPQ = $("#districtIdForDSPQ").val();
	
	if(districtIdForDSPQ==1200390)
		document.getElementById('ssn_pi').setAttribute('maxlength','9');
	else if(districtIdForDSPQ==4218990)
		document.getElementById('ssn_pi').setAttribute('maxlength','9');
	else if(districtIdForDSPQ==3680340)
		document.getElementById('ssn_pi').setAttribute('maxlength','4');
}

function chkRetired()
{
	var isretired = document.getElementById("isretired").checked;
	
	if(isretired==true)
		$('#isRetiredDiv').show();
	else
		$('#isRetiredDiv').hide();
}

function applyScrollOnTbl_CertificationsNoble()
{
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tblGridCertifications').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 380,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[300,560,100], //125,90,400,120,80
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblVideoLinks()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#videoLinktblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
          colratio:[650,200,112],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnTblStdTchr()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#StdTchrExpLinktblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
          colratio:[250,200,200,200,112],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}
function applyScrollOnInvl()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#involvementGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[250,250,175,176,111], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnHonors()
{
		var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#honorsGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
        colratio:[862,100], // table header width
        addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 100,
        wrapper: false
        });            
        });
}
function applyScrollOnTblteacherLang()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#teacherLanguageGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 962,
        minWidth: null,
        minWidthAuto: false,
          colratio:[450,200,200,112],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function applyScrollOnTblResidencyDiv()
{
	//alert("Hi")
	var $j=jQuery.noConflict();
        $j(document).ready(function() {
        $j('#tchrResidencytblGrid').fixheadertable({ //table id 
        caption: '',
        showhide: false,
        theme: 'ui',
        height: 350,
        width: 952,
        minWidth: null,
        minWidthAuto: false,
          colratio:[209,120,120,110,128,168,105],
	    addTitles: false,
        zebra: true,
        zebraClass: 'net-alternative-row',
        sortable: false,
        sortedColId: null,
        //sortType:[],
        dateFormat: 'd-m-y',
        pager: false,
        rowsPerPage: 10,
        resizeCol: false,
        minColWidth: 70,
        wrapper: false
        });
            
        });			
}

function certTextDiv(id){
	$("#myModalDymanicPortfolio").modal("hide");
	//$("#cerTTextContent"+id).val();
	$(".certTextContent").html($("#cerTTextContent"+id).val());
	$("#certTextDivDSPQ").modal("show");
}


function checkUniqueRankForPortfolio(dis)
{
	if(isNaN(dis.value))
	{
		dis.value="";
		dis.focus();
		return;
	}
	var arr =[];
	var ranks = document.getElementsByName("rankS");
	for(i=0;i<ranks.length;i++)
	{
		if(ranks[i].value!="" && ranks[i]!=dis)
		{
			arr.push(ranks[i].value);
		}
	}
	var o_rank = document.getElementsByName("o_rankS");

	var arr2 =[];
	for(i=0;i<o_rank.length;i++)
		arr2.push(o_rank[i].value);


	if(dis.value!="")
		if($.inArray(dis.value, arr2)==-1)
		{
			dis.value="";
			$('#warningImg1').html("<img src='images/info.png' align='top'>");
			$('#message2showConfirm1').html(resourceJSON.msgValidRank);
			$('#nextMsg').html("");
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#myModalv').modal('show');
			$('#vcloseBtn').html("<span>x</span>");
			
			dis.focus();
			return false;
		}


	$.each(arr, function(j, el){
		if(parseInt(el)==parseInt(dis.value))
		{
			dis.value="";
			$('#warningImg1').html("<img src='images/info.png' align='top'>");
			$('#message2showConfirm1').html(resourceJSON.msgUniqueRank);
			$('#nextMsg').html("");
			$('#footerbtn1').html("<button class='btn btn-primary' data-dismiss='modal' aria-hidden='true'>"+resourceJSON.btnOk+"</button>");
			$('#myModalv').modal('show');
			$('#vcloseBtn').html("<span>x</span>");
			dis.focus();

			return false;
		}
	});
}

//=================district search ==================

var count=0;
var index=-1;
var length=0;
var divid='';
var txtid='';
var hiddenDataArray = new Array();
var showDataArray = new Array();
var degreeTypeArray = new Array();
var hiddenId="";
function getDistrictMasterAutoComp(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("districtName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getDistrictMasterArray(txtSearch.value);
		fatchData(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getDistrictMasterArray(districtName){
	var searchArray = new Array();
	DistrictAjax.getFieldOfDistrictList(districtName,{ 
		async: false,
		callback: function(data){
		hiddenDataArray = new Array();
		showDataArray = new Array();
		for(i=0;i<data.length;i++){
			searchArray[i]=data[i].districtName;
			showDataArray[i]=data[i].districtName;
			hiddenDataArray[i]=data[i].districtId;
		}
	},
		errorHandler:handleError 
	});	

	return searchArray;
}


var selectFirst="";

var fatchData= function(txtSearch,searchArray,txtId,txtdivid){
	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
				
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideDistrictMasterDiv(dis,hiddenId,divId)
{
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

var downArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index<length-1){
			for(var i=0;i<10;i++){
				{
					if(document.getElementById('divResult'+txtdivid+i))
						var div_id=document.getElementById('divResult'+txtdivid+i);
					if(div_id)
					{
						if(div_id.className=='over')
							index=div_id.id.split('divResult'+txtdivid)[1];
						div_id.className='normal';
					}
				}
			}
			index++;
			if(document.getElementById('divResult'+txtdivid+index))
			{
				var div_id=document.getElementById('divResult'+txtdivid+index);
				div_id.className='over';
				document.getElementById(txtid).value = div_id.innerHTML;
				selectFirst=div_id.innerHTML;
			}
		}
	}
}

var upArrowKey = function(txtdivid){
	if(txtdivid)
	{
		if(index>0){
			for(var i=0;i<length;i++){

				var div_id=document.getElementById('divResult'+txtdivid+i);
				if(div_id)
				{
					if(div_id.className=='over')
						index=div_id.id.split('divResult'+txtdivid)[1];
					div_id.className='normal';
				}
			}
			index--;
			if(document.getElementById('divResult'+txtdivid+index))
				document.getElementById('divResult'+txtdivid+index).className='over';
			if(txtid && document.getElementById('divResult'+txtdivid+index))
			{
				document.getElementById(txtid).value=
					document.getElementById('divResult'+txtdivid+index).innerHTML;
				selectFirst=document.getElementById('divResult'+txtdivid+index).innerHTML;
			}
		}
	}
}

var overText=function (div_value,txtdivid) {

	for(var i=0;i<length;i++)
	{
		if(document.getElementById('divResult'+i))
		{
			var div_id=document.getElementById('divResult'+i);

			if(div_id.className=='over')
				index=div_id.id.split(txtdivid)[1];
			div_id.className='normal';
		}
	}
	div_value.className = 'over';
	document.getElementById(txtid).value= div_value.innerHTML;
}
//---------------school search----------------
function getSchoolMasterAutoCompQuestion(txtSearch,event,txtdivid,txtId,hiddenId,type)
{
	hiddenId=hiddenId;
	divid=txtdivid;
	txtid=txtSearch.id;
	if(event.keyCode==40){
		downArrowKey(txtdivid);
	} 
	else if(event.keyCode==38) //up key
	{
		upArrowKey(txtdivid);
	} 
	else if(event.keyCode==13) // RETURN
	{
		if(document.getElementById(divid))
			document.getElementById(divid).style.display='block';
		
		document.getElementById("schoolName").focus();
		
	} 
	else if(event.keyCode==9) // Tab
	{
		
	}
	else if(txtSearch.value!='')
	{
		index=-1;
		length=0;
		document.getElementById(divid).style.display='block';
		searchArray = getSchoolArrayQuestion(txtSearch.value);
		fatchData2Question(txtSearch,searchArray,txtId,txtdivid);
		
	}
	else if(txtSearch.value==""){
		document.getElementById(divid).style.display='none';
	}
}

function getSchoolArrayQuestion(schoolName){
	var searchArray = new Array();
	
	var districtId	= $("#districtIdForDSPQ").val();	
	DistrictPortfolioConfigAjax.getFieldOfSchoolList(districtId,schoolName,{ 
			async: false,
			callback: function(data){
			hiddenDataArray = new Array();
			showDataArray = new Array();
			for(i=0;i<data.length;i++){
				searchArray[i]=data[i].schoolName;
				showDataArray[i]=data[i].schoolName;
				hiddenDataArray[i]=data[i].schoolMaster.schoolId;
			}
		},
		errorHandler:handleError
		});	
	return searchArray;
}


selectFirst="";

var fatchData2Question= function(txtSearch,searchArray,txtId,txtdivid){

	var result = document.getElementById(txtdivid);
	try{
		result.style.display='block';
		result.innerHTML = '';
		var items='';
		count=0;
		var len=searchArray.length;
		if(document.getElementById(txtId).value!="")
		{
			
			for(var i=0;i<len;i++){
					items += "<div id='divResult"+txtdivid+count+"'  class='normal'>" +
					searchArray[i].toUpperCase() + "</div>";
					count++;
					length++;
					
				if(count==10)
					break;
			}
			
		}
		else {
			
		}
		if(count!=0)
			result.innerHTML = items;
		else{
			result.style.display='none';
			selectFirst="";
		}
		if(txtSearch.value!='' && document.getElementById('divResult'+txtdivid+0))
		{
			document.getElementById('divResult'+txtdivid+0).className='over';
			selectFirst=document.getElementById('divResult'+txtdivid+0).innerHTML;
		}
		
		scrolButtom();
	}catch (err){}
}
function hideSchoolMasterDivQuestion(dis,hiddenId,divId)
{
	document.getElementById(hiddenId).value="";
	if(parseInt(length)>0){
		if(index==-1){
			index=0;
		}
		if(hiddenDataArray && hiddenDataArray[index] && document.getElementById(hiddenId)){
			document.getElementById(hiddenId).value=hiddenDataArray[index];
		}
		if(dis.value==""){
			document.getElementById(hiddenId).value="";
		}
		else if(showDataArray && showDataArray[index]){
			dis.value=showDataArray[index];
			document.getElementById(hiddenId).value=hiddenDataArray[index];			
		}
		
	}else{
		if(document.getElementById(hiddenId))
			document.getElementById(hiddenId).value="";
	}
	
	if(document.getElementById(divId))
	{
		document.getElementById(divId).style.display="none";
	}
	index = -1;
	length = 0;
}

function onSelectOpenOptions(src,cnt){
	
	if(src=="show"){
		$('.textareaOp'+cnt).show();
	}else{
		$('.textareaOp'+cnt).hide();
		
		$('.'+cnt+'OSONPtext').val("");
	}
}

function openTFAOptionOther(valueId){
	var res =  $('#tfaOptId'+valueId).is(":checked");
	
	if(res==true){$("#tfaOthText").show();
	}else{
		$("#tfaOthText").hide();
	}
	
	
	}	
function closeAffOpenCl(){
	$("#affDivPHiL").hide();
	$("#myModalDymanicPortfolio").show();	
}
function showStdTchrSgrid(){
	if(document.getElementById('StuTchrChk').checked==true)
		$(".stuTchrExpDiv").show();
	else
		$(".stuTchrExpDiv").hide();
		hideStdTchrExp();
}
function showPhiladelphiaPdf()
{
	  var left = (screen.width/2)-(600/2);
	  var top = (screen.height/2)-(700/2);
	  return window.open("images/District_Map_LN_8x11_2014_09_05%5B1%5D%20copy.pdf", resourceJSON.msgPhiladelphiaGeoZone, "width=600, height=600,top="+top+",left="+left);
}

function closeDspqOPenCl(){
	$("#wrongDivPHiL").hide();
	//location.reload();
	var jobId = $("#jobId").val();
	window.location.href="applyjob.do?jobId="+jobId;
}
function checkEmployeeCode(){
	var districtIdForDSPQ = $("#districtIdForDSPQ").val();	
	
	//if($('#empfe2').val().length==10 && districtIdForDSPQ=="4218990" && $('#jobcategoryDsp').val().trim()=="Principal"){
	if(districtIdForDSPQ=="4218990" && $('#jobcategoryDsp').val().trim()=="Principal"){		
		var text="";
		var empCode= $('#empfe2').val();
		var countl =10-empCode.length;
		for (i = 0; i < countl; i++) {
			text+="0";
		}
		var finalempcode = text+empCode;

		DistrictPortfolioConfigAjax.checkEmployee(finalempcode,districtIdForDSPQ,{  
			async: false,
			callback: function(data){
			//alert(data);
			if(data[0]!=null){
				//if($('input:radio[name=staffType]').is(":checked") || $("#staffTypeSession").val()=="I"){
				if($("#isPrinciplePhiladelphia").val()=="1" && data[1]=="false" && data[0]==1 || data[0]==3){
					$("#myModalDymanicPortfolio").hide();				
					$("#affDivPHiL").hide();
					$("#dynamicInfoMsg").html(resourceJSON.msgCoverLetterdetails);
					$("#wrongDivPHiL").show();
				}else if(data[1]=="true" && data[0]==0){
					$("#myModalDymanicPortfolio").hide();
					
					$("#affDivPHiL").hide();
					$("#dynamicInfoMsg").html(resourceJSON.msgNonPrincipalCoverLetter);
					$("#wrongDivPHiL").show();
				}
				}else{
					$("#myModalDymanicPortfolio").hide();
					
					$("#affDivPHiL").hide();
					$("#dynamicInfoMsg").html(resourceJSON.msgNonPrincipalCoverLetter);
					$("#wrongDivPHiL").show();
				}
		},
		errorHandler:handleError
		});	
	}
}

function saveAnswer(fileName,path)
{
	//alert("fileName=="+fileName);
	//alert("path=="+path);
	$('#answerFileName').val(fileName);
	$('#answerFilePath').val(path);
	//document.getElementById("frmAnswerUpload").reset();
	var windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
	document.getElementById("divAnswerTxt").innerHTML="<a href='javascript:void(0)' rel='tooltip' data-original-title='Click here to view Answer !' id='hrefAnswer' onclick=\"downloadAnswer(1);"+windowFunc+"\">"+resourceJSON.ViewLink+"</a>";
	$('#hrefAnswer').tooltip();
}

function downloadAnswer(flag, answerId)
{
	if(flag=='0')
	{
		//alert("downloadAnswer0");
		PFExperiences.downloadAnswer(answerId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadFrameAnswerId').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefAnswer").href = data; 
					return false;
				}
			}
		});
	}
	else if(flag=='1')
	{
		//alert("downloadAnswer1");
		//alert($("#answerFilePath").val());
		document.getElementById("hrefAnswer").href = $("#answerFilePath").val();
	}
}

/*function uploadAnswer(questionId)
{
	//var questionId = document.getElementById("hdnQues").value;
	//alert("questionId=="+questionId);
	$('#errAnswer').empty();
	var cntErr=0;
	var fileName = document.getElementById(questionId).value;
	var ext = fileName.substr(fileName.lastIndexOf('.') + 1).toLowerCase();
	//alert("fileName=="+fileName);
	//alert("ext=="+ext);
	if(fileName=="")
	{
		$('#errAnswer').append("&#149; Please select file.</BR>");
		cntErr++;
	}
	if(ext!="")
	{
		if(!(ext=='jpg' || ext=='jpeg' || ext=='gif' || ext=='png' || ext=='pdf' || ext=='doc' || ext=='docx' || ext=='txt'))
		{
			$('#errAnswer').append("&#149; Please select Acceptable Answer formats which include PDF, MS-Word, GIF, PNG, and JPEG  files</BR>");
			cntErr++;
		}
	}
	else if(fileSize>=10485760){
		$('#errAnswer').append("&#149; Score Report File size must be less than 10mb.<br>");
		$('#generalKnowledgeScoreReport').css("background-color",txtBgColor);
		cnt_GeneralKnowledge++;focs++;
	}
	if(cntErr==0){
		var newName=$("#districtIdForDSPQ").val()+fileName 
		$('#answerFileName').val(newName);
		document.getElementById('frmAnswerUpload').submit();
		return true;
	}
}*/

function downloadPortfolioReport(teacherId, linkId)
{
	$('#loadingDiv').show();
	CandidateReportAjax.downloadPortfolioReport(teacherId,
	{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$('#loadingDiv').hide();
			if(data=="")
			{
					document.getElementById(linkId).href = "javascript:void(0)";
					alert(resourceJSON.MsgPortfolioNotCompleted)
					return true;
			}
			else	
			{
				
				//window.location.href=data;
  				//window.focus();
				document.getElementById(linkId).href = data; 
				return false;
			}
				
		}
	});
}


function saveOctUpload(){
	var octNumber=$("#octNumber").val();
	var octId=$("#octId").val();
	var octText =$('#octText').find(".jqte_editor").html();			
	var hdnOctUpload =document.getElementById("hdnOctUpload").value;
	var octUpload =document.getElementById("octUpload").value;
	var octExt = "";
	
	if(octUpload!=""){
		octExt = octUpload.substr(octUpload.lastIndexOf('.') + 1).toLowerCase();
	}
	$('#octNumber').css("background-color","");
	$('#octUpload').css("background-color","");
	var cnt=0;
	var focs=0;	
	$("#octErrors").empty();
	var octOption =0;
	if(document.getElementById("currMemOct1").checked){
		octOption = document.getElementById("currMemOct1").value;
	}	
	
	if(document.getElementById("currMemOct1").checked){/*
		if(octNumber==""){
			$("#octErrors").append("&#149; "+resourceJSON.msgOCTNumber+"</BR>");
			if(focs==0)
				$('#octNumber').focus();

			$('#octNumber').css("background-color",txtBgColor);
			cnt++;focs++;
		}
		if(octText==""){
			//octChk=true;					
		}				
		if(octUpload=="" && hdnOctUpload==""){
			$("#octErrors").append("&#149; "+resourceJSON.msguploadOCTCard+"</BR>");
			if(focs==0)
				$('#octUpload').focus();

			$('#octUpload').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	*/}

	if(octExt!="")
	{
		if(!(octExt=='jpg' || octExt=='jpeg' || octExt=='gif' || octExt=='png' || octExt=='pdf' || octExt=='doc' || octExt=='docx' || octExt=='txt'))
		{
			$('#octErrors').append("&#149; "+resourceJSON.msgselectAcceptable+" "+dwr.util.getValue("QS"+i+"question")+" "+resourceJSON.msgFormatsWhichIncludes+"</BR>");
			if(focs==0)
				$('#octUpload').focus();

			$('#octUpload').css("background-color",txtBgColor);
			cnt++;focs++;
		}
	}
	
	
	var erorr=false;
	if(cnt!=0){
		erorr=true;
	}
	
	if(!erorr)
	if(octUpload!=""){
		//saveOctUpload(fileName);
		$("#frmOctUpload").submit();

	}else{
		PFExperiences.saveOctDetails(octId,octOption,octNumber,hdnOctUpload,octText,{
							async: false,
				errorHandler:handleError,
				callback: function(data)
				{
			getOctFields();
				}
		});
	}
	
}

function saveOctUploadFile(filename){
	var octNumber=$("#octNumber").val();
	var octId=$("#octId").val();
	var octText =$('#octText').find(".jqte_editor").html();			
	var hdnOctUpload =document.getElementById("hdnOctUpload").value;
	var octUpload =document.getElementById("octUpload").value;
	var octExt = "";
	if(octUpload!=""){
		octExt = octUpload.substr(octUpload.lastIndexOf('.') + 1).toLowerCase();
	}
	var cnt=0;
	var focs=0;	
	var octOption =0;
	if(document.getElementById("currMemOct1").checked){
		octOption = document.getElementById("currMemOct1").value;
	}

		PFExperiences.saveOctDetails(octId,octOption,octNumber,filename,octText,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data)
				{
			getOctFields();
				}			
		});
	
}

function getOctFields(){
	PFExperiences.getOctDetails({ 
		async: false,
		errorHandler:handleError,
		callback: function(data)
		{
			if(data!=null){
				$("#octNumber").val(data.octNumber);
				$("#octId").val(data.octId);
				document.getElementById("hdnOctUpload").value=data.octFileName;
				$('#octText').find(".jqte_editor").html(data.octText);
				if(data.octOption==1){
					document.getElementById("currMemOct1").checked=true;
				}else{
					document.getElementById("currMemOct2").checked=true;
				}
				
				if(data.octFileName!=null && data.octFileName!="")					
					$("#displayOctFile").show();
				else
					$("#displayOctFile").hide();
			}
		}			
});
}


function downloadOctUpload(octId)
{
		//alert("downloadAnswer0");
		PFExperiences.downloadOctUpload(octId,{ 
			async: false,
			errorHandler:handleError,
			callback:function(data)
			{
				if (data.indexOf(".doc") !=-1)
				{
					document.getElementById('uploadOctID').src = ""+data+"";
				}
				else
				{
					document.getElementById("hrefOctUpload").href = data; 
					return false;
				}
			}
		});
	}

function getSeniorityNumber(){
	
	var jobOrder = {jobId:document.getElementById("jobId").value};
	PFExperiences.getSeniorityNumber(jobOrder,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{
			$("#seniorityNumb").val(data);
		}
	});


}
function saveSeniorityNumber(){

	var seniorityNumb = $("#seniorityNumb").val();
	var jobId=document.getElementById("jobId").value;
	
	PFExperiences.addSeniorityNumber(seniorityNumb,jobId,{ 
		async: false,
		errorHandler:handleError,
		callback:function(data)
		{}
	});

}

function sameAsAddress(){
	
	if($('input:checkbox[id=sameAddress]').is(":checked")){
		$("#addressLine1").val($("#addressLinePr").val());
		$("#addressLine2").val($("#addressLine2Pr").val());
		$("#zipCode").val($("#zipCodePr").val());
		$("#countryId").val($("#countryIdPr").val());
		
		$("#divothercity").hide();
		$("#divotherstate").hide();
		$("#divUSACity").hide();
		$("#divUSAState").hide();

		if($("#divUSAStatePr").is(':visible')){
			$("#divUSAState").show();
			$("#stateIdForDSPQ").html($("#stateIdForDSPQPr").html());
			$("#stateIdForDSPQ").val($("#stateIdForDSPQPr").val());
		}	
		if($("#divUSACityPr").is(':visible')){
			$("#divUSACity").show();
			$("#cityIdForDSPQ").html($("#cityIdForDSPQPr").html());
			$("#cityIdForDSPQ").val($("#cityIdForDSPQPr").val());
		}
		if($("#divotherstatePr").is(':visible')){
			$("#divotherstate").show();
			$("#otherState").val($("#otherStatePr").val());
		}		
		if($("#divothercityPr").is(':visible')){
			$("#divothercity").show();
			$("#otherCity").val($("#otherCityPr").val());
		}
	}
	
	//$("#stateIdForDSPQ").html($("#stateIdForDSPQPr").html());
}
//jeffco specific Employeement detail Start/
function openEmployeeNumberInfo(empId,zipCode){
	var empfe2= empId;//$("#empfe2").val();
	var tempCode=$("#jeffcoSeachDiv").html();
	$("#jeFFcoEmployeeInf").hide();
	if(empfe2==""){		
		$("#jeffcoSeachDiv").html("<div class='required' id='empInfoDetError'>&#149; Please provide Employee Number.</div>"+tempCode);
		$("#jeffcoSeachDiv").addClass("top5");
	    $("#jeffcoSeachDiv").removeClass("top20");
	}
	else{
		$("#empInfoDetError").remove();
		$("#jeffcoSeachDiv").addClass("top20");
	    $("#jeffcoSeachDiv").removeClass("top5");
		 
	$('#loadingDiv_dspq_ie').show();
	  try{
		  TeacherProfileViewInDivAjax.openEmployeeNumberInfo(empfe2,{
			  async: false,
			  cache: false,
			  errorHandler:handleError,
			  callback:function(data){			 
			  //alert(data);
			  var divData=data.split('####');
			  if(divData[0].trim()=='1'){
				  //$("#jeFFcoEmployeeInf").show();
				  //	$('#jeFFcoEmployeeInf').html(divData[1]);
				  	
				  	$("#jeffcoSeachDiv").html("<div class='required' id='empInfoDetError'>&#149; "+divData[1]+".</div>"+tempCode);
					$("#jeffcoSeachDiv").addClass("top5");
				    $("#jeffcoSeachDiv").removeClass("top20");
				    
				    //for Employee master
				    try{
						  $('[name="apiResponseFlag"]').remove();
					  }catch(e){}
				    $("#jeFFcoEmployeeInf").html($("#jeFFcoEmployeeInf").html()+"<input type='hidden' value='1' name='apiResponseFlag'/>");
				    //ended for Employee master
			  }else{				  
			var apiZipCode=$("#jeFFcoEmployeeInf .jeffCoApiPostal:nth-child(3)").text();
			  var currentZipCode=zipCode;//$("#zipCode").val();//$("#zipCodeCoverLtr").val();
			  
			  $("#empInfoDetError").remove();
				$("#jeffcoSeachDiv").addClass("top20");
			    $("#jeffcoSeachDiv").removeClass("top5");
			    
			  	$('#jeFFcoEmployeeInf').html("<p style='margin-left: 20px;'>In order to change any of the pre-populated information below, please contact the Jeffco HR team.</p>"+data);
			  	
				  //for Employee master
				  try{
					  $('[name="apiResponseFlag"]').remove();
				  }catch(e){}
				  $("#jeFFcoEmployeeInf").html($("#jeFFcoEmployeeInf").html()+"<input type='hidden' value='0' name='apiResponseFlag'/>");
				  //ended for Employee master
				  
				  //if(apiZipCode==zipCode)
				  {
					  $("#jeFFcoEmployeeInf").show();
					  
					  if($("#addressLine1").val()==""){
						  $("#addressLine1").val($("#jeFFcoEmployeeInf .jeffCoApiAdd1:nth-child(3)").text());
					  }
					  if($("#addressLine2").val()==""){
						  $("#addressLine2").val($("#jeFFcoEmployeeInf .jeffCoApiAdd2:nth-child(3)").text());
					  }
					  //changeCityStateByZipForDSPQ() zipcode 
					  //getCityListByStateForDSPQ()   city
					  //alert("hello    "+$('#countryId').find('option[text="Austria"]').val());
					  if($("#countryId").val()==""){

						  try{
							  setTimeout(function(){ 							  
							  var tempCoutyText = $("#jeFFcoEmployeeInf .jeffCoApiCountry:nth-child(3)").text();
							  var tempCountyId = $("#countryId" + " option").filter(function() {  return this.text == tempCoutyText}).val();
							  $("#countryId").val(tempCountyId).attr("selected","selected");
							  getStateByCountryForDspq('dspq');
							  }, 1000);
						  } catch (e) {}
					  }
					  if($("#zipCode").val()==""){
						  $("#zipCode").val(apiZipCode);
						  try{
							  changeCityStateByZipForDSPQ();
						  } catch (e) {}
					  }
					  if($("#stateIdForDSPQ").val()==""){
						  try{
							  var tempStateText = $("#jeFFcoEmployeeInf .jeffCoApiState:nth-child(3)").text();					  
							  var stVal = $('#stateIdForDSPQ option[shortname="'+tempStateText+'"]').val();
							  setTimeout(function(){ $("#stateIdForDSPQ option[value='"+stVal+"']").attr("selected","selected");getCityListByStateForDSPQ();}, 3000);
						  } catch (e) {}
					  }
					  
					  if($("#cityIdForDSPQ").val()==""){
							try{
							  setTimeout(function(){ 
								  var tempCityText = $("#jeFFcoEmployeeInf .jeffCoApiCity:nth-child(3)").text().toUpperCase();						  
								  var tempCityId = $("#cityIdForDSPQ" + " option").filter(function() { return this.text == tempCityText}).val();						  
								  $("#cityIdForDSPQ").val(tempCityId).attr("selected","selected");
							  }, 3500);
							} catch (e) {}
					  }
					  
					
					 // $("#cityIdForDSPQ").val();
				  }
			  }
			  $('#loadingDiv_dspq_ie').hide();
			  }
		   });
	  }catch(e){alert(e)}
	}
	  $('#loadingDiv_dspq_ie').hide();
}

function getEmployeeNumberInfo(flag,table){
		$('#jeFFcoEmployeeInf').html(table);		
	$('#loadingDiv_dspq_ie').hide();
 }

//jeffco specific Employeement detail end/
function callForMiamiDistrict(){
	setTimeout(function(){
		try{
			var districtId=document.getElementById("districtIdForDSPQ").value;
			if(districtId!=1200390){
				$('.miamionly').removeClass('miamionly_show').addClass('miamionly');
				$('#raceDiv .portfolio_Subheading').html('Race/Ethnicity');
				try{
					var raceValueObj=$('[name="raceId"]');
					for(var r=0;r<raceValueObj.length;r++){
						if($('[name="raceId"]:eq('+r+')').val()==4){
							$('[name="raceId"]:eq('+r+')').parents('.col-sm-3.col-md-3').css('display','block');
						}
					}
				}catch(ee){}
			}else{
				$('.miamionly').removeClass('miamionly').addClass('miamionly_show');
				$('#raceDiv .portfolio_Subheading').html('Race');
				try{
					var raceValueObj=$('[name="raceId"]');
					for(var r=0;r<raceValueObj.length;r++){
						if($('[name="raceId"]:eq('+r+')').val()==4){
							$('[name="raceId"]:eq('+r+')').parents('.col-sm-3.col-md-3').css('display','none');
						}
					}
				}catch(ee){}
			}
		}catch(ee){alert(ee);}
		},700);
}

function checkForIntEmpNum(evt){
	var districtIdForDSPQ = $("#districtIdForDSPQ").val();
	
	if(districtIdForDSPQ==1200390){
		var charCode = ( evt.which ) ? evt.which : event.keyCode;
		if(charCode==8)
			return true;
	
		return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
	}
}
$(function(){
	
	$("#empfe1").on("paste", function(event) {
		var districtIdForDSPQ = $("#districtIdForDSPQ").val();		
		if(districtIdForDSPQ==1200390){
			var charCode = ( event.which ) ? event.which : event.keyCode;
			if(charCode==8)
				return true;		
			return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
		}

	});

	$("#empfe2").on("paste", function(event) {
		var districtIdForDSPQ = $("#districtIdForDSPQ").val();		
		if(districtIdForDSPQ==1200390){
			var charCode = ( event.which ) ? event.which : event.keyCode;
			if(charCode==8)
				return true;		
			return( (charCode >= 48 && charCode <= 57)||(charCode==13) );
		}
	});
	
	$("#workForKelly1").click(function() {  
		$("#kellYnxtQ").show();
	});

	$("#workForKelly2").click(function() {  
			$("#kellYnxtQ").hide();
			$("#contactedKelly1").prop("checked",false);
			$("#contactedKelly2").prop("checked",false);
			//continueBtnNxt
			var isDspqReqForKelly=document.getElementById("isDspqReqForKelly").value;
			if(isDspqReqForKelly=="true")
			{
				if (typeof checkCL_dynamicPortfolio != 'undefined' && $.isFunction(checkCL_dynamicPortfolio)) {
					checkCL_dynamicPortfolio();
				} else {
					$('#applyTeacherJob').submit();
				}
			}
			else
			{
				var isExternal=document.getElementById("isExternal").value;
				if(isExternal=="1")
					$('#applyTeacherJob').submit();	
				else
					getDistrictSpecificQuestion();
			}
			
			
	});

	$("#contactedKelly1").click(function() {  
		$("#kellYcvRLtrMain").hide();
		$("#kellYnxtQ").hide();
		$("#kellYnxtInst").show();
		$(".continueBtnNxt").show();
	});

	$("#contactedKelly2").click(function() {  
		$(".cvrLtrClose").html("Ok");
		var jobId=document.getElementById("jobId").value;
			DistrictPortfolioConfigAjax.getDetailOnJObLevel(jobId,{ 
				async: true,
				errorHandler:handleError,
				callback: function(data)
				{
					$("#kellYcvRLtrMain").hide();
					$("#kellYnxtQ").hide();
					$("#kellYnxtDisDivData").html(data);
					$("#kellYnxtDisDivInf").show();
				}});
	});
	
	$(".cvrLtrClose").click(function() {  		
		$("#forKellyCvrLtr").hide();
		$("#kellYcvRLtrMain").show();
		$("#kellYnxtInst").hide();
		$("#kellYnxtDisDivInf").hide();
		$("#forAllDistCvrLtr").show();
		$("#workForKelly2").prop("checked",false);
		$("#workForKelly1").prop("checked",false);
		$("#contactedKelly1").prop("checked",false);
		$("#contactedKelly2").prop("checked",false);
		if($(".cvrLtrClose").html()=="Ok"){
			window.location.href="userdashboard.do";
		}
	});
})

function messageShowOrNot(dob,last4SSN){
	var employeeId="0";
	var messageShowFlag=false;
	try{
	var checkDistrictId=document.getElementById("districtIdForDSPQ").value;
	var isAffilated=$('#isAffilated').is(':checked');
	//alert('checkDistrictId='+checkDistrictId+"      isAffilated===="+isAffilated+"     employeeId======"+employeeId)
	//if(isAffilated && checkDistrictId!=null && checkDistrictId==804800 && employeeId>0){
		if(checkDistrictId!=null && checkDistrictId==804800){	
		//=============================================For Employee Master =======================================
		//if($('[name="apiResponseFlag"]').val()==0 || $('[name="apiResponseFlag"]').val()==1){
		try{
		    DistrictPortfolioConfigAjax.doNotHiredForJeffco(dob,last4SSN.trim(),{
		    	preHook:function(){$('#loadingDiv').css('z-index','99999px');$('#loadingDiv').show()},
				postHook:function(){$('#loadingDiv').css('z-index','99999px');$('#loadingDiv').hide()},
	              async: false,
	              errorHandler:handleError,
	              callback:function(data){
		    		if(data!=null){
			    		if(data.split("##")[1]=='A'){
			    			$('#isAffilated').prop('checked',true);
			    			$("input[name=staffType][value='I']").attr('checked', 'checked');
			    		}
			    		if(data.split("##")[0].trim()!='')
			    			$('#empNumCoverLtr').val(data.split("##")[0]);
		    		}
		    		
	              //alert("success");
	              //messageShowFlag= true;
	        }
	              
	        });

		}catch(err){}
		//}
		//=============================================Ended For Employee Master =======================================
		try{
			try{
				employeeId=$('#empNumCoverLtr').val();
				//alert(employeeId);
			}catch(ee){employeeId="0";}
		DistrictPortfolioConfigAjax.getDoNotHireStatusByEmpoloyeeId(employeeId,checkDistrictId,dob,last4SSN.trim(),{
			preHook:function(){$('#loadingDiv').show()},
			postHook:function(){$('#loadingDiv').hide()},
			async: false,
			errorHandler:handleError,
			callback: function(data)
			{
				var callbackInfo=data.split('##');
				if(callbackInfo[0]==1){
					messageShowFlag=true;
					getConfirmBox(callbackInfo[1],callbackInfo[2], "Ok");
				}
			}
		});
		}catch(e){alert(e);}
	}
	}catch(ee){}
	if(messageShowFlag)
		return true;
	else 
		return false;
}
function callForJeffcoDoNotHireAPI(){
	$('#errordivCL1').html('');
	$('#errordivCL1').hide();
	$('#last4SSN').css("background-color","");
	var dob="";
	var dobYear=$("#dobYear1").val();
	dobYear=trim(dobYear);
	var idobYear = new String(parseInt(dobYear));
	var currentFullYear = new Date().getFullYear();
	currentFullYear=currentFullYear-1;
	var dobMonth=$("#dobMonth1").val();
	var dobDay=$("#dobDay1").val();
	var scrollError="errordivCL1";
	var validDOB=true;
	if($('#last4SSN').val().trim()=="")
	{
		$('#errordivCL1').append("&#149; "+resourceJSON.msgSocialSecurityNumber+"<br>");
		$('#last4SSN').css("background-color",txtBgColor);
		$('#last4SSN').css('width','250px');
		$('#errordivCL1').show();
		validDOB=false;
	}
	if($('#last4SSN').val().trim()!='' && $('#last4SSN').val().trim().length<4)
	{
		$('#errordivCL1').append("&#149; Please enter valid Social Security Number<br>");
		$('#errordivCL1').css('width','250px');
		$('#last4SSN').css("background-color",txtBgColor);
		$('#errordivCL1').show();
		validDOB=false;
	}
	validDOB=validateDOB('dobDay1','dobMonth1','dobYear1','errordivCL1');
	if(!validDOB){
		document.getElementById(scrollError).scrollIntoView();
		return false;
	}
	dob=trim(dobMonth.trim().length==1?"0"+dobMonth.trim():dobMonth.trim())+trim(dobDay.trim().length==1?"0"+dobDay.trim():dobDay.trim())+trim(dobYear);
	$('#errordivCL1').hide();
	$('#errordivCL1').html('');
		if(messageShowOrNot(dob,$('#last4SSN').val())){
			return false;
		}
	return true;
}
var monthText={};
monthText[1]="January";
monthText[2]="February";
monthText[3]="March";
monthText[4]="April";
monthText[5]="May";
monthText[6]="June";
monthText[7]="July";
monthText[8]="August";
monthText[9]="September";
monthText[10]="October";
monthText[11]="November";
monthText[12]="December";
function monthArray(k) {
	return monthText[k];
}
var monthDays={};
monthDays[1]=31;
monthDays[2]=29;
monthDays[3]=31;
monthDays[4]=30;
monthDays[5]=31;
monthDays[6]=30;
monthDays[7]=31;
monthDays[8]=31;
monthDays[9]=30;
monthDays[10]=31;
monthDays[11]=30;
monthDays[12]=31;
function monthDaysArray(k) {
	return monthDays[k];
}
function callSetDayAndMonth(districtId){
	if(districtId.trim()=='' || districtId.trim()==null || districtId.trim()==undefined)
	districtId= document.getElementById("districtIdForDSPQ").value;
	if(districtId==804800){
		$('.jeffcoRequired').show();
		$('.notforJeffco').hide();
		$('#dobMonth1').html(getMonth());
		getDay();
		if($('[name="candidateDOB"]').val().trim()!=''){
			var candidateDOB=$('[name="candidateDOB"]').val().trim().substring(0,10).split("-");
			var year=candidateDOB[0];
			var month=candidateDOB[1];
			var day=candidateDOB[2];
			$('#dobMonth1').val(parseInt(month));
			getDay();
			$('#dobMonth1').trigger("change");
			$('#dobDay1').val(parseInt(day));
			$('#dobYear1').val(year);
			//alert("day=="+day+"  month=="+month+"  year=="+year);
		}
	}
}
function getMonth(){
	var month="<option value='0'>Select Month</option>";
	for(var i=1;i<=12;i++)
		month+="<option value="+i+">"+monthArray(i)+"</option>";
	return month;
}
function getDay(){
	var day="<option value='0'>Select Day</option>";
	try{
	for(var i=1;i<=monthDaysArray($('#dobMonth1').val());i++)
		day+="<option value=\""+i+"\">"+i+"</option>";
	}catch(e){}
	$('#dobDay1').html(day);
}
function leapYear(year)
{
	return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
}
function validateDOB(validateDayID,validateMonthID,validateYearID,errorId){
		var scrollError=errorId;
		$('#'+errorId).hide();
		$('#'+errorId).empty();
		$('#'+validateYearID).css("background-color","");
		$('#'+validateMonthID).css("background-color","");
		$('#'+validateDayID).css("background-color","");
		var returnFlag=true;
		var dobYear=$("#"+validateYearID).val();
		dobYear=trim(dobYear);
		var idobYear = new String(parseInt(dobYear));
		var currentFullYear = new Date().getFullYear();
		currentFullYear=currentFullYear-1;
		
		var dobMonth=$("#"+validateMonthID).val();
		var dobDay=$("#"+validateDayID).val();
		
		if(trim(dobMonth)=="0")
		{
			$('#'+errorId).append("&#149; "+resourceJSON.msgMonthofBirth+"<br>");
			$('#'+validateMonthID).css("background-color",txtBgColor);
			$('#'+errorId).show();
			returnFlag= false;
		}
		
		if(trim(dobDay)=="0")
		{
			$('#'+errorId).append("&#149; "+resourceJSON.msgDayofBirth+"<br>");
			$('#'+validateDayID).css("background-color",txtBgColor);
			$('#'+errorId).show();
			returnFlag= false;
		}
		
		if((dobYear=="") || ( dobYear==0 || idobYear=="NaN") || (idobYear > currentFullYear || idobYear < 1931))
		{
			$('#'+errorId).append("&#149; "+resourceJSON.msgValidYearBirth+"<br>");
			$('#'+validateYearID).css("background-color",txtBgColor);
			$('#'+errorId).show();
			returnFlag= false;
		}	
		if(!returnFlag){
			document.getElementById(scrollError).scrollIntoView();
			return false;
		}
		return returnFlag;
}

function getConfirmBox(header,confirmMessage,buttonNameChange){
	$('#loadingDiv').show();
	var div="<div class='modal hide in' id='jeffcoConfirmbox' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true' style='z-index:999999;'>"+
			"<div class='modal-dialog' style='width:550px;'>"+
			"<div class='modal-content'>"+
			"<div class='modal-header'>"+
	        "<button type='button' class='close confirmCloseCut' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>"+
	        "<h4 class='modal-title' id='myModalLabel' style='color:white;'></h4>"+
	      "</div>"+
	      "<div class='modal-body' id='confirmMessage'>"+	      
	      "</div>"+
	      "<div class='modal-footer'>"+
	      "<button type='button' class='btn btn-primary' id='confirmTrue' ></button>"+
	        "<!--<button type='button' class='btn btn-default' id='confirmFalse'>"+buttonNameChange+"</button>-->"+	        
	      "</div>"+
	    "</div>"+
	  "</div>"+
	"</div>";
	$('#loadingDiv').show();
	 var coverShowOrNot=0;
	 var timeDur=0;
	    try{coverShowOrNot=$('#coverShowOrNot').val();timeDur=6000;}catch(e){coverShowOrNot=0;}
	try{$('#myModalv').modal('hide');}catch(e){}
	$('#loadingDiv').show();
	setTimeout(function(){ 
	$('#loadingDiv').show();
	try{
	$('#loadingDiv').after(div);
	}catch(e){}
	/*setTimeout(function(){$('#topErrorMessageDSPQ').hide();},100);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},200);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},300);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},400);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},500);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},600);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},700);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},800);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},900);
	setTimeout(function(){$('#topErrorMessageDSPQ').hide();},1000);*/
    confirmMessage = confirmMessage || '';
    $('#jeffcoConfirmbox').modal({show:true,
                            backdrop: "static",
                            keyboard: false,
    });
    $('#loadingDiv').hide();
    try{$('#myModalCL').css('z-index','1000');}catch(e){}
    $('#jeffcoConfirmbox').css('z-index','99999');

    $('#jeffcoConfirmbox #confirmMessage').html(confirmMessage);
    $('#jeffcoConfirmbox #myModalLabel').html(header);
    $('#jeffcoConfirmbox #confirmTrue').html(buttonNameChange);
    var height=(screen.height/2)-$('#jeffcoConfirmbox .modal-dialog').height();
    $('#jeffcoConfirmbox .modal-dialog').css('margin-top',height);
    $('#confirmTrue').click(function(){
    	try{$('#myModalCL').css('z-index','99999');}catch(e){}
    	$('#jeffcoConfirmbox').modal('hide');
    	try{if(coverShowOrNot==0)$('#myModalCL').modal('show');}catch(e){}
        $('#jeffcoConfirmbox').remove();
    });
    $('.confirmCloseCut').click(function(){
    	try{$('#myModalCL').css('z-index','99999');}catch(e){}
    	$('#jeffcoConfirmbox').modal('hide');
    	try{if(coverShowOrNot==0)$('#myModalCL').modal('show');}catch(e){}
        $('#jeffcoConfirmbox').remove();
    });
   },timeDur);
}
function columbusCheck(obj,questionId,teacherId,isPerform){
	//alert("isPerform=="+isPerform);
	if(isPerform==1){
	var error =0;
	PFCertifications.getAnswerData(questionId,teacherId,{ 
		async: false,
		errorHandler:handleError,
		callback: function(data){
		if(data!=null)
		{	//alert(data.insertedText);
			var insertedText = data.insertedText;
			var array     = insertedText.split("$$");
			$('#columbus_date1').val(array[0]);
			$('#columbus_date2').val(array[1]);
			$('#columbus_position').val(array[2]);
			console.log(data);
		}
	}
	});	
 try{
	 	$("#myModalDymanicPortfolio").modal('hide');
	 	$("#columbusSetting").modal('show');
	 	$("#columbusBtnSave").click(function(){
	 		//alert("aaaaa");
	 		var intialValue  = $(obj).val();
	 		//alert(intialValue);
	 		var columbus_date1 		= $('#columbus_date1').val();
	 		var columbus_date2 		= $('#columbus_date2').val();
	 		var columbus_position 	= $('#columbus_position').val();
	 		var d1 = Date.parse(columbus_date1);
	 		var d2 = Date.parse(columbus_date2);
	 		
	 		var dateArray1 = columbus_date1.split("-");
	 		var dateArray2 = columbus_date2.split("-");
	 		var d1 = new Date(dateArray1[2], dateArray1[0], dateArray1[1]);
	 		var d2 = new Date(dateArray2[2], dateArray2[0], dateArray2[1]);
	 		
	 		var errorStr ="";
	 		if(columbus_date1==""){
	 			error =1;
	 			errorStr += "<br/>Please select From Date";
	 			
	 		}
	 		if(columbus_date2==""){
	 			error =1;
	 			errorStr += "<br/>Please select To Date";
	 			
	 		}
	 		/*if(columbus_date1!=null && columbus_date2!=null){
	 			
	 			if(d1>d2){
	 				error =1;
		 			errorStr += "<br/>From Date should be less than To Date";	
	 			}
	 		}*/
	 			
	 			
	 		if(columbus_position==""){
	 			error =1;
	 			errorStr += "<br/>Please enter Postition";
	 			
	 		}
	 		if(error==0){
		 		var strValue = columbus_date1+"$$"+columbus_date2+"$$"+columbus_position;
		 		
		 		$("#QS"+questionId+"columbus").val(strValue);
		 		$("#columbusSetting").modal('hide');
		 		$("#myModalDymanicPortfolio").modal('show');
	 		}else{
	 			
	 			$("#errordivColumbus").html(errorStr);
	 		}
	 	});
	 }
 catch(e){}
	}else{
		//alert("#QS"+questionId+"columbus");
		$("#QS"+questionId+"columbus").val("");
	}
}
jQuery("document").ready(function(){
	$(".columbusSettingBtn").click(function(){	
		$("#myModalDymanicPortfolio").modal('show');
	 	$("#columbusSetting").modal('hide');	
	});
	
});
