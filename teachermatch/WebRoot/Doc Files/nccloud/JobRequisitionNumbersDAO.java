package tm.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherDetail;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StatusMaster;
import tm.dao.generic.GenericHibernateDAO;

public class JobRequisitionNumbersDAO extends GenericHibernateDAO<JobRequisitionNumbers, Integer> {

	JobRequisitionNumbersDAO()
	{
		super(JobRequisitionNumbers.class);
	}
	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersByJobWithSchool(JobOrder jobOrder,SchoolMaster schoolMaster)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion3 = Restrictions.eq("status",new Integer(0));
			if(schoolMaster!=null)
			{
				Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1,criterion2);
				else
					list = findByCriteria(criterion1,criterion2,criterion3);
				
			}
			else
			{
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1);
				else
					list = findByCriteria(criterion1,criterion3);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersByJobWithSchool(JobOrder jobOrder,SchoolMaster schoolMaster,String offerReady)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion3= Restrictions.eq("status",new Integer(0));
			
			if(schoolMaster!=null)
			{
				Criterion criterion2 = Restrictions.eq("schoolMaster",schoolMaster);
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1,criterion2);
				else{
					if((offerReady!=null && offerReady.equals("Offer Ready"))){
						list = findByCriteria(criterion1,criterion2);	
					}else{
						list = findByCriteria(criterion1,criterion2,criterion3);
					}
				}
				
			}
			else
			{
				if(jobOrder.getIsExpHireNotEqualToReqNo()!=null && jobOrder.getIsExpHireNotEqualToReqNo().equals(new Boolean(true)))
					list = findByCriteria(criterion1);
				else{
					if((offerReady!=null && offerReady.equals("Offer Ready"))){
						list = findByCriteria(criterion1);	
					}else{
						list = findByCriteria(criterion1,criterion3);
					}
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public JobRequisitionNumbers findJobReqNumbersObj(DistrictRequisitionNumbers districtRequisitionNumbers)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		JobRequisitionNumbers jrn=null;
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1);
				if(list.size()>0){
					jrn=list.get(0);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return jrn;
	}
	
	
	public String updateRequisitionNumber(String requisitionNumberId){
		JobRequisitionNumbers jRNOb=null;
		String requisitionNumber=null;
			try{
				if(requisitionNumberId!=null && !requisitionNumberId.equals("")){
					jRNOb=findById(Integer.parseInt(requisitionNumberId),false,false);
					if(jRNOb!=null){
						requisitionNumber=jRNOb.getDistrictRequisitionNumbers().getRequisitionNumber();
						jRNOb.setStatus(1);
						updatePersistent(jRNOb);
					}
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		return requisitionNumber;
	}
	
	@Transactional(readOnly=false)
	public SchoolMaster findSchoolByJob(JobOrder jobOrder,DistrictRequisitionNumbers DRNObj)
	{
		SchoolMaster schoolMaster=null;
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("districtRequisitionNumbers",DRNObj);
			list = findByCriteria(criterion1,criterion2);
			if(list.size()>0){
				schoolMaster=list.get(0).getSchoolMaster();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolMaster;
	}
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findSchoolByJobsAndDRN(List<JobOrder> jobOrders,List<DistrictRequisitionNumbers> DRNObjs)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			if(jobOrders.size()>0 && DRNObjs.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobOrder",jobOrders);
				Criterion criterion2 = Restrictions.in("districtRequisitionNumbers",DRNObjs);
				list = findByCriteria(criterion1,criterion2);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findRequisitionsByJob(JobOrder jobOrder)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			list = findByCriteria(criterion1);
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobReqNumbers(DistrictRequisitionNumbers districtRequisitionNumbers)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> getJobRequisitionList(boolean hourlyCategory,DistrictRequisitionNumbers districtRequisitionNumbers,JobOrder jobOrder)
	{
		List<JobRequisitionNumbers> lstJobRequisitionNumbers= new ArrayList<JobRequisitionNumbers>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(hourlyCategory==false){
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				criteria.add(criterion1);
			}else{
				Criterion criterion1 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				criteria.add(criterion1);
				criteria.createCriteria("jobOrder").add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster())).add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}
			lstJobRequisitionNumbers = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobRequisitionNumbers;
	}
	@Transactional(readOnly=false)
	public SchoolMaster findSchoolByJobReqId(JobOrder jobOrder,String requisitionNumberId)
	{
		SchoolMaster schoolMaster=null;
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("jobRequisitionId",Integer.parseInt(requisitionNumberId));
			list = findByCriteria(criterion1,criterion2);
			if(list.size()>0){
				schoolMaster=list.get(0).getSchoolMaster();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return schoolMaster;
	}
	@Transactional(readOnly=false)
	public DistrictRequisitionNumbers findDistrictReqIdByJobReqId(JobOrder jobOrder,String requisitionNumberId)
	{
		DistrictRequisitionNumbers districtRequisitionNumbers=null;
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
			Criterion criterion2 = Restrictions.eq("jobRequisitionId",Integer.parseInt(requisitionNumberId));
			list = findByCriteria(criterion1,criterion2);
			if(list.size()>0){
				districtRequisitionNumbers=list.get(0).getDistrictRequisitionNumbers();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return districtRequisitionNumbers;
	}
	@Transactional(readOnly=false)
	public JobRequisitionNumbers findJobReqNumbersObjByJob(JobOrder jobOrder,DistrictRequisitionNumbers districtRequisitionNumbers)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		JobRequisitionNumbers jrn=null;
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.eq("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1,criterion2);
				if(list.size()>0){
					jrn=list.get(0);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return jrn;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersBySchoolInJobOrder(JobOrder jobOrder,List<SchoolMaster> schoolMasters)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			if(schoolMasters.size()>0){
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				Criterion criterion2 = Restrictions.in("schoolMaster",schoolMasters);
				list = findByCriteria(criterion1,criterion2);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}	

	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findReqsObjInJRN(List<DistrictRequisitionNumbers> DRNObjs)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			
			Criterion criterion1 = Restrictions.in("districtRequisitionNumbers",DRNObjs);
			list = findByCriteria(criterion1);
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersByJobAndDistrict(JobOrder jobOrder,DistrictMaster districtMaster,String requisitionNumber)
	{
		List<JobRequisitionNumbers> listNumbers=new ArrayList<JobRequisitionNumbers>();
		try{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
				criteria.add(criterion1);
				criteria.createCriteria("districtRequisitionNumbers").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.eq("requisitionNumber",requisitionNumber)).addOrder(Order.desc("districtRequisitionId"));
				listNumbers =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return listNumbers;
	}	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequisitionNumbersByDistrictOrJob(JobOrder jobOrder,DistrictMaster districtMaster,String requisitionNumber)
	{
		List<JobRequisitionNumbers> listNumbers=new ArrayList<JobRequisitionNumbers>();
		try{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(jobOrder!=null){
					Criterion criterion1 = Restrictions.eq("jobOrder",jobOrder);
					criteria.add(criterion1);
				}
				if(districtMaster!=null){
					criteria.createCriteria("districtRequisitionNumbers").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.eq("requisitionNumber",requisitionNumber)).addOrder(Order.desc("districtRequisitionId"));
				}else{
					criteria.createCriteria("districtRequisitionNumbers").add(Restrictions.eq("requisitionNumber",requisitionNumber)).addOrder(Order.desc("districtRequisitionId"));
				}
				listNumbers =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return listNumbers;
	}	
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobRequsition(List<DistrictRequisitionNumbers> DRNObjs)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			Criterion criterion2 = Restrictions.in("districtRequisitionNumbers",DRNObjs);
			list = findByCriteria(criterion2);
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> getHRStatusByJobId(List<JobOrder> jobOrders)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			//SessionFactory sessionFactory = teacherAnswerHistoryDAO.getSessionFactory();
			//StatelessSession statelessSession=sessionFactory.openStatelessSession();
			//Transaction transaction=statelessSession.beginTransaction();
			Criterion criterion2 = Restrictions.in("jobOrder",jobOrders);
			list=getSession().createCriteria(getPersistentClass()).add(criterion2).list();
			
			//list = findByCriteria(criterion2);
		}catch (Exception e) {
			
			e.printStackTrace();
		}	
		
		return list;
	}
	
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobReqNumbersForNC(List<DistrictRequisitionNumbers> districtRequisitionNumbers)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criterion1 = Restrictions.in("districtRequisitionNumbers",districtRequisitionNumbers);
				list = findByCriteria(criterion1);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public List<JobRequisitionNumbers> findJobReqNumbersByJobIdAndDRNumber(List<DistrictRequisitionNumbers> districtRequisitionNumbers, JobOrder jobOrder)
	{
		List<JobRequisitionNumbers> list=new ArrayList<JobRequisitionNumbers>();
		try{
			
			if(districtRequisitionNumbers!=null){
				Criterion criDRN = Restrictions.in("districtRequisitionNumbers",districtRequisitionNumbers);
				Criterion criJOBID = Restrictions.eq("jobOrder",jobOrder);
				list = findByCriteria(criDRN,criJOBID);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return list;
	}
	@Transactional(readOnly=false)
	public List<Object[]> getJobIdAndRequisitionNo(List<JobOrder> listjobOrder)
	{
		long l=new Date().getTime();
		List<Object[]> list=new ArrayList<Object[]>();
		try
		{
			/*if(listjobOrder!=null && !listjobOrder.isEmpty())
			{
				for(JobOrder jo:listjobOrder)
				{
					System.out.println(jo.getJobId()+" ");
				}
			}*/
			Criteria criteria=getSession().createCriteria(getPersistentClass(),"jrn");
			if(listjobOrder!=null && !listjobOrder.isEmpty())
				criteria.add(Restrictions.in("jrn.jobOrder", listjobOrder));
			list=criteria.createAlias("districtRequisitionNumbers", "drn")
			.setProjection(Projections.projectionList()
					.add(Projections.property("jrn.jobOrder.jobId"))
					.add(Projections.property("drn.requisitionNumber"))
					)
					//.setFirstResult(0)
					//.setMaxResults(2)
			.list();
			/*for(Object[] object:list)
			{
				System.out.println(object[0] instanceof Integer );
				System.out.println(object[0] instanceof String );
				System.out.println(object[0] instanceof Object );
				System.out.println("aaaaaaaaaaaaaaaaa");
			}*/
			
		}catch (Exception e) {
			
			e.printStackTrace();
		}	
		System.out.println("Fetch time getJobIdAndRequisitionNo"+(new Date().getTime()-l)/1000);
		return list;
		
	}



}
