package tm.services;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import tm.bean.BatchJobOrder;
import tm.bean.EventDetails;
import tm.bean.EventSchedule;
import tm.bean.JobAlerts;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MailToCandidateOnImport;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherPersonalInfo;
import tm.bean.districtassessment.ExaminationCodeDetails;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DemoClassAttendees;
import tm.bean.master.DemoClassSchedule;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SchoolMaster;
import tm.bean.master.TeacherAnswerDetailsForDistrictSpecificQuestions;
import tm.bean.user.UserMaster;
import tm.utility.Utility;
 
public class MailText 
{	
	public static String mailCssFontSize = "'font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'";
	public static String mailFooterCssFontSize = "'font-family: Century Gothic,Open Sans, sans-serif;font-size: 12px;'";
	public static String mailContactNo = "(855) 980-0511";
	public static String locale = "en";
		
	static
	{
		try
		{
			locale = Utility.getValueOfPropByKey("locale");
			System.out.println("Local:- "+locale);
		}
		catch(Exception e){e.printStackTrace();}
	}
	
	public static String getTeacherName(TeacherDetail teacherDetail){
		String teacherName="";
		try{
			if(teacherDetail.getFirstName()!=null){
				teacherName=teacherDetail.getFirstName();
			}
			if(teacherDetail.getFirstName()!=null && teacherDetail.getLastName()!=null){
				teacherName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
			}
		}catch(Exception e){	e.printStackTrace(); }
		return teacherName;
	}
	
	public static String getUserName(UserMaster userMaster){
		String userName="";
		try{
			if(userMaster.getFirstName()!=null){
				userName=userMaster.getFirstName();
			}
			if(userMaster.getFirstName()!=null && userMaster.getLastName()!=null){
				userName=userMaster.getFirstName()+" "+userMaster.getLastName();
			}
		}catch(Exception e){	e.printStackTrace(); }
		return userName;
	}
	public static String getRegistrationMailForQuest(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getRegistrationMailForQuestFR(request,teacherDetail);
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId()+"&quest=true";
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+": Thank you for registering with TeacherMatch<sup>TM</sup>, your first step toward finding an ideal position.  To complete the registration process, you need to confirm your email address by clicking on the link below.  Once you have done this, you will be able to log on through the Login Page using the information you provided during the sign up process.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Click here to verify your email: <Unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your login email address is: <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is the one you chose during the sign up process.   If you forgot your password, simply click the \"I forgot\" link on the Login Page.  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Partner Success Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String getRegistrationMailForQuestUser(HttpServletRequest request,UserMaster usermaster)
	{
		if(locale.equals("fr"))
			return getRegistrationMailForQuestUserFR(request,usermaster);
			
		HttpSession session		=	request.getSession(false);
		UserMaster userSession	=	(UserMaster) session.getAttribute("userMaster");
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(usermaster)+":  You have been added as a user on the TeacherMatch<sup>TM</sup> platform.");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Your Login Email is: <Email Address><a href='mailto:"+usermaster.getEmailAddress()+"'>"+usermaster.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is the one you chose during the sign up process.   If you forgot your password, simply click the \"I forgot\" link on the Login Page.  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	
	public static String getRegistrationMailForNonClientDistrict(HttpServletRequest request,UserMaster usermaster)
	{
		if(locale.equals("fr"))
			return getRegistrationMailForNonClientDistrictFR(request,usermaster);
			
		HttpSession session		=	request.getSession(false);
		UserMaster userSession	=	(UserMaster) session.getAttribute("userMaster");
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(usermaster)+":<br/><br/>Welcome to TeacherMatch Quest! You have been added as a user on the TeacherMatch<sup>TM</sup> platform. The next step is for our team to verify your account. If you used your district email address to create your account this process will be expedited. If you did not use your district email address one must be provided to activate your account.");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Your current Login Email is: <Email Address><a href='mailto:"+usermaster.getEmailAddress()+"'>"+usermaster.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Your password is the one you chose during the sign up process.   If you forgot your password, simply click the \"I forgot\" link on the Login Page.  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Partner Success Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	
	public static String getRegistrationNotificationMailForQuestUser(String districtName, String firstName, String lastName, String emailAaddress, String password,int check)
	{
		if(locale.equals("fr"))
			return getRegistrationNotificationMailForQuestUserFR(districtName,firstName,lastName,emailAaddress,password,check);
			
		System.out.println("districtName "+districtName+" firstName "+firstName+" lastName "+lastName+" emailAaddress "+emailAaddress+" password "+password);
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(check==0)
			{
				sb.append("Client Services Team,<br/><br/>");
			}			
			else if(check==1)
			{
				sb.append("Partner Success team,<br/><br/>");
			}
			sb.append(" You are receiving this email because <b>"+firstName+" "+lastName+"</b> signed up as non-client district. Details of the District is as following:  ");			
			sb.append("</td>");
			sb.append("</tr><br/>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>District Name: <b>"+districtName+"</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>First Name: <b>"+firstName+"</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Last Name: <b>"+lastName+"</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Email: <b><Email Address><a href='mailto:"+emailAaddress+"'>"+emailAaddress+"</a></b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Password: <b>"+password+"</b>");
			sb.append("</td>");
			sb.append("</tr>");		
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>TMAdmin<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	
	/**
	 * for mail to client success team to verified district to signup non-client district
	 * @param request
	 * @param usermaster
	 * @param url
	 * @return
	 */
	public static String getRegistrationForClientServices(HttpServletRequest request,UserMaster usermaster)
	{
		if(locale.equals("fr"))
			return getRegistrationForClientServicesFR(request,usermaster);
			
		HttpSession session		=	request.getSession(false);
		UserMaster userSession	=	(UserMaster) session.getAttribute("userMaster");
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+"font-weight: bold;>");
			sb.append("Client Services Team,<br/><br/>");
			sb.append(" You are receiving this email because "+usermaster.getFirstName()+" "+usermaster.getLastName()+" signed up as non-client district. Details of the District is as following:  ");
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+"font-weight: bold;>");
			sb.append("Name		:	<b>"+usermaster.getFirstName()+" "+usermaster.getLastName()+"</b></td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+"font-weight: bold;>");
			sb.append("Email	:	<b>"+usermaster.getEmailAddress()+"</b></td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+"font-weight: bold;>");		
			sb.append("District Name	:	<b>"+usermaster.getDistrictId().getDistrictName()+"</b></td>");		
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please contact your Admin at <a href='mailto:lientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
	
		return sb.toString();
	}
	
	
	public static String getRegistrationMailForTempQuest(HttpServletRequest request,TeacherDetail teacherDetail,String pass)
	{
		if(locale.equals("fr"))
			return getRegistrationMailForTempQuestFR(request,teacherDetail,pass);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId()+"&quest=true";
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+": Thank you for registering with TeacherMatch<sup>TM</sup>, your first step toward finding an ideal position.  To complete the registration process, you need to confirm your email address by clicking on the link below.  Once you have done this, you will be able to log on through the Login Page using the information you provided during the sign up process.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Click here to verify your email: <Unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is: <b>"+pass+"</b> <br/>  If you forgot your password, simply click the \"I forgot\" link on the Login Page.  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Partner Success team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String getRegistrationMail(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getRegistrationMailFR(request,teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			String baseUrl=Utility.getBaseURL(request);
			if(baseUrl.contains("nccloud") || baseUrl.contains("nc.teachermatch"))
				sb.append("Dear "+getTeacherName(teacherDetail)+":<br> <br>	Welcome! You have now completed the registration process for TeacherMatchTM.&nbsp;&nbsp;  At TeacherMatch, we take pride in helping candidates find their best-fit roles.&nbsp;&nbsp;  We also believe that great educators need actionable support, which is why we offer a customized professional development report, available after you have completed your Portfolio and the Base Inventory.<br/><br/>");
			else
				sb.append("Dear "+getTeacherName(teacherDetail)+": Thank you for registering with TeacherMatch<sup>TM</sup>, your first step toward finding an ideal position.  To complete the registration process, you need to confirm your email address by clicking on the link below.  Once you have done this, you will be able to log on through the Login Page using the information you provided during the sign up process.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Click here to verify your email: <Unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
			if(baseUrl.contains("nccloud") || baseUrl.contains("nc.teachermatch"))
				sb.append("<br><br>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(baseUrl.contains("nccloud") || baseUrl.contains("nc.teachermatch"))
				sb.append("Your password is the one you chose during the sign up process.&nbsp;&nbsp;  If you forgot your password, simply click the \"I forgot\" link on the Login Page.&nbsp;&nbsp;  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			else
				sb.append("Your password is the one you chose during the sign up process.   If you forgot your password, simply click the \"I forgot\" link on the Login Page.  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}	
		
		return sb.toString();
	}
	
	
	public static String getRegistrationMailForInternalCandidate(HttpServletRequest request,TeacherDetail teacherDetail,DistrictMaster districtMaster,String rPassword,int mailFlag)
	{
		if(locale.equals("fr"))
			return getRegistrationMailForInternalCandidateFR(request,teacherDetail,districtMaster,rPassword,mailFlag);
			
		StringBuffer sb = new StringBuffer();
		
		String districtDName="";
		String dmName="";
		String dmPhoneNumber="";
		if(districtMaster!=null){
			if(districtMaster.getDisplayName()!=null && !districtMaster.getDisplayName().equals("")){
				districtDName=districtMaster.getDisplayName();
			}else{
				districtDName=districtMaster.getDistrictName();
			}
			if(districtMaster.getDmName()!=null){
				dmName=districtMaster.getDmName();
				
				if(districtMaster.getDistrictId().equals(1201470)){
					dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
				}
			}
			if(districtMaster.getDmPhoneNumber()!=null){
				dmPhoneNumber=districtMaster.getDmPhoneNumber();
			}
		}
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId()+"&iId=543210";
			sb.append("<table>");
			
			if(mailFlag==0){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getTeacherName(teacherDetail)+": Thank you for registering for internal transfer opportunities at  "+districtDName+"! To complete the registration process, you need to confirm your email address by clicking on the link below. Once you have done this, we will send you job opportunities that match the interests you specified during the internal transfer opportunities registration process. Don't worry, if you don't wish to receive these notifications, you can always opt out of internal transfer opportunities notification process by logging into your TeacherMatch account and opting out of \"Internal Transfer\".<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Click here to verify your email: <a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("You can log into TeacherMatch <a target='blank' href='"+Utility.getBaseURL(request)+"' >here</a><br/>User Id:  <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
				sb.append("<br/>Password: "+rPassword+"<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("You can also change your password to your preference after logging into TeacherMatch.<br/>If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+"<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getTeacherName(teacherDetail)+": Thank you for registering for internal transfer opportunities! To complete the registration process, you need to confirm your email address by clicking on the link below. Once you have done this, you will be able to receive any matching job opportunities.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Click here to verify your email: <a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely<br/>");
			sb.append(dmName+"<br/>");
			if(!dmPhoneNumber.equals("")){
				sb.append(dmPhoneNumber+"<br/>");
			}
			sb.append(districtDName+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::1 "+sb.toString());
		return sb.toString();
	}
	
	
	public static String getRegConfirmationEmailToTeacher(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getRegConfirmationEmailToTeacherFR(request,teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			String loginUrl = Utility.getBaseURL(request)+"signin.do";
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+": Welcome. You have now completed the registration process for TeacherMatch<sup>TM</sup>. At TeacherMatch, we take pride in helping candidates find their best-fit roles. We also believe that great educators need actionable support, which is why we offer a customized professional development report, available after you have completed your Portfolio and the Base Inventory.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address>"+teacherDetail.getEmailAddress()+"");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is the one you chose during the sign up process. If you forgot your password, simply click the \"I forgot\" link on the Login Page. If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please click on <a href='"+loginUrl+"'>"+loginUrl+"</a> to build your Portfolio and to take the inventories appropriate for positions that interest you.");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static String getFgPwdMailToTeacher(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getFgPwdMailToTeacherFR(request,teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"resetpassword.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+":<br/> You are receiving this email because you clicked the \"I forgot\" link next to the password field on the TeacherMatch Login Page.  If you did not click this link, please notify us immediately at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> as this may be a sign that someone else is trying to gain access to your account.   To change your password, click the link below.  The link will only be active for 72 hours.   If you do not make the change within 72 hours, you will need to click the \"I forgot\" link again.<br/><br/> ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Change your password by clicking here: <unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			/*sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");*/


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely<br>TeacherMatch Client Services Team  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	/* @Author: Gagan */
	public static String getFgPwdMailToOtUser(HttpServletRequest request,UserMaster userMaster,String[] arrHrDetail)
	{
		if(locale.equals("fr"))
			return getFgPwdMailToOtUserFR(request,userMaster,arrHrDetail);
			
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1201470)){
			dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
		}
		
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(userMaster)+": <br/>You are receiving this email because you clicked the \"I forgot\" link next to the password field on the TeacherMatch Login Page.  If you did not click this link, please notify us immediately at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> as this may be a sign that someone else is trying to gain access to your account.   To change your password, click the link below.  The link will only be active for 72 hours.   If you do not make the change within 72 hours, you will need to click the \"I forgot\" link again.<br/><br/> ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Change your password by clicking here: <unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			/*sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");*/
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			
			
			if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1200390)){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely<br>TeacherMatch Client Services Team  <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			} else {
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				//sb.append("Client Services Team   <br/><br/>");
				if(isHrContactExist==0)
					sb.append("Sincerely<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
				else 
					if(isHrContactExist==1)
					{
						sb.append("Sincerely<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
						if(title!="")
						sb.append(title+"<br/>");
						if(phoneNumber!="")
							sb.append(phoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
					else
					{
						if(isHrContactExist==2)
						{
							sb.append("Sincerely<br/>"+dmName+"<br/>");
							if(dmPhoneNumber!="")
								sb.append(dmPhoneNumber+"<br/>");
							sb.append(schoolDistrictName+"<br/><br/>");
						}
					}
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			System.out.println("sb:::2"+sb.toString());

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	/* @Author: Gagan */
	public static String createUserPwdMailToOtUser(HttpServletRequest request,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return createUserPwdMailToOtUserFR(request,userMaster);
		String emailid ="";
		StringBuffer sb = new StringBuffer();
		try 
		{
			HttpSession session			=	request.getSession(false);
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			emailid = userSession.getEmailAddress();
			String verificationUrl		=	Utility.getBaseURL(request)+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
			String loginUrl		=	Utility.getBaseURL(request)+"signin.do";
		    if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==806900 && ( userMaster.getRoleId().getRoleId()==2 || userMaster.getRoleId().getRoleId()==3 || userMaster.getRoleId().getRoleId()==7) ){
		    	sb.append("<table>");
		    	sb.append("<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>Dear "+userMaster.getFirstName()+",</td></tr>");
		    	sb.append("<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>Welcome to the TeacherMatch system! You've been added as a new TeacherMatch user. Please use your universal email address and universal to password to access the system.</td></tr>");
		    	sb.append("<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>Click <a href='"+loginUrl+"'>here</a> to log in.</td></tr>");
		    	sb.append("<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>Sincerely,");
		    	sb.append("The Human Resources Team<br/>");
		    	sb.append("Adams 12 Five Star Schools");
		    	sb.append("</td></tr></table>");
		    	
		    }else{
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(userMaster)+":  You have been added as a user on the TeacherMatch<sup>TM</sup> platform.   Your Login Email is <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>. Your Authentication PIN is "+userMaster.getAuthenticationCode()+". In order to activate your account, please visit the link below to set your password: ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please contact me at "+emailid+" if you have questions.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please contact your Admin at "+emailid+".");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
		   }

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" createUserPwdMailToOtUserb :: "+sb.toString());
		
		return sb.toString();
	}
	/* @Author:Sekhar */
	public static String messageForDefaultFont(String messageText,UserMaster userSession)
	{
		if(locale.equals("fr"))
			return messageForDefaultFontFR(messageText,userSession);
			
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(messageText);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			
			boolean footerFlag = true;		
			if(userSession.getDistrictId()!=null && userSession.getDistrictId().getDistrictEmailFlag().equals(true))
			{
				footerFlag = false;
			}
			
			if(!messageText.contains("Please do not reply to this email.") && footerFlag)
			if(userSession!=null && userSession.getDistrictId()!=null && userSession.getDistrictId().getDistrictId()!=null && userSession.getDistrictId().getDistrictId()==4218990 && footerFlag)
				sb.append("Please do not reply to this e-mail. If you need assistance, contact <a href='mailto:recruitment@philasd.org'>recruitment@philasd.org</a>.");
			else
			if(userSession!=null && userSession.getDistrictId()!=null && userSession.getDistrictId().getDistrictId()!=null && userSession.getDistrictId().getDistrictId().equals(804800) && footerFlag)
				sb.append("Please do not reply to this email.");
			else if(footerFlag)
				sb.append("Please do not reply to this email. If you need to respond, please send your reply to <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	/* @Author:Sekhar */
	public static String messageForFinalDecisionMaker(String FinalDecisionMakerName,String dmPassword,UserMaster userSession)
	{
		if(locale.equals("fr"))
			return messageForFinalDecisionMakerFR(FinalDecisionMakerName,dmPassword,userSession);
			
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+FinalDecisionMakerName+",");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("</br>This email is being sent to inform you that your TeacherMatch account administrator has designated you as the Final Decision Maker for the account.  This means that will need to authorize any major changes to the account.   Please store the password that is included in this email in a secure location as you will need it to verify your identity to our help desk.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("</br>If you think this has been a mistake, please reach out to your TeacherMatch account administrator at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> otherwise, you don't need to take any action.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Your password is: "+dmPassword);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely,</br>TeacherMatch Client Service Team");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static String createJobOrderForDistrict(int noOfSchoolExpHires,UserMaster userSession,String baseURL,JobOrder jobOrder,String jcRecords,String supportOrUserName)
	{
		if(locale.equals("fr"))
			return createJobOrderForDistrictFR(noOfSchoolExpHires,userSession,baseURL,jobOrder,jcRecords,supportOrUserName);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			String noOfExpHires="NA";
			if(jobOrder.getNoOfExpHires()!=null && jobOrder.getNoOfExpHires()!=0){
				noOfExpHires=jobOrder.getNoOfExpHires()+"";
			}else{
				if(noOfSchoolExpHires!=0)
					noOfExpHires=noOfSchoolExpHires+"";
			}

			String JcRecords="NA";

			if(jcRecords!=null && !jcRecords.equals("")){
				JcRecords=jcRecords;
			}

			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+supportOrUserName+",</br></br>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("You are receiving this email because I have added you to a Job Order called "+jobOrder.getJobTitle()+" with the following characteristics:</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
		
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul>");
			sb.append("<li>");
			sb.append("Certification Name(s): "+JcRecords);
			sb.append("</li>");
			sb.append("<li>");
			sb.append("Number of Hires Allocated to You: "+noOfExpHires);
			sb.append("</li>");
			sb.append("<li>");
			sb.append("URL of Job Posting: <a target='blank' href='"+baseURL+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"' >"+baseURL+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"</a>");
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>You should now see this Job Order in the list located under the Job Orders menu on the TeacherMatch<sup>TM</sup> platform. You can also choose to view the Candidate Grid for each job order from the Job Orders menu.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Please contact me at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> if you have questions or would like to request a change.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Best wishes,</br>"+getUserName(userSession));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			//System.out.println("sb="+sb);

		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(" createJobOrderForDistrict :: 3"+sb.toString());
		
		return sb.toString();
	}
	/* @Author:Sekhar */
	public static String jobOrderSupport(int jobOrderType,String nameOfEntity,String Schools,String aUploadUrl,UserMaster userSession,String baseURL,JobOrder jobOrder)
	{
		if(locale.equals("fr"))
			return jobOrderSupportFR(jobOrderType,nameOfEntity,Schools,aUploadUrl,userSession,baseURL,jobOrder);
			
		StringBuffer sb = new StringBuffer();
		try 
		{

			int noOfExpHires=0;
			if(jobOrder.getNoOfExpHires()!=null && jobOrder.getNoOfExpHires()!=0){
				noOfExpHires=jobOrder.getNoOfExpHires();
			}
			String urlFileName="NA";
			String entityAdmin="";
			String jobJSIAttach="NA";
			int jobJSI=0;

			if(jobOrder.getIsJobAssessment()){
				jobJSI=1;
			}
			if(jobJSI==1){
				if(jobOrder.getAttachNewPillar()==1){
					if(jobOrderType==2){
						jobJSIAttach="district job order";
					}else{
						jobJSIAttach="school job order";
					}
				}else if(jobOrder.getAttachDefaultDistrictPillar()==1){
					jobJSIAttach="district default";
				}else if(jobOrder.getAttachDefaultSchoolPillar()==1){
					jobJSIAttach="school default";
				}
			}

			if(nameOfEntity==null){
				nameOfEntity="NA";
			}
			if(Schools==null){
				Schools="NA";
			}
			if(aUploadUrl==null){
				aUploadUrl="NA";
			}else{
				urlFileName=aUploadUrl.substring(aUploadUrl.indexOf("file=")+5);
			}

			if(userSession.getEntityType()==1){
				entityAdmin="TM Admin";
			}else if(userSession.getEntityType()==2){
				entityAdmin="District Admin";
			}else if(userSession.getEntityType()==3){
				entityAdmin="School Admin";
			}

			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear Support: You need to create a Job Specific Inventory (JSI).  Details:");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>-Type of JSI:  "+jobJSIAttach);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-Name of Entity: "+nameOfEntity);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-"+getUserName(userSession));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-"+userSession.getEmailAddress());
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-"+entityAdmin);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-School(s): "+Schools);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-Job Order Number: <a target='blank' href='"+baseURL+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"' >"+jobOrder.getJobId()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-"+jobOrder.getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-<a target='blank' href='"+aUploadUrl+"' >"+urlFileName+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");	
			//	System.out.println("sb>>>>>>>>"+sb);
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static String createBatchJobOrderForSchool(int noOfExpHires,UserMaster userSession,String baseURL,BatchJobOrder batchJobOrder,String jcRecords,String supportOrUserName)
	{
		if(locale.equals("fr"))
			return createBatchJobOrderForSchoolFR(noOfExpHires,userSession,baseURL,batchJobOrder,jcRecords,supportOrUserName);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			noOfExpHires=noOfExpHires;
			String JcRecords="NA";
			if(jcRecords!=null && !jcRecords.equals("")){
				JcRecords=jcRecords;
			}
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+supportOrUserName+": You are receiving this email because I have created a Job Order for you called "+batchJobOrder.getJobTitle()+" with  the following characteristics:");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-	Certification Name(s):  "+JcRecords);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-	Number of Hires Allocated to You: "+noOfExpHires);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-	URL of Job Posting: <a target='blank' href='"+baseURL+"applyteacherjob.do?batchJobId="+batchJobOrder.getBatchJobId()+"' >"+baseURL+"applyteacherjob.do?batchJobId="+batchJobOrder.getBatchJobId()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>You must accept this Job Order before it becomes active and viewable to candidates. To do so, click on Batch Job Orders under your Job Orders menu, find this Job Order in the table, then click  Accept. Upon acceptance, you own the job order and can edit it as you see fit. You have full control and ownership of this Job Order and can adjust all of its characteristics. I cannot make any changes since ownership has been given to you.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Please contact me at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> if you have questions.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Best wishes,</br>"+getUserName(userSession));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			//System.out.println("sb=order=="+sb);	
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static String newPwdMailToUser(HttpServletRequest request,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return newPwdMailToUserFR(request,userMaster);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			HttpSession session			=	request.getSession(false);
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			String verificationUrl		=	Utility.getBaseURL(request)+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(userMaster)+":  I have been sent a request for resetting the password on the TeacherMatch<sup>TM</sup> platform. Your Login Email is <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>. Please visit the link below to set your password:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please contact me at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>  if you have questions.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best wishes,<br/>"+getUserName(userSession)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please contact your Admin at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static String getSettingEmailChangeMailText(HttpServletRequest request,TeacherDetail teacherDetail, String oldEmailAdder)
	{
		if(locale.equals("fr"))
			return getSettingEmailChangeMailTextFR(request,teacherDetail,oldEmailAdder);
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+":  You are receiving this email because you recently edited your TeacherMatch<sup>TM</sup> settings.  You made the following change(s):<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your email was changed from <Original Email>"+oldEmailAdder+" to <Edited Email><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+". <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println("   4 "+sb.toString());
		
		return sb.toString();

	}
	public static String getSettingPWDChangeMailText(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getSettingPWDChangeMailTextFR(request,teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+":  You are receiving this email because you recently edited your TeacherMatch<sup>TM</sup> settings.  You made the following change(s):<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password was changed <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+". <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 5"+sb.toString());
		
		return sb.toString();

	}


	public static String getUserSettingEmailChangeMailText(HttpServletRequest request,UserMaster userMaster, String oldEmailAdder)
	{
		if(locale.equals("fr"))
			return getUserSettingEmailChangeMailTextFR(request,userMaster,oldEmailAdder);

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(userMaster)+":  You are receiving this email because you recently edited your TeacherMatch<sup>TM</sup> settings.  You made the following change(s):<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your email was changed from <Original Email>"+oldEmailAdder+" to <Edited Email><a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+". <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 6"+sb.toString());
		
		return sb.toString();

	}
	public static String getUserSettingPWDChangeMailText(HttpServletRequest request,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return getUserSettingPWDChangeMailTextFR(request,userMaster);
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(userMaster)+":  You are receiving this email because you recently edited your TeacherMatch<sup>TM</sup> settings.  You made the following change(s):<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("	Your password was changed    <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+". <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");


		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 7"+sb.toString());
		
		return sb.toString();

	}
	public static String getUserSettingPWDChangeMailTextFR(HttpServletRequest request,UserMaster userMaster)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+":  Vous recevez ce courriel car vous avez r�cemment modifi� vos param�tres de TeacherMatchTM.  Vous avez fait la modification suivante:<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("	Votre mot de passe a �t� chang�. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous vous remercions d'utiliser TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L��quipe du service � la client�le<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+". <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");


		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 7"+sb.toString());
		
		return sb.toString();
	}
	

	public static String getTeacherPDReportMailText(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getTeacherPDReportMailTextFR(request,teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear TeacherMatch Client Services Team Member,<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("One of TeacherMatch members has expressed interest in purchasing the Professional Development (PD) report and their details are presented below:    <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date of Request: "+new Date());
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Member Name: "+getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Member Email: <a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please contact me at <a href='mailto:admin@teachermatch.com'>admin@teachermatch.com</a> if you have questions about this email.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");



		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 8"+sb.toString());
		
		return sb.toString();

	}
	public static String getTeacherMailText(HttpServletRequest request,TeacherDetail teacherDetail,String defaultMessage)
	{
		if(locale.equals("fr"))
			return getTeacherMailTextFR(request,teacherDetail,defaultMessage);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(defaultMessage);
			sb.append("</td>");
			sb.append("</tr>");			


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 9"+sb.toString());
		
		return sb.toString();

	}
	public static String getTeacherEPIResetMailText(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getTeacherEPIResetMailTextFR(request,teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your EPI (Base Inventory) status has been re-set from \"timed out\" to \"incomplete\". You can now resume your EPI (Base Inventory) by logging on to the TeacherMatch platform.");
			sb.append("<br><br>Before you resume the Base Inventory, please note the following guidelines. These guidelines have been implemented to ensure that all candidates take the Base Inventory under equitable conditions.");
			sb.append("<br><br>Each item in the Base Inventory has a stipulated time limit and you must respond to each question within its stipulated time limit.");
			sb.append("<br>You must answer all of the questions in one sitting. ");
			sb.append("<br>Make sure that you have at least 90 minutes of uninterrupted time to complete the Base Inventory. ");
			sb.append("<br>Make sure that you have a stable and reliable Internet connection. ");
			sb.append("<br>Do not close your browser or hit the \"back\" button on your browser.");
			sb.append("<br>You are not able to skip questions.");
			sb.append("<br>If you do not follow these guidelines, you will receive two additional warnings before your status will become timed out again. If you have any questions about these guidelines or any aspect of the Base Inventory administration process, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	public static String getTeacherFirstMailText(TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getTeacherFirstMailTextFR(teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for registering on the TeacherMatch website and taking the opportunity to be part of our ground breaking research! �We appreciate your time and consideration.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("We noticed that you have not authenticated your account and we will strongly recommend you to take that step. By authenticating yourself, you will be joining scores of elite educators from all over the country who have complete their EPI and have becoming leading candidates for job opportunities across the board. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Since we host the jobs on our site as well, you will also have access to information about many job openings as well as the ability to apply to many positions in one place" +
					". For more information about all of the exciting opportunities at TeacherMatch, please visit <a href='http://www.teachermatch.org'>www.teachermatch.org</a>.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			String url = Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please follow the link in the email you received to verify your email address and log in at <a href='"+url+"'>"+url+"</a>" +
					". Can't find the authentication email..no worries! Just visit this link <> , enter your email and we will send you a fresh link for authentication.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If your profile is not completed within 30 days, your account will be deactivated. If you have any questions or concerns about the registration process, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely,<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String getTeacherFirstMailTextEPIRequest(TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getTeacherFirstMailTextEPIRequestFR(teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");
			String url = Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for creating a profile on the TeacherMatch website and taking the opportunity to be part of our ground breaking research! We appreciate your time and consideration.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If you choose to complete the TeacherMatch Base Inventory, you will be part of a group of teachers from around the country who have taken part in our revolutionary study. You will also have access to information about many job openings as well as the ability to apply to many positions in one place. For more information about all of the exciting initiatives at TeacherMatch, please visit <a href='http://www.teachermatch.org'>http://www.teachermatch.org</a>. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please log in at <a href='"+url+"'>"+url+"</a> to complete the inventory and gain access to hundreds of job postings.�If you have any questions or concerns about the registration process, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely,<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	public static String getTeacherFirstMailTextEPICompleted(TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getTeacherFirstMailTextEPICompletedFR(teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");
			String url = Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for creating a profile and completing the Base Inventory on the TeacherMatch website and taking the opportunity to be part of our ground breaking research! �We appreciate your time and consideration.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Now that you have achieved full profile completion, you can apply for jobs seamlessly through the site.  We have over 1,000 job postings from districts and schools all over the country. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please log in at <a href='"+url+"'>"+url+"</a> to apply for jobs. If you have any questions or concerns about the registration process, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely,<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String getShareJobMailText(HttpServletRequest request,String teacherFname, String teacherEmail, JobOrder jobOrder, String name, String email)
	{
		if(locale.equals("fr"))
			return getShareJobMailTextFR(request,teacherFname, teacherEmail, jobOrder, name, email);


		StringBuffer sb 					= 	new StringBuffer();
		String jobUrl 						=	Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId();		
		//String teacherLname				=	teacherDetail.getLastName()==null?"":teacherDetail.getLastName();		
		String schoolDistrictName			=	null;
		String schoolOrDistrictLocation		=	null;
		List<SchoolMaster> schoolList		=	null;

		try 
		{
			if(jobOrder.getCreatedForEntity()==2)
			{
				schoolDistrictName			=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
				schoolOrDistrictLocation	=	jobOrder.getDistrictMaster().getAddress()==null?"":jobOrder.getDistrictMaster().getAddress();
			}
			if(jobOrder.getCreatedForEntity()==3)
			{
				schoolList					=	jobOrder.getSchool();
				for(SchoolMaster schoolDetails:schoolList)
				{
					schoolDistrictName		=	schoolDetails.getSchoolName()==null?"":schoolDetails.getSchoolName();
					schoolOrDistrictLocation=	schoolDetails.getAddress()==null?"":schoolDetails.getAddress();
				}
			}
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("I thought you may be interested in this opportunity: <JOB TITLE>"+jobOrder.getJobTitle()+" at <SCHOOL>"+schoolDistrictName+" in <LOCATION>"+schoolOrDistrictLocation+" listed at <a href='http://www.teachermatch.org/'>www.teachermatch.org</a>. Click on this link to learn more: <Unique URL to Job Posting><a href="+jobUrl+">"+jobUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<First Name of Person Sharing Job>"+teacherFname+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you have questions, please contact the individual who shared this job with you at <Email of Person Sharing Job><a href='mailto:"+teacherEmail+"'>"+teacherEmail+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}

	/*===================== Gagan : Mail to teacher after Job Apply =============================== */
	public static String mailtoTeacherAfterJobApply(HttpServletRequest request,TeacherDetail teacherDetail,JobOrder jobOrder,String[] arrHrDetail)
	{
		if(locale.equals("fr"))
			return mailtoTeacherAfterJobApplyFR(request,teacherDetail,jobOrder,arrHrDetail);
			
		StringBuffer sb 					= 	new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 		= arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		String loginUrl	=	arrHrDetail[10];
		String cgUrl	=	arrHrDetail[11];
		String jobboardUrl	=	arrHrDetail[12];
		String forgetPwdUrl	=	arrHrDetail[13];
		
		String jobUrl 						=	Utility.getValueOfPropByKey("basePath")+"applyteacherjob.do?jobId="+jobOrder.getJobId();		
		//String teacherLname				=	teacherDetail.getLastName()==null?"":teacherDetail.getLastName();		
		List<SchoolMaster> schoolList		=	null;
		try 
		{
			//System.out.println("\n\n\n---------- schoolDistrictName "+schoolDistrictName);
			
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(Utility.isNC())
			  sb.append("Dear "+getTeacherName(teacherDetail)+":<br/><br/>");
			  else				  
		    	sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for applying for a "+jobOrder.getJobTitle()+" position with "+schoolDistrictName+". We are glad that you are exploring employment opportunities with us. Please ensure that your email settings allow you to receive messages from this email address.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(Utility.isNC())
			   {
				sb.append("Your Login Email is: "+teacherDetail.getEmailAddress()+"<br/><br/>");
				sb.append("Your password is the one you chose during the sign up process. If you forgot your password, simply click the \"Forgot Password?\" link <a href="+forgetPwdUrl+">on the Login Page</a>. If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			   sb.append("</td>");
			   }
			   else 
			   {
			     sb.append("Your Login Email is: "+teacherDetail.getEmailAddress()+" and your password is the one you chose during the sign up process. If you forgot your password, simply click the \"I forgot\" link <a href="+forgetPwdUrl+">here</a>. If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login into TeacherMatch <a href="+loginUrl+">here</a>.<br/><br/>");
			   }			sb.append("</td>");
			sb.append("</tr>");
			
			/*sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login into TeacherMatch <a href="+loginUrl+">here</a>.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");*/
			
			DistrictMaster districtMaster=null;
			if(jobOrder.getDistrictMaster()!=null){
				districtMaster=jobOrder.getDistrictMaster();
			}
			if(districtMaster!=null && districtMaster.getDistrictId().equals(806810)){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If you have any TeacherMatch system-specific questions or if you are having any technical issues, please email us at "+
						"<a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a> or call 8559800511.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");				
			}else{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call (855) 980-0511.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");				
			}
			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sincerely<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sincerely<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(!Utility.isNC())
					{
					if(title!="")
					sb.append(title+"<br/>");
					}
					if(Utility.isNC())
						if(jobOrder.getDistrictMaster().getDmName()!=null)
					sb.append(jobOrder.getDistrictMaster().getDmName()+"<br/>");
					else{
						sb.append(" ");
					}
					if(Utility.isNC()){
						String b="";
					if(phoneNumber!="")
						
						for(int i=0;i<phoneNumber.length();i++){   
							if(Character.isDigit(phoneNumber.charAt(i))){
							b=b+phoneNumber.charAt(i);
							   } 
							  }
							String phoneNumber1 = b.substring(10);
							phoneNumber="("+b.substring(0, 3)+")" +" " +b.substring(3,6)+"-"+b.substring(6, 10);
							if(b.length()>10){
							   	phoneNumber=phoneNumber+ " ext."+" "+phoneNumber1;
							    }
							    else{
							    	phoneNumber=phoneNumber; 
							    }    
							
							sb.append(phoneNumber+"<br/>");
					}
					else 
					{
						if(dmPhoneNumber!="")
						sb.append(dmPhoneNumber+"<br/>");	
						}
						
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sincerely<br/>");//"+dmName+"<br/>
						if(Utility.isNC())
							if(jobOrder.getDistrictMaster().getDmName()!=null)
						sb.append(jobOrder.getDistrictMaster().getDmName()+"<br/>");
						else{
							sb.append(" ");
						}
						if(Utility.isNC()){
						String b="";
						if(dmPhoneNumber!="")
							
							 for(int i=0;i<dmPhoneNumber.length();i++){   
							    if(Character.isDigit(dmPhoneNumber.charAt(i))){
							    b=b+dmPhoneNumber.charAt(i);
								   } 
								  }
								String phoneNumber1 = b.substring(10);
								dmPhoneNumber="("+b.substring(0, 3)+")" +" " + b.substring(3,6)+"-"+b.substring(6, 10);
								 if(b.length()>10){
								     dmPhoneNumber=dmPhoneNumber+ " ext."+" "+phoneNumber1;
								    }
								    else{
								     dmPhoneNumber=dmPhoneNumber; 
								    }    
							sb.append(dmPhoneNumber+"<br/>");
						}
						else {
							if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						}
						
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");
			if(districtMaster!=null && districtMaster.getDistrictId().equals(806810)){
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append(schoolDistrictName+" is an equal opportunity education institution and does not unlawfully discriminate on the basis of race,"+
						" color, national origin, age, sex, sexual orientation or disability in admission or access to, or treatment,"+
						" or employment in, its education programs or activities and provides equal access to the Boy Scouts and"+
						" other designated youth groups. Inquiries concerning non-discrimination policies may be referred to the "+schoolDistrictName);//schoolDistrictName);
				
				sb.append(" Attn: Superintendent, P.O. Box 7, Frisco, CO 80443, (970) 368-1000.");
				sb.append("</td></tr>");				
			}
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			if(districtMaster!=null && districtMaster.getDistrictId().equals(806810)){
				sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a> or call "+mailContactNo+".");
			}else{
				sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".");
			}
			sb.append("</tr>");			

			sb.append("</table>");
			//System.out.println("\n\n  Sekhar- :::::::::::::::::::::::\n "+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	
	/*===================== Gagan : Mail to Users ( Active DA/SA who has checked thier checkbox in User Email Nptification ) after Job Apply =============================== */
	public static String mailtoUsersAfterJobApply(HttpServletRequest request,TeacherDetail teacherDetail,JobOrder jobOrder,String[] arrHrDetail,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return mailtoUsersAfterJobApplyFR(request, teacherDetail, jobOrder, arrHrDetail, userMaster);
			
		StringBuffer sb 					= 	new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 		= arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		String cgUrl		 = arrHrDetail[11];
		String jobUrl 						=	Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId();	
		String statusShortName	= arrHrDetail[14];
		
		List<SchoolMaster> schoolList		=	null;
		System.out.println("Inside mailtoUsersAfterJobApply :::::::");
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(userMaster)+",<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			String districtName="";
			
			if(jobOrder.getDistrictMaster()!=null){
				if(jobOrder.getDistrictMaster().getDisplayName()!=null && !jobOrder.getDistrictMaster().getDisplayName().equals("")){
					districtName=jobOrder.getDistrictMaster().getDisplayName();
				}else{
					districtName=jobOrder.getDistrictMaster().getDistrictName();
				}
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
		//	sb.append("Applicant "+getTeacherName(teacherDetail)+" recently applied for "+jobOrder.getJobTitle()+" position at "+districtName+". You can visit the Candidate Grid for this job at <a href='"+cgUrl+"'>"+cgUrl+"</a><br/><br/>");
		
		
			if(Utility.isNC())
				if(jobOrder.getSchool()!=null &&  jobOrder.getSchool().size()>0    )
				sb.append("Applicant, "+getTeacherName(teacherDetail)+","+" recently applied for "+jobOrder.getJobTitle()+", position #"+jobOrder.getJobId()+" at "+jobOrder.getSchool().get(0).getSchoolName()+". You can visit the Candidate Grid for this job at  <a href='"+cgUrl+"'>"+cgUrl+"</a><br/><br/>");
				else
					sb.append("Applicant, "+getTeacherName(teacherDetail)+","+" recently applied for "+jobOrder.getJobTitle()+", position #"+jobOrder.getJobId()+" at "+districtName+". You can visit the Candidate Grid for this job at  <a href='"+cgUrl+"'>"+cgUrl+"</a><br/><br/>");	
			else
				sb.append("Applicant, "+getTeacherName(teacherDetail)+","+" recently applied for "+jobOrder.getJobTitle()+", position #"+jobOrder.getJobId()+" at "+districtName+". You can visit the Candidate Grid for this job at  <a href='"+cgUrl+"'>"+cgUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			if(statusShortName!=null && !statusShortName.equals("")){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				
					if(statusShortName.equalsIgnoreCase("comp"))
						sb.append("Candidate EPI status is \"Completed\".<br/><br/>");
					else
						if(statusShortName.equalsIgnoreCase("vlt"))
						sb.append("Candidate EPI status is \"Timed Out\".<br/><br/>");
					else
						if(statusShortName.equalsIgnoreCase("icomp"))
						sb.append("Candidate has not started his/her EPI.<br/><br/>");
				
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			
			if(Utility.isNC())
			sb.append("NC School Jobs powered by TeacherMatch�<br/><br/>");
			else
				sb.append("Sincerely<br/>TeacherMatch Client Services Team<br/><br/>");	
				
			
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
		//	sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
			//System.out.println("\n\n  Gagan:\n>>>>>>>>>mailtoUsersAfterJobApply "+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	

	public static String getJobAlertText(JobAlerts jobAlerts, List<JobOrder> lstJobOrders, HttpServletRequest request)
	{
		if(locale.equals("fr"))
			return getJobAlertTextFR(jobAlerts, lstJobOrders, request);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			//String loginUrl = Utility.getBaseURL(request)+"signin.do";
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+jobAlerts.getSenderName()+",");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>You are receiving this email because you had expressed interest in receiving job opportunities from TeacherMatch.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Check out these jobs that may be of interest to you:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			String cityName="";
			String stateName="";
			String joburl = "";	
			for(JobOrder jobOrder: lstJobOrders){
				joburl = Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId();
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+" style='padding-top: 10px;'>");
				sb.append(""+jobOrder.getJobTitle());
				if(jobOrder.getCreatedForEntity().equals(3)){
					cityName=jobOrder.getSchool().get(0).getCityName()==null?"":jobOrder.getSchool().get(0).getCityName();
					stateName=jobOrder.getSchool().get(0).getStateMaster().getStateName()==null?"":jobOrder.getSchool().get(0).getStateMaster().getStateName();
					sb.append("<br/>"+cityName);
					if(cityName.equals("")){
						sb.append(stateName);
					}
					else{
						sb.append(", "+stateName);
					}	
					sb.append("<br/><a href='"+joburl+"'>View Details</a>");					
				}
				else{
					cityName = jobOrder.getDistrictMaster().getCityName()==null?"":jobOrder.getDistrictMaster().getCityName();
					stateName = jobOrder.getDistrictMaster().getStateId().getStateName()==null?"":jobOrder.getDistrictMaster().getStateId().getStateName();
					sb.append("<br/>"+cityName);
					if(cityName.equals("")){
						sb.append(stateName);
					}
					else{
						sb.append(", "+stateName);
					}
					sb.append("<br/><a href='"+joburl+"'>View Details</a>");
				}
				sb.append("</td></tr>");
			}



			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Thank you for using TeacherMatch!<br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	public static String getCGMailText(List<JobOrder> jobOrders)
	{
		if(locale.equals("fr"))
			return getCGMailTextFR(jobOrders);
			
		StringBuffer sb = new StringBuffer();
		
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hello, <br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Candidate Grid(s) for the following open position(s) is available. ");
			sb.append("<ul>");
			String url = Utility.getValueOfPropByKey("basePath");
			for (JobOrder jobOrder : jobOrders) {
				sb.append("<li><a href='"+url+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity()+"'>"+jobOrder.getJobTitle()+"</a></li>");
			}
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("To access and view the Candidate Grid(s), please log in <a href='"+url+"'>here</a>");
			sb.append("<br/>Kindly note that we will continue to send this weekly email notification for all of your open positions till they are filled.");
			sb.append("<br/>Additional information on Candidate Grid is included in the FAQs below.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely,<br/>TeacherMatch Administrator<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Frequently Asked Questions:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>How do I use the Candidate Grid report on TeacherMatch platform?</b> We suggest you combine the information in the CG report with the application materials you have for each candidate to help you prioritize who to interview for your open position. Note that this list does not reflect every candidate that has applied to your position, only those that have chosen to participate in the EPI. Your CG will change from week to week as more candidates participate in the EPI.");
			sb.append("<br/><br/></td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>What do the colors mean?</b> The percent listed in each column reflects the candidate's percent correct per domain. The composite score takes into account the varied weighting per domain, and creates a composite score for overall performance in the three areas. Candidate scores (percentages) in each area are static upon completion of the EPI and will not change. Candidate color coding may change across job postings depending on the quality of each applicant pool. Green indicates the candidate scored better than most of the other candidates that applied to this job. Yellow indicates the candidate scored about the same and red indicates the candidate scored worse than most candidates. ");
			sb.append("<br/><br/></td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>What if additional candidates complete the EPI this week - Do I have to wait until next week to get an updated notification?</b> You can log into the <a href='"+url+"'>TeacherMatch platform</a> at any time to get a live Candidate Grid report.");
			sb.append("<br/><br/></td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>This position is already filled - why am I getting this notification?</b> It sometimes takes our systems a few days to synch up. You should not receive this report again next week for this same position. If you do, just let your HR team leader know so we can correctly fill your position in MyApp.");
			sb.append("<br/><br/></td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>Who do I contact with questions:</b> " +
					"<ul>  " +
					"<li>For technical questions about accessing the TeacherMatch platform contact TeacherMatch client services.</li>"+
					"<li>For other questions contact your HR team leader or the HR Sourcing & Onboarding team (980-343-1848).</li>"+
					"</ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String mailToTeacherForEpiOrNoRecord(String bitlyURL,UserMaster userSession,TeacherDetail teacherDetail,String jobTitle,boolean newUser,int mailType,String schoolDistrictName)
	{
		if(locale.equals("fr"))
			return mailToTeacherForEpiOrNoRecordFR( bitlyURL, userSession, teacherDetail, jobTitle, newUser, mailType, schoolDistrictName);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			String district_branch=userSession.getHeadQuarterMaster()==null?"District":"Branch";
			
			sb.append("<table>");
			//Get temporary base URL
			String loginUrl = teacherDetail.getIdentityVerificationPicture()+"signin.do";
			
			if(mailType==0){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/> Thank you for your interest in being a teacher with "+schoolDistrictName+". The next step in the application process is for you to complete the Educator's Professional Inventory (EPI) as requested by "+schoolDistrictName+" Human Resources. Applications are reviewed on a daily basis so we encourage you to complete the EPI as soon as you are available.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Please note the following:");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li> If you are a current, certified "+schoolDistrictName+" employee, please check with the "+district_branch+" you are applying to whether you are required to take the EPI.</br></li><li> If you believe you have already taken the EPI under a different email address, please contact <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("For a list of Frequently Asked Questions regarding the EPI, please click the following link: <a href='"+bitlyURL+"'>"+bitlyURL+"</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Before you start the EPI, please note that each item in the inventory has a stipulated time limit and you must respond to each question within its stipulated time limit. You must answer all of the questions in one sitting. If you continue, please be prepared to follow these guidelines:");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>Reserve at least 90 minutes of uninterrupted time to complete the Base Inventory.</br></li><li>Use a stable and reliable Internet connection.</br></li><li>Do not close your browser or hit the \"back\" button on your browser.</br></li><li>You are not able to skip questions.</li><li>By taking the EPI, you agree to TeacherMatch's Terms of Use.</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				/*sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If you do not follow these guidelines, your status may become \"timed out\" and you will not be able to complete the EPI for twelve months.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");*/
				
				if(newUser){
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("NEXT STEP: Start the EPI by clicking on the link below and using the provided login information.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<a href='"+loginUrl+"'>"+loginUrl+"</a><br/> Email: "+teacherDetail.getEmailAddress()+"<br/>Password: "+teacherDetail.getPassword()+"<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}else{
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("NEXT STEP: Start the EPI by clicking on the link below and using the provided login information.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<a href='"+loginUrl+"'>"+loginUrl+"</a><br/> Email: "+teacherDetail.getEmailAddress()+"<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If you have any questions about this process, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/> Thank you for your interest in the position of "+jobTitle+" at "+schoolDistrictName+". As you have already completed the Educator's Professional Inventory (EPI), your application is complete and you don't need to take any further action with regards to the EPI.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely,<br/>TeacherMatch Client Services<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
		
	}

	/*================== Gagan : get dynamic email for Candidates On Import ====================*/
	public static String mailToTeacherOnImport(String bitlyURL ,TeacherDetail teacherDetail,MailToCandidateOnImport mailToCandidateOnImport)
	{
		 
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			String mailBody	=	mailToCandidateOnImport.getMailBody();
			if(mailBody.contains("&lt; Teacher First Name &gt;") || mailBody.contains("&lt; Teacher Email &gt;"))
			{
				mailBody	=	mailBody.replaceAll("&lt; Teacher First Name &gt;",teacherDetail.getFirstName());
				mailBody	=	mailBody.replaceAll("&lt; Teacher Email &gt;",teacherDetail.getEmailAddress());
				mailBody	=	mailBody.replaceAll("&lt; Teacher Password &gt;",teacherDetail.getPassword());
				mailBody	=	mailBody.replaceAll("&lt; bitlyURL &gt;",bitlyURL);
			}
			sb.append(mailBody);
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static String getCancelledEvntMailText(HttpServletRequest request,DemoClassSchedule demoClassSchedule,DemoClassAttendees demoClassAttendees,List<DemoClassAttendees> demoClassAttendeesList)
	{
		if(locale.equals("fr"))
			return getCancelledEvntMailTextFR( request, demoClassSchedule, demoClassAttendees, demoClassAttendeesList);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = demoClassSchedule.getTeacherId();
			UserMaster userMaster = demoClassAttendees.getInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please note that the following demo lesson has been cancelled.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Demo Lesson Date/Time:");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat()+" "+demoClassSchedule.getTimeZoneId().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Demo Location:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoClassAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<demoClassAttendeesList.size();i++)
			{
				UserMaster um = demoClassAttendeesList.get(i).getInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	public static String getRemovedAttendeeMailText(HttpServletRequest request,DemoClassSchedule demoClassSchedule,DemoClassAttendees demoClassAttendees,List<DemoClassAttendees> demoClassAttendeesList)
	{
		if(locale.equals("fr"))
			return getRemovedAttendeeMailTextFR(request, demoClassSchedule, demoClassAttendees, demoClassAttendeesList);
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = demoClassSchedule.getTeacherId();
			UserMaster userMaster = demoClassAttendees.getInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("You are no longer on the attendee list for the demo lesson below.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Demo Lesson Date/Time:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat()+" "+demoClassSchedule.getTimeZoneId().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Demo Location:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoClassAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<demoClassAttendeesList.size();i++)
			{
				UserMaster um = demoClassAttendeesList.get(i).getInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String getNewAttendeeMailText(HttpServletRequest request,DemoClassSchedule demoClassSchedule,DemoClassAttendees demoClassAttendees,List<DemoClassAttendees> demoClassAttendeesList,boolean brandNew)
	{
		if(locale.equals("fr"))
			return getNewAttendeeMailTextFR( request, demoClassSchedule, demoClassAttendees, demoClassAttendeesList, brandNew);

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = demoClassSchedule.getTeacherId();
			UserMaster userMaster = demoClassAttendees.getInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(brandNew)
				sb.append("A new demo lesson is on the books. Details below:<br/><br/>");
			else
				sb.append("You have a demo lesson on the books. Details below:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Demo Lesson Date/Time:");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat()+" "+demoClassSchedule.getTimeZoneId().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Demo Location:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoClassAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<demoClassAttendeesList.size();i++)
			{
				UserMaster um = demoClassAttendeesList.get(i).getInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String getExistingAttendeeMailText(HttpServletRequest request,DemoClassSchedule demoClassSchedule,DemoClassAttendees demoClassAttendees,List<DemoClassAttendees> demoClassAttendeesList)
	{
		if(locale.equals("fr"))
			return getExistingAttendeeMailTextFR( request, demoClassSchedule, demoClassAttendees,demoClassAttendeesList);

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = demoClassSchedule.getTeacherId();
			UserMaster userMaster = demoClassAttendees.getInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Following changes have been made to a scheduled demo lesson. Please make a note<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Demo Lesson Date/Time:");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat()+" "+demoClassSchedule.getTimeZoneId().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Demo Location:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoClassAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<demoClassAttendeesList.size();i++)
			{
				UserMaster um = demoClassAttendeesList.get(i).getInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	public static String getExistingAttendeeMailTextFR(HttpServletRequest request,DemoClassSchedule demoClassSchedule,DemoClassAttendees demoClassAttendees,List<DemoClassAttendees> demoClassAttendeesList)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = demoClassSchedule.getTeacherId();
			UserMaster userMaster = demoClassAttendees.getInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour"+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Les modifications suivantes ont �t� apport�es � la d�monstration de la le�on pr�vue.  Veuillez noter :<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Date/heure de la d�monstration de la le�on : ");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat()+" "+demoClassSchedule.getTimeZoneId().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Lieu de la d�monstration :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoClassAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Commentaires :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Participants :  ");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<demoClassAttendeesList.size();i++)
			{
				UserMaster um = demoClassAttendeesList.get(i).getInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement, <br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	/* @Author:Sekhar */
	public static String statusNoteSendToAdmin(UserMaster userSession, String gridURL,String fromUserName,String statusName,String teacherName,String jobOrderName)
	{
		if(locale.equals("fr"))
			return statusNoteSendToAdminFR(userSession, gridURL, fromUserName, statusName, teacherName, jobOrderName);
		
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hello,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(fromUserName+" recently changed the status of applicant "+teacherName+" who had applied for "+jobOrderName+" to "+statusName+".</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("You can view the Candidate's Grid <a href='"+gridURL+"'>here</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best wishes,<br/>TM Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please contact your Admin at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			
			//System.out.println(" mail text----------- :: "+sb.toString());
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();
	}
	
	public static String statusNoteSendToAdminFR(UserMaster userSession, String gridURL,String fromUserName,String statusName,String teacherName,String jobOrderName)
	{
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(fromUserName+" a r�cemment chang� le statut du candidat potentiel "+teacherName+" qui avait postul� pour"+jobOrderName+"  �  "+statusName+".</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Vous pouvez consulter la grille du candidat ici  <a href='"+gridURL+"'>here</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Meilleurs v�ux,  <br/>TM Administrateur  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur �  <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			
			//System.out.println(" mail text----------- :: "+sb.toString());
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();
	}
	
	/*
	 * @Start
	 * @Ashish Kumar
	 * @Description :: Change only Text Format
	 */
	public static String statusNoteSendToCandidate(UserMaster userSession, String gridURL,String fromUserName,String statusShortName,String statusName,String teacherName,String jobTitle,String[] arrHrDetail)
	{
		if(locale.equals("fr"))
			return statusNoteSendToCandidateFR( userSession,  gridURL, fromUserName, statusShortName, statusName, teacherName, jobTitle, arrHrDetail);
			
		System.out.println(" teacher name :: "+teacherName+" joborder : "+jobTitle+"  statusName :"+statusName+" statusShortName : "+statusShortName);
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		String jobBoardURL	=	arrHrDetail[12];
		String contactEmailAddress = "";
		
		if(userSession.getDistrictId().getDistrictId().equals(1201470)){
			dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
		}
		if(isHrContactExist==1)
			contactEmailAddress =	hrContactEmailAddress;
		else
			if(isHrContactExist==2)
				contactEmailAddress = dmEmailAddress;
		
		System.out.println(" contactEmailAddress : "+contactEmailAddress);
		
		
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+teacherName+",</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			if(statusShortName.equalsIgnoreCase("rem"))
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Thank you for your interest in the position of "+jobTitle+" at the "+schoolDistrictName+". We had a number of qualified applicants for this position making our decisions very difficult, however, at this time, you have not been selected to move forward in the process.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("We encourage you to visit our website and to view other current and future opportunities with our District at <a href='"+jobBoardURL+"'>"+jobBoardURL+"</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Please feel free to email us with any questions. We can be reached at <a href='mailto:"+contactEmailAddress+"'>"+contactEmailAddress+"</a>. If this or any other email correspondence is unclear, please contact us for further information or clarification.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else{
				if(statusShortName.equalsIgnoreCase("dcln"))
				{
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Thank you for your interest in the position of "+jobTitle+" position at the "+schoolDistrictName+". We are disappointed that you have declined our offer. However if you change your mind or if there is anything we can do regarding your decision, please let us know.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}else{
					if(statusShortName.equalsIgnoreCase("hird") || statusShortName.equalsIgnoreCase("widrw"))
					{
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("Your Status for "+jobTitle+" has been changed to "+statusName+".<br/><br/>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("<tr>");
					}else{
						if(statusShortName.equalsIgnoreCase("oth") || statusShortName.equalsIgnoreCase("scomp") || statusShortName.equalsIgnoreCase("ecomp") || statusShortName.equalsIgnoreCase("vcomp"))
						{
							System.out.println("\n Gagan : Mail content for Secondry Status ="+statusName+"= statusShortName = "+statusShortName+" ");
							sb.append("<tr>");
							sb.append("<td style="+mailCssFontSize+">");
							sb.append("Your application status for the "+jobTitle+" position at the "+schoolDistrictName+" was recently updated to "+statusName+" completed.<br/><br/>");
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("<tr>");
						}
					}
				}
				if(statusShortName.equalsIgnoreCase("dcln")){
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("We can be reached at <a href='mailto:"+contactEmailAddress+"'>"+contactEmailAddress+"</a>. If this or any other email correspondence is unclear, please contact us for further information or clarification.  We wish you luck in your future endeavors.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}else{
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call 8559800511.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			/*========= Gagan : Footer row for different in Manage Status ==========*/
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sincerely<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sincerely<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sincerely<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</tr>");
			
			sb.append("</table>");
			System.out.println("sb:::3"+sb.toString());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//System.out.println(" Mail Check text :: "+sb.toString());
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();
	}
	public static String statusNoteSendToCandidateFR(UserMaster userSession, String gridURL,String fromUserName,String statusShortName,String statusName,String teacherName,String jobTitle,String[] arrHrDetail)
	{
		System.out.println(" teacher name :: "+teacherName+" joborder : "+jobTitle+"  statusName :"+statusName+" statusShortName : "+statusShortName);
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		String jobBoardURL	=	arrHrDetail[12];
		String contactEmailAddress = "";
		
		if(userSession.getDistrictId().getDistrictId().equals(1201470)){
			dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
		}
		if(isHrContactExist==1)
			contactEmailAddress =	hrContactEmailAddress;
		else
			if(isHrContactExist==2)
				contactEmailAddress = dmEmailAddress;
		
		System.out.println(" contactEmailAddress : "+contactEmailAddress);
		
		
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+teacherName+",</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			if(statusShortName.equalsIgnoreCase("rem"))
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Nous vous remercions de votre int�r�t pour le poste de "+jobTitle+" au "+schoolDistrictName+".  Nous avons eu un certain nombre de candidats qualifi�s pour ce poste ce qui rend nos d�cisions tr�s difficile, cependant, � ce moment, vous n'avez pas �t� s�lectionn� pour aller de l'avant dans le processus. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Nous vous encourageons � visiter notre site web pour voir les autres possibilit�s actuelles et futures avec notre Conseil scolaire �  <a href='"+jobBoardURL+"'>"+jobBoardURL+"</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Veuillez-vous sentir libre de nous faire parvenir toutes questions par courrier �lectronique. Vous pouvez nous rejoindre �  <a href='mailto:"+contactEmailAddress+"'>"+contactEmailAddress+"</a>. N�h�sitez pas � nous contacter pour toutes questions suppl�mentaires ou �claircissements.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else{
				if(statusShortName.equalsIgnoreCase("dcln"))
				{
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Nous vous remercions de votre int�r�t pour le poste de "+jobTitle+"  au "+schoolDistrictName+". Nous sommes d��us que vous ayez d�clin� notre offre. Toutefois, si vous changez d'avis ou s�il y a quelque chose que nous pouvons faire au sujet de votre d�cision, veuillez nous en faire part.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}else{
					if(statusShortName.equalsIgnoreCase("hird") || statusShortName.equalsIgnoreCase("widrw"))
					{
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("Votre statut pour  "+jobTitle+" a �t� chang� pour "+statusName+".<br/><br/>");
						sb.append("</td>");
						sb.append("</tr>");
						sb.append("<tr>");
					}else{
						if(statusShortName.equalsIgnoreCase("oth") || statusShortName.equalsIgnoreCase("scomp") || statusShortName.equalsIgnoreCase("ecomp") || statusShortName.equalsIgnoreCase("vcomp"))
						{
							System.out.println("\n Gagan : Mail content for Secondry Status ="+statusName+"= statusShortName = "+statusShortName+" ");
							sb.append("<tr>");
							sb.append("<td style="+mailCssFontSize+">");
							sb.append("YVotre statut d'application pour le poste "+jobTitle+" au "+schoolDistrictName+" a �t� r�cemment mis � jour  "+statusName+" termin�. <br/><br/>");
							sb.append("</td>");
							sb.append("</tr>");
							sb.append("<tr>");
						}
					}
				}
				if(statusShortName.equalsIgnoreCase("dcln")){
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Vous pouvez nous rejoindre �  <a href='mailto:"+contactEmailAddress+"'>"+contactEmailAddress+"</a>.  N�h�sitez pas � nous contacter pour toutes questions suppl�mentaires ou �claircissements. Nous vous souhaitons bonne chance dans vos projets. <br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}else{
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au 8559800511.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}
			}
			
			/*========= Gagan : Footer row for different in Manage Status ==========*/
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sinc�rement, <br/>"+schoolDistrictName+" L��quipe de recrutement et de s�lection  <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sinc�rement,<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sinc�rement<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</tr>");
			
			sb.append("</table>");
			System.out.println("sb:::3"+sb.toString());
		}catch(Exception e){
			e.printStackTrace();
		}
		
		//System.out.println(" Mail Check text :: "+sb.toString());
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();
	}
	

	public static String notifyJobAppliedMailText(HttpServletRequest request,List<JobOrder> jobs,UserMaster userMaster,TeacherDetail teacherDetail,String districtName,String JobBoardURL, boolean toTeacher)
	{
		if(locale.equals("fr"))
			return notifyJobAppliedMailTextFR(request, jobs, userMaster, teacherDetail, districtName, JobBoardURL,  toTeacher);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			String teacherName = getTeacherName(teacherDetail);
			String userName = getUserName(userMaster);
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(toTeacher)
				sb.append("Hello "+teacherDetail.getFirstName()+",<br><br>");	
			else
				sb.append("Hello "+userName+",<br><br>");
			
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(toTeacher){
				if(userMaster.getHeadQuarterMaster()!=null || userMaster.getBranchMaster()!=null)
					sb.append(userName+" from "+districtName+" recently added you as an applicant to the following job(s) at their Branch(s):<br><br>");
				else
					sb.append(userName+" from "+districtName+" recently added you as an applicant to the following job(s) at their District:<br><br>");	
			}else{
				sb.append("You recently added applicant "+teacherName+" to the following job(s) at "+districtName+"<br><br>");
			}
			sb.append("</td>");
			sb.append("</tr>");
			
			String url=Utility.getValueOfPropByKey("basePath");
			int i=0;
			for(JobOrder jobOrder: jobs)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1201470)){
					sb.append((++i)+". "+jobOrder.getJobTitle()+" <a href='"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"'>"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"</a>");	
				}
				else{
					sb.append((++i)+". "+jobOrder.getJobTitle()+" <a href='"+JobBoardURL+"'>"+JobBoardURL+"</a>");	
				}
				  
				
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>If you have any questions regarding this, please feel free to contact us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call 8559800511.");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		//System.out.println("sb::>>>>:::"+sb.toString());
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();

	}
	public static String notifyJobAppliedMailTextFR(HttpServletRequest request,List<JobOrder> jobs,UserMaster userMaster,TeacherDetail teacherDetail,String districtName,String JobBoardURL, boolean toTeacher)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			String teacherName = getTeacherName(teacherDetail);
			String userName = getUserName(userMaster);
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(toTeacher)
				sb.append("Bonjour "+teacherDetail.getFirstName()+",<br><br>");	
			else
				sb.append("Bonjour "+userName+",<br><br>");
			
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(toTeacher){
				if(userMaster.getHeadQuarterMaster()!=null || userMaster.getBranchMaster()!=null)
					sb.append(userName+" de "+districtName+" vous avez r�cemment ajout� comme un candidat aux offres d�emplois suivantes pour leur branche :  <br><br>");
				else
				sb.append(userName+" de "+districtName+" vous avez r�cemment ajout� comme un candidat aux offres d�emplois suivantes pour leur Conseil scolaire :  <br><br>");	
			}else{
				sb.append("Vous avez r�cemment ajout� "+teacherName+"  � l'offre d�emploi suivante au  "+districtName+"<br><br>");
			}
			sb.append("</td>");
			sb.append("</tr>");
			
			
			int i=0;
			for(JobOrder jobOrder: jobs)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append((++i)+". "+jobOrder.getJobTitle()+" <a href='"+JobBoardURL+"'>"+JobBoardURL+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Si vous avez des questions � ce sujet, contactez-nous au <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou 8559800511.");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement, <br/>L�administrateur de TeacherMatch<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		//System.out.println("sb::>>>>:::"+sb.toString());
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();

	}
	
	
	/*========== Gagan : mail content for withdarw a job from Teacher Dashbaord and Job of Interest ======*/
	public static String getWithdrawMailToTeacher(HttpServletRequest request,TeacherDetail teacherDetail,JobOrder jobOrder,String[] arrHrDetail)
	{
		if(locale.equals("fr"))
			return getWithdrawMailToTeacherFR(request, teacherDetail, jobOrder, arrHrDetail);
			
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		
		String jobordertype = arrHrDetail[16];	
		String joborderwithSingleSchoolAttachFlag = arrHrDetail[17];	
		String attachSchoolName	=	arrHrDetail[18];
		String districtNameforSJO	= arrHrDetail[19];
		if(jobOrder.getDistrictMaster().getDistrictId().equals(1201470)){
			dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
		}
		try 
		{
			//String loginUrl = Utility.getBaseURL(request)+"signin.do";
			String loginUrl = arrHrDetail[10];
			System.out.println(" login Url "+loginUrl);
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear <First Name>"+teacherDetail.getFirstName()+": Welcome. You have now completed the registration process for TeacherMatch<sup>TM</sup>. At TeacherMatch, we take pride in helping candidates find their best-fit roles. We also believe that great educators need actionable support, which is why we offer a customized professional development report, available after you have completed your Portfolio and the Base Inventory.<br/><br/>");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/>You are receiving this email because you withdrew your application for the "+jobOrder.getJobTitle()+"");
			
			/*==== [schoolDistrictName] will have value of district name if joborder is type 2[ DJO ] else School Name if jobordertype 3 [SJO] =========*/
			if(joborderwithSingleSchoolAttachFlag.equals("1"))
			{
				System.out.println(" If DJO with only  1 school Attach");
				sb.append(" position, "+attachSchoolName+" at the "+schoolDistrictName+".");
			}
			else
				if(jobordertype.equals("3"))
				{
					System.out.println("Else If [ SJO ]");
					sb.append(" position, "+schoolDistrictName+" at the "+districtNameforSJO+".");
				}
				else
				{
					System.out.println("Else Djo with Multiple School Attach : ");
					sb.append(" position at the "+schoolDistrictName+".");
				}
			sb.append(" We are disappointed, however if you change your mind, you can always re-apply by logging into your account at <a href='"+loginUrl+"'>"+loginUrl+"</a> and clicking the \"Apply\" link for the aforementioned job. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sincerely<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sincerely<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sincerely<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".");
			sb.append("</tr>");
              
			sb.append("</table>");
          
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 4"+sb.toString());
		
		return sb.toString();
	}
	
	public static String internalCandidateTransferMailText(String basePath,TeacherDetail teacherDetail,JobOrder jobOrder,List<SchoolMaster> schools,String[] arrHrDetail)
	{
		if(locale.equals("fr"))
			return internalCandidateTransferMailTextFR(basePath,teacherDetail,jobOrder,schools,arrHrDetail);

		StringBuffer sb = new StringBuffer();
		try 
		{
			String hrContactFirstName = arrHrDetail[0];
			String hrContactLastName = arrHrDetail[1];
			String hrContactEmailAddress = arrHrDetail[2];
			String phoneNumber			 = arrHrDetail[3];
			String title		 = arrHrDetail[4];
			int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
			String schoolDistrictName =	arrHrDetail[6];
			String dmName 			 = arrHrDetail[7];
			String dmEmailAddress = arrHrDetail[8];
			String dmPhoneNumber	 = arrHrDetail[9];
			
			String jobordertype = arrHrDetail[16];	
			String joborderwithSingleSchoolAttachFlag = arrHrDetail[17];	
			String attachSchoolName	=	arrHrDetail[18];
			String districtNameforSJO	= arrHrDetail[19];
			
			
			if(jobOrder.getDistrictMaster().getDistrictId().equals(1201470)){
				
				dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
			}

			DistrictMaster districtMaster = jobOrder.getDistrictMaster();
			String displayName = (districtMaster.getDisplayName()==null || districtMaster.getDisplayName().trim().length()==0)?districtMaster.getDistrictName():districtMaster.getDisplayName();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("A recent opportunity at "+displayName+" matched your internal transfer preferences. Details are below:<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Job Title: ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(jobOrder.getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr style="+mailCssFontSize+">");
			if(schools.size()>0)
			{
				sb.append("<td>");
				sb.append("School(s):");
				sb.append("</td>");
				sb.append("<td>");
				String sch="";
				for(SchoolMaster school: schools)
				{
					//String name = school.getDisplayName()==null?school.getSchoolName():districtMaster.getDisplayName();
					String name = (school.getDisplayName()==null || school.getDisplayName().trim().length()==0)?school.getSchoolName():school.getDisplayName();
					sch = sch+name+", ";
				}
				sb.append(sch.substring(0, sch.length()-1));
				sb.append("</td>");
			}else
			{
				sb.append("<td>");
				sb.append("District: ");
				sb.append("</td>");
				sb.append("<td>");
				sb.append(displayName);
				sb.append("</td>");
			}
			sb.append("</tr>");
		    sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			String joburl = basePath+"/"+"applyteacherjob.do?jobId="+jobOrder.getJobId();
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>to apply, please click <a href='"+joburl+"'>"+joburl+"</a><br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call 1-800-956-2861.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sincerely<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sincerely<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sincerely<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");
			System.out.println(sb.toString());

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String getNewPanelAttendeeMailText(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList,boolean brandNew,String externalURL)
	{
		if(locale.equals("fr"))
			return getNewPanelAttendeeMailTextFR( request, panelSchedule, panelAttendees, panelAttendeesList, brandNew, externalURL);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(brandNew)
				sb.append("A new Panel is on the books. Details below:<br/><br/>");
			else
				sb.append("You have a Panel on the books. Details below:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(getTeacherName(teacherDetail));
			sb.append(" "+candidateEmail);
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Panel Date/Time:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Panel Location:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			try{
				if(panelSchedule.getPanelDescription()!=null){
					sb.append(panelSchedule.getPanelDescription());
				}else{
					sb.append("");
				}
			}catch(Exception e){sb.append("");}
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			if(externalURL!=null)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br>Please Click on following link to score the candidate.<br/><br/><a href='"+externalURL+"'>"+externalURL+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			//only for marysville ==   5304860 =====//
			if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(5304860)){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("The issues of confidentiality and ethics are extremely important when taking part in the selection of new staff.  Each member of the team must make the commitment to have the process remain strictly confidential.  The following issues are not for public dissemination:");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>Documents developed</li><li>Statements made by the team or candidate</li><li>Deliberations</li><li>Impressions</li><li>Opinions</li><li>Ranking</li><li>Evaluations</li><li>Candidate materials</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("It is our obligation to protect the rights and preserve the self-respect of the candidate.");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("The only individuals authorized to give feedback information during the post interview conference are the administrator or Human Resources designee upon the request of the candidate. " +
						"Only certain data will be shared and then only to the specific candidate.  You may be pressured by coworkers and/or community members to share information about candidates, the process, questions asked of candidate, etc.  You may not respond to these questions other than to affirm that we are proceeding as planned and hope to be finished soon." +
						" If someone is insistent, refer him/her to Human Resources.");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br>Please remember, this is a professional process. <b>Confidentiality is an obligation of the team, not the candidate.</b><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a>. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			//End only for marysville ==   5304860 =====//
			else{
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	public static String getRemovedPanelAttendeeMailText(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList)
	{
		if(locale.equals("fr"))
			return getRemovedPanelAttendeeMailTextFR( request, panelSchedule, panelAttendees, panelAttendeesList);

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("You are no longer on the attendee list for the Panel below.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Panel Date/Time:");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Panel Location:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();

	}
	public static String getExistingPanelAttendeeMailText(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList)
	{
		if(locale.equals("fr"))
			return getExistingPanelAttendeeMailTextFR( request, panelSchedule, panelAttendees, panelAttendeesList);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Following changes have been made to a scheduled demo lesson. Please make a note<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Panel Date/Time:");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Panel Location:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	//public static String getNewPanelAttendeeMailText(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList,boolean brandNew)
	public static String getCancelledPanelEvntMailText(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList)
	{
		if(locale.equals("fr"))
			return getCancelledPanelEvntMailTextFR( request, panelSchedule, panelAttendees, panelAttendeesList);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please note that the following Panel has been cancelled.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Candidate Name:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Panel Date/Time:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Panel Location:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	public static String getMailToInternalTeacher(String loginURL,TeacherDetail teacherDetail,String jobTitle,String[] arrHrDetail)
	{
		if(locale.equals("fr"))
			return getMailToInternalTeacherFR( loginURL, teacherDetail, jobTitle, arrHrDetail);
			
		System.out.println("getMailToInternalTeacher::::");
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmPhoneNumber	 = arrHrDetail[9];
		
		
		

		
		try 
		{
			String DistrictDName = arrHrDetail[10];
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>Thank you for applying for "+jobTitle+" position at the "+DistrictDName+".</br></br>");
			sb.append("To complete your application for "+jobTitle+" position at the "+DistrictDName+", we would like you to complete the Educators Professional Inventory (EPI) as well.</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
					
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Here are the simple steps for your to complete your EPI:<br/><br/>");
			sb.append("<ol><li>Please log into your TeacherMatch account</br></li><li>Your login id is <a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a></br></li><li>Your password is the password you may have set for TeacherMatch</br></li><li>After login, please go to Dashboard </li><li>Please locate the EPI (Educators Professional Inventory) item under Milestones.</li><li>Your EPI should be showing a status of �Incomplete� and right next to it should be an icon to launch and complete the EPI. Please click on that icon to complete your EPI</li><li>You can log into TeacherMatch <a href='"+loginURL+"'>here</a></li></ol>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sincerely<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sincerely<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(DistrictDName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sincerely<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(DistrictDName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String getMailToSendJsiInvites(String loginURL,TeacherDetail teacherDetail,String[] arrHrDetail,JobOrder jobOrder)
	{
		if(locale.equals("fr"))
			return getMailToSendJsiInvitesFR( loginURL, teacherDetail, arrHrDetail, jobOrder);
			
		System.out.println("getMailToInternalTeacher::::");
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmPhoneNumber	 = arrHrDetail[9];
		String jobTitle = jobOrder.getJobTitle();
		String dmEmail	 = arrHrDetail[8];
		String hrEmail	 = arrHrDetail[2];
		
		if(jobOrder.getDistrictMaster().getDistrictId().equals(1201470)){
			dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
		}
		try 
		{
			String DistrictDName = arrHrDetail[10];
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>Thank you for applying for the "+jobTitle+" position at the "+DistrictDName+".</br></br>");
			sb.append("&nbsp;To complete your application for the "+jobTitle+" position at the "+DistrictDName+", we would like you to complete the Job Specific Inventory (JSI) as well.</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
					
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Here are the simple steps for you to complete your JSI:<br/><br/>");
			if(jobOrder.getJobAssessmentStatus()==2)
			{
				//send a link
				String baseURL=Utility.getValueOfPropByKey("basePath");
				String forMated = "";
				String tchrId=Utility.encryptNo(teacherDetail.getTeacherId());
				String jobId=Utility.encryptNo(jobOrder.getJobId());
				String linkIds= tchrId+"###"+jobId;
				try {
					forMated = Utility.encodeInBase64(linkIds);
					forMated = baseURL+"tmjsi.do?id="+forMated;
				} catch (Exception e) {
					e.printStackTrace();
				}	
				sb.append("Clicking the URL <a href='"+forMated+"'>"+forMated+"</a> <br><br>");
			}else
				sb.append("<ol><li>Please log into your TeacherMatch account</br></li><li>Your login id is <a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a></br></li><li>Your password is the password you may have set for TeacherMatch</br></li><li>After login, please go to Dashboard </li><li>Please locate the JSI (Job Specific Inventory) item under Milestones.</li><li>Your JSI should be showing a status of �Incomplete� and right next to it should be an icon to launch and complete the JSI. Please click on that icon to complete your JSI</li><li>You can log into TeacherMatch <a href='"+loginURL+"'>here</a></li></ol>");
			
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".<br/><br/>");
			sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a> or call "+mailContactNo+".<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			
			/**
			*Changes by Amit for changing the signature of Philadelphia
			*@Date: 10-Mar-15
			*/
			if(isHrContactExist==0)
			{
				if(jobOrder.getDistrictMaster().getDistrictId()!=null && !jobOrder.getDistrictMaster().getDistrictId().equals("") && jobOrder.getDistrictMaster().getDistrictId()!=0 && jobOrder.getDistrictMaster().getDistrictId()==4218990)
					sb.append("Sincerely<br/>Office of Talent<br/><a href='mailto:principalschoosephilly@philasd.org'>principalschoosephilly@philasd.org</a><br/><br/>");
				else
					sb.append("Sincerely<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
			}
			else 
				if(isHrContactExist==1)
				{
					if(jobOrder.getDistrictMaster().getDistrictId()!=null && !jobOrder.getDistrictMaster().getDistrictId().equals("") && jobOrder.getDistrictMaster().getDistrictId()!=0 && jobOrder.getDistrictMaster().getDistrictId()==4218990)
						sb.append("Sincerely<br/>Office of Talent<br/><a href='mailto:principalschoosephilly@philasd.org'>principalschoosephilly@philasd.org</a><br/><br/>");
					else
					{
						sb.append("Sincerely<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
						if(title!="")
						sb.append(title+"<br/>");
						if(hrEmail!="")
						sb.append(hrEmail+"<br/>");	
						if(phoneNumber!="")
							sb.append(phoneNumber+"<br/>");
						sb.append(DistrictDName+"<br/><br/>");
					}
				}
				else
				{
					if(isHrContactExist==2)
					{
						if(jobOrder.getDistrictMaster().getDistrictId()!=null && !jobOrder.getDistrictMaster().getDistrictId().equals("") && jobOrder.getDistrictMaster().getDistrictId()!=0 && jobOrder.getDistrictMaster().getDistrictId()==4218990)
							sb.append("Sincerely<br/>Office of Talent<br/><a href='mailto:principalschoosephilly@philasd.org'>principalschoosephilly@philasd.org</a><br/><br/>");
						else
						{
							sb.append("Sincerely<br/>"+dmName+"<br/>");
							if(dmEmail!="")
								sb.append(dmEmail+"<br/>");	
							if(dmPhoneNumber!="")
								sb.append(dmPhoneNumber+"<br/>");
							sb.append(DistrictDName+"<br/><br/>");
						}
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		//System.out.println("sb::::::::"+sb.toString());
		return sb.toString();
	}
	
	
	public static String getPanelEmailBody(String toEmail,String sEmailSubject,String sPanelUserName,String sPanelUserNameTo,String sTeacheName,String sJobTitle,String sStatusName,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return getPanelEmailBodyFR(toEmail, sEmailSubject, sPanelUserName, sPanelUserNameTo, sTeacheName, sJobTitle, sStatusName, userMaster);
			
		StringBuffer sb = new StringBuffer();
		
		try 
		{
			sb.append("<table>");
			
			
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+sPanelUserNameTo+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(sPanelUserName+" has set "+sStatusName+" for "+sJobTitle+" for "+sTeacheName+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				/*sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely<br/>");
				sb.append("Ramesh");
				sb.append("</td>");
				sb.append("</tr>");*/
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::"+sb);
		return sb.toString();
	}
	
	public static String getPCEmailBody(TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getPCEmailBodyFR( teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		
		try 
		{
				sb.append("<table>");
			
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getTeacherName(teacherDetail)+",<br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Miami-Dade County Public Schools Board Policy 3140, states, �No person who has been separated from the employ of the Board for cause shall be reemployed in any department on any basis unless a special request for so doing has been approved by the Board.� Accordingly, you are not eligible for reemployment with the District.");
				sb.append("</td>");
				sb.append("</tr>");
				 
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("We wish you success in future endeavors.");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely,<br/><br/>");
				sb.append("The Office of Human Capital Management<br/>");
				sb.append("Miami-Dade County Public Schools");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::"+sb);
		return sb.toString();
	}
	
	public static String getRestrictedEmailBody(TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getRestrictedEmailBodyFR( teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		
		try 
		{
				sb.append("<table>");
			
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getTeacherName(teacherDetail)+",<br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Thank you for applying for a new position with Miami-Dade County Public Schools. Our records indicate the position you are applying for is restricted and/or not within the scope of the position you currently hold; therefore, please contact the Office of Professional Standards at (305) 995-7120 to determine if you are eligible for the position you are seeking.");
				sb.append("</td>");
				sb.append("</tr>");
				 
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("We appreciate your continued dedication to the District.");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely,<br/><br/>");
				sb.append("The Office of Human Capital Management<br/>");
				sb.append("Miami-Dade County Public Schools");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::"+sb);
		return sb.toString();
	}
	/*  Mail Added By Hanzala Subhani Dated 28-05-2014  */
public static String getFPRejectMail(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getFPRejectMailFR( request, teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+":<br/><br/> Thank you for your interest in working for Miami-Dade County Public Schools .Please be advised that your fingerprints were rejected by the Federal Bureau of Investigation (FBI) for illegibility or other technical issues.  In order for the employment/volunteer/field experience/contracted services process to continue, you must return to be refingerprinted within 15 working days to the Fingerprint Office. There is no additional charge for the refingerprinting service.  You must present a valid picture identification such as a driver�s license, passport, State Identification Card in order to be refingerprinted.  Please call 305-995-7472 if you need additional information.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Office of Human Capital Management<br/>Miami-Dade County Public Schools<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			//System.out.println(sb);
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}

public static String getFPFailMail(HttpServletRequest request,TeacherDetail teacherDetail,TeacherPersonalInfo teacherPersonalInfo)
{
	if(locale.equals("fr"))
		return getFPFailMailFR( request, teacherDetail, teacherPersonalInfo);
		
	StringBuffer sb = new StringBuffer();
	try 
	{
		String city 		= 	"";
		String state 		= 	"";
		String zip 			= 	"";
		String address1		=	"";
		String address2		=	"";
		String address		=	"";
		//String firstName	=	"";
		String addressline2	=	"";
		if(teacherPersonalInfo.getCityId() != null){
			city	=	teacherPersonalInfo.getCityId().getCityName();
			state	=	teacherPersonalInfo.getStateId().getStateName();
			zip		=	teacherPersonalInfo.getCityId().getZipCode();	
		}
		if(teacherPersonalInfo != null){
			address1	=	teacherPersonalInfo.getAddressLine1();
			address2	=	teacherPersonalInfo.getAddressLine2();
			address		=	address1+", "+address2;
			//firstName	=	teacherDetail.getFirstName();
			addressline2	=	city+","+state+","+zip;
		}
		sb.append("<table>");
		System.out.println("NAME="+teacherPersonalInfo.getCityId());
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append(getTeacherName(teacherDetail)+"<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append(address+"<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append(addressline2+"<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("The Fingerprint Office has received the results of your fingerprint analysis from the Florida Department of Law Enforcement and the Federal Bureau of Investigation reflecting an incident that led to court action. Please be advised that in order for the employment/volunteer/field experience/contracted services process to continue, you must provide the following documents:<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("<ul><li>Incident Report (initial police investigative report) or Arrest Report/Affidavit (criminal report affidavit)</br></li><li>Information or Indictment (formal charges filed by the prosecutor with the court)</li><li>Court Adjudication (the court�s disposition of your case)</li><li>Documentation of successful completion of probation or pre-trial intervention and payment of fine(s), if applicable</li><li>A notarized statement written and signed by you explaining the circumstances of arrest(s)</li></ul>");
		sb.append("</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("Please reply to this email if you accept this employment assignment with Miami-Dade County Public Schools and contact the school for information on the next steps. This offer is void if you do not reply within 2 business days. We look forward to welcoming you to Miami-Dade County Public Schools.<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("<B>All photocopies of court documents or certification from the Clerk of the Court must be original, certified copies.</B>  You should contact the Clerk of the Court in the city or county where the case was disposed.  Documents should be submitted <B>within 15 working days</B><br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("from the date of this email to the address:<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("Miami-Dade Schools Police Fingerprinting Unit � 1450 N.E. 2nd Ave � Suite 110 � Miami, FL 33132<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");
		
		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("Sincerely,<br/><br/>Sigilenda Miles<br/>Executive Director of Fingerprinting<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("</table>");
	} 
	catch(Exception e) 
	{
		e.printStackTrace();
	}
	
	System.out.println(" "+sb.toString());
	
	return sb.toString();
}

public static String getDTRejectMail(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getDTRejectMailFR( request, teacherDetail);
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The analysis of your pre-employment drug screening has been confirmed positive.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("School Board Policy 1124, 3124, 4124, Drug-Free Workplace, states:");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Applicants will be informed in advance of the requirement of a </br>negative drug screen as condition of employment.  Applicants</br> testing positive will not be eligible for employment by Miami-Dade</br> County Public Schools for three (3) years from the date of the test.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("As a result, your name has been removed from the employment eligibility list for the period stipulated by Board Policy.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Office of Human Capital Management<br/>Miami-Dade County Public Schools<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}

public static String getFirstPanelMember(String sJobTitle,String sTeacheName,String schoolLocation,String requisitionNumber)
	{
		if(locale.equals("fr"))
			return getFirstPanelMemberFR( sJobTitle, sTeacheName, schoolLocation, requisitionNumber);
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("School Name : "+schoolLocation+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Applicant Name : "+sTeacheName+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Position : "+sJobTitle+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Position Number : "+requisitionNumber+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::"+sb);
		return sb.toString();
	}
	public static String getSecondOfferLetter(TeacherDetail teacherDetail,String jobTitle,String location,String acceptURL,String declineURL,boolean isSubstituteInstructionalJob)
	{
		if(locale.equals("fr"))
			return getSecondOfferLetterFR( teacherDetail, jobTitle, location, acceptURL, declineURL, isSubstituteInstructionalJob);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			String position = jobTitle+" at "+location;
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			if(isSubstituteInstructionalJob){
				position = jobTitle;
				sb.append("<table>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getTeacherName(teacherDetail)+" "+candidateEmail+",<br/><br/> Congratulations! The School Board of Miami-Dade County, Florida, has verified initial eligibility to employ you as a "+position+". Please visit the Office of Instructional Staffing and Recruitment (1450 Northeast Second Avenue, Suite 265, Miami, FL 33132) to complete the application process<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("To verify final eligibility and to accept our conditional offer of employment as a "+position+" and to begin the hiring process, please visit the Office of Instructional Staffing and Recruitment (Suite 265) with the following items:<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				//sb.append("<ul><li>I-9/Employment Eligibility Verification - Original social security card & driver's license/government-issued ID (work authorization, if needed)</li><li>$71.00 money order payable to \"SBMD Fingerprinting\" for background check/fingerprinting</li><li>$75.00 money order payable to \"M-DCPS\" for substitute certificate (not required if you hold a valid Florida Department of Education temporary or professional educator's certificate)</li><li>Bank information for direct deposit enrollment (voided check required)</li></ul>");
				sb.append("<ul><li>I-9/Employment Eligibility Verification - Original social security card & driver's license/government-issued ID (work authorization, if needed)</li><li>$71.00 money order payable to \"SBMD Fingerprinting\" for background check/fingerprinting</li><li>Bank information for direct deposit enrollment (voided check required)</li></ul>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("You will then be directed to:</br><ul><li>Complete drug testing (authorization/testing information will be provided by the District upon arrival)</li><li>Sign and acknowledge the Guidelines For Temporary Instructors</li><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("We look forward to meeting you soon.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely,<br/><br/>The Office of Instructional Staffing and Recruitment<br/>1450 Northeast Second Avenue, Suite 265, Miami, FL 33132<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("</table>");
			}else{
				sb.append("<table>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/> Congratulations! The School Board of Miami-Dade County, Florida, hereby offers to conditionally employ you as "+position+" for the 2015-2016 school year.  This offer is conditional and based on the following requirements including/but not limited to:<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>Certification/Eligibility Verification</br></li><li>Clearance of Fingerprints</li><li>Drug Testing Clearance</li><li>I-9/Employment Eligibility Verification</li><li>Acceptable Reference/Background Check</li><li>Transcript Verification</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Please reply to this email if you accept this employment assignment with Miami-Dade County Public Schools and contact the school for information on the next steps. This offer is void if you do not reply within 2 business days. We look forward to welcoming you to Miami-Dade County Public Schools.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href='"+acceptURL+"'>Accept the Offer</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineURL+"'>Decline the Offer</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely,<br/><br/>The Office of Human Capital Management<br/>Miami-Dade County Public Schools<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("</table>");
			}
			
			//System.out.println("sb 2nd::::"+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	public static String getThirdOfferNextStep(TeacherDetail teacherDetail,String jobTitle,String location)
	{
		if(locale.equals("fr"))
			return getThirdOfferNextStepFR( teacherDetail, jobTitle, location);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			String position = jobTitle; 
			try{
				if(location!=null && !location.equals("")){
					position = jobTitle+" at "+location; 
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			sb.append("<table>");
	
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			
			sb.append("Dear "+getTeacherName(teacherDetail)+" "+candidateEmail+"<br/><br/>Congratulations on your decision to accept our conditional offer of employment as a "+position+".<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul><li>If the offer of employment is for a Part-Time Coach, you must first contact the Division of Athletics and Activities at 305-995-7576 within two business days of receiving this email to make an appointment and receive further instructions prior to visiting the Employee Service Center.</br></li><li>If the offer of employment is for any position other than a part-time coach, you must visit the Employee Service Center located at 1450 NE 2nd Ave, Suite 150 Miami, FL 33132 within two business days of receiving this email.  The Employee Service Center is open Monday-Friday from 8:00 a.m. to 3:30 p.m. </li></ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("At the Employee Service Center, you will need to complete the following procedure. PLEASE READ CAREFULLY:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ol><li>Fingerprinting:  All new employees must be fingerprinted and cleared before beginning employment with M-DCPS.  To be fingerprinted you must bring:<ol type=\"a\"><li >A $71.00 money order or cashier�s check made out to \"SBMD Fingerprinting\"; and</li><li>Your driver license (or valid government-issued picture identification) and social security card</li></ol></br></li><li>I-9/Employment Eligibility Verification:  Per the Department of Homeland Security, new employees must fill out an I-9 form for employment eligibility.  To complete the process, please bring the required documents listed on Page 5 of:<br/><a href=\"http://www.uscis.gov/files/form/i-9.pdf\" >http://www.uscis.gov/files/form/i-9.pdf</a></li><li>Direct Deposit Authorization:  M-DCPS has moved to a paperless payroll system.  All salary and wages are paid through an automated direct deposit method.  Employees will have to choose from one of three following options:<ol type=\"a\" start=\"3\"><li>To directly deposit your paycheck into one of your existing bank accounts, please bring a voided check or deposit slip from that financial institution; or</li><li>Sign-up for a Skylight Pay Card, learn more at:<br/><a href=\"http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf\">http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf</a>; or</li><li>Open a new account with the South Florida Educational Federal Credit Union </li></ol></li><li>Drug-testing:  Drug-testing is mandatory for all new full time employees and part-time coaches and is provided at no cost.  The test is conducted off-site at multiple locations throughout the county.  You will be provided a drug test form when you visit the Employee Service Center.  The drug test must be conducted within two business days of receiving your form.</li><li>Part-Time Coaches MUST bring a $75.00 money order or cashier�s check made out to \"Florida Department of Education\" to the Division of Athletics and Activities.</li></ol>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("In addition to the documents and procedures described above, you will be asked to fill out various employment-related forms during your visit to the Employee Service Center.  Upon the completion and verification of all of the above requirements, your new supervisor will contact you with the date/time that you should report for work.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Thank you,<br/><br/>The Employee Service Center Staff <br/>1501 N.E. 2nd Ave., Suite 336, Miami, FL 33132<br/><br/>");
			sb.append("Thank you,<br/><br/>The Employee Service Center Staff <br/>1450 NE 2nd Ave, Suite 150 Miami, FL 33132<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			
			//System.out.println("sb:: 3rd::"+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" "+sb.toString());
		
		return sb.toString();
		
	}
	public static String getThirdOfferNextStepForInternal(int internal,TeacherDetail teacherDetail,String jobTitle,String location)
	{
		if(locale.equals("fr"))
			return getThirdOfferNextStepForInternalFR( internal, teacherDetail, jobTitle, location);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
	
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>Congratulations on your decision to accept our conditional offer of employment as a "+jobTitle+" at "+location+".  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul><li>If the offer of employment is for a Part-Time Coach, you must first contact the Division of Athletics and Activities at 305-995-7576 within two business days of receiving this email to make an appointment and receive further instructions prior to calling the Employee Service Center.<br/><br/></li><li>Part-Time Coaches MUST bring a $75.00 money order or cashier�s check made out to \"Florida Department of Education\" to the Division of Athletics and Activities.</li></ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("For any other position you must call our Employee Service Center at: 305-995-7186 within two business days of receiving this email. The Employee Service Center is open Monday-Friday from 8:00 a.m. to 3:30 p.m.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank You,<br/><br/>The Employee Service Center Staff <br/>1450 NE 2nd Ave, Suite 456 Miami, FL 33132<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			
			//System.out.println("sb:: 3rd::"+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
		
	}
	public static String getFourthAcceptOrDecline(String panelUserName,TeacherDetail teacherDetail,String jobTitle,String location,int acceptOrDecline,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return getFourthAcceptOrDeclineFR( panelUserName, teacherDetail, jobTitle, location, acceptOrDecline, userMaster);
			
		StringBuffer sb = new StringBuffer();
		int entityType=0;
		try{
			if(userMaster!=null && userMaster.getEntityType()!=null){
				entityType=userMaster.getEntityType();
			}
		}catch(Exception e){}
		
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hello "+panelUserName+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			if(acceptOrDecline==0){
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(getTeacherName(teacherDetail)+"("+teacherDetail.getEmailAddress()+") has declined the the position of "+jobTitle+" at "+location+".<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else{
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(getTeacherName(teacherDetail)+"("+teacherDetail.getEmailAddress()+") has accepted the position of "+jobTitle+" at "+location+".");
				if(entityType!=3){
					sb.append(" <b>You must now revisit the Status Life Cycle and finalize \"Offer Accepted\" to move the candidate to the Onboarding phase</b>.");	
				}
				sb.append("<br/><br/></td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The applicant will receive onboarding instructions via email and must report to the Employee Service Center to complete onboarding. You will receive notification when the hire is complete and the employee is cleared to report to work.");
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb Fourth:::"+sb);
		return sb.toString();
	}
	public static String getFifthNoResponseAcceptOrDecline(TeacherDetail teacherDetail,JobOrder jobOrder,String location,Date offerDate,int iUserId)
	{
		if(locale.equals("fr"))
			return getFifthNoResponseAcceptOrDeclineFR( teacherDetail, jobOrder, location, offerDate, iUserId);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Please note that the "+getTeacherName(teacherDetail)+" was sent an offer on "+Utility.getUSformatDateTime(offerDate)+" for Job ID "+jobOrder.getJobId()+" titled "+jobOrder.getJobTitle()+" for a position at "+location+". It has been more than 48 Hrs and hence this reminder is being sent to you.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Candidate's email address is "+teacherDetail.getEmailAddress()); 
				if(teacherDetail.getPhoneNumber()!=null && !teacherDetail.getPhoneNumber().equals("")){
					sb.append(" and phone number is "+teacherDetail.getPhoneNumber()+".<br/><br/>");
				}else{
					sb.append("<br/><br/>");
				}
				
				String sParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+iUserId+"##"+location);
				String sEmailURL=Utility.getValueOfPropByKey("basePath")+"/noresponseresendoffer.do?key="+sParameters;
				sb.append("<a href='"+sEmailURL+"'>Click Here to Resend the Email Offer Ready</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely<br/>TeacherMatch Administrator<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
				sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		
		System.out.println(" "+sb.toString());
		return sb.toString();
	}
	
	public static String PNRRejectOrUnHire(TeacherDetail teacherDetail,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return PNRRejectOrUnHireFR( teacherDetail, userMaster);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getUserName(userMaster)+",<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				
					sb.append("<td style="+mailCssFontSize+">");
					
						sb.append("Candidate "+getTeacherName(teacherDetail)+" was removed from the onboarding process. This could have been due to any of the following reasons:<br/><br/>");
						
						sb.append("1. Issues during on-boarding<br/>");
						sb.append("2. Withdrawal or Decline by the Candidate during on-boarding process<br/>");
						sb.append("3. Rejection by the staffer or on-boarding team<br/><br/>");
						
						sb.append("Please contact your staffer for any additional question related to this matter.<br/><br/>MDCPS HR<br/><br/>");
					
					sb.append("</td>");
				
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
				sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		
		System.out.println(" "+sb.toString());
		return sb.toString();
	}
	
	public static String OfficialTranscripts(TeacherDetail teacherDetail,String sDistrictName)
	{
		if(locale.equals("fr"))
			return OfficialTranscriptsFR( teacherDetail, sDistrictName);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getTeacherName(teacherDetail)+" "+candidateEmail+",<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Thank you for applying for an instructional position with "+sDistrictName+". This letter is to inform you that the district has received copies of your official transcripts as required in order to proceed with the application. We wish you the best of luck in your job search.<br/><br/>"); 
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely,<br/>The Office of Instructional Staffing<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
				sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}

	// added by ashish ratan
	public static String createMailToAddedSchoolsInDJO(JobOrder jobOrder,
			SchoolMaster schoolMaster) {
		if(locale.equals("fr"))
			return createMailToAddedSchoolsInDJOFR( jobOrder,	 schoolMaster);
			
		System.out.println("IN MAIL CREAET METHOD");

		StringBuffer sb = new StringBuffer();
		String MESSAGE = "";
		try {
			MESSAGE = "<p><span style='font-size: small;'>"
					+ "<strong>Dear school admin(s),</strong>"
					+ "</span></p>"
					+ "<p><br />"
					+ "<span style='font-size: small;'>"
					+ "Your School as been added to the "
					+ "<strong>"
					+ "</strong>"
					+ " job order. You will now have access to the candidate grid reflecting all applicants for this position."
					+ "</span></p>"
					+ "<p><br />"
					+ "<span style='font-size: small;'>If you have questions, please contact your <strong>"
					+ schoolMaster.getDistrictName() + "</strong></span>"
					+ " HR"
					+ "<span style='font-size: small;'> lead.</span></p>"
					+ "<p><span style='font-size: small;'>&nbsp;</span></p>"
					+ "<p><span style='fontsize: small;'>Best</span><br />"
					+ "<span style='font-size: small;'>"
					+ "<strong>TeacherMatch Client Services."
					+ "</strong></span></p>";

			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style=" + mailCssFontSize + ">");
			sb.append(MESSAGE);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style=" + mailCssFontSize + ">");
			sb.append("Sincerely<br/>TeacherMatch Administrator<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style=" + mailFooterCssFontSize + ">");
			sb
					.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	
	public static String emailToSchoolAdminsForAddSchool(HttpServletRequest request,UserMaster userMaster,DistrictMaster districtMaster,String djoName)
	{
		if(locale.equals("fr"))
			return emailToSchoolAdminsForAddSchoolFR( request, userMaster, districtMaster, djoName);

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear "+getUserName(userMaster)+",<br><br>");
			sb.append("Dear school admin(s),<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your school has been added to the "+djoName+" job order. You will now have access to the candidate grid reflecting all applicants for this position.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(districtMaster!=null && districtMaster.getDistrictId().equals(7800040))
				sb.append("If you have questions, please contact your "+districtMaster.getDistrictName()+" Talent Acquisition Team.");
			else
				sb.append("If you have questions, please contact your "+districtMaster.getDistrictName()+" HR lead.");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Best<br/>TeacherMatch Client Services<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String emailToSchoolAdminsForDeleteSchool(HttpServletRequest request,UserMaster userMaster,DistrictMaster districtMaster,String djoName)
	{
		if(locale.equals("fr"))
			return emailToSchoolAdminsForDeleteSchoolFR( request, userMaster, districtMaster, djoName);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear School admin(s),<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your school has been removed from the "+djoName+" job order. You will not have access to the candidate grid associated with this job order.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(districtMaster!=null && districtMaster.getDistrictId().equals(7800040))
				sb.append("If you have questions, please contact your "+districtMaster.getDistrictName()+" Talent Acquisition Team.");
			else
				sb.append("If you have questions, please contact your "+districtMaster.getDistrictName()+" HR lead.");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Best<br/>TeacherMatch Client Services<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	// Email to all users for share candidate
	public static String emailToAllUsersToShareCandidate(HttpServletRequest request,String usrName,String  usrNameshared,String teacherName,String folderName)
	{
		if(locale.equals("fr"))
			return emailToAllUsersToShareCandidateFR( request, usrName,  usrNameshared, teacherName, folderName);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+usrNameshared+",<br><br>");
			
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			
			/*if(teacherName.contains(","))
				sb.append(usrName+" would like to share "+teacherName+" TeacherMatch candidate profile with you. To view this candidate�s profile, please do the following:<br>");
			else
				sb.append(usrName+" would like to share "+teacherName+" TeacherMatch candidate profile with you. To view this candidate�s profile, please do the following:<br>");*/
			
			
			
			
			sb.append(usrName+" would like to share "+teacherName+" TeacherMatch candidate profile(s) with you. To view this candidate�s profile, please do the following:<br>");
			sb.append("<ol><li>Log into the TeacherMatch platform via your Employee Portal.</li><li>Click the drop down arrow next to your name in the top right corner of the screen.</li><li> Select \"My Folders\".</li><li>When the Manage Received/Saved Candidates screen opens, click the \"+\" next to \"My Folders.\" Shared candidates will appear in the \"Received Candidates\" folder.</li>");
		
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Note: Candidate(s) coming from \""+folderName+"\" folder. <br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Best<br/>TeacherMatch Client Services<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need additional assistance, please feel free to contact TeacherMatch Client Services at 855-980-0545 or <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	
	public static String offerAcceptMailToKeyContact(TeacherDetail teacherDetail,String schoolName,String jobTitle)
	{
		if(locale.equals("fr"))
			return offerAcceptMailToKeyContactFR( teacherDetail, schoolName, jobTitle);

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The following employee has accepted a conditional offer at "+schoolName+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Employee Name : "+getTeacherName(teacherDetail)+"<br>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date of birth : "+Utility.convertDateAndTimeToUSformatOnlyDate(teacherDetail.getFgtPwdDateTime()));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Employee Address : "+teacherDetail.getAuthenticationCode());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Employee Phone : "+teacherDetail.getPhoneNumber());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Position Number : "+jobTitle);
			sb.append("</td>");
			sb.append("</tr>");


			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	/*		Implemented By Hanzala Email for FP Reprint and Pending
	 * 		Dated : 24-07-2014
	 * */
	public static String getFPReprintMail(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getFPReprintMailFR( request, teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+":<br/><br/> Thank you for your interest in working for Miami-Dade County Public Schools. Please be advised that your fingerprints were rejected by the Federal Bureau of Investigation (FBI) for illegibility or other technical issues.  In order for the employment/volunteer/field experience/contracted services process to continue, you must return to be refingerprinted within 15 working days to the Fingerprint Office.  There is no additional charge for the refingerprinting service.  You must present a valid picture identification such as a driver�s license, passport, or state identification card in order to be refingerprinted.  If you need additional information, please call the Miami-Dade Schools Police Fingerprinting Unit at 305-995-7472.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Office of Human Capital Management<br/>Miami-Dade County Public Schools<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			//System.out.println(sb);
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getFPPendingMail(HttpServletRequest request,TeacherDetail teacherDetail,TeacherPersonalInfo teacherPersonalInfo)
	{
		if(locale.equals("fr"))
			return getFPPendingMailFR( request, teacherDetail, teacherPersonalInfo);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			String city 		= 	"";
			String state 		= 	"";
			String zip 			= 	"";
			String address1		=	"";
			String address2		=	"";
			String address		=	"";
			String firstName	=	"";
			String addressline2	=	"";
			if(teacherPersonalInfo.getCityId() != null){
				city	=	teacherPersonalInfo.getCityId().getCityName();
				state	=	teacherPersonalInfo.getStateId().getStateName();
				zip		=	teacherPersonalInfo.getCityId().getZipCode();	
			}
			if(teacherPersonalInfo != null){
				address1	=	teacherPersonalInfo.getAddressLine1();
				address2	=	teacherPersonalInfo.getAddressLine2();
				address		=	address1+", "+address2;
				firstName	=	teacherDetail.getFirstName();
				addressline2	=	city+","+state+","+zip;
			}
			
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			 
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+":<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Fingerprint Office has received the results of your fingerprint analysis from the Florida Department of Law Enforcement and the Federal Bureau of Investigation reflecting an incident that led to court action.  Please be advised that in order for the employment/volunteer/field experience/contracted services process to continue, you must provide the following documents:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul>");
			sb.append("<li>Incident Report (initial police investigative report) or Arrest Report/Affidavit (criminal report affidavit)</br></li>");
			sb.append("<li>Information or Indictment (formal charges filed by the prosecutor with the court)</li>");
			sb.append("<li>Court Adjudication (the court�s disposition of your case)</li>");
			sb.append("<li>Documentation of successful completion of probation or pre-trial intervention and payment of fine(s), if applicable</li>");
			sb.append("<li>A notarized statement written and signed by you explaining the circumstances of arrest(s)</li>");
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("All photocopies of court documents or certification from the Clerk of the Court must be original, certified copies.  You should contact the Clerk of the Court in the city or county where the case was disposed.  Documents must be submitted to Miami-Dade within 15 working days from the date of this letter to the address: Miami-Dade Schools Police Fingerprinting Unit, 1450 N.E. 2nd Avenue,  Suite 110,  Miami, FL 33132<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sigilenda Miles<br/>Executive Director of Fingerprinting<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
			
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();
	}
	
	public static String PNRUnHireMail(JobForTeacher jobForTeacher,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return PNRUnHireMailFR( jobForTeacher, userMaster);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			//String sCandidateName="";
			/*if(jobForTeacher.getTeacherId().getFirstName()!=null && !jobForTeacher.getTeacherId().getFirstName().equalsIgnoreCase(""))
				sCandidateName=jobForTeacher.getTeacherId().getFirstName();
			
			if(jobForTeacher.getTeacherId().getLastName()!=null && !jobForTeacher.getTeacherId().getLastName().equalsIgnoreCase(""))
				sCandidateName=sCandidateName+" "+jobForTeacher.getTeacherId().getLastName();*/
			
			//String sSAName="";
			
			//sSAName=getUserName(userMaster);
			
			/*if(userMaster!=null && userMaster.getFirstName()!=null && !userMaster.getFirstName().equalsIgnoreCase(""))
			{
				sSAName=getUserName(userMaster);
			}*/
				

				sb.append("<table>");
				sb.append("<table>");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+getUserName(userMaster)+",<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("This email is to inform you that "+getTeacherName(jobForTeacher.getTeacherId())+" was hired into the position of "+jobForTeacher.getJobId().getJobTitle()+"  in error on  Date:  "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"<br/><br/>");
				sb.append("</td>");
				
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Please do not allow this candidate to report to work at your location. If you have additional questions, please contact Mr. Dennis Carmona, Executive Director, Personnel Operations and Records at 305-995-7186.");
				sb.append("</td>");
				sb.append("</tr>");
				
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getDTFailedMail(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getDTFailedMailFR( request, teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The analysis of your pre-employment drug screening has been confirmed positive.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("School Board Policy Drug-Free Workplace states:");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Applicants will be informed in advance of the requirement of a <br>negative drug screen as condition of employment.  Applicants <br>testing positive will not be eligible for employment by Miami-Dade<br> County Public Schools for three (3) years from the date of the test.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("As a result, your name has been removed from the employment eligibility list for the period stipulated by Board Policy.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Office of Professional Standards<br/>Miami-Dade County Public Schools<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	/*
	 * 		Implemented By	:	Hanzala Subhani
	 * 		Dated			:	22 August	2014
	 * 		Purpose			:	Send Mail After send to SAP. This mail is sent by URL.
	 * 
	 */
	
	public static String PNRSentToSap(JobForTeacher jobForTeacher,String principalName)
	{
		if(locale.equals("fr"))
			return PNRSentToSapFR( jobForTeacher, principalName);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			//String sCandidateName	=	"";
			String positionName		=	"";
			/*if(jobForTeacher.getTeacherId().getFirstName()!=null && !jobForTeacher.getTeacherId().getFirstName().equalsIgnoreCase(""))
				sCandidateName=jobForTeacher.getTeacherId().getFirstName();

			if(jobForTeacher.getTeacherId().getLastName()!=null && !jobForTeacher.getTeacherId().getLastName().equalsIgnoreCase(""))
				sCandidateName=sCandidateName+" "+jobForTeacher.getTeacherId().getLastName();*/

			if(jobForTeacher.getJobId().getJobTitle()!=null && !jobForTeacher.getJobId().getJobTitle().equalsIgnoreCase(""))
				positionName=jobForTeacher.getJobId().getJobTitle();
			
			sb.append("<table>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");

			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+principalName+",<br/><br/>");
			sb.append("</td>");

			sb.append("</tr>");
			
			sb.append("<tr>");

			sb.append("<td style="+mailCssFontSize+">");
			
			sb.append("Please be advised that "+getTeacherName(jobForTeacher.getTeacherId())+" (<a href='mailto:'"+jobForTeacher.getTeacherId().getEmailAddress()+"> "+jobForTeacher.getTeacherId().getEmailAddress()+" </a>) has completed all necessary pre-employment requirements for the position of "+positionName+". He/she may report to your location on the first working day after the date printed at the top of this email.");

			sb.append("</td>");

			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append(" If you have any questions, please contact <a href='mailto:personnel@dadeschools.net'>personnel@dadeschools.net</a>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getMailTextInVacancy(HttpServletRequest request,String newRecords,String delRecords,String message)
	{
		if(locale.equals("fr"))
			return getMailTextInVacancyFR( request, newRecords, delRecords, message);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi All,");
			sb.append("</td>");
			sb.append("<tr><td>&nbsp;</td></tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(message);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(newRecords);
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(delRecords);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr><td>&nbsp;</td></tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely<br>TeacherMatch Client Services Team  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" getRegistrationMail :: "+sb.toString());
	
		return sb.toString();
	}

	public static String getApproveJObMailText(HttpServletRequest request, DistrictKeyContact districtKeyContact,String externalURL,String jobTitle)
	{
		if(locale.equals("fr"))
			return getApproveJObMailTextFR( request,  districtKeyContact, externalURL, jobTitle);
			
		StringBuffer sb = new StringBuffer();
		String emailContent = "";
		try 
		{	
			
			UserMaster userMaster = districtKeyContact.getUserMaster();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+"<br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>You need to approve the job \""+jobTitle+"\"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			if(externalURL!=null)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br>To approve this job click the below link.<br/><br/><a href='"+externalURL+"'>"+externalURL+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		emailContent = sb.toString();
		System.out.println(" "+emailContent);
		
		return emailContent;

	}
	
	public static String getApproveJObMailTextJeffcoSpecific(HttpServletRequest request,UserMaster userMaster,String externalURL,String jobTitle)
	{
		StringBuffer sb = new StringBuffer();
		String emailContent = "";
		try 
		{	
			
			//UserMaster userMaster = userMaster2;
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+userMaster.getFirstName()+" "+userMaster.getLastName()+"<br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>You need to approve the job \""+jobTitle+"\"");
			if(userMaster.getSchoolId()!=null)
			sb.append(" \""+userMaster.getSchoolId().getSchoolName()+"\"</br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			if(externalURL!=null)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br>To approve this job click the below link.<br/><br/><a href='"+externalURL+"'>"+externalURL+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		emailContent = sb.toString();
		System.out.println(" "+emailContent);
		
		return emailContent;

	}
	
	//Mail for tfa changed to teachers
	public static String emailToTeacherForTFAChanged(HttpServletRequest request,TeacherDetail teacherDetail,UserMaster userMaster)
	{
		if(locale.equals("fr"))
			return emailToTeacherForTFAChangedFR( request, teacherDetail, userMaster);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear "+getUserName(userMaster)+",<br><br>");
			sb.append("Dear "+getTeacherName(teacherDetail)+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("As part of your application, you identified as having participated in Teach For America. We have confirmed that you did not participate in TFA, and we have adjusted your portfolio to reflect this change. If you have evidence that you were in TFA, please contact the HR Department of the Noble Network of Charters.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely,</br>"+getUserName(userMaster));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");
			

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	
	//Mail for VVI to teachers
/*	public static String emailToTeacherForVVI(HttpServletRequest request,TeacherDetail teacherDetail,UserMaster userMaster,JobOrder joborder)
	{
		if(locale.equals("fr"))
			return emailToTeacherForVVIFR( request, teacherDetail, userMaster, joborder);
		
		StringBuffer sb = new StringBuffer();
		
		String quesSetName = "";
		String maxMarks ="";
		String linkStatus="";
		String timePerQues="";
		String ExpInDays="";
		
		try 
		{
			try
			{
				if(joborder.getOfferVirtualVideoInterview())
				{
					//For QuesSetName
					if(joborder.getI4QuestionSets()!=null && !joborder.getI4QuestionSets().getQuestionSetText().equals(""))
						quesSetName = joborder.getI4QuestionSets().getQuestionSetText();
						
					//For Max Marks
					if(joborder.getMaxScoreForVVI()!=null && !joborder.getMaxScoreForVVI().equals(""))
						maxMarks = joborder.getMaxScoreForVVI().toString();			
					
					//For autoLink
					if(joborder.getSendAutoVVILink())
					{
						if(joborder.getStatusMaster()!=null)
							linkStatus = joborder.getStatusMaster().getStatus();
						else if(joborder.getSecondaryStatus()!=null)
							linkStatus = joborder.getSecondaryStatus().getStatus();
					}
										
					//For Time Per Question
					if(joborder.getTimeAllowedPerQuestion()!=null && !joborder.getTimeAllowedPerQuestion().equals(""))
						timePerQues = joborder.getTimeAllowedPerQuestion().toString();
					
					//VVI Expires in days
					if(joborder.getVVIExpiresInDays()!=null && !joborder.getVVIExpiresInDays().equals(""))
						ExpInDays = joborder.getVVIExpiresInDays().toString();
										
				} else if(joborder.getJobCategoryMaster().getOfferVirtualVideoInterview())
				{
					//For QuesSetName
					if(joborder.getJobCategoryMaster().getI4QuestionSets()!=null && !joborder.getJobCategoryMaster().getI4QuestionSets().getQuestionSetText().equals(""))
							quesSetName = joborder.getJobCategoryMaster().getI4QuestionSets().getQuestionSetText();			
					
					//For Max Marks
					if(joborder.getJobCategoryMaster().getMaxScoreForVVI()!=null && !joborder.getJobCategoryMaster().getMaxScoreForVVI().equals(""))
						maxMarks = joborder.getJobCategoryMaster().getMaxScoreForVVI().toString();				
					
					//For autoLink
					if(joborder.getJobCategoryMaster().getSendAutoVVILink())
					{
						if(joborder.getJobCategoryMaster().getStatusMaster()!=null)
								linkStatus = joborder.getJobCategoryMaster().getStatusMaster().getStatus();
							else if(joborder.getJobCategoryMaster().getSecondaryStatus()!=null)
								linkStatus = joborder.getJobCategoryMaster().getSecondaryStatus().getSecondaryStatusName();
					}
					
					//For Time Per Question
					if(joborder.getJobCategoryMaster().getTimeAllowedPerQuestion()!=null && !joborder.getJobCategoryMaster().getTimeAllowedPerQuestion().equals(""))
							timePerQues = joborder.getJobCategoryMaster().getTimeAllowedPerQuestion().toString();			
					
					//VVI Expires in days
					if(joborder.getJobCategoryMaster().getVVIExpiresInDays()!=null && !joborder.getJobCategoryMaster().getVVIExpiresInDays().equals(""))
						ExpInDays = joborder.getJobCategoryMaster().getVVIExpiresInDays().toString();
					
				}else if(joborder.getDistrictMaster().getOfferVirtualVideoInterview())
				{
					//For QuesSetName
					if(joborder.getDistrictMaster().getI4QuestionSets()!=null && !joborder.getDistrictMaster().getI4QuestionSets().getQuestionSetText().equals(""))
							quesSetName = joborder.getDistrictMaster().getI4QuestionSets().getQuestionSetText();
					
					//For Max Marks
					if(joborder.getDistrictMaster().getMaxScoreForVVI()!=null && !joborder.getDistrictMaster().getMaxScoreForVVI().equals(""))
							maxMarks = joborder.getDistrictMaster().getMaxScoreForVVI().toString();
					
					//For autoLink
					if(joborder.getDistrictMaster().getSendAutoVVILink())
					{
								if(joborder.getDistrictMaster().getStatusMasterForVVI()!=null)
									linkStatus = joborder.getDistrictMaster().getStatusMasterForVVI().getStatus();
								else if(joborder.getDistrictMaster().getSecondaryStatusForVVI()!=null)
									linkStatus = joborder.getDistrictMaster().getSecondaryStatusForVVI().getSecondaryStatusName();
					}
					
					//For Time Per Question
					if(joborder.getDistrictMaster().getTimeAllowedPerQuestion()!=null && !joborder.getDistrictMaster().getTimeAllowedPerQuestion().equals(""))
							timePerQues = joborder.getDistrictMaster().getTimeAllowedPerQuestion().toString();
					
					//VVI Expires in days
					if(joborder.getDistrictMaster().getVVIExpiresInDays()!=null && !joborder.getDistrictMaster().getVVIExpiresInDays().equals(""))
						ExpInDays = joborder.getDistrictMaster().getVVIExpiresInDays().toString();
				}
				else {
					// no VVI is setup at any level
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear "+getUserName(userMaster)+",<br><br>");
			sb.append("Dear "+getTeacherName(teacherDetail)+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Virtual video Interview :: ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Question Set Name :: "+quesSetName);
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Max Score :: "+maxMarks);
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Link Status :: "+linkStatus);
			sb.append("</td>");
			sb.append("</tr>");
			System.out.println(" timePerQues :: "+timePerQues+" ExpInDays :: "+ExpInDays);
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Time Allowed Per Question :: "+timePerQues);
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Video Video interview Expires In Days :: "+ExpInDays);
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely,</br>"+getUserName(userMaster));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");
			

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
*/	
	public static String ReferenceCheckMail(TeacherElectronicReferences teacherElectronicReferences, JobOrder jobOrder, TeacherDetail teacherDetail, DistrictMaster districtMaster, String mainURLEncode)
	{
		if(locale.equals("fr"))
			return ReferenceCheckMailFR( teacherElectronicReferences,  jobOrder,  teacherDetail,  districtMaster,  mainURLEncode);
			
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request;// = context.getHttpServletRequest();
		
		if(WebContextFactory.get()!=null)
			request = WebContextFactory.get().getHttpServletRequest();
		else
			request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		
		String firstName 			= 	"";
		String lastName				=	"";
		String fullName				=	"";
		String candidateFirstName	=	"";
		String candidateLastName	=	"";
		String candidateFullName	=	"";
		String jobTitle				=	"";
		String dhName = "";
		//HeadQuarterMaster headQuarterMaster = null;
		if(jobOrder != null){
			jobTitle = jobOrder.getJobTitle();
		}
		
		firstName	=	teacherElectronicReferences.getFirstName() == null ? "" : teacherElectronicReferences.getFirstName();
		lastName	=	teacherElectronicReferences.getLastName() == null ? "" : teacherElectronicReferences.getLastName();
		fullName 	= 	firstName+" "+lastName;		
		fullName	=	fullName.replace("'","\\'");
		
		candidateFirstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		candidateLastName	=	teacherDetail.getLastName()	==	null ? "" : teacherDetail.getLastName();
		
		candidateFullName	=	candidateFirstName+" "+candidateLastName;
		candidateFullName	=	candidateFullName.replace("'", "\\'");
		if(districtMaster!=null)
			dhName = districtMaster.getDistrictName();
		else if(jobOrder.getHeadQuarterMaster()!=null)
			dhName = jobOrder.getBranchMaster().getBranchName();
		
		StringBuffer sb = new StringBuffer();
		try {

				String basePath	=	Utility.getBaseURL(request);
				sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+fullName+",<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(candidateFullName+ " has listed you as a reference as part of a recent job search with "+dhName+" for "+jobTitle+". Your feedback is being requested by the district to determine continued candidacy for "+candidateFullName+".<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("To complete the reference form, please click on the link below. Please note that a selection for each question is required to complete the reference form. Only one form completion is allowed per reference request.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("To begin:<BR><a href="+mainURLEncode+">");
				sb.append(mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<BR>We appreciate you assisting "+dhName+" as we work to bring the highest quality candidates to the district.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Best,");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(dhName);
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Staffing Team");
				sb.append("</td>");
				sb.append("</tr>");

			sb.append("</table>");
		}

		catch(Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	public static String sendMailLinkTemplate(HttpServletRequest request ,JobOrder jobOrder, TeacherDetail teacherDetail, String mainURLEncode,DistrictMaster districtMaster,int template,boolean principalCategory)
	{
		if(locale.equals("fr"))
			return sendMailLinkTemplateFR( request , jobOrder,  teacherDetail,  mainURLEncode, districtMaster, template, principalCategory);
			
		System.out.println(":::::::::::::::::sendMailLinkTemplate =:::::::::::::");
		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		StringBuffer sb = new StringBuffer();
		if(principalCategory){
		if(template==1){
			try {
				sb.append("<table>");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				
					sb.append("<br/>We are pleased to move you forward to the online activity for principalship with "+ districtMaster.getDistrictName()+". The deadline to submit the activity is Monday, March 2.<br/><br/>");
				
				sb.append("</td>");
				sb.append("</tr>");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("When you are ready to complete the online activity, please <a href="+url+mainURLEncode+">click here</a>. This is a 2.5 hour timed activity which you must finish in one sitting, so you should plan accordingly. Ensure you have a secure internet connection before your click on the link to start the activity. You may submit before the 2.5 hours are up, and if you have not completed the entire activity within the 2.5 hours, you must still submit it. There will be no extensions given.<br/></br>Please note, when you are in the portal, you cannot go back. If you click on the \"back\" button in your browser, you will be timed out of the activity. So, as you progress through the activity, please be certain you are completely finished with a question before moving on to the next one. There are six questions/activities to be completed within the timeframe, so pace yourself accordingly.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				String districtContactMail=Utility.getValueOfPropByKey("onlineActivityEmail");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If an emergency arises, please contact <a href='mailto:"+districtContactMail+"'>"+districtContactMail+"</a>. Do not reply to this e-mail.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Best regards,<br/>The Office of Talent<br/>"+districtMaster.getDistrictName());

				sb.append("</td>");
				sb.append("</tr>");
				/*sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+url+mainURLEncode+">");
				sb.append(url+mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}else{
			try {
				sb.append("<table>");
			
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+firstName+",");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");				
					sb.append("<br/>We are pleased to move you forward to the online activity for principalship with "+districtMaster.getDisplayName()+". The deadline to submit the activity is Monday, April 13.<br/><br/>");				
				sb.append("</td>");
				sb.append("</tr>");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("When you are ready to complete the online activity, please click <a href="+url+mainURLEncode+">"+url+mainURLEncode+"</a>. This is a 2.5 hour timed activity which you must finish in one sitting, so you should plan accordingly. Ensure you have a secure internet connection before your click on the link to start the activity. You may submit before the 2.5 hours are up, and if you have not completed the entire activity within the 2.5 hours, you must still submit it. There will be no extensions given.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Please note, when you are in the portal, you cannot go back. If you click on the \"back\" button in your browser, you will be timed out of the activity. So, as you progress through the activity, please be certain you are completely finished with a question before moving on to the next one. There are six questions/activities to be completed within the timeframe, so pace yourself accordingly.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				String districtContactMail=Utility.getValueOfPropByKey("onlineActivityEmail");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("if an emergency arises, please contact "+districtContactMail+". Do not reply to this e-mail.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Best regards,<br/>The Office of Talent<br/>"+districtMaster.getDistrictName());
				
				sb.append("</td>");
				sb.append("</tr>");
			
				/*sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+url+mainURLEncode+">");
				sb.append(url+mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				sb.append("</table>");
			}
	
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		}else{
			sb.append("<table>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+firstName+" "+lastName+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");				
				sb.append("<br/>We are pleased to move you forward to the online activity for your teacher application with The School District of Philadelphia.<br/><br/>");				
			sb.append("</td>");
			sb.append("</tr>");

			String url=Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("When you are ready to complete the online activity, please <a href="+url+mainURLEncode+">click here</a>. This is not a timed activity, though we do encourage that you complete and submit as soon as possible so you can be  eligible for Site Selection. Ensure you have a secure internet connection before you click on the link to start the activity.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If an emergency arises, please contact <a href='mailto:recruitment@philasd.org'>recruitment@philasd.org.</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best regards,<br/>The Office of Talent<br/>"+districtMaster.getDistrictName());			
			sb.append("</td>");
			sb.append("</tr>");
			//The School District of Philadelphia
		}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String resetOnlineActEmail(HttpServletRequest request,TeacherDetail teacherDetail, String mainURLEncode,DistrictMaster districtMaster,int template,boolean principalCategory)
	{
		if(locale.equals("fr"))
			return resetOnlineActEmailFR( request, teacherDetail,  mainURLEncode, districtMaster, template, principalCategory);
			
		System.out.println("::::::::::::resetOnlineActEmail::::");
		
		String teacherName 	= 	"";
		teacherName	=	getTeacherName(teacherDetail);
		StringBuffer sb = new StringBuffer();
		if(principalCategory){
		if(template==1){
			try {
				sb.append("<table>");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+teacherName+",");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				
					sb.append("<br/>Your online activity for principalship with "+ districtMaster.getDistrictName()+" has been reset. The deadline to submit the activity is Monday, March 2.<br/><br/>");
				
				sb.append("</td>");
				sb.append("</tr>");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("When you are ready to complete the online activity, please <a href="+url+mainURLEncode+">click here</a>. This is a two-hour timed activity which you must finish in one sitting, so you should plan accordingly. Ensure you have a secure internet connection before your click on the link to start the activity. You may submit before the two hours are up, and if you have not completed the entire activity within the two hours, you must still submit it. There will be no extensions given.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				String districtContactMail=Utility.getValueOfPropByKey("onlineActivityEmail");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If an emergency arises, please contact <a href='mailto:"+districtContactMail+"'>"+districtContactMail+"</a>. Do not reply to this e-mail.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Best regards,<br/>The Office of Talent<br/>"+districtMaster.getDistrictName());

				sb.append("</td>");
				sb.append("</tr>");
				/*sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+url+mainURLEncode+">");
				sb.append(url+mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}else{
			try {
				sb.append("<table>");
			
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+teacherName+",");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");				
					sb.append("<br/>Your online activity for principalship with "+ districtMaster.getDistrictName()+" has been reset. The deadline to submit the activity is Monday, April 13.<br/><br/>");				
				sb.append("</td>");
				sb.append("</tr>");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("When you are ready to complete the online activity, please click <a href="+url+mainURLEncode+">"+url+mainURLEncode+"</a>. This is a two-hour timed activity which you must finish in one sitting, so you should plan accordingly. Ensure you have a secure internet connection before your click on the link to start the activity. You may submit before the two hours are up, and if you have not completed the entire activity within the two hours, you must still submit it. There will be no extensions given.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				String districtContactMail=Utility.getValueOfPropByKey("onlineActivityEmail");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If an emergency arises, please contact "+districtContactMail+". Do not reply to this e-mail.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Best regards,<br/>The Office of Talent<br/>"+districtMaster.getDistrictName());
				
				sb.append("</td>");
				sb.append("</tr>");
			
				/*sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+url+mainURLEncode+">");
				sb.append(url+mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				sb.append("</table>");
			}
	
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		}else{
			sb.append("<table>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+teacherName+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");	
				sb.append("<br/>Your online activity for your teacher application with "+ districtMaster.getDistrictName()+" has been reset.<br/><br/>");				
			sb.append("</td>");
			sb.append("</tr>");

			String url=Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("When you are ready to complete the online activity, please <a href="+url+mainURLEncode+">click here</a>. This is not a timed activity, though we do encourage that you complete and submit as soon as possible so you can be  eligible for Site Selection. Ensure you have a secure internet connection before you click on the link to start the activity.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If an emergency arises, please contact <a href='mailto:recruitment@philasd.org'>recruitment@philasd.org.</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best regards,<br/>The Office of Talent<br/>"+districtMaster.getDistrictName());			
			sb.append("</td>");
			sb.append("</tr>");
			//The School District of Philadelphia
		}
		
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String mailSendToTeacherForSchoolSelection(HttpServletRequest request ,JobOrder jobOrder, TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return mailSendToTeacherForSchoolSelectionFR( request , jobOrder,  teacherDetail);
			
	      StringBuffer sb = new StringBuffer();
          try {

        	sb.append("<table>");
        	  

    	  	sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Greetings,");
			sb.append("</td>");
			sb.append("</tr>");
				
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Thank you for your interest in applying to lead a school for the 2015-2016 school year.  We are pleased to announce that you've been successful in our selection process and will be added to our principal talent pool! ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>You may now apply for school-specific vacancies.  You are required to upload a statement of interest via TeacherMatch for each school to which you wish to apply (directions below). The statement of interest should be no more than a single page, and as possible, please save and upload it as a PDF. The statement of interest should address:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");

            sb.append("<ul>");

            sb.append("<li>Why you want to lead the school</li>");

            sb.append("<li>How your skills and experience will enable you to lead the school successfully</li>");

            sb.append("<li>What you hope to accomplish as the school leader</li>");          

            sb.append("</ul>");
            
            sb.append("</td>");
			sb.append("</tr>");
			
		
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Each statement of interest should be specific to the school for which you are applying.  Visit our <a target='_blank' href='http://webgui.phila.k12.pa.us/offices/e/ee/opportunities/principals-and-assistant-principals'>website</a> for more information on each opening.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>All statements of interest are due by Friday, March 26 close of business. From that point, Assistant Superintendents will review the statements of interest for their schools and reach out individually to schedule interviews, if they are interested in your candidacy.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Please read the instructions below carefully, and if you need assistance, contact <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a>.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");

            sb.append("<ul>");

            sb.append("<li>Log back into your TeacherMatch account</li>");

            sb.append("<li>Click on your name in the upper right-hand corner of the screen</li>");

            sb.append("<li>A drop-down menu will appear</li>");

            sb.append("<ul type='circle'>");

            sb.append("<li>Choose \"School Selection\"</li>");

            sb.append("</ul>");

            sb.append("<li>Then, the \"Principal\" job will appear</li>");

            sb.append("<ul type='circle'>");

            sb.append("<li>On the right-hand side, click on \"Add/Edit Schools\"</li>");

            sb.append("</ul>");

            sb.append("<li>At the top of the grid, select \"Add School\"</li>");

            sb.append("<li>From there, select the schools you are interested in, and upload your statement of interest (see below for instructions)</li>");

            sb.append("<li>At any time, you may go back into \"School Selection\" to add or delete your school choices</li>");

            sb.append("<ul type='circle'>");

            sb.append("<li>This is important given that vacancies will be loaded on a rolling basis</li>");

            sb.append("</ul>");

            sb.append("</ul>");

        	sb.append("</td>");
			sb.append("</tr>");
    			
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Please do not reply to this e-mail. If you need assistance with the School Selection process, e-mail <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a>. For all other inquiries, please contact Stephanie Jerome at <a href='mailto:sejerome@philasd.org'>sejerome@philasd.org</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Again, congratulations!  We look forward to future correspondence.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Best regards,<br/>");
			sb.append("The Office of Talent<br/>");
			sb.append("The School District of Philadelphia<br/>");
			sb.append("</td>");
			sb.append("</tr>");

            sb.append("</table>");
          }catch(Exception e) {

                e.printStackTrace();

          }
    System.out.println("sb::::::::::"+sb.toString());
    return sb.toString();
	}
	
	public static String getNewPanelComonAttendeeMailText(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList,String cNContent)
	{
		if(locale.equals("fr"))
			return getNewPanelComonAttendeeMailTextFR( request, panelSchedule, panelAttendees, panelAttendeesList, cNContent);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			String districtName = userMaster.getDistrictId().getDistrictName();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hi "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");		
				sb.append("You have been invited by "+districtName+" to participate in a panel for candidate review. Additional details are provided below:<br/><br/>");
		
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Job Applied For:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Panel Date/Time:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Panel Location:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Comments:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			try{
				if(panelSchedule.getPanelDescription()!=null){
					sb.append(panelSchedule.getPanelDescription());
				}else{
					sb.append("");
				}
			}catch(Exception e){sb.append("");}
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Attendees:");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+"><b>Candidates</b>:</td>");			
			sb.append("</tr>");
			sb.append(cNContent);
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
		
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sincerely<br/>The TeacherMatch Team on behalf of "+districtName+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			//only for marysville ==   5304860 =====//
			if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(5304860)){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("The issues of confidentiality and ethics are extremely important when taking part in the selection of new staff.  Each member of the team must make the commitment to have the process remain strictly confidential.  The following issues are not for public dissemination:");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>Documents developed</li><li>Statements made by the team or candidate</li><li>Deliberations</li><li>Impressions</li><li>Opinions</li><li>Ranking</li><li>Evaluations</li><li>Candidate materials</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("It is our obligation to protect the rights and preserve the self-respect of the candidate.");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("The only individuals authorized to give feedback information during the post interview conference are the administrator or Human Resources designee upon the request of the candidate. " +
						"Only certain data will be shared and then only to the specific candidate.  You may be pressured by coworkers and/or community members to share information about candidates, the process, questions asked of candidate, etc.  You may not respond to these questions other than to affirm that we are proceeding as planned and hope to be finished soon." +
						" If someone is insistent, refer him/her to Human Resources.");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br>Please remember, this is a professional process. <b>Confidentiality is an obligation of the team, not the candidate.</b><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a>. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			//End only for marysville ==   5304860 =====//
			else{			
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact your Admin at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String mailSendToTeacherForAssessmentInvite(HttpServletRequest request ,JobOrder jobOrder, TeacherDetail teacherDetail,ExaminationCodeDetails examinationCodeDetails,String icalFile)
	{
		if(locale.equals("fr"))
			return mailSendToTeacherForAssessmentInviteFR( request , jobOrder,  teacherDetail, examinationCodeDetails, icalFile);
			
		System.out.println("::::::::::::::mailSendToTeacherForAssessmentInvite::::::::::::");	
		String firstName 	= 	"";
		String lastName		=	"";
		String city = "";
		String state = "";
		String zipcode = "";
		String address = "";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		city = jobOrder.getDistrictMaster().getCityName()==null?"":jobOrder.getDistrictMaster().getCityName();
		state = jobOrder.getDistrictMaster().getStateId().getStateName()==null?"":jobOrder.getDistrictMaster().getStateId().getStateName();
		zipcode = jobOrder.getDistrictMaster().getZipCode()==null?"":jobOrder.getDistrictMaster().getZipCode();
		address = jobOrder.getDistrictMaster().getAddress()==null?"":jobOrder.getDistrictMaster().getAddress();
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"50%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear&nbsp;"+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+"><br/>Ical File is ::"+icalFile+"</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+"><br/>Thanks for showing the interest in "+jobOrder.getJobTitle()+". As a next step, you need to take an online exam. Details of the online exam is as following:</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Examination Code:&nbsp;"+examinationCodeDetails.getExaminationCode()+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">Address:&nbsp;"+city+" "+state+" "+ address +" "+zipcode+" <br/></td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">Exam Date & Time:&nbsp;"+Utility.convertDateAndTimeFormatForEpiAndJsi(examinationCodeDetails.getExamDate())+"<br/></td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Regards,<br/>");
				sb.append(jobOrder.getDistrictMaster().getDistrictName());
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String mailTextForEvents(String messageText)
	{
		if(locale.equals("fr"))
			return mailTextForEventsFR( messageText);
		
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(messageText);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			/*sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Please do not reply to this email. If you need to respond, please send your reply to <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");*/
			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String sendVVIInviteInterviewEmail(String[] emailDetails)
	{
		if(locale.equals("fr"))
			return sendVVIInviteInterviewEmailFR(emailDetails);
		
		StringBuffer sb = new StringBuffer();
			try {
				
				String uanme = "";
				String pass = "";
				
				if(emailDetails[5]!=null)
					uanme = emailDetails[5];
				if(emailDetails[6]!=null)
					pass = emailDetails[6];
				
				
				sb.append("<table width=\"50%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+emailDetails[0]+",");
				sb.append("</td>");
				sb.append("</tr>");

			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Thank you for your recent application with "+emailDetails[1]+". As part of the application process, we would like you to complete an online video interview.<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Please record this video interview by following the instructions below. We look forward to receiving your interview video.<br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Thank you!<br/><hr>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>The interview consists of "+emailDetails[2]+" questions. Each response can be no more than "+emailDetails[3]+" seconds in length so the maximum time required for the interview is "+emailDetails[4]+" minutes.<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				//Sunday, January 18th, 2015 at 10:28 AM EST
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>You can complete your interview by using a PC/Laptop with a webcam and microphone. Please complete the interview by "+emailDetails[7]+" at the latest.<br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Step 1: Close out all running programs on your computer except your web browser and email program. Be sure to close any video or audio programs including Skype.<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Step 2: Go to the login page: <a href='https://teachermatch.interview4.com/test_taker_login.php'>teachermatch.interview4.com/test_taker_login.php</a><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Step 3:<br/><span class='left5'>&nbsp;&nbsp;Enter your username:&nbsp;"+uanme+"</span><br/><span class='left5'>&nbsp;&nbsp;Enter your password:&nbsp;"+pass+"</span><br/><span class='left5'>&nbsp;&nbsp;Then, select 'Log In'</span><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Step 4: Follow the on-screen instructions.<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/><b>Helpful Tips</b><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ol>");
				sb.append("<li>Dress professionally just as you would in a live interview.</br></li>");
				sb.append("<li>In addition to the time required to complete the interview, you will need 10 - 15 minutes for setup and practice. Make sure you have enough time to complete your Interview in one sitting.</li>");
				sb.append("<li>Make sure the room where you take your Interview is well lit.</li>");
				sb.append("<li>During the video interview setup make sure you are correctly positioned. When reviewing your test video you should see from the top of your head to the top of your shoulders. If your face fills the frame you are too close, if you can see your whole torso you are too far away.</li>");
				sb.append("</ol>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				//sb.append("<br/>Sincerely<br/>"+emailDetails[1]+"");
				sb.append("<br/>Sincerely<br/>TeacherMatch Client Services Team");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("<br/><br/>You are receiving this email from an automated system. Please do not reply to this email. If you are unable to access the questionnaire or require assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println(" Email to candidate for virtual interview :"+sb.toString());
		return sb.toString();
	}
	
	public static String sendReminderMailText(JobOrder jobOrder, TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return sendReminderMailTextFR( jobOrder,  teacherDetail);
				
		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		DistrictMaster districtMaster = jobOrder.getDistrictMaster();
		String districtName = districtMaster.getDisplayName()==null?districtMaster.getDistrictName():districtMaster.getDisplayName();
		if(districtName.equals(""))
			districtName = districtMaster.getDistrictName();
		int reminderFrequencyInDays = districtMaster.getReminderFrequencyInDays();
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"80%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				/*sb.append("<br/>Thank you for applying for employment with "+districtName+". We hire excellent teachers throughout the school year and are eager to learn more about you. " +
						"To make sure your application is up-to-date, please respond to the following questions by "+reminderFrequencyInDays+" days. <br/><br/>");*/
				String url=Utility.getValueOfPropByKey("basePath");
				// Redirect to login
				sb.append("<br/>Thank you for applying for employment with "+districtName+". We hire excellent teachers throughout the school year and are eager to learn more about you. " +
						"To make sure your application is up-to-date, please <a href='"+url+"'>Click Here</a> to access your Job Applications. <br/>");
				
/*				String forMated = "";
				String tchrId=Utility.encryptNo(teacherDetail.getTeacherId());
				String jobId=Utility.encryptNo(jobOrder.getJobId());
				String linkIds= tchrId+"###"+jobId;
				try {
					forMated = Utility.encodeInBase64(linkIds);
					forMated = url+"tmsurvey.do?key="+forMated;
				} catch (Exception e) {
					e.printStackTrace();
				}	
				
				sb.append("<br/>Thank you for applying for employment with "+districtName+". We hire excellent teachers throughout the school year and are eager to learn more about you. " +
						"To make sure your application is up-to-date, please respond to the survey questions by "+reminderFrequencyInDays+" days. <a href='"+forMated+"'>Click Here</a> to access the survey<br/>");*/
				
				//sb.append("Clicking the URL <a href='"+forMated+"'>"+forMated+"</a> <br><br>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Sincerely,<br/>");
				sb.append("The Office of Instructional Staffing and Recruitment<br/>");
				sb.append(""+districtName+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String sendReminderMailTextForFullerton(JobOrder jobOrder, TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return sendReminderMailTextForFullertonFR(jobOrder,  teacherDetail);
				
		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		DistrictMaster districtMaster = jobOrder.getDistrictMaster();
		String districtName = districtMaster.getDisplayName()==null?districtMaster.getDistrictName():districtMaster.getDisplayName();
		if(districtName.equals(""))
			districtName = districtMaster.getDistrictName();
		int reminderFrequencyInDays = districtMaster.getReminderFrequencyInDays();
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"80%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<br/>Thank you for your interest with the Fullerton School District. Our records indicate that your application is currently INCOMPLETE.");
				sb.append("<br/><br/>Please make sure to complete your application before the deadline. If your application is incomplete after our posted deadline your application will NOT be considered. <a href='"+url+"'> Click here</a> to access your job application.<br/>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Sincerely,<br/>");
				sb.append("Certificated Personnel<br/>");
				sb.append("Fullerton School District<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String sendInviteForInviteOnlyCandidatesMailText(JobOrder jobOrder, TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return sendInviteForInviteOnlyCandidatesMailTextFR( jobOrder,  teacherDetail);
				
		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		DistrictMaster districtMaster = jobOrder.getDistrictMaster();
		String districtName = districtMaster.getDisplayName()==null?districtMaster.getDistrictName():districtMaster.getDisplayName();
		if(districtName.equals(""))
			districtName = districtMaster.getDistrictName();
		
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"80%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<br/>Please complete an application for "+districtName+". <br/><br/>by clicking on this link:  <br/><a href='"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"'>"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"</a><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+"><br>Please reach out to the district human resources department with any questions.</a>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Sincerely,<br/>");
				sb.append(""+districtName+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("<br/><br/>You are receiving this email from an automated system. Please do not reply to this email. If you are unable to access the questionnaire or require assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
		
	public static String mailTextForExamSlotSelection(ExaminationCodeDetails examinationCodeDetails,String assessmentLink)
	{	
	
	if(locale.equals("fr"))
			return mailTextForExamSlotSelectionFR(examinationCodeDetails,assessmentLink);
			
		StringBuffer sb = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
		SimpleDateFormat formday=new SimpleDateFormat("EEEE");
		DistrictMaster districtMaster=examinationCodeDetails.getDistrictAssessmentDetail().getDistrictMaster();
		String JobTitle=examinationCodeDetails.getJobOrder().getJobTitle();
		String examAddress=districtMaster.getCityName()+", "+districtMaster.getAddress()+", "+districtMaster.getStateId().getStateName()+", "+districtMaster.getZipCode();
		String district=districtMaster.getDistrictName();	
		boolean slotSelectionNeeded = false;
		if(districtMaster.getDistrictId()==4218990)
			slotSelectionNeeded = true;
		
		try 
		{
			TeacherDetail  teacherDetail = examinationCodeDetails.getTeacherDetail();
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>We are pleased to invite you to take the "+examinationCodeDetails.getDistrictAssessmentDetail().getDistrictAssessmentName()+" assessment for the position of "+JobTitle+" with "+district+".");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			
			if(slotSelectionNeeded)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("</br>Please click here  to schedule your assessment. Please note the date and time you select.");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
			}
			
			sb.append("<td style="+mailCssFontSize+">");
			if(slotSelectionNeeded)
				sb.append("</br>Click on the following link to select the assessment Slot:<br> "+assessmentLink+"<br/>");
			else
				sb.append("</br>Click on the following link to take the assessment:<br> "+assessmentLink+"<br/>");
			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Please do not reply to this e-mail. If you need assistance, please contact <a href='mailto:"+Utility.getValueOfSmtpPropByKey("smtphost.noreplyusername")+"'>"+Utility.getValueOfSmtpPropByKey("smtphost.noreplyusername")+"</a>.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			/*sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Assessment Code: "+examinationCodeDetails.getExaminationCode()+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Address: "+examAddress+"</br>");
			sb.append("</td>");
			sb.append("</tr>");	*/		
		
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best regards,<br>"+district+"");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	public static String mailTextForExamSlotSelectionFR(ExaminationCodeDetails examinationCodeDetails,String assessmentLink)
	{
		StringBuffer sb = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
		SimpleDateFormat formday=new SimpleDateFormat("EEEE");
		DistrictMaster districtMaster=examinationCodeDetails.getDistrictAssessmentDetail().getDistrictMaster();
		String JobTitle=examinationCodeDetails.getJobOrder().getJobTitle();
		String examAddress=districtMaster.getCityName()+", "+districtMaster.getAddress()+", "+districtMaster.getStateId().getStateName()+", "+districtMaster.getZipCode();
		String district=districtMaster.getDistrictName();		
		boolean slotSelectionNeeded = false;
		if(districtMaster.getDistrictId()==4218990)
			slotSelectionNeeded = true;
		
		try 
		{
			TeacherDetail  teacherDetail = examinationCodeDetails.getTeacherDetail();
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour  "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Nous sommes heureux de vous inviter � prendre le  "+examinationCodeDetails.getDistrictAssessmentDetail().getDistrictAssessmentName()+" �valuation pour le poste de "+JobTitle+" au "+district+".");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
		/*	sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("</br>S'il vous pla�t, cliquez ici pour planifier votre �valuation. S'il vous pla�t, notez la date et l'heure que vous s�lectionnez.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("</br>Cliquez sur le lien suivant pour s�lectionner le lieu de l'�valuation:<br> "+assessmentLink+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");*/
			
			if(slotSelectionNeeded)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("</br>S'il vous pla�t, cliquez ici pour planifier votre �valuation.");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
			}
			
			sb.append("<td style="+mailCssFontSize+">");
			if(slotSelectionNeeded)
				sb.append("</br>Click on the following link to select the assessment Slot:<br> "+assessmentLink+"<br/>");
			else
				sb.append("</br>Click on the following link to take the assessment:<br> "+assessmentLink+"<br/>");
			
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:"+Utility.getValueOfSmtpPropByKey("smtphost.noreplyusername")+"'>"+Utility.getValueOfSmtpPropByKey("smtphost.noreplyusername")+"</a>.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
				
		
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement,<br>"+district+"");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	public String mailTextForDaExamConfirmed(TeacherDetail teacherDetail,ExaminationCodeDetails examinationCodeDetails,UserMaster userMaster,String timeZone)
	{	
		if(locale.equals("fr"))
			return mailTextForDaExamConfirmedFR( teacherDetail, examinationCodeDetails, userMaster, timeZone);
		
		
		StringBuffer sb = new StringBuffer();	
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
		SimpleDateFormat formday=new SimpleDateFormat("EEEE");
		String dayofweek=formday.format(examinationCodeDetails.getExamDate());
		String JobTitle=examinationCodeDetails.getJobOrder().getJobTitle();
		DistrictMaster districtMaster=examinationCodeDetails.getDistrictAssessmentDetail().getDistrictMaster();	
		String examDate=dayofweek+", "+dateFormat.format(examinationCodeDetails.getExamDate()).toString();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+userMaster.getFirstName()+" "+userMaster.getLastName()+",");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Applicant "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" has confirmed the following slot for Assessment for Job "+JobTitle+". Details are as following:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date: "+examDate+"<br/>Time: "+examinationCodeDetails.getExamStartTime()+" To "+examinationCodeDetails.getExamEndTime()+" "+timeZone);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Regards,<br>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		System.out.println(" DA Mail :: "+sb.toString());	
		return sb.toString();
	}
	public String mailTextForDaExamConfirmedFR(TeacherDetail teacherDetail,ExaminationCodeDetails examinationCodeDetails,UserMaster userMaster,String timeZone)
	{
		return null;
	}
	public String mailTextForTeacherExamConfirmed(TeacherDetail teacherDetail,ExaminationCodeDetails examinationCodeDetails,String timeZone)
	{
		if(locale.equals("fr"))
			return mailTextForTeacherExamConfirmedFR( teacherDetail, examinationCodeDetails, timeZone);
		
		StringBuffer sb = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
		SimpleDateFormat formday=new SimpleDateFormat("EEEE");
		String dayofweek=formday.format(examinationCodeDetails.getExamDate());
		String JobTitle=examinationCodeDetails.getJobOrder().getJobTitle();
		DistrictMaster districtMaster=examinationCodeDetails.getDistrictAssessmentDetail().getDistrictMaster();		
		String districtName=districtMaster.getDistrictName();
		String examDate=dayofweek+", "+dateFormat.format(examinationCodeDetails.getExamDate()).toString();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+",");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Thanks for confirming your slot for Assessment for Job   "+JobTitle+". Details are as following:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date: "+examDate+"<br/>Time: "+examinationCodeDetails.getExamStartTime()+" To "+examinationCodeDetails.getExamEndTime()+" "+timeZone);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Regards,<br>"+districtName+"");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		System.out.println(" TeacherMail :: "+sb.toString());	
		return sb.toString();
	}
	
	
	public static String messageForDefaultFontForQuestUser(String messageText,UserMaster userSession)
	{
		if(locale.equals("fr"))
			return messageForDefaultFontForQuestUserFR( messageText, userSession);
		
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(messageText);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");			
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a> or call 855-980-0545.");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	/*
	*@Author: Ram Nath
	*/
	public static String getTempAdvOnBoarding(HttpServletRequest request ,JobOrder jobOrder, TeacherDetail teacherDetail, DistrictMaster districtMaster,UserMaster userMaster){
		if(locale.equals("fr"))
			return getTempAdvOnBoardingFR( request , jobOrder,  teacherDetail,  districtMaster, userMaster);
		
		StringBuffer sb=new StringBuffer("");
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
			String mailDate=" "+dateFormat.format(new Date());					
			String acceptParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##1");
			String declineParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##0");
			String acceptURL=Utility.getValueOfPropByKey("basePath")+"/advOnBoarding.do?key="+acceptParameters;
			String declineURL=Utility.getValueOfPropByKey("basePath")+"/advOnBoarding.do?key="+declineParameters;
			sb.append("<table width='700' style="+mailCssFontSize+" align='center' border='0'>");
			sb.append("<tr>");
			sb.append("<td align='center' >");
			sb.append("<font size='+2'><b>COLUMBUS CITY SCHOOLS</b></font></br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td  align='center'>");
			sb.append("<b>");
			sb.append("OFFICE OF HUMAN RESOURCES");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td  align='center'>");
			sb.append("<b>");
			sb.append("<u>Department of Systems and Staffing</u>");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td align='center'>");
			sb.append("<b>");
			sb.append("270 E. State Street");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td align='center'>");
			sb.append("<b>");
			sb.append("Columbus, Ohio 43215 </br></br>");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("");
			sb.append("<tr>");
			sb.append("<td >");
			sb.append("<table width='700' ><tr><td width='50%' align='left'><b> Secondary (614) 365-5651</b></td>                        <td width='50%' align='right'><b> Elementary (614) 365-5609</b></td></tr></table>");
			sb.append("");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("");
			sb.append("<tr>");
			sb.append("<td align='center'>");
			sb.append("<b>");
			sb.append("<u>Commitment of Employment</u></br></br>");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("");
			sb.append("<tr>");
			sb.append("<td align='center'>");
			sb.append("<b>");
			sb.append("Date:"+mailDate+"  </br></br>");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("");
			sb.append("<tr>");
			sb.append("<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			sb.append("This is to state that            &nbsp;&nbsp;&nbsp;&nbsp;                          is to be employed as a teacher in the Columbus City Schools for the 2015-2016 school year.  The first paid work day will be the date negotiated between the Columbus Board of Education and the Columbus Education Association.</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("This agreement includes the following stipulations:");
			sb.append("<ol>");
			sb.append("<li>The Superintendent will recommend the applicant to the Board of Education for employment.</br></br> </li>");
			sb.append("<li>It assures the applicant is named to a teaching position in their certification area in the Columbus City Schools, for the time indicated.  This assignment is subject to state and federal statute, board policy and applicable provisions of the collective bargaining agreement.  This commitment is contingent upon:  a) the approval of the candidate for employment by the Columbus Board of Education;");
			sb.append("b) the applicant's ability to receive a valid Ohio teaching certificate/license.  (Applicants must have taken and fully passed the National Teachers Exam to become certified/licensed in the State of Ohio.  Applicants must also have successfully completed a student teaching experience in their area of certification); and c) the applicant's submittal and receipt by Columbus City Schools of the mandatory documentation for appointment and continued employment and compensation.</br></br>");
			sb.append("</li>");
			sb.append("<li> It assures the Columbus Board of Education that the applicant will be available for service at the time stipulated in this Agreement. </br></br></li>");
			sb.append("<li> This offer of Commitment of Employment is open for ACCEPTANCE FOR A PERIOD OF 15 DAYS FROM THE DATE INDICATED ABOVE.  Failure to execute and return this document within the allocated time will be considered a declination.</br></br> </li>");
			sb.append("</ol>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td></br>");
			sb.append("By clicking <a href='"+acceptURL+"'>accept</a>, you are accepting this position.  By clicking <a href='"+declineURL+"'>decline</a>, you are declining this position but can still be considered for any other position you may have applied for.");
			sb.append("</td>");
			sb.append("</tr>");
		
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("<br/>");
			sb.append("<table width='100%'>");
			sb.append("<tr><td width='70%'><b>___________________________________________</b></td><td>&nbsp;</td></tr>");
			sb.append("<tr><td  width='70%'><b>Anticipated Area of Certification</b></td><td>&nbsp;</td></tr>");
			sb.append("<tr><td  width='70%'><br/><b>___________________________________________	</b></td><td><br/><b>_______________</b></td></tr>");
			sb.append("<tr><td  width='70%'><b>Signature of Applicant</b></td><td><b>Date</b></td></tr>");
			sb.append("<tr><td  width='70%'><br/><b>___________________________________________</b></td><td><br/><b>_______________</b></td></tr>");
			sb.append("<tr><td  width='70%'><b>Signature of Superintendent's Representative</b></td><td><b>Date</b></td></tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
		}catch(Exception e){
			e.printStackTrace();
		}
		//System.out.println("letter of Commitment==="+sb.toString());																																																																																																																																																																																								sb.append("</table>");
		return sb.toString();
	}
	
	
	public static String getTempEvlCompl(JobOrder jobOrder, TeacherDetail teacherDetail, DistrictMaster districtMaster,UserMaster userMaster,int temp,String schoolName,String statusFlagAndStatusId){
		if(locale.equals("fr"))
			return getTempEvlComplFR( jobOrder,  teacherDetail,  districtMaster, userMaster, temp, schoolName, statusFlagAndStatusId);
		
		StringBuffer sb=new StringBuffer("");
		// temp==0 for mailBySchool
		// temp==1 for mailByDistrict
		// temp==2 for mailByTeacher(Accept Mail)
		// temp==3 for mailByTeacher(Decline Mail)
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
			String mailDate=" "+dateFormat.format(new Date());					
			String acceptParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##"+statusFlagAndStatusId+"##1");
			String declineParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##"+statusFlagAndStatusId+"##0");
			String acceptURL=Utility.getValueOfPropByKey("basePath")+"/evlComp.do?key="+acceptParameters;
			String declineURL=Utility.getValueOfPropByKey("basePath")+"/evlComp.do?key="+declineParameters;			
			sb.append("<table width='700' style="+mailCssFontSize+" align='center' border='0'>");
			if(temp==0){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("School Name : "+schoolName+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Applicant Name : "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Position : "+jobOrder.getJobTitle()+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else if(temp ==1){
				String position = jobOrder.getJobTitle()+" at "+schoolName;
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/> Congratulations! The School Board of Philadelphia, Florida, hereby offers to conditionally employ you as "+position+" for the 2015-2016 school year.  This offer is conditional and based on the following requirements including/but not limited to:<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>Certification/Eligibility Verification</br></li><li>Clearance of Fingerprints</li><li>Drug Testing Clearance</li><li>I-9/Employment Eligibility Verification</li><li>Acceptable Reference/Background Check</li><li>Transcript Verification</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Please reply to this email if you accept this employment assignment with Philadelphia Schools and contact the school for information on the next steps. This offer is void if you do not reply within 2 business days. We look forward to welcoming you to Philadelphia Schools.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href='"+acceptURL+"'>Accept the Offer</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineURL+"'>Decline the Offer</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely,<br/><br/>The Office of Talent<br/>The School District of Philadelphia<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
				sb.append("</td>");
				sb.append("</tr>");
			}else if(temp ==2){					
					System.out.println("Teacher Name=="+teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
					System.out.println("jobOrder.getJobTitle()=="+jobOrder.getJobTitle());
					System.out.println("Teacher Name=="+teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
				sb.append("<tr>");
				sb.append("<td>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" has accepted "+jobOrder.getJobTitle()+" at "+schoolName+" on "+mailDate+".</td>");
				sb.append("</tr>");
				
			}else if(temp ==3){	
				sb.append("<tr>");
				sb.append("<td>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" has declined "+jobOrder.getJobTitle()+" at "+schoolName+" on "+mailDate+".</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");			
		}catch(Exception e){
			e.printStackTrace();
		}
		//System.out.println("letter of Commitment==="+sb.toString());																																																																																																																																																																																								sb.append("</table>");
		return sb.toString();
	}
	
	
	//Sonu Gupta
	public static String getUpdateQQMailForDistrict(UserMaster usermaster,TeacherDetail teacherDetail,String questionAnswer)
	{
		if(locale.equals("fr"))
			return getUpdateQQMailForDistrictFR( usermaster, teacherDetail, questionAnswer);
		
		StringBuffer sb = new StringBuffer();
		String daFullName=usermaster.getFirstName()+" "+usermaster.getLastName();
		String applicantFullName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+daFullName+",");			
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Applicant "+applicantFullName+" has updated the following information:<br/><br>");			
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(questionAnswer);
			sb.append("</td>");
			sb.append("</tr>");				
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thanks");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Partner Success Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	
	
	public static String getOfferReadyByPrincipal(HttpServletRequest request,JobForTeacher jobForTeacher){
		if(locale.equals("fr"))
			return getOfferReadyByPrincipalFR( request, jobForTeacher);
		

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			String candidateEmail = "";
			try{
				if(jobForTeacher!=null && jobForTeacher.getTeacherId()!=null && jobForTeacher.getTeacherId().getEmailAddress()!=null)
					candidateEmail	=	" ( "+jobForTeacher.getTeacherId().getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(jobForTeacher.getTeacherId())+" "+candidateEmail+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Congratulations! The School Board of Miami-Dade County, Florida, hereby offers to conditionally employ you as  a ");
			sb.append(jobForTeacher.getJobId().getJobTitle());
			sb.append("teacher at ");
			if(jobForTeacher.getSchoolMaster()!=null && jobForTeacher.getSchoolMaster().getSchoolName()!=null){
				sb.append(jobForTeacher.getSchoolMaster().getSchoolName());
			}
			sb.append("  for the 2015-2016 school year.");
			sb.append(". In order to establish your eligibility for this position, please visit Miami-Dade County Public Schools� Office of " +
					  "Instructional Certification (1450 NE 2nd Avenue, Suite 260, Miami, FL 33132 � Monday through Friday, 8:00 a.m. to 3:30 p.m.) with the following documentation");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<U>For Part-Time Adult Education Positions:</U>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<UL>");
			sb.append("<LI>Official transcript</LI>");
			sb.append("<LI>Completed District Adult Certificate Application (Form 5584 attached)</LI>");
			sb.append("<LI>Money order for $75.00 made payable to �M-DCPS�</LI>");
			sb.append("</UL>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<U>For Full-Time or Part-Time Vocational Positions:</U>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<UL>");
			sb.append("<LI>Documentation of occupational experience on company letterhead</LI>");
			sb.append("<LI>Professional Licenses, if required</LI>");
			sb.append("<LI>Completed District Vocational Certificate Application (FM 5585 attached)</LI>");
			sb.append("<LI>Money order for $75.00 made payable to �M-DCPS�</LI>");
			sb.append("</UL>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<U>All Applicants:</U>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If the Office of Instructional Certification verifies that you are eligible for employment in this position, " +
					  "you will then need to complete pre-employment requirements including, but not limited to, background check, drug " +
					  "test, employment eligibility verification, and direct deposit enrollment. To ensure that you are prepared to meet " +
					  "these requirements, ALL APPLICANTS must bring:");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<UL>");
			sb.append("<LI>A $71.00 money order or cashier�s check made out to �SBMD Fingerprinting�</LI>");
			sb.append("<LI>Valid driver�s license (or valid government-issued picture identification)</LI>");
			sb.append("<LI>Completed District Vocational Certificate Application (FM 5585 attached)</LI>");
			sb.append("<LI>Social security card</LI>");
			sb.append("<LI>Required documents list on page 5 of the U.S. Department of Homeland Security�s I-9 form ");
			sb.append("<a href='http://www.uscis.gov/files/form/i-9.pdf'>(http://www.uscis.gov/files/form/i-9.pdf)</a>");
			sb.append("</LI>");
			sb.append("<LI>�Voided check or deposit slip from your financial institution; or sign-up for a Skylight Pay Card ");
			sb.append("<a href='http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf'>(http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf)</a>");
			sb.append("</LI>");
			sb.append("</UL>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Congratulations again and we look forward to welcoming you to Miami-Dade County Public Schools.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely,");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Office of Human Capital Management");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Miami-Dade County Public Schools");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" "+sb.toString());
		
		return sb.toString();
	
	}
	
	
	//public static String getOfferAcceptedByStaffer(HttpServletRequest request,JobForTeacher jobForTeacher){
	public static String getOfferReadyByStaffer(TeacherDetail teacherDetail,String jobTitle,String location){
		if(locale.equals("fr"))
			return getOfferReadyByStafferFR( teacherDetail, jobTitle, location);
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			String position = jobTitle; 
			try{
				if(location!=null && !location.equals("")){
					position = jobTitle+" at "+location; 
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();				
			}
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+" "+candidateEmail+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Congratulations! You on your decision to accept our conditional offer of employment as a ");
			sb.append(jobTitle);
			sb.append("teacher at ");
			if(location!=null && location!=null){
				sb.append(location);
			}
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("  Please visit the Employee Service Center (1450 NE 2nd Ave, Suite 456 Miami, FL 33132) within two business days of receiving this email. The Employee Service Center is open Monday through Friday, 8:00 a.m. to 3:30 p.m ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("As stated in your conditional Offer Letter, when you visit the Employee Service Center, you will need to complete the following procedures� <B>PLEASE READ CAREFULLY:</B> ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>Fingerprinting:</B> All new employees must be fingerprinted and cleared before beginning employment with M-DCPS. To be fingerprinted you must bring:</U>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("A $71.00 money order or cashier's check made out to �SBMD Fingerprinting�; and");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your driver license (or valid government - issued picture identification) and social security card");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>I-9/Employment Eligibility Verification:</B> Per the Department of Homeland Security, new employees must fill out an I-9 form for employment eligibility. To complete the process, please bring the required documents listed on Page 5 of the I-9 Form ");
			sb.append("<a href='http://www.uscis.gov/files/form/i-9.pdf'>(http://www.uscis.gov/files/form/i-9.pdf)</a>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>Direct Deposit Authorization:</B> All M-DCPS salary and wages are paid through an automated direct deposit system. Employees must choose from one of the three following options: ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("To directly deposit your paycheck into one of your existing bank accounts, please bring a voided check or deposit slip from that financial institution; or ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sign-up for a Skylight Pay Card ");
			sb.append("<a href='http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf'>(http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf)</a> ; or");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Open a new account with the South Florida Educational Federal Credit Union ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>Drug-testing:</B> Drug-testing is mandatory for all new full time employees and part-time coaches and is provided at no cost. The test is conducted off-site at multiple locations throughout the county. You will be provided a drug test form when you visit the Employee Service Center. The drug test must be conducted within two business days of receiving your form. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("In addition to the documents and procedures described above, you will be asked to fill out various employment-related forms during your visit to the Employee Service Center. <B>Upon the completion and verification of all of the above requirements, your new supervisor will contact you with the date/time that you should report for work.</B> ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you,");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Employee Service Center Staff ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("1450 NE 2nd Ave, Suite 456 Miami, FL 33132 ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("The information contained in this e-mail message is intended solely for the recipient(s) and may contain privileged information. Tampering with or altering the contents of this message is prohibited. This information is the same as any written document and may be subject to all rules governing public information according to Florida Statutes. If you have received this message in error, or are not the named recipient, notify the sender and delete this message from your computer. ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	
	public static String getNegativeQQMailText(UserMaster user, JobOrder jobOrder, DistrictMaster districtMaster, List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList)
	{
		if(locale.equals("fr"))
			return getNegativeQQMailTextFR( user,  jobOrder,  districtMaster, teacherAnswerDetailsForDistrictSpecificQuestionList);
		
		StringBuffer sb = new StringBuffer();
		
		try
		{
			String applicantFirstName = teacherAnswerDetailsForDistrictSpecificQuestionList.get(0).getTeacherDetail().getFirstName();
			String applicantLastName = teacherAnswerDetailsForDistrictSpecificQuestionList.get(0).getTeacherDetail().getLastName();
			
			String districtUserFirstName= user.getFirstName();
			String districtUserSecondName= user.getLastName();
			
			sb.append("<table>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+districtUserFirstName+" "+districtUserSecondName+",<BR><BR>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Applicant "+applicantFirstName+" "+applicantLastName+" has given one or more negative response(s) for Qualification Questions as following: <BR>");
			sb.append("</td>");
			sb.append("</tr>");
			
			int count=1;
			for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestions : teacherAnswerDetailsForDistrictSpecificQuestionList)
			{
				if(teacherAnswerDetailsForDistrictSpecificQuestions.getIsActive()!=null && teacherAnswerDetailsForDistrictSpecificQuestions.getIsValidAnswer()!=null && teacherAnswerDetailsForDistrictSpecificQuestions.getIsActive() && !teacherAnswerDetailsForDistrictSpecificQuestions.getIsValidAnswer())
				{
					String question=teacherAnswerDetailsForDistrictSpecificQuestions.getQuestion();
					String answer=teacherAnswerDetailsForDistrictSpecificQuestions.getSelectedOptions();
					String explanation=teacherAnswerDetailsForDistrictSpecificQuestions.getInsertedText();
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					
					if(question!=null)
						sb.append("<BR>Q"+count+". "+question);
					if(answer!=null)
						sb.append("<BR>Ans. "+answer);
					if(explanation!=null)
						sb.append("<BR>Explanation. "+explanation);
					
					sb.append("<BR></td>");
					sb.append("</tr>");
					count++;
				}
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<BR>Sincerely<br>TeacherMatch Client Services Team  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	public static String PortfolioReminderMail(TeacherDetail teacherDetail, DistrictMaster districtMaster, String mainURLEncode)
	{
		if(locale.equals("fr"))
			return PortfolioReminderMailFR(teacherDetail, districtMaster, mainURLEncode);
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request;// = context.getHttpServletRequest();
		
		if(WebContextFactory.get()!=null)
			request = WebContextFactory.get().getHttpServletRequest();
		else
			request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		
		StringBuffer sb = new StringBuffer();
		try {

				String basePath	=	Utility.getBaseURL(request);
				sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Thank you for applying to work with " +districtMaster.getDistrictName()+". In order to keep our records up-to-date, we require applicants to log in and update their profiles on a regular basis. Please click the link below to update your profile.   <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				

				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+mainURLEncode+">");
				sb.append(mainURLEncode);
				sb.append("</a> <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Please note that failure to update your profile within 30 days will result in your profile being withdrawn from the jobs you applied to. If you would like to reapply at any time, simply log into your profile and indicate that you are interested in these jobs again (this will not require you to start the application process from scratch).");
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");

				

				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Sincerely,<br><br>The Office of Instructional Staffing and Recruitment<br>"+districtMaster.getDistrictName()+"");
				sb.append("</td>");
				sb.append("</tr>");
				

			sb.append("</table>");
		}

		catch(Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	public static String getRegistrationMailForQuestFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId()+"&quest=true";
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+": Nous vous remercions de votre  l�inscription aupr�s de  TeacherMatchTM.  Votre premi�re �tape en vue de trouver une position id�ale. Pour terminer le processus d�inscription, vous devez confirmer votre adresse �lectronique en cliquant sur le lien ci-dessous. Une fois que vous l�aurez fait, vous serez en mesure de vous identifier sur la page de connexion en utilisant les informations que vous avez fournies pendant le processus d�inscription.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cliquez ici pour v�rifier votre courrier �lectronique�: <a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre courrier �lectronique de connexion est�: <Adresse �lectronique><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre mot de passe est celui que vous avez choisi pendant le processus d�inscription. Si vous avez oubli� votre mot de passe, cliquer simplement le lien ��j�ai oubli頻 � la Page de Connexion. Si vous voulez changer votre mot de passe, cliquez sur le lien param�tres sous votre nom dans le coin droit sup�rieur de votre �cran.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bienvenue!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L'�quipe de succ�s partenaire<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez clientservices@teachermatch.net ou t�l�phonez au <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	public static String getRegistrationMailForQuestUserFR(HttpServletRequest request,UserMaster usermaster)
	{
		HttpSession session		=	request.getSession(false);
		UserMaster userSession	=	(UserMaster) session.getAttribute("userMaster");
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(usermaster)+":   Vous avez �t� ajout� en tant qu'utilisateur sur la plate-forme de TeacherMatchTM.");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Votre courrier �lectronique de connexion est�:<a href='mailto:"+usermaster.getEmailAddress()+"'>"+usermaster.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre mot de passe est celui que vous avez choisi lors du processus d'inscription. Si vous avez oubli� votre mot de passe, il vous suffit de cliquer sur le lien 'j'ai oubli�' � la page de connexion. Si vous voulez changer votre mot de passe, cliquez sur le lien Param�tres sous votre nom dans le coin sup�rieur droit de votre �cran apr�s vous �tre connect�.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bienvenue!");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L��quipe du service � la client�le<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	
	
	public static String getRegistrationMailForNonClientDistrictFR(HttpServletRequest request,UserMaster usermaster)
	{
		HttpSession session		=	request.getSession(false);
		UserMaster userSession	=	(UserMaster) session.getAttribute("userMaster");
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(usermaster)+":<br/><br/>Bienvenue � TeacherMatch Quest!  Vous avez �t� ajout� en tant qu'utilisateur sur la plate-forme de TeacherMatchTM. La prochaine �tape est pour notre �quipe de v�rifier votre compte.  Si vous avez utilis� votre adresse �lectronique du Conseil scolaire pour cr�er votre compte ce processus sera acc�l�r�.  Si vous n'avez pas utilis� votre adresse �lectronique du Conseil scolaire l'un doit �tre fourni pour activer votre compte. ");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Votre courrier �lectronique de connexion est�actuellement :<a href='mailto:"+usermaster.getEmailAddress()+"'>"+usermaster.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Votre mot de passe est celui que vous avez choisi lors du processus d'inscription.  Si vous avez oubli� votre mot de passe, il vous suffit de cliquer sur le lien 'j'ai oubli�' � la page de connexion.  Si vous voulez changer votre mot de passe, cliquez sur le lien Param�tres sous votre nom dans le coin sup�rieur droit de votre �cran apr�s vous �tre connect�.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bienvenue!");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L'�quipe de succ�s partenaire  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce courriel qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	
	
	public static String getRegistrationNotificationMailForQuestUserFR(String districtName, String firstName, String lastName, String emailAaddress, String password,int check)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(check==0)
			{
				sb.append("�quipe du service � la client�le,<br/><br/>");
			}			
			else if(check==1)
			{
				sb.append("�quipe partenaire de r�ussite, <br/><br/>");
			}
			sb.append(" Vous recevez ce courriel parce que <b>"+firstName+" "+lastName+"</b>s�est inscrit comme non-client du Conseil scolaire.  Les d�tails du Conseil scolaire sont les suivants : ");			
			sb.append("</td>");
			sb.append("</tr><br/>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Nom du Conseil scolaire : <b>"+districtName+"</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Pr�nom: <b>"+firstName+"</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Nom:  <b>"+lastName+"</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Adresse �lectronique : <b><Email Address><a href='mailto:"+emailAaddress+"'>"+emailAaddress+"</a></b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Mot de passe: <b>"+password+"</b>");
			sb.append("</td>");
			sb.append("</tr>");		
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Administrateur TM <br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce courriel qui est un courriel automatis� g�n�r� par le syst�me.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	
	public static String getRegistrationForClientServicesFR(HttpServletRequest request,UserMaster usermaster)
	{
		HttpSession session		=	request.getSession(false);
		UserMaster userSession	=	(UserMaster) session.getAttribute("userMaster");
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+"font-weight: bold;>");
			sb.append("Client Services Team,<br/><br/>");
			sb.append(" Vous recevez ce courriel parce que "+usermaster.getFirstName()+" "+usermaster.getLastName()+"s�est inscrit comme non-client du Conseil scolaire.  Les d�tails du Conseil scolaire sont les suivants :  ");
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+"font-weight: bold;>");
			sb.append("Nom		:	<b>"+usermaster.getFirstName()+" "+usermaster.getLastName()+"</b></td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+"font-weight: bold;>");
			sb.append("Adresse �lectronique : <b>"+usermaster.getEmailAddress()+"</b></td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+"font-weight: bold;>");		
			sb.append("Nom du Conseil scolaire : <b>"+usermaster.getDistrictId().getDistrictName()+"</b></td>");		
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>Veuillez ne pas r�pondre � ce courriel qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur �  <a href='mailto:lientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
	
		return sb.toString();
	}
	
	public static String getRegistrationMailForTempQuestFR(HttpServletRequest request,TeacherDetail teacherDetail,String pass)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId()+"&quest=true";
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+": Nous vous remercions de votre inscription avec TeacherMatchTM. , votre premi�re �tape en vue de trouver un poste id�al.  Afin de terminer le processus d'inscription, vous devez confirmer votre adresse �lectronique en cliquant sur le lien ci-dessous.  Lorsque vous aurez confirm� votre adresse �lectronique, vous serez en mesure de vous identifier sur la page de connexion en utilisant les informations que vous avez fournies lors du processus d'inscription.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cliquez ici pour v�rifier votre adresse �lectronique : <a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre adresse �lectronique de connexion est :<a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre mot de passe est :  <b>"+pass+"</b> <br/>  Si vous avez oubli� votre mot de passe, il vous suffit de cliquer sur le lien 'j'ai oubli�' � la page de connexion. Si vous voulez changer votre mot de passe, cliquez sur le lien Param�tres sous votre nom dans le coin sup�rieur droit de votre �cran apr�s vous �tre connect�.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bienvenue!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("�quipe partenaire de r�ussite<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce courriel qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String getRegistrationMailFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+": Nous vous remercions de votre inscription avec TeacherMatchTM. , votre premi�re �tape en vue de trouver un poste id�al.  Afin de terminer le processus d'inscription, vous devez confirmer votre adresse �lectronique en cliquant sur le lien ci-dessous.  Lorsque vous aurez confirm� votre adresse �lectronique, vous serez en mesure de vous identifier sur la page de connexion en utilisant les informations que vous avez fournies lors du processus d'inscription. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cliquez ici pour v�rifier votre adresse �lectronique : <a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre adresse �lectronique de connexion est : <a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Si vous avez oubli� votre mot de passe, il vous suffit de cliquer sur le lien 'j'ai oubli�' � la page de connexion. Si vous voulez changer votre mot de passe, cliquez sur le lien Param�tres sous votre nom dans le coin sup�rieur droit de votre �cran apr�s vous �tre connect�.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bienvenue!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("�quipe partenaire de r�ussite<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce courriel qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}	
		return sb.toString();
	}
	
	public static String getRegistrationMailForInternalCandidateFR(HttpServletRequest request,TeacherDetail teacherDetail,DistrictMaster districtMaster,String rPassword,int mailFlag)
	{
StringBuffer sb = new StringBuffer();
		
		String districtDName="";
		String dmName="";
		String dmPhoneNumber="";
		if(districtMaster!=null){
			if(districtMaster.getDisplayName()!=null && !districtMaster.getDisplayName().equals("")){
				districtDName=districtMaster.getDisplayName();
			}else{
				districtDName=districtMaster.getDistrictName();
			}
			if(districtMaster.getDmName()!=null){
				dmName=districtMaster.getDmName();
				
				if(districtMaster.getDistrictId().equals(1201470)){
					dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
				}
			}
			if(districtMaster.getDmPhoneNumber()!=null){
				dmPhoneNumber=districtMaster.getDmPhoneNumber();
			}
		}
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId()+"&iId=543210";
			sb.append("<table>");
			
			if(mailFlag==0){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getTeacherName(teacherDetail)+": Nous vous remercions de votre inscription pour les possibilit�s de transferts internes au  "+districtDName+"! Pour terminer le processus d'inscription, vous devez confirmer votre adresse �lectronique en cliquant sur le lien ci-dessous.  Lorsque vous l�aurez fait, nous vous enverrons les possibilit�s d'emploi qui correspondent � l'int�r�t que vous avez sp�cifi� au cours du processus d'enregistrement en ce qui concerne des opportunit�s de transfert interne.  Ne vous inqui�tez pas, si vous ne souhaitez pas recevoir ces notifications, vous pouvez toujours choisir de ne pas les recevoir en vous connectant � votre compte et en choisissant de ne pas vous inscrire aux notifications \" interne Transfert\".<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cliquez ici pour v�rifier votre adresse �lectronique : <a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Vous pouvez vous connecter � TeacherMatch <a target='blank' href='"+Utility.getBaseURL(request)+"' >here</a><br/>utilisateur:<a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
				sb.append("<br/>Mot de passe:  "+rPassword+"<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Vous pouvez �galement modifier votre mot de passe apr�s l�identification dans TeacherMatch.  <br/>Pour toutes questions suppl�mentaires ou si vous rencontrez des probl�mes techniques, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou appelez "+mailContactNo+"<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getTeacherName(teacherDetail)+": Nous vous remercions de votre inscription pour les possibilit�s de transferts internes!  Pour terminer le processus d'inscription, vous devez confirmer votre adresse �lectronique en cliquant sur le lien ci-dessous.  Lorsque vous l�aurez fait, vous serez en mesure de recevoir les offres d�emplois correspondants.  <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cliquez ici pour v�rifier votre adresse �lectronique : <a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sinc�rement,<br/>");
			sb.append(dmName+"<br/>");
			if(!dmPhoneNumber.equals("")){
				sb.append(dmPhoneNumber+"<br/>");
			}
			sb.append(districtDName+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au  "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::1 "+sb.toString());
		return sb.toString();
	}
	
	public static String getFgPwdMailToTeacherFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"resetpassword.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+":<br/> Vous recevez ce courriel parce que vous avez cliqu� sur le lien 'J�ai oubli�' � c�t� du champ de mot de passe � la page de Connextion de TeacherMatch.  Si vous n�avez pas cliqu� sur ce lien, s'il vous pla�t, nous en aviser imm�diatement � l�adresse  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> car cela pourrait �tre un signe que quelqu'un d'autre tente d'acc�der � votre compte.  Pour changer votre mot de passe, cliquez sur le lien ci-dessous.  Le lien sera actif pour 72 heures.  Si vous ne faites pas le changement dans les 72 heures, vous aurez besoin de cliquer sur le lien 'j�ai oubli�' � nouveau.<br/><br/> ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Changez votre mot de passe en cliquant ici: <a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			/*sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");*/


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sinc�rement,<br>L��quipe du service � la client�le   <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		return sb.toString();
	}
	
	public static String getRegConfirmationEmailToTeacherFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			String loginUrl = Utility.getBaseURL(request)+"signin.do";
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+": Bienvenue.  Vous avez maintenant termin� le processus d'inscription pour TeacherMatchTM .  Chez TeacherMatch, nous sommes fiers d'aider les candidats � trouver les meilleurs r�les.   Nous croyons �galement que le personnel �ducateur hautement qualifi� a besoin de soutien.  C�est pourquoi nous proposons un rapport de d�veloppement professionnel personnalis�, disponible apr�s que vous avez termin� votre porte-folio et la base de l'inventaire.  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre adresse �lectronique de connexion est : "+teacherDetail.getEmailAddress()+"");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre mot de passe est celui que vous avez choisi lors du processus d'inscription.  Si vous avez oubli� votre mot de passe, il vous suffit de cliquer sur le lien 'j'ai oubli�' sur la page de connexion.  Si vous voulez changer votre mot de passe, cliquez sur le lien Param�tres sous votre nom dans le coin sup�rieur droit de votre �cran apr�s vous �tre connect�.  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Veuillez cliquer sur <a href='"+loginUrl+"'>"+loginUrl+"</a> afin de construire votre porte-folio et de compl�ter les inventaires appropri�s pour les postes qui vous int�ressent.");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous vous remercions d�avoir utilis� TeacherMatch! <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L��quipe du service � la client�le<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String mailtoTeacherAfterJobApplyFR(HttpServletRequest request,TeacherDetail teacherDetail,JobOrder jobOrder,String[] arrHrDetail)
	{
		StringBuffer sb 					= 	new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 		= arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		String loginUrl	=	arrHrDetail[10];
		String cgUrl	=	arrHrDetail[11];
		String jobboardUrl	=	arrHrDetail[12];
		String forgetPwdUrl	=	arrHrDetail[13];
		
		String jobUrl 						=	Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId();		
		//String teacherLname				=	teacherDetail.getLastName()==null?"":teacherDetail.getLastName();		
		List<SchoolMaster> schoolList		=	null;
		try 
		{
			//System.out.println("\n\n\n---------- schoolDistrictName "+schoolDistrictName);
			
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous vous remercions d�avoir postul� pour le poste "+jobOrder.getJobTitle()+" avec "+schoolDistrictName+". Nous sommes heureux que vous explorez les possibilit�s d'emploi avec nous.  Veuillez vous assurer que vos param�tres de courrier �lectronique vous permettent de recevoir des messages � partir de cette adresse courriel.  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre adresse �lectronique de connexion est : "+teacherDetail.getEmailAddress()+"  et votre mot de passe est celui que vous avez choisi lors du processus d'inscription.  Si vous avez oubli� votre mot de passe, il vous suffit de cliquer sur le lien 'j'ai oubli�' ici. Si vous voulez changer votre mot de passe, \"I forgot\" link <a href="+forgetPwdUrl+">here</a>.  cliquez sur le lien Param�tres sous votre nom dans le coin sup�rieur droit de votre �cran apr�s que vous vous �tes connect� � TeacherMatch ici.  <a href="+loginUrl+">here</a>.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			/*sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login into TeacherMatch <a href="+loginUrl+">here</a>.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");*/
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Si vous avez des questions ou si vous rencontrez des probl�mes techniques, veuillez nous �crire �  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou appelez 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sincerely<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sinc�rement,  <br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sinc�rement,  <br/>");//"+dmName+"<br/>
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Vous recevez ce courriel � partir d'un syst�me automatis�. Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
			//System.out.println("\n\n  Sekhar- :::::::::::::::::::::::\n "+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getFgPwdMailToOtUserFR(HttpServletRequest request,UserMaster userMaster,String[] arrHrDetail)
	{
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		if(userMaster!=null && userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId().equals(1201470)){
			dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
		}
		
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+": <br/>Vous recevez ce courriel parce que vous avez cliqu� sur le lien 'J�ai oubli�' � c�t� du champ de mot de passe � la page de Connextion de TeacherMatch.  Si vous n�avez pas cliqu� sur ce lien, s'il vous pla�t, nous en aviser imm�diatement � l�adresse  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>car cela pourrait �tre un signe que quelqu'un d'autre tente d'acc�der � votre compte.  Pour changer votre mot de passe, cliquez sur le lien ci-dessous.  Le lien sera actif pour une dur�e de 72 heures.  Si vous ne faites pas le changement dans les 72 heures, vous aurez besoin de cliquer sur le lien j�ai oubli� � nouveau.<br/><br/> ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Changez votre mot de passe en cliquant ici:<a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			/*sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");*/
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Client Services Team   <br/><br/>");
			if(isHrContactExist==0)
				sb.append("Sinc�rement,   <br/>"+schoolDistrictName+" L��quipe de recrutement et de la s�lection <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sinc�rement<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sinc�rement<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			System.out.println("sb:::2"+sb.toString());

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String createUserPwdMailToOtUserFR(HttpServletRequest request,UserMaster userMaster)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			HttpSession session			=	request.getSession(false);
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			String verificationUrl		=	Utility.getBaseURL(request)+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+":  Vous avez �t� ajout� en tant qu'utilisateur sur la plate-forme de TeacherMatchTM .  Votre adresse courriel de connexion est : <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>. Votre authentification de NIP authentification  "+userMaster.getAuthenticationCode()+". Pour activer votre compte, s'il vous pla�t, visiter le lien ci-dessous pour r�gler votre mot de passe: ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("S'il vous pla�t, me contacter �  <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> i vous avez des questions. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Meilleurs voeux,<br/>"+getUserName(userSession)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" createUserPwdMailToOtUserb :: "+sb.toString());
		
		return sb.toString();
	}
	
	public static String messageForDefaultFontFR(String messageText,UserMaster userSession)
	{
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(messageText);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			if(!messageText.contains("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me."))
			if(userSession!=null && userSession.getDistrictId()!=null && userSession.getDistrictId().getDistrictId()!=null && userSession.getDistrictId().getDistrictId()==4218990)
				sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:recruitment@philasd.org'>recruitment@philasd.org</a>.");
			else
				sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String messageForFinalDecisionMakerFR(String FinalDecisionMakerName,String dmPassword,UserMaster userSession)
	{
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+FinalDecisionMakerName+",");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("</br>Ce courriel est envoy� pour vous informer que votre administrateur de compte de TeacherMatch vous a d�sign� comme �tant un approbateur final dans la proc�dure de d�cision.  Cela signifie qu'il faudra autoriser des changements majeurs sur le compte.  S'il vous pla�t, sauvegardez le mot de passe inclus dans ce courriel dans un endroit s�r car vous en aurez besoin pour v�rifier votre identit� lorsque vous communiquez avec notre ligne d�assistance.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("</br>Si vous croyez que ceci a �t� une erreur, veuillez communiquer avec votre administrateur de compte TeacherMatch � <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> sinon, vous ne devez pas prendre d�action. ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Votre mot de passe est :  "+dmPassword);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Cordialement,</br>L��quipe TeacherMatch du service � la client�le ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur � <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String createJobOrderForDistrictFR(int noOfSchoolExpHires,UserMaster userSession,String baseURL,JobOrder jobOrder,String jcRecords,String supportOrUserName)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String noOfExpHires="NA";
			if(jobOrder.getNoOfExpHires()!=null && jobOrder.getNoOfExpHires()!=0){
				noOfExpHires=jobOrder.getNoOfExpHires()+"";
			}else{
				if(noOfSchoolExpHires!=0)
					noOfExpHires=noOfSchoolExpHires+"";
			}

			String JcRecords="NA";

			if(jcRecords!=null && !jcRecords.equals("")){
				JcRecords=jcRecords;
			}

			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+supportOrUserName+",</br></br>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Vous recevez ce courriel parce que je vous ai ajout� � une offre d�emploi appel�  "+jobOrder.getJobTitle()+" avec les caract�ristiques suivantes: </br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
		
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul>");
			sb.append("<li>");
			sb.append("Nom de la certification :  "+JcRecords);
			sb.append("</li>");
			sb.append("<li>");
			sb.append("Nombre d'embauches qui vous sont attribu�es : "+noOfExpHires);
			sb.append("</li>");
			sb.append("<li>");
			sb.append("URL de l'affichage du poste: <a target='blank' href='"+baseURL+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"' >"+baseURL+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"</a>");
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Vous devriez maintenant voir cette offre d'emploi dans la liste situ�e sous le menu des commandes de l'emploi sur la plate-forme de TeacherMatchTM.  Vous pouvez �galement choisir d'afficher la grille de candidats pour chaque offre d�emploi dans le menu des commandes de l'emploi. ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/> S'il vous pla�t, me contacter �  <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> si vous avez des questions ou si vous souhaitez demander un changement. ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Best wishes,</br>"+getUserName(userSession));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur � <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			//System.out.println("sb="+sb);

		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(" createJobOrderForDistrict :: 3"+sb.toString());
		
		return sb.toString();
	}
	
	
	public static String jobOrderSupportFR(int jobOrderType,String nameOfEntity,String Schools,String aUploadUrl,UserMaster userSession,String baseURL,JobOrder jobOrder)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{

			int noOfExpHires=0;
			if(jobOrder.getNoOfExpHires()!=null && jobOrder.getNoOfExpHires()!=0){
				noOfExpHires=jobOrder.getNoOfExpHires();
			}
			String urlFileName="NA";
			String entityAdmin="";
			String jobJSIAttach="NA";
			int jobJSI=0;

			if(jobOrder.getIsJobAssessment()){
				jobJSI=1;
			}
			if(jobJSI==1){
				if(jobOrder.getAttachNewPillar()==1){
					if(jobOrderType==2){
						jobJSIAttach="district job order";
					}else{
						jobJSIAttach="school job order";
					}
				}else if(jobOrder.getAttachDefaultDistrictPillar()==1){
					jobJSIAttach="district default";
				}else if(jobOrder.getAttachDefaultSchoolPillar()==1){
					jobJSIAttach="school default";
				}
			}

			if(nameOfEntity==null){
				nameOfEntity="NA";
			}
			if(Schools==null){
				Schools="NA";
			}
			if(aUploadUrl==null){
				aUploadUrl="NA";
			}else{
				urlFileName=aUploadUrl.substring(aUploadUrl.indexOf("file=")+5);
			}

			if(userSession.getEntityType()==1){
				entityAdmin="TM Admin";
			}else if(userSession.getEntityType()==2){
				entityAdmin="District Admin";
			}else if(userSession.getEntityType()==3){
				entityAdmin="School Admin";
			}

			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour,   Vous devez cr�er les d�tails de l�inventaire sp�cifique � l�emploi (ISE) : ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>-Type d�inventaire sp�cifique � l�emploi:  "+jobJSIAttach);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-Nom de l'entit�:   "+nameOfEntity);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-"+getUserName(userSession));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-"+userSession.getEmailAddress());
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-"+entityAdmin);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-L'�cole :   "+Schools);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-Num�ro de l�offre d�emploi: <a target='blank' href='"+baseURL+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"' >"+jobOrder.getJobId()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-"+jobOrder.getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-<a target='blank' href='"+aUploadUrl+"' >"+urlFileName+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");	
			//	System.out.println("sb>>>>>>>>"+sb);
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	
	public static String createBatchJobOrderForSchoolFR(int noOfExpHires,UserMaster userSession,String baseURL,BatchJobOrder batchJobOrder,String jcRecords,String supportOrUserName)
	{
		
		StringBuffer sb = new StringBuffer();
		try 
		{
			noOfExpHires=noOfExpHires;
			String JcRecords="NA";
			if(jcRecords!=null && !jcRecords.equals("")){
				JcRecords=jcRecords;
			}
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+supportOrUserName+": Vous recevez ce courriel parce que j'ai cr�� une offre d�emploi vous appel� "+batchJobOrder.getJobTitle()+" avec les caract�ristiques suivantes :");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("-Nom de la certification :   "+JcRecords);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("- Nombre d'embauches qui vous sont attribu�es : "+noOfExpHires);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("URL de l'affichage de l�offre d�emploi <a target='blank' href='"+baseURL+"applyteacherjob.do?batchJobId="+batchJobOrder.getBatchJobId()+"' >"+baseURL+"applyteacherjob.do?batchJobId="+batchJobOrder.getBatchJobId()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Vous devez accepter cette offre d�emploi avant qu'elle ne devienne active et visible aux candidats. Pour ce faire, cliquez sur Offre d�emploi en masse sous le menu des commandes de l'emploi, trouvez cette offre d�emploi dans le tableau, puis cliquez sur Accepter. Apr�s l�acceptation, vous devenez le propri�taire de l�offre d�emploi et pouvez la modifier comme bon vous semble. Vous avez le plein contr�le et la propri�t� de la pr�sente offre d�emploi et pouvez r�gler l'ensemble de ses caract�ristiques.  Je ne peux pas faire des changements puisque la propri�t� vous a �t� donn�e.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>S'il vous pla�t, me contacter � <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> si vous avez des questions.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Meilleurs v�ux, </br>"+getUserName(userSession));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur � <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			//System.out.println("sb=order=="+sb);	
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public static String newPwdMailToUserFR(HttpServletRequest request,UserMaster userMaster)
	{	
		StringBuffer sb = new StringBuffer();
		try 
		{
			HttpSession session			=	request.getSession(false);
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			String verificationUrl		=	Utility.getBaseURL(request)+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+":  J�ai envoy� une demande de r�initialisation du mot de passe sur la plate-forme de TeacherMatchTM .  Votre adresse �lectronique de connexion est <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>.  S'il vous pla�t, visitez le lien ci-dessous pour d�finir votre mot de passe :<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("S'il vous pla�t, me contacter �  <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> si vous avez des questions.  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Meilleurs v�ux,<br/>"+getUserName(userSession)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur �  <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static String getSettingEmailChangeMailTextFR(HttpServletRequest request,TeacherDetail teacherDetail, String oldEmailAdder)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+":  Vous recevez ce courriel, car vous avez r�cemment modifi� vos param�tres de TeacherMatchTM.  Vous avez fait la modification suivante : <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre adresse �lectronique a �t� chang�e de"+oldEmailAdder+" �  <a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous vous remercions d'utiliser TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L��quipe du service � la client�le <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+". <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println("   4 "+sb.toString());
		
		return sb.toString();


	}
	
	
	public static String getSettingPWDChangeMailTextFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+":  Vous recevez ce courriel car vous avez r�cemment modifi� vos param�tres de TeacherMatchTM.  Vous avez fait la modification suivante : <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre mot de passe a �t� chang�.  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous vous remercions d'utiliser TeacherMatch!  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L��quipe du service � la client�le  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+". <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 5"+sb.toString());
		
		return sb.toString();

	}
	
	public static String getUserSettingEmailChangeMailTextFR(HttpServletRequest request,UserMaster userMaster, String oldEmailAdder)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+":  Vous recevez ce courriel car vous avez r�cemment modifi� vos param�tres de TeacherMatchTM.  Vous avez fait la modification suivante :  <br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre adresse �lectronique a �t� chang�e de "+oldEmailAdder+"  � <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+". <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 6"+sb.toString());
		
		return sb.toString();

	}
	
	public static String  getTeacherPDReportMailTextFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cher membre de l��quipe du service � la client�le,<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Un des membres de TeacherMatch a exprim� son int�r�t pour l'achat du Rapport de d�veloppement professionnel.  Les d�tails sont pr�sent�s ci-dessous :   <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date de la demande : "+new Date());
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nom du membre :  "+getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Adresse �lectronique du membre :  <a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("S'il vous pla�t, me contacter � <a href='mailto:admin@teachermatch.com'>admin@teachermatch.com</a> si vous avez des questions au sujet de ce courriel.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sinc�rement,<br/>L�administrateur TeacherMatch<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  votre administrateur �  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");



		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 8"+sb.toString());
		
		return sb.toString();
	}
	
	
	public static String  getTeacherMailTextFR(HttpServletRequest request,TeacherDetail teacherDetail,String defaultMessage)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(defaultMessage);
			sb.append("</td>");
			sb.append("</tr>");			


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Sincerely<br/>Administrateur TeacherMatch<br/><br/>");
			sb.append("<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur �  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 9"+sb.toString());
		
		return sb.toString();

	}
	
	
	public static String  getTeacherEPIResetMailTextFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre statut EPI (Inventaire de base) a �t� r�tabli.  Le changement a �t� fait pour lui donner un statut d'incomplet au lieu  de temps allou� d�pass�.  Vous pouvez maintenant reprendre votre EPI (inventaire de base) en vous connectant � la plate-forme de TeacherMatch. ");
			sb.append("<br><br>Avant de reprendre l'inventaire de base, s'il vous pla�t, notez les lignes directrices suivantes. Ces lignes directrices ont �t� mises en place pour veiller � ce que tous les candidats prennent l�inventaire de base dans des conditions �quitables.");
			sb.append("<br><br>Chaque �l�ment dans l'inventaire de base a une stipulation de limite de temps.  Vous devez r�pondre � chaque question en respectant les d�lais stipul�s.");
			
			
			sb.append("<br>Vous devez r�pondre � toutes les questions en une seule s�ance. ");
			sb.append("<br>Assurez-vous que vous avez au moins 90 minutes de temps ininterrompu pour compl�ter l�inventaire de base.  ");
			sb.append("<br>Assurez-vous que vous avez une connexion Internet stable et fiable.   ");
			sb.append("<br>Ne fermez pas votre navigateur ou cliquez sur le bouton retour de votre navigateur.  ");
			sb.append("<br>Il est impossible de sauter des questions.");
			sb.append("<br>Si vous ne respectez pas ces consignes, vous recevrez deux avertissements suppl�mentaires avant que votre statut devienne expir� � nouveau.   <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sincerely<br/>Administrateur TeacherMatch<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Si vous avez des questions au sujet de ces lignes directrices ou tout aspect du processus d'administration de l'inventaire de base, s'il vous pla�t, communiqu� avec le Service � la client�le de TeacherMatch  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	
	public static String  getTeacherFirstMailTextFR(TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous vous remercions de vous �tre enregistr� sur le site Web de TeacherMatch et profiter de l'occasion de faire partie de notre recherche innovatrice!  Nous vous remercions de votre temps et de votre consid�ration. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous avons remarqu� que vous n'avez pas authentifi� votre compte.  Nous vous recommandons fortement de prendre cette mesure. En vous authentifiant, vous vous joindrez aux �ducateurs �lites de partout � travers le pays qui ont compl�t� leur EPI et qui vont devenir les principaux candidats pour les possibilit�s d'emploi dans l'ensemble du Conseil scolaire. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Puisque nous h�bergeons les emplois sur notre site, ainsi, vous aurez �galement acc�s � des informations sur de nombreux d�bouch�s ainsi que la capacit� de postuler � de nombreuses positions en un seul endroit" +
					".  Pour plus amples informations sur l'ensemble des possibilit�s int�ressantes chez TeacherMatch, s'il vous pla�t, visitez  <a href='http://www.teachermatch.org'>www.teachermatch.org</a>.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			String url = Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Veuillez suivre le lien dans le courriel que vous avez re�u vous demandant de v�rifier votre adresse �lectronique et de vous connecter �  <a href='"+url+"'>"+url+"</a>" +
					". Vous ne trouvez pas le courriel d�authentification� aucun souci! Il suffit de visiter ce lien <>, entrer votre adresse �lectronique, nous vous enverrons un nouveau lien pour l'authentification.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Si votre profil n�est pas termin� dans les 30 jours qui suivent, votre compte sera d�sactiv�. Si vous avez des questions ou des pr�occupations au sujet du processus d'inscription, veuillez communiquer avec les Services � la client�le de TeacherMatch <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Cordialement,,<br/>L�administrateur TeacherMatch<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getTeacherFirstMailTextEPIRequestFR(TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Ch�rer "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");
			String url = Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous vous remercions de vous �tre enregistr� sur le site Web de TeacherMatch et profiter de l'occasion de faire partie de notre recherche innovatrice!  Nous vous remercions de votre temps et de votre consid�ration. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous avons remarqu� que vous n'avez pas authentifi� votre compte.  Nous vous recommandons fortement de prendre cette mesure. En vous authentifiant, vous vous joindrez aux �ducateurs �lites de partout � travers le pays qui ont compl�t� leur EPI et qui vont devenir les principaux candidats pour les possibilit�s d'emploi dans l'ensemble du Conseil scolaire.  Puisque nous h�bergeons les emplois sur notre site, ainsi, vous aurez �galement acc�s � des informations sur de nombreux d�bouch�s ainsi que la capacit� de postuler � de nombreuses positions en un seul endroit <a href='http://www.teachermatch.org'>http://www.teachermatch.org</a>. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Veuillez suivre le lien dans le courriel que vous avez re�u vous demandant de v�rifier votre adresse �lectronique et de vous connecter �  <a href='"+url+"'>"+url+"</a> Vous ne trouvez pas le courriel d�authentification� aucun souci! Il suffit de visiter ce lien <>, entrer votre adresse courriel, nous vous enverrons un nouveau lien pour l'authentification.  Si votre profil n�est pas termin� dans les 30 jours qui suivent, votre compte sera d�sactiv�. Si vous avez des questions ou des pr�occupations au sujet du processus d'inscription, veuillez communiquer avec les Services � la client�le de TeacherMatch  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Cordialement,,<br/>L�administrateur TeacherMatch<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getTeacherFirstMailTextEPICompletedFR(TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");
			String url = Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nous vous remercions d�avoir cr�� un profil sur le site Web de TeacherMatch,  profitant de l'occasion de faire partie de notre recherche innovatrice! Nous vous remercions de votre temps et de votre consid�ration.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Maintenant que vous avez compl�t� votre profil, vous pouvez postuler � des emplois de fa�on transparente � travers le site. Nous avons plus de 1000 offres d'emploi issues de Conseil soclaires et d'�coles � travers le pays. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Veuillez vous connecter � <url> pour postuler � des emplois. Si vous avez des questions ou des pr�occupations au sujet du processus d'inscription, communiquez avec les Services � la client�le au TeacherMatch  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Cordialement,,<br/>L�administrateur TeacherMatch<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Vous recevez ce courriel d'un syst�me automatis�. S'il vous pla�t,, ne pas r�pondre � ce courriel. Si vous avez besoin d'aide, veuillez communiquer avec les Services � la client�le au TeacherMatch  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getShareJobMailTextFR(HttpServletRequest request,String teacherFname, String teacherEmail, JobOrder jobOrder, String name, String email)
	{

		StringBuffer sb 					= 	new StringBuffer();
		String jobUrl 						=	Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId();		
		//String teacherLname				=	teacherDetail.getLastName()==null?"":teacherDetail.getLastName();		
		String schoolDistrictName			=	null;
		String schoolOrDistrictLocation		=	null;
		List<SchoolMaster> schoolList		=	null;

		try 
		{
			if(jobOrder.getCreatedForEntity()==2)
			{
				schoolDistrictName			=	jobOrder.getDistrictMaster().getDistrictName()==null?"":jobOrder.getDistrictMaster().getDistrictName();
				schoolOrDistrictLocation	=	jobOrder.getDistrictMaster().getAddress()==null?"":jobOrder.getDistrictMaster().getAddress();
			}
			if(jobOrder.getCreatedForEntity()==3)
			{
				schoolList					=	jobOrder.getSchool();
				for(SchoolMaster schoolDetails:schoolList)
				{
					schoolDistrictName		=	schoolDetails.getSchoolName()==null?"":schoolDetails.getSchoolName();
					schoolOrDistrictLocation=	schoolDetails.getAddress()==null?"":schoolDetails.getAddress();
				}
			}
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Je pensais que vous pourriez �tre int�ress� par cette opportunit�: "+jobOrder.getJobTitle()+" � "+schoolDistrictName+" dans "+schoolOrDistrictLocation+" affich�e au <a href='http://www.teachermatch.org/'>www.teachermatch.org</a>.   Cliquez sur ce lien pour en savoir d�avantage <a href="+jobUrl+">"+jobUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<First Name of Person Sharing Job>"+teacherFname+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me.  Si vous avez des questions, veuillez communiquer avec la personne qui a partag� cette offre d�emploi avec vous �  <a href='mailto:"+teacherEmail+"'>"+teacherEmail+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String mailtoUsersAfterJobApplyFR(HttpServletRequest request,TeacherDetail teacherDetail,JobOrder jobOrder,String[] arrHrDetail,UserMaster userMaster)
	{

		
		StringBuffer sb 					= 	new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 		= arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		String cgUrl		 = arrHrDetail[11];
		String jobUrl 						=	Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId();	
		String statusShortName	= arrHrDetail[14];
		List<SchoolMaster> schoolList		=	null;
		System.out.println("Inside mailtoUsersAfterJobApply :::::::");
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+",<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			String districtName="";
			
			if(jobOrder.getDistrictMaster()!=null){
				if(jobOrder.getDistrictMaster().getDisplayName()!=null && !jobOrder.getDistrictMaster().getDisplayName().equals("")){
					districtName=jobOrder.getDistrictMaster().getDisplayName();
				}else{
					districtName=jobOrder.getDistrictMaster().getDistrictName();
				}
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Candidat potentiel "+getTeacherName(teacherDetail)+" r�cemment postul� pour le poste de "+jobOrder.getJobTitle()+" � "+districtName+". Vous pouvez visualiser la grille du candidat pour ce poste � <a href='"+cgUrl+"'>"+cgUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			if(statusShortName!=null && !statusShortName.equals("")){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				
					if(statusShortName.equalsIgnoreCase("comp"))
						sb.append("Le statut du EPI du candidat est termin�.<br/><br/>");
					else
						if(statusShortName.equalsIgnoreCase("vlt"))
						sb.append("Le statut du EPI du candidat est expir�. <br/><br/>");
					else
						if(statusShortName.equalsIgnoreCase("icomp"))
						sb.append("Le candidat n'a pas commenc� son EPI.<br/><br/>");
				
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sinc�rement,<br/>Le Service � la client�le de TeacherMatch �<br/><br/>");
			
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Vous recevez ce courriel � partir d'un syst�me automatis�. Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au 8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
			//System.out.println("\n\n  Gagan:\n>>>>>>>>>mailtoUsersAfterJobApply "+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String getJobAlertTextFR(JobAlerts jobAlerts, List<JobOrder> lstJobOrders, HttpServletRequest request)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			//String loginUrl = Utility.getBaseURL(request)+"signin.do";
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+jobAlerts.getSenderName()+",");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Vous recevez ce courriel parce que vous avez manifest� un int�r�t pour les possibilit�s d'emploi � partir de TeacherMatch. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("D�couvrez les emplois qui peuvent vous int�resser : <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			String cityName="";
			String stateName="";
			String joburl = "";	
			for(JobOrder jobOrder: lstJobOrders){
				joburl = Utility.getBaseURL(request)+"applyteacherjob.do?jobId="+jobOrder.getJobId();
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+" style='padding-top: 10px;'>");
				sb.append(""+jobOrder.getJobTitle());
				if(jobOrder.getCreatedForEntity().equals(3)){
					cityName=jobOrder.getSchool().get(0).getCityName()==null?"":jobOrder.getSchool().get(0).getCityName();
					stateName=jobOrder.getSchool().get(0).getStateMaster().getStateName()==null?"":jobOrder.getSchool().get(0).getStateMaster().getStateName();
					sb.append("<br/>"+cityName);
					if(cityName.equals("")){
						sb.append(stateName);
					}
					else{
						sb.append(", "+stateName);
					}	
					sb.append("<br/><a href='"+joburl+"'>Visualiser les d�tails </a>");					
				}
				else{
					cityName = jobOrder.getDistrictMaster().getCityName()==null?"":jobOrder.getDistrictMaster().getCityName();
					stateName = jobOrder.getDistrictMaster().getStateId().getStateName()==null?"":jobOrder.getDistrictMaster().getStateId().getStateName();
					sb.append("<br/>"+cityName);
					if(cityName.equals("")){
						sb.append(stateName);
					}
					else{
						sb.append(", "+stateName);
					}
					sb.append("<br/><a href='"+joburl+"'>Visualiser les d�tails</a>");
				}
				sb.append("</td></tr>");
			}



			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Nous vous remercions d�avoir utilis� TeacherMatch! <br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L��quipe du service � la client�le<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Vous recevez ce courriel � partir d'un syst�me automatis�. Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getCGMailTextFR(List<JobOrder> jobOrders)
	{
StringBuffer sb = new StringBuffer();
		
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour, <br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("La grille du candidat pour le poste ouvert suivant est disponible.");
			sb.append("<ul>");
			String url = Utility.getValueOfPropByKey("basePath");
			for (JobOrder jobOrder : jobOrders) {
				sb.append("<li><a href='"+url+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity()+"'>"+jobOrder.getJobTitle()+"</a></li>");
			}
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Pour acc�der et consulter la grille du candidat, veuillez-vous connecter ici. <a href='"+url+"'>here</a>");
			sb.append("<br/>Veuillez noter que nous allons continuer d'envoyer cette notification �lectronique hebdomadaire pour l'ensemble de vos ouvertures de postes jusqu'� ce qu'ils soient combl�s. ");
			sb.append("<br/> Des informations suppl�mentaires sur la grille du candidat sont incluses dans la foire aux questions ci-dessous.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Cordialement,<br/>L'administrateur TeacherMatch<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Foire aux questions : <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>Comment puis-je utiliser le rapport de la grille du candidat sur la plateforme TeacherMatch? Nous vous sugg�rons de combiner l'information dans le rapport GC avec les mat�riaux du candidat potentiel que vous avez pour chaque candidat afin de vous aider � prioriser qui passer en entrevue pour votre ouverture de poste. Veuillez noter que cette liste ne refl�te pas tous les candidats qui ont postul� � votre poste, seulement ceux qui ont choisi de participer au EPI. Votre GC changera de semaine en semaine lorsque plusieurs candidats participent au EPI. ");
			sb.append("<br/><br/></td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>Que signifient les couleurs? Le pourcentage figurant dans chaque colonne refl�te le pourcentage du candidat exact par domaine. Le pointage compos� tient compte de la pond�ration vari�e par domaine et cr�e un pointage compos� pour une performance globale dans les trois domaines. Les pointages de candidats (en pourcentage) dans chaque domaine sont statiques apr�s l'ach�vement du EPI et ne changeront pas. Le codage de couleur du candidat peut changer � travers les offres d'emploi en fonction de la qualit� de chaque bassin de candidats. La couleur verte indique que le candidat a marqu� mieux que la plupart des autres candidats qui ont postul�s pour ce poste. Le jaune indique que le candidat a re�u environ le m�me pointage et le rouge indique le candidat a obtenu un pointage qui est inf�rieur� la plupart des candidats. ");
			sb.append("<br/><br/></td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>What if additional candidates complete the EPI this week - Do I have to wait until next week to get an updated notification?</b> You can log into the <a href='"+url+"'>TeacherMatch platform</a> at any time to get a live Candidate Grid report.");
			sb.append("<br/><br/></td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>This position is already filled - why am I getting this notification?</b> It sometimes takes our systems a few days to synch up. You should not receive this report again next week for this same position. If you do, just let your HR team leader know so we can correctly fill your position in MyApp.");
			sb.append("<br/><br/></td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<b>Who do I contact with questions:</b> " +
					"<ul>  " +
					"<li>For technical questions about accessing the TeacherMatch platform contact TeacherMatch client services.</li>"+
					"<li>For other questions contact your HR team leader or the HR Sourcing & Onboarding team (980-343-1848).</li>"+
					"</ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Vous recevez ce courriel d'un syst�me automatis�. S'il vous pla�t,, ne pas r�pondre � ce courriel. Si vous avez besoin d'aide, veuillez communiquer avec les Services � la client�le au TeacherMatch  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String mailToTeacherForEpiOrNoRecordFR(String bitlyURL,UserMaster userSession,TeacherDetail teacherDetail,String jobTitle,boolean newUser,int mailType,String schoolDistrictName)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			//Get temporary base URL
			String loginUrl = teacherDetail.getIdentityVerificationPicture()+"signin.do";
			
			if(mailType==0){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getTeacherName(teacherDetail)+"<br/><br/> Nous vous remercions de votre int�r�t � �tre un enseignant avec "+schoolDistrictName+". La prochaine �tape dans le processus est pour vous de compl�ter l'inventaire professionnel de l'�ducateur (EPI) tel que demand� par le Service des ressources humaines du "+schoolDistrictName+" Les demandes sont examin�es sur une base quotidienne.  Nous vous encourageons de compl�ter le EPI d�s que vous �tes disponible.  <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Veuillez noter les points suivants : ");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li> Si vous �tes un enseignant certifi� d�j� l�embauche du "+schoolDistrictName+" veuillez v�rifier avec le Conseil scolaire ou vous poser votre candidature � savoir si vous �tes tenu de prendre le EPI.  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Pour une liste des questions fr�quemment pos�es au sjuet du EPI, cliquez sur le lien suivant:  <a href='"+bitlyURL+"'>"+bitlyURL+"</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Avant de commencer le EPI, veuillez noter que chaque �l�ment de l'inventaire stipule une limite de temps.  Vous devez r�pondre � chaque question au sein du d�lai stipul�.�Vous devez r�pondre � toutes les questions en une seule s�ance.�Si vous continuez, soyez pr�t � suivre ces lignes directrices:");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>R�servez au moins 90 minutes de temps ininterrompu pour compl�ter l�inventaire de base.  </br></li><li>Utiliser une connexion Internet stable et fiable.  </br></li><li>Ne fermez pas votre navigateur ou cliquez sur le bouton retour de votre navigateur.  </br></li><li>Il est impossible de sauter des questions.</li><li>En prenant le EPI, vous acceptez les conditions d'utilisation de TeacherMatch.</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				/*sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If you do not follow these guidelines, your status may become \"timed out\" and you will not be able to complete the EPI for twelve months.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");*/
				
				if(newUser){
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("PROCHAINE �TAPE : D�marrer le EPI en cliquant sur le lien ci-dessous et en utilisant les informations de connexion fournies.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<a href='"+loginUrl+"'>"+loginUrl+"</a><br/> Adresse �lectronique : "+teacherDetail.getEmailAddress()+"<br/>Mot de passe : "+teacherDetail.getPassword()+"<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}else{
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("PROCHAINE �TAPE: D�marrer le EPI en cliquant sur le lien ci-dessous et en utilisant les informations de connexion fournies<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<a href='"+loginUrl+"'>"+loginUrl+"</a><br/> Adresse �lectronique�:"+teacherDetail.getEmailAddress()+"<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
				}
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Si vous avez des questions au sujet de ce processus, communiquez avec le Service � la client�le au TeacherMatch  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getTeacherName(teacherDetail)+"<br/><br/> Nous vous remercions de votre int�r�t pour le poste de "+jobTitle+"  au "+schoolDistrictName+".Comme vous avez d�j� termin� l'inventaire professionnel de l'�ducateur (EPI, votre demande est compl�te et vous ne devez pas prendre d'autres mesures en ce qui concerne le EPI.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement,<br/>Le service � la client�le TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
		
	}
	
	public static String getCancelledEvntMailTextFR(HttpServletRequest request,DemoClassSchedule demoClassSchedule,DemoClassAttendees demoClassAttendees,List<DemoClassAttendees> demoClassAttendeesList)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = demoClassSchedule.getTeacherId();
			UserMaster userMaster = demoClassAttendees.getInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Veuillez noter que la d�monstration de la le�on ci-dessous a �t� annul�e.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat :");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Date/heure de la d�monstration de la le�on : ");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat()+" "+demoClassSchedule.getTimeZoneId().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Lieu de la d�monstration : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoClassAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Commentaires :");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Participants :  ");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<demoClassAttendeesList.size();i++)
			{
				UserMaster um = demoClassAttendeesList.get(i).getInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement,<br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	public static String getRemovedAttendeeMailTextFR(HttpServletRequest request,DemoClassSchedule demoClassSchedule,DemoClassAttendees demoClassAttendees,List<DemoClassAttendees> demoClassAttendeesList)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = demoClassSchedule.getTeacherId();
			UserMaster userMaster = demoClassAttendees.getInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Vous n'�tes plus sur la liste des participants pour la d�monstration de la le�on ci-dessous. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("date/heure de la d�monstration de la le�on : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat()+" "+demoClassSchedule.getTimeZoneId().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Lieu de la d�monstration :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoClassAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Commentaires :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Participants :  ");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<demoClassAttendeesList.size();i++)
			{
				UserMaster um = demoClassAttendeesList.get(i).getInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement<br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();

	}
	
	
	public static String getNewAttendeeMailTextFR(HttpServletRequest request,DemoClassSchedule demoClassSchedule,DemoClassAttendees demoClassAttendees,List<DemoClassAttendees> demoClassAttendeesList,boolean brandNew)
	{


		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = demoClassSchedule.getTeacherId();
			UserMaster userMaster = demoClassAttendees.getInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(brandNew)
				sb.append("Si (brandNew) une nouvelle d�monstration de la le�on est program�e.  D�tails ci-dessous : <br/><br/>");
			else
				sb.append("Vous avez une autre d�monstration de la le�on de programm�e.  D�tails ci-dessous :<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Date/heure de la d�monstration de la le�on: ");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(demoClassSchedule.getDemoDate())+" "+demoClassSchedule.getDemoTime()+" "+demoClassSchedule.getTimeFormat()+" "+demoClassSchedule.getTimeZoneId().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Lieu de la d�monstration :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoClassAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Commentaires :  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(demoClassSchedule.getDemoDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Participants :  ");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<demoClassAttendeesList.size();i++)
			{
				UserMaster um = demoClassAttendeesList.get(i).getInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement,<br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	
	public static String getWithdrawMailToTeacherFR(HttpServletRequest request,TeacherDetail teacherDetail,JobOrder jobOrder,String[] arrHrDetail)
	{
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String hrContactEmailAddress = arrHrDetail[2];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmEmailAddress = arrHrDetail[8];
		String dmPhoneNumber	 = arrHrDetail[9];
		
		String jobordertype = arrHrDetail[16];	
		String joborderwithSingleSchoolAttachFlag = arrHrDetail[17];	
		String attachSchoolName	=	arrHrDetail[18];
		String districtNameforSJO	= arrHrDetail[19];
		if(jobOrder.getDistrictMaster().getDistrictId().equals(1201470)){
			dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
		}
		try 
		{
			//String loginUrl = Utility.getBaseURL(request)+"signin.do";
			String loginUrl = arrHrDetail[10];
			System.out.println(" login Url "+loginUrl);
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear <First Name>"+teacherDetail.getFirstName()+": Welcome. You have now completed the registration process for TeacherMatch<sup>TM</sup>. At TeacherMatch, we take pride in helping candidates find their best-fit roles. We also believe that great educators need actionable support, which is why we offer a customized professional development report, available after you have completed your Portfolio and the Base Inventory.<br/><br/>");
			sb.append("Cher "+getTeacherName(teacherDetail)+",<br/>Vous recevez ce courriel parce que vous avez retir� votre demande pour le poste de  "+jobOrder.getJobTitle()+"");
			
			/*==== [schoolDistrictName] will have value of district name if joborder is type 2[ DJO ] else School Name if jobordertype 3 [SJO] =========*/
			if(joborderwithSingleSchoolAttachFlag.equals("1"))
			{
				System.out.println(" If DJO with only  1 school Attach");
				sb.append(" position, "+attachSchoolName+" at the "+schoolDistrictName+".");
			}
			else
				if(jobordertype.equals("3"))
				{
					System.out.println("Else If [ SJO ]");
					sb.append(" position, "+schoolDistrictName+" at the "+districtNameforSJO+".");
				}
				else
				{
					System.out.println("Else Djo with Multiple School Attach : ");
					sb.append(" position at the "+schoolDistrictName+".");
				}
			sb.append(" Nous sommes d��us, mais si vous changez d'avis, vous pouvez toujours postuler de nouveau en vous connectant � votre compte �  <a href='"+loginUrl+"'>"+loginUrl+"</a> et en cliquant sur le lien Postuler pour l�offre d�emploi pr�cit�e.  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Si vous avez des questions ou si vous rencontrez des probl�mes techniques, contactez-nous �  <a href='mailto:clientservices@teachermatch.net'>clientservices</a> ou appelez  8559800511.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sinc�rement, <br/>"+schoolDistrictName+"  L��quipe de recrutement et de s�lection <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sincerely<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sincerely<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".");
			sb.append("</tr>");
              
			sb.append("</table>");
          
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" 4"+sb.toString());
		
		return sb.toString();
	}
	
	public static String internalCandidateTransferMailTextFR(String basePath,TeacherDetail teacherDetail,JobOrder jobOrder,List<SchoolMaster> schools,String[] arrHrDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			String hrContactFirstName = arrHrDetail[0];
			String hrContactLastName = arrHrDetail[1];
			String hrContactEmailAddress = arrHrDetail[2];
			String phoneNumber			 = arrHrDetail[3];
			String title		 = arrHrDetail[4];
			int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
			String schoolDistrictName =	arrHrDetail[6];
			String dmName 			 = arrHrDetail[7];
			String dmEmailAddress = arrHrDetail[8];
			String dmPhoneNumber	 = arrHrDetail[9];
			
			String jobordertype = arrHrDetail[16];	
			String joborderwithSingleSchoolAttachFlag = arrHrDetail[17];	
			String attachSchoolName	=	arrHrDetail[18];
			String districtNameforSJO	= arrHrDetail[19];
			
			
			if(jobOrder.getDistrictMaster().getDistrictId().equals(1201470)){
				
				dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
			}

			DistrictMaster districtMaster = jobOrder.getDistrictMaster();
			String displayName = (districtMaster.getDisplayName()==null || districtMaster.getDisplayName().trim().length()==0)?districtMaster.getDistrictName():districtMaster.getDisplayName();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Une occasion r�cente � "+displayName+" correspondu � vos pr�f�rences de transfert internes. Les d�tails sont ci-dessous: <br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Titre du poste:  ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(jobOrder.getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr style="+mailCssFontSize+">");
			if(schools.size()>0)
			{
				sb.append("<td>");
				sb.append("�cole: ");
				sb.append("</td>");
				sb.append("<td>");
				String sch="";
				for(SchoolMaster school: schools)
				{
					//String name = school.getDisplayName()==null?school.getSchoolName():districtMaster.getDisplayName();
					String name = (school.getDisplayName()==null || school.getDisplayName().trim().length()==0)?school.getSchoolName():school.getDisplayName();
					sch = sch+name+", ";
				}
				sb.append(sch.substring(0, sch.length()-1));
				sb.append("</td>");
			}else
			{
				sb.append("<td>");
				sb.append("Conseil scolaire : sb.append  ");
				sb.append("</td>");
				sb.append("<td>");
				sb.append(displayName);
				sb.append("</td>");
			}
			sb.append("</tr>");
		    sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			String joburl = basePath+"/"+"applyteacherjob.do?jobId="+jobOrder.getJobId();
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Pour postuler, cliquez sur <a href='"+joburl+"'>"+joburl+"</a><br><br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Pour toutes questions, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez 1-800-956-2861.<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sinc�rement, <br/>"+schoolDistrictName+" L��quipe de recrutement et de s�lection de l'�cole <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sinc�rement<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(schoolDistrictName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sinc�rement<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(schoolDistrictName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");
			System.out.println(sb.toString());

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getNewPanelAttendeeMailTextFR(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList,boolean brandNew,String externalURL)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(brandNew)
				sb.append("Un nouveau comit� a �t� organis�. D�tails ci-dessous: <br/><br/>");
			else
				sb.append("Vous disposez d'un comit�. D�tails ci-dessous: <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nom du candidat : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(getTeacherName(teacherDetail));
			sb.append(" "+candidateEmail);
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date / heure du comit� : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Lieu du comit� : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Commentaires : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			try{
				if(panelSchedule.getPanelDescription()!=null){
					sb.append(panelSchedule.getPanelDescription());
				}else{
					sb.append("");
				}
			}catch(Exception e){sb.append("");}
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Participants : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			if(externalURL!=null)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br>Cliquez sur le lien suivant pour donner le pointage au candidat. <br/><br/><a href='"+externalURL+"'>"+externalURL+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement,<br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur �  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	
	public static String getRemovedPanelAttendeeMailTextFR(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Vous n'�tes plus sur la liste des participants du comit� ci-dessous. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Date / heure du comit� : ");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Lieu du comit� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Commentaires :");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Participants : ");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement,<br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur � <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();

	}
	
	public static String getExistingPanelAttendeeMailTextFR(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Veuillez noter que des modifications ont �t� apport�es � l'horaire de la d�monstration de la le�on pr�vue.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Date / heure du comit� : ");
			sb.append("</td>");
			sb.append("<td>");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Lieu du comit� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Commentaires : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Participants : ");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement,<br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur �  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getCancelledPanelEvntMailTextFR(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour"+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Veuillez noter que le comit� a �t� annul�.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Nom du candidat :");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(getTeacherName(teacherDetail));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Date / heure du comit� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Lieu du comit� : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Commentaires : ");
			sb.append("</td>");
			sb.append("<td>");
			sb.append(panelSchedule.getPanelDescription());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Participants : ");
			sb.append("</td>");
			sb.append("<td>");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement,<br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur �  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getMailToInternalTeacherFR(String loginURL,TeacherDetail teacherDetail,String jobTitle,String[] arrHrDetail)
	{

		System.out.println("getMailToInternalTeacher::::");
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmPhoneNumber	 = arrHrDetail[9];
		
		
		

		
		try 
		{
			String DistrictDName = arrHrDetail[10];
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/><br/>Nous vous remercions d�avoir postul� pour le poste de "+jobTitle+" au "+DistrictDName+".</br></br>");
			sb.append("Pour compl�ter votre demande pour le poste de "+jobTitle+" au "+DistrictDName+",  nous aimerions que vous compl�tiez l�inventaire professionnel des �ducateurs (EPI).</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
					
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Voici les �tapes simples pour pour compl�ter votre EPI : <br/><br/>");
			sb.append("<ol><li>S'il vous pla�t, connectez vous � votre compte TeacherMatch </br></li><li>Votre ID de connexion est  <a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a></br></li><li>YVotre mot de passe est le mot de passe que vous avez d�fini pour TeacherMatch </br></li><li>Apr�s la connexion, s'il vous pla�t, aller � Tableau de bord</li><li>Veuillez trouver l'�l�ment EPI (inventaire professionnel de l��ducateur) sous Jalons.</li><li>Votre EPI devrait faire preuve d'un statut �incomplet� et juste � c�t�, il devrait y avoir une ic�ne pour d�marrer et terminer le EPI. S'il vous pla�t, cliquez sur cette ic�ne pour compl�ter votre EPI.</li><li>Vous pouvez vous connecter � TeacherMatch ici   <a href='"+loginURL+"'>here</a></li></ol>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices</a>  ou t�l�phonez au"+mailContactNo+".<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(isHrContactExist==0)
				sb.append("Sinc�rement, <br/>"+schoolDistrictName+" L��quipe de recrutement et de s�lection de <br/><br/>");
			else 
				if(isHrContactExist==1)
				{
					sb.append("Sinc�rement<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
					if(title!="")
					sb.append(title+"<br/>");
					if(phoneNumber!="")
						sb.append(phoneNumber+"<br/>");
					sb.append(DistrictDName+"<br/><br/>");
				}
				else
				{
					if(isHrContactExist==2)
					{
						sb.append("Sinc�rement<br/>"+dmName+"<br/>");
						if(dmPhoneNumber!="")
							sb.append(dmPhoneNumber+"<br/>");
						sb.append(DistrictDName+"<br/><br/>");
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices</a>  ou t�l�phonez au "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String getMailToSendJsiInvitesFR(String loginURL,TeacherDetail teacherDetail,String[] arrHrDetail,JobOrder jobOrder)
	{

		
		System.out.println("getMailToInternalTeacher::::");
		StringBuffer sb = new StringBuffer();
		String hrContactFirstName = arrHrDetail[0];
		String hrContactLastName = arrHrDetail[1];
		String phoneNumber			 = arrHrDetail[3];
		String title		 = arrHrDetail[4];
		int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
		String schoolDistrictName =	arrHrDetail[6];
		String dmName 			 = arrHrDetail[7];
		String dmPhoneNumber	 = arrHrDetail[9];
		String jobTitle = jobOrder.getJobTitle();
		String dmEmail	 = arrHrDetail[8];
		String hrEmail	 = arrHrDetail[2];
		
		if(jobOrder.getDistrictMaster().getDistrictId().equals(1201470)){
			dmName=Utility.getValueOfPropByKey("sincerelyNameForOSCEOLA");
		}
		try 
		{
			String DistrictDName = arrHrDetail[10];
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/><br/>Nous vous remercions d�avoir postul� pour le poste de "+jobTitle+" au "+DistrictDName+".</br></br>");
			sb.append("&nbsp;Pour compl�ter votre demande pour le poste de"+jobTitle+" au "+DistrictDName+",  nous aimerions que vous pour compl�tiez �galement l�inventaire sp�cifique � l'emploi (ISE).</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
					
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("S'il vous pla�t, connectez vous � votre compte TeacherMatch <br/><br/>");
			if(jobOrder.getJobAssessmentStatus()==2)
			{
				//send a link
				String baseURL=Utility.getValueOfPropByKey("basePath");
				String forMated = "";
				String tchrId=Utility.encryptNo(teacherDetail.getTeacherId());
				String jobId=Utility.encryptNo(jobOrder.getJobId());
				String linkIds= tchrId+"###"+jobId;
				try {
					forMated = Utility.encodeInBase64(linkIds);
					forMated = baseURL+"tmjsi.do?id="+forMated;
				} catch (Exception e) {
					e.printStackTrace();
				}	
				sb.append("Cliquez sur la URL <a href='"+forMated+"'>"+forMated+"</a> <br><br>");
			}else
				sb.append("<ol><liS'il vous pla�t, connectez vous � votre compte TeacherMatch </br></li><li>Votre ID de connexion est  <a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a></br></li><li>Votre mot de passe est le mot de passe que vous avez d�fini pour TeacherMatch </br></li><li>Apr�s la connexion, s'il vous pla�t, allez � Tableau de bord </li><li>Veuillez trouver l'�l�ment ISE (inventaire sp�cifique � l'emploi) sous Jalons.</li><li>Votre ISE devrait faire preuve d'un statut �incomplet� et juste � c�t�, il devrait y avoir une ic�ne pour d�marrer et terminer le ISE. S'il vous pla�t, cliquez sur cette ic�ne pour compl�ter votre ISE</li><li>Pour toutes questions suppl�mentaires, contactez <a href='"+loginURL+"'>here</a></li></ol>");
			
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("If you have any questions or if you are having any technical issues, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices</a> or call "+mailContactNo+".<br/><br/>");
			sb.append("Pour toutes questions suppl�mentaires, contactez  <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a> ou t�l�phonez au "+mailContactNo+".<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			
			/**
			*Changes by Amit for changing the signature of Philadelphia
			*@Date: 10-Mar-15
			*/
			if(isHrContactExist==0)
			{
				if(jobOrder.getDistrictMaster().getDistrictId()!=null && !jobOrder.getDistrictMaster().getDistrictId().equals("") && jobOrder.getDistrictMaster().getDistrictId()!=0 && jobOrder.getDistrictMaster().getDistrictId()==4218990)
					sb.append("Sinc�rement,<br/>Bureau de talent <br/><a href='mailto:principalschoosephilly@philasd.org'>principalschoosephilly@philasd.org</a><br/><br/>");
				else
					sb.append("Sinc�rement<br/>"+schoolDistrictName+" L'�quipe de recrutement et de s�lection de  <br/><br/>");
			}
			else 
				if(isHrContactExist==1)
				{
					if(jobOrder.getDistrictMaster().getDistrictId()!=null && !jobOrder.getDistrictMaster().getDistrictId().equals("") && jobOrder.getDistrictMaster().getDistrictId()!=0 && jobOrder.getDistrictMaster().getDistrictId()==4218990)
						sb.append("Sinc�rement<br/>Bureau de talent  <br/><a href='mailto:principalschoosephilly@philasd.org'>principalschoosephilly@philasd.org</a><br/><br/>");
					else
					{
						sb.append("Sinc�rement<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
						if(title!="")
						sb.append(title+"<br/>");
						if(hrEmail!="")
						sb.append(hrEmail+"<br/>");	
						if(phoneNumber!="")
							sb.append(phoneNumber+"<br/>");
						sb.append(DistrictDName+"<br/><br/>");
					}
				}
				else
				{
					if(isHrContactExist==2)
					{
						if(jobOrder.getDistrictMaster().getDistrictId()!=null && !jobOrder.getDistrictMaster().getDistrictId().equals("") && jobOrder.getDistrictMaster().getDistrictId()!=0 && jobOrder.getDistrictMaster().getDistrictId()==4218990)
							sb.append("Sinc�rement<br/>Bureau de talent<br/><a href='mailto:principalschoosephilly@philasd.org'>principalschoosephilly@philasd.org</a><br/><br/>");
						else
						{
							sb.append("Sinc�rement<br/>"+dmName+"<br/>");
							if(dmEmail!="")
								sb.append(dmEmail+"<br/>");	
							if(dmPhoneNumber!="")
								sb.append(dmPhoneNumber+"<br/>");
							sb.append(DistrictDName+"<br/><br/>");
						}
					}
				}
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:clientservices@teachermatch.net'>clientservices</a>  ou t�l�phonez au  "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		//System.out.println("sb::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String getPanelEmailBodyFR(String toEmail,String sEmailSubject,String sPanelUserName,String sPanelUserNameTo,String sTeacheName,String sJobTitle,String sStatusName,UserMaster userMaster)
	{

StringBuffer sb = new StringBuffer();
		
		try 
		{
			sb.append("<table>");
			
			
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+sPanelUserNameTo+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(sPanelUserName+"  a mis "+sStatusName+"  � "+sJobTitle+"  � "+sTeacheName+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				/*sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sincerely<br/>");
				sb.append("Ramesh");
				sb.append("</td>");
				sb.append("</tr>");*/
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>  ou t�l�phonez au "+mailContactNo+".");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::"+sb);
		return sb.toString();
	}
	
	
	public static String getPCEmailBodyFR(TeacherDetail teacherDetail)
	{

StringBuffer sb = new StringBuffer();
		
		try 
		{
				sb.append("<table>");
			
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Miami-Dade County Public Schools politique du Conseil 3140, les �tats, Aucune personne qui ont �t� s�par�s de l'emploi de la Commission pour la cause doit n'�tre employ� � nouveau dans un minist�re sur aucune base � moins qu'une demande sp�ciale pour ce faire n'ait �t� approuv�e par le Conseil. En cons�quence, vous n'�tes pas admissible � �tre embauch� � nouveau avec le Conseil scolaire.  Nous vous souhaitons du succ�s dans ses projets.   ");
				sb.append("</td>");
				sb.append("</tr>");
				 
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Le Bureau de la gestion du capital humain  ");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement,  <br/><br/>");
				sb.append("Le Bureau de la gestion du capital humain <br/>");
				sb.append("Miami-Dade County Public Schools  ");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Les informations contenues dans ce message ce courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumisee � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::"+sb);
		return sb.toString();
	}
	
	public static String getRestrictedEmailBodyFR(TeacherDetail teacherDetail)
	{

StringBuffer sb = new StringBuffer();
		
		try 
		{
				sb.append("<table>");
			
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Merci d�avoir postul� pour un nouveau poste avec Miami-Dade County Public Schools. Nos dossiers indiquent que la position � laquelle vous avez postul� est restreinte et/ou non dans le cadre de la position que vous occupez actuellement; donc, s'il vous pla�t, communiquez avec le Bureau des normes professionnelles au (305) 995-7120 afin de d�terminer si vous �tes admissible pour la position que vous recherchez. ");
				sb.append("</td>");
				sb.append("</tr>");
				 
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Nous vous remercions de votre d�vouement continu au Conseil scolaire. ");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement,  <br/><br/>");
				sb.append("Le Bureau de la gestion du   <br/>");
				sb.append("capital humain  Miami-Dade County Public Schools");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::"+sb);
		return sb.toString();
	}
	public static String getFPRejectMailFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{

		
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+":<br/><br/> Nous vous remercions de votre int�r�t � travailler pour Miami-Dade County Public Schools . Nous vous  informons que vos empreintes digitales ont �t� rejet�es par le Federal Bureau of Investigation (FBI) pour illisibilit� ou d'autres probl�mes techniques. Afin de continuer le processus, vous devez retourner pour reprendre vos empreintes digitales dans les 15 jours ouvrables.  Il n'y a pas de frais suppl�mentaires pour le service. Vous devez pr�senter une pi�ce d'identit� valide avec photo, comme un permis de conduire, passeport, carte d'identit�. S'il vous pla�t, appelez 305-995-7472 si vous avez besoin d'informations suppl�mentaires.  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement,  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Le Bureau de la gestion du <br> capital humain  Miami-Dade County Public Schools  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			//System.out.println(sb);
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}

	public static String getFPFailMailFR(HttpServletRequest request,TeacherDetail teacherDetail,TeacherPersonalInfo teacherPersonalInfo)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			String city 		= 	"";
			String state 		= 	"";
			String zip 			= 	"";
			String address1		=	"";
			String address2		=	"";
			String address		=	"";
			//String firstName	=	"";
			String addressline2	=	"";
			if(teacherPersonalInfo.getCityId() != null){
				city	=	teacherPersonalInfo.getCityId().getCityName();
				state	=	teacherPersonalInfo.getStateId().getStateName();
				zip		=	teacherPersonalInfo.getCityId().getZipCode();	
			}
			if(teacherPersonalInfo != null){
				address1	=	teacherPersonalInfo.getAddressLine1();
				address2	=	teacherPersonalInfo.getAddressLine2();
				address		=	address1+", "+address2;
				//firstName	=	teacherDetail.getFirstName();
				addressline2	=	city+","+state+","+zip;
			}
			sb.append("<table>");
			System.out.println("NAME="+teacherPersonalInfo.getCityId());
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(getTeacherName(teacherDetail)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(address+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(addressline2+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Le Bureau d'empreintes digitales a re�u les r�sultats de l'analyse de l'empreinte de la Florida Department of Law Enforcement et le Federal Bureau of Investigation refl�tant un incident qui a conduit � une action en justice. Veuillez noter pour que pour continuer le processus, vous devez fournir les documents suivants:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul><li>Rapport d'incident (rapport d'enqu�te initiale de la police) ou le rapport d�arrestation / affidavit (affidavit du rapport d'enqu�te criminelle)</br></li><li>Informations ou mise en accusation (accusations formelles d�pos�es par le procureur aupr�s du tribunal) </li><li>Adjudication de la Cour (la d�cision du tribunal de votre cas) </li><li>Documentation de la r�ussite de la probation ou une intervention pr�ventive et le paiement de l'amende, le cas �ch�ant </li><li>Une d�claration notari�e �crite et sign�e par vous expliquant les circonstances de l'arrestation </li></ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("S'il vous pla�t, r�pondez � ce message si vous acceptez cette affectation avec Miami-Dade County Public Schools et contactez l'�cole pour obtenir des informations sur les prochaines �tapes. Cette offre est nulle si vous ne r�pondez pas dans les 2 jours ouvrables. Nous sommes impatients de vous accueillir � Miami-Dade County Public Schools.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>Toutes les photocopies de documents ou de certification par le greffier de la Cour judiciaire doivent �tre des originaux, des copies certifi�es conformes. Vous devriez communiquer avec le greffier de la Cour dans la ville ou du comt� o� l'affaire a �t� dispos�e. Les documents doivent �tre soumis dans les 15 jours ouvrables  � partir de la date de ce courriel � l'adresse:  </B><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("from the date of this email to the address:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Miami-Dade Unit� Police �coles d'empreintes digitales � 1450 NE � 2nd Ave Suite 110 � Miami, FL 33132<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement,,<br/><br/>Sigilenda Miles <br> Directeur g�n�ral de dactyloscopie <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getDTRejectMailFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L'analyse de votre d�pistage des drogues pr�alables � l'emploi a �t� confirm�e comme �tant positive. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Conseil d'orientation 1124, 3124, 4124 l'�cole, en milieu de travail sans drogue, d�clare: ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Les candidats seront inform�s � l'avance de l'exigence d'un d�pistage de drogues n�gatif comme condition d'emploi.</br> testing positive will not be eligible for employment by Miami-Dade</br> County Public Schools for three (3) years from the date of the test.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Les candidats qui auront obtenu un test positif ne seront pas �ligibles � l'emploi par Miami-Dade County Public Schools pour une dur�e de trois (3) ans � compter de la date du test.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Le Bureau de la gestion du capital humain br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modifier au contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getFirstPanelMemberFR(String sJobTitle,String sTeacheName,String schoolLocation,String requisitionNumber)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nom de l'�cole : "+schoolLocation+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nom du postulant : "+sTeacheName+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Poste : "+sJobTitle+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Num�ro du poste : "+requisitionNumber+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb:::"+sb);
		return sb.toString();
	}
	
	public static String getSecondOfferLetterFR(TeacherDetail teacherDetail,String jobTitle,String location,String acceptURL,String declineURL,boolean isSubstituteInstructionalJob)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			String position = jobTitle+" at "+location;
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			if(isSubstituteInstructionalJob){
				position = jobTitle;
				sb.append("<table>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getTeacherName(teacherDetail)+" "+candidateEmail+",<br/><br/> F�licitations! Le Conseil scolaire du comt� de Miami-Dade, en Floride, a v�rifi� l'admissibilit� initiale pour vous employer comme  "+position+".S'il vous pla�t, visitez le Bureau p�dagogique de dotation et de recrutement (1450 Northeast Second Avenue, Suite 265, Miami, FL 33132) pour compl�ter le processus de la demande.  <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Pour v�rifier l'admissibilit� finale et d'accepter notre offre d'emploi conditionnelle comme un <Position> et de commencer le processus d'embauche, s'il vous pla�t, visitez le Bureau p�dagogique de la dotation et de recrutement (Suite 265) avec les �l�ments suivants: <br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>I-9 / v�rification de l'admissibilit� de l'emploi � Original de la carte de s�curit� sociale et licence / ID �mise par le gouvernement du conducteur (l'autorisation de travail, si n�cessaire) </li><li>$ 71,00 Mandats payables � SBMD empreintes digitales pour la v�rification des ant�c�dents / empreintes digitales </li><li>$ 75,00 Mandats payables � M-DCPS pour le certificat de remplacement (pas n�cessaire si vous d�tenez un certificat de l�enseignement �ducateur temporaire ou professionnel)</li><li>de la Floride Informations bancaires pour l'inscription au d�p�t direct (un ch�que annul� n�cessaire)</li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Vous serez alors redirig� vers: </br><ul><li>Test de d�pistage de drogue (autorisation / information de test sera fournie par le Conseil scolaire � l'arriv�e) </li><li>Inscrivez-vous et veuillez reconna�tre les lignes directrices pour les instructeurs temporaires �</li><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Nous sommes impatients de vous rencontrer. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement,  <br/><br/>Le Bureau p�dagogique de la dotation et de recrutement 1450 Northeast Second Avenue, Suite 265, Miami, FL 33132 �<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modifier au contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur. ");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("</table>");
			}else{
				sb.append("<table>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Bonjour "+getTeacherName(teacherDetail)+"<br/><br/> F�licitations! Le conseil du comt� de Miami-Dade, en Floride, propose par la pr�sente de vous engager conditionnellemen "+position+"  pour l'ann�e 2015-2016.  Cette offre est conditionnelle et sur la base des conditions suivantes, y compris / mais non limit�s �: <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>V�rification de la certification / Admissibilit� </br></li><li>Empreintes digitales avec un r�sultat n�gatif </li><li>Test de d�pistage de drogues avec un r�sultat n�gatif</li><li>I-9 V�rification / Admissibilit� � l�emploi</li><li>R�f�rence acceptable / V�rification d�ant�c�dent criminel </li><li>V�rification de transcription </li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("S'il vous pla�t, r�pondez � ce message si vous acceptez cette offre d�emploi avec Miami-Dade County Public Schools et contactez l'�cole pour obtenir des informations sur les prochaines �tapes. Cette offre sera nulle si vous ne r�pondez pas dans les 2 jours ouvrables. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href='"+acceptURL+"'>Accepter l'offre     </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineURL+"'>D�cliner l'offre</a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement, <br/><br/>Le Bureau de la gestion du capital humain  <br> Miami-Dade County Public Schools  <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire (s) et peuvent contenir des informations privil�gi�es. Toute intervention ou modifier le contenu de ce message est interdite. Cette information est le m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n'�tes pas le destinataire d�sign�, informez l'exp�diteur et supprimez ce message de votre ordinateur.");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("</table>");
			}
			
			//System.out.println("sb 2nd::::"+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getThirdOfferNextStepFR(TeacherDetail teacherDetail,String jobTitle,String location)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			String position = jobTitle; 
			try{
				if(location!=null && !location.equals("")){
					position = jobTitle+" at "+location; 
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			sb.append("<table>");
	
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			
			sb.append("Bonjour "+getTeacherName(teacherDetail)+" "+candidateEmail+"<br/><br/>F�licitations pour votre d�cision d'accepter notre offre d'emploi conditionnelle comme un "+position+".<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul><li>Si l'offre d'emploi est un entra�neur � temps partiel, vous devez d'abord contacter la Division des sports et des activit�s au 305-995-7576 dans les deux jours ouvrables suivant la r�ception de ce courriel pour prendre rendez-vous et recevoir des instructions suppl�mentaires avant de visiter le Centre de Services de l�employ�. </br></li><li>Si l'offre d'emploi est pour un posite autre qu�un entra�neur � temps partiel, vous devez visiter le Centre de service aux employ�s situ� � 1450 NE 2e Avenue, Miami, FL, Suite 456, dans les deux jours ouvrables suivant la r�ception de ce courriel.  Le Centre de services aux employ�s est ouvert du lundi au vendredi 08h00-15h30.   </li></ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Au Centre de service aux employ�s, vous devrez compl�ter la proc�dure suivante. S'IL VOUS PLA�T, LIRE ATTENTIVEMENT:<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ol><li>Empreintes digitales: Tous les nouveaux employ�s doivent faire prendre leurs empreintes digitales et n�avoir aucun r�sultat n�gatif avant de commencer l'emploi avec M-DCPS. Pour prendre ses empreintes digitales vous devez apporter: Un mandat <ol type=\"a\"><li >$ 71,00 ou ch�que de banque libell� � SBMD empreintes digitales</li><li>et votre permis de conduire (ou pi�ce d'identit� officielle avec photo valide) et la carte de s�curit� sociale </li></ol></br></li><li>I-9 / v�rification de l'admissibilit� de l'emploi: par le Department of Homeland Security.  Les nouveaux employ�s doivent remplir un formulaire I-9 pour l'admissibilit� de l'emploi. Pour compl�ter le processus, s'il vous pla�t, apportez les documents requis indiqu�s � la page 5 de:http://www.uscis.gov/files/form/i-9.pdf </a></li><li>Autorisation de d�p�t direct : M-DCPS a maintenant un syst�me de paie sans papier. Tous les traitements et salaires sont pay�s par une m�thode de d�p�t direct automatis�. Les employ�s auront � choisir une des trois options suivantes:<ol type=\"a\" start=\"3\"><li>Pour d�poser votre ch�que de paie directement dans l'un de vos comptes bancaires existants, s'il vous pla�t, apportez un ch�que annul� ou bordereau de d�p�t de cette institution financi�re;</li><li>ou inscrivez-vous pour une carte Pay Lucarne, en savoir plus sur: http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf ou </li><li>Ouvrir un nouveau compte avec le sud de la Floride pour l'�ducation Federal Credit Union  </li></ol></li><li>D�pistage des drogues: un test de d�pistage des drogues est obligatoire pour tous les nouveaux employ�s � temps plein et les entra�neurs � temps partiel.  Ce test est fourni sans frais. Le test est effectu� hors site � plusieurs endroits dans tout le comt�. Vous recevrez un formulaire de d�pistage  des drogues lorsque vous visiterez le Centre de service des employ�s. Le test de d�pistage doit �tre effectu� dans les deux jours ouvrables suivant la r�ception de votre formulaire.</li><li>Les entra�neurs � temps partiel doivent apporter un mandat de $ 75,00 ou ch�que de banque libell� � Florida Department of Education � la Division des sports et activit�s. </li></ol>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("En plus des documents et des proc�dures d�crites ci-dessus, vous serez invit� � remplir divers formulaires li�s � l'emploi au cours de votre visite au Centre de service aux employ�s. D�s l'ach�vement et la v�rification de toutes les exigences ci-dessus, votre nouveau superviseur vous contactera avec la date / heure que vous devez pr�senter au travail<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");


			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Merci,<br/><br/>Le personnel du Centre de services aux employ�s 1450 N.E. 2nd Ave., Suite 456, Miami, FL 33132 <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modifier au contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			
			//System.out.println("sb:: 3rd::"+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getThirdOfferNextStepForInternalFR(int internal,TeacherDetail teacherDetail,String jobTitle,String location)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
	
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",<br/><br/>F�licitations pour votre d�cision d'accepter notre offre d'emploi conditionnelle comme un "+jobTitle+" au "+location+".  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul><li>Si l'offre d'emploi est un entra�neur � temps partiel, vous devez d'abord contacter la Division des sports et des activit�s au 305-995-7576 dans les deux jours ouvrables suivant la r�ception de ce courriel pour prendre rendez-vous et recevoir des instructions suppl�mentaires avant de visiter le Centre de Services de l�employ�.</li></ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Les entra�neurs � temps partiel doivent apporter un mandat $ 75,00 ou ch�que de banque libell� � Florida Department of Education � la Division des sports et activit�s. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Merci, <br/><br/>Le personnel du Centre de services aux employ�s 1450 N.E. 2e Avenue., Suite 456, Miami, FL 33132 <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modifier au contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			
			//System.out.println("sb:: 3rd::"+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getFourthAcceptOrDeclineFR(String panelUserName,TeacherDetail teacherDetail,String jobTitle,String location,int acceptOrDecline,UserMaster userMaster)
	{

		StringBuffer sb = new StringBuffer();
		int entityType=0;
		try{
			if(userMaster!=null && userMaster.getEntityType()!=null){
				entityType=userMaster.getEntityType();
			}
		}catch(Exception e){}
		
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+panelUserName+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			if(acceptOrDecline==0){
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(getTeacherName(teacherDetail)+"("+teacherDetail.getEmailAddress()+") a accept� le poste de "+jobTitle+" au "+location+".<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else{
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(getTeacherName(teacherDetail)+"("+teacherDetail.getEmailAddress()+")  a accept� le poste de "+jobTitle+" au "+location+".");
				if(entityType!=3){
					sb.append(" <b>Vous devez maintenant revenir au statut du cycle de vie et finaliser l�acceptation de l'offre pour d�placer le candidat � la phase Accueil et int�gration. </b>.");	
				}
				sb.append("<br/><br/></td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Le candidat recevra des instructions d�accueil et d�int�gration par courrier �lectronique et devra se rapporter au Service des ressources humaines pour compl�ter l�accueil et l�int�gration. Vous recevrez une notification lorsque l�embauche sera termin�e et que l'employ� sera autoris� � se pr�senter au travail.");
				sb.append("</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("sb Fourth:::"+sb);
		return sb.toString();
	}
	
	public static String getFifthNoResponseAcceptOrDeclineFR(TeacherDetail teacherDetail,JobOrder jobOrder,String location,Date offerDate,int iUserId)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Veuillez noter que"+getTeacherName(teacherDetail)+" a re�u une offre le  "+Utility.getUSformatDateTime(offerDate)+" pour le poste "+jobOrder.getJobId()+"intitul� "+jobOrder.getJobTitle()+"  pour "+location+". Il y a de cela plus de 48 heures, donc, ce rappel vous est envoy�.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Adresse �lectronique du candidat est�:  "+teacherDetail.getEmailAddress()); 
				if(teacherDetail.getPhoneNumber()!=null && !teacherDetail.getPhoneNumber().equals("")){
					sb.append(" et le num�ro de t�l�phone est "+teacherDetail.getPhoneNumber()+".<br/><br/>");
				}else{
					sb.append("<br/><br/>");
				}
				
				String sParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+iUserId+"##"+location);
				String sEmailURL=Utility.getValueOfPropByKey("basePath")+"/noresponseresendoffer.do?key="+sParameters;
				sb.append("<a href='"+sEmailURL+"'>Cliquez ici pour envoyer � nouveau le courriel Offre d'emploi </a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br>Sinc�rement,<br/>L�administrateur TeacherMatch <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
				sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		
		System.out.println(" "+sb.toString());
		return sb.toString();
	}
	
	public static String PNRRejectOrUnHireFR(TeacherDetail teacherDetail,UserMaster userMaster)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getUserName(userMaster)+",<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				
					sb.append("<td style="+mailCssFontSize+">");
					
						sb.append("Candidat  "+getTeacherName(teacherDetail)+" a �t� retir� du processus d�accueil et d'int�gration. Cela pourrait �tre d� � l'un des motifs suivants: <br/><br/>");
						
						sb.append("1.  Questions durant le processus d�accueil et d�int�gration<br/>");
						sb.append("2. Retrait ou Refuser par le candidat pendant le processus d'embauche <br/>");
						sb.append("3. Le rejet par l'�quipe de collaborateur ou d�accueil et d�int�gration<br/><br/>");
						
						sb.append("S'il vous pla�t, contactez votre agent de dotation pour toutes questions suppl�mentaires li�es � cette question.<br/><br/>MDCPS HR<br/><br/>");
					
					sb.append("</td>");
				
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
				sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		
		System.out.println(" "+sb.toString());
		return sb.toString();
	}
	
	public static String OfficialTranscriptsFR(TeacherDetail teacherDetail,String sDistrictName)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getTeacherName(teacherDetail)+" "+candidateEmail+",<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Merci d�avoir postul� pour un poste d'enseignement avec "+sDistrictName+". Le but de cette lettre est de vous informer que le conseil scolaire a re�u des copies de vos relev�s de notes officiels tels que requis afin de proc�der � la demande. Nous vous souhaitons la meilleure des chances dans votre recherche d'emploi. <br/><br/>"); 
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement,<br/>Le Bureau de la dotation p�dagogique<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
				sb.append("</td>");
				sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}

	public static String createMailToAddedSchoolsInDJOFR(JobOrder jobOrder,	SchoolMaster schoolMaster)
	{

		System.out.println("IN MAIL CREAET METHOD");

		StringBuffer sb = new StringBuffer();
		String MESSAGE = "";
		try {
			MESSAGE = "<p><span style='font-size: small;'>"
					+ "<strong>Bonjour administrateur scolaire, </strong>"
					+ "</span></p>"
					+ "<p><br />"
					+ "<span style='font-size: small;'>"
					+ "Votre �cole a �t� ajout�e � l'offre d�emploi. "
					+ "<strong>"
					+ "</strong>"
					+ " Vous allez maintenant avoir acc�s � la grille de candidat refl�tant tous les candidats pour ce poste.  "
					+ "</span></p>"
					+ "<p><br />"
					+ "<span style='font-size: small;'>Si vous avez des questions, s'il vous pla�t, contactez votre agent de dotation <strong>"
					+ schoolMaster.getDistrictName() + "</strong></span>"
					+ " HR"
					+ "<span style='font-size: small;'> lead.</span></p>"
					+ "<p><span style='font-size: small;'>&nbsp;</span></p>"
					+ "<p><span style='fontsize: small;'>Sinc�rement, </span><br />"
					+ "<span style='font-size: small;'>"
					+ "<strong>L��quipe du service � la client�le "
					+ "</strong></span></p>";

			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style=" + mailCssFontSize + ">");
			sb.append(MESSAGE);
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style=" + mailCssFontSize + ">");
			sb.append("<br>Sinc�rement,<br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style=" + mailFooterCssFontSize + ">");
			sb.append("sb .append (Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} catch (Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
		
	public static String emailToSchoolAdminsForAddSchoolFR(HttpServletRequest request,UserMaster userMaster,DistrictMaster districtMaster,String djoName)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear "+getUserName(userMaster)+",<br><br>");
			sb.append("Bonjour administrateur scolaire, <br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre �cole a �t� ajout�e � l�offre d�emploi "+djoName+" Vous allez maintenant avoir acc�s � la grille de candidat refl�tant tous les candidats pour ce poste. <br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(districtMaster!=null && districtMaster.getDistrictId().equals(7800040))
				sb.append("Si vous avez des questions, s'il vous pla�t, contactez votre �quipe au Service des ressources humaines du "+districtMaster.getDistrictName()+" Talent Acquisition Team.");
			else
				sb.append("Si vous avez des questions, s'il vous pla�t, contactez votre agent de dotation au "+districtMaster.getDistrictName()+" HR lead.");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement, <br/>L��quipe du service � la client�le <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String emailToSchoolAdminsForDeleteSchoolFR(HttpServletRequest request,UserMaster userMaster,DistrictMaster districtMaster,String djoName)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour administrateur scolaire, <br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre �cole a �t� retir�e de l�offre d�emploi"+djoName+" Vous n'aurez pas  acc�s � la grille de candidat refl�tant tous les candidats pour ce poste.  <br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(districtMaster!=null && districtMaster.getDistrictId().equals(7800040))
				sb.append("Si vous avez des questions, s'il vous pla�t, "+districtMaster.getDistrictName()+"  contactez votre �quipe au Service des ressources humaines du ");
			else
				sb.append("Si vous avez des questions, s'il vous pla�t,"+districtMaster.getDistrictName()+" HR lead.");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement,<br> L��quipe du service � la client�le <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String emailToAllUsersToShareCandidateFR(HttpServletRequest request,String usrName,String  usrNameshared,String teacherName,String folderName)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+usrNameshared+",<br><br>");
			
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			
			/*if(teacherName.contains(","))
				sb.append(usrName+" would like to share "+teacherName+" TeacherMatch candidate profile with you. To view this candidate�s profile, please do the following:<br>");
			else
				sb.append(usrName+" would like to share "+teacherName+" TeacherMatch candidate profile with you. To view this candidate�s profile, please do the following:<br>");*/
			
			
			
			
			sb.append(usrName+" aimerait partager le profil Teachermatch du candidat "+teacherName+" avec vous. Pour voir le profil de ce candidat, S'il vous pla�t, faire les d�marches suivantes: <br>");
			sb.append("<ol><li>Connectez-vous � la plate-forme de TeacherMatch via votre portail de l�employ�. </li><li>Cliquez sur la fl�che d�roulante � c�t� de votre nom dans le coin sup�rieur droit de l'�cran. </li><li> S�lectionnez Mes dossiers.</li><li>Lorsque l'�cran G�rer les candidats / sauvegarder sera ouvert, cliquez sur le c�t� de Mes dossiers. Les candidats partag�s appara�tront dans le dossier des candidats re�us.</li>");
		
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Les candidats partag�s appara�tront dans le dossier des candidats re�us\""+folderName+"\" folder. <br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement,<br/>L��quipe du service � la client�le TeacherMatch �<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  855-980-0545 ou t�l�phonez au  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	
	public static String offerAcceptMailToKeyContactFR(TeacherDetail teacherDetail,String schoolName,String jobTitle)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L'employ� suivant a accept� une offre conditionnelle � "+schoolName+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Nom de l'employ� :  "+getTeacherName(teacherDetail)+"<br>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date de naissance :  "+Utility.convertDateAndTimeToUSformatOnlyDate(teacherDetail.getFgtPwdDateTime()));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Adresse de l�employ� :  "+teacherDetail.getAuthenticationCode());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Num�ro de t�l�phone de l�employ� :"+teacherDetail.getPhoneNumber());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Num�ro de l�offre d�emploi : "+jobTitle);
			sb.append("</td>");
			sb.append("</tr>");


			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getFPReprintMailFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+":<br/><br/> Merci pour votre int�r�t � travailler pour Miami-Dade County Public Schools. S'il vous pla�t, notez que vos empreintes digitales ont �t� rejet�es par le Federal Bureau of Investigation (FBI) pour illisibilit� ou d'autres probl�mes techniques. Pour continuer le processus d�emploi, vous devez retourner dans les 15 jours ouvrables � l'Office d'empreintes digitales. Il n'y a pas de frais suppl�mentaires pour le service. Vous devez pr�senter une pi�ce d'identit� valide avec photo, comme un permis de conduire, passeport ou carte d'identit� d��tat. Si vous avez besoin d'informations suppl�mentaires, s'il vous pla�t appelez le District de Miami-Dade, Unit� d'empreintes digitales au 305-995-7472. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement, <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Le Bureau de la gestion du capital humain <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modifier au contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
			//System.out.println(sb);
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getFPPendingMailFR(HttpServletRequest request,TeacherDetail teacherDetail,TeacherPersonalInfo teacherPersonalInfo)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String city 		= 	"";
			String state 		= 	"";
			String zip 			= 	"";
			String address1		=	"";
			String address2		=	"";
			String address		=	"";
			String firstName	=	"";
			String addressline2	=	"";
			if(teacherPersonalInfo.getCityId() != null){
				city	=	teacherPersonalInfo.getCityId().getCityName();
				state	=	teacherPersonalInfo.getStateId().getStateName();
				zip		=	teacherPersonalInfo.getCityId().getZipCode();	
			}
			if(teacherPersonalInfo != null){
				address1	=	teacherPersonalInfo.getAddressLine1();
				address2	=	teacherPersonalInfo.getAddressLine2();
				address		=	address1+", "+address2;
				firstName	=	teacherDetail.getFirstName();
				addressline2	=	city+","+state+","+zip;
			}
			
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			 
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+":<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("TLe Bureau d'empreintes digitales a re�u les r�sultats de l'analyse de l'empreinte de la Florida Department of Law Enforcement et le Federal Bureau of Investigation refl�tant un incident qui a conduit � une action en justice. Veuillez noter que pour continuer le processus d'emploi, vous devez fournir les documents suivants :<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul>");
			sb.append("<li>Rapport d'incident (rapport d'enqu�te initiale de la police) ou le rapport d'arrestation / affidavit (l�affidavit du rapport d'enqu�te criminelle)</br></li>");
			sb.append("<li>Informations ou mise en accusation (accusations formelles d�pos�es par le procureur aupr�s du tribunal) </li>");
			sb.append("<li>Adjudication Cour (la d�cision du tribunal) </li>");
			sb.append("<li>DDocumentation de la r�ussite de la probation ou une intervention pr�ventive et le paiement de l'amende, le cas �ch�ant  </li>");
			sb.append("<li>Une d�claration notari�e �crite et sign�e par vous expliquant les circonstances de l'arrestation</li>");
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Toutes les photocopies de documents ou de certification par le greffier de la Cour judiciaire doivent �tre originales, des copies certifi�es conformes.Vous devriez communiquer avec le greffier de la Cour dans la ville ou du comt� o� l'affaire a �t� dispos�e. Les documents doivent �tre soumis � Miami-Dade dans les 15 jours ouvrables � compter de la date de cette lettre � l'adresse: Miami-Dade Unit� d'empreintes digitales,1450 NE 2e Avenue, Suite 110, Miami, FL 33132 <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement, <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sigilenda Miles <br/>Directeur g�n�ral de dactyloscopie <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire (s) et peuvent contenir des informations privil�gi�es. Toute intervention ou modifier le contenu de ce message est interdite. Cette information est le m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n'est pas le destinataire d�sign�, informez l'exp�diteur et supprimez ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
			
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();
	}
	
	public static String PNRUnHireMailFR(JobForTeacher jobForTeacher,UserMaster userMaster)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			//String sCandidateName="";
			/*if(jobForTeacher.getTeacherId().getFirstName()!=null && !jobForTeacher.getTeacherId().getFirstName().equalsIgnoreCase(""))
				sCandidateName=jobForTeacher.getTeacherId().getFirstName();
			
			if(jobForTeacher.getTeacherId().getLastName()!=null && !jobForTeacher.getTeacherId().getLastName().equalsIgnoreCase(""))
				sCandidateName=sCandidateName+" "+jobForTeacher.getTeacherId().getLastName();*/
			
			//String sSAName="";
			
			//sSAName=getUserName(userMaster);
			
			/*if(userMaster!=null && userMaster.getFirstName()!=null && !userMaster.getFirstName().equalsIgnoreCase(""))
			{
				sSAName=getUserName(userMaster);
			}*/
				

				sb.append("<table>");
				sb.append("<table>");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+getUserName(userMaster)+",<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Le but de ce courriel est de vous informer que "+getTeacherName(jobForTeacher.getTeacherId())+"  a �t� embauch� dans le poste de "+jobForTeacher.getJobId().getJobTitle()+"  par erreur le :  "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"<br/><br/>");
				sb.append("</td>");
				
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("S'il vous pla�t, ne laissez pas ce candidat se pr�senter au travail � votre lieu de travail. Si vous avez des questions suppl�mentaires, s'il vous pla�t, contactez le Service des ressources humaines.");
				sb.append("</td>");
				sb.append("</tr>");
				
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getDTFailedMailFR(HttpServletRequest request,TeacherDetail teacherDetail)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("L'analyse de votre d�pistage des drogues pr�alables � l'emploi a �t� confirm�e positive. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Les politiques de drogues en milieu de travail stipulent que : ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Les candidats seront inform�s � l'avance de l'exigence d'un d�pistage de drogues n�gatif  comme condition d'emploi. Les candidats qui re�oivent un r�sultat de test positif ne seront pas �ligibles � l'emploi par Miami-Dade County Public Schools pendant trois (3) ans � compter de la date du test. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("En cons�quence, votre nom a �t� retir� de la liste d'admissibilit� de l'emploi pour la p�riode pr�vue par la politique du Conseil. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement, <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Le Bureau des normes professionnelles Miami-Dade County Public Schools <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire (s) et peuvent contenir des informations privil�gi�es. Toute intervention ou modifier le contenu de ce message est interdite. Cette information est le m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n'est pas le destinataire d�sign�, informez l'exp�diteur et supprimez ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String PNRSentToSapFR(JobForTeacher jobForTeacher,String principalName)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			//String sCandidateName	=	"";
			String positionName		=	"";
			/*if(jobForTeacher.getTeacherId().getFirstName()!=null && !jobForTeacher.getTeacherId().getFirstName().equalsIgnoreCase(""))
				sCandidateName=jobForTeacher.getTeacherId().getFirstName();

			if(jobForTeacher.getTeacherId().getLastName()!=null && !jobForTeacher.getTeacherId().getLastName().equalsIgnoreCase(""))
				sCandidateName=sCandidateName+" "+jobForTeacher.getTeacherId().getLastName();*/

			if(jobForTeacher.getJobId().getJobTitle()!=null && !jobForTeacher.getJobId().getJobTitle().equalsIgnoreCase(""))
				positionName=jobForTeacher.getJobId().getJobTitle();
			
			sb.append("<table>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(new Date())+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");

			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+principalName+",<br/><br/>");
			sb.append("</td>");

			sb.append("</tr>");
			
			sb.append("<tr>");

			sb.append("<td style="+mailCssFontSize+">");
			
			sb.append("Veuillez noter que"+getTeacherName(jobForTeacher.getTeacherId())+" a rempli toutes les exigences pr�alables � l'embauche n�cessaire pour le poste de "+positionName+". Il / elle peut se rapporter au travail le premier jour ouvrable apr�s la date imprim�e au haut de ce courriel.");

			sb.append("</td>");

			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append(" Si vous avez des questions, contactez  ee Service des ressources humaines. <a href='mailto:personnel@dadeschools.net'>personnel@dadeschools.net</a>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String getMailTextInVacancyFR(HttpServletRequest request,String newRecords,String delRecords,String message)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour tout le monde, ");
			sb.append("</td>");
			sb.append("<tr><td>&nbsp;</td></tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(message);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(newRecords);
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(delRecords);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr><td>&nbsp;</td></tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Sinc�rement, <br>L��quipe de TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou appelez "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" getRegistrationMail :: "+sb.toString());
	
		return sb.toString();
	}
	
	public static String getApproveJObMailTextFR(HttpServletRequest request, DistrictKeyContact districtKeyContact,String externalURL,String jobTitle)
	{

		StringBuffer sb = new StringBuffer();
		String emailContent = "";
		try 
		{	
			
			UserMaster userMaster = districtKeyContact.getUserMaster();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+"<br>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Vous devez approuver l�offre d�emploi \""+jobTitle+"\"<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			if(externalURL!=null)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br>Pour approuver cette offre, cliquez sur le lien ci-dessous. <br/><br/><a href='"+externalURL+"'>"+externalURL+"</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement, <br/>L�administrateur TeacherMatch <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		emailContent = sb.toString();
		System.out.println(" "+emailContent);
		
		return emailContent;
	}
	
	public static String ReferenceCheckMailFR(TeacherElectronicReferences teacherElectronicReferences, JobOrder jobOrder, TeacherDetail teacherDetail, DistrictMaster districtMaster, String mainURLEncode)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request;// = context.getHttpServletRequest();
		
		if(WebContextFactory.get()!=null)
			request = WebContextFactory.get().getHttpServletRequest();
		else
			request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		
		String firstName 			= 	"";
		String lastName				=	"";
		String fullName				=	"";
		String candidateFirstName	=	"";
		String candidateLastName	=	"";
		String candidateFullName	=	"";
		String jobTitle				=	"";
		
		
		if(jobOrder != null){
			jobTitle = jobOrder.getJobTitle();
		}
		
		firstName	=	teacherElectronicReferences.getFirstName() == null ? "" : teacherElectronicReferences.getFirstName();
		lastName	=	teacherElectronicReferences.getLastName() == null ? "" : teacherElectronicReferences.getLastName();
		fullName 	= 	firstName+" "+lastName;		
		fullName	=	fullName.replace("'","\\'");
		
		candidateFirstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		candidateLastName	=	teacherDetail.getLastName()	==	null ? "" : teacherDetail.getLastName();
		
		candidateFullName	=	candidateFirstName+" "+candidateLastName;
		candidateFullName	=	candidateFullName.replace("'", "\\'");
		
		StringBuffer sb = new StringBuffer();
		try {

				String basePath	=	Utility.getBaseURL(request);
				sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+fullName+",<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(candidateFullName+ " ous a �num�r� comme une r�f�rence dans le cadre d'une recherche d'emploi r�cente avec  "+districtMaster.getDistrictName()+" pour le poste de  "+jobTitle+". Vos commentaires sont demand�s par le Conseil scolaire pour d�terminer la poursuite de  la candidature de "+candidateFullName+".<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Pour remplir le formulaire de r�f�rence, s'il vous pla�t, cliquez sur le lien ci-dessous. Veuillez noter qu'une s�lection pour chaque question est n�cessaire afin de remplir le formulaire de r�f�rence. Un seul formulaire est autoris� pour la demande de r�f�rence. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Commencer: <BR><a href="+mainURLEncode+">");
				sb.append(mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<BR>Le  "+districtMaster.getDistrictName()+" vous remercie de votre participation dans notre d�marche d�embaucher les candidats les plus qualit�s pour le Conseil scolaire. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Sinc�rement, ");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append(districtMaster.getDistrictName());
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("L��quipe de la dotation");
				sb.append("</td>");
				sb.append("</tr>");

			sb.append("</table>");
		}

		catch(Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	
	public static String sendMailLinkTemplateFR(HttpServletRequest request ,JobOrder jobOrder, TeacherDetail teacherDetail, String mainURLEncode,DistrictMaster districtMaster,int template,boolean principalCategory)
	{

		System.out.println(":::::::::::::::::sendMailLinkTemplate =:::::::::::::");
		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		StringBuffer sb = new StringBuffer();
		if(principalCategory){
		if(template==1){
			try {
				sb.append("<table>");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				
					sb.append("<br/>Nous sommes heureux d�ajouter votre nom pour l'activit� en ligne avec  "+ districtMaster.getDistrictName()+". La date limite pour soumettre l'activit� est le  Monday, March 2.<br/><br/>");
				
				sb.append("</td>");
				sb.append("</tr>");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Lorsque vous serez pr�t � terminer l'activit� en ligne, S'il vous pla�t,  <a href="+url+mainURLEncode+">cliquez ici.</a>.  Ceci est une activit� chronom�tr�e de 2,5 heures que vous devez terminer en une seule s�ance, vous devriez donc planifier en cons�quence. V�rifiez que vous disposez d'une connexion Internet s�curis�e avant de cliquer sur le lien pour d�marrer l'activit�. Vous pouvez soumettre avant que les 2,5 heures sont termin�es, si vous n�avez pas termin� toute l'activit� durant les 2,5 heures, vous devez quand m�me soumettre. Il n'y aura pas d'extensions de temps allou�es.  <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				String districtContactMail=Utility.getValueOfPropByKey("onlineActivityEmail");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("S'il vous pla�t,,notez que lorsque vous �tes dans le portail, <a href='mailto:"+districtContactMail+"'>"+districtContactMail+"</a>.   vous ne pouvez pas revenir en arri�re.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement, <br/>The Office of Talent<br/>"+districtMaster.getDistrictName());

				sb.append("</td>");
				sb.append("</tr>");
				/*sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+url+mainURLEncode+">");
				sb.append(url+mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}else{
			try {
				sb.append("<table>");
			
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+firstName+",");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");				
					sb.append("<br/>Nous sommes heureux d�ajouter votre nom pour l'activit� en ligne avec  "+districtMaster.getDisplayName()+". La date limite pour soumettre l'activit� est le Monday, April 13.<br/><br/>");				
				sb.append("</td>");
				sb.append("</tr>");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Lorsque vous serez pr�t � terminer l'activit� en ligne, s'il vous pla�t, <a href="+url+mainURLEncode+">"+url+mainURLEncode+"</a>.  Ceci est une activit� chronom�tr�e de 2,5 heures que vous devez terminer en une seule s�ance.  Vous devriez donc planifier en cons�quence. V�rifiez que vous disposez d'une connexion Internet s�curis�e avant de cliquer sur le lien pour d�marrer l'activit�. Vous pouvez soumettre avant que les 2,5 heures sont termin�es, si vous n�avez pas termin� toute l'activit� durant les 2,5 heures, vous devez quand m�me soumettre. Il n'y aura pas d'extensions de temps allou�es. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("S'il vous pla�t, notez que lorsque vous �tes dans le portail, vous ne pouvez pas revenir en arri�re. Si vous cliquez sur le bouton de retour dans votre navigateur, vous serez chronom�tr� sur l'activit�. Donc, comme vous progressez dans l'activit�, s'il vous pla�t, s�assurer que vous avez termin�  avec une question avant de passer � la suivante. Il y a six questions / activit�s devant �tre r�alis�es dans les d�lais, veuillez ajuster votre rythme en cons�quence. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				String districtContactMail=Utility.getValueOfPropByKey("onlineActivityEmail");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("if an emergency arises, please contact "+districtContactMail+". Do not reply to this e-mail.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement, <br/>The Office of Talent<br/>"+districtMaster.getDistrictName());
				
				sb.append("</td>");
				sb.append("</tr>");
			
				/*sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+url+mainURLEncode+">");
				sb.append(url+mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				sb.append("</table>");
			}
	
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		}else{
			sb.append("<table>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+firstName+" "+lastName+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");				
				sb.append("<br/>Nous sommes heureux de vous inscrire pour l'activit� en ligne pour votre demande d�emploi avec le Conseil scolaire<br/><br/>");				
			sb.append("</td>");
			sb.append("</tr>");

			String url=Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Lorsque vous serez pr�t � terminer l'activit� en ligne, s'il vous pla�t,  <a href="+url+mainURLEncode+">cliquez ici.</a>.  Ceci est une activit� qui n�est pas chronom�tr�e, mais nous vous encourageons � la remplir et la pr�senter d�s que possible afin que vous puissiez �tre �ligible pour la s�lection du site. V�rifiez que vous disposez d'une connexion Internet s�curis�e avant de cliquer sur le lien pour d�marrer l'activit�.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Si une urgence survient, contactez  <a href='mailto:recruitment@philasd.org'>recruitment@philasd.org.</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement, <br/>The Office of Talent<br/>"+districtMaster.getDistrictName());			
			sb.append("</td>");
			sb.append("</tr>");
			//The School District of Philadelphia
		}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	
	public static String resetOnlineActEmailFR(HttpServletRequest request,TeacherDetail teacherDetail, String mainURLEncode,DistrictMaster districtMaster,int template,boolean principalCategory)
	{

System.out.println("::::::::::::resetOnlineActEmail::::");
		
		String teacherName 	= 	"";
		teacherName	=	getTeacherName(teacherDetail);
		StringBuffer sb = new StringBuffer();
		if(principalCategory){
		if(template==1){
			try {
				sb.append("<table>");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+teacherName+",");
				sb.append("</td>");
				sb.append("</tr>");

				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				
					sb.append("<br/>Votre activit� en ligne avec le"+ districtMaster.getDistrictName()+"  a �t� r�initialis�e. La date limite pour soumettre l'activit� est le Monday, March 2.<br/><br/>");
				
				sb.append("</td>");
				sb.append("</tr>");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Lorsque vous serez pr�t � terminer l'activit� en ligne, S'il vous pla�t,<a href="+url+mainURLEncode+">cliquez ici.</a>. Ceci est une activit� chronom�tr�e de 2 heures que vous devez terminer en une seule s�ance, vous devriez donc planifier en cons�quence. V�rifiez que vous disposez d'une connexion Internet s�curis�e avant de cliquer sur le lien pour d�marrer l'activit�. Vous pouvez soumettre avant que les 2 heures sont termin�es, si vous n�avez pas termin� toute l'activit� durant les 2 heures, vous devez quand m�me soumettre. Il n'y aura pas d'extensions de temps allou�es.   <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				String districtContactMail=Utility.getValueOfPropByKey("onlineActivityEmail");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Si une urgence survient, contactez  <a href='mailto:"+districtContactMail+"'>"+districtContactMail+"</a>. Ne pas r�pondre � ce courriel. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement,<br/>The Office of Talent<br/>"+districtMaster.getDistrictName());

				sb.append("</td>");
				sb.append("</tr>");
				/*sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+url+mainURLEncode+">");
				sb.append(url+mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		}else{
			try {
				sb.append("<table>");
			
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+teacherName+",");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");				
					sb.append("<br/>Votre activit� en ligne avec le  "+ districtMaster.getDistrictName()+" a �t� r�initialis�e. La date limite pour soumettre l'activit� est le Monday, April 13.<br/><br/>");				
				sb.append("</td>");
				sb.append("</tr>");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Lorsque vous serez pr�t � terminer l'activit� en ligne, s'il vous pla�t, cliquez sur  <a href="+url+mainURLEncode+">"+url+mainURLEncode+"</a>. Ceci est une activit� chronom�tr�e de 2 heures que vous devez terminer en une seule s�ance, vous devriez donc planifier en cons�quence. V�rifiez que vous disposez d'une connexion Internet s�curis�e avant de cliquer sur le lien pour d�marrer l'activit�. Vous pouvez soumettre avant que les 2 heures sont termin�es, si vous n�avez pas termin� toute l'activit� durant les 2 heures, vous devez quand m�me soumettre. Il n'y aura pas d'extensions de temps allou�es. <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				String districtContactMail=Utility.getValueOfPropByKey("onlineActivityEmail");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("onlineActivityEmail Si une urgence survient, contactez "+districtContactMail+".  Ne pas r�pondre � ce courriel.  <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement,  <br/>The Office of Talent<br/>"+districtMaster.getDistrictName());
				
				sb.append("</td>");
				sb.append("</tr>");
			
				/*sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+url+mainURLEncode+">");
				sb.append(url+mainURLEncode);
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");*/
				sb.append("</table>");
			}
	
			catch(Exception e) {
				e.printStackTrace();
			}
		}
		}else{
			sb.append("<table>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+teacherName+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");	
				sb.append("<br/>Votre activit� en ligne avec le  "+ districtMaster.getDistrictName()+"  a �t� r�initialis�e. <br/><br/>");				
			sb.append("</td>");
			sb.append("</tr>");

			String url=Utility.getValueOfPropByKey("basePath");
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Lorsque vous �tes pr�t � terminer l'activit� en ligne, s'il vous pla�t,  <a href="+url+mainURLEncode+">cliquez ici. </a>. Cela n'est pas une activit� chronom�tr�e, mais nous vous encourageons � remplir et � pr�senter d�s que possible afin que vous puissiez �tre �ligible pour la s�lection du site. V�rifiez que vous disposez d'une connexion Internet s�curis�e avant de cliquer sur le lien pour d�marrer l'activit�. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Si une urgence survient, contactez <a href='mailto:recruitment@philasd.org'>recruitment@philasd.org.</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement, <br/>The Office of Talent<br/>"+districtMaster.getDistrictName());			
			sb.append("</td>");
			sb.append("</tr>");
			//The School District of Philadelphia
		}
		
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String mailSendToTeacherForSchoolSelectionFR(HttpServletRequest request ,JobOrder jobOrder, TeacherDetail teacherDetail)
	{

		 StringBuffer sb = new StringBuffer();
         try {

       	sb.append("<table>");
       	  

   	  	sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour, ");
			sb.append("</td>");
			sb.append("</tr>");
				
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Nous vous remercions de votre int�r�t dans un poste de direction d��cole pour l'ann�e scolaire 2015-2016. Nous sommes heureux de vous annoncer que vous avez r�ussi notre processus de s�lection et serez ajout� � notre bassin de talents de direction!  ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Vous pouvez maintenant postuler aux postes vacants sp�cifiquement pour une �cole. Vous devez t�l�charger une d�claration d'int�r�t par l'interm�diaire TeacherMatch pour chaque �cole � laquelle vous souhaitez postuler. La d�claration d'int�r�t ne devrait pas �tre plus d'une seule page, et si possible, s'il vous pla�t, enregistrez et t�l�chargez un fichier PDF. La d�claration d'int�r�t devrait aborder : <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");

           sb.append("<ul>");

           sb.append("<li>Pourquoi voulez-vous g�rer l'�cole?</li>");

           sb.append("<li>Comment vos comp�tences et vos exp�riences vous permettront de g�rer avec succ�s l'�cole?</li>");

           sb.append("<li>Qu'est-ce que vous esp�rez accomplir en tant que direction de l'�cole? </li>");          

           sb.append("</ul>");
           
           sb.append("</td>");
			sb.append("</tr>");
			
		
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Chaque d�claration d'int�r�t doit �tre sp�cifique � l'�cole pour laquelle vous postulez. Visitez notre site <a target='_blank' href='http://webgui.phila.k12.pa.us/offices/e/ee/opportunities/principals-and-assistant-principals'>website</a> pour plus d'informations sur chaque ouverture. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Toutes les d�clarations d'int�r�t sont dues d'ici la fin de la journ�e ouvrable, le  Friday, March 26 Votre demande sera par la suite examin�e afin de planifier des entrevues, si votre candidature est int�ressante. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>S'il vous pla�t, lire attentivement les instructions suivantes, et si vous avez besoin d'aide, contactez  <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a>.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");

           sb.append("<ul>");

           sb.append("<li>Connectez-vous � votre compte de  TeacherMatch </li>");

           sb.append("<li>Cliquez sur votre nom dans le coin sup�rieur droit de l'�cran  </li>");

           sb.append("<li>Un menu d�roulant appara�tra Choisissez une �cole ensuite,</li>");

           sb.append("<ul type='circle'>");

           sb.append("<li>Choose \"School Selection\"</li>");

           sb.append("</ul>");

           sb.append("<li>Then, the \"Principal\" job will appear</li>");

           sb.append("<ul type='circle'>");

           sb.append("<li>l�offre d�emploi appara�tra � la droite, cliquez sur Ajouter / Modifier �coles ,</li>");

           sb.append("</ul>");

           sb.append("<li>Au haut de la grille, s�lectionnez Ajouter �cole </li>");

           sb.append("<li>De l�, s�lectionnez les �coles qui vous int�ressent, et t�l�chargez votre d�claration d'int�r�t (voir ci-dessous pour les instructions) </li>");

           sb.append("<li>� tout moment, vous pouvez revenir � s�lection des �coles pour ajouter ou supprimer vos choix </li>");

           sb.append("<ul type='circle'>");

           sb.append("<li>Ceci est important, �tant donn� que les postes vacants seront charg�s sur une base continue </li>");

           sb.append("</ul>");

           sb.append("</ul>");

       	sb.append("</td>");
			sb.append("</tr>");
   			
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a>.Pour toutes questions suppl�mentaires, contactez le Service des ressources humaines. <a href='mailto:sejerome@philasd.org'>sejerome@philasd.org</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Encore une fois, f�licitations! Nous nous r�jouissons � la correspondance future. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><br/>Cordialement, <br/>");
			sb.append("The Office of Talent<br/>");
			sb.append("The School District of Philadelphia<br/>");
			sb.append("</td>");
			sb.append("</tr>");

           sb.append("</table>");
         }catch(Exception e) {

               e.printStackTrace();

         }
   System.out.println("sb::::::::::"+sb.toString());
   return sb.toString();
	}
	
	public static String getNewPanelComonAttendeeMailTextFR(HttpServletRequest request,PanelSchedule panelSchedule,PanelAttendees panelAttendees,List<PanelAttendees> panelAttendeesList,String cNContent)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			TeacherDetail teacherDetail = panelSchedule.getTeacherDetail();
			UserMaster userMaster = panelAttendees.getPanelInviteeId();
			String districtName = userMaster.getDistrictId().getDistrictName();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");		
				sb.append("Vous avez �t� invit� par  "+districtName+" � participer � un comit� de r�vision de candidats. Des d�tails suppl�mentaires sont fournis ci-dessous :<br/><br/>");
		
			sb.append("</td>");
			sb.append("</tr>");	

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Emploi postul� : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(panelSchedule.getJobOrder().getJobTitle());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date / heure du comit� : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			//Demo Lesson Date/Time: January X, XXXX    11:30 am CST 
			sb.append(Utility.convertDateAndTimeToUSformatOnlyDate(panelSchedule.getPanelDate())+" "+panelSchedule.getPanelTime()+" "+panelSchedule.getTimeFormat()+" "+panelSchedule.getTimeZoneMaster().getTimeZoneShortName());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Lieu du comit� : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(panelSchedule.getPanelAddress());
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Commentaires : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			try{
				if(panelSchedule.getPanelDescription()!=null){
					sb.append(panelSchedule.getPanelDescription());
				}else{
					sb.append("");
				}
			}catch(Exception e){sb.append("");}
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Participants : ");
			sb.append("</td>");
			sb.append("<td style="+mailCssFontSize+">");
			String attendees = "";
			for(int i=0;i<panelAttendeesList.size();i++)
			{
				UserMaster um = panelAttendeesList.get(i).getPanelInviteeId();
				attendees = attendees+um.getFirstName()+" "+(um.getLastName()==null?"":um.getLastName())+", ";
			}
			if(attendees.length()>1)
			sb.append(attendees.substring(0,attendees.length()-2));
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+"><b>Les candidats : </b>:</td>");			
			sb.append("</tr>");
			sb.append(cNContent);
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");	
		
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>Sinc�rement, <br/>L'�quipe TeacherMatch au nom du "+districtName+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre Admin �  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String mailSendToTeacherForAssessmentInviteFR(HttpServletRequest request ,JobOrder jobOrder, TeacherDetail teacherDetail,ExaminationCodeDetails examinationCodeDetails,String icalFile)
	{
		System.out.println("::::::::::::::mailSendToTeacherForAssessmentInvite::::::::::::");	
		String firstName 	= 	"";
		String lastName		=	"";
		String city = "";
		String state = "";
		String zipcode = "";
		String address = "";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		city = jobOrder.getDistrictMaster().getCityName()==null?"":jobOrder.getDistrictMaster().getCityName();
		state = jobOrder.getDistrictMaster().getStateId().getStateName()==null?"":jobOrder.getDistrictMaster().getStateId().getStateName();
		zipcode = jobOrder.getDistrictMaster().getZipCode()==null?"":jobOrder.getDistrictMaster().getZipCode();
		address = jobOrder.getDistrictMaster().getAddress()==null?"":jobOrder.getDistrictMaster().getAddress();
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"50%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour&nbsp;"+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+"><br/>Le fichier est : ::"+icalFile+"</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+"><br/>Nous vous remercions pour votre int�r�t dans le poste de "+jobOrder.getJobTitle()+". Dans une prochaine �tape, vous avez besoin de prendre un examen en ligne. Les d�tails de l'examen en ligne sont les suivants : </td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Code d'examen: &nbsp;"+examinationCodeDetails.getExaminationCode()+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">Adresse:&nbsp;"+city+" "+state+" "+ address +" "+zipcode+" <br/></td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">Date et heure de l�examen : &nbsp;"+Utility.convertDateAndTimeFormatForEpiAndJsi(examinationCodeDetails.getExamDate())+"<br/></td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Cordialement,<br/>");
				sb.append(jobOrder.getDistrictMaster().getDistrictName());
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String emailToTeacherForTFAChangedFR(HttpServletRequest request,TeacherDetail teacherDetail,UserMaster userMaster)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear "+getUserName(userMaster)+",<br><br>");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("Dear "+getUserName(userMaster)+",<br><br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dans le cadre de votre demande d�emploi, vous vous �tes identifi� comme ayant particip� � Teach For America. Nous avons confirm� que vous ne participez � TFA, et nous avons ajust� votre porte-folio pour refl�ter ce changement. Si vous avez des preuves que vous �tes dans le TFA, s'il vous pla�t, contacter le Service des ressources humaines.  ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Cordialement,  </br>"+getUserName(userMaster));
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/>Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez votre administrateur �  <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>.  ");
			sb.append("</td>");
			sb.append("</tr>");
			

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	public static String mailTextForEventsFR(String messageText)
	{
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(messageText);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			/*sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Please do not reply to this email. If you need to respond, please send your reply to <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");*/
			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
		
		
	}
	
	public static String sendVVIInviteInterviewEmailFR(String[] emailDetails)
	{

		StringBuffer sb = new StringBuffer();
		try {
			
			String uanme = "";
			String pass = "";
			
			if(emailDetails[5]!=null)
				uanme = emailDetails[5];
			if(emailDetails[6]!=null)
				pass = emailDetails[6];
			
			
			sb.append("<table width=\"50%\">");
		
		    sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+emailDetails[0]+",");
			sb.append("</td>");
			sb.append("</tr>");

		
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Nous vous remercions de votre r�cente demande d�emploi avec  "+emailDetails[1]+".  Dans le cadre du processus de la demande, nous aimerions que vous compl�tiez une entrevue vid�o en ligne<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>S'il vous pla�t, enregistrez cette entrevue vid�o en suivant les instructions ci-dessous. Nous sommes impatients de recevoir votre entrevue vid�o. <br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Merci!<br/><hr>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>L'entrevue se compose de "+emailDetails[2]+" questions. Chaque r�ponse ne peut �tre pas plus de "+emailDetails[3]+"secondes de longueur de sorte que le temps maximal requis pour l'entrevue est "+emailDetails[4]+"  minutes. <br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			//Sunday, January 18th, 2015 at 10:28 AM EST
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Vous pouvez compl�ter votre entrevue en utilisant un PC / ordinateur portable avec une cam�ra vid�o et un microphone. S'il vous pla�t, compl�tez l'entrevue au plus tard le "+emailDetails[7]+" at the latest.<br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>�tape 1: Fermez tous les programmes en cours d'ex�cution sur votre ordinateur � l'exception de votre navigateur Web et un programme de courrier �lectronique. Veuillez fermer tous les programmes vid�o ou audio, y compris Skype. <br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>�tape 2: Acc�dez � la page de connexion:  <a href='https://teachermatch.interview4.com/test_taker_login.php'>teachermatch.interview4.com/test_taker_login.php</a><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>�tape 3: ��Entrez votre nom d'utilisateur:<br/><span class='left5'>&nbsp;&nbsp;Entrez votre mot de passe:&nbsp;"+uanme+"</span><br/><span class='left5'>&nbsp;&nbsp;�Ensuite, s�lectionnez �Connexion� :&nbsp;"+pass+"</span><br/><span class='left5'>&nbsp;&nbsp;Then, select 'Log In'</span><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>�tape 4: Suivre les instructions � l'�cran. <br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><b>Conseils utiles </b><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ol>");
			sb.append("<li>Habillez-vous de fa�on professionnelle tout comme vous le feriez dans une entrevue en direct. </br></li>");
			sb.append("<li>En plus du temps n�cessaire pour terminer l'entrevue, vous aurez besoin de 10 - 15 minutes pour la pr�paration et la pratique. Assurez-vous d'avoir suffisamment de temps pour compl�ter votre entrevue en une seule s�ance.  </li>");
			sb.append("<li>Assurez-vous que la pi�ce o� vous prenez votre entrevue est bien �clair�e Lors de l'installation de l'entrevue vid�o assurez-vous d��tre correctement positionn�. </li>");
			sb.append("<li>Lors de la visualisation de votre vid�o de test, vous devriez voir du haut de votre t�te jusqu�au haut de vos �paules. Si votre visage remplit le cadre, vous �tes trop pr�s; si vous pouvez voir l'ensemble de votre torse,  vous �tes trop loin. </li>");
			sb.append("</ol>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			//sb.append("<br/>Sincerely<br/>"+emailDetails[1]+"");
			sb.append("<br/>Sinc�rement,<br/>L��quipe du service � la client�le TeacherMatch ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("<br/><br/>Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au"+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
		}catch(Exception e) {
			e.printStackTrace();
		}
	System.out.println(" Email to candidate for virtual interview :"+sb.toString());
	return sb.toString();
	}
	
	
	public static String sendReminderMailTextFR(JobOrder jobOrder, TeacherDetail teacherDetail)
	{

		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		DistrictMaster districtMaster = jobOrder.getDistrictMaster();
		String districtName = districtMaster.getDisplayName()==null?districtMaster.getDistrictName():districtMaster.getDisplayName();
		if(districtName.equals(""))
			districtName = districtMaster.getDistrictName();
		int reminderFrequencyInDays = districtMaster.getReminderFrequencyInDays();
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"80%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				/*sb.append("<br/>Thank you for applying for employment with "+districtName+". We hire excellent teachers throughout the school year and are eager to learn more about you. " +
						"To make sure your application is up-to-date, please respond to the following questions by "+reminderFrequencyInDays+" days. <br/><br/>");*/
				String url=Utility.getValueOfPropByKey("basePath");
				// Redirect to login
				sb.append("<br/>Nous vous remercions d�avoir postul� pour un poste au "+districtName+".. Nous recrutons d'excellents enseignants tout au long de l'ann�e scolaire et nous voulons en apprendre davantage � votre sujet.   " +
						"Pour vous assurer que votre demande soit mise � jour, s'il vous pla�t, <a href='"+url+"'>Click Here</a>  cliquez ici pour acc�der � vos demandes d'emploi. <br/>");
				
/*				String forMated = "";
				String tchrId=Utility.encryptNo(teacherDetail.getTeacherId());
				String jobId=Utility.encryptNo(jobOrder.getJobId());
				String linkIds= tchrId+"###"+jobId;
				try {
					forMated = Utility.encodeInBase64(linkIds);
					forMated = url+"tmsurvey.do?key="+forMated;
				} catch (Exception e) {
					e.printStackTrace();
				}	
				
				sb.append("<br/>Thank you for applying for employment with "+districtName+". We hire excellent teachers throughout the school year and are eager to learn more about you. " +
						"To make sure your application is up-to-date, please respond to the survey questions by "+reminderFrequencyInDays+" days. <a href='"+forMated+"'>Click Here</a> to access the survey<br/>");*/
				
				//sb.append("Clicking the URL <a href='"+forMated+"'>"+forMated+"</a> <br><br>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Cordialement,<br/>");
				sb.append("Le Service de la dotation<br/>");
				sb.append(""+districtName+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String sendReminderMailTextForFullertonFR(JobOrder jobOrder, TeacherDetail teacherDetail)
	{

		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		DistrictMaster districtMaster = jobOrder.getDistrictMaster();
		String districtName = districtMaster.getDisplayName()==null?districtMaster.getDistrictName():districtMaster.getDisplayName();
		if(districtName.equals(""))
			districtName = districtMaster.getDistrictName();
		int reminderFrequencyInDays = districtMaster.getReminderFrequencyInDays();
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"80%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<br/>Nous vous remercions de votre int�r�t avec le Conseil scolaire de");
				sb.append("<br/><br/>S'il vous pla�t, assurez-vous de ompl�ter votre demande avant la date limite. Si votre demande est incompl�te apr�s notre date d��ch�ance votre demande NE SERA PAS consid�r�e.   <a href='"+url+"'> Cliquez ici</a> pour acc�der � votre demande d'emploi.<br/>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Cordialement,<br/>");
				sb.append("Certificated Personnel<br/>");
				sb.append("Fullerton School District<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	public static String sendInviteForInviteOnlyCandidatesMailTextFR(JobOrder jobOrder, TeacherDetail teacherDetail)
	{

		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		DistrictMaster districtMaster = jobOrder.getDistrictMaster();
		String districtName = districtMaster.getDisplayName()==null?districtMaster.getDistrictName():districtMaster.getDisplayName();
		if(districtName.equals(""))
			districtName = districtMaster.getDistrictName();
		
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"80%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Bonjour "+firstName+" "+lastName+",");
				sb.append("</td>");
				sb.append("</tr>");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				String url=Utility.getValueOfPropByKey("basePath");
				sb.append("<br/>S'il vous pla�t, compl�ter une demande du  "+districtName+". <br/><br/> en cliquant sur ce lien : <br/><a href='"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"'>"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"</a><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+"><br>Pour toutes questions, contactez le Service des ressources humaines au Conseil scolaire. </a>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Cordialement, <br/>");
				sb.append(""+districtName+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("<br/><br/>Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au  "+mailContactNo+".");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	
	public static String mailTextForTeacherExamConfirmedFR(TeacherDetail teacherDetail,ExaminationCodeDetails examinationCodeDetails,String timeZone)
	{

		StringBuffer sb = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
		SimpleDateFormat formday=new SimpleDateFormat("EEEE");
		String dayofweek=formday.format(examinationCodeDetails.getExamDate());
		String JobTitle=examinationCodeDetails.getJobOrder().getJobTitle();
		DistrictMaster districtMaster=examinationCodeDetails.getDistrictAssessmentDetail().getDistrictMaster();		
		String districtName=districtMaster.getDistrictName();
		String examDate=dayofweek+", "+dateFormat.format(examinationCodeDetails.getExamDate()).toString();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+",");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Nous vous remercions d�avoir confirm� votre plage de temps pour l'�valuation pour le poste de  "+JobTitle+". Les d�tails sont les suivants: <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Date : "+examDate+"<br/>Dur�e : "+examinationCodeDetails.getExamStartTime()+" To "+examinationCodeDetails.getExamEndTime()+" "+timeZone);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Cordialement,<br>"+districtName+"");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		System.out.println(" TeacherMail :: "+sb.toString());	
		return sb.toString();
	}
	
	public static String messageForDefaultFontForQuestUserFR(String messageText,UserMaster userSession)
	{
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(messageText);
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");			
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a> ou t�l�phonez 855-980-0545.");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		return sb.toString();
	}
	/*public static String mailTextForExamSlotSelection(ExaminationCodeDetails examinationCodeDetails,String assessmentLink)
	{	
		if(locale.equals("fr"))
			return mailTextForExamSlotSelectionFR(examinationCodeDetails, assessmentLink);
		
		StringBuffer sb = new StringBuffer();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
		SimpleDateFormat formday=new SimpleDateFormat("EEEE");
		DistrictMaster districtMaster=examinationCodeDetails.getDistrictAssessmentDetail().getDistrictMaster();
		String JobTitle=examinationCodeDetails.getJobOrder().getJobTitle();
		String examAddress=districtMaster.getCityName()+", "+districtMaster.getAddress()+", "+districtMaster.getStateId().getStateName()+", "+districtMaster.getZipCode();
		String district=districtMaster.getDistrictName();	
		boolean slotSelectionNeeded = false;
		if(districtMaster.getDistrictId()==4218990)
			slotSelectionNeeded = true;
		
		try 
		{
			TeacherDetail  teacherDetail = examinationCodeDetails.getTeacherDetail();
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"");			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>We are pleased to invite you to take the "+examinationCodeDetails.getDistrictAssessmentDetail().getDistrictAssessmentName()+" assessment for the position of "+JobTitle+" with "+district+".");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			
			if(slotSelectionNeeded)
			{
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("</br>Please click here  to schedule your assessment. Please note the date and time you select.");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
			}
			
			sb.append("<td style="+mailCssFontSize+">");
			if(slotSelectionNeeded)
				sb.append("</br>Click on the following link to select the assessment Slot:<br> "+assessmentLink+"<br/>");
			else
				sb.append("</br>Click on the following link to take the assessment:<br> "+assessmentLink+"<br/>");
			
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Please do not reply to this e-mail. If you need assistance, please contact <a href='mailto:"+Utility.getValueOfSmtpPropByKey("smtphost.noreplyusername")+"'>"+Utility.getValueOfSmtpPropByKey("smtphost.noreplyusername")+"</a>.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Assessment Code: "+examinationCodeDetails.getExaminationCode()+"<br/>");
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Address: "+examAddress+"</br>");
			sb.append("</td>");
			sb.append("</tr>");			
		
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best regards,<br>"+district+"");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		}catch (Exception e) {
		}
		return sb.toString();
		
	}*/
		
	public static String getTempAdvOnBoardingFR(HttpServletRequest request ,JobOrder jobOrder, TeacherDetail teacherDetail, DistrictMaster districtMaster,UserMaster userMaster)
	{


		StringBuffer sb=new StringBuffer("");
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
			String mailDate=" "+dateFormat.format(new Date());					
			String acceptParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##1");
			String declineParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##0");
			String acceptURL=Utility.getValueOfPropByKey("basePath")+"/advOnBoarding.do?key="+acceptParameters;
			String declineURL=Utility.getValueOfPropByKey("basePath")+"/advOnBoarding.do?key="+declineParameters;
			sb.append("<table width='700' style="+mailCssFontSize+" align='center' border='0'>");
			sb.append("<tr>");
			sb.append("<td align='center' >");
			sb.append("<font size='+2'><b>COLUMBUS CITY �COLES</b></font></br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td  align='center'>");
			sb.append("<b>");
			sb.append("SERVICE DES RESSOURCES HUMAINES");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td  align='center'>");
			sb.append("<b>");
			sb.append("<u>L��quipe de la dotation� </u>");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td align='center'>");
			sb.append("<b>");
			sb.append("270 E. State Street");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td align='center'>");
			sb.append("<b>");
			sb.append("Columbus, Ohio 43215 </br></br>");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("");
			sb.append("<tr>");
			sb.append("<td >");
			sb.append("<table width='700' ><tr><td width='50%' align='left'><b> Secondary (614) 365-5651</b></td>                        <td width='50%' align='right'><b> Elementary (614) 365-5609</b></td></tr></table>");
			sb.append("");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("");
			sb.append("<tr>");
			sb.append("<td align='center'>");
			sb.append("<b>");
			sb.append("<u>Commitment of Employment</u></br></br>");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("");
			sb.append("<tr>");
			sb.append("<td align='center'>");
			sb.append("<b>");
			sb.append("Date:"+mailDate+"  </br></br>");
			sb.append("</b>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("");
			sb.append("<tr>");
			sb.append("<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
			sb.append("Ceci est pour confirmer que            &nbsp;&nbsp;&nbsp;&nbsp;                          doit �tre embauch� comme enseignant dans les �coles de la ville de Columbus pour l'ann�e scolaire 2015-2016. La premi�re journ�e de travail pay�e sera la date n�goci�e entre le Conseil Columbus de l'�ducation et l'Association d'�ducation Columbus. </br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("Cet accord comprend les dispositions suivantes:");
			sb.append("<ol>");
			sb.append("<li> Le surintendant recommandera le candidat au Conseil de l'�ducation pour le poste. </br></br> </li>");
			sb.append("<li>Il assure que le demandeur est nomm� � un poste d'enseignant dans leur domaine de certification dans les �coles de la ville de Columbus, pendant le temps indiqu�. Cette affectation cession est soumise � l'Etat et la loi f�d�rale, la politique du conseil d'administration et les dispositions applicables de la convention collective. Cet engagement est subordonn� �: a) l'approbation du candidat � un emploi par le Conseil Columbus de l'�ducation;");
			sb.append("b) b) la capacit� du demandeur � recevoir un certificat/licence valide d�enseignement d�Ohio. (Les candidats doivent avoir pris et pleinement r�ussi l'Examen national des enseignants pour devenir certifi� / autoris� dans l'�tat de l'Ohio. Les candidats doivent �galement avoir compl�t� avec succ�s une exp�rience de l'enseignement des �tudiants dans leur domaine de certification.); et c) la soumission et la r�ception de la requ�rante par Columbus City Schools de la documentation obligatoire pour la nomination et l'emploi continu et l'indemnisation.</br></br>");
			sb.append("</li>");
			sb.append("<li> Il assure le Conseil Columbus de l'�ducation que le requ�rant sera disponible pour le service � la date stipul�e dans le pr�sent accord. </br></br></li>");
			sb.append("<li> Cette offre d'engagement de l'emploi est ouverte pour acceptation pendant une p�riode de 15 jours � la date susmentionn�e. Le d�faut de remplir et de retourner ce document dans le temps requis sera consid�r� comme un refus.  </br></br> </li>");
			sb.append("</ol>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td></br>");
			sb.append(" En cliquant  <a href='"+acceptURL+"'>accepte</a>, vous acceptez cette position.  En cliquant  <a href='"+declineURL+"'>je refuse</a>, vous refusez cette position, mais pouvez encore �tre consid�r�e pour toute autre position que vous avez demand�e.");
			sb.append("</td>");
			sb.append("</tr>");
		
			sb.append("<tr>");
			sb.append("<td>");
			sb.append("<br/>");
			sb.append("<table width='100%'>");
			sb.append("<tr><td width='70%'><b>___________________________________________</b></td><td>&nbsp;</td></tr>");
			sb.append("<tr><td  width='70%'><b>Zone pr�vue de la certification </b></td><td>&nbsp;</td></tr>");
			sb.append("<tr><td  width='70%'><br/><b>___________________________________________	</b></td><td><br/><b>_______________</b></td></tr>");
			sb.append("<tr><td  width='70%'><b>Signature du demandeur</b></td><td><b>Date</b></td></tr>");
			sb.append("<tr><td  width='70%'><br/><b>___________________________________________</b></td><td><br/><b>_______________</b></td></tr>");
			sb.append("<tr><td  width='70%'><b>Signature du repr�sentant du surintendant</b></td><td><b>Date</b></td></tr>");
			sb.append("</table>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
		}catch(Exception e){
			e.printStackTrace();
		}
		//System.out.println("letter of Commitment==="+sb.toString());																																																																																																																																																																																								sb.append("</table>");
		return sb.toString();
	}
	
	public static String getTempEvlComplFR(JobOrder jobOrder, TeacherDetail teacherDetail, DistrictMaster districtMaster,UserMaster userMaster,int temp,String schoolName,String statusFlagAndStatusId)
	{
   
		StringBuffer sb=new StringBuffer("");
		// temp==0 for mailBySchool
		// temp==1 for mailByDistrict
		// temp==2 for mailByTeacher(Accept Mail)
		// temp==3 for mailByTeacher(Decline Mail)
		try{
			SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM dd, yyyy");		
			String mailDate=" "+dateFormat.format(new Date());					
			String acceptParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##"+statusFlagAndStatusId+"##1");
			String declineParameters = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+userMaster.getUserId()+"##"+statusFlagAndStatusId+"##0");
			String acceptURL=Utility.getValueOfPropByKey("basePath")+"/evlComp.do?key="+acceptParameters;
			String declineURL=Utility.getValueOfPropByKey("basePath")+"/evlComp.do?key="+declineParameters;			
			sb.append("<table width='700' style="+mailCssFontSize+" align='center' border='0'>");
			if(temp==0){
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Nom de l'�cole : "+schoolName+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Nom du candidat : "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Poste : "+jobOrder.getJobTitle()+"<br/>");
				sb.append("</td>");
				sb.append("</tr>");
			}else if(temp ==1){
				String position = jobOrder.getJobTitle()+" at "+schoolName;
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				
				sb.append("Bonjour "+getTeacherName(teacherDetail)+"<br/><br/> F�licitations! Le Conseil scolaire de Philadelphie, en Floride, offre par la pr�sente de vous employer conditionnellement  "+position+" pour l'ann�e 2015-2016. Cette offre est conditionnelle et sur la base des conditions suivantes, y compris / mais non limit�s �:<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul><li>V�rification de certification / Admissibilit� </br></li><li>Empreintes digitales avec un r�sultat n�gatif </li><li>Test de drogue avec un r�sultat n�gatif </li><li>V�rification I-9 / Admissibilit� d�emploi </li><li>R�f�rence acceptable / V�rification des ant�c�dents criminels</li><li>V�rification de relev� de notes </li></ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Veuillez r�pondre � ce message si vous acceptez cette offre d�emploi avec les �coles de Philadelphie et contacter l'�cole pour des informations sur les prochaines �tapes. Cette offre est nulle si vous ne r�pondez pas dans les 2 jours ouvrables. Nous sommes impatients de vous accueillir � Philadelphie �coles.<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href='"+acceptURL+"'>Accepter l'offre </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href='"+declineURL+"'>d�cliner l'offre </a><br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Cordialement,<br/><br/>The Office of Talent<br/>The School District of Philadelphia<br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailFooterCssFontSize+">");
				sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur. ");
				sb.append("</td>");
				sb.append("</tr>");
			}else if(temp ==2){					
					System.out.println("Teacher Name=="+teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
					System.out.println("jobOrder.getJobTitle()=="+jobOrder.getJobTitle());
					System.out.println("Teacher Name=="+teacherDetail.getFirstName()+" "+teacherDetail.getLastName());
				sb.append("<tr>");
				sb.append("<td>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+"  a accept�  "+jobOrder.getJobTitle()+" � "+schoolName+" sur "+mailDate+".</td>");
				sb.append("</tr>");
				
			}else if(temp ==3){	
				sb.append("<tr>");
				sb.append("<td>"+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" a diminu� "+jobOrder.getJobTitle()+" � "+schoolName+" sur "+mailDate+".</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");			
		}catch(Exception e){
			e.printStackTrace();
		}
		//System.out.println("letter of Commitment==="+sb.toString());																																																																																																																																																																																								sb.append("</table>");
		return sb.toString();
		
	}
	public static String getUpdateQQMailForDistrictFR(UserMaster usermaster,TeacherDetail teacherDetail,String questionAnswer)
	{

		StringBuffer sb = new StringBuffer();
		String daFullName=usermaster.getFirstName()+" "+usermaster.getLastName();
		String applicantFullName=teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+daFullName+",");			
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Candidat "+applicantFullName+"a mis � jour les informations suivantes:<br/><br>");			
			sb.append("</td>");
			sb.append("</tr>");			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(questionAnswer);
			sb.append("</td>");
			sb.append("</tr>");				
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Merci");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("The Partner Success Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez  <a href='mailto:partners@teachermatch.org'>partners@teachermatch.org</a> ou t�l�phonique "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}		
		//System.out.println(" getRegistrationMail :: "+sb.toString());	
		return sb.toString();
	}
	
	public static String getOfferReadyByPrincipalFR(HttpServletRequest request,JobForTeacher jobForTeacher)
	{

		StringBuffer sb = new StringBuffer();
		try 
		{
			
			String candidateEmail = "";
			try{
				if(jobForTeacher!=null && jobForTeacher.getTeacherId()!=null && jobForTeacher.getTeacherId().getEmailAddress()!=null)
					candidateEmail	=	" ( "+jobForTeacher.getTeacherId().getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(jobForTeacher.getTeacherId())+" "+candidateEmail+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("F�licitations! Le conseil du comt� de Miami-Dade, en Floride, propose par la pr�sente de vous offrir un poste conditionnellement en tant qu� ");
			sb.append(jobForTeacher.getJobId().getJobTitle());
			sb.append("professeur � ");
			if(jobForTeacher.getSchoolMaster()!=null && jobForTeacher.getSchoolMaster().getSchoolName()!=null){
				sb.append(jobForTeacher.getSchoolMaster().getSchoolName());
			}
			sb.append("  pour l'ann�e 2015-2016. ");
			sb.append(" Afin d'�tablir votre admissibilit� pour ce poste, s'il vous pla�t, visitez le Bureau de Miami-Dade County Public Schools de certification p�dagogique (1450 NE 2nd Avenue, Suite 260, Miami, FL 33132 - du lundi au vendredi, 08h00-15h30 ) avec la documentation suivante  ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<U>Pour les postes � temps partiel d'�ducation des adultes: </U>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<UL>");
			sb.append("<LI>Relev� de notes officiel </LI>");
			sb.append("<LI> Demande  de certificat d�adultes termin�e du District (formulaire 5584 ci-joint)</LI>");
			sb.append("<LI>Mandat pour $ 75,00 payable � \"M-DCPS\" </LI>");
			sb.append("</UL>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<U>Pour les postes � temps plein ou � temps partiel pour une profession: </U>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<UL>");
			sb.append("<LI>Documentation de l'exp�rience professionnelle sur l�ent�te de l'entreprise</LI>");
			sb.append("<LI>Licences professionnelles, si n�cessaire </LI>");
			sb.append("<LI>Demande de certificat d'enseignement g�n�ral et professionnel (FM 5585 ci-joint)</LI>");
			sb.append("<LI>Mandat pour $ 75,00 payable � \"M-DCPS\" </LI>");
			sb.append("</UL>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<U>Tous les candidats: </U>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Si le bureau de certification p�dagogique v�rifie que vous �tes admissible � un emploi dans cette position, vous devrez alors remplir les exigences de pr�emploi, y compris, mais sans s'y limiter, la v�rification des ant�c�dents, test de drogue, v�rification de l'�ligibilit� de l'emploi, et les informations de d�p�t direct. Pour vous s�assurer que vous �tes pr�t � r�pondre � ces exigences, tous les candidats doivent apporter: ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<UL>");
			sb.append("<LI>Un mandat $ 71,00 ou ch�que de banque libell� � �SBMD empreintes digitales\"</LI>");
			sb.append("<LI>Permis de conduire valide (ou pi�ce d'identit� officielle avec photo valide)</LI>");
			sb.append("<LI>Demande de certificat d'enseignement g�n�ral et professionnel (FM 5585 ci-joint) </LI>");
			sb.append("<LI>Carte de s�curit� sociale </LI>");
			sb.append("<LI>Liste des documents n�cessaires � la page 5 du D�partement am�ricain de l'I-9 la forme ");
			sb.append("<a href='http://www.uscis.gov/files/form/i-9.pdf'>(http://www.uscis.gov/files/form/i-9.pdf)</a>");
			sb.append("</LI>");
			sb.append("<LI>�Ch�que ou bordereau de d�p�t de votre institution financi�re annul�; ou inscrivez-vous pour une carte Pay Skylight ");
			sb.append("<a href='http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf'>(http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf)</a>");
			sb.append("</LI>");
			sb.append("</UL>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("F�licitations encore et nous sommes impatients de vous accueillir � Miami-Dade County Public Schools. ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Cordialement,");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Le Bureau de la gestion du capital humain");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Miami-Dade County Public Schools");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" "+sb.toString());
		
		return sb.toString();
	
	}
	
	
	public static String getOfferReadyByStafferFR(TeacherDetail teacherDetail,String jobTitle,String location)
	{
 
		StringBuffer sb = new StringBuffer();
		try 
		{
			
			String position = jobTitle; 
			try{
				if(location!=null && !location.equals("")){
					position = jobTitle+" at "+location; 
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			String candidateEmail = "";
			try{
				if(teacherDetail!=null && teacherDetail.getEmailAddress()!=null)
					candidateEmail	=	" ("+teacherDetail.getEmailAddress()+") ";
			}catch(Exception exception){
				exception.printStackTrace();				
			}
			
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+getTeacherName(teacherDetail)+" "+candidateEmail+",");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("F�licitations pour votre d�cision d'accepter notre offre d'emploi conditionnelle en tant qu�enseignant au  ");
			sb.append(jobTitle);
			sb.append("teacher at ");
			if(location!=null && location!=null){
				sb.append(location);
			}
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("  S'il vous pla�t, visitez le Centre des fonctionnaires (1450 NE 2 nd Avenue, Suite 456, Miami, FL 33121) dans les deux jours ouvrables suivant la r�ception de ce courriel. Le Centre de services aux employ�s est ouvert du lundi au vendredi, 08h00-15h30  ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Tel qu�indiqu� dans votre lettre d'offre conditionnelle, lorsque vous visitez le Centre de service aux employ�s, vous devrez effectuer les proc�dures suivantes ... S'il vous pla�t, LIRE ATTENTIVEMENT : </B> ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>Empreintes digitales:</B> Tous les nouveaux employ�s doivent fournir des empreintes digitales avant de commencer l'emploi avec M-DCPS. Pour prendre ses empreintes digitales, vous devez apporter : </U>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Un mandat $ 71,00 ou ch�que de banque libell� � �SBMD empreintes digitales  et ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Votre permis de conduire (carte d'identit� officielle avec photo) et une carte de s�curit� sociale ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>I-9 / v�rification de l'admissibilit� de l'emploi:</B> par le Department of Homeland Security, les nouveaux employ�s doivent remplir un formulaire I-9 pour l'admissibilit� de l'emploi. Pour compl�ter le processus, s'il vous pla�t, apporter les documents requis indiqu�s � la page 5 du formulaire I-9 ");
			sb.append("<a href='http://www.uscis.gov/files/form/i-9.pdf'>(http://www.uscis.gov/files/form/i-9.pdf)</a>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>Autorisation de d�p�t direct:</B>  Tous salaires M-DCPS et les salaires sont pay�s par un syst�me de d�p�t direct automatis�. Les employ�s doivent choisir parmi l'une des trois options suivantes :  ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Pour d�poser votre ch�que de paie directement dans l'un de vos comptes bancaires existants, s'il vous pla�t, apporter un ch�que annul� ou bordereau de d�p�t de cette institution financi�re; ou ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Inscrivez-vous pour une carte Pay Skylight ou ");
			sb.append("<a href='http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf'>(http://payroll.dadeschools.net/pdfs/sky-visa_debitcard.pdf)</a> ; or");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Ouvrir un nouveau compte avec le sud de la Floride pour l'�ducation Federal Credit Union  ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<B>Drug-testing:</B> D�pistage des drogues: un test de drogue est obligatoire pour tous les nouveaux employ�s � temps plein et les entra�neurs � temps partiel fourni sans frais. Le test est effectu� hors site � plusieurs endroits � travers le comt�. Vous recevrez un formulaire de test de drogue lorsque vous visiterez le Centre de service des employ�s. Le test de d�pistage doit �tre effectu� dans les deux jours ouvrables suivant la r�ception de votre formulaire.  ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("En plus des documents et des proc�dures d�crites ci-dessus, vous serez invit� � remplir divers formulaires li�s � l'emploi au cours de votre visite au Centre de service aux employ�s. D�s l'ach�vement et la v�rification de toutes les exigences ci-dessus, votre nouveau superviseur vous confirmera la date / heure ou vous devez pr�senter au travail. </B> ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(" Merci, ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Le personnel du Centre de services aux employ�s ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("1450 NE 2 nd Avenue, Suite 456, Miami, FL 33132 ");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr><td style="+mailCssFontSize+"><BR></td></tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Les informations contenues dans ce message courriel sont destin�es uniquement pour le destinataire et peuvent contenir des informations privil�gi�es. Toute intervention ou modification du contenu de ce message est interdite. Cette information est la m�me que tout document �crit et peut �tre soumise � toutes les r�gles r�gissant l'information du public selon les lois de la Floride. Si vous avez re�u ce message par erreur, ou n��tes pas le destinataire d�sign�, informer l'exp�diteur et supprimer ce message de votre ordinateur.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		//System.out.println(" "+sb.toString());
		
		return sb.toString();
		
		
	}
	
	
	public static String getNegativeQQMailTextFR(UserMaster user, JobOrder jobOrder, DistrictMaster districtMaster, List<TeacherAnswerDetailsForDistrictSpecificQuestions> teacherAnswerDetailsForDistrictSpecificQuestionList)
	{

StringBuffer sb = new StringBuffer();
		
		try
		{
			String applicantFirstName = teacherAnswerDetailsForDistrictSpecificQuestionList.get(0).getTeacherDetail().getFirstName();
			String applicantLastName = teacherAnswerDetailsForDistrictSpecificQuestionList.get(0).getTeacherDetail().getLastName();
			
			String districtUserFirstName= user.getFirstName();
			String districtUserSecondName= user.getLastName();
			
			sb.append("<table>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Bonjour "+districtUserFirstName+" "+districtUserSecondName+",<BR><BR>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Le candidat "+applicantFirstName+" "+applicantLastName+" a donn� une ou plusieurs r�ponses n�gatives pour la qualification des questions comme les suivantes: <BR>");
			sb.append("</td>");
			sb.append("</tr>");
			
			int count=1;
			for(TeacherAnswerDetailsForDistrictSpecificQuestions teacherAnswerDetailsForDistrictSpecificQuestions : teacherAnswerDetailsForDistrictSpecificQuestionList)
			{
				if(teacherAnswerDetailsForDistrictSpecificQuestions.getIsActive()!=null && teacherAnswerDetailsForDistrictSpecificQuestions.getIsValidAnswer()!=null && teacherAnswerDetailsForDistrictSpecificQuestions.getIsActive() && !teacherAnswerDetailsForDistrictSpecificQuestions.getIsValidAnswer())
				{
					String question=teacherAnswerDetailsForDistrictSpecificQuestions.getQuestion();
					String answer=teacherAnswerDetailsForDistrictSpecificQuestions.getSelectedOptions();
					String explanation=teacherAnswerDetailsForDistrictSpecificQuestions.getInsertedText();
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					
					if(question!=null)
						sb.append("<BR>Question "+count+". "+question);
					if(answer!=null)
						sb.append("<BR>R�ponse. "+answer);
					if(explanation!=null)
						sb.append("<BR>Explication. "+explanation);
					
					sb.append("<BR></td>");
					sb.append("</tr>");
					count++;
				}
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<BR>Sinc�rement,<br>TeacherMatch Client Services Team  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("Veuillez ne pas r�pondre � ce message qui est un courriel automatis� g�n�r� par le syst�me. Pour toutes questions suppl�mentaires, contactez <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> ou t�l�phonez au "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	public static String PortfolioReminderMailFR(TeacherDetail teacherDetail, DistrictMaster districtMaster, String mainURLEncode)
	{

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request;// = context.getHttpServletRequest();
		
		if(WebContextFactory.get()!=null)
			request = WebContextFactory.get().getHttpServletRequest();
		else
			request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();
		
		StringBuffer sb = new StringBuffer();
		try {

				String basePath	=	Utility.getBaseURL(request);
				sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Nous vous remercions d�avoir postul� pour travailler au  " +districtMaster.getDistrictName()+". Afin de garder nos dossiers � jour, nous demandons � ce que les candidats mettre � jour leurs profils sur une base r�guli�re.   <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");

				

				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+mainURLEncode+">");
				sb.append(mainURLEncode);
				sb.append("</a> <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("S'il vous pla�t, cliquez sur le lien ci-dessous pour mettre � jour votre profil.  Veuillez noter le fait de ne pas mettre � jour votre profil dans les 30 jours se traduira dans votre profil �tant retir� des emplois auxquels vous postul�.  Si vous souhaitez pr�senter une nouvelle demande � tout moment, connectez-vous simplement � votre profil et indiquez que vous �tes int�ress� � nouveau � ces postes (il ne sera pas n�cessaire de commencer le processus de demande � partir de z�ro). ");
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");

				

				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Cordialement,<br><br>Le Bureau du p�dagogique dotation et de recrutement<br>"+districtMaster.getDistrictName()+"");
				sb.append("</td>");
				sb.append("</tr>");
				

			sb.append("</table>");
		}

		catch(Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	public static String createUserForUpload(UserMaster userSession,UserMaster userMaster)
	{
		System.out.println("::::::::::::::createUserForUpload::::::::::::::::::");
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl		=	Utility.getValueOfPropByKey("basePath")+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(userMaster)+":  You have been added as a user on the TeacherMatch<sup>TM</sup> platform.   Your Login Email is <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>. Your Authentication PIN is "+userMaster.getAuthenticationCode()+". In order to activate your account, please visit the link below to set your password: ");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please contact me at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a> if you have questions.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best wishes,<br/>"+getUserName(userSession)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please contact your Admin at <a href='mailto:"+userSession.getEmailAddress()+"'>"+userSession.getEmailAddress()+"</a>.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" createUserForUpload :: "+sb.toString());
		
		return sb.toString();
	}
	
	public static String sendInviteForInviteOnlyCandidatesMailTextForKelly(String password,JobOrder jobOrder, TeacherDetail teacherDetail,List<TeacherDetail> newTeacherList)
	{
		
		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		DistrictMaster districtMaster = jobOrder.getDistrictMaster();
		String districtName = jobOrder.getHeadQuarterMaster().getHeadQuarterName();
		if(districtName.equals(""))
			districtName = districtMaster.getDistrictName();
		boolean epiFlag=false;
		boolean newCanFlag=false;
		boolean smartPracticeFlag=false;
		boolean newTalentApp=false;
		boolean qqFlag=false;
		if(jobOrder.getJobCategoryMaster().getBaseStatus()!=null && jobOrder.getJobCategoryMaster().getBaseStatus()){
			epiFlag=true;
		}
		
		if(newTeacherList.contains(teacherDetail)){
			newCanFlag=true;
		}
		
		if(jobOrder.getJobCategoryMaster().getPreHireSmartPractices()!=null && jobOrder.getJobCategoryMaster().getPreHireSmartPractices()){
			smartPracticeFlag=true;
		}
		
		if(jobOrder.getJobCategoryMaster().getOfferQualificationItems()!=null && jobOrder.getJobCategoryMaster().getOfferQualificationItems()){
			qqFlag=true;
		}
		
		if(jobOrder.getJobCategoryMaster().getOfferDSPQ()!=null && jobOrder.getJobCategoryMaster().getOfferDSPQ()){
			newTalentApp=true;
		}
	    String bitlyURL   = "http://bit.ly/1az1fhe";
		String url        = Utility.getValueOfPropByKey("basePath");
		String platformUrl        = Utility.getValueOfPropByKey("platformBasePath");
		String	loginUrl  =platformUrl+"signin.do";
		String  jobUrl="<a href='"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"'>"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"</a>";
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"80%\">");
			
				if(newTalentApp){
					sb.append("<tr>");			
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Dear Kelly Educational Staffing Talent,<br><br>");
					sb.append("</td>");
					sb.append("</tr>");
				}else{
					sb.append("<tr>");			
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Dear "+firstName+" "+lastName+",<br><br>");
					sb.append("</td>");
					sb.append("</tr>");
				}
				
				
			int counter=1;
				if(smartPracticeFlag  && !epiFlag){
					sb.append("<tr>");		
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Welcome to Kelly Educational Staffing&reg; (KES&reg;)! We are happy to welcome you onboard and will work with you to make this a seamless process.<br>");
					sb.append("</td>");
					sb.append("</tr>");	
					
					sb.append("<tr>");		
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("You are receiving this email as the first step in the "+jobOrder.getHeadQuarterMaster().getHeadQuarterName()+" onboarding process. The next step is for you to complete Smart Practices&trade;, a professional development series for substitute teachers and paraprofessionals, designed to give you the insights and skills you need to excel in the classroom. Both "+jobOrder.getHeadQuarterMaster().getHeadQuarterName()+" and TeacherMatch� are committed to providing candidates with effective, professional training focused on the principles and tactics of successful classroom management.<br>");
					sb.append("</td>");
					sb.append("</tr>");	
					
					sb.append("<tr>");		
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("After completing the Smart Practices training suite and demonstrating proficiency on the Smart Practices assessment, you will enter the substitute pool well-qualified and prepared to maximize student learning. Completion of these modules and assessment is required for employment consideration.<br>");
					sb.append("</td>");
					sb.append("</tr>");

					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<br/>Please click below to get started: <br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("You will use the login information to access the system:<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Email: "+teacherDetail.getEmailAddress()+"<br/>" );
					if(password!=null){
						 sb.append("Password: "+password+"<br/><br/>");
						 sb.append("If you have previously changed your password from that listed above, you will need to use the \"Forgot Password\" feature on the main login screen at <a href='"+loginUrl+"'>"+loginUrl+"</a>.<br/><br/>");
					}else{
						if(newCanFlag)
						  sb.append("Password: "+teacherDetail.getPassword()+"<br/><br/>");
					}
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<br/>Please click the link below to get started: <br/><a href='"+loginUrl+"'>"+loginUrl+"</a><br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("When you arrive at your applicant dashboard, look for section \"Personal Planning\", and click the icon associated with \"Smart Practices Professional Development\". You will then be routed to the Smart Practices dashboard to begin the training. <br>");
					sb.append("</td>");
					sb.append("</tr>");
					
				}
				else if(smartPracticeFlag  || epiFlag){
					
					String two="";
					String EPIOraplication="";
					if((newTalentApp && epiFlag && smartPracticeFlag && !qqFlag) || (newTalentApp && epiFlag && !smartPracticeFlag && !qqFlag) ){
						two="two";
					}
					if((qqFlag)){
						EPIOraplication="EPI";
					}else{
						EPIOraplication="application";
					}
					
					String brief="";
					if(newTalentApp){
						brief="brief application,";
					}
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Thank you for your interest in being a substitute teacher with Kelly Educational Staffing&reg; (KES&reg;).<br></br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("The next step in the application process are for you to complete the "+brief+" Educators Professional Inventory (EPI)&trade; and the Smart Practices&trade;  pre-hire professional development training modules and assessment. </br></br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("We encourage you to complete these "+two+" application elements as soon as possible so we can expedite you through our onboarding process.</br></br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					//**********************QQ
					if(qqFlag || newTalentApp){
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("<b>"+counter+". Application</b><br><br>");
						sb.append("</td>");
						sb.append("</tr>");
						
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("Upon login you will be directed to the application requirements. Please complete all steps.<br><br>");
						sb.append("</td>");
						sb.append("</tr>");
						counter++;
					}
					
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<b>"+counter+". Educators Professional Inventory (EPI) </b><br><br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					counter++;
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Important Note: The TeacherMatch EPI is NOT used as a hiring assessment tool by KES, and your score will NEVER be used as a qualifier or filter for you to be able to view or accept substitute teacher positions within our partner school districts. <br><br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("We strive to maintain the integrity of the EPI and ensure that everyone has a fair and equal opportunity to complete the EPI.  Therefore, before beginning, please note these IMPORTANT guidelines:<br><br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<ul><li> While the average person takes approximately 45 minutes to complete, please reserve at least 90 minutes of uninterrupted time to complete it and avoid any \"timed out\" issues</li>" +
							"<li>Make sure that you have a stable and reliable internet connection</li>"+
							"<li>Do not close your browser or hit the \"back\" button on your browser</li>"+
							"<li> There are 75 questions � each one should take about 30 seconds to answer, but you have a maximum 75 seconds per question</li>"+
							"<li>A timer at corner of the screen will show you how much time you have left per question and for the remainder of the test</li>"+
							"<li>You are not allowed to skip questions � it�s to your advantage to try to answer each one within the allocated time, even if you must guess</li>"+
							"</ul>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Please note the following:");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("<ul><li> If you believe you have already taken the EPI under a different email address, please contact <a href='mailto:applicants@teachermatch.org'>applicants@teachermatch.org</a>.</li></ul>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("NEXT STEP: Start the "+EPIOraplication+" by clicking on the link below and using the provided login information.<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append(jobUrl+"<br/> Email: "+teacherDetail.getEmailAddress()+"<br/>");
					if(newCanFlag)
					 sb.append("Password: "+teacherDetail.getPassword()+"<br/><br/>");
					else
						sb.append("Password: the one you created during your initial account signup<br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					if(smartPracticeFlag || newTalentApp){
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("<b>"+counter+".Smart Practices</b> <br><br>");
						sb.append("</td>");
						sb.append("</tr>");
						
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("Following the EPI&trade;, you will go back to your personal applicant dashboard. Upon arrival, look for section \"Personal Planning\", and click the icon associated with \"Smart Practices Professional Development\". You will be routed to the Smart Practices dashboard to begin the pre-hire training modules and assessment.<br><br>");
						sb.append("</td>");
						sb.append("</tr>");
					}
					
					
					
				}else{
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("Thank you for your interest in being a substitute teacher with Kelly Educational Staffing&reg; (KES&reg;).<br></br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("The next step in the application process is for you to complete the brief application.<br></br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("We encourage you to complete the application as soon as possible so we can expedite you through our onboarding process.<br></br>Application<br></br>");
					sb.append("</td>");
					sb.append("</tr>");
					
					sb.append("<tr>");
					sb.append("<td style="+mailCssFontSize+">");
					sb.append("NEXT STEP: Start the application by clicking on the link below and using the provided login information.<br/><br/><a href='"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"'>"+url+"applyteacherjob.do?jobId="+jobOrder.getJobId()+"</a><br/><br/>");
					sb.append("</td>");
					sb.append("</tr>");
					
					
					if(newCanFlag){
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("Email: "+teacherDetail.getEmailAddress()+"<br/>Password: "+teacherDetail.getPassword()+"<br/><br/>");
						sb.append("</td>");
						sb.append("</tr>");
					}else{
						sb.append("<tr>");
						sb.append("<td style="+mailCssFontSize+">");
						sb.append("Email: "+teacherDetail.getEmailAddress()+"<br/>Password: the one you created during your initial account signup<br/><br/>");
						sb.append("</td>");
						sb.append("</tr>");
					}
				}
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+"><br>Please contact "+jobOrder.getHeadQuarterMaster().getHeadQuarterName()+" via phone at 855-535-5915 or via email at KESTMSP@kellyservices.com for the following: </br>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul>"+
						"<li>Technical support or questions related TeacherMatch&trade;</li>"+
						"<li>Technical support or questions related to Smart Practices&trade; (KES substitute teacher and paraprofessional talent only)</li>"+
						"<li> To request a copy of your Professional Development Profile (PDP) upon completion of the TeacherMatch Educators Professional Inventory (EPI) (KES substitute teacher talent only)</li>"+
						"</ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Best,<br/>");
				sb.append("Kelly Educational Staffing&reg; (KES&reg;)<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
			 sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("MMMMMMMMMMMMMMMMM ::::::::::"+sb.toString());
		return sb.toString();
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param request
	 * @param teacherDetail
	 * @param encryptText
	 * @return {@link StringBuffer}
	 */
	public static String getInventoryInvitationMail(HttpServletRequest request,TeacherDetail teacherDetail, String encryptText)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String invitationUrl = Utility.getBaseURL(request)+"service/inventory.do?id="+encryptText;
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Click on this link to start your assessment: <Unique URL><a target='blank' href='"+invitationUrl+"' >"+invitationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}	
		return sb.toString();
	}
	
	public static String sendReminderMailTextForHQ(JobOrder jobOrder, TeacherDetail teacherDetail)
	{
		String firstName 	= 	"";
		String lastName		=	"";
		firstName	=	teacherDetail.getFirstName() == null ? "" : teacherDetail.getFirstName();
		lastName	=	teacherDetail.getLastName() == null ? "" : teacherDetail.getLastName();
		HeadQuarterMaster headQuarterMaster = null;
		if(jobOrder!=null)
			headQuarterMaster = jobOrder.getHeadQuarterMaster();
		String hqName = headQuarterMaster.getHeadQuarterName();
		
		String url=Utility.getValueOfPropByKey("basePath");
		StringBuffer sb = new StringBuffer();
			try {
				sb.append("<table width=\"80%\">");
			
			    sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Dear "+firstName+" "+lastName+",</br></br>");
				sb.append("</td>");
				sb.append("</tr>");
			
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Thank you for your interest as a substitute employee with Kelly Educational Staffing&reg; (KES&reg;). We are always looking for qualified educational staffing talent throughout the school year and are eager to learn more about you.</br></br>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");		
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("To make sure your application is up to date, please <a target='blank' href='"+url+"'> click here </a> to log into your account and view your incomplete job applications under section \"Job Applications\" in the lower right area of the page. Under column \"Status\" look for positions that are listed as \"Incomplete\". You may then click the first icon under \"Actions\" to complete the application.</br></br>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Please contact Kelly Educational Staffing&reg; (KES&reg;) via phone at 855-535-5915 or via email at  <a href='mailto:KESTMSP@kellyservices.com'>KESTMSP@kellyservices.com</a> for the following:<br/></br>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<ul>");
				sb.append("<li>");
				sb.append("Technical support or questions related TeacherMatch�<br/>");
				sb.append("</li>");
				sb.append("<li>");
				sb.append("Technical support or questions related to Smart Practices&reg; (KES substitute teacher and paraprofessional talent only)<br/>");
				sb.append("</li>");
				sb.append("<li>");
				sb.append("To request a copy of your Professional Development Profile (PDP) upon completion of the TeacherMatch Educators Professional Inventory (EPI) (KES substitute teacher talent only) <br/>");
				sb.append("</li>");
				sb.append("</ul>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Best,<br/>Kelly Educational Staffing&reg; (KES&reg;)<br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
			}catch(Exception e) {
				e.printStackTrace();
			}
		System.out.println("sb::::::::::"+sb.toString());
		return sb.toString();
	}
	
	
	public static String getRegistrationMailForSmartPractices(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId()+"&smartpractices=true";
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+": Thank you for registering with TeacherMatch<sup>TM</sup>, your first step toward finding an ideal position.  To complete the registration process, you need to confirm your email address by clicking on the link below.  Once you have done this, you will be able to log on through the Login Page using the information you provided during the sign up process.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Click here to verify your email: <Unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is the one you chose during the sign up process.   If you forgot your password, simply click the \"I forgot\" link on the Login Page.  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("getRegistrationMailForSmartPractices :::::::: "+sb.toString());
		return sb.toString();
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param request
	 * @param teacherDetail
	 * @return {@link String}
	 */
	public static String getRegConfirmationEmailToTeacherForSmartPractices(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			//String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			//String loginUrl = Utility.getBaseURL(request)+"signin.do";
			String loginUrl = Utility.getBaseURL(request)+"smartpractices.do";
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+": Welcome. You have now completed the registration process for TeacherMatch<sup>TM</sup>. At TeacherMatch, we take pride in helping candidates find their best-fit roles. We also believe that great educators need actionable support, which is why we offer a customized professional development report, available after you have completed your Portfolio and the Base Inventory.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address>"+teacherDetail.getEmailAddress()+"");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is the one you chose during the sign up process. If you forgot your password, simply click the \"I forgot\" link on the Login Page. If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please click on <a href='"+loginUrl+"'>"+loginUrl+"</a> to build your Portfolio and to take the inventories appropriate for positions that interest you.");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for using TeacherMatch!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println("getRegConfirmationEmailToTeacherForSmartPractices :::::::: "+sb.toString());
		return sb.toString();
	}
	public static String createUserPwdMailToOtUserForKelly(HttpServletRequest request,UserMaster userMaster,String roleid)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			HttpSession session			=	request.getSession(false);
			UserMaster userSession		=	(UserMaster) session.getAttribute("userMaster");
			String verificationUrl		=	Utility.getBaseURL(request)+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
			sb.append("<table>");

			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getUserName(userMaster)+":<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("You have been added as a Kelly Educational Staffing&reg; (KES&reg;) "+roleid+" on the TeacherMatchTM platform.<br/><br/> Your Login email address is <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>.<br/><br/> In order to activate your account, please visit the link below to set your password: <br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please contact Kelly Educational Staffing via phone at 855-535-5915 or via email at <a href='mailto:KESTMSP@kellyservices.com'>KESTMSP@kellyservices.com</a> for the following:<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul>");
			sb.append("<li>");
			sb.append("Technical support or questions related TeacherMatch�<br/>");
			sb.append("</li>");
			sb.append("<li>");
			sb.append("Technical support or questions related to Smart Practices� (KES substitute teacher and paraprofessional talent only)<br/>");
			sb.append("</li>");
			sb.append("<li>");
			sb.append("To request a copy of your Professional Development Profile (PDP) upon completion of the TeacherMatch Educators Professional Inventory (EPI) (KES substitute teacher talent only) <br/>");
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best,<br/>Kelly Educational Staffing&reg; (KES&reg;)<br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>> :: "+sb.toString());
		
		return sb.toString();
	}
	
	//Mail for Registering in Smart Practices.......  
	public static String getRegistrationMailForSmartPracticesNew(HttpServletRequest request,TeacherDetail teacherDetail,BranchMaster branchMaster)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId()+"&smartpractices=true";
			sb.append("<table>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for registering for Smart Practices, the Kelly Educational Staffing&reg; (KES&reg;), mandatory pre-hire online training program. Powered by TeacherMatch, this introductory professional development series will prepare you for a rewarding position as a substitute educator. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("To complete the registration process, please confirm your email address by clicking on the link below. Once you have done this, you will be able to log on via the Login Page using the information you provided during the sign up process.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			

			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Click here to verify your email: <Unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is the one you chose during the sign up process. If you forgot your password, simply click the \"I forgot\" link on the Login Page. If you want to change your password, click on the Settings link under your name in the upper right of the screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			/*sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Kelly Support Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");*/
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Please contact Kelly Educational Staffing via phone at 855-535-5915 or via email at <a href='mailto:KESTMSP@kellyservices.com'>KESTMSP@kellyservices.com</a> for the following:");
			sb.append("<ul>");
			sb.append("<li>Technical support or questions related TeacherMatch&#8482;</li>");
			sb.append("<li>Technical support or questions related to Smart Practices&#8482; (KES substitute teacher and paraprofessional talent only)</li>");
			sb.append("<li>To request a copy of your Professional Development Profile (PDP) upon completion of the TeacherMatch Educators Professional Inventory (EPI) (KES substitute teacher talent only)</li>");
			sb.append("</ul>");
			sb.append("Best,<br/>");
			sb.append("Kelly Educational Staffing&reg; (KES&reg;)");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please email us <a href='mailto:KESTMSP@kellyservices.com'>KESTMSP@kellyservices.com</a>.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>> :::::::: "+sb.toString());
		return sb.toString();
	}
	
	public static String getRegistrationMailWithKellyJob(HttpServletRequest request,TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = Utility.getBaseURL(request)+"verify.do?key="+teacherDetail.getVerificationCode()+"&id="+teacherDetail.getTeacherId();
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for your interest in being a substitute teacher with Kelly Educational Staffing&reg; (KES&reg;).<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("To complete the registration process, please confirm your email address by clicking on the link below. Once you have done this, you will be able to log on through the Sign In page using the information you provided during the sign up process.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Click here to verify your email:<br/><br/> <Unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is the one you chose during the sign up process.   If you forgot your password, simply click the \"I forgot\" link on the Login Page.  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Please contact Kelly Educational Staffing&reg; (KES&reg;) via phone at 855-535-5915 or via email at KESTMSP@kellyservices.com for the following:<br/>");
			sb.append("<ul>");
			sb.append("<li>Technical support or questions related TeacherMatch&#8482;</li>");
			sb.append("<li>Technical support or questions related to Smart Practices&#8482; (KES substitute teacher and paraprofessional talent only)</li>");
			sb.append("<li>To request a copy of your Professional Development Profile (PDP) upon completion of the TeacherMatch Educators Professional Inventory (EPI) (KES substitute teacher talent only)</li>");
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best,<br/>Kelly Support Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			/*sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:KESTMSP@kellyservices.com.'>KESTMSP@kellyservices.com</a>.");
			sb.append("</td>");
			sb.append("</tr>");*/

			sb.append("</table>");
			System.out.println(">>>>>>>>>>>>>>>   :: "+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}	
		return sb.toString();
	}
	
	public static String getCancelEventMailText(EventSchedule eventSchedule, String userFirstName, String userLastName,EventDetails eventDetails)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String username="";
			String eventTitle="";
			String distritcOrHeadQuarterName="";
			String dateTime="";
			
			if(eventSchedule!=null)
			  dateTime=", scheduled on " +Utility.convertDateAndTimeToUSformatOnlyDate(eventSchedule.getEventDateTime())+","+eventSchedule.getEventStartTime()+""+eventSchedule.getEventStartTimeFormat()+" to "+eventSchedule.getEventEndTime()+""+eventSchedule.getEventEndTimeFormat();
			
			if(userFirstName!=null)
				username = username+userFirstName;
			if(userLastName!=null)
				username = username+" "+userLastName;
			
			if(eventDetails.getCreatedBY()!=null && eventDetails.getCreatedBY().getDistrictId()!=null){
				distritcOrHeadQuarterName=eventDetails.getCreatedBY().getSchoolId()!=null?eventDetails.getCreatedBY().getSchoolId().getSchoolName():eventDetails.getCreatedBY().getDistrictId().getDistrictName();
			}else if(eventDetails.getCreatedBY()!=null && eventDetails.getCreatedBY().getHeadQuarterMaster()!=null){
				distritcOrHeadQuarterName=eventDetails.getCreatedBY().getBranchMaster()!=null?eventDetails.getCreatedBY().getBranchMaster().getBranchName():eventDetails.getCreatedBY().getHeadQuarterMaster().getHeadQuarterName();
			}
				
			if(eventDetails.getEventName()!=null)
				eventTitle = eventDetails.getEventName();
			
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+username+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append(eventTitle+dateTime+" , for the "+distritcOrHeadQuarterName+" has been canceled. Please contact the district for more information about this change.");
			
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<BR>Sincerely,<br>TeacherMatch Client Services Team  <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
			System.out.println(">>>>>>>>>>>>>>>> :: "+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}	
		return sb.toString();
	}
	
	public static String PortfolioReminderMailByLink(TeacherDetail teacherDetail, DistrictMaster districtMaster, String mainURLEncode)
	{
		
		/*WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request;// = context.getHttpServletRequest();
		
		if(WebContextFactory.get()!=null)
			request = WebContextFactory.get().getHttpServletRequest();
		else
			request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();*/
		
		StringBuffer sb = new StringBuffer();
		try {

				//String basePath	=	Utility.getBaseURL(request);
				sb.append("<table>");
				sb.append("<tr>");			
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Thank you for applying to work with " +districtMaster.getDistrictName()+". In order to keep our records up-to-date, we require applicants to log in and update their profiles on a regular basis. Please click the link below to update your profile.   <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<a href="+mainURLEncode+">");
				sb.append(mainURLEncode);
				sb.append("</a> <br/><br/>");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("<tr>");	
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Please note that failure to update your profile within 30 days will result in your profile being withdrawn from the jobs you applied to. If you would like to reapply at any time, simply log into your profile and indicate that you are interested in these jobs again (this will not require you to start the application process from scratch).");
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");
	
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("<br/>Sincerely,<br><br>The Office of Instructional Staffing and Recruitment<br>"+districtMaster.getDistrictName()+"");
				sb.append("</td>");
				sb.append("</tr>");
				
				sb.append("</table>");
		}

		catch(Exception e) {
			e.printStackTrace();
		}

		return sb.toString();
	}
	
	public static String getPNrTextMail(JobOrder jobOrder, SchoolMaster schoolMaster)
	{
		StringBuffer sb = new StringBuffer();
		
		String jobId = (jobOrder!=null&&jobOrder.getJobId()!=null)? ""+jobOrder.getJobId() : "";
		String jobTitle = (jobOrder!=null&&jobOrder.getJobTitle()!=null)? jobOrder.getJobTitle() : "";
		String location = (schoolMaster!=null&&schoolMaster.getAddress()!=null)? schoolMaster.getAddress() : "";
		
		try
		{
			sb.append("<table>");
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Congratulations, you have completed all steps required to be hired into "+jobId+" "+jobTitle+" at "+location+" in Jeffco Public Schools. Please click here <a href='http://www.jeffcopublicschools.org/employment/new_employees/' target='_blank'>http://www.jeffcopublicschools.org/employment/new_employees/</a> to visit our New employee web site. While there please:<br>");
			sb.append("<ul> <li> Download the correct New Hire Paperwork Packet that matches your position and </li>");
			sb.append("<li>Click on the New Employee Orientation link to register for orientation.</li> </ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");	
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br>We look forward to seeing you at an orientation soon.");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely,<br><br>Jeffco Public Schools Employment Team");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sb.toString();
		
	}
	
	public static String statusNoteSendToBranchAdmin(JobOrder jobOrder, TeacherDetail teacherDetail,String statusName)
	{
		StringBuffer sb = new StringBuffer();
		String gridURL = "";
		try{
			gridURL = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&jobOrderType=5";
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hello,<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Status changed of applicant "+teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" who had applied for "+jobOrder.getJobTitle()+" to "+statusName+".</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("You can view the Candidate's Grid <a href='"+gridURL+"'>here</a><br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best,<br/>Kelly Educational Staffing&reg; (KES&reg;)<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:KESTMSP@kellyservices.com.'>KESTMSP@kellyservices.com</a>.");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
			
			//System.out.println(" mail text----------- :: "+sb.toString());
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		
		return sb.toString();
	}
	
	public static String getTeacherSpResetMailText(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		if(locale.equals("fr"))
			return getTeacherEPIResetMailTextFR(request,teacherDetail);
			
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");

			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Smart Practices status has been re-set from \"timed out\" to \"incomplete\". You can now resume your Smart Practices by logging on to the TeacherMatch platform.");
			sb.append("<br><br>Before you resume the Smart Practices, please note the following guidelines. These guidelines have been implemented to ensure that all candidates take the Smart Practices under equitable conditions.");
			sb.append("<br><br>Each item in the Smart Practices has a stipulated time limit and you must respond to each question within its stipulated time limit.");
			sb.append("<br>You must answer all of the questions in one sitting. ");
			sb.append("<br>Make sure that you have at least 90 minutes of uninterrupted time to complete the Smart Practices. ");
			sb.append("<br>Make sure that you have a stable and reliable Internet connection. ");
			sb.append("<br>Do not close your browser or hit the \"back\" button on your browser.");
			sb.append("<br>You are not able to skip questions.");
			sb.append("<br>If you do not follow these guidelines, you will receive two additional warnings before your status will become timed out again. If you have any questions about these guidelines or any aspect of the Smart Practices administration process, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
		
		System.out.println(" "+sb.toString());
		
		return sb.toString();
	}
	
	
	public static String getRegistrationMailForKellyUrlJobSpecific(HttpServletRequest request,TeacherDetail teacherDetail,JobOrder jobOrder,String refType)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			String verificationUrl = "";
			if(jobOrder!=null)
			verificationUrl= Utility.getBaseURL(request)+"signin.do?teacherId="+teacherDetail.getTeacherId()+"&validate=true&jobId="+jobOrder.getJobId()+"&refType="+refType;
			else
				verificationUrl= Utility.getBaseURL(request)+"signin.do?teacherId="+teacherDetail.getTeacherId()+"&validate=true&refType="+refType;
			
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+"<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for your interest in being a substitute teacher with Kelly Educational Staffing&reg; (KES&reg;).<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("To complete the registration process, please confirm your email address by clicking on the link below. Once you have done this, you will be able to log on through the Sign In page using the information you provided during the sign up process.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			
			sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Click here to verify your email:<br/><br/> <Unique URL><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>"); // add <br>
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your Login Email is: <Email Address><a href='mailto:"+teacherDetail.getEmailAddress()+"'>"+teacherDetail.getEmailAddress()+"</a><br/><br/>"); // add <br>
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your password is the one you chose during the sign up process.   If you forgot your password, simply click the \"I forgot\" link on the Login Page.  If you want to change your password, click on the Settings link under your name in the upper right of your screen after you login.<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Please contact Kelly Educational Staffing&reg; (KES&reg;) via phone at 855-535-5915 or via email at KESTMSP@kellyservices.com for the following:<br/>");
			sb.append("<ul>");
			sb.append("<li>Technical support or questions related TeacherMatch&#8482;</li>");
			sb.append("<li>Technical support or questions related to Smart Practices&#8482; (KES substitute teacher and paraprofessional talent only)</li>");
			sb.append("<li>To request a copy of your Professional Development Profile (PDP) upon completion of the TeacherMatch Educators Professional Inventory (EPI) (KES substitute teacher talent only)</li>");
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+"><br/>Best,"); // add <br> and best
			sb.append("<br/>Kelly Educational Staffing<br/><br/>"); // change support team to educational staffing
			sb.append("</td>");
			sb.append("</tr>");

			/*sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:KESTMSP@kellyservices.com.'>KESTMSP@kellyservices.com</a>.");
			sb.append("</td>");
			sb.append("</tr>");*/

			sb.append("</table>");
			System.out.println(">>>>>>>>>>>>>>>   :: "+sb.toString());
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}	
		return sb.toString();
	}
	
	public static String getAssessmentResetMailText(HttpServletRequest request,TeacherDetail teacherDetail,String inventoryName)
	{
		if(locale.equals("fr"))
			return getTeacherEPIResetMailTextFR(request,teacherDetail);

		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Your "+inventoryName+" status has been re-set from \"timed out\" to \"incomplete\". You can now resume your "+inventoryName+" by logging on to the TeacherMatch platform.");
			sb.append("<br><br>Before you resume the "+inventoryName+", please note the following guidelines. These guidelines have been implemented to ensure that all candidates take the "+inventoryName+" under equitable conditions.");
			sb.append("<br><br>Each item in the "+inventoryName+" has a stipulated time limit and you must respond to each question within its stipulated time limit.");
			sb.append("<br>You must answer all of the questions in one sitting. ");
			sb.append("<br>Make sure that you have at least 90 minutes of uninterrupted time to complete the "+inventoryName+". ");
			sb.append("<br>Make sure that you have a stable and reliable Internet connection. ");
			sb.append("<br>Do not close your browser or hit the \"back\" button on your browser.");
			sb.append("<br>You are not able to skip questions.");
			sb.append("<br>If you do not follow these guidelines, you will receive two additional warnings before your status will become timed out again. If you have any questions about these guidelines or any aspect of the "+inventoryName+" administration process, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>.");
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("getAssessmentResetMailText >>>>>>>>>>>>> : "+sb.toString());
		return sb.toString();
	}
	
	public static String notifyDistrictDistribution(TeacherDetail teacherDetail)
	{
		StringBuffer sb = new StringBuffer();
		try 
		{
			sb.append("<table>");
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Applicant "+getTeacherName(teacherDetail)+" has complete the EPI.");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Welcome!<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Client Services Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please email us at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a> or call "+mailContactNo+".");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");

		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}	
		return sb.toString();
	}
	
	public static String getERegContent(HttpServletRequest request , UserMaster userMaster , JobForTeacher jobForTeacher,String workFlow)
	{

	    StringBuffer sb = new StringBuffer();
	    //sb.append("ERegistration Process");
		System.out.println("::::::::::::::::::::getERegContent::::::::::::::::::");
		/*sb.append("<table>");

		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("<a  href='"+Utility.getValueOfPropByKey("baseKellyPath")+"/signin.do'>Click here to begin</a> or you may copy/paste the following link into your browser's address bar: <a href='"+Utility.getValueOfPropByKey("baseKellyPath")+"/signin.do'>"+Utility.getValueOfPropByKey("baseKellyPath")+"/signin.do</a>.");
		sb.append("<br><br>Within the <b>KES Staff and Existing Substitute Talent</b> section, enter your existing TeacherMatch credentials (email address and password) and click the Login button.");
		sb.append("<br><br>From your TeacherMatch Dashboard in <b>Personal Planning</b> (upper left quadrant), locate the eRegistration section and click the associated icon.");
		sb.append("<br><br><b>Helpful Tips:</b>");
		sb.append("<ol>");
		sb.append("<br><br><li>As you move through eRegistration, use the <b>Previous</b> and <b>Save and Continue</b> buttons located at the bottom of each page. <u>Do not use your browser's back button</u>, as it may result in an incomplete transmission of data.</li>");
		sb.append("<br><li>If you have any questions, please contact the Kelly Services IT Service Desk at <b>1-800-KELLY-28</b> (1-800-535-5928).</li>");
		sb.append("</ol>");
		sb.append("</td>");
		sb.append("</tr>");			

		sb.append("<tr>");
		sb.append("<td style="+mailCssFontSize+">");
		sb.append("<br/>Best,<br/>Kelly Educational Staffing<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("<tr>");
		sb.append("<td style="+mailFooterCssFontSize+">");
		sb.append("This is an automated e-mail message. Please do not respond to this e-mail, as this mailbox is not monitored.<br/><br/>");
		sb.append("</td>");
		sb.append("</tr>");

		sb.append("</table>");*/
		
		////////////////////////////////////////////////////////////////////////////////////
		
		try {
			sb.append("<table width=\"80%\">");
		
		    sb.append("<tr>");			
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Hello "+jobForTeacher.getTeacherId().getFirstName()+" "+jobForTeacher.getTeacherId().getLastName()+":</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
		
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			if(workFlow.equals("1"))
				sb.append("<br/>Thank you for choosing Kelly Educational Staffing&reg;. Use the following information to access our eRegistration website:</br></br>");
			else if(workFlow.equalsIgnoreCase("2"))
				sb.append("<br/>Thank you for completing the initial step of Kelly Educational Staffing's eRegistration Process. To continue with the process, use the following information to access our eRegistration website:</br></br>");
			else if(workFlow.equalsIgnoreCase("3"))
				sb.append("<br/>Thank you for completing the second step of Kelly Educational Staffing's eRegistration Process. To continue with the process, use the following information to access our eRegistration website:</br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");		
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><a href='"+Utility.getValueOfPropByKey("baseKellyPath")+"signin.do'>Click here to begin</a> or you may copy/paste the following link into your browser's address bar:</br>");
			sb.append("<br/><a href='"+Utility.getValueOfPropByKey("baseKellyPath")+"signin.do'>"+Utility.getValueOfPropByKey("baseKellyPath")+"signin.do</a></br></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Within the <b><i>KES Staff and Existing Substitute Talent</i></b> section, enter your existing TeacherMatch credentials (email address and password) and click the Login button.<br/></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>From your TeacherMatch&reg; Dashboard in <b><i>Personal Planning</i></b> (upper left quadrant), locate the eRegistration section and click the associated icon.<br/></br>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/><b>Notes:</b></br>");
			sb.append("</td>");
			sb.append("</tr>");				
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<ul>");
			sb.append("<li>");
			sb.append("As you move through eRegistration, use the <b>Previous</b> and <b>Save and Continue</b> buttons located at the bottom of each page. Do not use your browser's back button, as it may result in an incomplete transmissions of data.<br/>");
			sb.append("</li>");
			sb.append("<li>");
			sb.append("If you have any questions, Please contact Kelly Educational Staffing via <a href='https://home-c4.incontact.com/inContact/ChatClient/ChatClient.aspx?poc=366af169-88ee-4e39-b4c9-3502581854ea&bu=4592562' target='_blank'>Chat Support</a>, via email at <a href='mailto:KESTMSP@kellyservices.com'>KESTMSP@kellyservices.com</a> or via phone at 855-535-5915.<br/>");
			sb.append("</li>");
			sb.append("<li>");
			sb.append("This is an automated e-mail message. Please do not respond to this e-mail, as this mailbox is not monitored.<br/>");
			sb.append("</li>");
			sb.append("</ul>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Best,<br/>Kelly Educational Staffing&reg; (KES&reg;)<br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			sb.append("</table>");
		}catch(Exception e) {
			e.printStackTrace();
		}
	System.out.println("sb::::::::::"+sb.toString());
		
		
	return sb.toString();

	}
	
	public static String getAssessmentNotificationsMailText(TeacherDetail teacherDetail,Integer assessmentType,String status){
		StringBuffer sb = new StringBuffer();
		try{
			String inventoryName = "EPI";
			if(assessmentType==2){
				inventoryName = "JSI";
			}
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+getTeacherName(teacherDetail)+",<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");
			
			if(status.equalsIgnoreCase("strt")){//Scenario 1: Started
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Thanks for starting the "+inventoryName+" with TeacherMatch&#33; Keep going&#33;&#33;");
				sb.append("</td>");
				sb.append("</tr>");
			}
			else if(status.equalsIgnoreCase("vlt1")){//Scenario 2: First Timed-Out
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Looks like you&#39;ve taken more than 75 seconds on the question you were working on in the "+inventoryName+". When you restart the assessment, try not to go over the time limit for the individual questions&#33;<br/><br/>");
				sb.append("Just log back in to TeacherMatch to continue from where you left off. Keep going&#33;<br/><br/>");
				sb.append("If you have any questions about this, feel free to call us at 888.312.7231 or email us at <a href='mailto:clientservices@teachermatch.org'>clientservices@teachermatch.org</a>.");
				sb.append("</td>");
				sb.append("</tr>");
			}
			else if(status.equalsIgnoreCase("vlt2")){//Scenario 3: Second Timed-Out
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Oops! Looks like it&#39;s the second time you&#39;ve gone over the time limit of 75 seconds&#46;&#46;&#46; You have one last chance though. Otherwise you&#39;ll have to wait 12 months before you can start the "+inventoryName+" again.<br/><br/>");
				sb.append("Just log back in to TeacherMatch to continue from where you left off. You can do it&#33;&#33;<br/><br/>");
				sb.append("If you have any questions about this, feel free to call us at 888.312.7231 or email us at <a href='mailto:clientservices@teachermatch.org'>clientservices@teachermatch.org</a>.");
				sb.append("</td>");
				sb.append("</tr>");
			}
			else if(assessmentType==1 && status.equalsIgnoreCase("vlt")){//Scenario 4: Final Timed-Out For EPI
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Uh oh&#33; Looks like you didn&#39;t respond in time again, and you&#39;re now timed out of the EPI. Your TeacherMatch "+inventoryName+" status will remain &#8220;Timed Out&#8221; for 12 months.<br/>");
				sb.append("You can still apply to any positions you are interested in that don&#39;t require the EPI and schools are free to continue considering you for employment. EPI results are one of the many data points schools use to guide their hiring process.<br/><br/>");
				sb.append("If you have any questions about this, feel free to call us at 888.312.7231 or email us at <a href='mailto:clientservices@teachermatch.org'>clientservices@teachermatch.org</a>.<br/><br/>");
				sb.append("Thanks for your time &#8212; and best of luck in your job search&#33;");
				sb.append("</td>");
				sb.append("</tr>");
			}
			else if(assessmentType==2 && status.equalsIgnoreCase("vlt")){//Scenario 4: Final Timed-Out For JSI
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Uh oh&#33; Looks like you didn&#39;t respond in time again, and you&#39;re now timed out of "+inventoryName+".<br/>");
				sb.append("If you have any questions about this, feel free to call us at 888.312.7231 or email us at <a href='mailto:clientservices@teachermatch.org'>clientservices@teachermatch.org</a>.<br/><br/>");
				sb.append("Thanks for your time &#8212; and best of luck in your job search&#33;");
				sb.append("</td>");
				sb.append("</tr>");
			}
			else if(status.equalsIgnoreCase("comp")){//Scenario 5: Completion
				sb = new StringBuffer();
				sb.append("<table>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("Good job, "+getTeacherName(teacherDetail)+"&#33; Congratulations on completing the TeacherMatch "+inventoryName+"&#33;");
				sb.append("</td>");
				sb.append("</tr>");
				sb.append("<tr>");
				sb.append("<td style="+mailCssFontSize+">");
				sb.append("If you have any questions about this, feel free to call us at 888.312.7231 or email us at <a href='mailto:clientservices@teachermatch.org'>clientservices@teachermatch.org</a>.<br/><br/>");
				sb.append("Thanks for your time &#8212; We look forward to your continued use of TeacherMatch&#33;");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>The TeacherMatch Team<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailFooterCssFontSize+">");
			sb.append("You are receiving this email from an automated system. Please do not reply to this email. If you need assistance, please contact TeacherMatch Client Services at <a href='mailto:clientservices@teachermatch.net'>clientservices@teachermatch.net</a>. <br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("getAssessmentNotificationsMailText "+status+" >>>>>>>>>>>>> : "+sb.toString());
		return sb.toString();
	}

	public static String getEpiVviCompletionDAMailText(TeacherDetail teacherDetail, UserMaster usermaster, String epivvi,int jobId,String jobname){
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+usermaster.getFirstName()+",<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			if(jobId>0)
				sb.append(teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" has completed the "+epivvi+" portion of the application process for "+jobname+" ( "+jobId+" ).");
			else
				sb.append(teacherDetail.getFirstName()+" "+teacherDetail.getLastName()+" has completed the "+epivvi+" portion of the application process for "+jobname+" event.");
			
			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("For more details, please click "+"<a href='"+Utility.getValueOfPropByKey("basePath")+"/teacherinfo.do'>here</a>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely<br/>TeacherMatch Admin<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("getEpiVviCompletionDAMailText >>>>>>>>>>>>> : "+sb.toString());
		return sb.toString();
	}	
	public static String createUserPwdMailToOtUser(UserMaster userMaster)
	 {
	  
	  String emailid ="";
	  StringBuffer sb = new StringBuffer();
	  try 
	  {
	   String verificationUrl  = Utility.getValueOfPropByKey("basePath")+"resetpassword.do?key="+userMaster.getVerificationCode()+"&id="+userMaster.getUserId();
	   String loginUrl  = Utility.getValueOfPropByKey("basePath")+"signin.do";
	      if(userMaster.getDistrictId()!=null && userMaster.getDistrictId().getDistrictId()==806900 && ( userMaster.getRoleId().getRoleId()==2 || userMaster.getRoleId().getRoleId()==3 || userMaster.getRoleId().getRoleId()==7 )){
	       sb.append("<table>");
	       sb.append("<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>Dear "+userMaster.getFirstName()+",</td></tr>");
	       sb.append("<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>Welcome to the TeacherMatch system! You've been added as a new TeacherMatch user. Please use your universal email address and universal to password to access the system.</td></tr>");
	       sb.append("<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>Click <a href='"+loginUrl+"'>here</a> to log in.</td></tr>");
	       sb.append("<tr><td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>Sincerely,");
	       sb.append("The Human Resources Team<br/>");
	       sb.append("Adams 12 Five Star Schools");
	       sb.append("</td></tr></table>");
	       
	      }else{
	   
	   sb.append("<table>");

	   sb.append("<tr>");
	   sb.append("<td style="+mailCssFontSize+">");
	   sb.append("Dear "+getUserName(userMaster)+":  You have been added as a user on the TeacherMatch<sup>TM</sup> platform.   Your Login Email is <a href='mailto:"+userMaster.getEmailAddress()+"'>"+userMaster.getEmailAddress()+"</a>. Your Authentication PIN is "+userMaster.getAuthenticationCode()+". In order to activate your account, please visit the link below to set your password: ");
	   sb.append("</td>");
	   sb.append("</tr>");

	   sb.append("<tr>");   
	   sb.append("<td style="+mailCssFontSize+">");
	   sb.append("<br/><a target='blank' href='"+verificationUrl+"' >"+verificationUrl+"</a><br/><br/>");
	   sb.append("</td>");
	   sb.append("</tr>");
	   
	   sb.append("<tr>");
	   sb.append("<td style="+mailCssFontSize+">");
	   sb.append("Please contact me at "+emailid+" if you have questions.<br/><br/>");
	   sb.append("</td>");
	   sb.append("</tr>");

	   sb.append("<tr>");
	   sb.append("<td style="+mailFooterCssFontSize+">");
	   sb.append("You are receiving this email from an automated system.   Please do not reply to this email.   If you need assistance, please contact your Admin at "+emailid+".");
	   sb.append("</td>");
	   sb.append("</tr>");
	   
	   sb.append("</table>");
	     }

	  } 
	  catch(Exception e) 
	  {
	   e.printStackTrace();
	  }
	  
	  System.out.println(" createUserPwdMailToOtUserb :: "+sb.toString());
	  
	  return sb.toString();
	 }
	public static String getSARejectMailText(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobOrder jobOrder){
		StringBuffer sb = new StringBuffer();
		try{
			sb.append("<table>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Dear "+teacherDetail.getFirstName()+",<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Thank you for submitting your application to Summit School District for the position of "+ jobOrder.getJobTitle()+" at "+districtMaster.getAddress()+". We have completed our paper screening process for this position.  At this time, we will not be inviting you to interview in person with us for this vacancy.");

			sb.append("</td>");
			sb.append("</tr>");			

			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("We appreciate your interest in Summit School District and would like to invite you to apply for other positions that you may be qualified for and/or interested in.  To learn more about our current vacancies, please visit our website at <a href='"+"www.summit.k12.co.us"+"'>www.summit.k12.co.us.</a>");
			sb.append("</td>");
			sb.append("</tr>");	
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("Again, thank you for your interest in Summit School District and we wish you the best of luck in your employment search.");
			sb.append("</td>");
			sb.append("</tr>");			
			
			sb.append("<tr>");
			sb.append("<td style="+mailCssFontSize+">");
			sb.append("<br/>Sincerely<br/><br/><br/>Summit School District<br/><br/>");
			sb.append("</td>");
			sb.append("</tr>");

			sb.append("</table>");
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("getSARejectMailText >>>>>>>>>>>>> : "+sb.toString());
		return sb.toString();
	}
}