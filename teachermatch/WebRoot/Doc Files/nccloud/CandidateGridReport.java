package tm.controller.report;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherDetail;
import tm.bean.TeacherElectronicReferences;
import tm.bean.TeacherElectronicReferencesHistory;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.PanelAttendees;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.UserFolderStructure;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherElectronicReferencesDAO;
import tm.dao.TeacherElectronicReferencesHistoryDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.PanelAttendeesDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.master.UserFolderStructureDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.UserMasterDAO;
import tm.utility.TestTool;
import tm.utility.Utility;

/*
 * Candidate Grid report page url
 */
@Controller
public class CandidateGridReport 
{
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	public void setTimeZoneMasterDAO(TimeZoneMasterDAO timeZoneMasterDAO) {
		this.timeZoneMasterDAO = timeZoneMasterDAO;
	}
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) {
		this.userMasterDAO = userMasterDAO;
	}
	@Autowired
	private UserFolderStructureDAO userFolderStructureDAO;
	public void setUserFolderStructureDAO(
			UserFolderStructureDAO userFolderStructureDAO) {
		this.userFolderStructureDAO = userFolderStructureDAO;
	}
	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private StateMasterDAO stateMasterDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private PanelAttendeesDAO panelAttendeesDAO;
	
	@Autowired 
	private TeacherElectronicReferencesDAO teacherElectronicReferencesDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherElectronicReferencesHistoryDAO teacherElectronicReferencesHistoryDAO;
	
	@Autowired
	HeadQuarterMasterDAO headQuarterMasterDAO;
	
	String locale = Utility.getValueOfPropByKey("locale");	 
	
	@RequestMapping(value="/candidatereport.do", method=RequestMethod.GET)
	public String doCandidateGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("candidategridreport");
		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("roleAccess", roleAccess);
		
		return "candidategridreport";
	}
	@RequestMapping(value="/candidategridold.do", method=RequestMethod.GET)
	public String doCandidateGridGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
		}
		String jobTitle="";
		JobOrder jobOrder=null;
		try{
			String jobId=null;
			if(request.getParameter("jobId")!=null){
				jobId=request.getParameter("jobId").toString();
			}
			if(jobId!=null){
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId),false,false);
				jobTitle=jobOrder.getJobTitle();
			}
		}catch(Exception e){}
		
		try{
			if(jobOrder!=null && userMaster.getEntityType()!=1){
				if(jobOrder.getDistrictMaster().getIsReqNoForHiring()!=null && jobOrder.getDistrictMaster().getIsReqNoForHiring()){
					map.addAttribute("isReqNoForHiring",1);	
				}else{
					map.addAttribute("isReqNoForHiring",0);
				}
			}else{
				map.addAttribute("isReqNoForHiring",0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("recentJobTitle",jobTitle);
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
		String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
		try{
			lstStatusMasters = statusMasterDAO.findStatusByShortNames(statusTotal);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if(userMaster.getEntityType()!=1){
			map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
			map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
		}else{
			map.addAttribute("DistrictName",null);
		}
		if(userMaster.getEntityType()==3){
			map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
			map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
		}else{
			map.addAttribute("SchoolName",null);
		}
		map.addAttribute("entityType",userMaster.getEntityType());
		
		map.addAttribute("lstStatusMasters",lstStatusMasters);
		List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
		lstStateMaster = stateMasterDAO.findAllStateByOrder();
		map.addAttribute("lstStateMaster",lstStateMaster);
		map.addAttribute("roleAccess", roleAccess);
		map.addAttribute("dateTime", Utility.getDateTime());
		map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
		
		// Change Format for date
		String	dateWithoutTime	=	Utility.getDateWithoutTime(new Date());
		System.out.println(":::::::::::	dateWithoutTime ::::::::::::::"+dateWithoutTime);
		
		try{
			String assetClasses = "Gold:Stocks:Fixed Income:Commodity:Interest Rates";
			String[] splits = dateWithoutTime.split("-");

			String year		=	splits[0];
			String month	=	splits[1];
			String day		=	splits[2];
			
			dateWithoutTime	=	month+"-"+day+"-"+year;
			map.addAttribute("getDateWithoutTime", dateWithoutTime);
			System.out.println(":::::::::::	dateWithoutTime ::::::::::::::"+dateWithoutTime);

		} catch(Exception e){
			e.printStackTrace();
		}
		
		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,statusNotes=false;
		if(userMaster.getEntityType()!=1)
		{

			try{

				DistrictMaster districtMasterObj=(DistrictMaster)userMaster.getDistrictId();
				if(districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj.getDisplayTFA()){
					tFA=true;
				}
				if(districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				if(districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}
				if(districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}
				if(districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
				if(districtMasterObj.getStatusNotes()){
					statusNotes=true;
				}
			}catch(Exception e){ e.printStackTrace();}
		}else
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}
		Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
		prefMap.put("achievementScore", achievementScore);
		prefMap.put("tFA", tFA);
		prefMap.put("demoClass", demoClass);
		prefMap.put("JSI", JSI);
		prefMap.put("teachingOfYear", teachingOfYear);
		prefMap.put("expectedSalary", expectedSalary);
		prefMap.put("fitScore", fitScore);
		map.addAttribute("statusNotes",statusNotes);
		map.addAttribute("prefMap",prefMap);
		
		return "candidategrid";
	} 
	@RequestMapping(value="/slideractForDecimal.do", method=RequestMethod.GET)
	public String impDecimalGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println(" ================ slideractForDecimal.do ================= ");
		try 
		{
			String name="sliderdemo";
			if(request.getParameter("name")!=null){
				name=request.getParameter("name");
			}
			
			String max="100";
			if(request.getParameter("max")!=null){
				max=request.getParameter("max");
			}
			String tickInterval="1";
			if(request.getParameter("tickInterval")!=null){
				tickInterval=request.getParameter("tickInterval");
			}
			String swidth="200";
			if(request.getParameter("swidth")!=null){
				swidth=request.getParameter("swidth");
			}
			String svalue="0";
			if(request.getParameter("svalue")!=null){
				svalue=request.getParameter("svalue");
			}
			String dslider="1";
			if(request.getParameter("dslider")!=null){
				dslider=request.getParameter("dslider");
			}
			
			String step="0.1";
			if(request.getParameter("step")!=null){
				step=request.getParameter("step");
			}
			
			String answerId="0";
			if(request.getParameter("answerId")!=null){
				answerId=request.getParameter("answerId");
			}
			
			String answerId_msu="0";
			if(request.getParameter("answerId_msu")!=null){
				answerId_msu=request.getParameter("answerId_msu");
			}
			
			map.addAttribute("max",max);
			map.addAttribute("tickInterval",tickInterval);
			map.addAttribute("hiddenval",name);
			map.addAttribute("swidth",swidth);
			map.addAttribute("svalue",svalue);
			map.addAttribute("dslider",dslider);
			map.addAttribute("step",step);
			map.addAttribute("answerId",answerId);
			
			map.addAttribute("answerId_msu",answerId_msu);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "slideract";
	}
	
	
	@RequestMapping(value="/slideract.do", method=RequestMethod.GET)
	public String impGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			String name="sliderdemo";
			if(request.getParameter("name")!=null){
				name=request.getParameter("name");
			}
			
			String max="100";
			if(request.getParameter("max")!=null){
				max=request.getParameter("max");
			}
			String tickInterval="1";
			if(request.getParameter("tickInterval")!=null){
				tickInterval=request.getParameter("tickInterval");
			}
			String swidth="200";
			if(request.getParameter("swidth")!=null){
				swidth=request.getParameter("swidth");
			}
			String svalue="0";
			if(request.getParameter("svalue")!=null){
				svalue=request.getParameter("svalue");
			}
			String dslider="1";
			if(request.getParameter("dslider")!=null){
				dslider=request.getParameter("dslider");
			}
			
			String step="1";
			if(request.getParameter("step")!=null){
				step=request.getParameter("step");
			}
			
			String answerId="0";
			if(request.getParameter("answerId")!=null){
				answerId=request.getParameter("answerId");
			}
			
			String answerId_msu="0";
			if(request.getParameter("answerId_msu")!=null){
				answerId_msu=request.getParameter("answerId_msu");
			}
			
			map.addAttribute("max",max);
			map.addAttribute("tickInterval",tickInterval);
			map.addAttribute("hiddenval",name);
			map.addAttribute("swidth",swidth);
			map.addAttribute("svalue",svalue);
			map.addAttribute("dslider",dslider);
			map.addAttribute("step",step);
			map.addAttribute("answerId",answerId);
			
			map.addAttribute("answerId_msu",answerId_msu);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "slideract";
	} 
	
	/*@RequestMapping(value="/questionslideract.do", method=RequestMethod.GET)
	public String getQuestionSliderGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			String name="sliderdemo";
			if(request.getParameter("name")!=null){
				name=request.getParameter("name");
			}
			
			String max="100";
			if(request.getParameter("max")!=null){
				max=request.getParameter("max");
			}
			String tickInterval="1";
			if(request.getParameter("tickInterval")!=null){
				tickInterval=request.getParameter("tickInterval");
			}
			String swidth="200";
			if(request.getParameter("swidth")!=null){
				swidth=request.getParameter("swidth");
			}
			String svalue="0";
			if(request.getParameter("svalue")!=null){
				svalue=request.getParameter("svalue");
			}
			String dslider="1";
			if(request.getParameter("dslider")!=null){
				dslider=request.getParameter("dslider");
			}
			
			String step="1";
			if(request.getParameter("step")!=null){
				step=request.getParameter("step");
			}
			
			String answerId="0";
			if(request.getParameter("answerId")!=null){
				answerId=request.getParameter("answerId");
			}
			
			map.addAttribute("max",max);
			map.addAttribute("tickInterval",tickInterval);
			map.addAttribute("hiddenval",name);
			map.addAttribute("swidth",swidth);
			map.addAttribute("svalue",svalue);
			map.addAttribute("dslider",dslider);
			map.addAttribute("step",step);
			map.addAttribute("answerId",answerId);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "questionslideract";
	}*/
	
	@RequestMapping(value="/tree.do", method=RequestMethod.GET)
	public String treeGET(ModelMap map,HttpServletRequest request)
	{
			HttpSession session = request.getSession(false);
			if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		    }
			UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
			UserMaster userMaster					=	null;
			List<UserFolderStructure> lstTreeStructure=	null;
			StringBuffer sb 						=	new StringBuffer();
			int sharedFolderParentId=0;
			try 
			{
				
				userMaster							=	userMasterDAO.findById(userSession.getUserId(), false, false);
				Criterion criterion1				=	Restrictions.eq("usermaster", userMaster);
				lstTreeStructure					=	userFolderStructureDAO.findByCriteria(criterion1);
				sb.append("<ul>");
				for(UserFolderStructure tree: lstTreeStructure)
				{
					if(tree.getUserFolderStructure()==null)
					{
						
						sb.append("<li id='"+tree.getFolderId()+"' class=\"folder\">"+tree.getFolderName()+" ");
						if(tree.getChildren().size()>0)
						sb.append(printChild(request, tree.getChildren(),tree));
					}
				}
				sb.append("</ul>");
				
				String name="Treedemo";
				
				map.addAttribute("tree",sb.toString());
				map.addAttribute("name",name);
				map.addAttribute("frame_folderId","1");
				
				
			}catch (Exception e) {
				e.printStackTrace();
			}		
		 
		
		return "tree";
	}
	
	public String printChild(HttpServletRequest request,List<UserFolderStructure> lstuserChildren,UserFolderStructure tree)
	{
		StringBuffer sbchild=new StringBuffer();
		sbchild.append("<ul>");
		for(UserFolderStructure subfL: lstuserChildren )
		{
				sbchild.append("<li id='"+subfL.getFolderId()+"' class=\"folder\">"+subfL.getFolderName()+"");
					if(subfL.getChildren().size()>0){
						
						sbchild.append(printChild(request, subfL.getChildren(),subfL));
					}
					sbchild.append("</li>");
		}
		sbchild.append("</ul>");
		return sbchild.toString();
	}
	
	@RequestMapping(value="/panelscore.do", method=RequestMethod.GET)
	public String doPanelScoreGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("panelscore.do");
		UserMaster userMaster=null;
		
		String id = request.getParameter("id");
		if(id!=null)
		{
			HttpSession session = request.getSession(true);
			
			//String encId = Utility.encodeText(id.trim());
			String encId = id.trim();
			try {
				String forMated = Utility.decodeBase64(encId);
				String arr[] = forMated.split("###");
				if(arr==null || arr.length!=2)
				{
					map.addAttribute("errorMsg","1");
				}else
				{
					Integer pnlId=Utility.decryptNo(Integer.parseInt(arr[0]));
					Integer usrId=Utility.decryptNo(Integer.parseInt(arr[1]));
					userMaster = userMasterDAO.findById(usrId, false, false);
					if(userMaster.getIsExternal()==null || userMaster.getIsExternal()==false)
					{
						map.addAttribute("errorMsg","2");
					}else
					{
						PanelSchedule panelSchedule = panelScheduleDAO.findById(pnlId, false, false);
						List<PanelAttendees> panelAttendees = panelAttendeesDAO.getPanelAttendee(panelSchedule,userMaster);
						System.out.println("panelSchedule:"+panelSchedule.getPanelStatus());
						System.out.println("userMaster.getStatus():"+userMaster.getStatus());
						if(panelAttendees.size()>0)
						{
							String panelStatus = panelSchedule.getPanelStatus();
							if(panelStatus.equalsIgnoreCase("Scheduled"))
							{
								map.addAttribute("panelSchedule",panelSchedule);
								map.addAttribute("userMaster",userMaster);
								map.addAttribute("errorMsg","0");
								session.setAttribute("userMaster",userMaster);
							}else if(panelStatus.equalsIgnoreCase("Cancelled"))
							{
								map.addAttribute("errorMsg","4");
							}else if(panelStatus.equalsIgnoreCase("Completed"))
							{
								map.addAttribute("errorMsg","5");
							}else
								map.addAttribute("errorMsg","6");
								
						}else
						{
							map.addAttribute("errorMsg","3");
						}
					}
				}
				
			} catch (Exception e) {
				//e.printStackTrace();
				map.addAttribute("errorMsg","1");
			}
		}else
		{
			map.addAttribute("errorMsg","1");
		}
		
		return "panelscore";
	}
	
	
	
	@RequestMapping(value="/candidategrid.do", method=RequestMethod.GET)
	public String doCandidateGridGETNew(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		TestTool.getTraceTime("100");
		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		boolean notAccesFlag=false;
		boolean isHqBr = false;
		DistrictMaster dMaster=null;
		Cookie[] cookies = request.getCookies();
		for(int i=0;i<cookies.length;i++)
		{
			if(cookies[i].getName().equals("cgUrl"))
			{
				System.out.println("Cookie already exists");
				cookies[i].setValue("");
			}
		}
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			try
			
			{
				String jobId=(String)request.getParameter("jobId");
				String jobOrderType=(String)request.getParameter("JobOrderType");
				if(jobId!=null && jobOrderType!=null)
				{
					Cookie cookie = new Cookie ("cgUrl","candidategrid.do?jobId="+jobId+"&JobOrderType="+jobOrderType);
					cookie.setMaxAge(60);
					response.addCookie(cookie);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			

			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
				dMaster=userMaster.getDistrictId();
			}
		}
		String jobTitle="";
		JobOrder jobOrder=null;
		try{
			String jobId=null;
			if(request.getParameter("jobId")!=null){
				jobId=request.getParameter("jobId").toString();
			}
			if(jobId!=null){
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId),false,false);
				try{
					map.addAttribute("jobCategoryId",jobOrder.getJobCategoryMaster().getJobCategoryId());
					map.addAttribute("districtMaster",jobOrder.getDistrictMaster());
				}catch(Exception e){
					e.printStackTrace();
				}
				jobTitle=jobOrder.getJobTitle();
			}
		}catch(Exception e){}
		
		try{
			if(!userMaster.getEntityType().equals(5) && !userMaster.getEntityType().equals(6)){
				if(jobOrder!=null && !userMaster.getEntityType().equals(1)){
					if(jobOrder.getDistrictMaster()!=null && dMaster.getDistrictId()!=null &&  !jobOrder.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
			}
			else if(userMaster.getEntityType().equals(5)){
				if(jobOrder!=null && jobOrder.getHeadQuarterMaster()==null){
					notAccesFlag=true;
					isHqBr = true;
				}
			}
			else if(userMaster.getEntityType().equals(6)){
				if((jobOrder!=null && jobOrder.getHeadQuarterMaster()==null) || (jobOrder!=null && jobOrder.getBranchMaster()!=null && !jobOrder.getBranchMaster().getBranchId().equals(userMaster.getBranchMaster().getBranchId()))){
					notAccesFlag=true;
					isHqBr = true;
				}
			}
			
			if(notAccesFlag)
			{
				PrintWriter out = response.getWriter();
				response.setContentType("text/html"); 
				out.write("<script type='text/javascript'>");
				out.write("alert('You have no access to this job.');");
				if(isHqBr)
					out.write("window.location.href='managehqbrjoborders.do';");
				else
					out.write("window.location.href='managejoborders.do';");
				out.write("</script>");
				return null;
			}
		}catch(Exception e){e.printStackTrace();}
		
		try{
			if(jobOrder!=null && userMaster.getEntityType()!=1){
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getIsReqNoForHiring()!=null && jobOrder.getDistrictMaster().getIsReqNoForHiring()){
					map.addAttribute("isReqNoForHiring",1);	
				}else{
					map.addAttribute("isReqNoForHiring",0);
				}
				//By Mukesh
				List<JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
				if(userMaster.getDistrictId()!=null)
					lstJobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userMaster.getDistrictId());
				else if(userMaster.getBranchMaster()!=null)
					lstJobCategoryMasters = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(userMaster.getHeadQuarterMaster(),userMaster.getBranchMaster(),null);
				else if(userMaster.getHeadQuarterMaster()!=null)
					lstJobCategoryMasters = jobCategoryMasterDAO.findJobCategorysByHeadQuarter(userMaster.getHeadQuarterMaster());
								
				map.addAttribute("lstJobCategoryMasters",lstJobCategoryMasters);
				
				List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
				subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(userMaster.getDistrictId());
				map.addAttribute("subjectMasters",subjectMasters);
				List<GeoZoneMaster> listGeoZoneMasters=new ArrayList<GeoZoneMaster>();
				if(userMaster.getDistrictId()!=null){	
					Criterion criterion=Restrictions.eq("districtMaster",userMaster.getDistrictId());
					listGeoZoneMasters=geoZoneMasterDAO.findByCriteria(criterion);
				}
			 	map.addAttribute("listGeoZoneMasters",listGeoZoneMasters);
			}else{
				map.addAttribute("isReqNoForHiring",0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("recentJobTitle",jobTitle);
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
		String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
		try{
			lstStatusMasters = statusMasterDAO.findStatusByShortNames(statusTotal);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if(userMaster.getEntityType()!=1){
			if(userMaster.getEntityType()==5){
				map.addAttribute("HeadQuarterName",userMaster.getHeadQuarterMaster().getHeadQuarterName());
				map.addAttribute("headQuarterId",userMaster.getHeadQuarterMaster().getHeadQuarterId());
			}else if(userMaster.getEntityType()==6){
				map.addAttribute("BranchName",userMaster.getBranchMaster().getBranchName());
				map.addAttribute("branchId",userMaster.getBranchMaster().getBranchId());
				if(userMaster.getBranchMaster().getHeadQuarterMaster()!=null){
					map.addAttribute("HeadQuarterName",userMaster.getBranchMaster().getHeadQuarterMaster().getHeadQuarterName());
					map.addAttribute("headQuarterId",userMaster.getBranchMaster().getHeadQuarterMaster().getHeadQuarterId());
				}
			}else {
				map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
			}
		}else{
			map.addAttribute("DistrictName",null);
		}
		if(userMaster.getEntityType()==3){
			map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
			map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
		}else{
			map.addAttribute("SchoolName",null);
		}
		map.addAttribute("entityType",userMaster.getEntityType());
		
		map.addAttribute("lstStatusMasters",lstStatusMasters);
		List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
		lstStateMaster = stateMasterDAO.findAllStateByOrder();
		map.addAttribute("lstStateMaster",lstStateMaster);
		map.addAttribute("roleAccess", roleAccess);
		map.addAttribute("dateTime", Utility.getDateTime());
		map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
		
		// Change Format for date
		String	dateWithoutTime	=	Utility.getDateWithoutTime(new Date());
		System.out.println(":::::::::::	dateWithoutTime ::::::::::::::"+dateWithoutTime);
		
		try{
			String assetClasses = "Gold:Stocks:Fixed Income:Commodity:Interest Rates";
			String[] splits = dateWithoutTime.split("-");

			String year		=	splits[0];
			String month	=	splits[1];
			String day		=	splits[2];
			
			dateWithoutTime	=	month+"-"+day+"-"+year;
			map.addAttribute("getDateWithoutTime", dateWithoutTime);
			System.out.println(":::::::::::	dateWithoutTime ::::::::::::::"+dateWithoutTime);

		} catch(Exception e){
			e.printStackTrace();
		}
		
		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,statusNotes=false;
		if(userMaster.getEntityType()!=1)
		{

			try{
				if(userMaster.getDistrictId()!=null){
					DistrictMaster districtMasterObj=(DistrictMaster)userMaster.getDistrictId();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getStatusNotes()){
						statusNotes=true;
					}
				}
			}catch(Exception e){ e.printStackTrace();}
		}else
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}
		Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
		prefMap.put("achievementScore", achievementScore);
		prefMap.put("tFA", tFA);
		prefMap.put("demoClass", demoClass);
		prefMap.put("JSI", JSI);
		prefMap.put("teachingOfYear", teachingOfYear);
		prefMap.put("expectedSalary", expectedSalary);
		prefMap.put("fitScore", fitScore);
		map.addAttribute("statusNotes",statusNotes);
		map.addAttribute("prefMap",prefMap);
		
		StringBuffer taglists=new StringBuffer();
		if(jobOrder!=null && jobOrder.getDistrictMaster()!=null){
			List<SecondaryStatusMaster> listSecondaryStatusMasters = secondaryStatusMasterDAO.findTagswithDistrictSpecificTags(jobOrder);
			if(listSecondaryStatusMasters!=null && listSecondaryStatusMasters.size()>0){
				taglists.append("<option value=''>Select Tag</option>");
				for(SecondaryStatusMaster ssm : listSecondaryStatusMasters){
					taglists.append("<option value='"+ssm.getSecStatusId()+"'>"+ssm.getSecStatusName()+"</option>");	
				}
			}
		}
		map.put("tagslist", taglists.toString());
		if(jobOrder.getDistrictMaster()!=null)
			map.put("districtIdforEvent", jobOrder.getDistrictMaster().getDistrictId());
		TestTool.getTraceTime("199");
		return "candidategridnew";
	}
	
	@RequestMapping(value="/referencecheck.do", method=RequestMethod.GET)
	public String doReferenceCheckGET(ModelMap map,HttpServletRequest request)
	{
		//request.getSession();
		System.out.println(":::::::::::::::::: referencecheck.do ::::::::::::::::::::::::");
		
		String jobTitle = "";
		String p	 	= "";
		
		TeacherElectronicReferences teacherElectronicReferences	=null;
		DistrictMaster districtMaster	=	null;
		HeadQuarterMaster headQuarterMaster = null;
		JobOrder jobOrder 				= 	null;
		TeacherDetail teacherDetail		=	null;
		
		try{
			Integer jobId 			= 	0;
			Integer distId 			= 	0;
			Integer headId 			= 	0;
			Integer teacherId 		= 	0;
			Integer elerefAutoId	=	0;
			if(request.getParameter("p")!=null){
				p=request.getParameter("p").toString();
			}
			p = Utility.decodeBase64(p);
			System.out.println(p);
			
			String array[]		=	p.split("&");
			String arrayRef[]	=	array[0].split("=");
			String arrayDist[]	=	array[1].split("=");
			String arrayHead[] = new String[2];
			String arrayJob[] = new String[2];
			String arrayTeach[] = new String[2];
			if(array.length==5)
			{
				arrayHead	=	array[2].split("=");
				arrayJob	=	array[3].split("=");
				arrayTeach	=	array[4].split("=");
			}else
			{
				arrayJob	=	array[2].split("=");
				arrayTeach	=	array[3].split("=");
			}
			
			
			elerefAutoId		= 	Utility.decryptNo(Integer.valueOf(arrayRef[1]));
			try {
				if(arrayDist.length==2)
				distId 				= 	Utility.decryptNo(Integer.valueOf(arrayDist[1]));
				if(arrayHead.length==2 && array.length==5)
				headId 				= 	Utility.decryptNo(Integer.valueOf(arrayHead[1]));
				teacherId 			= 	Utility.decryptNo(Integer.valueOf(arrayTeach[1]));
			} catch (Exception e) {
				e.printStackTrace();
			}

			try{
				if(arrayJob[1] != null){
					jobId 				= 	Utility.decryptNo(Integer.valueOf(arrayJob[1]));
				}
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			if(elerefAutoId!=null){
				teacherElectronicReferences = teacherElectronicReferencesDAO.findById(elerefAutoId, false, false);
			}
			if(distId!=null && distId!=0){
				districtMaster = districtMasterDAO.findById(distId, false, false);
			}
			else if(headId!=null && headId !=0){
				headQuarterMaster = headQuarterMasterDAO.findById(headId, false, false);
			}
			if(jobId!=null) {
				jobOrder	=	jobOrderDAO.findById(jobId, false,false);
			}
			if(teacherId!=null) {
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}
			
			map.addAttribute("teacherElectronicReferences",teacherElectronicReferences);
			map.addAttribute("districtMaster",districtMaster);
			map.addAttribute("headQuarterMaster", headQuarterMaster);
		
			map.addAttribute("jobOrder",jobOrder);
			map.addAttribute("teacherDetail",teacherDetail);
			
			JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
			
			map.addAttribute("jobForTeacher", jobForTeacher);
			
		}catch(Exception e){e.printStackTrace();}

		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory = null;
		//teacherElectronicReferencesHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdContactStatus(teacherElectronicReferences, districtMaster, jobOrder, teacherDetail);
		teacherElectronicReferencesHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDistIdContactStatus(teacherElectronicReferences,headQuarterMaster, districtMaster, teacherDetail);
		if(teacherElectronicReferencesHistory.size() > 0){
			Integer contactStatus = 1;
			map.addAttribute("contactStatus",contactStatus);
			return "referencecheckfinal";
			
		} else {
			return "referencecheck";
			
		}
	}
	
	@RequestMapping(value="/referencecheckfinal.do", method=RequestMethod.GET)
	public String doReferenceCheckFinalGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(":::::::::::::: referencecheckfinal.do :::::::::::::::::");
		return "referencecheckfinal";
		
	}


	@RequestMapping(value="/referencecheckpool.do", method=RequestMethod.GET)
	public String doReferenceCheckPoolGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(":::::::::::::::::: referencecheckpool.do ::::::::::::::::::::::::");
		
		String jobTitle = "";
		String p	 	= "";
		
		TeacherElectronicReferences teacherElectronicReferences	=null;
		DistrictMaster districtMaster	=	null;
		HeadQuarterMaster headQuarterMaster = null;
		JobOrder jobOrder 				= 	null;
		TeacherDetail teacherDetail		=	null;
		
		try{
			Integer jobId 			= 	0;
			Integer distId 			= 	0;
			Integer teacherId 		= 	0;
			Integer elerefAutoId	=	0;
			if(request.getParameter("p")!=null){
				p=request.getParameter("p").toString();
			}
			p = Utility.decodeBase64(p);
			
			String array[]		=	p.split("&");
			String arrayRef[]	=	array[0].split("=");
			String arrayDist[]	=	array[1].split("=");
			String arrayJob[]	=	array[2].split("=");
			String arrayTeach[]	=	array[3].split("=");
			
			elerefAutoId		= 	Utility.decryptNo(Integer.valueOf(arrayRef[1]));
			distId 				= 	Utility.decryptNo(Integer.valueOf(arrayDist[1]));
			teacherId 			= 	Utility.decryptNo(Integer.valueOf(arrayTeach[1]));

			if(elerefAutoId!=null){
				teacherElectronicReferences = teacherElectronicReferencesDAO.findById(elerefAutoId, false, false);
			}
			if(distId!=null){
				districtMaster = districtMasterDAO.findById(distId, false, false);
			}
			/*if(jobId!=null) {
				jobOrder	=	jobOrderDAO.findById(jobId, false,false);
			}*/
			if(teacherId!=null) {
				teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
			}

			JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);

			map.addAttribute("teacherElectronicReferences",teacherElectronicReferences);
			map.addAttribute("districtMaster",districtMaster);
			map.addAttribute("jobOrder",jobOrder);
			map.addAttribute("teacherDetail",teacherDetail);
			map.addAttribute("jobForTeacher", jobForTeacher);
			
		}catch(Exception e){e.printStackTrace();}

		List<TeacherElectronicReferencesHistory> teacherElectronicReferencesHistory = null;
		//teacherElectronicReferencesHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdContactStatus(teacherElectronicReferences, districtMaster, jobOrder, teacherDetail);
		teacherElectronicReferencesHistory	=	teacherElectronicReferencesHistoryDAO.findAllHistoryByRefIdAndDistIdContactStatus(teacherElectronicReferences,headQuarterMaster, districtMaster, teacherDetail);
		if(teacherElectronicReferencesHistory.size() > 0){
			Integer contactStatus = 1;
			map.addAttribute("contactStatus",contactStatus);
			return "referencecheckfinal";
			
		} else {
			return "referencecheck";
			
		}
	}
	
	// for Optimization  BY Anurag

	@RequestMapping(value="/candidatesgrid.do", method=RequestMethod.GET)
	public String doCandidateGridGEToptimizeNew(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		TestTool.getTraceTime("100");
		UserMaster userMaster=null;
		HttpSession session = request.getSession(false);
		int roleId=0;
		boolean notAccesFlag=false;
		boolean isHqBr = false;
		DistrictMaster dMaster=null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
				dMaster=userMaster.getDistrictId();
			}
		}
		String jobTitle="";
		JobOrder jobOrder=null;
		try{
			String jobId=null;
			if(request.getParameter("jobId")!=null){
				jobId=request.getParameter("jobId").toString();
			}
			if(jobId!=null){
				jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId),false,false);
				try{
					map.addAttribute("jobCategoryId",jobOrder.getJobCategoryMaster().getJobCategoryId());
					map.addAttribute("districtMaster",jobOrder.getDistrictMaster());
				}catch(Exception e){
					e.printStackTrace();
				}
				jobTitle=jobOrder.getJobTitle();
			}
		}catch(Exception e){}
		
		try{
			if(!userMaster.getEntityType().equals(5) && !userMaster.getEntityType().equals(6)){
				if(jobOrder!=null && !userMaster.getEntityType().equals(1)){
					if(jobOrder.getDistrictMaster()!=null && dMaster.getDistrictId()!=null &&  !jobOrder.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
			}
			else if(userMaster.getEntityType().equals(5)){
				if(jobOrder!=null && jobOrder.getHeadQuarterMaster()==null){
					notAccesFlag=true;
					isHqBr = true;
				}
			}
			else if(userMaster.getEntityType().equals(6)){
				if((jobOrder!=null && jobOrder.getHeadQuarterMaster()==null) || (jobOrder!=null && jobOrder.getBranchMaster()!=null && !jobOrder.getBranchMaster().getBranchId().equals(userMaster.getBranchMaster().getBranchId()))){
					notAccesFlag=true;
					isHqBr = true;
				}
			}
			
			if(notAccesFlag)
			{
				PrintWriter out = response.getWriter();
				response.setContentType("text/html"); 
				out.write("<script type='text/javascript'>");
				out.write("alert('You have no access to this job.');");
				if(isHqBr)
					out.write("window.location.href='managehqbrjoborders.do';");
				else
					out.write("window.location.href='managejoborders.do';");
				out.write("</script>");
				return null;
			}
		}catch(Exception e){e.printStackTrace();}
		
		try{
			if(jobOrder!=null && userMaster.getEntityType()!=1){
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getIsReqNoForHiring()!=null && jobOrder.getDistrictMaster().getIsReqNoForHiring()){
					map.addAttribute("isReqNoForHiring",1);	
				}else{
					map.addAttribute("isReqNoForHiring",0);
				} 
				//By Mukesh
				List<JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
				    if(userMaster.getDistrictId()!=null)
					lstJobCategoryMasters = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(userMaster.getDistrictId());
				else if(userMaster.getBranchMaster()!=null)
					lstJobCategoryMasters = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(userMaster.getHeadQuarterMaster(),userMaster.getBranchMaster(),null);
				else if(userMaster.getHeadQuarterMaster()!=null)
					lstJobCategoryMasters = jobCategoryMasterDAO.findJobCategorysByHeadQuarter(userMaster.getHeadQuarterMaster());
				 
				List<SubjectMaster> subjectMasters = new ArrayList<SubjectMaster>();
				 	subjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(userMaster.getDistrictId());
				map.addAttribute("subjectMasters",subjectMasters);
				List<GeoZoneMaster> listGeoZoneMasters=new ArrayList<GeoZoneMaster>();
				if(userMaster.getDistrictId()!=null){	
					Criterion criterion=Restrictions.eq("districtMaster",userMaster.getDistrictId());
					  	listGeoZoneMasters=geoZoneMasterDAO.findByCriteria(criterion);
				}
			 	map.addAttribute("listGeoZoneMasters",listGeoZoneMasters);
			}else{
				map.addAttribute("isReqNoForHiring",0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		map.addAttribute("recentJobTitle",jobTitle);
		String roleAccess=null;
		try{
			roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
		}catch(Exception e){
			e.printStackTrace();
		}
		List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
		String[] statusTotal = {"comp","icomp","hird","rem","widrw","vlt"};
		try{
			  	lstStatusMasters = statusMasterDAO.findStatusByShortNames(statusTotal);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		if(userMaster.getEntityType()!=1){
			if(userMaster.getEntityType()==5){
				map.addAttribute("HeadQuarterName",userMaster.getHeadQuarterMaster().getHeadQuarterName());
				map.addAttribute("headQuarterId",userMaster.getHeadQuarterMaster().getHeadQuarterId());
			}else if(userMaster.getEntityType()==6){
				map.addAttribute("BranchName",userMaster.getBranchMaster().getBranchName());
				map.addAttribute("branchId",userMaster.getBranchMaster().getBranchId());
			}else {
				map.addAttribute("DistrictName",userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictId",userMaster.getDistrictId().getDistrictId());
			}
		}else{
			map.addAttribute("DistrictName",null);
		}
		if(userMaster.getEntityType()==3){
			map.addAttribute("SchoolName",userMaster.getSchoolId().getSchoolName());
			map.addAttribute("SchoolId",userMaster.getSchoolId().getSchoolId());
		}else{
			map.addAttribute("SchoolName",null);
		}
		map.addAttribute("entityType",userMaster.getEntityType());
		
		map.addAttribute("lstStatusMasters",lstStatusMasters);
		List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
		   lstStateMaster = stateMasterDAO.findAllStateByOrder();
		map.addAttribute("lstStateMaster",lstStateMaster);
		map.addAttribute("roleAccess", roleAccess);
		map.addAttribute("dateTime", Utility.getDateTime());
		 	map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
		
		// Change Format for date
		String	dateWithoutTime	=	Utility.getDateWithoutTime(new Date());
		System.out.println(":::::::::::	dateWithoutTime ::::::::::::::"+dateWithoutTime);
		
		try{
			String assetClasses = "Gold:Stocks:Fixed Income:Commodity:Interest Rates";
			String[] splits = dateWithoutTime.split("-");

			String year		=	splits[0];
			String month	=	splits[1];
			String day		=	splits[2];
			
			dateWithoutTime	=	month+"-"+day+"-"+year;
			map.addAttribute("getDateWithoutTime", dateWithoutTime);
			System.out.println(":::::::::::	dateWithoutTime ::::::::::::::"+dateWithoutTime);

		} catch(Exception e){
			e.printStackTrace();
		}
		
		boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false,statusNotes=false;
		if(userMaster.getEntityType()!=1)
		{

			try{
				if(userMaster.getDistrictId()!=null){
					DistrictMaster districtMasterObj=(DistrictMaster)userMaster.getDistrictId();
					if(districtMasterObj.getDisplayAchievementScore()){
						achievementScore=true;
					}
					if(districtMasterObj.getDisplayTFA()){
						tFA=true;
					}
					if(districtMasterObj.getDisplayDemoClass()){
						demoClass=true;
					}
					if(districtMasterObj.getDisplayJSI()){
						JSI=true;
					}
					if(districtMasterObj.getDisplayYearsTeaching()){
						teachingOfYear=true;
					}
					if(districtMasterObj.getDisplayExpectedSalary()){
						expectedSalary=true;
					}
					if(districtMasterObj.getDisplayFitScore()){
						fitScore=true;
					}
					if(districtMasterObj.getStatusNotes()){
						statusNotes=true;
					}
				}
			}catch(Exception e){ e.printStackTrace();}
		}else
		{
			achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
		}
		Map<String,Boolean> prefMap = new HashMap<String, Boolean>();
		prefMap.put("achievementScore", achievementScore);
		prefMap.put("tFA", tFA);
		prefMap.put("demoClass", demoClass);
		prefMap.put("JSI", JSI);
		prefMap.put("teachingOfYear", teachingOfYear);
		prefMap.put("expectedSalary", expectedSalary);
		prefMap.put("fitScore", fitScore);
		map.addAttribute("statusNotes",statusNotes);
		map.addAttribute("prefMap",prefMap);
		
		StringBuffer taglists=new StringBuffer();
		if(jobOrder!=null && jobOrder.getDistrictMaster()!=null){
			List<SecondaryStatusMaster> listSecondaryStatusMasters =  secondaryStatusMasterDAO.findTagswithDistrictSpecificTags(jobOrder);
			if(listSecondaryStatusMasters!=null && listSecondaryStatusMasters.size()>0){
				taglists.append("<option value=''>Select Tag</option>");
				for(SecondaryStatusMaster ssm : listSecondaryStatusMasters){
					taglists.append("<option value='"+ssm.getSecStatusId()+"'>"+ssm.getSecStatusName()+"</option>");	
				}
			}
		}
		map.put("tagslist", taglists.toString());
		if(jobOrder.getDistrictMaster()!=null)
			map.put("districtIdforEvent", jobOrder.getDistrictMaster().getDistrictId());
		TestTool.getTraceTime("199");
		return "candidategridnewOp";
	}
}

