package tm.dao;

import static tm.services.district.GlobalServices.println;

import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictSpecificApprovalFlow;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.EventDetails;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.MailSendToTeacher;
import tm.services.ReferenceCheckAjax;
import tm.services.district.GlobalServices;
import tm.services.district.PrintOnConsole;
import tm.services.es.ElasticSearchService;
import tm.services.teacher.AcceptOrDeclineMailHtml;
import tm.servlet.WorkThreadServlet;
import tm.utility.ElasticSearchConfig;
import tm.utility.TestTool;
import tm.utility.Utility;
import org.hibernate.type.Type;
import org.hibernate.Hibernate;

public class JobForTeacherDAO extends GenericHibernateDAO<JobForTeacher, Long> 
{
	public JobForTeacherDAO() 
	{
		super(JobForTeacher.class);
	}
	@Autowired
	private CommonService commonService;
	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;
	public void setUserEmailNotificationsDAO(
			UserEmailNotificationsDAO userEmailNotificationsDAO) {
		this.userEmailNotificationsDAO = userEmailNotificationsDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	
	

	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private ReferenceCheckAjax referenceCheckAjax;
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private EmailerService emailerService;
	@Autowired
	private DistrictSpecificApprovalFlowDAO districtSpecificApprovalFlowDAO;
	@Autowired
	private DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	@Transactional(readOnly=false)
	public void makePersistent(JobForTeacher entity)
	{
		JobForTeacher entityPrevious=null;
		try{
			if(entity.getJobForTeacherId()!=null && entity.getJobId().getDistrictMaster()!=null && entity.getJobId().getDistrictMaster().getAutoInactiveOrNot()){
				entityPrevious=(JobForTeacher)getSessionFactory().openSession().load(JobForTeacher.class, entity.getJobForTeacherId());
			}
		}catch (HibernateException h) {
			h.printStackTrace();
		}
		int sendMail=0;
		try
		{
			if(entity!=null && entity.getJobId().getDistrictMaster()!=null
					&& entity.getJobId().getDistrictMaster().getJobCompletionEmail()
					&& entity.getJobId().getDistrictMaster().getHeadQuarterMaster()!=null && entity.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()!=null && entity.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId().equals(2) 
					&& entity.getStatus()!=null && entity.getStatus().getStatusId()!=null &&  entity.getStatus().getStatusId()==4 && (entity.getSendMail()==null || !entity.getSendMail()))
			{
				System.out.println("-------------------JobForTeacher--------------");
				entity.setSendMail(true);
				sendMail=1;
				System.out.println(entity.getJobId());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try {
			super.makePersistent(entity);
			/*******************************************All District***************************************/
			try{
							if(entityPrevious!=null && entity.getJobId().getDistrictMaster()!=null && ((entityPrevious.getStatus().getStatusId()!=6 && entity.getStatus().getStatusId()==6) || (entityPrevious.getStatus().getStatusId()==6 && entity.getStatus().getStatusId()!=6)))
							{
								try {
										List<String> allEmailIds=new ArrayList<String>();
										if(entity.getJobId().getDistrictMaster().getDistrictId().equals(804800))
											allEmailIds=getGroupWiseAllUser(entity.getJobId(), entity.getJobId().getSchool().get(0));
										else
											allEmailIds=getAllSAandDAsEmailIds(entity.getJobId());
											
										new Thread(new AutoInactiveJobOrderThread(entityPrevious.getJobId(),jobOrderDAO,emailerService,allEmailIds)).start();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							try{
								 if(entity!=null && entity.getJobId().getDistrictMaster()!=null
											&& entity.getJobId().getDistrictMaster().getJobCompletionEmail()
											&& entity.getJobId().getDistrictMaster().getHeadQuarterMaster()!=null && entity.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()!=null && entity.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId().equals(2) 
											&& entity.getStatus()!=null && entity.getStatus().getStatusId()!=null &&  entity.getStatus().getStatusId()==4 && sendMail==1)
									{//headquartercheck
									try
									{
										System.out.println(entity.getStatus());
										if(entity.getStatus()!=null)
											System.out.println(entity.getStatus().getStatusId());
										if(entity.getStatus()!=null && entity.getStatus().getStatusId()!=null && entity.getStatus().getStatusId().equals(new Integer(4)))
										{
											WebContext context;
											context = WebContextFactory.get();
											HttpServletRequest request = context.getHttpServletRequest();
											HttpSession session = request.getSession(false);
											TeacherDetail teacherDetail = null;
											if (session != null || session.getAttribute("teacherDetail") != null) 
												teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
											JobOrder jobOrder = entity.getJobId();
											List<UserMaster> lstusermaster	=	new ArrayList<UserMaster>();

											try{
												/* ===== Gagan : statusJobApply is used to get status short name against Apply which is used in Email sending to all active DA\SA Active Admins who has checked thier job apply CHECKBOX in notification [ Available ]======= */
												StatusMaster statusJobApply = statusMasterDAO.findStatusByShortName("apl");
												String statusShortName = statusJobApply.getStatusShortName();
												boolean isSchool=false;
												if(jobOrder.getSelectedSchoolsInDistrict()!=null){
													if(jobOrder.getSelectedSchoolsInDistrict()==1){
														isSchool=true;
													}
												}
												if(jobOrder.getCreatedForEntity()==2 && isSchool==false){
													lstusermaster=userEmailNotificationsDAO.getUserByDistrict(jobOrder.getDistrictMaster(),statusShortName);
												}else if(jobOrder.getCreatedForEntity()==2 && isSchool){
													List<SchoolMaster>  lstschoolMaster= new ArrayList<SchoolMaster>();
													if(jobOrder.getSchool()!=null){
														if(jobOrder.getSchool().size()!=0){
															for(SchoolMaster sMaster : jobOrder.getSchool()){
																lstschoolMaster.add(sMaster);
															}
														}
													}
													lstusermaster=userEmailNotificationsDAO.getUserByDistrictAndSchoolList(jobOrder.getDistrictMaster(),lstschoolMaster,jobOrder,statusShortName);
												}else if(jobOrder.getCreatedForEntity()==3){
													lstusermaster=userEmailNotificationsDAO.getUserByOnlySchool(jobOrder.getSchool().get(0),statusShortName);
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(entity.getTeacherId());
											TeacherAssessmentStatus _teacherAssessmentStatus =null;
											StatusMaster _statusMaster =	null;

											if(entity.getJobId().getJobCategoryMaster().getBaseStatus())
											{
												List<TeacherAssessmentStatus> _teacherAssessmentStatusList = null;
												JobOrder _jOrder = new JobOrder();
												_jOrder.setJobId(0);
												_teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(entity.getTeacherId(),_jOrder);
												if(_teacherAssessmentStatusList.size()>0){
													_teacherAssessmentStatus = _teacherAssessmentStatusList.get(0);
													_statusMaster = teacherAssessmentStatus.getStatusMaster();
												}else{
													_statusMaster = statusMasterDAO.findStatusByShortName("icomp");
												}
											}
											String[] arrHrDetail = commonService.getHrDetailToTeacher(request,entity.getJobId());
											arrHrDetail[10]	=	Utility.getBaseURL(request)+"signin.do";
											arrHrDetail[11] =	Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+entity.getJobId().getJobId()+"&JobOrderType="+entity.getJobId().getCreatedForEntity();
											if(entity.getJobId().getDistrictMaster()!=null)
												try {
													arrHrDetail[12] =	Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(entity.getJobId().getDistrictMaster().getDistrictId()));
												} catch (IOException e1) {
													arrHrDetail[12] ="";
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
											else
												arrHrDetail[12] ="";
											arrHrDetail[13] =	Utility.getValueOfPropByKey("basePath")+"/forgotpassword.do";
											try {
												if(_statusMaster.getStatusShortName()!=null)
													arrHrDetail[14] =	_statusMaster.getStatusShortName();
											} catch (NullPointerException e) {
												//e.printStackTrace();
											}
											String flagforEmail	="applyJob";
											//entity.setSendMail(true);
											MailSendToTeacher mst =new MailSendToTeacher(teacherDetail,jobOrder,request,arrHrDetail,flagforEmail,lstusermaster,emailerService,entity);
											Thread currentThread = new Thread(mst);
											currentThread.start(); 
										
										}
									}
									catch(Exception e)
									{
										e.printStackTrace();
									}
								}
								else{
									System.out.println(" entity (Jobforteacher ) is NULL ");
								}
								
							}catch(Exception e){
								e.printStackTrace();
							}
							/*String num = Utility.getValueOfPropByKey("isAPILive");
							if(num!=null && !num.equals(""))
							{
								int live=Utility.getIntValue(num);
								if(live==1)
								 {
									try {
											if(entity.getJobId().getDistrictMaster()!=null && entity.getJobId().getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800") && entity.isStatusChange() && entity.isStatusMasterChange() && entity.getStatus().getStatusShortName().equalsIgnoreCase("hird") && entity.getStatusMaster().getStatusShortName().equalsIgnoreCase("hird"))
											{
												try {
													JeffcoAPIController.jcTMHireCandidate(entity,super.getSessionFactory());
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}*/
			}catch(Exception e){e.printStackTrace();}
			/*******************************************End Jeffco***************************************/
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			if( entity.isStatusChange() )
				referenceCheckAjax.sendMail(entity);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		
		
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherForOffer(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("offerReady",true);
			Criterion criterion3 = Restrictions.eq("offerReady",false);
			Criterion criterion4 = Restrictions.or(criterion3,criterion2);
			lstJobForTeacher = findByCriteria(criterion1,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherOneYear(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Date sDate= null;
			Date eDate= null;
			
			/*Calendar cal = Calendar.getInstance();
			cal.set(cal.get(cal.YEAR),0,1,0,0,0);
			sDate=cal.getTime();
			cal.set(cal.get(cal.YEAR),11,31,23,59,59);
			eDate = cal.getTime();*/
			
			
			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			cal1.add(Calendar.DATE, -365);
			sDate = cal1.getTime();
			eDate = cal2.getTime();
			
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacher(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstJobForTeacher = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	/*======== Gagan  ============*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findDistrictOrSchoolLatestAppliedJobByTeacher(TeacherDetail teacherDetail,DistrictMaster districtMaster,List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstStatusMasters.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.in("status", lstStatusMasters);
	
				criteria.add(criterion1);
				criteria.add(criterion2);
				if(districtMaster!=null)
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				lstJobForTeacher = criteria.list();
			}

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/*======== Gagan:findIncompleteJoborderforwhichBaseIsRequired  ============*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findIncompleteJoborderforwhichBaseIsRequired(TeacherDetail teacherDetail,StatusMaster statusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:1");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", statusMaster);
			Criterion criterion3 = Restrictions.eq("isAffilated", 0); 

			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);

			Criteria criteria1= criteria.createCriteria("jobId").add(Restrictions.le("jobStartDate",dateWithoutTime)).add(Restrictions.ge("jobEndDate",dateWithoutTime));
			criteria1.createCriteria("jobCategoryMaster").add(Restrictions.le("baseStatus",true));
			//lstJobForTeacher = findByCriteria(criterion3);	

			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}


	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByFilterTeacher(List<TeacherDetail> teacherDetailsist)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(teacherDetailsist.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",teacherDetailsist);
				lstJobForTeacher = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* Gagan : Get Jobforteacherlist from jobforteacherids ========== */
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherList(List jobforteacherlistteacherIdList)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{	if(jobforteacherlistteacherIdList.size()>0){
				Criterion criterion1 = Restrictions.in("jobForTeacherId",jobforteacherlistteacherIdList);
				lstJobForTeacher = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJobByTeacher(Order  sortOrderStrVal,TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstJobForTeacher = findByCriteria(sortOrderStrVal,criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/*
	 * 
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatus(TeacherDetail teacherDetail, StatusMaster statusMaster)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",statusMaster);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			lstJobForTeacher = findByCriteria(Order.desc("createdDateTime"),criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}


	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJobByTeacherAndStatus(Order  sortOrderStrVal,TeacherDetail teacherDetail, StatusMaster statusMaster)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",statusMaster);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			lstJobForTeacher = findByCriteria(sortOrderStrVal,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JobByTeacher And Status During CurrentYear.
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusDuringCurrentYear(TeacherDetail teacherDetail, StatusMaster statusMaster)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(cal.YEAR),0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(cal.YEAR),11,31,23,59,59);

			Date eDate=cal.getTime();

			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",statusMaster);
			//Criterion criterion3 = Restrictions.between("createdDateTime", sDate, eDate);

			//lstJobForTeacher = findByCriteria(criterion1,criterion2,criterion3);
			//Date check removed for time being
			lstJobForTeacher = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JobByTeacher And Status During CurrentYear.
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusDuringCurrentYear(TeacherDetail teacherDetail, List<StatusMaster> statusMasterList)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(statusMasterList.size()>0){	
				Calendar cal = Calendar.getInstance();
				cal.set(cal.get(cal.YEAR),0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(cal.YEAR),11,31,23,59,59);
				Date eDate=cal.getTime();
	
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.in("status",statusMasterList);
				//Date check removed for time being
				lstJobForTeacher = findByCriteria(criterion1,criterion2);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JobByTeacherAndSatusFormJobId.
	 */

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusFormJobId(TeacherDetail teacherDetail, JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			lstJobForTeacher = findByCriteria(criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JobByTeacherAndSatusFormJobIds.
	 */

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusFormJobIds(TeacherDetail teacherDetail, List<JobOrder> jobOrders)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(jobOrders.size()>0){
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.in("jobId",jobOrders);
				Criterion criterion3 = Restrictions.and(criterion1,criterion2);
				lstJobForTeacher = findByCriteria(criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobsForJobSpecificInventory(TeacherDetail teacherDetail, StatusMaster statusMaster, Boolean assesmentStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:2");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", statusMaster);
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime))
			.add(Restrictions.eq("status", "A"));
			//lstJobForTeacher = findByCriteria(criterion3);	

			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJobsForJobSpecificInventory(Order sortOrderStrVal,TeacherDetail teacherDetail, StatusMaster statusMaster, Boolean assesmentStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:3");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", statusMaster);
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime))
			.add(Restrictions.eq("status", "A"))
			.addOrder(sortOrderStrVal);
			//lstJobForTeacher = findByCriteria(criterion3);	

			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public JobForTeacher findJobByTeacherAndJob(TeacherDetail teacherDetail, JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			lstJobForTeacher = findByCriteria(criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			return null;
		else
			return lstJobForTeacher.get(0);	
	}

	// Gagan : Getting Latest Cover Letter by Teacher
	@Transactional(readOnly=false)
	public JobForTeacher findJobByTeacherAndCoverLetter(TeacherDetail teacherDetail)
	{
		JobForTeacher jobForTeacher	=	 null;	
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.isNotNull("coverLetter");
			Criterion criterion3 = Restrictions.ne("coverLetter","");
			Criterion criterion4 = Restrictions.and(criterion2,criterion3);

			lstJobForTeacher = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			return null;
		else
		{
			for(JobForTeacher jft: lstJobForTeacher)
			{
				if(jft.getCoverLetter().replaceAll("\\<[^>]*>","").length()>0)
				{
					jobForTeacher	=	jft;
					break;
				}
			}
			return jobForTeacher;
		}
	}


	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTbyJobOrder(JobOrder jobOrder, List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstStatusMasters.size()>0){
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.in("status", lstStatusMasters);			
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.ne("status",statusMaster);
				lstJobForTeacher = findByCriteria(criterion1,criterion2,criterion3);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTbyJobOrderForCG(JobOrder jobOrder, List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstStatusMasters.size()>0){
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.in("status", lstStatusMasters);			
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.ne("status",statusMaster);
				Criterion criterion4 = Restrictions.eq("cgUpdated",true);			
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3, criterion4);	
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findByJobOrderForCG(JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.in("status", lstStatusMasters);			
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion3 = Restrictions.ne("status",statusMaster);
			Criterion criterion7 = Restrictions.ne("isAffilated",1);
			if(cgUpdate==0){
				Criterion criterion4 = Restrictions.isNull("cgUpdated");
				Criterion criterion5 = Restrictions.eq("cgUpdated",false);
				Criterion criterion6 = Restrictions.or(criterion4, criterion5);
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3,criterion6,criterion7);
			}else if(cgUpdate==1){
				Criterion criterion4 = Restrictions.eq("cgUpdated",true);
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3,criterion4,criterion7);
			}else if(cgUpdate==2){
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3,criterion7);
			}else if(cgUpdate==5){
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3);
			}else if(cgUpdate==3){
				criterion7 = Restrictions.eq("isAffilated",1);
				lstJobForTeacher = findByCriteria(criterion7,criterion2, criterion3);
			}else{
				lstJobForTeacher = findByCriteria(criterion2, criterion3);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}
	/* @Author: Gagan 
	 * @Discription: For Sorting Hired Applicant Grid
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJFTbyJobOrder(Order  sortOrderStrVal,JobOrder jobOrder, List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstStatusMasters.size()>0){
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.in("status", lstStatusMasters);			
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.ne("status",statusMaster);
				lstJobForTeacher = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
	/* @Author: Gagan 
	 * @Discription: Get Total no of Applicant from JobForTeacher Table from Joborder.
	 */
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTApplicantbyJobOrder(JobOrder jobOrder)
	{
		List<JobForTeacher> JobForTeacherApplicant= null;
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);		
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			JobForTeacherApplicant = findByCriteria(Order.asc("createdDateTime"),criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return JobForTeacherApplicant;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobForTeacher> findJFTApplicantbyJobOrders(List<JobOrder> lstJobOrder) 
	{
		Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.add(criterion1)
		.list();
		Map<Integer,JobForTeacher> jobForTeacherMap = new HashMap<Integer, JobForTeacher>();
		JobForTeacher jobForTeacher = null;
		int i=0;
		for (Object object : result) {
			jobForTeacher=((JobForTeacher)object);
			jobForTeacherMap.put(new Integer(""+i),jobForTeacher);
			i++;
		}
		return jobForTeacherMap;
	}

	/* @Author: Gagan 
	 * @Discription: Get Total no of Applicant from JobForTeacher Table from Joborder.
	 */
	/*======= Find findSortedJFTApplicantbyJobOrder  Method */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJFTApplicantbyJobOrder(Order  sortOrderStrVal,JobOrder jobOrder)
	{
		List<JobForTeacher> JobForTeacherApplicant= null;
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);		
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			JobForTeacherApplicant = findByCriteria(sortOrderStrVal,criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return JobForTeacherApplicant;
	}
	/* @Author: Sekhar 
	 * @Discription: Get Total no of Records from JobForTeacher Table from Joborder.
	 */
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobForTeacher> findJFTApplicantbyJobOrder() 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.list();
		Map<Integer,JobForTeacher> jobForTeacherMap = new HashMap<Integer, JobForTeacher>();
		JobForTeacher jobForTeacher = null;
		int i=0;
		for (Object object : result) {
			jobForTeacher=((JobForTeacher)object);
			jobForTeacherMap.put(new Integer(""+i),jobForTeacher);
			i++;
		}
		return jobForTeacherMap;
	}
	/* @Author: Sekhar 
	 * @Discription: Get Total no of Hires from JobForTeacher Table from Joborder.
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTHiresByJobOrder(JobOrder jobOrder)
	{
		List<JobForTeacher> JobForTeacherApplicant= null;
		try{
			//StatusMaster statusMaster= statusMasterDAO.findStatusByShortName("hird");
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion criterion1 = Restrictions.eq("status",statusMaster);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			JobForTeacherApplicant = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return JobForTeacherApplicant;
	}

	@Transactional(readOnly=false)	
	public List<JobForTeacher> findHireByJobForDistrict(JobOrder jobOrder,DistrictMaster districtMaster) 
	{		
		Criterion criterion3 =null;
		if(districtMaster!=null)
		{
			List<UserMaster> lstUserMasters = userMasterDAO.getUserByDistrict(districtMaster);

			if(lstUserMasters.size()>0)
				criterion3 = Restrictions.in("updatedBy",lstUserMasters);
		}
		//StatusMaster statusMaster = statusMasterDAO.findStatusByShortName("hird");
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
		List<JobForTeacher> lstJobForTeachers = null;		
		Criterion criterion1 = Restrictions.eq("jobId", jobOrder);	
		Criterion criterion2 = Restrictions.eq("status", statusMaster);	

		if(criterion3!=null)
			lstJobForTeachers = findByCriteria(criterion1,criterion2,criterion3);
		else
			lstJobForTeachers = findByCriteria(criterion1,criterion2);


		return lstJobForTeachers;
	}

	@Transactional(readOnly=false)	
	public List<JobForTeacher> findHireByJobForSchool(JobOrder jobOrder,SchoolMaster schoolMaster) 
	{	
		Criterion criterion3 =null;
		if(schoolMaster!=null && schoolMaster.getSchoolId()>0)
		{
			List<UserMaster> lstUserMasters = userMasterDAO.getUserBySchool(schoolMaster);

			if(lstUserMasters.size()>0)
				criterion3 = Restrictions.in("updatedBy",lstUserMasters);
		}
		//StatusMaster statusMaster = statusMasterDAO.findStatusByShortName("hird");
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
		List<JobForTeacher> lstJobForTeachers = null;		
		Criterion criterion1 = Restrictions.eq("jobId", jobOrder);	
		Criterion criterion2 = Restrictions.eq("status", statusMaster);	
		if(criterion3!=null)
			lstJobForTeachers = findByCriteria(criterion1,criterion2,criterion3);
		else
			lstJobForTeachers = findByCriteria(criterion1,criterion2);

		return lstJobForTeachers;
	}


	/* @Author: Gagan 
	 * @Discription: Get Total no of Applicant from JobForTeacher Table from Joborder(JobId).
	 */
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTApplicantsbyJobOrder(List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try {// vishwanath
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			String hql = " from jobforteacher WHERE jobId IN (:lstJobOrder) and status !=(:status)  group by teacherId";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameterList("lstJobOrder", lstJobOrder);
			query.setParameter("status", statusMaster);
			lstJobForTeacher = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
	/* @Author: Vishwanath 
	 * @Discription: Get Total no of Applicant from jobforteacher Table from Joborder(JobId).
	 */
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<TeacherDetail> findUniqueApplicantsbyJobOrder(List<JobOrder> lstJobOrder)
	{
		List<TeacherDetail> lstJobForTeacher= null;
		try 
		{
			if(lstJobOrder.size()>0){
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				criteria.setProjection(Projections.distinct(Projections.property("teacherId")));
				lstJobForTeacher = criteria 
				.add(Restrictions.in("jobId",lstJobOrder)) 
				.list();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}

	/* @Author: Vishwanath 
	 * @Discription: Get Total no of Applicant from jobforteacher Table from Joborder(JobId).
	 */
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacherJobsHasSameAssessment(TeacherDetail teacherDetail,AssessmentDetail assessmentDetail)
	{
		PrintOnConsole.getJFTPrint("JFT:4");
		List<JobForTeacher> jobForTeacherList= new ArrayList<JobForTeacher>();
		try 
		{

			Session session = getSession();
			Query qry = session.createQuery("from jobforteacher jft, AssessmentJobRelation ajr where jft.jobId=ajr.jobId and jft.teacherId="+teacherDetail.getTeacherId()+" and ajr.assessmentId="+assessmentDetail.getAssessmentId());

			//jobForTeacherList = qry.list();
			List l = qry.list();
			Iterator it=l.iterator();

			while(it.hasNext())
			{
				Object rows[] = (Object[])it.next();
				jobForTeacherList.add((JobForTeacher)rows[0]);
			}
			return jobForTeacherList;

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return null;
	}

	@Transactional(readOnly=false)
	public JobForTeacher findJFTByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			return null;
		else
			return lstJobForTeacher.get(0);		
	}

	/* @Author: Vishwanath 
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public List<TeacherDetail> getJobAppliedByTeachers(List<TeacherDetail> teacherDetails)
	{
		try 
		{
			if(teacherDetails.size()>0){
				Session session = getSession();
				List results = session.createCriteria(getPersistentClass())
				.add(Restrictions.in("teacherId",teacherDetails)) 
				.setProjection(Projections.groupProperty("teacherId")
				).list();
	
				return results;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		


		return null;
	}

	/* @Author: Vishwanath 
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public List<TeacherDetail> getJobAppliedByTeachersYesterday(List<TeacherDetail> teacherDetails)
	{
		PrintOnConsole.getJFTPrint("JFT:5");
		try 
		{
			if(teacherDetails.size()>0){
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				Date dateWithoutTime = cal.getTime();
	
				Calendar cal1 = Calendar.getInstance();
				cal1.add(Calendar.DATE, -1);
				cal1.set(Calendar.HOUR_OF_DAY, 0);
				cal1.set(Calendar.MINUTE, 0);
				cal1.set(Calendar.SECOND, 0);
				cal1.set(Calendar.MILLISECOND, 0);
	
				Criterion criterion1 = Restrictions.le("createdDateTime",dateWithoutTime);
				Criterion criterion2 = Restrictions.ge("createdDateTime",cal1.getTime());
	
				Session session = getSession();
				List results = session.createCriteria(getPersistentClass())
				.add(Restrictions.in("teacherId",teacherDetails)) 
				.setProjection(Projections.groupProperty("teacherId")
				).list();
	
				return results;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		


		return null;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByJob(int entityID,List<JobOrder> lstJobOrder,Order order, DistrictMaster districtMaster,String firstName,String lastName,String emailAddress)
	{
		PrintOnConsole.getJFTPrint("JFT:6");
		List<TeacherDetail> lstTeacherDetail= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			boolean flag=false;
			if(lstJobOrder.size()>0 && entityID==3){
				flag=true;
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null && entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			if(entityID==1){
				flag=true;
			}
			if(flag){
				if(entityID!=1){
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.eq("status","A"))
					.addOrder(order);
				}else{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
					.addOrder(order);
				}
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrderByTeacher(List<Integer> statusMasterList,Order order, TeacherDetail teacherDetail,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,SubjectMaster subjectMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:7");
		List<JobOrder> jobOrderList= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(lstJobOrder.size()>0){
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
				criteria.createCriteria("jobId").addOrder(order);
			}else if(districtMaster!=null)
			{
				Criteria criteriaJobId=criteria.createCriteria("jobId");
				criteriaJobId.add(Restrictions.eq("districtMaster", districtMaster)).addOrder(order);
				if(jobCategoryMaster!=null)
					criteriaJobId.add(Restrictions.eq("jobCategoryMaster", jobCategoryMaster));
				if(subjectMaster!=null)
					criteriaJobId.add(Restrictions.eq("subjectMaster", subjectMaster));
				
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).addOrder(order);
			}
			else if(headQuarterMaster!=null && branchMaster==null){
				if(headQuarterMaster.getHeadQuarterId()==1)
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}
			else if(branchMaster!=null){
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			}
			else{
				criteria.createCriteria("jobId").addOrder(order);
			}

			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("jobId"));
			jobOrderList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrderList;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForTeacher(){
		List<JobForTeacher> lstJobForTeacher= null;
		try{
			lstJobForTeacher = findByCriteria(Order.asc("createdDateTime"));		
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByDS(List<Integer> statusMasterList,List<JobOrder> lstJobOrder,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:8");
		List<JobForTeacher> lstJobForTeacher= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			if(lstJobOrder.size()>0){
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
			//criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public Boolean findIsPortfolioNeededByTeacher(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstJobForTeacher = findByCriteria(criterion1);	
			if(lstJobForTeacher.size()>0)
			{
				for (JobForTeacher jobForTeacher : lstJobForTeacher) {
					if(jobForTeacher.getJobId().getIsPortfolioNeeded())
						return true;
				}
			}
			else
				return true;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return false;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForUpdateInCG()
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			//StatusMaster statusHide = statusMasterDAO.findStatusByShortName("hide");
			StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.eq("cgUpdated",false);			
			Criterion criterion2 = Restrictions.ne("status",statusHide);
			Criterion criterion3 = Restrictions.isNull("cgUpdated");
			Criterion criterion4 = Restrictions.or(criterion1, criterion3);
			Criterion criterion5 = Restrictions.and(criterion4, criterion2);
			lstJobForTeacher = findByCriteria(criterion5);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForCG(List<TeacherDetail> lstTeacherDetails)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstTeacherDetails.size()>0){
				//StatusMaster statusHide = statusMasterDAO.findStatusByShortName("hide");
				StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
				//StatusMaster statusWithdraw = statusMasterDAO.findStatusByShortName("widrw");
				StatusMaster statusWithdraw= WorkThreadServlet.statusMap.get("widrw");
				Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
				Criterion criterion2 = Restrictions.ne("status",statusHide);
				Criterion criterion3 = Restrictions.ne("status",statusWithdraw);
				lstJobForTeacher = findByCriteria(criterion1,criterion2,criterion3 );
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByDSTeacherList(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:9");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
			if(lstJobOrder.size()>0){
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			if(lstTeacherDetails.size()>0){
				criteria.add(criterionTeacher);
			}
			//criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
			criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASA(Order sortOrderStrVal,int start,int end,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress)
	{
		PrintOnConsole.getJFTPrint("JFT:10");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailList= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criteria criteria = session.createCriteria(getPersistentClass());
			boolean flag=false;
			if(lstJobOrder.size()>0 && entityID==3){
				flag=true;
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null && entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			if(flag){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			if(lstTeacherDetail.size()<end)
				end=lstTeacherDetail.size();

			teacherDetailList=lstTeacherDetail.subList(start, end);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherDetailList;
	}

	@Transactional(readOnly=false)
	public int getTeacherDetailByDASARecords(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress)
	{
		PrintOnConsole.getJFTPrint("JFT:12");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		int totalRecords=0;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			boolean flag=false;
			if(lstJobOrder.size()>0 && entityID==3){
				flag=true;
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null && entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			criteria.createCriteria("status").add(Restrictions.ne("statusId", statusMaster.getStatusId()));
			if(flag){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			totalRecords=lstTeacherDetail.size();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return totalRecords;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASA(Order sortOrderStrVal,int start,int end,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> notTeacherDetails,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:13");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}

			criteria.add(Restrictions.ne("status",statusMaster));

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				flag=true;
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
			if(status){
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}else{
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			}
			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}

			// job applied flag
			if(appliedfDate!=null && appliedtDate!=null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			}else if(appliedfDate!=null && appliedtDate==null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			}else if(appliedtDate!=null && appliedfDate==null){
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}


			if(notTeacherDetails.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", notTeacherDetails)));	
			//new change fresh candidates
			flag=true;
			
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				criteria.createCriteria("teacherId")
				.add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				criteria.setFirstResult(start);
				criteria.setMaxResults(end);

				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return lstTeacherDetail;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecords(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> notTeacherDetails,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:15");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}

			criteria.add(Restrictions.ne("status",statusMaster));

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
			if(status){
				//criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}else{
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			}

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			//job applied flag
			if(appliedfDate!=null && appliedtDate!=null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			}else if(appliedfDate!=null && appliedtDate==null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			}else if(appliedtDate!=null && appliedfDate==null){
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}
			flag=true;
			if(notTeacherDetails.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", notTeacherDetails)));

			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsBeforeDate(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds)
	{
		PrintOnConsole.getJFTPrint("JFT:17");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			boolean flag=false;
			boolean dateDistrictFlag=false;
			
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				flag=true;
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
			if(status){
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
			}else{
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			}

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			} 
			//job applied flag
			
			flag=true;
			if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
				//criteria.add(Restrictions.or( Restrictions.le("createdDateTime",appliedfDate),Restrictions.ge("createdDateTime",appliedtDate)));
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			else if(daysVal==5 && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
			else if(appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));


			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByDSTeachersList(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
	{
		PrintOnConsole.getJFTPrint("JFT:18");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		if(lstTeacherDetails.size()>0){
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
				
				if(entityID!=1){
					Criteria jobOrderSet= criteria.createCriteria("jobId");
					if(districtMaster!=null){
						jobOrderSet.add(Restrictions.eq("districtMaster",districtMaster));
					}else{
						//jobOrderSet.add(Restrictions.isNull("districtMaster"));
					}
					if(branchMaster!=null){
						jobOrderSet.add(Restrictions.eq("branchMaster",branchMaster));
					}else{
						//jobOrderSet.add(Restrictions.isNull("branchMaster"));
					}
					if(headQuarterMaster!=null){
						if(headQuarterMaster.getHeadQuarterId()==1){
						   jobOrderSet.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
						}
					}else{
						//jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
					}
				}
			if(lstTeacherDetails.size()>0){
				criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
				criteria.add(criterionTeacher);
			}
				//criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForUpdateVltInCG(List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			//StatusMaster statusHide = statusMasterDAO.findStatusByShortName("hide");
			StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.eq("cgUpdated",false);			
			Criterion criterion2 = Restrictions.ne("status",statusHide);
			Criterion criterion3 = Restrictions.isNull("cgUpdated");
			Criterion criterion4 = Restrictions.or(criterion1, criterion3);
			Criterion criterion5 = Restrictions.and(criterion4, criterion2);
			Criterion criterion6= Restrictions.in("status",lstStatusMasters); 
			lstJobForTeacher = findByCriteria(criterion5,criterion6);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}


	@Transactional(readOnly=false)
	public List<JobForTeacher> findByJobOrderForCG(UserMaster userMaster,JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate,String firstName,String lastName,String emailAddress,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean all,boolean teachersOnly,List<Long> selectedTeachers)
	{
		PrintOnConsole.getJFTPrint("JFT:19");
		List<JobForTeacher> lstJobForTeacher=  new ArrayList<JobForTeacher>();
		try{
			Session session = getSession();
			List <StatusMaster> statusMasterLst=statusMasterDAO.findAllStatusMaster();
			Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
			for (StatusMaster sMaster : statusMasterLst) {
				mapStatus.put(sMaster.getStatusShortName(),sMaster);
			}

			Criterion criterionSchool =null;
            if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
                criterionSchool=Restrictions.eq("schoolMaster",userMaster.getSchoolId());
            }
			
			StatusMaster statusMaster= mapStatus.get("hide");
			StatusMaster internalStatus= mapStatus.get("icomp");
			Criterion criterion10 = Restrictions.ne("internalStatus",internalStatus);
			Criterion criterion14 = Restrictions.isNull("internalStatus");
			Criterion criterion15 = Restrictions.or(criterion10,criterion14);
			Criterion criterion11 = Restrictions.eq("internalStatus",internalStatus);
			Criterion criterion1 = Restrictions.in("status", lstStatusMasters);		
			Criterion criterion13 = Restrictions.or(criterion11,criterion1);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion3 = Restrictions.ne("status",statusMaster);
			Criterion criterion4 = Restrictions.eq("cgUpdated",true);
			Criterion criterion8 = Restrictions.isNull("cgUpdated");
			Criterion criterion5 = Restrictions.eq("cgUpdated",false);
			Criterion criterion6 = Restrictions.or(criterion5, criterion8);
			Criterion criterion9 = Restrictions.or(criterion6, criterion4);
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(teachersOnly)
			{
				System.out.println("teachersOnly :"+teachersOnly+" ====== selectedTeachers.size() :: "+selectedTeachers.size());
				if(selectedTeachers.size()>0)
				{
					criteria.add(Restrictions.in("jobForTeacherId", selectedTeachers));
				}
			}
			
			if(cgUpdate==0){
				criteria.add(criterion13);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion9);
			}else if(cgUpdate==1){
				criteria.add(criterion15);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				/*boolean epiInternal=false;
				try{
					if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
						epiInternal=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobOrder.getJobCategoryMaster()!=null){
					if(jobOrder.getJobCategoryMaster().getBaseStatus() && jobOrder.getJobCategoryMaster().getStatus().equalsIgnoreCase("A")){
						if(epiInternal==false){
							Criterion criterion7 = Restrictions.eq("isAffilated",1);
							Criterion criterion12 =Restrictions.or(criterion7, criterion4) ;
							criteria.add(criterion12);
						}else{
							criteria.add(criterion4);
						}
					}
				}*/
			}else if(cgUpdate==7){
				criteria.add(criterion15);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
			}else if(cgUpdate==2){
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
			}else if(cgUpdate==5){
				criteria.add(criterion15);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
			}else if(cgUpdate==3){
				criteria.add(criterion2);
				criteria.add(criterion3);
			}else{
				criteria.add(criterion2);
				criteria.add(criterion3);
			}
			
			 if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
	                criteria.add(criterionSchool);
	           }
			
			if(teacherFlag){
				criteria.add(Restrictions.in("teacherId", filterTeacherList));
			}
			criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
			.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
			.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplicantsByJobOrders(int status,List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:20");
		Session session = getSession();

		Criterion criterion = Restrictions.in("jobId", jobOrders);		
		Criteria c= session.createCriteria(getPersistentClass());

		if(status>0)
		{
			StatusMaster statusMaster = new StatusMaster();
			statusMaster.setStatusId(status);
			c.add(Restrictions.eq("status", statusMaster));
		}

		c.add(criterion).setProjection( Projections.projectionList()
				.add(Projections.groupProperty("jobId"))
				.add(Projections.count("jobId"))) ; 

		return c.list();

	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplicantsByJobOrdersHQL(int status,List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:21");
		Session session = getSession();

		String sql =" select jft.jobId,count(*) as appliedCnt,COUNT(IF(jft.status="+status+",1,NULL)) AS hiredCnt,MAX( lastActivityDate ),lastActivity,lastActivityDate,lastActivityDoneBy,jobForTeacherId  " +
		" ,COUNT(IF(teacherNormScore>If(jo.createdForEntity=3,sm.jobFeedCriticalNormScore,dm.jobFeedCriticalNormScore),1,NULL)) AS normCandidateCriticalCnt," +
		" COUNT(IF(teacherNormScore>If(jo.createdForEntity=3,sm.jobFeedAttentionNormScore,dm.jobFeedAttentionNormScore),1,NULL)) AS normCandidateAttentionCnt " +
		" from jobforteacher jft left join teachernormscore tns on jft.teacherId=tns.teacherId join joborder jo on jft.jobId=jo.jobId left join schoolinjoborder sij on jo.jobId=sij.jobId " +
		" join districtmaster dm on jo.districtId = dm.districtId " +
		" left join schoolmaster sm on sij.schoolId = sm.schoolId " +
		" where jft.jobId IN (:jobOrders) group by jft.jobId ";
		Query query = session.createSQLQuery(sql);

		query.setParameterList("jobOrders", jobOrders);

		List<Object[]> rows = query.list();
		return rows;

	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplicantsByJobOrdersSchoolHQL(int status,List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:22");
		Session session = getSession();

		String sql =" select jft.jobId,count(*) as appliedCnt,COUNT(IF(jft.status="+status+",1,NULL)) AS hiredCnt,MAX( lastActivityDate ),lastActivity,lastActivityDate,lastActivityDoneBy,jobForTeacherId  " +
		" ,COUNT( IF( teacherNormScore > sm.jobFeedCriticalNormScore, 1, NULL ) ) AS normCandidateCriticalCnt," +
		" COUNT( IF( teacherNormScore > sm.jobFeedAttentionNormScore, 1, NULL ) ) AS normCandidateAttentionCnt" +
		" ,sm.schoolId from jobforteacher jft left join teachernormscore tns on jft.teacherId=tns.teacherId join joborder jo on jft.jobId=jo.jobId left join schoolinjoborder sij on jo.jobId=sij.jobId " +
		" join districtmaster dm on jo.districtId = dm.districtId " +
		" join schoolmaster sm on sij.schoolId = sm.schoolId " +
		" where jft.jobId IN (:jobOrders) group by jft.jobId ";
		Query query = session.createSQLQuery(sql);

		query.setParameterList("jobOrders", jobOrders);

		List<Object[]> rows = query.list();
		return rows;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplicantsByJobOrdersBySchoolHQL(int status,JobOrder jobOrder,SchoolMaster schoolMaster) 
	{
		PrintOnConsole.getJFTPrint("JFT:23");
		Session session = getSession();

		String sql =" select jft.jobId,count(*) as appliedCnt,COUNT(IF(jft.status="+status+",1,NULL)) AS hiredCnt,MAX( lastActivityDate ),lastActivity,lastActivityDate,lastActivityDoneBy,jobForTeacherId  " +
		" ,COUNT( IF( teacherNormScore > sm.jobFeedCriticalNormScore, 1, NULL ) ) AS normCandidateCriticalCnt," +
		" COUNT( IF( teacherNormScore > sm.jobFeedAttentionNormScore, 1, NULL ) ) AS normCandidateAttentionCnt" +
		" ,sm.schoolId from jobforteacher jft left join teachernormscore tns on jft.teacherId=tns.teacherId join joborder jo on jft.jobId=jo.jobId left join schoolinjoborder sij on jo.jobId=sij.jobId " +
		" join districtmaster dm on jo.districtId = dm.districtId " +
		" join schoolmaster sm on sij.schoolId = sm.schoolId " +
		" where jft.jobId =:jobOrder and schoolMaster=:schoolMaster group by jft.jobId ";

		Query query = session.createSQLQuery(sql);

		query.setParameter("jobOrder", jobOrder);
		query.setParameter("schoolMaster", schoolMaster);

		List<Object[]> rows = query.list();
		return rows;
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,String> countApplicantsByJobOrdersHQL(List<JobOrder> jobOrders,String hrdstatus,String hidestatus) 
	{
		PrintOnConsole.getJFTPrint("JFT:24");
		Session session = getSession();
		String sql =" SELECT jft.jobId, COUNT(*) AS appliedCnt, COUNT( IF( jft.status ="+hrdstatus+", 1, NULL ) ) AS hiredCnt " +
		" from jobforteacher jft where jft.jobId IN (:jobOrders) and jft.status!="+hidestatus+" group by jft.jobId ";
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);

		List<Object[]> rows = query.list();
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], obj[1]+"##"+obj[2]);
			}
		}
		return map;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplidJobsHQL(DistrictMaster districtMaster,SchoolMaster schoolMaster,List<TeacherDetail> teacherDetails) 
	{
		PrintOnConsole.getJFTPrint("JFT:25");
		Session session = getSession();
		String sql = "";
		if(districtMaster!=null && schoolMaster==null)
		{
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT(IF(jo.districtId="+districtMaster.getDistrictId()+" and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}if(schoolMaster!=null)
		{
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT( IF( jo.jobId IN ( SELECT jobId FROM schoolinjoborder WHERE schoolId ="+schoolMaster.getSchoolId()+" ) and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}


		Query query = session.createSQLQuery(sql);
		String[] statuss = {"icomp","comp","hird","vlt","scomp","ecomp","vcomp","dcln","rem"};
		//List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);
		List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		query.setParameterList("teacherDetails", teacherDetails);
		query.setParameterList("statuss", statusMasters );

		List<Object[]> rows = query.list();
		return rows;

	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List findTotalCandidateCountByJobIds(String jobIds) 
	{
		PrintOnConsole.getJFTPrint("JFT:26");
		Session session = getSession();
		String sql = "";
		List<Object[]> rows=null;
	
		
		if(jobIds!=""){
			sql = "select jft.jobId,COUNT(*) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where jft.jobId IN  ( "+jobIds +")  and jft.status != 8  group by jobId";
		
		if(!sql.equals(""))
		{
			Query query = session.createSQLQuery(sql);
			rows = query.list();
		}
		}
		return rows;

	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List findAvlCandidateCountByJobIds(String jobIds) 
	{
		PrintOnConsole.getJFTPrint("JFT:27");
		Session session = getSession();
		String sql = "";
		List<Object[]> rows=null;
	
		
		if(jobIds!=""){
			sql = "select jft.jobId,COUNT(*) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where jft.jobId IN  ( "+jobIds +") and jft.status NOT IN(:statuss) group by jobId";
		
			if(!sql.equals(""))
			{
				Query query = session.createSQLQuery(sql);
				String[] statuss = {"icomp","vlt","widrw","hide"};
				//List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);
				List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
				
				query.setParameterList("statuss", statusMasters );
				rows = query.list();
				
			}
		}
		
		return rows;

	}
	
	
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List getLastActivityOnJobOrders(List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:28");
		Session session = getSession();

		Criterion criterion = Restrictions.in("jobId", jobOrders);		
		Criteria c= session.createCriteria(getPersistentClass());

		c.add(criterion).setProjection( Projections.projectionList()
				.add(Projections.max("lastActivityDate"))
				.add(Projections.property("jobForTeacherId"))
				.add(Projections.property("lastActivity"))
				.add(Projections.property("lastActivityDate"))
				//.add(Projections.property("lastActivityDoneBy"))
				.add(Projections.groupProperty("jobId"))) ; 

		return c.list();

	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusList(TeacherDetail teacherDetail, List<StatusMaster> statusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(statusMasters.size()>0)
			{
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.in("status",statusMasters);
				Criterion criterion3 = Restrictions.and(criterion1,criterion2);

				lstJobForTeacher = findByCriteria(Order.desc("createdDateTime"),criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersBySubject(SubjectMaster subjectMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:30");
		try 
		{
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass()); 
			Criteria incriteria = criteria.createCriteria("jobId");
			incriteria.add(Restrictions.eq("subjectMaster",subjectMaster));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherId"))
			);

			return criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersBySubjectList(List<SubjectMaster> subjectMasters)
	{
		PrintOnConsole.getJFTPrint("JFT:31");
		try 
		{
			Session session = getSession();
			List<TeacherDetail> results = new ArrayList<TeacherDetail>();
			if(subjectMasters.size()>0)
			{
				Criteria criteria= session.createCriteria(getPersistentClass()); 
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				criteria.add(Restrictions.ne("status",statusMaster));
				
				Criteria incriteria = criteria.createCriteria("jobId");
				incriteria.add(Restrictions.in("subjectMaster",subjectMasters));
				criteria.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherId"))
				);
				results = criteria.list();
			}
			return results;
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherIDs(Long[] teacherIds)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("jobForTeacherId",teacherIds);
			lstJobForTeacher = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findInternalCandidate(List<TeacherDetail> lstteacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstteacherDetail.size()>0)
			{
				Criterion criterion_tId = Restrictions.in("teacherId",lstteacherDetail);
				Criterion criterion_IsAffi = Restrictions.eq("isAffilated", 1); 
				lstJobForTeacher = findByCriteria(criterion_tId,criterion_IsAffi);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			return lstJobForTeacher;
		}
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findDistrictOrSchoolAppliedJobByTeacher(TeacherDetail teacherDetail,DistrictMaster districtMaster,List<StatusMaster> lstStatusMasters)
	{
		PrintOnConsole.getJFTPrint("JFT:32");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.in("status", lstStatusMasters);

			criteria.add(criterion1);
			if(lstStatusMasters.size()>0)
				criteria.add(criterion2);
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			//lstJobForTeacher = findByCriteria(criterion3);	

			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> findDistrictOrSchoolAppliedJobByTeacherAll(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,List<StatusMaster> lstStatusMasters)
	{
		PrintOnConsole.getJFTPrint("JFT:33");
		List<JobOrder> lstJobOrder= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.in("status", lstStatusMasters);

			criteria.add(criterion1);
			if(lstStatusMasters.size()>0)
				criteria.add(criterion2);
			if(headQuarterMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			else if(branchMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			else if(districtMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			criteria.setProjection((Projections.property("jobId"))) ; 
			lstJobOrder = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrder;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findUniqueApplicantsbyJobOrders(List<JobOrder> lstJobOrder)
	{
		List<TeacherDetail> lstJobForTeacher= null;
		try 
		{

			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			//criteria.setResultTransformer(Criteria.PROJECTION);
			criteria.setProjection(Projections.distinct(Projections.property("teacherId")));
			lstJobForTeacher = criteria 
			.add(Restrictions.in("jobId",lstJobOrder)) 
			.list();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTbyJobOrders(List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			lstJobForTeacher = criteria 
			.add(Restrictions.in("jobId",lstJobOrder)) 
			.addOrder(Order.asc("jobId"))
			.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public JobForTeacher getJobForTeacherDetails(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		JobForTeacher jobForTeacher=null;
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher.size()==1)
			jobForTeacher=lstJobForTeacher.get(0);
		return jobForTeacher;
	}
	@Transactional(readOnly=false)	
	public List<JobForTeacher> findHireBySchool(JobOrder jobOrder,SchoolMaster schoolMaster) 
	{	

		//StatusMaster statusMaster = statusMasterDAO.findStatusByShortName("hird");
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();
		try{
			Criterion criterion1 = Restrictions.eq("jobId", jobOrder);	
			Criterion criterion2 = Restrictions.eq("status", statusMaster);	
			Criterion criterion3 = Restrictions.eq("schoolMaster",schoolMaster);
			if(criterion3!=null)
				lstJobForTeachers = findByCriteria(criterion1,criterion2,criterion3);
			else
				lstJobForTeachers = findByCriteria(criterion1,criterion2);

		}catch(Exception e){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}
	/* Sekhar : Get getJobForTeacherInternalList from getJobForTeacherInternalList ========== */
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherInternalList(List jobforteacherlistteacherIdList)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("jobForTeacherId",jobforteacherlistteacherIdList);
			Criterion criterion2= Restrictions.eq("isAffilated",new Integer(1));
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherExternalList(List jobforteacherlistteacherIdList)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("jobForTeacherId",jobforteacherlistteacherIdList);
			Criterion criterion2= Restrictions.eq("isAffilated",new Integer(0));
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobAppliedByTeacherAndDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster,Boolean isFinalize)
	{
		PrintOnConsole.getJFTPrint("JFT:35");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);

			if(isFinalize!=null)
			{
				Criterion criterion2 = Restrictions.eq("isDistrictSpecificNoteFinalize",isFinalize);
				criteria.add(criterion2);
			}

			criteria.add(criterion1);

			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));

			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* @Start
	 * @Ashish Kumar
	 * @Description :: Find JobForTeacher Data By Teacher ID And District Master 
	 * */

	@Transactional(readOnly=false)	
	public List<JobForTeacher> findByTeacherIdDistrictId(TeacherDetail teacherDetail,DistrictMaster districtMaster) 
	{	
		PrintOnConsole.getJFTPrint("JFT:36");
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}

			criteria.addOrder(Order.desc("createdDateTime"));
			lstJobForTeachers = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}

	/* @End
	 * @Ashish Kumar
	 * @Description :: Find JobForTeacher Data By Teacher ID And District Master 
	 * */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobsForJobSpecificInventoryAll(TeacherDetail teacherDetail,Boolean assesmentStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:37");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			String[] statuss = {"hide","widrw"};
			Criterion criterion2 = Restrictions.not(Restrictions.in("status", Utility.getStaticMasters(statuss)));
			criteria.add(criterion2);
			criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime))
			.add(Restrictions.eq("status", "A"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByJIDsAndTID_msu(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.in("jobId",lstJobOrder);

			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacersByDistrict(DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:38");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			criteria.addOrder(Order.asc("teacherId"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findWeeklyJobsByDistrict(DistrictMaster districtMaster2)
	{
		List<JobForTeacher> jobForTeacherList=new ArrayList<JobForTeacher>();
		try 
		{
			List<JobOrder> jobOrderList=jobOrderDAO.findByCriteria(Restrictions.eq("districtMaster", districtMaster2));
			
     		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			String s1=dateFormat.format(cal.getTime());
			Date d1 = dateFormat.parse(s1);
			System.out.println(":: "+d1);
			cal.add(Calendar.DATE, -7); 
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			System.out.println(":: "+d2);
		
			Criterion  criterion1=Restrictions.between("createdDateTime",d2,d1);
			Criterion criterion2=Restrictions.in("jobId", jobOrderList);
			
			jobForTeacherList = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobForTeacherList;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findAllJobForTeacherByDistrictId(List<Long> jftIds,List<StatusMaster> lstStatusMasters,DistrictMaster districtMaster,TeacherDetail teacherDetail,boolean ssnFlag,String teacherFNames,String teacherLNames,String position,Order order,int startPos,int limit)
	{
		PrintOnConsole.getJFTPrint("JFT:39");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			
			if(jftIds.size()>0){
				criteria.add(Restrictions.not(Restrictions.in("jobForTeacherId",jftIds)));
			}
			if(!(districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId().equals(2)))
			criteria.add(Restrictions.isNotNull("requisitionNumber"));
			criteria.add(Restrictions.in("status",lstStatusMasters));
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			
			if(teacherDetail!=null || ssnFlag){
				criteria.add(Restrictions.eq("teacherId",teacherDetail));
			}
			if((teacherFNames!= null && !teacherFNames.equals(""))&&(teacherLNames!= null && !teacherLNames.equals(""))){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName","%"+teacherFNames+"%")).add(Restrictions.like("lastName","%"+teacherLNames+"%"));
			}else if(teacherFNames!= null && !teacherFNames.equals("")){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName","%"+teacherFNames+"%"));
			}else if(teacherLNames!= null && !teacherLNames.equals("")){
				criteria.createCriteria("teacherId").add(Restrictions.like("lastName","%"+teacherLNames+"%"));
			}	
			if(position!= null && !position.equals("")){
				criteria.add(Restrictions.like("requisitionNumber","%"+position+"%"));
			}	
			
			criteria.addOrder(order);
			criteria.addOrder(Order.asc("createdDateTime"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherActiveJob(TeacherDetail teacherDetail)
	{
		PrintOnConsole.getJFTPrint("JFT:40");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
		// Sandeep All Job Show on DashBoard 31-08-15	
			//criteria.createCriteria("jobId").add(Restrictions.eq("status","A"));

			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getOfferReadyDate(List<JobOrder> jobOrders)
	{
		List<JobForTeacher> jobForTeachers = new ArrayList<JobForTeacher>();
		try{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -2); 
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			System.out.println(":: "+d2);
			Session session = getSession();
			Criterion criterion1 = Restrictions.isNull("noResponseEmail");
			Criterion criterion2 = Restrictions.eq("noResponseEmail",false);
			
			
				Criterion criterion3 = Restrictions.in("jobId",jobOrders);
			
			List result = session.createCriteria(getPersistentClass()) 
			.add(Restrictions.eq("offerReady",true)) 
			.add(Restrictions.or(criterion1,criterion2)) 
			.add(Restrictions.isNull("offerAccepted")) 
			.add(Restrictions.le("offerMadeDate",d2))
			.add(criterion3)
			.list();
			jobForTeachers = result;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jobForTeachers;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getTeacherIdsAndJobIds(List<TeacherDetail> lstTeacherDetail, List<JobOrder> jobOrders)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetail);
			Criterion criterion2 = Restrictions.in("jobId",jobOrders);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);
			lstJobForTeacher = findByCriteria(criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndJobCategory(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,String jobForTeacherId)
	{
		PrintOnConsole.getJFTPrint("JFT:41");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster)).add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	

	@Transactional(readOnly=false)
	public List<JobForTeacher> findbyJobOrder(JobOrder jobOrder)
	{
		List<JobForTeacher> JobForTeacherApplicant= null;
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);		
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			JobForTeacherApplicant = findByCriteria(Order.asc("createdDateTime"),criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return JobForTeacherApplicant;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacherByTeacherDeatail(List<TeacherDetail> lstTeacherDetails)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			//StatusMaster statusHide = statusMasterDAO.findStatusByShortName("hide");
			//StatusMaster statusWithdraw = statusMasterDAO.findStatusByShortName("widrw");
			Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
			//?/Criterion criterion2 = Restrictions.ne("status",statusHide);
			//Criterion criterion3 = Restrictions.ne("status",statusWithdraw);
			lstJobForTeacher = findByCriteria(criterion1 );		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster,String jobForTeacherId)
	{
		PrintOnConsole.getJFTPrint("JFT:42");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacersByDistrictAndSchool(DistrictMaster districtMaster,SchoolMaster schoolId)
	{
		PrintOnConsole.getJFTPrint("JFT:43");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			criteria.addOrder(Order.asc("teacherId"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByJOB(List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
		//	Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.in("jobId",lstJobOrder);
            
			lstJobForTeacher = findByCriteria(criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacersByDistrictStatus(DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:44");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			criteria.createCriteria("jobId").add(Restrictions.eq("status", "A"));
			criteria.addOrder(Order.asc("teacherId"));
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByJOBStatus(List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
		//	Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",lstJobOrder);
            
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacherByTeacherDeatailAndJobStatus(List<TeacherDetail> lstTeacherDetails)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion2 = Restrictions.ne("status",statusMaster);
			lstJobForTeacher = findByCriteria(criterion1,criterion2 );		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findbyJobOrderAndStatus(List<Integer> jobIds)
	{
		List<JobForTeacher> JobForTeacherApplicant= new ArrayList<JobForTeacher>();
		try 
		{
			if(jobIds.size()>0){
				List<JobOrder> listJobOrders = jobOrderDAO.findByJobIDS(jobIds);
				 if(listJobOrders.size()>0){
					StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
					Criterion criterion1 = Restrictions.ne("status",statusMaster);		
					Criterion criterion2 = Restrictions.in("jobId",listJobOrders);
					JobForTeacherApplicant = findByCriteria(criterion1,criterion2);	
				 }
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return JobForTeacherApplicant;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findBySecondStatus(List<SecondaryStatus> lstseSecondaryStatus)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstseSecondaryStatus.size()>0){
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.ne("status",statusMaster);
				Criterion criterion2 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion3 = Restrictions.isNull("statusMaster");
				lstJobForTeacher = findByCriteria(criterion1,criterion2,criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findByStatus(StatusMaster statusMaster1)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			List<StatusMaster> statusList =new ArrayList<StatusMaster>();
			
			statusList.add(statusMaster1);
		
			//StatusMaster statusHide= statusMasterDAO.findStatusByShortName("hide");
			StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusHide);
			Criterion criterion2 = Restrictions.eq("statusMaster",statusMaster1);
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findAvailableCandidte(DistrictMaster  districtMaster,StatusMaster statusMaster2)
	{
		PrintOnConsole.getJFTPrint("JFT:45");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("status",statusMaster2);
			Criterion criterion2 = Restrictions.eq("statusMaster",statusMaster2);
			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();
			
					
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndJobTitle(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,String jobForTeacherId,String jobTitle)
	{
		PrintOnConsole.getJFTPrint("JFT:46");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster)).add(Restrictions.eq("districtMaster",districtMaster)).add(Restrictions.like("jobTitle","%"+jobTitle+"%"));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTIDsAndJID(List<TeacherDetail> lstTeacherDetail, JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstTeacherDetail.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetail);
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.and(criterion1,criterion2);
				lstJobForTeacher = findByCriteria(criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> OfferCandidateList(List<TeacherDetail> lstTeacherDetails)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstTeacherDetails.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
				Criterion criterion2 = Restrictions.eq("offerReady",true);
				Criterion criterion3 = Restrictions.eq("offerReady",false);
				Criterion criterion4 = Restrictions.or(criterion3,criterion2);
				lstJobForTeacher = findByCriteria(criterion1,criterion4);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTIDsAndSID_CGMass(List<TeacherDetail> lstTeacherDetails,List<StatusMaster> lstStatusMasters,JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstStatusMasters!=null && lstStatusMasters.size() >0 && lstTeacherDetails!=null && lstTeacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
				Criterion criterion2 = Restrictions.in("status", lstStatusMasters);
				Criterion criterion3 = Restrictions.eq("jobId",jobOrder);
				lstJobForTeacher=findByCriteria(criterion1,criterion2,criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForTeacherbyInputDateFilterwithDistrict(DistrictMaster districtMaster ,String sfromDate, String stoDate,String endfromDate, String endtoDate, String appsfromDate, String appstoDate,List<TeacherDetail> lstTeacherDetails, String normScoreSelectVal, int intenalchk)
	{
		PrintOnConsole.getJFTPrint("JFT:47");
		List<JobForTeacher> lstjft1 = new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionhide = Restrictions.ne("status",statusMaster);
			
			Criterion internalCriterion1=null;
			Criterion internalCriterion2=null;
			Criterion internalCriterion=null;
			
			if(intenalchk==1)
			 internalCriterion = Restrictions.eq("isAffilated",intenalchk);
			else if(intenalchk==0){
				internalCriterion1 = Restrictions.eq("isAffilated",intenalchk);
				internalCriterion2 = Restrictions.eq("isAffilated",null);
				internalCriterion = Restrictions.or(internalCriterion1, internalCriterion2);
			}
			if(internalCriterion!=null)
			criteria.add(internalCriterion);
			
			//System.out.println("111111111");
			Date fDate=null;
			Date tDate=null;
			Date endfDate=null;
			Date endtDate =null;
			Date appliedfDate=null;
			Date appliedtDate=null;
			
			
			if(!sfromDate.equals(""))
			 fDate = Utility.getCurrentDateFormart(sfromDate);
			if(!stoDate.equals(""))
			 tDate = Utility.getCurrentDateFormart(stoDate);
			if(!endfromDate.equals(""))
			 endfDate = Utility.getCurrentDateFormart(endfromDate);
			if(!endtoDate.equals(""))
			 endtDate= Utility.getCurrentDateFormart(endtoDate);
       //String appsfromDate, String appstoDate
			if(!appsfromDate.equals(""))
				appliedfDate= Utility.getCurrentDateFormart(appsfromDate);
			if(!appstoDate.equals(""))
				appliedtDate= Utility.getCurrentDateFormart(appstoDate);
			
			 if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
			 {
				 if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
			       criteria.add(Restrictions.in("teacherId", lstTeacherDetails));
				 else
				  return lstjft1;
			 }
			 if(normScoreSelectVal.equals("6")){
				 criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetails)));
			 }
			criteria.add(criterionhide);
			Criteria c2 = criteria.createCriteria("jobId");
					
				if(districtMaster!=null)
					c2.add(Restrictions.eq("districtMaster", districtMaster));
				
				if(fDate!=null && tDate!=null){
					c2.add(Restrictions.ge("jobStartDate",fDate)).add(Restrictions.le("jobStartDate",tDate));
				}else if(fDate!=null && tDate==null){
					c2.add(Restrictions.ge("jobStartDate",fDate));
				}else if(fDate==null && tDate!=null){
					c2.add(Restrictions.le("jobStartDate",tDate));
				}
					
				if(endfDate!=null && endtDate!=null){
					c2.add(Restrictions.ge("jobEndDate",endfDate)).add(Restrictions.le("jobEndDate",endtDate));
				}else if(endfDate!=null && endtDate==null){
					c2.add(Restrictions.ge("jobEndDate",endfDate));
				}else if(endfDate==null && endtDate!=null){
					c2.add(Restrictions.le("jobEndDate",endtDate));
				}
				
				if(appliedfDate!=null && appliedtDate!=null){
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				}else if(appliedfDate!=null && appliedtDate==null){
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				}else if(appliedtDate!=null && appliedfDate==null){
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
			  
			   lstjft1= criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstjft1;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherListPC(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster,List<StatusMaster> listStatusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:48");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
					//criteria.createCriteria("status").add(Restrictions.ne("statusShortName","hird"));
				if(listStatusMaster!=null)
					criteria.add(Restrictions.not(Restrictions.in("status",listStatusMaster)));
				
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherListOC(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster,List<StatusMaster> listStatusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:49");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null)
				{
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					criteria.createCriteria("jobId").add(Restrictions.ne("jobType", "F"));
				}
				
					//criteria.createCriteria("status").add(Restrictions.ne("statusShortName","hird"));
				if(listStatusMaster!=null)
					criteria.add(Restrictions.not(Restrictions.in("status",listStatusMaster)));
					
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getQQFinalizedTeacherList(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:50");
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				
				criteria.setProjection(Projections.groupProperty("teacherId"));
					
				lstTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByJobCategory(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		PrintOnConsole.getJFTPrint("JFT:51");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster())).add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherListExIn(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:52");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findAvailableCandidtes(DistrictMaster  districtMaster,List<StatusMaster> statusMasterList,List<SecondaryStatus> lstseSecondaryStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:53");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    //StatusMaster statusMaster3= statusMasterDAO.findStatusByShortName("hide");
		    StatusMaster statusMaster3= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionHide = Restrictions.ne("status",statusMaster3);
			Criterion criterionAll=null;
			if( statusMasterList.size()>0 &&  lstseSecondaryStatus.size()>0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				Criterion criterionStatus = Restrictions.and(criterion1, criterion2);
			
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				Criterion criterionSecStatus = Restrictions.and(criterion4, criterion5);
				
				criterionAll = Restrictions.or(criterionStatus, criterionSecStatus);
			}
			if(statusMasterList.size()>0 &&  lstseSecondaryStatus.size()==0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				criterionAll = Restrictions.and(criterion1, criterion2);
			}
			
			if(statusMasterList.size()==0 &&  lstseSecondaryStatus.size()>0)
			{
				
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				criterionAll = Restrictions.and(criterion4, criterion5);
				
			}
			
			criteria.add(criterionAll);
			criteria.add(criterionHide);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findAvailableCandidtesForReport(DistrictMaster  districtMaster,List<StatusMaster> statusMasterList,List<SecondaryStatus> lstseSecondaryStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:54");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    //StatusMaster statusMaster3= statusMasterDAO.findStatusByShortName("hide");
		    StatusMaster statusMaster3= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionHide = Restrictions.ne("status",statusMaster3);
			Criterion criterionAll=null;
			if( statusMasterList.size()>0 &&  lstseSecondaryStatus.size()>0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				Criterion criterionStatus = Restrictions.and(criterion1, criterion2);
			
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				Criterion criterionSecStatus = Restrictions.and(criterion4, criterion5);
				
				criterionAll = Restrictions.or(criterionStatus, criterionSecStatus);
			}
			if(statusMasterList.size()>0 &&  lstseSecondaryStatus.size()==0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				criterionAll = Restrictions.and(criterion1, criterion2);
			}
			
			if(statusMasterList.size()==0 &&  lstseSecondaryStatus.size()>0)
			{
				
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				criterionAll = Restrictions.and(criterion4, criterion5);
				
			}
			
			criteria.add(criterionAll);
			criteria.add(criterionHide);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	/*****************/
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getHiredRequisitionNumbers(List<String> requisitionNumbers,List<JobOrder> jobOrder)
	{
		List<JobForTeacher> lstHiredRequisitionNumbers= null;
		
		try 
		{
			StatusMaster status = statusMasterDAO.findById(6, false, false);
			Criterion criterion1 = Restrictions.in("requisitionNumber",requisitionNumbers);			
			//Criterion criterion2 = Restrictions.eq("statusMaster",requisitionNumbers);
			//Criterion criterion2 = Restrictions.eq("status",status);
			Criterion criterion2 = Restrictions.in("jobId",jobOrder);
			
			lstHiredRequisitionNumbers = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstHiredRequisitionNumbers;
	}
	
	
	/*
	 * for Offer Ready
	 * */
		@Transactional(readOnly=false)
		public List<JobForTeacher> findJFTforOfferReadystatus(DistrictMaster districtMaster ,Order sortOrderStrVal,List<JobOrder> jobOrders, boolean sortingcheck)
		{
			PrintOnConsole.getJFTPrint("JFT:55");
			List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				//criteria.addOrder(Order.asc("teacherId"));
				
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.ne("status",statusMaster);
				criteria.add(criterion1);
				
				Criterion isnotNullCriterion    =Restrictions.isNotNull("offerReady");
				criteria.add(isnotNullCriterion);
				
				if(!sortingcheck){
				   //System.out.println("sortingchecksortingchecksortingchecksortingchecksortingcheck true");
				 criteria.addOrder(sortOrderStrVal);
				}
				
				if(jobOrders!=null && jobOrders.size()>0)
					criteria.add(Restrictions.in("jobId",jobOrders));
				
				lstJobForTeacher = criteria.list();
				System.out.println("offerReadyofferReady :: "+lstJobForTeacher.size());
				
				
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}
		
		@Transactional(readOnly=false)
		public List<JobForTeacher> getRequisitionNumbers(JobOrder jobOrder,String requisitionNumbers,TeacherDetail teacherDetail)
		{
			List<JobForTeacher> lstHiredRequisitionNumbers= new ArrayList<JobForTeacher>();
			try 
			{
				Criterion criterion1 = Restrictions.eq("requisitionNumber",requisitionNumbers);	
				if(jobOrder!=null && teacherDetail!=null){
					Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
					Criterion criterion3 = Restrictions.eq("teacherId",teacherDetail);
					if(requisitionNumbers!=null){
						lstHiredRequisitionNumbers = findByCriteria(criterion1,criterion2,criterion3);
					}else{
						lstHiredRequisitionNumbers = findByCriteria(criterion2,criterion3);
					}
				}else if(jobOrder!=null){
					Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
					if(requisitionNumbers!=null){
						lstHiredRequisitionNumbers = findByCriteria(criterion1,criterion2);
					}else{
						lstHiredRequisitionNumbers = findByCriteria(criterion2);
					}
				}else if(teacherDetail!=null){
					Criterion criterion3 = Restrictions.eq("teacherId",teacherDetail);
					if(requisitionNumbers!=null){
						lstHiredRequisitionNumbers = findByCriteria(criterion1,criterion3);
					}else{
						lstHiredRequisitionNumbers = findByCriteria(criterion3);
					}
				}else{
					lstHiredRequisitionNumbers = findByCriteria(criterion1);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstHiredRequisitionNumbers;
		}
		
		@Transactional(readOnly=false)
		public List<JobForTeacher> findJobByTeacherForOffers(List<TeacherDetail> teacherDetails)
		{
			List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
			try 
			{
				if(teacherDetails!=null && teacherDetails.size() >0)
				{
					//StatusMaster statusMaster= statusMasterDAO.findStatusByShortName("hird");
					StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
					Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
					Criterion criterion2 = Restrictions.not(Restrictions.isNull("requisitionNumber"));	
					Criterion criterion3 = Restrictions.not(Restrictions.eq("requisitionNumber",""));
					Criterion criterion4 = Restrictions.or(criterion3,criterion2);
					Criterion criterion5 = Restrictions.eq("status",statusMaster);
					lstJobForTeacher = findByCriteria(criterion1,criterion4,criterion5);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}
		
		/*Applicant status count */
		@Transactional(readOnly=false)
		public List fintApplicntByStatus(DistrictMaster districtMaster,String jobStatus,int sortingchk, Order sortOrderStrVal,List<JobOrder> lstJobOrders)
		{
			PrintOnConsole.getJFTPrint("JFT:57");
			List resultList =new ArrayList();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());				
				Criteria c1= criteria.createCriteria("jobId");
				         //c1.add(Restrictions.eq("districtMaster", districtMaster));				       
				         if(sortingchk==1)
				         c1.addOrder(sortOrderStrVal);
				
				criteria.add(Restrictions.eq("districtId",districtMaster.getDistrictId()));
				         
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion = Restrictions.ne("status",statusMaster);
				criteria.add(criterion);
				
				if(lstJobOrders!=null && lstJobOrders.size()>0)// 
				{
					criteria.add(Restrictions.in("jobId",lstJobOrders));
				}
				
				if(!jobStatus.equalsIgnoreCase("All")){
					jobStatus = jobStatus.trim();
					c1.add(Restrictions.eq("status", jobStatus));
				}
					
				criteria.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("jobId"))
						.add(Projections.groupProperty("secondaryStatus"))
						.add(Projections.groupProperty("statusMaster"))
						.add(Projections.count("jobId"))
						.add(Projections.count("secondaryStatus"))
						.add(Projections.count("statusMaster")));
				if(lstJobOrders!=null && lstJobOrders.size()>0)						
				resultList = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return resultList;
		}
	
		
	/********Hired Candidate with and without district ********swadesh*/	
		@Transactional(readOnly=false)
		public List<JobForTeacher> hiredCandidateListWithAndWithoutDistrict(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal,Date hiredStartdate,Date hiredEndDtae)
		{
			PrintOnConsole.getJFTPrint("JFT:58");
		
			List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
				Criterion hiredcriterion = Restrictions.eq("status", statusMaster);
				//***********SWADESH*****************/
				if(hiredStartdate!=null && hiredEndDtae!=null)
					criteria.add(Restrictions.ge("hiredByDate",hiredStartdate)).add(Restrictions.le("hiredByDate",hiredEndDtae));
				else if(hiredStartdate!=null && hiredEndDtae==null)
					criteria.add(Restrictions.ge("hiredByDate",hiredStartdate));
				else if(hiredEndDtae!=null && hiredStartdate==null)
					criteria.add(Restrictions.le("hiredByDate",hiredEndDtae));
				//////////******************************************/
				criteria.add(hiredcriterion);
				Criteria c1 = null;
				Criteria c2 = null;
				c1 = criteria.createCriteria("jobId");
				c2 = criteria.createCriteria("teacherId");
				if(districtMaster!=null)
				 c1.add(Restrictions.eq("districtMaster",districtMaster));
				
				if(sortingcheck==1){
					if(c2!=null)
					c2.addOrder(sortOrderStrVal);
				}
				else if(sortingcheck==2){
					if(c1!=null)
					c1.addOrder(sortOrderStrVal);
				}else if(sortingcheck==3){
					criteria.addOrder(sortOrderStrVal);
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			return lstJobForTeacher;
		
		}
		
	//find total/available/hired candidates in a job	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,String> countApplicantsByJobOrdersAllll(List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:59");
		Session session = getSession();
		String sql =" SELECT jft.jobId, COUNT(*) AS appliedCnt, COUNT( IF( jft.status NOT IN (:statuss), 1, NULL )) AS avlCnt, COUNT( IF( jft.status =6, 1, NULL ) ) AS hiredCnt " +
		" from jobforteacher jft where jft.jobId IN (:jobOrders) and jft.status!=8 group by jft.jobId ";
		
		String[] statuss = {"dcln","icomp","vlt","widrw","hide","rem"};
		//List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);
		List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		query.setParameterList("statuss", statusMasters );
		
		System.out.println("query   "+query);
		List<Object[]> rows = query.list();
		
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], obj[1]+"##"+obj[2]+"##"+obj[3]);
			}
		}
		return map;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherIDExIn(TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:60");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(teacherDetail!=null && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.eq("teacherId",teacherDetail));
				criteria.add(Restrictions.ne("isAffilated",1));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}

/*Applicant status count by jobs*/
	@Transactional(readOnly=false)
	public List fintApplicntsStatusByJob(List<JobOrder> lstJobOrders)
	{
		PrintOnConsole.getJFTPrint("JFT:61");
		List resultList =new ArrayList();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionhide = Restrictions.ne("status",statusMaster);
			criteria.add(criterionhide);
			
			if(lstJobOrders!=null && lstJobOrders.size()>0)
			{
				criteria.add(Restrictions.in("jobId",lstJobOrders));
				criteria.addOrder(Order.asc("jobId"));
			}
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("jobId"))
					.add(Projections.groupProperty("secondaryStatus"))
					.add(Projections.groupProperty("statusMaster"))
					.add(Projections.count("secondaryStatus"))
					.add(Projections.count("statusMaster"))
			);
			resultList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return resultList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List<Integer> findJobOrderByCandidtesJobStatus(DistrictMaster  districtMaster,List<StatusMaster> statusMasterList,List<SecondaryStatus> lstseSecondaryStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:62");
		List<Integer> lstjoborders = new ArrayList<Integer>(); 
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionAll=null;
			if( statusMasterList.size()>0 &&  lstseSecondaryStatus.size()>0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				Criterion criterionStatus = Restrictions.and(criterion1, criterion2);
			
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				Criterion criterionSecStatus = Restrictions.and(criterion4, criterion5);
				criterionAll = Restrictions.or(criterionStatus, criterionSecStatus);
			}
			if(statusMasterList.size()>0 &&  lstseSecondaryStatus.size()==0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				criterionAll = Restrictions.and(criterion1, criterion2);
			}
			
			if(statusMasterList.size()==0 &&  lstseSecondaryStatus.size()>0)
			{
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				criterionAll = Restrictions.and(criterion4, criterion5);
				
			}
			criteria.add(criterionAll);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("jobForTeacherId")));
			lstjoborders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstjoborders;
	}
	
	//for Average EPI/Norm Score by Job  
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countCandidatesNormHQLJobListWise(List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:63");
		Session session = getSession();
		String sql = "";
		/*sql = "SELECT jobId, sum(tns.teacherNormScore), count(jft.teacherId) as cnt  FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId " +
		        " WHERE  `jobId` IN (:jobOrders) and jft.status!=8  GROUP BY `jobId`";
		*/
		sql = "SELECT jobId, sum(tns.teacherNormScore), count(jft.teacherId) as cnt,displaySecondaryStatusId,displayStatusId  FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId " +
        " WHERE  `jobId` IN (:jobOrders) and jft.status!=8   GROUP BY `jobId`,displaySecondaryStatusId,displayStatusId";

		try {
			Query query = session.createSQLQuery(sql);
			query.setParameterList("jobOrders", jobOrders );
			List<Object[]> rows = query.list();
			return rows;
		  } catch (HibernateException e) {
			e.printStackTrace();
		  }
		return null;
	}
	
	//for Average EPI/Norm Score by Job  
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List avgFilterNormHQL(String normScoreVal,String normScoreSelectVal, DistrictMaster districtMaster,List<Integer> jftList, boolean jstSSatusFlag) 
	{
		PrintOnConsole.getJFTPrint("JFT:64");
		Session session = getSession();
		String sql = "";
		sql = "SELECT jo.jobId, avg(tns.teacherNormScore) as avgNorm  FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId  join joborder jo on jft.jobId=jo.jobId " +
		        " WHERE  jft.status!=8   ";
		
		if(districtMaster!=null){
			sql=sql+" and	jo.districtId=:districtMaster ";
		}
		if(jstSSatusFlag)
			sql=sql+" and jft.jobForTeacherId  IN (:jftList) ";
		
		sql = sql+ " GROUP BY jo.`jobId` ,displaySecondaryStatusId,displayStatusId HAVING "; 
		
		if(normScoreSelectVal.equals("1")){
			sql=sql+" avgNorm=:normScoreVal ";
		}else if(normScoreSelectVal.equals("2")){
			sql=sql+" avgNorm<:normScoreVal ";
		}else if(normScoreSelectVal.equals("3")){
			sql=sql+" avgNorm<=:normScoreVal ";
		}else if(normScoreSelectVal.equals("4")){
			sql=sql+" avgNorm>:normScoreVal ";
		}else if(normScoreSelectVal.equals("5")){
			sql=sql+" avgNorm>=:normScoreVal ";
		}
		
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameter("normScoreVal", normScoreVal);
			if(jstSSatusFlag)
			   query.setParameterList("jftList", jftList);
			if(districtMaster!=null){
				query.setParameter("districtMaster", districtMaster);
			}
			List<Object[]> rows = query.list();
			return rows;
		  } catch (HibernateException e) {
			e.printStackTrace();
		  }
		return null;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findMostRecentJobByTeacher(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);

			criteria.add(criterion1);
			criteria.addOrder(Order.desc("createdDateTime"));
			criteria.setMaxResults(1);
			lstJobForTeacher = criteria.list();	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getHiredListByJFTs(List<Long> forTeachers,DistrictMaster districtMaster,List<TeacherDetail> teacherDetails)
	{
		PrintOnConsole.getJFTPrint("JFT:65");	
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(forTeachers!=null && forTeachers.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
				criteria.add(Restrictions.eq("status", statusMaster));
				
				if(forTeachers!=null && forTeachers.size()>0)
					criteria.add(Restrictions.not(Restrictions.in("jobForTeacherId",forTeachers)));
				
				if(teacherDetails!=null && teacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",teacherDetails));
				
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherListAndJobTitle(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,List<Long> jobForTeacherIds,String jobTitle)
	{
		PrintOnConsole.getJFTPrint("JFT:66");	
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
			Criterion criterion2 = Restrictions.in("jobForTeacherId",jobForTeacherIds);
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster)).add(Restrictions.eq("districtMaster",districtMaster)).add(Restrictions.like("jobTitle","%"+jobTitle+"%"));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
		@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,String> check(List<JobOrder> jobOrders) 
	{
			PrintOnConsole.getJFTPrint("JFT:67");	
		Session session = getSession();
		String sql =" SELECT jft.jobId, COUNT(*) AS appliedCnt, COUNT( IF( jft.status NOT IN (:statuss), 1, NULL )) AS avlCnt, COUNT( IF( jft.status =6, 1, NULL ) ) AS hiredCnt " +
		" from jobforteacher jft where jft.jobId IN (:jobOrders) and jft.status!=8 group by jft.jobId ";
		
		String[] statuss = {"dcln","icomp","vlt","widrw","hide","rem"};
		//List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);
		List<StatusMaster> statusMasters =   Utility.getStaticMasters(statuss);
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		query.setParameterList("statuss", statusMasters );
		
		List<Object[]> rows = query.list();
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], obj[1]+"##"+obj[2]+"##"+obj[3]);
			}
		}
		return map;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobsByTeacherListAndJobTitles(String queryString)
	{
		PrintOnConsole.getJFTPrint("JFT:68");	
		List<Integer> jobForTeacherIds= new ArrayList<Integer>();
		List<Long> jobForTeacherId= new ArrayList<Long>();
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try { 
			System.out.println("::::::::::::::::::InSide Job Method:::::::::::::=");
			String sql = "select jft.jobForTeacherId from jobforteacher jft join joborder jo on jo.jobId=jft.jobId  WHERE ("+queryString+")";
			System.out.println("sql:::::::-"+sql);
			Query query = session.createSQLQuery(sql);
			jobForTeacherIds= query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		if(jobForTeacherIds.size()>0){
			for (Integer jId : jobForTeacherIds) {
				jobForTeacherId.add(Long.parseLong(jId+""));
			}
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion2 = Restrictions.in("jobForTeacherId",jobForTeacherId);
				criteria.add(criterion2);
				lstJobForTeacher = criteria.list();
				System.out.println("lstJobForTeacher::::::"+lstJobForTeacher.size());
	
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}

	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int getApplicatCount(int iJobId)
	{
		int iReturnValue=0;
		Session session = getSession();
		String sql =" SELECT COUNT(*) AS appliedCnt from jobforteacher jft where jft.jobId = "+iJobId+" and jft.status!=8";
		Query query = session.createSQLQuery(sql);
		List<BigInteger> rowCount = query.list();
		if(rowCount!=null && rowCount.size()==1)
			iReturnValue=rowCount.get(0).intValue();
		return iReturnValue;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherListForOffers(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,StatusMaster statusMaster,StatusMaster secondaryStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:69");	
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(teacherDetails.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
				Criterion criterion2 = Restrictions.not(Restrictions.isNull("requisitionNumber"));	
				Criterion criterion3 = Restrictions.not(Restrictions.eq("requisitionNumber",""));
				Criterion criterion4 = Restrictions.or(criterion3,criterion2);
				Criterion criterion5 = Restrictions.eq("status",statusMaster);
				if(districtMaster!=null && districtMaster.getDistrictId()==1200390){
					Criterion criterion6 = Restrictions.eq("statusMaster",secondaryStatus);
					Criterion criterion7 = Restrictions.or(criterion5, criterion6);
					lstJobForTeacher = findByCriteria(criterion1,criterion4,criterion7);
				}else{
					lstJobForTeacher = findByCriteria(criterion1,criterion4,criterion5);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsData(Order sortOrderStrVal,int start,int end,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> lstTeacherDetailAllFilter,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,boolean normScoreFlag)
	{
		
		PrintOnConsole.getJFTPrint("JFT:71");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			
			Criterion criterionSchool =null;
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			}
			
			criteria.add(Restrictions.ne("status",statusMaster));
			
			
			if(fDate!=null && tDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
			else if(fDate!=null && tDate==null)
				criteria.add(Restrictions.ge("createdDateTime",fDate));
			else if(tDate!=null && fDate==null)
				criteria.add(Restrictions.le("createdDateTime",tDate));
			

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				}else if(entityID==4 && dateFlag){
					dateDistrictFlag=true;
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}*/
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				flag=true;
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				dateDistrictFlag=true;
			}
			else if(entityID==5)
			{

				System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
				flag=true;
				if(branchMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				else
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));

			}
			else if(entityID==6)
			{
				System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
				flag=true;
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			}
			if(dateDistrictFlag==false && dateFlag)
			{
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				flag=true;
			}
			if(status){
				criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			
			//job applied flag
			if(newcandidatesonlyNew)
			{
				if(appliedfDate!=null && appliedtDate!=null)
				{
					System.out.println("############### newcandidatesonlyNew FT #######################");
					criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedfDate!=null && appliedtDate==null)
				{
					System.out.println("############### newcandidatesonlyNew F #######################");
					//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedtDate!=null && appliedfDate==null)
				{
					System.out.println("############### newcandidatesonlyNew T #######################");
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
			}
			else
			{
				if(appliedfDate!=null && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				else if(appliedfDate!=null && appliedtDate==null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				else if(appliedtDate!=null && appliedfDate==null)
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}
			
			flag=true;
			if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
			{
				criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetailAllFilter))); // filter first time
			}

			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size() >0){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
					/*if(!sortOrderStrVal.toString().contains("spStatus"))
						criteria.addOrder(sortOrderStrVal);*/
				}
				/*else{
					if((headQuarterMaster==null || branchMaster==null) && !(sortOrderStrVal.toString().contains("spStatus") || sortOrderStrVal.toString().contains("normScore")))
					criteria.createCriteria("teacherId").addOrder(sortOrderStrVal);	
				}*/
				
				criteria.setProjection(Projections.groupProperty("teacherId"));
				if(!normScoreFlag){
					criteria.setFirstResult(start);
					criteria.setMaxResults(end);	
				}
				
				
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criteria.add(criterionSchool);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	
	
	@Transactional(readOnly=false)
	public List<Integer> getTeacherDetailByDASARecordsCount(Order sortOrderStrVal,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> lstTeacherDetailAllFilter,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP)
	{
		PrintOnConsole.getJFTPrint("JFT:72");	
		TestTool.getTraceTime("500");
		List<Integer> lstTeacherDetail= new ArrayList<Integer>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(internalCandVal==1){
				TestTool.getTraceTime("501");
				criteria.add(criterionInterCand);
			}

			criteria.add(Restrictions.ne("status",statusMaster));
			
			Criterion criterionSchool =null;
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			}
			
			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				TestTool.getTraceTime("502");
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				TestTool.getTraceTime("503");
				flag=true;
				if(lstJobOrder.size()>0)
				{
					TestTool.getTraceTime("504");
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					TestTool.getTraceTime("505");
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					TestTool.getTraceTime("506");
					dateDistrictFlag=true;
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}*/
					
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
					
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				TestTool.getTraceTime("507");
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				TestTool.getTraceTime("508");
				flag=true;
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				dateDistrictFlag=true;
			}
			else if(entityID==5)
			{
				System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
				flag=true;
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}
			else if(entityID==6)
			{
				System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
				flag=true;
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			}
			if(dateDistrictFlag==false && dateFlag){
				TestTool.getTraceTime("509");
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				flag=true;
			}
			if(status){
				TestTool.getTraceTime("510");
				criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			
			//job applied flag
			//job applied flag
			if(newcandidatesonlyNew)
			{
				if(appliedfDate!=null && appliedtDate!=null)
				{
					System.out.println("############### newcandidatesonlyNew Count FT #######################");
					criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedfDate!=null && appliedtDate==null)
				{
					System.out.println("############### newcandidatesonlyNew Count F #######################");
					//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedtDate!=null && appliedfDate==null)
				{
					System.out.println("############### newcandidatesonlyNew Count T #######################");
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
			}
			else
			{
				if(appliedfDate!=null && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				else if(appliedfDate!=null && appliedtDate==null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				else if(appliedtDate!=null && appliedfDate==null)
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}
			
			
			flag=true;
			if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
			{
				TestTool.getTraceTime("511 T");
				criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetailAllFilter)));
			}

			if(teacherFlag){
				if(filterTeacherList!=null && filterTeacherList.size() >0)
				{
					TestTool.getTraceTime("512 T");
					flag=true; //
					criteria.add(Restrictions.in("teacherId", filterTeacherList)); //
				}
			}
			if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size()>0) //
			{
				TestTool.getTraceTime("513 T");
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));
			}
			
			if(districtMaster!=null  && utype==3)
			{
				TestTool.getTraceTime("514");
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					TestTool.getTraceTime("515");
					System.out.println("master: "+master.getStatus());
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			System.out.println("*****************************************************");
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				TestTool.getTraceTime("516");
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					TestTool.getTraceTime("517");
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				}				
				
				//criteria.setProjection(Projections.countDistinct("teacherId"));
				String countRow= " count(distinct this_.teacherId)";
				criteria.setProjection( Projections.projectionList().add(Projections.sqlProjection(""+countRow+" as countRow", new String[]{"countRow"}, new Type[]{ Hibernate.INTEGER}),"countRow"));
				
				lstTeacherDetail =  criteria.list();
				TestTool.getTraceTime("518 lstTeacherDetail "+lstTeacherDetail.size());
			}
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criteria.add(criterionSchool);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		TestTool.getTraceTime("599");
		return lstTeacherDetail;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsBeforeDateNew(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,StatusMaster statusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:73");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			boolean flag=true;
			boolean dateDistrictFlag=false;
			
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
			}
			if(status)
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
			else
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			
			//job applied flag
			
			/*if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			else if(daysVal==5 && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
			else if(appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));*/
			
			if(appliedfDate!=null && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			else if(appliedfDate!=null && appliedtDate==null)
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			else if(appliedtDate!=null && appliedfDate==null)
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			
			if(teacherFlag && filterTeacherList!=null && filterTeacherList.size() >0)
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			
			if(nByAflag && NbyATeacherList!=null &&NbyATeacherList.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				}
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}
			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsL1(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,StatusMaster statusMaster,List<TeacherDetail> lstTeacherDetailAllFilter)
	{
		PrintOnConsole.getJFTPrint("JFT:74");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			boolean flag=true;
			boolean dateDistrictFlag=false;
			
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
			}
			if(status)
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
			else
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			
			//job applied flag
			
			if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			else if(daysVal==5 && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
			else if(appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			
			filterTeacherList.addAll(lstTeacherDetailAllFilter);
			
			//if(teacherFlag && filterTeacherList!=null && filterTeacherList.size() >0)
			if(filterTeacherList!=null && filterTeacherList.size() >0)
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			
			if(nByAflag && NbyATeacherList!=null &&NbyATeacherList.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				}
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}
			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List<Integer> findJobIdsbyJFTID(List<Integer> jftIds)
	{
		List<Integer> lstjoborders = new ArrayList<Integer>(); 
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    
		    Criterion criterion = Restrictions.in("jobForTeacherId",jftIds);
			criteria.add(criterion);
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("jobId.jobId")));
			lstjoborders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstjoborders;
	}

	
	@Transactional(readOnly=false)
	public List<TeacherDetail> getQuestTeacherDetailRecords(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> notTeacherDetails,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster)
	{
		//System.out.println("========getQuestTeacherDetailRecords=======");
		PrintOnConsole.getJFTPrint("JFT:75");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}

			criteria.add(Restrictions.ne("status",statusMaster));

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				flag=true;
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
			if(status){
				//criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}else{
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			}

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			//job applied flag
			if(appliedfDate!=null && appliedtDate!=null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			}else if(appliedfDate!=null && appliedtDate==null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			}else if(appliedtDate!=null && appliedfDate==null){
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}
			flag=true;
			if(notTeacherDetails.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", notTeacherDetails)));

			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.eq("questCandidate", 1))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				// System.out.println("===Criteria :: "+criteria.toString());
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAvailableCandidteBYDistrict(Order order,int startPos,int limit,DistrictMaster  districtMaster,JobOrder joborder,Criterion firstlastEmailCri,EventDetails eventDetails,UserMaster userMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:76");	
		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    
		    Criteria c1 = criteria.createCriteria("teacherId");
		    
		    if(joborder!=null){
			    Criterion crjobId =Restrictions.eq("jobId", joborder);
			    criteria.add(crjobId);
		    }else{
    			if(userMaster.getEntityType().equals(3))
    			 {
			        List<JobOrder> listJoOrders =schoolInJobOrderDAO.findAllJobBySchool(userMaster.getSchoolId());
			        System.out.println(" SSSSSSSSSSSSSSSSSSSSSSSSS  :: "+userMaster.getSchoolId().getSchoolId());
			        Criterion criterionJObs =Restrictions.in("jobId", listJoOrders);
			        criteria.add(criterionJObs);
    			 } else if(eventDetails.getDistrictMaster()!=null){
		    		criteria.add(Restrictions.eq("districtId", eventDetails.getDistrictMaster().getDistrictId()));
		    	   //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
		    	}else if(eventDetails.getHeadQuarterMaster()!=null)
		    	{
		    		if(eventDetails.getBranchMaster()!=null){
		    			criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster",eventDetails.getBranchMaster()));
		    		}else{
		    			criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster",eventDetails.getHeadQuarterMaster()));
		    		}
		    	}
		    }
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherId")));
			
			if(firstlastEmailCri!=null ){
				//for(Criterion cri: firstlastEmailCri)
					c1.add(firstlastEmailCri);
			}
			
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);			
			c1.addOrder(order);
			
			lstJobForTeacher = criteria.list();
			
					
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAvailableCandidteBYDistrictCount(DistrictMaster  districtMaster,JobOrder joborder,List<Criterion> firstlastEmailCri)
	{
		PrintOnConsole.getJFTPrint("JFT:77");	
		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    Criteria c1 = criteria.createCriteria("teacherId");		    
		    if(joborder!=null){
			    Criterion crjobId =Restrictions.eq("jobId", joborder);
			    criteria.add(crjobId);
		    }else{
		    	if(districtMaster!=null){
		    		criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
		    	   //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
		    	}
		    }			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherId")));
			
			if(firstlastEmailCri!=null && firstlastEmailCri.size()>0){
				for(Criterion cri: firstlastEmailCri)
				    c1.add(cri);
			}
			
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List findUniqueApplicants(DistrictMaster districtMaster,EventDetails eventDetails)
	{
		PrintOnConsole.getJFTPrint("JFT:77");
		 					  districtMaster	 = eventDetails.getDistrictMaster();
		 HeadQuarterMaster headQuarterMaster 	 = eventDetails.getHeadQuarterMaster();
		 BranchMaster branchMaster				 = eventDetails.getBranchMaster();
		List rows= new ArrayList();
		try 
		{
			Session session = getSession();
			/*Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			criteria.setProjection(Projections.distinct(Projections.property("teacherId")));			 
			lstJobForTeacher = criteria.list();*/
			String sql = "";
			//sql = "SELECT distinct(teacherId) as teacherId  FROM  `jobforteacher` where districtId=:districtId";
		
			//sql = "SELECT distinct(teacherId)  FROM  `jobforteacher` jft join joborder jo on jft.jobId=jo.jobId " +
	       // " WHERE  jo.districtId=:districtMaster";
			if(districtMaster!=null)
			  sql = "SELECT distinct(teacherId)  FROM  `jobforteacher`  WHERE districtId=:districtId";
			else if(branchMaster!=null)
				sql = "SELECT distinct(teacherId)  FROM  `jobforteacher` jft join joborder jo on jft.jobId=jo.jobId  WHERE  jo.branchId=:branchMaster";
			else if(headQuarterMaster!=null)
				sql = "SELECT distinct(teacherId)  FROM  `jobforteacher` jft join joborder jo on jft.jobId=jo.jobId  WHERE  jo.headQuarterId=:headQuarterMaster";
			
			if(!sql.equals(""))
			 {
				Query query = session.createSQLQuery(sql);
				if(districtMaster!=null)
					query.setParameter("districtId", districtMaster.getDistrictId());
				else if(branchMaster!=null)
					query.setParameter("branchMaster", branchMaster);
				else if(headQuarterMaster!=null)
					query.setParameter("headQuarterMaster", headQuarterMaster);
			
			  rows = query.list();
			 }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return rows;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrderBySelectedSchool(TeacherDetail teacherDetail,Order order,int startPos,int limit)
	{
		PrintOnConsole.getJFTPrint("JFT:78");	
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1=Restrictions.eq("candidateConsideration",false);
			Criterion criterion2=Restrictions.eq("candidateConsideration",true);
			Criterion criterion=Restrictions.or(criterion1, criterion2);
			criteria.add(criterion);
			Criterion criterion3=Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion3);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.createCriteria("jobId").addOrder(order);
			criteria.setProjection(Projections.groupProperty("jobId"));
			jobOrderList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrderList;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrderBySelectedSchoolAll(TeacherDetail teacherDetail)
	{
		PrintOnConsole.getJFTPrint("JFT:79");	
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1=Restrictions.eq("candidateConsideration",false);
			Criterion criterion2=Restrictions.eq("candidateConsideration",true);
			Criterion criterion=Restrictions.or(criterion1, criterion2);
			criteria.add(criterion);
			Criterion criterion3=Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion3);
			criteria.setProjection(Projections.groupProperty("jobId"));
			jobOrderList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrderList;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> getJobForTeacherByStatus(List<JobOrder> lstJobOrder)
	{
		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass()); 
		//	Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",lstJobOrder);
            
			criteria.add(criterion1);
			criteria.add(criterion2);
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherId"))
			);
			lstJobForTeacher = criteria.list();
			//lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findByIncompJobs(List<JobOrder> jobOrders)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			//StatusMaster statusMaster= statusMasterDAO.findStatusByShortName("icomp");
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("icomp");
			Criterion criterion1 = Restrictions.eq("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",jobOrders);
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getFullTimeDetla(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:80");	
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null)
				{
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					criteria.createCriteria("jobId").add(Restrictions.eq("jobType", "F"));
					criteria.createCriteria("status").add(Restrictions.eq("statusShortName","hird"));
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public void addReminderCount(List<Long> jobForTeacherIds){
		try {
			PrintOnConsole.getJFTPrint("JFT:81");	
			String hql = "update JobForTeacher set noOfReminderSent=1,reminderSentDate=:currDate WHERE jobForTeacherId IN (:jobForTeacherIds)";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameter("currDate", new Date());
			query.setParameterList("jobForTeacherIds", jobForTeacherIds);
			System.out.println("New  OOO  "+query.executeUpdate());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	@Transactional(readOnly=false)
	public void updateReminderCount(List<Long> jobForTeacherIds){
		try {
			PrintOnConsole.getJFTPrint("JFT:82");	
			String hql = "update JobForTeacher set noOfReminderSent=noOfReminderSent+1,reminderSentDate=:currDate WHERE jobForTeacherId IN (:jobForTeacherIds)";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameter("currDate", new Date());
			query.setParameterList("jobForTeacherIds", jobForTeacherIds);
			System.out.println("Existing  OOO  "+query.executeUpdate());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	
	
	/********	Teacher applied job list did not have any communication *********/
	/************by Ram Nath*********************/	
	@Transactional(readOnly=true)
	public int applicantsNotContactedListWithAndWithoutDistrictCountRow(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal, int schoolId,String startDate,String enddate,String jobOrderId)
	{	
		PrintOnConsole.getJFTPrint("JFT:83");	
		System.out.println("order iddd"+jobOrderId);
		 Date startD = new Date("1/1/2000");
         Date endD = new Date("12/31/9999"); 

        if(startDate!=null && !startDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(startDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(enddate!=null && !enddate.equals(""))
			try {
				endD = Utility.getCurrentDateFormart(enddate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		
		int countrow=0;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			LogicalExpression orExp = Restrictions.or(Restrictions.isNull("lastActivity"), Restrictions.eq("lastActivity", ""));					
			criteria.add(orExp);
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",endD));
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
						
			List<JobOrder> lstJobOrderBySchoolId=new ArrayList<JobOrder>();;
			if(schoolId!=0){
				Criterion scriterion = Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false));
				List<SchoolInJobOrder> lstSchInJOrder=schoolInJobOrderDAO.findByCriteria(scriterion);
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null)
						lstJobOrderBySchoolId.add(sijo.getJobId());
				System.out.println("jobid=="+lstJobOrderBySchoolId);
				if(lstJobOrderBySchoolId.size()>0)
				criteria.add(Restrictions.in("jobId",lstJobOrderBySchoolId));
			}					
			//Criteria c1 = null;				
			//c1 = criteria.createCriteria("jobId");
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			 //c1.add(Restrictions.eq("districtMaster",districtMaster));
			 
			}
			
			if(schoolId!=0 && lstJobOrderBySchoolId.size()==0)
			countrow=0;
			else
			countrow= ((Number)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return countrow;
	}
	@Transactional(readOnly=true)
	public List<String []> applicantsNotContactedListWithAndWithoutDistrict(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal ,int start, int noOfRow,int schoolId,boolean report,String startDate,String enddate,String jobOrderId)
	{
		PrintOnConsole.getJFTPrint("JFT:84");	
		System.out.println("job iddddddddddd"+jobOrderId);
		
		 Date startD = new Date("1/1/2000");
         Date endD = new Date("12/31/9999"); 

        if(startDate!=null && !startDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(startDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(enddate!=null && !enddate.equals(""))
			try {
				endD = Utility.getCurrentDateFormart(enddate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		List<String []> lstJobForTeacher=new ArrayList<String []>();
		try{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			criteria.createAlias("jobId", "jo").createAlias("teacherId", "td").createAlias("jo.districtMaster", "dm")
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("dm.districtName"), "districtName" )
		        .add( Projections.property("td.firstName"), "firstName" )
		        .add( Projections.property("td.lastName"), "lastName" )
		        .add( Projections.property("td.emailAddress"), "emailAddress" )
		        .add( Projections.property("jo.jobTitle"), "jobTitle" )
		       .add( Projections.property("createdDateTime"),"createdDateTime132")	
		       .add( Projections.property("jo.status"), "status" )
		       .add( Projections.property("jo.jobId"), "jobId11" )
		    );
			if(districtMaster!=null )
			    criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			LogicalExpression orExp = Restrictions.or(Restrictions.isNull("lastActivity"), Restrictions.eq("lastActivity", ""));
			criteria.add(orExp);
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
			
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",endD));
						List<JobOrder> lstJobOrderBySchoolId=new ArrayList<JobOrder>();;
			if(schoolId!=0){
				
				Criterion scriterion = Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false));
				List<SchoolInJobOrder> lstSchInJOrder=schoolInJobOrderDAO.findByCriteria(scriterion);
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null){
						lstJobOrderBySchoolId.add(sijo.getJobId());
						//System.out.println("sijo=="+sijo.getSchoolId().getSchoolName());
					}
				//System.out.println("jobid=="+lstJobOrderBySchoolId);
				if(lstJobOrderBySchoolId.size()>0)
				criteria.add(Restrictions.in("jobId",lstJobOrderBySchoolId));
			}
			if(!report){
			criteria.setFirstResult(start);
			criteria.setMaxResults(noOfRow);
			}
							
			criteria.addOrder(sortOrderStrVal);
			
			long a = System.currentTimeMillis();
			if(schoolId!=0 && lstJobOrderBySchoolId.size()==0)
			lstJobForTeacher = new ArrayList<String[]>();
			else
			lstJobForTeacher = criteria.list() ;			
			long b = System.currentTimeMillis();
			System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + lstJobForTeacher.size() );
			
			} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}
	/*************status filter**************/
	
	@Transactional(readOnly=true)
	public int applicantsNotContactedListWithAndWithoutDistrictCountRowForStatus(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal, int schoolId,String startDate,String enddate,String jobOrderId,List<Long> jftIds,boolean statusflag)
	{	
		PrintOnConsole.getJFTPrint("JFT:85");	
		System.out.println("order iddd"+jobOrderId);
		 Date startD = new Date("1/1/2000");
         Date endD = new Date("12/31/9999"); 

        if(startDate!=null && !startDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(startDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(enddate!=null && !enddate.equals(""))
			try {
				endD = Utility.getCurrentDateFormart(enddate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		
		int countrow=0;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());	
			
			if(statusflag && jftIds.size()>0){
				criteria.add(Restrictions.in("jobForTeacherId", jftIds));
			}else if(statusflag && jftIds.size()==0){
				return countrow;
			}
			
			LogicalExpression orExp = Restrictions.or(Restrictions.isNull("lastActivity"), Restrictions.eq("lastActivity", ""));					
			criteria.add(orExp);
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",endD));
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
						
			List<JobOrder> lstJobOrderBySchoolId=new ArrayList<JobOrder>();;
			if(schoolId!=0){
				Criterion scriterion = Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false));
				List<SchoolInJobOrder> lstSchInJOrder=schoolInJobOrderDAO.findByCriteria(scriterion);
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null)
						lstJobOrderBySchoolId.add(sijo.getJobId());
				System.out.println("jobid=="+lstJobOrderBySchoolId);
				if(lstJobOrderBySchoolId.size()>0)
				criteria.add(Restrictions.in("jobId",lstJobOrderBySchoolId));
			}					
			//Criteria c1 = null;				
			//c1 = criteria.createCriteria("jobId");
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			 //c1.add(Restrictions.eq("districtMaster",districtMaster));
			}
			
			if(schoolId!=0 && lstJobOrderBySchoolId.size()==0)
			countrow=0;
			else
			countrow= ((Number)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return countrow;
	}
	@Transactional(readOnly=true)
	public List<String []> applicantsNotContactedListWithAndWithoutDistrictForStatus(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal ,int start, int noOfRow,int schoolId,boolean report,String startDate,String enddate,String jobOrderId,List<Long> jftIds,boolean statusflag)
	{
		PrintOnConsole.getJFTPrint("JFT:86");	
		System.out.println("job iddddddddddd"+jobOrderId);
		
		 Date startD = new Date("1/1/2000");
         Date endD = new Date("12/31/9999"); 

        if(startDate!=null && !startDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(startDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(enddate!=null && !enddate.equals(""))
			try {
				endD = Utility.getCurrentDateFormart(enddate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		List<String []> lstJobForTeacher=new ArrayList<String []>();
		try{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(statusflag && jftIds.size()>0){
				criteria.add(Restrictions.in("jobForTeacherId", jftIds));
			}else if(statusflag && jftIds.size()==0){
				return lstJobForTeacher;
			}
			criteria.createAlias("jobId", "jo").createAlias("teacherId", "td").createAlias("jo.districtMaster", "dm")
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("dm.districtName"), "districtName" )
		        .add( Projections.property("td.firstName"), "firstName" )
		        .add( Projections.property("td.lastName"), "lastName" )
		        .add( Projections.property("td.emailAddress"), "emailAddress" )
		        .add( Projections.property("jo.jobTitle"), "jobTitle" )
		       .add( Projections.property("createdDateTime"),"createdDateTime132")	
		       .add( Projections.property("jo.status"), "status" )
		       .add( Projections.property("jo.jobId"), "jobId11" )
		    );
			if(districtMaster!=null )
			criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			
			LogicalExpression orExp = Restrictions.or(Restrictions.isNull("lastActivity"), Restrictions.eq("lastActivity", ""));
			criteria.add(orExp);
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
			
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",endD));
						List<JobOrder> lstJobOrderBySchoolId=new ArrayList<JobOrder>();;
			if(schoolId!=0){
				
				Criterion scriterion = Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false));
				List<SchoolInJobOrder> lstSchInJOrder=schoolInJobOrderDAO.findByCriteria(scriterion);
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null){
						lstJobOrderBySchoolId.add(sijo.getJobId());
						}
				
				if(lstJobOrderBySchoolId.size()>0)
				criteria.add(Restrictions.in("jobId",lstJobOrderBySchoolId));
			}
			if(!report){
			criteria.setFirstResult(start);
			criteria.setMaxResults(noOfRow);
			}
							
			criteria.addOrder(sortOrderStrVal);
			
			long a = System.currentTimeMillis();
			if(schoolId!=0 && lstJobOrderBySchoolId.size()==0)
			lstJobForTeacher = new ArrayList<String []>();
			else
			lstJobForTeacher = criteria.list() ;			
			long b = System.currentTimeMillis();
			System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + lstJobForTeacher.size() );
			
			} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}
	/************end Ram Nath*********************/
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherListAllJob(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster,List<StatusMaster> listStatusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:87");	
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}

				//criteria.createCriteria("status").add(Restrictions.ne("statusShortName","hird"));
				if(listStatusMaster!=null)
					criteria.add(Restrictions.not(Restrictions.in("status",listStatusMaster)));
					
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	/**
	 * Amit Kumar
	 */
	@Transactional(readOnly=false)
	public int applicantsByCertificationsListWithAndWithoutDistrictCountRow(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal, int schoolId,Integer stateId,Integer certTypeId,Double experience,String sfromDate,String stoDate)
	{
		PrintOnConsole.getJFTPrint("JFT:88");	
		int countrow=0;
		List<Object[]> list=new ArrayList<Object[]>();
		boolean certificateFlag=false,experienceFlag=false,appliedFlag=false;
		try
		{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(getPersistentClass());			
			criteria
			.createAlias("jobId", "jo")
			.createAlias("teacherId", "td")
			.createAlias("jo.districtMaster", "dm")
			.setProjection( Projections.projectionList()
							.add( Projections.property("dm.districtName"), "districtName")
							.add( Projections.property("td.firstName"), "firstName")
							.add( Projections.property("td.lastName"), "lastName")
							.add( Projections.property("td.emailAddress"), "emailAddress")
							.add( Projections.max("createdDateTime"))
							.add( Projections.property("redirectedFromURL"), "redirectedFromURL")
							.add( Projections.property("td.teacherId"), "teacherId")
							.add( Projections.groupProperty("teacherId"))
		    );
			
			if(districtMaster!=null)
				criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			
			if(schoolId!=0)
				criteria.add(Restrictions.eq("schoolMaster", schoolMasterDAO.findById(new Long(schoolId), false, false)));
			
			Date fDate=null;
			Date tDate=null;
			if(!sfromDate.equals(""))
				 fDate = Utility.getCurrentDateFormart(sfromDate);
			if(!stoDate.equals(""))
				 tDate = Utility.getCurrentDateFormart(stoDate);
			
			if(fDate!=null && tDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
			else if(fDate!=null && tDate==null)
				criteria.add(Restrictions.ge("createdDateTime",fDate));
			else if(fDate==null && tDate!=null)
				criteria.add(Restrictions.le("createdDateTime",tDate));
			
			criteria.addOrder(sortOrderStrVal);
			
			list = criteria.list();
			
			List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
			Map<Integer, List<TeacherCertificate>> teacherCertificateMap= new HashMap<Integer, List<TeacherCertificate>>();
			Map<Integer, TeacherExperience> teacherExperienceMap = new HashMap<Integer, TeacherExperience>();
			
			for (Iterator it = list.iterator(); it.hasNext();)
			{
				
				Integer teacherId=null;
	            Object[] row = (Object[]) it.next();
	            if(row[6]!=null)
	            {
	            	teacherId=Integer.parseInt(row[6].toString());
	            	TeacherDetail td = new TeacherDetail();
	            	td.setTeacherId(teacherId);
	            	teacherDetailList.add(td);
	            }
			}
			
			List<TeacherCertificate> teacherCertificateList=null;
			List<TeacherExperience> teacherExperienceList=null;
			if(teacherDetailList.size()>0)
			{
				if(stateId==0 && certTypeId==0)
				{
					teacherCertificateList = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList);
					certificateFlag=false;
				}
				else if(stateId!=0 && certTypeId!=0)
				{
					teacherCertificateList = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList, certificateTypeMasterDAO.findById(certTypeId, false, false));
					certificateFlag=true;
				}
				if(experience==null||experience==0.0)
				{
					teacherExperienceList  = teacherExperienceDAO.findExperienceForTeachers(teacherDetailList);
					experienceFlag=false;
				}
				else if(experience!=0.0)
				{
					teacherExperienceList  = teacherExperienceDAO.findExperienceForTeachers(teacherDetailList, experience);
					experienceFlag=true;
				}
			}
			
			if(teacherCertificateList!=null && teacherCertificateList.size()>0)
			{
				for(TeacherCertificate teacherCertificate : teacherCertificateList)
				{
					TeacherDetail teacherDetail = teacherCertificate.getTeacherDetail();
					Integer teacherId = teacherDetail.getTeacherId();
					List<TeacherCertificate> certificateListOfTeacher = null;
						
					if(teacherCertificateMap.get(teacherId)!=null)
					{
						certificateListOfTeacher=teacherCertificateMap.get(teacherId);
						certificateListOfTeacher.add(teacherCertificate);
					}
					else
					{
						certificateListOfTeacher = new ArrayList<TeacherCertificate>();
						certificateListOfTeacher.add(teacherCertificate);
					}
					teacherCertificateMap.put(teacherId, certificateListOfTeacher);
				}
			}
				
			if(teacherExperienceList!=null && teacherExperienceList.size()>0)
			{
				for(TeacherExperience teacherExperience : teacherExperienceList)
					teacherExperienceMap.put(teacherExperience.getTeacherId().getTeacherId(), teacherExperience);
			}
			
			List<Object[]> list1=null;
			List<Object[]> list2=null;
			List<Object[]> list3=null;
			List<Object[]> list4=null;
			if(certificateFlag)
			{
				list1=new ArrayList<Object[]>();
				for (Iterator it = list.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherCertificateMap.get(Integer.parseInt(row[6].toString()))!=null && teacherCertificateMap.get(Integer.parseInt(row[6].toString())).size()>0)
		            	{
		            		list1.add(row);
		            	}
		            }
				}
			}
			
			if(experienceFlag)
			{
				int i=1;
				list2=new ArrayList<Object[]>();
				for (Iterator it = list.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherExperienceMap.get(Integer.parseInt(row[6].toString()))!=null)
		            	{
		            		if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=null && teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=0.0)
		            		{
		            			if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining().equals(experience))
		            			{
		            				list2.add(row);
		            				System.out.println("Count Row:::::list.size()===="+list.size()+":::::::::::::::"+i);
		            				++i;
		            			}
		            		}
		            	}
		            }
				}
			}
			
			if(certificateFlag && experienceFlag)
			{
				list3=new ArrayList<Object[]>();
				list4=new ArrayList<Object[]>();
				for (Iterator it = list.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherCertificateMap.get(Integer.parseInt(row[6].toString()))!=null && teacherCertificateMap.get(Integer.parseInt(row[6].toString())).size()>0)
		            	{
		            		list3.add(row);
		            	}
		            }
				}
				if(list3!=null && list3.size()>0)
				{
					for (Iterator it = list3.iterator(); it.hasNext();)
					{
						Object[] row = (Object[]) it.next();
						if(row[6]!=null)
						{
							if(teacherExperienceMap.get(Integer.parseInt(row[6].toString()))!=null)
			            	{
			            		if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=null && teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=0.0)
			            		{
			            			if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()==experience)
			            			{
			            				System.out.println("CountRow::::::::::::::teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()=="+teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()+"::::::::::::"+"experience=="+experience);
			            				list4.add(row);
			            			}			            			
			            		}
			            	}
						}
					}
				}
			}
			
			if(!certificateFlag && !experienceFlag)
			{
				countrow=list.size();
				System.out.println("CountRow::::::::::::::!certificateFlag && !experienceFlag");
				System.out.println("list.size()=="+list.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(certificateFlag && experienceFlag)
			{
				countrow=list4.size();
				System.out.println("CountRow::::::::::::::certificateFlag && experienceFlag");
				System.out.println("list4.size()=="+list4.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(certificateFlag)
			{
				countrow=list1.size();
				System.out.println("CountRow::::::::::::::certificateFlag");
				System.out.println("list1.size()=="+list1.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(experienceFlag)
			{
				countrow=list2.size();
				System.out.println("CountRow::::::::::::::experienceFlag");
				System.out.println("list2.size()=="+list2.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			return countrow;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return countrow;	
	}

	/**
	 * Amit Kumar
	 */
	@Transactional(readOnly=false)
	public List applicantsByCertificationsListWithAndWithoutDistrict(DistrictMaster districtMaster,int sortingcheck,Order sortOrderStrVal,int start,int noOfRow,int schoolId,boolean report,Integer stateId,Integer certTypeId,Double experience,String sfromDate,String stoDate)
	{
		PrintOnConsole.getJFTPrint("JFT:89");	
		List<Object[]> list=new ArrayList<Object[]>();
		List<Object[]> tempList=new ArrayList<Object[]>();
		List finalList=new ArrayList();
		boolean certificateFlag=false,experienceFlag=false,appliedFlag=false;
		try
		{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(getPersistentClass());			
			criteria
			.createAlias("jobId", "jo")
			.createAlias("teacherId", "td")
			.createAlias("jo.districtMaster", "dm")
			.setProjection( Projections.projectionList()
							.add( Projections.property("dm.districtName"), "districtName")
							.add( Projections.property("td.firstName"), "firstName")
							.add( Projections.property("td.lastName"), "lastName")
							.add( Projections.property("td.emailAddress"), "emailAddress")
							.add( Projections.max("createdDateTime"))
							.add( Projections.property("redirectedFromURL"), "redirectedFromURL")
							.add( Projections.property("td.teacherId"), "teacherId")
							.add( Projections.groupProperty("teacherId"))
		    );
			
			if(districtMaster!=null)
				criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			
			if(schoolId!=0)
				criteria.add(Restrictions.eq("schoolMaster", schoolMasterDAO.findById(new Long(schoolId), false, false)));
			
			Date fDate=null;
			Date tDate=null;
			if(!sfromDate.equals(""))
				 fDate = Utility.getCurrentDateFormart(sfromDate);
			if(!stoDate.equals(""))
				 tDate = Utility.getCurrentDateFormart(stoDate);
			
			if(fDate!=null && tDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				//criteria.add(Restrictions.between("createdDateTime", fDate, tDate));
			else if(fDate!=null && tDate==null)
				criteria.add(Restrictions.ge("createdDateTime",fDate));
			else if(fDate==null && tDate!=null)
				criteria.add(Restrictions.le("createdDateTime",tDate));
			
			System.out.println("");
			System.out.println("report=="+report+":::start=="+start+":::noOfRow=="+noOfRow);
			System.out.println("");
			criteria.addOrder(sortOrderStrVal);
			tempList=criteria.list();
			if(!report)
			{
				criteria.setFirstResult(start);
				criteria.setMaxResults(noOfRow);
				
				System.out.println("report=="+report+"	start=="+start+"	noOfRow=="+noOfRow);
			}
			//System.out.println("start=="+start+"	noOfRow=="+noOfRow+"\n");
			list = criteria.list();
			//System.out.println(""+list.size());
			List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
			Map<Integer, List<TeacherCertificate>> teacherCertificateMap= new HashMap<Integer, List<TeacherCertificate>>();
			Map<Integer, TeacherExperience> teacherExperienceMap = new HashMap<Integer, TeacherExperience>();
			
			//int j=1;
			for (Iterator it = tempList.iterator(); it.hasNext();)
			{
				Integer teacherId=null;
	            Object[] row = (Object[]) it.next();
	            if(row[6]!=null)
	            {
	            	teacherId=Integer.parseInt(row[6].toString());
	            	TeacherDetail td = new TeacherDetail();
	            	td.setTeacherId(teacherId);
	            	teacherDetailList.add(td);
	            	//System.out.println(""+j+"\n teacherId=="+teacherId+"::::::::: td=="+td);
	            }
	           //++j;
			}
			
			List<TeacherCertificate> teacherCertificateList=null;
			List<TeacherExperience> teacherExperienceList=null;
			if(teacherDetailList.size()>0)
			{
				if(stateId==0 && certTypeId==0)
				{
					teacherCertificateList = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList);
					certificateFlag=false;
				}
				else if(stateId!=0 && certTypeId!=0)
				{
					teacherCertificateList = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList, certificateTypeMasterDAO.findById(certTypeId, false, false));
					certificateFlag=true;
				}
				if(experience==null||experience==0.0)
				{
					teacherExperienceList  = teacherExperienceDAO.findExperienceForTeachers(teacherDetailList);
					experienceFlag=false;
				}
				else if(experience!=0.0)
				{
					teacherExperienceList  = teacherExperienceDAO.findExperienceForTeachers(teacherDetailList, experience);
					experienceFlag=true;
				}
			}
			
			if(teacherCertificateList!=null && teacherCertificateList.size()>0)
			{
				System.out.println("teacherCertificateList.size()=="+teacherCertificateList.size()+"\n");
				for(TeacherCertificate teacherCertificate : teacherCertificateList)
				{
					TeacherDetail teacherDetail = teacherCertificate.getTeacherDetail();
					Integer teacherId = teacherDetail.getTeacherId();
					List<TeacherCertificate> certificateListOfTeacher = null;
					//System.out.println("teacherId=="+teacherId);
					if(teacherCertificateMap.get(teacherId)!=null)
					{
						System.out.println("teacherCertificateMap.get(teacherId).size()=="+teacherCertificateMap.get(teacherId).size());
						certificateListOfTeacher=teacherCertificateMap.get(teacherId);
						certificateListOfTeacher.add(teacherCertificate);
					}
					else
					{
						certificateListOfTeacher = new ArrayList<TeacherCertificate>();
						certificateListOfTeacher.add(teacherCertificate);
					}
					teacherCertificateMap.put(teacherId, certificateListOfTeacher);
				}
			}
				
			if(teacherExperienceList!=null && teacherExperienceList.size()>0)
			{
				for(TeacherExperience teacherExperience : teacherExperienceList)
					teacherExperienceMap.put(teacherExperience.getTeacherId().getTeacherId(), teacherExperience);
			}
			
			List<Object[]> list1=null;
			List<Object[]> list2=null;
			List<Object[]> list3=null;
			List<Object[]> list4=null;
			if(certificateFlag)
			{
				list1=new ArrayList<Object[]>();
				for (Iterator it = tempList.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherCertificateMap.get(Integer.parseInt(row[6].toString()))!=null && teacherCertificateMap.get(Integer.parseInt(row[6].toString())).size()>0)
		            	{
		            		list1.add(row);
		            	}
		            }
				}
			}
			
			if(experienceFlag)
			{
				int i=1;
				list2=new ArrayList<Object[]>();
				for (Iterator it = tempList.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherExperienceMap.get(Integer.parseInt(row[6].toString()))!=null)
		            	{
		            		if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=null && teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=0.0)
		            		{
		            			if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining().equals(experience))
		            			{
		            				list2.add(row);
		            				System.out.println("tempList.size()===="+tempList.size()+":::::::::::::::::::======"+i);
		            				++i;
		            			}
		            		}
		            			
		            	}
		            }
				}
			}
			
			if(certificateFlag && experienceFlag)
			{
				list3=new ArrayList<Object[]>();
				list4=new ArrayList<Object[]>();
				for (Iterator it = tempList.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherCertificateMap.get(Integer.parseInt(row[6].toString()))!=null && teacherCertificateMap.get(Integer.parseInt(row[6].toString())).size()>0)
		            	{
		            		list3.add(row);
		            	}
		            }
				}
				if(list3!=null && list3.size()>0)
				{
					for (Iterator it = list3.iterator(); it.hasNext();)
					{
						Object[] row = (Object[]) it.next();
						if(row[6]!=null)
						{
							if(teacherExperienceMap.get(Integer.parseInt(row[6].toString()))!=null)
			            	{
			            		if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=null && teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=0.0)
			            		{
			            			if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining().equals(experience))
			            			{
			            				System.out.println("teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()=="+teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()+"::::::::::::"+"experience=="+experience);
			            				list4.add(row);
			            			}			            			
			            		}
			            	}
						}
					}
				}
			}
			
			if(!certificateFlag && !experienceFlag)
			{
				if(list!=null && list.size()>0)
					finalList.add(list);
				else
				{
					list=new ArrayList<Object[]>();
					finalList.add(list);
				}
				//finalList.add(list);
				finalList.add(teacherCertificateMap);
				finalList.add(teacherExperienceMap);
				System.out.println("::::::::::::::::::::::!certificateFlag && !experienceFlag");
				System.out.println("list.size()=="+list.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(certificateFlag && experienceFlag)
			{
				if(list4!=null && list4.size()>0)
					finalList.add(list4);
				else
				{
					list4=new ArrayList<Object[]>();
					finalList.add(list4);
				}
				//finalList.add(list4);
				finalList.add(teacherCertificateMap);
				finalList.add(teacherExperienceMap);
				System.out.println("::::::::::::::::::::::certificateFlag && experienceFlag");
				System.out.println("list4.size()=="+list4.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(certificateFlag)
			{
				if(list1!=null && list1.size()>0)
					finalList.add(list1);
				else
				{
					list1=new ArrayList<Object[]>();
					finalList.add(list1);
				}
				//finalList.add(list1);
				finalList.add(teacherCertificateMap);
				finalList.add(teacherExperienceMap);
				System.out.println("::::::::::::::::::::::certificateFlag");
				System.out.println("list1.size()=="+list1.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(experienceFlag)
			{
				if(list2!=null && list2.size()>0)
					finalList.add(list2);
				else
				{
					list2=new ArrayList<Object[]>();
					finalList.add(list2);
				}
				//finalList.add(list2);
				finalList.add(teacherCertificateMap);
				finalList.add(teacherExperienceMap);
				System.out.println("::::::::::::::::::::::experienceFlag");
				System.out.println("list2.size()=="+list2.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			return finalList;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return finalList;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getApplicantByAvailableAndTransList(StatusMaster statusMaster,List<SecondaryStatus> secondaryStatusList,DistrictMaster districtMaster,String sfromDate,String stoDate,String jobOrderId)
	{
		PrintOnConsole.getJFTPrint("JFT:90");	
		System.out.println("job id=="+jobOrderId);
		 Date d = new Date(); 
     	 Date startD = new Date("1/1/2000");
         Date endD = new Date(); 
         Date CretedendD = new Date("12/31/9999");
        if(sfromDate!=null && !sfromDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(sfromDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(stoDate!=null && !stoDate.equals(""))
			try {
				CretedendD = Utility.getCurrentDateFormart(stoDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",CretedendD));
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
			
			
						
			Criterion criterion1 = Restrictions.in("secondaryStatus", secondaryStatusList);			
			Criterion criterion2 = Restrictions.isNull("statusMaster");
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);
			
			Criterion criterion4 = Restrictions.eq("status",statusMaster);
			Criterion criterion5 = Restrictions.eq("statusMaster",statusMaster);
			Criterion criterion6 = Restrictions.and(criterion4,criterion5);
			
			Criterion criterion7= Restrictions.or(criterion6,criterion3);
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			}
			criteria.createCriteria("jobId").add(Restrictions.eq("status", "A")).add(Restrictions.ge("jobEndDate", d));
			
			criteria.add(criterion7);
			
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	/*********************/
	@Transactional(readOnly=false)
	public List<JobForTeacher> getApplicantByAvailableAndTransListHired(StatusMaster statusMaster,List<SecondaryStatus> secondaryStatusList,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:91");	
		Date d = new Date(); 
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.createCriteria("status").add(Restrictions.eq("statusId", Integer.parseInt("6")));
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			}
			//criteria.createCriteria("jobId").add(Restrictions.eq("status", "A"));
			lstJobForTeacher =  criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	/********************/
		
	@Transactional(readOnly=false)
    public List<String[]> getJobForTeacherByJobAndTeachersList(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,int entityID)
    {
    	PrintOnConsole.getJFTPrint("JFT:92");	
    	 List<String[]> lstJobForTeacher=new ArrayList<String[]>();
    	 if(lstTeacherDetails.size()>0){
	         try{
	               Session session = getSession();
	               Criteria criteria = session.createCriteria(getPersistentClass());             
	               criteria.setProjection( Projections.projectionList()
	                       .add( Projections.property("teacherId.teacherId"), "teachId" )
	                       .add( Projections.count("teacherId") )              
	                       .add( Projections.groupProperty("teacherId") )
	                   );
	               Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
	               if(districtMaster!=null && entityID!=1){
	            	     criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
	                     //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
	               }
	               if(lstTeacherDetails.size()>0){
	                     criteria.add(criterionTeacher);
	               }
	              // criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
	               criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
	               lstJobForTeacher = criteria.list() ;
	         }catch (Throwable e) {
	               e.printStackTrace();
	         }   
    	 }
         return lstJobForTeacher;
    }
    
  //added By 21-03-2015 Ram nath
    @Transactional(readOnly=false)
    public List<JobForTeacher> findAllTeacherByDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster)
    {
    	PrintOnConsole.getJFTPrint("JFT:93");	
    	List<JobForTeacher> lstJobForTeacher= null;
    	try 
    	{
    		
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			criteria.add(Restrictions.eq("teacherId",teacherDetail));
    			criteria.add(Restrictions.isNotNull("offerReady"));
    			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			lstJobForTeacher = criteria.list();
    	} 
    	catch (Exception e) {
    		e.printStackTrace();
    	}		
    	return lstJobForTeacher;
    }
  //added By 21-03-2015 Ram nath
    
  //added By 21-03-2015 Ram nath
    @Transactional(readOnly=false)
    public List<JobForTeacher> findAllTeacherByDistrictAndJobId(JobOrder jobOrder,DistrictMaster districtMaster)
    {
    	PrintOnConsole.getJFTPrint("JFT:93");	
    	List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    	try 
    	{
    		
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			criteria.add(Restrictions.eq("jobId",jobOrder));
    			criteria.add(Restrictions.isNotNull("offerReady"));
    			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			lstJobForTeacher = criteria.list();
    	} 
    	catch (Exception e) {
    		e.printStackTrace();
    	}		
    	return lstJobForTeacher;
    }
  //added By 21-03-2015 Ram nath
    
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllQQFinalizedTeacherList(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster)
	{
    	PrintOnConsole.getJFTPrint("JFT:94");	
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
				    criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				    criteria.setProjection(Projections.groupProperty("teacherId"));
				    lstTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstTeacher;
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> getQQFinalizedNotesForTeacher(TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
    	PrintOnConsole.getJFTPrint("JFT:95");	
		List<JobForTeacher> lstjob=new ArrayList<JobForTeacher>();
		
		if(teacherDetail!=null && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(teacherDetail!=null)
					criteria.add(Restrictions.eq("teacherId",teacherDetail));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				    //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				    //criteria.setProjection(Projections.distinct(Projections.property("teacherId")));
				    lstjob =  criteria.list();
				    } 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstjob;
	}
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllQQFinalizeTeacherList(DistrictMaster districtMaster)
	{
    	PrintOnConsole.getJFTPrint("JFT:96");	
		List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
		if(districtMaster!=null){
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
			    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
			    criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			    //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			    criteria.setProjection(Projections.groupProperty("teacherId"));
			    teacherDetailList = criteria.list();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return teacherDetailList;
	}

    //getting applied detail by jobs and teachers   
	@Transactional(readOnly=false)
	public List<JobForTeacher> getAppliedDetailsByTeacherIdsAndJobIds(List<TeacherDetail> lstTeacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetail);
			lstJobForTeacher = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   @Transactional(readOnly=false)
   public   List <JobForTeacher>  findAllAppliedJobsOfTeacher(Integer teacherId)
   {
      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
		try{
			 Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
			 lstJobForTeacher2=findByCriteria(criterion);
			 
		}catch(Exception e){
			
				e.printStackTrace();
		}
		
	  return lstJobForTeacher2;
   }
   
   
   @Transactional(readOnly=false)
   public   List <JobForTeacher>  getEpiStatusByTeacher(Integer teacherId)
   {
	   PrintOnConsole.getJFTPrint("JFT:97");	
      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
		try{
			
			      Session session = getSession();
			      
			      Criteria criteria = session.createCriteria(getPersistentClass());
			      Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
			      Criterion criterion1 =Restrictions.eq("baseStatus", true);
			      Criterion criterion2 =Restrictions.eq("epiForFullTimeTeachers",true);
			      Criterion criterion3 =Restrictions.or(criterion1, criterion2);
			
			criteria.add(criterion);
			
			Criteria c1 = criteria.createCriteria("jobId").add(Restrictions.eq("status","A"));
			c1.createCriteria("jobCategoryMaster").add(criterion3);
			
			lstJobForTeacher2 = criteria.list();
			}catch(Exception e){
			
				e.printStackTrace();
		}
		
	  return lstJobForTeacher2;
   }
   
   @Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJobByTeacherforOnBoarding(Order  sortOrderStrVal,TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
	   PrintOnConsole.getJFTPrint("JFT:98");	
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
		List<JobForTeacher> lstJobForTeacher = new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(teacherDetail!=null && districtMaster!=null){
				criteria.add(Restrictions.eq("teacherId",teacherDetail));
				criteria.add(Restrictions.ne("status",statusMaster));
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				lstJobForTeacher = criteria.list();
			}	
					
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   
   /********Hired Candidate with district *********/	
	@Transactional(readOnly=false)
	public List<JobForTeacher> hiredCandidateListByDistrict(DistrictMaster districtMaster, Order sortOrderStrVal, boolean teacherDetailFlag)
	{
		PrintOnConsole.getJFTPrint("JFT:99");	
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion hiredcriterion = Restrictions.eq("status", statusMaster);
			
			criteria.add(hiredcriterion);
			//Criteria c1 = null;
			Criteria c2 = null;
			
			//c1 = criteria.createCriteria("jobId");
			c2 = criteria.createCriteria("teacherId");
			
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			 //c1.add(Restrictions.eq("districtMaster",districtMaster));
		     
			if(teacherDetailFlag){
				c2.addOrder(sortOrderStrVal);
			}
			lstJobForTeacher =  criteria.list();
		}else{
			lstJobForTeacher = null;
		}
	}catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}

	/***************/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestapplication(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName) 
	{
		PrintOnConsole.getJFTPrint("JFT:100");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
          
			
           if(sortColomnName.equalsIgnoreCase("jobforteacherid"))
				sortColomnName = "Internal_Record_ID";
			else if(sortColomnName.equalsIgnoreCase("teacherid"))
				sortColomnName = "Internal_Teacher_ID";
			else if(sortColomnName.equalsIgnoreCase("jobid"))
				sortColomnName = "Internal_Job_ID";
			else if(sortColomnName.equalsIgnoreCase("jobtitle"))
				sortColomnName = "Job_Title";
			else if(sortColomnName.equalsIgnoreCase("jobstartdate"))
				sortColomnName = "Job_Posted_Date";
			else if(sortColomnName.equalsIgnoreCase("jobenddate"))
				sortColomnName = "Job_Post_End_Date";
			else if(sortColomnName.equalsIgnoreCase("jobCategoryName"))
				sortColomnName = "Job_Category";
			else if(sortColomnName.equalsIgnoreCase("subjectname"))
				sortColomnName = "Job_Subject";
			else if(sortColomnName.equalsIgnoreCase("createdDateTime"))
				sortColomnName = "Application_Record_date";
			else if(sortColomnName.equalsIgnoreCase("status"))
				sortColomnName = "Application_Status";
			else if(sortColomnName.equalsIgnoreCase("AmricanIndian"))
				sortColomnName = "Status_Life_Cycle_Status";
			else if(sortColomnName.equalsIgnoreCase("schoolname"))
				sortColomnName = "Hired_By_School";
			else if(sortColomnName.equalsIgnoreCase("coverletter"))
				sortColomnName = "Have_Cover_Letter";
			else if(sortColomnName.equalsIgnoreCase("isAffilated"))
				sortColomnName = "Is_Affilated";
			else if(sortColomnName.equalsIgnoreCase("lastactivity"))
				sortColomnName = "Last_Activity_On_Application";
			else if(sortColomnName.equalsIgnoreCase("lastActivityDate"))
				sortColomnName = "Last_Activity_Date";
			else if(sortColomnName.equalsIgnoreCase("demodatescheduled"))
				sortColomnName = "Demo_Date_Scheduled";
			else if(sortColomnName.equalsIgnoreCase("demodatecancelled"))
				sortColomnName = "Demo_Date_Cancelled";
           
           
			
          
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql="  select "+
			 "	jft.jobforteacherid as `Internal_Record_ID`, "+
			 "	jft.teacherid as `Internal_Teacher_ID`,  "+
			  "   jo.jobid as `Internal_Job_ID`,  "+
			 "	jo.jobtitle as `Job_Title`,  "+
			  "   jo.jobstartdate as `Job_Posted_Date`,  "+
			 "	ifnull(jo.jobenddate,'') as `Job_Post_End_Date`,  "+
			  "   ifnull(jcm.jobCategoryName,'') as `Job_Category`,  "+
			 "	ifnull(sjm.subjectname,'') as `Job_Subject`,  "+
			  "   jft.createdDateTime as `Application_Record_date`, "+
			 "	sm.status as `Application_Status`, "+
			 "	IF(jft.displaystatusid is null, pss.secondarystatusname,  "+
			 "	   IF(ps.secondarystatusname is null,  "+
			 "		  IF(sm.status = 'Completed', 'Available', sm.status),  "+
			 "		  ps.secondarystatusname)) as `Status_Life_Cycle_Status`,  "+
			 "	ifnull(schm.schoolname,'') as `Hired_By_School`, "+
			 "	case  "+
			 "		when jft.coverletter is null then 'No' else 'Yes' end as `Have_Cover_Letter`, "+
			 "	case  "+
			 "		when jft.isAffilated = 1 then 'Yes' else 'No' end as `Is_Affilated`,  "+
			 "	ifnull(jft.lastactivity,'') as `Last_Activity_On_Application`,  "+
			  "   ifnull(jft.lastActivityDate,'') as `Last_Activity_Date`, "+
			   "  ifnull(dcs1.demodate,'') as `Demo_Date_Scheduled`, "+
			 "	ifnull(dcs2.demodate,'') as `Demo_Date_Cancelled` "+
			 " from jobforteacher jft "+
			 "	left join joborder jo on jft.jobid=jo.jobid "+
			 "	left join schoolmaster schm on schm.schoolid=jft.hiredBySchool "+
			 "	left join jobcategorymaster jcm on jcm.jobCategoryId=jo.jobCategoryId "+
			 "	left join subjectmaster sjm on sjm.subjectid=jo.subjectId "+
			 "	left join statusmaster sm on sm.statusid=jft.status "+
			 "	left join secondarystatus ps on jft.displaystatusId =ps.parentStatusId "+
			 "		and ps.districtid = jo.districtid "+
			 "		and ps.jobcategoryid = jo.jobcategoryid "+
			 "	left join secondarystatus pss on jft.displaysecondarystatusid=pss.secondarystatusid  "+
			 "		and pss.districtid = jo.districtid "+
			 "		and pss.jobcategoryid = jo.jobcategoryid "+
			 "	left join democlassschedule dcs1 on dcs1.teacherid=jft.teacherid  "+
			 "		and dcs1.jobid=jft.jobid and dcs1.demostatus!='cancelled' and dcs1.demoId= "+
			 "					(select max(demoid) from democlassschedule where dcs1.teacherid=teacherid  "+
			 "						and dcs1.jobid=jobid and demostatus!='cancelled') "+
			 "	left join democlassschedule dcs2 on dcs2.teacherid=jft.teacherid and dcs2.demoId= "+
			 "					(select max(demoid) from democlassschedule where dcs2.teacherid=teacherid  "+
			 "						and dcs2.jobid=jobid and demostatus='cancelled') "+
			 "		and dcs2.jobid=jft.jobid and dcs2.demostatus='cancelled' "+
			 " where jo.districtid='7800038' and jft.status!='8' "+
		""+orderby;
			 
		 
		 
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	
		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}
	/**************/
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestapplicationRun() 
	{
		System.out.println("Dao method call from ajax for schduler");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
          
			String orderby = " order by Internal_Record_ID ";
			
			 sql="  select "+
			 "	jft.jobforteacherid as `Internal_Record_ID`, "+
			 "	jft.teacherid as `Internal_Teacher_ID`,  "+
			  "   jo.jobid as `Internal_Job_ID`,  "+
			 "	jo.jobtitle as `Job_Title`,  "+
			  "   jo.jobstartdate as `Job_Posted_Date`,  "+
			 "	ifnull(jo.jobenddate,'') as `Job_Post_End_Date`,  "+
			  "   ifnull(jcm.jobCategoryName,'') as `Job_Category`,  "+
			 "	ifnull(sjm.subjectname,'') as `Job_Subject`,  "+
			  "   jft.createdDateTime as `Application_Record_date`, "+
			 "	sm.status as `Application_Status`, "+
			 "	IF(jft.displaystatusid is null, pss.secondarystatusname,  "+
			 "	   IF(ps.secondarystatusname is null,  "+
			 "		  IF(sm.status = 'Completed', 'Available', sm.status),  "+
			 "		  ps.secondarystatusname)) as `Status_Life_Cycle_Status`,  "+
			 "	ifnull(schm.schoolname,'') as `Hired_By_School`, "+
			 "	case  "+
			 "		when jft.coverletter is null then 'No' else 'Yes' end as `Have_Cover_Letter`, "+
			 "	case  "+
			 "		when jft.isAffilated = 1 then 'Yes' else 'No' end as `Is_Affilated`,  "+
			 "	ifnull(jft.lastactivity,'') as `Last_Activity_On_Application`,  "+
			  "   ifnull(jft.lastActivityDate,'') as `Last_Activity_Date`, "+
			   "  ifnull(dcs1.demodate,'') as `Demo_Date_Scheduled`, "+
			 "	ifnull(dcs2.demodate,'') as `Demo_Date_Cancelled` "+
			 " from jobforteacher jft "+
			 "	left join joborder jo on jft.jobid=jo.jobid "+
			 "	left join schoolmaster schm on schm.schoolid=jft.hiredBySchool "+
			 "	left join jobcategorymaster jcm on jcm.jobCategoryId=jo.jobCategoryId "+
			 "	left join subjectmaster sjm on sjm.subjectid=jo.subjectId "+
			 "	left join statusmaster sm on sm.statusid=jft.status "+
			 "	left join secondarystatus ps on jft.displaystatusId =ps.parentStatusId "+
			 "		and ps.districtid = jo.districtid "+
			 "		and ps.jobcategoryid = jo.jobcategoryid "+
			 "	left join secondarystatus pss on jft.displaysecondarystatusid=pss.secondarystatusid  "+
			 "		and pss.districtid = jo.districtid "+
			 "		and pss.jobcategoryid = jo.jobcategoryid "+
			 "	left join democlassschedule dcs1 on dcs1.teacherid=jft.teacherid  "+
			 "		and dcs1.jobid=jft.jobid and dcs1.demostatus!='cancelled' and dcs1.demoId= "+
			 "					(select max(demoid) from democlassschedule where dcs1.teacherid=teacherid  "+
			 "						and dcs1.jobid=jobid and demostatus!='cancelled') "+
			 "	left join democlassschedule dcs2 on dcs2.teacherid=jft.teacherid and dcs2.demoId= "+
			 "					(select max(demoid) from democlassschedule where dcs2.teacherid=teacherid  "+
			 "						and dcs2.jobid=jobid and demostatus='cancelled') "+
			 "		and dcs2.jobid=jft.jobid and dcs2.demostatus='cancelled' "+
			 " where jo.districtid='7800038' and jft.status!='8' "+
		""+orderby;
			 
		 
		 
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	
		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			System.out.println("return list size======"+lst.size());
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
		}
		return null;
	}
	
	/*********nobleeeeeeee************/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestcandidate(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName) 
	{
		PrintOnConsole.getJFTPrint("JFT:101");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
          
			
           if(sortColomnName.equalsIgnoreCase("IntenalTeacherId"))
				sortColomnName = "Internal_Teacher_ID";
			else if(sortColomnName.equalsIgnoreCase("fname"))
				sortColomnName = "Candidate_First_Name";
			else if(sortColomnName.equalsIgnoreCase("lname"))
				sortColomnName = "Candidate_Last_Name";
			else if(sortColomnName.equalsIgnoreCase("mname"))
				sortColomnName = "Candidate_Middle_Name";
			else if(sortColomnName.equalsIgnoreCase("email"))
				sortColomnName = "Teacher_Email";
			else if(sortColomnName.equalsIgnoreCase("pno"))
				sortColomnName = "Phone_Number";
			else if(sortColomnName.equalsIgnoreCase("mno"))
				sortColomnName = "Mobile_Number";
			else if(sortColomnName.equalsIgnoreCase("add1"))
				sortColomnName = "Address1";
			else if(sortColomnName.equalsIgnoreCase("add2"))
				sortColomnName = "Address2";
			else if(sortColomnName.equalsIgnoreCase("city"))
				sortColomnName = "City";
			else if(sortColomnName.equalsIgnoreCase("state"))
				sortColomnName = "State";
			else if(sortColomnName.equalsIgnoreCase("zip"))
				sortColomnName = "Zip_Code";
			else if(sortColomnName.equalsIgnoreCase("country"))
				sortColomnName = "Country";
			else if(sortColomnName.equalsIgnoreCase("gender"))
				sortColomnName = "Gender";
			else if(sortColomnName.equalsIgnoreCase("race"))
				sortColomnName = "Race";
			else if(sortColomnName.equalsIgnoreCase("ic"))
				sortColomnName = "Internal_Candidate";
			
          
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql=" Select "+
			 "	x.`Internal_Teacher_ID`, `Candidate_First_Name`, `Candidate_Last_Name`,  `Candidate_Middle_Name`, `Teacher_Email`,  "+
			 "	`Phone_Number`, `Mobile_Number`, `Address1`, `Address2`, `City`, `State`, `Zip_Code`, `Country`,  "+
			 "	`Gender`,  "+
			 "    case  "+
			 "		when `Race`='Asian' then 'Asian or Pacific Islander' "+
			 "		when `Race` like 'Black/%' then 'Black or African American' "+
			 "		when `Race` like 'White' then 'White/Not Hispanic origin' "+
			 "		when `Race` like 'Hispanic' then 'Hispanic/Latino'         "+
			 "		else `Race` "+
			 "    end as `Race`,  "+
			  "   `Internal_Candidate`, `Record_Status`, `District_Name`, `Affidavit_Accepted`, `Years_of_Teaching`,  "+
			 "	`Year_of_National_Board_Certified`, `Teach_for_America`,  `Year_at_TFA`,  `TFA_Region`, `EPI_Norm_Score`, "+
			 "	`Attitudinal_Factors_Score`,  "+
			  "   `Cognitive_Ability_Score`,  "+
			  "   `Teaching_Skills_Score`, "+
			 "	ifnull(`A_Score`,'') as `Achievement_Score`,  "+
			 "	ifnull(`L_R_Score`,'') as `L_R_Score`,   "+
			 "    ifnull(`MA_Score`, '') as `MA_Score`, "+
			 "    ifnull(`NF_Score`,'') as `NF_Score`, "+
			 "	`PNQ`,  "+
			 "    `Other_Languages`,  "+
			 "    `Can_Sub`,  "+
			 "	ifnull(`Doctorates_Year`,'') as `Doctorates_Year`,  "+
			 "    ifnull(`Doctorates_Degree`,'') as `Doctorates_Degree`,  "+
			 "    ifnull(`Doctorates_School`,'') as `Doctorates_School`,  "+
			 "    ifnull(`Doctorates_GPA`,'') as `Doctorates_GPA`,  "+
			 "	ifnull(`Doctorates_field`,'') as `Doctorates_field`, "+
			 "    ifnull(`Doctorates_Transcript`,'') as `Doctorates_Transcript`, "+
			 "	ifnull(`Masters_Year`,'') as `Masters_Year`,  "+
			 "    ifnull(`Masters_Degree`,'') as `Masters_Degree`,  "+
			 "    ifnull(`Masters_School`,'') as `Masters_School`,  "+
			 "    ifnull(`Masters_GPA`,'') as `Masters_GPA`,  "+
			 "	ifnull(`Masters_field`,'') as `Masters_field`, "+
			 "    ifnull(`Masters_Transcript`,'') as `Masters_Transcript`, "+
			 "    ifnull(`Bachelors_Year`,'') as `Bachelors_Year`,  "+
			 "	ifnull(`Bachelors_Degree`,'') as `Bachelors_Degree`,  "+
			 "    ifnull(`Bachelors_School`,'') as `Bachelors_School`,  "+
			 "    ifnull(`Bachelors_GPA`,'') as `Bachelors_GPA`, "+
			 "	ifnull(`Bachelors_field`,'') as `Bachelors_field`, "+
			 "    ifnull(`Bachelors_Transcript`,'') as `Bachelors_Transcript`,  "+
			 "    ifnull(`Associates_Year`,'') as `Associates_Year`,  "+
			 "	ifnull(`Associates_Degree`,'') as `Associates_Degree`,  "+
			 "    ifnull(`Associates_School`,'') as `Associates_School`,  "+
			 "    ifnull(`Associates_GPA`,'') as `Associates_GPA`,  "+
			 "	ifnull(`Associates_field`,'') as `Associates_field`, "+
			 "    ifnull(`Associates_Transcript`,'') as `Associates_Transcript` "+
			 " from "+
			 "	(select  "+
			 "		t.teacherid `Internal_Teacher_ID`, "+
			 "		t.firstName `Candidate_First_Name`,  "+
			 "		t.lastName `Candidate_Last_Name`,   "+
			 "		ifnull(tp.middleName,'') `Candidate_Middle_Name`, "+
			 "		t.emailAddress `Teacher_Email`,  "+
			 "		ifnull(tp.phonenumber,'') `Phone_Number`, "+
			 "		ifnull(tp.mobileNumber,'') `Mobile_Number`, "+
			 "		ifnull(tp.addressline1,'') `Address1`,  "+
			 "		ifnull(tp.addressline2,'') `Address2`,  "+
			 "		ifnull(cmtp.cityName,'') `City`,  "+
			 "		ifnull(smtp.stateShortName,'') `State`, "+
			 "		ifnull(tp.zipcode,'') `Zip_Code`,  "+
			 "		ifnull(ctrytp.name,'') `Country`, "+
			 "		ifnull(gm.genderName,'') `Gender`,  "+
			  "       ifnull(case when tp.ethnicityId=1 then 'Hispanic/Latino' else "+
			 "			if (tp.ethnicOriginId in (3,7,8), 'Hispanic/Latino', "+
			 "				case "+
			 "					when length(tp.raceid)>2 then 'Multi-Racial' "+
			 "					when length(tp.raceid)<=2 then rm.raceName      "+      
			 "					when tp.raceid is null and tp.ethnicOriginId is not null then eom.ethnicOriginName "+
			 "					when tp.raceid is null and tp.ethnicOriginId is null and tp.ethnicityId is not null then em.ethnicityName "+
			 "				end "+
			 "			) "+
			 "		end,'') as `Race`, "+
			 "		case when ta.affidavitAccepted ='1' then 'Yes' else 'No' end as `Affidavit_Accepted`, "+
			 "		ifnull(te.expcertteachertraining,'') `Years_of_Teaching`,  "+
			 "		ifnull(te.nationalboardcertyear,'') `Year_of_National_Board_Certified`, "+
			 "		ifnull(tfam.tfaAffiliateName,'') `Teach_for_America`,  "+
			 "		ifnull(te.corpsyear,'') `Year_at_TFA`,  "+
			 "		ifnull(tfa.tfaRegionName,'') `TFA_Region`, "+
			 "		ifnull(epi.teachernormscore,'') `EPI_Norm_Score`, "+
			 "		ifnull(adsAT.normscore,'') as `Attitudinal_Factors_Score`, "+
			 "		ifnull(adsCA.normscore,'') as `Cognitive_Ability_Score`, "+
			 "		ifnull(adsTS.normscore,'') as `Teaching_Skills_Score`, "+
			 "		case "+
			 "			when internalTransferCandidate='0' then 'No' "+
			 "			else 'Yes' end as `Internal_Candidate`,  "+
			 "		case "+
			 "			when t.status='A' then 'Active' else 'Inactive' end as `Record_Status`, "+
			 "			d.districtName `District_Name`, "+
			 "		case "+
			 "			when te.potentialQuality = '0' then 'Not Selected' "+
			 "			when te.potentialQuality = '1' then 'Yes' "+
			 "			when te.potentialQuality = '2'  then 'No' "+
			 "			else 'Pre Historical' end as  `PNQ`, "+
			 "		case  "+
			 "			when te.knowOtherlanguages=1 then 'Yes' else 'No' end as `Other_Languages`, "+
			 "		case  "+
			 "			when te.canServeAsSubTeacher = 1 then 'Yes' else 'No' end as `Can_Sub`, "+
			 "		ifnull(academicAchievementScore,'') as `A_Score`, "+
			 "		ifnull(leadershipAchievementScore,'') as `L_R_Score`,  "+
			  "       ifnull(dspanf.sliderScore,'') as `NF_Score`,  "+
			  "       ifnull(dspama.sliderScore,'') as `MA_Score` "+
			 "	from jobforteacher jt "+
			 "		left join teacherdetail t on jt.teacherid=t.teacherid "+
			 "		left join teacherpersonalinfo tp on t.teacherid=tp.teacherid "+
			 "		left join gendermaster gm on tp.genderId=gm.genderId "+
			 "		left join racemaster rm on tp.raceId=rm.raceId "+
			  "       left join ethnicoriginmaster eom on eom.ethnicOriginId=tp.ethnicOriginId "+
			 "        left join ethinicitymaster em on em.ethnicityId=tp.ethnicityId "+
			 "		left join joborder jo on jt.jobid=jo.jobid "+
			 "		left join districtmaster d on jo.districtid=d.districtid and d.status='A' "+
			 "		left join statusmaster sm on sm.statusid=jt.status "+
			 "		left join teachernormscore epi on t.teacherid=epi.teacherid "+
			 "		left join statemaster smtp on tp.stateid=smtp.stateid "+
			 "		left join citymaster cmtp on tp.cityid=cmtp.cityid "+
			 "		left join countrymaster ctrytp on tp.countryId=ctrytp.countryId "+
			 "		left join assessmentdomainscore adsAT on epi.teacherid=adsAT.teacherid and adsAT.domainid=1  "+
			 "		left join assessmentdomainscore adsTS on epi.teacherid=adsTS.teacherid and adsTS.domainid=3  "+
			 "		left join assessmentdomainscore adsCA on epi.teacherid=adsCA.teacherid and adsCA.domainid=2 "+
			 "		left join teacherexperience te on t.teacherid=te.teacherid "+
			 "		left join tfaregionmaster tfa on te.tfaRegionId=tfa.tfaregionID "+
			 "		left join teacheraffidavit ta on ta.teacherid=t.teacherid  "+
			 "					and ta.affidavitid=(select min(affidavitid) min_aid from teacheraffidavit where t.teacherid=teacherid) "+
			 "		left join tfaaffiliatemaster as tfam on tfam.tfaAffiliateId=te.tfaAffiliateId "+
			  "       left join teacherachievementscore as tas on tas.teacherid=t.teacherid "+
			  "                                           and tas.teacherAchievementScoreId=(select max(teacherAchievementScoreId) lastscore "+
			 "												from teacherachievementscore  where tas.teacherid=teacherid) "+
			 "		left join districtspecificportfolioanswers as dspanf on dspanf.teacherid=t.teacherid  "+
			 "											and dspanf.questionid in (3) and dspanf.sliderScore is not null "+
			 "		left join districtspecificportfolioanswers as dspama on dspama.teacherid=t.teacherid  "+
			 "											and dspama.questionid in (4) and dspama.sliderScore is not null "+
			 "		inner join (select min(jobforteacherid) min_jid  "+
			 "					from jobforteacher  "+
			 "						inner join joborder on joborder.jobid=jobforteacher.jobid  "+
			 "					where jobforteacher.status != '8'  "+
			 "						and joborder.districtid='7800038'  "+
			 "					group by teacherid) A on min_jid = jt.jobforteacherid "+
			 "	) as x  "+
			 "	, "+
			 "	(select  jt.teacherid `Internal_Teacher_ID`,  "+
			 "		max(leftinYear), "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Doctorates_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_Transcript, "+
			 "			group_concat(case "+
			 "				when dm.degreeType='M'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Masters_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Masters_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Masters_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Masters_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Masters_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Masters_Transcript, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B' "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Bachelors_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_Transcript, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Associates_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Associates_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Associates_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Associates_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Associates_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Associates_Transcript "+
			 "		from jobforteacher jt "+
			 "			left join joborder jo on jt.jobid=jo.jobid "+
			 "			left join teacheracademics ta on ta.teacherid=jt.teacherid "+
			 "			left join degreemaster dm on ta.degreeId=dm.degreeId "+
			 "			left join universitymaster um on ta.universityId=um.universityId "+
			 "			left join fieldofstudymaster fsm on ta.fieldId=fsm.fieldId "+
			 "			inner join (select min(jobforteacherid) min_jid  "+
			 "							from jobforteacher  "+
			 "								inner join joborder on joborder.jobid=jobforteacher.jobid  "+
			 "							where jobforteacher.status != '8'  "+
			 "								and joborder.districtid='7800038'  "+
			 "							group by teacherid) A on min_jid = jt.jobforteacherid "+
			 "		group by jt.teacherId "+
			 "	) as z  "+
			 "	where x.Internal_Teacher_ID=z.Internal_Teacher_ID "+
			 ""+orderby;
			 
		 
		 
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	
		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}
	/**********************/
	/*******csv schduler for noble candidate*********/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestCandidateRun() 
	{
		PrintOnConsole.getJFTPrint("JFT:102");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
			String orderby = " order by Internal_Teacher_ID";
			
			 sql=" Select "+
			 "	x.`Internal_Teacher_ID`, `Candidate_First_Name`, `Candidate_Last_Name`,  `Candidate_Middle_Name`, `Teacher_Email`,  "+
			 "	`Phone_Number`, `Mobile_Number`, `Address1`, `Address2`, `City`, `State`, `Zip_Code`, `Country`,  "+
			 "	`Gender`,  "+
			 "    case  "+
			 "		when `Race`='Asian' then 'Asian or Pacific Islander' "+
			 "		when `Race` like 'Black/%' then 'Black or African American' "+
			 "		when `Race` like 'White' then 'White/Not Hispanic origin' "+
			 "		when `Race` like 'Hispanic' then 'Hispanic/Latino'         "+
			 "		else `Race` "+
			 "    end as `Race`,  "+
			  "   `Internal_Candidate`, `Record_Status`, `District_Name`, `Affidavit_Accepted`, `Years_of_Teaching`,  "+
			 "	`Year_of_National_Board_Certified`, `Teach_for_America`,  `Year_at_TFA`,  `TFA_Region`, `EPI_Norm_Score`, "+
			 "	`Attitudinal_Factors_Score`,  "+
			  "   `Cognitive_Ability_Score`,  "+
			  "   `Teaching_Skills_Score`, "+
			 "	ifnull(`A_Score`,'') as `Achievement_Score`,  "+
			 "	ifnull(`L_R_Score`,'') as `L_R_Score`,   "+
			 "    ifnull(`MA_Score`, '') as `MA_Score`, "+
			 "    ifnull(`NF_Score`,'') as `NF_Score`, "+
			 "	`PNQ`,  "+
			 "    `Other_Languages`,  "+
			 "    `Can_Sub`,  "+
			 "	ifnull(`Doctorates_Year`,'') as `Doctorates_Year`,  "+
			 "    ifnull(`Doctorates_Degree`,'') as `Doctorates_Degree`,  "+
			 "    ifnull(`Doctorates_School`,'') as `Doctorates_School`,  "+
			 "    ifnull(`Doctorates_GPA`,'') as `Doctorates_GPA`,  "+
			 "	ifnull(`Doctorates_field`,'') as `Doctorates_field`, "+
			 "    ifnull(`Doctorates_Transcript`,'') as `Doctorates_Transcript`, "+
			 "	ifnull(`Masters_Year`,'') as `Masters_Year`,  "+
			 "    ifnull(`Masters_Degree`,'') as `Masters_Degree`,  "+
			 "    ifnull(`Masters_School`,'') as `Masters_School`,  "+
			 "    ifnull(`Masters_GPA`,'') as `Masters_GPA`,  "+
			 "	ifnull(`Masters_field`,'') as `Masters_field`, "+
			 "    ifnull(`Masters_Transcript`,'') as `Masters_Transcript`, "+
			 "    ifnull(`Bachelors_Year`,'') as `Bachelors_Year`,  "+
			 "	ifnull(`Bachelors_Degree`,'') as `Bachelors_Degree`,  "+
			 "    ifnull(`Bachelors_School`,'') as `Bachelors_School`,  "+
			 "    ifnull(`Bachelors_GPA`,'') as `Bachelors_GPA`, "+
			 "	ifnull(`Bachelors_field`,'') as `Bachelors_field`, "+
			 "    ifnull(`Bachelors_Transcript`,'') as `Bachelors_Transcript`,  "+
			 "    ifnull(`Associates_Year`,'') as `Associates_Year`,  "+
			 "	ifnull(`Associates_Degree`,'') as `Associates_Degree`,  "+
			 "    ifnull(`Associates_School`,'') as `Associates_School`,  "+
			 "    ifnull(`Associates_GPA`,'') as `Associates_GPA`,  "+
			 "	ifnull(`Associates_field`,'') as `Associates_field`, "+
			 "    ifnull(`Associates_Transcript`,'') as `Associates_Transcript` "+
			 " from "+
			 "	(select  "+
			 "		t.teacherid `Internal_Teacher_ID`, "+
			 "		t.firstName `Candidate_First_Name`,  "+
			 "		t.lastName `Candidate_Last_Name`,   "+
			 "		ifnull(tp.middleName,'') `Candidate_Middle_Name`, "+
			 "		t.emailAddress `Teacher_Email`,  "+
			 "		ifnull(tp.phonenumber,'') `Phone_Number`, "+
			 "		ifnull(tp.mobileNumber,'') `Mobile_Number`, "+
			 "		ifnull(tp.addressline1,'') `Address1`,  "+
			 "		ifnull(tp.addressline2,'') `Address2`,  "+
			 "		ifnull(cmtp.cityName,'') `City`,  "+
			 "		ifnull(smtp.stateShortName,'') `State`, "+
			 "		ifnull(tp.zipcode,'') `Zip_Code`,  "+
			 "		ifnull(ctrytp.name,'') `Country`, "+
			 "		ifnull(gm.genderName,'') `Gender`,  "+
			 "       ifnull(case when tp.ethnicityId=1 then 'Hispanic/Latino' else "+
			 "			if (tp.ethnicOriginId in (3,7,8), 'Hispanic/Latino', "+
			 "				case "+
			 "					when length(tp.raceid)>2 then 'Multi-Racial' "+
			 "					when length(tp.raceid)<=2 then rm.raceName      "+      
			 "					when tp.raceid is null and tp.ethnicOriginId is not null then eom.ethnicOriginName "+
			 "					when tp.raceid is null and tp.ethnicOriginId is null and tp.ethnicityId is not null then em.ethnicityName "+
			 "				end "+
			 "			) "+
			 "		end,'') as `Race`, "+
			 "		case when ta.affidavitAccepted ='1' then 'Yes' else 'No' end as `Affidavit_Accepted`, "+
			 "		ifnull(te.expcertteachertraining,'') `Years_of_Teaching`,  "+
			 "		ifnull(te.nationalboardcertyear,'') `Year_of_National_Board_Certified`, "+
			 "		ifnull(tfam.tfaAffiliateName,'') `Teach_for_America`,  "+
			 "		ifnull(te.corpsyear,'') `Year_at_TFA`,  "+
			 "		ifnull(tfa.tfaRegionName,'') `TFA_Region`, "+
			 "		ifnull(epi.teachernormscore,'') `EPI_Norm_Score`, "+
			 "		ifnull(adsAT.normscore,'') as `Attitudinal_Factors_Score`, "+
			 "		ifnull(adsCA.normscore,'') as `Cognitive_Ability_Score`, "+
			 "		ifnull(adsTS.normscore,'') as `Teaching_Skills_Score`, "+
			 "		case "+
			 "			when internalTransferCandidate='0' then 'No' "+
			 "			else 'Yes' end as `Internal_Candidate`, "+ 
			 "		case "+
			 "			when t.status='A' then 'Active' else 'Inactive' end as `Record_Status`, "+
			 "			d.districtName `District_Name`, "+
			 "		case "+
			 "			when te.potentialQuality = '0' then 'Not Selected' "+
			 "			when te.potentialQuality = '1' then 'Yes' "+
			 "			when te.potentialQuality = '2'  then 'No' "+
			 "			else 'Pre Historical' end as  `PNQ`, "+
			 "		case  "+
			 "			when te.knowOtherlanguages=1 then 'Yes' else 'No' end as `Other_Languages`, "+
			 "		case  "+
			 "			when te.canServeAsSubTeacher = 1 then 'Yes' else 'No' end as `Can_Sub`, "+
			 "		ifnull(academicAchievementScore,'') as `A_Score`, "+
			 "		ifnull(leadershipAchievementScore,'') as `L_R_Score`,  "+
			  "       ifnull(dspanf.sliderScore,'') as `NF_Score`,  "+
			  "       ifnull(dspama.sliderScore,'') as `MA_Score` "+
			 "	from jobforteacher jt "+
			 "		left join teacherdetail t on jt.teacherid=t.teacherid "+
			 "		left join teacherpersonalinfo tp on t.teacherid=tp.teacherid "+
			 "		left join gendermaster gm on tp.genderId=gm.genderId "+
			 "		left join racemaster rm on tp.raceId=rm.raceId "+
			  "       left join ethnicoriginmaster eom on eom.ethnicOriginId=tp.ethnicOriginId "+
			 "        left join ethinicitymaster em on em.ethnicityId=tp.ethnicityId "+
			 "		left join joborder jo on jt.jobid=jo.jobid "+
			 "		left join districtmaster d on jo.districtid=d.districtid and d.status='A' "+
			 "		left join statusmaster sm on sm.statusid=jt.status "+
			 "		left join teachernormscore epi on t.teacherid=epi.teacherid "+
			 "		left join statemaster smtp on tp.stateid=smtp.stateid "+
			 "		left join citymaster cmtp on tp.cityid=cmtp.cityid "+
			 "		left join countrymaster ctrytp on tp.countryId=ctrytp.countryId "+
			 "		left join assessmentdomainscore adsAT on epi.teacherid=adsAT.teacherid and adsAT.domainid=1  "+
			 "		left join assessmentdomainscore adsTS on epi.teacherid=adsTS.teacherid and adsTS.domainid=3  "+
			 "		left join assessmentdomainscore adsCA on epi.teacherid=adsCA.teacherid and adsCA.domainid=2 "+
			 "		left join teacherexperience te on t.teacherid=te.teacherid "+
			 "		left join tfaregionmaster tfa on te.tfaRegionId=tfa.tfaregionID "+
			 "		left join teacheraffidavit ta on ta.teacherid=t.teacherid  "+
			 "					and ta.affidavitid=(select min(affidavitid) min_aid from teacheraffidavit where t.teacherid=teacherid) "+
			 "		left join tfaaffiliatemaster as tfam on tfam.tfaAffiliateId=te.tfaAffiliateId "+
			  "       left join teacherachievementscore as tas on tas.teacherid=t.teacherid "+
			  "                                           and tas.teacherAchievementScoreId=(select max(teacherAchievementScoreId) lastscore "+
			 "												from teacherachievementscore  where tas.teacherid=teacherid) "+
			 "		left join districtspecificportfolioanswers as dspanf on dspanf.teacherid=t.teacherid  "+
			 "											and dspanf.questionid in (3) and dspanf.sliderScore is not null "+
			 "		left join districtspecificportfolioanswers as dspama on dspama.teacherid=t.teacherid  "+
			 "											and dspama.questionid in (4) and dspama.sliderScore is not null "+
			 "		inner join (select min(jobforteacherid) min_jid  "+
			 "					from jobforteacher  "+
			 "						inner join joborder on joborder.jobid=jobforteacher.jobid  "+
			 "					where jobforteacher.status != '8'  "+
			 "						and joborder.districtid='7800038'  "+
			 "					group by teacherid) A on min_jid = jt.jobforteacherid "+
			 "	) as x  "+
			 "	, "+
			 "	(select  jt.teacherid `Internal_Teacher_ID`,  "+
			 "		max(leftinYear), "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Doctorates_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_Transcript, "+
			 "			group_concat(case "+
			 "				when dm.degreeType='M'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Masters_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Masters_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Masters_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Masters_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Masters_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Masters_Transcript, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B' "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Bachelors_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_Transcript, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Associates_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Associates_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Associates_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Associates_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Associates_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Associates_Transcript "+
			 "		from jobforteacher jt "+
			 "			left join joborder jo on jt.jobid=jo.jobid "+
			 "			left join teacheracademics ta on ta.teacherid=jt.teacherid "+
			 "			left join degreemaster dm on ta.degreeId=dm.degreeId "+
			 "			left join universitymaster um on ta.universityId=um.universityId "+
			 "			left join fieldofstudymaster fsm on ta.fieldId=fsm.fieldId "+
			 "			inner join (select min(jobforteacherid) min_jid  "+
			 "							from jobforteacher  "+
			 "								inner join joborder on joborder.jobid=jobforteacher.jobid  "+
			 "							where jobforteacher.status != '8'  "+
			 "								and joborder.districtid='7800038'  "+
			 "							group by teacherid) A on min_jid = jt.jobforteacherid "+
			 "		group by jt.teacherId "+
			 "	) as z  "+
			 "	where x.Internal_Teacher_ID=z.Internal_Teacher_ID "+
			 ""+orderby;
			
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
		ResultSet rs=ps.executeQuery();
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}
	/***************/
	

	@Transactional(readOnly=false)
	public List<JobForTeacher> verifyJobByDistrictId(TeacherDetail teacherDetail, DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:103");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			System.out.println(teacherDetail.getTeacherId()+"\taaaaaaaaaa"+districtMaster.getDistrictId());
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			//criteria.setFirstResult(0);
			//criteria.setMaxResults(1);
			lstJobForTeacher = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findbyJobId(JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session=getSession();
			return session.createCriteria(getPersistentClass()).add(Restrictions.eq("jobId", jobOrder))
			//.setProjection(Projections.projectionList().add(Projections.property("jobForTeacherId")))
			.list();
			
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	
	

	@Transactional(readOnly=false)
	public List<JobForTeacher> findByTeacherDistrictAndJobCategory(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCateg) 
	{	
		PrintOnConsole.getJFTPrint("JFT:104");
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			}
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster", jobCateg));
//			if(jobCateg!=null)
//			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster", jobCateg));

			criteria.addOrder(Order.desc("createdDateTime"));
			lstJobForTeachers = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}





	@Transactional(readOnly=false)
	public List<TeacherDetail> getAllQQFinalizedTeacherListByJobCategory(List<TeacherDetail> lstTeacherDetails,JobCategoryMaster jobCat)
	{
		PrintOnConsole.getJFTPrint("JFT:105");
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
					criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster", jobCat));
				    criteria.setProjection(Projections.groupProperty("teacherId"));
				    lstTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTforOfferReadystatusByJobList(List<JobOrder> jobOrderList,Order sortOrderStrVal,List<JobOrder> jobOrders, boolean sortingcheck)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			if(jobOrderList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.ne("status",statusMaster);
				Criterion criterion2 = Restrictions.in("jobId",jobOrderList);
				criteria.add(criterion1);
				criteria.add(criterion2);
				Criterion isnotNullCriterion    =Restrictions.isNotNull("offerReady");
				criteria.add(isnotNullCriterion);
				
				if(!sortingcheck){
				 criteria.addOrder(sortOrderStrVal);
				}
				if(jobOrders!=null && jobOrders.size()>0)
					criteria.add(Restrictions.in("jobId",jobOrders));
				
				lstJobForTeacher = criteria.list();
			}
			System.out.println("offerReadyofferReady :: "+lstJobForTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> hiredCandidateListWithAndWithoutDistrictByJobList(List<JobOrder> jobOrderList, int sortingcheck, Order sortOrderStrVal)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			//StatusMaster statusMaster = statusMasterDAO.findStatusByShortName("hird");
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion hiredcriterion = Restrictions.eq("status", statusMaster);
			criteria.add(hiredcriterion);
			Criterion criterion2 = Restrictions.in("jobId",jobOrderList);
			criteria.add(criterion2);
			if(sortingcheck==3){
				criteria.addOrder(sortOrderStrVal);
			}
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}
	public List<StatusMaster> getObjectStatus(List<Integer> statusMasterList){
		List<StatusMaster> statusMasters=new ArrayList<StatusMaster>();
		try{
			for(int i=0; i<statusMasterList.size();i++){
				StatusMaster statusMaster=new StatusMaster();
				statusMaster.setStatusId(statusMasterList.get(i));
				statusMasters.add(statusMaster);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return statusMasters;
	}

	@Transactional(readOnly=false)
    public List<String[]> getCandidateJobStatusReport(DistrictMaster districtMaster,List<String> queryList,String orderBy)
    {
    	  System.out.println(queryList.size()+"::::::::getCandidateJobStatusReport ::::"+orderBy);
    	  Session session = getSession();
          List<String[]> rows=new ArrayList<String[]>();
          Connection connection =null;
  		  try {
  		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
  		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
  		    connection = connectionProvider.getConnection();
	
  		      	   String query="";
		  		   for(String queryStr:queryList){
		          	  query+=" "+queryStr;
		           }
		  		   System.out.println("query:::::"+query);
	        	  String sql="select "+

	              "concat(td.firstName,' ', td.lastName,'\n(',td.emailAddress,')'),  "+

	              "dm.districtName, jo.jobTitle,  "+

	              "jo.jobStartDate, jo.jobEndDate,  "+

	              "jft.createdDateTime,  "+

	              "(select concat(IFNULL(tns.teacherNormScore,0),'##',tns.decileColor) from teachernormscore tns where tns.teacherId=jft.teacherId) normScore_colorCode,  "+

	              "case WHEN jft.displayStatusId=null   "+

	                "THEN (select sm.secondaryStatusName from  secondarystatus sm where sm.secondaryStatusId = jft.displaySecondaryStatusId)   "+

	                "ELSE (select  if(sm1.status='Completed','Available',sm1.status) from statusmaster sm1 where sm1.statusId = jft.displayStatusId)   "+

	              "END as jobStatus,  "+

	              "(select tshj.hiredByDate from teacherstatushistoryforjob tshj where tshj.teacherId=jft.teacherId and tshj.jobId=jft.jobId and tshj.status='A' ) hireDate,  "+

	              "IFNULL((select sm.schoolName from schoolmaster sm where sm.schoolId=jft.hiredBySchool),dm.districtName) schoolName,  "+

	              "(SELECT concat(jft1.jobId,'##',COUNT(*),'##',COUNT( IF( jft1.status NOT IN (select statusId from statusmaster sm where sm.statusShortName in ('dcln','icomp','vlt','widrw','hide','rem')), 1, NULL )),'##',COUNT( IF( jft1.status =6, 1, NULL ) )) from jobforteacher jft1 where jft1.jobId =jft.jobId and jft1.status!=8 group by jft1.jobId) allInfo_jobId_toCan_toAvl_toHir,  "+

	              "IF(jft.isAffilated=1, 'Yes', 'No') extOrInternal  "+

	              ",(select concat(tas.academicAchievementScore,'##',tas.academicAchievementMaxScore,'##',leadershipAchievementScore,'##',leadershipAchievementMaxScore) from teacherachievementscore tas where tas.teacherId=jft.teacherId and tas.districtId = dm.districtId) allInfo_TeacherAchievementScore  "+

	              ",(select concat(jwcts.jobWiseConsolidatedScore,'##',jwcts.jobWiseMaxScore) from jobwiseconsolidatedteacherscore jwcts where jwcts.teacherId=jft.teacherId and jwcts.jobId=jo.jobId) allInfo_JobWiseConsolidatedTeacherScore "+

	              "from jobforteacher jft,   "+

	              "teacherdetail td,   "+

	              "districtmaster dm,  "+

	              "joborder jo   "+

	              "where   "+

	              "jft.teacherId=td.teacherId "+

	              "and jft.jobId=jo.jobId   "+

	              "and jo.districtId=dm.districtId  and jft.status!=8   "+query+

	              " and jo.districtId= ? "+

	              " order by "+orderBy;

	
	          System.out.println("sql:::"+sql);
	          PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	          ps.setString(1,districtMaster.getDistrictId().toString());
	          //ps.setString(2,orderBy);
	
	  		  ResultSet rs=ps.executeQuery();
	  			if(rs.next()){				
	  				do{
	  					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
	  					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
	  					   {
	  						allInfo[i-1]=rs.getString(i);
	  					   }
	  					rows.add(allInfo);
	  					//System.out.println("allInfo::::"+allInfo[0]);
	  				}while(rs.next());
	  			}
	  			else{
	  				System.out.println("Record not found.");
	  			}
	  			return rows;
	         }catch (Exception e) {
                e.printStackTrace();
	         }finally{
	 			if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
          return rows;
    }
	@Transactional(readOnly=false)
    public List<String[]> getCandidateEEOCReport(DistrictMaster districtMaster,List<String> queryList,String orderBy)
    {
    	  System.out.println(queryList.size()+"::::::::getCandidateEEOCReport ::::"+orderBy);
    	  Session session = getSession();
          List<String[]> rows=new ArrayList<String[]>();
          Connection connection =null;
  		  try {
  		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
  		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
  		    connection = connectionProvider.getConnection();
	
  		      	   String query="";
		  		   for(String queryStr:queryList){
		          	  query+=" "+queryStr;
		           }
		  		   System.out.println("query:::::"+query);
	        	  String sql="select td.firstName as fName,IFNULL(td.lastName,'') as lName,td.emailAddress as email,raceName,gender,ethName,originName,this.jobId,td.teacherId as teacherId  from"+

	              " jobforteacher this inner join joborder jo on this.jobId=jo.jobId"+

	              " inner join districtmaster dm on jo.districtId=dm.districtId"+

	              " inner join teacherdetail td on this.teacherId=td.teacherId"+
	              
	              " left join( select tpi.teacherId teacherId1, case WHEN tpi.raceId is NULL THEN NULL"+
	              
	              " ELSE (select GROUP_CONCAT(rm.raceName) from racemaster rm where rm.raceId in(tpi.raceId))"+
	              
	              " END as raceName,gm.genderName as gender,em.ethnicityName as ethName,eom.ethnicOriginName as originName from teacherpersonalinfo tpi"+
	              
	              " left join gendermaster gm on tpi.genderId = gm.genderId"+
	              
	              " inner join ethinicitymaster em on tpi.ethnicityId=em.ethnicityId"+
	              
	              " inner join ethnicoriginmaster eom on tpi.ethnicOriginId=eom.ethnicOriginId"+
	              
	              " ) as tpInfo on tpInfo.teacherId1=td.teacherId"+
	              
	              
	              " where"+

	              " this.status!=8 and dm.districtId=? "+query+

	              "  group by this.teacherId order by "+orderBy;

	
	          System.out.println("sql>:::"+sql);
	          PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	          ps.setString(1,districtMaster.getDistrictId().toString());
	
	  		  ResultSet rs=ps.executeQuery();
	  			if(rs.next()){				
	  				do{
	  					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
	  					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
	  					   {
	  						allInfo[i-1]=rs.getString(i);
	  					   }
	  					rows.add(allInfo);
	  					//System.out.println("allInfo::::"+allInfo[0]);
	  				}while(rs.next());
	  			}
	  			else{
	  				System.out.println("Record not found.");
	  			}
	  			return rows;
	         }catch (Exception e) {
                e.printStackTrace();
	         }finally{
	 			if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
          return rows;
    }

    @Transactional
	public JobForTeacher saveJobForTeacher(JobForTeacher jobForTeacher,JobOrder jobOrder,TeacherDetail teacherDetail, String redirectedFromURL, String jobBoardReferralURL)
	{

		StatusMaster statusMaster = null; 
		
		if(jobForTeacher==null || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide")){
			
			if(jobForTeacher==null){
				jobForTeacher = new JobForTeacher();
				jobForTeacher.setTeacherId(teacherDetail);
				jobForTeacher.setJobId(jobOrder);
				jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
				jobForTeacher.setRedirectedFromURL(redirectedFromURL);
				jobForTeacher.setJobBoardReferralURL(jobBoardReferralURL);
				jobForTeacher.setCoverLetter("");
				jobForTeacher.setIsAffilated(0);
				jobForTeacher.setCreatedDateTime(new Date());
			}
			else{
				jobForTeacher.setUpdatedDate(new Date());
			}
			
			//statusMaster = statusMasterDAO.findStatusByShortName("icomp");
			statusMaster= WorkThreadServlet.statusMap.get("icomp");
			
			AssessmentDetail assessmentDetail = null;
			TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jobForTeacher.getTeacherId());
			if(!jobOrder.getJobCategoryMaster().getBaseStatus()){
				if(jobOrder.getIsJobAssessment()){
					List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
					if(lstAssessmentJobRelation.size()>0)
					{
						AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
						assessmentDetail = assessmentJobRelation.getAssessmentId();
						List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
						
						if(lstTeacherAssessmentStatus.size()>0)
						{						
							teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
							teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
							teacherAssessmentStatus.setJobOrder(jobOrder);
							teacherAssessmentStatus.setCreatedDateTime(new Date());
							teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);						
							statusMaster = teacherAssessmentStatus.getStatusMaster();						
						}
						else
						{
							statusMaster= WorkThreadServlet.statusMap.get("icomp");
						}					
					}
					else
					{
						statusMaster= WorkThreadServlet.statusMap.get("icomp");
					}
				}
				else{
					statusMaster= WorkThreadServlet.statusMap.get("comp");
				}
			}
			else if(jobOrder.getIsJobAssessment()){
				List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
				if(lstAssessmentJobRelation.size()>0)
				{
					AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
					assessmentDetail = assessmentJobRelation.getAssessmentId();
					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
					
					if(lstTeacherAssessmentStatus.size()>0)
					{					
						teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
						teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
						teacherAssessmentStatus.setJobOrder(jobOrder);
						teacherAssessmentStatus.setCreatedDateTime(new Date());
						teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);						
						statusMaster = teacherAssessmentStatus.getStatusMaster();						
					}
					else
					{
						statusMaster= WorkThreadServlet.statusMap.get("icomp");
					}					
				}
				else
				{
					statusMaster= WorkThreadServlet.statusMap.get("icomp");
				}
			}
			else{
				
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
				JobOrder jOrder = new JobOrder();
				jOrder.setJobId(0);
				teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),jOrder);
				if(teacherAssessmentStatusList.size()>0){
					teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
					statusMaster = teacherAssessmentStatus.getStatusMaster();
				}else{
					statusMaster= WorkThreadServlet.statusMap.get("icomp");
				}							
			}
			jobForTeacher.setStatus(statusMaster);
			jobForTeacher.setStatusMaster(statusMaster);
			//jobForTeacherDAO.makePersistent(jobForTeacher);
			makePersistent(jobForTeacher);
			return jobForTeacher;
		}
		
		return jobForTeacher;
	}
    
    @Transactional(readOnly=false)
    public boolean getJFTByTeachers(TeacherDetail teacherDetail)
    {
    	boolean isKellyOnly = true;
    	List<JobForTeacher> lstJobForTeacherKellyOnly= new ArrayList<JobForTeacher>();
    	List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    	String sqlNonKelly = "";
    	String sqlKellyOnly = "";
    	List<Object[]> rows1 = null;
    	List<Object[]> rows2 = null;
 		try 
		{
			sqlNonKelly = "select jobId from jobforteacher jft where teacherId="+teacherDetail.getTeacherId()+" and jft.jobId in (select jobId from joborder where headQuarterId is null)";
			sqlKellyOnly = "select jobId from jobforteacher jft where teacherId="+teacherDetail.getTeacherId()+" and jft.jobId in (select jobId from joborder where headQuarterId is not null)";
			//System.out.println("sqlNonKelly:: "+sqlNonKelly);
			//System.out.println("sqlKellyOnly:: "+sqlKellyOnly);
			Session session = getSession();
			Query queryNonKelly = session.createSQLQuery(sqlNonKelly);
			Query queryKellyOnly = session.createSQLQuery(sqlKellyOnly);
			
			rows1 = queryNonKelly.list();
			rows2 = queryKellyOnly.list();
			
			if(rows1!=null && rows1.size()>0 && rows2!=null && rows2.size()>0)
				isKellyOnly = false;
			else if(rows1!=null && rows1.size()==0 && rows2!=null && rows2.size()==0)
				isKellyOnly = false;
			else if(rows1!=null && rows1.size()>0 && rows2!=null && rows2.size()==0)
				isKellyOnly = false;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return isKellyOnly;
		}	
		//System.out.println("isKellyOnly::: "+ isKellyOnly);
		return isKellyOnly;
    }
     @Transactional(readOnly=false)
   public List<JobForTeacher> findAllTeacherByHBD(TeacherDetail teacherDetail,JobOrder jobOrder)
   {
   	List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
   	try 
   	{
   		
   			Session session = getSession();
   			Criteria criteria = session.createCriteria(getPersistentClass());
   			criteria.add(Restrictions.eq("teacherId",teacherDetail));
   			criteria.add(Restrictions.isNotNull("offerReady"));
   			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(jobOrder.getDistrictMaster()!=null){
				jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			}
   			lstJobForTeacher = criteria.list();
   	} 
   	catch (Exception e) {
   		e.printStackTrace();
   	}		
   	return lstJobForTeacher;
   }
   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndJobCategoryHBD(TeacherDetail teacherDetail,JobOrder jobOrder,String jobForTeacherId)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(jobOrder.getDistrictMaster()!=null){
				jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			}
			jobOrderSet.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));
			criteria.add(Restrictions.eq("teacherId",teacherDetail));
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			criteria.add(Restrictions.not(criterion2));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndDistrictHBD(TeacherDetail teacherDetail,JobOrder jobOrder,String jobForTeacherId)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(jobOrder.getDistrictMaster()!=null){
				jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			}
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndJobTitleHBD(TeacherDetail teacherDetail,JobOrder jobOrder,String jobForTeacherId,String jobTitle)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(jobOrder.getDistrictMaster()!=null){
				jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			}
			jobOrderSet.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster())).add(Restrictions.like("jobTitle","%"+jobTitle+"%"));
			criteria.add(Restrictions.eq("teacherId",teacherDetail));
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			criteria.add(Restrictions.not(criterion2));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

   
   @Transactional(readOnly=false)
   public   List <JobForTeacher>  getSPStatusByTeacher(Integer teacherId)
   {
      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
		try{
			
		      Session session = getSession();
		      
		      Criteria criteria = session.createCriteria(getPersistentClass());
		      Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
		      criteria.add(criterion);
		      Criteria c1 = criteria.createCriteria("jobId").add(Restrictions.eq("status","A"))
		      .createCriteria("jobCategoryMaster").add(Restrictions.eq("preHireSmartPractices", true))
		      //.createCriteria("districtMaster").add(Restrictions.eq("districtId", 5509600))
		      .setFirstResult(0)
		      .setMaxResults(1);
			
			lstJobForTeacher2 = criteria.list();
			}catch(Exception e){
			
				e.printStackTrace();
		}
		
	  return lstJobForTeacher2;
   }
   
   /*@Transactional(readOnly=false)
   public   List <JobForTeacher>  findAllAppliedJobsOfTeacherKelly(Integer teacherId)
   {
      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
		try{
			 Session session=getSession();
			 Criteria criteria=session.createCriteria(getPersistentClass());
			 lstJobForTeacher2= criteria.add(Restrictions.eq("teacherId.teacherId", teacherId))
			 .createCriteria("jobId").add(Restrictions.eq("status","A"))
			 .createCriteria("districtMaster").add(Restrictions.eq("districtId", 5509600))
			 .list();
			 
		}catch(Exception e){
			
				e.printStackTrace();
		}
		
	  return lstJobForTeacher2;
   }*/

   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherForKES(TeacherDetail teacherDetail,boolean flag)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			
			jobOrderSet.add(Restrictions.isNotNull("headQuarterMaster"));
			criteria.add(criterion1);
			if(flag)
				criteria.add(Restrictions.eq("statusMaster",statusMaster));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   
   
   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherNotKES(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			criteria.add(criterion1);
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
   	public  Map<Integer,Boolean> getJobForTeacherByTeachersList(List<TeacherDetail> lstTeacherDetails){
	   	List<String[]> lstJobForTeacher=new ArrayList<String[]>();
	    Map<Integer,Boolean> mapTeacherPreHire=new LinkedHashMap<Integer,Boolean>();
	   	if(lstTeacherDetails.size()>0){
		   	 for(TeacherDetail td:lstTeacherDetails){
		   		 mapTeacherPreHire.put(td.getTeacherId(), false);
		   	 }
	         try{
	        	 JobForTeacher jjj=null;
	               Session session = getSession();
	               Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo").createAlias("jo.jobCategoryMaster", "jc");             
	               criteria.setProjection( Projections.projectionList()
	                       .add( Projections.property("teacherId.teacherId"), "teachId" )
	                       .add( Projections.groupProperty("teacherId") )
	                );
	                criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
	                criteria.add(Restrictions.eq("jc.preHireSmartPractices",true));
	                lstJobForTeacher = criteria.list() ;
	               if(lstJobForTeacher.size()>0){
                      for (Iterator it = lstJobForTeacher.iterator(); it.hasNext();) {
                          Object[] row = (Object[]) it.next();                                                                 
                          mapTeacherPreHire.put(Integer.parseInt(row[0].toString()),true);
                      }   
	               }
	         }catch (Throwable e) {
	               e.printStackTrace();
	         }   
   	 }
   	 return mapTeacherPreHire;
   }
    
    @Transactional(readOnly=false)
	public List<JobForTeacher> getHQBRQQFinalizedNotesForTeacher(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster ,BranchMaster branchMaster)
	{
		List<JobForTeacher> lstjob=new ArrayList<JobForTeacher>();
		
		if(teacherDetail!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(teacherDetail!=null)
					criteria.add(Restrictions.eq("teacherId",teacherDetail));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    if(headQuarterMaster!=null)
				    	criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
				    else if(branchMaster!=null)
				    	criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
				    //criteria.setProjection(Projections.distinct(Projections.property("teacherId")));
				    lstjob =  criteria.list();
				    } 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstjob;
	}
    @Transactional(readOnly=false)
    public List<JobForTeacher> getJobForTeacherByTeachersListNew(List<Integer> statusIds,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
    {
    	 List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
         try{
               Session session = getSession();
               Criteria criteria = session.createCriteria(getPersistentClass());             
              
               Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
               if(entityID!=1){
            	 if(districtMaster!=null){
                       criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
                 }
                 if(headQuarterMaster!=null){
                     criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
                 }
                 if(branchMaster!=null){
                     criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
                 }
               }
               if(lstTeacherDetails.size()>0){
                   criteria.add(criterionTeacher);
                   criteria.createCriteria("status").add(Restrictions.in("statusId",statusIds));
                   lstJobForTeacher = criteria.list() ;
              }
              
         }catch (Exception e) {
               e.printStackTrace();
         }           
         return lstJobForTeacher;
    }
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplidJobsHQLByHBD(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,SchoolMaster schoolMaster,List<TeacherDetail> teacherDetails) 
	{
		Session session = getSession();
		String sql = "";
		if(branchMaster!=null){
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT(IF(jo.branchId="+branchMaster.getBranchId()+" and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}
		if(headQuarterMaster!=null){
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT(IF(jo.headQuarterId="+headQuarterMaster.getHeadQuarterId()+" and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}
		if(districtMaster!=null && schoolMaster==null)
		{
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT(IF(jo.districtId="+districtMaster.getDistrictId()+" and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}if(schoolMaster!=null)
		{
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT( IF( jo.jobId IN ( SELECT jobId FROM schoolinjoborder WHERE schoolId ="+schoolMaster.getSchoolId()+" ) and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}


		Query query = session.createSQLQuery(sql);
		String[] statuss = {"icomp","comp","hird","vlt","scomp","ecomp","vcomp","dcln","rem"};
		//List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);
		List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		query.setParameterList("teacherDetails", teacherDetails);
		query.setParameterList("statuss", statusMasters );

		List<Object[]> rows = query.list();
		return rows;

	}
    
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List getAppliedJobListByTeacher(int teacherId,int districtId,StatusMaster statusMaster) 
	{
		Session session = getSession();
		String sDistrictQuery="";
		if(districtId>0)
		{
			sDistrictQuery=" and districtId=:districtId ";
		}
		if(statusMaster!=null)
		{
			sDistrictQuery=sDistrictQuery+" and status=:status";
		}
		
		Query query = session.createSQLQuery(" select districtId,jobId,status,jobForTeacherId from jobforteacher where teacherId=:teacherId"+sDistrictQuery);
		query.setParameter("teacherId", teacherId);
		if(districtId>0)
		{
			query.setParameter("districtId", districtId);
		}
		
		if(statusMaster!=null)
		{
			query.setParameter("status", statusMaster.getStatusId());
		}
		
		List<Object[]> rows = query.list();
		return rows;
		
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> findByTeacherAndQuestionSet(TeacherDetail teacherDetail,QqQuestionSets questionSets) 
	{	
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			criteria.createCriteria("jobId").createCriteria("jobCategoryMaster").add(Restrictions.eq("questionSets",questionSets));
			criteria.addOrder(Order.desc("createdDateTime"));
			lstJobForTeachers = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}
    
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllHiredTeacher(DistrictMaster districtMaster) 
	{	
		//List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();
		List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			StatusMaster statusMasterHired = WorkThreadServlet.statusMap.get("hird");
			StatusMaster statusMasterTimed = WorkThreadServlet.statusMap.get("vlt");
			System.out.println(statusMasterHired.getStatusId()+":::::::::::::"+statusMasterTimed.getStatusId()+":::::::::::"+districtMaster.getDistrictId());
			Criterion criterion1 = Restrictions.eq("status",statusMasterHired);
			Criterion criterion2 = Restrictions.eq("status",statusMasterTimed);
			Criterion criterion3 = Restrictions.eq("districtId",districtMaster.getDistrictId());	
			Criterion criterionMix=Restrictions.or(criterion1, criterion2);
			
			criteria.add(criterionMix);
			criteria.add(criterion3);
			criteria.setProjection(Projections.groupProperty("teacherId"));
			lstTeacherDetails = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstTeacherDetails;
	}
    
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllUnhiredTeacher(DistrictMaster districtMaster,List<TeacherDetail> teacherDetail) 
	{	
		//List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();
		List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
		List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			String[] status = {"hird","vlt","hide"};
			//lstStatusMasters = statusMasterDAO.findStatusByShortNames(status);
			lstStatusMasters = Utility.getStaticMasters(status);
			
			Criterion criterion1 = Restrictions.not(Restrictions.in("status", lstStatusMasters));
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.not(Restrictions.in("teacherId", teacherDetail));
			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			criteria.setProjection(Projections.groupProperty("teacherId"));
			lstTeacherDetails = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstTeacherDetails;
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByDSTeachersList(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,int entityID)
	{
		System.out.println("::::::::::::::::::::::getJobForTeacherByDSTeachersList:::::::::::::::::::::::::::::::");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		if(lstTeacherDetails.size()>0){
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
				
				if(districtMaster!=null && entityID!=1){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				
				criteria.add(criterionTeacher);
				criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
    public boolean isJobApply(TeacherDetail teacherDetail)
    {
    	boolean isJobApply = false;
    	String sql = "";
    	List<Object[]> rows = null;
 		try 
		{
 			Session session = getSession();
 			sql = "select jobId from jobforteacher jft where teacherId="+teacherDetail.getTeacherId();
			Query query = session.createSQLQuery(sql);
			rows = query.list();
			if(rows!=null && rows.size()>0)
				isJobApply = true;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return isJobApply;
		}	
		System.out.println("isJobApply::: "+ isJobApply);
		return isJobApply;
    }
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllQQFinalizedTeacherListOfHQAndBranch(List<TeacherDetail> lstTeacherDetails,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
    	PrintOnConsole.getJFTPrint("JFT:94");	
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && (headQuarterMaster!=null || branchMaster!=null))
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
				    if(headQuarterMaster!=null && branchMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				    else if(headQuarterMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
				    else if(branchMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
				    
				    criteria.setProjection(Projections.groupProperty("teacherId"));
				    lstTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstTeacher;
	}
    @Transactional(readOnly=false)	
	public List<JobForTeacher> findByTeacherIdHQAndBranch(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster) 
	{	
		PrintOnConsole.getJFTPrint("JFT:36");
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			if(headQuarterMaster!=null && branchMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
			else if(headQuarterMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			else if(branchMaster!=null)
			    criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));

			criteria.addOrder(Order.desc("createdDateTime"));
			lstJobForTeachers = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}  
    @Transactional(readOnly=false)
	public JobForTeacher getLatestCL(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		JobForTeacher jobForTeacher	=	 null;	
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.isNotNull("coverLetter");
			Criterion criterion3 = Restrictions.ne("coverLetter","");
			Criterion criterion4 = Restrictions.and(criterion2,criterion3);
			Criterion criterion5 = Restrictions.ne("jobId",jobOrder);
			lstJobForTeacher = findWithLimit(Order.desc("createdDateTime"), 0, 1, criterion1,criterion4,criterion5);
			
			System.out.println("lstJobForTeacher.size() "+lstJobForTeacher.size());
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			return null;
		else
		{
			for(JobForTeacher jft: lstJobForTeacher)
			{
				if(jft.getCoverLetter().replaceAll("\\<[^>]*>","").length()>0)
				{
					jobForTeacher	=	jft;
					break;
				}
			}
			return jobForTeacher;
		}
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByJobCategory(List<JobOrder> jobOrderList)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",jobOrderList);
			criteria.add(criterion1);
			criteria.add(criterion2);
			/*criteria.createCriteria("jobId").add(Restrictions.eq("jobId",jobOrder));
			criteria.setProjection(Projections.distinct(Projections.property("jobId")));*/
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> findJobListForJobSpecificInventory(TeacherDetail teacherDetail,Boolean assesmentStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:last");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime))
			.add(Restrictions.eq("status", "A"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
    
    @Transactional(readOnly=false)
	public Map<String,String> findJFTByTeaJobForONB(List<Integer> teacherIds,List<Integer> jobIds)
	{
    	System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyfindTADFDQByTeaJobForONB");
		Map<String,String> thumbsMap = new LinkedHashMap<String, String>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(JobForTeacher.class);
			criteria.createAlias("jobId", "jo").createAlias("teacherId", "td")
		    .setProjection( Projections.projectionList()
		    	.add( Projections.property("jo.jobId"), "jobId" )
				.add( Projections.property("td.teacherId"), "teacherId" )
		        .add( Projections.property("isDistrictSpecificNoteFinalizeONB"), "isDistrictSpecificNoteFinalizeONB" )
		        .add( Projections.property("flagForDistrictSpecificQuestionsONB"), "flagForDistrictSpecificQuestionsONB" )
		    );
			criteria.add(Restrictions.in("jo.jobId", jobIds));
			criteria.add(Restrictions.in("td.teacherId", teacherIds));
			List<String[]>  listTeacherAndJobId= criteria.list();
			for(Object[] str:listTeacherAndJobId){
				thumbsMap.put(str[0].toString()+"##"+str[1].toString(), ((str[2]!=null?str[2].toString():"0"))+"##"+(str[3]!=null?str[3].toString():"0"));
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return thumbsMap;
	}
    
    @Transactional(readOnly=false)
	public List<JobForTeacher> findBySmartPracticesAssessment(TeacherDetail teacherDetail) 
	{	
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			criteria.createCriteria("jobId").createCriteria("jobCategoryMaster").add(Restrictions.eq("preHireSmartPractices",true));
			lstJobForTeachers = criteria.list();
		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}
    
    @Transactional(readOnly=false)
	public List<JobForTeacher> getAllUnhiredJob(List<TeacherDetail> lstTeacherDetails,StatusMaster statusMaster)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));

				if(statusMaster!=null)
					criteria.add(Restrictions.not(Restrictions.ne("status",statusMaster)));
					
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
    
    @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByJobCategoryAndDistrict(List<JobOrder> jobList)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{			
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion criterion1 = Restrictions.eq("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",jobList);
							
			lstJobForTeacher = findByCriteria(criterion1,criterion2);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
    public List getOfferMade(int districtId,List jobforteacherList){
    	//System.out.println(":::::::::::::::getOfferMade()get districtId====="+districtId);
    	int count=0;
    	PrintOnConsole.getJFTPrint("JFT:101");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		Connection connection =null;
	    
    	try{
    		Iterator itr = jobforteacherList.iterator();
    		 StringBuilder commaSepValueBuilder = new StringBuilder();
    		List<String> jobIdList = new ArrayList<String>();
    		int counter =0;
    		if(jobforteacherList!=null && jobforteacherList.size()>0){
				while(itr.hasNext()){
	        		Object[] obj = (Object[]) itr.next();
					JobOrder jobOrder=(JobOrder)obj[0];
					if(jobOrder!=null && !jobIdList.contains(jobOrder.getJobId().toString())){
					  jobIdList.add(jobOrder.getJobId().toString());
					  commaSepValueBuilder.append(jobOrder.getJobId().toString());
					  commaSepValueBuilder.append(",");
					} 
					  counter++;
		         }
				
    		}
        	List list=null;
        	String strJobId = commaSepValueBuilder.toString();
        	String jobIds   =  strJobId.substring(0, strJobId.length()-1);
        	System.out.println("::::::::::::::::::::jobIds====="+jobIds);
        	
    		 SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
 		     ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
 		     connection = connectionProvider.getConnection();
		sql = "SELECT jobId FROM teacherstatushistoryforjob " +
				"WHERE jobId in  ("+jobIds+
				") and statusId = 18 and jobId not in " +
				"(SELECT jobId FROM jobforteacher WHERE jobId in " +
				"("+jobIds+") " +
				"and status = 6 group by jobId) " +
				"group by jobId";
		 PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);
		  ResultSet rs=ps.executeQuery();
		 
				if(rs.next()){				
					do{
						final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
						for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
						   {
							allInfo[i-1]=rs.getString(i);
						   }
						lst.add(allInfo);
						
					}while(rs.next());
				}
				else{
					System.out.println("Record not found.");
				}
				return lst;
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return null;
    }
    public class AutoInactiveJobOrderThread implements Runnable{
    	private JobOrder jobOrderIn=null;
    	private JobOrderDAO jobOrderDAO=null;
    	private EmailerService emailerService=null;
    	private String[] allEmailIds=null;
		public AutoInactiveJobOrderThread(JobOrder jobOrder,JobOrderDAO jobOrderDAO, EmailerService emailerService, List<String> allEmailIds){
			this.jobOrderIn=jobOrder;
			this.jobOrderDAO=jobOrderDAO;
			this.emailerService=emailerService;
			this.allEmailIds=allEmailIds.toArray(new String[allEmailIds.size()]);
		}
		public void run() {
			try {
				GlobalServices.println("yyyyyyyyyyyyyyyyyyyyyyyyyy--------------------------------------------------1");
				Session session=jobOrderDAO.getSessionFactory().openSession();
				Thread.sleep(5000);
				Query exQuery=session.createSQLQuery("CALL JOB_DEACTIVE_AUTO_IF_HIREEXPECTED_FULL(:jobId)").setParameter("jobId", jobOrderIn.getJobId());
				int exRows = exQuery.executeUpdate();
				System.out.println("exRows======="+exRows);
				Thread.sleep(500);
				JobOrder jobOrder=(JobOrder)session.load(JobOrder.class, jobOrderIn.getJobId());
				ElasticSearchService es=new ElasticSearchService();
				es.updateESSchoolJobOrderByJobId(jobOrder);
				if(jobOrder.getStatus().trim().equalsIgnoreCase("I")){
				es.searchDataAndDelete(jobOrder.getJobId(),ElasticSearchConfig.indexForhqDistrictJobBoard);
				String notificationTemplate=AcceptOrDeclineMailHtml.getAutoInactiveNotificationMail(jobOrder);
				System.out.println("\nTemp::"+notificationTemplate);
				emailerService.sendMailAsHTMLTextCC(allEmailIds,"Notification of inactive JOB :"+jobOrderIn.getJobId(),notificationTemplate,new String[]{});
				}else{
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
							&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
							&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
							&& (jobOrder.getHiddenJob()==null ||  !jobOrder.getHiddenJob())	)
					{
						es.updateESDistrictJobBoardByJobId(jobOrder);
					}
				}
				GlobalServices.println("yyyyyyyyyyyyyyyyyyyyyyyyyy--------------------------------------------------2");
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
    private List<String> getAllSAandDAsEmailIds(JobOrder jobOrder){
		List<String> allUserEmail=new ArrayList<String>();
		List<String> emailListDAs=new LinkedList<String>();
		List<String> emailListSAs=new LinkedList<String>();
		for(UserMaster um:userMasterDAO.getUserByDistrict(jobOrder.getDistrictMaster())){
			if(um.getEmailAddress()!=null && um.getEmailAddress().trim().equalsIgnoreCase(""))
			emailListDAs.add(um.getEmailAddress());
		}
		for(UserMaster um:userMasterDAO.getUserIDSBySchools(schoolInJobOrderDAO.findSchoolIds(jobOrder))){
			if(um.getEmailAddress()!=null && um.getEmailAddress().trim().equalsIgnoreCase(""))
				emailListSAs.add(um.getEmailAddress());
		}
		System.out.println("********************SAs********************"+emailListSAs);
		System.out.println("********************DAs********************"+emailListDAs);
		allUserEmail.addAll(emailListDAs);
		allUserEmail.addAll(emailListSAs);
		return allUserEmail;
	}
	private List<String> getGroupWiseAllUser(JobOrder jobOrder,SchoolMaster schoolMaster){
		List<String> allUserEmail=new ArrayList<String>();
		List<String> emailListDAs=new LinkedList<String>();
		List<String> emailListSAs=new LinkedList<String>();
		List<String> emailListEST=new LinkedList<String>();
		Map<String,String> groupNameToShortNameList=new LinkedHashMap<String,String>();
		if(jobOrder.getEmploymentServicesTechnician()!=null){
			emailListEST.add(jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress());
			emailListEST.add(jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress());
		}
		if(schoolMaster!=null){
			List<UserMaster> umListSAs=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
			for(UserMaster um:umListSAs)
				if(!emailListSAs.contains(um.getEmailAddress())){
					emailListSAs.add(um.getEmailAddress());
				}
		}
		
		if(jobOrder.getSpecialEdFlag()!=null){
			List<DistrictSpecificApprovalFlow> listDSAF=districtSpecificApprovalFlowDAO.findByCriteria(Restrictions.eq("specialEdFlag", jobOrder.getSpecialEdFlag()),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.eq("status", "A"));
			if(listDSAF!=null && listDSAF.size()>0){
				List<String> groupShortName=new ArrayList<String>();
				for(String group:listDSAF.get(0).getApprovalFlow().split("#")){
					if(!group.trim().equals("")){
						groupShortName.add(group.trim());
					}
				}
				if(groupShortName.size()>0){
				List<DistrictWiseApprovalGroup> listDWAG=districtWiseApprovalGroupDAO.findByCriteria(Restrictions.in("groupShortName", groupShortName),Restrictions.eq("status", "A"));
					if(listDWAG!=null && listDWAG.size()>0){
						List<String> groupName=new ArrayList<String>();
						for(DistrictWiseApprovalGroup dwag:listDWAG){
							groupName.add(dwag.getGroupName());
							groupNameToShortNameList.put(dwag.getGroupName(), dwag.getGroupShortName());
						}
						
						
						for(Map.Entry<String,String> entry:groupNameToShortNameList.entrySet())
							System.out.println(entry.getKey()+"      ============    "+entry.getValue());
						
						
						if(groupName.size()>0){
						List<DistrictApprovalGroups> dAGList=districtApprovalGroupsDAO.findByCriteria(Restrictions.in("groupName",groupName),Restrictions.isNull("jobCategoryId"),Restrictions.isNull("jobId"),Restrictions.eq("districtId", jobOrder.getDistrictMaster()));
						//System.out.println("dAGList=============="+dAGList.size());
							if(dAGList.size()>0){
								List<Integer> keyContactId=new ArrayList<Integer>();
								Map<String,DistrictKeyContact> memberToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
									for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
										if(!memberKey.trim().equals(""))
											try{
											keyContactId.add(Integer.parseInt(memberKey));
											//System.out.println("keyId============="+memberKey);
											}catch(NumberFormatException e){}
									}
								}
								if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										memberToKeyContactMap.put(dkc.getKeyContactId().toString(), dkc);
								}
								
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
									for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
										if(!memberKey.trim().equals(""))
											try{
												if(groupNameToShortNameList.get(districtAppGroup.getGroupName()).trim().equalsIgnoreCase("SA")){
												emailListSAs.add(memberToKeyContactMap.get(memberKey).getUserMaster().getEmailAddress());
												}else{
												emailListDAs.add(memberToKeyContactMap.get(memberKey).getUserMaster().getEmailAddress());
												}
											}catch(NumberFormatException e){}
									}
								}
								/*if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										if(!allUserEmail.contains(dkc.getUserMaster().getEmailAddress()))
											allUserEmail.add(dkc.getUserMaster().getEmailAddress());
								}*/
							}
						}
					}
				}
			}
		}
		System.out.println("********************ES*********************"+emailListEST);
		System.out.println("********************SAs********************"+emailListSAs);
		System.out.println("********************DAs********************"+emailListDAs);
		allUserEmail.addAll(emailListDAs);
		allUserEmail.addAll(emailListSAs);
		allUserEmail.addAll(emailListEST);
		println("umList==================================================="+allUserEmail);
		return allUserEmail;
	}
   
    @Transactional(readOnly=false)
	public List<JobForTeacher> findJFTforOfferReadystatusNew(DistrictMaster districtMaster ,Order sortOrderStrVal,List<JobOrder> jobOrders, boolean sortingcheck,Integer status,String startDate,String enddate)
	{

		System.out.println("::::::::::::: STATUS 11111111111::::::::::::::"+status);
		Date startD = null;
        Date endD   = null;
        
        try{
        	if(!startDate.equals(""))
            	startD = Utility.getCurrentDateFormart(startDate);
        } catch(Exception exception){
        	exception.printStackTrace();
        }
        
        try{
        	if(!enddate.equals(""))
        		endD = Utility.getCurrentDateFormart(enddate);
        } catch(Exception exception){
        	exception.printStackTrace();
        }
        
		System.out.println("start date============+"+startD+"End date============="+endD);
        
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			//criteria.createCriteria("jobId").add(Restrictions.eq("status", "A"));
			//criteria.addOrder(Order.asc("teacherId"));
			
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			
			Criterion isnotNullCriterion    =Restrictions.isNotNull("offerReady");
			criteria.add(isnotNullCriterion);
			
			if(!status.equals(0)){
				if(status.equals(1)){
					Criterion offerReadyCompletedPending = Restrictions.eq("offerReady", true);
					criteria.add(offerReadyCompletedPending);
				} else if(status.equals(2)){
					Criterion offerReadyCompletedPending = Restrictions.eq("offerReady", false);
					criteria.add(offerReadyCompletedPending);
				}
			}
			
			if(startD!=null && endD!=null){
				criteria.add(Restrictions.ge("offerMadeDate",startD)).add(Restrictions.le("offerMadeDate",endD));
			}
			
			if(!sortingcheck){
			 criteria.addOrder(sortOrderStrVal);
			}
			
			if(jobOrders!=null && jobOrders.size()>0)
				criteria.add(Restrictions.in("jobId",jobOrders));
			
			lstJobForTeacher = criteria.list();
			System.out.println("offerReadyofferReady :: "+lstJobForTeacher.size());
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		System.out.println("::::::: lstJobForTeacher :::::::::"+lstJobForTeacher.size());
		return lstJobForTeacher;
	
	}
    
    @Transactional(readOnly=false)
    public List<JobForTeacher> findAllTeacherByDistrictId(TeacherDetail teacherDetail,DistrictMaster districtMaster)
    {
    	PrintOnConsole.getJFTPrint("JFT:93");	
    	List<JobForTeacher> lstJobForTeacher= null;
    	try 
    	{
    		
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			criteria.add(Restrictions.eq("teacherId",teacherDetail));
    			if(districtMaster!=null)
    			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			lstJobForTeacher = criteria.list();
    	} 
    	catch (Exception e) {
    		e.printStackTrace();
    	}		
    	return lstJobForTeacher;
    }
    @Transactional(readOnly=false)
	public List<JobForTeacher> firstJobDateAppliedbyKellyApplicant(List<TeacherDetail> teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(1, false, false);
		try 
		{
			if(teacherDetail!=null && teacherDetail.size()>0){
				/*Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass())
				.createAlias("teacherId", "td");
				DetachedCriteria subQuery = DetachedCriteria.forClass(JobForTeacher.class, "tns")
				.setProjection(Projections.projectionList()
			    		.add(Projections.min("tns.createdDateTime")))
			    		.add(Restrictions.eqProperty("tns.teacherId.teacherId", "td.teacherId"));
				criteria.add(Property.forName("createdDateTime").eq(subQuery));
				criteria.add(Restrictions.in("teacherId", teacherDetail));
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
				lstJobForTeacher = criteria.list() ;*/	
				
				Criterion criterion1 = Restrictions.in("teacherId", teacherDetail);
				Criterion criterion2 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				Session session = getSession();
				Criteria criteria=	session.createCriteria(getPersistentClass()) 
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherId"))
						.add(Projections.min("createdDateTime"))
				);
				criteria.add(criterion1);
					lstJobForTeacher=criteria.createCriteria("jobId").add(criterion2).list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int totalAppliedApplicantsByJobOrders(List<JobOrder> jobOrders ,String hird) 
	{
		Session session = getSession();
		String sql =" SELECT COUNT(*) AS appliedCnt " +
		                  " from jobforteacher jft where jft.jobId IN (:jobOrders)";
		
		if(hird!=null && hird.equalsIgnoreCase("hird"))
			sql+=	" and  jft.status=6";
		
		/*String[] statuss = {"dcln","icomp","vlt","widrw","hide","rem"};
		List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);*/
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		
		List rows = query.list();
		if(rows!=null){		
			return Integer.parseInt(rows.get(0)+"");
		}else{
			return 0;
		}
		
	}
    
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int totalUniqueAppliedApplicantsByJobOrders(List<JobOrder> jobOrders) 
	{
		Session session = getSession();
		String sql =" SELECT COUNT(*) AS appliedCnt " +
		                  " from jobforteacher jft where jft.jobId IN (:jobOrders) group by jft.teacherId";
		
		/*String[] statuss = {"dcln","icomp","vlt","widrw","hide","rem"};
		List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);*/
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		
		List rows = query.list();
		if(rows!=null){		
			return rows.size();
		}else{
			return 0;
		}
		
	}
    
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> countApplicantsByJobOrdersMukesh(List<JobOrder> jobOrders) 
	{
		Session session = getSession();
		String sql =" SELECT jft.districtId, COUNT( IF( jft.status =6, 1, NULL ) ) AS hiredCnt " +
		" from jobforteacher jft where jft.jobId IN (:jobOrders)  group by jft.districtId ";
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		
		List<Object[]> rows = query.list();
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], Integer.parseInt(obj[1].toString()));
			}
		}
		return map;
	}
    
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,String> countApplicantsByJobOrders(List<JobOrder> jobOrders) 
	{
		Session session = getSession();
		String sql =" SELECT jft.jobId, COUNT(*) AS appliedCnt,jft.districtId "+
			" from jobforteacher jft where jft.jobId IN (:jobOrders) and jft.status!=8 group by jft.jobId  having appliedCnt>=5";
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		
		
		List<Object[]> rows = query.list();
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				
				Object obj[] = (Object[])oo;
				
				map.put((Integer)obj[0], obj[1]+"##"+obj[2]);
			}
		}
		return map;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAllJFTForKellyDashboard(List<TeacherDetail> teacherDetail,UserMaster userMaster,boolean globalFlag)
	{
		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			
  		    if((teacherDetail!=null && teacherDetail.size()>0) || globalFlag)
  		    {
			  criteria.add(Restrictions.in("teacherId",teacherDetail));			
			}
  		    if(userMaster.getHeadQuarterMaster()!=null)
  		    	criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster",userMaster.getHeadQuarterMaster()));
  		    else if(userMaster.getBranchMaster()!=null)
  		    	criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster",userMaster.getBranchMaster()));
			
			criteria.add(Restrictions.ne("status",statusMaster));								
			criteria.setProjection(Projections.groupProperty("teacherId"));
			lstJobForTeacher = criteria.list();
			System.out.println(" Size onbarding :"+lstJobForTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		System.out.println("------------------------------------------------------");
		return lstJobForTeacher;
	}
	
	// for Optimization by Anurag   replacement of Method  =>  getJobForTeacherByDSTeachersList 
    @Transactional(readOnly=false)
  public List<String[]> getJobForTeacherByJobAndTeachersListWithHQandBR(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
  {
    System.out.println("getJobForTeacherByJobAndTeachersListWithHQandBR  method::::::::::::::::");
    PrintOnConsole.getJFTPrint("JFT:92");     
     List<String[]> lstJobForTeacher=new ArrayList<String[]>();
     if(lstTeacherDetails.size()>0){
             try{
                   Session session = getSession();
                  
                   Criteria criteria = session.createCriteria(getPersistentClass());             
                   criteria.setProjection( Projections.projectionList()
                           .add( Projections.property("teacherId.teacherId"), "teachId" )
                           .add( Projections.count("teacherId") )              
                           .add( Projections.groupProperty("teacherId") )
                       );
                   Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
                   
                   if(entityID!=1){                                   
                                  if(districtMaster!=null){
                                        criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
                                  }else{
                                        //jobOrderSet.add(Restrictions.isNull("districtMaster"));
                                  }
                                  if(branchMaster!=null){
                                        criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster.branchId",branchMaster.getBranchId()));
                                  }else{
                                        //jobOrderSet.add(Restrictions.isNull("branchMaster"));
                                  }
                                  if(headQuarterMaster!=null){
                                        if(headQuarterMaster.getHeadQuarterId()==1){
                                              criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterId",headQuarterMaster.getHeadQuarterId()));
                                        }
                                  }else{
                                        //jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
                                  }
                            }
                  
               if(lstTeacherDetails.size()>0){
                            criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
                            criteria.add(criterionTeacher);
                      }
                      
                      //  System.out.println(criteria);
                            //criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
                   
                   
                   lstJobForTeacher = criteria.list() ;
                               }catch (Throwable e) {
                   e.printStackTrace();
             }   
     }
       return lstJobForTeacher;
  }
  
	
// For Optimization 
    
    @Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsDataOp(Order sortOrderStrVal,int start,int end,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> lstTeacherDetailAllFilter,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,boolean normScoreFlag,List<StatusMaster> lstStatusMastersNew)
	{

		
		PrintOnConsole.getJFTPrint("JFT:71");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("inside 1");
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			System.out.println("inside 2");
			Criterion criterionSchool =null;
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			}
			
			criteria.add(Restrictions.ne("status",statusMaster));
			if(lstStatusMastersNew.size()>0)
				criteria.add(Restrictions.in("status",lstStatusMastersNew));
			
			if(fDate!=null && tDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
			else if(fDate!=null && tDate==null)
				criteria.add(Restrictions.ge("createdDateTime",fDate));
			else if(tDate!=null && fDate==null)
				criteria.add(Restrictions.le("createdDateTime",tDate));
			

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			System.out.println("inside 3");
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				}else if(entityID==4 && dateFlag){
					dateDistrictFlag=true;
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}*/
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				flag=true;
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				dateDistrictFlag=true;
			}
			else if(entityID==5)
			{

				System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
				flag=true;
				if(branchMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				else
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));

			}
			else if(entityID==6)
			{
				System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
				flag=true;
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			}
			if(dateDistrictFlag==false && dateFlag)
			{
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				flag=true;
			}
			System.out.println("inside 6");
			if(status){
				criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			System.out.println("inside 7");
			//job applied flag
			if(newcandidatesonlyNew)
			{
				if(appliedfDate!=null && appliedtDate!=null)
				{
					System.out.println("############### newcandidatesonlyNew FT #######################");
					criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedfDate!=null && appliedtDate==null)
				{
					System.out.println("############### newcandidatesonlyNew F #######################");
					//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedtDate!=null && appliedfDate==null)
				{
					System.out.println("############### newcandidatesonlyNew T #######################");
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
			}
			else
			{System.out.println("inside 8");
				if(appliedfDate!=null && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				else if(appliedfDate!=null && appliedtDate==null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				else if(appliedtDate!=null && appliedfDate==null)
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				System.out.println("inside 9");
			}
			
			flag=true;
			if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
			{
				criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetailAllFilter))); // filter first time
			}
			System.out.println("inside 10");
			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size() >0){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			System.out.println("inside 11");
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			System.out.println("inside 12  ");
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
					.addOrder(sortOrderStrVal);
				}
				/*else{
					if((headQuarterMaster==null || branchMaster==null) && !(sortOrderStrVal.toString().contains("spStatus") || sortOrderStrVal.toString().contains("normScore")))
					criteria.createCriteria("teacherId").addOrder(sortOrderStrVal);	
				}*/
				
				criteria.setProjection(Projections.groupProperty("teacherId"));
				if(!normScoreFlag){
					criteria.setFirstResult(start);
					criteria.setMaxResults(end);	
				}
			
				try {
					criteria.setFetchMode("teacherId", FetchMode.SELECT);
					criteria.setFetchMode("jobId", FetchMode.SELECT);
					criteria.setFetchMode("status", FetchMode.SELECT);
					criteria.setFetchMode("updatedBy", FetchMode.SELECT);					
					criteria.setFetchMode("districtSpecificNoteFinalizeONBBy", FetchMode.SELECT);
					criteria.setFetchMode("userMaster", FetchMode.SELECT);
					criteria.setFetchMode("statusMaster", FetchMode.SELECT);
					criteria.setFetchMode("secondaryStatus", FetchMode.SELECT);
					criteria.setFetchMode("internalStatus", FetchMode.SELECT);
					criteria.setFetchMode("internalSecondaryStatus", FetchMode.SELECT);
					criteria.setFetchMode("schoolMaster", FetchMode.SELECT);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("13");
				
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			System.out.println("inside 15");
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criteria.add(criterionSchool);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	
}   
    
    
    
    public List<String[]> findMinJobEndDateFromJobForteacher(TeacherDetail teacherDetail,UserMaster userMaster){
    	  List<String[]> jobOrderList =  new ArrayList<String[]>();
    	  //JobOrder
    	  try{
    	   Session session = getSessionFactory().openSession(); 
    	   Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo");   
    	   criteria.createAlias("jo.districtMaster", "dm")
    	      .setProjection( Projections.projectionList()
    	          .add( Projections.max("createdDateTime"), "jobEndDateMax" )
    	          .add( Projections.min("createdDateTime"), "jobEndDateMin" )
    	      );
    	   
    	   if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6){
    	    criteria.add(Restrictions.eq("dm.districtId",userMaster.getDistrictId().getDistrictId())).add(Restrictions.eq("teacherId", teacherDetail));
    	   }else{
    	     criteria.add(Restrictions.eq("teacherId", teacherDetail));
    	   }
    	   
    	   
    	   List<Integer> listJobIds=null;
    	   if(userMaster.getEntityType()==3){
    	    listJobIds=new ArrayList<Integer>();
    	    listJobIds=schoolInJobOrderDAO.findJobIdBySchool(userMaster.getSchoolId(),userMaster.getDistrictId().getDistrictId());
    	    listJobIds.add(0);
    	    
    	    if(listJobIds!=null){
    	     criteria.add(Restrictions.in("jo.jobId", listJobIds));
    	    }
    	   }
    	   
    	   jobOrderList=criteria.list();   
    	  
    	  }catch(Exception e){
    	   e.printStackTrace();
    	  }   
    	   return jobOrderList; 
    	 }
    	 

    @Transactional(readOnly=false)
    	 public List<String[]> findLastContactedOnJobForteacher(TeacherDetail teacherDetail,UserMaster userMaster,JobOrder jobOrder){

    	  List<String[]> jobOrderList =  new ArrayList<String[]>();
    	  //JobOrder
    	  try{
    	   Session session = getSessionFactory().openSession(); 
    	   Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo");   
    	   criteria.createAlias("jo.districtMaster", "dm")
    	      .setProjection( Projections.projectionList()
    	          .add(Projections.max("lastActivityDate"), "lastActivityDatefvghyg" )
    	          .add(Projections.property("jobForTeacherId"))
    	          .add(Projections.groupProperty("teacherId"))
    	      );
    	   criteria.add(Restrictions.isNotNull("lastActivityDate"));
    	   if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6){
    	    criteria.add(Restrictions.eq("dm.districtId",userMaster.getDistrictId().getDistrictId())).add(Restrictions.eq("teacherId", teacherDetail));
    	   }else{
    	     criteria.add(Restrictions.eq("teacherId", teacherDetail));
    	   }
    	   
    	   
    	   List<Integer> listJobIds=new ArrayList<Integer>();
    	   if(jobOrder!=null){
    	    listJobIds.add(jobOrder.getJobId());
    	   }
    	   if(userMaster.getEntityType()==3){
    	    
    	    listJobIds=schoolInJobOrderDAO.findJobIdBySchool(userMaster.getSchoolId(),userMaster.getDistrictId().getDistrictId());
    	    listJobIds.add(0);
    	   }

    	   if(listJobIds!=null && listJobIds.size()>0){
    	    criteria.add(Restrictions.in("jo.jobId", listJobIds));
    	   }
    	   jobOrderList=criteria.list();   
    	  
    	  }catch(Exception e){
    	   e.printStackTrace();
    	  }   
    	   return jobOrderList; 
    	 
    	 }
    	 
    	 
    	 
    	 // for projection
    	 
    	 
    	 @Transactional(readOnly=false)
    		public List<Integer> getTeacherDetailByDASARecordsCountOp(Order sortOrderStrVal,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> lstTeacherDetailAllFilter,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,List<StatusMaster> lstStatusMastersNew)
    		{
    			PrintOnConsole.getJFTPrint("JFT:72");	
    			TestTool.getTraceTime("500");
    			List<Integer> lstTeacherDetail= new ArrayList<Integer>();
    			try{
    				Session session = getSession();
    				Criteria criteria = session.createCriteria(getPersistentClass());
    				Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
    				try{
    					if(Utility.getValueOfPropByKey("basePath").contains("platform")){
    						//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
    						JobOrder jobOrder = new JobOrder();
    						 jobOrder.setJobId(11136);
    						 
    						if(jobOrder!=null)
    						criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
    					}
    				}catch(Exception e){
    					e.printStackTrace();
    				}
    				if(internalCandVal==1){
    					TestTool.getTraceTime("501");
    					criteria.add(criterionInterCand);
    				}

    				criteria.add(Restrictions.ne("status",statusMaster));
    				if(lstStatusMastersNew.size()>0)
    					criteria.add(Restrictions.in("status",lstStatusMastersNew));
    				
    				Criterion criterionSchool =null;
    				if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
    					criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
    				}
    				
    				boolean flag=false;
    				boolean dateDistrictFlag=false;
    				if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
    				{
    					TestTool.getTraceTime("502");
    					Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
    					criteria.add(criterion1);
    				}
    				
    				if((entityID==3 || entityID==4)){
    					TestTool.getTraceTime("503");
    					flag=true;
    					if(lstJobOrder.size()>0)
    					{
    						TestTool.getTraceTime("504");
    						Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
    						criteria.add(criterion1);
    					}
    					if(entityID==4 && dateFlag==false){
    						TestTool.getTraceTime("505");
    						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    					}else if(entityID==4 && dateFlag){
    						TestTool.getTraceTime("506");
    						dateDistrictFlag=true;
    						/*if(fDate!=null && tDate!=null){
    							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    						}else if(fDate!=null && tDate==null){
    							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
    						}else if(fDate==null && tDate!=null){
    							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
    						}*/
    						
    						if(lstJobOP!=null && lstJobOP.size()>0)
    						{
    							Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
    							criteria.add(criterion1);
    						}
    						
    					}
    				}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
    					TestTool.getTraceTime("507");
    					flag=true;
    					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    				}else if(dateFlag && districtMaster!=null &&  entityID==2){
    					TestTool.getTraceTime("508");
    					flag=true;
    					/*if(fDate!=null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    					}else if(fDate!=null && tDate==null){
    						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
    					}else if(fDate==null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
    					}*/
    					
    					if(lstJobOP!=null && lstJobOP.size()>0)
    					{
    						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
    						criteria.add(criterion1);
    					}
    					
    					dateDistrictFlag=true;
    				}
    				else if(entityID==5)
    				{
    					System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
    					flag=true;
    					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
    				}
    				else if(entityID==6)
    				{
    					System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
    					flag=true;
    					criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
    				}
    				if(dateDistrictFlag==false && dateFlag){
    					TestTool.getTraceTime("509");
    					/*if(fDate!=null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    					}else if(fDate!=null && tDate==null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    					}else if(fDate==null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    					}*/
    					
    					if(lstJobOP!=null && lstJobOP.size()>0)
    					{
    						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
    						criteria.add(criterion1);
    					}
    					
    					flag=true;
    				}
    				if(status){
    					TestTool.getTraceTime("510");
    					criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
    					if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
    						criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
    				}
    				
    				//job applied flag
    				//job applied flag
    				if(newcandidatesonlyNew)
    				{
    					if(appliedfDate!=null && appliedtDate!=null)
    					{
    						System.out.println("############### newcandidatesonlyNew Count FT #######################");
    						criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
    					}
    					else if(appliedfDate!=null && appliedtDate==null)
    					{
    						System.out.println("############### newcandidatesonlyNew Count F #######################");
    						//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
    						criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
    					}
    					else if(appliedtDate!=null && appliedfDate==null)
    					{
    						System.out.println("############### newcandidatesonlyNew Count T #######################");
    						criteria.add(Restrictions.le("createdDateTime",appliedtDate));
    					}
    				}
    				else
    				{
    					if(appliedfDate!=null && appliedtDate!=null)
    						criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
    					else if(appliedfDate!=null && appliedtDate==null)
    						criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
    					else if(appliedtDate!=null && appliedfDate==null)
    						criteria.add(Restrictions.le("createdDateTime",appliedtDate));
    				}
    				
    				
    				flag=true;
    				if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
    				{
    					TestTool.getTraceTime("511 T");
    					criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetailAllFilter)));
    				}

    				if(teacherFlag){
    					if(filterTeacherList!=null && filterTeacherList.size() >0)
    					{
    						TestTool.getTraceTime("512 T");
    						flag=true; //
    						criteria.add(Restrictions.in("teacherId", filterTeacherList)); //
    					}
    				}
    				if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size()>0) //
    				{
    					TestTool.getTraceTime("513 T");
    					criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));
    				}
    				
    				if(districtMaster!=null  && utype==3)
    				{
    					TestTool.getTraceTime("514");
    					StatusMaster master = districtMaster.getStatusMaster();
    					SecondaryStatus smaster = districtMaster.getSecondaryStatus();
    					if(master!=null)
    					{
    						TestTool.getTraceTime("515");
    						System.out.println("master: "+master.getStatus());
    						String sts = master.getStatusShortName();
    						if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
    						{
    							List<StatusMaster> sList = new ArrayList<StatusMaster>();
    							sList.add(master);
    							if(sts.equals("scomp"))
    							{
    								StatusMaster st1 = new StatusMaster();
    								st1.setStatusId(17);
    								sList.add(st1);
    								
    								StatusMaster st2 = new StatusMaster();
    								st2.setStatusId(18);
    								sList.add(st2);
    								
    							}else if(sts.equals("vcomp"))
    							{
    								StatusMaster st2 = new StatusMaster();
    								st2.setStatusId(18);
    								sList.add(st2);
    							}
    							
    							/*
    							6 - Hired
    							19 - Declined
    							10 - rejected
    							7 -- Withdrew
    							*/
    							
    							StatusMaster stHird = new StatusMaster();
    							stHird.setStatusId(6);
    							sList.add(stHird);

    							StatusMaster stDeclined = new StatusMaster();
    							stDeclined.setStatusId(19);
    							sList.add(stDeclined);
    							
    							StatusMaster stRejected = new StatusMaster();
    							stRejected.setStatusId(10);
    							sList.add(stRejected);
    							
    							StatusMaster stWithdrew = new StatusMaster();
    							stWithdrew.setStatusId(7);
    							sList.add(stWithdrew);
    							
    							criteria.add(Restrictions.in("status",sList));
    						}else
    						{
    							List<StatusMaster> sList = new ArrayList<StatusMaster>();
    							sList.add(master);
    							if(sts.equals("hird"))
    							{
    								StatusMaster stDeclined = new StatusMaster();
    								stDeclined.setStatusId(19);
    								sList.add(stDeclined);
    								
    								StatusMaster stRejected = new StatusMaster();
    								stRejected.setStatusId(10);
    								sList.add(stRejected);
    								
    								StatusMaster stWithdrew = new StatusMaster();
    								stWithdrew.setStatusId(7);
    								sList.add(stWithdrew);
    							}else if(sts.equals("dcln"))
    							{
    								StatusMaster stRejected = new StatusMaster();
    								stRejected.setStatusId(10);
    								sList.add(stRejected);
    								
    								StatusMaster stWithdrew = new StatusMaster();
    								stWithdrew.setStatusId(7);
    								sList.add(stWithdrew);

    							}else if(sts.equals("rem"))
    							{
    								StatusMaster stWithdrew = new StatusMaster();
    								stWithdrew.setStatusId(7);
    								sList.add(stWithdrew);
    							}
    							criteria.add(Restrictions.in("status",sList));
    						}
    					}
    					if(smaster!=null)
    					{
    						System.out.println("smaster: "+smaster.getSecondaryStatusName());
    						Criteria c = criteria.createCriteria("secondaryStatus");
    						
    						Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
    						if(!stq.equals(""))
    						{
    							Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
    							Criterion c3 = Restrictions.or(c2, criterion1);
    							c.add(c3);
    						}else
    							c.add(c2);
    					}
    				}
    				System.out.println("*****************************************************");
    				if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
    					TestTool.getTraceTime("516");
    					if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
    					{
    						TestTool.getTraceTime("517");
    						criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
    						.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
    						.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
    					}				
    					//criteria.setProjection(Projections.countDistinct("teacherId"));
    					String countRow= " count(distinct this_.teacherId)";
    					criteria.setProjection( Projections.projectionList().add(Projections.sqlProjection(""+countRow+" as countRow", new String[]{"countRow"}, new Type[]{ Hibernate.INTEGER}),"countRow"));
    				 
    					
    					// for Optimization Anurag start....
    					try { 
    						criteria.setFetchMode("jobId", FetchMode.SELECT); 
    						criteria.setFetchMode("updatedBy", FetchMode.SELECT);					
    						criteria.setFetchMode("districtSpecificNoteFinalizeONBBy", FetchMode.SELECT);
    						criteria.setFetchMode("userMaster", FetchMode.SELECT);
    						criteria.setFetchMode("statusMaster", FetchMode.SELECT); 
    						criteria.setFetchMode("internalStatus", FetchMode.SELECT);
    						criteria.setFetchMode("internalSecondaryStatus", FetchMode.SELECT);
    						criteria.setFetchMode("schoolMaster", FetchMode.SELECT);
    						
    					} catch (Exception e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
    					// for Optimization Anurag End....
    					
    					
    					
    					lstTeacherDetail =  criteria.list();
    					TestTool.getTraceTime("518 lstTeacherDetail "+lstTeacherDetail.size());
    				}
    				if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
    					criteria.add(criterionSchool);
    				}
    			} 
    			catch (Exception e) {
    				e.printStackTrace();
    			}		
    			TestTool.getTraceTime("599");
    			return lstTeacherDetail;
    		}
    
 
    	 // Anurag for Optimization replacement of Method == >    getAllQQFinalizedTeacherListByJobCategory	 
    	 
    	 
    	 @Transactional(readOnly=false)
    		public List<TeacherDetail> getAllQQFinalizedTeacherListByJobCategoryOp(List<TeacherDetail> lstTeacherDetails,JobCategoryMaster jobCat)
    		{
    			PrintOnConsole.getJFTPrint("JFT:105");
    			List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
    			
    			if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
    			{
    				try{
    					Session session = getSession();
    					Criteria criteria = session.createCriteria(getPersistentClass());
    					 long start = new Date().getTime();
    					 
    					List<JobOrder> lstJobOrder =  jobOrderDAO.findJobIdByJobCategoryMasterAll(jobCat);
    					 
    					 
    					if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
    						criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
    					    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
    					    criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
    					    criteria.add(Restrictions.in("jobId.jobId",lstJobOrder));
    					  //  criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster", jobCat));
    					    criteria.setProjection(Projections.groupProperty("teacherId"));
    					    
    					    System.out.println("Criteria ======"+criteria);
    					    lstTeacher =  criteria.list();
    					    
    					    
    					   
    					    
    					     long end = new Date().getTime();
    					     System.out.println("total time taken ==="+(end-start)+"ms");
    					     
    					     
    					     
    					    
    					    System.out.println(" getAllQQFinalizedTeacherListByJobCategory ::::::::::  list Size == "+lstTeacher.size());
    				} 
    				catch (Exception e) {
    					e.printStackTrace();
    				}
    			}
    			return lstTeacher;
    		}
    	 
	
  @Transactional(readOnly=false)
  @SuppressWarnings("unchecked")
  public Map<Integer,String> countJobsByTeachers(List<TeacherDetail> teacherDetails,UserMaster userMaster) 
  {
	  Session session = getSession();
	  String sql ="";
	  Query query = null;
	  if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){
		 sql = "SELECT jft.teacherId, COUNT(*) AS appliedCnt from jobforteacher jft, joborder jo where jft.teacherId IN (:teacherDetails) and jft.jobId=jo.jobId and jo.headQuarterId=(:id) group by jft.teacherId";
		 query = session.createSQLQuery(sql);
		 query.setParameter("id", userMaster.getHeadQuarterMaster());
	  }
	  else if(userMaster.getBranchMaster()!=null){
		  sql = "SELECT jft.teacherId, COUNT(*) AS appliedCnt from jobforteacher jft, joborder jo where jft.teacherId IN (:teacherDetails) and jft.jobId=jo.jobId and jo.branchId=(:id) group by jft.teacherId";
		  query = session.createSQLQuery(sql);
		  query.setParameter("id", userMaster.getBranchMaster());
	  }
	  else if(userMaster.getDistrictId()!=null){
		  sql ="SELECT jft.teacherId, COUNT(*) AS appliedCnt from jobforteacher jft, joborder jo where jft.teacherId IN (:teacherDetails) and jft.jobId=jo.jobId and jo.districtId=(:id) group by jft.teacherId";
		  query = session.createSQLQuery(sql);
		  query.setParameter("id", userMaster.getDistrictId());
	  }
	  else{
		  sql = "SELECT jft.teacherId, COUNT(*) AS appliedCnt from jobforteacher jft where jft.teacherId IN (:teacherDetails) group by jft.teacherId";
		  query = session.createSQLQuery(sql);
	  }

	  query.setParameterList("teacherDetails", teacherDetails);

	  List<Object[]> rows = query.list();
	  Map<Integer,String> map = new HashMap<Integer, String>();
	  if(rows.size()>0)
	  {
		  for(Object oobz: rows){

			  Object obj[] = (Object[])oobz;
			  map.put((Integer)obj[0], obj[0]+"###"+obj[1]);
		  }
	  }
	  return map;
  }
  @Transactional(readOnly=false)
	public List<JobForTeacher> findJFTForKellyDashboard(List<TeacherDetail> teacherDetail,List<StatusMaster> statusMasterList,UserMaster userMaster,boolean globalFlag,List<JobOrder> listjJobOrders,boolean statusFlag)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			
		    if((teacherDetail!=null && teacherDetail.size()>0) || globalFlag)
		    {
			  criteria.add(Restrictions.in("teacherId",teacherDetail));			 
			}
		    if(listjJobOrders.size()>0)
		      criteria.add(Restrictions.in("jobId",listjJobOrders));
		    	
		    if(userMaster.getHeadQuarterMaster()!=null)
		    	jobOrderSet.add(Restrictions.eq("headQuarterMaster",userMaster.getHeadQuarterMaster()));
		    else if(userMaster.getBranchMaster()!=null)
		    	jobOrderSet.add(Restrictions.eq("branchMaster",userMaster.getBranchMaster()));
		    if(statusMasterList.size()>0 || statusFlag)
			    criteria.add(Restrictions.in("status", statusMasterList)).add((Restrictions.in("statusMaster", statusMasterList)));
		    
			lstJobForTeacher = criteria.list();
			
			System.out.println(" Size Onboarding :"+lstJobForTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
  @Transactional(readOnly=false)
  public List<String[]> getJobForTeacherByJobAndTeachersListNew(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
  {
  	PrintOnConsole.getJFTPrint("JFT:92");	
  	 List<String[]> lstJobForTeacher=new ArrayList<String[]>();
  	 if(lstTeacherDetails.size()>0){
	         try{
	               Session session = getSession();
	               Criteria criteria = session.createCriteria(getPersistentClass());             
	               criteria.setProjection( Projections.projectionList()
	                       .add( Projections.property("teacherId.teacherId"), "teachId" )
	                       .add( Projections.count("teacherId") )              
	                       .add( Projections.groupProperty("teacherId") )
	                   );
	               Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
	              /* if(districtMaster!=null && entityID!=1){
	            	     criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
	                     //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
	               }*/
	               
	               if(entityID!=1){
						if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()==1)
						{
							Criteria jobOrderSet= criteria.createCriteria("jobId");
							if(districtMaster!=null){
								jobOrderSet.add(Restrictions.eq("districtMaster",districtMaster));
							}else{
								//jobOrderSet.add(Restrictions.isNull("districtMaster"));
							}
							if(branchMaster!=null){
								jobOrderSet.add(Restrictions.eq("branchMaster",branchMaster));
							}else{
								//jobOrderSet.add(Restrictions.isNull("branchMaster"));
							}
							if(headQuarterMaster!=null){
								if(headQuarterMaster.getHeadQuarterId()==1){
								   jobOrderSet.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
								}
							}else{
								//jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
							}
						}else
							criteria.add(Restrictions.eq("districtId",districtMaster.getDistrictId()));
						}
	               if(lstTeacherDetails.size()>0){
	                     criteria.add(criterionTeacher);
	               }
	              // criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
	               criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
	               lstJobForTeacher = criteria.list() ;
	         }catch (Throwable e) {
	               e.printStackTrace();
	         }   
  	 }
       return lstJobForTeacher;
  }

  
  	@Transactional(readOnly=false)
	 public List<TeacherDetail> getAllTeacherByStatus(List<StatusMaster> statusMasterList,DistrictMaster districtMaster,List<TeacherDetail> teacherDetail) 
	 {
		 System.out.println(":::::: getAllTeacherByStatus :::::::::::");
		 List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
		 
		 try{
			 	Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				Criterion criterion1 = Restrictions.in("status", statusMasterList);
				Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
				Criterion criterion3 = Restrictions.not(Restrictions.in("teacherId", teacherDetail));
				
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				
				criteria.setProjection(Projections.groupProperty("teacherId"));
				teacherDetailList = criteria.list();
			 
		 } catch(Exception exception) {
			 exception.printStackTrace();
		 }
		 
		 return teacherDetailList;
	 }
  	
  	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherInDesc(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				lstJobForTeacher = findByCriteria(Order.desc("jobForTeacherId"),criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
  	
  	@Transactional(readOnly=false)
	 public List<JobForTeacher> getTeacherByStatusAndSecondaryStatus(List<TeacherDetail> teacherDetailsList,DistrictMaster districtMaster,List<SecondaryStatus> secondaryStatusList) 
	 {
  		List<JobForTeacher> jobForTeacherList = new ArrayList<JobForTeacher>();
  		
  		String[] statuss = {"hird","widrw","hide","vcomp"};
  		//List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);
  		List<StatusMaster> statusMasters = Utility.getStaticMasters(statuss);
  		try{
  			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.in("teacherId",teacherDetailsList);
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.not(Restrictions.in("status", statusMasters));
			Criterion criterion4 = Restrictions.not(Restrictions.in("secondaryStatus", secondaryStatusList));
  			
			if(teacherDetailsList!=null && teacherDetailsList.size()>0)
				criteria.add(criterion1);
			
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			
			//criteria.setProjection(Projections.groupProperty("teacherId"));
			jobForTeacherList = criteria.list();
			
  		} catch(Exception exception){
  			exception.printStackTrace();
  		}
  		
  		return jobForTeacherList;
	 }
  	
  	@Transactional(readOnly=false)
	 public List<TeacherDetail> getTeacherByStatusAndSecondaryStatusBackUp(DistrictMaster districtMaster,List<SecondaryStatus> secondaryStatusList) 
	 {
 		
 		List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
 		
 		String[] statuss = {"hird","widrw","hide","vcomp"};
 		//List<StatusMaster> statusMasters =  statusMasterDAO.findStatusByShortNames(statuss);
 		List<StatusMaster> statusMasters = Utility.getStaticMasters(statuss);
 		try{
 			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion2 = Restrictions.not(Restrictions.in("status", statusMasters));
			Criterion criterion3 = Restrictions.not(Restrictions.in("secondaryStatus", secondaryStatusList));
 			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			//criteria.setProjection(Projections.groupProperty("teacherId"));
			teacherDetailList = criteria.list();
			
 		} catch(Exception exception){
 			exception.printStackTrace();
 		}
 		
 		return teacherDetailList;
	 }
  	
  	@Transactional(readOnly=false)
    public Map<Integer,String> getJobForTeacherByJobAndTeachersListWithHQandBRSQL(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
    {
      System.out.println(":::::::::::::;getJobForTeacherByJobAndTeachersListWithHQandBR  method::::::::::::::::");
      PrintOnConsole.getJFTPrint("JFT:92");     
       
      Session session = getSession();
	  String sql ="";
	  Query query = null;
	
	  
          if(districtMaster!=null){
        	  sql = "SELECT jft.teacherId,COUNT(IF(jft.districtId="+districtMaster.getDistrictId()+",1,NULL)) AS districtApplied ,COUNT(jft.teacherId) allApplied from jobforteacher jft where " +
        	  		" jft.teacherId IN (:teacherDetails) and jft.status IN(:statuss) group by jft.teacherId";
          }else if(headQuarterMaster!=null){
        	  sql = "SELECT jft.teacherId, COUNT(IF(jo.headQuarterId="+headQuarterMaster.getHeadQuarterId()+",1,NULL)) AS districtApplied ,COUNT(jft.teacherId) allApplied " +
        	  		" from jobforteacher jft,joborder jo WHERE jo.jobId = jft.jobId  and " +
  	  		" jft.teacherId IN (:teacherDetails) and jft.status IN(:statuss) group by jft.teacherId";
          }else if(branchMaster!=null && headQuarterMaster!=null){
        	  sql = "SELECT jft.teacherId, COUNT(IF(branch.branchId="+branchMaster.getBranchId()+",1,NULL)) AS districtApplied ,COUNT(IF(jo.headQuarterId="+headQuarterMaster.getHeadQuarterId()+",1,NULL)) allApplied  " +
        	  		" from jobforteacher jft,joborder jo,branchmaster branch WHERE jo.jobId = jft.jobId AND branch.branchId=jo.branchId and " +
  	  		" jft.teacherId IN (:teacherDetails) and jft.status IN(:statuss) and jo.headQuarterId="+headQuarterMaster.getHeadQuarterId()+" group by jft.teacherId";
          }else
          {
          	sql = "SELECT jft.teacherId,COUNT(jft.teacherId) AS districtApplied ,COUNT(jft.teacherId) allApplied from jobforteacher jft where " +
        		" jft.teacherId IN (:teacherDetails) and jft.status IN(:statuss) group by jft.teacherId";
          }
          
	  query = session.createSQLQuery(sql);
	  
	  query.setParameterList("teacherDetails", lstTeacherDetails);
	  query.setParameterList("statuss", statusMasterList);
	  System.out.println("sql::::::::::::::: "+sql);
	  List<Object[]> rows = query.list();
	  Map<Integer,String> map = new HashMap<Integer, String>();
	  if(rows.size()>0)
	  {
		  for(Object oobz: rows){

			  Object obj[] = (Object[])oobz;
			  //System.out.println(obj[0]+" "+obj[1]+"###"+obj[2]);
			  map.put((Integer)obj[0], obj[1]+"###"+obj[2]);
		  }
	  }
	  return map;
    }
  	@Transactional(readOnly=false)
	public List findRecordForNotReviewdCandidate(List<TeacherDetail> teacherDetailsist)
	{
		List lstJobForTeacher= new ArrayList();
		try 
		{
			Session session = getSession();
			Criterion criterion = Restrictions.in("teacherId", teacherDetailsist);		
			Criteria c= session.createCriteria(getPersistentClass());

			c.add(criterion).setProjection( Projections.projectionList()
					.add(Projections.property("teacherId.teacherId"))
					.add(Projections.property("notReviewedFlag"))
					.add(Projections.property("lastActivity"))
					.add(Projections.property("secondaryStatus.secondaryStatusId")));
		
			lstJobForTeacher = c.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
  	 @Transactional(readOnly=false)
		public List<Integer> getAllQQFinalizeTeacherList_Op(DistrictMaster districtMaster,List<Integer> teachersId)
		{
	    	PrintOnConsole.getJFTPrint("JFT:96");	
			List<Integer> teacherDetailList = new ArrayList<Integer>();
			if(teachersId!=null && teachersId.size()>0)
				if(districtMaster!=null){
					try{
						Session session = getSession();
						Criteria criteria = session.createCriteria(getPersistentClass());
					    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
					    criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					    
					    criteria.add(Restrictions.in("teacherId.teacherId", teachersId));
						criteria.setProjection(Projections.property("teacherId.teacherId")); 
						criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
					    teacherDetailList = criteria.list();
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			return teacherDetailList;
		}
  	 
  	 @Transactional(readOnly=false)
		public List<Integer> findTeacersIdByDistrict(UserMaster userMaster)
		{
			PrintOnConsole.getJFTPrint("JFT:38");
			List<Integer> lstJobForTeacher=new ArrayList<Integer>();
			List<Integer> jobsId = new ArrayList<Integer>();
			Integer entityType = 0;
			if(userMaster!=null)
				entityType = userMaster.getEntityType();
			
			Criterion mainCriterion = null;
			
			switch (entityType) {
			
			case 1:
			{
				// for TM Admin
				 
				
				
			}
				 break;
			case 2:
			{
				// for District Admin
				mainCriterion = Restrictions.eq("districtId", userMaster.getDistrictId().getDistrictId());
			}
				 break;
			case 3:
			{
				// For School Admin
				jobsId = schoolInJobOrderDAO.findJobsIdByCriterion(Restrictions.eq("schoolId", userMaster.getSchoolId()));
				if(jobsId!=null){
				jobsId.add(0);
				mainCriterion = Restrictions.in("jobId.jobId", jobsId);
				}
				
			}
				 break;
			
			case 5:
			{
				// for HeadQuarter Admin
				jobsId = jobOrderDAO.findJobsIdByCriterion(Restrictions.eq("headQuarterMaster", userMaster.getHeadQuarterMaster()));
				if(jobsId!=null){
				jobsId.add(0);
				mainCriterion = Restrictions.in("jobId.jobId", jobsId);
				}
			}
				 break;
				 
			case 6:
			{
				// For Branch Admin
				
				jobsId = jobOrderDAO.findJobsIdByCriterion(Restrictions.eq("branchMaster", userMaster.getBranchMaster()));
				if(jobsId!=null){
				jobsId.add(0);
				mainCriterion = Restrictions.in("jobId.jobId", jobsId);
				}
				
			}
				 break;
				
	
				

			default:
				break;
			}
			
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(mainCriterion!=null)
				criteria.add(mainCriterion); 
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				criteria.setCacheable(true); 
				lstJobForTeacher = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}
  	 
  	 @Transactional(readOnly=false)
		public List<Integer> getJobForTeacherByStatus_Op(List<Integer> lstJobOrder)
		{
			List<Integer> lstJobForTeacher= new ArrayList<Integer>();
			
			if(lstJobOrder!=null && lstJobOrder.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria= session.createCriteria(getPersistentClass()); 
			//	Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.ne("status",statusMaster);
				Criterion criterion2 = Restrictions.in("jobId.jobId",lstJobOrder);
	            
				criteria.add(criterion1);
				criteria.add(criterion2);
				
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId"))); 
				lstJobForTeacher = criteria.list();
				//lstJobForTeacher = findByCriteria(criterion1,criterion2);		
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}
  	 
  	 @Transactional(readOnly=false)
		public List<Integer> findTeachersBySubjectList_Op(List<SubjectMaster> subjectMasters,List<Integer> teachersId)
		{
			PrintOnConsole.getJFTPrint("JFT:31");
			List<Integer> results = new ArrayList<Integer>();
			
			if(teachersId!=null && teachersId.size()>0)
				try 
				{
					Session session = getSession();
					
					if(subjectMasters.size()>0)
					{
						Criteria criteria= session.createCriteria(getPersistentClass()); 
						StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
						criteria.add(Restrictions.ne("status",statusMaster));
						
						Criteria incriteria = criteria.createCriteria("jobId");
						incriteria.add(Restrictions.in("subjectMaster",subjectMasters));
						 criteria.add(Restrictions.in("teacherId.teacherId", teachersId));
							criteria.setProjection(Projections.property("teacherId.teacherId")); 
							criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId"))); 
						results = criteria.list();
					}
					 
				}catch (Exception e) {
					e.printStackTrace();
				}		
			return results;
		}
  	 
 	@Transactional(readOnly=false)
	public List<Integer> getTeacherDetailByDASARecordsBeforeDateNew_Op(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<Integer> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<Integer> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,StatusMaster statusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:73");	
		List<Integer> lstTeacherDetail= new ArrayList<Integer>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			try{ 
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
		}
		String[] candidatetyp=null;
		if(internalCandVal!=null){
				candidatetyp=internalCandVal.split("|");
		}
		
		List<Integer> candidatetypec=new ArrayList<Integer>();
		if(candidatetyp!=null){
			for(String s:candidatetyp){
				if(s.equalsIgnoreCase("0")){
					candidatetypec.add(1);
					candidatetypec.add(5);
				}else{
					if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
						int val=Integer.parseInt(s);
						candidatetypec.add(val);
					}
					
				}
			}
		}
		System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
		if(candidatetypec.size()>0){
			criteria.add(Restrictions.in("isAffilated",candidatetypec));
		}
		
		
		/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
		if(internalCandVal==1){
			criteria.add(criterionInterCand);
		}*/
		boolean flag=true;
		boolean dateDistrictFlag=false;
		
		if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
			}
			if(status)
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
			else
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			
			//job applied flag
			
			/*if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			else if(daysVal==5 && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
			else if(appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));*/
			
			if(appliedfDate!=null && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			else if(appliedfDate!=null && appliedtDate==null)
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			else if(appliedtDate!=null && appliedfDate==null)
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			
			if(teacherFlag && filterTeacherList!=null && filterTeacherList.size() >0)
				criteria.add(Restrictions.in("teacherId.teacherId", filterTeacherList));	
			
			if(nByAflag && NbyATeacherList!=null &&NbyATeacherList.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", NbyATeacherList)));	
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				}
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				lstTeacherDetail =  criteria.list();
			}
			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
 	
 	@Transactional(readOnly=false)
	public List<Integer> getTeacherDetailByDASARecordsL1_Op(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<Integer> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<Integer> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,StatusMaster statusMaster,List<Integer> lstTeacherDetailAllFilter)
	{
		PrintOnConsole.getJFTPrint("JFT:74");	
		List<Integer> lstTeacherDetail= new ArrayList<Integer>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

		
		try{
			if(Utility.getValueOfPropByKey("basePath").contains("platform")){
				//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
				JobOrder jobOrder = new JobOrder();
				 jobOrder.setJobId(11136);
				 
				if(jobOrder!=null)
				criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		String[] candidatetyp=null;
		if(internalCandVal!=null){
				candidatetyp=internalCandVal.split("|");
		}
		List<Integer> candidatetypec=new ArrayList<Integer>();
		if(candidatetyp!=null){
			for(String s:candidatetyp){
				if(s.equalsIgnoreCase("0")){
					candidatetypec.add(1);
					candidatetypec.add(5);
				}else{
					if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
						int val=Integer.parseInt(s);
						candidatetypec.add(val);
					}
					
				}
			}
		}
		System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
		if(candidatetypec.size()>0){
			criteria.add(Restrictions.in("isAffilated",candidatetypec));
		}
		/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
		if(internalCandVal==1){
			criteria.add(criterionInterCand);
		}*/
		boolean flag=true;
			boolean dateDistrictFlag=false;
			
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
			}
			if(status)
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
			else
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			
			//job applied flag
			
			if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			else if(daysVal==5 && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
			else if(appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			
			filterTeacherList.addAll(lstTeacherDetailAllFilter);
			
			//if(teacherFlag && filterTeacherList!=null && filterTeacherList.size() >0)
			if(filterTeacherList!=null && filterTeacherList.size() >0)
				criteria.add(Restrictions.in("teacherId.teacherId", filterTeacherList));	
			
			if(nByAflag && NbyATeacherList!=null &&NbyATeacherList.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", NbyATeacherList)));	
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				}
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				lstTeacherDetail =  criteria.list();
			}
			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	 @Transactional(readOnly=false)
		public List<TeacherDetail> getTeacherDetailByDASARecordsDataOp_Op(Order sortOrderStrVal,int start,int end,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<Integer> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<Integer> lstTeacherDetailAllFilter,boolean nByAflag,List<Integer> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,boolean normScoreFlag,List<StatusMaster> lstStatusMastersNew)
		{

			
			PrintOnConsole.getJFTPrint("JFT:71");
			
			List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
		
				try{
					if(Utility.getValueOfPropByKey("basePath").contains("platform")){
						//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
						JobOrder jobOrder = new JobOrder();
						 jobOrder.setJobId(11136);
						 
						if(jobOrder!=null)
						criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				System.out.println("inside 1");
		String[] candidatetyp=null;
		if(internalCandVal!=null){
				candidatetyp=internalCandVal.split("|");
		}
		
		List<Integer> candidatetypec=new ArrayList<Integer>();
		if(candidatetyp!=null){
			for(String s:candidatetyp){
				if(s.equalsIgnoreCase("0")){
					candidatetypec.add(1);
					candidatetypec.add(5);
				}else{
					if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
						int val=Integer.parseInt(s);
						candidatetypec.add(val);
					}
					
				}
			}
		}
		System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
		if(candidatetypec.size()>0){
			criteria.add(Restrictions.in("isAffilated",candidatetypec));
		}
		
		/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
		if(internalCandVal==1){
			criteria.add(criterionInterCand);
		}*/
		System.out.println("inside 2");
				Criterion criterionSchool =null;
				if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
					criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
				}
				
				criteria.add(Restrictions.ne("status",statusMaster));
				if(lstStatusMastersNew.size()>0)
					criteria.add(Restrictions.in("status",lstStatusMastersNew));
				
				
				if(fDate!=null && tDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				else if(fDate!=null && tDate==null)
					criteria.add(Restrictions.ge("createdDateTime",fDate));
				else if(tDate!=null && fDate==null)
					criteria.add(Restrictions.le("createdDateTime",tDate));
				

				boolean flag=false;
				boolean dateDistrictFlag=false;
				if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
					criteria.add(criterion1);
				}
				System.out.println("inside 3");
				if((entityID==3 || entityID==4)){
					flag=true;
					if(lstJobOrder.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
						criteria.add(criterion1);
					}
					if(entityID==4 && dateFlag==false){
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					}else if(entityID==4 && dateFlag){
						dateDistrictFlag=true;
						/*if(fDate!=null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
						}else if(fDate!=null && tDate==null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
						}else if(fDate==null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
						}*/
						if(lstJobOP!=null && lstJobOP.size()>0)
						{
							Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
							criteria.add(criterion1);
						}
					}
				}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
					flag=true;
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(dateFlag && districtMaster!=null &&  entityID==2){
					flag=true;
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}*/
					
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
					
					dateDistrictFlag=true;
				}
				else if(entityID==5)
				{

					System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
					flag=true;
					if(branchMaster!=null)
						criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
					else
						criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));

						}
						else if(entityID==6)
						{
							System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
							flag=true;
							criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
						}
						if(dateDistrictFlag==false && dateFlag)
						{
							/*if(fDate!=null && tDate!=null){
								criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
							}else if(fDate!=null && tDate==null){
								criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
							}else if(fDate==null && tDate!=null){
								criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
							}*/
							
							if(lstJobOP!=null && lstJobOP.size()>0)
							{
								Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
								criteria.add(criterion1);
							}
							
							flag=true;
						}
						System.out.println("inside 6");
						if(status){
							criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
							if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
								criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
						}
						System.out.println("inside 7");
						//job applied flag
						if(newcandidatesonlyNew)
						{
							if(appliedfDate!=null && appliedtDate!=null)
							{
								System.out.println("############### newcandidatesonlyNew FT #######################");
								criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
							}
							else if(appliedfDate!=null && appliedtDate==null)
							{
								System.out.println("############### newcandidatesonlyNew F #######################");
								//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
								criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
							}
							else if(appliedtDate!=null && appliedfDate==null)
							{
								System.out.println("############### newcandidatesonlyNew T #######################");
								criteria.add(Restrictions.le("createdDateTime",appliedtDate));
							}
						}
						else
						{
							System.out.println("inside 8");
							if(appliedfDate!=null && appliedtDate!=null)
								criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
							else if(appliedfDate!=null && appliedtDate==null)
								criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
							else if(appliedtDate!=null && appliedfDate==null)
								criteria.add(Restrictions.le("createdDateTime",appliedtDate));
							System.out.println("inside 9");
						}
						
						flag=true;
						if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
						{
							criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", lstTeacherDetailAllFilter))); // filter first time
						}
						System.out.println("inside 10");
						if(teacherFlag){
							flag=true;
							criteria.add(Restrictions.in("teacherId.teacherId", filterTeacherList));	
						}
						if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size() >0){
							criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", NbyATeacherList)));	
						}
						System.out.println("inside 11");
						if(districtMaster!=null  && utype==3)
						{
							StatusMaster master = districtMaster.getStatusMaster();
							SecondaryStatus smaster = districtMaster.getSecondaryStatus();
							if(master!=null)
							{
								System.out.println("master: "+master.getStatus());
								//String sts = master.getStatus();
								String sts = master.getStatusShortName();
								if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
								{
									List<StatusMaster> sList = new ArrayList<StatusMaster>();
									sList.add(master);
									if(sts.equals("scomp"))
									{
										StatusMaster st1 = new StatusMaster();
										st1.setStatusId(17);
										sList.add(st1);
										
										StatusMaster st2 = new StatusMaster();
										st2.setStatusId(18);
										sList.add(st2);
										
									}else if(sts.equals("vcomp"))
									{
										StatusMaster st2 = new StatusMaster();
										st2.setStatusId(18);
										sList.add(st2);
									}
									
									/*
									6 - Hired
									19 - Declined
									10 - rejected
									7 -- Withdrew
									*/
									
									StatusMaster stHird = new StatusMaster();
									stHird.setStatusId(6);
									sList.add(stHird);

							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
							
							criteria.add(Restrictions.in("status",sList));
						}else
						{
							List<StatusMaster> sList = new ArrayList<StatusMaster>();
							sList.add(master);
							if(sts.equals("hird"))
							{
								StatusMaster stDeclined = new StatusMaster();
								stDeclined.setStatusId(19);
								sList.add(stDeclined);
								
								StatusMaster stRejected = new StatusMaster();
								stRejected.setStatusId(10);
								sList.add(stRejected);
								
								StatusMaster stWithdrew = new StatusMaster();
								stWithdrew.setStatusId(7);
								sList.add(stWithdrew);
							}else if(sts.equals("dcln"))
							{
								StatusMaster stRejected = new StatusMaster();
								stRejected.setStatusId(10);
								sList.add(stRejected);
								
								StatusMaster stWithdrew = new StatusMaster();
								stWithdrew.setStatusId(7);
								sList.add(stWithdrew);

							}else if(sts.equals("rem"))
							{
								StatusMaster stWithdrew = new StatusMaster();
								stWithdrew.setStatusId(7);
								sList.add(stWithdrew);
							}
							criteria.add(Restrictions.in("status",sList));
							//criteria.add(Restrictions.eq("status",master));
						}
					}
					if(smaster!=null)
					{
						System.out.println("smaster: "+smaster.getSecondaryStatusName());
						Criteria c = criteria.createCriteria("secondaryStatus");
						
						Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
						if(!stq.equals(""))
						{
							Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
							Criterion c3 = Restrictions.or(c2, criterion1);
							c.add(c3);
						}else
							c.add(c2);
							}
						}
						System.out.println("inside 12  ");
						if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
							if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
							{
								criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
								.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
								.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
								 
							}
							criteria.setProjection(Projections.groupProperty("teacherId"));
							if(!normScoreFlag){
								criteria.setFirstResult(start);
								criteria.setMaxResults(end);	
							}
							
					try {
						criteria.setFetchMode("teacherId", FetchMode.SELECT);
						criteria.setFetchMode("jobId", FetchMode.SELECT);
						criteria.setFetchMode("status", FetchMode.SELECT);
						criteria.setFetchMode("updatedBy", FetchMode.SELECT);					
						criteria.setFetchMode("districtSpecificNoteFinalizeONBBy", FetchMode.SELECT);
						criteria.setFetchMode("userMaster", FetchMode.SELECT);
						criteria.setFetchMode("statusMaster", FetchMode.SELECT);
						criteria.setFetchMode("secondaryStatus", FetchMode.SELECT);
						criteria.setFetchMode("internalStatus", FetchMode.SELECT);
						criteria.setFetchMode("internalSecondaryStatus", FetchMode.SELECT);
						criteria.setFetchMode("schoolMaster", FetchMode.SELECT);
						
					
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println("13");
					
					lstTeacherDetail =  criteria.list();
				}else{
					lstTeacherDetail=new ArrayList<TeacherDetail>();
				}
				System.out.println("inside 15");
				if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
					criteria.add(criterionSchool);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstTeacherDetail;
		
}   
	 @Transactional(readOnly=false)
	 	public JobForTeacher getJFTByTeacherIdAndJobId(Integer teacherId,Integer jobId)
	 	{
			JobForTeacher jobForTeacher=null;
	 		Session session = getSession();
	 		try{
	 			Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("teacherId", "td").createAlias("jobId", "jo");
	 			criteria.add(Restrictions.eq("td.teacherId",teacherId));
	 			criteria.add(Restrictions.eq("jo.jobId",jobId));
				List<JobForTeacher> list=criteria.list();
				if(list.size()>0)
				jobForTeacher = list.get(0);
	 		}catch (Exception e) {
				e.printStackTrace();
			}
	 		return jobForTeacher;
	 	}
}