package tm.services.teacher;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.context.support.WebApplicationContextUtils;

import edu.emory.mathcs.backport.java.util.concurrent.ExecutorService;
import edu.emory.mathcs.backport.java.util.concurrent.Executors;

import tm.bean.DistrictPortfolioConfig;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherPreference;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.WorkFlowStatus;
import tm.bean.master.CertificateTypeMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.mq.MQEvent;
import tm.bean.mq.MQEventHistory;
import tm.bean.teacher.SpInboundAPICallRecord;
import tm.bean.user.UserMaster;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherCertificateDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.WorkFlowStatusDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.mq.MQEventHistoryDAO;
import tm.dao.teacher.SpInboundAPICallRecordDAO;
import tm.services.CommonDashboardAjax;
import tm.services.CommonService;
import tm.services.PaginationAndSorting;
import tm.services.district.PrintOnConsole;
import tm.services.es.ElasticSearchService;
import tm.services.report.CandidateGridService;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class DashboardAjax {
	
	private static final Logger logger = Logger.getLogger(DashboardAjax.class.getName());
	private static final int MYTHREADS = 1000;
//sandeep	
	 public static int NC_HEADQUARTER=2;
	
	 String locale = Utility.getValueOfPropByKey("locale");
	 String lblPersonalPlng=Utility.getLocaleValuePropByKey("lblPersonalPlng", locale);
	 String lblJoOfInterest=Utility.getLocaleValuePropByKey("lblJoOfInterest", locale);
	 String lblStatus=Utility.getLocaleValuePropByKey("lblStatus", locale);
	 String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
	 String lblDistrictName=Utility.getLocaleValuePropByKey("lblDistrictName", locale);
	 String headCom=Utility.getLocaleValuePropByKey("headCom", locale);
	 String lblJoApp=Utility.getLocaleValuePropByKey("lblJoApp", locale);
	 String lblJobPref=Utility.getLocaleValuePropByKey("lblJobPref", locale);
	 String lblPwrFrof=Utility.getLocaleValuePropByKey("lblPwrFrof", locale);
	 String lblCompleted=Utility.getLocaleValuePropByKey("lblCompleted", locale);
	 String optIncomlete=Utility.getLocaleValuePropByKey("optIncomlete", locale);
	 String lblPortfolio=Utility.getLocaleValuePropByKey("lblPortfolio", locale);
	 String lblJobSpeInve=Utility.getLocaleValuePropByKey("lblJobSpeInve", locale);
	 String lblPortfolioReport=Utility.getLocaleValuePropByKey("lblPortfolioReport", locale);
	 String lblForCertiPosiOnly=Utility.getLocaleValuePropByKey("lblForCertiPosiOnly", locale);
	 String lblNotRequired=Utility.getLocaleValuePropByKey("lblNotRequired", locale);
	 String lblSeeAll=Utility.getLocaleValuePropByKey("lblSeeAll", locale);
	 String lblJoTil=Utility.getLocaleValuePropByKey("lblJoTil", locale);
	 String lblExpiryDate=Utility.getLocaleValuePropByKey("lblExpiryDate", locale);
	 String lblZone=Utility.getLocaleValuePropByKey("lblZone", locale);
	 String lblSchool=Utility.getLocaleValuePropByKey("lblSchool", locale);
	 String lblAdd=Utility.getLocaleValuePropByKey("lblAdd", locale);
	 String lblJoStatus=Utility.getLocaleValuePropByKey("lblJoStatus", locale);
	 String lblAvailable=Utility.getLocaleValuePropByKey("lblAvailable", locale);
	 String btnUpdate=Utility.getLocaleValuePropByKey("btnUpdate", locale);
	 String lblPowProfIndex=Utility.getLocaleValuePropByKey("lblPowProfIndex", locale);
	 String lblApplyNow=Utility.getLocaleValuePropByKey("lblApplyNow", locale);
	 String lblhide=Utility.getLocaleValuePropByKey("lblhide", locale);
	 String btnShare=Utility.getLocaleValuePropByKey("btnShare", locale);
	 String lnkV=Utility.getLocaleValuePropByKey("lnkV", locale);
	 String lblWithdraw=Utility.getLocaleValuePropByKey("lblWithdrew", locale);
	 String headCoverLetr=Utility.getLocaleValuePropByKey("headCoverLetr", locale);
	 String lblCompleteNow=Utility.getLocaleValuePropByKey("lblCompleteNow", locale);
	 String lblQQ=Utility.getLocaleValuePropByKey("lblQQ", locale);
	 String msgYrSesstionExp=Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale);
	 String msgForProfessionalInventoryTm=Utility.getLocaleValuePropByKey("msgForProfessionalInventoryTm", locale);
	 String lblEPI=Utility.getLocaleValuePropByKey("lblEPI", locale);
	 String btnResume=Utility.getLocaleValuePropByKey("btnResume", locale);
	 String toolUpdateProf=Utility.getLocaleValuePropByKey("toolUpdateProf", locale);
	 String lblYouhave=Utility.getLocaleValuePropByKey("lblYouhave", locale);
	 String msgIncJobSpeInvt=Utility.getLocaleValuePropByKey("msgIncJobSpeInvt", locale);
	 String lblUntilfilled=Utility.getLocaleValuePropByKey("lblUntilfilled", locale);
	 String lnkManySchools=Utility.getLocaleValuePropByKey("lnkManySchools", locale);
	 String lblManyAddress=Utility.getLocaleValuePropByKey("lblManyAddress", locale);
	 String toolJSI=Utility.getLocaleValuePropByKey("toolJSI", locale);
	 String optWithdrawn=Utility.getLocaleValuePropByKey("optWithdrawn", locale);
	 String toolRe_Apply=Utility.getLocaleValuePropByKey("toolRe_Apply", locale);
	 String optTOut=Utility.getLocaleValuePropByKey("optTOut", locale);
	 String optHid=Utility.getLocaleValuePropByKey("optHid", locale);
	 String lblUn_hide=Utility.getLocaleValuePropByKey("lblUn_hide", locale);
	 String msgNorecordfound=Utility.getLocaleValuePropByKey("msgNorecordfound", locale);
	 String btnComp=Utility.getLocaleValuePropByKey("btnComp", locale);
	 String lblNotAppliedand=Utility.getLocaleValuePropByKey("lblNotAppliedand", locale);
	 
	 
	/*@Autowired
	private UserMasterDAO userMasterDAO;*/
	 
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	 
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	 
	@Autowired
	private JobCertificationDAO jobCertificationDAO;
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	
	@Autowired
	private CommonService commonService;
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	@Autowired
	private SpInboundAPICallRecordDAO spInboundAPICallRecordDAO;

	/*@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	 */
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO) 
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO(TeacherPreferenceDAO teacherPreferenceDAO) 
	{
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}

	/*@Autowired
	private FavTeacherDAO favTeacherDAO;
	public void setFavTeacherDAO(FavTeacherDAO favTeacherDAO) 
	{
		this.favTeacherDAO = favTeacherDAO;
	}*/

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) 
	{
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) 
	{
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) 
	{
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}

	/*@Autowired
	private JobShareDAO jobShareDAO;
	public void setJobShareDAO(JobShareDAO jobShareDAO) {
		this.jobShareDAO = jobShareDAO;
	}

	@Autowired
	private ServletContext servletContext;
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}*/

	@Autowired
	private CityMasterDAO cityMasterDAO;

	@Autowired
	private StateMasterDAO stateMasterDAO;

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;

	@Autowired
	private SubjectMasterDAO subjectMasterDAO;

	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;
	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	@Autowired
	private MQEventDAO mqEventDAO;
	
	@Autowired 
	private WorkFlowStatusDAO workFlowStatusDAO;
	
	@Autowired
	private MQEventHistoryDAO mqEventHistoryDAO;
	
	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true,10));
	}


	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String saveCoverLetter(Long jobForTeacherId,String coverLetter)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		JobForTeacher jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);
		jobForTeacher.setCoverLetter(coverLetter);
		jobForTeacherDAO.makePersistent(jobForTeacher);

		return "1";
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String getCoverLetter(Long jobForTeacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		JobForTeacher jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);


		return jobForTeacher.getCoverLetter();
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String getStateBeforeWithdraw(Long jobForTeacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String returnVal ="0";
		JobForTeacher jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);

		JobOrder jobOrder = jobForTeacher.getJobId();
		List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
		if(jobOrder.getIsJobAssessment()==true){
			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),jobOrder);
		}else{
			JobOrder jOrder = new JobOrder();
			jOrder.setJobId(0);
			teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),jOrder);
		}
		StatusMaster statusMaster = null;

		int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
		statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
		boolean completeFlag=false;
		try{
			String jftStatus=jobForTeacher.getStatus().getStatusShortName();
			if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
				completeFlag=true;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		if(statusMaster!=null){
			if(statusMaster.getStatusShortName().equalsIgnoreCase("icomp")){
				returnVal = "1";
			}else if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
				returnVal = "2";
			}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt")){
				returnVal = "3";
			}
		}

		if(statusMaster!=null)
		{
			TeacherStatusHistoryForJob lastSelectedObj=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryLastSelected(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());
			if(lastSelectedObj!=null && !lastSelectedObj.getStatusMaster().getStatusShortName().equals("hird")){
				statusMaster=lastSelectedObj.getStatusMaster();
			}
			CandidateGridService cgService=new CandidateGridService();
			jobForTeacher.setStatus(statusMaster);
			if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus())){
				jobForTeacher.setStatusMaster(null);
			}else{
				jobForTeacher.setStatusMaster(statusMaster);
			}
			jobForTeacherDAO.makePersistent(jobForTeacher);

			try{
				if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
					commonService.futureJobStatus(jobOrder, jobForTeacher.getTeacherId(),jobForTeacher);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return returnVal;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public String unHideJob(Long jobForTeacherId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		String returnVal ="0";
		JobForTeacher jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);

		returnVal = "1";

		jobForTeacherDAO.makeTransient(jobForTeacher);

		return returnVal;
	}


	/* === ============== getMilestonesGrid  ==================*/
	@Transactional(readOnly=false)
	public String getMilestonesGrid()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb =new StringBuffer();
		/*List<JobForTeacher> jobForTeacher=null;	
		jobForTeacher = jobForTeacherDAO.findWithLimit(Order.asc("status"), 0, 10);

		for(JobForTeacher jb: jobForTeacher)
		{
			sb.append(" "+jb.getTeacherId().getFirstName());
		}*/

		//HttpSession session = request.getSession();
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");		
		TeacherPreference tPreference = null;
		TeacherPortfolioStatus tPortfolioStatus;

		TeacherAssessmentStatus teacherBaseAssessmentStatus = null;
		TeacherAssessmentStatus teacherSPAssessmentStatus = null;

		List<JobForTeacher> lstJBSIncompJobForTeacher = null;
		List<JobForTeacher> lstJBSCompJobForTeacher = null;
		List<JobForTeacher> lstJBSViolatedJobForTeacher = null;

		int noOfJBSIncompJobForTeacher = 0;
		int noOfJBSCompJobForTeacher = 0;
		int noOfJBSViolatedJobForTeacher = 0;

		boolean prefStatus = false;
		boolean portfolioStatus = false;
		boolean baseInvStatus = false;
		boolean SPInvStatus = false;
		String baseStatus="";
		String SPStatus="";


		try 
		{
			StatusMaster statusComplete =statusMasterDAO.findStatusByShortName("comp");
			//StatusMaster statusInComplete =statusMasterDAO.findStatusByShortName("icomp");
			StatusMaster statusViolated =statusMasterDAO.findStatusByShortName("vlt");

			tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			if(tPreference!=null)
			{
				prefStatus = true;
			}

			tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted())
			{
				portfolioStatus = true;
			}

			teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
			{
				baseInvStatus=true;

			}
			if(teacherBaseAssessmentStatus!=null)
				baseStatus = teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName();

			lstJBSIncompJobForTeacher = getJobSpecInventory(request);
			lstJBSCompJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail, statusComplete);
			lstJBSViolatedJobForTeacher = jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail, statusViolated);

			if(lstJBSIncompJobForTeacher!=null)
			{
				noOfJBSIncompJobForTeacher = lstJBSIncompJobForTeacher.size();
			}
			if(lstJBSCompJobForTeacher!=null)
			{
				noOfJBSCompJobForTeacher = lstJBSCompJobForTeacher.size();
			}
			if(lstJBSViolatedJobForTeacher!=null)
			{

				for (JobForTeacher jobForTeacher : lstJBSViolatedJobForTeacher) {
					if(jobForTeacher.getJobId().getIsJobAssessment())
						noOfJBSViolatedJobForTeacher++;
				}
			}

			//PrintWriter pw = response.getWriter();			
			sb.append("<table width='100%' border='0' class='table table-striped' style='border-top-color:#007ab4;border-top-radius:0px;'>");
			sb.append("<div class='bucketheader' style='height:28px;margin-left: -1.01px;'></div>");
			sb.append("<thead class='bg'>");
			sb.append("<tr style='background-color: #007ab4;'>");
			sb.append("<th ><img class='fl' src='images/personal_planning_bucket.png' style='height:35px;width:35px;'> <div  class='subheading' style='color:white;font-size:13px;margin-left:45px;'>"+lblPersonalPlng+"</div></th>");
			sb.append("<th width='25%' style='vertical-align: middle;'>"+lblStatus+"</th>");
			sb.append("<th width='25%' style='vertical-align: middle;'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			//--Job Preferences row Start 
			sb.append("<tr>");
			sb.append("<td>"+lblJobPref+"</td>");
			sb.append("<td>");
			if(prefStatus)
			{
				sb.append("<strong class='text-success'>"+lblCompleted+"</strong>");				
			}
			else
			{
				sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
			}
			sb.append("</td>");
			sb.append("<td class=''>");
			sb.append("<a data-original-title="+btnUpdate+" rel='tooltip' id='iconpophover1' href='userpreference.do'>");
			sb.append("<img src='images/option08.png'style='width:21px; height:21px;'>");
			sb.append("</a>");
			sb.append("</td>");
			sb.append("</tr>");
			//--Job Preferences row End
			
			//--Power Profile row Start  /* Added By Ankit Sharma */
			//QuestAjax questAjax = new QuestAjax();
			// int score = questAjax.powerTracker(teacherID);
			//List<JobForTeacher> jobForTeacherList1= jobForTeacherDAO.findJobByTeacherNotKES(teacherDetail);
			//List<JobForTeacher> jobForTeacherListForKES1= jobForTeacherDAO.findJobByTeacherForKES(teacherDetail,false);
			boolean isKellyJobApply = false;
			isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
			boolean isKelly = false;
			if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
				isKelly = true;
			}
			
			if(!isKellyJobApply)
			{
				request.getSession().removeAttribute("powerProfile");
				sb.append("<tr>");
				sb.append("<td>"+lblPwrFrof+"</td>");
				
				sb.append("<td>");
				sb.append("<strong class='text-success'><div id='score'></div></strong>");
				sb.append("</td>");
				sb.append("<td class=''>");
				//sb.append("<a data-original-title='Power Profile Index' rel='tooltip' id='iconpophoverscore' href='powerprofile.do' target='_blank'>");
				sb.append("<a href='powerprofile.do' target='_blank' id='iconpophoverscore' rel='tooltip' data-original-title="+lblPowProfIndex+"><img src='images/option06.png' style='width:21px; height:21px;'></a>");
				sb.append("<script type='text/javascript'>$('#iconpophoverscore').tooltip();</script>");
				/*sb.append("<img src='images/option06.png'>");
				sb.append("</a>");*/
				sb.append("</td>");
				sb.append("</tr>");
				//--Power Profile row End
			}
			
			
			//--Portfolio row Start
			if(!isKellyJobApply)
			{
				sb.append("<tr>");
				sb.append("<td>"+lblPortfolio+"</td>");
				sb.append("<td>");

				String portfolioTooltip="";
				if(portfolioStatus)
				{
					portfolioTooltip=btnUpdate;
					sb.append("<strong class='text-success'>"+lblCompleted+"</strong>");
				}
				else
				{
					String portFolioStatus=lblCompleteNow;
					
					int i=0;
					List<JobForTeacher> jobForTeacherList= jobForTeacherDAO.findSortedJobByTeacher(Order.asc("jobForTeacherId"), teacherDetail);
					for(JobForTeacher jobForTeacher:jobForTeacherList)
					{
						if(i==0 )
						{
							if(jobForTeacher.getJobId().getHeadQuarterMaster()!=null)
							{
								portFolioStatus="Not Required";
								i++;
								continue;
							}
							else
								break;
								
						}
						
						
						//if(i!=0 && jobForTeacher.getJobId().getHeadQuarterMaster()==null && jobForTeacher.getStatus().getStatusId()==4)
						if(i!=0 && jobForTeacher.getJobId().getHeadQuarterMaster()==null )
						{
							portFolioStatus=lblCompleteNow;
							break;
						}
						
					}
					
					//shadab end
					portfolioTooltip=lblCompleteNow;
					if(portFolioStatus.equalsIgnoreCase("Incomplete"))
						sb.append("<strong class='text-error'>"+portFolioStatus+"</strong>");
					else
						sb.append("<strong class='text-success'>"+portFolioStatus+"</strong>");
				}
				sb.append("</td>");
				sb.append("<td class=''>");
				sb.append("<a data-original-title='"+portfolioTooltip+"' rel='tooltip' id='iconpophover2' href='portfolio.do'>");
				sb.append("<img src='images/option08.png' style='width:21px; height:21px;'>");
				sb.append("</a>");
				sb.append("</td>");
				sb.append("</tr>");
			}
			
			//--Portfolio row End

			//--Base Inventory start
			sb.append("<tr>");
			//sb.append("<td>Base Inventory</td>");
			String baseMsg=msgForProfessionalInventoryTm;
			if(isKellyJobApply || isKelly)
				baseMsg="Kelly Educational Staffing� (KES�) and TeacherMatch� share a common philosophy and believe high-quality substitute teachers are the most critical factor in fostering student achievement. We are proud to offer you � a valued KES substitute teacher candidate � exclusive access to the TeacherMatch Educator's Professional Inventory EPI�.   While developed specifically for full-time teaching candidates, taking the EPI with KES offers you a great opportunity to gain valuable insights into the essential teaching skill sets that are critical to success in the classroom.  The EPI is NOT used as a hiring assessment tool by KES, and your performance will NEVER be used as a qualifier or filter for you to be able to view or accept substitute teaching positions with KES school districts.  The EPI is for KES substitute teacher talent only.";
			
			sb.append("<td>"+Utility.getLocaleValuePropByKey("msgEducatorProfInvtry", locale)+"&nbsp;<a href='javascript:void(0);' id='iconpophoverBase' rel='tooltip' data-original-title=\""+baseMsg+"\"><img src='images/qua-icon.png' width='15' height='15' alt=''></a></td>");
			sb.append("<td>");
		
			Integer teacherId1=((TeacherDetail)session.getAttribute("teacherDetail")).getTeacherId();
			List <JobForTeacher> listjobsforTeacher=jobForTeacherDAO.findAllAppliedJobsOfTeacher(teacherId1);
            List<JobForTeacher> jobCategorys=null;
            List<JobForTeacher> jobCategorysSP=null;
		   
            if(listjobsforTeacher.size()>0)
			{
                jobCategorys=jobForTeacherDAO.getEpiStatusByTeacher(teacherId1);	
			}
			if(baseInvStatus)
			{
				sb.append("<strong class='text-success'>"+lblCompleted+"</strong>");
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{
				sb.append("<strong class='text-error'>"+optTOut+"</strong>");
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
			{
				sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
			}
			else
			{
				if(listjobsforTeacher==null||listjobsforTeacher.size()==0)
			    {
				   sb.append("<strong class='text-error' id='bStatus' >"+lblForCertiPosiOnly+"</strong>");
			    }else
			    {
			    	if(jobCategorys.size()==0 || jobCategorys==null){
			    		    sb.append("<strong class='text-error' id='bStatus' >"+lblNotRequired+"</strong>");
			    	}else{
				            sb.append("<strong class='text-error' id='bStatus' >"+optIncomlete+"</strong>");
			    	     }
			     }
			  }
			
			
			
			sb.append("</td>");
			sb.append("<td class='' id='bClick'>");
			if(prefStatus && portfolioStatus )
			{		
				String toolTipText = "";
				if(teacherBaseAssessmentStatus==null)
				{
					toolTipText = lblEPI;			
				}
				else if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{
					toolTipText = btnResume;				
				}

				if(baseInvStatus)
				{}
				else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				{}
				else
				{
					sb.append("<a data-original-title='"+toolTipText+"' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"openEpiInfo();\">");
					sb.append("<img src='images/option06.png' style='width:21px; height:21px;'>");
					sb.append("</a>");
				}		
			}
			else
			{

				if(((teacherBaseAssessmentStatus==null) ||(!baseInvStatus) && (!teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))))
				{
					Boolean epiStandalone = session.getAttribute("epiStandalone")==null?false:(Boolean)session.getAttribute("epiStandalone");
					if(epiStandalone)
						sb.append("<a data-original-title='"+lblEPI+"' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"checkInventory(0,null);\">");
					else if(isKellyJobApply || isKelly)
						sb.append("<a data-original-title='"+lblEPI+"' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"chkBaseInventoryStatus("+prefStatus+",true);\">");
					else
						sb.append("<a data-original-title='"+lblEPI+"' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"chkBaseInventoryStatus("+prefStatus+","+portfolioStatus+");\">");
					sb.append("<img src='images/option06.png' style='width:21px; height:21px;'>");
					sb.append("</a>");
				}
			}
			sb.append("</td>");
			sb.append("</tr>");
			//--Base Inventory end
			
			
			
			//List <JobForTeacher> listjobsforTeacherKelly=jobForTeacherDAO.findAllAppliedJobsOfTeacherKelly(teacherId1);
			List <JobForTeacher> listjobsforTeacherKelly=jobForTeacherDAO.getSPStatusByTeacher(teacherId1);
			if(listjobsforTeacherKelly!=null && listjobsforTeacherKelly.size()>0)
			{
				//Smart Practice Professional Development start
				//shadab
				TeacherDetail teacherDetail2=new TeacherDetail();
				teacherDetail2.setTeacherId(teacherId1);
				List<SpInboundAPICallRecord> spInboundAPICallRecordList= spInboundAPICallRecordDAO.getDetailsByTID(teacherDetail2);
				SpInboundAPICallRecord spInboundAPICallRecord=new SpInboundAPICallRecord();
				if(spInboundAPICallRecordList!=null && spInboundAPICallRecordList.size()>0)
				{
					spInboundAPICallRecord=spInboundAPICallRecordList.get(spInboundAPICallRecordList.size()-1);
				}
				sb.append("<tr>");
				sb.append("<td>SmartPractices Professional Development &nbsp;</td>");
				sb.append("<td>");
				
				/*if(spInboundAPICallRecord.getCurrentLessonNo()!=null && spInboundAPICallRecord.getTotalLessons()!=null 
						&& spInboundAPICallRecord.getCurrentLessonNo()>= spInboundAPICallRecord.getTotalLessons() )
				{
					sb.append("<strong class='text-success'>Unit "+spInboundAPICallRecord.getCurrentLessonNo()+" Complete</strong>");
				}
				else if(spInboundAPICallRecord.getCurrentLessonNo()!=null && spInboundAPICallRecord.getCurrentLessonNo()>0)
				{
					sb.append("<strong class='text-success'>Unit "+spInboundAPICallRecord.getCurrentLessonNo()+" In Progress </strong>");
				}
				else 
				{
					sb.append("<strong class='text-success'>Not Started</strong>");
				}*/	
				
				float percentage=0;
				if(spInboundAPICallRecord.getCurrentLessonNo()!=null && spInboundAPICallRecord.getTotalLessons()!=null && spInboundAPICallRecord.getCurrentLessonNo() >0 && spInboundAPICallRecord.getTotalLessons() >0)
				{
					int iCurrentLessonNo=spInboundAPICallRecord.getCurrentLessonNo();
					int iTotalLessons=spInboundAPICallRecord.getTotalLessons();
					
					try {
						percentage=(iCurrentLessonNo * 100/ iTotalLessons);
					} catch (Exception e) {
						e.printStackTrace();
					}
				    System.out.println("The percentage is = " + percentage + " %");
					sb.append("<strong class='text-success'>"+Math.round(percentage)+"% Complete</strong>");
				}
				else 
				{
					sb.append("<strong class='text-success'>Not Started</strong>");
				}
								
				sb.append("</td>");
				sb.append("<td class='' id='bClick'>");
					//sb.append("<a data-original-title='SPP' rel='tooltip' id='iconpophover3' href='javascript:void(0)' onclick=\"readLesson();\">");
					sb.append("<a data-original-title='SPD' rel='tooltip' id='iconpophoverSPD' href='spuserdashboard.do'\">");
					/*if(percentage<100)
						sb.append("<img src='images/unhide_grey.png' style='width:21px; height:21px;'></a>");
					else*/
						sb.append("<img src='images/option06.png' style='width:21px; height:21px;'></a>");
				sb.append("<script>$('#iconpophoverSPD').tooltip();</script>");
				sb.append("</td>");
				sb.append("</tr>");
				//Smart Practice Professional Development end
				
				//smart practice Assessment  start
				
				teacherSPAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForSP(teacherDetail);
				if(teacherSPAssessmentStatus!=null && (teacherSPAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
				{
					SPInvStatus=true;
				}
				if(teacherSPAssessmentStatus!=null)
					SPStatus = teacherSPAssessmentStatus.getStatusMaster().getStatusShortName();
				
				sb.append("<tr>");
				sb.append("<td>SmartPractices Assessment &nbsp;</td>");
				sb.append("<td>");
				jobCategorys=null;
				
				/*if(listjobsforTeacherKelly.size()>0)
				{
	                jobCategorysSP=jobForTeacherDAO.getSPStatusByTeacher(teacherId1);	
				}*/
				String status="";
				boolean showCertificate=false;
				String toolTipText = "";
				if(SPInvStatus)
				{
					if(teacherSPAssessmentStatus.getPass()!=null && teacherSPAssessmentStatus.getPass().equalsIgnoreCase("P"))
					{
						showCertificate=true;
						status="PASS";
						toolTipText = "Click to View Your Certificate.";
					}
					else if(teacherSPAssessmentStatus.getPass()!=null && teacherSPAssessmentStatus.getPass().equalsIgnoreCase("F"))
						status="FAIL";
					if(!status.equals(""))
						sb.append("<strong class='text-success'>Completed("+status+")</strong>");
					else
						sb.append("<strong class='text-success'>"+lblCompleted+"</strong>");
				}
				else if(teacherSPAssessmentStatus!=null && teacherSPAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
				{
					sb.append("<strong class='text-error'>"+Utility.getLocaleValuePropByKey("optTOut", locale)+"</strong>");
				}
				else if(teacherSPAssessmentStatus!=null && teacherSPAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{
					sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
				}
				else
				{
					sb.append("<strong class='text-success' id='bStatus' >Not Started</strong>");
				}
				
				sb.append("</td>");
				sb.append("<td class='' id='bClick'>");
					
				if(teacherSPAssessmentStatus==null)
				{
					toolTipText = "SP";			
				}
				else if(teacherSPAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{
					toolTipText = "Resume SP";				
				}
				
				if(percentage<100){
					sb.append("<a data-original-title='' rel='tooltip' id='iconpophoverSPA' href='javascript:void(0)'>");
					sb.append("<img src='images/unhide_grey.png' style='width:21px; height:21px;'>");
					sb.append("</a>");
				}
				
				if(spInboundAPICallRecord.getCurrentLessonNo()!=null && spInboundAPICallRecord.getTotalLessons()!=null  
						&& spInboundAPICallRecord.getTotalLessons() >0 
						&& spInboundAPICallRecord.getCurrentLessonNo().intValue() == spInboundAPICallRecord.getTotalLessons().intValue())
				{
					if(!showCertificate)
					{
						sb.append("<a data-original-title='"+toolTipText+"' rel='tooltip' id='iconpophoverSPA' href='javascript:void(0)' onclick=\"openSPInfo();\">");
						sb.append("<img src='images/option06.png' style='width:21px; height:21px;'>");
						sb.append("</a>");
						sb.append("<script>$('#iconpophoverSPA').tooltip();</script>");
					}
					else
					{
						//certificate to show TPL 1889
						//sb.append("<a data-original-title='"+toolTipText+"' rel='tooltip' id='iconpophover3' href='printcertificate.do' target='_blank' >");
						sb.append("<a data-original-title='"+toolTipText+"' rel='tooltip' id='iconpophoverSPA' href='javascript:void(0)' onclick=\"generateSkillPDF("+teacherDetail.getTeacherId()+");\">");
						sb.append("<span class='icon-book icon-large iconcolor'></span>");
						sb.append("</a>");
						sb.append("<script>$('#iconpophoverSPA').tooltip();</script>");
						//sb.append("<span class='icon-book icon-large iconcolor'></span>");
					}
				}
				
				sb.append("</td>");
				sb.append("</tr>");
				
				//smart practice Assessment end
				
			}
			

			String  className="text-success";
			String  jsiStatus=lblCompleted;
			int count=0;
			boolean vltCheck=false;
			boolean icompCheck=false;

			try{
				List<JobForTeacher> lstJobForTeacher = jobForTeacherDAO.findJobsForJobSpecificInventoryAll(teacherDetail,true);
				if(lstJobForTeacher.size()>0){
					List<JobOrder> jobOrderList=new ArrayList<JobOrder>();
					Map<Integer, AssessmentDetail> jobAss = new HashMap<Integer, AssessmentDetail>();
					for (JobForTeacher jobTeacher : lstJobForTeacher) {
						JobOrder jobOrder = jobTeacher.getJobId();
						if(jobOrder.getJobAssessmentStatus()==1)
						{
							if(jobTeacher.getIsAffilated()==null || jobTeacher.getIsAffilated()==0)
								jobOrderList.add(jobOrder);
							else if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0 && (jobTeacher.getIsAffilated()==1))
							{
								jobOrderList.add(jobOrder);
							}
						}
					}
					List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
					List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
					//System.out.println("jobOrderList:: "+jobOrderList.size());
					List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobOrderList);

					if(assessmentJobRelations1.size()>0){
						for(AssessmentJobRelation ajr: assessmentJobRelations1){
							adList.add(ajr.getAssessmentId());
						}
						if(adList.size()>0)
							lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentList(teacherDetail,adList);
					}

					for (TeacherAssessmentStatus teacherAssessmentStatus : lstJSI) {
						jobAss.put(teacherAssessmentStatus.getAssessmentDetail().getAssessmentId(),teacherAssessmentStatus.getAssessmentDetail());
						if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")){
							jsiStatus=optIncomlete;
							className="text-error";
							count=0;
							icompCheck=true;
							break;
						}else if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
							vltCheck=true;
						}else if(teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
							jsiStatus=lblCompleted;
							className="text-success";
							count++;
						}
					}
					if(icompCheck==false){
						for (AssessmentJobRelation jobRelObj : assessmentJobRelations1){
							AssessmentDetail assessmentDetail=jobAss.get(jobRelObj.getAssessmentId().getAssessmentId());
							if(assessmentDetail!=null){
								jsiStatus=lblCompleted;
								className="text-success";
							}else{
								jsiStatus=optIncomlete;
								className="text-error";
								icompCheck=true;
								break;
							}
						}
					}
					if(icompCheck==false &&  vltCheck){
						jsiStatus=optTOut;
						className="text-error";
					}else if(icompCheck==false && vltCheck==false){
						jsiStatus=lblCompleted;
						className="text-success";
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(!isKellyJobApply)
			{
				//--Job Specific Inventory start
				sb.append("<tr>");
				sb.append("<td>"+lblJobSpeInve+"</td>");
				sb.append("<td>");
				sb.append("<strong class='"+className+"'>"+jsiStatus+"</strong>");


				/*if(noOfJBSIncompJobForTeacher>0)
				{
					sb.append("<strong class='text-error'>Incomplete :"+jsiStatus+"</strong>");
				}
				else if(noOfJBSIncompJobForTeacher==0 && noOfJBSCompJobForTeacher==0 && noOfJBSViolatedJobForTeacher>0 )
				{
					sb.append("<strong class='text-error'>Timed Out :"+jsiStatus+"</strong>");
				}
				else
				{
					sb.append("<strong class='text-success'>Completed :"+jsiStatus+"</strong>");
				}*/
				sb.append("</td>");
				sb.append("<td class='' >");
				if(noOfJBSIncompJobForTeacher==0)
				{
					/*sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='iconpophover4' onclick=\"alert('You do not have any Job Specific Inventory pending')\">");
					sb.append("<img src='images/option02.png'>");
					sb.append("</a>");
					 */
				}
				else if(prefStatus == false || portfolioStatus == false )
				{
					JobForTeacher jft = lstJBSIncompJobForTeacher.get(0);
					//sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='iconpophover4' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+");\">");
					sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='iconpophover4' href='#' onclick=\"getTeacherCriteria("+jft.getJobId().getJobId()+");\">");
					sb.append("<img src='images/option02.png' style='width:21px; height:21px;'>");
					sb.append("</a>");
				}
				else if(noOfJBSIncompJobForTeacher==1)
				{
					JobForTeacher jft = lstJBSIncompJobForTeacher.get(0);
					if(jft.getJobId().getJobCategoryMaster().getBaseStatus() && (!baseInvStatus) && !baseStatus.equalsIgnoreCase("vlt"))
					{
						sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='iconpophover4' href='javascript:void(0);'  onclick=\"checkInventory("+0+",null);\">");
						sb.append("<img src='images/option02.png' style='width:21px; height:21px;'>");
						sb.append("</a>");
					}else
					{
						sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='iconpophover4' href='javascript:void(0);'  onclick=\"getTeacherCriteria("+jft.getJobId().getJobId()+");\">");
						sb.append("<img src='images/option02.png' style='width:21px; height:21px;'>");
						sb.append("</a>");
					}
				}
				else
				{
					sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='iconpophover4' href='jobspecificinventory.do' >");
					sb.append("<img src='images/option02.png' style='width:21px; height:21px;'>");
					sb.append("</a>");
				}

				sb.append("</td>");
				sb.append("</tr>");			
				//--Job Specific Inventory end
			}
			

			//	start  ( EReg )
			if(isKellyJobApply)
	        try {
	        	List<WorkFlowStatus> workFlowStatusList = workFlowStatusDAO.findActiveSpecificWFStatus();
	        	
				List<MQEvent> mqEventList = mqEventDAO.findMQEventByTeacherWithoutWF(teacherDetail,workFlowStatusList);
				
				String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
				String WF1COM = Utility.getLocaleValuePropByKey("lblWF1COM", locale);
				String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
				String WF2COM = Utility.getLocaleValuePropByKey("lblWF2COM", locale);
				String INVWF3 = Utility.getLocaleValuePropByKey("lblINVWF3", locale);
				String WF3COM = Utility.getLocaleValuePropByKey("lblWF3COM", locale);
				
				Boolean wf1 = false;
				Boolean wf2 = false;
				Boolean wf3 = false;
				Boolean wf1Com = false;
				Boolean wf2Com = false;
				Boolean wf3Com = false;
				/*Boolean wf1ComExists = false;
				Boolean wf2ComExists = false;
				Boolean wf3ComExists = false;*/
				
				sb.append("<tr>");
				sb.append("<td>E-Registration</td>");
				
				if(mqEventList != null && mqEventList.size() >0){
					for(MQEvent m : mqEventList){
						if(m.getEventType()!=null && m.getEventType().equals(INVWF1) &&  m.getWorkFlowStatusId()!=null && m.getWorkFlowStatusId().getWorkFlowStatus().equals("INIT") ){
							wf1 = true;
						}
						else if(m.getEventType()!=null && m.getEventType().equals(INVWF2) && m.getWorkFlowStatusId()!=null && m.getWorkFlowStatusId().getWorkFlowStatus().equals("INIT") ){
							wf2 = true;
						}
						else if(m.getEventType()!=null && m.getEventType().equals(INVWF3) && m.getWorkFlowStatusId()!=null && m.getWorkFlowStatusId().getWorkFlowStatus().equals("INIT") ){
							wf3 = true;
						}
						
						if(m.getEventType()!=null && m.getEventType().equals(WF1COM) && m.getWorkFlowStatusId()!=null && m.getWorkFlowStatusId().getWorkFlowStatus().equals("Y")){
							wf1Com = true;
						}
						else if(m.getEventType()!=null && m.getEventType().equals(WF2COM) && m.getWorkFlowStatusId()!=null && (m.getWorkFlowStatusId().getWorkFlowStatus().equals("AWAIT") || m.getWorkFlowStatusId().getWorkFlowStatus().equals("Y"))){
							wf2Com = true;
						}
						else if(m.getEventType()!=null && m.getEventType().equals(WF3COM) && m.getWorkFlowStatusId()!=null && m.getWorkFlowStatusId().getWorkFlowStatus().equals("Y")){
							wf3Com = true;
						}
						
						/*if(m.getEventType()!=null && m.getEventType().equals(WF1COM))
							wf1ComExists = true;
						
						if(m.getEventType()!=null && m.getEventType().equals(WF2COM))
							wf2ComExists = true;
						
						if(m.getEventType()!=null && m.getEventType().equals(WF3COM))
							wf3ComExists = true;*/
					}
					
					if(((wf1 && wf1Com) || (wf2 && wf2Com) || (wf3 && wf3Com))  && teacherDetail.getKSNID()!=null){	// blue icon
						sb.append("<td>");
						sb.append("<strong class='"+className+"'>Pending</strong>");
						sb.append("</td>");
						sb.append("<td class=''>");
						sb.append("<a data-original-title='eRegistration' rel='tooltip' id='iconpophover4' target='_blank' href='javascript:void(0);' onclick='eRegConnect();' >");
						sb.append("<span class='fa fa-downcase-e' style='line-height:0px;'></span>");
						sb.append("</a>");
						sb.append("</td>");
					}
					else{		// grey icon
						sb.append("<td>");
						sb.append("<strong class='"+className+"'></strong>");
						sb.append("</td>");
						sb.append("<td class=''>");
						sb.append("<a data-original-title='No eReg activities to complete at this time' rel='tooltip' id='iconpophover4' target='_blank' href='javascript:void(0);' >");
						sb.append("<span class='fa fa-downcase-e-grey' style='line-height:0px;'></span>");
						sb.append("</a>");
						sb.append("</td>");
					}
				}
				else{			// grey icon
					sb.append("<td>");
					sb.append("<strong class='"+className+"'></strong>");
					sb.append("</td>");
					sb.append("<td class=''>");
					sb.append("<a data-original-title='No eReg activities to complete at this time' rel='tooltip' id='iconpophover4' target='_blank' href='javascript:void(0);' >");
					sb.append("<span class='fa fa-downcase-e-grey' style='line-height:0px;'></span>");
					sb.append("</a>");
					sb.append("</td>");
				}
				sb.append("</tr>");
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			//	end (e-Reg)


			
			sb.append("<tr>");			
			sb.append("<td colspan='3'>"+lblYouhave+" "+(noOfJBSIncompJobForTeacher<2?" "+noOfJBSIncompJobForTeacher:"<a href='jobspecificinventory.do'>"+noOfJBSIncompJobForTeacher+"</a>")+" "+msgIncJobSpeInvt+" ");
			sb.append("<div class='pagination-right'>&nbsp;</div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}


		return sb.toString();
	}

	@Transactional(readOnly=false)
	public String displayTeacher()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		List<JobForTeacher> jobForTeacher=null;	
		jobForTeacher = jobForTeacherDAO.findWithLimit(Order.asc("status"), 0, 10);

		StringBuffer sb =new StringBuffer();
		for(JobForTeacher jb: jobForTeacher)
		{
			sb.append(" "+jb.getTeacherId().getFirstName());
		}

		return sb.toString();
	}

	/*================  Milestone Grid ==================*/
	/*================  Report Grid ==================*/
	public String getReportGrid()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb =new StringBuffer();
		try 
		{
			//PrintWriter pw = response.getWriter();
			//HttpSession session = request.getSession();
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");

			TeacherPreference tPreference = null;
			TeacherPortfolioStatus tPortfolioStatus;

			boolean prefStatus = false;
			boolean portfolioStatus = false;
			boolean baseInvStatus = false;
			String baseStatus="";

			tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			if(tPreference!=null)
			{
				prefStatus = true;
			}

			tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
			if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted())
			{
				portfolioStatus = true;
			}

			TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
			{
				baseInvStatus=true;				 
			}
			if(teacherBaseAssessmentStatus!=null)
				baseStatus = teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName();
			sb.append("<table width='100%' border='0' class='table table-striped' style='border-top-color:#007ab4;border-radius:0px;'>");
			sb.append("<div class='bucketheader' style='height:28px;margin-left: -1.01px;'></div>");
			sb.append("<thead class='bg'>");
			sb.append("<tr style='background-color: #007ab4;'>");		
			//sb.append("<th><img class='fl' src='images/reports-icon.png'> <h4>Reports</h4></th>");
			sb.append("<th width='50%;'><img class='fl' src='images/communications_bucket.png' style='height:35px;width:35px;'> <div  class='subheading' style='color:white;font-size:13px;margin-left:45px;'>Communications</div></th>");		
			sb.append("<th width='25%;' style='vertical-align: middle;' class='subheading'>"+lblStatus+"</th>");
			sb.append("<th width='25%;' style='vertical-align: middle;' class='subheading'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			/*sb.append("<tr>");
			sb.append("<td>Professional Development Report&nbsp;<a  href='#' id='iconpophoverRe' rel='tooltip' data-original-title='Upon completion of the Base Inventory, a detailed and customized Professional Development Report is available for purchase. The report identifies strengths and opportunities and provides specific, actionable steps you can take to grow your skills - all based on decades of research.  Click on the cart icon to gain access to this valuable learning opportunity.'><img src='images/qua-icon.png' width='15' height='15' alt=''></a></td>");

			sb.append("<td>");
			if(baseInvStatus)
			{
				sb.append("<strong class='text-success'>Completed</strong>");
			}
			else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
			{
				sb.append("<strong class='text-error'>Timed Out</strong>");
			}
			else
			{
				sb.append("<strong class='text-error'>Incomplete</strong>");
			}
			sb.append("</td>");

			if(portfolioStatus)
			{
				sb.append("<td class='pagination-centered'><a data-original-title='Buy' rel='tooltip' id='iconpophover5' href='javascript:void(0);' onclick=\"checkBaseCompeleted("+teacherDetail.getTeacherId()+")\"><img src='images/option07.png'></a></td>");				
			}
			else
			{
				sb.append("<td class='pagination-centered'><a data-original-title='Buy' rel='tooltip' id='iconpophover5' href='javascript:void(0);' onclick=\"alert('Please complete your Portfolio.')\"><img src='images/option07.png'></a></td>");
			}

			sb.append("</tr>");*/
			
			List<JobForTeacher> jobForTeacherList= jobForTeacherDAO.findJobByTeacherNotKES(teacherDetail);
			List<JobForTeacher> jobForTeacherListForKES= jobForTeacherDAO.findJobByTeacherForKES(teacherDetail,false);
			if(jobForTeacherList.size()>0 || jobForTeacherListForKES.size()==0)
			{
				
					sb.append("<tr>");
					sb.append("<td>"+lblPortfolioReport+"</td>");
					

					sb.append("<td>");
					if(portfolioStatus)
					{
						sb.append("<strong class='text-success'>"+lblCompleted+"</strong>");
					}
					else
					{
						sb.append("<strong class='text-error'>"+optIncomlete+"</strong>");
					}
					
					sb.append("</td>");


					if(portfolioStatus)
					{
						String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						sb.append("<td class=''><a data-original-title='"+lnkV+"' rel='tooltip' id='iconpophover6' href='javascript:void(0);'  onclick=\"downloadPortfolioReport('"+teacherDetail.getTeacherId()+"','iconpophover6');"+windowFunc+"\"><img src='images/option06.png' style='width:21px; height:21px;'></a></td>");
					}
					else
					{
						sb.append("<td class=''><a data-original-title='"+lnkV+"' rel='tooltip' id='iconpophover6' href='#' onclick=\"alert('Please complete your Portfolio.')\"><img src='images/option06.png' style='width:21px; height:21px;'></a></td>");
					}
					
					//sb.append("<td></td>");
					sb.append("</tr>");
				
			}
			
			
			
			/*sb.append("<tr>");
			sb.append("<td>&nbsp;</td>");
			sb.append("<td>&nbsp;</td>");
			sb.append("<td>&nbsp;</td>");
			sb.append("</tr>");	
			sb.append("<tr>");
			sb.append("<td colspan='3' align='center'><b>Coming Shortly</b></td>");*/
			//sb.append("<td>&nbsp;</td>");
			//sb.append("<td>&nbsp;</td>");
			//sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td>&nbsp;</td>");
			sb.append("<td>&nbsp;</td>");
			sb.append("<td>&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td>&nbsp;</td>");
			sb.append("<td>&nbsp;</td>");
			sb.append("<td>&nbsp;</td>");
			sb.append("</tr>");
			sb.append("<tr>");
			sb.append("<td>&nbsp;</td>");
			sb.append("<td>&nbsp;</td>");
			sb.append("<td>&nbsp;</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	/* ===== J*/
	public String getJbofIntresGrid()
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb =new StringBuffer();
		try 
		{
			//PrintWriter pw = response.getWriter();			
			sb.append(showDashboardJbofIntresGrid());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}
	public String getJbofIntresGridAvailable()
	{
		StringBuffer sb =new StringBuffer();
		try 
		{
			//PrintWriter pw = response.getWriter();			
			sb.append(showDashboardJbofIntresGridAvailable());
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return sb.toString();
	}

	public List<JobForTeacher> getJobSpecInventory(HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		List<JobForTeacher> lstJobForTeacher = null;
		try 
		{
			StatusMaster statusInComplete = statusMasterDAO.findStatusByShortName("icomp");
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			//lstJobForTeacher = jobForTeacherDAO.findJobsForJobSpecificInventory(teacherDetail,statusInComplete,true);
			lstJobForTeacher = jobForTeacherDAO.findJobListForJobSpecificInventory(teacherDetail,true);
			List<JobForTeacher> lstTempJobForTeacher = new ArrayList<JobForTeacher>(lstJobForTeacher);
			List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacher(teacherDetail);
			JobOrder jobOrder = null;
			for(TeacherAssessmentStatus teacherAssessmentStatus: lstTeacherAssessmentStatus)
			{
				if(!teacherAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
				{
					jobOrder = teacherAssessmentStatus.getJobOrder();
					for(JobForTeacher jbfTeacher: lstTempJobForTeacher)
					{
						if(jbfTeacher.getJobId().equals(teacherAssessmentStatus.getJobOrder()))
						{
							lstJobForTeacher.remove(jbfTeacher);							
						}
						else
						{

						}
					}
				}
			}

		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}

	public String showDashboardJbofIntresGrid()
	{
	WebContext context;
    context = WebContextFactory.get();
    HttpServletRequest request = context.getHttpServletRequest();
    HttpSession session = request.getSession(false);
    if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
    {
                    throw new IllegalStateException(msgYrSesstionExp);
    }
    StringBuffer sb = new StringBuffer();
    try 
    {                              
                    //HttpSession session = request.getSession();
                    TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");

                    boolean prefStatus = false;
                    boolean portfolioStatus = false;
                    boolean baseInvStatus=false;
                    TeacherPreference tPreference=null;
                    TeacherPortfolioStatus tPortfolioStatus=null;

                    System.out.println(" =========== showDashboardJbofIntresGrid >>>>>>=========== ");
                    tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
                    if(tPreference!=null){
                                    prefStatus = true;
                    }

                    tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
                    if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()){
                                    portfolioStatus = true;
                    }
                    TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
                    if((teacherBaseAssessmentStatus!=null) && (!teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") )){
                                    baseInvStatus=true;                                                       
                    }

                    List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacherActiveJob(teacherDetail);


                    List<JobOrder> jobLst= new ArrayList<JobOrder>();
                    List<DistrictMaster> distritMastersLst= new ArrayList<DistrictMaster>();
                    for (JobForTeacher jobForTeacher : lstAllJFT) {
                                    jobLst.add(jobForTeacher.getJobId());
                                    distritMastersLst.add(jobForTeacher.getJobId().getDistrictMaster());
                    }
                    
                    Map<String, Boolean> checkDspqMap = new HashMap<String, Boolean>();
                    List<DistrictPortfolioConfig> distPortfolioList = new ArrayList<DistrictPortfolioConfig>();
                    	distPortfolioList = districtPortfolioConfigDAO.getAlldistrictDspq(distritMastersLst);
                    	for (DistrictPortfolioConfig districtPortfolioConfig : distPortfolioList) {
                    		if(districtPortfolioConfig.getJobCategoryMaster()!=null)
	                    		checkDspqMap.put(districtPortfolioConfig.getDistrictMaster().getDistrictId()+"##"+districtPortfolioConfig.getJobCategoryMaster().getJobCategoryId(), true);
                    		else                    		
	                    		checkDspqMap.put(districtPortfolioConfig.getDistrictMaster().getDistrictId()+"", true);
						}
                    	
                    List <StatusMaster> statusMasterList=statusMasterDAO.findAllStatusMaster();
                    Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
                    for (StatusMaster statusMaster : statusMasterList) {
                                    mapStatus.put(statusMaster.getStatusShortName(),statusMaster);
                    }

                    TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
                    Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
                    Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
                    List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
                    List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
                    List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
                    if(assessmentJobRelations1.size()>0){
                                    for(AssessmentJobRelation ajr: assessmentJobRelations1){
                                                    mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
                                                    adList.add(ajr.getAssessmentId());
                                    }
                                    if(adList.size()>0)
                                                    lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentList(teacherDetail,adList);
                    }
                    for(TeacherAssessmentStatus ts : lstJSI){
                                    mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
                    }

                    DistrictMaster districtMasterInternal=null;
                    List<InternalTransferCandidates> lstITRA=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
                    InternalTransferCandidates internalITRA = null;
                    if(lstITRA.size()>0){
                                    internalITRA=lstITRA.get(0);
                                    districtMasterInternal=internalITRA.getDistrictMaster();
                    }
                    List<JobForTeacher> lstcompletJFT = new ArrayList<JobForTeacher>();
                    List<JobForTeacher> lstincompletJFT = new ArrayList<JobForTeacher>();
                    List<JobForTeacher> lstviolatedJFT = new ArrayList<JobForTeacher>();
                    for (JobForTeacher jobForTeacher : lstAllJFT) {
                        StatusMaster statusMaster = mapStatus.get("icomp");
                        DashboardAjax dbajax=new DashboardAjax();
                        statusMaster=dbajax.findByTeacherIdJobStausForLoop(internalITRA,jobForTeacher,mapAssess,teacherAssessmentStatusJSI,mapJSI,districtMasterInternal,tPortfolioStatus,mapStatus,teacherBaseAssessmentStatus);
                       
                        if(statusMaster.getStatusShortName().equalsIgnoreCase("comp") && (jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")|| jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("rem") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("scomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("ecomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vcomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("dcln")))
                                        lstcompletJFT.add(jobForTeacher);
                        else if(!statusMaster.getStatusShortName().equalsIgnoreCase("vlt") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide") && (statusMaster.getStatusShortName().equalsIgnoreCase("icomp") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp")|| jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("rem") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("scomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("ecomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vcomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("dcln"))){
                                        lstincompletJFT.add(jobForTeacher);
                        }else if((statusMaster.getStatusShortName().equalsIgnoreCase("vlt") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt")) && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw")){
                                        lstviolatedJFT.add(jobForTeacher);
                        }
                    }

                    sb.append("<table width='100%' border='0' class='table table-striped' style='border-top-color:#007ab4;border-top-radius:0px;'>");
                    sb.append("<div class='bucketheader' style='height:28px;margin-left: -1.01px;'></div>");
                    sb.append("<thead class='bg'>");
                    sb.append("<tr style='background-color: #007ab4;'>");
                    sb.append("<th width='42%'><img class='fl' src='images/job_applications_bucket.png' style='height:35px;width:35px;'> <div  class='subheading' style='color:white;font-size:13px;margin-left:45px; background-color: #007ab4;'>"+lblJoApp+"</div></th>");

                    sb.append("<th  width='20%' style='vertical-align: middle;'>"+lblStatus+"</th>");
                    sb.append("<th  width='50%' style='vertical-align: middle;'>"+lblAct+"</th>");
                    sb.append("</tr>");
                    sb.append("</thead>");

                    int rowCount=0;
                    for(JobForTeacher jbTeacher : lstincompletJFT)
                    {
                                    if(rowCount==4)
                                                    break;
                                    rowCount++;                                                                                                                     
//added by sandeep 27-july for check job is inactive or expire.
                                    
                                    Boolean jobStatusFlag = false;
                                    Boolean ncDistrictFlag = false;
                                    
                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster() != null && jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() ==NC_HEADQUARTER){
                                    	System.out.print("NC_HeadQuarter---"+jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId());
                                    	ncDistrictFlag = true;
                                    }
                                    
                                    
                                    if(jbTeacher.getJobId().getStatus()!=null && !jbTeacher.getJobId().getStatus().equalsIgnoreCase("A"))
                    				    jobStatusFlag = true;
                    				else
                    					if(!Utility.checkDateBetweenTwoDate(jbTeacher.getJobId().getJobStartDate(), jbTeacher.getJobId().getJobEndDate()))
                    						jobStatusFlag = true;
                    					
//end                                    
                                    sb.append("<tr>");
                                    sb.append("<td><a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
                                    //baseInvStatus 
                                    if((!jbTeacher.getJobId().getIsPortfolioNeeded()) && (jbTeacher.getJobId().getJobCategoryMaster().getBaseStatus()) &&  (!baseInvStatus)){
                                                    sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
                                                    
                                                    
                                                    if(jobStatusFlag && ncDistrictFlag){
                                                    	 sb.append(getInactiveNdExpIncompJobImage(rowCount));
                                                    	continue;
                                                    }
                                                    
                                                    sb.append("<td>");
                                                    sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria("+jbTeacher.getJobId().getJobId()+");\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jbTeacher.getJobForTeacherId()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>"); /* by rajendra*/
                                                    //tttttttttttttttttttttttttttttttt
                                                    
                                                    //checkDspqMap(&& )
                                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getJobCategoryMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+jbTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else if(jbTeacher.getJobId().getDistrictMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+""))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else
                                                    	sb.append("&nbsp;<img src='images/personalinformation_grey.png' style='width:21px; height:21px;'>");
                                                    
                                                    sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>");
                                                    sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblQQ+"' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+(rowCount+4)+"' href='javascript:void(0);' onclick=\"return getQQDistrictQuestion('"+jbTeacher.getJobId().getJobId()+"')\"></a>");
                                                    sb.append("</td>");
                                    }
                                    else if((!jbTeacher.getJobId().getIsPortfolioNeeded()) && (!jbTeacher.getJobId().getJobCategoryMaster().getBaseStatus()) && (jbTeacher.getJobId().getIsJobAssessment()) ){
                                                    sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
                                                    
                                                    if(jobStatusFlag && ncDistrictFlag){
                                                    	 sb.append(getInactiveNdExpIncompJobImage(rowCount));
                                                    	continue;
                                                    }
                                                    
                                                    sb.append("<td>");
                                                    sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria("+jbTeacher.getJobId().getJobId()+");\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='Withdraw1' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jbTeacher.getJobForTeacherId()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>"); /* by rajendra*/
                                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getJobCategoryMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+jbTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()))
                                                    	sb.append("&nbsp;<a data-original-title=''"+toolUpdateProf+"'' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else if(jbTeacher.getJobId().getDistrictMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+""))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else
                                                    	sb.append("&nbsp;<img src='images/personalinformation_grey.png' style='width:21px; height:21px;'>");
                                                    sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>");
                                                    sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblQQ+"' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+(rowCount+4)+"' href='javascript:void(0);' onclick=\"return getQQDistrictQuestion('"+jbTeacher.getJobId().getJobId()+"')\"></a>");
                                                    sb.append("</td>");
                                    }else if(prefStatus == false || portfolioStatus == false){
                                                    sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
                                                   
                                                    if(jobStatusFlag && ncDistrictFlag){
                                                    	 sb.append(getInactiveNdExpIncompJobImage(rowCount));
                                                    	continue;
                                                    }
                                                    
                                                    sb.append("<td>");
                                                    sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria("+jbTeacher.getJobId().getJobId()+");\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jbTeacher.getJobForTeacherId()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>"); /* by rajendra*/
                                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getJobCategoryMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+jbTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else if(jbTeacher.getJobId().getDistrictMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+""))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else
                                                    	sb.append("&nbsp;<img src='images/personalinformation_grey.png' style='width:21px; height:21px;'>");
                                                    sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>");
                                                    sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblQQ+"' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+(rowCount+4)+"' href='javascript:void(0);' onclick=\"return getQQDistrictQuestion('"+jbTeacher.getJobId().getJobId()+"')\"></a>");
                                                    sb.append("</td>");
                                    }
                                    else if(jbTeacher.getJobId().getJobCategoryMaster().getBaseStatus() && (!baseInvStatus)){
                                                    boolean  evDistrict=false;
                                                    try{
                                                                    if(jbTeacher.getJobId().getDistrictMaster()!=null){
                                                                                    if(jbTeacher.getJobId().getDistrictMaster().getDistrictId()==5513170){
                                                                                                    evDistrict=true;
                                                                                    }
                                                                    }
                                                    }catch(Exception e){}
                                                    /*if((jbTeacher.getIsAffilated()==1) && evDistrict==false)
                                                    {
                                                                    sb.append("<td><strong class='text-success'>Completed</strong></td>");
                                                                    sb.append("<td>");
                                                                    sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png'></a>");
                                                                    sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jbTeacher.getJobForTeacherId()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");  by rajendra
                                                                    sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png'></a>");
                                                                    sb.append("</td>");                                                                      
                                                    }
                                                    else
                                                    {*/
                                                    sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
                                                   
                                                    if(jobStatusFlag && ncDistrictFlag){
                                                   	  sb.append(getInactiveNdExpIncompJobImage(rowCount));
                                                   	  continue;
                                                    }
                                                    
                                                    sb.append("<td>");
                                                    sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria("+jbTeacher.getJobId().getJobId()+");\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jbTeacher.getJobForTeacherId()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>"); /* by rajendra*/
                                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getJobCategoryMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+jbTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else if(jbTeacher.getJobId().getDistrictMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+""))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else
                                                    	sb.append("&nbsp;<img src='images/personalinformation_grey.png' style='width:21px; height:21px;'>");
                                                    sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>");
                                                    sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='QQ' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+(rowCount+4)+"' href='javascript:void(0);' onclick=\"return getQQDistrictQuestion('"+jbTeacher.getJobId().getJobId()+"')\"></a>");
                                                    sb.append("</td>");
                                                    //}
                                    }
                                    else{
                                                    sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
                                                  
                                                    if(jobStatusFlag && ncDistrictFlag){
                                                    	 sb.append(getInactiveNdExpIncompJobImage(rowCount));
                                                    	continue;
                                                    }
                                                    
                                                    
                                                    sb.append("<td>");
                                                    sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria("+jbTeacher.getJobId().getJobId()+");\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jbTeacher.getJobForTeacherId()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>"); /* by rajendra*/
                                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getJobCategoryMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+jbTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else if(jbTeacher.getJobId().getDistrictMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+""))
                                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                                    else
                                                    	sb.append("&nbsp;<img src='images/personalinformation_grey.png' style='width:21px; height:21px;'>");
                                                    sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>");
                                                    sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
                                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblQQ+"' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+(rowCount+4)+"' href='javascript:void(0);' onclick=\"return getQQDistrictQuestion('"+jbTeacher.getJobId().getJobId()+"')\"></a>");
                                                    sb.append("</td>");
                                    }
                                    sb.append("</tr>");
                    }
                    for(JobForTeacher jbTeacher : lstcompletJFT)
                    {
                                    if(rowCount==4)
                                                    break;
                                    rowCount++;
                                    
//added by sandeep 27-july
                                    Boolean jobStatusFlag = false;
                                    Boolean ncDistrictFlag = false;
                                    
                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster() != null && jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() ==NC_HEADQUARTER){
                                    	System.out.print("NC_HeadQuarter---"+jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId());
                                    	ncDistrictFlag = true;
                                    }
                                    
                                    
                                    if(jbTeacher.getJobId().getStatus()!=null && !jbTeacher.getJobId().getStatus().equalsIgnoreCase("A"))
                    				    jobStatusFlag = true;
                    				else
                    					if(!Utility.checkDateBetweenTwoDate(jbTeacher.getJobId().getJobStartDate(), jbTeacher.getJobId().getJobEndDate()))
                    						jobStatusFlag = true;
                    					
//end  
                                    sb.append("<tr>");
                                    sb.append("<td> <a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
                                    sb.append("<td><strong class='text-success'>Completed</strong></td>");
                                   
                                    if(jobStatusFlag && ncDistrictFlag){
                                   	    sb.append(getInactiveNdExpCompJobImage(rowCount));
                                    	continue;
                                    }
                                    
                                    sb.append("<td>");
                                    sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
                                    sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jbTeacher.getJobForTeacherId()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>"); /* by rajendra*/
                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getJobCategoryMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+jbTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()))
                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                    else if(jbTeacher.getJobId().getDistrictMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+""))
                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                    else
                                    	sb.append("&nbsp;<img src='images/personalinformation_grey.png' style='width:21px; height:21px;'>");
                                    sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>");
                                    sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
                                    sb.append("&nbsp;&nbsp;<a data-original-title='"+lblQQ+"' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+(rowCount+4)+"' href='javascript:void(0);' onclick=\"return getQQDistrictQuestion('"+jbTeacher.getJobId().getJobId()+"')\"></a>");
                                    sb.append("</td>");                                                                      
                                    sb.append("</tr>");
                    }

                    for(JobForTeacher jbTeacher : lstviolatedJFT)
                    {
                                    if(rowCount==4)
                                                    break;
                                    rowCount++;
                                    
//added by sandeep
                                    Boolean jobStatusFlag = false;
                                    Boolean ncDistrictFlag = false;
                                    
                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster() != null && jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId() ==NC_HEADQUARTER){
                                    	System.out.print("NC_HeadQuarter---"+jbTeacher.getJobId().getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId());
                                    	ncDistrictFlag = true;
                                    }

                                    if(jbTeacher.getJobId().getStatus()!=null && !jbTeacher.getJobId().getStatus().equalsIgnoreCase("A"))
                    				    jobStatusFlag = true;
                    				else
                    					if(!Utility.checkDateBetweenTwoDate(jbTeacher.getJobId().getJobStartDate(), jbTeacher.getJobId().getJobEndDate()))
                    						jobStatusFlag = true;
                    					
//end  								
                                    sb.append("<tr>");
                                    sb.append("<td> <a href='applyjob.do?jobId="+jbTeacher.getJobId().getJobId()+"'>"+jbTeacher.getJobId().getJobTitle()+"</a></td>");
                                    sb.append("<td><strong class='text-error'>"+optTOut+"</strong></td>");
                                   
                                    if(jobStatusFlag && ncDistrictFlag){
                                   	    sb.append(getInactiveNdExpCompJobImage(rowCount));
                                    	continue;
                                    }
                                    
                                    sb.append("<td>");
                                    sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return jbWithdraw('"+jbTeacher.getJobForTeacherId()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
                                    sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jbTeacher.getJobForTeacherId()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>"); /* by rajendra*/
                                    if(jbTeacher.getJobId().getDistrictMaster()!=null && jbTeacher.getJobId().getJobCategoryMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+"##"+jbTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()))
                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                    else if(jbTeacher.getJobId().getDistrictMaster()!=null && checkDspqMap.containsKey(jbTeacher.getJobId().getDistrictMaster().getDistrictId()+""))
                                    	sb.append("&nbsp;<a data-original-title='"+toolUpdateProf+"' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' href='#' onclick=\"editDspq("+jbTeacher.getJobId().getJobId()+");\"><img src='images/personalinformation.png' style='width:21px; height:21px;'></a>");
                                    else
                                    	sb.append("&nbsp;<img src='images/personalinformation_grey.png' style='width:21px; height:21px;'>");
                                    sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>");
                                    sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbTeacher.getJobId().getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
                                    sb.append("&nbsp;&nbsp;<a data-original-title='QQ' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+(rowCount+4)+"' href='javascript:void(0);' onclick=\"return getQQDistrictQuestion('"+jbTeacher.getJobId().getJobId()+"')\"></a>");
                                    sb.append("</td>");                                                                      
                                    sb.append("</tr>");
                    }

                    sb.append("<tr>");
                    sb.append("<td colspan='3' >"+lblYouhave+" "+" "+(lstcompletJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=comp'>"+" "+lstcompletJFT.size()+"</a>")
                                                    +" "+ btnComp +", "+(lstincompletJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=icomp'>"+lstincompletJFT.size()+"</a>")
                                                    +" "+optIncomlete+", "
                                                    +"and "+(lstviolatedJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=vlt'>"+lstviolatedJFT.size()+"</a>")
                                                    +" "+optTOut+"");
                    sb.append("<div class='right'><a href='jobsofinterest.do'><strong>"+lblSeeAll+"</strong></a></div>");
                    sb.append("</td>");
                    sb.append("</tr>");
                    sb.append("</table>");
    } 
    catch (Exception e) 
    {
                    e.printStackTrace();
    }

    return sb.toString();
}
	
	
	public StringBuffer getInactiveNdExpIncompJobImage(int rowCount ){
		
		StringBuffer sb = new StringBuffer();
		sb.append("<td>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpJbStatus"+rowCount+"' ><img src='images/option02_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpWithdraw"+rowCount+"' ><img src='images/option05_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpCoverLetter"+rowCount+"' ><img src='images/clip_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' ><img src='images/personalinformation_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>"); 
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpShare"+rowCount+"' ><img src='images/option04_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='QQ"+(rowCount+4)+"' ><img src='images/qqthumb_grey.png' style='width:28px; height:20px;'></a>");
       
        sb.append("</td>");
		return sb;
		
		
	}
	
	
	public StringBuffer getInactiveNdExpCompJobImage(int rowCount ){
		
		StringBuffer sb = new StringBuffer();
		sb.append("<td>");
        //sb.append("&nbsp;<a data-original-title='This job is Expired, Please contact TeacherMatch Administrator.' rel='tooltip' id='tpJbStatus"+rowCount+"' ><img src='images/option02_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpWithdraw"+rowCount+"' ><img src='images/option05_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpCoverLetter"+rowCount+"' ><img src='images/clip_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpJbUpPotStatus"+rowCount+"' ><img src='images/personalinformation_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("<script>$('#tpJbUpPotStatus"+rowCount+"').tooltip();</script>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='tpShare"+rowCount+"' ><img src='images/option04_grey.png' style='width:21px; height:21px;'></a>");
        sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='QQ"+(rowCount+4)+"' ><img src='images/qqthumb_grey.png' style='width:28px; height:20px;'></a>");
       
        sb.append("</td>");
		return sb;
		
		
	}


	public String showDashboardJbofIntresGridAvailable_old()
	{
		System.out.println("::::::::::::::showDashboardJbofIntresGridAvailable:::::::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		StringBuffer sb = new StringBuffer();
		try 
		{	
			//HttpSession session = request.getSession();
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");

			Order sortOrderStrVal	=	null;

			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			if(teacherDetail.getUserType().equalsIgnoreCase("N")){
				lstJobOrder = getJobByPref(request,sortOrderStrVal);
			}else{	
				List<JobForTeacher> lstJFT =  jobForTeacherDAO.findWithLimit(null, 0, 1, Restrictions.eq("teacherId",teacherDetail));
				if(lstJFT.size()>0){
					if(lstJFT.get(0)!=null && lstJFT.get(0).getJobId().getHeadQuarterMaster()!=null){
						lstJobOrder = jobOrderDAO.findJobOrderbyHBD(lstJFT.get(0).getJobId().getHeadQuarterMaster());
					}else{
						lstJobOrder = jobOrderDAO.findJobOrderbyDistrict(lstJFT.get(0).getJobId().getDistrictMaster());
					}
				}
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
				if(itcList.size()>0){
					if(itcList.get(0)!=null && itcList.get(0).getHeadQuarterMaster()!=null){
						lstJobOrder = jobOrderDAO.findJobOrderbyHBD(itcList.get(0).getHeadQuarterMaster());
					}else{
						lstJobOrder = jobOrderDAO.findJobOrderbyDistrict(itcList.get(0).getDistrictMaster());
					}
				}
			}
			
			HashSet<JobOrder> masterJOb = new HashSet<JobOrder>();
			HashSet<JobOrder> associateJobs = new HashSet<JobOrder>();
			List<JobOrder> temJobLIsts=new ArrayList<JobOrder>();
			
			//creating temporary list for deleting 
			temJobLIsts.addAll(lstJobOrder);

			
			for (JobOrder jobOrder : temJobLIsts) 
			{
				if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && jobOrder.getIsVacancyJob()){
					associateJobs.add(jobOrder);
					lstJobOrder.remove(jobOrder);
				}				
				if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && !jobOrder.getIsVacancyJob()){					
					masterJOb.add(jobOrder);
				}
			}			
			System.out.println(">>>>>>> masterJOb.size() :: "+masterJOb.size()+" associateJobs.size() :: "+associateJobs.size());

			//check for Screaning complete status
			List<TeacherStatusHistoryForJob>  historyforScreaningComlete= new ArrayList<TeacherStatusHistoryForJob>();
			//Map<jobId, TeacherStatusHistoryForJob> 
			Map<Integer, TeacherStatusHistoryForJob> applyJObStatus = new HashMap<Integer, TeacherStatusHistoryForJob>();
			if(masterJOb!=null && masterJOb.size()>0){
				StatusMaster statusMaster=statusMasterDAO.findById(16,false,false);
				historyforScreaningComlete = teacherStatusHistoryForJobDAO.getScreningCompleteJobsByJobsAndTeacher(teacherDetail, masterJOb, statusMaster);			
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyforScreaningComlete) {
					applyJObStatus.put(teacherStatusHistoryForJob.getJobOrder().getJobId(), teacherStatusHistoryForJob);
				}
			}
			
			List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);

			List<JobForTeacher> lstwidrwJFT = new ArrayList<JobForTeacher>();

			for(JobForTeacher jft: lstAllJFT)
			{				
				JobOrder jobOrder = jft.getJobId();	
				if(masterJOb.contains(jobOrder) && applyJObStatus.containsKey(jobOrder.getJobId()))
				{
					for (JobOrder jobOrderAssoc : associateJobs) {
						if(jobOrderAssoc.getJobCategoryMaster().equals(jobOrder.getJobCategoryMaster())){
							lstJobOrder.add(jobOrderAssoc);
						}
					}
					
				}
				if(lstJobOrder.contains(jobOrder))
				{
					lstJobOrder.remove(jobOrder);
				}
				if(jft.getStatus().getStatusShortName().equalsIgnoreCase("widrw"))
					lstwidrwJFT.add(jft);			

			}
			
			List<CertificateTypeMaster> certObjList = new ArrayList<CertificateTypeMaster>();
			String crtIdList="";
			List<String> certList = new ArrayList<String>();
			
			List<TeacherCertificate> lstTeacherCertificates=teacherCertificateDAO.findCertificateAscByTeacher(teacherDetail);//teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
			if(lstTeacherCertificates!=null){
				for(TeacherCertificate tc : lstTeacherCertificates){
					if(tc.getCertificateTypeMaster()!=null)
					{
						if(crtIdList.equalsIgnoreCase(""))
							crtIdList+=tc.getCertificateTypeMaster().getCertTypeId();
			             else
							crtIdList+=","+tc.getCertificateTypeMaster().getCertTypeId();
						certList.add(tc.getCertificateTypeMaster().getCertTypeId()+"");
						certList.add(crtIdList);
						certObjList.add(tc.getCertificateTypeMaster());
					}
				}
			}

			List<JobOrder> listOfJobId = new ArrayList<JobOrder>();
			for (JobOrder jobOrder : lstJobOrder){
				if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==4218990){
					listOfJobId.add(jobOrder);					
				}
			}
			
			Map<JobOrder, String> jobCertMap = new HashMap<JobOrder, String>();
			if(listOfJobId!=null && listOfJobId.size()>0){
			List<JobCertification> findByJobIDS = jobCertificationDAO.findCertificationsByJobOrders(listOfJobId,certObjList);
				for (JobCertification jobCertification : findByJobIDS) {
					String certId = jobCertification.getCertificateTypeMaster().getCertTypeId()+"";
					lstJobOrder.remove(jobCertification.getJobId());                                                                                      
						if(jobCertMap.containsKey(jobCertification.getJobId())){
							String existingCertId = jobCertMap.get(jobCertification.getJobId());
							jobCertMap.put(jobCertification.getJobId(), existingCertId+","+certId);
							System.out.println(jobCertification.getJobId().getJobId()+"  "+existingCertId+","+certId);
						}else{
							jobCertMap.put(jobCertification.getJobId(), certId);
							System.out.println(jobCertification.getJobId().getJobId()+"  "+certId);
						}
					}
				}
			
			List<JobOrder> addMatchJobs=new ArrayList<JobOrder>(); 
				for (String c1 : certList) {
					for (java.util.Map.Entry<JobOrder, String> entry : jobCertMap.entrySet()){							                                                                                                    
						if(c1.equalsIgnoreCase(entry.getValue())){                                                                                                                                                                                                                                                                                         
							addMatchJobs.add(entry.getKey());
							lstJobOrder.add(entry.getKey());
						}
					}
				}
			
			sb.append("<table width='100%'  class='table  table-striped' style='border-top-color:#007ab4;border-top-radius:0px;'>");
			sb.append("<div class='bucketheader' style='height:28px;margin-left: -1.01px;'></div>");
			sb.append("<thead class='bg'>");
			sb.append("<tr style='background-color: #007ab4;'>");
			sb.append("<th width='40%' style='height:40px;'><img class='fl' src='images/jobs_of_interest_bucket.png' style='height:35px;width:35px;'> <div  class='subheading' style='color:white;font-size:13px;margin-left:45px;'>"+Utility.getLocaleValuePropByKey("lblJoOfInterest", locale)+"</div></th>");
			//sb.append("<th width='25%'>Status</th>");
			sb.append("<th width='35%' style='vertical-align: middle;'>"+lblDistrictName+"</th>");
			sb.append("<th width='25%' style='vertical-align: middle;'>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");

			int rowCount=0;

			for(JobOrder jbOrder: lstJobOrder)
			{
				if(rowCount==4)
					break;
				rowCount++;
				sb.append("<tr>");
				if(jbOrder.getHeadQuarterMaster()==null && jbOrder.getDistrictMaster()!=null && jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
				{
					sb.append("<td><a onclick=\"chkApplyJobNonClientDashBoard('"+jbOrder.getJobId()+"','"+jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jbOrder.getExitURL()+"','"+jbOrder.getDistrictMaster().getDistrictId()+"','')\" href='javascript:void(0)' >"+jbOrder.getJobTitle()+"</a></td>");
				}
				else
				{
					sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
				}
				String districtName="";
				if(jbOrder.getHeadQuarterMaster()!=null){
					districtName=jbOrder.getHeadQuarterMaster().getHeadQuarterName();
				}else if(jbOrder.getDistrictMaster()!=null){
					districtName=jbOrder.getDistrictMaster().getDistrictName();
				}
				sb.append("<td class='text-info' style='color:#007ab4;'>"+districtName+"</td>");
				sb.append("<td>");
				if(jbOrder.getHeadQuarterMaster()==null && jbOrder.getDistrictMaster()!=null && jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
				{
					sb.append("<a data-original-title='"+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' onclick=\"chkApplyJobNonClientDashBoard('"+jbOrder.getJobId()+"','"+jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jbOrder.getExitURL()+"','"+jbOrder.getDistrictMaster().getDistrictId()+"','');\"   href='javascript:void(0)' ><img src='images/option01.png'  \">");					
				}
				else
				{
					//System.out.println(" Client District Job Id :: "+jbOrder.getJobId());
					sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png'style='width:21px; height:21px;'></a>");
				}
				sb.append("&nbsp;&nbsp;<a data-original-title='"+lblhide+"' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return jbHide('"+jbOrder.getJobId()+"')\"><img src='images/option03.png'style='width:21px; height:21px;'></a>");
				sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+(rowCount+4)+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png'style='width:21px; height:21px;'></a>");

				sb.append("</td>");	   				
				sb.append("</tr>");
			}


			sb.append("<tr>");
			sb.append("<td colspan='3' >You have "
					+(lstJobOrder.size()==0?"0":"<a href='jobsofinterest.do?searchby=avlbl'>"+lstJobOrder.size()+"</a>")
					+" Not Applied and "+(lstwidrwJFT.size()==0?"0":"<a href='jobsofinterest.do?searchby=widrw'>"+lstwidrwJFT.size()+"</a>")+" "+optWithdrawn);
			sb.append("<div class='right'><a href='jobsofinterest.do'><strong>See All</strong></a></div>");
			sb.append("</td>");
			sb.append("</tr>");
			sb.append("</table>");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}
	public List<JobOrder> getJobByPref(HttpServletRequest request,Order sortOrderStrVal)
	{	
		List<JobOrder> lstJobOrder = null;
		try
		{	
			List<String> lstRegionId=null;
			List<String> lstSchoolTypeId=null;
			List<String> lstGeographyId=null;
			List<SchoolMaster> lstSchoolMasters = null;
			List<SchoolMaster> lstSMtoRemove = new ArrayList<SchoolMaster>();



			HttpSession session = request.getSession();			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			TeacherPreference preference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);

			lstRegionId = Arrays.asList(preference.getRegionId().trim().split("\\|"));

			lstSchoolTypeId = Arrays.asList(preference.getSchoolTypeId().trim().split("\\|"));

			lstGeographyId = Arrays.asList(preference.getGeoId().trim().split("\\|"));



			if(sortOrderStrVal!=null)
			{
				lstJobOrder = jobOrderDAO.findSortedJobtoShow(sortOrderStrVal);
			}
			else
			{
				lstJobOrder = jobOrderDAO.findJobtoShow();
			}
			for(JobOrder jb:lstJobOrder)
			{
				if(jb.getCreatedForEntity() == 3)
				{
					lstSchoolMasters=jb.getSchool();
					if(lstSchoolMasters!=null)
					{			
						for(SchoolMaster school: lstSchoolMasters)
						{				
							if(lstGeographyId.contains(""+school.getGeoMapping().getGeoId().getGeoId()) && lstSchoolTypeId.contains(""+school.getSchoolTypeId().getSchoolTypeId() ) && lstRegionId.contains(""+school.getRegionId().getRegionId()))
							{
								//do nothing for now
							}
							else
							{
								//lstSMasters.remove(school);
								lstSMtoRemove.add(school);

							}

						}
						//if(jb.getCreatedForEntity() == 3)
						//{
							for(SchoolMaster school:lstSMtoRemove)
							{
								lstSchoolMasters.remove(school);
							}
							lstSMtoRemove.clear();
						//}
					}
				}
			}

			List<JobOrder> lstJbOdrToremove = new ArrayList<JobOrder>(lstJobOrder);
			for( JobOrder jb:lstJbOdrToremove)
			{

				if(jb.getCreatedForEntity() == 3)
				{
					if(jb.getSchool()==null || jb.getSchool().size()==0)
					{

						lstJobOrder.remove(jb);
					}
					else
					{

					}
				}
				else
				{
					if(!lstRegionId.contains(""+jb.getDistrictMaster().getStateId().getRegionId().getRegionId()))
					{
						lstJobOrder.remove(jb);
					}
				}
			}

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			lstJobOrder = new ArrayList<JobOrder>();

		}
		return lstJobOrder;
	}


	public String[] getTeacherCriteria(String jobId)
	{
		System.out.println("================== Dashbaord Ajax getTeacherCriteria:::::");
		String returnval = "";
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String[] resp = new String[13];
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{		
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			boolean prefStatus = false;
			boolean portfolioStatus = false;
			boolean baseInvStatus=false;
			boolean isNonTeacher = false;
			TeacherPreference tPreference=null;
			TeacherPortfolioStatus tPortfolioStatus=null;

			tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			if(tPreference!=null){
				prefStatus = true;
			}

			TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
			if((teacherBaseAssessmentStatus!=null) && (!teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") )){
				baseInvStatus=true;				 
			}
			JobOrder jobOrder = new JobOrder();
			jobOrder.setJobId(Integer.parseInt(jobId));
			JobForTeacher jbTeacher = jobForTeacherDAO.findJFTByTeacherAndJob(teacherDetail, jobOrder);

			jobOrder = jbTeacher.getJobId();
			boolean baseRequired = jobOrder.getJobCategoryMaster().getBaseStatus();
			boolean isJobAssessment = jobOrder.getIsJobAssessment();
			boolean portfolioRequired = jobOrder.getIsPortfolioNeeded(); 

			////////***********Sekhar  Add*****///////
			if(!portfolioRequired)
				portfolioStatus=true;

			DistrictMaster districtMaster=null;
			if(jobOrder.getDistrictMaster()!=null){
				districtMaster=jobOrder.getDistrictMaster();
				String distName = districtMaster.getDisplayName();
				if(distName!=null)
					distName = districtMaster.getDisplayName().equals("")?districtMaster.getDistrictName():districtMaster.getDisplayName();
					resp[6]=distName;
					resp[7]=districtMaster.getDistrictId().toString();
					resp[8]=teacherDetail.getTeacherId().toString();
					resp[11]= "false";
					resp[12]= "false";

			}else if(jobOrder.getHeadQuarterMaster()!=null){
				resp[6]=jobOrder.getHeadQuarterMaster().getHeadQuarterName();
				resp[11]=jobOrder.getJobCategoryMaster().getOfferDSPQ()!=null?jobOrder.getJobCategoryMaster().getOfferDSPQ()?"true":"false":"false";
				resp[12]="true";
			}
			
			try
	          {
	            if ((jobOrder != null) && (districtMaster!=null && districtMaster.getDistrictId().equals(Integer.valueOf(4218990)))) {
	            	if ((jobOrder.getJobCategoryMaster() != null) && (jobOrder.getJobCategoryMaster().getJobCategoryName().contains("School Support"))) {		                
		                resp[10]="true";
		                resp[9]="false";
		            }else if ((jobOrder.getJobCategoryMaster() != null) && (!jobOrder.getJobCategoryMaster().getJobCategoryName().equals("Teacher"))) {	                
		                resp[9]="true";
		                resp[10]="false";
	              } else {
	            	  resp[9]="false";
	            	  resp[10]="false";
	              }
	              
	            }
	          }
	          catch (Exception e)
	          {
	            e.printStackTrace();
	          }

			List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
			boolean internalFlag=false;
			if(itcList.size()>0){
				internalFlag=true;
			}
			boolean offerEPI =false;
			boolean offerJSI =false;
			boolean epiInternal =false;
			Integer jobAssessmentStatus = jobOrder.getJobAssessmentStatus();
			if(jobOrder.getDistrictMaster()!=null && internalFlag){
				districtMaster = jobOrder.getDistrictMaster();
				if(districtMaster.getOfferEPI()){
					offerEPI=true;
				}else{
					offerEPI=false;
				}// && jobAssessmentStatus==1
				if(districtMaster.getOfferJSI()){
					offerJSI=true;
				}else{
					offerJSI=false;
				}
				if(districtMaster.getOfferPortfolioNeeded()){
					portfolioStatus=false;
					portfolioRequired=true;
				}else{
					portfolioStatus=true;
					portfolioRequired=false;
				}
			}
			int IsAffilated=0;
			try{
				if(jbTeacher!=null && jbTeacher.getIsAffilated()!=null){
					if(jbTeacher.getIsAffilated()==1)
						IsAffilated=1;
				}
				if(jobOrder.getJobCategoryMaster()!=null && IsAffilated==1 && internalFlag==false){
					if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
						offerEPI=true;
					}else{
						offerEPI=false;
					}
					if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0){
						offerJSI=true;
					}else{
						offerJSI=false;
					}
					if(jobOrder.getJobCategoryMaster().getOfferPortfolioNeeded()){
						portfolioStatus=false;
						portfolioRequired=true;
					}else{
						portfolioStatus=true;
						portfolioRequired=false;
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			try{
				tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()){
					portfolioStatus = true;
				}
				
				resp[1]=""+portfolioStatus;
				if(jobOrder!=null && jobOrder.getDistrictMaster().getIsPortfolioNeeded()==null || (jobOrder.getDistrictMaster().getIsPortfolioNeeded()!=null && jobOrder.getDistrictMaster().getIsPortfolioNeeded().equals(false)))
				{
					portfolioStatus = true;
					resp[1]="false";
				}
								
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("offerJSIofferJSIofferJSIofferJSIofferJSIofferJSIofferJSIofferJSI:: "+offerJSI);
			////////***********Sekhar  End*****///////
			resp[0]=""+prefStatus;
			//resp[1]=""+portfolioStatus;
			resp[2]=""+jobId;

			if((!baseInvStatus) && (((!jbTeacher.getJobId().getIsPortfolioNeeded()) && (baseRequired) && internalFlag==false && IsAffilated==0 ) || (internalFlag==false && IsAffilated==1 && offerEPI) || (internalFlag && offerEPI))){
				returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",'epi'";
				resp[3]="epi";
			}
			else if((isJobAssessment) && jobAssessmentStatus==1 && (((!portfolioRequired) && (!baseRequired) && internalFlag==false && IsAffilated==0 ) || (internalFlag==false && IsAffilated==1 && offerJSI) ||(internalFlag && offerJSI))){
				returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",'jsi'";
				resp[3]="jsi";
			}
			else if(prefStatus == false || portfolioStatus == false){
				returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",''";
				resp[3]="''";
			}
			else if((!baseInvStatus) && ((baseRequired && internalFlag==false  && IsAffilated==0 ) || (internalFlag==false && IsAffilated==1 && offerEPI)  || (internalFlag && offerEPI))){
				if((jbTeacher.getIsAffilated()==1))
				{
					returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",''";
					resp[3]="''";
				}
				else
				{
					returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",'epi'";
					resp[3]="epi";
				}
			}
			else if(baseRequired==false && isJobAssessment==false && portfolioRequired==false)
			{
				returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",'done'";
				resp[3]="done";
			}
			else if(baseRequired==false && isJobAssessment==false && portfolioRequired==true && portfolioStatus)
			{
				returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",'done'";
				resp[3]="done";
			}
			else
			{
				if(isJobAssessment && jobAssessmentStatus==1 && ((internalFlag==false && IsAffilated==0 ) || (internalFlag==false && IsAffilated==1 && offerJSI) ||(internalFlag && offerJSI))){
					returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",'jsi'";
					resp[3]="jsi";
				}else{
					returnval=""+prefStatus+","+portfolioStatus+","+jbTeacher.getJobId().getJobId()+",'done'";
					resp[3]="done";
				}
			}
			if(jbTeacher.getIsAffilated()!=null){
				if(offerEPI){
					resp[4]=""+jbTeacher.getIsAffilated();
				}
				else if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2)
				{
					resp[4]=""+jbTeacher.getIsAffilated();
				}
				else{
					resp[4]=""+0;
				}
			}else{
				resp[4]=""+0;
			}

			if(offerJSI){
				resp[5]=""+isJobAssessment;
			}else{
				resp[5]=""+false;
			}
			try{
				System.out.println("resp::::"+resp[0]+":::::::"+resp[1]+":::::::::::"+resp[2]+":::::"+resp[3]+":::::::"+resp[4]+":::::::"+resp[5]);
			}catch(Exception e){
				e.printStackTrace();
			}
			
			try
			{
				if(jobOrder.getDistrictMaster()!=null && (jobOrder.getDistrictMaster().getDistrictId()==1303840 || jobOrder.getDistrictMaster().getDistrictId()==1200420)){
					resp[1] = "true";
					resp[3] = "epi";
				}
			}catch(Exception e){
				e.printStackTrace();	
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return resp;
	}
	public String[] getTeacherJobDone(String jobId)
	{
		System.out.println("=================================getTeacherJobDone:::::::::::::::::::;; ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		String[] resp = new String[6];
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		try 
		{		
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			JobOrder jobOrder = new JobOrder();
			if(jobId!=null && !jobId.equals(""))
			jobOrder.setJobId(Integer.parseInt(jobId));
			List<JobForTeacher> lstJft = jobForTeacherDAO.findJobByTeacherAndSatusFormJobId(teacherDetail, jobOrder);
			if(lstJft.size()!=0)
			{
				System.out.println("1");
				JobForTeacher jobForTeacher = lstJft.get(0);
				jobOrder=jobForTeacher.getJobId();
				boolean completeFlag=false;
				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				StatusMaster statusMaster = statusMasterDAO.findStatusByShortName("icomp");
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
				boolean prevStatus=false;
				System.out.println("2");
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==2)
				{
					System.out.println("3");
					String ncInternalFlag ="";
					try {
						ncInternalFlag = (String) session
								.getAttribute("Currentforinternal");
						ncInternalFlag=(ncInternalFlag==null?"":ncInternalFlag);
					} catch (NullPointerException e) {
						ncInternalFlag ="";
						e.printStackTrace();
					}
					if(ncInternalFlag!=null && !ncInternalFlag.equals(""))
					{
						if(ncInternalFlag.equalsIgnoreCase("c"))
							statusMaster =statusMasterDAO.findStatusByShortName("comp");
					}
				}
				System.out.println("4");
				if(jobForTeacher.getInternalSecondaryStatus()!=null){
					System.out.println("5");
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(null);
					jobForTeacher.setSecondaryStatus(jobForTeacher.getInternalSecondaryStatus());
				}else if(jobForTeacher.getInternalStatus()!=null){
					System.out.println("6");
					jobForTeacher.setStatus(jobForTeacher.getInternalStatus());
					jobForTeacher.setStatusMaster(jobForTeacher.getInternalStatus());
				}else{
					System.out.println("7");
					String jftStatus=jobForTeacher.getStatus().getStatusShortName();
					if(jftStatus.equalsIgnoreCase("icomp")|| jftStatus.equalsIgnoreCase("comp") || jftStatus.equalsIgnoreCase("vlt")){
						try{
							if(!jftStatus.equalsIgnoreCase("comp") && statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
								completeFlag=true;
							}
						}catch(Exception e){
							e.printStackTrace();
						}
						jobForTeacher.setStatus(statusMaster);
						if(jobForTeacher.getSecondaryStatus()!=null){
							jobForTeacher.setStatusMaster(null);
						}else{
							jobForTeacher.setStatusMaster(statusMaster);
						}
					}else{
						prevStatus=true;
					}
				}
				jobForTeacherDAO.makePersistent(jobForTeacher);
				try{
					if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
						commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				System.out.println("8");
				jobOrder = jobForTeacher.getJobId();

				try {
					System.out.println("9");
					resp[0] = jobOrder.getExitURL();
					resp[1] = jobOrder.getExitMessage();
					StatusMaster stm =jobForTeacher.getStatus(); 
					if((stm!=null && (stm.getStatusShortName().equalsIgnoreCase("comp") || prevStatus)))
						resp[2] = "y";
					else
						resp[2] = "n";

					System.out.println("resp[0]:"+resp[0]+" resp[1]:"+resp[1]+" resp[2]:"+resp[2]);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}


		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}


		return resp;
	}


	public String getJobsOfInterestAjax_old(String searchBy,String noOfRows,String page,String sortOrderStr,String sortOrderType,String dt,String districtId,String schoolId,String stateId,String cityId,String subjectId,String sGeoZoneId)
	{
		PrintOnConsole.debugPrintJOI("500");
		System.out.println("Calling getJobsOfInterestgrid Ajax");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		boolean  isImCandidate=false;
		if(session != null && (session.getAttribute("isImCandidate")!=null))
		{
			isImCandidate = (Boolean)session.getAttribute("isImCandidate");
		}
		try 
		{		
			String teacherclassification=(String) session.getAttribute("classification");
			////////////////////////


			boolean statusAvlbl = false;
			boolean statusComp = false;
			boolean statusIcomp = false;
			boolean statusVlt =	false;
			boolean statusWidrwn = false;
			boolean statusHiden = false;
			boolean statusAll =	false;
			boolean isEpiStandalone = false; 

			Integer geoZoneId=0;
			String zoneId   =sGeoZoneId;  //request.getParameter("geoZoneId");

			System.out.println("zoneId "+zoneId +" sGeoZoneId "+sGeoZoneId);

			if(zoneId!=null && !zoneId.equalsIgnoreCase(""))
				geoZoneId =  Integer.parseInt(zoneId);

			try
			{
				TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
				//-- set start and end position
				if(session.getAttribute("epiStandalone")!=null)
					isEpiStandalone = (Boolean) session.getAttribute("epiStandalone");

				String noOfRow =noOfRows; //request.getParameter("noOfRows")==null?"1":request.getParameter("noOfRows");
				if(noOfRows==null)
					noOfRow="1";

				String pageNo =page;	//request.getParameter("page")==null?"1":request.getParameter("page");
				if(page==null)
					pageNo="1";


				int noOfRowInPage =	Integer.parseInt(noOfRow);
				int pgNo =	Integer.parseInt(pageNo);
				String sortOrder =sortOrderStr; //request.getParameter("sortOrderStr").trim();

				//String sortOrderType = request.getParameter("sortOrderType").trim();

				// For searching parameter
				//String districtId = request.getParameter("districtId").trim();
				String schoolId1 =schoolId; //request.getParameter("schoolId").trim();
				String cityName =cityId; //request.getParameter("cityId").trim();
				//String stateId = request.getParameter("stateId").trim();
				//String subjectId = request.getParameter("subjectId").trim();

				int start = ((pgNo-1)*noOfRowInPage);
				int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------


				/***** Sorting by Domain Name,Competency Name and Objective Name Stating ( Sekhar ) ******/
				SortedMap map = new TreeMap();
				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"createdDateTime";
				String sortOrderNoField		=	"createdDateTime";
				//System.out.println("\n  Gagan : sort order sortOrderFieldName  "+sortOrderFieldName+" sortOrderNoField "+sortOrderNoField);
				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal		=	null;
				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("jobTitle") && !sortOrder.equals("districtName")&& !sortOrder.equals("schoolName") && !sortOrder.equals("location") && !sortOrder.equals("geoZoneName")){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
					if(sortOrder.equals("jobTitle"))
					{
						sortOrderNoField="jobTitle";
					}
					if(sortOrder.equals("districtName"))
					{
						sortOrderNoField="districtName";
					}
					if(sortOrder.equals("schoolName"))
					{
						sortOrderNoField="schoolName";
					}
					if(sortOrder.equals("location"))
					{
						sortOrderNoField="location";
					}
					if(sortOrder.equals("geoZoneName"))
					{
						sortOrderNoField="geoZoneName";
					}
				}
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else
					{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
				/**End ------------------------------------**/




				boolean prefStatus = false;
				boolean portfolioStatus = false;
				boolean baseInvStatus=false;
				TeacherPreference tPreference=null;
				TeacherPortfolioStatus tPortfolioStatus=null;

				boolean flagJobOrderList = false; 

				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;

				ArrayList<Integer> subjectIdList=  new ArrayList<Integer>();
				List<SubjectMaster> ObjSubjectList = new ArrayList<SubjectMaster>();

				List<JobOrder> jobOrderAllList = new ArrayList<JobOrder>(); 
				List<JobOrder> districtMasterlst = new ArrayList<JobOrder>(); 
				List<JobOrder> schoolMasterlst = new ArrayList<JobOrder>(); 
				List<JobOrder> subjectList = new ArrayList<JobOrder>(); 
				List<JobOrder> zipCodeList2 = new ArrayList<JobOrder>();
				List<String> zipList	=	new ArrayList<String>();
				List<CityMaster> lstcityMasters = new ArrayList<CityMaster>();
				List<JobOrder> jobOrderAllListWithIsPool = new ArrayList<JobOrder>();
				List<JobOrder> jobByState	=	new ArrayList<JobOrder>();

				try
				{
					if(!isImCandidate){
						//jobOrderAllListWithIsPool = jobOrderDAO.findAllJobWithPoolCondition();
						jobOrderAllListWithIsPool = jobOrderDAO.findAllJobWithPoolCondition(sortOrderStrVal);
					 }
				}catch(Exception e)
				{
					e.printStackTrace();
				}


				tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
				if(tPreference!=null){
					prefStatus = true;
				}

				tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()){
					portfolioStatus = true;
				}
				TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
				if((teacherBaseAssessmentStatus!=null) && (!teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") )){
					baseInvStatus=true;				 
				}
				List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
				HashSet<JobOrder> masterJOb = new HashSet<JobOrder>();
				HashSet<JobOrder> associateJobs = new HashSet<JobOrder>();
				Map<Integer, TeacherStatusHistoryForJob> applyJObStatus = new HashMap<Integer, TeacherStatusHistoryForJob>();
				//mukesh
				if(isImCandidate)
				{
					DistrictMaster districtMaster1=new DistrictMaster();
					districtMaster1=(DistrictMaster) session.getAttribute("districtIdForImCandidate");
					List<JobOrder> jobOrderAllList1 =jobOrderDAO.findActiveJobOrderbyDistrict(districtMaster1);
					
					lstJobOrder.addAll(jobOrderAllList1);
				}else
				{
					if(teacherDetail.getUserType().equalsIgnoreCase("N"))
					{
						List<JobOrder> lstJobs = new ArrayList<JobOrder>(jobOrderAllListWithIsPool);
						PrintOnConsole.debugPrintJOI("501");
						lstJobOrder = getJobByPrefnew(request,sortOrderStrVal,lstJobs);
						/*lstJobOrder = getJobByPref(request,sortOrderStrVal);*/
						PrintOnConsole.debugPrintJOI("502");	
					}
					else
					{
						List<JobForTeacher> lstJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
						if(lstJFT.size()>0){
							if(lstJFT.get(0)!=null && lstJFT.get(0).getJobId().getHeadQuarterMaster()!=null){
								lstJobOrder = jobOrderDAO.findSortedJobOrderbyHBD(lstJFT.get(0).getJobId().getHeadQuarterMaster(),sortOrderStrVal);
							}else{
								lstJobOrder = jobOrderDAO.findSortedJobOrderbyDistrict(lstJFT.get(0).getJobId().getDistrictMaster(),sortOrderStrVal);
							}
						}
					}
					//******************* Umrella/Associate/Certificates Logic on philli jobs**********************************
					
					List<JobOrder> temJobLIsts=new ArrayList<JobOrder>();
					//creating temporary list for deleting 
					temJobLIsts.addAll(lstJobOrder);
					for (JobOrder jobOrder : temJobLIsts) 
					{
						if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && jobOrder.getIsVacancyJob()){
							associateJobs.add(jobOrder);
							lstJobOrder.remove(jobOrder);
						}				
						if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && !jobOrder.getIsVacancyJob()){					
							masterJOb.add(jobOrder);
						}
					}			
					System.out.println("111>>>>>>>11 masterJOb.size() :: "+masterJOb.size()+" associateJobs.size() :: "+associateJobs.size());
					//check for Screaning complete status
					List<TeacherStatusHistoryForJob>  historyforScreaningComlete= new ArrayList<TeacherStatusHistoryForJob>();
					//Map<jobId, TeacherStatusHistoryForJob> 
					
					if(masterJOb!=null && masterJOb.size()>0){
						StatusMaster statusMaster=statusMasterDAO.findById(16,false,false);
						historyforScreaningComlete = teacherStatusHistoryForJobDAO.getScreningCompleteJobsByJobsAndTeacher(teacherDetail, masterJOb, statusMaster);			
						for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyforScreaningComlete) {
							applyJObStatus.put(teacherStatusHistoryForJob.getJobOrder().getJobId(), teacherStatusHistoryForJob);
						}
					}
				}

				CandidateGridService gridService=new CandidateGridService();
				List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
				String[] statuss = {"icomp","vlt","widrw","hide"};
				try{
					lstStatusMasters = statusMasterDAO.findStatusByShortNames(statuss);
				}catch (Exception e) {
					e.printStackTrace();
				}
				List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);

				///////////////////////////////////////////////////////////////////


				List<JobOrder> jobLst= new ArrayList<JobOrder>();
				for (JobForTeacher jobForTeacher : lstAllJFT) {
					jobLst.add(jobForTeacher.getJobId());
				}
				List <StatusMaster> statusMasterList=statusMasterDAO.findAllStatusMaster();
				Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
				for (StatusMaster statusMaster : statusMasterList) {
					mapStatus.put(statusMaster.getStatusShortName(),statusMaster);
				}

				TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
				Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
				Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
				List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
				List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
				List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
				if(assessmentJobRelations1.size()>0){
					for(AssessmentJobRelation ajr: assessmentJobRelations1){
						mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
						adList.add(ajr.getAssessmentId());
					}
					if(adList.size()>0)
						lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentList(teacherDetail,adList);
				}
				for(TeacherAssessmentStatus ts : lstJSI){
					mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
				}

				DistrictMaster districtMasterInternal=null;
				List<InternalTransferCandidates> lstITRA=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
				InternalTransferCandidates internalITRA = null;
				if(lstITRA.size()>0){
					internalITRA=lstITRA.get(0);
					districtMasterInternal=internalITRA.getDistrictMaster();
				}
				//////////////////////////////////
				List<JobForTeacher> lstincompletJFT =new ArrayList<JobForTeacher>(); // jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusInComplete);			
				List<JobForTeacher> lstviolatedJFT = new ArrayList<JobForTeacher>();//jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusViolated);
				List<JobForTeacher> lstHideJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusHide);
				List<JobForTeacher> lstWidrwJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusWidrw);

				List<JobForTeacher> lstcompletJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatusList(teacherDetail,statusMasters);

				for(JobForTeacher jobForTeacher: lstAllJFT)
				{				
					JobOrder jobOrder = jobForTeacher.getJobId();				
					if(!isImCandidate){
						if(masterJOb.contains(jobOrder) && applyJObStatus.containsKey(jobOrder.getJobId()))
						{
							for (JobOrder jobOrderAssoc : associateJobs) {
								if(jobOrderAssoc.getJobCategoryMaster().equals(jobOrder.getJobCategoryMaster())){
									lstJobOrder.add(jobOrderAssoc);
								}
							}
							
						}
					}
					
					if(lstJobOrder.contains(jobOrder))
					{
						lstJobOrder.remove(jobOrder);					
					}

					StatusMaster statusMaster = mapStatus.get("icomp");
					statusMaster=findByTeacherIdJobStausForLoop(internalITRA,jobForTeacher,mapAssess,teacherAssessmentStatusJSI,mapJSI,districtMasterInternal,tPortfolioStatus,mapStatus,teacherBaseAssessmentStatus);
					//System.out.println(":::::::::::::::::::::::::::::::::::::::::::::"+jobOrder.getJobId()+"  "+statusMaster.getStatusShortName());

					if(statusMaster.getStatusShortName().equalsIgnoreCase("comp") && (jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")|| jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("rem") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("scomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("ecomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vcomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("dcln"))){
						lstcompletJFT.add(jobForTeacher);
					}else if(!statusMaster.getStatusShortName().equalsIgnoreCase("vlt") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide") && (statusMaster.getStatusShortName().equalsIgnoreCase("icomp") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp")|| jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("rem") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("scomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("ecomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vcomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("dcln"))){
						lstincompletJFT.add(jobForTeacher);
					}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt")){
						lstviolatedJFT.add(jobForTeacher);
					}else if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw")){
						lstWidrwJFT.add(jobForTeacher);
					}else if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide")){
						lstHideJFT.add(jobForTeacher);
					}
					/////////////////////////////////////////////////////////////////
				}

				/*	System.out.println("lstcompletJFT:::::::"+lstcompletJFT.size());
				System.out.println("lstincompletJFT:::::::"+lstincompletJFT.size());
				System.out.println("lstviolatedJFT:::::::"+lstviolatedJFT.size());
				System.out.println("lstWidrwJFT:::::::"+lstWidrwJFT.size());
				System.out.println("lstHideJFT:::::::"+lstHideJFT.size());
				 */
				List<JobForTeacher> finalListForJobsOfInterest	=	new ArrayList<JobForTeacher>();
				List<JobOrder> finalListForJobsOfInterestJobOrders	=	new ArrayList<JobOrder>();

				//String searchBy = request.getParameter("searchBy")==null?"":request.getParameter("searchBy");

				// Start Searching	
				if(searchBy.equalsIgnoreCase("avlbl"))
				{
					statusAvlbl=true;
					finalListForJobsOfInterestJobOrders.addAll(lstJobOrder);
					flagJobOrderList=true;
				}
				else if(searchBy.equalsIgnoreCase("comp"))
				{
					statusComp=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstcompletJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else if(searchBy.equalsIgnoreCase("icomp"))
				{
					statusIcomp=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstincompletJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else if(searchBy.equalsIgnoreCase("vlt"))
				{
					statusVlt=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstviolatedJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else if(searchBy.equalsIgnoreCase("widrw"))
				{
					statusWidrwn=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstWidrwJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else if(searchBy.equalsIgnoreCase("hide"))
				{
					statusHiden=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstHideJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else
				{
					statusAll = true;
					flagJobOrderList=true;
					finalListForJobsOfInterestJobOrders.clear();

					for (JobForTeacher jobForTeacher : lstincompletJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Incomplete");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
					for (JobOrder job : lstJobOrder) 
					{
						JobOrder jo=job;
						jo.setExitMessage(job.getExitURL());
						jo.setExitURL("Available");						
						finalListForJobsOfInterestJobOrders.add(jo);
					}

					for (JobForTeacher jobForTeacher : lstcompletJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Completed");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
					for (JobForTeacher jobForTeacher : lstviolatedJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Timed Out");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
					for (JobForTeacher jobForTeacher : lstWidrwJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Withdrawn");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
					for (JobForTeacher jobForTeacher : lstHideJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Hidden");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}

				/* @Start
				 * @Ashish Kumar
				 * @Description :: Searching Data
				 * */

				if(!cityName.equals("0")){
					zipList = cityMasterDAO.findCityByName(cityName);
				}else{
					/*if(stateId!=null && !stateId.equals("")){
						StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);

						// city List By State Id
						lstcityMasters	=	cityMasterDAO.findCityByState(stateMaster);

						for(CityMaster c:lstcityMasters)
						{
							//System.out.println(" city name :: "+c.getCityName()+" zip code :: "+c.getZipCode());
							zipList.add(c.getZipCode());
						}
					}*/
				}

				/*for(String g:zipList)
				{
					System.out.println(" "+g);
				}*/



				if(finalListForJobsOfInterestJobOrders.size()>0){
					flagJobOrderList=true;
					jobOrderAllList.addAll(finalListForJobsOfInterestJobOrders);
				}

				// For District Search
				if(!districtId.equals("") && !districtId.equals(""))
				{
					districtMaster  = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
					districtMasterlst = jobOrderDAO.findJobOrderByDistrict(districtMaster);

					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(districtMasterlst);
					}else{
						jobOrderAllList.addAll(districtMasterlst);
					}

				}

				// For School Search
				if(!schoolId1.equals("0") && !schoolId1.equals(""))
				{
					schoolMaster 	=	schoolMasterDAO.findById(new Long(schoolId1), false, false);
					schoolMasterlst.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList));

					if(flagJobOrderList==true)
						jobOrderAllList.retainAll(schoolMasterlst);
					else
						jobOrderAllList.addAll(schoolMasterlst);

				}


				// For Subject
				if(subjectId !=null && !subjectId.equals("0,") && !subjectId.equals(""))
				{
					String subIdList[] = subjectId.split(",");
					if(Integer.parseInt(subIdList[0])!=0)
					{
						for(int i=0;i<subIdList.length;i++)
						{
							subjectIdList.add(Integer.parseInt(subIdList[i]));
						}
						ObjSubjectList = subjectMasterDAO.getSubjectMasterlist(subjectIdList);

						subjectList = jobOrderDAO.findJobOrderBySubjectList(ObjSubjectList);

						if(flagJobOrderList==true){
							jobOrderAllList.retainAll(subjectList);
						}else{
							jobOrderAllList.addAll(subjectList);
						}
					}
				}

				// For Cities (ZipCode List)
				if(zipList!=null && zipList.size()>0)
				{
					List<DistrictMaster> lstDistrictMasters = new ArrayList<DistrictMaster>();
					List<SchoolMaster> lstSchoolMasters = null;

					lstSchoolMasters = schoolMasterDAO.findByZipCodeList(zipList);

					if(lstSchoolMasters!=null)
					{
						for(SchoolMaster s:lstSchoolMasters)
						{
							lstDistrictMasters.add(s.getDistrictId());
						}
					}

					if(lstDistrictMasters!=null && lstDistrictMasters.size()>0)
					{
						zipCodeList2 = jobOrderDAO.findJobOrderByDistrictList(lstDistrictMasters);
					}

					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(zipCodeList2);
					}else{
						jobOrderAllList.addAll(zipCodeList2);
					}

				}

				try{
					if(!stateId.equals("0") && !stateId.equals(""))
					{
						List<DistrictMaster> districtByState = new ArrayList<DistrictMaster>();
						StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
						districtByState = districtMasterDAO.getDistrictListByState(stateMaster);

						if(districtByState!=null && districtByState.size()>0)
						{
							jobByState = jobOrderDAO.findJobOrderByDistrictList(districtByState);
						}	

						if(flagJobOrderList==true){
							jobOrderAllList.retainAll(jobByState);
						}else{
							jobOrderAllList.addAll(jobByState);
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}




				/* @End
				 * @Ashish Kumar
				 * @Description :: Searching Data
				 * */
				// filtering by mukesh

				List<JobOrder> listjobOrdersgeoZone = new ArrayList<JobOrder>();
				if(geoZoneId>0)
				{
					GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);

				}

				if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
				{
					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(listjobOrdersgeoZone);
					}else{
						jobOrderAllList.addAll(listjobOrdersgeoZone);
					}

				}				

				if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
				{
					jobOrderAllList.addAll(listjobOrdersgeoZone);

				}
				else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
				{
					jobOrderAllList.retainAll(listjobOrdersgeoZone);
				}
				/*============For Paging and  Sorting ===============*/
				List<JobOrder> sortedlstJobOrder		=	new ArrayList<JobOrder>();

				SortedMap<String,JobOrder>	sortedMap 	= 	new TreeMap<String,JobOrder>();
				if(sortOrderNoField.equals("jobTitle"))
				{
					sortOrderFieldName	=	"jobTitle";
				}
				if(sortOrderNoField.equals("jobEndDate"))
				{
					sortOrderFieldName	=	"jobEndDate";
				}
				if(sortOrderNoField.equals("districtName"))
				{
					sortOrderFieldName	=	"districtName";
				}
				if(sortOrderNoField.equals("schoolName"))
				{
					sortOrderFieldName	=	"schoolName";
				}
				if(sortOrderNoField.equals("location"))
				{
					sortOrderFieldName	=	"location";
				}
				if(sortOrderNoField.equals("geoZoneName"))
				{
					sortOrderFieldName	=	"geoZoneName";
				}
				if(jobOrderAllList!=null && jobOrderAllList.size()>0 && jobOrderAllListWithIsPool!=null && jobOrderAllListWithIsPool.size()>0)
				{
					jobOrderAllList.retainAll(jobOrderAllListWithIsPool);
				}
				//******************** here will be Job Certification logic for Philly ***************************

				List<CertificateTypeMaster> certObjList = new ArrayList<CertificateTypeMaster>();
				String crtIdList="";
				List<String> certList = new ArrayList<String>();
				
				List<TeacherCertificate> lstTeacherCertificates=teacherCertificateDAO.findCertificateAscByTeacher(teacherDetail);//teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
				if(lstTeacherCertificates!=null){
					for(TeacherCertificate tc : lstTeacherCertificates){
						if(tc.getCertificateTypeMaster()!=null)
						{
							if(crtIdList.equalsIgnoreCase(""))
								crtIdList+=tc.getCertificateTypeMaster().getCertTypeId();
				             else
								crtIdList+=","+tc.getCertificateTypeMaster().getCertTypeId();
							certList.add(tc.getCertificateTypeMaster().getCertTypeId()+"");
							certList.add(crtIdList);
							certObjList.add(tc.getCertificateTypeMaster());
						}
					}
				}
				
				List<JobOrder> listOfJobId = new ArrayList<JobOrder>();
				//listOfJobId.addAll(jobOrderAllList1);
				for (JobOrder jobOrder : jobOrderAllList){
					if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==4218990){
						listOfJobId.add(jobOrder);					
					}
				}
				
				Map<JobOrder, String> jobCertMap = new HashMap<JobOrder, String>();
				if(listOfJobId!=null && listOfJobId.size()>0){
				List<JobCertification> findByJobIDS = jobCertificationDAO.findCertificationsByJobOrders(listOfJobId,certObjList);
					for (JobCertification jobCertification : findByJobIDS) {
						String certId = jobCertification.getCertificateTypeMaster().getCertTypeId()+"";
						jobOrderAllList.remove(jobCertification.getJobId());                                                                                      
						if(jobCertMap.containsKey(jobCertification.getJobId())){
							String existingCertId = jobCertMap.get(jobCertification.getJobId());
							jobCertMap.put(jobCertification.getJobId(), existingCertId+","+certId);
							System.out.println(jobCertification.getJobId().getJobId()+"  "+existingCertId+","+certId);
						}else{
							jobCertMap.put(jobCertification.getJobId(), certId);
							System.out.println(jobCertification.getJobId().getJobId()+"  "+certId);
						}
					}
				}
				List<JobOrder> addMatchJobs=new ArrayList<JobOrder>(); 
				for (String c1 : certList) {
					 for(java.util.Map.Entry<JobOrder, String> entry : jobCertMap.entrySet()){
						 /*if(entry.getKey().getClassification()!=null){
							 if(c1.equalsIgnoreCase(entry.getValue()) && entry.getKey().getClassification().equalsIgnoreCase(teacherclassification)){                                                                                                                                                                                                                                                                                         
									addMatchJobs.add(entry.getKey());
									jobOrderAllList.add(entry.getKey());
							 }
						 }else{
							 if(c1.equalsIgnoreCase(entry.getValue())){                                                                                                                                                                                                                                                                                         
									addMatchJobs.add(entry.getKey());
									jobOrderAllList.add(entry.getKey());
							 }
						 }*/
						 if(c1.equalsIgnoreCase(entry.getValue())){                                                                                                                                                                                                                                                                                         
								addMatchJobs.add(entry.getKey());
								jobOrderAllList.add(entry.getKey());
						 }
					  }
				 }
		   System.out.println("addMatchJobs.size() :: "+addMatchJobs.size()+"jobOrderAllList :: "+jobOrderAllList.size());
		
		//####################################################################

				int mapFlag=2;
				for (JobOrder jobOrder : jobOrderAllList){
					String orderFieldName=jobOrder.getStatus();
					if(sortOrderFieldName.equals("jobTitle")){
						orderFieldName=jobOrder.getJobTitle().toUpperCase()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					if(sortOrderFieldName.equals("jobEndDate")){
						orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate())+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}

					if(jobOrder.getGeoZoneMaster()!=null){
						orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName();
						if(sortOrderFieldName.equals("geoZoneName")){
							orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName()+"||"+jobOrder.getJobId();
							sortedMap.put(orderFieldName+"||",jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
					}else{
						if(sortOrderFieldName.equals("geoZoneName")){
							orderFieldName="0||"+jobOrder.getJobId();
							sortedMap.put(orderFieldName+"||",jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
					}

					if(sortOrderFieldName.equals("districtName")){
						if(jobOrder.getHeadQuarterMaster()!=null){
							orderFieldName=jobOrder.getHeadQuarterMaster().getHeadQuarterName()+"||"+jobOrder.getJobId();
						}else{
							orderFieldName=jobOrder.getDistrictMaster().getDistrictName()+"||"+jobOrder.getJobId();
						}
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}

					if(sortOrderFieldName.equals("schoolName")){
						if(jobOrder.getCreatedForEntity().equals(3)){
							orderFieldName=jobOrder.getSchool().get(0).getSchoolName()+"||"+jobOrder.getJobId();
							sortedMap.put(orderFieldName+"||",jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
						else{
							if(jobOrder.getSchool().size()==1){
								orderFieldName=jobOrder.getSchool().get(0).getSchoolName()+"||"+jobOrder.getJobId();
							}else if(jobOrder.getSchool().size()>1){
								orderFieldName="Many Schools||"+jobOrder.getJobId();
							}else{
								orderFieldName=""+"0000||"+jobOrder.getJobId();
							}
							sortedMap.put(orderFieldName+"||",jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
					}
					if(sortOrderFieldName.equals("location")){

						if(jobOrder.getCreatedForEntity()!=3)
						{
							if(jobOrder.getDistrictMaster()!=null && !jobOrder.getDistrictMaster().getAddress().isEmpty())
								orderFieldName=jobOrder.getDistrictMaster().getAddress()+"||"+jobOrder.getJobId();
							else
								orderFieldName="0000"+"||"+jobOrder.getJobId();
						}
						else
						{
							if(!jobOrder.getSchool().get(0).getAddress().isEmpty())
								orderFieldName=jobOrder.getSchool().get(0).getAddress()+"||"+jobOrder.getJobId();
							else
								orderFieldName="0000"+"||"+jobOrder.getJobId();
						}
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}

					}
				}
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();

					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
					}
				}else{

					sortedlstJobOrder=jobOrderAllList;
				}

				totalRecord =sortedlstJobOrder.size();

				if(totalRecord<end)
					end=totalRecord;
				List<JobOrder> lstsortedJobOrder =	sortedlstJobOrder.subList(start,end);


				String responseText="";

				sb.append("<table border='0' id='tblGridJoF'>");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				responseText=PaginationAndSorting.responseSortingLink(lblJoTil,sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblExpiryDate,sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
				sb.append("<th width='7%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
				sb.append("<th width='18%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblZone,sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
				sb.append("<th width='10%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblSchool,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
				sb.append("<th width='18%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblAdd,sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
				sb.append("<th width='15%' valign='top'>"+responseText+"</th>");

				sb.append("<th width='5%' valign='top'>"+lblJoStatus+"</th>");
				sb.append("<th width='10%' valign='top'>"+lblAct+"</th>");
				sb.append("</tr>");
				sb.append("</thead>");

				int rowCount=0;
				int tempCount = 0;
				boolean dispName=false;
				String zone = "";
				if(statusIcomp)//for status Incomplete
				{
					for(JobOrder jb : lstsortedJobOrder)
					{						
						dispName=false;
						rowCount++;
						sb.append("<tr>");
						sb.append("<td><a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
						
						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>+lblUntilfilled+</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");
						if(jb.getCreatedForEntity()==3)
						{
							tempCount=0;
							for(SchoolMaster school:jb.getSchool())
							{								
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{				
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append(school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");		
								}
								break;
							}	
						}
						else
						{

							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}

							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{	
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+")\";>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}

							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}

							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}

							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}

							}else{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}
						}


						sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
						sb.append("<td>");


						//sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							/*if(prefStatus == false || portfolioStatus == false){
								sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+","+false+");\"><img src='images/option02.png'></a>");
							}
							else if(jb.getJobCategoryMaster().getBaseStatus() && (!baseInvStatus)){
								sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('0')\"><img src='images/option02.png'></a>");
							}
							else{
								sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
							}*/
						
							sb.append("<a data-original-title='"+lblCompleteNow+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria("+jb.getJobId()+");\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						
						}



						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}


				if(statusAvlbl)//for status available jobs
				{
					//totaRecord=totaRecord+lstJobOrder.size();
					for(JobOrder jbOrder: lstsortedJobOrder)
					{			
						rowCount++;
						sb.append("<tr>");
						sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
						

						//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>"+lblUntilfilled+"</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate())+",11:59 PM CST</td>");

						if(jbOrder.getCreatedForEntity()==3)//School
						{
							for(SchoolMaster school:jbOrder.getSchool())
							{
								if(jbOrder.getDistrictMaster().getDisplayName()!=null && (!jbOrder.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jbOrder.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jbOrder.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jbOrder.getGeoZoneMaster()!=null && !jbOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
								{
									if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
								{
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append(school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jbOrder));
									sb.append("</td>");		
								}
								break;
							}

						}
						else
						{
							sb.append("<td>");
							if(jbOrder.getDistrictMaster()!=null)
								sb.append(""+jbOrder.getDistrictMaster().getDistrictName());
							else
								sb.append("");
							sb.append("</td>");
							if(jbOrder.getGeoZoneMaster()!=null && !jbOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}						
							if(jbOrder.getSchool().size()==1)
							{
								if(jbOrder.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
								}else{
									if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
									}
									else
									{
										sb.append("<td>"+jbOrder.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jbOrder));
										sb.append("</td>");
									}
								}
							}
							else if(jbOrder.getSchool().size()>1)
							{
								String schoolListIds = jbOrder.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jbOrder.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jbOrder.getSchool().get(i).getSchoolId().toString();
								}
								if(jbOrder.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
								}else{
									if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>"+lnkManySchools+"</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>"+lblManyAddress+"</a></td>");
									}
								}
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jbOrder));
								sb.append("</td>");
							}

						}

						sb.append("<td><strong class='text-info'>"+lblAvailable+"</strong></td>");
						sb.append("<td>");

						if(isEpiStandalone){
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='"+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+lblhide+"' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return getJbsIHide('"+jbOrder.getJobId()+"')\"><img src='images/unhide.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}


						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}

				if(statusComp)//for status complete
				{
					//totaRecord=totaRecord+lstcompletJFT.size();
					for(JobOrder jb : lstsortedJobOrder)
					{

						rowCount++;
						sb.append("<tr>");
						sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
						//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							tempCount=0;
							for(SchoolMaster school:jb.getSchool())
							{

								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{				

									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//sb.append("&nbsp;"+school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								if(tempCount==0)
								{
									tempCount++;
									break;
								}
							}
							if(tempCount==0)
							{
								sb.append("<td>&nbsp;&nbsp;</td>");
								sb.append("<td>&nbsp;&nbsp;</td>");
								sb.append("<td>&nbsp;&nbsp;</td>");

							}

						}
						else
						{
							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>"+lnkManySchools+"</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>"+lblManyAddress+"</a></td>");

									}
								}
							}
							else
							{
								//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$44");    
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}

						}

						sb.append("<td><strong class='text-success'>"+lblCompleted+"</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							
							if(jb.getDistrictMaster()!= null && jb.getDistrictMaster().getDistrictId()!=1200390) {
								if(mapAssess!=null){
									Integer assessmentId=mapAssess.get(jb.getJobId());								
									if(assessmentId!=null)
									{
										teacherAssessmentStatusJSI = mapJSI.get(assessmentId);
										if(teacherAssessmentStatusJSI.getStatusMaster().getStatusId()==4)
											sb.append("&nbsp;&nbsp;<a data-original-title='JSI' class='fa fa-book fa-2x thumbUpIconBlueColor' rel='tooltip' id='JSI"+(rowCount)+"' href='javascript:void(0);' onclick=\"return checkJSI('"+jb.getJobId()+"')\"></a>");
									}
								}
							}
						}						
						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}

				if(statusVlt)//for violated
				{
					//totaRecord=totaRecord+lstviolatedJFT.size();
					//for(JobForTeacher jbTeacher : lstviolatedJFT)
					for(JobOrder jb : lstsortedJobOrder)
					{
						rowCount++;
						sb.append("<tr>");
						sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");

						//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>"+lblUntilfilled+"</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							for(SchoolMaster school:jb.getSchool())
							{
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{				
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");


									sb.append("<td>");
									//	sb.append("&nbsp;"+school.getAddress());

									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								break;
							}
						}
						else
						{
							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{	
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");
										sb.append("<td>"+getDistrictAddress(jb)+"</td>"); 
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>"+lnkManySchools+"</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>"+lblManyAddress+"</a></td>");
									}
								}
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}

						}

						sb.append("<td><strong class='text-error'>"+optTOut+"</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' ></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}

						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}

				/////////////////// Withdrawn ///////////////////////////
				if(statusWidrwn)//for Withdrawn
				{
					//totaRecord=totaRecord+lstWidrwJFT.size();
					//for(JobForTeacher jbTeacher : lstWidrwJFT)
					for(JobOrder jb : lstsortedJobOrder)
					{
						rowCount++;
						sb.append("<tr>");
						sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");

						//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>"+lblUntilfilled+"</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							for(SchoolMaster school:jb.getSchool())
							{

								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{	
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390  || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append("&nbsp;"+school.getAddress());

									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								break;
							}
						}
						else
						{

							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}

							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td<a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}
						}

						sb.append("<td><strong class='text-error'>"+optWithdrawn+"</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){						
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='"+toolRe_Apply+"' rel='tooltip' id='tpApply"+rowCount+"' href='javascript:void(0);' onclick=\"getStateBeforeWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option01.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}

						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}
				/////////////////// widraw end ///////////////////////////
				if(statusHiden)//for Hidden
				{
					//totaRecord=totaRecord+lstHideJFT.size();
					for(JobOrder jb : lstsortedJobOrder)
					{
						rowCount++;
						sb.append("<tr>");
						sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");

						//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							for(SchoolMaster school:jb.getSchool())
							{

								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{		
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}

								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append("&nbsp;"+school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								break;
							}
						}
						else
						{

							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}	
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}
						}

						sb.append("<td><strong class='text-error'>Hidden</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){						
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='Un-hide' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"unHideJob('"+jb.getIpAddress()+"');\"><img src='images/option03.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}

						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}
				/////////////////// Hidden end ///////////////////////////
				if(statusAll)//for statusAll
				{
					//totaRecord=totaRecord+lstHideJFT.size();
					//for(JobForTeacher jbTeacher : finalListForJobsOfInterest)
					for(JobOrder jb : lstsortedJobOrder)
					{
						rowCount++;
						sb.append("<tr>");
						if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getExitURL().equalsIgnoreCase("Available") && jb.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
						{
							sb.append("<td><a data-original-title='"+lblApplyNow+"' rel='tooltip' id='apply' onclick=\"chkApplyJobNonClientDashJOI('"+jb.getJobId()+"','"+jb.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jb.getExitMessage()+"','"+jb.getDistrictMaster().getDistrictId()+"','');\" href='javascript:void(0);' >"+jb.getJobTitle()+"</a></td>");						
						}						
						else
						{
							sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
						}

						//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							for(SchoolMaster school:jb.getSchool())
							{
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{	
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");								
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{						
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append(school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								break;
							}
						}
						else
						{
							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}else{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{		
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");											
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}	
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{	
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");											
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}	
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");							
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}
						}					
						if(jb.getExitURL().equalsIgnoreCase("Incomplete"))
						{
							sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
							sb.append("<td>");



							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
							}
							else{
								/*if(prefStatus == false || portfolioStatus == false){
									sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+","+false+");\"><img src='images/option02.png'></a>");
								}
								else if(jb.getJobCategoryMaster().getBaseStatus() && (!baseInvStatus)){
									sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('0')\"><img src='images/option02.png'></a>");
								}
								else{
									sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
								}*/
								sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblCompleteNow", locale)+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria('"+jb.getJobId()+"');\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='QQ' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+rowCount+"' href='javascript:void(0);' onclick=\"getQQDistrictQuestion("+jb.getJobId()+");\"></a>");
							}

							sb.append("</td>");	   		

						}
						else if(jb.getExitURL().equalsIgnoreCase("Available"))
						{
							sb.append("<td><strong class='text-info'>Available</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
							}
							else{
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
								{
									//System.out.println(" Non-Client Job :: "+jb.getJobId());
									sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' onclick=\"chkApplyJobNonClientDashJOI('"+jb.getJobId()+"','"+jb.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jb.getExitMessage()+"','"+jb.getDistrictMaster().getDistrictId()+"','')\" href='javascript:void(0);' ><img src='images/option01.png' style='width:21px; height:21px;' ></a>");
								}
								else
								{
									sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jb.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;' ></a>");
									//sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
								}
								//sb.append("<a data-original-title='"+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jb.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;' ></a>");
								sb.append("&nbsp;<a data-original-title='"+lblhide+"' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return getJbsIHide('"+jb.getJobId()+"')\"><img src='images/unhide.png' style='width:21px; height:21px;' ></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}

							sb.append("</td>");	   		
						}
						else if(jb.getExitURL().equalsIgnoreCase("Completed"))
						{
							sb.append("<td><strong class='text-success'>"+lblCompleted+"</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else{
								sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}

							sb.append("</td>");	
						}
						else if(jb.getExitURL().equalsIgnoreCase("Timed Out"))
						{
							sb.append("<td><strong class='text-error'>"+Utility.getLocaleValuePropByKey("optTOut", locale)+"</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else{
								sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}

							sb.append("</td>");	   	
						}
						else if(jb.getExitURL().equalsIgnoreCase("Withdrawn"))
						{
							sb.append("<td><strong class='text-error'>"+optWithdrawn+"</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
							}
							else{
								sb.append("<a data-original-title='"+toolRe_Apply+"' rel='tooltip' id='tpApply"+rowCount+"' href='javascript:void(0);' onclick=\"getStateBeforeWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option01.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}

							sb.append("</td>");	   		
						}
						else if(jb.getExitURL().equalsIgnoreCase("Hidden"))
						{
							sb.append("<td><strong class='text-error'>Hidden</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else{
								//sb.append("<a data-original-title='"+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jb.getJobId()+"' ><img src='images/option01.png'></a>");
								sb.append("<a data-original-title='Un-hide' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"unHideJob('"+jb.getIpAddress()+"');\"><img src='images/option03.png' style='width:21px; height:21px;'></a>");
								//sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							sb.append("</td>");	 
						}

						sb.append("</tr>");
					}
				}
				if(rowCount==0)
				{
					sb.append("<tr>");
					sb.append("<td colspan='6'>");
					sb.append(msgNorecordfound);
					sb.append("</td>");	   				
					sb.append("</tr>");
				}

				sb.append("</table>");
				sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			///////////////////////

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		PrintOnConsole.debugPrintJOI("599");
		return sb.toString();
	}


	public String getDistrictAddress(JobOrder jobOrderDetails)
	{
		////System.out.println(" >>>>>>>>>>>>>>>>>>>> Inside Get Dsitrict Address :: >>>>>>>>>>>>>>>>>>> ");
		String districtAddress="";
		String address="";
		String state="";
		String city = "";
		String zipcode="";

		//Get district address
		if(jobOrderDetails.getDistrictMaster()!=null)
		{
			if(jobOrderDetails.getDistrictMaster().getAddress()!=null && !jobOrderDetails.getDistrictMaster().getAddress().equals(""))
			{
				if(!jobOrderDetails.getDistrictMaster().getCityName().equals(""))
				{
					districtAddress = jobOrderDetails.getDistrictMaster().getAddress()+", ";
				}
				else
				{
					districtAddress = jobOrderDetails.getDistrictMaster().getAddress();
				}
			}
			if(!jobOrderDetails.getDistrictMaster().getCityName().equals(""))
			{
				if(jobOrderDetails.getDistrictMaster().getStateId()!=null && !jobOrderDetails.getDistrictMaster().getStateId().getStateName().equals(""))
				{
					city = jobOrderDetails.getDistrictMaster().getCityName()+", ";
				}else{
					city = jobOrderDetails.getDistrictMaster().getCityName();
				}
			}
			if(jobOrderDetails.getDistrictMaster().getStateId()!=null && !jobOrderDetails.getDistrictMaster().getStateId().getStateName().equals(""))
			{

				if(jobOrderDetails.getDistrictMaster().getZipCode()!=null && !jobOrderDetails.getDistrictMaster().getZipCode().equals(""))
				{
					state = jobOrderDetails.getDistrictMaster().getStateId().getStateName()+", ";
				}
				else
				{
					state = jobOrderDetails.getDistrictMaster().getStateId().getStateName();
				}	
			}
			if(jobOrderDetails.getDistrictMaster().getZipCode()!=null && !jobOrderDetails.getDistrictMaster().getZipCode().equals(""))
			{
				zipcode = jobOrderDetails.getDistrictMaster().getZipCode();
			}

			if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
				address = districtAddress+city+state+zipcode;
			}else{
				address="";
			}

		}

		return address;
	}


	public String getSchoolAddress(JobOrder jbOrder){
		String schoolAddress="";
		String sAddress="";
		String sstate="";
		String scity = "";
		String szipcode="";

		// Get School address
		if(jbOrder.getSchool()!=null)
		{
			if(jbOrder.getSchool().get(0).getAddress()!=null && !jbOrder.getSchool().get(0).getAddress().equals(""))
			{
				if(!jbOrder.getSchool().get(0).getCityName().equals(""))
				{
					schoolAddress = jbOrder.getSchool().get(0).getAddress()+", ";
				}
				else
				{
					schoolAddress = jbOrder.getSchool().get(0).getAddress();
				}
			}
			if(!jbOrder.getSchool().get(0).getCityName().equals(""))
			{
				if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
				{
					scity = jbOrder.getSchool().get(0).getCityName()+", ";
				}else{
					scity = jbOrder.getSchool().get(0).getCityName();
				}
			}
			if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
			{
				if(!jbOrder.getSchool().get(0).getZip().equals(""))
				{
					sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName()+", ";
				}
				else
				{
					sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName();
				}	
			}
			if(!jbOrder.getSchool().get(0).getZip().equals(""))
			{
				szipcode = jbOrder.getSchool().get(0).getZip().toString();
			}

			if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
				sAddress = schoolAddress+scity+sstate+szipcode;
			}else{
				sAddress="";
			}
		}

		return sAddress;
	}
	public StatusMaster findByTeacherIdJobStausForLoop(InternalTransferCandidates internalITRA,JobForTeacher jobForTeacher,Map<Integer, Integer> mapAssess,TeacherAssessmentStatus teacherAssessmentStatusJSI,Map<Integer, TeacherAssessmentStatus>mapJSI,DistrictMaster districtMasterInternal,TeacherPortfolioStatus tPortfolioStatus,Map<String, StatusMaster>mapStatus,TeacherAssessmentStatus teacherBaseAssessmentStatus) 
	{	
		TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
		JobOrder jobOrder =  jobForTeacher.getJobId();
		Integer jobAssessmentStatus = jobOrder.getJobAssessmentStatus();
		StatusMaster statusMaster =  mapStatus.get("icomp");
		try{
			if(mapAssess!=null){
				Integer assessmentId=mapAssess.get(jobForTeacher.getJobId().getJobId());
				teacherAssessmentStatusJSI = mapJSI.get(assessmentId);
			}
			int isAffilated=0;
			boolean epiInternal=false;
			int isPortfolio=0;
			if(jobOrder.getIsPortfolioNeeded()!=null){
				if(jobOrder.getIsPortfolioNeeded())
					isPortfolio=1;
			}
			int offerEPI=0;
			int offerJSI=0;
			int internalFlag=0;
			try{
				if(districtMasterInternal!=null)
					if(jobOrder.getDistrictMaster()!=null && internalITRA!=null && districtMasterInternal.getDistrictId().equals(jobOrder.getDistrictMaster().getDistrictId())){
						internalFlag=1;
						if(districtMasterInternal.getOfferPortfolioNeeded()){
							isPortfolio=1;
						}else{
							isPortfolio=0;
						}
						if(districtMasterInternal.getOfferEPI()){
							offerEPI=1;
						}else{
							offerEPI=0;
						}
						if(districtMasterInternal.getOfferJSI()){
							offerJSI=1;
						}else{
							offerJSI=0;
						}
					}
			}catch(Exception e){
				e.printStackTrace();
			}

			try{
				if(jobOrder.getJobCategoryMaster()!=null){
					if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()!=null){
						if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
							epiInternal=true;
						}
						if(jobForTeacher.getIsAffilated()!=null){
							isAffilated=jobForTeacher.getIsAffilated();
						}
						if(internalFlag==0 && isAffilated==1){
							if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
								offerEPI=1;
							}else{
								offerEPI=0;
							}
							if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0){
								offerJSI=1;
							}else{
								offerJSI=0;
							}
							if(jobOrder.getJobCategoryMaster().getOfferPortfolioNeeded()){
								isPortfolio=1;
							}else{
								isPortfolio=0;
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(jobForTeacher.getIsAffilated()!=null){
				if(epiInternal==false){
					isAffilated=jobForTeacher.getIsAffilated();
				}else{
					isAffilated=0;
				}
			}
			
			//JSI optional for internal principal and principal job category
			try{
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getDistrictMaster()!=null){
	                if(jobOrder.getDistrictMaster().getDistrictId()==4218990 && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Principal") && isAffilated==1){
	                    offerJSI=0;
	                }
	            }
			}catch(Exception e){
				e.printStackTrace();
			}
			try
			{
				if((jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster()!=null && jobOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId().equals(2))&& isAffilated==1  )
				{
					 offerJSI=0;
					 isPortfolio=0;
				}
				
			}catch (Exception e) {
				e.printStackTrace();
			}

			//System.out.println(jobOrder.getJobId()+" Flags :"+isAffilated+" "+isPortfolio+" "+internalFlag+" "+offerEPI+" "+offerJSI+" ");
			boolean statusIcomp=false;
			try{
				if(jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly() && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp")){
					statusIcomp=true;
	                }
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(((teacherDetail.getIsPortfolioNeeded()) && tPortfolioStatus==null && internalFlag==0 && isPortfolio==1) || statusIcomp)
			{
				statusMaster = mapStatus.get("icomp");
			}else{
				if(isPortfolio==0 || (isPortfolio==1 && tPortfolioStatus!=null  && ((teacherDetail.getIsPortfolioNeeded()&& tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()))))
				{
					statusMaster = mapStatus.get("comp");
					if(!jobOrder.getJobCategoryMaster().getBaseStatus()  || (internalFlag==1 && offerEPI==0 )||(internalFlag==0 && offerEPI==0 && isAffilated==1)){
						if(jobOrder.getIsJobAssessment() && jobAssessmentStatus==1 && ((offerJSI==1 && internalFlag==1 )||(internalFlag==0 && isAffilated==1 && offerJSI==1)|| (internalFlag==0 && isAffilated==0)))
						{
							if(teacherAssessmentStatusJSI!=null){
								statusMaster=teacherAssessmentStatusJSI.getStatusMaster();
							}else{
								statusMaster = mapStatus.get("icomp");
							}
						}else{
							statusMaster = mapStatus.get("comp");
						}
					}                                                                        
					else if(jobOrder.getIsJobAssessment() && jobAssessmentStatus==1 && ((offerJSI==1 && internalFlag==1 )|| (internalFlag==0 && isAffilated==1 && offerJSI==1)|| (internalFlag==0 && isAffilated==0)))
					{
						/*============== Gagan : Job specific Inventory (JSI) Status is checking here : ==============*/
						if(teacherBaseAssessmentStatus!=null){
							if(jobForTeacher.getIsAffilated()!=null && jobOrder.getJobCategoryMaster().getOfferJSI()==0 && jobForTeacher.getIsAffilated()==1)
							{
								statusMaster = mapStatus.get("comp");
							}else{
								if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
									if(teacherAssessmentStatusJSI!=null){
										statusMaster=teacherAssessmentStatusJSI.getStatusMaster();
									}else{
										statusMaster = mapStatus.get("icomp");
									}
								}else{
									statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
								}
							}
						}else{
							statusMaster = mapStatus.get("icomp");
						}
					}
					else{
						boolean  evDistrict=false;
						try{
							if(jobOrder.getDistrictMaster()!=null){
								if(jobOrder.getDistrictMaster().getDistrictId()==5513170){
									evDistrict=true;
								}
							}
						}catch(Exception e){}
						/*============== Gagan : Base Status is checking here : ==============*/
						if((isAffilated!=1 || evDistrict )&&((offerEPI==1 && internalFlag==1 )|| ( internalFlag==0 && isAffilated==0) || ( internalFlag==0 && isAffilated==1 && offerEPI==1))) //if block will execute only if Integer.parseInt(isAffilated) value is 0 or null
						{	
							if(teacherBaseAssessmentStatus!=null){
								statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
							}else{
								statusMaster = mapStatus.get("icomp");
							}
						}
						else // isAffilated else block
						{
							statusMaster = mapStatus.get("comp");
						}
					}
				}
				else
				{
					statusMaster = mapStatus.get("icomp");
				}
			}
			//System.out.println(""+statusMaster.getStatusShortName());
		}catch(Exception e){
			e.printStackTrace();
		}
		return statusMaster;

	}

	public boolean getInventoryStatus()
	{
		System.out.println("=====Inventory Status Call======");
		boolean invstatus=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
		TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
		if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
		{
			invstatus=true;
		}
		return invstatus;
	}
	
	
	//shadab
	public String getJobsOfInterestAjaxESTest(String searchBy,String noOfRows,String page,String sortOrderStr,String sortOrderType,String dt,String districtId,String schoolId,String stateId,String cityId,String subjectId,String sGeoZoneId,
			String searchTerm,String subjectName,String stateName)
	{
		if(searchTerm!=null)
			searchTerm=searchTerm.replaceAll("\"", "");
		System.out.println("Calling getJobsOfInterestAjaxESTest Test");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		boolean  isImCandidate=false;
		if(session != null && (session.getAttribute("isImCandidate")!=null))
		{
			isImCandidate = (Boolean)session.getAttribute("isImCandidate");
		}
		try 
		{		
			String teacherclassification=(String) session.getAttribute("classification");
			////////////////////////


			boolean statusAvlbl = false;
			boolean statusComp = false;
			boolean statusIcomp = false;
			boolean statusVlt =	false;
			boolean statusWidrwn = false;
			boolean statusHiden = false;
			boolean statusAll =	false;
			boolean isEpiStandalone = false; 

			Integer geoZoneId=0;
			String zoneId   =sGeoZoneId;  //request.getParameter("geoZoneId");

			System.out.println("zoneId "+zoneId +" sGeoZoneId "+sGeoZoneId);

			if(zoneId!=null && !zoneId.equalsIgnoreCase(""))
				geoZoneId =  Integer.parseInt(zoneId);

			try
			{
				TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
				//-- set start and end position
				if(session.getAttribute("epiStandalone")!=null)
					isEpiStandalone = (Boolean) session.getAttribute("epiStandalone");

				String noOfRow =noOfRows; //request.getParameter("noOfRows")==null?"1":request.getParameter("noOfRows");
				if(noOfRows==null)
					noOfRow="1";

				String pageNo =page;	//request.getParameter("page")==null?"1":request.getParameter("page");
				if(page==null)
					pageNo="1";


				int noOfRowInPage =	Integer.parseInt(noOfRow);
				int pgNo =	Integer.parseInt(pageNo);
				String sortOrder =sortOrderStr; //request.getParameter("sortOrderStr").trim();


				// For searching parameter
				String schoolId1 =schoolId; //request.getParameter("schoolId").trim();
				String cityName =cityId; //request.getParameter("cityId").trim();

				int start = ((pgNo-1)*noOfRowInPage);
				int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
				int totalRecord = 0;
				//------------------------------------


				/***** Sorting by Domain Name,Competency Name and Objective Name Stating ( Sekhar ) ******/
				SortedMap map = new TreeMap();
				/** set default sorting fieldName **/
				String sortOrderFieldName	=	"createdDateTime";
				String sortOrderNoField		=	"createdDateTime";
				//System.out.println("\n  Gagan : sort order sortOrderFieldName  "+sortOrderFieldName+" sortOrderNoField "+sortOrderNoField);
				/**Start set dynamic sorting fieldName **/
				Order  sortOrderStrVal		=	null;
				if(sortOrder!=null){
					if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("jobTitle") && !sortOrder.equals("districtName")&& !sortOrder.equals("schoolName") && !sortOrder.equals("location") && !sortOrder.equals("geoZoneName")){
						sortOrderFieldName=sortOrder;
						sortOrderNoField=sortOrder;
					}
					if(sortOrder.equals("jobTitle"))
					{
						sortOrderNoField="jobTitle";
					}
					if(sortOrder.equals("districtName"))
					{
						sortOrderNoField="districtName";
					}
					if(sortOrder.equals("schoolName"))
					{
						sortOrderNoField="schoolName";
					}
					if(sortOrder.equals("location"))
					{
						sortOrderNoField="location";
					}
					if(sortOrder.equals("geoZoneName"))
					{
						sortOrderNoField="geoZoneName";
					}
				}
				String sortOrderTypeVal="0";
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal=Order.asc(sortOrderFieldName);
					}else
					{
						sortOrderTypeVal="1";
						sortOrderStrVal=Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
				/**End ------------------------------------**/




				boolean prefStatus = false;
				boolean portfolioStatus = false;
				boolean baseInvStatus=false;
				TeacherPreference tPreference=null;
				TeacherPortfolioStatus tPortfolioStatus=null;

				boolean flagJobOrderList = false; 

				DistrictMaster districtMaster=null;
				SchoolMaster schoolMaster=null;

				ArrayList<Integer> subjectIdList=  new ArrayList<Integer>();
				List<SubjectMaster> ObjSubjectList = new ArrayList<SubjectMaster>();

				List<JobOrder> jobOrderAllList = new ArrayList<JobOrder>(); 
				List<JobOrder> districtMasterlst = new ArrayList<JobOrder>(); 
				List<JobOrder> schoolMasterlst = new ArrayList<JobOrder>(); 
				List<JobOrder> subjectList = new ArrayList<JobOrder>(); 
				List<JobOrder> zipCodeList2 = new ArrayList<JobOrder>();
				List<String> zipList	=	new ArrayList<String>();
				List<CityMaster> lstcityMasters = new ArrayList<CityMaster>();
				//shadab List<JobOrder> jobOrderAllListWithIsPool = new ArrayList<JobOrder>();
				List<JobOrder> jobByState	=	new ArrayList<JobOrder>();
				
				
				//shadab start new to get data with condition
				List<Integer> jobOrderAllListWithIsPoolES = new ArrayList<Integer>();
				List<Integer> isImCandidateListES = new ArrayList<Integer>();
				JSONObject jsonObject=new JSONObject();
				int from=0;
				int ddistrictId=0, sschoolId=0, ggeoZoneId=0;
				if(districtId!=null && !districtId.trim().equals(""))
					ddistrictId=Integer.parseInt(districtId);
				if(schoolId!=null && !schoolId.trim().equals(""))
					sschoolId=Integer.parseInt(schoolId);
				if(zoneId!=null && !zoneId.equalsIgnoreCase(""))
					ggeoZoneId =  Integer.parseInt(zoneId);
				
				
				if(!isImCandidate)
				{
					
				
					ElasticSearchService es=new ElasticSearchService();
					jsonObject=es.esForJobsOfInterest(ddistrictId, sschoolId, ggeoZoneId,from, noOfRow, pageNo, pgNo, sortOrderFieldName, sortOrderTypeVal, searchTerm,subjectName,subjectId,stateName,cityId,isImCandidate);
					//System.out.println("total esForJobsOfInterest from elastic when not candidate=="+((JSONObject)jsonObject.get("hits")).get("total"));
					
				     totalRecord=((JSONObject)jsonObject.get("hits")).getInt("total");
				    JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
				    System.out.println("hits size=="+hits.size());
				    
				    
				    
				    for(int i=0; i<hits.size(); i++)
						{
							jsonObject=(JSONObject)hits.get(i);
							String index=(String)jsonObject.get("_index");
							String type=(String)jsonObject.get("_type");
							String id=(String)jsonObject.get("_id");
							jsonObject=(JSONObject)jsonObject.get("_source");
							//JobOrder jo=new JobOrder();
							//jo.setJobId(jsonObject.getInt("jobId"));
							jobOrderAllListWithIsPoolES.add(jsonObject.getInt("jobId"));
						}
				    //System.out.println("--------------111----------"+jobOrderAllListWithIsPoolES.size());
				}
				if(isImCandidate)
				{
					ElasticSearchService es=new ElasticSearchService();
					DistrictMaster districtMaster1=new DistrictMaster();
					districtMaster1=(DistrictMaster) session.getAttribute("districtIdForImCandidate");
					if(ddistrictId==districtMaster1.getDistrictId() || ddistrictId==0)
					{
						//jsonObject=es.esForJobsOfInterestForCandidate(districtMaster1.getDistrictId(), sschoolId, ggeoZoneId,from, noOfRow, pageNo, pgNo, sortOrderFieldName, sortOrderTypeVal, searchTerm,subjectName,subjectId,stateName,cityId);
						jsonObject=es.esForJobsOfInterest(districtMaster1.getDistrictId(), sschoolId, ggeoZoneId,from, noOfRow, pageNo, pgNo, sortOrderFieldName, sortOrderTypeVal, searchTerm,subjectName,subjectId,stateName,cityId,isImCandidate);
						
						//System.out.println("total jobsofinterestforcandidate from elastic=="+((JSONObject)jsonObject.get("hits")).get("total"));
					     totalRecord=((JSONObject)jsonObject.get("hits")).getInt("total");
					    JSONArray hits= (JSONArray) ((JSONObject)jsonObject.get("hits")).get("hits");
					    
					    
					    
					    for(int i=0; i<hits.size(); i++)
							{
								jsonObject=(JSONObject)hits.get(i);
								String index=(String)jsonObject.get("_index");
								String type=(String)jsonObject.get("_type");
								String id=(String)jsonObject.get("_id");
								jsonObject=(JSONObject)jsonObject.get("_source");
								//JobOrder jo=new JobOrder();
								//jo.setJobId(jsonObject.getInt("jobId"));
								isImCandidateListES.add(jsonObject.getInt("jobId"));
							}
					}
					
				}
				//shadab end 
				
				

				try
				{
					//shadab if(!isImCandidate)
						//shadab  jobOrderAllListWithIsPool = jobOrderDAO.findAllJobWithPoolCondition();

				}catch(Exception e)
				{
					e.printStackTrace();
				}


				tPreference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
				if(tPreference!=null){
					prefStatus = true;
				}

				tPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);
				if(tPortfolioStatus!=null && tPortfolioStatus.getIsPersonalInfoCompleted() && tPortfolioStatus.getIsAcademicsCompleted() && tPortfolioStatus.getIsCertificationsCompleted() && tPortfolioStatus.getIsExperiencesCompleted() && tPortfolioStatus.getIsAffidavitCompleted()){
					portfolioStatus = true;
				}
				TeacherAssessmentStatus teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
				if((teacherBaseAssessmentStatus!=null) && (!teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") )){
					baseInvStatus=true;				 
				}
				List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
				HashSet<JobOrder> masterJOb = new HashSet<JobOrder>();
				HashSet<JobOrder> associateJobs = new HashSet<JobOrder>();
				Map<Integer, TeacherStatusHistoryForJob> applyJObStatus = new HashMap<Integer, TeacherStatusHistoryForJob>();
				//mukesh
				if(isImCandidate)
				{
					DistrictMaster districtMaster1=new DistrictMaster();
					districtMaster1=(DistrictMaster) session.getAttribute("districtIdForImCandidate");
					List<JobOrder> jobOrderAllList1 =jobOrderDAO.findActiveJobOrderbyDistrict(districtMaster1);
					
					lstJobOrder.addAll(jobOrderAllList1);
				}else
				{
					if(teacherDetail.getUserType().equalsIgnoreCase("N"))
					{
						lstJobOrder = getJobByPrefES(request,sortOrderStrVal);
					}
					else
					{
						List<JobForTeacher> lstJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);
						if(lstJFT.size()>0)
						{
							if(lstJFT.get(0)!=null && lstJFT.get(0).getJobId().getHeadQuarterMaster()!=null){
								lstJobOrder = jobOrderDAO.findSortedJobOrderbyHBD(lstJFT.get(0).getJobId().getHeadQuarterMaster(),sortOrderStrVal);
							}else{
								lstJobOrder = jobOrderDAO.findSortedJobOrderbyDistrict(lstJFT.get(0).getJobId().getDistrictMaster(),sortOrderStrVal);
							}
						}
					}
					//******************* Umrella/Associate/Certificates Logic on philli jobs**********************************
					
					List<JobOrder> temJobLIsts=new ArrayList<JobOrder>();
					//creating temporary list for deleting 
					temJobLIsts.addAll(lstJobOrder);
					for (JobOrder jobOrder : temJobLIsts) 
					{
						if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && jobOrder.getIsVacancyJob()){
							associateJobs.add(jobOrder);
							lstJobOrder.remove(jobOrder);
						}				
						if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && !jobOrder.getIsVacancyJob()){					
							masterJOb.add(jobOrder);
						}
					}			
					//System.out.println("111>>>>>>>11 masterJOb.size() :: "+masterJOb.size()+" associateJobs.size() :: "+associateJobs.size());
					//check for Screaning complete status
					List<TeacherStatusHistoryForJob>  historyforScreaningComlete= new ArrayList<TeacherStatusHistoryForJob>();
					//Map<jobId, TeacherStatusHistoryForJob> 
					
					if(masterJOb!=null && masterJOb.size()>0){
						StatusMaster statusMaster=statusMasterDAO.findById(16,false,false);
						historyforScreaningComlete = teacherStatusHistoryForJobDAO.getScreningCompleteJobsByJobsAndTeacher(teacherDetail, masterJOb, statusMaster);			
						for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyforScreaningComlete) {
							applyJObStatus.put(teacherStatusHistoryForJob.getJobOrder().getJobId(), teacherStatusHistoryForJob);
						}
					}
				}

				CandidateGridService gridService=new CandidateGridService();
				List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
				String[] statuss = {"icomp","vlt","widrw","hide"};
				try{
					lstStatusMasters = statusMasterDAO.findStatusByShortNames(statuss);
				}catch (Exception e) {
					e.printStackTrace();
				}
				List<JobForTeacher> lstAllJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);

				///////////////////////////////////////////////////////////////////


				List<JobOrder> jobLst= new ArrayList<JobOrder>();
				for (JobForTeacher jobForTeacher : lstAllJFT) {
					jobLst.add(jobForTeacher.getJobId());
				}
				List <StatusMaster> statusMasterList=statusMasterDAO.findAllStatusMaster();
				Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
				for (StatusMaster statusMaster : statusMasterList) {
					mapStatus.put(statusMaster.getStatusShortName(),statusMaster);
				}

				TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
				Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
				Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
				List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
				List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
				List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
				if(assessmentJobRelations1.size()>0){
					for(AssessmentJobRelation ajr: assessmentJobRelations1){
						mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
						adList.add(ajr.getAssessmentId());
					}
					if(adList.size()>0)
						lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentList(teacherDetail,adList);
				}
				for(TeacherAssessmentStatus ts : lstJSI){
					mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
				}

				DistrictMaster districtMasterInternal=null;
				List<InternalTransferCandidates> lstITRA=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
				InternalTransferCandidates internalITRA = null;
				if(lstITRA.size()>0){
					internalITRA=lstITRA.get(0);
					districtMasterInternal=internalITRA.getDistrictMaster();
				}
				//////////////////////////////////
				List<JobForTeacher> lstincompletJFT =new ArrayList<JobForTeacher>(); // jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusInComplete);			
				List<JobForTeacher> lstviolatedJFT = new ArrayList<JobForTeacher>();//jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusViolated);
				List<JobForTeacher> lstHideJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusHide);
				List<JobForTeacher> lstWidrwJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatus(teacherDetail,statusWidrw);

				List<JobForTeacher> lstcompletJFT =new ArrayList<JobForTeacher>(); //jobForTeacherDAO.findJobByTeacherAndSatusList(teacherDetail,statusMasters);

				for(JobForTeacher jobForTeacher: lstAllJFT)
				{				
					JobOrder jobOrder = jobForTeacher.getJobId();				
					if(!isImCandidate){
						if(masterJOb.contains(jobOrder) && applyJObStatus.containsKey(jobOrder.getJobId()))
						{
							for (JobOrder jobOrderAssoc : associateJobs) {
								if(jobOrderAssoc.getJobCategoryMaster().equals(jobOrder.getJobCategoryMaster())){
									lstJobOrder.add(jobOrderAssoc);
								}
							}
							
						}
					}
					
					if(lstJobOrder.contains(jobOrder))
					{
						lstJobOrder.remove(jobOrder);					
					}

					StatusMaster statusMaster = mapStatus.get("icomp");
					statusMaster=findByTeacherIdJobStausForLoop(internalITRA,jobForTeacher,mapAssess,teacherAssessmentStatusJSI,mapJSI,districtMasterInternal,tPortfolioStatus,mapStatus,teacherBaseAssessmentStatus);
					//System.out.println(":::::::::::::::::::::::::::::::::::::::::::::"+jobOrder.getJobId()+"  "+statusMaster.getStatusShortName());

					if(statusMaster.getStatusShortName().equalsIgnoreCase("comp") && (jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")|| jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("rem") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("scomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("ecomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vcomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("dcln"))){
						lstcompletJFT.add(jobForTeacher);
					}else if(!statusMaster.getStatusShortName().equalsIgnoreCase("vlt") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") && !jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide") && (statusMaster.getStatusShortName().equalsIgnoreCase("icomp") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp")|| jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("rem") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("scomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("ecomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vcomp")||jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("dcln"))){
						lstincompletJFT.add(jobForTeacher);
					}else if(statusMaster.getStatusShortName().equalsIgnoreCase("vlt") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt")){
						lstviolatedJFT.add(jobForTeacher);
					}else if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw")){
						lstWidrwJFT.add(jobForTeacher);
					}else if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide")){
						lstHideJFT.add(jobForTeacher);
					}
					/////////////////////////////////////////////////////////////////
				}

				List<JobForTeacher> finalListForJobsOfInterest	=	new ArrayList<JobForTeacher>();
				List<JobOrder> finalListForJobsOfInterestJobOrders	=	new ArrayList<JobOrder>();


				// Start Searching	
				if(searchBy.equalsIgnoreCase("avlbl"))
				{
					statusAvlbl=true;
					finalListForJobsOfInterestJobOrders.addAll(lstJobOrder);
					flagJobOrderList=true;
				}
				else if(searchBy.equalsIgnoreCase("comp"))
				{
					statusComp=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstcompletJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else if(searchBy.equalsIgnoreCase("icomp"))
				{
					statusIcomp=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstincompletJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else if(searchBy.equalsIgnoreCase("vlt"))
				{
					statusVlt=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstviolatedJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else if(searchBy.equalsIgnoreCase("widrw"))
				{
					statusWidrwn=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstWidrwJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else if(searchBy.equalsIgnoreCase("hide"))
				{
					statusHiden=true;
					flagJobOrderList=true;
					for (JobForTeacher jobForTeacher : lstHideJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}
				else
				{
					statusAll = true;
					flagJobOrderList=true;
					finalListForJobsOfInterestJobOrders.clear();

					for (JobForTeacher jobForTeacher : lstincompletJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Incomplete");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
					for (JobOrder job : lstJobOrder) 
					{
						JobOrder jo=job;
						jo.setExitMessage(job.getExitURL());
						jo.setExitURL("Available");						
						finalListForJobsOfInterestJobOrders.add(jo);
					}

					for (JobForTeacher jobForTeacher : lstcompletJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Completed");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
					for (JobForTeacher jobForTeacher : lstviolatedJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Timed Out");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
					for (JobForTeacher jobForTeacher : lstWidrwJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Withdrawn");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
					for (JobForTeacher jobForTeacher : lstHideJFT) 
					{
						JobOrder jo=jobForTeacher.getJobId();
						jo.setIpAddress(jobForTeacher.getJobForTeacherId()+"");
						jo.setExitURL("Hidden");
						finalListForJobsOfInterestJobOrders.add(jo);
					}
				}

				/* @Start
				 * @Ashish Kumar
				 * @Description :: Searching Data
				 * */

				if(!cityName.equals("0")){
					zipList = cityMasterDAO.findCityByName(cityName);
				}else{
					/*if(stateId!=null && !stateId.equals("")){
						StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);

						// city List By State Id
						lstcityMasters	=	cityMasterDAO.findCityByState(stateMaster);

						for(CityMaster c:lstcityMasters)
						{
							//System.out.println(" city name :: "+c.getCityName()+" zip code :: "+c.getZipCode());
							zipList.add(c.getZipCode());
						}
					}*/
				}

				/*for(String g:zipList)
				{
					System.out.println(" "+g);
				}*/



				if(finalListForJobsOfInterestJobOrders.size()>0){
					flagJobOrderList=true;
					jobOrderAllList.addAll(finalListForJobsOfInterestJobOrders);
				}

				
				//shadab  below all search criteria
			/*	// For District Search
				if(!districtId.equals("") && !districtId.equals(""))
				{
					districtMaster  = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
					districtMasterlst = jobOrderDAO.findJobOrderByDistrict(districtMaster);

					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(districtMasterlst);
					}else{
						jobOrderAllList.addAll(districtMasterlst);
					}

				}

				// For School Search
				if(!schoolId1.equals("0") && !schoolId1.equals(""))
				{
					schoolMaster 	=	schoolMasterDAO.findById(new Long(schoolId1), false, false);
					schoolMasterlst.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList));

					if(flagJobOrderList==true)
						jobOrderAllList.retainAll(schoolMasterlst);
					else
						jobOrderAllList.addAll(schoolMasterlst);

				}


				// For Subject
				if(subjectId !=null && !subjectId.equals("0,") && !subjectId.equals(""))
				{
					String subIdList[] = subjectId.split(",");
					if(Integer.parseInt(subIdList[0])!=0)
					{
						for(int i=0;i<subIdList.length;i++)
						{
							subjectIdList.add(Integer.parseInt(subIdList[i]));
						}
						ObjSubjectList = subjectMasterDAO.getSubjectMasterlist(subjectIdList);

						subjectList = jobOrderDAO.findJobOrderBySubjectList(ObjSubjectList);

						if(flagJobOrderList==true){
							jobOrderAllList.retainAll(subjectList);
						}else{
							jobOrderAllList.addAll(subjectList);
						}
					}
				}

				// For Cities (ZipCode List)
				if(zipList!=null && zipList.size()>0)
				{
					List<DistrictMaster> lstDistrictMasters = new ArrayList<DistrictMaster>();
					List<SchoolMaster> lstSchoolMasters = null;

					lstSchoolMasters = schoolMasterDAO.findByZipCodeList(zipList);

					if(lstSchoolMasters!=null)
					{
						for(SchoolMaster s:lstSchoolMasters)
						{
							lstDistrictMasters.add(s.getDistrictId());
						}
					}

					if(lstDistrictMasters!=null && lstDistrictMasters.size()>0)
					{
						zipCodeList2 = jobOrderDAO.findJobOrderByDistrictList(lstDistrictMasters);
					}

					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(zipCodeList2);
					}else{
						jobOrderAllList.addAll(zipCodeList2);
					}

				}

				try{
					if(!stateId.equals("0") && !stateId.equals(""))
					{
						List<DistrictMaster> districtByState = new ArrayList<DistrictMaster>();
						StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
						districtByState = districtMasterDAO.getDistrictListByState(stateMaster);

						if(districtByState!=null && districtByState.size()>0)
						{
							jobByState = jobOrderDAO.findJobOrderByDistrictList(districtByState);
						}	

						if(flagJobOrderList==true){
							jobOrderAllList.retainAll(jobByState);
						}else{
							jobOrderAllList.addAll(jobByState);
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}*/




				/* @End
				 * @Ashish Kumar
				 * @Description :: Searching Data
				 * */
				// filtering by mukesh
				//shadab  below all search criteria
				
				/*List<JobOrder> listjobOrdersgeoZone = new ArrayList<JobOrder>();
				if(geoZoneId>0)
				{
					GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
					listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);

				}

				if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
				{
					if(flagJobOrderList==true){
						jobOrderAllList.retainAll(listjobOrdersgeoZone);
					}else{
						jobOrderAllList.addAll(listjobOrdersgeoZone);
					}

				}				

				if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
				{
					jobOrderAllList.addAll(listjobOrdersgeoZone);

				}
				else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
				{
					jobOrderAllList.retainAll(listjobOrdersgeoZone);
				}*/
				/*============For Paging and  Sorting ===============*/
				List<JobOrder> sortedlstJobOrder		=	new ArrayList<JobOrder>();

				SortedMap<String,JobOrder>	sortedMap 	= 	new TreeMap<String,JobOrder>();
				if(sortOrderNoField.equals("jobTitle"))
				{
					sortOrderFieldName	=	"jobTitle";
				}
				if(sortOrderNoField.equals("jobEndDate"))
				{
					sortOrderFieldName	=	"jobEndDate";
				}
				if(sortOrderNoField.equals("districtName"))
				{
					sortOrderFieldName	=	"districtName";
				}
				if(sortOrderNoField.equals("schoolName"))
				{
					sortOrderFieldName	=	"schoolName";
				}
				if(sortOrderNoField.equals("location"))
				{
					sortOrderFieldName	=	"location";
				}
				if(sortOrderNoField.equals("geoZoneName"))
				{
					sortOrderFieldName	=	"geoZoneName";
				}
				
				
				//shadab if(jobOrderAllList!=null && jobOrderAllList.size()>0 && jobOrderAllListWithIsPool!=null && jobOrderAllListWithIsPool.size()>0)
				{
					//shadab commented below line
					//jobOrderAllList.retainAll(jobOrderAllListWithIsPool);
				}
				
				//shadab start new to get data with condition
				System.out.println("jobOrderAllList size=="+jobOrderAllList.size());
				System.out.println("jobOrderAllListWithIsPoolES size=="+jobOrderAllListWithIsPoolES.size());
				
				List<JobOrder> jobOrderAllListTemp=new ArrayList<JobOrder>();
				if(!isImCandidate && jobOrderAllList!=null && jobOrderAllList.size()>0 && jobOrderAllListWithIsPoolES!=null)
				{
					//jobOrderAllList.retainAll(jobOrderAllListWithIsPoolES);
					for(JobOrder jo:jobOrderAllList)
					//for(int i=0;i< jobOrderAllList.size()-1;i++)
					{
						
						if(jobOrderAllListWithIsPoolES.contains(jo.getJobId()))
						{
							//jobOrderAllList.remove(jobOrderAllList.get(i));
							jobOrderAllListTemp.add(jo);
							
						}
					}
					jobOrderAllList=jobOrderAllListTemp;
				}
				//System.out.println();
				//System.out.println("jobOrderAllList size=="+jobOrderAllList.size());
				//System.out.println("jobOrderAllListWithIsPoolES size=="+jobOrderAllListWithIsPoolES.size());
				//System.out.println(jobOrderAllListWithIsPoolES);
				
				//new
				if(isImCandidate && jobOrderAllList!=null && jobOrderAllList.size()>0)
				{
					//jobOrderAllList.retainAll(jobOrderAllListWithIsPoolES);
					for(JobOrder jo:jobOrderAllList)
					//for(int i=0;i< jobOrderAllList.size()-1;i++)
					{
						
						if(isImCandidateListES.contains(jo.getJobId()))
						{
							//jobOrderAllList.remove(jobOrderAllList.get(i));
							jobOrderAllListTemp.add(jo);
							
						}
					}
					jobOrderAllList=jobOrderAllListTemp;
				}
				//new end
				
				
				
				//shadab end new to get data with condition
				
				
				
				//******************** here will be Job Certification logic for Philly ***************************

				List<CertificateTypeMaster> certObjList = new ArrayList<CertificateTypeMaster>();
				String crtIdList="";
				List<String> certList = new ArrayList<String>();
				
				List<TeacherCertificate> lstTeacherCertificates=teacherCertificateDAO.findCertificateAscByTeacher(teacherDetail);//teacherCertificateDAO.findCertificateByTeacher(teacherDetail);
				if(lstTeacherCertificates!=null){
					for(TeacherCertificate tc : lstTeacherCertificates){
						if(tc.getCertificateTypeMaster()!=null)
						{
							if(crtIdList.equalsIgnoreCase(""))
								crtIdList+=tc.getCertificateTypeMaster().getCertTypeId();
				             else
								crtIdList+=","+tc.getCertificateTypeMaster().getCertTypeId();
							certList.add(tc.getCertificateTypeMaster().getCertTypeId()+"");
							certList.add(crtIdList);
							certObjList.add(tc.getCertificateTypeMaster());
						}
					}
				}
				
				List<JobOrder> listOfJobId = new ArrayList<JobOrder>();
				//listOfJobId.addAll(jobOrderAllList1);
				for (JobOrder jobOrder : jobOrderAllList){
					if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==4218990){
						listOfJobId.add(jobOrder);					
					}
				}
				
				Map<JobOrder, String> jobCertMap = new HashMap<JobOrder, String>();
				if(listOfJobId!=null && listOfJobId.size()>0){
				List<JobCertification> findByJobIDS = jobCertificationDAO.findCertificationsByJobOrders(listOfJobId,certObjList);
					for (JobCertification jobCertification : findByJobIDS) {
						String certId = jobCertification.getCertificateTypeMaster().getCertTypeId()+"";
						jobOrderAllList.remove(jobCertification.getJobId());                                                                                      
						if(jobCertMap.containsKey(jobCertification.getJobId())){
							String existingCertId = jobCertMap.get(jobCertification.getJobId());
							jobCertMap.put(jobCertification.getJobId(), existingCertId+","+certId);
							System.out.println(jobCertification.getJobId().getJobId()+"  "+existingCertId+","+certId);
						}else{
							jobCertMap.put(jobCertification.getJobId(), certId);
							System.out.println(jobCertification.getJobId().getJobId()+"  "+certId);
						}
					}
				}
				List<JobOrder> addMatchJobs=new ArrayList<JobOrder>(); 
				for (String c1 : certList) {
					 for(java.util.Map.Entry<JobOrder, String> entry : jobCertMap.entrySet()){
						 /*if(entry.getKey().getClassification()!=null){
							 if(c1.equalsIgnoreCase(entry.getValue()) && entry.getKey().getClassification().equalsIgnoreCase(teacherclassification)){                                                                                                                                                                                                                                                                                         
									addMatchJobs.add(entry.getKey());
									jobOrderAllList.add(entry.getKey());
							 }
						 }else{
							 if(c1.equalsIgnoreCase(entry.getValue())){                                                                                                                                                                                                                                                                                         
									addMatchJobs.add(entry.getKey());
									jobOrderAllList.add(entry.getKey());
							 }
						 }*/
						 if(c1.equalsIgnoreCase(entry.getValue())){                                                                                                                                                                                                                                                                                         
								addMatchJobs.add(entry.getKey());
								jobOrderAllList.add(entry.getKey());
						 }
					  }
				 }
		   System.out.println("addMatchJobs.size() :: "+addMatchJobs.size()+"jobOrderAllList :: "+jobOrderAllList.size());
		
		//####################################################################

				int mapFlag=2;
				for (JobOrder jobOrder : jobOrderAllList){
					String orderFieldName=jobOrder.getStatus();
					if(sortOrderFieldName.equals("jobTitle")){
						orderFieldName=jobOrder.getJobTitle().toUpperCase()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					if(sortOrderFieldName.equals("jobEndDate")){
						orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate())+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}

					if(jobOrder.getGeoZoneMaster()!=null){
						orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName();
						if(sortOrderFieldName.equals("geoZoneName")){
							orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName()+"||"+jobOrder.getJobId();
							sortedMap.put(orderFieldName+"||",jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
					}else{
						if(sortOrderFieldName.equals("geoZoneName")){
							orderFieldName="0||"+jobOrder.getJobId();
							sortedMap.put(orderFieldName+"||",jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
					}

					if(sortOrderFieldName.equals("districtName")){
						if(jobOrder.getHeadQuarterMaster()!=null){
							orderFieldName=jobOrder.getHeadQuarterMaster().getHeadQuarterName()+"||"+jobOrder.getJobId();
						}else{
							orderFieldName=jobOrder.getDistrictMaster().getDistrictName()+"||"+jobOrder.getJobId();
						}
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}

					if(sortOrderFieldName.equals("schoolName")){
						if(jobOrder.getCreatedForEntity().equals(3)){
							orderFieldName=jobOrder.getSchool().get(0).getSchoolName()+"||"+jobOrder.getJobId();
							sortedMap.put(orderFieldName+"||",jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
						else{
							if(jobOrder.getSchool().size()==1){
								orderFieldName=jobOrder.getSchool().get(0).getSchoolName()+"||"+jobOrder.getJobId();
							}else if(jobOrder.getSchool().size()>1){
								orderFieldName="Many Schools||"+jobOrder.getJobId();
							}else{
								orderFieldName=""+"0000||"+jobOrder.getJobId();
							}
							sortedMap.put(orderFieldName+"||",jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
					}
					if(sortOrderFieldName.equals("location")){

						if(jobOrder.getCreatedForEntity()!=3)
						{
							if(jobOrder.getHeadQuarterMaster()==null && jobOrder.getDistrictMaster()!=null && !jobOrder.getDistrictMaster().getAddress().isEmpty())
								orderFieldName=jobOrder.getDistrictMaster().getAddress()+"||"+jobOrder.getJobId();
							else
								orderFieldName="0000"+"||"+jobOrder.getJobId();
						}
						else
						{
							if(!jobOrder.getSchool().get(0).getAddress().isEmpty())
								orderFieldName=jobOrder.getSchool().get(0).getAddress()+"||"+jobOrder.getJobId();
							else
								orderFieldName="0000"+"||"+jobOrder.getJobId();
						}
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}

					}
				}
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();

					while (iterator.hasNext()) {
						Object key = iterator.next();
						sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
					}
				}else{

					sortedlstJobOrder=jobOrderAllList;
				}

				totalRecord =sortedlstJobOrder.size();

				if(totalRecord<end)
					end=totalRecord;
				List<JobOrder> lstsortedJobOrder =	sortedlstJobOrder.subList(start,end);


				String responseText="";

				sb.append("<table border='0' id='tblGridJoF'>");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				responseText=PaginationAndSorting.responseSortingLink("Job Title",sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				sb.append("<th width='20%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink("Expiry Date",sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
				sb.append("<th width='7%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink("District Name",sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
				sb.append("<th width='18%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink("Zone",sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
				sb.append("<th width='10%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink("School",sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
				sb.append("<th width='18%' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink("Address",sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
				sb.append("<th width='15%' valign='top'>"+responseText+"</th>");

				sb.append("<th width='5%' valign='top'>Job Status</th>");
				sb.append("<th width='10%' valign='top'>Actions</th>");
				sb.append("</tr>");
				sb.append("</thead>");

				int rowCount=0;
				int tempCount = 0;
				boolean dispName=false;
				String zone = "";
				if(statusIcomp)//for status Incomplete
				{
					for(JobOrder jb : lstsortedJobOrder)
					{						
						dispName=false;
						rowCount++;
						sb.append("<tr>");
						sb.append("<td><a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
						
						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");
						if(jb.getCreatedForEntity()==3)
						{
							tempCount=0;
							for(SchoolMaster school:jb.getSchool())
							{								
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{				
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append(school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");		
								}
								break;
							}	
						}
						else
						{

							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}

							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{	
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+")\";>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}

							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}

							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}

							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}

							}else{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}
						}


						sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
						sb.append("<td>");


						//sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							/*if(prefStatus == false || portfolioStatus == false){
								sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+","+false+");\"><img src='images/option02.png'></a>");
							}
							else if(jb.getJobCategoryMaster().getBaseStatus() && (!baseInvStatus)){
								sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('0')\"><img src='images/option02.png'></a>");
							}
							else{
								sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
							}*/
							sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblCompleteNow", locale)+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria("+jb.getJobId()+");\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}



						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}


				if(statusAvlbl)//for status available jobs
				{
					//totaRecord=totaRecord+lstJobOrder.size();
					for(JobOrder jbOrder: lstsortedJobOrder)
					{			
						rowCount++;
						sb.append("<tr>");
						sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
						

						//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate())+",11:59 PM CST</td>");

						if(jbOrder.getCreatedForEntity()==3)//School
						{
							for(SchoolMaster school:jbOrder.getSchool())
							{
								if(jbOrder.getDistrictMaster().getDisplayName()!=null && (!jbOrder.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jbOrder.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jbOrder.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jbOrder.getGeoZoneMaster()!=null && !jbOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
								{
									if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
								{
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append(school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jbOrder));
									sb.append("</td>");		
								}
								break;
							}

						}
						else
						{
							sb.append("<td>");
							sb.append(""+jbOrder.getDistrictMaster().getDistrictName());
							sb.append("</td>");
							if(jbOrder.getGeoZoneMaster()!=null && !jbOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}						
							if(jbOrder.getSchool().size()==1)
							{
								if(jbOrder.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
								}else{
									if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
									}
									else
									{
										sb.append("<td>"+jbOrder.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jbOrder));
										sb.append("</td>");
									}
								}
							}
							else if(jbOrder.getSchool().size()>1)
							{
								String schoolListIds = jbOrder.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jbOrder.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jbOrder.getSchool().get(i).getSchoolId().toString();
								}
								if(jbOrder.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
								}else{
									if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jbOrder));
								sb.append("</td>");
							}

						}

						sb.append("<td><strong class='text-info'>Available</strong></td>");
						sb.append("<td>");

						if(isEpiStandalone){
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+lblhide+"' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return getJbsIHide('"+jbOrder.getJobId()+"')\"><img src='images/unhide.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}


						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}

				if(statusComp)//for status complete
				{
					//totaRecord=totaRecord+lstcompletJFT.size();
					for(JobOrder jb : lstsortedJobOrder)
					{

						rowCount++;
						sb.append("<tr>");
						sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
						//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							tempCount=0;
							for(SchoolMaster school:jb.getSchool())
							{

								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{				

									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//sb.append("&nbsp;"+school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								if(tempCount==0)
								{
									tempCount++;
									break;
								}
							}
							if(tempCount==0)
							{
								sb.append("<td>&nbsp;&nbsp;</td>");
								sb.append("<td>&nbsp;&nbsp;</td>");
								sb.append("<td>&nbsp;&nbsp;</td>");

							}

						}
						else
						{
							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");

									}
								}
							}
							else
							{
								//System.out.println("$$$$$$$$$$$$$$$$$$$$$$$$$44");    
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}

						}

						sb.append("<td><strong class='text-success'>"+lblCompleted+"</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							
							if(mapAssess!=null){
								Integer assessmentId=mapAssess.get(jb.getJobId());								
								if(assessmentId!=null)
								{
									teacherAssessmentStatusJSI = mapJSI.get(assessmentId);
									if(teacherAssessmentStatusJSI.getStatusMaster().getStatusId()==4)
										sb.append("&nbsp;&nbsp;<a data-original-title='JSI' class='fa fa-book fa-2x thumbUpIconBlueColor' rel='tooltip' id='JSI"+(rowCount)+"' href='javascript:void(0);' onclick=\"return checkJSI('"+jb.getJobId()+"')\"></a>");
								}
							}
						}						
						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}

				if(statusVlt)//for violated
				{
					//totaRecord=totaRecord+lstviolatedJFT.size();
					//for(JobForTeacher jbTeacher : lstviolatedJFT)
					for(JobOrder jb : lstsortedJobOrder)
					{
						rowCount++;
						sb.append("<tr>");
						sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");

						//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							for(SchoolMaster school:jb.getSchool())
							{
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{				
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");


									sb.append("<td>");
									//	sb.append("&nbsp;"+school.getAddress());

									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								break;
							}
						}
						else
						{
							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{	
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");
										sb.append("<td>"+getDistrictAddress(jb)+"</td>"); 
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}

						}

						sb.append("<td><strong class='text-error'>"+Utility.getLocaleValuePropByKey("optTOut", locale)+"</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' ></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}

						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}

				/////////////////// Withdrawn ///////////////////////////
				if(statusWidrwn)//for Withdrawn
				{
					//totaRecord=totaRecord+lstWidrwJFT.size();
					//for(JobForTeacher jbTeacher : lstWidrwJFT)
					for(JobOrder jb : lstsortedJobOrder)
					{
						rowCount++;
						sb.append("<tr>");
						sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");

						//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							for(SchoolMaster school:jb.getSchool())
							{
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{	
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append("&nbsp;"+school.getAddress());

									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								break;
							}
						}
						else
						{

							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}

							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td<a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}
						}

						sb.append("<td><strong class='text-error'>"+optWithdrawn+"</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){						
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='"+toolRe_Apply+"' rel='tooltip' id='tpApply"+rowCount+"' href='javascript:void(0);' onclick=\"getStateBeforeWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option01.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}

						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}
				/////////////////// widraw end ///////////////////////////
				if(statusHiden)//for Hidden
				{
					//totaRecord=totaRecord+lstHideJFT.size();
					for(JobOrder jb : lstsortedJobOrder)
					{
						rowCount++;
						sb.append("<tr>");
						sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");

						//		sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							for(SchoolMaster school:jb.getSchool())
							{

								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{		
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}

								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");	
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append("&nbsp;"+school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								break;
							}
						}
						else
						{

							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}
							else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}	
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}
						}

						sb.append("<td><strong class='text-error'>Hidden</strong></td>");
						sb.append("<td>");
						if(isEpiStandalone){						
							sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							sb.append("<a data-original-title='Un-hide' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"unHideJob('"+jb.getIpAddress()+"');\"><img src='images/option03.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}

						sb.append("</td>");	   				
						sb.append("</tr>");
					}
				}
				/////////////////// Hidden end ///////////////////////////
				if(statusAll)//for statusAll
				{
					//totaRecord=totaRecord+lstHideJFT.size();
					//for(JobForTeacher jbTeacher : finalListForJobsOfInterest)
					for(JobOrder jb : lstsortedJobOrder)
					{
						rowCount++;
						sb.append("<tr>");
						if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getExitURL().equalsIgnoreCase("Available") && jb.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
						{
							sb.append("<td><a data-original-title='"+lblApplyNow+"' rel='tooltip' id='apply' onclick=\"chkApplyJobNonClientDashJOI('"+jb.getJobId()+"','"+jb.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jb.getExitMessage()+"','"+jb.getDistrictMaster().getDistrictId()+"','');\" href='javascript:void(0);' >"+jb.getJobTitle()+"</a></td>");						
						}						
						else
						{
							sb.append("<td> <a href='applyjob.do?jobId="+jb.getJobId()+"'>"+jb.getJobTitle()+"</a></td>");
						}

						//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>Until filled</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jb.getJobEndDate())+",11:59 PM CST</td>");

						if(jb.getCreatedForEntity()==3)
						{
							for(SchoolMaster school:jb.getSchool())
							{
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDisplayName());
									sb.append("</td>");
								}else if(jb.getHeadQuarterMaster()!=null){
									sb.append("<td>");
									sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
									sb.append("</td>");
								}
								else
								{
									sb.append("<td>");
									sb.append(""+jb.getDistrictMaster().getDistrictName());
									sb.append("</td>");
								}
								if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
								{	
									if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
									{
										sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
									else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
										//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
										sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}else{
										int jobDistrictId=0;
										if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
											jobDistrictId=jb.getDistrictMaster().getDistrictId();
										}
										sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									}
								}
								else
								{
									sb.append("<td>");							
									sb.append("</td>");
								}
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
								{
									sb.append("<td>&nbsp;</td>");								
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}
								else
								{						
									sb.append("<td>");
									sb.append("&nbsp;"+school.getSchoolName());
									sb.append("</td>");

									sb.append("<td>");
									//		sb.append(school.getAddress());
									sb.append("&nbsp;"+getSchoolAddress(jb));
									sb.append("</td>");
								}
								break;
							}
						}
						else
						{
							if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDisplayName()!=null && (!jb.getDistrictMaster().getDisplayName().trim().equals("")))
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDisplayName());
								sb.append("</td>");
							}else if(jb.getHeadQuarterMaster()!=null){
								sb.append("<td>");
								sb.append(""+jb.getHeadQuarterMaster().getHeadQuarterName());
								sb.append("</td>");
							}else
							{
								sb.append("<td>");
								sb.append(""+jb.getDistrictMaster().getDistrictName());
								sb.append("</td>");
							}
							if(jb.getGeoZoneMaster()!=null && !jb.getGeoZoneMaster().getGeoZoneName().equals(""))
							{		
								if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jb.getGeoZoneMaster().getDistrictMaster()!=null && jb.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									int jobDistrictId=0;
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null){
										jobDistrictId=jb.getDistrictMaster().getDistrictId();
									}
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jb.getGeoZoneMaster().getGeoZoneId()+",'"+jb.getGeoZoneMaster().getGeoZoneName()+"',"+jobDistrictId+");\">"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							if(jb.getSchool().size()==1)
							{
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");											
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td>"+jb.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jb));
										sb.append("</td>");
									}	
								}
							}
							else if(jb.getSchool().size()>1)
							{
								String schoolListIds = jb.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jb.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jb.getSchool().get(i).getSchoolId().toString();
								}
								if(jb.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jb)+"</td>");
								}else{	
									if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getDistrictId()==1200390 || jb.getDistrictMaster().getDistrictId()==614730)
									{
										sb.append("<td>&nbsp;</td>");											
										sb.append("<td>"+getDistrictAddress(jb)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}	
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");							
								sb.append(""+getDistrictAddress(jb));
								sb.append("</td>");
							}
						}					
						if(jb.getExitURL().equalsIgnoreCase("Incomplete"))
						{
							sb.append("<td><strong class='text-error'>"+optIncomlete+"</strong></td>");
							sb.append("<td>");



							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
							}
							else{
								/*if(prefStatus == false || portfolioStatus == false){
									sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"chkJobSpecificInventory("+prefStatus+","+portfolioStatus+","+false+");\"><img src='images/option02.png'></a>");
								}
								else if(jb.getJobCategoryMaster().getBaseStatus() && (!baseInvStatus)){
									sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('0')\"><img src='images/option02.png'></a>");
								}
								else{
									sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"return checkInventory('"+jb.getJobId()+"')\"><img src='images/option02.png'></a>");
								}*/
								sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblCompleteNow", locale)+"' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria('"+jb.getJobId()+"');\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='QQ' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+rowCount+"' href='javascript:void(0);' onclick=\"getQQDistrictQuestion("+jb.getJobId()+");\"></a>");
							}

							sb.append("</td>");	   		

						}
						else if(jb.getExitURL().equalsIgnoreCase("Available"))
						{
							sb.append("<td><strong class='text-info'>Available</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
							}
							else{
								if(jb.getHeadQuarterMaster()==null && jb.getDistrictMaster()!=null && jb.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
								{
									//System.out.println(" Non-Client Job :: "+jb.getJobId());
									sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' onclick=\"chkApplyJobNonClientDashJOI('"+jb.getJobId()+"','"+jb.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jb.getExitMessage()+"','"+jb.getDistrictMaster().getDistrictId()+"','')\" href='javascript:void(0);' ><img src='images/option01.png' style='width:21px; height:21px;' ></a>");
								}
								else
								{
									sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jb.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;' ></a>");
									//sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
								}
								//sb.append("<a data-original-title='"+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jb.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;' ></a>");
								sb.append("&nbsp;<a data-original-title='"+lblhide+"' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return getJbsIHide('"+jb.getJobId()+"')\"><img src='images/unhide.png' style='width:21px; height:21px;' ></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}

							sb.append("</td>");	   		
						}
						else if(jb.getExitURL().equalsIgnoreCase("Completed"))
						{
							sb.append("<td><strong class='text-success'>"+lblCompleted+"</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else{
								sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}

							sb.append("</td>");	
						}
						else if(jb.getExitURL().equalsIgnoreCase("Timed Out"))
						{
							sb.append("<td><strong class='text-error'>"+Utility.getLocaleValuePropByKey("optTOut", locale)+"</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else{
								sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}

							sb.append("</td>");	   	
						}
						else if(jb.getExitURL().equalsIgnoreCase("Withdrawn"))
						{
							sb.append("<td><strong class='text-error'>"+optWithdrawn+"</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png'></a>");
							}
							else{
								sb.append("<a data-original-title='"+toolRe_Apply+"' rel='tooltip' id='tpApply"+rowCount+"' href='javascript:void(0);' onclick=\"getStateBeforeWithdraw('"+jb.getIpAddress()+"')\"><img src='images/option01.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}

							sb.append("</td>");	   		
						}
						else if(jb.getExitURL().equalsIgnoreCase("Hidden"))
						{
							sb.append("<td><strong class='text-error'>Hidden</strong></td>");
							sb.append("<td>");
							if(isEpiStandalone){
								sb.append("<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else{
								//sb.append("<a data-original-title='"+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jb.getJobId()+"' ><img src='images/option01.png'></a>");
								sb.append("<a data-original-title='Un-hide' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"unHideJob('"+jb.getIpAddress()+"');\"><img src='images/option03.png' style='width:21px; height:21px;'></a>");
								//sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+jb.getIpAddress()+"');\"><img src='images/clip.png'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jb.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							sb.append("</td>");	 
						}

						sb.append("</tr>");
					}
				}
				if(rowCount==0)
				{
					sb.append("<tr>");
					sb.append("<td colspan='6'>");
					sb.append(msgNorecordfound);
					sb.append("</td>");	   				
					sb.append("</tr>");
				}

				sb.append("</table>");
				sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,totalRecord,noOfRow, pageNo));
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			///////////////////////

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return sb.toString();
	}


	
	public String getESDistrictAddress(JSONObject jsonObject)
	{
		////System.out.println(" >>>>>>>>>>>>>>>>>>>> Inside Get Dsitrict Address :: >>>>>>>>>>>>>>>>>>> ");
		String districtAddress="";
		String address="";
		String state="";
		String city = "";
		String zipcode="";

		//Get district address
		if(jsonObject.getInt("districtId")!=0)
		{
			if(!jsonObject.getString("distaddress").equals("null") && !jsonObject.getString("distaddress").trim().equals(""))
			{
				if(!jsonObject.getString("disCityName").equals(""))
				{
					districtAddress = jsonObject.getString("distaddress")+", ";
				}
				else
				{
					districtAddress = jsonObject.getString("distaddress");
				}
			}
			if(!jsonObject.getString("disCityName").equals(""))
			{
				if(!jsonObject.getString("disStateName").equals(""))
				{
					city = jsonObject.getString("disCityName")+", ";
				}else{
					city = jsonObject.getString("disCityName");
				}
			}
			if(!jsonObject.getString("disStateName").equals(""))
			{

				if(!jsonObject.getString("zipCode").equals(""))
				{
					state = jsonObject.getString("disStateName")+", ";
				}
				else
				{
					state = jsonObject.getString("disStateName");
				}	
			}
			if(!jsonObject.getString("zipCode").equals(""))
			{
				zipcode = jsonObject.getString("zipCode");
			}

			if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
				address = districtAddress+city+state+zipcode;
			}else{
				address="";
			}

		}

		return address;
	}
	
	public String getESSchoolAddress(JSONObject jsonObject)
	{
		String schoolAddress="";
		String sAddress="";
		String sstate="";
		String scity = "";
		String szipcode="";

		// Get School address
		if(jsonObject.getJSONArray("schoolId").size()>0 && jsonObject.getJSONArray("schoolId").getInt(0)!=0)
		{
			if(jsonObject.getJSONArray("schoolAddress").size()>0 && !jsonObject.getJSONArray("schoolAddress").getString(0).equals(""))
			{
				if(!jsonObject.getJSONArray("schoolCityName").getString(0).equals(""))
				{
					schoolAddress = jsonObject.getJSONArray("schoolAddress").getString(0)+", ";
				}
				else
				{
					schoolAddress = jsonObject.getJSONArray("schoolAddress").getString(0);
				}
			}
			if(!jsonObject.getJSONArray("schoolCityName").getString(0).equals(""))
			{
				if(jsonObject.getJSONArray("schoolStateName").size()>0 && !jsonObject.getJSONArray("schoolStateName").getString(0).equals(""))
				{
					scity = jsonObject.getJSONArray("schoolCityName").getString(0)+", ";
				}else{
					scity = jsonObject.getJSONArray("schoolCityName").getString(0);
				}
			}
			if(jsonObject.getJSONArray("schoolStateName").size()>0 && !jsonObject.getJSONArray("schoolStateName").getString(0).equals(""))
			{
				if(jsonObject.getJSONArray("schoolZip").getInt(0)!=0)
				{
					sstate = jsonObject.getJSONArray("schoolStateName").getString(0)+", ";
				}
				else
				{
					sstate = jsonObject.getJSONArray("schoolStateName").getString(0);
				}	
			}
			if(jsonObject.getJSONArray("schoolZip").getInt(0)!=0)
			{
				szipcode = jsonObject.getJSONArray("schoolZip").getString(0);
			}

			if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
				sAddress = schoolAddress+scity+sstate+szipcode;
			}else{
				sAddress="";
			}
		}

		return sAddress;
	}
	
	
	public List<JobOrder> getJobByPrefES(HttpServletRequest request,Order sortOrderStrVal)
	{
		System.out.println("*****************calling getJobByPrefES***************************");
		
		List<JobOrder> lstJobOrder = null;
		try
		{	
			List<String> lstRegionId=null;
			List<String> lstSchoolTypeId=null;
			List<String> lstGeographyId=null;
			List<SchoolMaster> lstSchoolMasters = null;
			List<SchoolMaster> lstSMtoRemove = new ArrayList<SchoolMaster>();



			HttpSession session = request.getSession();			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			TeacherPreference preference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
			System.out.println(preference.getRegionId());
			System.out.println(preference.getSchoolTypeId());
			System.out.println(preference.getGeoId());

			lstRegionId = Arrays.asList(preference.getRegionId().trim().split("\\|"));

			lstSchoolTypeId = Arrays.asList(preference.getSchoolTypeId().trim().split("\\|"));

			lstGeographyId = Arrays.asList(preference.getGeoId().trim().split("\\|"));



			if(sortOrderStrVal!=null)
			{
				lstJobOrder = jobOrderDAO.findSortedJobtoShow(sortOrderStrVal);
			}
			else
			{
				lstJobOrder = jobOrderDAO.findJobtoShow();
			}
			
			System.out.println(lstJobOrder.size());
			for(JobOrder jb:lstJobOrder)
			{
				if(jb.getCreatedForEntity() == 3)
				{
					lstSchoolMasters=jb.getSchool();
					if(lstSchoolMasters!=null)
					{			
						for(SchoolMaster school: lstSchoolMasters)
						{				
							if(lstGeographyId.contains(""+school.getGeoMapping().getGeoId().getGeoId()) && lstSchoolTypeId.contains(""+school.getSchoolTypeId().getSchoolTypeId() ) && lstRegionId.contains(""+school.getRegionId().getRegionId()))
							{
								//do nothing for now
							}
							else
							{
								//lstSMasters.remove(school);
								lstSMtoRemove.add(school);

							}

						}
						if(jb.getCreatedForEntity() == 3)
						{
							for(SchoolMaster school:lstSMtoRemove)
							{
								lstSchoolMasters.remove(school);
							}
							lstSMtoRemove.clear();
						}
					}
				}
				

			}

			List<JobOrder> lstJbOdrToremove = new ArrayList<JobOrder>(lstJobOrder);
			for( JobOrder jb:lstJbOdrToremove)
			{

				if(jb.getCreatedForEntity() == 3)
				{
					if(jb.getSchool()==null || jb.getSchool().size()==0)
					{

						lstJobOrder.remove(jb);
					}
					else
					{

					}
				}
				else
				{
					if(!lstRegionId.contains(""+jb.getDistrictMaster().getStateId().getRegionId().getRegionId()))
					{
						lstJobOrder.remove(jb);
					}
				}
			}

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			lstJobOrder = new ArrayList<JobOrder>();

		}
		System.out.println(lstJobOrder.size());
		
		
		return lstJobOrder;
	}
	public List<JobOrder> getJobByPrefnew(HttpServletRequest request,Order sortOrderStrVal,List<JobOrder> jobOrderAllList)
	{	
		List<JobOrder> lstJobOrder = jobOrderAllList;
		try
		{	
			List<String> lstRegionId=null;
			List<String> lstSchoolTypeId=null;
			List<String> lstGeographyId=null;
			List<SchoolMaster> lstSchoolMasters = null;
			List<SchoolMaster> lstSMtoRemove = new ArrayList<SchoolMaster>();

			HttpSession session = request.getSession();			
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");

			TeacherPreference preference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);

			lstRegionId = Arrays.asList(preference.getRegionId().trim().split("\\|"));

			lstSchoolTypeId = Arrays.asList(preference.getSchoolTypeId().trim().split("\\|"));

			lstGeographyId = Arrays.asList(preference.getGeoId().trim().split("\\|"));

			System.out.println("lstJobOrder====+++++++getJobByPrefnew++++++++++Size+++++====="+lstJobOrder.size());
		
			for(JobOrder jb:lstJobOrder)
			{
				if(jb.getCreatedForEntity() == 3)
				{
					lstSchoolMasters=jb.getSchool();
					if(lstSchoolMasters!=null)
					{			
						for(SchoolMaster school: lstSchoolMasters)
						{				
							if(lstGeographyId.contains(""+school.getGeoMapping().getGeoId().getGeoId()) && lstSchoolTypeId.contains(""+school.getSchoolTypeId().getSchoolTypeId() ) && lstRegionId.contains(""+school.getRegionId().getRegionId()))
							{
								//do nothing for now
							}
							else
							{
								//lstSMasters.remove(school);
								lstSMtoRemove.add(school);

							}

						}
						//if(jb.getCreatedForEntity() == 3)
						//{
							for(SchoolMaster school:lstSMtoRemove)
							{
								lstSchoolMasters.remove(school);
							}
							lstSMtoRemove.clear();
						//}
					}
				}
			}

			List<JobOrder> lstJbOdrToremove = new ArrayList<JobOrder>(lstJobOrder);
			for( JobOrder jb:lstJbOdrToremove)
			{

				if(jb.getCreatedForEntity() == 3)
				{
					if(jb.getSchool()==null || jb.getSchool().size()==0)
					{

						lstJobOrder.remove(jb);
					}
					else
					{

					}
				}
				else
				{
					if(!lstRegionId.contains(""+jb.getDistrictMaster().getStateId().getRegionId().getRegionId()))
					{
						lstJobOrder.remove(jb);
					}
				}
			}

		} 
		catch (Exception e) 
		{
			//e.printStackTrace();
			lstJobOrder = new ArrayList<JobOrder>();

		}
		return lstJobOrder;
	}
	
	
	public String getJobsOfInterestAjax(String searchBy,String noOfRows,String page,String sortOrderStr,String sortOrderType,String dt,String districtId,String schoolId,String stateId,String cityId,String subjectId[],String sGeoZoneId)
	{
		PrintOnConsole.debugPrintJOI("9000");
		System.out.println("Calling getJobsOfInterestAjax");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
		
		
			try
			{
				TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
				
				String noOfRow =noOfRows; 
				if(noOfRows==null)
					noOfRow="1";

				String pageNo =page;	
				if(page==null)
					pageNo="1";

				int noOfRowInPage =	Integer.parseInt(noOfRow);
				int pgNo =	Integer.parseInt(pageNo);
				
				int start = ((pgNo-1)*noOfRowInPage);
				int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;

				Order sortOrderStrVal=Order.desc("createdDateTime");
				if(sortOrderStr!=null && !sortOrderStr.trim().equalsIgnoreCase(""))
				{
					if(sortOrderType!=null && sortOrderType.equals("0"))
						sortOrderStrVal=Order.asc(sortOrderStr);
					else
						sortOrderStrVal=Order.desc(sortOrderStr);
				}

				Map<Integer, Integer> mapJFTIdByJobId=new HashMap<Integer, Integer>();
				Map<Integer, Integer> mapStatusByJob=new HashMap<Integer, Integer>();
				
				Map<Integer, String> mapStatusNameByStatusId=new HashMap<Integer, String>();
				mapStatusNameByStatusId.put(3, "<strong class='text-error'>"+optIncomlete+"</strong>");
				mapStatusNameByStatusId.put(4, "<strong class='text-success'>"+lblCompleted+"</strong>");
				mapStatusNameByStatusId.put(7, "<strong class='text-error'>"+optWithdrawn+"</strong>");
				mapStatusNameByStatusId.put(8, "<strong class='text-error'>"+optHid+"</strong>");
				mapStatusNameByStatusId.put(9, "<strong class='text-error'>"+optTOut+"</strong>");
				
				List<Integer> jobOrdersApplied=new ArrayList<Integer>();
				
				boolean IsReffer=false;
				if(teacherDetail!=null && teacherDetail.getUserType().equalsIgnoreCase("R"))
					IsReffer=true;
				
				DistrictMaster districtMaster=null;
				PrintOnConsole.debugPrintJOI("9001");
				List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
				//List<JobForTeacher> jobForTeachers=jobForTeacherDAO.findJobByTeacher(teacherDetail);
				//PrintOnConsole.debugPrintJOI("9002");
				boolean bDistrictIdSearch=false;
				int iDistrictIdSearch=Utility.getIntValue(districtId);
				if(iDistrictIdSearch >0)
					bDistrictIdSearch=true;
				
				boolean bJFTStatus=false;
				boolean bJFTStatusAv=false;
				StatusMaster statusMaster=null;
				if(searchBy!=null && !searchBy.equals("") && !searchBy.equalsIgnoreCase("all"))
				{
					statusMaster=new StatusMaster();
					bJFTStatus=true;
					if(searchBy.equalsIgnoreCase("comp"))
						statusMaster.setStatusId(4);
					else if(searchBy.equalsIgnoreCase("icomp"))
						statusMaster.setStatusId(3);
					else if(searchBy.equalsIgnoreCase("vlt"))
						statusMaster.setStatusId(9);
					else if(searchBy.equalsIgnoreCase("widrw"))
						statusMaster.setStatusId(7);
					else if(searchBy.equalsIgnoreCase("hide"))
						statusMaster.setStatusId(8);
					else
					{
						bJFTStatusAv=true;
						bJFTStatus=false;
						statusMaster=null;
					}
				}
				//New Condition for Kelly
				boolean isKellyJobApply = false;
				TeacherPreference teacherPreference=null;
		        try {
		            isKellyJobApply = (Boolean)session.getAttribute("isKelly");//jobForTeacherDAO.getJFTByTeachers(teacherDetail);
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        List<JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
		        List<BranchMaster> lstBranchMasters 		  = new ArrayList<BranchMaster>();
		        if(isKellyJobApply){
		        	
		        	teacherPreference=teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
		        	List<Integer> lstJobCatIds=new ArrayList<Integer>();
		        	List<Long> lstStateIds=new ArrayList<Long>();
		        	if(teacherPreference!=null){
		        		//for JobnCategory
		        		String jobCategoryStr=teacherPreference.getJobCategoryId();
		        		if(jobCategoryStr!=null && jobCategoryStr.trim().length()>0){
		        			String	[]jobCategoryList=jobCategoryStr.split("\\|");
		        			for(String str : jobCategoryList){
		        				lstJobCatIds.add(Utility.getIntValue(str));
		        			}
		        		}
		        		//for States
		        		String stateStr=teacherPreference.getStateId();
		        		if(stateStr!=null && stateStr.trim().length()>0){
		        			String	[]stateList=stateStr.split("\\|");
		        			for(String str : stateList){
		        				lstStateIds.add(Utility.getLongValue(str));
		        			}
		        		}
		        	}
		        	if(lstJobCatIds!=null && lstJobCatIds.size()>0){
		        		lstJobCategoryMasters=jobCategoryMasterDAO.findByCategoryID(lstJobCatIds);
		        	}
		        	if(lstStateIds!=null && lstStateIds.size()>0){
		        		lstBranchMasters=branchMasterDAO.findBrancheMasterByStates(lstStateIds);
		        	}
		        	
		        }
				List<Object> objJobs=jobForTeacherDAO.getAppliedJobListByTeacher(teacherDetail.getTeacherId(),iDistrictIdSearch,statusMaster);
				PrintOnConsole.debugPrintJOI("9002.1");
				
				if(objJobs!=null && objJobs.size() >0)
				{
					System.out.println("objJobs "+objJobs.size());
					for(Object Obj:objJobs)
					{
						Object[] objArr=(Object[])Obj;
						int iJobId=Utility.getIntValue(objArr[1]+"");
						jobOrdersApplied.add(iJobId);
						
						int iStatus=Utility.getIntValue(objArr[2]+"");
						mapStatusByJob.put(iJobId, iStatus);
						
						int iJFTId=Utility.getIntValue(objArr[3]+"");
						mapJFTIdByJobId.put(iJobId, iJFTId);
						
						if(IsReffer)
						{
							int iDistrictId=Utility.getIntValue(objArr[0]+"");
							districtMaster=new DistrictMaster();
							districtMaster.setDistrictId(iDistrictId);
							//System.out.println("iDistrictId "+iDistrictId);
						}
					}
				}
				
				if(bDistrictIdSearch)
				{
					districtMaster=new DistrictMaster();
					districtMaster.setDistrictId(iDistrictIdSearch);
				}
				
				PrintOnConsole.debugPrintJOI("9002.2");
				
				/*if(jobForTeachers!=null)
				{
					for(JobForTeacher forTeacher :jobForTeachers)
					{
						jobOrdersApplied.add(forTeacher.getJobId().getJobId());
					}
					
					
					if(teacherDetail!=null && teacherDetail.getUserType().equalsIgnoreCase("R"))
					{
						districtMaster=jobForTeachers.get(0).getJobId().getDistrictMaster();
						IsReffer=true;
					}
					else
					{
						districtMaster=null;
					}
				}*/
				System.out.println("ppppppppppppppppppppppppppppppppppppppppppppppppppp schoolId:: "+schoolId);
				String schoolId1 =schoolId; //request.getParameter("schoolId").trim();
				String cityName =cityId; //request.getParameter("cityId").trim();
				SchoolMaster schoolMaster = null;
				List<Integer> filteredjobOrders = new ArrayList<Integer>();
				List<String> zipList	=	new ArrayList<String>();
				Boolean schoolFlag = false;
				// For School Search
				if(!schoolId1.equals("0") && !schoolId1.equals(""))
				{
					schoolMaster 	=	schoolMasterDAO.findById(new Long(schoolId1), false, false);
					if(filteredjobOrders.size()>0){
						filteredjobOrders.retainAll(schoolInJobOrderDAO.getSchoolJobs(schoolMaster));
					}else{
						filteredjobOrders.addAll(schoolInJobOrderDAO.getSchoolJobs(schoolMaster));
					}
						
					schoolFlag = true;
					/*schoolMasterlst.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList));

					if(flagJobOrderList==true)
						jobOrderAllList.retainAll(schoolMasterlst);
					else
						jobOrderAllList.addAll(schoolMasterlst);*/

				}
				
				// For Subject
				Boolean subjectFlag=false;
				if(subjectId !=null && !subjectId.equals("") && subjectId.length>0)
				{
					ArrayList<Integer> subjectIdList=  new ArrayList<Integer>();
					List<SubjectMaster> ObjSubjectList = new ArrayList<SubjectMaster>();
					List<Integer> zipJOblst = new ArrayList<Integer>();
					String subIdList[] = subjectId;
					subjectFlag=true;
					for(int i=0;i<subIdList.length;i++)
					{
						subjectIdList.add(Integer.parseInt(subIdList[i]));
						SubjectMaster subjectMaster = new SubjectMaster();
						subjectMaster.setSubjectId(Integer.parseInt(subIdList[i]));
						ObjSubjectList.add(subjectMaster);
					}
					//ObjSubjectList = subjectMasterDAO.getSubjectMasterlist(subjectIdList);
					zipJOblst = jobOrderDAO.findJobIdsBySubjectList(ObjSubjectList);
					System.out.println("zipJOblst subject   "+zipJOblst.size());
					if(filteredjobOrders.size()>0){
						filteredjobOrders.retainAll(zipJOblst);
					}
					else{
						filteredjobOrders.addAll(zipJOblst);
					}					
				
				}
				
				Boolean jobByState=false;
				try{
					if(!stateId.equals("0") && !stateId.equals(""))
					{
						List<DistrictMaster> districtByState = new ArrayList<DistrictMaster>();
						StateMaster stateMaster = stateMasterDAO.findById(Long.parseLong(stateId), false, false);
						districtByState = districtMasterDAO.getDistrictListByState(stateMaster);
						jobByState=true;
						List<Integer> zipJOblst = new ArrayList<Integer>();
						if(districtByState!=null && districtByState.size()>0)
						{
							zipJOblst=jobOrderDAO.findJobIdsByDistrictList(districtByState);							
						}	
						
						if(filteredjobOrders.size()>0){
							filteredjobOrders.retainAll(zipJOblst);
						}
						else{
							filteredjobOrders.addAll(zipJOblst);
						}
						
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
				}
				Boolean cityFlag = false;
				if(!cityName.equals("0")){
					zipList = cityMasterDAO.findCityByName(cityName);
					
					// For Cities (ZipCode List)
					if(zipList!=null && zipList.size()>0)
					{
						//List<DistrictMaster> lstDistrictMasters = new ArrayList<DistrictMaster>();
						List<Integer> zipJOblst = new ArrayList<Integer>();
						List<SchoolMaster> lstSchoolMasters = null;
						cityFlag=true;
						lstSchoolMasters = schoolMasterDAO.findByZipCodeList(zipList);
						if(lstSchoolMasters!=null)
						{
							zipJOblst=schoolInJobOrderDAO.getSchoolListJobs(lstSchoolMasters);
						}
						
						if(filteredjobOrders.size()>0){
							filteredjobOrders.retainAll(zipJOblst);
						}
						else{
							filteredjobOrders.addAll(zipJOblst);
						}							
					}
				}
				
				Integer geoZoneId=0;
				String zoneId   =sGeoZoneId;  //request.getParameter("geoZoneId");
				Boolean zoneFlag=false;
				if(zoneId!=null && !zoneId.equalsIgnoreCase(""))
					geoZoneId =  Integer.parseInt(zoneId);
				if(geoZoneId>0)
				{
					System.out.println("geoZoneId   "+geoZoneId);
					List<Integer> listjobOrdersgeoZone = new ArrayList<Integer>();
					GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
					
					zoneFlag=true;
					if(geoZoneMaster!=null)
						listjobOrdersgeoZone = jobOrderDAO.getJobIdsBygeoZone(geoZoneMaster);
					
					System.out.println("listjobOrdersgeoZone   "+listjobOrdersgeoZone.size());
					System.out.println("geoZoneId 2  "+geoZoneId);
					if(filteredjobOrders.size()>0){
						filteredjobOrders.retainAll(listjobOrdersgeoZone);
					}
					else{
						filteredjobOrders.addAll(listjobOrdersgeoZone);
					}	
					
					System.out.println("geoZoneId 3  "+geoZoneId);

				}
				PrintOnConsole.debugPrintJOI("9003");
				List<JobOrder> jobOrders=new ArrayList<JobOrder>();
				Date dateWithoutTime = Utility.getDateWithoutTime();
				
				Criterion criterion1 = Restrictions.eq("status","a");
				Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
				Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
				Criterion criterion6 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion7 = Restrictions.isNotNull("headQuarterMaster");
				Criterion criterion8 = Restrictions.in("jobCategoryMaster",lstJobCategoryMasters);
				Criterion criterion9 = Restrictions.in("branchMaster",lstBranchMasters);
				
				List<Criterion> criterions=new ArrayList<Criterion>();
				
				if(schoolFlag || jobByState || cityFlag || subjectFlag || zoneFlag)
				{					
					Criterion criterion11 = Restrictions.in("jobId",filteredjobOrders);
					criterions.add(criterion11);
					IsReffer=false;
				}
				
				if(bJFTStatus)
				{
					if(jobOrdersApplied!=null && jobOrdersApplied.size() >0)
					{
						Criterion criterion10 = Restrictions.in("jobId",jobOrdersApplied);
						criterions.add(criterion10);
					}
				}
				else
				{
					criterions.add(criterion1);
					criterions.add(criterion2);
					criterions.add(criterion3);
					criterions.add(criterion4);
					criterions.add(criterion5);
					
					if(isKellyJobApply){
						criterions.add(criterion7);
						criterions.add(criterion8);
						criterions.add(criterion9);
						if(bDistrictIdSearch)
							criterions.add(criterion6);
					}
					else if(IsReffer || bDistrictIdSearch)
					{
						criterions.add(criterion6);
					}
					
					if(bJFTStatusAv)
					{
						if(jobOrdersApplied!=null && jobOrdersApplied.size()>0)
						{
							System.out.println("jobOrdersApplied "+jobOrdersApplied.size());
							Criterion criterion11 = Restrictions.not(Restrictions.in("jobId",jobOrdersApplied));
							criterions.add(criterion11);
						}
					}
				}
				//schoolFilter
				/*if(schoolFlag || jobByState || cityFlag || subjectFlag || zoneFlag)
				{					
					Criterion criterion11 = Restrictions.in("jobId",filteredjobOrders);
					criterions.add(criterion11);					
				}*/
				/*if(jobOrdersApplied!=null && jobOrdersApplied.size() >0)
				{
					Criterion criterion7 = Restrictions.not(Restrictions.in("jobId", jobOrdersApplied));
					criterions.add(criterion7);
				}*/
				
				int iTotalJobCount=0;
				if(criterions!=null && criterions.size()>0)
				{
					jobOrders=jobOrderDAO.getJobListForInterest(sortOrderStrVal, start, end, criterions);
					PrintOnConsole.debugPrintJOI("9004");
					iTotalJobCount=jobOrderDAO.getJobListForInterestCount(criterions);
				}
				

				PrintOnConsole.debugPrintJOI("9005");
				
				System.out.println("jobOrders size "+jobOrders.size() +" iTotalJobCount "+iTotalJobCount +" UserType "+teacherDetail.getUserType() +" jobForTeachers Size "+jobForTeachers.size());
				PrintOnConsole.debugPrintJOI("9006");
				
				String sortOrderFieldName="createdDateTime";
				String sortOrderTypeVal="0";
				String responseText="";

				sb.append("<table border='0' id='tblGridJoF'>");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");
				responseText=PaginationAndSorting.responseSortingLink(lblJoTil,sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				sb.append("<th width='20%' valign='top'>"+responseText+"</th>");
				/*sb.append("<th width='20%' valign='top'>Job Title</th>");*/

				responseText=PaginationAndSorting.responseSortingLink(lblExpiryDate,sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
				sb.append("<th width='7%' valign='top'>"+responseText+"</th>");
				//sb.append("<th width='7%' valign='top'>"+lblExpiryDate+"</th>");

				/*responseText=PaginationAndSorting.responseSortingLink(lblDistrictName,sortOrderFieldName,"districtName",sortOrderTypeVal,pgNo);
				sb.append("<th width='18%' valign='top'>"+responseText+"</th>");*/
				sb.append("<th width='18%' valign='top'>"+lblDistrictName+"</th>");

				/*responseText=PaginationAndSorting.responseSortingLink(lblZone,sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
				sb.append("<th width='10%' valign='top'>"+responseText+"</th>");*/
				sb.append("<th width='10%' valign='top'>"+lblZone+"</th>");

				/*responseText=PaginationAndSorting.responseSortingLink(lblSchool,sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
				sb.append("<th width='18%' valign='top'>"+responseText+"</th>");*/
				sb.append("<th width='18%' valign='top'>"+lblSchool+"</th>");

				/*responseText=PaginationAndSorting.responseSortingLink(lblAdd,sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
				sb.append("<th width='15%' valign='top'>"+responseText+"</th>");*/
				sb.append("<th width='15%' valign='top'>"+lblAdd+"</th>");

				sb.append("<th width='5%' valign='top'>"+lblJoStatus+"</th>");
				sb.append("<th width='10%' valign='top'>"+lblAct+"</th>");
				sb.append("</tr>");
				sb.append("</thead>");
				
				int iJFTIdInner=0;
				int rowCount=0;
				PrintOnConsole.debugPrintJOI("Start Build Text 9007");
				
					//totaRecord=totaRecord+lstJobOrder.size();
					for(JobOrder jbOrder: jobOrders)
					{			
						rowCount++;
						sb.append("<tr>");
						sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
						

						//	sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobStartDate())+"</td>");

						if(Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>"+lblUntilfilled+"</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate())+",11:59 PM CST</td>");
						
						
						
							sb.append("<td>");
							if(jbOrder.getDistrictMaster()!=null)
							sb.append(""+jbOrder.getDistrictMaster().getDistrictName());
							sb.append("</td>");
							
							if(jbOrder.getGeoZoneMaster()!=null && !jbOrder.getGeoZoneMaster().getGeoZoneName().equals(""))
							{			
								if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
								{
									sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
								else if(jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
									//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jb.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
									sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}else{
									sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jbOrder.getDistrictMaster().getDistrictId()+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
								}
							}
							else
							{
								sb.append("<td>");							
								sb.append("</td>");
							}
							
							//sb.append("<td>&nbsp;</td>");
							
							if(jbOrder.getSchool().size()==1)
							{
								if(jbOrder.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
								}else{
									if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
									}
									else
									{
										sb.append("<td>"+jbOrder.getSchool().get(0).getSchoolName()+"</td>");
										sb.append("<td>");
										sb.append(""+getSchoolAddress(jbOrder));
										sb.append("</td>");
									}
								}
							}
							else if(jbOrder.getSchool().size()>1)
							{
								String schoolListIds = jbOrder.getSchool().get(0).getSchoolId().toString();
								for(int i=1;i<jbOrder.getSchool().size();i++)
								{
									schoolListIds = schoolListIds+","+jbOrder.getSchool().get(i).getSchoolId().toString();
								}
								if(jbOrder.getIsPoolJob()==2){
									sb.append("<td>&nbsp;</td>");
									sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
								}else{
									if(jbOrder.getDistrictMaster().getDistrictId()==1200390)
									{
										sb.append("<td>&nbsp;</td>");	
										sb.append("<td>"+getDistrictAddress(jbOrder)+"</td>");
									}
									else
									{
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
										sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
									}
								}
							}
							else
							{
								sb.append("<td>&nbsp;</td>");
								sb.append("<td>");
								sb.append(""+getDistrictAddress(jbOrder));
								sb.append("</td>");
							}

							/*sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jbOrder));
							sb.append("</td>");*/
							
						if(mapStatusByJob!=null && mapStatusByJob.get(jbOrder.getJobId())!=null)
						{
							int iStatusInner= mapStatusByJob.get(jbOrder.getJobId());
							String sDispalyStatusName="";
							if(mapStatusNameByStatusId!=null && mapStatusNameByStatusId.get(iStatusInner)!=null)
								sDispalyStatusName=mapStatusNameByStatusId.get(iStatusInner);
							sb.append("<td>"+sDispalyStatusName+"</td>");
							
							sb.append("<td>");
							if(iStatusInner==4)
							{
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+iJFTIdInner+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+iJFTIdInner+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else if(iStatusInner==9)
							{ // Timeout
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+iJFTIdInner+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+iJFTIdInner+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else if(iStatusInner==7)
							{//with
								
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title=' Re-Apply' rel='tooltip' id='tpApply"+rowCount+"' href='javascript:void(0);' onclick=\"getStateBeforeWithdraw('"+iJFTIdInner+"')\"><img src='images/option01.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+iJFTIdInner+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else if(iStatusInner==8)
							{ // Hidden
								
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title='Un-hide' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"unHideJob('"+iJFTIdInner+"');\"><img src='images/option03.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							}
							else if(iStatusInner==3)
							{	// In
								
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title=''"+lblCompleteNow+"'' rel='tooltip' id='tpJbStatus"+rowCount+"' href='#' onclick=\"getTeacherCriteria('"+jbOrder.getJobId()+"');\"><img src='images/option02.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+lblWithdraw+"' rel='tooltip' id='tpWithdraw"+rowCount+"' href='#' onclick=\"return getJbsIWithdraw('"+iJFTIdInner+"')\"><img src='images/option05.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' href='javascript:void(0);' onclick=\"getCoverLetter('"+iJFTIdInner+"');\"><img src='images/clip.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='QQ' class='icon-thumbs-up icon-large thumbUpIconBlueColor' rel='tooltip' id='QQ"+rowCount+"' href='javascript:void(0);' onclick=\"getQQDistrictQuestion("+jbOrder.getJobId()+");\"></a>");
							}
							sb.append("</td>");
							
						}
						else
						{
							
							sb.append("<td><strong class='text-info'>"+lblAvailable+"</strong></td>");
							sb.append("<td>");
							sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='"+lblhide+"' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return getJbsIHide('"+jbOrder.getJobId()+"')\"><img src='images/unhide.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
							sb.append("</td>");
						}
						
						
						/*if(isEpiStandalone){
							sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}
						else{
							
							sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='Hide' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return getJbsIHide('"+jbOrder.getJobId()+"')\"><img src='images/unhide.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:21px; height:21px;'></a>");
						}*/

						
						
						sb.append("</tr>");
					}
				
					
//start sandeep 27-july
					
					 
					List<JobOrder> expJobList = new ArrayList<JobOrder>();
					try {
						if(jobOrdersApplied.size()>0){
						Criterion criterion15 = Restrictions.in("jobId",jobOrdersApplied);
						expJobList=jobOrderDAO.findByCriteria(criterion15);
						System.out.println("Applid TeacherJob Size############"+expJobList.size()+"-------"+jobOrdersApplied);
						}
					} catch (Exception e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
						System.out.println("Applid TeacherJob Size############-------"+jobOrdersApplied.size());
					}
					
					
					for(JobOrder jbOrder: expJobList)
					{		
						if(! (jbOrder.getDistrictMaster()!=null && jbOrder.getDistrictMaster().getHeadQuarterMaster() != null && jbOrder.getDistrictMaster().getHeadQuarterMaster().getHeadQuarterId()==NC_HEADQUARTER)){
							continue;
						}	
						
						Boolean jobStatusFlag = false;
						
	                        if(jbOrder.getStatus()!=null && !jbOrder.getStatus().equalsIgnoreCase("A"))
	        				    jobStatusFlag = true;
	        				else{
	        					if(!Utility.checkDateBetweenTwoDate(jbOrder.getJobStartDate(), jbOrder.getJobEndDate()))
	        						jobStatusFlag = true;
							}
						
                        if(!jobStatusFlag){
                        	System.out.println("active job----------"+ jbOrder.getJobId());
                        	continue;
                        }	
                        	System.out.println("Inactive job----------"+ jbOrder.getJobId());
						rowCount++;
						sb.append("<tr>");
						sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
						if(Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate()).equals("Dec 25, 2099"))
							sb.append("<td>"+lblUntilfilled+"</td>");
						else
							sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate())+",11:59 PM CST</td>");
							sb.append("<td>");
							if(jbOrder.getDistrictMaster()!=null)
							sb.append(""+jbOrder.getDistrictMaster().getDistrictName());
							sb.append("</td>");
							
							sb.append("<td>&nbsp;</td>");
							

							sb.append("<td>&nbsp;</td>");
							sb.append("<td>");
							sb.append(""+getDistrictAddress(jbOrder));
							sb.append("</td>");
							
						if(mapStatusByJob!=null && mapStatusByJob.get(jbOrder.getJobId())!=null)
						{
							int iStatusInner= mapStatusByJob.get(jbOrder.getJobId());
							String sDispalyStatusName="";
							if(mapStatusNameByStatusId!=null && mapStatusNameByStatusId.get(iStatusInner)!=null)
								sDispalyStatusName=mapStatusNameByStatusId.get(iStatusInner);
							sb.append("<td>"+sDispalyStatusName+"</td>");
							
							sb.append("<td>");
							if(iStatusInner==4)
							{
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title='This Job is Expired.' rel='tooltip' id='tpWithdraw"+rowCount+"'><img src='images/option05_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpCoverLetter"+rowCount+"'><img src='images/clip_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpShare"+rowCount+"'><img src='images/option04_grey.png' style='width:21px; height:21px;'></a>");
							}
							else if(iStatusInner==9)
							{ // Timeout
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title='This Job is Expired.' rel='tooltip' id='tpWithdraw"+rowCount+"' ><img src='images/option05_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpCoverLetter"+rowCount+"' ><img src='images/clip_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpShare"+rowCount+"' ><img src='images/option04_grey.png' style='width:21px; height:21px;'></a>");
							}
							else if(iStatusInner==7)
							{//with
								
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title='This Job is Expired.' rel='tooltip' id='tpApply"+rowCount+"' ><img src='images/option01_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpCoverLetter"+rowCount+"'><img src='images/clip_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpShare"+rowCount+"'><img src='images/option04_grey.png' style='width:21px; height:21px;'></a>");
							}
							else if(iStatusInner==8)
							{ // Hidden
								
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title='This Job is Expired.' rel='tooltip' id='tpWithdraw"+rowCount+"' ><img src='images/option03_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpShare"+rowCount+"' ><img src='images/option04_grey.png' style='width:21px; height:21px;'></a>");
							}
							else if(iStatusInner==3)
							{	// In
								
								iJFTIdInner=getJFTId(mapJFTIdByJobId, jbOrder);
								
								sb.append("<a data-original-title='This Job is Expired.' rel='tooltip' id='tpJbStatus"+rowCount+"' ><img src='images/option02_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpWithdraw"+rowCount+"' ><img src='images/option05_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='"+headCoverLetr+"' rel='tooltip' id='tpCoverLetter"+rowCount+"' ><img src='images/clip_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpShare"+rowCount+"' ><img src='images/option04_grey.png' style='width:21px; height:21px;'></a>");
								sb.append("&nbsp;<a data-original-title='This job is Expired.' rel='tooltip' id='QQ"+rowCount+"' ><img src='images/qqthumb_grey.png' style='width:25px; height:20px;'></a>");
							}
							sb.append("</td>");
							
						}
						else
						{
							
							sb.append("<td><strong class='text-info'>"+lblAvailable+"</strong></td>");
							sb.append("<td>");
							sb.append("<a data-original-title='This Job is Expired.' rel='tooltip' id='tpApply"+rowCount+"'  ><img src='images/option01_grey.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpHide"+rowCount+"' ><img src='images/unhide_grey.png' style='width:21px; height:21px;'></a>");
							sb.append("&nbsp;&nbsp;<a data-original-title='This Job is Expired.' rel='tooltip' id='tpShare"+rowCount+"' ><img src='images/option04_grey.png' style='width:21px; height:21px;'></a>");
							sb.append("</td>");
						}
						
						
							sb.append("</tr>");
					}				
						
//end	sandeep 27-july					
					
					
					
				PrintOnConsole.debugPrintJOI("End Build Text 9008");
				if(iTotalJobCount==0)
				{
					sb.append("<tr>");
					sb.append("<td colspan='6'>");
					sb.append(msgNorecordfound);
					sb.append("</td>");	   				
					sb.append("</tr>");
				}

				sb.append("</table>");
				PrintOnConsole.debugPrintJOI("9009");
				sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request,iTotalJobCount,noOfRow, pageNo));
				PrintOnConsole.debugPrintJOI("9010");
				
				try {
					teacherDetail=null;
					mapJFTIdByJobId=null;
					mapStatusByJob=null;
					mapStatusNameByStatusId=null;
					jobOrdersApplied=null;
					districtMaster=null;
					jobForTeachers=null;
					objJobs=null;
					jobOrders=null;
					criterions=null;
				} catch (Exception e) {
					// TODO: handle exception
				}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
		
		PrintOnConsole.debugPrintJOI("9011");
		return sb.toString();
	}
	
	
	public int getJFTId(Map<Integer, Integer> mapJFTIdByJobId,JobOrder jbOrder)
	{
		int iJFTIdInner=0;
		if(mapJFTIdByJobId!=null && mapJFTIdByJobId.get(jbOrder.getJobId())!=null)
			iJFTIdInner=mapJFTIdByJobId.get(jbOrder.getJobId());
		return iJFTIdInner;
	}
	
	public String showDashboardJbofIntresGridAvailable()
	{
		PrintOnConsole.debugPrintJOI("8000");
		System.out.println("Calling showDashboardJbofIntresGridAvailable");

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		StringBuffer sb=new StringBuffer();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(msgYrSesstionExp);
		}
		
			try
			{
				TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
				
				List<Integer> jobOrdersApplied=new ArrayList<Integer>();
				boolean IsReffer=false;
				if(teacherDetail!=null && teacherDetail.getUserType().equalsIgnoreCase("R"))
					IsReffer=true;
				
				DistrictMaster districtMaster=null;
				PrintOnConsole.debugPrintJOI("8001");
				boolean isKellyJobApply = false;
				try{
				isKellyJobApply = (Boolean)session.getAttribute("isKelly");//jobForTeacherDAO.getJFTByTeachers(teacherDetail);
				}catch(Exception e){
					e.printStackTrace();
					isKellyJobApply=false;
				}
				
				//kelly specific
				TeacherPreference teacherPreference=null;
				List<JobCategoryMaster> lstJobCategoryMasters = new ArrayList<JobCategoryMaster>();
				List<BranchMaster> lstBranchMasters 		  = new ArrayList<BranchMaster>();
				if(isKellyJobApply){
					
					teacherPreference=teacherPreferenceDAO.findPrefByTeacher(teacherDetail);
					List<Integer> lstJobCatIds=new ArrayList<Integer>();
					List<Long> lstStateIds=new ArrayList<Long>();
					if(teacherPreference!=null){
						//for JobnCategory
						String jobCategoryStr=teacherPreference.getJobCategoryId();
						if(jobCategoryStr!=null && jobCategoryStr.trim().length()>0){
							String	[]jobCategoryList=jobCategoryStr.split("\\|");
							for(String str : jobCategoryList){
								lstJobCatIds.add(Utility.getIntValue(str));
							}
						}
						//for States
						String stateStr=teacherPreference.getStateId();
						if(stateStr!=null && stateStr.trim().length()>0){
							String	[]stateList=stateStr.split("\\|");
							for(String str : stateList){
								lstStateIds.add(Utility.getLongValue(str));
							}
						}
					}
					if(lstJobCatIds!=null && lstJobCatIds.size()>0){
						lstJobCategoryMasters=jobCategoryMasterDAO.findByCategoryID(lstJobCatIds);
					}
					if(lstStateIds!=null && lstStateIds.size()>0){
						lstBranchMasters=branchMasterDAO.findBrancheMasterByStates(lstStateIds);
					}
					
				}
				//***************************************
				
				List<Object> objJobs=jobForTeacherDAO.getAppliedJobListByTeacher(teacherDetail.getTeacherId(),0,null);
				PrintOnConsole.debugPrintJOI("8002");
				int iWithDrawn=0;
				boolean bDistFound=false;
				if(objJobs!=null && objJobs.size() >0)
				{
					for(Object Obj:objJobs)
					{
						Object[] objArr=(Object[])Obj;
						int iJobId=Utility.getIntValue(objArr[1]+"");
						jobOrdersApplied.add(iJobId);
						
						int iStatus=Utility.getIntValue(objArr[2]+"");
						if(iStatus==7)
							iWithDrawn++;
						
						if(!bDistFound)
						{
							int iDistrictId=Utility.getIntValue(objArr[0]+"");
							districtMaster=new DistrictMaster();
							districtMaster.setDistrictId(iDistrictId);
						}
					}
				}
				PrintOnConsole.debugPrintJOI("8003");
				List<JobOrder> jobOrders=new ArrayList<JobOrder>();
				Date dateWithoutTime = Utility.getDateWithoutTime();
				
				Criterion criterion1 = Restrictions.eq("status","a");
				Criterion criterion2 = Restrictions.le("jobStartDate",dateWithoutTime);
				Criterion criterion3 = Restrictions.ge("jobEndDate",dateWithoutTime);
				Criterion criterion4 = Restrictions.eq("approvalBeforeGoLive",1);
				Criterion criterion5 = Restrictions.ne("isInviteOnly",true);
				Criterion criterion6 = Restrictions.eq("districtMaster",districtMaster);
				Criterion criterion7 = Restrictions.isNotNull("headQuarterMaster");
				Criterion criterion8 = Restrictions.in("jobCategoryMaster",lstJobCategoryMasters);
				Criterion criterion9 = Restrictions.in("branchMaster",lstBranchMasters);
				
				List<Criterion> criterions=new ArrayList<Criterion>();
				criterions.add(criterion1);
				criterions.add(criterion2);
				criterions.add(criterion3);
				criterions.add(criterion4);
				criterions.add(criterion5);
				
				if(IsReffer && !isKellyJobApply)
				{
					criterions.add(criterion6);
				}
				else if(isKellyJobApply)
				{
					criterions.add(criterion7);
					criterions.add(criterion8);
					criterions.add(criterion9);
				}
				if(jobOrdersApplied!=null && jobOrdersApplied.size()>0)
				{
					Criterion criterion11 = Restrictions.not(Restrictions.in("jobId",jobOrdersApplied));
					criterions.add(criterion11);
				}
					PrintOnConsole.debugPrintJOI("8004");
				int iTotalJobCount=0;
				if(criterions!=null && criterions.size()>0)
				{
					Order sortOrderStrVal=Order.desc("createdDateTime");
						PrintOnConsole.debugPrintJOI("8005");
					jobOrders=jobOrderDAO.getJobListForInterest(sortOrderStrVal, 0, 4, criterions);
						PrintOnConsole.debugPrintJOI("8006");
					iTotalJobCount=jobOrderDAO.getJobListForInterestCount(criterions);
						PrintOnConsole.debugPrintJOI("8007");
				}
					PrintOnConsole.debugPrintJOI("8008");
				
				try {
					
					sb.append("<table width='100%'  class='table  table-striped' style='border-top-color:#007ab4;border-top-radius:0px;'>");
					sb.append("<div class='bucketheader' style='height:28px;margin-left: -1.01px;'></div>");
					sb.append("<thead class='bg'>");
					sb.append("<tr style='background-color: #007ab4;'>");
					sb.append("<th width='40%' style='height:40px;'><img class='fl' src='images/jobs_of_interest_bucket.png' style='height:35px;width:35px;'> <div  class='subheading' style='color:white;font-size:13px;margin-left:45px;'>"+Utility.getLocaleValuePropByKey("lblJoOfInterest", locale)+"</div></th>");
					sb.append("<th width='35%' style='vertical-align: middle;'>"+lblDistrictName+"</th>");
					sb.append("<th width='25%' style='vertical-align: middle;'>"+lblAct+"</th>");
					sb.append("</tr>");
					sb.append("</thead>");

					int rowCount=0;
					PrintOnConsole.debugPrintJOI("8009");
					for(JobOrder jbOrder: jobOrders)
					{
						rowCount++;
						sb.append("<tr>");
						if(jbOrder.getDistrictMaster()!=null && jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
						{
							sb.append("<td><a onclick=\"chkApplyJobNonClientDashBoard('"+jbOrder.getJobId()+"','"+jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jbOrder.getExitURL()+"','"+jbOrder.getDistrictMaster().getDistrictId()+"','')\" href='javascript:void(0)' >"+jbOrder.getJobTitle()+"</a></td>");
						}
						else
						{
							sb.append("<td><a href='applyjob.do?jobId="+jbOrder.getJobId()+"'>"+jbOrder.getJobTitle()+"</a></td>");
						}
						if(jbOrder.getDistrictMaster()!=null)
							sb.append("<td class='text-info' style='color:#007ab4;'>"+jbOrder.getDistrictMaster().getDistrictName()+"</td>");
						else
							sb.append("<td class='text-info' style='color:#007ab4;'>"+jbOrder.getHeadQuarterMaster().getHeadQuarterName()+"</td>");
						
						sb.append("<td>");
						if(jbOrder.getDistrictMaster()!=null && jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()==2)
						{
							sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' onclick=\"chkApplyJobNonClientDashBoard('"+jbOrder.getJobId()+"','"+jbOrder.getDistrictMaster().getJobApplicationCriteriaForProspects()+"','"+jbOrder.getExitURL()+"','"+jbOrder.getDistrictMaster().getDistrictId()+"','');\"   href='javascript:void(0)' ><img src='images/option01.png'  \">");					
						}
						else
						{
							sb.append("<a data-original-title=' "+lblApplyNow+"' rel='tooltip' id='tpApply"+rowCount+"' href='applyjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png'style='width:21px; height:21px;'></a>");
						}
						sb.append("&nbsp;&nbsp;<a data-original-title='"+lblhide+"' rel='tooltip' id='tpHide"+rowCount+"' href='#' onclick=\"return jbHide('"+jbOrder.getJobId()+"')\"><img src='images/option03.png'style='width:21px; height:21px;'></a>");
						sb.append("&nbsp;&nbsp;<a data-original-title='"+btnShare+"' rel='tooltip' id='tpShare"+(rowCount+4)+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png'style='width:21px; height:21px;'></a>");

						sb.append("</td>");	   				
						sb.append("</tr>");
					}
					PrintOnConsole.debugPrintJOI("8010");

					sb.append("<tr>");
					sb.append("<td colspan='3' >"+lblYouhave+" "
							+(iTotalJobCount==0?"0":"<a href='jobsofinterest.do?searchby=avlbl'>"+iTotalJobCount+"</a>")
							+" "+lblNotAppliedand+" "+(iWithDrawn==0?"0":"<a href='jobsofinterest.do?searchby=widrw'>"+iWithDrawn+"</a>")+" "+optWithdrawn);
					sb.append("<div class='right'><a href='jobsofinterest.do'><strong>"+lblSeeAll+"</strong></a></div>");
					sb.append("</td>");
					sb.append("</tr>");
					sb.append("</table>");
					
				} catch (Exception e) {
					
				}
				PrintOnConsole.debugPrintJOI("8011");
				try {
					teacherDetail=null;
					jobOrdersApplied=null;
					districtMaster=null;
					objJobs=null;
					jobOrders=null;
					criterions=null;
				} catch (Exception e) {}
			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}
			PrintOnConsole.debugPrintJOI("8012");
		return sb.toString();
	
	}
	
	public String connectEREG(String teacherId){
		logger.setLevel(Level.TRACE);
		logger.trace(" connectEREG call ");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster = null;
		TeacherDetail teacherDetail = null;
		MQEvent mqEvent = null;
		MQEventHistory mqEventHistory = null;
		String eRegSSO = Utility.getLocaleValuePropByKey("lbleRegSSO", locale);
		String result ="";
		try{
			if(request.getSession()!=null && session.getAttribute("userMaster")!=null)
				userMaster = (UserMaster) session.getAttribute("userMaster");

			if(teacherId!=null && teacherId.length()>0)
				teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);

			Map<Object, Object> mapData = new HashMap<Object, Object>();
			mapData.put("teacherId", teacherDetail.getTeacherId());
			if(teacherDetail.getKSNID()!=null){
				mapData.put("KSNID", teacherDetail.getKSNID()+"");
				logger.trace(" ksn id : "+teacherDetail.getKSNID());
			}
			mqEvent = mqEventDAO.findMQEventSSO(teacherDetail);

			if(mqEvent==null){
				mqEvent = new MQEvent();
				mqEvent.setEventType(eRegSSO);
				mqEvent.setCreatedDateTime(new Date());
				mqEvent.setTeacherdetail(teacherDetail);
				mqEvent.setStatus("R");
				mqEvent.setIpAddress(IPAddressUtility.getIpAddress(request));
				
				/**
				 * for testing Start
				 */
				String tokenId = "";
				String msg ="";
				String key = Utility.getLocaleValuePropByKey("SSOLoginKeyWord", locale);
				try {
					msg = key + teacherDetail.getKSNID().toString();
					tokenId = Utility.getTrustedTokenId(msg, key, "HmacMD5");
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				mqEvent.setStatus("C");
				mqEvent.setAckStatus("Success");
				mqEvent.setMsgStatus("Data Sent To TeacherMatch");
				mqEvent.seteRegURL("https://eonbdev.kellyservices.com/AuthenticateTeacherMatchUser.aspx?username=0&ksnid="+teacherDetail.getKSNID()+"&tokenid="+tokenId+"");
				/**
				 * for testing End
				 */
				
				mqEventDAO.makePersistent(mqEvent);
			}
			
				mqEventHistory = new MQEventHistory();
				mqEventHistory.setCreatedDateTime(new Date());
				mqEventHistory.setMqEvent(mqEvent);
				mqEventHistory.setCreatedBy(1);
				mqEventHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				mqEventHistoryDAO.makePersistent(mqEventHistory);

			mapData.put("MQEventID", mqEvent.getEventId());

			if(mqEventHistory!=null)
				mapData.put("MQEventHistoryID", mqEventHistory.getEventHistoryId());
			
			/*ApplicationContext APPLICATIONCONTEXT = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
			JmsTemplate jmsTemplate =  (JmsTemplate) APPLICATIONCONTEXT.getBean("JMSTemplate");

			ActiveMQQueue eventSend =  (ActiveMQQueue)  APPLICATIONCONTEXT.getBean("EReg");
			logger.trace(" -----------Before sending active mq of -------------");
			jmsTemplate.convertAndSend(eventSend,mapData);
			logger.trace(" -----------After sending active mq of -------------");*/
			
			MQEvent mqEventObj=mqEventDAO.findMQEventSSO(teacherDetail);
			boolean SSOFlag=true;
			if(mqEventObj!=null){
				if(mqEventObj.getStatus()!=null && mqEventObj.getStatus().equalsIgnoreCase("C") && mqEventObj.geteRegURL()!=null){
					SSOFlag=false;
				}
			}
			
			int counter=0;
			logger.trace(" SSOFlag ::: "+SSOFlag);
			if(SSOFlag){
				while(!(mqEventObj!=null && mqEventObj.getMsgStatus()!=null) && counter<8 ){
					ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
					logger.trace("Time Mid "+counter+": "+new Date());
					mqEventObj=mqEventDAO.findMQEventSSO(teacherDetail);
					logger.trace(counter+"::::::::::MQEvent :::>>>>>>>>>>>: "+mqEventObj);
					if(mqEventObj!=null && mqEventObj.getAckStatus()!=null && mqEventObj.getAckStatus().equalsIgnoreCase("success")){
						result = mqEventObj.getAckStatus()+"###"+mqEventObj.geteRegURL();
					}
					else if(mqEventObj!=null && mqEventObj.getAckStatus()!=null && mqEventObj.getAckStatus().equalsIgnoreCase("failed")){
						result = mqEventObj.getAckStatus()+"###"+mqEventObj.getMsgStatus();
					}
					counter++;
					try {
						Runnable worker = new MQEventThread();
						executor.execute(worker);
					} catch (Exception e) {
					} 
					executor.shutdown();
					while (!executor.isTerminated()){}
				}
			}
			else{
				if(mqEventObj!=null && mqEventObj.getAckStatus()!=null  && mqEventObj.getAckStatus().equalsIgnoreCase("success")){
					result = mqEventObj.getAckStatus()+"###"+mqEventObj.geteRegURL();
				}
				else if(mqEventObj!=null && mqEventObj.getAckStatus()!=null && mqEventObj.getAckStatus().equalsIgnoreCase("failed")){
					result = mqEventObj.getAckStatus()+"###"+mqEventObj.getMsgStatus();
				}
			}
			
		}
		catch(Exception ex){
			logger.setLevel(Level.ERROR);
			logger.trace(""+ex.getMessage());
			ex.printStackTrace();
		}

		return result;
	}
}
