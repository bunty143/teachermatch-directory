import sys
from scipy import cluster, array, stats
import random
num_cluster = 2
#k means is also quite random, meaning that everytime you run it it will be a bit different. So what's best if we run this multiple times, and take the median, values, and get the cut off that is the closest to the median. This will determine however many times you want to run kmeans. 
num_run = 100

#randomly generate data. If you want to play with the array that you pass onto Java, then you can use that instead.
#obs = [56.00,74.00,92.00,64.00,52.00,76.00,15.00,19.00,29.00,30.00,30.00,30.00,31.00,32.00,32.00,32.00,33.00,33.00,33.00,34.00,34.00,34.00,34.00,35.00,35.00,35.00,39.00,39.00,39.00,39.00,39.00,39.00,39.00,39.00,40.00,40.00,40.00,40.00,40.00,40.00,40.00,40.00,40.00,40.00,40.00,41.00,41.00,41.00,41.00,49.00,49.00,50.00,50.00,50.00,51.00,52.00]
# for x in range(500):
	# obs.append(random.uniform(0,15))
	# obs.append(random.uniform(18,57))
	# obs.append(random.uniform(60,80))

if len(sys.argv) == 0:
	print "argument list is empty"
	exit(3)
elif len(sys.argv) > 2:
	print "more than two argument"
	exit(4)
# print "arguments length is" + str(len(sys.argv))
longString = ""
for arg in sys.argv:
	longString = arg
obs = [float(x) for x in longString.split(",")]

#convert to array
p = array(obs)

#kmeans returns the centroids, rather than the cutoffs. So what we do is we turn them into a list, sort it, and then take the average between the two. This average should be the cut off, since kmeans groups observations to the nearest centroid.
count = 0

def run_cluster(p):
	j = []
	for i in range(num_run): 
		t = cluster.vq.kmeans2(p,num_cluster,iter=100)
		w = [pow((obs[o] - t[0][t[1][o]]),2) for o in range(len(obs))]
		#w2 = [abs((obs[o] - t[0][t[1][o]])) for o in range(len(obs))]
		t = list(t[0])
		t.sort()
		t = (t,sum(w))
		j.append(t)
	return j
	
for w in range(1):
	counter = 0
	while counter == 0:
		try: 
			j = run_cluster(p)
			w = sorted(j,key = lambda t: t[1])
			j = w[0][0]
			print j[0]
			print j[1]
			counter = 1
		except:
			counter = 0