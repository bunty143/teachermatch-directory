<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!-- 1 -->
<input type="hidden"  name="questionSetIdForCg" id="questionSetIdForCg"/>
<input type="hidden" id="statusUpdateFromTP" name="statusUpdateFromTP" value="1"/>
<input type="hidden" id="statusTeacherId_Global" name="statusTeacherId_Global" value=""/> 
<input type="hidden" id="jobForTeacherIdForSNote" name="jobForTeacherIdForSNote" value=""/>
<input type="hidden" id="doActivity" name="doActivity" value=""/>
<input type="hidden" id="doActivityForOfferReady" name="doActivityForOfferReady"/>
<input type="hidden" id="statusIdForStatusNote" name="statusIdForStatusNote" value=""/>
<input type="hidden" id="fitScoreForStatusNote" name="fitScoreForStatusNote" value=""/>
<input type="hidden" id="secondaryStatusIdForStatusNote" name="secondaryStatusIdForStatusNote" value=""/>
<input type="hidden" id="jobCategoryDiv" name="jobCategoryDiv" value=""/>
<input type="hidden" id="hireFlag" name="hireFlag" value="0"/>
<input type="hidden" id="finalizeStatus" name="finalizeStatus" value="0"/>
<input type="hidden" id="teacherStatusNoteId" name="teacherStatusNoteId" value="0"/>
<input type="hidden" id="jobCategoryFlag" name="jobCategoryFlag" />
<input type="hidden" id="jobCategoryDiv" name="jobCategoryDiv"  value="2"/>
<input type="hidden" id="offerAccepted" name="offerAccepted" value="1"/>
<input type="hidden" id="reqNumber" name="reqNumber" />
<input type="hidden" id="offerReady" name="offerReady" />
<input type="hidden" id="updateStatusHDRFlag" name="updateStatusHDRFlag"/>
<!-- 2 -->
<div  class="modal hide"  id="myModalStatus"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog" style="width: 950px;">
	<div class="modal-content">		
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="hideStatusCGTP()">x</button>
		<h3 id="myModalLabel" style="text-align:center;"><span class="left7" id="status_l_title"><spring:message code="lblStatus"/></span></h3>
	</div>
         <div class="modal-body-cgstatus" style="max-width: 950px;max-height: 100%;padding-top: 0px;padding-left: 0px;padding-right: 0px;padding-bottom: 0px;">		
           <div id="divStatus"style="width:98.4%;">
	       </div>
         </div>
 	<div class="modal-footer">
 		<button class="flatbtn" data-dismiss="modal" aria-hidden="true" style="width:15%;background:#22C589;"  onclick="hideStatusCGTP()"><spring:message code="btnDone"/></button>	
 	</div>
</div>
</div>
</div>
<!-- 3 -->
<div class="modal hide" id="jWTeacherStatusNotesDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">

  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='jWTeacherStatusNotesOnCloseForSLC()'>x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title"><spring:message code="lblStatus"/></span></h3>
 	
		<input type="hidden" id="statusName" name="statusName" value="">
		<input type="hidden" id="statusShortName" name="statusShortName" value="">
		<input type="hidden" id="isEmailTemplateChanged" name="isEmailTemplateChanged" value="0">
		<input type="hidden" id="isEmailTemplateChangedTeacher" name="isEmailTemplateChangedTeacher" value="0">
		
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;">
		<div class="control-group">
			<input type="hidden" name="scoreProvided" id="scoreProvided" />
			<div class='divErrorMsg left2' id='errorStatusNote' style="display: block;"></div>
			
			<div class="row">
				  <div class="control-group span16">               
			 			<div id='hiredDateDiv' style="padding-left:15px; padding-top: 10px; display: none;">
			 			<label><strong><spring:message code="lblHiredDate"/></strong></label><BR/>
							<input type="text" id="setHiredDate" name="setHiredDate" value="${getDateWithoutTime}" maxlength="0" class="help-inline form-control" style="width: 250px;">
			 			</div>
				  </div>
			</div>
				
			<div id="divStatusNoteGrid" style='width:700px;overflow:hidden;' ></div>
			
			<div id="noteMainDiv" class="hide">
			
				<%---------- Gourav :Status wise Dynamic Template [Start]  --%>
			<div id="templateDivStatus" style="display: none;">		     	
		    	<div class="row col-md-12" >
		    	<div class="row col-sm-8" style="max-width:300px;padding-top:10px;">		    	
			    	<label><strong><spring:message code="lblTemplate"/></strong></label>
		        	<br/>
		        	<select class="form-control" id="statuswisesection" onchange="getsectionwiselist(this.value)">
		        		<option value="0"><spring:message code="sltTemplate"/></option>
		        	</select>
		         </div>	
			
		    	</div>
		    	
		    	<div class="row col-md-12" id="showTemplateslist" style="display:none">		    	
		    	<div class="row col-sm-12" style="padding-top:10px;">
		    	<label><strong><spring:message code="lblSubTmpt"/></strong></label>
		        	<br/>
		        	<span id="addTemplateslist"></span>
		        	
		         </div>		        	
					<button class="btn btn-primary hide top26" id="changeTemplateBody" onclick="getTemplateByLIst()"><spring:message code="btnAplyTemplate"/></button>
					<input type="hidden" id="templateId" value=""/>
			</div>
		    </div>
		<%---------- Gourav :Status wise Dynamic Template [END]  --%>
				<div class="row mt5" >
				    	<div class="left16" id='statusNotes' >
				    	<label class=""><spring:message code="lblNote"/><c:if test="${statusNotes eq true}"><span class="required">*</span></c:if>
				    	</label>
				    		<textarea readonly id="statusNotes" name="statusNotes" class="span6" rows="2"   ></textarea>
				    	</div> 
				</div>  
				<iframe id='uploadStatusFileFrameID' name='frmStatusFileFrame' height='0' width='0'  frameborder='0' scrolling='yes' sytle='display:none;'>
				</iframe>
				<form id='frmStatusNote' enctype='multipart/form-data' method='post' target='frmStatusFileFrame' onsubmit="saveStatusNote(0);"  class="form-inline" action='statusNoteUploadServlet.do' accept-charset="UTF-8">
				<input type="hidden" id="statusTeacherId" name="statusTeacherId" value=""/>
				<div style="clear: both"></div>
				<div class="row mt5">  	
						<div class="span4 left16" >
						<div style="clear: both"></div>
				    		<div id='statusNoteFileNames' style="padding-top:12px;">
			        			 <a href='javascript:void(0);' onclick='addStatusNoteFileType();'><img src='images/attach.png'/></a>
			        			 <a href='javascript:void(0);' onclick='addStatusNoteFileType();'><spring:message code="lnkAttachFile"/></a>
			        		</div> 
				    	</div>
				    	<div class="span4" id="showStatusNoteFile"  style="padding-top:4px;">&nbsp;
			        	</div>   
				</div>
				</form>
		 	</div> <!-- Main Div -->
		 	
		 	<div class="row" id="schoolAutoSuggestDivId" style="display: none;">
            	<div class="col-sm-8 col-md-8">
            		
            		<c:if test="${entityType eq 2}">
					  	<label><spring:message code="lblSchoolName"/><span class="required">*</span></label>
		 				<input type="text" id="schoolNameCG" class="form-control" maxlength="100" name="schoolNameCG" style="width: 657px;" placeholder="" onfocus="getSchoolAuto_Hired(this, event, 'divTxtShowData2CG', 'schoolNameCG','schoolIdCG','');"
							onkeyup="getSchoolAuto_Hired(this, event, 'divTxtShowData2CG', 'schoolNameCG','schoolIdCG','');"
							onblur="hideSchoolMasterDiv_djo(this,'schoolIdCG','divTxtShowData2CG'),requisitionNumbers();"	/>
							
						 <div id='divTxtShowData2CG' style=' display:none;' onmouseover="mouseOverChk_djo('divTxtShowData2CG','schoolNameCG')" class='result' ></div>
				 	</c:if>
				 	<input type="hidden" id="schoolIdCG"/>
				 	
		    	</div>
            </div>
            
            <div class="row" id="requisitionNumbersGrid"  style="display: none;">
            		<c:if test="${entityType ne 1}">
            			<label style='padding-top:10px;'><spring:message code="lblPosit/Req"/><span class="required"  id='reqstar' sytle='display:none;'>*</span> <spring:message code="msgHirignACand"/></label>
            			<c:choose>
				        	<c:when test="${DistrictId==3628590}">
				        		<label class="radio inline">
								<input type="radio" name="rdReqSel"  id="rdReqPre" value="1" checked><spring:message code="lblSltPosit/Req"/>
								</label>
								<div class="left16">
 							 		<span id="requisitionNumbers"> </span>
 							 	</div>
 							 	<div class="row mt10">
					            	<div class="left16">
					            		<label class="radio inline">
									 	<input type="radio" name="rdReqSel"  id="rdReqPost" value="2" ><spring:message code="lblEtrNewPost/Req"/>
									 	<input  type="text" id="txtNewReqNo" name="txtNewReqNo" class="form-control" maxlength="50" style="width: 350px;" />
					                 	</label>
							    	</div>
					            </div>
				           	</c:when>
				           	<c:when test="${DistrictId ne 3628590}">
 							 	<span id="requisitionNumbers"> </span>
				           	</c:when>
				       </c:choose>
 					</c:if>
            </div>
               <!-- Strength & Opportunity Div -->
            <div id="strengthOppDiv" style="display: none;"></div>	
            
            <span id="spanOverride" style='display:none;'>            
            			<label class='checkbox inline'>
            				<input type='checkbox' name='chkOverride' id='chkOverride' value='1'/>
            				<spring:message code="lblOverrideStatus"/>
            			</label>			    
            </span>
            
		 	<span id="checkOptions" style='display:none;'>
	            <c:set var="disabledChecked" value=""></c:set>
	            <c:set var="hideView" value=""> </c:set>
			 	<c:if test="${entityType eq 1}">
			 		<c:set var="disabledChecked" value="disabled=\"disabled\""> </c:set>
			 		<c:set var="hideView" value="hide"> </c:set>
			 	</c:if>
			 	<div class="row mt10" id="email_da_sa_div">
			 	
	            	<div class="left16">
	            	
	            		<c:choose>
							<c:when test="${DistrictId == '1200390'}">
								<label style="display: none;">
							 	<input type="checkbox" name="email_da_sa" ${disabledChecked} id="email_da_sa"  value="1"/>
			                 	<spring:message code="msgNotifyAllAssoDistAndSchlAdminOfStatus"/>
			                 	</label>
			                 	<a href='javascript:void(0)' class="${hideView}" style="padding-left: 540px;" onclick="getStatusWiseEmailForAdmin()"><spring:message code="lnkV/Emag"/></a>
							</c:when>
							<c:otherwise>
								<label>
							 	<input type="checkbox" name="email_da_sa" ${disabledChecked} id="email_da_sa"  value="1"/>
			                 <spring:message code="msgNotifyAllAssoDistAndSchlAdminOfStatus"/>
			                 	</label>
			                 	<a href='javascript:void(0)' class="${hideView}" style="padding-left: 90px;" onclick="getStatusWiseEmailForAdmin()"><spring:message code="lnkV/Emag"/></a>
							</c:otherwise>
						</c:choose>
	            	
			    	</div>
	            </div>
            </span>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=655 border=0>
	 		<tr class="left15">
		 		<td width=118 nowrap align=left>
		 		<span id="statusSave" style='display:none;'><button class="btn  btn-large btn-orange"  onclick='saveStatusNote(0)'><spring:message code="btnIn-Progress"/></button>&nbsp;</span>
		 		</td>
		 		<td width=122  nowrap align=left>
		 			<span id="unHire" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(2)'><spring:message code="btnUnHire"/> </button></span>
			 		<span id="unDecline" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(3)'><spring:message code="btnUnDecline"/> </button></span>
			 		<span id="unRemove" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(4)'><spring:message code="btnUnReject"/> </button></span>
		 		</td>
		 		<td width=450  nowrap>
			 		<span id="undo" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(5)'><spring:message code="btnUndo"/></button>&nbsp;&nbsp;</span>
			 		<span id="resend" style='display:none;'><button class="btn  btn-large btn-primary" onclick='updateStatusHDR(6)'><spring:message code="btnResend"/></button>&nbsp;&nbsp;</span>
			 		<span id="waived" style='display:none;'><button class="btn  btn-large btn-primary" onclick='saveStatusNote(7)'>Waived</button>&nbsp;&nbsp;</span>
			 		<input type="hidden" id="statusNotesForStatus" name="statusNotesForStatus" value="${statusNotes}"/>

					<span id="statusFinalize" style='display:none;'><button class="btn  btn-large btn-primary" onclick='saveStatusNote(1)'><spring:message code="btnFinalize"/> <i class="iconlock"></i></button>&nbsp;&nbsp;</span>		 		
			 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='jWTeacherStatusNotesOnClose()'><spring:message code="btnClr"/></button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>
<!-- 4 -->

<div class="modal hide" id="updateStatusModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:720px;">
	<div class="modal-content">
	<div class="modal-header" id="printDataProfileDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='cancelOfferAccepted();'>x</button>
		<h3><spring:message code="lblStatus"/></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div id="updateStatusDiv"  data-spy="scroll" data-offset="50" >
			<spring:message code="msgUpdateStatusinOtherDsit"/>
		</div>	
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:370px;">
	 		<tr>
	 			<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='updateStatusOverrideSkip(1);'>&nbsp;&nbsp;&nbsp;<spring:message code="lblOverwrite"/>&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='updateStatusOverrideSkip(0);'>&nbsp;&nbsp;&nbsp;<spring:message code="btnSkip"/>&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
				<td   nowrap>
		 			<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='cancelUpdateStatus();'><spring:message code="btnClr"/></button>
				</td>
	 		</tr>
	 	</table>
	</div>
</div>
	</div>
</div>

<!-- 5 -->

<div  class="modal hide"  id="myModalViewCompleteTeacherStatusNotes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="viewCompleteTeacherStatusNotesClose()">x</button>
		<h3 id="myModalLabel"><spring:message code="headStatusNotes"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="myModalViewCompleteTeacherStatusNotesInnerText" style="max-height:350px;">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick="viewCompleteTeacherStatusNotesClose()"><spring:message code="btnClose"/></button> 		
 	</div>
  </div>
</div>
</div>

<!-- 6 -->

<div class="modal hide" id="myModalEmailTeacher" >
		<div class="modal-dialog-for-cgmessage">
        <div class="modal-content">		
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='sendOriginalEmailTeacher()'>x</button>
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmailTeacher'></div>
				</div>
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLineTeacher" name="subjectLineTeacher" class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBodyTeacher">
					    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
				        	<textarea rows="5" class="form-control" cols="" id="" name=""></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn"  onclick='sendOriginalEmailTeacher()'><spring:message code="btnClr"/></button>
		 		<button class="btn btn-primary"  onclick='return sendChangedEmailTeacher();' ><spring:message code="btnOk"/></button>
		 	</div>
	    </div>
	</div>
</div>

<!-- 7 -->

<div class="modal hide" id="myModalEmail" >
	<div class="modal-dialog-for-cgmessage">
        <div class="modal-content">	
			<div class="modal-header">
		  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='sendOriginalEmail()'>x</button>
				<h3 id="myModalLabel"><spring:message code="headTm"/></h3>
			</div>
		
			<div  class="modal-body">
				<div class="control-group">
					<div class='divErrorMsg' id='errordivEmail'></div>
				</div>
				<div id='support'>
					<div class="control-group">
						<div class="">
					    	<label><strong><spring:message code="lblSub"/></strong><span class="required">*</span></label>
				        	<input  type="text"  id="subjectLine" name="subjectLine"class="form-control" maxlength="250" />
						</div>
					</div>
				            
			        <div class="control-group">
						<div class="" id="mailBody">
					    	<label><strong><spring:message code="lblMsg"/></strong><span class="required">*</span></label>
				        	<textarea rows="5" class="form-control" cols="" id="" name="" style="margin-top: 0px;"></textarea>
						</div>
					</div>
		 		</div>
		 	</div>
		 	
		 	<div class="modal-footer">
		 		<button class="btn"  onclick='sendOriginalEmail()'><spring:message code="btnClr"/></button>
		 		<button class="btn btn-primary"  onclick='return sendChangedEmail();' ><spring:message code="btnOk"/></button>
		 	</div>
	      </div>
		</div>
	</div>

<!-- 8 -->
<div class="modal hide" id="offerReadyModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog"  style="width: 520px;padding-top:200px; ">
	<div class="modal-content">
	<div class="modal-header" id="offerReadyDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='offerReadyCheck();'>x</button>
		<h3 id="myModalLabel"><span class="left7" id="offer_Ready_title"><spring:message code="lblOfferReady"/></span></h3>
	</div>
	<div class="modal-body" style="max-height:370px;overflow-y:auto">
		<div id="offerReadyDiv"  data-spy="scroll" data-offset="50" >
		</div>	
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:390px;">
	 		<tr>
		 		<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='offerReadyCheck();'>&nbsp;&nbsp;&nbsp;<spring:message code="btnOk"/>&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;
				</td>
	 		</tr>
	 	</table>
	</div>
</div>
	</div>
</div>
<!-- 9 -->
<div class="modal hide"  id="modalUpdateStatusNoteMsg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  style="z-index: 5000;"  data-backdrop="static">
	<div class="modal-dialog">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='closeStatusNoteMsg(0);'>x</button>
		<h3 id="myModalLabel"><spring:message code="lblStatus"/></h3>
	</div>
	<div class="modal-body">		
		<div class="control-group">
			<div id="updateStatusNoteMsg">
			</div>
		</div>
 	</div>
 	<div class="modal-footer">
 		<button class="btn" data-dismiss="modal" aria-hidden="true" onclick='closeUpdateStatusNoteMsg(1);'><spring:message code="btnOk"/></button> 
 		<button class="btn" data-dismiss="modal" aria-hidden="true"  onclick='closeUpdateStatusNoteMsg(0);'><spring:message code="btnClr"/></button> 		
 	</div>
   </div>
  </div>
 </div>
<!-- 10 -->
<div class="modal hide" id="outerStatusModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:720px;">
	<div class="modal-content">
	<div class="modal-header" id="printDataProfileDivHeader">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='outerStatusClose();'>x</button>
		<h3 id="myModalLabel"><span class="left7" id="outer_status_l_title"><spring:message code="lblStatus"/></span></h3>
	</div>
	<div class="modal-body" style="max-height: 450px;overflow-y:auto">
		<div id="OuterStatusDiv"  data-spy="scroll" data-offset="50" >
			<spring:message code="msgYuLocDoesNotAcceToHire"/>
		</div>	
	</div>
	<div class="modal-footer">
	 	<table border=0 style="margin-left:590px;">
	 		<tr>
	 			<td  nowrap>
		 			<button class="btn  btn-large btn-primary" aria-hidden="true" onclick='outerStatusClose();'>&nbsp;&nbsp;&nbsp;Ok&nbsp;&nbsp;&nbsp;</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
	 		</tr>
	 	</table>
	</div>
</div>
	</div>
</div>
<div class="modal hide" id="onlineActivityDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='onlineActivityOnClose()'>x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title"><spring:message code="lblStatus"/></span></h3>
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;">
		<div class="row">
		<div class="col-sm-6 col-md-6">
			 <div class='divErrorMsg' id='errordivnlineActivityquestion' style="display: block;"></div>
		</div>
		</div>
		<div class="control-group">
			<div id="onlineActivityGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=680 border=0>
	 		<tr class="left15">
		 		<td width=680 align=right  nowrap>
				<span id="onlineActivityFinalize" style='display:inline;'><button class="btn  btn-large btn-primary" onclick='saveOnlineActivity(1)'><spring:message code="btnSave"/></button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='onlineActivityOnClose()'><spring:message code="btnClr"/></button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>
<div class="modal hide" id="onlineActivityMSGDiv" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="z-index: 5000;"   data-backdrop="static">
	<div class="modal-dialog" style="width:760px;">
	<div class="modal-content">
	<div class="modal-header">
  		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"  onclick='onlineActivityMSGOnClose()'>x</button>
		<h3 id="myModalLabel" ><span class="left7" id="status_title"><spring:message code="lblStatus"/></span></h3>
	</div>
	<div class="modal-body" style=" max-height: 400px;overflow-y: auto;">
		<div class="row">
		<div class="col-sm-12 col-md-12">
			<spring:message code="msgWeFoundScoreForQues"/>
		</div>
		</div>
		<div class="control-group">
			<div id="onlineActivityGrid"></div>
		</div>
 	</div>
 	<div class="modal-footer" style="padding: 14px 20px 15px;">
	 	<table width=680 border=0>
	 		<tr class="left15">
		 		<td width=680 align=right  nowrap>
				<span id="onlineActivityFinalize" style='display:inline;'><button class="btn  btn-large btn-primary" onclick='saveOnlineActivity(2)'><spring:message code="btnOk"/></button>&nbsp;&nbsp;</span>		 		
		 		<button class="btn btn-large" data-dismiss="modal" aria-hidden="true" onclick='onlineActivityMSGOnClose()'><spring:message code="btnClr"/></button>
		 		</td>
	 		</tr>
	 	</table>
    </div>
  </div>
 </div>
</div>

<script type="text/javascript">//<![CDATA[ 
  var now = new Date();
  var calHired = Calendar.setup({ 
 	 onSelect: function(cal) { calHired.hide() },
 	 max : now,
 	 showTime: true
 	 
 	 });
  	calHired.manageFields("setHiredDate", "setHiredDate", "%m-%d-%Y");
//]]>
$(document).mouseup(function (e)
{
    var container = $(".DynarchCalendar-topCont");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.hide();
    }
}); 
</script>