package tm.controller.teacher;

import static tm.services.district.GlobalServices.IS_PRINT;
import static tm.services.district.GlobalServices.getTeacherOrUserName;
import static tm.services.district.GlobalServices.println;
import static tm.services.teacher.AcceptOrDeclineAjax.getAllEmailId;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.DistrictSpecificApprovalFlow;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.EmployeeMaster;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SchoolInJobOrder;
import tm.bean.SchoolWiseCandidateStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictApprovalGroupsDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.DistrictSpecificApprovalFlowDAO;
import tm.dao.DistrictWiseApprovalGroupDAO;
import tm.dao.EligibilityStatusMasterDAO;
import tm.dao.EligibilityVerificationHistroyDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.EligibilityMasterDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.MD5Encryption;
import tm.services.ONBAjaxNew;
import tm.services.district.ManageStatusAjax;
import tm.services.report.CandidateGridService;
import tm.services.teacher.AcceptOrDeclineAjax;
import tm.services.teacher.AcceptOrDeclineThread;
import tm.servlet.WorkThreadServlet;
import tm.utility.Utility;


/*
*@Author: Ram Nath
*/
@Controller
public class AcceptOrDeclineController {
	String locale = Utility.getValueOfPropByKey("locale");
	
	private static final int MYTHREADS = 1000;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	
	@Autowired
	RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private AcceptOrDeclineAjax acceptOrDeclineAjax;	
	
	@Autowired
	SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	TeacherStatusNotesDAO teacherStatusNotesDAO;
		
	@Autowired
	SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Autowired
	private DistrictSpecificApprovalFlowDAO districtSpecificApprovalFlowDAO;
	
	@Autowired
	private DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private EligibilityVerificationHistroyDAO eligibilityVerificationHistroyDAO;
	
	@Autowired
	private EligibilityStatusMasterDAO eligibilityStatusMasterDAO;
	
	@Autowired
	private EligibilityMasterDAO eligibilityMasterDAO;
	
	@Autowired
	JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	
	@Autowired
	private ONBAjaxNew oNRAjaxNew;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	@Autowired
	private ManageStatusAjax manageStatusAjax; 
	
	
	@RequestMapping(value="/insertManuallySAPDetails.do", method=RequestMethod.GET)
	@ResponseBody
	public String insertManuallySAPDetails(ModelMap map,HttpServletRequest request)
	{	
		if(request.getSession(false)==null){
			request.getSession();
		}
		String teacherstatushistoryforjobId = request.getParameter("teacherStatusHistoryForJobId");
		String errorMSG = request.getParameter("errorMSG");
		String passMSG = request.getParameter("passMSG");
		try {
			passMSG=MD5Encryption.toMD5(passMSG);
			System.out.println("passMSG===="+passMSG);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(teacherstatushistoryforjobId!=null && !teacherstatushistoryforjobId.equalsIgnoreCase("") && errorMSG!=null && !errorMSG.equalsIgnoreCase("") && passMSG!=null && passMSG.equals("31528198109743225ff9d0cf04d1fdd1")){
			Long teacherstatushistoryforjobId1=Long.parseLong(teacherstatushistoryforjobId);
			if(oNRAjaxNew.sendToPeopleSoftONB(new Long[]{teacherstatushistoryforjobId1},errorMSG))
				return "success";
			else
				return "Not inserted successfully";
			
		}
		return "please check your input(teacherStatusHistoryForJobId=<\"teacherStatusHistoryForJobId\">&errorMSG=<\"ErrorMessage\">)";
		
	}
	@RequestMapping(value="/acceptordecline.do", method=RequestMethod.GET)
	public String acceptordecline(ModelMap map,HttpServletRequest request)
	{	
		if(request.getSession(false)==null){
			request.getSession();
		}
		println("Calling acceptordecline.do ..."+request.getParameter("key"));
		String msg="";
		int verifyFlag=0;
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			String offerUrl="";
			
			
			if(key!=null && !key.equals(""))
			{
				//teacherId[0]##JobId[1]##userId[2]##statusFlagAndStatus[3]##AcceptOrDecline[4]##callingControllerText[5]##SchoolId[6]	
				String keyValue=Utility.decodeBase64(key);
				println("keyValue "+keyValue);
				if(keyValue!=null && !keyValue.equalsIgnoreCase(""))
				{
					String arr[]=keyValue.split("##");
					
					String teacherId="";
					if(arr.length > 0)
					{
						teacherId=arr[0];
					}
					
					String jobId="";
					if(arr.length > 1)
					{
						jobId=arr[1];
					}
					
					String userId="";
					if(arr.length > 2)
					{
						userId=arr[2];	
					}
					
					String acceptOrDecline="";
					if(arr.length > 4)
					{
						acceptOrDecline=arr[4];						
					}
					if(arr[5].trim().equalsIgnoreCase("HR Review/Offer")){
						offerUrl=Utility.getValueOfPropByKey("basePath")+"/acceptordeclineconfirm.do?key="+key;
					}
					else if(arr[5].trim().equalsIgnoreCase("Evaluation Complete")){
						offerUrl=Utility.getValueOfPropByKey("basePath")+"/acceptordeclineconfirm.do?key="+key;
					}
					else if(arr[5].trim().equalsIgnoreCase("Offer Made")){
						offerUrl=Utility.getValueOfPropByKey("basePath")+"/acceptordeclineconfirm.do?key="+key;
					}else if(arr[5].trim().equalsIgnoreCase("Recommend for Hire")){
						offerUrl=Utility.getValueOfPropByKey("basePath")+"/acceptordeclineconfirm.do?key="+key;
					}else if(arr[5].trim().equalsIgnoreCase("Preliminary Offer")){
						offerUrl=Utility.getValueOfPropByKey("basePath")+"/acceptordeclineconfirm.do?key="+key;
					}
					
					
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(804800) && arr[5].trim().equalsIgnoreCase("Offer Made") && jobOrder.getJobCategoryMaster().getQuestionSetsForOnboarding()!=null){
						map.addAttribute("loginFlag",1);
						map.addAttribute("emailAddress",teacherDetail.getEmailAddress());
					}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900) && arr[5].trim().equalsIgnoreCase("Recommend for Hire") && jobOrder.getJobCategoryMaster().getQuestionSetsForOnboarding()!=null){
						if(acceptOrDecline.trim().equals("1"))
							map.addAttribute("loginFlag",1);
						else
							map.addAttribute("loginFlag",0);
						map.addAttribute("emailAddress",teacherDetail.getEmailAddress());
					}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806810) && arr[5].trim().equalsIgnoreCase("Preliminary Offer")){
						map.addAttribute("loginFlag",0);
						map.addAttribute("emailAddress",teacherDetail.getEmailAddress());
					}
					else{
						map.addAttribute("loginFlag",0);
						map.addAttribute("emailAddress","");
					}
					map.addAttribute("jobId",jobId);
					map.addAttribute("teacherId",teacherId);
					map.addAttribute("acceptordecline",acceptOrDecline.trim());
					
					if(jobForTeacher!=null){
						if(jobForTeacher.getOfferAccepted()!=null){
							if(acceptOrDecline.trim().equals("1")){
								if(jobForTeacher.getOfferAccepted()){
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
									verifyFlag=2;
								}else{
									msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
									verifyFlag=2;
								}
								}
								if(acceptOrDecline.trim().equals("0")){
									if(jobForTeacher.getOfferAccepted()){
										msg=Utility.getLocaleValuePropByKey("msgYouAlreadyAcceptedPos3", locale);
										verifyFlag=2;
									}else{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
										verifyFlag=2;
									}	
									
								}
							}
							else
							{
									if(acceptOrDecline!=null && acceptOrDecline.equals("1")){
										if(jobOrder.getDistrictMaster().getDistrictId().equals(806900))
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline55", locale)+" "+jobOrder.getJobTitle()+" at location "+jobOrder.getDistrictMaster().getDistrictName()+" "+jobOrder.getDistrictMaster().getAddress()+".";
										else
											msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline5", locale);
										verifyFlag=1;
									}else if(acceptOrDecline!=null && acceptOrDecline.equals("0")){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline6", locale);
										verifyFlag=1;
									}													
							}
					}
				}
			}
			map.addAttribute("offerUrl",offerUrl);
			}
			catch (Exception e) {
				e.printStackTrace();
				// TODO: handle exception
			}
		if(verifyFlag==1){
				map.addAttribute("verifyFlag",1);
				map.addAttribute("msg",msg);
			}else {
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			}
			
			println(msg);					
		return "acceptordecline";
		
		}
	
	
	
	@RequestMapping(value="/acceptordeclineconfirm.do", method=RequestMethod.GET)
	public String acceptOrDeclineConfirm(ModelMap map,HttpServletRequest request)
	{
		IS_PRINT=true;
		if(request.getSession(false)==null){
			request.getSession();
		}
		println("Calling acceptordecline.do ...>>>>>>>>>>>>>>>>"+request.getParameter("key"));
		//teacherId[0]##JobId[1]##userId[2]##statusFlagAndStatus[3]##AcceptOrDecline[4]##callingControllerText[5]##SchoolId[6]	
		String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
		try{
		if(key!=null){
			String keyValue=Utility.decodeBase64(key);
			println("keyValue "+keyValue);
			if(keyValue!=null && !keyValue.equalsIgnoreCase(""))
			{
				String arr[]=keyValue.split("##");
				//Hr Review/Offer Status
				JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(arr[1].trim()), false,false);
				System.out.println("jobId==============="+jobOrder.getDistrictMaster().getDistrictId());
				if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(5304860) && arr[5].trim().equalsIgnoreCase("HR Review/Offer")){
					return getHrReviewOfferConfirmUrl(request,map,arr);
				}
				else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(4218990) && arr[5].trim().equalsIgnoreCase("Evaluation Complete")){
					return getEvaluationCompleteConfirmUrl(request,map,arr);
				}
				else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1201470) && arr[5].trim().equalsIgnoreCase("Offer Made")){
					return getOscalaOfferMadeConfirmUrl(request,map,arr);
				}
				else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(804800) && arr[5].trim().equalsIgnoreCase("Offer Made")){
					return getJeffcoOfferMadeConfirmUrl(request,map,arr);
				}
				else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806900) && arr[5].trim().equalsIgnoreCase("Recommend for Hire")){
					return getAdamsOfferMadeConfirmUrl(request,map,arr);
				}else if(jobOrder!=null && jobOrder.getDistrictMaster().getDistrictId().equals(806810) && arr[5].trim().equalsIgnoreCase("Preliminary Offer")){
					return getAdamsOfferMadeConfirmUrl(request,map,arr);
				}
			}
			
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	private String getHrReviewOfferConfirmUrl(HttpServletRequest request,ModelMap map,String arr[]){
		
		println("Calling getHrReviewOfferConfirmUrl ...>>>>>>>>>>>>>>>>"+request.getParameter("key"));
		String msg="";
		Session statelessSession=null;
		try{
			//SessionFactory factory = jobForTeacherDAO.getSessionFactory();
			statelessSession = sessionFactory.openSession();
			//Transaction txOpen = statelessSession.beginTransaction();
			for(String str:arr)
				println("str========="+str);
				//teacherId[0]##JobId[1]##userId[2]##statusFlagAndStatus[3]##AcceptOrDecline[4]##callingControllerText[5]##SchoolId[6]					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 4)
					{
						acceptOrDecline=arr[4];						
					}
					
					String schoolId=arr[6];
					String statusFlag="";
					String statusId="";
					
					
					try{
					String statusFlagAndStatus[]=arr[3].split("@@");
					
					if(statusFlagAndStatus.length==2)
					{
					statusFlag=statusFlagAndStatus[0];
					statusId=statusFlagAndStatus[1];
					}
					}catch(Exception e){e.printStackTrace();}
					
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					UserMaster userMaster=userMasterDAO.findById(iuserId,false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					SchoolMaster schoolMaster=null;
					if(schoolId!=null && !schoolId.trim().equalsIgnoreCase("") && !schoolId.trim().equalsIgnoreCase("null"))
						schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
					List<StatusMaster> lstStatusMaster=WorkThreadServlet.statusMasters;
					SecondaryStatus secondaryStatus_temp=null;
					StatusMaster statusMaster_temp=null;
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					try{
					if(arr[6]!=null && statusFlag.equals("ss")){
						secondaryStatus_temp=secondaryStatusDAO.findById(Integer.parseInt(statusId),false,false);
					}else{
						statusMaster_temp=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
					}
					}catch(Exception e){e.printStackTrace();}
					
					
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					
	
					if(jobForTeacher!=null){
						//Start Code for already Accept And Decline Offer
							if(jobForTeacher.getOfferAccepted()!=null)
							{
								if(acceptOrDecline.trim().equals("1"))
								{
									if(jobForTeacher.getOfferAccepted())
									{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
									}
									else
									{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
									}
								 }
								if(acceptOrDecline.trim().equals("0"))
								{
									if(jobForTeacher.getOfferAccepted()){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline3", locale);									
									}else{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
									}										
								}
							}
						else
						{	
							//ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
							List<UserMaster> umList=new LinkedList<UserMaster>();											
							if(schoolMaster!=null){
								umList=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
							}
							String allEmailId=getAllEmailId(umList);
							if(allEmailId!=null && !allEmailId.trim().equalsIgnoreCase("")){
								allEmailId+=","+userMaster.getEmailAddress();
							}else{
							allEmailId="";
							allEmailId+=userMaster.getEmailAddress();
							}	
							println("umList==================================================="+umList.size());
							//Start Code for Accept Offer 
							if(acceptOrDecline.equals("1"))
							{
								try{
									println("yesssssssssssss");
										//Offer accepted code for job for teacher
										StatusMaster statusMasterHIRD= findStatusByShortName(lstStatusMaster,"vcomp");
										jobForTeacher.setStatus(statusMasterHIRD);
										jobForTeacher.setStatusMaster(statusMasterHIRD);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMasterHIRD.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setOfferAccepted(true);
										jobForTeacher.setOfferAcceptedDate(new Date());
										/*if(schoolMaster!=null)
											jobForTeacher.setSchoolMaster(schoolMaster);*/
										jobForTeacherDAO.updatePersistent(jobForTeacher);
										//Insert History for teacher (TeacherStatusHistoryForJob)
										try{
										saveStatusNoteAfterAccepOffer(teacherDetail,jobOrder,statusMasterHIRD,userMaster);
										} catch (Exception e) {
												e.printStackTrace();
										}
											//Insert History for teacher (TeacherStatusHistoryForJob)
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											tSHJ.setTeacherDetail(teacherDetail);
											tSHJ.setJobOrder(jobOrder);
											tSHJ.setStatusMaster(statusMasterHIRD);
											tSHJ.setActiontakenByCandidate(true);
											tSHJ.setStatus("S");
											tSHJ.setCreatedDateTime(new Date());
											tSHJ.setUserMaster(userMaster);
											teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									
											//Notification of HR Review/Offer Accepted Send Mail to District 
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);
											loct.setUserMaster(userMaster);
											loct.setEmailId(allEmailId);
											loct.setCallingControllerText("teacherAcceptedMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											loct.setStatelessSession(statelessSession);
											loct.setSubject(arr[5].trim()+" Accepted");
											try {
												new Thread(loct).start();
												/*Runnable worker = loct;
												executor.execute(worker);*/
											} catch (Exception e) {}					
								    }catch(Exception e){
								    	e.printStackTrace();							    	
								    }																					
								    msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline7", locale)+" "+jobOrder.getJobTitle()+". "+Utility.getLocaleValuePropByKey("msgAcceptOrDecline8", locale)+"";
							}
									
							//Start Code for Decline Offer
							else if(acceptOrDecline.equals("0")){
								CandidateGridService cgService=new CandidateGridService();
									Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
									Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
									Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
									List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());									
									List<TeacherStatusNotes> statusNoteList=teacherStatusNotesDAO.getStatusNoteAllList(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());											
									List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategory(jobOrder);
									LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
									Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
								if(schoolMaster!=null && schoolMaster.getSchoolName()!=null && schoolMaster.getSchoolName().trim().equalsIgnoreCase(""))
									districtOrSchoolName=schoolMaster.getSchoolName();
								boolean isSendMail=false;
								if(schoolMaster==null)
								{/*	
									try{
										jobForTeacher.setSchoolMaster(null);
										jobForTeacher.setOfferReady(null);
										jobForTeacher.setOfferAccepted(false);
										jobForTeacher.setOfferAcceptedDate(new Date());
										StatusMaster statusMasterDCLN= findStatusByShortName(lstStatusMaster,"dcln");
										jobForTeacher.setStatus(statusMasterDCLN);
										jobForTeacher.setStatusMaster(statusMasterDCLN);
										jobForTeacherDAO.updatePersistent(jobForTeacher);										
										StatusMaster statusMasterRem= findStatusByShortName(lstStatusMaster,"rem");
										
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterRem,"A");
										if(tSHJ!=null){
											tSHJ.setStatus("I");
											tSHJ.setHiredByDate(null);
											tSHJ.setUpdatedDateTime(new Date());
											teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
										}
										tSHJ= new TeacherStatusHistoryForJob();
										tSHJ.setTeacherDetail(teacherDetail);										
										tSHJ.setJobOrder(jobOrder);
										tSHJ.setStatusMaster(statusMasterDCLN);
										tSHJ.setStatus("A");
										tSHJ.setCreatedDateTime(new Date());
										tSHJ.setUserMaster(userMaster);
										teacherStatusHistoryForJobDAO.makePersistent(tSHJ);	
										isSendMail=true;
									}catch(Exception e){
										e.printStackTrace();
										isSendMail=false;
									}
								*/}else{
										
										try{																						
											//***********************************************start************************************************//											
											int counter=0; 
											for(SecondaryStatus tree: lstTreeStructure)
											{
												if(tree.getSecondaryStatus()==null)
												{
													if(tree.getChildren().size()>0){
														counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
													}
												}
											}
											println("counter==="+counter);
												if(statusNoteList.size()>0)
												for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
													if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("HR Review/Offer"))){
														teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
													}
												}
												
												boolean vComp=false;
												if(historyList.size()>0)
												for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
													if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("HR Review/Offer"))){
														if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
															vComp=true;	
														}
														teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
													}
													
												}
						
													
													historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());										
													println("historyList::::::::::"+historyList.size());
													
													List<String> statusList=new ArrayList<String>();
													if(historyList.size()>0)
													for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
														if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
															statusList=new ArrayList<String>();
															if(teacherStatusHistoryForJob.getStatusMaster()!=null){
																statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
															}else{
																statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
															}
															statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
														}else{
															statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
															if(teacherStatusHistoryForJob.getStatusMaster()!=null){
																statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
															}else{
																statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
															}
															statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
														}
													}
							
							
												for(Map.Entry<Integer, List<String>> entry:statusHistoryMap.entrySet())
													println("key=="+entry.getKey()+" Value==="+entry.getValue());
												println("statusHistoryMap>:::::::::::"+statusHistoryMap.size());
												List<String> statusIds=statusHistoryMap.get(jobForTeacher.getTeacherId().getTeacherId());
												println("statusIds===="+statusIds);
												String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
												println("schoolStatusId===="+schoolStatusId);
												SecondaryStatus secondaryStatusObj=null;
												StatusMaster statusMasterObj=null;
												int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
							                    StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
							                    println("statusMasterMain=="+statusMasterMain.getStatus());

							
										String statusIdMapValue[]=schoolStatusId.split("#");
										println("statusIdMapValue[]==="+statusIdMapValue);
										if(statusIdMapValue[0].equals("0") && statusIdMapValue[1].equals("0")){
											jobForTeacher.setStatusMaster(statusMasterMain);												
											jobForTeacher.setStatus(statusMasterMain);
											jobForTeacher.setSecondaryStatus(null);
										}
										else if(statusIdMapValue[0].equals("0")){
												secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
												jobForTeacher.setSecondaryStatus(secondaryStatusObj);												
												jobForTeacher.setStatus(statusMasterMain);
												jobForTeacher.setStatusMaster(null);
										}else{
												statusMasterObj=statusMasterMap.get(schoolStatusId);
												jobForTeacher.setStatusMaster(statusMasterObj);												
												jobForTeacher.setStatus(statusMasterObj);
												jobForTeacher.setSecondaryStatus(null);																								
										}

							//***********************************************end************************************************//
										
											println("Status=="+jobForTeacher.getStatusMaster()+" status=="+jobForTeacher.getStatus());
											println("Sectatus=="+jobForTeacher.getSecondaryStatus());
											jobForTeacher.setSchoolMaster(null);
											jobForTeacher.setOfferReady(null);
											jobForTeacher.setOfferAccepted(false);
											jobForTeacher.setOfferAcceptedDate(new Date());
											jobForTeacherDAO.updatePersistent(jobForTeacher);
									
										StatusMaster statusMasterDCLN= findStatusByShortName(lstStatusMaster,"dcln");
																		
											////////////////////
											SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
											swcsObj.setStatusMaster(statusMasterDCLN);
											swcsObj.setJobOrder(jobForTeacher.getJobId());
											swcsObj.setDistrictId(userMaster.getDistrictId().getDistrictId());
											swcsObj.setSchoolId(schoolMaster.getSchoolId());
											if(umList.size()>0){
											swcsObj.setUserMaster(umList.get(0));
											}else{
												swcsObj.setUserMaster(userMaster);
											}
											swcsObj.setCreatedDateTime(new Date());
											swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
											swcsObj.setStatus("A");
											swcsObj.setJobForTeacher(jobForTeacher);
											schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
											
											/////////////////////////////////////////////////
											StatusMaster statusMasterHird= findStatusByShortName(lstStatusMaster,"hird");
											//StatusMaster statusMasterRem= findStatusByShortName(lstStatusMaster,"rem");
											
														TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
														tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterHird,"A");
														if(tSHJ!=null){
															tSHJ.setStatus("I");
															tSHJ.setHiredByDate(null);
															tSHJ.setUpdatedDateTime(new Date());
															tSHJ.setActiontakenByCandidate(true);
															teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
														}
														
											isSendMail=true;															
										    }catch(Exception e){
										    	e.printStackTrace();							    	
										    }
										}
										if(isSendMail){
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);
											loct.setUserMaster(userMaster);
											loct.setEmailId(allEmailId);
											loct.setCallingControllerText("teacherDeclinedMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											loct.setStatelessSession(statelessSession);
											loct.setSubject(arr[5].trim()+" declined "+districtOrSchoolName+" by "+getTeacherOrUserName(teacherDetail));
												try {
													new Thread(loct).start();
													/*Runnable worker = loct;
													executor.execute(worker);*/
												} catch (Exception e) {}
												msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline9", locale)+" "+jobOrder.getJobTitle()+".";
										}
									
							}	
							/*executor.shutdown();
							// Wait until all threads are finish
							while (!executor.isTerminated()){}*/
							System.out.println("\nFinished all threads");
						}
					}
				//txOpen.commit();
			}catch(Exception e) {e.printStackTrace();}
		//finally{if(statelessSession!=null){statelessSession.close();}}
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			println(msg);		
		return "acceptordecline";
	}
	
	private String getEvaluationCompleteConfirmUrl(HttpServletRequest request,ModelMap map,String arr[]){
		println("Calling getEvaluationCompleteConfirmUrl ...>>>>>>>>>>>>>>>>"+request.getParameter("key"));
		String msg="";
		Session statelessSession=null;
		try{
			//SessionFactory factory = jobForTeacherDAO.getSessionFactory();
			//statelessSession = factory.openStatelessSession();
			statelessSession = sessionFactory.openSession();
			//Transaction txOpen = statelessSession.beginTransaction();
			for(String str:arr)
				println("str========="+str);
				//teacherId[0]##JobId[1]##userId[2]##statusFlagAndStatus[3]##AcceptOrDecline[4]##callingControllerText[5]##SchoolId[6]					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 4)
					{
						acceptOrDecline=arr[4];						
					}
					
					String schoolId=arr[6];
					String statusFlag="";
					String statusId="";
					
					
					try{
					String statusFlagAndStatus[]=arr[3].split("@@");
					
					if(statusFlagAndStatus.length==2)
					{
					statusFlag=statusFlagAndStatus[0];
					statusId=statusFlagAndStatus[1];
					}
					}catch(Exception e){e.printStackTrace();}
					
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					UserMaster userMaster=userMasterDAO.findById(iuserId,false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					SchoolMaster schoolMaster=null;
					if(schoolId!=null && !schoolId.trim().equalsIgnoreCase("") && !schoolId.trim().equalsIgnoreCase("null"))
						schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
					List<StatusMaster> lstStatusMaster=WorkThreadServlet.statusMasters;
					SecondaryStatus secondaryStatus_temp=null;
					StatusMaster statusMaster_temp=null;
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					try{
					if(arr[6]!=null && statusFlag.equals("ss")){
						secondaryStatus_temp=secondaryStatusDAO.findById(Integer.parseInt(statusId),false,false);
					}else{
						statusMaster_temp=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
					}
					}catch(Exception e){e.printStackTrace();}
					
					
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					
	
					if(jobForTeacher!=null){
						//Start Code for already Accept And Decline Offer
							if(jobForTeacher.getOfferAccepted()!=null)
							{
								if(acceptOrDecline.trim().equals("1"))
								{
									if(jobForTeacher.getOfferAccepted())
									{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
									}
									else
									{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
									}
								 }
								if(acceptOrDecline.trim().equals("0"))
								{
									if(jobForTeacher.getOfferAccepted()){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline3", locale);									
									}else{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
									}										
								}
							}
						else
						{
							//ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
							List<UserMaster> umList=new LinkedList<UserMaster>();											
							if(schoolMaster!=null){
								umList=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
							}
							println("umList==================================================="+umList.size());
							String allEmailId=getAllEmailId(umList);
							if(allEmailId!=null && !allEmailId.trim().equalsIgnoreCase("")){
								allEmailId+=","+userMaster.getEmailAddress();
							}else{
							allEmailId="";
							allEmailId+=userMaster.getEmailAddress();
							}		
							//Start Code for Accept Offer 
							if(acceptOrDecline.equals("1"))
							{
								try{
									println("yesssssssssssss");									
										//Offer accepted code for job for teacher
										StatusMaster statusMasterHIRD= findStatusByShortName(lstStatusMaster,"hird");
										jobForTeacher.setStatus(statusMasterHIRD);
										jobForTeacher.setStatusMaster(statusMasterHIRD);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMasterHIRD.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setOfferAccepted(true);
										jobForTeacher.setOfferAcceptedDate(new Date());
										/*if(schoolMaster!=null)
											jobForTeacher.setSchoolMaster(schoolMaster);*/
										jobForTeacherDAO.updatePersistent(jobForTeacher);
											
										//Insert History for teacher (TeacherStatusHistoryForJob)
										try{
										saveStatusNoteAfterAccepOffer(teacherDetail,jobOrder,statusMasterHIRD,userMaster);
										} catch (Exception e) {
												e.printStackTrace();
										}
											//Insert History for teacher (TeacherStatusHistoryForJob)
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											tSHJ.setTeacherDetail(teacherDetail);
											tSHJ.setJobOrder(jobOrder);
											tSHJ.setStatusMaster(statusMasterHIRD);
											tSHJ.setActiontakenByCandidate(true);
											tSHJ.setStatus("A");
											tSHJ.setCreatedDateTime(new Date());
											tSHJ.setUserMaster(userMaster);
											teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									
											//Notification of HR Review/Offer Accepted Send Mail to District 
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);											
											loct.setUserMaster(userMaster);	
											loct.setStatelessSession(statelessSession);
											if(userMaster.getEntityType()==3){	
											loct.setEmailId(userMaster.getEmailAddress());
											}else{
											loct.setEmailId(allEmailId);
											}
											loct.setCallingControllerText("teacherAcceptedMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											loct.setSubject(arr[5].trim()+" Accepted");
											try {
												new Thread(loct).start();
												/*Runnable worker = loct;
												executor.execute(worker);*/
											} catch (Exception e) {}					
								    }catch(Exception e){
								    	e.printStackTrace();							    	
								    }																					
								    msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline7", locale)+" "+jobOrder.getJobTitle()+". "+Utility.getLocaleValuePropByKey("msgAcceptOrDecline8", locale)+"";
							}
									
							//Start Code for Decline Offer
							else if(acceptOrDecline.equals("0")){
								CandidateGridService cgService=new CandidateGridService();
								Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
								Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
								Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
								List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());									
								List<TeacherStatusNotes> statusNoteList=teacherStatusNotesDAO.getStatusNoteAllList(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());											
								List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategory(jobOrder);
								LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
								Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								
								String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
								if(schoolMaster!=null && schoolMaster.getSchoolName()!=null && schoolMaster.getSchoolName().trim().equalsIgnoreCase(""))
									districtOrSchoolName=schoolMaster.getSchoolName();
								boolean isSendMail=false;
								if(schoolMaster==null)
								{	
									try{
										int counter=0; 
										for(SecondaryStatus tree: lstTreeStructure)
										{
											if(tree.getSecondaryStatus()==null)
											{
												if(tree.getChildren().size()>0){
													counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
												}
											}
										}
										println("counter==="+counter);
											if(statusNoteList.size()>0)
											for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
												if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Evaluation Complete"))){
													teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
												}
											}
											
											boolean eComp=false;
											if(historyList.size()>0)
											for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
												if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Evaluation Complete"))){
													if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("ecomp")){
														eComp=true;	
													}
													teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
												}
												
											}
											
											historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());										
											println("historyList::::::::::"+historyList.size());
											
											List<String> statusList=new ArrayList<String>();
											if(historyList.size()>0)
											for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
												if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
													statusList=new ArrayList<String>();
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}else{
													statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
													if(teacherStatusHistoryForJob.getStatusMaster()!=null){
														statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
													}else{
														statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
													}
													statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
												}
											}
					
					
										for(Map.Entry<Integer, List<String>> entry:statusHistoryMap.entrySet())
											println("key=="+entry.getKey()+" Value==="+entry.getValue());
										println("statusHistoryMap>:::::::::::"+statusHistoryMap.size());
										List<String> statusIds=statusHistoryMap.get(jobForTeacher.getTeacherId().getTeacherId());
										println("statusIds===="+statusIds);
										String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
										println("schoolStatusId===="+schoolStatusId);
										SecondaryStatus secondaryStatusObj=null;
										StatusMaster statusMasterObj=null;
										int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
					                    StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
					                    println("statusMasterMain=="+statusMasterMain.getStatus());

					
										String statusIdMapValue[]=schoolStatusId.split("#");
										println("statusIdMapValue[]==="+statusIdMapValue);
										if(statusIdMapValue[0].equals("0") && statusIdMapValue[1].equals("0")){
											jobForTeacher.setStatusMaster(statusMasterMain);												
											jobForTeacher.setStatus(statusMasterMain);
											jobForTeacher.setSecondaryStatus(null);
										}
										else if(statusIdMapValue[0].equals("0")){
												secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
												jobForTeacher.setSecondaryStatus(secondaryStatusObj);												
												jobForTeacher.setStatus(statusMasterMain);
												jobForTeacher.setStatusMaster(null);
										}else{
												statusMasterObj=statusMasterMap.get(schoolStatusId);
												jobForTeacher.setStatusMaster(statusMasterObj);												
												jobForTeacher.setStatus(statusMasterObj);
												jobForTeacher.setSecondaryStatus(null);																								
										}
										
										
										jobForTeacher.setOfferReady(null);
										jobForTeacher.setSchoolMaster(null);
										jobForTeacher.setOfferAccepted(false);
										jobForTeacher.setOfferAcceptedDate(new Date());
										StatusMaster statusMasterDCLN= findStatusByShortName(lstStatusMaster,"dcln");
										jobForTeacher.setStatus(statusMasterDCLN);
										jobForTeacher.setStatusMaster(statusMasterDCLN);
										jobForTeacher.setLastActivity(statusMasterDCLN.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacherDAO.updatePersistent(jobForTeacher);										
										StatusMaster statusMasterRem= findStatusByShortName(lstStatusMaster,"rem");
										
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterRem,"A");
										if(tSHJ!=null){
											tSHJ.setStatus("I");
											tSHJ.setHiredByDate(null);
											tSHJ.setUpdatedDateTime(new Date());
											teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
										}
										tSHJ= new TeacherStatusHistoryForJob();
										tSHJ.setTeacherDetail(teacherDetail);										
										tSHJ.setJobOrder(jobOrder);
										tSHJ.setActiontakenByCandidate(true);
										tSHJ.setStatusMaster(statusMasterDCLN);
										tSHJ.setStatus("A");
										tSHJ.setCreatedDateTime(new Date());
										tSHJ.setUserMaster(userMaster);
										teacherStatusHistoryForJobDAO.makePersistent(tSHJ);	
										isSendMail=true;
									}catch(Exception e){
										e.printStackTrace();
										isSendMail=false;
									}
								}else{
										
										try{
											
											
								//***********************************************start************************************************//											
											int counter=0; 
											for(SecondaryStatus tree: lstTreeStructure)
											{
												if(tree.getSecondaryStatus()==null)
												{
													if(tree.getChildren().size()>0){
														counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
													}
												}
											}
											println("counter==="+counter);
												if(statusNoteList.size()>0)
												for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
													if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Evaluation Complete"))){
														teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
													}
												}
												
												boolean eComp=false;
												if(historyList.size()>0)
												for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
													if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("ecomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Evaluation Complete"))){
														if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("ecomp")){
															eComp=true;	
														}
														teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
													}
													
												}
						
													
													historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());										
													println("historyList::::::::::"+historyList.size());
													
													List<String> statusList=new ArrayList<String>();
													if(historyList.size()>0)
													for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
														if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
															statusList=new ArrayList<String>();
															if(teacherStatusHistoryForJob.getStatusMaster()!=null){
																statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
															}else{
																statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
															}
															statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
														}else{
															statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
															if(teacherStatusHistoryForJob.getStatusMaster()!=null){
																statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
															}else{
																statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
															}
															statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
														}
													}
							
							
												for(Map.Entry<Integer, List<String>> entry:statusHistoryMap.entrySet())
													println("key=="+entry.getKey()+" Value==="+entry.getValue());
												println("statusHistoryMap>:::::::::::"+statusHistoryMap.size());
												List<String> statusIds=statusHistoryMap.get(jobForTeacher.getTeacherId().getTeacherId());
												println("statusIds===="+statusIds);
												String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
												println("schoolStatusId===="+schoolStatusId);
												SecondaryStatus secondaryStatusObj=null;
												StatusMaster statusMasterObj=null;
												int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
							                    StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
							                    println("statusMasterMain=="+statusMasterMain.getStatus());

							
										String statusIdMapValue[]=schoolStatusId.split("#");
										println("statusIdMapValue[]==="+statusIdMapValue);
										if(statusIdMapValue[0].equals("0") && statusIdMapValue[1].equals("0")){
											jobForTeacher.setStatusMaster(statusMasterMain);												
											jobForTeacher.setStatus(statusMasterMain);
											jobForTeacher.setSecondaryStatus(null);
										}
										else if(statusIdMapValue[0].equals("0")){
												secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
												jobForTeacher.setSecondaryStatus(secondaryStatusObj);												
												jobForTeacher.setStatus(statusMasterMain);
												jobForTeacher.setStatusMaster(null);
										}else{
												statusMasterObj=statusMasterMap.get(schoolStatusId);
												jobForTeacher.setStatusMaster(statusMasterObj);												
												jobForTeacher.setStatus(statusMasterObj);
												jobForTeacher.setSecondaryStatus(null);																								
										}

							//***********************************************end************************************************//
										
											println("Status=="+jobForTeacher.getStatusMaster()+" status=="+jobForTeacher.getStatus());
											println("Sectatus=="+jobForTeacher.getSecondaryStatus());
											jobForTeacher.setSchoolMaster(null);
											jobForTeacher.setOfferReady(null);
											jobForTeacher.setOfferAccepted(false);
											jobForTeacher.setOfferAcceptedDate(new Date());
											jobForTeacherDAO.updatePersistent(jobForTeacher);
									
										StatusMaster statusMasterDCLN= findStatusByShortName(lstStatusMaster,"dcln");
																		
											////////////////////
											SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
											swcsObj.setStatusMaster(statusMasterDCLN);
											swcsObj.setJobOrder(jobForTeacher.getJobId());
											swcsObj.setDistrictId(userMaster.getDistrictId().getDistrictId());
											swcsObj.setSchoolId(schoolMaster.getSchoolId());
											if(umList.size()>0){
												swcsObj.setUserMaster(umList.get(0));
												}else{
													swcsObj.setUserMaster(userMaster);
												}		
											swcsObj.setCreatedDateTime(new Date());
											swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
											swcsObj.setStatus("A");
											swcsObj.setJobForTeacher(jobForTeacher);
											schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
											
											/////////////////////////////////////////////////
											StatusMaster statusMasterHird= findStatusByShortName(lstStatusMaster,"hird");
											//StatusMaster statusMasterRem= findStatusByShortName(lstStatusMaster,"rem");
											
														TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
														tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterHird,"A");
														if(tSHJ!=null){
															tSHJ.setStatus("I");
															tSHJ.setHiredByDate(null);
															tSHJ.setUpdatedDateTime(new Date());
															teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
														}
														
											isSendMail=true;															
										    }catch(Exception e){
										    	e.printStackTrace();							    	
										    }
										}
										if(isSendMail){
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);
											loct.setUserMaster(userMaster);
											if(userMaster.getEntityType()==3){	
											loct.setEmailId(userMaster.getEmailAddress());
											}else{
											loct.setEmailId(allEmailId);
											}											
											loct.setCallingControllerText("teacherDeclinedMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											loct.setStatelessSession(statelessSession);
											loct.setSubject(arr[5].trim()+" "+Utility.getLocaleValuePropByKey("lblDeclined", locale)+" "+districtOrSchoolName+" by "+getTeacherOrUserName(teacherDetail));
												try {
													new Thread(loct).start();	
													/*Runnable worker = loct;
													executor.execute(worker);*/
												} catch (Exception e) {}
												msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline9", locale)+" "+jobOrder.getJobTitle()+".";
										}
									
							}
						/*	executor.shutdown();
							// Wait until all threads are finish
							while (!executor.isTerminated()){}*/
							System.out.println("\nFinished all threads");
						}
					}
					//txOpen.commit();
			}catch(Exception e) {e.printStackTrace();}
			//finally{if(statelessSession!=null){statelessSession.close();}}
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			println(msg);		
		return "acceptordecline";
	}
	
	private String getOscalaOfferMadeConfirmUrl(HttpServletRequest request,ModelMap map,String arr[]){
		println("Calling getOfferMadeConfirmUrl ...>>>>>>>>>>>>>>>>"+request.getParameter("key"));
		String msg="";
		Session statelessSession=null;
		try{
			//SessionFactory factory = jobForTeacherDAO.getSessionFactory();
			//statelessSession = factory.openStatelessSession();
			statelessSession = sessionFactory.openSession();
			//Transaction txOpen = statelessSession.beginTransaction();
			for(String str:arr)
				println("str========="+str);
				//teacherId[0]##JobId[1]##userId[2]##statusFlagAndStatus[3]##AcceptOrDecline[4]##callingControllerText[5]##SchoolId[6]					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 4)
					{
						acceptOrDecline=arr[4];						
					}
					
					String schoolId=arr[6];
					String statusFlag="";
					String statusId="";
					
					
					try{
					String statusFlagAndStatus[]=arr[3].split("@@");
					
					if(statusFlagAndStatus.length==2)
					{
					statusFlag=statusFlagAndStatus[0];
					statusId=statusFlagAndStatus[1];
					}
					}catch(Exception e){e.printStackTrace();}
					
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					UserMaster userMaster=userMasterDAO.findById(iuserId,false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					SchoolMaster schoolMaster=null;
					if(schoolId!=null && !schoolId.trim().equalsIgnoreCase("") && !schoolId.trim().equalsIgnoreCase("null")){
						schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
					}
					if(userMaster.getEntityType()==3){
						schoolMaster=userMaster.getSchoolId();
					}
					List<StatusMaster> lstStatusMaster=WorkThreadServlet.statusMasters;
					SecondaryStatus secondaryStatus_temp=null;
					StatusMaster statusMaster_temp=null;
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					try{
					if(arr[6]!=null && statusFlag.equals("ss")){
						secondaryStatus_temp=secondaryStatusDAO.findById(Integer.parseInt(statusId),false,false);
					}else{
						statusMaster_temp=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
					}
					}catch(Exception e){e.printStackTrace();}
					
					
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					
	
					if(jobForTeacher!=null){
						//Start Code for already Accept And Decline Offer
							if(jobForTeacher.getOfferAccepted()!=null)
							{
								if(acceptOrDecline.trim().equals("1"))
								{
									if(jobForTeacher.getOfferAccepted())
									{
										msg="You have already accepted this position.";
									}
									else
									{
										msg="You have already declined this position. Now you cannot accept it.";
									}
								 }
								if(acceptOrDecline.trim().equals("0"))
								{
									if(jobForTeacher.getOfferAccepted()){
										msg="You have already accepted this position. Now you cannot declined it.";									
									}else{
										msg="You have already declined this position.";
									}										
								}
							}
						else
						{
							//ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
							List<UserMaster> umList=new LinkedList<UserMaster>();	
							if(schoolMaster!=null && userMaster.getEntityType()==2){
								umList.clear();
								umList=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
							}
							if(userMaster!=null && userMaster.getEntityType()==3){
								umList.clear();
								umList=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), userMaster.getSchoolId());
							}
							
							String allEmailId=getAllEmailId(umList);	
							
							
							String[] ccEmailId=null;
							List<String> ccmail=new ArrayList<String>();
							try{
								System.out.println("jobOrder.getDistrictMaster()==="+jobOrder.getDistrictMaster().getDistrictId());
							List<DistrictKeyContact> lstDKC=districtKeyContactDAO.findByContactType(jobOrder.getDistrictMaster(),"HR Staff");
							System.out.println("lstDKC================="+lstDKC.size());
							ccEmailId=new String[lstDKC.size()+1];
							int count=0;
							for(DistrictKeyContact dkc:lstDKC){
								ccEmailId[count]=dkc.getUserMaster().getEmailAddress();
								count++;
								//ccmail.add(dkc.getUserMaster().getEmailAddress());
							}
							
							List<UserMaster> umList1=userMasterDAO.findByByDistrictAndEmail(jobOrder.getDistrictMaster(),"maldonas@osceola.k12.fl.us");
							if(umList1!=null && umList1.size()>0)
								ccEmailId[count]=umList1.get(0).getEmailAddress();
							if(userMaster.getEntityType()==2 && !userMaster.getEmailAddress().equalsIgnoreCase("maldonas@osceola.k12.fl.us"))
								ccEmailId[count]=userMaster.getEmailAddress();
							
							//ccEmailId=lstDKC.toArray(new String[ccmail.size()]);
							}catch(Exception e){e.printStackTrace();ccEmailId=new String[0];}	
							
							//System.out.println("str=========="+Arrays.asList(ccEmailId));
							/*String allEmailId=getAllEmailId(removeDuplicates(umList));							
							if(allEmailId!=null && !allEmailId.trim().equalsIgnoreCase("")){
								allEmailId+=",maldonas@osceola.k12.fl.us,"+userMaster.getEmailAddress();
							}else{
							allEmailId="";
							allEmailId+="maldonas@osceola.k12.fl.us,"+userMaster.getEmailAddress();
							}	*/
							println("umList==================================================="+umList.size());
							//Start Code for Accept Offer 
							if(acceptOrDecline.equals("1"))
							{
								try{
									println("yesssssssssssss");
										//Offer accepted code for job for teacher
										StatusMaster statusMasterHIRD= findStatusByShortName(lstStatusMaster,"vcomp");
										jobForTeacher.setStatus(statusMasterHIRD);
										jobForTeacher.setStatusMaster(statusMasterHIRD);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMasterHIRD.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setOfferAccepted(true);
										jobForTeacher.setOfferAcceptedDate(new Date());
										/*if(schoolMaster!=null)
											jobForTeacher.setSchoolMaster(schoolMaster);*/
										jobForTeacherDAO.updatePersistent(jobForTeacher);
											
										//Insert History for teacher (TeacherStatusHistoryForJob)
										try{
										saveStatusNoteAfterAccepOffer(teacherDetail,jobOrder,statusMasterHIRD,userMaster);
										} catch (Exception e) {
												e.printStackTrace();
										}
											//Insert History for teacher (TeacherStatusHistoryForJob)
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											tSHJ.setTeacherDetail(teacherDetail);
											tSHJ.setJobOrder(jobOrder);
											tSHJ.setStatusMaster(statusMasterHIRD);
											tSHJ.setActiontakenByCandidate(true);
											tSHJ.setStatus("S");
											tSHJ.setCreatedDateTime(new Date());
											tSHJ.setUserMaster(userMaster);
											teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
											//Notification of Offer Made Accepted Send Mail to District 
											
											//for(UserMaster um:umList){
												AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
												loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
												loct.setSchoolId(schoolMaster);
												loct.setCallbytemplete(0);
												loct.setJobId(jobOrder);
												loct.setTeacherId(teacherDetail);
												loct.setUserMaster(userMaster);
												loct.setEmailId(allEmailId);
												loct.setCallingControllerText("teacherAcceptedOfferMadeMail");
												loct.setDistrictId(jobOrder.getDistrictMaster());
												loct.setCcEmailId(ccEmailId);
												loct.setStatelessSession(statelessSession);
												loct.setSubject("Offer Accepted (Job "+jobOrder.getJobId()+"- "+jobOrder.getJobTitle()+")");
												try {
													new Thread(loct).start();
													/*Runnable worker = loct;
													executor.execute(worker);*/
												} catch (Exception e) {}
											//}
								    }catch(Exception e){
								    	e.printStackTrace();							    	
								    }																					
							msg="Congratulations on your decision to accept our conditional offer of employment as a "+jobOrder.getJobTitle()+". We will contact you shortly.";
							}
									
							//Start Code for Decline Offer
							else if(acceptOrDecline.equals("0")){
								CandidateGridService cgService=new CandidateGridService();
									Map<Integer,List<String>> statusHistoryMap=new HashMap<Integer, List<String>>();
									Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
									Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
									List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());									
									List<TeacherStatusNotes> statusNoteList=teacherStatusNotesDAO.getStatusNoteAllList(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());											
									List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategory(jobOrder);
									LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
									Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
								String districtOrSchoolName=jobOrder.getDistrictMaster().getDistrictName();
								if(schoolMaster!=null && schoolMaster.getSchoolName()!=null && schoolMaster.getSchoolName().trim().equalsIgnoreCase(""))
									districtOrSchoolName=schoolMaster.getSchoolName();
								
								boolean isSendMail=false;
								if(schoolMaster==null)
								{/*	
									try{
										jobForTeacher.setSchoolMaster(null);
										jobForTeacher.setOfferReady(null);
										jobForTeacher.setOfferAccepted(false);
										jobForTeacher.setOfferAcceptedDate(new Date());
										StatusMaster statusMasterDCLN= findStatusByShortName(lstStatusMaster,"dcln");
										jobForTeacher.setStatus(statusMasterDCLN);
										jobForTeacher.setStatusMaster(statusMasterDCLN);
										jobForTeacherDAO.updatePersistent(jobForTeacher);										
										StatusMaster statusMasterRem= findStatusByShortName(lstStatusMaster,"rem");
										
										TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
										tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterRem,"A");
										if(tSHJ!=null){
											tSHJ.setStatus("I");
											tSHJ.setHiredByDate(null);
											tSHJ.setUpdatedDateTime(new Date());
											teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
										}
										tSHJ= new TeacherStatusHistoryForJob();
										tSHJ.setTeacherDetail(teacherDetail);										
										tSHJ.setJobOrder(jobOrder);
										tSHJ.setStatusMaster(statusMasterDCLN);
										tSHJ.setStatus("A");
										tSHJ.setCreatedDateTime(new Date());
										tSHJ.setUserMaster(userMaster);
										teacherStatusHistoryForJobDAO.makePersistent(tSHJ);	
										isSendMail=true;
									}catch(Exception e){
										e.printStackTrace();
										isSendMail=false;
									}
								*/}else{
										
										try{																						
											//***********************************************start************************************************//											
											int counter=0; 
											for(SecondaryStatus tree: lstTreeStructure)
											{
												if(tree.getSecondaryStatus()==null)
												{
													if(tree.getChildren().size()>0){
														counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
													}
												}
											}
											println("counter==="+counter);
												if(statusNoteList.size()>0)
												for (TeacherStatusNotes teacherStatusNotes : statusNoteList) {
													if((teacherStatusNotes.getStatusMaster()!=null && teacherStatusNotes.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusNotes.getSecondaryStatus()!=null && teacherStatusNotes.getSecondaryStatus().getSecondaryStatusName().equals("Offer Made"))){
														teacherStatusNotesDAO.makeTransient(teacherStatusNotes);
													}
												}
												
												boolean vComp=false;
												if(historyList.size()>0)
												for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
													if((teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp"))||(teacherStatusHistoryForJob.getSecondaryStatus()!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equals("Offer Made"))){
														if(teacherStatusHistoryForJob.getStatusMaster()!=null && teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equals("vcomp")){
															vComp=true;	
														}
														teacherStatusHistoryForJobDAO.makeTransient(teacherStatusHistoryForJob);
													}
													
												}
						
													
													historyList=teacherStatusHistoryForJobDAO.findByTeacherAndJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId());										
													println("historyList::::::::::"+historyList.size());
													
													List<String> statusList=new ArrayList<String>();
													if(historyList.size()>0)
													for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : historyList) {
														if(statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null){
															statusList=new ArrayList<String>();
															if(teacherStatusHistoryForJob.getStatusMaster()!=null){
																statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
															}else{
																statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
															}
															statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
														}else{
															statusList=statusHistoryMap.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
															if(teacherStatusHistoryForJob.getStatusMaster()!=null){
																statusList.add(teacherStatusHistoryForJob.getStatusMaster().getStatusId()+"##0");
															}else{
																statusList.add("0##"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusId());
															}
															statusHistoryMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(),statusList);
														}
													}
							
							
												for(Map.Entry<Integer, List<String>> entry:statusHistoryMap.entrySet())
													println("key=="+entry.getKey()+" Value==="+entry.getValue());
												println("statusHistoryMap>:::::::::::"+statusHistoryMap.size());
												List<String> statusIds=statusHistoryMap.get(jobForTeacher.getTeacherId().getTeacherId());
												println("statusIds===="+statusIds);
												String schoolStatusId=cgService.getSchoolStatusId(statusIds, statusHistMap, statusNameMap);
												println("schoolStatusId===="+schoolStatusId);
												SecondaryStatus secondaryStatusObj=null;
												StatusMaster statusMasterObj=null;
												int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
							                    StatusMaster statusMasterMain = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
							                    println("statusMasterMain=="+statusMasterMain.getStatus());

							            String[] statusIdMapValue=null;
							            if(schoolStatusId!=null)      
										statusIdMapValue=schoolStatusId.split("#");
							            else
							            statusIdMapValue="0#0".split("#");	
							            
										println("statusIdMapValue[]==="+statusIdMapValue);
										if(statusIdMapValue[0].equals("0") && statusIdMapValue[1].equals("0")){
											jobForTeacher.setStatusMaster(statusMasterMain);												
											jobForTeacher.setStatus(statusMasterMain);
											jobForTeacher.setSecondaryStatus(null);
										}
										else if(statusIdMapValue[0].equals("0")){
												secondaryStatusObj=secondaryStatusMap.get(schoolStatusId);
												jobForTeacher.setSecondaryStatus(secondaryStatusObj);												
												jobForTeacher.setStatus(statusMasterMain);
												jobForTeacher.setStatusMaster(null);
										}else{
												statusMasterObj=statusMasterMap.get(schoolStatusId);
												jobForTeacher.setStatusMaster(statusMasterObj);												
												jobForTeacher.setStatus(statusMasterObj);
												jobForTeacher.setSecondaryStatus(null);																								
										}
							           

							//***********************************************end************************************************//
										
											println("Status=="+jobForTeacher.getStatusMaster()+" status=="+jobForTeacher.getStatus());
											println("Sectatus=="+jobForTeacher.getSecondaryStatus());
											jobForTeacher.setSchoolMaster(null);
											jobForTeacher.setOfferReady(null);
											jobForTeacher.setOfferAccepted(false);
											jobForTeacher.setOfferAcceptedDate(new Date());
											jobForTeacherDAO.updatePersistent(jobForTeacher);
									
										StatusMaster statusMasterDCLN= findStatusByShortName(lstStatusMaster,"dcln");
																		
											////////////////////
											SchoolWiseCandidateStatus swcsObj= new SchoolWiseCandidateStatus();
											swcsObj.setStatusMaster(statusMasterDCLN);
											swcsObj.setJobOrder(jobForTeacher.getJobId());
											swcsObj.setDistrictId(userMaster.getDistrictId().getDistrictId());
											swcsObj.setSchoolId(schoolMaster.getSchoolId());
											if(userMaster.getEntityType()==3){
												swcsObj.setUserMaster(userMaster);
											}
											else if(umList.size()>0){
											swcsObj.setUserMaster(umList.get(0));
											}else{
												swcsObj.setUserMaster(userMaster);
											}
											swcsObj.setCreatedDateTime(new Date());
											swcsObj.setTeacherDetail(jobForTeacher.getTeacherId());
											swcsObj.setStatus("A");
											swcsObj.setJobForTeacher(jobForTeacher);
											schoolWiseCandidateStatusDAO.makePersistent(swcsObj);
											//System.out.println("swcsObjswcsObjswcsObjswcsObjswcsObjswcsObjswcsObjswcsObjswcsObjswcsObjswcsObj======="+swcsObj.getSchoolWiseStatusId());
											
											/////////////////////////////////////////////////
											//StatusMaster statusMasterHird= findStatusByShortName(lstStatusMaster,"hird");
											StatusMaster statusMasterRem= findStatusByShortName(lstStatusMaster,"rem");
											
														/*TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
														tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterHird,"A");
														if(tSHJ!=null){
															tSHJ.setStatus("I");
															tSHJ.setHiredByDate(null);
															tSHJ.setUpdatedDateTime(new Date());
															teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
														}*/
											try{
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											tSHJ=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryForJob(jobForTeacher.getTeacherId(),jobForTeacher.getJobId(),statusMasterRem,"A");
											if(tSHJ!=null){
												tSHJ.setStatus("I");
												tSHJ.setHiredByDate(null);
												tSHJ.setActiontakenByCandidate(true);
												tSHJ.setUpdatedDateTime(new Date());
												teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
											}
											/*tSHJ= new TeacherStatusHistoryForJob();
											tSHJ.setTeacherDetail(teacherDetail);										
											tSHJ.setJobOrder(jobOrder);
											tSHJ.setStatusMaster(statusMasterDCLN);
											tSHJ.setStatus("A");
											tSHJ.setCreatedDateTime(new Date());
											if(userMaster.getEntityType()==3){
												tSHJ.setUserMaster(userMaster);
											}
											else if(umList.size()>0){
												tSHJ.setUserMaster(umList.get(0));
											}else{
												tSHJ.setUserMaster(userMaster);
											}
											teacherStatusHistoryForJobDAO.makePersistent(tSHJ);	*/
											isSendMail=true;
										}catch(Exception e){
											e.printStackTrace();
											isSendMail=false;
										}
														
											isSendMail=true;															
										    }catch(Exception e){
										    	e.printStackTrace();							    	
										    }
										}
										if(isSendMail){
											//for(UserMaster um:umList){
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);
											loct.setUserMaster(userMaster);
											loct.setEmailId(allEmailId);
											loct.setCallingControllerText("teacherDeclinedOfferMadeMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											loct.setCcEmailId(ccEmailId);
											loct.setStatelessSession(statelessSession);
											loct.setSubject("Offer Declined (Job "+jobOrder.getJobId()+"- "+jobOrder.getJobTitle()+") by "+getTeacherOrUserName(teacherDetail));
												try {
													/*Runnable worker = loct;
													executor.execute(worker);*/
													new Thread(loct).start();
												} catch (Exception e) {}
											//}
										msg="We have received your rejection for our conditional offer of employment as a "+jobOrder.getJobTitle()+".";
										}
							}	
							/*executor.shutdown();
							// Wait until all threads are finish
							while (!executor.isTerminated()){}*/
							System.out.println("\nFinished all threads");
						}
					}
				//txOpen.commit();
			}catch(Exception e) {e.printStackTrace();}
			//finally{if(statelessSession!=null){statelessSession.close();}}
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			println(msg);		
		return "acceptordecline";
	}
	
	
	private String getJeffcoOfferMadeConfirmUrl(HttpServletRequest request,ModelMap map,String arr[]){
		println("Calling getEvaluationCompleteConfirmUrl ...>>>>>>>>>>>>>>>>"+request.getParameter("key"));
		String msg="";
		Session statelessSession=null;
		try{
			statelessSession = sessionFactory.openSession();
			for(String str:arr)
				println("str========="+str);
				//teacherId[0]##JobId[1]##userId[2]##statusFlagAndStatus[3]##AcceptOrDecline[4]##callingControllerText[5]##SchoolId[6]					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 4)
					{
						acceptOrDecline=arr[4];						
					}
					
					String schoolId=arr[6];
					String statusFlag="";
					String statusId="";
					
					
					try{
					String statusFlagAndStatus[]=arr[3].split("@@");
					
					if(statusFlagAndStatus.length==2)
					{
					statusFlag=statusFlagAndStatus[0];
					statusId=statusFlagAndStatus[1];
					}
					}catch(Exception e){e.printStackTrace();}
					
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					UserMaster userMaster=userMasterDAO.findById(iuserId,false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					SchoolMaster schoolMaster=null;
					if(schoolId!=null && !schoolId.trim().equalsIgnoreCase("") && !schoolId.trim().equalsIgnoreCase("null"))
						schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
					if(schoolMaster==null && userMaster.getEntityType()==3)
						schoolMaster=userMaster.getSchoolId();
					List<StatusMaster> lstStatusMaster=WorkThreadServlet.statusMasters;
					SecondaryStatus secondaryStatus_temp=null;
					StatusMaster statusMaster_temp=null;
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					try{
					if(arr[6]!=null && statusFlag.equals("ss")){
						secondaryStatus_temp=secondaryStatusDAO.findById(Integer.parseInt(statusId),false,false);
					}else{
						statusMaster_temp=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
					}
					}catch(Exception e){e.printStackTrace();}
					
					
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					
	
					if(jobForTeacher!=null){
						//Start Code for already Accept And Decline Offer
							if(jobForTeacher.getOfferAccepted()!=null)
							{
								if(acceptOrDecline.trim().equals("1"))
								{
									if(jobForTeacher.getOfferAccepted())
									{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
									}
									else
									{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
									}
								 }
								if(acceptOrDecline.trim().equals("0"))
								{
									if(jobForTeacher.getOfferAccepted()){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline3", locale);									
									}else{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
									}										
								}
							}
						else
						{
							List<String> userEmailList=getGroupWiseAllUser(jobOrder,userMaster,schoolMaster);
							String allEmailId=userEmailList.toString().substring(1, (userEmailList.toString().length()-1));
							System.out.println("allEmailId========="+allEmailId);
							//Start Code for Accept Offer 
							if(acceptOrDecline.equals("1"))
							{
								int totalHire=0;
								int expectedHire=0;
								try{
									List<SchoolInJobOrder> listSIJ=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId", jobOrder));
									if(listSIJ!=null && !listSIJ.isEmpty()){
										for(SchoolInJobOrder sijo:listSIJ)
											if(sijo!=null && sijo.getNoOfSchoolExpHires()!=null)
												expectedHire+=listSIJ.get(0).getNoOfSchoolExpHires();
									}else{
										expectedHire=jobOrder.getNoOfExpHires();
									}
									totalHire=teacherStatusHistoryForJobDAO.getTotalHireInJobOrder(jobOrder);
								}catch (Exception e) {
									e.printStackTrace();
								}
								try{
									println("yesssssssssssss");									
										//Offer accepted code for job for teacher
										StatusMaster statusMasterHIRD= findStatusByShortName(lstStatusMaster,"hird");
										jobForTeacher.setStatus(statusMasterHIRD);
										jobForTeacher.setStatusMaster(statusMasterHIRD);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMasterHIRD.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setOfferAccepted(true);
										jobForTeacher.setOfferAcceptedDate(new Date());
										jobForTeacher.setRequisitionNumber(jobOrder.getRequisitionNumber());
										if(schoolMaster!=null)
											jobForTeacher.setSchoolMaster(schoolMaster);
										jobForTeacherDAO.makePersistent(jobForTeacher);
										totalHire+=1;
											
										//Insert History for teacher (TeacherStatusHistoryForJob)
										try{
										saveStatusNoteAfterAccepOffer(teacherDetail,jobOrder,statusMasterHIRD,userMaster);
										} catch (Exception e) {
												e.printStackTrace();
										}
											//Insert History for teacher (TeacherStatusHistoryForJob)
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											tSHJ.setTeacherDetail(teacherDetail);
											tSHJ.setJobOrder(jobOrder);
											tSHJ.setStatusMaster(statusMasterHIRD);
											tSHJ.setActiontakenByCandidate(true);
											tSHJ.setStatus("A");
											tSHJ.setCreatedDateTime(new Date());
											tSHJ.setUserMaster(userMaster);
											teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
									
											//Update job RequisitionNumber
											List<DistrictRequisitionNumbers> listDRN=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),jobOrder.getRequisitionNumber());
											System.out.println("expectedHire===="+expectedHire+"  totalHire==="+totalHire);
											if(listDRN.size()>0 && expectedHire<=totalHire){
												List<JobRequisitionNumbers> listJRN=jobRequisitionNumbersDAO.findJobReqNumbersByJobIdAndDRNumber(listDRN,jobOrder);
												if(listJRN.size()>0){
													JobRequisitionNumbers jrn=listJRN.get(0);
													jrn.setStatus(1);
													jobRequisitionNumbersDAO.makePersistent(jrn);
												}
											}
											
											try{
												TeacherPersonalInfo  tpi=teacherPersonalInfoDAO.findById(tSHJ.getTeacherDetail().getTeacherId(), false, false);
												List<EmployeeMaster> listOBJEMP=employeeMasterDAO.findByCriteria(Restrictions.eq("employeeCode", tpi.getEmployeeNumber()),Restrictions.eq("districtMaster", tSHJ.getJobOrder().getDistrictMaster()));
												if(listOBJEMP!=null && listOBJEMP.size()>0){
													EmployeeMaster objEmp=listOBJEMP.get(0);
													if(objEmp.getEmploymentStatus()!=null && objEmp.getEmploymentStatus().trim().equalsIgnoreCase("A")){
														Map<String,String>  waivedStatus=new LinkedHashMap<String,String>();
														waivedStatus.put("employeeId", objEmp.getEmployeeCode());
														waivedStatus.put("status", "Waived");
														waivedStatus.put("eligibilityId", eligibilityMasterDAO.getEligibilityId(tSHJ.getJobOrder().getDistrictMaster().getDistrictId(),"Employee ID/BISI").toString());
														waivedStatus.put("teacherHistoryId", tSHJ.getTeacherStatusHistoryForJobId().toString());
														waivedStatus.put("empOrSalary","emp");
														waivedStatus.put("checkSession","1");
														oNRAjaxNew.saveNotesONB(waivedStatus);
													}
												}
											}catch(Exception e){e.printStackTrace();}
												
											//Notification of HR Review/Offer Accepted Send Mail to District 
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);											
											loct.setUserMaster(userMaster);	
											loct.setStatelessSession(statelessSession);
											loct.setEmailId(allEmailId);
											loct.setCcEmailId(new String[]{});
											loct.setCallingControllerText("teacherAcceptedOfferMadeJeffcoMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											loct.setSubject("Welcome to Jeffco!");
											try {
												new Thread(loct).start();
											} catch (Exception e) {}					
								    }catch(Exception e){
								    	e.printStackTrace();							    	
								    }																					
								    msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline7", locale)+" "+jobOrder.getJobTitle()+". "+Utility.getLocaleValuePropByKey("msgAcceptOrDecline8", locale)+"";
							}
									
							//Start Code for Decline Offer
							else if(acceptOrDecline.equals("0")){
										try{
											StatusMaster statusMasterWIDRW= findStatusByShortName(lstStatusMaster,"widrw");
											println("Status=="+jobForTeacher.getStatusMaster()+" status=="+jobForTeacher.getStatus());
											println("Sectatus=="+jobForTeacher.getSecondaryStatus());
											jobForTeacher.setStatusMaster(statusMasterWIDRW);												
											jobForTeacher.setStatus(statusMasterWIDRW);
											jobForTeacher.setSecondaryStatus(null);
											jobForTeacher.setSchoolMaster(null);
											jobForTeacher.setOfferReady(null);
											jobForTeacher.setOfferAccepted(false);
											jobForTeacher.setOfferAcceptedDate(new Date());
											jobForTeacherDAO.updatePersistent(jobForTeacher);
										    }catch(Exception e){
										    	e.printStackTrace();							    	
										    }
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);
											loct.setUserMaster(userMaster);
											loct.setEmailId(allEmailId);
											loct.setCallingControllerText("teacherDeclinedOfferMadeJeffcoMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											loct.setStatelessSession(statelessSession);
											loct.setSubject("Applicant Declined");
												try {
													new Thread(loct).start();	
												} catch (Exception e) {}
												msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline9", locale)+" "+jobOrder.getJobTitle()+".";
									
							}
							System.out.println("\nFinished all threads"+msg);
						}
					}
			}catch(Exception e) {e.printStackTrace();}
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			println(msg);		
		return "acceptordecline";
	}
	
	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}	
	
	
	static List<UserMaster> removeDuplicates(List<UserMaster> list) {
		// Remove Duplicates: place them in new list (see above example).
		List<UserMaster> result = new ArrayList<UserMaster>();
		Set<Integer> set = new HashSet<Integer>();
		for (UserMaster item : list) {
		    if (!set.contains(item.getUserId()) && !item.getEmailAddress().equalsIgnoreCase("maldonas@osceola.k12.fl.us")) {
			result.add(item);
			set.add(item.getUserId());
		    }
		}
		set.clear();
		return result;
	    }
	
	private List<String> getGroupWiseAllUser(JobOrder jobOrder, UserMaster userMaster,SchoolMaster schoolMaster){
		List<String> allUserEmail=new ArrayList<String>();
		List<String> emailListDAs=new LinkedList<String>();
		List<String> emailListSAs=new LinkedList<String>();
		List<String> emailListEST=new LinkedList<String>();
		Map<String,String> groupNameToShortNameList=new LinkedHashMap<String,String>();
		if(jobOrder.getEmploymentServicesTechnician()!=null){
			emailListEST.add(jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress());
			emailListEST.add(jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress());
		}
		if(schoolMaster!=null){
			List<UserMaster> umListSAs=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
			for(UserMaster um:umListSAs)
				if(!emailListSAs.contains(um.getEmailAddress())){
					emailListSAs.add(um.getEmailAddress());
				}
		}
		
		if(jobOrder.getSpecialEdFlag()!=null){
			List<DistrictSpecificApprovalFlow> listDSAF=districtSpecificApprovalFlowDAO.findByCriteria(Restrictions.eq("specialEdFlag", jobOrder.getSpecialEdFlag()),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.eq("status", "A"));
			if(listDSAF!=null && listDSAF.size()>0){
				List<String> groupShortName=new ArrayList<String>();
				for(String group:listDSAF.get(0).getApprovalFlow().split("#")){
					if(!group.trim().equals("")){
						groupShortName.add(group.trim());
					}
				}
				if(groupShortName.size()>0){
				List<DistrictWiseApprovalGroup> listDWAG=districtWiseApprovalGroupDAO.findByCriteria(Restrictions.in("groupShortName", groupShortName),Restrictions.eq("status", "A"));
					if(listDWAG!=null && listDWAG.size()>0){
						List<String> groupName=new ArrayList<String>();
						for(DistrictWiseApprovalGroup dwag:listDWAG){
							groupName.add(dwag.getGroupName());
							groupNameToShortNameList.put(dwag.getGroupName(), dwag.getGroupShortName());
						}
						
						
						for(Map.Entry<String,String> entry:groupNameToShortNameList.entrySet())
							System.out.println(entry.getKey()+"      ============    "+entry.getValue());
						
						
						if(groupName.size()>0){
						List<DistrictApprovalGroups> dAGList=districtApprovalGroupsDAO.findByCriteria(Restrictions.in("groupName",groupName),Restrictions.isNull("jobCategoryId"),Restrictions.isNull("jobId"),Restrictions.eq("districtId", jobOrder.getDistrictMaster()));
						//System.out.println("dAGList=============="+dAGList.size());
							if(dAGList.size()>0){
								List<Integer> keyContactId=new ArrayList<Integer>();
								Map<String,DistrictKeyContact> memberToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
									for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
										if(!memberKey.trim().equals(""))
											try{
											keyContactId.add(Integer.parseInt(memberKey));
											//System.out.println("keyId============="+memberKey);
											}catch(NumberFormatException e){}
									}
								}
								if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										memberToKeyContactMap.put(dkc.getKeyContactId().toString(), dkc);
								}
								
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
									for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
										if(!memberKey.trim().equals(""))
											try{
												if(groupNameToShortNameList.get(districtAppGroup.getGroupName()).trim().equalsIgnoreCase("SA")){
												emailListSAs.add(memberToKeyContactMap.get(memberKey).getUserMaster().getEmailAddress());
												}else{
												emailListDAs.add(memberToKeyContactMap.get(memberKey).getUserMaster().getEmailAddress());
												}
											}catch(NumberFormatException e){}
									}
								}
								/*if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										if(!allUserEmail.contains(dkc.getUserMaster().getEmailAddress()))
											allUserEmail.add(dkc.getUserMaster().getEmailAddress());
								}*/
							}
						}
					}
				}
			}
		}
		if(userMaster.getEntityType()==2 && !allUserEmail.contains(userMaster.getEmailAddress())){
			emailListDAs.add(userMaster.getEmailAddress());	
		}
		System.out.println("********************ES*********************"+emailListEST);
		System.out.println("********************SAs********************"+emailListSAs);
		System.out.println("********************DAs********************"+emailListDAs);
		allUserEmail.addAll(emailListDAs);
		allUserEmail.addAll(emailListSAs);
		allUserEmail.addAll(emailListEST);
		println("umList==================================================="+allUserEmail);
		return allUserEmail;
	}
	
	
	/*-------------------------------------------------------------------------*/
	private String getAdamsOfferMadeConfirmUrl(HttpServletRequest request,ModelMap map,String arr[]){
		println("Calling getEvaluationCompleteConfirmUrl ...>>>>>>>>>>>>>>>>"+request.getParameter("key"));
		String msg="";
		Session statelessSession=null;
		try{
			statelessSession = sessionFactory.openSession();
			for(String str:arr)
				println("str=====11===="+str);
				//teacherId[0]##JobId[1]##userId[2]##statusFlagAndStatus[3]##AcceptOrDecline[4]##callingControllerText[5]##SchoolId[6]					
					String teacherId="";
					int iTeacherId=0;
					if(arr.length > 0)
					{
						teacherId=arr[0];
						iTeacherId=Utility.getIntValue(teacherId);
					}
					
					String jobId="";
					int ijobId=0;
					if(arr.length > 1)
					{
						jobId=arr[1];
						ijobId=Utility.getIntValue(jobId);
					}
					
					String userId="";
					int iuserId=0;
					if(arr.length > 2)
					{
						userId=arr[2];	
						iuserId=Utility.getIntValue(userId);
					}
					
					String acceptOrDecline="";
					if(arr.length > 4)
					{
						acceptOrDecline=arr[4];						
					}
					
					String schoolId=arr[6];
					String statusFlag="";
					String statusId="";
					
					
					try{
					String statusFlagAndStatus[]=arr[3].split("@@");
					
					if(statusFlagAndStatus.length==2)
					{
					statusFlag=statusFlagAndStatus[0];
					statusId=statusFlagAndStatus[1];
					}
					}catch(Exception e){e.printStackTrace();}
					
					TeacherDetail teacherDetail= teacherDetailDAO.findById(Integer.parseInt(teacherId), false,false);
					JobOrder jobOrder= jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
					UserMaster userMaster=userMasterDAO.findById(iuserId,false,false);
					JobForTeacher jobForTeacher=jobForTeacherDAO.getJobForTeacherDetails(teacherDetail, jobOrder);
					SchoolMaster schoolMaster=null;
					if(schoolId!=null && !schoolId.trim().equalsIgnoreCase("") && !schoolId.trim().equalsIgnoreCase("null")){
						try{
							schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					//	schoolMaster=schoolMasterDAO.findById(Long.parseLong(schoolId),false,false);
					if(schoolMaster==null && userMaster.getEntityType()==3)
						schoolMaster=userMaster.getSchoolId();
					List<StatusMaster> lstStatusMaster=WorkThreadServlet.statusMasters;
					SecondaryStatus secondaryStatus_temp=null;
					StatusMaster statusMaster_temp=null;
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					try{
					if(arr[6]!=null && statusFlag.equals("ss")){
						secondaryStatus_temp=secondaryStatusDAO.findById(Integer.parseInt(statusId),false,false);
					}else{
						statusMaster_temp=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
					}
					}catch(Exception e){e.printStackTrace();}
					
					
					println(" "+teacherId+" "+jobId+" "+iuserId+" "+schoolId+" "+acceptOrDecline+" "+arr[3]);
					
	
					if(jobForTeacher!=null){
						//Start Code for already Accept And Decline Offer
							if(jobForTeacher.getOfferAccepted()!=null)
							{
								if(acceptOrDecline.trim().equals("1"))
								{
									if(jobForTeacher.getOfferAccepted())
									{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline1", locale);
									}
									else
									{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline2", locale);
									}
								 }
								if(acceptOrDecline.trim().equals("0"))
								{
									if(jobForTeacher.getOfferAccepted()){
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline3", locale);									
									}else{
										msg=Utility.getLocaleValuePropByKey("msgAcceptOrDecline4", locale);
									}										
								}
							}
						else
						{
							List<String> userEmailList=null;
							if(jobOrder.getDistrictMaster().getDistrictId().equals(806810))
								userEmailList=getAllSupportUser(jobOrder, userMaster, schoolMaster,teacherDetail);
							else
							userEmailList=getGroupWiseAllUser(jobOrder,userMaster,schoolMaster);
							
							String allEmailId=userEmailList.toString().substring(1, (userEmailList.toString().length()-1));
							System.out.println("allEmailId========="+allEmailId);
							//Start Code for Accept Offer 
							if(acceptOrDecline.equals("1"))
							{
								try{
									println("yesssssssssssss");									
										//Offer accepted code for job for teacher
										StatusMaster statusMasterHIRD= findStatusByShortName(lstStatusMaster,"vcomp");
										jobForTeacher.setStatus(statusMasterHIRD);
										jobForTeacher.setStatusMaster(statusMasterHIRD);
										jobForTeacher.setUpdatedBy(userMaster);
										jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
										jobForTeacher.setUpdatedDate(new Date());
										jobForTeacher.setLastActivity(statusMasterHIRD.getStatus());
										jobForTeacher.setLastActivityDate(new Date());
										jobForTeacher.setOfferAccepted(true);
										jobForTeacher.setOfferAcceptedDate(new Date());
										jobForTeacher.setRequisitionNumber(jobOrder.getRequisitionNumber());
										if(schoolMaster!=null)
											jobForTeacher.setSchoolMaster(schoolMaster);
										jobForTeacherDAO.updatePersistent(jobForTeacher);
											
											//Insert History for teacher (TeacherStatusHistoryForJob)
											try{
											saveStatusNoteAfterAccepOffer(teacherDetail,jobOrder,statusMasterHIRD,userMaster);
											} catch (Exception e) {
													e.printStackTrace();
											}
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											tSHJ.setTeacherDetail(teacherDetail);
											tSHJ.setJobOrder(jobOrder);
											tSHJ.setStatusMaster(statusMasterHIRD);
											tSHJ.setActiontakenByCandidate(true);
											tSHJ.setStatus("S");
											tSHJ.setCreatedDateTime(new Date());
											tSHJ.setUserMaster(userMaster);
											teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
												
												/*List<RaceMaster> lstRace= null;
												lstRace = raceMasterDAO.findAllRaceByOrder();
												Map<String,RaceMaster> raceMap = new HashMap<String, RaceMaster>();
												for (RaceMaster raceMaster : lstRace) {
													raceMap.put(""+raceMaster.getRaceId(), raceMaster);
												}
												//TMCommonUtil tmCommonUtil=new TMCommonUtil();
												TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(jobForTeacher.getTeacherId().getTeacherId(), false, false);
												try {
													TMCommonUtil.startBackgroundCheck(teacherDetail, teacherPersonalInfo, raceMap, jobOrder.getDistrictMaster());
												} catch (Exception e) {
													e.printStackTrace();
													// TODO: handle exception
												}*/
											
											//Update job RequisitionNumber 
												try{
											List<DistrictRequisitionNumbers> listDRN=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(jobOrder.getDistrictMaster(),jobOrder.getRequisitionNumber());
											if(listDRN.size()>0){
												List<JobRequisitionNumbers> listJRN=jobRequisitionNumbersDAO.findJobReqNumbersByJobIdAndDRNumber(listDRN,jobOrder);
												if(listJRN.size()>0){
													JobRequisitionNumbers jrn=listJRN.get(0);
													jrn.setStatus(1);
													jobRequisitionNumbersDAO.makePersistent(jrn);
												}
											}
											
											
												List<EmployeeMaster> listOBJEMP=employeeMasterDAO.findByCriteria(Restrictions.eq("emailAddress", tSHJ.getTeacherDetail().getEmailAddress()));
												if(listOBJEMP!=null && listOBJEMP.size()>0){
													EmployeeMaster objEmp=listOBJEMP.get(0);
													if(objEmp.getEmploymentStatus()!=null && objEmp.getEmploymentStatus().trim().equalsIgnoreCase("A")){
														Map<String,String>  waivedStatus=new LinkedHashMap<String,String>();
														waivedStatus.put("employeeId", objEmp.getEmployeeCode());
														waivedStatus.put("status", "Waived");
														waivedStatus.put("eligibilityId", eligibilityMasterDAO.getEligibilityId(tSHJ.getJobOrder().getDistrictMaster().getDistrictId(),"Employee ID/BISI").toString());
														waivedStatus.put("teacherHistoryId", tSHJ.getTeacherStatusHistoryForJobId().toString());
														waivedStatus.put("empOrSalary","emp");
														waivedStatus.put("checkSession","1");
														oNRAjaxNew.saveNotesONB(waivedStatus);
													}
												}
											}catch(Exception e){e.printStackTrace();}
												
											//Notification of HR Review/Offer Accepted Send Mail to District 
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);											
											loct.setUserMaster(userMaster);	
											loct.setStatelessSession(statelessSession);
											loct.setEmailId(allEmailId);
											loct.setCcEmailId(new String[]{});
											loct.setCallingControllerText("teacherAcceptedMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											if(jobOrder.getDistrictMaster().getDistrictId().equals(806810))
											loct.setSubject("Welcome to Summit School District!");
											else
											{
												try{
												if(schoolMaster!=null)
													loct.setSubject("Recommend for Hire/"+schoolMaster.getSchoolName()+schoolMaster.getAddress()+schoolMaster.getCityName()+schoolMaster.getZip()+"/"+jobOrder.getJobId());
												else
													loct.setSubject("Recommend for Hire/"+jobOrder.getDistrictMaster().getDistrictName()+jobOrder.getDistrictMaster().getAddress()+jobOrder.getDistrictMaster().getCityName()+jobOrder.getDistrictMaster().getZipCode()+"/"+jobOrder.getJobId());
											}catch(Exception e){}
											}
											try {
												new Thread(loct).start();
											} catch (Exception e) {}					
								    }catch(Exception e){
								    	e.printStackTrace();							    	
								    }	
								    if(jobOrder.getDistrictMaster().getDistrictId().equals(806810))
								    	msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline10", locale)+"</br>"+Utility.getLocaleValuePropByKey("msgAcceptOrDecline11", locale)+"</br>Summit School District</br> P.O. Box 7, Frisco, CO 80443, (970) 368-1000.";
								    else
								    msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline10", locale)+"</br>"+Utility.getLocaleValuePropByKey("msgAcceptOrDecline11", locale)+"</br>"+Utility.getLocaleValuePropByKey("msgAcceptOrDecline12", locale)+"";
							}
									
							//Start Code for Decline Offer
							else if(acceptOrDecline.equals("0")){
										try{
											StatusMaster statusMasterWIDRW= findStatusByShortName(lstStatusMaster,"dcln");
											println("Status=="+jobForTeacher.getStatusMaster()+" status=="+jobForTeacher.getStatus());
											println("Sectatus=="+jobForTeacher.getSecondaryStatus());
											jobForTeacher.setStatusMaster(statusMasterWIDRW);												
											jobForTeacher.setStatus(statusMasterWIDRW);
											jobForTeacher.setSecondaryStatus(null);
											jobForTeacher.setSchoolMaster(null);
											jobForTeacher.setOfferReady(null);
											jobForTeacher.setOfferAccepted(false);
											jobForTeacher.setOfferAcceptedDate(new Date());
											jobForTeacherDAO.updatePersistent(jobForTeacher);
											try{
											//Insert Notes for teacher (TeacherStatusNotes)
											saveStatusNoteAfterAccepOffer(teacherDetail,jobOrder,statusMasterWIDRW,userMaster);
											}catch(Exception e){
												e.printStackTrace();							    	
											}
											
											TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
											tSHJ.setTeacherDetail(teacherDetail);
											tSHJ.setJobOrder(jobOrder);
											tSHJ.setStatusMaster(statusMasterWIDRW);
											tSHJ.setActiontakenByCandidate(true);
											tSHJ.setStatus("A");
											tSHJ.setCreatedDateTime(new Date());
											tSHJ.setUserMaster(userMaster);
											teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
										    }catch(Exception e){
										    	e.printStackTrace();							    	
										    }
										    
											AcceptOrDeclineThread loct = new AcceptOrDeclineThread();
											loct.setAcceptOrDeclineAjax(acceptOrDeclineAjax);
											loct.setSchoolId(schoolMaster);
											loct.setCallbytemplete(0);
											loct.setJobId(jobOrder);
											loct.setTeacherId(teacherDetail);
											loct.setUserMaster(userMaster);
											loct.setEmailId(allEmailId);
											loct.setCallingControllerText("teacherDeclinedMail");
											loct.setDistrictId(jobOrder.getDistrictMaster());
											loct.setStatelessSession(statelessSession);
											if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==806900){
												String subject=manageStatusAjax.getCustomSubjectFromDistrictTemplateForMessage(jobOrder, "Applicant NOT Accept Conditional Job Offer. DA. SA", teacherDetail, schoolMaster);
												loct.setSubject(subject);
											}else
											loct.setSubject("Applicant Declined");
												try {
													new Thread(loct).start();	
												} catch (Exception e) {}
												msg=""+Utility.getLocaleValuePropByKey("msgAcceptOrDecline9", locale)+" "+jobOrder.getJobTitle()+".";
									
							}
							System.out.println("\nFinished all threads"+msg);
						}
					}
			}catch(Exception e) {e.printStackTrace();}
				map.addAttribute("verifyFlag",2);
				map.addAttribute("msgpage",msg);
			println(":::::::::::::::::::::::::>>>>>>>>>>>>>>>>>>>>>>>>       "+msg);		
		return "acceptordecline";
	}
	
	private List<String> getAllSupportUser(JobOrder jobOrder, UserMaster userMaster,SchoolMaster schoolMaster,TeacherDetail teacherDetail){
		List<String> allSAs = new ArrayList<String>();
		List<String> allDAs = new ArrayList<String>();
		List<String> allEmails = new ArrayList<String>();
		List<UserMaster> userMasters= new ArrayList<UserMaster>();
		
		/*Criterion criterion=Restrictions.eq("teacherId", teacherDetail);
		Criterion criterion1=Restrictions.eq("districtId", jobOrder.getDistrictMaster().getDistrictId());
		Criterion criterion2=Restrictions.eq("jobId", jobOrder);
		Criterion criterion3=Restrictions.eq("schoolMaster", schoolMaster);
		//Criterion criterion4=Restrictions.isNotNull("schoolMaster");
		try{
		List<JobForTeacher> jobForTeacher=jobForTeacherDAO.findByCriteria(criterion,criterion1,criterion2,criterion3);
		jobForTeacher.get(0).getSchoolMaster();
		}catch(Exception e){
			e.printStackTrace();
		}*/
		if(schoolMaster!=null){
			List<UserMaster> umListSAs=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
			for(UserMaster um:umListSAs)
				if(!allSAs.contains(um.getEmailAddress())){
					allSAs.add(um.getEmailAddress());
				}
		}
		JobCategoryMaster jobCategoryMaster = new JobCategoryMaster();
		if(jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Support Staff") || jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Coaching") || jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Paraprofessional") || jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute"))
		jobCategoryMaster.setJobCategoryId(jobOrder.getJobCategoryMaster().getJobCategoryId());
		try{
		List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivilege =new ArrayList<JobCategoryWiseStatusPrivilege>();
		jobCategoryWiseStatusPrivilege=jobCategoryWiseStatusPrivilegeDAO.getAllSupportUserIn(jobOrder,jobOrder.getJobCategoryMaster().getJobCategoryId());
		String [] getAllEmail=null;
		if(jobCategoryWiseStatusPrivilege.size()>0)
		getAllEmail=jobCategoryWiseStatusPrivilege.get(0).getEmailNotificationToSelectedDistrictAdmins().split("#");
		List<Integer> userMasterDAId=new ArrayList<Integer>();
		if(getAllEmail!=null && getAllEmail.length>0){
		for(String getAllEmail1:getAllEmail){
			userMasterDAId.add(Integer.parseInt(getAllEmail1));
		}
		}
		if (userMasterDAId.size()>0)
			userMasters=userMasterDAO.findByCriteria(Restrictions.in("userId", userMasterDAId));
		
		for (UserMaster userMaster2 : userMasters) {
			allDAs.add(userMaster2.getEmailAddress());
		}
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("********************SAs********************"+allSAs);
		System.out.println("********************DAs********************"+allDAs);
		allEmails.addAll(allSAs);
		if(!(jobOrder.getDistrictMaster().getDistrictId().equals(806810) && (jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Administrator") || jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Teacher"))))
		allEmails.addAll(allDAs);
		System.out.println("********************allEmails********************"+allEmails);
		return allEmails;
	}
	
	private void saveStatusNoteAfterAccepOffer(TeacherDetail teacherDetail,JobOrder jobOrder,StatusMaster statusMasterHIRD,UserMaster userMaster)
	{
		TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
		teacherStatusNoteObj.setTeacherDetail(teacherDetail);
		teacherStatusNoteObj.setJobOrder(jobOrder);
		teacherStatusNoteObj.setStatusMaster(statusMasterHIRD);
		teacherStatusNoteObj.setSecondaryStatus(null);
		teacherStatusNoteObj.setUserMaster(userMaster);
		System.out.println("::::::::::::::::::::::::::::::::::::::::::::saveStatusNoteAfterAccepOffersaveStatusNoteAfterAccepOffer:::::::::::::::::::::::::::::::::");
		teacherStatusNoteObj.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
		teacherStatusNoteObj.setStatusNotes("Offer Accepted");
		teacherStatusNoteObj.setStatusNoteFileName(null);
		teacherStatusNoteObj.setEmailSentTo(1);
		teacherStatusNoteObj.setCreatedDateTime(new Date());
		teacherStatusNoteObj.setFinalizeStatus(true);
		teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
	}
}
