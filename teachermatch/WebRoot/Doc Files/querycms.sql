-----Rahul Tyagi------------------------------------

ALTER TABLE `cmseventtypemaster` ADD CONSTRAINT FOREIGN KEY ( createdBy) REFERENCES usermaster( (userID  );

ALTER TABLE cmseventformatmaster ADD CONSTRAINT  FOREIGN KEY(eventTypeId) REFERENCES cmseventtypemaster(eventTypeId ),
ADD CONSTRAINT  FOREIGN KEY(createdBy) REFERENCES userMaster.userID );

ALTER TABLE `cmseventchannelmaster` ADD CONSTRAINT  FOREIGN KEY(createdBy) REFERENCES usermaster(userID ),
ADD CONSTRAINT FOREIGN KEY ( eventTypeId ) REFERENCES cmseventtypemaster(eventTypeId);

ALTER TABLE `cmseventschedulemaster` ADD CONSTRAINT FOREIGN KEY(createdBy) REFERENCES usermaster(userID ),
ADD CONSTRAINT FOREIGN KEY ( eventTypeId ) REFERENCES cmseventtypemaster(eventTypeId);


ALTER TABLE `eventdetails` ADD CONSTRAINT FOREIGN KEY(createdBy) REFERENCES usermaster(userID ),
ADD CONSTRAINT FOREIGN KEY( districtId ) REFERENCES districtmaster(districtId),
ADD CONSTRAINT FOREIGN KEY( eventTypeId ) REFERENCES cmseventtypemaster(eventTypeId),
ADD CONSTRAINT FOREIGN KEY( eventFormatId ) REFERENCES cmseventformatMaster(eventFormatId),
ADD CONSTRAINT FOREIGN KEY( eventChannelId) REFERENCES cmseventchannelMaster(eventChannelId),
ADD CONSTRAINT FOREIGN KEY( eventSchedulelId ) REFERENCES cmseventscheduleMaster(eventSchedulelId);

ALTER TABLE `eventschedule` ADD CONSTRAINT FOREIGN KEY(createdBy) REFERENCES usermaster(userID ),
ADD CONSTRAINT FOREIGN KEY ( createdBy ) REFERENCES usermaster(userID),
ADD CONSTRAINT FOREIGN KEY ( eventId ) REFERENCES eventdetails(eventId);

ALTER TABLE `eventdescriptiontemplates` ADD CONSTRAINT FOREIGN KEY(createdBy) REFERENCES usermaster(userID ),
ADD CONSTRAINT FOREIGN KEY ( districtId ) REFERENCES districtMaster(districtId);
 
ALTER TABLE `eventemailmessagetemplates` ADD CONSTRAINT FOREIGN KEY(createdBy) REFERENCES usermaster(userID ),
ADD CONSTRAINT FOREIGN KEY ( districtId ) REFERENCES districtMaster(districtId);

ALTER TABLE `eventparticipantslist` ADD CONSTRAINT FOREIGN KEY(createdBy) REFERENCES usermaster(userID ),
ADD CONSTRAINT FOREIGN KEY ( districtId ) REFERENCES districtMaster(districtId),
ADD CONSTRAINT FOREIGN KEY(eventId) references eventDetails(eventId);

ALTER TABLE `eventfacilitatorslist` ADD CONSTRAINT FOREIGN KEY(createdBy) REFERENCES usermaster(userID ),
ADD CONSTRAINT FOREIGN KEY(districtId) references districtMaster(districtId),
ADD CONSTRAINT FOREIGN KEY(eventId) references eventDetails(eventId),
ADD CONSTRAINT FOREIGN KEY(schoolId) references schoolMaster(schoolId);
