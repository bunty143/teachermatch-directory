	
		$('#errordivspecificquestion').empty();
		var arr =[];
		var jobOrder = {jobId:document.getElementById("jobId").value};
		for(i=1;i<=totalQuestionsList;i++)
		{
			var districtSpecificQuestion = {questionId:dwr.util.getValue("QS"+i+"questionId")};
			var questionTypeShortName = dwr.util.getValue("QS"+i+"questionTypeShortName");
			var questionTypeMaster = {questionTypeId:dwr.util.getValue("QS"+i+"questionTypeId")};
			var qType = dwr.util.getValue("QS"+i+"questionTypeShortName");
			var o_maxMarks = dwr.util.getValue("o_maxMarksS");
			if(qType=='tf' || qType=='slsel' ||  qType=='slsel')
			{
				var optId="";
	
				if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
				{
					optId=$("input[name=QS"+i+"opt]:radio:checked").val();
				}else
				{
					$("#errordivspecificquestion").html("&#149; Please provide responses to all the questions since they are required.<br>");
					cnt_QS++;
					dSPQuestionsErrorCount=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : dwr.util.getValue("QS"+optId+"validQuestion"),
					"jobOrder" : jobOrder,
				});
	
			}else if(qType=='ml' || qType=='sl')
			{
				var insertedText = dwr.util.getValue("QS"+i+"opt");
				if(insertedText!=null && insertedText!="")
				{
					arr.push({ 
						"insertedText"    : insertedText,
						"question"  : dwr.util.getValue("QS"+i+"question"),
						"questionTypeMaster" : questionTypeMaster,
						"questionType" : questionTypeShortName,
						"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
						"jobOrder" : jobOrder
					});
				}else
				{
					$("#errordivspecificquestion").html("&#149; Please provide responses to all the questions since they are required.<br>");
					cnt_QS++;
					dSPQuestionsErrorCount=1;
				}
			}else if(qType=='et')
			{
				var optId="";
	
				if($("input[name=QS"+i+"opt]:radio:checked").length > 0 )
				{
					
					optId=$("input[name=QS"+i+"opt]:radio:checked").val();
				}else
				{
					
					$("#errordivspecificquestion").html("&#149; Please provide responses to all the questions since they are required.<br>");
					dSPQuestionsErrorCount=1;
				}
				
				var insertedText = dwr.util.getValue("QS"+i+"optet");
				var isValidAnswer = dwr.util.getValue("QS"+optId+"validQuestion");
				if(isValidAnswer=="false" && insertedText.trim()==""){
					$("#errordivspecificquestion").html("&#149; Please provide responses to all the questions since they are required.<br>");
					dSPQuestionsErrorCount=1;
				}
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"insertedText"    : insertedText,
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : isValidAnswer,
					"jobOrder" : jobOrder,
				});
			}else if(qType=='rt')
			{
				var optId="";
				var score="";
				var rank="";
				var scoreRank=0;
				var opts = document.getElementsByName("optS");
				var scores = document.getElementsByName("scoreS");
				var ranks = document.getElementsByName("rankS");
				var o_ranks = document.getElementsByName("o_rankS");
				
				var tt=0;
				var uniqueflag=false;
				for(var i = 0; i < opts.length; i++) {
					optId += opts[i].value+"|";
					score += scores[i].value+"|";
					rank += ranks[i].value+"|";
					if(checkUniqueRankForPortfolio(ranks[i]))
					{
						uniqueflag=true;
						break;
					}
	
					if(ranks[i].value==o_ranks[i].value)
					{
						scoreRank+=parseInt(scores[i].value);
					}
					if(ranks[i].value=="")
					{
						tt++;
					}
				}
				if(uniqueflag)
					return;
	
				if(tt!=0)
					optId=""; 
	
				if(optId=="")
				{
					//totalSkippedQuestions++;
					//strike checking
				}
				var totalScore = scoreRank;
				arr.push({ 
					"selectedOptions"  : optId,
					"question"  : dwr.util.getValue("QS"+i+"question"),
					"insertedText"    : insertedText,
					"questionTypeMaster" : questionTypeMaster,
					"questionType" : questionTypeShortName,
					"questionOption" : dwr.util.getValue("qOptS"+optId),
					"districtSpecificPortfolioQuestions" : districtSpecificQuestion,
					"isValidAnswer" : isValidAnswer,
					"jobOrder" : jobOrder,
					"optionScore"      : score,
					"totalScore"       : totalScore,
					"insertedRanks"    : rank,
					"maxMarks" :o_maxMarks
				});
			}
		}
		//alert('arr.length::::'+arr.length);
		//alert('totalQuestionsList::'+totalQuestionsList)
		if(arr.length==totalQuestionsList)
		{
			//sekharalert('Set Now')
			PFCertifications.setDistrictQPortfoliouestions(arr,{ 
				async: false,
				errorHandler:handleError,
				callback: function(data){
				if(data!=null)
				{
					//alert('return');
					
					arr =[];
				}
	
			}
			});	
		}else
		{
			$("#errordivspecificquestion").html("&#149; Please provide responses to all the questions since they are required.<br>");
			cnt_QS++;
			dSPQuestionsErrorCount=1;
		}
	