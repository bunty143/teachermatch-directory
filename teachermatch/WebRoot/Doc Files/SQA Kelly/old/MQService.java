package tm.services.mq;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.datacontract.schemas._2004._07.KSN_Entities_Data_KSNData_TeacherMatch.TalentDetail;
import org.datacontract.schemas._2004._07.KSN_Entities_Request.ExternalRequestBase;
import org.datacontract.schemas._2004._07.KSN_Entities_Request_KSNData.CheckForTalentEmailAddressRequest;
import org.datacontract.schemas._2004._07.KSN_Entities_Response_KSNData.CheckForTalentEmailAddressResponse;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.tempuri.IKSNDataServiceProxy;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.TalentKSNDetail;
import tm.bean.TeacherDetail;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.master.ApplitrackDistricts;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.mq.MQEvent;
import tm.bean.user.UserMaster;
import tm.dao.JobForTeacherDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.report.CandidateGridService;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class MQService {
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private MessageToTeacherDAO messageToTeacherDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private MQEventDAO mqEventDAO;
	
	@Autowired
	private CandidateGridService candidateGridService;
	
	String locale = Utility.getValueOfPropByKey("locale");
	
	public  void statusNote(HttpServletRequest request,UserMaster userMaster, JobForTeacher jobForTeacher,MQEvent mqEvent,String note,ApplicationContext context0){
		System.out.println("::::::::::::::statusNote:::::::::::::::");
		try{
			teacherStatusNotesDAO = (TeacherStatusNotesDAO)context0.getBean("teacherStatusNotesDAO"); 
			TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
			teacherStatusNoteObj.setTeacherDetail(jobForTeacher.getTeacherId());
			teacherStatusNoteObj.setJobOrder(jobForTeacher.getJobId());
			if(mqEvent.getStatusMaster()!=null){
				teacherStatusNoteObj.setStatusMaster(mqEvent.getStatusMaster());
			}else if(mqEvent.getSecondaryStatus()!=null){
				teacherStatusNoteObj.setSecondaryStatus(mqEvent.getSecondaryStatus());
			}
			teacherStatusNoteObj.setUserMaster(userMaster);
			teacherStatusNoteObj.setStatusNotes(note);
			teacherStatusNoteObj.setEmailSentTo(0);
			teacherStatusNoteObj.setFinalizeStatus(true);
			teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
			//System.out.println(teacherStatusNoteObj.getTeacherStatusNoteId()+":::::::::AfterInsert Note::::::::"+noteObj.getTeacherStatusNoteId());
		}catch(Exception ee){
			ee.printStackTrace();
		}
	}
	
	
	
	public  void sendERegMailToCand(HttpServletRequest request,UserMaster userMaster, JobForTeacher jobForTeacher,MQEvent mqEvent,String workFlow){
		System.out.println("::::::::::::::sendERegMailToCand:::filePathEReg:: new===:::::");
		try{
	    	 DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
	    	 String serverPath ="/usr/share/tomcat8/webapps/ROOT/"; //request.getScheme()+"://"+request.getServerName()+""+request.getContextPath()+"/";
		     String locale = Utility.getValueOfPropByKey("locale");
		     String mailContent = MailText.getERegContent(request,userMaster , jobForTeacher,workFlow);
		     String emailto =jobForTeacher.getTeacherId().getEmailAddress(); //"sandeep.yadav@netsutra.com";
		     ServletContext context0 = request.getSession().getServletContext();
		     ApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(context0);
		     Object myDao = context.getBean("myMailSender");
		     EmailerService emailerService = (EmailerService)myDao;
		     List<String> bcList=new ArrayList<String>();
		     dsmt.setLstBcc(bcList);
		     dsmt.setFilepath(serverPath+Utility.getLocaleValuePropByKey("fileEReg", locale));
		     dsmt.setEmailerService(emailerService);
		     dsmt.setMailfrom("sandeep.yadav@netsutra.com");
		     dsmt.setMailto(emailto);
		     if(workFlow.equalsIgnoreCase("1"))
		    	 dsmt.setMailsubject("Kelly Educational Staffing=?utf-8?q?=c2=ae=?= (KES=?utf-8?q?=c2=ae=?=) eRegistration Process - ACTION REQUIRED ");   // Utility.getLocaleValuePropByKey("msgSubjectEReg", locale)
		     else if(workFlow.equalsIgnoreCase("2"))
		    	 dsmt.setMailsubject("Kelly Educational Staffing=?utf-8?q?=c2=ae=?= (KES=?utf-8?q?=c2=ae=?=) eRegistration Process - Step 2 - ACTION REQUIRED ");   // Utility.getLocaleValuePropByKey("msgSubjectEReg", locale)
		     else if(workFlow.equalsIgnoreCase("3"))
		    	 dsmt.setMailsubject("Kelly Educational Staffing=?utf-8?q?=c2=ae=?= (KES=?utf-8?q?=c2=ae=?=) eRegistration Process - Step 3 - ACTION REQUIRED ");   // Utility.getLocaleValuePropByKey("msgSubjectEReg", locale)

		     dsmt.setMailcontent(mailContent);
		     dsmt.start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void statusUpdate(JobForTeacher jft,StatusMaster statusMaster,SecondaryStatus secondaryStatus,UserMaster userMaster,ApplicationContext context0,String statusNote,boolean isNote){
		System.out.println(":::::::::::::::::::::statusUpdate::::: ===|=|>>:::::::::::::");
		CandidateGridService cgService=new CandidateGridService();
		 List<TeacherStatusHistoryForJob> historyForJoblist=null;
		 TeacherStatusHistoryForJob historyForJob=null;
		 TeacherDetail teacherDetail=jft.getTeacherId();
		 JobOrder jobOrder=jft.getJobId();
		 try{
			 if(isNote){
				boolean selectedstatus=false;
				boolean selectedSecondaryStatus=false;
				try{
				    if(jft.getStatus()!=null && statusMaster!=null){
				        selectedstatus=cgService.selectedStatusCheck(statusMaster,jft.getStatus());
				    }
				}catch(Exception e){
				    e.printStackTrace();
				}
				try{
				    if(secondaryStatus!=null && jft.getStatus()!=null &&  statusMaster!=null){// && (jobForTeacherObj.getStatus().getStatusShortName().equals("scomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("ecomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("vcomp"))
				        if(jft.getSecondaryStatus()!=null){
				            selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatus,jft.getSecondaryStatus());
				            if(selectedSecondaryStatus==false){
				                selectedstatus=false;
				            }
				        }
				    }else if(jft.getSecondaryStatus()!=null && secondaryStatus!=null && statusMaster==null){
				        if(jft.getStatus()!=null){
				            if(cgService.priSecondaryStatusCheck(jft.getStatus(),secondaryStatus)){
				                selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jft.getSecondaryStatus());
				            }
				        }
				    }else if(jft.getSecondaryStatus()==null && secondaryStatus!=null && statusMaster==null){
				        if(jft.getStatus()!=null){
				            if(cgService.priSecondaryStatusCheck(jft.getStatus(),secondaryStatus)){
				                selectedSecondaryStatus=true;
				            }
				        }
				    }
				}catch(Exception e){
				    e.printStackTrace();
				}
				System.out.println("::::::selectedstatus::::::::;;>>>>>>>>>->>>>>>>>>>>>>>>>>>;>>"+selectedstatus);
				System.out.println(":::::::;;selectedSecondaryStatus::::::::;;;>>"+selectedSecondaryStatus);
				teacherStatusHistoryForJobDAO = (TeacherStatusHistoryForJobDAO)context0.getBean("teacherStatusHistoryForJobDAO"); 
				jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO"); 
				historyForJoblist=teacherStatusHistoryForJobDAO.findByTeacherStatusHistoryByStatus(jft.getTeacherId(),jft.getJobId(),statusMaster,secondaryStatus);
				System.out.println("historyForJoblist::::"+historyForJoblist.size());
				if(statusMaster!=null){
					if(selectedstatus){
						jft.setStatusMaster(statusMaster);
						jft.setStatus(statusMaster);
						jobForTeacherDAO.updatePersistent(jft);
					}
						    //updating History Table	
				if(historyForJoblist!=null && historyForJoblist.size()>0){
				}else{
					historyForJob=new TeacherStatusHistoryForJob();
					historyForJob.setJobOrder(jobOrder);
					historyForJob.setTeacherDetail(teacherDetail);
					historyForJob.setStatus("S");
						if(statusMaster!=null){
							historyForJob.setStatusMaster(statusMaster);
						}else{
							historyForJob.setSecondaryStatus(secondaryStatus);
						}
						historyForJob.setUserMaster(userMaster);
						historyForJob.setCreatedDateTime(new Date());
						teacherStatusHistoryForJobDAO.makePersistent(historyForJob);
					}	
				}else if(secondaryStatus!=null){
					if(selectedSecondaryStatus){
						jft.setStatusMaster(null);
						jft.setSecondaryStatus(secondaryStatus);
						jobForTeacherDAO.updatePersistent(jft);
					}
					//updating History Table	
				if(historyForJoblist!=null && historyForJoblist.size()>0){
				}else{
					historyForJob=new TeacherStatusHistoryForJob();
					historyForJob.setJobOrder(jobOrder);
					historyForJob.setTeacherDetail(teacherDetail);
					historyForJob.setStatus("S");
							historyForJob.setSecondaryStatus(secondaryStatus);
							historyForJob.setUserMaster(userMaster);
							historyForJob.setCreatedDateTime(new Date());
							teacherStatusHistoryForJobDAO.makePersistent(historyForJob);
					  }	
				 }
			 }
			try{
				if(statusNote!=null){
					teacherStatusNotesDAO = (TeacherStatusNotesDAO)context0.getBean("teacherStatusNotesDAO"); 
					TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
					teacherStatusNoteObj.setTeacherDetail(jft.getTeacherId());
					teacherStatusNoteObj.setJobOrder(jft.getJobId());
					if(statusMaster!=null && secondaryStatus==null){
						teacherStatusNoteObj.setStatusMaster(statusMaster);
					}else if(secondaryStatus!=null){
						teacherStatusNoteObj.setSecondaryStatus(secondaryStatus);
					}
					teacherStatusNoteObj.setUserMaster(userMaster);
					teacherStatusNoteObj.setStatusNotes(statusNote);
					teacherStatusNoteObj.setEmailSentTo(0);
					teacherStatusNoteObj.setFinalizeStatus(true);
					teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
				}
			}catch(Exception ee){
				ee.printStackTrace();
			}	
		 }catch(Exception e){
			 e.printStackTrace();
		 }
	}
	
	public void saveMessageToTeacher(TeacherDetail teacherDetail,String statusNote,UserMaster userMaster,ApplicationContext context0,String subject){
		System.out.println(":::::::::::::::::::::saveMessageToTeacher::::: =======:::::::::::::");
			try{
				if(statusNote!=null){
					messageToTeacherDAO = (MessageToTeacherDAO)context0.getBean("messageToTeacherDAO"); 
					MessageToTeacher messageToTeacher = new MessageToTeacher();
					messageToTeacher.setCreatedDateTime(new Date());
					messageToTeacher.setMessageSend(statusNote);
					messageToTeacher.setTeacherId(teacherDetail);
					messageToTeacher.setMessageSubject(subject);
					messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
					messageToTeacher.setEntityType(userMaster.getEntityType());
					messageToTeacher.setSenderId(userMaster);
					messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
					messageToTeacherDAO.makePersistent(messageToTeacher);

				}
			}catch(Exception ee){
				ee.printStackTrace();
			}	
	}
	
	/**
	 * 
	 * @param mqEvent
	 * @param statusNote
	 * @param userMaster
	 * @param context0
	 * @param subject
	 * @param jobOrder
	 * Date Nov 16, 2015
	 */
	public void saveTeacherStatusNotes(MQEvent mqEvent,String statusNote,UserMaster userMaster,ApplicationContext context0,String subject,JobOrder jobOrder){
		System.out.println("Calling MQService => saveTeacherStatusNotes  ... Under guidence of Sandeep ");
			try{
				TeacherDetail teacherDetail=mqEvent.getTeacherdetail();
				if(statusNote!=null && jobOrder!=null)
				{
					
					teacherStatusNotesDAO = (TeacherStatusNotesDAO)context0.getBean("teacherStatusNotesDAO"); 
					
					TeacherStatusNotes tsDetailsObj = new TeacherStatusNotes();
					DistrictMaster districtMaster=null;
					if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
						districtMaster=jobOrder.getDistrictMaster();
					
					tsDetailsObj.setTeacherDetail(teacherDetail);
					tsDetailsObj.setJobOrder(jobOrder);
					tsDetailsObj.setUserMaster(userMaster);
					
					if(mqEvent!=null && mqEvent.getStatusMaster()!=null)
						tsDetailsObj.setStatusMaster(mqEvent.getStatusMaster());
					
					if(mqEvent!=null && mqEvent.getSecondaryStatus()!=null)
						tsDetailsObj.setSecondaryStatus(mqEvent.getSecondaryStatus());
					
					if(districtMaster!=null)
						tsDetailsObj.setDistrictId(districtMaster.getDistrictId());
					
					if(statusNote!=null)
						tsDetailsObj.setStatusNotes(subject+"</BR>"+statusNote);
					
					tsDetailsObj.setEmailSentTo(0);

					tsDetailsObj.setTeacherAssessmentQuestionId(null);
					tsDetailsObj.setFinalizeStatus(true);
					tsDetailsObj.setCreatedDateTime(new Date());
					
					teacherStatusNotesDAO.makePersistent(tsDetailsObj);
				}
			}catch(Exception ee){
				ee.printStackTrace();
			}	
	}
	
	public void statusUpdateForKellyWorkFlowNodes(String nodeName,TeacherDetail teacherDetail){
		
		System.out.println(":::::::::::::::::::statusUpdateForKellyWorkFlowNodes::::::::::::::::::"+nodeName);
		try {
			ApplicationContext context0 =  WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
			headQuarterMasterDAO = (HeadQuarterMasterDAO)context0.getBean("headQuarterMasterDAO");
			jobForTeacherDAO = (JobForTeacherDAO)context0.getBean("jobForTeacherDAO");
			userMasterDAO = (UserMasterDAO)context0.getBean("userMasterDAO");
			secondaryStatusDAO = (SecondaryStatusDAO)context0.getBean("secondaryStatusDAO");
			teacherStatusHistoryForJobDAO = (TeacherStatusHistoryForJobDAO) context0.getBean("teacherStatusHistoryForJobDAO");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(1, false, false);
		UserMaster userMaster = userMasterDAO.findById(1, false, false);
		Map<Integer,StatusMaster> parentStatus = new HashMap<Integer,StatusMaster>();
		Map<Integer,SecondaryStatus> parentSecStatus = new HashMap<Integer,SecondaryStatus>();
		List<JobForTeacher> kellyAppliedJobs = new ArrayList<JobForTeacher>();

		List<JobOrder> listJobs = new ArrayList<JobOrder>();
		List<JobCategoryMaster> jobCategories = new ArrayList<JobCategoryMaster>();
		kellyAppliedJobs = jobForTeacherDAO.findByTeacherIdHQAndBranch(teacherDetail, headQuarterMaster, null);
		
		 SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
		 StatelessSession statelesSsession = sessionFactory.openStatelessSession();
	     Transaction txOpen = statelesSsession.beginTransaction();
		
		if(kellyAppliedJobs.size()>0){
			Map<String,TeacherStatusHistoryForJob> applyHistForJobsMap = new HashMap<String, TeacherStatusHistoryForJob>();
			List<TeacherStatusHistoryForJob> applyHistForJobs = teacherStatusHistoryForJobDAO.findJobsHistoryByTeachers(teacherDetail);
			if(applyHistForJobs!=null && applyHistForJobs.size()>0)
				for(TeacherStatusHistoryForJob tsjb : applyHistForJobs){
					if(tsjb.getSecondaryStatus()!=null)
						applyHistForJobsMap.put(tsjb.getTeacherDetail().getTeacherId()+"###"+tsjb.getJobOrder().getJobId()+"###"+tsjb.getSecondaryStatus().getSecondaryStatusId(), tsjb);
					else if(tsjb.getStatusMaster()!=null)
						applyHistForJobsMap.put(tsjb.getTeacherDetail().getTeacherId()+"###"+tsjb.getJobOrder().getJobId()+"###"+tsjb.getStatusMaster().getStatusId(), tsjb);
				}
			
			if(nodeName!=null && nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgHired2", locale))){
				System.out.println("::Hired Section:::::::::::::::::::::::::::::::::::::::");
				JobForTeacher jobForTeacher=null;
				for(JobForTeacher jft : kellyAppliedJobs){
					System.out.println("jft:::::::::::::::::::::::::::::::::::::::::::::"+jft.getJobId().getJobId());
					jobForTeacher=jft;
					break;
				}
				
				if(jobForTeacher!=null){
					System.out.println("jobForTeacher::::::::Hired");
					StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
					jobForTeacher.setStatus(statusMaster);
					jobForTeacher.setStatusMaster(statusMaster);
					statelesSsession.update(jobForTeacher);
					if(applyHistForJobsMap.get(jobForTeacher.getTeacherId().getTeacherId()+"###"+jobForTeacher.getJobId().getJobId()+"###"+statusMaster.getStatusId())==null){
						TeacherStatusHistoryForJob tshj = new TeacherStatusHistoryForJob();
						tshj.setJobOrder(jobForTeacher.getJobId());
						tshj.setSecondaryStatus(null);
						tshj.setStatus("A");
						tshj.setStatusMaster(statusMaster);
						tshj.setTeacherDetail(jobForTeacher.getTeacherId());
						tshj.setCreatedDateTime(new Date());
						tshj.setUserMaster(userMaster);
						statelesSsession.insert(tshj);
					}
				}
			}else if(nodeName!=null && (nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblInElig", locale))|| nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblInInlig", locale)))){
				System.out.println("::::::lblInElig :::::::::::::::::lblInInlig::::::::::::::::::::::");
				for(JobForTeacher jft : kellyAppliedJobs){
					StatusMaster status = null;
					if(nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblInElig", locale))){
						status=WorkThreadServlet.statusMap.get("ielig");
					}else{
						status=WorkThreadServlet.statusMap.get("iielig");
					}
					jft.setStatus(status);
					jft.setStatusMaster(status);
					statelesSsession.update(jft);
					
					if(status!=null){
						if(applyHistForJobsMap.get(jft.getTeacherId().getTeacherId()+"###"+jft.getJobId().getJobId()+"###"+status.getStatusId())==null){

							TeacherStatusHistoryForJob tshj = new TeacherStatusHistoryForJob();
							tshj.setJobOrder(jft.getJobId());
							if(nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblInElig", locale))){
								status=WorkThreadServlet.statusMap.get("ielig");
							}else{
								status=WorkThreadServlet.statusMap.get("iielig");
							}
							tshj.setStatus("A");
							tshj.setStatusMaster(status);
							tshj.setTeacherDetail(jft.getTeacherId());
							tshj.setCreatedDateTime(new Date());
							tshj.setUserMaster(userMaster);
							statelesSsession.insert(tshj);
						}
					}
				}
			}else if(nodeName!=null && (nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblScreening", locale)))){
				System.out.println("::::::lblScreening ::::::::::::::::::::::::::::::::::::::");
				for(JobForTeacher jft : kellyAppliedJobs){
					StatusMaster status = WorkThreadServlet.statusMap.get("vcomp");
					jft.setStatus(status);
					jft.setStatusMaster(status);
					statelesSsession.update(jft);
					
					if(status!=null){
						if(applyHistForJobsMap.get(jft.getTeacherId().getTeacherId()+"###"+jft.getJobId().getJobId()+"###"+status.getStatusId())==null){
							TeacherStatusHistoryForJob tshj = new TeacherStatusHistoryForJob();
							tshj.setJobOrder(jft.getJobId());
							tshj.setStatus("S");
							tshj.setStatusMaster(status);
							tshj.setTeacherDetail(jft.getTeacherId());
							tshj.setCreatedDateTime(new Date());
							tshj.setUserMaster(userMaster);
							statelesSsession.insert(tshj);
						}
					}
				}
			}else{
				// get all parent category of all applied jobs
				for(JobForTeacher jft : kellyAppliedJobs){
					listJobs.add(jft.getJobId());
					if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
						jobCategories.add(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
						System.out.println(" jft.getJobId().getJobCategoryMaster().getParentJobCategoryId() :: "+jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
					}
				}
	
				List<SecondaryStatus> secondaryStatuses =null;
				if(jobCategories!=null && jobCategories.size()>0)
					secondaryStatuses = secondaryStatusDAO.findByNameAndJobCats(nodeName,jobCategories);
				
				if(secondaryStatuses!=null && secondaryStatuses.size()>0)
					for(SecondaryStatus st : secondaryStatuses){
						if(st.getStatusMaster()!=null){
							if(st.getSecondaryStatusName().equalsIgnoreCase(nodeName)){
								parentStatus.put(st.getJobCategoryMaster().getJobCategoryId(),st.getStatusMaster());
							}
						}
						else{
							parentSecStatus.put(st.getJobCategoryMaster().getJobCategoryId(),st);
						}
					}
				
				int parentCatId=0;
				for(JobForTeacher jft : kellyAppliedJobs){
					SecondaryStatus sec = null;
					StatusMaster status = null;
					Boolean historyExists = false;
					if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
						parentCatId = jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();
					
					if(parentStatus.get(parentCatId)!=null)
						status = parentStatus.get(parentCatId);
					else if(parentSecStatus.get(parentCatId)!=null)
						sec = parentSecStatus.get(parentCatId);
					
					if(sec!=null){
						jft.setStatusMaster(null);
						jft.setSecondaryStatus(sec);
					}
					else if(status!=null){
						jft.setStatus(status);
						jft.setStatusMaster(status);
					}
					
					statelesSsession.update(jft);
					
					if(sec!=null || status!=null){
						if(sec!=null && applyHistForJobsMap.get(jft.getTeacherId().getTeacherId()+"###"+jft.getJobId().getJobId()+"###"+sec.getSecondaryStatusId())!=null)
							historyExists =true;
						else if(status!=null &&  applyHistForJobsMap.get(jft.getTeacherId().getTeacherId()+"###"+jft.getJobId().getJobId()+"###"+status.getStatusId())!=null)
							historyExists = true;
						
						if(!historyExists)
						{
							TeacherStatusHistoryForJob tshj = new TeacherStatusHistoryForJob();
							tshj.setJobOrder(jft.getJobId());
							tshj.setSecondaryStatus(sec);
							tshj.setStatus("S");
							tshj.setStatusMaster(status);
							tshj.setTeacherDetail(jft.getTeacherId());
							tshj.setCreatedDateTime(new Date());
							tshj.setUserMaster(userMaster);
							statelesSsession.insert(tshj);
						}
					}
				}
			}
			txOpen.commit();
			statelesSsession.close();
		}
	}
	
	public void callCheckEmailAPI(TeacherDetail teacherDetail,ApplitrackDistricts applitrackDistricts,MQEvent mqEvent,UserMaster userMaster, List<TalentKSNDetail> talentksndList,MQEventDAO mqEventDAO,SessionFactory sessionFactory,StatelessSession statelesSsession,Transaction txOpen,int counter){
			System.out.println(" callCheckEmailAPI ");
			try{
				//ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("CheckForTalentEmail");
				if(applitrackDistricts == null){
				    System.out.println("applitrackDistricts Object Can Not Be Null");
				}
				else{
					Integer selectedKSNID = 0;
					Boolean isNew = false;
					String status = null;
					Long ksnTalentID = null;
			        String firstName = null;
			        String lastName = null;
			        String lastFourSSN= null;
			        Long tmTalentID = null;
					String tmTransactionId = "TM-"+(Utility.randomString(8)+Utility.getDateTime());
					if(counter%100==1){
						txOpen.commit();
						txOpen = statelesSsession.beginTransaction();
					}
					
					if(mqEvent==null){
						isNew = true;
						mqEvent = new MQEvent();
						mqEvent.setTeacherdetail(teacherDetail);
						mqEvent.setEventType(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
						mqEvent.setTmTransId(tmTransactionId);
						mqEvent.setStatus("R");
						mqEvent.setAckStatus("Recieved");
						mqEvent.setCreatedBy(userMaster.getUserId());
						mqEvent.setCreatedDateTime(new Date());
						mqEvent.setIpAddress(null);
						mqEvent.setCheckEmailAPI(true);
						//mqEventDAO.makePersistent(mqEvent);
						statelesSsession.insert(mqEvent);
					}
					else{
						mqEvent.setCheckEmailAPI(true);
						mqEvent.setMsgStatus(null);
						//mqEventDAO.updatePersistent(mqEvent);
						statelesSsession.update(mqEvent);
					}
					if(mqEvent.getStatus()==null || (mqEvent.getStatus()!=null && !mqEvent.getStatus().equalsIgnoreCase("C"))){
						IKSNDataServiceProxy proxy = new IKSNDataServiceProxy();
						CheckForTalentEmailAddressRequest chkRequest = new CheckForTalentEmailAddressRequest();
						ExternalRequestBase authorization = new ExternalRequestBase();
						authorization.setPassword(applitrackDistricts.getPassword());
						authorization.setUserName(applitrackDistricts.getUserName());
						chkRequest.setAuthorization(authorization);
						chkRequest.setTMTransactionID(tmTransactionId);
						chkRequest.setEmailID(teacherDetail.getEmailAddress());
						CheckForTalentEmailAddressResponse response = proxy.checkForTalentEmailAddress(chkRequest);
						status = response.getResponseBase().getResponseStatus().toString();
						System.out.println("ResponseStatus :    "+response.getResponseBase().getResponseStatus());
						if(status.equalsIgnoreCase("Successful")){
							System.out.println("TalentList Length    "+response.getTalentList().length);
							TalentDetail[] talentList = response.getTalentList();
							System.out.println("talentList       "+talentList);

							if(talentksndList!=null && talentksndList.size()>0){
								for(TalentKSNDetail tsn : talentksndList){
									if(tsn.getIsSelected()!=null && tsn.getIsSelected().equalsIgnoreCase("n")){
										tsn.setStatus("I");
										statelesSsession.update(tsn);
									}
									else{
										selectedKSNID = tsn.getKSNID();
									}
								}
							}

							for(TalentDetail talentDetail : talentList){  
								ksnTalentID = talentDetail.getKSNTalentID();
								firstName = talentDetail.getFirstName();
								lastName = talentDetail.getLastName();
								tmTalentID = talentDetail.getTeacherMatchID();
								lastFourSSN = talentDetail.getLastFourSSN();
								System.out.println("KSNTalentID: "+ksnTalentID+" FirstName: "+firstName+" LastName: "+lastName+" TMTalentID: "+tmTalentID+" LastFourSSN: "+lastFourSSN);
								if(mqEvent!= null){
									TalentKSNDetail talentKSNDetail = new TalentKSNDetail();
									talentKSNDetail.setFirstName(firstName);
									talentKSNDetail.setEmailAddress(teacherDetail.getEmailAddress());
									talentKSNDetail.setTeacherId(teacherDetail);
									talentKSNDetail.setLastName(lastName);
									talentKSNDetail.setLastFourSSN(lastFourSSN);
									talentKSNDetail.setKSNID((int) (long)ksnTalentID);
									talentKSNDetail.setEventId(mqEvent);
									if(tmTalentID!=null)
										talentKSNDetail.setTmTalentId((int) (long)tmTalentID);
									else
										talentKSNDetail.setTmTalentId(0);
									talentKSNDetail.setCreatedDateTime(new Date());
									talentKSNDetail.setStatus("A");
									talentKSNDetail.setIsSelected("N");
									if(selectedKSNID==0 || (selectedKSNID!=0 && !(selectedKSNID.intValue()==(ksnTalentID.intValue()))))
										statelesSsession.insert(talentKSNDetail);
								} 	   
							}
							
							try {
								CandidateGridService candidateGridService = new CandidateGridService();
								String color = "Grey";
								color = candidateGridService.getNodeColorWithMqEvent(mqEvent, null);
								StatusNodeColorHistory statusNodeColorHistory = new StatusNodeColorHistory();
								HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
								headQuarterMaster.setHeadQuarterId(1);
								statusNodeColorHistory.setHeadQuarterMaster(headQuarterMaster);
								statusNodeColorHistory.setCreatedDateTime(new Date());
								statusNodeColorHistory.setTeacherDetail(teacherDetail);
								statusNodeColorHistory.setPrevColor(color);
								statusNodeColorHistory.setMqEvent(mqEvent);
								statusNodeColorHistory.setCurrentColor(color);
								statusNodeColorHistory.setUserMaster(userMaster);
								statelesSsession.insert(statusNodeColorHistory);
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						else{
							mqEvent.setAckStatus(status);
						}

						if(mqEvent != null){
							if(isNew)
								statelesSsession.insert(mqEvent);
							else
								statelesSsession.update(mqEvent);
						}
					}
					//statelesSsession.close();
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}
	}
}