package tm.dao;

import static tm.services.district.GlobalServices.println;

import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.hibernate.type.Type;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import tm.bean.DistrictApprovalGroups;
import tm.bean.DistrictSpecificApprovalFlow;
import tm.bean.DistrictWiseApprovalGroup;
import tm.bean.EventDetails;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.SchoolInJobOrder;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherCertificate;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.user.UserMaster;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.generic.GenericHibernateDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CertificateTypeMasterDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.MailSendToTeacher;
import tm.services.ReferenceCheckAjax;
import tm.services.district.GlobalServices;
import tm.services.district.PrintOnConsole;
import tm.services.es.ElasticSearchService;
import tm.services.report.MailSendVVIThread;
import tm.services.teacher.AcceptOrDeclineMailHtml;
import tm.servlet.WorkThreadServlet;
import tm.utility.ElasticSearchConfig;
import tm.utility.TestTool;
import tm.utility.Utility;

public class JobForTeacherDAO extends GenericHibernateDAO<JobForTeacher, Long> 
{
	public JobForTeacherDAO() 
	{
		super(JobForTeacher.class);
	}
	@Autowired
	private CommonService commonService;
	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;
	public void setUserEmailNotificationsDAO(
			UserEmailNotificationsDAO userEmailNotificationsDAO) {
		this.userEmailNotificationsDAO = userEmailNotificationsDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TeacherCertificateDAO teacherCertificateDAO;
	
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	
	@Autowired
	private CertificateTypeMasterDAO certificateTypeMasterDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	
	
	

	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO)
	{
		this.teacherDetailDAO = teacherDetailDAO;
	}
	
	@Autowired
	DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	SchoolMasterDAO schoolMasterDAO;
	
	@Autowired
	SchoolInJobOrderDAO schoolInJobOrderDAO;
	
	@Autowired
	private ReferenceCheckAjax referenceCheckAjax;
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private EmailerService emailerService;
	@Autowired
	private DistrictSpecificApprovalFlowDAO districtSpecificApprovalFlowDAO;
	@Autowired
	private DistrictWiseApprovalGroupDAO districtWiseApprovalGroupDAO;
	@Autowired
	private DistrictApprovalGroupsDAO districtApprovalGroupsDAO;
	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	
	public SchoolInJobOrderDAO getSchoolInJobOrderDAO()
	{
		return schoolInJobOrderDAO;
	}
	
	@Transactional(readOnly=false)
	public void makePersistent(JobForTeacher entity)
	{
		JobForTeacher entityPrevious=null;
		try{
			if(entity.getJobForTeacherId()!=null && entity.getJobId().getDistrictMaster()!=null && entity.getJobId().getDistrictMaster().getAutoInactiveOrNot()){
				entityPrevious=(JobForTeacher)getSessionFactory().openSession().load(JobForTeacher.class, entity.getJobForTeacherId());
			}
		}catch (HibernateException h) {
			h.printStackTrace();
		}
		int sendMail=0;
		try
		{
			if(entity!=null && entity.getJobId().getDistrictMaster()!=null
					&& entity.getJobId().getDistrictMaster().getJobCompletionEmail()
					&& Utility.isNC() 
					&& entity.getStatus()!=null && entity.getStatus().getStatusId()!=null &&  entity.getStatus().getStatusId()==4 && (entity.getSendMail()==null || !entity.getSendMail()))
			{
				System.out.println("-------------------JobForTeacher--------------");
				entity.setSendMail(true);
				sendMail=1;
				System.out.println(entity.getJobId());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try {
			super.makePersistent(entity);
			/*******************************************All District***************************************/
			try{
							if(entityPrevious!=null && entity.getJobId().getDistrictMaster()!=null && ((entityPrevious.getStatus().getStatusId()!=6 && entity.getStatus().getStatusId()==6) || (entityPrevious.getStatus().getStatusId()==6 && entity.getStatus().getStatusId()!=6)))
							{
								try {
										List<String> allEmailIds=new ArrayList<String>();
										if(entity.getJobId().getDistrictMaster().getDistrictId().equals(804800))
											allEmailIds=getGroupWiseAllUser(entity.getJobId(), entity.getJobId().getSchool().get(0));
										else
											allEmailIds=getAllSAandDAsEmailIds(entity.getJobId());
											
										new Thread(new AutoInactiveJobOrderThread(entityPrevious.getJobId(),jobOrderDAO,emailerService,allEmailIds)).start();
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
							
							
							try{
								System.out.println("entity sendmail flag="+entity.getSendMail());
								if(entity!=null && entity.getJobId().getDistrictMaster()!=null && entity.getStatus()!=null && entity.getStatus().getStatusId()==4 && entity.getJobId().getJobCompletedVVILink()!=null && entity.getJobId().getJobCompletedVVILink())
								{
									if( entity.getJobId().getOfferVirtualVideoInterview()!=null && entity.getJobId().getOfferVirtualVideoInterview() && entity.getJobId().getSendAutoVVILink()!=null && entity.getJobId().getSendAutoVVILink()){
										System.out.println(" entity.getVviMailSendFlag() :: "+entity.getVviMailSendFlag());
										
										//if(entity.getVviMailSendFlag()==null || !entity.getVviMailSendFlag()){	
											System.out.println(" AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA ");
											MailSendVVIThread mailForVVI = new MailSendVVIThread();
											mailForVVI.setJobForTeacher(entity);
											mailForVVI.start();
											mailForVVI.join();
										//}
									}
									
									
								}
								else if(entity!=null && entity.getJobId().getDistrictMaster()!=null
										&& entity.getJobId().getDistrictMaster().getJobCompletionEmail()
										&& Utility.isNC() 
										&& entity.getStatus()!=null && entity.getStatus().getStatusId()!=null &&  entity.getStatus().getStatusId()==4 && sendMail==1)
								{//headquartercheck
									try
									{
										System.out.println(entity.getStatus());
										if(entity.getStatus()!=null)
											System.out.println(entity.getStatus().getStatusId());
										//if(entity.getStatus()!=null && entity.getStatus().getStatusId()!=null && entity.getStatus().getStatusId().equals(new Integer(4)))
										{
											WebContext context;
											context = WebContextFactory.get();
											HttpServletRequest request = context.getHttpServletRequest();
											HttpSession session = request.getSession(false);
											TeacherDetail teacherDetail = null;
											if (session != null || session.getAttribute("teacherDetail") != null) 
												teacherDetail=(TeacherDetail)session.getAttribute("teacherDetail");
											JobOrder jobOrder = entity.getJobId();
											List<UserMaster> lstusermaster	=	new ArrayList<UserMaster>();

											try{
												/* ===== Gagan : statusJobApply is used to get status short name against Apply which is used in Email sending to all active DA\SA Active Admins who has checked thier job apply CHECKBOX in notification [ Available ]======= */
												StatusMaster statusJobApply = WorkThreadServlet.statusMap.get("apl");
												String statusShortName = statusJobApply.getStatusShortName();
												boolean isSchool=false;
												if(jobOrder.getSelectedSchoolsInDistrict()!=null){
													if(jobOrder.getSelectedSchoolsInDistrict()==1){
														isSchool=true;
													}
												}
												if(jobOrder.getCreatedForEntity()==2 && isSchool==false){
													lstusermaster=userEmailNotificationsDAO.getUserByDistrict(jobOrder.getDistrictMaster(),statusShortName);
												}else if(jobOrder.getCreatedForEntity()==2 && isSchool){
													List<SchoolMaster>  lstschoolMaster= new ArrayList<SchoolMaster>();
													if(jobOrder.getSchool()!=null){
														if(jobOrder.getSchool().size()!=0){
															for(SchoolMaster sMaster : jobOrder.getSchool()){
																lstschoolMaster.add(sMaster);
															}
														}
													}
													lstusermaster=userEmailNotificationsDAO.getUserByDistrictAndSchoolList(jobOrder.getDistrictMaster(),lstschoolMaster,jobOrder,statusShortName);
												}else if(jobOrder.getCreatedForEntity()==3){
													lstusermaster=userEmailNotificationsDAO.getUserByOnlySchool(jobOrder.getSchool().get(0),statusShortName);
												}
											}catch(Exception e){
												e.printStackTrace();
											}
											TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(entity.getTeacherId());
											TeacherAssessmentStatus _teacherAssessmentStatus =null;
											StatusMaster _statusMaster =	null;

											if(entity.getJobId().getJobCategoryMaster().getBaseStatus())
											{
												List<TeacherAssessmentStatus> _teacherAssessmentStatusList = null;
												JobOrder _jOrder = new JobOrder();
												_jOrder.setJobId(0);
												_teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(entity.getTeacherId(),_jOrder);
												if(_teacherAssessmentStatusList.size()>0){
													_teacherAssessmentStatus = _teacherAssessmentStatusList.get(0);
													_statusMaster = teacherAssessmentStatus.getStatusMaster();
												}else{
													_statusMaster = WorkThreadServlet.statusMap.get("icomp");
												}
											}
											String[] arrHrDetail = commonService.getHrDetailToTeacher(request,entity.getJobId());
											arrHrDetail[10]	=	Utility.getBaseURL(request)+"signin.do";
											arrHrDetail[11] =	Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+entity.getJobId().getJobId()+"&JobOrderType="+entity.getJobId().getCreatedForEntity();
											if(entity.getJobId().getDistrictMaster()!=null)
												try {
													arrHrDetail[12] =	Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(entity.getJobId().getDistrictMaster().getDistrictId()));
												} catch (IOException e1) {
													arrHrDetail[12] ="";
													// TODO Auto-generated catch block
													e1.printStackTrace();
												}
											else
												arrHrDetail[12] ="";
											arrHrDetail[13] =	Utility.getValueOfPropByKey("basePath")+"/forgotpassword.do";
											try {
												if(_statusMaster.getStatusShortName()!=null)
													arrHrDetail[14] =	_statusMaster.getStatusShortName();
											} catch (NullPointerException e) {
												//e.printStackTrace();
											}
											String flagforEmail	="applyJob";
											//entity.setSendMail(true);
											MailSendToTeacher mst =new MailSendToTeacher(teacherDetail,jobOrder,request,arrHrDetail,flagforEmail,lstusermaster,emailerService,entity);
											Thread currentThread = new Thread(mst);
											currentThread.start(); 
										
										}
									}
									catch(Exception e)
									{
										e.printStackTrace();
									}
								}
								else{
									System.out.println(" entity (Jobforteacher ) is NULL ");
								}
								
							}catch(Exception e){
								e.printStackTrace();
							}
							/*String num = Utility.getValueOfPropByKey("isAPILive");
							if(num!=null && !num.equals(""))
							{
								int live=Utility.getIntValue(num);
								if(live==1)
								 {
									try {
											if(entity.getJobId().getDistrictMaster()!=null && entity.getJobId().getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800") && entity.isStatusChange() && entity.isStatusMasterChange() && entity.getStatus().getStatusShortName().equalsIgnoreCase("hird") && entity.getStatusMaster().getStatusShortName().equalsIgnoreCase("hird"))
											{
												try {
													JeffcoAPIController.jcTMHireCandidate(entity,super.getSessionFactory());
												} catch (Exception e) {
													e.printStackTrace();
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								}*/
			}catch(Exception e){e.printStackTrace();}
			/*******************************************End Jeffco***************************************/
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			if( entity.isStatusChange() )
				referenceCheckAjax.sendMail(entity);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		
		
		
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherForOffer(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("offerReady",true);
			Criterion criterion3 = Restrictions.eq("offerReady",false);
			Criterion criterion4 = Restrictions.or(criterion3,criterion2);
			lstJobForTeacher = findByCriteria(criterion1,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherOneYear(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		List<Object> result =new ArrayList<Object>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Date sDate= null;
			Date eDate= null;
			
			/*Calendar cal = Calendar.getInstance();
			cal.set(cal.get(cal.YEAR),0,1,0,0,0);
			sDate=cal.getTime();
			cal.set(cal.get(cal.YEAR),11,31,23,59,59);
			eDate = cal.getTime();*/
			
			
			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			cal1.add(Calendar.DATE, -365);
			sDate = cal1.getTime();
			eDate = cal2.getTime();
			
			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
			lstJobForTeacher = findByCriteria(criterion1,criterion2);	
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacher(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstJobForTeacher = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	/*======== Gagan  ============*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findDistrictOrSchoolLatestAppliedJobByTeacher(TeacherDetail teacherDetail,DistrictMaster districtMaster,List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstStatusMasters.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.in("status", lstStatusMasters);
	
				criteria.add(criterion1);
				criteria.add(criterion2);
				if(districtMaster!=null)
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				lstJobForTeacher = criteria.list();
			}

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/*======== Gagan:findIncompleteJoborderforwhichBaseIsRequired  ============*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findIncompleteJoborderforwhichBaseIsRequired(TeacherDetail teacherDetail,StatusMaster statusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:1");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", statusMaster);
			Criterion criterion3 = Restrictions.eq("isAffilated", 0); 

			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);

			Criteria criteria1= criteria.createCriteria("jobId").add(Restrictions.le("jobStartDate",dateWithoutTime)).add(Restrictions.ge("jobEndDate",dateWithoutTime));
			criteria1.createCriteria("jobCategoryMaster").add(Restrictions.le("baseStatus",true));
			//lstJobForTeacher = findByCriteria(criterion3);	

			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}


	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByFilterTeacher(List<TeacherDetail> teacherDetailsist)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(teacherDetailsist.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",teacherDetailsist);
				lstJobForTeacher = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* Gagan : Get Jobforteacherlist from jobforteacherids ========== */
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherList(List jobforteacherlistteacherIdList)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{	if(jobforteacherlistteacherIdList.size()>0){
				Criterion criterion1 = Restrictions.in("jobForTeacherId",jobforteacherlistteacherIdList);
				lstJobForTeacher = findByCriteria(criterion1);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJobByTeacher(Order  sortOrderStrVal,TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstJobForTeacher = findByCriteria(sortOrderStrVal,criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/*
	 * 
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatus(TeacherDetail teacherDetail, StatusMaster statusMaster)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",statusMaster);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			lstJobForTeacher = findByCriteria(Order.desc("createdDateTime"),criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}


	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJobByTeacherAndStatus(Order  sortOrderStrVal,TeacherDetail teacherDetail, StatusMaster statusMaster)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",statusMaster);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			lstJobForTeacher = findByCriteria(sortOrderStrVal,criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JobByTeacher And Status During CurrentYear.
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusDuringCurrentYear(TeacherDetail teacherDetail, StatusMaster statusMaster)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Calendar cal = Calendar.getInstance();

			cal.set(cal.get(cal.YEAR),0,1,0,0,0);
			Date sDate=cal.getTime();
			cal.set(cal.get(cal.YEAR),11,31,23,59,59);

			Date eDate=cal.getTime();

			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("status",statusMaster);
			//Criterion criterion3 = Restrictions.between("createdDateTime", sDate, eDate);

			//lstJobForTeacher = findByCriteria(criterion1,criterion2,criterion3);
			//Date check removed for time being
			lstJobForTeacher = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JobByTeacher And Status During CurrentYear.
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusDuringCurrentYear(TeacherDetail teacherDetail, List<StatusMaster> statusMasterList)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(statusMasterList.size()>0){	
				Calendar cal = Calendar.getInstance();
				cal.set(cal.get(cal.YEAR),0,1,0,0,0);
				Date sDate=cal.getTime();
				cal.set(cal.get(cal.YEAR),11,31,23,59,59);
				Date eDate=cal.getTime();
	
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.in("status",statusMasterList);
				//Date check removed for time being
				lstJobForTeacher = findByCriteria(criterion1,criterion2);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JobByTeacherAndSatusFormJobId.
	 */

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusFormJobId(TeacherDetail teacherDetail, JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			lstJobForTeacher = findByCriteria(criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* @Author: Vishwanath Kumar
	 * @Discription: It is used to find JobByTeacherAndSatusFormJobIds.
	 */

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusFormJobIds(TeacherDetail teacherDetail, List<JobOrder> jobOrders)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(jobOrders.size()>0){
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.in("jobId",jobOrders);
				Criterion criterion3 = Restrictions.and(criterion1,criterion2);
				lstJobForTeacher = findByCriteria(criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobsForJobSpecificInventory(TeacherDetail teacherDetail, StatusMaster statusMaster, Boolean assesmentStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:2");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", statusMaster);
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime))
			.add(Restrictions.eq("status", "A"));
			//lstJobForTeacher = findByCriteria(criterion3);	

			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJobsForJobSpecificInventory(Order sortOrderStrVal,TeacherDetail teacherDetail, StatusMaster statusMaster, Boolean assesmentStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:3");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			Criterion criterion2 = Restrictions.eq("status", statusMaster);
			Criterion criterion3 = Restrictions.and(criterion1, criterion2);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime))
			.add(Restrictions.eq("status", "A"))
			.addOrder(sortOrderStrVal);
			//lstJobForTeacher = findByCriteria(criterion3);	

			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public JobForTeacher findJobByTeacherAndJob(TeacherDetail teacherDetail, JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);

			lstJobForTeacher = findByCriteria(criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			return null;
		else
			return lstJobForTeacher.get(0);	
	}

	// Gagan : Getting Latest Cover Letter by Teacher
	@Transactional(readOnly=false)
	public JobForTeacher findJobByTeacherAndCoverLetter(TeacherDetail teacherDetail)
	{
		JobForTeacher jobForTeacher	=	 null;	
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.isNotNull("coverLetter");
			Criterion criterion3 = Restrictions.ne("coverLetter","");
			Criterion criterion4 = Restrictions.and(criterion2,criterion3);

			lstJobForTeacher = findByCriteria(Order.desc("createdDateTime"),criterion1,criterion4);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			return null;
		else
		{
			for(JobForTeacher jft: lstJobForTeacher)
			{
				if(jft.getCoverLetter().replaceAll("\\<[^>]*>","").length()>0)
				{
					jobForTeacher	=	jft;
					break;
				}
			}
			return jobForTeacher;
		}
	}


	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTbyJobOrder(JobOrder jobOrder, List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstStatusMasters.size()>0){
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.in("status", lstStatusMasters);			
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.ne("status",statusMaster);
				lstJobForTeacher = findByCriteria(criterion1,criterion2,criterion3);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTbyJobOrderForCG(JobOrder jobOrder, List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstStatusMasters.size()>0){
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.in("status", lstStatusMasters);			
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.ne("status",statusMaster);
				Criterion criterion4 = Restrictions.eq("cgUpdated",true);			
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3, criterion4);	
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findByJobOrderForCG(JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.in("status", lstStatusMasters);			
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion3 = Restrictions.ne("status",statusMaster);
			Criterion criterion7 = Restrictions.ne("isAffilated",1);
			if(cgUpdate==0){
				Criterion criterion4 = Restrictions.isNull("cgUpdated");
				Criterion criterion5 = Restrictions.eq("cgUpdated",false);
				Criterion criterion6 = Restrictions.or(criterion4, criterion5);
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3,criterion6,criterion7);
			}else if(cgUpdate==1){
				Criterion criterion4 = Restrictions.eq("cgUpdated",true);
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3,criterion4,criterion7);
			}else if(cgUpdate==2){
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3,criterion7);
			}else if(cgUpdate==5){
				lstJobForTeacher = findByCriteria(criterion1,criterion2, criterion3);
			}else if(cgUpdate==3){
				criterion7 = Restrictions.eq("isAffilated",1);
				lstJobForTeacher = findByCriteria(criterion7,criterion2, criterion3);
			}else{
				lstJobForTeacher = findByCriteria(criterion2, criterion3);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}
	/* @Author: Gagan 
	 * @Discription: For Sorting Hired Applicant Grid
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJFTbyJobOrder(Order  sortOrderStrVal,JobOrder jobOrder, List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstStatusMasters.size()>0){
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.in("status", lstStatusMasters);			
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.ne("status",statusMaster);
				lstJobForTeacher = findByCriteria(sortOrderStrVal,criterion1,criterion2,criterion3);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
	/* @Author: Gagan 
	 * @Discription: Get Total no of Applicant from JobForTeacher Table from Joborder.
	 */
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTApplicantbyJobOrder(JobOrder jobOrder)
	{
		List<JobForTeacher> JobForTeacherApplicant= null;
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);		
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			JobForTeacherApplicant = findByCriteria(Order.asc("createdDateTime"),criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return JobForTeacherApplicant;
	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobForTeacher> findJFTApplicantbyJobOrders(List<JobOrder> lstJobOrder) 
	{
		Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.add(criterion1)
		.list();
		Map<Integer,JobForTeacher> jobForTeacherMap = new HashMap<Integer, JobForTeacher>();
		JobForTeacher jobForTeacher = null;
		int i=0;
		for (Object object : result) {
			jobForTeacher=((JobForTeacher)object);
			jobForTeacherMap.put(new Integer(""+i),jobForTeacher);
			i++;
		}
		return jobForTeacherMap;
	}

	/* @Author: Gagan 
	 * @Discription: Get Total no of Applicant from JobForTeacher Table from Joborder.
	 */
	/*======= Find findSortedJFTApplicantbyJobOrder  Method */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJFTApplicantbyJobOrder(Order  sortOrderStrVal,JobOrder jobOrder)
	{
		List<JobForTeacher> JobForTeacherApplicant= null;
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);		
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			JobForTeacherApplicant = findByCriteria(sortOrderStrVal,criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return JobForTeacherApplicant;
	}
	/* @Author: Sekhar 
	 * @Discription: Get Total no of Records from JobForTeacher Table from Joborder.
	 */
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,JobForTeacher> findJFTApplicantbyJobOrder() 
	{
		Session session = getSession();
		List result = session.createCriteria(getPersistentClass())
		.list();
		Map<Integer,JobForTeacher> jobForTeacherMap = new HashMap<Integer, JobForTeacher>();
		JobForTeacher jobForTeacher = null;
		int i=0;
		for (Object object : result) {
			jobForTeacher=((JobForTeacher)object);
			jobForTeacherMap.put(new Integer(""+i),jobForTeacher);
			i++;
		}
		return jobForTeacherMap;
	}
	/* @Author: Sekhar 
	 * @Discription: Get Total no of Hires from JobForTeacher Table from Joborder.
	 */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTHiresByJobOrder(JobOrder jobOrder)
	{
		List<JobForTeacher> JobForTeacherApplicant= null;
		try{
			//StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion criterion1 = Restrictions.eq("status",statusMaster);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			JobForTeacherApplicant = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return JobForTeacherApplicant;
	}

	@Transactional(readOnly=false)	
	public List<JobForTeacher> findHireByJobForDistrict(JobOrder jobOrder,DistrictMaster districtMaster) 
	{		
		Criterion criterion3 =null;
		if(districtMaster!=null)
		{
			List<UserMaster> lstUserMasters = userMasterDAO.getUserByDistrict(districtMaster);

			if(lstUserMasters.size()>0)
				criterion3 = Restrictions.in("updatedBy",lstUserMasters);
		}
		//StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
		List<JobForTeacher> lstJobForTeachers = null;		
		Criterion criterion1 = Restrictions.eq("jobId", jobOrder);	
		Criterion criterion2 = Restrictions.eq("status", statusMaster);	

		if(criterion3!=null)
			lstJobForTeachers = findByCriteria(criterion1,criterion2,criterion3);
		else
			lstJobForTeachers = findByCriteria(criterion1,criterion2);


		return lstJobForTeachers;
	}

	@Transactional(readOnly=false)	
	public List<JobForTeacher> findHireByJobForSchool(JobOrder jobOrder,SchoolMaster schoolMaster) 
	{	
		Criterion criterion3 =null;
		if(schoolMaster!=null && schoolMaster.getSchoolId()>0)
		{
			List<UserMaster> lstUserMasters = userMasterDAO.getUserBySchool(schoolMaster);

			if(lstUserMasters.size()>0)
				criterion3 = Restrictions.in("updatedBy",lstUserMasters);
		}
		//StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
		List<JobForTeacher> lstJobForTeachers = null;		
		Criterion criterion1 = Restrictions.eq("jobId", jobOrder);	
		Criterion criterion2 = Restrictions.eq("status", statusMaster);	
		if(criterion3!=null)
			lstJobForTeachers = findByCriteria(criterion1,criterion2,criterion3);
		else
			lstJobForTeachers = findByCriteria(criterion1,criterion2);

		return lstJobForTeachers;
	}


	/* @Author: Gagan 
	 * @Discription: Get Total no of Applicant from JobForTeacher Table from Joborder(JobId).
	 */
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTApplicantsbyJobOrder(List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try {// vishwanath
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			String hql = " from jobforteacher WHERE jobId IN (:lstJobOrder) and status !=(:status)  group by teacherId";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameterList("lstJobOrder", lstJobOrder);
			query.setParameter("status", statusMaster);
			lstJobForTeacher = query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
	/* @Author: Vishwanath 
	 * @Discription: Get Total no of Applicant from jobforteacher Table from Joborder(JobId).
	 */
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<TeacherDetail> findUniqueApplicantsbyJobOrder(List<JobOrder> lstJobOrder)
	{
		List<TeacherDetail> lstJobForTeacher= null;
		try 
		{
			if(lstJobOrder.size()>0){
				Session session = getSession();
				Criteria  criteria = session.createCriteria(getPersistentClass()) ;
				criteria.setProjection(Projections.distinct(Projections.property("teacherId")));
				lstJobForTeacher = criteria 
				.add(Restrictions.in("jobId",lstJobOrder)) 
				.list();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}

	/* @Author: Vishwanath 
	 * @Discription: Get Total no of Applicant from jobforteacher Table from Joborder(JobId).
	 */
	/*======= Find findJFTApplicantbyJobOrder  Method ========*/
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacherJobsHasSameAssessment(TeacherDetail teacherDetail,AssessmentDetail assessmentDetail)
	{
		PrintOnConsole.getJFTPrint("JFT:4");
		List<JobForTeacher> jobForTeacherList= new ArrayList<JobForTeacher>();
		try 
		{

			Session session = getSession();
			Query qry = session.createQuery("from jobforteacher jft, AssessmentJobRelation ajr where jft.jobId=ajr.jobId and jft.teacherId="+teacherDetail.getTeacherId()+" and ajr.assessmentId="+assessmentDetail.getAssessmentId());

			//jobForTeacherList = qry.list();
			List l = qry.list();
			Iterator it=l.iterator();

			while(it.hasNext())
			{
				Object rows[] = (Object[])it.next();
				jobForTeacherList.add((JobForTeacher)rows[0]);
			}
			return jobForTeacherList;

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return null;
	}

	@Transactional(readOnly=false)
	public JobForTeacher findJFTByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			return null;
		else
			return lstJobForTeacher.get(0);		
	}

	/* @Author: Vishwanath 
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public List<TeacherDetail> getJobAppliedByTeachers(List<TeacherDetail> teacherDetails)
	{
		try 
		{
			if(teacherDetails.size()>0){
				Session session = getSession();
				List results = session.createCriteria(getPersistentClass())
				.add(Restrictions.in("teacherId",teacherDetails)) 
				.setProjection(Projections.groupProperty("teacherId")
				).list();
	
				return results;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		


		return null;
	}

	/* @Author: Vishwanath 
	 * @Discription: 
	 */
	@Transactional(readOnly=false)
	public List<TeacherDetail> getJobAppliedByTeachersYesterday(List<TeacherDetail> teacherDetails)
	{
		PrintOnConsole.getJFTPrint("JFT:5");
		try 
		{
			if(teacherDetails.size()>0){
				Calendar cal = Calendar.getInstance();
				cal.set(Calendar.HOUR_OF_DAY, 0);
				cal.set(Calendar.MINUTE, 0);
				cal.set(Calendar.SECOND, 0);
				cal.set(Calendar.MILLISECOND, 0);
				Date dateWithoutTime = cal.getTime();
	
				Calendar cal1 = Calendar.getInstance();
				cal1.add(Calendar.DATE, -1);
				cal1.set(Calendar.HOUR_OF_DAY, 0);
				cal1.set(Calendar.MINUTE, 0);
				cal1.set(Calendar.SECOND, 0);
				cal1.set(Calendar.MILLISECOND, 0);
	
				Criterion criterion1 = Restrictions.le("createdDateTime",dateWithoutTime);
				Criterion criterion2 = Restrictions.ge("createdDateTime",cal1.getTime());
	
				Session session = getSession();
				List results = session.createCriteria(getPersistentClass())
				.add(Restrictions.in("teacherId",teacherDetails)) 
				.setProjection(Projections.groupProperty("teacherId")
				).list();
	
				return results;
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		


		return null;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByJob(int entityID,List<JobOrder> lstJobOrder,Order order, DistrictMaster districtMaster,String firstName,String lastName,String emailAddress)
	{
		PrintOnConsole.getJFTPrint("JFT:6");
		List<TeacherDetail> lstTeacherDetail= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			boolean flag=false;
			if(lstJobOrder.size()>0 && entityID==3){
				flag=true;
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null && entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			if(entityID==1){
				flag=true;
			}
			if(flag){
				if(entityID!=1){
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.eq("status","A"))
					.addOrder(order);
				}else{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
					.addOrder(order);
				}
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrderByTeacher(List<Integer> statusMasterList,Order order, TeacherDetail teacherDetail,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,SubjectMaster subjectMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:7");
		List<JobOrder> jobOrderList= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(lstJobOrder.size()>0){
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
				criteria.createCriteria("jobId").addOrder(order);
			}else if(districtMaster!=null)
			{
				Criteria criteriaJobId=criteria.createCriteria("jobId");
				criteriaJobId.add(Restrictions.eq("districtMaster", districtMaster)).addOrder(order);
				if(jobCategoryMaster!=null)
					criteriaJobId.add(Restrictions.eq("jobCategoryMaster", jobCategoryMaster));
				if(subjectMaster!=null)
					criteriaJobId.add(Restrictions.eq("subjectMaster", subjectMaster));
				
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).addOrder(order);
			}
			else if(headQuarterMaster!=null && branchMaster==null){
				if(headQuarterMaster.getHeadQuarterId()==1)
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}
			else if(branchMaster!=null){
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			}
			else{
				criteria.createCriteria("jobId").addOrder(order);
			}

			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
			criteria.add(criterion1);
			criteria.setProjection(Projections.groupProperty("jobId"));
			jobOrderList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrderList;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForTeacher(){
		List<JobForTeacher> lstJobForTeacher= null;
		try{
			lstJobForTeacher = findByCriteria(Order.asc("createdDateTime"));		
		} 
		catch (Exception e){
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByDS(List<Integer> statusMasterList,List<JobOrder> lstJobOrder,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:8");
		List<JobForTeacher> lstJobForTeacher= null;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			if(lstJobOrder.size()>0){
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
			//criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public Boolean findIsPortfolioNeededByTeacher(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			lstJobForTeacher = findByCriteria(criterion1);	
			if(lstJobForTeacher.size()>0)
			{
				for (JobForTeacher jobForTeacher : lstJobForTeacher) {
					if(jobForTeacher.getJobId().getIsPortfolioNeeded())
						return true;
				}
			}
			else
				return true;
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return false;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForUpdateInCG()
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			//StatusMaster statusHide = WorkThreadServlet.statusMap.get("hide");
			StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.eq("cgUpdated",false);			
			Criterion criterion2 = Restrictions.ne("status",statusHide);
			Criterion criterion3 = Restrictions.isNull("cgUpdated");
			Criterion criterion4 = Restrictions.or(criterion1, criterion3);
			Criterion criterion5 = Restrictions.and(criterion4, criterion2);
			lstJobForTeacher = findByCriteria(criterion5);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForCG(List<TeacherDetail> lstTeacherDetails)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstTeacherDetails.size()>0){
				//StatusMaster statusHide = WorkThreadServlet.statusMap.get("hide");
				StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
				//StatusMaster statusWithdraw = WorkThreadServlet.statusMap.get("widrw");
				StatusMaster statusWithdraw= WorkThreadServlet.statusMap.get("widrw");
				Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
				Criterion criterion2 = Restrictions.ne("status",statusHide);
				Criterion criterion3 = Restrictions.ne("status",statusWithdraw);
				lstJobForTeacher = findByCriteria(criterion1,criterion2,criterion3 );
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByDSTeacherList(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:9");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
			if(lstJobOrder.size()>0){
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			if(lstTeacherDetails.size()>0){
				criteria.add(criterionTeacher);
			}
			//criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
			criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASA(Order sortOrderStrVal,int start,int end,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress)
	{
		PrintOnConsole.getJFTPrint("JFT:10");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		List<TeacherDetail> teacherDetailList= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criteria criteria = session.createCriteria(getPersistentClass());
			boolean flag=false;
			if(lstJobOrder.size()>0 && entityID==3){
				flag=true;
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null && entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			if(flag){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			if(lstTeacherDetail.size()<end)
				end=lstTeacherDetail.size();

			teacherDetailList=lstTeacherDetail.subList(start, end);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return teacherDetailList;
	}

	@Transactional(readOnly=false)
	public int getTeacherDetailByDASARecords(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress)
	{
		PrintOnConsole.getJFTPrint("JFT:12");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		int totalRecords=0;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			boolean flag=false;
			if(lstJobOrder.size()>0 && entityID==3){
				flag=true;
				Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
				criteria.add(criterion1);
			}else if(districtMaster!=null && entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			criteria.createCriteria("status").add(Restrictions.ne("statusId", statusMaster.getStatusId()));
			if(flag){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			totalRecords=lstTeacherDetail.size();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return totalRecords;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASA(Order sortOrderStrVal,int start,int end,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> notTeacherDetails,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:13");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			
			criteria.add(Restrictions.ne("status",statusMaster));

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				flag=true;
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
			if(status){
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}else{
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			}
			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}

			// job applied flag
			if(appliedfDate!=null && appliedtDate!=null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			}else if(appliedfDate!=null && appliedtDate==null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			}else if(appliedtDate!=null && appliedfDate==null){
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}


			if(notTeacherDetails.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", notTeacherDetails)));	
			//new change fresh candidates
			flag=true;
			
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				criteria.createCriteria("teacherId")
				.add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				criteria.setFirstResult(start);
				criteria.setMaxResults(end);

				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		return lstTeacherDetail;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecords(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> notTeacherDetails,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:15");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			criteria.add(Restrictions.ne("status",statusMaster));

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
			if(status){
				//criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}else{
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			}

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			//job applied flag
			if(appliedfDate!=null && appliedtDate!=null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			}else if(appliedfDate!=null && appliedtDate==null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			}else if(appliedtDate!=null && appliedfDate==null){
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}
			flag=true;
			if(notTeacherDetails.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", notTeacherDetails)));

			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsBeforeDate(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds)
	{
		PrintOnConsole.getJFTPrint("JFT:17");
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			boolean flag=false;
			boolean dateDistrictFlag=false;
			
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				flag=true;
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
			if(status){
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
			}else{
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			}

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			} 
			//job applied flag
			
			flag=true;
			if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
				//criteria.add(Restrictions.or( Restrictions.le("createdDateTime",appliedfDate),Restrictions.ge("createdDateTime",appliedtDate)));
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			else if(daysVal==5 && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
			else if(appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));


			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByDSTeachersList(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
	{
		PrintOnConsole.getJFTPrint("JFT:18");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		if(lstTeacherDetails.size()>0){
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
				
				if(entityID!=1){
					Criteria jobOrderSet= criteria.createCriteria("jobId");
					if(districtMaster!=null){
						jobOrderSet.add(Restrictions.eq("districtMaster",districtMaster));
					}else{
						//jobOrderSet.add(Restrictions.isNull("districtMaster"));
					}
					if(branchMaster!=null){
						jobOrderSet.add(Restrictions.eq("branchMaster",branchMaster));
					}else{
						//jobOrderSet.add(Restrictions.isNull("branchMaster"));
					}
					if(headQuarterMaster!=null){
						if(headQuarterMaster.getHeadQuarterId()==1){
						   jobOrderSet.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
						}
					}else{
						//jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
					}
				}
			if(lstTeacherDetails.size()>0){
				criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
				criteria.add(criterionTeacher);
			}
				//criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForUpdateVltInCG(List<StatusMaster> lstStatusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			//StatusMaster statusHide = WorkThreadServlet.statusMap.get("hide");
			StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.eq("cgUpdated",false);			
			Criterion criterion2 = Restrictions.ne("status",statusHide);
			Criterion criterion3 = Restrictions.isNull("cgUpdated");
			Criterion criterion4 = Restrictions.or(criterion1, criterion3);
			Criterion criterion5 = Restrictions.and(criterion4, criterion2);
			Criterion criterion6= Restrictions.in("status",lstStatusMasters); 
			lstJobForTeacher = findByCriteria(criterion5,criterion6);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}


	@Transactional(readOnly=false)
	public List<JobForTeacher> findByJobOrderForCG(UserMaster userMaster,JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate,String firstName,String lastName,String emailAddress,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean all,boolean teachersOnly,List<Long> selectedTeachers)
	{
		PrintOnConsole.getJFTPrint("JFT:19");
		List<JobForTeacher> lstJobForTeacher=  new ArrayList<JobForTeacher>();
		try{
			Session session = getSession();
			List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
			Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
			for (StatusMaster sMaster : statusMasterLst) {
				mapStatus.put(sMaster.getStatusShortName(),sMaster);
			}

			Criterion criterionSchool =null;
            if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
                criterionSchool=Restrictions.eq("schoolMaster",userMaster.getSchoolId());
            }
			
			StatusMaster statusMaster= mapStatus.get("hide");
			StatusMaster internalStatus= mapStatus.get("icomp");
			Criterion criterion10 = Restrictions.ne("internalStatus",internalStatus);
			Criterion criterion14 = Restrictions.isNull("internalStatus");
			Criterion criterion15 = Restrictions.or(criterion10,criterion14);
			Criterion criterion11 = Restrictions.eq("internalStatus",internalStatus);
			Criterion criterion1 = Restrictions.in("status", lstStatusMasters);		
			Criterion criterion13 = Restrictions.or(criterion11,criterion1);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
			Criterion criterion3 = Restrictions.ne("status",statusMaster);
			Criterion criterion4 = Restrictions.eq("cgUpdated",true);
			Criterion criterion8 = Restrictions.isNull("cgUpdated");
			Criterion criterion5 = Restrictions.eq("cgUpdated",false);
			Criterion criterion6 = Restrictions.or(criterion5, criterion8);
			Criterion criterion9 = Restrictions.or(criterion6, criterion4);
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(teachersOnly)
			{
				System.out.println("teachersOnly :"+teachersOnly+" ====== selectedTeachers.size() :: "+selectedTeachers.size());
				if(selectedTeachers.size()>0)
				{
					criteria.add(Restrictions.in("jobForTeacherId", selectedTeachers));
				}
			}
			
			if(cgUpdate==0){
				criteria.add(criterion13);
				criteria.add(criterion2);
				criteria.add(criterion3);
				criteria.add(criterion9);
			}else if(cgUpdate==1){
				criteria.add(criterion15);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
				/*boolean epiInternal=false;
				try{
					if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
						epiInternal=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobOrder.getJobCategoryMaster()!=null){
					if(jobOrder.getJobCategoryMaster().getBaseStatus() && jobOrder.getJobCategoryMaster().getStatus().equalsIgnoreCase("A")){
						if(epiInternal==false){
							Criterion criterion7 = Restrictions.eq("isAffilated",1);
							Criterion criterion12 =Restrictions.or(criterion7, criterion4) ;
							criteria.add(criterion12);
						}else{
							criteria.add(criterion4);
						}
					}
				}*/
			}else if(cgUpdate==7){
				criteria.add(criterion15);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
			}else if(cgUpdate==2){
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
			}else if(cgUpdate==5){
				criteria.add(criterion15);
				criteria.add(criterion1);
				criteria.add(criterion2);
				criteria.add(criterion3);
			}else if(cgUpdate==3){
				criteria.add(criterion2);
				criteria.add(criterion3);
			}else{
				criteria.add(criterion2);
				criteria.add(criterion3);
			}
			
			 if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
	                criteria.add(criterionSchool);
	           }
			
			if(teacherFlag){
				criteria.add(Restrictions.in("teacherId", filterTeacherList));
			}
			criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
			.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
			.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplicantsByJobOrders(int status,List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:20");
		Session session = getSession();

		Criterion criterion = Restrictions.in("jobId", jobOrders);		
		Criteria c= session.createCriteria(getPersistentClass());

		if(status>0)
		{
			StatusMaster statusMaster = new StatusMaster();
			statusMaster.setStatusId(status);
			c.add(Restrictions.eq("status", statusMaster));
		}

		c.add(criterion).setProjection( Projections.projectionList()
				.add(Projections.groupProperty("jobId"))
				.add(Projections.count("jobId"))) ; 

		return c.list();

	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplicantsByJobOrdersHQL(int status,List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:21");
		Session session = getSession();

		String sql =" select jft.jobId,count(*) as appliedCnt,COUNT(IF(jft.status="+status+",1,NULL)) AS hiredCnt,MAX( lastActivityDate ),lastActivity,lastActivityDate,lastActivityDoneBy,jobForTeacherId  " +
		" ,COUNT(IF(teacherNormScore>If(jo.createdForEntity=3,sm.jobFeedCriticalNormScore,dm.jobFeedCriticalNormScore),1,NULL)) AS normCandidateCriticalCnt," +
		" COUNT(IF(teacherNormScore>If(jo.createdForEntity=3,sm.jobFeedAttentionNormScore,dm.jobFeedAttentionNormScore),1,NULL)) AS normCandidateAttentionCnt " +
		" from jobforteacher jft left join teachernormscore tns on jft.teacherId=tns.teacherId join joborder jo on jft.jobId=jo.jobId left join schoolinjoborder sij on jo.jobId=sij.jobId " +
		" join districtmaster dm on jo.districtId = dm.districtId " +
		" left join schoolmaster sm on sij.schoolId = sm.schoolId " +
		" where jft.jobId IN (:jobOrders) group by jft.jobId ";
		Query query = session.createSQLQuery(sql);

		query.setParameterList("jobOrders", jobOrders);

		List<Object[]> rows = query.list();
		return rows;

	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplicantsByJobOrdersSchoolHQL(int status,List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:22");
		Session session = getSession();

		String sql =" select jft.jobId,count(*) as appliedCnt,COUNT(IF(jft.status="+status+",1,NULL)) AS hiredCnt,MAX( lastActivityDate ),lastActivity,lastActivityDate,lastActivityDoneBy,jobForTeacherId  " +
		" ,COUNT( IF( teacherNormScore > sm.jobFeedCriticalNormScore, 1, NULL ) ) AS normCandidateCriticalCnt," +
		" COUNT( IF( teacherNormScore > sm.jobFeedAttentionNormScore, 1, NULL ) ) AS normCandidateAttentionCnt" +
		" ,sm.schoolId from jobforteacher jft left join teachernormscore tns on jft.teacherId=tns.teacherId join joborder jo on jft.jobId=jo.jobId left join schoolinjoborder sij on jo.jobId=sij.jobId " +
		" join districtmaster dm on jo.districtId = dm.districtId " +
		" join schoolmaster sm on sij.schoolId = sm.schoolId " +
		" where jft.jobId IN (:jobOrders) group by jft.jobId ";
		Query query = session.createSQLQuery(sql);

		query.setParameterList("jobOrders", jobOrders);

		List<Object[]> rows = query.list();
		return rows;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplicantsByJobOrdersBySchoolHQL(int status,JobOrder jobOrder,SchoolMaster schoolMaster) 
	{
		PrintOnConsole.getJFTPrint("JFT:23");
		Session session = getSession();

		String sql =" select jft.jobId,count(*) as appliedCnt,COUNT(IF(jft.status="+status+",1,NULL)) AS hiredCnt,MAX( lastActivityDate ),lastActivity,lastActivityDate,lastActivityDoneBy,jobForTeacherId  " +
		" ,COUNT( IF( teacherNormScore > sm.jobFeedCriticalNormScore, 1, NULL ) ) AS normCandidateCriticalCnt," +
		" COUNT( IF( teacherNormScore > sm.jobFeedAttentionNormScore, 1, NULL ) ) AS normCandidateAttentionCnt" +
		" ,sm.schoolId from jobforteacher jft left join teachernormscore tns on jft.teacherId=tns.teacherId join joborder jo on jft.jobId=jo.jobId left join schoolinjoborder sij on jo.jobId=sij.jobId " +
		" join districtmaster dm on jo.districtId = dm.districtId " +
		" join schoolmaster sm on sij.schoolId = sm.schoolId " +
		" where jft.jobId =:jobOrder and schoolMaster=:schoolMaster group by jft.jobId ";

		Query query = session.createSQLQuery(sql);

		query.setParameter("jobOrder", jobOrder);
		query.setParameter("schoolMaster", schoolMaster);

		List<Object[]> rows = query.list();
		return rows;
	}

	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,String> countApplicantsByJobOrdersHQL(List<JobOrder> jobOrders,String hrdstatus,String hidestatus) 
	{
		PrintOnConsole.getJFTPrint("JFT:24");
		Session session = getSession();
		String sql =" SELECT jft.jobId, COUNT(*) AS appliedCnt, COUNT( IF( jft.status ="+hrdstatus+", 1, NULL ) ) AS hiredCnt " +
		" from jobforteacher jft where jft.jobId IN (:jobOrders) and jft.status!="+hidestatus+" group by jft.jobId ";
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);

		List<Object[]> rows = query.list();
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], obj[1]+"##"+obj[2]);
			}
		}
		return map;
	}
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplidJobsHQL(DistrictMaster districtMaster,SchoolMaster schoolMaster,List<TeacherDetail> teacherDetails) 
	{
		PrintOnConsole.getJFTPrint("JFT:25");
		Session session = getSession();
		String sql = "";
		if(districtMaster!=null && schoolMaster==null)
		{
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT(IF(jo.districtId="+districtMaster.getDistrictId()+" and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}if(schoolMaster!=null)
		{
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT( IF( jo.jobId IN ( SELECT jobId FROM schoolinjoborder WHERE schoolId ="+schoolMaster.getSchoolId()+" ) and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}


		Query query = session.createSQLQuery(sql);
		String[] statuss = {"icomp","comp","hird","vlt","scomp","ecomp","vcomp","dcln","rem"};
		//List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		query.setParameterList("teacherDetails", teacherDetails);
		query.setParameterList("statuss", statusMasters );

		List<Object[]> rows = query.list();
		return rows;

	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List findTotalCandidateCountByJobIds(String jobIds) 
	{
		PrintOnConsole.getJFTPrint("JFT:26");
		Session session = getSession();
		String sql = "";
		List<Object[]> rows=null;
	
		
		if(jobIds!=""){
			sql = "select jft.jobId,COUNT(*) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where jft.jobId IN  ( "+jobIds +")  and jft.status != 8  group by jobId";
		
		if(!sql.equals(""))
		{
			Query query = session.createSQLQuery(sql);
			rows = query.list();
		}
		}
		return rows;

	}
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List findAvlCandidateCountByJobIds(String jobIds) 
	{
		PrintOnConsole.getJFTPrint("JFT:27");
		Session session = getSession();
		String sql = "";
		List<Object[]> rows=null;
	
		
		if(jobIds!=""){
			sql = "select jft.jobId,COUNT(*) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where jft.jobId IN  ( "+jobIds +") and jft.status NOT IN(:statuss) group by jobId";
		
			if(!sql.equals(""))
			{
				Query query = session.createSQLQuery(sql);
				String[] statuss = {"icomp","vlt","widrw","hide"};
				//List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
				List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
				
				query.setParameterList("statuss", statusMasters );
				rows = query.list();
				
			}
		}
		
		return rows;

	}
	
	
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List getLastActivityOnJobOrders(List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:28");
		Session session = getSession();

		Criterion criterion = Restrictions.in("jobId", jobOrders);		
		Criteria c= session.createCriteria(getPersistentClass());

		c.add(criterion).setProjection( Projections.projectionList()
				.add(Projections.max("lastActivityDate"))
				.add(Projections.property("jobForTeacherId"))
				.add(Projections.property("lastActivity"))
				.add(Projections.property("lastActivityDate"))
				//.add(Projections.property("lastActivityDoneBy"))
				.add(Projections.groupProperty("jobId"))) ; 

		return c.list();

	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndSatusList(TeacherDetail teacherDetail, List<StatusMaster> statusMasters)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(statusMasters.size()>0)
			{
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				Criterion criterion2 = Restrictions.in("status",statusMasters);
				Criterion criterion3 = Restrictions.and(criterion1,criterion2);

				lstJobForTeacher = findByCriteria(Order.desc("createdDateTime"),criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersBySubject(SubjectMaster subjectMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:30");
		try 
		{
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass()); 
			Criteria incriteria = criteria.createCriteria("jobId");
			incriteria.add(Restrictions.eq("subjectMaster",subjectMaster));
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherId"))
			);

			return criteria.list();
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findTeachersBySubjectList(List<SubjectMaster> subjectMasters)
	{
		PrintOnConsole.getJFTPrint("JFT:31");
		try 
		{
			Session session = getSession();
			List<TeacherDetail> results = new ArrayList<TeacherDetail>();
			if(subjectMasters.size()>0)
			{
				Criteria criteria= session.createCriteria(getPersistentClass()); 
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				criteria.add(Restrictions.ne("status",statusMaster));
				
				Criteria incriteria = criteria.createCriteria("jobId");
				incriteria.add(Restrictions.in("subjectMaster",subjectMasters));
				criteria.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherId"))
				);
				results = criteria.list();
			}
			return results;
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherIDs(Long[] teacherIds)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("jobForTeacherId",teacherIds);
			lstJobForTeacher = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findInternalCandidate(List<TeacherDetail> lstteacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstteacherDetail.size()>0)
			{
				Criterion criterion_tId = Restrictions.in("teacherId",lstteacherDetail);
				Criterion criterion_IsAffi = Restrictions.eq("isAffilated", 1); 
				lstJobForTeacher = findByCriteria(criterion_tId,criterion_IsAffi);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			
			return lstJobForTeacher;
		}
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findDistrictOrSchoolAppliedJobByTeacher(TeacherDetail teacherDetail,DistrictMaster districtMaster,List<StatusMaster> lstStatusMasters)
	{
		PrintOnConsole.getJFTPrint("JFT:32");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.in("status", lstStatusMasters);

			criteria.add(criterion1);
			if(lstStatusMasters.size()>0)
				criteria.add(criterion2);
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			//lstJobForTeacher = findByCriteria(criterion3);	

			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> findDistrictOrSchoolAppliedJobByTeacherAll(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,List<StatusMaster> lstStatusMasters)
	{
		PrintOnConsole.getJFTPrint("JFT:33");
		List<JobOrder> lstJobOrder= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.in("status", lstStatusMasters);

			criteria.add(criterion1);
			if(lstStatusMasters.size()>0)
				criteria.add(criterion2);
			if(headQuarterMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			else if(branchMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			else if(districtMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			criteria.setProjection((Projections.property("jobId"))) ; 
			lstJobOrder = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobOrder;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> findUniqueApplicantsbyJobOrders(List<JobOrder> lstJobOrder)
	{
		List<TeacherDetail> lstJobForTeacher= null;
		try 
		{

			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			//criteria.setResultTransformer(Criteria.PROJECTION);
			criteria.setProjection(Projections.distinct(Projections.property("teacherId")));
			lstJobForTeacher = criteria 
			.add(Restrictions.in("jobId",lstJobOrder)) 
			.list();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTbyJobOrders(List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			lstJobForTeacher = criteria 
			.add(Restrictions.in("jobId",lstJobOrder)) 
			.addOrder(Order.asc("jobId"))
			.list();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public JobForTeacher getJobForTeacherDetails(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		JobForTeacher jobForTeacher=null;
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher.size()==1)
			jobForTeacher=lstJobForTeacher.get(0);
		return jobForTeacher;
	}
	@Transactional(readOnly=false)	
	public List<JobForTeacher> findHireBySchool(JobOrder jobOrder,SchoolMaster schoolMaster) 
	{	

		//StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();
		try{
			Criterion criterion1 = Restrictions.eq("jobId", jobOrder);	
			Criterion criterion2 = Restrictions.eq("status", statusMaster);	
			Criterion criterion3 = Restrictions.eq("schoolMaster",schoolMaster);
			if(criterion3!=null)
				lstJobForTeachers = findByCriteria(criterion1,criterion2,criterion3);
			else
				lstJobForTeachers = findByCriteria(criterion1,criterion2);

		}catch(Exception e){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}
	/* Sekhar : Get getJobForTeacherInternalList from getJobForTeacherInternalList ========== */
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherInternalList(List jobforteacherlistteacherIdList)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("jobForTeacherId",jobforteacherlistteacherIdList);
			Criterion criterion2= Restrictions.eq("isAffilated",new Integer(1));
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherExternalList(List jobforteacherlistteacherIdList)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("jobForTeacherId",jobforteacherlistteacherIdList);
			Criterion criterion2= Restrictions.eq("isAffilated",new Integer(0));
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobAppliedByTeacherAndDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster,Boolean isFinalize)
	{
		PrintOnConsole.getJFTPrint("JFT:35");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);

			if(isFinalize!=null)
			{
				Criterion criterion2 = Restrictions.eq("isDistrictSpecificNoteFinalize",isFinalize);
				criteria.add(criterion2);
			}

			criteria.add(criterion1);

			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));

			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	/* @Start
	 * @Ashish Kumar
	 * @Description :: Find JobForTeacher Data By Teacher ID And District Master 
	 * */

	@Transactional(readOnly=false)	
	public List<JobForTeacher> findByTeacherIdDistrictId(TeacherDetail teacherDetail,DistrictMaster districtMaster) 
	{	
		PrintOnConsole.getJFTPrint("JFT:36");
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}

			criteria.addOrder(Order.desc("createdDateTime"));
			lstJobForTeachers = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}

	/* @End
	 * @Ashish Kumar
	 * @Description :: Find JobForTeacher Data By Teacher ID And District Master 
	 * */
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobsForJobSpecificInventoryAll(TeacherDetail teacherDetail,Boolean assesmentStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:37");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			Session session = getSession();

			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			String[] statuss = {"hide","widrw"};
			Criterion criterion2 = Restrictions.not(Restrictions.in("status", Utility.getStaticMasters(statuss)));
			criteria.add(criterion2);
			criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime))
			.add(Restrictions.eq("status", "A"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByJIDsAndTID_msu(TeacherDetail teacherDetail,List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.in("jobId",lstJobOrder);

			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacersByDistrict(DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:38");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			criteria.addOrder(Order.asc("teacherId"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findWeeklyJobsByDistrict(DistrictMaster districtMaster2)
	{
		List<JobForTeacher> jobForTeacherList=new ArrayList<JobForTeacher>();
		try 
		{
			List<JobOrder> jobOrderList=jobOrderDAO.findByCriteria(Restrictions.eq("districtMaster", districtMaster2));
			
     		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			String s1=dateFormat.format(cal.getTime());
			Date d1 = dateFormat.parse(s1);
			System.out.println(":: "+d1);
			cal.add(Calendar.DATE, -7); 
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			System.out.println(":: "+d2);
		
			Criterion  criterion1=Restrictions.between("createdDateTime",d2,d1);
			Criterion criterion2=Restrictions.in("jobId", jobOrderList);
			
			jobForTeacherList = findByCriteria(criterion1,criterion2);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobForTeacherList;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findAllJobForTeacherByDistrictId(List<Long> jftIds,List<StatusMaster> lstStatusMasters,DistrictMaster districtMaster,TeacherDetail teacherDetail,boolean ssnFlag,String teacherFNames,String teacherLNames,String position,Order order,int startPos,int limit,ArrayList<JobOrder> listJobOrderIds)
	{
		PrintOnConsole.getJFTPrint("JFT:39");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			
			if(jftIds.size()>0){
				criteria.add(Restrictions.not(Restrictions.in("jobForTeacherId",jftIds)));
			}
			criteria.add(Restrictions.isNotNull("requisitionNumber"));
			criteria.add(Restrictions.in("status",lstStatusMasters));
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			 if(listJobOrderIds!=null && listJobOrderIds.size()>0)
		         criteria.add(Restrictions.in("jobId", listJobOrderIds));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			
			if(teacherDetail!=null || ssnFlag){
				criteria.add(Restrictions.eq("teacherId",teacherDetail));
			}
			if((teacherFNames!= null && !teacherFNames.equals(""))&&(teacherLNames!= null && !teacherLNames.equals(""))){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName","%"+teacherFNames+"%")).add(Restrictions.like("lastName","%"+teacherLNames+"%"));
			}else if(teacherFNames!= null && !teacherFNames.equals("")){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName","%"+teacherFNames+"%"));
			}else if(teacherLNames!= null && !teacherLNames.equals("")){
				criteria.createCriteria("teacherId").add(Restrictions.like("lastName","%"+teacherLNames+"%"));
			}	
			if(position!= null && !position.equals("")){
				criteria.add(Restrictions.like("requisitionNumber","%"+position+"%"));
			}	
			/*criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit*2);*/	
			criteria.addOrder(order);
			criteria.addOrder(Order.asc("createdDateTime"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherActiveJob(TeacherDetail teacherDetail)
	{
		PrintOnConsole.getJFTPrint("JFT:40");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
		// Sandeep All Job Show on DashBoard 31-08-15	
			//criteria.createCriteria("jobId").add(Restrictions.eq("status","A"));
			lstJobForTeacher = criteria.list();
			System.out.println("List Size ...."+lstJobForTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
		
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getOfferReadyDate(List<JobOrder> jobOrders)
	{
		List<JobForTeacher> jobForTeachers = new ArrayList<JobForTeacher>();
		try{
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -2); 
			String s2=dateFormat.format(cal.getTime());
			Date d2 = dateFormat.parse(s2);
			System.out.println(":: "+d2);
			Session session = getSession();
			Criterion criterion1 = Restrictions.isNull("noResponseEmail");
			Criterion criterion2 = Restrictions.eq("noResponseEmail",false);
			
			
				Criterion criterion3 = Restrictions.in("jobId",jobOrders);
			
			List result = session.createCriteria(getPersistentClass()) 
			.add(Restrictions.eq("offerReady",true)) 
			.add(Restrictions.or(criterion1,criterion2)) 
			.add(Restrictions.isNull("offerAccepted")) 
			.add(Restrictions.le("offerMadeDate",d2))
			.add(criterion3)
			.list();
			jobForTeachers = result;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return jobForTeachers;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getTeacherIdsAndJobIds(List<TeacherDetail> lstTeacherDetail, List<JobOrder> jobOrders)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetail);
			Criterion criterion2 = Restrictions.in("jobId",jobOrders);
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);
			lstJobForTeacher = findByCriteria(criterion3);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndJobCategory(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,String jobForTeacherId)
	{
		PrintOnConsole.getJFTPrint("JFT:41");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster)).add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	

	@Transactional(readOnly=false)
	public List<JobForTeacher> findbyJobOrder(JobOrder jobOrder)
	{
		List<JobForTeacher> JobForTeacherApplicant= null;
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);		
			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);

			JobForTeacherApplicant = findByCriteria(Order.asc("createdDateTime"),criterion1,criterion2);		
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return JobForTeacherApplicant;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacherByTeacherDeatail(List<TeacherDetail> lstTeacherDetails)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			//StatusMaster statusHide = WorkThreadServlet.statusMap.get("hide");
			//StatusMaster statusWithdraw = WorkThreadServlet.statusMap.get("widrw");
			Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
			//?/Criterion criterion2 = Restrictions.ne("status",statusHide);
			//Criterion criterion3 = Restrictions.ne("status",statusWithdraw);
			lstJobForTeacher = findByCriteria(criterion1 );		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster,String jobForTeacherId)
	{
		PrintOnConsole.getJFTPrint("JFT:42");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacersByDistrictAndSchool(DistrictMaster districtMaster,SchoolMaster schoolId)
	{
		PrintOnConsole.getJFTPrint("JFT:43");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			criteria.addOrder(Order.asc("teacherId"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByJOB(List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
		//	Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.in("jobId",lstJobOrder);
            
			lstJobForTeacher = findByCriteria(criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacersByDistrictStatus(DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:44");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			criteria.createCriteria("jobId").add(Restrictions.eq("status", "A"));
			criteria.addOrder(Order.asc("teacherId"));
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacersByDistrictStatusAll(DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:44");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			System.out.println("findTeacersByDistrictStatusAll------Start");
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			criteria.addOrder(Order.asc("teacherId"));
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			//criteria.setMaxResults(10);
			lstJobForTeacher = criteria.list();
			System.out.println("findTeacersByDistrictStatusAll------End"+"-------lstJobForTeacher.size----"+lstJobForTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByJOBStatus(List<JobOrder> lstJobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
		//	Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",lstJobOrder);
            
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findTeacherByTeacherDeatailAndJobStatus(List<TeacherDetail> lstTeacherDetails)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion2 = Restrictions.ne("status",statusMaster);
			lstJobForTeacher = findByCriteria(criterion1,criterion2 );		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findbyJobOrderAndStatus(List<Integer> jobIds)
	{
		List<JobForTeacher> JobForTeacherApplicant= new ArrayList<JobForTeacher>();
		try 
		{
			if(jobIds.size()>0){
				List<JobOrder> listJobOrders = jobOrderDAO.findByJobIDS(jobIds);
				 if(listJobOrders.size()>0){
					StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
					Criterion criterion1 = Restrictions.ne("status",statusMaster);		
					Criterion criterion2 = Restrictions.in("jobId",listJobOrders);
					JobForTeacherApplicant = findByCriteria(criterion1,criterion2);	
				 }
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return JobForTeacherApplicant;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findBySecondStatus(List<SecondaryStatus> lstseSecondaryStatus)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstseSecondaryStatus.size()>0){
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.ne("status",statusMaster);
				Criterion criterion2 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion3 = Restrictions.isNull("statusMaster");
				lstJobForTeacher = findByCriteria(criterion1,criterion2,criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findByStatus(StatusMaster statusMaster1)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			List<StatusMaster> statusList =new ArrayList<StatusMaster>();
			
			statusList.add(statusMaster1);
		
			//StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
			StatusMaster statusHide= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusHide);
			Criterion criterion2 = Restrictions.eq("statusMaster",statusMaster1);
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findAvailableCandidte(DistrictMaster  districtMaster,StatusMaster statusMaster2)
	{
		PrintOnConsole.getJFTPrint("JFT:45");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("status",statusMaster2);
			Criterion criterion2 = Restrictions.eq("statusMaster",statusMaster2);
			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();
			
					
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndJobTitle(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,String jobForTeacherId,String jobTitle)
	{
		PrintOnConsole.getJFTPrint("JFT:46");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster)).add(Restrictions.eq("districtMaster",districtMaster)).add(Restrictions.like("jobTitle","%"+jobTitle+"%"));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTIDsAndJID(List<TeacherDetail> lstTeacherDetail, JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			if(lstTeacherDetail.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetail);
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.and(criterion1,criterion2);
				lstJobForTeacher = findByCriteria(criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> OfferCandidateList(List<TeacherDetail> lstTeacherDetails)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstTeacherDetails.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
				Criterion criterion2 = Restrictions.eq("offerReady",true);
				Criterion criterion3 = Restrictions.eq("offerReady",false);
				Criterion criterion4 = Restrictions.or(criterion3,criterion2);
				lstJobForTeacher = findByCriteria(criterion1,criterion4);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTIDsAndSID_CGMass(List<TeacherDetail> lstTeacherDetails,List<StatusMaster> lstStatusMasters,JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(lstStatusMasters!=null && lstStatusMasters.size() >0 && lstTeacherDetails!=null && lstTeacherDetails.size()>0)
			{
				Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetails);
				Criterion criterion2 = Restrictions.in("status", lstStatusMasters);
				Criterion criterion3 = Restrictions.eq("jobId",jobOrder);
				lstJobForTeacher=findByCriteria(criterion1,criterion2,criterion3);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobForTeacherbyInputDateFilterwithDistrict(DistrictMaster districtMaster ,String sfromDate, String stoDate,String endfromDate, String endtoDate, String appsfromDate, String appstoDate,List<TeacherDetail> lstTeacherDetails, String normScoreSelectVal, int intenalchk)
	{
		PrintOnConsole.getJFTPrint("JFT:47");
		List<JobForTeacher> lstjft1 = new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionhide = Restrictions.ne("status",statusMaster);
			
			Criterion internalCriterion1=null;
			Criterion internalCriterion2=null;
			Criterion internalCriterion=null;
			
			if(intenalchk==1)
			 internalCriterion = Restrictions.eq("isAffilated",intenalchk);
			else if(intenalchk==0){
				internalCriterion1 = Restrictions.eq("isAffilated",intenalchk);
				internalCriterion2 = Restrictions.eq("isAffilated",null);
				internalCriterion = Restrictions.or(internalCriterion1, internalCriterion2);
			}
			if(internalCriterion!=null)
			criteria.add(internalCriterion);
			
			//System.out.println("111111111");
			Date fDate=null;
			Date tDate=null;
			Date endfDate=null;
			Date endtDate =null;
			Date appliedfDate=null;
			Date appliedtDate=null;
			
			
			if(!sfromDate.equals(""))
			 fDate = Utility.getCurrentDateFormart(sfromDate);
			if(!stoDate.equals(""))
			 tDate = Utility.getCurrentDateFormart(stoDate);
			if(!endfromDate.equals(""))
			 endfDate = Utility.getCurrentDateFormart(endfromDate);
			if(!endtoDate.equals(""))
			 endtDate= Utility.getCurrentDateFormart(endtoDate);
			//String appsfromDate, String appstoDate
			if(!appsfromDate.equals("")){
				appliedfDate= Utility.getCurrentDateFormart(appsfromDate);
				appliedfDate.setHours(0);appliedfDate.setMinutes(0);appliedfDate.setSeconds(0);
			}
			if(!appstoDate.equals("")){
				appliedtDate= Utility.getCurrentDateFormart(appstoDate);
				appliedtDate.setHours(23);appliedtDate.setMinutes(59);appliedtDate.setSeconds(59);
			}
			
			 if(!normScoreSelectVal.equals("") && !normScoreSelectVal.equals("0") && !normScoreSelectVal.equals("6"))
			 {
				 if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
			       criteria.add(Restrictions.in("teacherId", lstTeacherDetails));
				 else
				  return lstjft1;
			 }
			 if(normScoreSelectVal.equals("6")){
				 criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetails)));
			 }
			criteria.add(criterionhide);
			Criteria c2 = criteria.createCriteria("jobId");
					
				if(districtMaster!=null)
					c2.add(Restrictions.eq("districtMaster", districtMaster));
				
				if(fDate!=null && tDate!=null){
					c2.add(Restrictions.ge("jobStartDate",fDate)).add(Restrictions.le("jobStartDate",tDate));
				}else if(fDate!=null && tDate==null){
					c2.add(Restrictions.ge("jobStartDate",fDate));
				}else if(fDate==null && tDate!=null){
					c2.add(Restrictions.le("jobStartDate",tDate));
				}
					
				if(endfDate!=null && endtDate!=null){
					c2.add(Restrictions.ge("jobEndDate",endfDate)).add(Restrictions.le("jobEndDate",endtDate));
				}else if(endfDate!=null && endtDate==null){
					c2.add(Restrictions.ge("jobEndDate",endfDate));
				}else if(endfDate==null && endtDate!=null){
					c2.add(Restrictions.le("jobEndDate",endtDate));
				}
				
				if(appliedfDate!=null && appliedtDate!=null){
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				}else if(appliedfDate!=null && appliedtDate==null){
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				}else if(appliedtDate!=null && appliedfDate==null){
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
			  
			   lstjft1= criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstjft1;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherListPC(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster,List<StatusMaster> listStatusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:48");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
					//criteria.createCriteria("status").add(Restrictions.ne("statusShortName","hird"));
				if(listStatusMaster!=null)
					criteria.add(Restrictions.not(Restrictions.in("status",listStatusMaster)));
				
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherListOC(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster,List<StatusMaster> listStatusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:49");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null)
				{
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					criteria.createCriteria("jobId").add(Restrictions.ne("jobType", "F"));
				}
				
					//criteria.createCriteria("status").add(Restrictions.ne("statusShortName","hird"));
				if(listStatusMaster!=null)
					criteria.add(Restrictions.not(Restrictions.in("status",listStatusMaster)));
					
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getQQFinalizedTeacherList(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:50");
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				
				criteria.setProjection(Projections.groupProperty("teacherId"));
					
				lstTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByJobCategory(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		PrintOnConsole.getJFTPrint("JFT:51");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster())).add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherListExIn(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:52");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findAvailableCandidtes(DistrictMaster  districtMaster,List<StatusMaster> statusMasterList,List<SecondaryStatus> lstseSecondaryStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:53");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    //StatusMaster statusMaster3= WorkThreadServlet.statusMap.get("hide");
		    StatusMaster statusMaster3= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionHide = Restrictions.ne("status",statusMaster3);
			Criterion criterionAll=null;
			if( statusMasterList.size()>0 &&  lstseSecondaryStatus.size()>0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				Criterion criterionStatus = Restrictions.and(criterion1, criterion2);
			
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				Criterion criterionSecStatus = Restrictions.and(criterion4, criterion5);
				
				criterionAll = Restrictions.or(criterionStatus, criterionSecStatus);
			}
			if(statusMasterList.size()>0 &&  lstseSecondaryStatus.size()==0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				criterionAll = Restrictions.and(criterion1, criterion2);
			}
			
			if(statusMasterList.size()==0 &&  lstseSecondaryStatus.size()>0)
			{
				
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				criterionAll = Restrictions.and(criterion4, criterion5);
				
			}
			
			criteria.add(criterionAll);
			criteria.add(criterionHide);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findAvailableCandidtesForReport(DistrictMaster  districtMaster,List<StatusMaster> statusMasterList,List<SecondaryStatus> lstseSecondaryStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:54");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    //StatusMaster statusMaster3= WorkThreadServlet.statusMap.get("hide");
		    StatusMaster statusMaster3= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionHide = Restrictions.ne("status",statusMaster3);
			Criterion criterionAll=null;
			if( statusMasterList.size()>0 &&  lstseSecondaryStatus.size()>0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				Criterion criterionStatus = Restrictions.and(criterion1, criterion2);
			
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				Criterion criterionSecStatus = Restrictions.and(criterion4, criterion5);
				
				criterionAll = Restrictions.or(criterionStatus, criterionSecStatus);
			}
			if(statusMasterList.size()>0 &&  lstseSecondaryStatus.size()==0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				criterionAll = Restrictions.and(criterion1, criterion2);
			}
			
			if(statusMasterList.size()==0 &&  lstseSecondaryStatus.size()>0)
			{
				
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				criterionAll = Restrictions.and(criterion4, criterion5);
				
			}
			
			criteria.add(criterionAll);
			criteria.add(criterionHide);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	/*****************/
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getHiredRequisitionNumbers(List<String> requisitionNumbers,List<JobOrder> jobOrder)
	{
		List<JobForTeacher> lstHiredRequisitionNumbers= null;
		
		try 
		{
			Criterion criterion1 = Restrictions.in("requisitionNumber",requisitionNumbers);			
			//Criterion criterion2 = Restrictions.eq("statusMaster",requisitionNumbers);
			//Criterion criterion2 = Restrictions.eq("status",status);
			Criterion criterion2 = Restrictions.in("jobId",jobOrder);
			
			lstHiredRequisitionNumbers = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstHiredRequisitionNumbers;
	}
	
	
	/*
	 * for Offer Ready
	 * */
		@Transactional(readOnly=false)
		public List<JobForTeacher> findJFTforOfferReadystatus(DistrictMaster districtMaster ,Order sortOrderStrVal,List<JobOrder> jobOrders, boolean sortingcheck)
		{
			PrintOnConsole.getJFTPrint("JFT:55");
			List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				//criteria.addOrder(Order.asc("teacherId"));
				
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.ne("status",statusMaster);
				criteria.add(criterion1);
				
				Criterion isnotNullCriterion    =Restrictions.isNotNull("offerReady");
				criteria.add(isnotNullCriterion);
				
				if(!sortingcheck){
				   //System.out.println("sortingchecksortingchecksortingchecksortingchecksortingcheck true");
				 criteria.addOrder(sortOrderStrVal);
				}
				
				if(jobOrders!=null && jobOrders.size()>0)
					criteria.add(Restrictions.in("jobId",jobOrders));
				
				lstJobForTeacher = criteria.list();
				System.out.println("offerReadyofferReady :: "+lstJobForTeacher.size());
				
				
				
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}
		
		@Transactional(readOnly=false)
		public List<JobForTeacher> getRequisitionNumbers(JobOrder jobOrder,String requisitionNumbers,TeacherDetail teacherDetail)
		{
			List<JobForTeacher> lstHiredRequisitionNumbers= new ArrayList<JobForTeacher>();
			try 
			{
				Criterion criterion1 = Restrictions.eq("requisitionNumber",requisitionNumbers);	
				if(jobOrder!=null && teacherDetail!=null){
					Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
					Criterion criterion3 = Restrictions.eq("teacherId",teacherDetail);
					if(requisitionNumbers!=null){
						lstHiredRequisitionNumbers = findByCriteria(criterion1,criterion2,criterion3);
					}else{
						lstHiredRequisitionNumbers = findByCriteria(criterion2,criterion3);
					}
				}else if(jobOrder!=null){
					Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
					if(requisitionNumbers!=null){
						lstHiredRequisitionNumbers = findByCriteria(criterion1,criterion2);
					}else{
						lstHiredRequisitionNumbers = findByCriteria(criterion2);
					}
				}else if(teacherDetail!=null){
					Criterion criterion3 = Restrictions.eq("teacherId",teacherDetail);
					if(requisitionNumbers!=null){
						lstHiredRequisitionNumbers = findByCriteria(criterion1,criterion3);
					}else{
						lstHiredRequisitionNumbers = findByCriteria(criterion3);
					}
				}else{
					lstHiredRequisitionNumbers = findByCriteria(criterion1);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstHiredRequisitionNumbers;
		}
		
		@Transactional(readOnly=false)
		public List<JobForTeacher> findJobByTeacherForOffers(List<TeacherDetail> teacherDetails)
		{
			List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
			try 
			{
				if(teacherDetails!=null && teacherDetails.size() >0)
				{
					//StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
					StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
					Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
					Criterion criterion2 = Restrictions.not(Restrictions.isNull("requisitionNumber"));	
					Criterion criterion3 = Restrictions.not(Restrictions.eq("requisitionNumber",""));
					Criterion criterion4 = Restrictions.or(criterion3,criterion2);
					Criterion criterion5 = Restrictions.eq("status",statusMaster);
					lstJobForTeacher = findByCriteria(criterion1,criterion4,criterion5);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}
		
		/*Applicant status count */
		@Transactional(readOnly=false)
		public List fintApplicntByStatus(DistrictMaster districtMaster,String jobStatus,int sortingchk, Order sortOrderStrVal,List<JobOrder> lstJobOrders)
		{
			PrintOnConsole.getJFTPrint("JFT:57");
			List resultList =new ArrayList();
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());				
				Criteria c1= criteria.createCriteria("jobId");
				         //c1.add(Restrictions.eq("districtMaster", districtMaster));				       
				         if(sortingchk==1)
				         c1.addOrder(sortOrderStrVal);
				
				criteria.add(Restrictions.eq("districtId",districtMaster.getDistrictId()));
				         
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion = Restrictions.ne("status",statusMaster);
				criteria.add(criterion);
				
				if(lstJobOrders!=null && lstJobOrders.size()>0)// 
				{
					criteria.add(Restrictions.in("jobId",lstJobOrders));
				}
				
				if(!jobStatus.equalsIgnoreCase("All")){
					jobStatus = jobStatus.trim();
					c1.add(Restrictions.eq("status", jobStatus));
				}
					
				criteria.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("jobId"))
						.add(Projections.groupProperty("secondaryStatus"))
						.add(Projections.groupProperty("statusMaster"))
						.add(Projections.count("jobId"))
						.add(Projections.count("secondaryStatus"))
						.add(Projections.count("statusMaster")));
				if(lstJobOrders!=null && lstJobOrders.size()>0)						
				resultList = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return resultList;
		}
	
		
	/********Hired Candidate with and without district ********swadesh*/	
		@Transactional(readOnly=false)
		public List<JobForTeacher> hiredCandidateListWithAndWithoutDistrict(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal,Date hiredStartdate,Date hiredEndDtae)
		{
			PrintOnConsole.getJFTPrint("JFT:58");
		
			List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
				Criterion hiredcriterion = Restrictions.eq("status", statusMaster);
				//***********SWADESH*****************/
				if(hiredStartdate!=null && hiredEndDtae!=null)
					criteria.add(Restrictions.ge("hiredByDate",hiredStartdate)).add(Restrictions.le("hiredByDate",hiredEndDtae));
				else if(hiredStartdate!=null && hiredEndDtae==null)
					criteria.add(Restrictions.ge("hiredByDate",hiredStartdate));
				else if(hiredEndDtae!=null && hiredStartdate==null)
					criteria.add(Restrictions.le("hiredByDate",hiredEndDtae));
				//////////******************************************/
				criteria.add(hiredcriterion);
				Criteria c1 = null;
				Criteria c2 = null;
				c1 = criteria.createCriteria("jobId");
				c2 = criteria.createCriteria("teacherId");
				if(districtMaster!=null)
				 c1.add(Restrictions.eq("districtMaster",districtMaster));
				
				if(sortingcheck==1){
					if(c2!=null)
					c2.addOrder(sortOrderStrVal);
				}
				else if(sortingcheck==2){
					if(c1!=null)
					c1.addOrder(sortOrderStrVal);
				}else if(sortingcheck==3){
					criteria.addOrder(sortOrderStrVal);
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
			return lstJobForTeacher;
		
		}
		
	//find total/available/hired candidates in a job	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,String> countApplicantsByJobOrdersAllll(List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:59");
		Session session = getSession();
		String sql =" SELECT jft.jobId, COUNT(*) AS appliedCnt, COUNT( IF( jft.status NOT IN (:statuss), 1, NULL )) AS avlCnt, COUNT( IF( jft.status =6, 1, NULL ) ) AS hiredCnt " +
		" from jobforteacher jft where jft.jobId IN (:jobOrders) and jft.status!=8 group by jft.jobId ";
		
		String[] statuss = {"dcln","icomp","vlt","widrw","hide","rem"};
		//List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		query.setParameterList("statuss", statusMasters );
		
		System.out.println("query   "+query);
		List<Object[]> rows = query.list();
		
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], obj[1]+"##"+obj[2]+"##"+obj[3]);
			}
		}
		return map;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherIDExIn(TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:60");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(teacherDetail!=null && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				criteria.add(Restrictions.eq("teacherId",teacherDetail));
				criteria.add(Restrictions.ne("isAffilated",1));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}

/*Applicant status count by jobs*/
	@Transactional(readOnly=false)
	public List fintApplicntsStatusByJob(List<JobOrder> lstJobOrders)
	{
		PrintOnConsole.getJFTPrint("JFT:61");
		List resultList =new ArrayList();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionhide = Restrictions.ne("status",statusMaster);
			criteria.add(criterionhide);
			
			if(lstJobOrders!=null && lstJobOrders.size()>0)
			{
				criteria.add(Restrictions.in("jobId",lstJobOrders));
				criteria.addOrder(Order.asc("jobId"));
			}
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("jobId"))
					.add(Projections.groupProperty("secondaryStatus"))
					.add(Projections.groupProperty("statusMaster"))
					.add(Projections.count("secondaryStatus"))
					.add(Projections.count("statusMaster"))
			);
			resultList = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return resultList;
	}
	
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List<Integer> findJobOrderByCandidtesJobStatus(DistrictMaster  districtMaster,List<StatusMaster> statusMasterList,List<SecondaryStatus> lstseSecondaryStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:62");
		List<Integer> lstjoborders = new ArrayList<Integer>(); 
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionAll=null;
			if( statusMasterList.size()>0 &&  lstseSecondaryStatus.size()>0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				Criterion criterionStatus = Restrictions.and(criterion1, criterion2);
			
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				Criterion criterionSecStatus = Restrictions.and(criterion4, criterion5);
				criterionAll = Restrictions.or(criterionStatus, criterionSecStatus);
			}
			if(statusMasterList.size()>0 &&  lstseSecondaryStatus.size()==0)
			{
				Criterion criterion1 = Restrictions.in("status",statusMasterList);
				Criterion criterion2 = Restrictions.in("statusMaster",statusMasterList);
				criterionAll = Restrictions.and(criterion1, criterion2);
			}
			
			if(statusMasterList.size()==0 &&  lstseSecondaryStatus.size()>0)
			{
				Criterion criterion4 = Restrictions.in("secondaryStatus",lstseSecondaryStatus);
				Criterion criterion5 = Restrictions.isNull("statusMaster");
				criterionAll = Restrictions.and(criterion4, criterion5);
				
			}
			criteria.add(criterionAll);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("jobForTeacherId")));
			lstjoborders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstjoborders;
	}
	
	//for Average EPI/Norm Score by Job  
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countCandidatesNormHQLJobListWise(List<JobOrder> jobOrders) 
	{
		PrintOnConsole.getJFTPrint("JFT:63");
		Session session = getSession();
		String sql = "";
		/*sql = "SELECT jobId, sum(tns.teacherNormScore), count(jft.teacherId) as cnt  FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId " +
		        " WHERE  `jobId` IN (:jobOrders) and jft.status!=8  GROUP BY `jobId`";
		*/
		sql = "SELECT jobId, sum(tns.teacherNormScore), count(jft.teacherId) as cnt,displaySecondaryStatusId,displayStatusId  FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId " +
        " WHERE  `jobId` IN (:jobOrders) and jft.status!=8   GROUP BY `jobId`,displaySecondaryStatusId,displayStatusId";

		try {
			Query query = session.createSQLQuery(sql);
			query.setParameterList("jobOrders", jobOrders );
			List<Object[]> rows = query.list();
			return rows;
		  } catch (HibernateException e) {
			e.printStackTrace();
		  }
		return null;
	}
	
	//for Average EPI/Norm Score by Job  
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List avgFilterNormHQL(String normScoreVal,String normScoreSelectVal, DistrictMaster districtMaster,List<Integer> jftList, boolean jstSSatusFlag) 
	{
		PrintOnConsole.getJFTPrint("JFT:64");
		Session session = getSession();
		String sql = "";
		sql = "SELECT jo.jobId, avg(tns.teacherNormScore) as avgNorm  FROM  `jobforteacher` jft JOIN teachernormscore tns ON jft.teacherId = tns.teacherId  join joborder jo on jft.jobId=jo.jobId " +
		        " WHERE  jft.status!=8   ";
		
		if(districtMaster!=null){
			sql=sql+" and	jo.districtId=:districtMaster ";
		}
		if(jstSSatusFlag)
			sql=sql+" and jft.jobForTeacherId  IN (:jftList) ";
		
		sql = sql+ " GROUP BY jo.`jobId` ,displaySecondaryStatusId,displayStatusId HAVING "; 
		
		if(normScoreSelectVal.equals("1")){
			sql=sql+" avgNorm=:normScoreVal ";
		}else if(normScoreSelectVal.equals("2")){
			sql=sql+" avgNorm<:normScoreVal ";
		}else if(normScoreSelectVal.equals("3")){
			sql=sql+" avgNorm<=:normScoreVal ";
		}else if(normScoreSelectVal.equals("4")){
			sql=sql+" avgNorm>:normScoreVal ";
		}else if(normScoreSelectVal.equals("5")){
			sql=sql+" avgNorm>=:normScoreVal ";
		}
		
		try {
			Query query = session.createSQLQuery(sql);
			query.setParameter("normScoreVal", normScoreVal);
			if(jstSSatusFlag)
			   query.setParameterList("jftList", jftList);
			if(districtMaster!=null){
				query.setParameter("districtMaster", districtMaster);
			}
			List<Object[]> rows = query.list();
			return rows;
		  } catch (HibernateException e) {
			e.printStackTrace();
		  }
		return null;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findMostRecentJobByTeacher(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);

			criteria.add(criterion1);
			criteria.addOrder(Order.desc("createdDateTime"));
			criteria.setMaxResults(1);
			lstJobForTeacher = criteria.list();	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getHiredListByJFTs(List<Long> forTeachers,DistrictMaster districtMaster,List<TeacherDetail> teacherDetails)
	{
		PrintOnConsole.getJFTPrint("JFT:65");	
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(forTeachers!=null && forTeachers.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
				criteria.add(Restrictions.eq("status", statusMaster));
				
				if(forTeachers!=null && forTeachers.size()>0)
					criteria.add(Restrictions.not(Restrictions.in("jobForTeacherId",forTeachers)));
				
				if(teacherDetails!=null && teacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",teacherDetails));
				
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherListAndJobTitle(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,JobCategoryMaster jobCategoryMaster,List<Long> jobForTeacherIds,String jobTitle)
	{
		PrintOnConsole.getJFTPrint("JFT:66");	
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
			Criterion criterion2 = Restrictions.in("jobForTeacherId",jobForTeacherIds);
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster",jobCategoryMaster)).add(Restrictions.eq("districtMaster",districtMaster)).add(Restrictions.like("jobTitle","%"+jobTitle+"%"));
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
		@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,String> check(List<JobOrder> jobOrders) 
	{
			PrintOnConsole.getJFTPrint("JFT:67");	
		Session session = getSession();
		String sql =" SELECT jft.jobId, COUNT(*) AS appliedCnt, COUNT( IF( jft.status NOT IN (:statuss), 1, NULL )) AS avlCnt, COUNT( IF( jft.status =6, 1, NULL ) ) AS hiredCnt " +
		" from jobforteacher jft where jft.jobId IN (:jobOrders) and jft.status!=8 group by jft.jobId ";
		
		String[] statuss = {"dcln","icomp","vlt","widrw","hide","rem"};
		//List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		List<StatusMaster> statusMasters =   Utility.getStaticMasters(statuss);
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		query.setParameterList("statuss", statusMasters );
		
		List<Object[]> rows = query.list();
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], obj[1]+"##"+obj[2]+"##"+obj[3]);
			}
		}
		return map;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobsByTeacherListAndJobTitles(String queryString)
	{
		PrintOnConsole.getJFTPrint("JFT:68");	
		List<Integer> jobForTeacherIds= new ArrayList<Integer>();
		List<Long> jobForTeacherId= new ArrayList<Long>();
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try { 
			System.out.println("::::::::::::::::::InSide Job Method:::::::::::::=");
			String sql = "select jft.jobForTeacherId from jobforteacher jft join joborder jo on jo.jobId=jft.jobId  WHERE ("+queryString+")";
			System.out.println("sql:::::::-"+sql);
			Query query = session.createSQLQuery(sql);
			jobForTeacherIds= query.list();
		} catch (HibernateException e) {
			e.printStackTrace();
		}
		if(jobForTeacherIds.size()>0){
			for (Integer jId : jobForTeacherIds) {
				jobForTeacherId.add(Long.parseLong(jId+""));
			}
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion2 = Restrictions.in("jobForTeacherId",jobForTeacherId);
				criteria.add(criterion2);
				lstJobForTeacher = criteria.list();
				System.out.println("lstJobForTeacher::::::"+lstJobForTeacher.size());
	
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}

	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int getApplicatCount(int iJobId)
	{
		int iReturnValue=0;
		Session session = getSession();
		String sql =" SELECT COUNT(*) AS appliedCnt from jobforteacher jft where jft.jobId = "+iJobId+" and jft.status!=8";
		Query query = session.createSQLQuery(sql);
		List<BigInteger> rowCount = query.list();
		if(rowCount!=null && rowCount.size()==1)
			iReturnValue=rowCount.get(0).intValue();
		return iReturnValue;
	}

	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherListForOffers(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,StatusMaster statusMaster,StatusMaster secondaryStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:69");	
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			if(teacherDetails.size()>0){
				Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
				Criterion criterion2 = Restrictions.not(Restrictions.isNull("requisitionNumber"));	
				Criterion criterion3 = Restrictions.not(Restrictions.eq("requisitionNumber",""));
				Criterion criterion4 = Restrictions.or(criterion3,criterion2);
				Criterion criterion5 = Restrictions.eq("status",statusMaster);
				if(districtMaster!=null && districtMaster.getDistrictId()==1200390){
					Criterion criterion6 = Restrictions.eq("statusMaster",secondaryStatus);
					Criterion criterion7 = Restrictions.or(criterion5, criterion6);
					lstJobForTeacher = findByCriteria(criterion1,criterion4,criterion7);
				}else{
					lstJobForTeacher = findByCriteria(criterion1,criterion4,criterion5);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsData(Order sortOrderStrVal,int start,int end,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> lstTeacherDetailAllFilter,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,boolean normScoreFlag)
	{
		
		PrintOnConsole.getJFTPrint("JFT:71");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}
			
			Criterion criterionSchool =null;
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			}
			
			criteria.add(Restrictions.ne("status",statusMaster));
			
			
			if(fDate!=null && tDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
			else if(fDate!=null && tDate==null)
				criteria.add(Restrictions.ge("createdDateTime",fDate));
			else if(tDate!=null && fDate==null)
				criteria.add(Restrictions.le("createdDateTime",tDate));
			

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				}else if(entityID==4 && dateFlag){
					dateDistrictFlag=true;
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}*/
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				flag=true;
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				dateDistrictFlag=true;
			}
			else if(entityID==5)
			{

				System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
				flag=true;
				if(branchMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				else
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));

			}
			else if(entityID==6)
			{
				System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
				flag=true;
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			}
			if(dateDistrictFlag==false && dateFlag)
			{
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				flag=true;
			}
			if(status){
				criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			
			//job applied flag
			if(newcandidatesonlyNew)
			{
				if(appliedfDate!=null && appliedtDate!=null)
				{
					System.out.println("############### newcandidatesonlyNew FT #######################");
					criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedfDate!=null && appliedtDate==null)
				{
					System.out.println("############### newcandidatesonlyNew F #######################");
					//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedtDate!=null && appliedfDate==null)
				{
					System.out.println("############### newcandidatesonlyNew T #######################");
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
			}
			else
			{
				if(appliedfDate!=null && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				else if(appliedfDate!=null && appliedtDate==null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				else if(appliedtDate!=null && appliedfDate==null)
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}
			
			flag=true;
			if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
			{
				criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetailAllFilter))); // filter first time
			}

			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size() >0){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
					/*if(!sortOrderStrVal.toString().contains("spStatus"))
						criteria.addOrder(sortOrderStrVal);*/
				}
				/*else{
					if((headQuarterMaster==null || branchMaster==null) && !(sortOrderStrVal.toString().contains("spStatus") || sortOrderStrVal.toString().contains("normScore")))
					criteria.createCriteria("teacherId").addOrder(sortOrderStrVal);	
				}*/
				
				criteria.setProjection(Projections.groupProperty("teacherId"));
				if(!normScoreFlag){
					criteria.setFirstResult(start);
					criteria.setMaxResults(end);	
				}
				
				
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criteria.add(criterionSchool);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	
	
	@Transactional(readOnly=false)
	public List<Integer> getTeacherDetailByDASARecordsCount(Order sortOrderStrVal,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> lstTeacherDetailAllFilter,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP)
	{
		PrintOnConsole.getJFTPrint("JFT:72");	
		TestTool.getTraceTime("500");
		List<Integer> lstTeacherDetail= new ArrayList<Integer>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(internalCandVal==1){
				TestTool.getTraceTime("501");
				criteria.add(criterionInterCand);
			}

			criteria.add(Restrictions.ne("status",statusMaster));
			
			Criterion criterionSchool =null;
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			}
			
			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				TestTool.getTraceTime("502");
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				TestTool.getTraceTime("503");
				flag=true;
				if(lstJobOrder.size()>0)
				{
					TestTool.getTraceTime("504");
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					TestTool.getTraceTime("505");
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					TestTool.getTraceTime("506");
					dateDistrictFlag=true;
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}*/
					
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
					
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				TestTool.getTraceTime("507");
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				TestTool.getTraceTime("508");
				flag=true;
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				dateDistrictFlag=true;
			}
			else if(entityID==5)
			{
				System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
				flag=true;
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			}
			else if(entityID==6)
			{
				System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
				flag=true;
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			}
			if(dateDistrictFlag==false && dateFlag){
				TestTool.getTraceTime("509");
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				flag=true;
			}
			if(status){
				TestTool.getTraceTime("510");
				criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			
			//job applied flag
			//job applied flag
			if(newcandidatesonlyNew)
			{
				if(appliedfDate!=null && appliedtDate!=null)
				{
					System.out.println("############### newcandidatesonlyNew Count FT #######################");
					criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedfDate!=null && appliedtDate==null)
				{
					System.out.println("############### newcandidatesonlyNew Count F #######################");
					//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedtDate!=null && appliedfDate==null)
				{
					System.out.println("############### newcandidatesonlyNew Count T #######################");
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
			}
			else
			{
				if(appliedfDate!=null && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				else if(appliedfDate!=null && appliedtDate==null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				else if(appliedtDate!=null && appliedfDate==null)
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}
			
			
			flag=true;
			if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
			{
				TestTool.getTraceTime("511 T");
				criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetailAllFilter)));
			}

			if(teacherFlag){
				if(filterTeacherList!=null && filterTeacherList.size() >0)
				{
					TestTool.getTraceTime("512 T");
					flag=true; //
					criteria.add(Restrictions.in("teacherId", filterTeacherList)); //
				}
			}
			if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size()>0) //
			{
				TestTool.getTraceTime("513 T");
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));
			}
			
			if(districtMaster!=null  && utype==3)
			{
				TestTool.getTraceTime("514");
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					TestTool.getTraceTime("515");
					System.out.println("master: "+master.getStatus());
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			System.out.println("*****************************************************");
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				TestTool.getTraceTime("516");
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					TestTool.getTraceTime("517");
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				}				
				
				//criteria.setProjection(Projections.countDistinct("teacherId"));
				String countRow= " count(distinct this_.teacherId)";
				criteria.setProjection( Projections.projectionList().add(Projections.sqlProjection(""+countRow+" as countRow", new String[]{"countRow"}, new Type[]{ Hibernate.INTEGER}),"countRow"));
				
				lstTeacherDetail =  criteria.list();
				TestTool.getTraceTime("518 lstTeacherDetail "+lstTeacherDetail.size());
			}
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criteria.add(criterionSchool);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		TestTool.getTraceTime("599");
		return lstTeacherDetail;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsBeforeDateNew(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,StatusMaster statusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:73");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String[] candidatetyp=null;
			if(internalCandVal!=null){
					candidatetyp=internalCandVal.split("|");
			}
			
			List<Integer> candidatetypec=new ArrayList<Integer>();
			if(candidatetyp!=null){
				for(String s:candidatetyp){
					if(s.equalsIgnoreCase("0")){
						candidatetypec.add(1);
						candidatetypec.add(5);
					}else{
						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
							int val=Integer.parseInt(s);
							candidatetypec.add(val);
						}
						
					}
				}
			}
			System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
			if(candidatetypec.size()>0){
				criteria.add(Restrictions.in("isAffilated",candidatetypec));
			}
			
			
			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}*/
			boolean flag=true;
			boolean dateDistrictFlag=false;
			
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
			}
			if(status)
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
			else
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			
			//job applied flag
			
			/*if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			else if(daysVal==5 && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
			else if(appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));*/
			if(appliedfDate!=null && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			else if(appliedfDate!=null && appliedtDate==null)
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			else if(appliedtDate!=null && appliedfDate==null)
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			
			if(teacherFlag && filterTeacherList!=null && filterTeacherList.size() >0)
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			
			if(nByAflag && NbyATeacherList!=null &&NbyATeacherList.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				}
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}
			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsL1(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,StatusMaster statusMaster,List<TeacherDetail> lstTeacherDetailAllFilter)
	{
		PrintOnConsole.getJFTPrint("JFT:74");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());

			
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String[] candidatetyp=null;
			if(internalCandVal!=null){
					candidatetyp=internalCandVal.split("|");
			}
			List<Integer> candidatetypec=new ArrayList<Integer>();
			if(candidatetyp!=null){
				for(String s:candidatetyp){
					if(s.equalsIgnoreCase("0")){
						candidatetypec.add(1);
						candidatetypec.add(5);
					}else{
						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
							int val=Integer.parseInt(s);
							candidatetypec.add(val);
						}
						
					}
				}
			}
			System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
			if(candidatetypec.size()>0){
				criteria.add(Restrictions.in("isAffilated",candidatetypec));
			}
			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}*/
			boolean flag=true;
			boolean dateDistrictFlag=false;
			
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			
			if((entityID==3 || entityID==4)){
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
			}
			if(status)
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
			else
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			
			//job applied flag
			
			if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			else if(daysVal==5 && appliedtDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
			else if(appliedfDate!=null)
				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
			
			filterTeacherList.addAll(lstTeacherDetailAllFilter);
			
			//if(teacherFlag && filterTeacherList!=null && filterTeacherList.size() >0)
			if(filterTeacherList!=null && filterTeacherList.size() >0)
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			
			if(nByAflag && NbyATeacherList!=null &&NbyATeacherList.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
				}
				criteria.setProjection(Projections.groupProperty("teacherId"));
				lstTeacherDetail =  criteria.list();
			}
			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	
	@SuppressWarnings("unchecked")
	@Transactional(readOnly=false)
	public List<Integer> findJobIdsbyJFTID(List<Integer> jftIds)
	{
		List<Integer> lstjoborders = new ArrayList<Integer>(); 
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    
		    Criterion criterion = Restrictions.in("jobForTeacherId",jftIds);
			criteria.add(criterion);
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("jobId.jobId")));
			lstjoborders = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstjoborders;
	}

	
	@Transactional(readOnly=false)
	public List<TeacherDetail> getQuestTeacherDetailRecords(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,List<DistrictMaster> districtMasterList,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,int internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> notTeacherDetails,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster)
	{
		//System.out.println("========getQuestTeacherDetailRecords=======");
		PrintOnConsole.getJFTPrint("JFT:75");	
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}

			criteria.add(Restrictions.ne("status",statusMaster));

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
			Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
		//shriram	
			List<Integer> distIdList=new ArrayList<Integer>();
			if(districtMasterList!=null && districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()==3)
			{
			for(DistrictMaster dms:districtMasterList)
			{
				distIdList.add(dms.getDistrictId());
			}
			}
			//ended by shriram
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(entityID==4 && dateFlag){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					dateDistrictFlag=true;
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2 && districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()==3){
				flag=true;
				criteria.add(Restrictions.in("districtId", distIdList));
			}
				else if(dateFlag==false && districtMaster!=null &&  entityID==2 )
				{
					flag=true;
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				}
			else if(dateFlag && districtMaster!=null &&  entityID==2 && districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId()==3){
				criteria.add(Restrictions.in("districtId", distIdList));
				flag=true;
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			else if(dateFlag && districtMaster!=null &&  entityID==2){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				flag=true;
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				dateDistrictFlag=true;
			}
			if(dateDistrictFlag==false && dateFlag){
				if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}
				flag=true;
			}
			if(status){
				//criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}else{
				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
			}

			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);

			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);

			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			//job applied flag
			if(appliedfDate!=null && appliedtDate!=null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
			}else if(appliedfDate!=null && appliedtDate==null){
				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
			}else if(appliedtDate!=null && appliedfDate==null){
				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
			}
			flag=true;
			if(notTeacherDetails.size()>0)
				criteria.add(Restrictions.not(Restrictions.in("teacherId", notTeacherDetails)));

			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
				.addOrder(sortOrderStrVal);
				criteria.setProjection(Projections.groupProperty("teacherId"));
				// System.out.println("===Criteria :: "+criteria.toString());
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAvailableCandidteBYDistrict(Order order,int startPos,int limit,DistrictMaster  districtMaster,JobOrder joborder,Criterion firstlastEmailCri,EventDetails eventDetails,UserMaster userMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:76");	
		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    
		    Criteria c1 = criteria.createCriteria("teacherId");
		    
		    if(joborder!=null){
			    Criterion crjobId =Restrictions.eq("jobId", joborder);
			    criteria.add(crjobId);
		    }else{
    			if(userMaster.getEntityType().equals(3))
    			 {
			        List<JobOrder> listJoOrders =schoolInJobOrderDAO.findAllJobBySchool(userMaster.getSchoolId());
			        System.out.println(" SSSSSSSSSSSSSSSSSSSSSSSSS  :: "+userMaster.getSchoolId().getSchoolId());
			        Criterion criterionJObs =Restrictions.in("jobId", listJoOrders);
			        criteria.add(criterionJObs);
    			 } else if(eventDetails.getDistrictMaster()!=null){
		    		criteria.add(Restrictions.eq("districtId", eventDetails.getDistrictMaster().getDistrictId()));
		    	   //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
		    	}else if(eventDetails.getHeadQuarterMaster()!=null)
		    	{
		    		if(eventDetails.getBranchMaster()!=null){
		    			criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster",eventDetails.getBranchMaster()));
		    		}else{
		    			criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster",eventDetails.getHeadQuarterMaster()));
		    		}
		    	}
		    }
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherId")));
			
			if(firstlastEmailCri!=null ){
				//for(Criterion cri: firstlastEmailCri)
					c1.add(firstlastEmailCri);
			}
			
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);			
			c1.addOrder(order);
			
			lstJobForTeacher = criteria.list();
			
					
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAvailableCandidteBYDistrictCount(DistrictMaster  districtMaster,JobOrder joborder,List<Criterion> firstlastEmailCri)
	{
		PrintOnConsole.getJFTPrint("JFT:77");	
		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
		    Criteria criteria = session.createCriteria(getPersistentClass());
		    Criteria c1 = criteria.createCriteria("teacherId");		    
		    if(joborder!=null){
			    Criterion crjobId =Restrictions.eq("jobId", joborder);
			    criteria.add(crjobId);
		    }else{
		    	if(districtMaster!=null){
		    		criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
		    	   //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
		    	}
		    }			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherId")));
			
			if(firstlastEmailCri!=null && firstlastEmailCri.size()>0){
				for(Criterion cri: firstlastEmailCri)
				    c1.add(cri);
			}
			
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List findUniqueApplicants(DistrictMaster districtMaster,EventDetails eventDetails)
	{
		PrintOnConsole.getJFTPrint("JFT:77");
		 					  districtMaster	 = eventDetails.getDistrictMaster();
		 HeadQuarterMaster headQuarterMaster 	 = eventDetails.getHeadQuarterMaster();
		 BranchMaster branchMaster				 = eventDetails.getBranchMaster();
		List rows= new ArrayList();
		try 
		{
			Session session = getSession();
			/*Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			criteria.setProjection(Projections.distinct(Projections.property("teacherId")));			 
			lstJobForTeacher = criteria.list();*/
			String sql = "";
			//sql = "SELECT distinct(teacherId) as teacherId  FROM  `jobforteacher` where districtId=:districtId";
		
			//sql = "SELECT distinct(teacherId)  FROM  `jobforteacher` jft join joborder jo on jft.jobId=jo.jobId " +
	       // " WHERE  jo.districtId=:districtMaster";
			if(districtMaster!=null)
			  sql = "SELECT distinct(teacherId)  FROM  `jobforteacher`  WHERE districtId=:districtId";
			else if(branchMaster!=null)
				sql = "SELECT distinct(teacherId)  FROM  `jobforteacher` jft join joborder jo on jft.jobId=jo.jobId  WHERE  jo.branchId=:branchMaster";
			else if(headQuarterMaster!=null)
				sql = "SELECT distinct(teacherId)  FROM  `jobforteacher` jft join joborder jo on jft.jobId=jo.jobId  WHERE  jo.headQuarterId=:headQuarterMaster";
			
			if(!sql.equals(""))
			 {
				Query query = session.createSQLQuery(sql);
				if(districtMaster!=null)
					query.setParameter("districtId", districtMaster.getDistrictId());
				else if(branchMaster!=null)
					query.setParameter("branchMaster", branchMaster);
				else if(headQuarterMaster!=null)
					query.setParameter("headQuarterMaster", headQuarterMaster);
			
			  rows = query.list();
			 }
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return rows;
	}

	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrderBySelectedSchool(TeacherDetail teacherDetail,Order order,int startPos,int limit)
	{
		PrintOnConsole.getJFTPrint("JFT:78");	
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1=Restrictions.eq("candidateConsideration",false);
			Criterion criterion2=Restrictions.eq("candidateConsideration",true);
			Criterion criterion=Restrictions.or(criterion1, criterion2);
			criteria.add(criterion);
			Criterion criterion3=Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion3);
			criteria.setFirstResult(startPos);
			criteria.setMaxResults(limit);
			criteria.createCriteria("jobId").addOrder(order);
			criteria.setProjection(Projections.groupProperty("jobId"));
			jobOrderList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrderList;
	}
	
	@Transactional(readOnly=false)
	public List<JobOrder> getJobOrderBySelectedSchoolAll(TeacherDetail teacherDetail)
	{
		PrintOnConsole.getJFTPrint("JFT:79");	
		List<JobOrder> jobOrderList= new ArrayList<JobOrder>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1=Restrictions.eq("candidateConsideration",false);
			Criterion criterion2=Restrictions.eq("candidateConsideration",true);
			Criterion criterion=Restrictions.or(criterion1, criterion2);
			criteria.add(criterion);
			Criterion criterion3=Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion3);
			criteria.setProjection(Projections.groupProperty("jobId"));
			jobOrderList =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return jobOrderList;
	}

	@Transactional(readOnly=false)
	public List<TeacherDetail> getJobForTeacherByStatus(List<JobOrder> lstJobOrder)
	{
		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
		try 
		{
			Session session = getSession();
			Criteria criteria= session.createCriteria(getPersistentClass()); 
		//	Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",lstJobOrder);
            
			criteria.add(criterion1);
			criteria.add(criterion2);
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.groupProperty("teacherId"))
			);
			lstJobForTeacher = criteria.list();
			//lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findByIncompJobs(List<JobOrder> jobOrders)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			//StatusMaster statusMaster= WorkThreadServlet.statusMap.get("icomp");
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("icomp");
			Criterion criterion1 = Restrictions.eq("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",jobOrders);
			lstJobForTeacher = findByCriteria(criterion1,criterion2);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getFullTimeDetla(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:80");	
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null)
				{
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					criteria.createCriteria("jobId").add(Restrictions.eq("jobType", "F"));
					criteria.createCriteria("status").add(Restrictions.eq("statusShortName","hird"));
				}
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public void addReminderCount(List<Long> jobForTeacherIds){
		try {
			PrintOnConsole.getJFTPrint("JFT:81");	
			String hql = "update JobForTeacher set noOfReminderSent=1,reminderSentDate=:currDate WHERE jobForTeacherId IN (:jobForTeacherIds)";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameter("currDate", new Date());
			query.setParameterList("jobForTeacherIds", jobForTeacherIds);
			System.out.println("New  OOO  "+query.executeUpdate());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	@Transactional(readOnly=false)
	public void updateReminderCount(List<Long> jobForTeacherIds){
		try {
			PrintOnConsole.getJFTPrint("JFT:82");	
			String hql = "update JobForTeacher set noOfReminderSent=noOfReminderSent+1,reminderSentDate=:currDate WHERE jobForTeacherId IN (:jobForTeacherIds)";
			Query query = null;
			Session session = getSession();

			query = session.createQuery(hql);
			query.setParameter("currDate", new Date());
			query.setParameterList("jobForTeacherIds", jobForTeacherIds);
			System.out.println("Existing  OOO  "+query.executeUpdate());
		} catch (HibernateException e) {
			e.printStackTrace();
		}
	}
	
	
	/********	Teacher applied job list did not have any communication *********/
	/************by Ram Nath*********************/	
	@Transactional(readOnly=true)
	public int applicantsNotContactedListWithAndWithoutDistrictCountRow(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal, int schoolId,String startDate,String enddate,String jobOrderId)
	{	
		PrintOnConsole.getJFTPrint("JFT:83");	
		System.out.println("order iddd"+jobOrderId);
		 Date startD = new Date("1/1/2000");
         Date endD = new Date("12/31/9999"); 

        if(startDate!=null && !startDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(startDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(enddate!=null && !enddate.equals(""))
			try {
				endD = Utility.getCurrentDateFormart(enddate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		
		int countrow=0;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			LogicalExpression orExp = Restrictions.or(Restrictions.isNull("lastActivity"), Restrictions.eq("lastActivity", ""));					
			criteria.add(orExp);
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",endD));
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
						
			List<JobOrder> lstJobOrderBySchoolId=new ArrayList<JobOrder>();;
			if(schoolId!=0){
				Criterion scriterion = Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false));
				List<SchoolInJobOrder> lstSchInJOrder=schoolInJobOrderDAO.findByCriteria(scriterion);
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null)
						lstJobOrderBySchoolId.add(sijo.getJobId());
				System.out.println("jobid=="+lstJobOrderBySchoolId);
				if(lstJobOrderBySchoolId.size()>0)
				criteria.add(Restrictions.in("jobId",lstJobOrderBySchoolId));
			}					
			//Criteria c1 = null;				
			//c1 = criteria.createCriteria("jobId");
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			 //c1.add(Restrictions.eq("districtMaster",districtMaster));
			 
			}
			
			if(schoolId!=0 && lstJobOrderBySchoolId.size()==0)
			countrow=0;
			else
			countrow= ((Number)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return countrow;
	}
	@Transactional(readOnly=true)
	public List<String []> applicantsNotContactedListWithAndWithoutDistrict(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal ,int start, int noOfRow,int schoolId,boolean report,String startDate,String enddate,String jobOrderId)
	{
		PrintOnConsole.getJFTPrint("JFT:84");	
		System.out.println("job iddddddddddd"+jobOrderId);
		
		 Date startD = new Date("1/1/2000");
         Date endD = new Date("12/31/9999"); 

        if(startDate!=null && !startDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(startDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(enddate!=null && !enddate.equals(""))
			try {
				endD = Utility.getCurrentDateFormart(enddate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		List<String []> lstJobForTeacher=new ArrayList<String []>();
		try{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			criteria.createAlias("jobId", "jo").createAlias("teacherId", "td").createAlias("jo.districtMaster", "dm")
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("dm.districtName"), "districtName" )
		        .add( Projections.property("td.firstName"), "firstName" )
		        .add( Projections.property("td.lastName"), "lastName" )
		        .add( Projections.property("td.emailAddress"), "emailAddress" )
		        .add( Projections.property("jo.jobTitle"), "jobTitle" )
		       .add( Projections.property("createdDateTime"),"createdDateTime132")	
		       .add( Projections.property("jo.status"), "status" )
		       .add( Projections.property("jo.jobId"), "jobId11" )
		    );
			if(districtMaster!=null )
			    criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			LogicalExpression orExp = Restrictions.or(Restrictions.isNull("lastActivity"), Restrictions.eq("lastActivity", ""));
			criteria.add(orExp);
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
			
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",endD));
						List<JobOrder> lstJobOrderBySchoolId=new ArrayList<JobOrder>();;
			if(schoolId!=0){
				
				Criterion scriterion = Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false));
				List<SchoolInJobOrder> lstSchInJOrder=schoolInJobOrderDAO.findByCriteria(scriterion);
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null){
						lstJobOrderBySchoolId.add(sijo.getJobId());
						//System.out.println("sijo=="+sijo.getSchoolId().getSchoolName());
					}
				//System.out.println("jobid=="+lstJobOrderBySchoolId);
				if(lstJobOrderBySchoolId.size()>0)
				criteria.add(Restrictions.in("jobId",lstJobOrderBySchoolId));
			}
			if(!report){
			criteria.setFirstResult(start);
			criteria.setMaxResults(noOfRow);
			}
							
			criteria.addOrder(sortOrderStrVal);
			
			long a = System.currentTimeMillis();
			if(schoolId!=0 && lstJobOrderBySchoolId.size()==0)
			lstJobForTeacher = new ArrayList<String[]>();
			else
			lstJobForTeacher = criteria.list() ;			
			long b = System.currentTimeMillis();
			System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + lstJobForTeacher.size() );
			
			} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}
	/*************status filter**************/
	
	@Transactional(readOnly=true)
	public int applicantsNotContactedListWithAndWithoutDistrictCountRowForStatus(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal, int schoolId,String startDate,String enddate,String jobOrderId,List<Long> jftIds,boolean statusflag)
	{	
		PrintOnConsole.getJFTPrint("JFT:85");	
		System.out.println("order iddd"+jobOrderId);
		 Date startD = new Date("1/1/2000");
         Date endD = new Date("12/31/9999"); 

        if(startDate!=null && !startDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(startDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(enddate!=null && !enddate.equals(""))
			try {
				endD = Utility.getCurrentDateFormart(enddate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		
		int countrow=0;
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());	
			
			if(statusflag && jftIds.size()>0){
				criteria.add(Restrictions.in("jobForTeacherId", jftIds));
			}else if(statusflag && jftIds.size()==0){
				return countrow;
			}
			
			LogicalExpression orExp = Restrictions.or(Restrictions.isNull("lastActivity"), Restrictions.eq("lastActivity", ""));					
			criteria.add(orExp);
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",endD));
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
						
			List<JobOrder> lstJobOrderBySchoolId=new ArrayList<JobOrder>();;
			if(schoolId!=0){
				Criterion scriterion = Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false));
				List<SchoolInJobOrder> lstSchInJOrder=schoolInJobOrderDAO.findByCriteria(scriterion);
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null)
						lstJobOrderBySchoolId.add(sijo.getJobId());
				System.out.println("jobid=="+lstJobOrderBySchoolId);
				if(lstJobOrderBySchoolId.size()>0)
				criteria.add(Restrictions.in("jobId",lstJobOrderBySchoolId));
			}					
			//Criteria c1 = null;				
			//c1 = criteria.createCriteria("jobId");
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			 //c1.add(Restrictions.eq("districtMaster",districtMaster));
			}
			
			if(schoolId!=0 && lstJobOrderBySchoolId.size()==0)
			countrow=0;
			else
			countrow= ((Number)criteria.setProjection(Projections.rowCount()).uniqueResult()).intValue();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return countrow;
	}
	@Transactional(readOnly=true)
	public List<String []> applicantsNotContactedListWithAndWithoutDistrictForStatus(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal ,int start, int noOfRow,int schoolId,boolean report,String startDate,String enddate,String jobOrderId,List<Long> jftIds,boolean statusflag)
	{
		PrintOnConsole.getJFTPrint("JFT:86");	
		System.out.println("job iddddddddddd"+jobOrderId);
		
		 Date startD = new Date("1/1/2000");
         Date endD = new Date("12/31/9999"); 

        if(startDate!=null && !startDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(startDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(enddate!=null && !enddate.equals(""))
			try {
				endD = Utility.getCurrentDateFormart(enddate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		List<String []> lstJobForTeacher=new ArrayList<String []>();
		try{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(statusflag && jftIds.size()>0){
				criteria.add(Restrictions.in("jobForTeacherId", jftIds));
			}else if(statusflag && jftIds.size()==0){
				return lstJobForTeacher;
			}
			criteria.createAlias("jobId", "jo").createAlias("teacherId", "td").createAlias("jo.districtMaster", "dm")
		    .setProjection( Projections.projectionList()
		        .add( Projections.property("dm.districtName"), "districtName" )
		        .add( Projections.property("td.firstName"), "firstName" )
		        .add( Projections.property("td.lastName"), "lastName" )
		        .add( Projections.property("td.emailAddress"), "emailAddress" )
		        .add( Projections.property("jo.jobTitle"), "jobTitle" )
		       .add( Projections.property("createdDateTime"),"createdDateTime132")	
		       .add( Projections.property("jo.status"), "status" )
		       .add( Projections.property("jo.jobId"), "jobId11" )
		    );
			if(districtMaster!=null )
			criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			
			LogicalExpression orExp = Restrictions.or(Restrictions.isNull("lastActivity"), Restrictions.eq("lastActivity", ""));
			criteria.add(orExp);
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
			
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",endD));
						List<JobOrder> lstJobOrderBySchoolId=new ArrayList<JobOrder>();;
			if(schoolId!=0){
				
				Criterion scriterion = Restrictions.eq("schoolId", schoolMasterDAO.findById(new Long(schoolId), false, false));
				List<SchoolInJobOrder> lstSchInJOrder=schoolInJobOrderDAO.findByCriteria(scriterion);
				for(SchoolInJobOrder sijo:lstSchInJOrder)
					if(sijo.getJobId()!=null){
						lstJobOrderBySchoolId.add(sijo.getJobId());
						}
				
				if(lstJobOrderBySchoolId.size()>0)
				criteria.add(Restrictions.in("jobId",lstJobOrderBySchoolId));
			}
			if(!report){
			criteria.setFirstResult(start);
			criteria.setMaxResults(noOfRow);
			}
							
			criteria.addOrder(sortOrderStrVal);
			
			long a = System.currentTimeMillis();
			if(schoolId!=0 && lstJobOrderBySchoolId.size()==0)
			lstJobForTeacher = new ArrayList<String []>();
			else
			lstJobForTeacher = criteria.list() ;			
			long b = System.currentTimeMillis();
			System.out.println("end running Execution time: " + TimeUnit.MILLISECONDS.toSeconds((b - a))+ " Sec; Number of records fetch: " + lstJobForTeacher.size() );
			
			} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}
	/************end Ram Nath*********************/
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> getJFTByTeacherListAllJob(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster,List<StatusMaster> listStatusMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:87");	
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				if(districtMaster!=null){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}

				//criteria.createCriteria("status").add(Restrictions.ne("statusShortName","hird"));
				if(listStatusMaster!=null)
					criteria.add(Restrictions.not(Restrictions.in("status",listStatusMaster)));
					
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
	/**
	 * Amit Kumar
	 */
	@Transactional(readOnly=false)
	public int applicantsByCertificationsListWithAndWithoutDistrictCountRow(DistrictMaster districtMaster, int sortingcheck, Order sortOrderStrVal, int schoolId,Integer stateId,Integer certTypeId,Double experience,String sfromDate,String stoDate)
	{
		PrintOnConsole.getJFTPrint("JFT:88");	
		int countrow=0;
		List<Object[]> list=new ArrayList<Object[]>();
		boolean certificateFlag=false,experienceFlag=false,appliedFlag=false;
		try
		{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(getPersistentClass());			
			criteria
			.createAlias("jobId", "jo")
			.createAlias("teacherId", "td")
			.createAlias("jo.districtMaster", "dm")
			.setProjection( Projections.projectionList()
							.add( Projections.property("dm.districtName"), "districtName")
							.add( Projections.property("td.firstName"), "firstName")
							.add( Projections.property("td.lastName"), "lastName")
							.add( Projections.property("td.emailAddress"), "emailAddress")
							.add( Projections.max("createdDateTime"))
							.add( Projections.property("redirectedFromURL"), "redirectedFromURL")
							.add( Projections.property("td.teacherId"), "teacherId")
							.add( Projections.groupProperty("teacherId"))
		    );
			
			if(districtMaster!=null)
				criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			
			if(schoolId!=0)
				criteria.add(Restrictions.eq("schoolMaster", schoolMasterDAO.findById(new Long(schoolId), false, false)));
			
			Date fDate=null;
			Date tDate=null;
			if(!sfromDate.equals(""))
				 fDate = Utility.getCurrentDateFormart(sfromDate);
			if(!stoDate.equals(""))
				 tDate = Utility.getCurrentDateFormart(stoDate);
			
			if(fDate!=null && tDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
			else if(fDate!=null && tDate==null)
				criteria.add(Restrictions.ge("createdDateTime",fDate));
			else if(fDate==null && tDate!=null)
				criteria.add(Restrictions.le("createdDateTime",tDate));
			
			criteria.addOrder(sortOrderStrVal);
			
			list = criteria.list();
			
			List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
			Map<Integer, List<TeacherCertificate>> teacherCertificateMap= new HashMap<Integer, List<TeacherCertificate>>();
			Map<Integer, TeacherExperience> teacherExperienceMap = new HashMap<Integer, TeacherExperience>();
			
			for (Iterator it = list.iterator(); it.hasNext();)
			{
				
				Integer teacherId=null;
	            Object[] row = (Object[]) it.next();
	            if(row[6]!=null)
	            {
	            	teacherId=Integer.parseInt(row[6].toString());
	            	TeacherDetail td = new TeacherDetail();
	            	td.setTeacherId(teacherId);
	            	teacherDetailList.add(td);
	            }
			}
			
			List<TeacherCertificate> teacherCertificateList=null;
			List<TeacherExperience> teacherExperienceList=null;
			if(teacherDetailList.size()>0)
			{
				if(stateId==0 && certTypeId==0)
				{
					teacherCertificateList = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList);
					certificateFlag=false;
				}
				else if(stateId!=0 && certTypeId!=0)
				{
					teacherCertificateList = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList, certificateTypeMasterDAO.findById(certTypeId, false, false));
					certificateFlag=true;
				}
				if(experience==null||experience==0.0)
				{
					teacherExperienceList  = teacherExperienceDAO.findExperienceForTeachers(teacherDetailList);
					experienceFlag=false;
				}
				else if(experience!=0.0)
				{
					teacherExperienceList  = teacherExperienceDAO.findExperienceForTeachers(teacherDetailList, experience);
					experienceFlag=true;
				}
			}
			
			if(teacherCertificateList!=null && teacherCertificateList.size()>0)
			{
				for(TeacherCertificate teacherCertificate : teacherCertificateList)
				{
					TeacherDetail teacherDetail = teacherCertificate.getTeacherDetail();
					Integer teacherId = teacherDetail.getTeacherId();
					List<TeacherCertificate> certificateListOfTeacher = null;
						
					if(teacherCertificateMap.get(teacherId)!=null)
					{
						certificateListOfTeacher=teacherCertificateMap.get(teacherId);
						certificateListOfTeacher.add(teacherCertificate);
					}
					else
					{
						certificateListOfTeacher = new ArrayList<TeacherCertificate>();
						certificateListOfTeacher.add(teacherCertificate);
					}
					teacherCertificateMap.put(teacherId, certificateListOfTeacher);
				}
			}
				
			if(teacherExperienceList!=null && teacherExperienceList.size()>0)
			{
				for(TeacherExperience teacherExperience : teacherExperienceList)
					teacherExperienceMap.put(teacherExperience.getTeacherId().getTeacherId(), teacherExperience);
			}
			
			List<Object[]> list1=null;
			List<Object[]> list2=null;
			List<Object[]> list3=null;
			List<Object[]> list4=null;
			if(certificateFlag)
			{
				list1=new ArrayList<Object[]>();
				for (Iterator it = list.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherCertificateMap.get(Integer.parseInt(row[6].toString()))!=null && teacherCertificateMap.get(Integer.parseInt(row[6].toString())).size()>0)
		            	{
		            		list1.add(row);
		            	}
		            }
				}
			}
			
			if(experienceFlag)
			{
				int i=1;
				list2=new ArrayList<Object[]>();
				for (Iterator it = list.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherExperienceMap.get(Integer.parseInt(row[6].toString()))!=null)
		            	{
		            		if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=null && teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=0.0)
		            		{
		            			if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining().equals(experience))
		            			{
		            				list2.add(row);
		            				System.out.println("Count Row:::::list.size()===="+list.size()+":::::::::::::::"+i);
		            				++i;
		            			}
		            		}
		            	}
		            }
				}
			}
			
			if(certificateFlag && experienceFlag)
			{
				list3=new ArrayList<Object[]>();
				list4=new ArrayList<Object[]>();
				for (Iterator it = list.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherCertificateMap.get(Integer.parseInt(row[6].toString()))!=null && teacherCertificateMap.get(Integer.parseInt(row[6].toString())).size()>0)
		            	{
		            		list3.add(row);
		            	}
		            }
				}
				if(list3!=null && list3.size()>0)
				{
					for (Iterator it = list3.iterator(); it.hasNext();)
					{
						Object[] row = (Object[]) it.next();
						if(row[6]!=null)
						{
							if(teacherExperienceMap.get(Integer.parseInt(row[6].toString()))!=null)
			            	{
			            		if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=null && teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=0.0)
			            		{
			            			if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()==experience)
			            			{
			            				System.out.println("CountRow::::::::::::::teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()=="+teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()+"::::::::::::"+"experience=="+experience);
			            				list4.add(row);
			            			}			            			
			            		}
			            	}
						}
					}
				}
			}
			
			if(!certificateFlag && !experienceFlag)
			{
				countrow=list.size();
				System.out.println("CountRow::::::::::::::!certificateFlag && !experienceFlag");
				System.out.println("list.size()=="+list.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(certificateFlag && experienceFlag)
			{
				countrow=list4.size();
				System.out.println("CountRow::::::::::::::certificateFlag && experienceFlag");
				System.out.println("list4.size()=="+list4.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(certificateFlag)
			{
				countrow=list1.size();
				System.out.println("CountRow::::::::::::::certificateFlag");
				System.out.println("list1.size()=="+list1.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(experienceFlag)
			{
				countrow=list2.size();
				System.out.println("CountRow::::::::::::::experienceFlag");
				System.out.println("list2.size()=="+list2.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			return countrow;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return countrow;	
	}

	/**
	 * Amit Kumar
	 */
	@Transactional(readOnly=false)
	public List applicantsByCertificationsListWithAndWithoutDistrict(DistrictMaster districtMaster,int sortingcheck,Order sortOrderStrVal,int start,int noOfRow,int schoolId,boolean report,Integer stateId,Integer certTypeId,Double experience,String sfromDate,String stoDate)
	{
		PrintOnConsole.getJFTPrint("JFT:89");	
		List<Object[]> list=new ArrayList<Object[]>();
		List<Object[]> tempList=new ArrayList<Object[]>();
		List finalList=new ArrayList();
		boolean certificateFlag=false,experienceFlag=false,appliedFlag=false;
		try
		{
			Session session = getSession();				
			Criteria criteria = session.createCriteria(getPersistentClass());			
			criteria
			.createAlias("jobId", "jo")
			.createAlias("teacherId", "td")
			.createAlias("jo.districtMaster", "dm")
			.setProjection( Projections.projectionList()
							.add( Projections.property("dm.districtName"), "districtName")
							.add( Projections.property("td.firstName"), "firstName")
							.add( Projections.property("td.lastName"), "lastName")
							.add( Projections.property("td.emailAddress"), "emailAddress")
							.add( Projections.max("createdDateTime"))
							.add( Projections.property("redirectedFromURL"), "redirectedFromURL")
							.add( Projections.property("td.teacherId"), "teacherId")
							.add( Projections.groupProperty("teacherId"))
		    );
			
			if(districtMaster!=null)
				criteria.add(Restrictions.eq("dm.districtId",districtMaster.getDistrictId()));
			
			if(schoolId!=0)
				criteria.add(Restrictions.eq("schoolMaster", schoolMasterDAO.findById(new Long(schoolId), false, false)));
			
			Date fDate=null;
			Date tDate=null;
			if(!sfromDate.equals(""))
				 fDate = Utility.getCurrentDateFormart(sfromDate);
			if(!stoDate.equals(""))
				 tDate = Utility.getCurrentDateFormart(stoDate);
			
			if(fDate!=null && tDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				//criteria.add(Restrictions.between("createdDateTime", fDate, tDate));
			else if(fDate!=null && tDate==null)
				criteria.add(Restrictions.ge("createdDateTime",fDate));
			else if(fDate==null && tDate!=null)
				criteria.add(Restrictions.le("createdDateTime",tDate));
			
			System.out.println("");
			System.out.println("report=="+report+":::start=="+start+":::noOfRow=="+noOfRow);
			System.out.println("");
			criteria.addOrder(sortOrderStrVal);
			tempList=criteria.list();
			if(!report)
			{
				criteria.setFirstResult(start);
				criteria.setMaxResults(noOfRow);
				
				System.out.println("report=="+report+"	start=="+start+"	noOfRow=="+noOfRow);
			}
			//System.out.println("start=="+start+"	noOfRow=="+noOfRow+"\n");
			list = criteria.list();
			//System.out.println(""+list.size());
			List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
			Map<Integer, List<TeacherCertificate>> teacherCertificateMap= new HashMap<Integer, List<TeacherCertificate>>();
			Map<Integer, TeacherExperience> teacherExperienceMap = new HashMap<Integer, TeacherExperience>();
			
			//int j=1;
			for (Iterator it = tempList.iterator(); it.hasNext();)
			{
				Integer teacherId=null;
	            Object[] row = (Object[]) it.next();
	            if(row[6]!=null)
	            {
	            	teacherId=Integer.parseInt(row[6].toString());
	            	TeacherDetail td = new TeacherDetail();
	            	td.setTeacherId(teacherId);
	            	teacherDetailList.add(td);
	            	//System.out.println(""+j+"\n teacherId=="+teacherId+"::::::::: td=="+td);
	            }
	           //++j;
			}
			
			List<TeacherCertificate> teacherCertificateList=null;
			List<TeacherExperience> teacherExperienceList=null;
			if(teacherDetailList.size()>0)
			{
				if(stateId==0 && certTypeId==0)
				{
					teacherCertificateList = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList);
					certificateFlag=false;
				}
				else if(stateId!=0 && certTypeId!=0)
				{
					teacherCertificateList = teacherCertificateDAO.findCertificateByTeacher(teacherDetailList, certificateTypeMasterDAO.findById(certTypeId, false, false));
					certificateFlag=true;
				}
				if(experience==null||experience==0.0)
				{
					teacherExperienceList  = teacherExperienceDAO.findExperienceForTeachers(teacherDetailList);
					experienceFlag=false;
				}
				else if(experience!=0.0)
				{
					teacherExperienceList  = teacherExperienceDAO.findExperienceForTeachers(teacherDetailList, experience);
					experienceFlag=true;
				}
			}
			
			if(teacherCertificateList!=null && teacherCertificateList.size()>0)
			{
				System.out.println("teacherCertificateList.size()=="+teacherCertificateList.size()+"\n");
				for(TeacherCertificate teacherCertificate : teacherCertificateList)
				{
					TeacherDetail teacherDetail = teacherCertificate.getTeacherDetail();
					Integer teacherId = teacherDetail.getTeacherId();
					List<TeacherCertificate> certificateListOfTeacher = null;
					//System.out.println("teacherId=="+teacherId);
					if(teacherCertificateMap.get(teacherId)!=null)
					{
						System.out.println("teacherCertificateMap.get(teacherId).size()=="+teacherCertificateMap.get(teacherId).size());
						certificateListOfTeacher=teacherCertificateMap.get(teacherId);
						certificateListOfTeacher.add(teacherCertificate);
					}
					else
					{
						certificateListOfTeacher = new ArrayList<TeacherCertificate>();
						certificateListOfTeacher.add(teacherCertificate);
					}
					teacherCertificateMap.put(teacherId, certificateListOfTeacher);
				}
			}
				
			if(teacherExperienceList!=null && teacherExperienceList.size()>0)
			{
				for(TeacherExperience teacherExperience : teacherExperienceList)
					teacherExperienceMap.put(teacherExperience.getTeacherId().getTeacherId(), teacherExperience);
			}
			
			List<Object[]> list1=null;
			List<Object[]> list2=null;
			List<Object[]> list3=null;
			List<Object[]> list4=null;
			if(certificateFlag)
			{
				list1=new ArrayList<Object[]>();
				for (Iterator it = tempList.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherCertificateMap.get(Integer.parseInt(row[6].toString()))!=null && teacherCertificateMap.get(Integer.parseInt(row[6].toString())).size()>0)
		            	{
		            		list1.add(row);
		            	}
		            }
				}
			}
			
			if(experienceFlag)
			{
				int i=1;
				list2=new ArrayList<Object[]>();
				for (Iterator it = tempList.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherExperienceMap.get(Integer.parseInt(row[6].toString()))!=null)
		            	{
		            		if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=null && teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=0.0)
		            		{
		            			if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining().equals(experience))
		            			{
		            				list2.add(row);
		            				System.out.println("tempList.size()===="+tempList.size()+":::::::::::::::::::======"+i);
		            				++i;
		            			}
		            		}
		            			
		            	}
		            }
				}
			}
			
			if(certificateFlag && experienceFlag)
			{
				list3=new ArrayList<Object[]>();
				list4=new ArrayList<Object[]>();
				for (Iterator it = tempList.iterator(); it.hasNext();)
				{
					Object[] row = (Object[]) it.next();
		            if(row[6]!=null)
		            {
		            	if(teacherCertificateMap.get(Integer.parseInt(row[6].toString()))!=null && teacherCertificateMap.get(Integer.parseInt(row[6].toString())).size()>0)
		            	{
		            		list3.add(row);
		            	}
		            }
				}
				if(list3!=null && list3.size()>0)
				{
					for (Iterator it = list3.iterator(); it.hasNext();)
					{
						Object[] row = (Object[]) it.next();
						if(row[6]!=null)
						{
							if(teacherExperienceMap.get(Integer.parseInt(row[6].toString()))!=null)
			            	{
			            		if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=null && teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()!=0.0)
			            		{
			            			if(teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining().equals(experience))
			            			{
			            				System.out.println("teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()=="+teacherExperienceMap.get(Integer.parseInt(row[6].toString())).getExpCertTeacherTraining()+"::::::::::::"+"experience=="+experience);
			            				list4.add(row);
			            			}			            			
			            		}
			            	}
						}
					}
				}
			}
			
			if(!certificateFlag && !experienceFlag)
			{
				if(list!=null && list.size()>0)
					finalList.add(list);
				else
				{
					list=new ArrayList<Object[]>();
					finalList.add(list);
				}
				//finalList.add(list);
				finalList.add(teacherCertificateMap);
				finalList.add(teacherExperienceMap);
				System.out.println("::::::::::::::::::::::!certificateFlag && !experienceFlag");
				System.out.println("list.size()=="+list.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(certificateFlag && experienceFlag)
			{
				if(list4!=null && list4.size()>0)
					finalList.add(list4);
				else
				{
					list4=new ArrayList<Object[]>();
					finalList.add(list4);
				}
				//finalList.add(list4);
				finalList.add(teacherCertificateMap);
				finalList.add(teacherExperienceMap);
				System.out.println("::::::::::::::::::::::certificateFlag && experienceFlag");
				System.out.println("list4.size()=="+list4.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(certificateFlag)
			{
				if(list1!=null && list1.size()>0)
					finalList.add(list1);
				else
				{
					list1=new ArrayList<Object[]>();
					finalList.add(list1);
				}
				//finalList.add(list1);
				finalList.add(teacherCertificateMap);
				finalList.add(teacherExperienceMap);
				System.out.println("::::::::::::::::::::::certificateFlag");
				System.out.println("list1.size()=="+list1.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			else if(experienceFlag)
			{
				if(list2!=null && list2.size()>0)
					finalList.add(list2);
				else
				{
					list2=new ArrayList<Object[]>();
					finalList.add(list2);
				}
				//finalList.add(list2);
				finalList.add(teacherCertificateMap);
				finalList.add(teacherExperienceMap);
				System.out.println("::::::::::::::::::::::experienceFlag");
				System.out.println("list2.size()=="+list2.size()+":::::::::::teacherCertificateMap.size()=="+teacherCertificateMap.size()+":::::::::::teacherExperienceMap.size()=="+teacherExperienceMap.size());
			}
			return finalList;
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return finalList;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> getApplicantByAvailableAndTransList(StatusMaster statusMaster,List<SecondaryStatus> secondaryStatusList,DistrictMaster districtMaster,String sfromDate,String stoDate,String jobOrderId)
	{
		PrintOnConsole.getJFTPrint("JFT:90");	
		System.out.println("job id=="+jobOrderId);
		 Date d = new Date(); 
     	 Date startD = new Date("1/1/2000");
         Date endD = new Date(); 
         Date CretedendD = new Date("12/31/9999");
        if(sfromDate!=null && !sfromDate.equals(""))
			try {
				startD =  Utility.getCurrentDateFormart(sfromDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}

        if(stoDate!=null && !stoDate.equals(""))
			try {
				CretedendD = Utility.getCurrentDateFormart(stoDate);
			} catch (ParseException e1) {
				e1.printStackTrace();
			}
		System.out.println("start date============+"+startD+"End date============="+endD);
		
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(startD!=null && endD!=null)
				criteria.add(Restrictions.ge("createdDateTime",startD)).add(Restrictions.le("createdDateTime",CretedendD));
			
			if((jobOrderId!=null)&&(!jobOrderId.equals("")))
				criteria.add(Restrictions.eq("jobId.jobId", Integer.parseInt(jobOrderId)));
			
			
						
			Criterion criterion1 = Restrictions.in("secondaryStatus", secondaryStatusList);			
			Criterion criterion2 = Restrictions.isNull("statusMaster");
			Criterion criterion3 = Restrictions.and(criterion1,criterion2);
			
			Criterion criterion4 = Restrictions.eq("status",statusMaster);
			Criterion criterion5 = Restrictions.eq("statusMaster",statusMaster);
			Criterion criterion6 = Restrictions.and(criterion4,criterion5);
			
			Criterion criterion7= Restrictions.or(criterion6,criterion3);
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			}
			criteria.createCriteria("jobId").add(Restrictions.eq("status", "A")).add(Restrictions.ge("jobEndDate", d));
			
			criteria.add(criterion7);
			
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	/*********************/
	@Transactional(readOnly=false)
	public List<JobForTeacher> getApplicantByAvailableAndTransListHired(StatusMaster statusMaster,List<SecondaryStatus> secondaryStatusList,DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:91");	
		Date d = new Date(); 
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.createCriteria("status").add(Restrictions.eq("statusId", Integer.parseInt("6")));
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			}
			//criteria.createCriteria("jobId").add(Restrictions.eq("status", "A"));
			lstJobForTeacher =  criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	
	/********************/
		
	@Transactional(readOnly=false)
    public List<String[]> getJobForTeacherByJobAndTeachersList(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,int entityID)
    {
    	PrintOnConsole.getJFTPrint("JFT:92");	
    	 List<String[]> lstJobForTeacher=new ArrayList<String[]>();
    	 if(lstTeacherDetails.size()>0){
	         try{
	               Session session = getSession();
	               Criteria criteria = session.createCriteria(getPersistentClass());             
	               criteria.setProjection( Projections.projectionList()
	                       .add( Projections.property("teacherId.teacherId"), "teachId" )
	                       .add( Projections.count("teacherId") )              
	                       .add( Projections.groupProperty("teacherId") )
	                   );
	               Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
	               if(districtMaster!=null && entityID!=1){
	            	     criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
	                     //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
	               }
	               if(lstTeacherDetails.size()>0){
	                     criteria.add(criterionTeacher);
	               }
	              // criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
	               criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
	               lstJobForTeacher = criteria.list() ;
	         }catch (Throwable e) {
	               e.printStackTrace();
	         }   
    	 }
         return lstJobForTeacher;
    }
    
  //added By 21-03-2015 Ram nath
    @Transactional(readOnly=false)
    public List<JobForTeacher> findAllTeacherByDistrict(TeacherDetail teacherDetail,DistrictMaster districtMaster)
    {
    	PrintOnConsole.getJFTPrint("JFT:93");	
    	List<JobForTeacher> lstJobForTeacher= null;
    	try 
    	{
    		
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			criteria.add(Restrictions.eq("teacherId",teacherDetail));
    			criteria.add(Restrictions.isNotNull("offerReady"));
    			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			lstJobForTeacher = criteria.list();
    	} 
    	catch (Exception e) {
    		e.printStackTrace();
    	}		
    	return lstJobForTeacher;
    }
  //added By 21-03-2015 Ram nath
    
  //added By 21-03-2015 Ram nath
    @Transactional(readOnly=false)
    public List<TeacherStatusHistoryForJob> findAllTeacherByDistrictAndJobId(JobOrder jobOrder,TeacherDetail td)
    {
    	PrintOnConsole.getJFTPrint("JFT:93");	
    	List<TeacherStatusHistoryForJob> lstJSHJ= new ArrayList<TeacherStatusHistoryForJob>();
    	try 
    	{
    		
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(TeacherStatusHistoryForJob.class).createAlias("statusMaster", "sm");
    			criteria.add(Restrictions.eq("jobOrder",jobOrder));
    			criteria.add(Restrictions.eq("sm.statusId",18));
    			criteria.add(Restrictions.ne("teacherDetail",td));
    			criteria.add(Restrictions.eq("status", "S"));
    			//criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			lstJSHJ = criteria.list();
    	} 
    	catch (Exception e) {
    		e.printStackTrace();
    	}		
    	return lstJSHJ;
    }
  //added By 21-03-2015 Ram nath
    
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllQQFinalizedTeacherList(List<TeacherDetail> lstTeacherDetails,DistrictMaster districtMaster)
	{
    	PrintOnConsole.getJFTPrint("JFT:94");	
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
				    criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				    criteria.setProjection(Projections.groupProperty("teacherId"));
				    lstTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstTeacher;
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> getQQFinalizedNotesForTeacher(TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
    	PrintOnConsole.getJFTPrint("JFT:95");	
		List<JobForTeacher> lstjob=new ArrayList<JobForTeacher>();
		
		if(teacherDetail!=null && districtMaster!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(teacherDetail!=null)
					criteria.add(Restrictions.eq("teacherId",teacherDetail));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				    //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				    //criteria.setProjection(Projections.distinct(Projections.property("teacherId")));
				    lstjob =  criteria.list();
				    } 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstjob;
	}
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllQQFinalizeTeacherList(DistrictMaster districtMaster)
	{
    	PrintOnConsole.getJFTPrint("JFT:96");	
		List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
		if(districtMaster!=null){
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
			    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
			    criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			    //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			    criteria.setProjection(Projections.groupProperty("teacherId"));
			    teacherDetailList = criteria.list();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return teacherDetailList;
	}

    //getting applied detail by jobs and teachers   
	@Transactional(readOnly=false)
	public List<JobForTeacher> getAppliedDetailsByTeacherIdsAndJobIds(List<TeacherDetail> lstTeacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Criterion criterion1 = Restrictions.in("teacherId",lstTeacherDetail);
			lstJobForTeacher = findByCriteria(criterion1);		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   @Transactional(readOnly=false)
   public   List <JobForTeacher>  findAllAppliedJobsOfTeacher(Integer teacherId)
   {
      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
		try{
			 Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
			 lstJobForTeacher2=findByCriteria(criterion);
			
		}catch(Exception e){
			
				e.printStackTrace();
		}
		
	  return lstJobForTeacher2;
   }
   
   
   @Transactional(readOnly=false)
   public   List <JobForTeacher>  getEpiStatusByTeacher(Integer teacherId)
   {
	   PrintOnConsole.getJFTPrint("JFT:97");	
      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
		try{
			
			      Session session = getSession();
			      
			      Criteria criteria = session.createCriteria(getPersistentClass());
			      Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
			      Criterion criterion1 =Restrictions.eq("baseStatus", true);
			      Criterion criterion2 =Restrictions.eq("epiForFullTimeTeachers",true);
			      Criterion criterion3 =Restrictions.or(criterion1, criterion2);
			
			criteria.add(criterion);
			
			Criteria c1 = criteria.createCriteria("jobId").add(Restrictions.eq("status","A"));
			c1.createCriteria("jobCategoryMaster").add(criterion3);
			
			lstJobForTeacher2 = criteria.list();
			}catch(Exception e){
			
				e.printStackTrace();
		}
		
	  return lstJobForTeacher2;
   }
   
   @Transactional(readOnly=false)
	public List<JobForTeacher> findSortedJobByTeacherforOnBoarding(Order  sortOrderStrVal,TeacherDetail teacherDetail,DistrictMaster districtMaster)
	{
	   PrintOnConsole.getJFTPrint("JFT:98");	
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
		List<JobForTeacher> lstJobForTeacher = new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			if(teacherDetail!=null && districtMaster!=null){
				criteria.add(Restrictions.eq("teacherId",teacherDetail));
				criteria.add(Restrictions.ne("status",statusMaster));
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				lstJobForTeacher = criteria.list();
			}	
					
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   
   /********Hired Candidate with district *********/	
	@Transactional(readOnly=false)
	public List<JobForTeacher> hiredCandidateListByDistrict(DistrictMaster districtMaster, Order sortOrderStrVal, boolean teacherDetailFlag)
	{
		PrintOnConsole.getJFTPrint("JFT:99");	
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion hiredcriterion = Restrictions.eq("status", statusMaster);
			
			criteria.add(hiredcriterion);
			//Criteria c1 = null;
			Criteria c2 = null;
			
			//c1 = criteria.createCriteria("jobId");
			c2 = criteria.createCriteria("teacherId");
			
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			 //c1.add(Restrictions.eq("districtMaster",districtMaster));
		     
			if(teacherDetailFlag){
				c2.addOrder(sortOrderStrVal);
			}
			lstJobForTeacher =  criteria.list();
		}else{
			lstJobForTeacher = null;
		}
	}catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}

	/***************/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestapplication(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName) 
	{
		PrintOnConsole.getJFTPrint("JFT:100");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
          
			
           if(sortColomnName.equalsIgnoreCase("jobforteacherid"))
				sortColomnName = "Internal_Record_ID";
			else if(sortColomnName.equalsIgnoreCase("teacherid"))
				sortColomnName = "Internal_Teacher_ID";
			else if(sortColomnName.equalsIgnoreCase("jobid"))
				sortColomnName = "Internal_Job_ID";
			else if(sortColomnName.equalsIgnoreCase("jobtitle"))
				sortColomnName = "Job_Title";
			else if(sortColomnName.equalsIgnoreCase("jobstartdate"))
				sortColomnName = "Job_Posted_Date";
			else if(sortColomnName.equalsIgnoreCase("jobenddate"))
				sortColomnName = "Job_Post_End_Date";
			else if(sortColomnName.equalsIgnoreCase("jobCategoryName"))
				sortColomnName = "Job_Category";
			else if(sortColomnName.equalsIgnoreCase("subjectname"))
				sortColomnName = "Job_Subject";
			else if(sortColomnName.equalsIgnoreCase("createdDateTime"))
				sortColomnName = "Application_Record_date";
			else if(sortColomnName.equalsIgnoreCase("status"))
				sortColomnName = "Application_Status";
			else if(sortColomnName.equalsIgnoreCase("AmricanIndian"))
				sortColomnName = "Status_Life_Cycle_Status";
			else if(sortColomnName.equalsIgnoreCase("schoolname"))
				sortColomnName = "Hired_By_School";
			else if(sortColomnName.equalsIgnoreCase("coverletter"))
				sortColomnName = "Have_Cover_Letter";
			else if(sortColomnName.equalsIgnoreCase("isAffilated"))
				sortColomnName = "Is_Affilated";
			else if(sortColomnName.equalsIgnoreCase("lastactivity"))
				sortColomnName = "Last_Activity_On_Application";
			else if(sortColomnName.equalsIgnoreCase("lastActivityDate"))
				sortColomnName = "Last_Activity_Date";
			else if(sortColomnName.equalsIgnoreCase("demodatescheduled"))
				sortColomnName = "Demo_Date_Scheduled";
			else if(sortColomnName.equalsIgnoreCase("demodatecancelled"))
				sortColomnName = "Demo_Date_Cancelled";
           
           
			
          
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql="  select "+
			 "	jft.jobforteacherid as `Internal_Record_ID`, "+
			 "	jft.teacherid as `Internal_Teacher_ID`,  "+
			  "   jo.jobid as `Internal_Job_ID`,  "+
			 "	jo.jobtitle as `Job_Title`,  "+
			  "   jo.jobstartdate as `Job_Posted_Date`,  "+
			 "	ifnull(jo.jobenddate,'') as `Job_Post_End_Date`,  "+
			  "   ifnull(jcm.jobCategoryName,'') as `Job_Category`,  "+
			 "	ifnull(sjm.subjectname,'') as `Job_Subject`,  "+
			  "   jft.createdDateTime as `Application_Record_date`, "+
			 "	sm.status as `Application_Status`, "+
			 "	IF(jft.displaystatusid is null, pss.secondarystatusname,  "+
			 "	   IF(ps.secondarystatusname is null,  "+
			 "		  IF(sm.status = 'Completed', 'Available', sm.status),  "+
			 "		  ps.secondarystatusname)) as `Status_Life_Cycle_Status`,  "+
			 "	ifnull(schm.schoolname,'') as `Hired_By_School`, "+
			 "	case  "+
			 "		when jft.coverletter is null then 'No' else 'Yes' end as `Have_Cover_Letter`, "+
			 "	case  "+
			 "		when jft.isAffilated = 1 then 'Yes' else 'No' end as `Is_Affilated`,  "+
			 "	ifnull(jft.lastactivity,'') as `Last_Activity_On_Application`,  "+
			  "   ifnull(jft.lastActivityDate,'') as `Last_Activity_Date`, "+
			   "  ifnull(dcs1.demodate,'') as `Demo_Date_Scheduled`, "+
			 "	ifnull(dcs2.demodate,'') as `Demo_Date_Cancelled` "+
			 " from jobforteacher jft "+
			 "	left join joborder jo on jft.jobid=jo.jobid "+
			 "	left join schoolmaster schm on schm.schoolid=jft.hiredBySchool "+
			 "	left join jobcategorymaster jcm on jcm.jobCategoryId=jo.jobCategoryId "+
			 "	left join subjectmaster sjm on sjm.subjectid=jo.subjectId "+
			 "	left join statusmaster sm on sm.statusid=jft.status "+
			 "	left join secondarystatus ps on jft.displaystatusId =ps.parentStatusId "+
			 "		and ps.districtid = jo.districtid "+
			 "		and ps.jobcategoryid = jo.jobcategoryid "+
			 "	left join secondarystatus pss on jft.displaysecondarystatusid=pss.secondarystatusid  "+
			 "		and pss.districtid = jo.districtid "+
			 "		and pss.jobcategoryid = jo.jobcategoryid "+
			 "	left join democlassschedule dcs1 on dcs1.teacherid=jft.teacherid  "+
			 "		and dcs1.jobid=jft.jobid and dcs1.demostatus!='cancelled' and dcs1.demoId= "+
			 "					(select max(demoid) from democlassschedule where dcs1.teacherid=teacherid  "+
			 "						and dcs1.jobid=jobid and demostatus!='cancelled') "+
			 "	left join democlassschedule dcs2 on dcs2.teacherid=jft.teacherid and dcs2.demoId= "+
			 "					(select max(demoid) from democlassschedule where dcs2.teacherid=teacherid  "+
			 "						and dcs2.jobid=jobid and demostatus='cancelled') "+
			 "		and dcs2.jobid=jft.jobid and dcs2.demostatus='cancelled' "+
			 " where jo.districtid='7800038' and jft.status!='8' "+
		""+orderby;
			 
		 
		 
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	
		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}
	/**************/
	
	
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestapplicationRun() 
	{
		System.out.println("Dao method call from ajax for schduler");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
          
			String orderby = " order by Internal_Record_ID ";
			
			 sql="  select "+
			 "	jft.jobforteacherid as `Internal_Record_ID`, "+
			 "	jft.teacherid as `Internal_Teacher_ID`,  "+
			  "   jo.jobid as `Internal_Job_ID`,  "+
			 "	jo.jobtitle as `Job_Title`,  "+
			  "   jo.jobstartdate as `Job_Posted_Date`,  "+
			 "	ifnull(jo.jobenddate,'') as `Job_Post_End_Date`,  "+
			  "   ifnull(jcm.jobCategoryName,'') as `Job_Category`,  "+
			 "	ifnull(sjm.subjectname,'') as `Job_Subject`,  "+
			  "   jft.createdDateTime as `Application_Record_date`, "+
			 "	sm.status as `Application_Status`, "+
			 "	IF(jft.displaystatusid is null, pss.secondarystatusname,  "+
			 "	   IF(ps.secondarystatusname is null,  "+
			 "		  IF(sm.status = 'Completed', 'Available', sm.status),  "+
			 "		  ps.secondarystatusname)) as `Status_Life_Cycle_Status`,  "+
			 "	ifnull(schm.schoolname,'') as `Hired_By_School`, "+
			 "	case  "+
			 "		when jft.coverletter is null then 'No' else 'Yes' end as `Have_Cover_Letter`, "+
			 "	case  "+
			 "		when jft.isAffilated = 1 then 'Yes' else 'No' end as `Is_Affilated`,  "+
			 "	ifnull(jft.lastactivity,'') as `Last_Activity_On_Application`,  "+
			  "   ifnull(jft.lastActivityDate,'') as `Last_Activity_Date`, "+
			   "  ifnull(dcs1.demodate,'') as `Demo_Date_Scheduled`, "+
			 "	ifnull(dcs2.demodate,'') as `Demo_Date_Cancelled` "+
			 " from jobforteacher jft "+
			 "	left join joborder jo on jft.jobid=jo.jobid "+
			 "	left join schoolmaster schm on schm.schoolid=jft.hiredBySchool "+
			 "	left join jobcategorymaster jcm on jcm.jobCategoryId=jo.jobCategoryId "+
			 "	left join subjectmaster sjm on sjm.subjectid=jo.subjectId "+
			 "	left join statusmaster sm on sm.statusid=jft.status "+
			 "	left join secondarystatus ps on jft.displaystatusId =ps.parentStatusId "+
			 "		and ps.districtid = jo.districtid "+
			 "		and ps.jobcategoryid = jo.jobcategoryid "+
			 "	left join secondarystatus pss on jft.displaysecondarystatusid=pss.secondarystatusid  "+
			 "		and pss.districtid = jo.districtid "+
			 "		and pss.jobcategoryid = jo.jobcategoryid "+
			 "	left join democlassschedule dcs1 on dcs1.teacherid=jft.teacherid  "+
			 "		and dcs1.jobid=jft.jobid and dcs1.demostatus!='cancelled' and dcs1.demoId= "+
			 "					(select max(demoid) from democlassschedule where dcs1.teacherid=teacherid  "+
			 "						and dcs1.jobid=jobid and demostatus!='cancelled') "+
			 "	left join democlassschedule dcs2 on dcs2.teacherid=jft.teacherid and dcs2.demoId= "+
			 "					(select max(demoid) from democlassschedule where dcs2.teacherid=teacherid  "+
			 "						and dcs2.jobid=jobid and demostatus='cancelled') "+
			 "		and dcs2.jobid=jft.jobid and dcs2.demostatus='cancelled' "+
			 " where jo.districtid='7800038' and jft.status!='8' "+
		""+orderby;
			 
		 
		 
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	
		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			System.out.println("return list size======"+lst.size());
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					
					e.printStackTrace();
				}
		}
		return null;
	}
	
	/*********nobleeeeeeee************/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestcandidate(int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName) 
	{
		PrintOnConsole.getJFTPrint("JFT:101");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
			
            System.out.println(" sortOrderStrVal :: "+sortOrderStrVal+" sortColomnName "+sortColomnName);
          
			
           if(sortColomnName.equalsIgnoreCase("IntenalTeacherId"))
				sortColomnName = "Internal_Teacher_ID";
			else if(sortColomnName.equalsIgnoreCase("fname"))
				sortColomnName = "Candidate_First_Name";
			else if(sortColomnName.equalsIgnoreCase("lname"))
				sortColomnName = "Candidate_Last_Name";
			else if(sortColomnName.equalsIgnoreCase("mname"))
				sortColomnName = "Candidate_Middle_Name";
			else if(sortColomnName.equalsIgnoreCase("email"))
				sortColomnName = "Teacher_Email";
			else if(sortColomnName.equalsIgnoreCase("pno"))
				sortColomnName = "Phone_Number";
			else if(sortColomnName.equalsIgnoreCase("mno"))
				sortColomnName = "Mobile_Number";
			else if(sortColomnName.equalsIgnoreCase("add1"))
				sortColomnName = "Address1";
			else if(sortColomnName.equalsIgnoreCase("add2"))
				sortColomnName = "Address2";
			else if(sortColomnName.equalsIgnoreCase("city"))
				sortColomnName = "City";
			else if(sortColomnName.equalsIgnoreCase("state"))
				sortColomnName = "State";
			else if(sortColomnName.equalsIgnoreCase("zip"))
				sortColomnName = "Zip_Code";
			else if(sortColomnName.equalsIgnoreCase("country"))
				sortColomnName = "Country";
			else if(sortColomnName.equalsIgnoreCase("gender"))
				sortColomnName = "Gender";
			else if(sortColomnName.equalsIgnoreCase("race"))
				sortColomnName = "Race";
			else if(sortColomnName.equalsIgnoreCase("ic"))
				sortColomnName = "Internal_Candidate";
			
          
			String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
			
			 sql=" Select "+
			 "	x.`Internal_Teacher_ID`, `Candidate_First_Name`, `Candidate_Last_Name`,  `Candidate_Middle_Name`, `Teacher_Email`,  "+
			 "	`Phone_Number`, `Mobile_Number`, `Address1`, `Address2`, `City`, `State`, `Zip_Code`, `Country`,  "+
			 "	`Gender`,  "+
			 "    case  "+
			 "		when `Race`='Asian' then 'Asian or Pacific Islander' "+
			 "		when `Race` like 'Black/%' then 'Black or African American' "+
			 "		when `Race` like 'White' then 'White/Not Hispanic origin' "+
			 "		when `Race` like 'Hispanic' then 'Hispanic/Latino'         "+
			 "		else `Race` "+
			 "    end as `Race`,  "+
			  "   `Internal_Candidate`, `Record_Status`, `District_Name`, `Affidavit_Accepted`, `Years_of_Teaching`,  "+
			 "	`Year_of_National_Board_Certified`, `Teach_for_America`,  `Year_at_TFA`,  `TFA_Region`, `EPI_Norm_Score`, "+
			 "	`Attitudinal_Factors_Score`,  "+
			  "   `Cognitive_Ability_Score`,  "+
			  "   `Teaching_Skills_Score`, "+
			 "	ifnull(`A_Score`,'') as `Achievement_Score`,  "+
			 "	ifnull(`L_R_Score`,'') as `L_R_Score`,   "+
			 "    ifnull(`MA_Score`, '') as `MA_Score`, "+
			 "    ifnull(`NF_Score`,'') as `NF_Score`, "+
			 "	`PNQ`,  "+
			 "    `Other_Languages`,  "+
			 "    `Can_Sub`,  "+
			 "	ifnull(`Doctorates_Year`,'') as `Doctorates_Year`,  "+
			 "    ifnull(`Doctorates_Degree`,'') as `Doctorates_Degree`,  "+
			 "    ifnull(`Doctorates_School`,'') as `Doctorates_School`,  "+
			 "    ifnull(`Doctorates_GPA`,'') as `Doctorates_GPA`,  "+
			 "	ifnull(`Doctorates_field`,'') as `Doctorates_field`, "+
			 "    ifnull(`Doctorates_Transcript`,'') as `Doctorates_Transcript`, "+
			 "	ifnull(`Masters_Year`,'') as `Masters_Year`,  "+
			 "    ifnull(`Masters_Degree`,'') as `Masters_Degree`,  "+
			 "    ifnull(`Masters_School`,'') as `Masters_School`,  "+
			 "    ifnull(`Masters_GPA`,'') as `Masters_GPA`,  "+
			 "	ifnull(`Masters_field`,'') as `Masters_field`, "+
			 "    ifnull(`Masters_Transcript`,'') as `Masters_Transcript`, "+
			 "    ifnull(`Bachelors_Year`,'') as `Bachelors_Year`,  "+
			 "	ifnull(`Bachelors_Degree`,'') as `Bachelors_Degree`,  "+
			 "    ifnull(`Bachelors_School`,'') as `Bachelors_School`,  "+
			 "    ifnull(`Bachelors_GPA`,'') as `Bachelors_GPA`, "+
			 "	ifnull(`Bachelors_field`,'') as `Bachelors_field`, "+
			 "    ifnull(`Bachelors_Transcript`,'') as `Bachelors_Transcript`,  "+
			 "    ifnull(`Associates_Year`,'') as `Associates_Year`,  "+
			 "	ifnull(`Associates_Degree`,'') as `Associates_Degree`,  "+
			 "    ifnull(`Associates_School`,'') as `Associates_School`,  "+
			 "    ifnull(`Associates_GPA`,'') as `Associates_GPA`,  "+
			 "	ifnull(`Associates_field`,'') as `Associates_field`, "+
			 "    ifnull(`Associates_Transcript`,'') as `Associates_Transcript` "+
			 " from "+
			 "	(select  "+
			 "		t.teacherid `Internal_Teacher_ID`, "+
			 "		t.firstName `Candidate_First_Name`,  "+
			 "		t.lastName `Candidate_Last_Name`,   "+
			 "		ifnull(tp.middleName,'') `Candidate_Middle_Name`, "+
			 "		t.emailAddress `Teacher_Email`,  "+
			 "		ifnull(tp.phonenumber,'') `Phone_Number`, "+
			 "		ifnull(tp.mobileNumber,'') `Mobile_Number`, "+
			 "		ifnull(tp.addressline1,'') `Address1`,  "+
			 "		ifnull(tp.addressline2,'') `Address2`,  "+
			 "		ifnull(cmtp.cityName,'') `City`,  "+
			 "		ifnull(smtp.stateShortName,'') `State`, "+
			 "		ifnull(tp.zipcode,'') `Zip_Code`,  "+
			 "		ifnull(ctrytp.name,'') `Country`, "+
			 "		ifnull(gm.genderName,'') `Gender`,  "+
			  "       ifnull(case when tp.ethnicityId=1 then 'Hispanic/Latino' else "+
			 "			if (tp.ethnicOriginId in (3,7,8), 'Hispanic/Latino', "+
			 "				case "+
			 "					when length(tp.raceid)>2 then 'Multi-Racial' "+
			 "					when length(tp.raceid)<=2 then rm.raceName      "+      
			 "					when tp.raceid is null and tp.ethnicOriginId is not null then eom.ethnicOriginName "+
			 "					when tp.raceid is null and tp.ethnicOriginId is null and tp.ethnicityId is not null then em.ethnicityName "+
			 "				end "+
			 "			) "+
			 "		end,'') as `Race`, "+
			 "		case when ta.affidavitAccepted ='1' then 'Yes' else 'No' end as `Affidavit_Accepted`, "+
			 "		ifnull(te.expcertteachertraining,'') `Years_of_Teaching`,  "+
			 "		ifnull(te.nationalboardcertyear,'') `Year_of_National_Board_Certified`, "+
			 "		ifnull(tfam.tfaAffiliateName,'') `Teach_for_America`,  "+
			 "		ifnull(te.corpsyear,'') `Year_at_TFA`,  "+
			 "		ifnull(tfa.tfaRegionName,'') `TFA_Region`, "+
			 "		ifnull(epi.teachernormscore,'') `EPI_Norm_Score`, "+
			 "		ifnull(adsAT.normscore,'') as `Attitudinal_Factors_Score`, "+
			 "		ifnull(adsCA.normscore,'') as `Cognitive_Ability_Score`, "+
			 "		ifnull(adsTS.normscore,'') as `Teaching_Skills_Score`, "+
			 "		case "+
			 "			when internalTransferCandidate='0' then 'No' "+
			 "			else 'Yes' end as `Internal_Candidate`,  "+
			 "		case "+
			 "			when t.status='A' then 'Active' else 'Inactive' end as `Record_Status`, "+
			 "			d.districtName `District_Name`, "+
			 "		case "+
			 "			when te.potentialQuality = '0' then 'Not Selected' "+
			 "			when te.potentialQuality = '1' then 'Yes' "+
			 "			when te.potentialQuality = '2'  then 'No' "+
			 "			else 'Pre Historical' end as  `PNQ`, "+
			 "		case  "+
			 "			when te.knowOtherlanguages=1 then 'Yes' else 'No' end as `Other_Languages`, "+
			 "		case  "+
			 "			when te.canServeAsSubTeacher = 1 then 'Yes' else 'No' end as `Can_Sub`, "+
			 "		ifnull(academicAchievementScore,'') as `A_Score`, "+
			 "		ifnull(leadershipAchievementScore,'') as `L_R_Score`,  "+
			  "       ifnull(dspanf.sliderScore,'') as `NF_Score`,  "+
			  "       ifnull(dspama.sliderScore,'') as `MA_Score` "+
			 "	from jobforteacher jt "+
			 "		left join teacherdetail t on jt.teacherid=t.teacherid "+
			 "		left join teacherpersonalinfo tp on t.teacherid=tp.teacherid "+
			 "		left join gendermaster gm on tp.genderId=gm.genderId "+
			 "		left join racemaster rm on tp.raceId=rm.raceId "+
			  "       left join ethnicoriginmaster eom on eom.ethnicOriginId=tp.ethnicOriginId "+
			 "        left join ethinicitymaster em on em.ethnicityId=tp.ethnicityId "+
			 "		left join joborder jo on jt.jobid=jo.jobid "+
			 "		left join districtmaster d on jo.districtid=d.districtid and d.status='A' "+
			 "		left join statusmaster sm on sm.statusid=jt.status "+
			 "		left join teachernormscore epi on t.teacherid=epi.teacherid "+
			 "		left join statemaster smtp on tp.stateid=smtp.stateid "+
			 "		left join citymaster cmtp on tp.cityid=cmtp.cityid "+
			 "		left join countrymaster ctrytp on tp.countryId=ctrytp.countryId "+
			 "		left join assessmentdomainscore adsAT on epi.teacherid=adsAT.teacherid and adsAT.domainid=1  "+
			 "		left join assessmentdomainscore adsTS on epi.teacherid=adsTS.teacherid and adsTS.domainid=3  "+
			 "		left join assessmentdomainscore adsCA on epi.teacherid=adsCA.teacherid and adsCA.domainid=2 "+
			 "		left join teacherexperience te on t.teacherid=te.teacherid "+
			 "		left join tfaregionmaster tfa on te.tfaRegionId=tfa.tfaregionID "+
			 "		left join teacheraffidavit ta on ta.teacherid=t.teacherid  "+
			 "					and ta.affidavitid=(select min(affidavitid) min_aid from teacheraffidavit where t.teacherid=teacherid) "+
			 "		left join tfaaffiliatemaster as tfam on tfam.tfaAffiliateId=te.tfaAffiliateId "+
			  "       left join teacherachievementscore as tas on tas.teacherid=t.teacherid "+
			  "                                           and tas.teacherAchievementScoreId=(select max(teacherAchievementScoreId) lastscore "+
			 "												from teacherachievementscore  where tas.teacherid=teacherid) "+
			 "		left join districtspecificportfolioanswers as dspanf on dspanf.teacherid=t.teacherid  "+
			 "											and dspanf.questionid in (3) and dspanf.sliderScore is not null "+
			 "		left join districtspecificportfolioanswers as dspama on dspama.teacherid=t.teacherid  "+
			 "											and dspama.questionid in (4) and dspama.sliderScore is not null "+
			 "		inner join (select min(jobforteacherid) min_jid  "+
			 "					from jobforteacher  "+
			 "						inner join joborder on joborder.jobid=jobforteacher.jobid  "+
			 "					where jobforteacher.status != '8'  "+
			 "						and joborder.districtid='7800038'  "+
			 "					group by teacherid) A on min_jid = jt.jobforteacherid "+
			 "	) as x  "+
			 "	, "+
			 "	(select  jt.teacherid `Internal_Teacher_ID`,  "+
			 "		max(leftinYear), "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Doctorates_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_Transcript, "+
			 "			group_concat(case "+
			 "				when dm.degreeType='M'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Masters_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Masters_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Masters_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Masters_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Masters_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Masters_Transcript, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B' "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Bachelors_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_Transcript, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Associates_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Associates_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Associates_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Associates_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Associates_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Associates_Transcript "+
			 "		from jobforteacher jt "+
			 "			left join joborder jo on jt.jobid=jo.jobid "+
			 "			left join teacheracademics ta on ta.teacherid=jt.teacherid "+
			 "			left join degreemaster dm on ta.degreeId=dm.degreeId "+
			 "			left join universitymaster um on ta.universityId=um.universityId "+
			 "			left join fieldofstudymaster fsm on ta.fieldId=fsm.fieldId "+
			 "			inner join (select min(jobforteacherid) min_jid  "+
			 "							from jobforteacher  "+
			 "								inner join joborder on joborder.jobid=jobforteacher.jobid  "+
			 "							where jobforteacher.status != '8'  "+
			 "								and joborder.districtid='7800038'  "+
			 "							group by teacherid) A on min_jid = jt.jobforteacherid "+
			 "		group by jt.teacherId "+
			 "	) as z  "+
			 "	where x.Internal_Teacher_ID=z.Internal_Teacher_ID "+
			 ""+orderby;
			 
		 
		 
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	
		ResultSet rs=ps.executeQuery();
			
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}
	/**********************/
	/*******csv schduler for noble candidate*********/
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public  List<String[]> noblestCandidateRun() 
	{
		PrintOnConsole.getJFTPrint("JFT:102");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		 Connection connection =null;
		try {
		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		      connection = connectionProvider.getConnection();
			
			String orderby = " order by Internal_Teacher_ID";
			
			 sql=" Select "+
			 "	x.`Internal_Teacher_ID`, `Candidate_First_Name`, `Candidate_Last_Name`,  `Candidate_Middle_Name`, `Teacher_Email`,  "+
			 "	`Phone_Number`, `Mobile_Number`, `Address1`, `Address2`, `City`, `State`, `Zip_Code`, `Country`,  "+
			 "	`Gender`,  "+
			 "    case  "+
			 "		when `Race`='Asian' then 'Asian or Pacific Islander' "+
			 "		when `Race` like 'Black/%' then 'Black or African American' "+
			 "		when `Race` like 'White' then 'White/Not Hispanic origin' "+
			 "		when `Race` like 'Hispanic' then 'Hispanic/Latino'         "+
			 "		else `Race` "+
			 "    end as `Race`,  "+
			  "   `Internal_Candidate`, `Record_Status`, `District_Name`, `Affidavit_Accepted`, `Years_of_Teaching`,  "+
			 "	`Year_of_National_Board_Certified`, `Teach_for_America`,  `Year_at_TFA`,  `TFA_Region`, `EPI_Norm_Score`, "+
			 "	`Attitudinal_Factors_Score`,  "+
			  "   `Cognitive_Ability_Score`,  "+
			  "   `Teaching_Skills_Score`, "+
			 "	ifnull(`A_Score`,'') as `Achievement_Score`,  "+
			 "	ifnull(`L_R_Score`,'') as `L_R_Score`,   "+
			 "    ifnull(`MA_Score`, '') as `MA_Score`, "+
			 "    ifnull(`NF_Score`,'') as `NF_Score`, "+
			 "	`PNQ`,  "+
			 "    `Other_Languages`,  "+
			 "    `Can_Sub`,  "+
			 "	ifnull(`Doctorates_Year`,'') as `Doctorates_Year`,  "+
			 "    ifnull(`Doctorates_Degree`,'') as `Doctorates_Degree`,  "+
			 "    ifnull(`Doctorates_School`,'') as `Doctorates_School`,  "+
			 "    ifnull(`Doctorates_GPA`,'') as `Doctorates_GPA`,  "+
			 "	ifnull(`Doctorates_field`,'') as `Doctorates_field`, "+
			 "    ifnull(`Doctorates_Transcript`,'') as `Doctorates_Transcript`, "+
			 "	ifnull(`Masters_Year`,'') as `Masters_Year`,  "+
			 "    ifnull(`Masters_Degree`,'') as `Masters_Degree`,  "+
			 "    ifnull(`Masters_School`,'') as `Masters_School`,  "+
			 "    ifnull(`Masters_GPA`,'') as `Masters_GPA`,  "+
			 "	ifnull(`Masters_field`,'') as `Masters_field`, "+
			 "    ifnull(`Masters_Transcript`,'') as `Masters_Transcript`, "+
			 "    ifnull(`Bachelors_Year`,'') as `Bachelors_Year`,  "+
			 "	ifnull(`Bachelors_Degree`,'') as `Bachelors_Degree`,  "+
			 "    ifnull(`Bachelors_School`,'') as `Bachelors_School`,  "+
			 "    ifnull(`Bachelors_GPA`,'') as `Bachelors_GPA`, "+
			 "	ifnull(`Bachelors_field`,'') as `Bachelors_field`, "+
			 "    ifnull(`Bachelors_Transcript`,'') as `Bachelors_Transcript`,  "+
			 "    ifnull(`Associates_Year`,'') as `Associates_Year`,  "+
			 "	ifnull(`Associates_Degree`,'') as `Associates_Degree`,  "+
			 "    ifnull(`Associates_School`,'') as `Associates_School`,  "+
			 "    ifnull(`Associates_GPA`,'') as `Associates_GPA`,  "+
			 "	ifnull(`Associates_field`,'') as `Associates_field`, "+
			 "    ifnull(`Associates_Transcript`,'') as `Associates_Transcript` "+
			 " from "+
			 "	(select  "+
			 "		t.teacherid `Internal_Teacher_ID`, "+
			 "		t.firstName `Candidate_First_Name`,  "+
			 "		t.lastName `Candidate_Last_Name`,   "+
			 "		ifnull(tp.middleName,'') `Candidate_Middle_Name`, "+
			 "		t.emailAddress `Teacher_Email`,  "+
			 "		ifnull(tp.phonenumber,'') `Phone_Number`, "+
			 "		ifnull(tp.mobileNumber,'') `Mobile_Number`, "+
			 "		ifnull(tp.addressline1,'') `Address1`,  "+
			 "		ifnull(tp.addressline2,'') `Address2`,  "+
			 "		ifnull(cmtp.cityName,'') `City`,  "+
			 "		ifnull(smtp.stateShortName,'') `State`, "+
			 "		ifnull(tp.zipcode,'') `Zip_Code`,  "+
			 "		ifnull(ctrytp.name,'') `Country`, "+
			 "		ifnull(gm.genderName,'') `Gender`,  "+
			 "       ifnull(case when tp.ethnicityId=1 then 'Hispanic/Latino' else "+
			 "			if (tp.ethnicOriginId in (3,7,8), 'Hispanic/Latino', "+
			 "				case "+
			 "					when length(tp.raceid)>2 then 'Multi-Racial' "+
			 "					when length(tp.raceid)<=2 then rm.raceName      "+      
			 "					when tp.raceid is null and tp.ethnicOriginId is not null then eom.ethnicOriginName "+
			 "					when tp.raceid is null and tp.ethnicOriginId is null and tp.ethnicityId is not null then em.ethnicityName "+
			 "				end "+
			 "			) "+
			 "		end,'') as `Race`, "+
			 "		case when ta.affidavitAccepted ='1' then 'Yes' else 'No' end as `Affidavit_Accepted`, "+
			 "		ifnull(te.expcertteachertraining,'') `Years_of_Teaching`,  "+
			 "		ifnull(te.nationalboardcertyear,'') `Year_of_National_Board_Certified`, "+
			 "		ifnull(tfam.tfaAffiliateName,'') `Teach_for_America`,  "+
			 "		ifnull(te.corpsyear,'') `Year_at_TFA`,  "+
			 "		ifnull(tfa.tfaRegionName,'') `TFA_Region`, "+
			 "		ifnull(epi.teachernormscore,'') `EPI_Norm_Score`, "+
			 "		ifnull(adsAT.normscore,'') as `Attitudinal_Factors_Score`, "+
			 "		ifnull(adsCA.normscore,'') as `Cognitive_Ability_Score`, "+
			 "		ifnull(adsTS.normscore,'') as `Teaching_Skills_Score`, "+
			 "		case "+
			 "			when internalTransferCandidate='0' then 'No' "+
			 "			else 'Yes' end as `Internal_Candidate`, "+ 
			 "		case "+
			 "			when t.status='A' then 'Active' else 'Inactive' end as `Record_Status`, "+
			 "			d.districtName `District_Name`, "+
			 "		case "+
			 "			when te.potentialQuality = '0' then 'Not Selected' "+
			 "			when te.potentialQuality = '1' then 'Yes' "+
			 "			when te.potentialQuality = '2'  then 'No' "+
			 "			else 'Pre Historical' end as  `PNQ`, "+
			 "		case  "+
			 "			when te.knowOtherlanguages=1 then 'Yes' else 'No' end as `Other_Languages`, "+
			 "		case  "+
			 "			when te.canServeAsSubTeacher = 1 then 'Yes' else 'No' end as `Can_Sub`, "+
			 "		ifnull(academicAchievementScore,'') as `A_Score`, "+
			 "		ifnull(leadershipAchievementScore,'') as `L_R_Score`,  "+
			  "       ifnull(dspanf.sliderScore,'') as `NF_Score`,  "+
			  "       ifnull(dspama.sliderScore,'') as `MA_Score` "+
			 "	from jobforteacher jt "+
			 "		left join teacherdetail t on jt.teacherid=t.teacherid "+
			 "		left join teacherpersonalinfo tp on t.teacherid=tp.teacherid "+
			 "		left join gendermaster gm on tp.genderId=gm.genderId "+
			 "		left join racemaster rm on tp.raceId=rm.raceId "+
			  "       left join ethnicoriginmaster eom on eom.ethnicOriginId=tp.ethnicOriginId "+
			 "        left join ethinicitymaster em on em.ethnicityId=tp.ethnicityId "+
			 "		left join joborder jo on jt.jobid=jo.jobid "+
			 "		left join districtmaster d on jo.districtid=d.districtid and d.status='A' "+
			 "		left join statusmaster sm on sm.statusid=jt.status "+
			 "		left join teachernormscore epi on t.teacherid=epi.teacherid "+
			 "		left join statemaster smtp on tp.stateid=smtp.stateid "+
			 "		left join citymaster cmtp on tp.cityid=cmtp.cityid "+
			 "		left join countrymaster ctrytp on tp.countryId=ctrytp.countryId "+
			 "		left join assessmentdomainscore adsAT on epi.teacherid=adsAT.teacherid and adsAT.domainid=1  "+
			 "		left join assessmentdomainscore adsTS on epi.teacherid=adsTS.teacherid and adsTS.domainid=3  "+
			 "		left join assessmentdomainscore adsCA on epi.teacherid=adsCA.teacherid and adsCA.domainid=2 "+
			 "		left join teacherexperience te on t.teacherid=te.teacherid "+
			 "		left join tfaregionmaster tfa on te.tfaRegionId=tfa.tfaregionID "+
			 "		left join teacheraffidavit ta on ta.teacherid=t.teacherid  "+
			 "					and ta.affidavitid=(select min(affidavitid) min_aid from teacheraffidavit where t.teacherid=teacherid) "+
			 "		left join tfaaffiliatemaster as tfam on tfam.tfaAffiliateId=te.tfaAffiliateId "+
			  "       left join teacherachievementscore as tas on tas.teacherid=t.teacherid "+
			  "                                           and tas.teacherAchievementScoreId=(select max(teacherAchievementScoreId) lastscore "+
			 "												from teacherachievementscore  where tas.teacherid=teacherid) "+
			 "		left join districtspecificportfolioanswers as dspanf on dspanf.teacherid=t.teacherid  "+
			 "											and dspanf.questionid in (3) and dspanf.sliderScore is not null "+
			 "		left join districtspecificportfolioanswers as dspama on dspama.teacherid=t.teacherid  "+
			 "											and dspama.questionid in (4) and dspama.sliderScore is not null "+
			 "		inner join (select min(jobforteacherid) min_jid  "+
			 "					from jobforteacher  "+
			 "						inner join joborder on joborder.jobid=jobforteacher.jobid  "+
			 "					where jobforteacher.status != '8'  "+
			 "						and joborder.districtid='7800038'  "+
			 "					group by teacherid) A on min_jid = jt.jobforteacherid "+
			 "	) as x  "+
			 "	, "+
			 "	(select  jt.teacherid `Internal_Teacher_ID`,  "+
			 "		max(leftinYear), "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Doctorates_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='D'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Doctorates_Transcript, "+
			 "			group_concat(case "+
			 "				when dm.degreeType='M'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Masters_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Masters_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Masters_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Masters_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Masters_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='M'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Masters_Transcript, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B' "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Bachelors_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='B'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Bachelors_Transcript, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then LeftInYear end order by LeftInYear Desc SEPARATOR '||') as Associates_Year, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then universityName end order by LeftInYear Desc SEPARATOR '||')  as Associates_School, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then degreeName end order by LeftInYear Desc SEPARATOR '||')  as Associates_Degree, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then gpaCumulative end order by LeftInYear Desc SEPARATOR '||')  as Associates_GPA, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then fieldname end order by LeftInYear Desc SEPARATOR '||')  as Associates_field, "+
			 "			group_concat(case  "+
			 "				when dm.degreeType='A'  "+
			 "				then case when pathOfTranscript is null then 'No' else 'Yes' end end order by LeftInYear Desc SEPARATOR '||')  as Associates_Transcript "+
			 "		from jobforteacher jt "+
			 "			left join joborder jo on jt.jobid=jo.jobid "+
			 "			left join teacheracademics ta on ta.teacherid=jt.teacherid "+
			 "			left join degreemaster dm on ta.degreeId=dm.degreeId "+
			 "			left join universitymaster um on ta.universityId=um.universityId "+
			 "			left join fieldofstudymaster fsm on ta.fieldId=fsm.fieldId "+
			 "			inner join (select min(jobforteacherid) min_jid  "+
			 "							from jobforteacher  "+
			 "								inner join joborder on joborder.jobid=jobforteacher.jobid  "+
			 "							where jobforteacher.status != '8'  "+
			 "								and joborder.districtid='7800038'  "+
			 "							group by teacherid) A on min_jid = jt.jobforteacherid "+
			 "		group by jt.teacherId "+
			 "	) as z  "+
			 "	where x.Internal_Teacher_ID=z.Internal_Teacher_ID "+
			 ""+orderby;
			
		PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
		ResultSet rs=ps.executeQuery();
			if(rs.next()){				
				do{
					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
					   {
						allInfo[i-1]=rs.getString(i);
					   }
					lst.add(allInfo);
					
				}while(rs.next());
			}
			else{
				System.out.println("Record not found.");
			}
			
			return lst;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(connection!=null)
				try {
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return null;
	}
	/***************/
	

	@Transactional(readOnly=false)
	public List<JobForTeacher> verifyJobByDistrictId(TeacherDetail teacherDetail, DistrictMaster districtMaster)
	{
		PrintOnConsole.getJFTPrint("JFT:103");
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			System.out.println(teacherDetail.getTeacherId()+"\taaaaaaaaaa"+districtMaster.getDistrictId());
			Date dateWithoutTime = Utility.getDateWithoutTime();
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
			//criteria.setFirstResult(0);
			//criteria.setMaxResults(1);
			lstJobForTeacher = criteria.list();
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> findbyJobId(JobOrder jobOrder)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session=getSession();
			return session.createCriteria(getPersistentClass()).add(Restrictions.eq("jobId", jobOrder))
			//.setProjection(Projections.projectionList().add(Projections.property("jobForTeacherId")))
			.list();
			
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

	
	

	@Transactional(readOnly=false)
	public List<JobForTeacher> findByTeacherDistrictAndJobCategory(TeacherDetail teacherDetail,DistrictMaster districtMaster,JobCategoryMaster jobCateg) 
	{	
		PrintOnConsole.getJFTPrint("JFT:104");
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
			}
			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster", jobCateg));
//			if(jobCateg!=null)
//			criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster", jobCateg));

			criteria.addOrder(Order.desc("createdDateTime"));
			lstJobForTeachers = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}





	@Transactional(readOnly=false)
	public List<TeacherDetail> getAllQQFinalizedTeacherListByJobCategory(List<TeacherDetail> lstTeacherDetails,JobCategoryMaster jobCat)
	{
		PrintOnConsole.getJFTPrint("JFT:105");
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
					criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster", jobCat));
				    criteria.setProjection(Projections.groupProperty("teacherId"));
				    lstTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstTeacher;
	}
	
	@Transactional(readOnly=false)
	public List<JobForTeacher> findJFTforOfferReadystatusByJobList(List<JobOrder> jobOrderList,Order sortOrderStrVal,List<JobOrder> jobOrders, boolean sortingcheck)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			if(jobOrderList.size()>0){
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.ne("status",statusMaster);
				Criterion criterion2 = Restrictions.in("jobId",jobOrderList);
				criteria.add(criterion1);
				criteria.add(criterion2);
				Criterion isnotNullCriterion    =Restrictions.isNotNull("offerReady");
				criteria.add(isnotNullCriterion);
				
				if(!sortingcheck){
				 criteria.addOrder(sortOrderStrVal);
				}
				if(jobOrders!=null && jobOrders.size()>0)
					criteria.add(Restrictions.in("jobId",jobOrders));
				
				lstJobForTeacher = criteria.list();
			}
			System.out.println("offerReadyofferReady :: "+lstJobForTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
	@Transactional(readOnly=false)
	public List<JobForTeacher> hiredCandidateListWithAndWithoutDistrictByJobList(List<JobOrder> jobOrderList, int sortingcheck, Order sortOrderStrVal)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			//StatusMaster statusMaster = WorkThreadServlet.statusMap.get("hird");
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion hiredcriterion = Restrictions.eq("status", statusMaster);
			criteria.add(hiredcriterion);
			Criterion criterion2 = Restrictions.in("jobId",jobOrderList);
			criteria.add(criterion2);
			if(sortingcheck==3){
				criteria.addOrder(sortOrderStrVal);
			}
			lstJobForTeacher =  criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
		return lstJobForTeacher;
	}
	public List<StatusMaster> getObjectStatus(List<Integer> statusMasterList){
		List<StatusMaster> statusMasters=new ArrayList<StatusMaster>();
		try{
			for(int i=0; i<statusMasterList.size();i++){
				StatusMaster statusMaster=new StatusMaster();
				statusMaster.setStatusId(statusMasterList.get(i));
				statusMasters.add(statusMaster);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return statusMasters;
	}

	@Transactional(readOnly=false)
    public List<String[]> getCandidateJobStatusReport(DistrictMaster districtMaster,List<String> queryList,String orderBy)
    {
    	  System.out.println(queryList.size()+"::::::::getCandidateJobStatusReport ::::"+orderBy);
    	  Session session = getSession();
          List<String[]> rows=new ArrayList<String[]>();
          Connection connection =null;
  		  try {
  		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
  		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
  		    connection = connectionProvider.getConnection();
	
  		      	   String query="";
		  		   for(String queryStr:queryList){
		          	  query+=" "+queryStr;
		           }
		  		   System.out.println("query:::::"+query);
	        	  String sql="select "+

	              "concat(td.firstName,' ', td.lastName,'\n(',td.emailAddress,')'),  "+

	              "dm.districtName, jo.jobTitle,  "+

	              "jo.jobStartDate, jo.jobEndDate,  "+

	              "jft.createdDateTime,  "+

	              "(select concat(IFNULL(tns.teacherNormScore,0),'##',tns.decileColor) from teachernormscore tns where tns.teacherId=jft.teacherId) normScore_colorCode,  "+

	              "case WHEN jft.displayStatusId=null   "+

	                "THEN (select sm.secondaryStatusName from  secondarystatus sm where sm.secondaryStatusId = jft.displaySecondaryStatusId)   "+

	                "ELSE (select  if(sm1.status='Completed','Available',sm1.status) from statusmaster sm1 where sm1.statusId = jft.displayStatusId)   "+

	              "END as jobStatus,  "+

	              "(select tshj.hiredByDate from teacherstatushistoryforjob tshj where tshj.teacherId=jft.teacherId and tshj.jobId=jft.jobId and tshj.status='A' ) hireDate,  "+

	              "IFNULL((select sm.schoolName from schoolmaster sm where sm.schoolId=jft.hiredBySchool),dm.districtName) schoolName,  "+

	              "(SELECT concat(jft1.jobId,'##',COUNT(*),'##',COUNT( IF( jft1.status NOT IN (select statusId from statusmaster sm where sm.statusShortName in ('dcln','icomp','vlt','widrw','hide','rem')), 1, NULL )),'##',COUNT( IF( jft1.status =6, 1, NULL ) )) from jobforteacher jft1 where jft1.jobId =jft.jobId and jft1.status!=8 group by jft1.jobId) allInfo_jobId_toCan_toAvl_toHir,  "+

	              "IF(jft.isAffilated=1, 'Yes', 'No') extOrInternal  "+

	              ",(select concat(tas.academicAchievementScore,'##',tas.academicAchievementMaxScore,'##',leadershipAchievementScore,'##',leadershipAchievementMaxScore) from teacherachievementscore tas where tas.teacherId=jft.teacherId and tas.districtId = dm.districtId) allInfo_TeacherAchievementScore  "+

	              ",(select concat(jwcts.jobWiseConsolidatedScore,'##',jwcts.jobWiseMaxScore) from jobwiseconsolidatedteacherscore jwcts where jwcts.teacherId=jft.teacherId and jwcts.jobId=jo.jobId) allInfo_JobWiseConsolidatedTeacherScore "+

	              "from jobforteacher jft,   "+

	              "teacherdetail td,   "+

	              "districtmaster dm,  "+

	              "joborder jo   "+

	              "where   "+

	              "jft.teacherId=td.teacherId "+

	              "and jft.jobId=jo.jobId   "+

	              "and jo.districtId=dm.districtId  and jft.status!=8   "+query+

	              " and jo.districtId= ? "+

	              " order by "+orderBy;

	
	          System.out.println("sql:::"+sql);
	          PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	          ps.setString(1,districtMaster.getDistrictId().toString());
	          //ps.setString(2,orderBy);
	
	  		  ResultSet rs=ps.executeQuery();
	  			if(rs.next()){				
	  				do{
	  					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
	  					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
	  					   {
	  						allInfo[i-1]=rs.getString(i);
	  					   }
	  					rows.add(allInfo);
	  					//System.out.println("allInfo::::"+allInfo[0]);
	  				}while(rs.next());
	  			}
	  			else{
	  				System.out.println("Record not found.");
	  			}
	  			return rows;
	         }catch (Exception e) {
                e.printStackTrace();
	         }finally{
	 			if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
          return rows;
    }
	@Transactional(readOnly=false)
    public List<String[]> getCandidateEEOCReport(DistrictMaster districtMaster,List<String> queryList,String orderBy)
    {
    	  System.out.println(queryList.size()+"::::::::getCandidateEEOCReport ::::"+orderBy);
    	  Session session = getSession();
          List<String[]> rows=new ArrayList<String[]>();
          Connection connection =null;
  		  try {
  		    SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
  		    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
  		    connection = connectionProvider.getConnection();
	
  		      	   String query="";
		  		   for(String queryStr:queryList){
		          	  query+=" "+queryStr;
		           }
		  		   System.out.println("query:::::"+query);
	        	  String sql="select td.firstName as fName,IFNULL(td.lastName,'') as lName,td.emailAddress as email,raceName,gender,ethName,originName,this.jobId,td.teacherId as teacherId  from"+

	              " jobforteacher this inner join joborder jo on this.jobId=jo.jobId"+

	              " inner join districtmaster dm on jo.districtId=dm.districtId"+

	              " inner join teacherdetail td on this.teacherId=td.teacherId"+
	              
	              " left join( select tpi.teacherId teacherId1, case WHEN tpi.raceId is NULL THEN NULL"+
	              
	              " ELSE (select GROUP_CONCAT(rm.raceName) from racemaster rm where rm.raceId in(tpi.raceId))"+
	              
	              " END as raceName,gm.genderName as gender,em.ethnicityName as ethName,eom.ethnicOriginName as originName from teacherpersonalinfo tpi"+
	              
	              " left join gendermaster gm on tpi.genderId = gm.genderId"+
	              
	              " inner join ethinicitymaster em on tpi.ethnicityId=em.ethnicityId"+
	              
	              " inner join ethnicoriginmaster eom on tpi.ethnicOriginId=eom.ethnicOriginId"+
	              
	              " ) as tpInfo on tpInfo.teacherId1=td.teacherId"+
	              
	              
	              " where"+

	              " this.status!=8 and dm.districtId=? "+query+

	              "  group by this.teacherId order by "+orderBy;

	
	          System.out.println("sql>:::"+sql);
	          PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
	          ps.setString(1,districtMaster.getDistrictId().toString());
	
	  		  ResultSet rs=ps.executeQuery();
	  			if(rs.next()){				
	  				do{
	  					final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
	  					for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
	  					   {
	  						allInfo[i-1]=rs.getString(i);
	  					   }
	  					rows.add(allInfo);
	  					//System.out.println("allInfo::::"+allInfo[0]);
	  				}while(rs.next());
	  			}
	  			else{
	  				System.out.println("Record not found.");
	  			}
	  			return rows;
	         }catch (Exception e) {
                e.printStackTrace();
	         }finally{
	 			if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
          return rows;
    }

    @Transactional
	public JobForTeacher saveJobForTeacher(JobForTeacher jobForTeacher,JobOrder jobOrder,TeacherDetail teacherDetail, String redirectedFromURL, String jobBoardReferralURL)
	{

		StatusMaster statusMaster = null; 
		
		if(jobForTeacher==null || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("widrw") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hide")){
			
			if(jobForTeacher==null){
				jobForTeacher = new JobForTeacher();
				jobForTeacher.setTeacherId(teacherDetail);
				jobForTeacher.setJobId(jobOrder);
				jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
				jobForTeacher.setRedirectedFromURL(redirectedFromURL);
				jobForTeacher.setJobBoardReferralURL(jobBoardReferralURL);
				jobForTeacher.setCoverLetter("");
				jobForTeacher.setIsAffilated(0);
				jobForTeacher.setCreatedDateTime(new Date());
			}
			else{
				jobForTeacher.setUpdatedDate(new Date());
			}
			
			//statusMaster = WorkThreadServlet.statusMap.get("icomp");
			statusMaster= WorkThreadServlet.statusMap.get("icomp");
			
			AssessmentDetail assessmentDetail = null;
			TeacherAssessmentStatus teacherAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(jobForTeacher.getTeacherId());
			if(!jobOrder.getJobCategoryMaster().getBaseStatus()){
				if(jobOrder.getIsJobAssessment()){
					List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
					if(lstAssessmentJobRelation.size()>0)
					{
						AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
						assessmentDetail = assessmentJobRelation.getAssessmentId();
						List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
						
						if(lstTeacherAssessmentStatus.size()>0)
						{						
							teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
							teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
							teacherAssessmentStatus.setJobOrder(jobOrder);
							teacherAssessmentStatus.setCreatedDateTime(new Date());
							teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);						
							statusMaster = teacherAssessmentStatus.getStatusMaster();						
						}
						else
						{
							statusMaster= WorkThreadServlet.statusMap.get("icomp");
						}					
					}
					else
					{
						statusMaster= WorkThreadServlet.statusMap.get("icomp");
					}
				}
				else{
					statusMaster= WorkThreadServlet.statusMap.get("comp");
				}
			}
			else if(jobOrder.getIsJobAssessment()){
				List<AssessmentJobRelation> lstAssessmentJobRelation = assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
				if(lstAssessmentJobRelation.size()>0)
				{
					AssessmentJobRelation assessmentJobRelation = lstAssessmentJobRelation.get(0);
					assessmentDetail = assessmentJobRelation.getAssessmentId();
					List<TeacherAssessmentStatus> lstTeacherAssessmentStatus = teacherAssessmentStatusDAO.findTASByTeacherAndAssessment(teacherDetail, assessmentDetail);
					
					if(lstTeacherAssessmentStatus.size()>0)
					{					
						teacherAssessmentStatus = lstTeacherAssessmentStatus.get(0);
						teacherAssessmentStatus.setTeacherAssessmentStatusId(null);
						teacherAssessmentStatus.setJobOrder(jobOrder);
						teacherAssessmentStatus.setCreatedDateTime(new Date());
						teacherAssessmentStatusDAO.makePersistent(teacherAssessmentStatus);						
						statusMaster = teacherAssessmentStatus.getStatusMaster();						
					}
					else
					{
						statusMaster= WorkThreadServlet.statusMap.get("icomp");
					}					
				}
				else
				{
					statusMaster= WorkThreadServlet.statusMap.get("icomp");
				}
			}
			else{
				
				List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
				JobOrder jOrder = new JobOrder();
				jOrder.setJobId(0);
				teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTaken(jobForTeacher.getTeacherId(),jOrder);
				if(teacherAssessmentStatusList.size()>0){
					teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
					statusMaster = teacherAssessmentStatus.getStatusMaster();
				}else{
					statusMaster= WorkThreadServlet.statusMap.get("icomp");
				}							
			}
			jobForTeacher.setStatus(statusMaster);
			jobForTeacher.setStatusMaster(statusMaster);
			if(statusMaster!=null){
				jobForTeacher.setApplicationStatus(statusMaster.getStatusId());
			}
			//jobForTeacherDAO.makePersistent(jobForTeacher);
			makePersistent(jobForTeacher);
			return jobForTeacher;
		}
		
		return jobForTeacher;
	}
    
    @Transactional(readOnly=false)
    public boolean getJFTByTeachers(TeacherDetail teacherDetail)
    {
    	boolean isKellyOnly = true;
    	List<JobForTeacher> lstJobForTeacherKellyOnly= new ArrayList<JobForTeacher>();
    	List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    	String sqlNonKelly = "";
    	String sqlKellyOnly = "";
    	List<Object[]> rows1 = null;
    	List<Object[]> rows2 = null;
 		try 
		{
			sqlNonKelly = "select jobId from jobforteacher jft where teacherId="+teacherDetail.getTeacherId()+" and jft.jobId in (select jobId from joborder where headQuarterId is null)";
			sqlKellyOnly = "select jobId from jobforteacher jft where teacherId="+teacherDetail.getTeacherId()+" and jft.jobId in (select jobId from joborder where headQuarterId is not null)";
			//System.out.println("sqlNonKelly:: "+sqlNonKelly);
			//System.out.println("sqlKellyOnly:: "+sqlKellyOnly);
			Session session = getSession();
			Query queryNonKelly = session.createSQLQuery(sqlNonKelly);
			Query queryKellyOnly = session.createSQLQuery(sqlKellyOnly);
			
			rows1 = queryNonKelly.list();
			rows2 = queryKellyOnly.list();
			
			if(rows1!=null && rows1.size()>0 && rows2!=null && rows2.size()>0)
				isKellyOnly = false;
			else if(rows1!=null && rows1.size()==0 && rows2!=null && rows2.size()==0)
				isKellyOnly = false;
			else if(rows1!=null && rows1.size()>0 && rows2!=null && rows2.size()==0)
				isKellyOnly = false;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return isKellyOnly;
		}	
		//System.out.println("isKellyOnly::: "+ isKellyOnly);
		return isKellyOnly;
    }
     @Transactional(readOnly=false)
   public List<JobForTeacher> findAllTeacherByHBD(TeacherDetail teacherDetail,JobOrder jobOrder)
   {
   	List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
   	try 
   	{
   		
   			Session session = getSession();
   			Criteria criteria = session.createCriteria(getPersistentClass());
   			criteria.add(Restrictions.eq("teacherId",teacherDetail));
   			criteria.add(Restrictions.isNotNull("offerReady"));
   			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(jobOrder.getDistrictMaster()!=null){
				jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			}
   			lstJobForTeacher = criteria.list();
   	} 
   	catch (Exception e) {
   		e.printStackTrace();
   	}		
   	return lstJobForTeacher;
   }
   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndJobCategoryHBD(TeacherDetail teacherDetail,JobOrder jobOrder,String jobForTeacherId)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(jobOrder.getDistrictMaster()!=null){
				jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("districtMaster"));
			}
			/*if(jobOrder.getBranchMaster()!=null){
				jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("branchMaster"));
			}*/
			if(jobOrder.getHeadQuarterMaster()!=null){
				jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			}
			jobOrderSet.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster()));			
			criteria.add(Restrictions.eq("teacherId",teacherDetail));
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			criteria.add(Restrictions.not(criterion2));
			lstJobForTeacher = criteria.list();		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   
   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndDistrictHBD(TeacherDetail teacherDetail,JobOrder jobOrder,String jobForTeacherId)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			Criterion criterion3=Restrictions.not(criterion2);
			criteria.add(criterion1);
			criteria.add(criterion3);
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(jobOrder.getDistrictMaster()!=null){
				jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("districtMaster"));
			}
			/*if(jobOrder.getBranchMaster()!=null){
				jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("branchMaster"));
			}*/
			if(jobOrder.getHeadQuarterMaster()!=null){
				jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			}
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherAndJobTitleHBD(TeacherDetail teacherDetail,JobOrder jobOrder,String jobForTeacherId,String jobTitle)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(jobOrder.getDistrictMaster()!=null){
				jobOrderSet.add(Restrictions.eq("districtMaster",jobOrder.getDistrictMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("districtMaster"));
			}
			if(jobOrder.getBranchMaster()!=null){
				jobOrderSet.add(Restrictions.eq("branchMaster",jobOrder.getBranchMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("branchMaster"));
			}
			if(jobOrder.getHeadQuarterMaster()!=null){
				jobOrderSet.add(Restrictions.eq("headQuarterMaster",jobOrder.getHeadQuarterMaster()));
			}else{
				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			}
			jobOrderSet.add(Restrictions.eq("jobCategoryMaster",jobOrder.getJobCategoryMaster())).add(Restrictions.like("jobTitle","%"+jobTitle+"%"));
			criteria.add(Restrictions.eq("teacherId",teacherDetail));
			Criterion criterion2 = Restrictions.eq("jobForTeacherId",Long.parseLong(jobForTeacherId));
			criteria.add(Restrictions.not(criterion2));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}

   
   @Transactional(readOnly=false)
   public   List <JobForTeacher>  getSPStatusByTeacher(Integer teacherId)
   {
      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
		try{
			
		      Session session = getSession();
		      
		      Criteria criteria = session.createCriteria(getPersistentClass());
		      Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
		      criteria.add(criterion);
		      Criteria c1 = criteria.createCriteria("jobId").add(Restrictions.eq("status","A"))
		      .createCriteria("jobCategoryMaster").add(Restrictions.eq("preHireSmartPractices", true))
		      //.createCriteria("districtMaster").add(Restrictions.eq("districtId", 5509600))
		      .setFirstResult(0)
		      .setMaxResults(1);
			
			lstJobForTeacher2 = criteria.list();
			}catch(Exception e){
			
				e.printStackTrace();
		}
		
	  return lstJobForTeacher2;
   }
   
   /*@Transactional(readOnly=false)
   public   List <JobForTeacher>  findAllAppliedJobsOfTeacherKelly(Integer teacherId)
   {
      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
		try{
			 Session session=getSession();
			 Criteria criteria=session.createCriteria(getPersistentClass());
			 lstJobForTeacher2= criteria.add(Restrictions.eq("teacherId.teacherId", teacherId))
			 .createCriteria("jobId").add(Restrictions.eq("status","A"))
			 .createCriteria("districtMaster").add(Restrictions.eq("districtId", 5509600))
			 .list();
			 
		}catch(Exception e){
			
				e.printStackTrace();
		}
		
	  return lstJobForTeacher2;
   }*/

   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherForKES(TeacherDetail teacherDetail,boolean flag)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			
			jobOrderSet.add(Restrictions.isNotNull("headQuarterMaster"));
			criteria.add(criterion1);
			if(flag)
				criteria.add(Restrictions.eq("statusMaster",statusMaster));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
   
   
   @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherNotKES(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
			criteria.add(criterion1);
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
   	public  Map<Integer,Boolean> getJobForTeacherByTeachersList(List<TeacherDetail> lstTeacherDetails){
	   	List<String[]> lstJobForTeacher=new ArrayList<String[]>();
	    Map<Integer,Boolean> mapTeacherPreHire=new LinkedHashMap<Integer,Boolean>();
	   	if(lstTeacherDetails.size()>0){
		   	 for(TeacherDetail td:lstTeacherDetails){
		   		 mapTeacherPreHire.put(td.getTeacherId(), false);
		   	 }
	         try{
	        	 JobForTeacher jjj=null;
	               Session session = getSession();
	               Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo").createAlias("jo.jobCategoryMaster", "jc");             
	               criteria.setProjection( Projections.projectionList()
	                       .add( Projections.property("teacherId.teacherId"), "teachId" )
	                       .add( Projections.groupProperty("teacherId") )
	                );
	                criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
	                criteria.add(Restrictions.eq("jc.preHireSmartPractices",true));
	                lstJobForTeacher = criteria.list() ;
	               if(lstJobForTeacher.size()>0){
                      for (Iterator it = lstJobForTeacher.iterator(); it.hasNext();) {
                          Object[] row = (Object[]) it.next();                                                                 
                          mapTeacherPreHire.put(Integer.parseInt(row[0].toString()),true);
                      }   
	               }
	         }catch (Throwable e) {
	               e.printStackTrace();
	         }   
   	 }
   	 return mapTeacherPreHire;
   }
    
    @Transactional(readOnly=false)
	public List<JobForTeacher> getHQBRQQFinalizedNotesForTeacher(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster ,BranchMaster branchMaster)
	{
		List<JobForTeacher> lstjob=new ArrayList<JobForTeacher>();
		
		if(teacherDetail!=null)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(teacherDetail!=null)
					criteria.add(Restrictions.eq("teacherId",teacherDetail));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    if(headQuarterMaster!=null)
				    	criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
				    else if(branchMaster!=null)
				    	criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
				    //criteria.setProjection(Projections.distinct(Projections.property("teacherId")));
				    lstjob =  criteria.list();
				    } 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstjob;
	}
    @Transactional(readOnly=false)
    public List<JobForTeacher> getJobForTeacherByTeachersListNew(List<Integer> statusIds,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
    {
    	 List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
         try{
               Session session = getSession();
               Criteria criteria = session.createCriteria(getPersistentClass());             
              
               Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
               if(entityID!=1){
            	   if(districtMaster!=null){
	                   criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster)).add(Restrictions.eq("districtMaster", districtMaster));
	        	   }else if(branchMaster!=null){
	                     criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
	                 }else  if(headQuarterMaster!=null){
                     criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
                 }
               }
               if(lstTeacherDetails.size()>0){
                   criteria.add(criterionTeacher);
                   criteria.createCriteria("status").add(Restrictions.in("statusId",statusIds));
                   lstJobForTeacher = criteria.list() ;
              }
              
         }catch (Exception e) {
               e.printStackTrace();
         }           
         return lstJobForTeacher;
    }
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List countApplidJobsHQLByHBD(HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,SchoolMaster schoolMaster,List<TeacherDetail> teacherDetails) 
	{
		Session session = getSession();
		String sql = "";
		if(branchMaster!=null){
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT(IF(jo.branchId="+branchMaster.getBranchId()+" and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}
		if(headQuarterMaster!=null){
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT(IF(jo.headQuarterId="+headQuarterMaster.getHeadQuarterId()+" and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}
		if(districtMaster!=null && schoolMaster==null)
		{
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT(IF(jo.districtId="+districtMaster.getDistrictId()+" and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}if(schoolMaster!=null)
		{
			sql = "select jft.teacherId, jft.`createdDateTime`,COUNT( IF( jft.status IN (:statuss), 1, NULL )),COUNT( IF( jo.jobId IN ( SELECT jobId FROM schoolinjoborder WHERE schoolId ="+schoolMaster.getSchoolId()+" ) and jft.status IN (:statuss),1,NULL)) from jobforteacher jft join joborder jo on jo.jobId=jft.jobId " +
			" where teacherId IN (:teacherDetails) group by teacherId";
		}


		Query query = session.createSQLQuery(sql);
		String[] statuss = {"icomp","comp","hird","vlt","scomp","ecomp","vcomp","dcln","rem"};
		//List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
		query.setParameterList("teacherDetails", teacherDetails);
		query.setParameterList("statuss", statusMasters );

		List<Object[]> rows = query.list();
		return rows;

	}
    
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public List getAppliedJobListByTeacher(int teacherId,int districtId,StatusMaster statusMaster) 
	{
		Session session = getSession();
		String sDistrictQuery="";
		if(districtId>0)
		{
			sDistrictQuery=" and districtId=:districtId ";
		}
		if(statusMaster!=null)
		{		// Handle by Nadeem	
			if(statusMaster.getStatusId()==4)
				sDistrictQuery=sDistrictQuery+" and status IN(:status)";
			else
				sDistrictQuery=sDistrictQuery+" and status =:status";	
		}
		
		Query query = session.createSQLQuery("select districtId,jobId,status,jobForTeacherId from jobforteacher where teacherId=:teacherId"+sDistrictQuery);
		query.setParameter("teacherId", teacherId);
		if(districtId>0)
		{
			query.setParameter("districtId", districtId);
		}
		// handle by Nadeem
		if(statusMaster!=null)
		{
			  if(statusMaster.getStatusId()==4){
			    ArrayList<Integer> statusList = new ArrayList<Integer>();
			    statusList.add(4);
			    statusList.add(6);
			    statusList.add(7);
			    statusList.add(16);
			    statusList.add(17);
			    statusList.add(18);
			    statusList.add(19);
				query.setParameterList("status", statusList);
			  }
			  else{ 
				  query.setParameter("status", statusMaster.getStatusId());
			  }
		}		
		List<Object[]> rows = query.list();
		return rows;
		
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> findByTeacherAndQuestionSet(TeacherDetail teacherDetail,QqQuestionSets questionSets) 
	{	
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			criteria.createCriteria("jobId").createCriteria("jobCategoryMaster").add(Restrictions.eq("questionSets",questionSets));
			criteria.addOrder(Order.desc("createdDateTime"));
			lstJobForTeachers = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}
    
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllHiredTeacher(DistrictMaster districtMaster) 
	{	
		//List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();
		List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			StatusMaster statusMasterHired = WorkThreadServlet.statusMap.get("hird");
			StatusMaster statusMasterTimed = WorkThreadServlet.statusMap.get("vlt");
			System.out.println(statusMasterHired.getStatusId()+":::::::::::::"+statusMasterTimed.getStatusId()+":::::::::::"+districtMaster.getDistrictId());
			Criterion criterion1 = Restrictions.eq("status",statusMasterHired);
			Criterion criterion2 = Restrictions.eq("status",statusMasterTimed);
			Criterion criterion3 = Restrictions.eq("districtId",districtMaster.getDistrictId());	
			Criterion criterionMix=Restrictions.or(criterion1, criterion2);
			
			criteria.add(criterionMix);
			criteria.add(criterion3);
			criteria.setProjection(Projections.groupProperty("teacherId"));
			lstTeacherDetails = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstTeacherDetails;
	}
    
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllUnhiredTeacher(DistrictMaster districtMaster,List<TeacherDetail> teacherDetail) 
	{	
		//List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();
		List<TeacherDetail> lstTeacherDetails = new ArrayList<TeacherDetail>();
		List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			String[] status = {"hird","vlt","hide"};
			//lstStatusMasters = Utility.getStaticMasters(status);
			lstStatusMasters = Utility.getStaticMasters(status);
			
			Criterion criterion1 = Restrictions.not(Restrictions.in("status", lstStatusMasters));
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.not(Restrictions.in("teacherId", teacherDetail));
			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			criteria.setProjection(Projections.groupProperty("teacherId"));
			lstTeacherDetails = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstTeacherDetails;
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> getJobForTeacherByDSTeachersList(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,int entityID)
	{
		System.out.println("::::::::::::::::::::::getJobForTeacherByDSTeachersList:::::::::::::::::::::::::::::::");
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		if(lstTeacherDetails.size()>0){
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
				
				if(districtMaster!=null && entityID!=1){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}
				
				criteria.add(criterionTeacher);
				criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
    public boolean isJobApply(TeacherDetail teacherDetail)
    {
    	boolean isJobApply = false;
    	String sql = "";
    	List<Object[]> rows = null;
 		try 
		{
 			Session session = getSession();
 			sql = "select jobId from jobforteacher jft where teacherId="+teacherDetail.getTeacherId();
			Query query = session.createSQLQuery(sql);
			rows = query.list();
			if(rows!=null && rows.size()>0)
				isJobApply = true;
		} 
		catch (Exception e) {
			e.printStackTrace();
			return isJobApply;
		}	
		System.out.println("isJobApply::: "+ isJobApply);
		return isJobApply;
    }
    @Transactional(readOnly=false)
	public List<TeacherDetail> getAllQQFinalizedTeacherListOfHQAndBranch(List<TeacherDetail> lstTeacherDetails,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster)
	{
    	PrintOnConsole.getJFTPrint("JFT:94");	
		List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0 && (headQuarterMaster!=null || branchMaster!=null))
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
				    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
				    criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
				    if(headQuarterMaster!=null && branchMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				    else if(headQuarterMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
				    else if(branchMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
				    
				    criteria.setProjection(Projections.groupProperty("teacherId"));
				    lstTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstTeacher;
	}
    @Transactional(readOnly=false)	
	public List<JobForTeacher> findByTeacherIdHQAndBranch(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster) 
	{	
		PrintOnConsole.getJFTPrint("JFT:36");
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			if(headQuarterMaster!=null && branchMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
			else if(headQuarterMaster!=null)
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
			else if(branchMaster!=null)
			    criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));

			criteria.addOrder(Order.desc("createdDateTime"));
			lstJobForTeachers = criteria.list();

		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}  
    @Transactional(readOnly=false)
	public JobForTeacher getLatestCL(TeacherDetail teacherDetail,JobOrder jobOrder)
	{
		JobForTeacher jobForTeacher	=	 null;	
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			Criterion criterion2 = Restrictions.isNotNull("coverLetter");
			Criterion criterion3 = Restrictions.ne("coverLetter","");
			Criterion criterion4 = Restrictions.and(criterion2,criterion3);
			Criterion criterion5 = Restrictions.ne("jobId",jobOrder);
			lstJobForTeacher = findWithLimit(Order.desc("createdDateTime"), 0, 1, criterion1,criterion4,criterion5);
			
			System.out.println("lstJobForTeacher.size() "+lstJobForTeacher.size());
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		if(lstJobForTeacher==null || lstJobForTeacher.size()==0)
			return null;
		else
		{
			for(JobForTeacher jft: lstJobForTeacher)
			{
				if(jft.getCoverLetter().replaceAll("\\<[^>]*>","").length()>0)
				{
					jobForTeacher	=	jft;
					break;
				}
			}
			return jobForTeacher;
		}
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByJobCategory(List<JobOrder> jobOrderList)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria  criteria = session.createCriteria(getPersistentClass()) ;
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",jobOrderList);
			criteria.add(criterion1);
			criteria.add(criterion2);
			/*criteria.createCriteria("jobId").add(Restrictions.eq("jobId",jobOrder));
			criteria.setProjection(Projections.distinct(Projections.property("jobId")));*/
			lstJobForTeacher = criteria.list();

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
	public List<JobForTeacher> findJobListForJobSpecificInventory(TeacherDetail teacherDetail,Boolean assesmentStatus)
	{
		PrintOnConsole.getJFTPrint("JFT:last");
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
			Date dateWithoutTime = Utility.getDateWithoutTime();
			
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			criteria.add(criterion1);
			criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
			.add(Restrictions.le("jobStartDate",dateWithoutTime))
			.add(Restrictions.ge("jobEndDate",dateWithoutTime))
			.add(Restrictions.eq("status", "A"));
			lstJobForTeacher = criteria.list();
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
    
    @Transactional(readOnly=false)
	public Map<String,String> findJFTByTeaJobForONB(List<Integer> teacherIds,List<Integer> jobIds)
	{
    	System.out.println("yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyfindTADFDQByTeaJobForONB");
		Map<String,String> thumbsMap = new LinkedHashMap<String, String>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(JobForTeacher.class);
			criteria.createAlias("jobId", "jo").createAlias("teacherId", "td")
		    .setProjection( Projections.projectionList()
		    	.add( Projections.property("jo.jobId"), "jobId" )
				.add( Projections.property("td.teacherId"), "teacherId" )
		        .add( Projections.property("isDistrictSpecificNoteFinalizeONB"), "isDistrictSpecificNoteFinalizeONB" )
		        .add( Projections.property("flagForDistrictSpecificQuestionsONB"), "flagForDistrictSpecificQuestionsONB" )
		    );
			criteria.add(Restrictions.in("jo.jobId", jobIds));
			criteria.add(Restrictions.in("td.teacherId", teacherIds));
			List<String[]>  listTeacherAndJobId= criteria.list();
			for(Object[] str:listTeacherAndJobId){
				thumbsMap.put(str[0].toString()+"##"+str[1].toString(), ((str[2]!=null?str[2].toString():"0"))+"##"+(str[3]!=null?str[3].toString():"0"));
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return thumbsMap;
	}
    
    @Transactional(readOnly=false)
	public List<JobForTeacher> findBySmartPracticesAssessment(TeacherDetail teacherDetail) 
	{	
		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

		try
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
			criteria.add(criterion1);
			criteria.createCriteria("jobId").createCriteria("jobCategoryMaster").add(Restrictions.eq("preHireSmartPractices",true));
			lstJobForTeachers = criteria.list();
		}catch(Exception e ){
			e.printStackTrace();
		}

		return lstJobForTeachers;
	}
    
    @Transactional(readOnly=false)
	public List<JobForTeacher> getAllUnhiredJob(List<TeacherDetail> lstTeacherDetails,StatusMaster statusMaster)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		
		if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
		{
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
					criteria.add(Restrictions.in("teacherId",lstTeacherDetails));

				if(statusMaster!=null)
					criteria.add(Restrictions.not(Restrictions.ne("status",statusMaster)));
					
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lstJobForTeacher;
	}
    
    @Transactional(readOnly=false)
	public List<JobForTeacher> findJobByJobCategoryAndDistrict(List<JobOrder> jobList)
	{
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{			
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			Criterion criterion1 = Restrictions.eq("status",statusMaster);
			Criterion criterion2 = Restrictions.in("jobId",jobList);
							
			lstJobForTeacher = findByCriteria(criterion1,criterion2);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
    public List getOfferMade(int districtId,List jobforteacherList){
    	//System.out.println(":::::::::::::::getOfferMade()get districtId====="+districtId);
    	int count=0;
    	PrintOnConsole.getJFTPrint("JFT:101");
		List<String[]> lst=new ArrayList<String[]>(); 
		Session session = getSession();
		String sql = "";
		Connection connection =null;
	    
    	try{
    		Iterator itr = jobforteacherList.iterator();
    		 StringBuilder commaSepValueBuilder = new StringBuilder();
    		List<String> jobIdList = new ArrayList<String>();
    		int counter =0;
    		if(jobforteacherList!=null && jobforteacherList.size()>0){
				while(itr.hasNext()){
	        		Object[] obj = (Object[]) itr.next();
					JobOrder jobOrder=(JobOrder)obj[0];
					if(jobOrder!=null && !jobIdList.contains(jobOrder.getJobId().toString())){
					  jobIdList.add(jobOrder.getJobId().toString());
					  commaSepValueBuilder.append(jobOrder.getJobId().toString());
					  commaSepValueBuilder.append(",");
					} 
					  counter++;
		         }
				
    		}
        	List list=null;
        	String strJobId = commaSepValueBuilder.toString();
        	String jobIds   =  strJobId.substring(0, strJobId.length()-1);
        	System.out.println("::::::::::::::::::::jobIds====="+jobIds);
        	
    		 SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
 		     ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
 		     connection = connectionProvider.getConnection();
		sql = "SELECT jobId FROM teacherstatushistoryforjob " +
				"WHERE jobId in  ("+jobIds+
				") and statusId = 18 and jobId not in " +
				"(SELECT jobId FROM jobforteacher WHERE jobId in " +
				"("+jobIds+") " +
				"and status = 6 group by jobId) " +
				"group by jobId";
		 PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);
		  ResultSet rs=ps.executeQuery();
		 
				if(rs.next()){				
					do{
						final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
						for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
						   {
							allInfo[i-1]=rs.getString(i);
						   }
						lst.add(allInfo);
						
					}while(rs.next());
				}
				else{
					System.out.println("Record not found.");
				}
				return lst;
			} catch (Exception e) {
				e.printStackTrace();
			}finally{
				if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return null;
    }
    public class AutoInactiveJobOrderThread implements Runnable{
    	private JobOrder jobOrderIn=null;
    	private JobOrderDAO jobOrderDAO=null;
    	private EmailerService emailerService=null;
    	private String[] allEmailIds=null;
		public AutoInactiveJobOrderThread(JobOrder jobOrder,JobOrderDAO jobOrderDAO, EmailerService emailerService, List<String> allEmailIds){
			this.jobOrderIn=jobOrder;
			this.jobOrderDAO=jobOrderDAO;
			this.emailerService=emailerService;
			this.allEmailIds=allEmailIds.toArray(new String[allEmailIds.size()]);
		}
		public void run() {
			try {
				GlobalServices.println("yyyyyyyyyyyyyyyyyyyyyyyyyy--------------------------------------------------1");
				Session session=jobOrderDAO.getSessionFactory().openSession();
				Thread.sleep(5000);
				Query exQuery=session.createSQLQuery("CALL JOB_DEACTIVE_AUTO_IF_HIREEXPECTED_FULL(:jobId)").setParameter("jobId", jobOrderIn.getJobId());
				int exRows = exQuery.executeUpdate();
				System.out.println("exRows======="+exRows);
				Thread.sleep(500);
				JobOrder jobOrder=(JobOrder)session.load(JobOrder.class, jobOrderIn.getJobId());
				ElasticSearchService es=new ElasticSearchService();
				es.updateESSchoolJobOrderByJobId(jobOrder);
				if(jobOrder.getStatus().trim().equalsIgnoreCase("I")){
				es.searchDataAndDelete(jobOrder.getJobId(),ElasticSearchConfig.indexForhqDistrictJobBoard);
				String notificationTemplate=AcceptOrDeclineMailHtml.getAutoInactiveNotificationMail(jobOrder);
				System.out.println("\nTemp::"+notificationTemplate);
				emailerService.sendMailAsHTMLTextCC(allEmailIds,"Notification of inactive JOB :"+jobOrderIn.getJobId(),notificationTemplate,new String[]{});
				}else{
					SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
					if(jobOrder.getStatus().equalsIgnoreCase("A") && (jobOrder.getIsInviteOnly()==null ||  !jobOrder.getIsInviteOnly()) && jobOrder.getApprovalBeforeGoLive()!=null && jobOrder.getApprovalBeforeGoLive()==1 
							&& (jobOrder.getJobStartDate().before(new Date()) ||  jobOrder.getJobStartDate().equals(new Date())) 
							&& (jobOrder.getJobEndDate().after(sdf.parse(sdf.format(new Date()))) ||  jobOrder.getJobEndDate().equals(sdf.parse(sdf.format(new Date())))) 
							&& (jobOrder.getHiddenJob()==null ||  !jobOrder.getHiddenJob())	)
					{
						es.updateESDistrictJobBoardByJobId(jobOrder);
					}
				}
				GlobalServices.println("yyyyyyyyyyyyyyyyyyyyyyyyyy--------------------------------------------------2");
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
    }
    private List<String> getAllSAandDAsEmailIds(JobOrder jobOrder){
		List<String> allUserEmail=new ArrayList<String>();
		List<String> emailListDAs=new LinkedList<String>();
		List<String> emailListSAs=new LinkedList<String>();
		for(UserMaster um:userMasterDAO.getUserByDistrict(jobOrder.getDistrictMaster())){
			if(um.getEmailAddress()!=null && um.getEmailAddress().trim().equalsIgnoreCase(""))
			emailListDAs.add(um.getEmailAddress());
		}
		for(UserMaster um:userMasterDAO.getUserIDSBySchools(schoolInJobOrderDAO.findSchoolIds(jobOrder))){
			if(um.getEmailAddress()!=null && um.getEmailAddress().trim().equalsIgnoreCase(""))
				emailListSAs.add(um.getEmailAddress());
		}
		System.out.println("********************SAs********************"+emailListSAs);
		System.out.println("********************DAs********************"+emailListDAs);
		allUserEmail.addAll(emailListDAs);
		allUserEmail.addAll(emailListSAs);
		return allUserEmail;
	}
	private List<String> getGroupWiseAllUser(JobOrder jobOrder,SchoolMaster schoolMaster){
		List<String> allUserEmail=new ArrayList<String>();
		List<String> emailListDAs=new LinkedList<String>();
		List<String> emailListSAs=new LinkedList<String>();
		List<String> emailListEST=new LinkedList<String>();
		Map<String,String> groupNameToShortNameList=new LinkedHashMap<String,String>();
		if(jobOrder.getEmploymentServicesTechnician()!=null){
			emailListEST.add(jobOrder.getEmploymentServicesTechnician().getPrimaryEmailAddress());
			emailListEST.add(jobOrder.getEmploymentServicesTechnician().getBackupEmailAddress());
		}
		if(schoolMaster!=null){
			List<UserMaster> umListSAs=userMasterDAO.getAllUserByDistrictAndSchool(jobOrder.getDistrictMaster(), schoolMaster);
			for(UserMaster um:umListSAs)
				if(!emailListSAs.contains(um.getEmailAddress())){
					emailListSAs.add(um.getEmailAddress());
				}
		}
		
		if(jobOrder.getSpecialEdFlag()!=null){
			List<DistrictSpecificApprovalFlow> listDSAF=districtSpecificApprovalFlowDAO.findByCriteria(Restrictions.eq("specialEdFlag", jobOrder.getSpecialEdFlag()),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.eq("status", "A"));
			if(listDSAF!=null && listDSAF.size()>0){
				List<String> groupShortName=new ArrayList<String>();
				for(String group:listDSAF.get(0).getApprovalFlow().split("#")){
					if(!group.trim().equals("")){
						groupShortName.add(group.trim());
					}
				}
				if(groupShortName.size()>0){
				List<DistrictWiseApprovalGroup> listDWAG=districtWiseApprovalGroupDAO.findByCriteria(Restrictions.in("groupShortName", groupShortName),Restrictions.eq("status", "A"));
					if(listDWAG!=null && listDWAG.size()>0){
						List<String> groupName=new ArrayList<String>();
						for(DistrictWiseApprovalGroup dwag:listDWAG){
							groupName.add(dwag.getGroupName());
							groupNameToShortNameList.put(dwag.getGroupName(), dwag.getGroupShortName());
						}
						
						
						for(Map.Entry<String,String> entry:groupNameToShortNameList.entrySet())
							System.out.println(entry.getKey()+"      ============    "+entry.getValue());
						
						
						if(groupName.size()>0){
						List<DistrictApprovalGroups> dAGList=districtApprovalGroupsDAO.findByCriteria(Restrictions.in("groupName",groupName),Restrictions.isNull("jobCategoryId"),Restrictions.isNull("jobId"),Restrictions.eq("districtId", jobOrder.getDistrictMaster()));
						//System.out.println("dAGList=============="+dAGList.size());
							if(dAGList.size()>0){
								List<Integer> keyContactId=new ArrayList<Integer>();
								Map<String,DistrictKeyContact> memberToKeyContactMap=new LinkedHashMap<String,DistrictKeyContact>();
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
									for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
										if(!memberKey.trim().equals(""))
											try{
											keyContactId.add(Integer.parseInt(memberKey));
											//System.out.println("keyId============="+memberKey);
											}catch(NumberFormatException e){}
									}
								}
								if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										memberToKeyContactMap.put(dkc.getKeyContactId().toString(), dkc);
								}
								
								for(DistrictApprovalGroups districtAppGroup:dAGList){
									if(districtAppGroup.getGroupMembers()!=null && !districtAppGroup.getGroupMembers().trim().equals(""))
									for(String memberKey:districtAppGroup.getGroupMembers().split("#")){
										if(!memberKey.trim().equals(""))
											try{
												if(groupNameToShortNameList.get(districtAppGroup.getGroupName()).trim().equalsIgnoreCase("SA")){
												emailListSAs.add(memberToKeyContactMap.get(memberKey).getUserMaster().getEmailAddress());
												}else{
												emailListDAs.add(memberToKeyContactMap.get(memberKey).getUserMaster().getEmailAddress());
												}
											}catch(NumberFormatException e){}
									}
								}
								/*if(keyContactId.size()>0){
									List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findByCriteria(Restrictions.in("keyContactId", keyContactId));
									for(DistrictKeyContact dkc:districtKeyContactList)
										if(!allUserEmail.contains(dkc.getUserMaster().getEmailAddress()))
											allUserEmail.add(dkc.getUserMaster().getEmailAddress());
								}*/
							}
						}
					}
				}
			}
		}
		System.out.println("********************ES*********************"+emailListEST);
		System.out.println("********************SAs********************"+emailListSAs);
		System.out.println("********************DAs********************"+emailListDAs);
		allUserEmail.addAll(emailListDAs);
		allUserEmail.addAll(emailListSAs);
		allUserEmail.addAll(emailListEST);
		println("umList==================================================="+allUserEmail);
		return allUserEmail;
	}
   
    @Transactional(readOnly=false)
	public List<JobForTeacher> findJFTforOfferReadystatusNew(DistrictMaster districtMaster ,Order sortOrderStrVal,List<JobOrder> jobOrders, boolean sortingcheck,Integer status,String startDate,String enddate)
	{

		System.out.println("::::::::::::: STATUS 11111111111::::::::::::::"+status);
		Date startD = null;
        Date endD   = null;
        
        try{
        	if(!startDate.equals(""))
            	startD = Utility.getCurrentDateFormart(startDate);
        } catch(Exception exception){
        	exception.printStackTrace();
        }
        
        try{
        	if(!enddate.equals(""))
        		endD = Utility.getCurrentDateFormart(enddate);
        } catch(Exception exception){
        	exception.printStackTrace();
        }
        
		System.out.println("start date============+"+startD+"End date============="+endD);
        
		List<JobForTeacher> lstJobForTeacher=new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			if(districtMaster!=null){
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}
			//criteria.createCriteria("jobId").add(Restrictions.eq("status", "A"));
			//criteria.addOrder(Order.asc("teacherId"));
			
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			Criterion criterion1 = Restrictions.ne("status",statusMaster);
			criteria.add(criterion1);
			
			Criterion isnotNullCriterion    =Restrictions.isNotNull("offerReady");
			criteria.add(isnotNullCriterion);
			
			if(!status.equals(0)){
				if(status.equals(1)){
					Criterion offerReadyCompletedPending = Restrictions.eq("offerReady", true);
					criteria.add(offerReadyCompletedPending);
				} else if(status.equals(2)){
					Criterion offerReadyCompletedPending = Restrictions.eq("offerReady", false);
					criteria.add(offerReadyCompletedPending);
				}
			}
			
			if(startD!=null && endD!=null){
				criteria.add(Restrictions.ge("offerMadeDate",startD)).add(Restrictions.le("offerMadeDate",endD));
			}
			
			if(!sortingcheck){
			 criteria.addOrder(sortOrderStrVal);
			}
			
			if(jobOrders!=null && jobOrders.size()>0)
				criteria.add(Restrictions.in("jobId",jobOrders));
			
			lstJobForTeacher = criteria.list();
			System.out.println("offerReadyofferReady :: "+lstJobForTeacher.size());
			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		System.out.println("::::::: lstJobForTeacher :::::::::"+lstJobForTeacher.size());
		return lstJobForTeacher;
	
	}
    
    @Transactional(readOnly=false)
    public List<JobForTeacher> findAllTeacherByDistrictId(TeacherDetail teacherDetail,DistrictMaster districtMaster)
    {
    	PrintOnConsole.getJFTPrint("JFT:93");	
    	List<JobForTeacher> lstJobForTeacher= null;
    	try 
    	{
    		
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			criteria.add(Restrictions.eq("teacherId",teacherDetail));
    			if(districtMaster!=null)
    			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			lstJobForTeacher = criteria.list();
    	} 
    	catch (Exception e) {
    		e.printStackTrace();
    	}		
    	return lstJobForTeacher;
    }
    @Transactional(readOnly=false)
	public List<JobForTeacher> firstJobDateAppliedbyKellyApplicant(List<TeacherDetail> teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		HeadQuarterMaster headQuarterMaster = headQuarterMasterDAO.findById(1, false, false);
		try 
		{
			if(teacherDetail!=null && teacherDetail.size()>0){
				/*Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass())
				.createAlias("teacherId", "td");
				DetachedCriteria subQuery = DetachedCriteria.forClass(JobForTeacher.class, "tns")
				.setProjection(Projections.projectionList()
			    		.add(Projections.min("tns.createdDateTime")))
			    		.add(Restrictions.eqProperty("tns.teacherId.teacherId", "td.teacherId"));
				criteria.add(Property.forName("createdDateTime").eq(subQuery));
				criteria.add(Restrictions.in("teacherId", teacherDetail));
				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
				lstJobForTeacher = criteria.list() ;*/	
				
				Criterion criterion1 = Restrictions.in("teacherId", teacherDetail);
				Criterion criterion2 = Restrictions.eq("headQuarterMaster", headQuarterMaster);
				Session session = getSession();
				Criteria criteria=	session.createCriteria(getPersistentClass()) 
				.setProjection(Projections.projectionList()
						.add(Projections.groupProperty("teacherId"))
						.add(Projections.min("createdDateTime"))
				);
				criteria.add(criterion1);
					lstJobForTeacher=criteria.createCriteria("jobId").add(criterion2).list();
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int totalAppliedApplicantsByJobOrders(List<JobOrder> jobOrders ,String hird) 
	{
		Session session = getSession();
		String sql =" SELECT COUNT(*) AS appliedCnt " +
		                  " from jobforteacher jft where jft.jobId IN (:jobOrders)";
		
		if(hird!=null && hird.equalsIgnoreCase("hird"))
			sql+=	" and  jft.status=6";
		
		/*String[] statuss = {"dcln","icomp","vlt","widrw","hide","rem"};
		List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);*/
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		
		List rows = query.list();
		if(rows!=null){		
			return Integer.parseInt(rows.get(0)+"");
		}else{
			return 0;
		}
		
	}
    
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public int totalUniqueAppliedApplicantsByJobOrders(List<JobOrder> jobOrders) 
	{
		Session session = getSession();
		String sql =" SELECT COUNT(*) AS appliedCnt " +
		                  " from jobforteacher jft where jft.jobId IN (:jobOrders) group by jft.teacherId";
		
		/*String[] statuss = {"dcln","icomp","vlt","widrw","hide","rem"};
		List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);*/
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		
		List rows = query.list();
		if(rows!=null){		
			return rows.size();
		}else{
			return 0;
		}
		
	}
    
    @Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,Integer> countApplicantsByJobOrdersMukesh(List<JobOrder> jobOrders) 
	{
		Session session = getSession();
		String sql =" SELECT jft.districtId, COUNT( IF( jft.status =6, 1, NULL ) ) AS hiredCnt " +
		" from jobforteacher jft where jft.jobId IN (:jobOrders)  group by jft.districtId ";
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		
		List<Object[]> rows = query.list();
		Map<Integer,Integer> map = new HashMap<Integer, Integer>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				Object obj[] = (Object[])oo;
				map.put((Integer)obj[0], Integer.parseInt(obj[1].toString()));
			}
		}
		return map;
	}
    
	@Transactional(readOnly=false)
	@SuppressWarnings("unchecked")
	public Map<Integer,String> countApplicantsByJobOrders(List<JobOrder> jobOrders) 
	{
		Session session = getSession();
		String sql =" SELECT jft.jobId, COUNT(*) AS appliedCnt,jft.districtId "+
			" from jobforteacher jft where jft.jobId IN (:jobOrders) and jft.status!=8 group by jft.jobId  having appliedCnt>=5";
		
		Query query = session.createSQLQuery(sql);
		query.setParameterList("jobOrders", jobOrders);
		
		
		List<Object[]> rows = query.list();
		Map<Integer,String> map = new HashMap<Integer, String>();
		if(rows.size()>0)
		{
			for(Object oo: rows){
				
				Object obj[] = (Object[])oo;
				
				map.put((Integer)obj[0], obj[1]+"##"+obj[2]);
			}
		}
		return map;
	}
	@Transactional(readOnly=false)
	public List<TeacherDetail> findAllJFTForKellyDashboard(List<TeacherDetail> teacherDetail,UserMaster userMaster,boolean globalFlag)
	{
		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
		StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			
  		    if((teacherDetail!=null && teacherDetail.size()>0) || globalFlag)
  		    {
			  criteria.add(Restrictions.in("teacherId",teacherDetail));			
			}
  		    if(userMaster.getHeadQuarterMaster()!=null)
  		    	criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster",userMaster.getHeadQuarterMaster()));
  		    else if(userMaster.getBranchMaster()!=null)
  		    	criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster",userMaster.getBranchMaster()));
			
			criteria.add(Restrictions.ne("status",statusMaster));								
			criteria.setProjection(Projections.groupProperty("teacherId"));
			lstJobForTeacher = criteria.list();
			System.out.println(" Size onbarding :"+lstJobForTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		System.out.println("------------------------------------------------------");
		return lstJobForTeacher;
	}
	
	// for Optimization by Anurag   replacement of Method  =>  getJobForTeacherByDSTeachersList 
    @Transactional(readOnly=false)
  public List<String[]> getJobForTeacherByJobAndTeachersListWithHQandBR(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
  {
    System.out.println("getJobForTeacherByJobAndTeachersListWithHQandBR  method::::::::::::::::");
    PrintOnConsole.getJFTPrint("JFT:92");     
     List<String[]> lstJobForTeacher=new ArrayList<String[]>();
     if(lstTeacherDetails.size()>0){
             try{
                   Session session = getSession();
                  
                   Criteria criteria = session.createCriteria(getPersistentClass());             
                   criteria.setProjection( Projections.projectionList()
                           .add( Projections.property("teacherId.teacherId"), "teachId" )
                           .add( Projections.count("teacherId") )              
                           .add( Projections.groupProperty("teacherId") )
                       );
                   Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
                   
                   if(entityID!=1){                                   
                                  if(districtMaster!=null){
                                        criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
                                  }else{
                                        //jobOrderSet.add(Restrictions.isNull("districtMaster"));
                                  }
                                  if(branchMaster!=null){
                                        criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster",branchMaster));
                                  }else{
                                        //jobOrderSet.add(Restrictions.isNull("branchMaster"));
                                  }
                                  if(headQuarterMaster!=null){
                                        if(headQuarterMaster.getHeadQuarterId()==1){
                                              criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
                                        }
                                  }else{
                                        //jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
                                  }
                            }
                  
               if(lstTeacherDetails.size()>0){
                            criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
                            criteria.add(criterionTeacher);
                      }
                      
                      //  System.out.println(criteria);
                            //criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
                   
                   
                   lstJobForTeacher = criteria.list() ;
                               }catch (Throwable e) {
                   e.printStackTrace();
             }   
     }
       return lstJobForTeacher;
  }
  
	
// For Optimization 
    
    @Transactional(readOnly=false)
	public List<TeacherDetail> getTeacherDetailByDASARecordsDataOp(Order sortOrderStrVal,int start,int end,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> lstTeacherDetailAllFilter,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,boolean normScoreFlag,List<StatusMaster> lstStatusMastersNew)
	{
		PrintOnConsole.getJFTPrint("JFT:71");
		
		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
		try{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			System.out.println("inside 1");
			String[] candidatetyp=null;
			if(internalCandVal!=null){
					candidatetyp=internalCandVal.split("|");
			}
			
			List<Integer> candidatetypec=new ArrayList<Integer>();
			if(candidatetyp!=null){
				for(String s:candidatetyp){
					if(s.equalsIgnoreCase("0")){
						candidatetypec.add(1);
						candidatetypec.add(5);
					}else{
						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
							int val=Integer.parseInt(s);
							candidatetypec.add(val);
						}
						
					}
				}
			}
			System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
			if(candidatetypec.size()>0){
				criteria.add(Restrictions.in("isAffilated",candidatetypec));
			}
			
			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}*/
			System.out.println("inside 2");
			Criterion criterionSchool =null;
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
			}
			
			criteria.add(Restrictions.ne("status",statusMaster));
			if(lstStatusMastersNew.size()>0)
				criteria.add(Restrictions.in("status",lstStatusMastersNew));
			
			
			if(fDate!=null && tDate!=null)
				criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
			else if(fDate!=null && tDate==null)
				criteria.add(Restrictions.ge("createdDateTime",fDate));
			else if(tDate!=null && fDate==null)
				criteria.add(Restrictions.le("createdDateTime",tDate));
			

			boolean flag=false;
			boolean dateDistrictFlag=false;
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
			{
				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
				criteria.add(criterion1);
			}
			System.out.println("inside 3");
			if((entityID==3 || entityID==4)){
				flag=true;
				if(lstJobOrder.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
					criteria.add(criterion1);
				}
				if(entityID==4 && dateFlag==false){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				}else if(entityID==4 && dateFlag){
					dateDistrictFlag=true;
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}*/
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
				}
			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
				flag=true;
				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
			}else if(dateFlag && districtMaster!=null &&  entityID==2){
				flag=true;
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				dateDistrictFlag=true;
			}
			else if(entityID==5)
			{

				System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
				flag=true;
				if(branchMaster!=null)
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
				else
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));

			}
			else if(entityID==6)
			{
				System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
				flag=true;
				criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
			}
			if(dateDistrictFlag==false && dateFlag)
			{
				/*if(fDate!=null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
				}else if(fDate!=null && tDate==null){
					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
				}else if(fDate==null && tDate!=null){
					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
				}*/
				
				if(lstJobOP!=null && lstJobOP.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
					criteria.add(criterion1);
				}
				
				flag=true;
			}
			System.out.println("inside 6");
			if(status){
				criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
			}
			System.out.println("inside 7");
			//job applied flag
			if(newcandidatesonlyNew)
			{
				if(appliedfDate!=null && appliedtDate!=null)
				{
					System.out.println("############### newcandidatesonlyNew FT #######################");
					criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				
				else if(appliedfDate!=null && appliedtDate==null)
				{
					System.out.println("############### newcandidatesonlyNew F #######################");
					//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
				}
				else if(appliedtDate!=null && appliedfDate==null)
				{
					System.out.println("############### newcandidatesonlyNew T #######################");
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
			}
			else
			{System.out.println("inside 8");
				if(appliedfDate!=null && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				else if(appliedfDate!=null && appliedtDate==null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				else if(appliedtDate!=null && appliedfDate==null)
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				System.out.println("inside 9");
			}
			
			flag=true;
			if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
			{
				criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetailAllFilter))); // filter first time
			}
			System.out.println("inside 10");
			if(teacherFlag){
				flag=true;
				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
			}
			if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size() >0){
				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
			}
			System.out.println("inside 11");
			if(districtMaster!=null  && utype==3)
			{
				StatusMaster master = districtMaster.getStatusMaster();
				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
				if(master!=null)
				{
					System.out.println("master: "+master.getStatus());
					//String sts = master.getStatus();
					String sts = master.getStatusShortName();
					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("scomp"))
						{
							StatusMaster st1 = new StatusMaster();
							st1.setStatusId(17);
							sList.add(st1);
							
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
							
						}else if(sts.equals("vcomp"))
						{
							StatusMaster st2 = new StatusMaster();
							st2.setStatusId(18);
							sList.add(st2);
						}
						
						/*
						6 - Hired
						19 - Declined
						10 - rejected
						7 -- Withdrew
						*/
						
						StatusMaster stHird = new StatusMaster();
						stHird.setStatusId(6);
						sList.add(stHird);

						StatusMaster stDeclined = new StatusMaster();
						stDeclined.setStatusId(19);
						sList.add(stDeclined);
						
						StatusMaster stRejected = new StatusMaster();
						stRejected.setStatusId(10);
						sList.add(stRejected);
						
						StatusMaster stWithdrew = new StatusMaster();
						stWithdrew.setStatusId(7);
						sList.add(stWithdrew);
						
						criteria.add(Restrictions.in("status",sList));
					}else
					{
						List<StatusMaster> sList = new ArrayList<StatusMaster>();
						sList.add(master);
						if(sts.equals("hird"))
						{
							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}else if(sts.equals("dcln"))
						{
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);

						}else if(sts.equals("rem"))
						{
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
						}
						criteria.add(Restrictions.in("status",sList));
						//criteria.add(Restrictions.eq("status",master));
					}
				}
				if(smaster!=null)
				{
					System.out.println("smaster: "+smaster.getSecondaryStatusName());
					Criteria c = criteria.createCriteria("secondaryStatus");
					
					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
					if(!stq.equals(""))
					{
						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
						Criterion c3 = Restrictions.or(c2, criterion1);
						c.add(c3);
					}else
						c.add(c2);
				}
			}
			System.out.println("inside 12  ");
			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
				if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
				{
					criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
					.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
					//.addOrder(sortOrderStrVal);
				}
				/*else{
					if((headQuarterMaster==null || branchMaster==null) && !(sortOrderStrVal.toString().contains("spStatus") || sortOrderStrVal.toString().contains("normScore")))
					criteria.createCriteria("teacherId").addOrder(sortOrderStrVal);	
				}*/
				
				criteria.setProjection(Projections.groupProperty("teacherId"));
				if(!normScoreFlag){
					criteria.setFirstResult(start);
					criteria.setMaxResults(end);	
				}
			
				try {
					criteria.setFetchMode("teacherId", FetchMode.SELECT);
					criteria.setFetchMode("jobId", FetchMode.SELECT);
					criteria.setFetchMode("status", FetchMode.SELECT);
					criteria.setFetchMode("updatedBy", FetchMode.SELECT);					
					criteria.setFetchMode("districtSpecificNoteFinalizeONBBy", FetchMode.SELECT);
					criteria.setFetchMode("userMaster", FetchMode.SELECT);
					criteria.setFetchMode("statusMaster", FetchMode.SELECT);
					criteria.setFetchMode("secondaryStatus", FetchMode.SELECT);
					criteria.setFetchMode("internalStatus", FetchMode.SELECT);
					criteria.setFetchMode("internalSecondaryStatus", FetchMode.SELECT);
					criteria.setFetchMode("schoolMaster", FetchMode.SELECT);
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("13");
				
				lstTeacherDetail =  criteria.list();
			}else{
				lstTeacherDetail=new ArrayList<TeacherDetail>();
			}
			System.out.println("inside 15");
			if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
				criteria.add(criterionSchool);
			}
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstTeacherDetail;
	
}   
    
    
    
    public List<String[]> findMinJobEndDateFromJobForteacher(TeacherDetail teacherDetail,UserMaster userMaster){
    	  List<String[]> jobOrderList =  new ArrayList<String[]>();
    	  //JobOrder
    	  try{
    	   Session session = getSessionFactory().openSession(); 
    	   Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo");   
    	   criteria.createAlias("jo.districtMaster", "dm")
    	      .setProjection( Projections.projectionList()
    	          .add( Projections.max("createdDateTime"), "jobEndDateMax" )
    	          .add( Projections.min("createdDateTime"), "jobEndDateMin" )
    	      );
    	   if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6){
    	    criteria.add(Restrictions.eq("dm.districtId",userMaster.getDistrictId().getDistrictId())).add(Restrictions.eq("teacherId", teacherDetail));
    	   }else{
    	     criteria.add(Restrictions.eq("teacherId", teacherDetail));
    	   }
    	   List<Integer> listJobIds=null;
    	   if(userMaster.getEntityType()==3){
    	    listJobIds=new ArrayList<Integer>();
    	    listJobIds=schoolInJobOrderDAO.findJobIdBySchool(userMaster.getSchoolId(),userMaster.getDistrictId().getDistrictId());
    	    listJobIds.add(0);
    	    
    	    if(listJobIds!=null){
    	     criteria.add(Restrictions.in("jo.jobId", listJobIds));
    	    }
    	   }
    	   
    	   jobOrderList=criteria.list();   
    	  
    	  }catch(Exception e){
    	   e.printStackTrace();
    	  }   
    	   return jobOrderList; 
    	 }
    	 

    @Transactional(readOnly=false)
    	 public List<String[]> findLastContactedOnJobForteacher(TeacherDetail teacherDetail,UserMaster userMaster,JobOrder jobOrder){

    	  List<String[]> jobOrderList =  new ArrayList<String[]>();
    	  //JobOrder
    	  try{
    	   Session session = getSessionFactory().openSession(); 
    	   Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo");   
    	   criteria.createAlias("jo.districtMaster", "dm")
    	      .setProjection( Projections.projectionList()
    	          .add(Projections.max("lastActivityDate"), "lastActivityDatefvghyg" )
    	          .add(Projections.property("jobForTeacherId"))
    	          .add(Projections.groupProperty("teacherId"))
    	      );
    	   criteria.add(Restrictions.isNotNull("lastActivityDate"));
    	   if(userMaster.getEntityType()!=1 && userMaster.getEntityType()!=5 && userMaster.getEntityType()!=6){
    	    criteria.add(Restrictions.eq("dm.districtId",userMaster.getDistrictId().getDistrictId())).add(Restrictions.eq("teacherId", teacherDetail));
    	   }else{
    	     criteria.add(Restrictions.eq("teacherId", teacherDetail));
    	   }
    	   
    	   
    	   List<Integer> listJobIds=new ArrayList<Integer>();
    	   if(jobOrder!=null){
    	    listJobIds.add(jobOrder.getJobId());
    	   }
    	   if(userMaster.getEntityType()==3){
    	    
    	    listJobIds=schoolInJobOrderDAO.findJobIdBySchool(userMaster.getSchoolId(),userMaster.getDistrictId().getDistrictId());
    	    listJobIds.add(0);
    	   }

    	   if(listJobIds!=null && listJobIds.size()>0){
    	    criteria.add(Restrictions.in("jo.jobId", listJobIds));
    	   }
    	   jobOrderList=criteria.list();   
    	  
    	  }catch(Exception e){
    	   e.printStackTrace();
    	  }   
    	   return jobOrderList; 
    	 
    	 }
    	 
    	 
    	 
    	 // for projection
    	 
    	 
    	 @Transactional(readOnly=false)
    		public List<Integer> getTeacherDetailByDASARecordsCountOp(Order sortOrderStrVal,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> lstTeacherDetailAllFilter,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,List<StatusMaster> lstStatusMastersNew)
    		{
    			PrintOnConsole.getJFTPrint("JFT:72");	
    			TestTool.getTraceTime("500");
    			List<Integer> lstTeacherDetail= new ArrayList<Integer>();
    			try{
    				Session session = getSession();
    				Criteria criteria = session.createCriteria(getPersistentClass());
    				
    				try{
    					if(Utility.getValueOfPropByKey("basePath").contains("platform")){
    						//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
    						JobOrder jobOrder = new JobOrder();
    						 jobOrder.setJobId(11136);
    						 
    						if(jobOrder!=null)
    						criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
    					}
    				}catch(Exception e){
    					e.printStackTrace();
    				}
    				String[] candidatetyp=null;
    				if(internalCandVal!=null){
    						candidatetyp=internalCandVal.split("|");
    				}
    				
    				List<Integer> candidatetypec=new ArrayList<Integer>();
    				if(candidatetyp!=null){
    					for(String s:candidatetyp){
    						if(s.equalsIgnoreCase("0")){
    							candidatetypec.add(1);
    							candidatetypec.add(5);
    						}else{
    							if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
    								int val=Integer.parseInt(s);
    								candidatetypec.add(val);
    							}
    							
    						}
    					}
    				}
    				System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
    				if(candidatetypec.size()>0){
    					criteria.add(Restrictions.in("isAffilated",candidatetypec));
    				}
    				
    				
    				
    				/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
    				if(internalCandVal==1){
    					TestTool.getTraceTime("501");
    					criteria.add(criterionInterCand);
    				}*/

    				criteria.add(Restrictions.ne("status",statusMaster));
    				if(lstStatusMastersNew.size()>0)
    					criteria.add(Restrictions.in("status",lstStatusMastersNew));
    				
    				Criterion criterionSchool =null;
    				if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
    					criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
    				}
    				
    				boolean flag=false;
    				boolean dateDistrictFlag=false;
    				if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
    				{
    					TestTool.getTraceTime("502");
    					Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
    					criteria.add(criterion1);
    				}
    				
    				if((entityID==3 || entityID==4)){
    					TestTool.getTraceTime("503");
    					flag=true;
    					if(lstJobOrder.size()>0)
    					{
    						TestTool.getTraceTime("504");
    						Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
    						criteria.add(criterion1);
    					}
    					if(entityID==4 && dateFlag==false){
    						TestTool.getTraceTime("505");
    						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    					}else if(entityID==4 && dateFlag){
    						TestTool.getTraceTime("506");
    						dateDistrictFlag=true;
    						/*if(fDate!=null && tDate!=null){
    							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    						}else if(fDate!=null && tDate==null){
    							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
    						}else if(fDate==null && tDate!=null){
    							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
    						}*/
    						
    						if(lstJobOP!=null && lstJobOP.size()>0)
    						{
    							Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
    							criteria.add(criterion1);
    						}
    						
    					}
    				}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
    					TestTool.getTraceTime("507");
    					flag=true;
    					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    				}else if(dateFlag && districtMaster!=null &&  entityID==2){
    					TestTool.getTraceTime("508");
    					flag=true;
    					/*if(fDate!=null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    					}else if(fDate!=null && tDate==null){
    						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
    					}else if(fDate==null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
    					}*/
    					
    					if(lstJobOP!=null && lstJobOP.size()>0)
    					{
    						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
    						criteria.add(criterion1);
    					}
    					
    					dateDistrictFlag=true;
    				}
    				else if(entityID==5)
    				{
    					System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
    					flag=true;
    					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
    				}
    				else if(entityID==6)
    				{
    					System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
    					flag=true;
    					criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
    				}
    				if(dateDistrictFlag==false && dateFlag){
    					TestTool.getTraceTime("509");
    					/*if(fDate!=null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    					}else if(fDate!=null && tDate==null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    					}else if(fDate==null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    					}*/
    					
    					if(lstJobOP!=null && lstJobOP.size()>0)
    					{
    						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
    						criteria.add(criterion1);
    					}
    					
    					flag=true;
    				}
    				if(status){
    					TestTool.getTraceTime("510");
    					criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
    					if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
    						criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
    				}
    				
    				//job applied flag
    				//job applied flag
    				if(newcandidatesonlyNew)
    				{
    					if(appliedfDate!=null && appliedtDate!=null)
    					{
    						System.out.println("############### newcandidatesonlyNew Count FT #######################");
    						criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
    					}
    					else if(appliedfDate!=null && appliedtDate==null)
    					{
    						System.out.println("############### newcandidatesonlyNew Count F #######################");
    						//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
    						criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
    					}
    					else if(appliedtDate!=null && appliedfDate==null)
    					{
    						System.out.println("############### newcandidatesonlyNew Count T #######################");
    						criteria.add(Restrictions.le("createdDateTime",appliedtDate));
    					}
    				}
    				else
    				{
    					if(appliedfDate!=null && appliedtDate!=null)
    						criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
    					else if(appliedfDate!=null && appliedtDate==null)
    						criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
    					else if(appliedtDate!=null && appliedfDate==null)
    						criteria.add(Restrictions.le("createdDateTime",appliedtDate));
    				}
    				
    				
    				flag=true;
    				if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
    				{
    					TestTool.getTraceTime("511 T");
    					criteria.add(Restrictions.not(Restrictions.in("teacherId", lstTeacherDetailAllFilter)));
    				}

    				if(teacherFlag){
    					if(filterTeacherList!=null && filterTeacherList.size() >0)
    					{
    						TestTool.getTraceTime("512 T");
    						flag=true; //
    						criteria.add(Restrictions.in("teacherId", filterTeacherList)); //
    					}
    				}
    				if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size()>0) //
    				{
    					TestTool.getTraceTime("513 T");
    					criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));
    				}
    				
    				if(districtMaster!=null  && utype==3)
    				{
    					TestTool.getTraceTime("514");
    					StatusMaster master = districtMaster.getStatusMaster();
    					SecondaryStatus smaster = districtMaster.getSecondaryStatus();
    					if(master!=null)
    					{
    						TestTool.getTraceTime("515");
    						System.out.println("master: "+master.getStatus());
    						String sts = master.getStatusShortName();
    						if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
    						{
    							List<StatusMaster> sList = new ArrayList<StatusMaster>();
    							sList.add(master);
    							if(sts.equals("scomp"))
    							{
    								StatusMaster st1 = new StatusMaster();
    								st1.setStatusId(17);
    								sList.add(st1);
    								
    								StatusMaster st2 = new StatusMaster();
    								st2.setStatusId(18);
    								sList.add(st2);
    								
    							}else if(sts.equals("vcomp"))
    							{
    								StatusMaster st2 = new StatusMaster();
    								st2.setStatusId(18);
    								sList.add(st2);
    							}
    							
    							/*
    							6 - Hired
    							19 - Declined
    							10 - rejected
    							7 -- Withdrew
    							*/
    							
    							StatusMaster stHird = new StatusMaster();
    							stHird.setStatusId(6);
    							sList.add(stHird);

    							StatusMaster stDeclined = new StatusMaster();
    							stDeclined.setStatusId(19);
    							sList.add(stDeclined);
    							
    							StatusMaster stRejected = new StatusMaster();
    							stRejected.setStatusId(10);
    							sList.add(stRejected);
    							
    							StatusMaster stWithdrew = new StatusMaster();
    							stWithdrew.setStatusId(7);
    							sList.add(stWithdrew);
    							
    							criteria.add(Restrictions.in("status",sList));
    						}else
    						{
    							List<StatusMaster> sList = new ArrayList<StatusMaster>();
    							sList.add(master);
    							if(sts.equals("hird"))
    							{
    								StatusMaster stDeclined = new StatusMaster();
    								stDeclined.setStatusId(19);
    								sList.add(stDeclined);
    								
    								StatusMaster stRejected = new StatusMaster();
    								stRejected.setStatusId(10);
    								sList.add(stRejected);
    								
    								StatusMaster stWithdrew = new StatusMaster();
    								stWithdrew.setStatusId(7);
    								sList.add(stWithdrew);
    							}else if(sts.equals("dcln"))
    							{
    								StatusMaster stRejected = new StatusMaster();
    								stRejected.setStatusId(10);
    								sList.add(stRejected);
    								
    								StatusMaster stWithdrew = new StatusMaster();
    								stWithdrew.setStatusId(7);
    								sList.add(stWithdrew);

    							}else if(sts.equals("rem"))
    							{
    								StatusMaster stWithdrew = new StatusMaster();
    								stWithdrew.setStatusId(7);
    								sList.add(stWithdrew);
    							}
    							criteria.add(Restrictions.in("status",sList));
    						}
    					}
    					if(smaster!=null)
    					{
    						System.out.println("smaster: "+smaster.getSecondaryStatusName());
    						Criteria c = criteria.createCriteria("secondaryStatus");
    						
    						Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
    						if(!stq.equals(""))
    						{
    							Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
    							Criterion c3 = Restrictions.or(c2, criterion1);
    							c.add(c3);
    						}else
    							c.add(c2);
    					}
    				}
    				System.out.println("*****************************************************");
    				if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
    					TestTool.getTraceTime("516");
    					if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
    					{
    						TestTool.getTraceTime("517");
    						criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
    						.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
    						.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
    					}				
    					//criteria.setProjection(Projections.countDistinct("teacherId"));
    					String countRow= " count(distinct this_.teacherId)";
    					criteria.setProjection( Projections.projectionList().add(Projections.sqlProjection(""+countRow+" as countRow", new String[]{"countRow"}, new Type[]{ Hibernate.INTEGER}),"countRow"));
    				 
    					
    					// for Optimization Anurag start....
    					try { 
    						criteria.setFetchMode("jobId", FetchMode.SELECT); 
    						criteria.setFetchMode("updatedBy", FetchMode.SELECT);					
    						criteria.setFetchMode("districtSpecificNoteFinalizeONBBy", FetchMode.SELECT);
    						criteria.setFetchMode("userMaster", FetchMode.SELECT);
    						criteria.setFetchMode("statusMaster", FetchMode.SELECT); 
    						criteria.setFetchMode("internalStatus", FetchMode.SELECT);
    						criteria.setFetchMode("internalSecondaryStatus", FetchMode.SELECT);
    						criteria.setFetchMode("schoolMaster", FetchMode.SELECT);
    						
    					} catch (Exception e) {
    						// TODO Auto-generated catch block
    						e.printStackTrace();
    					}
    					// for Optimization Anurag End....
    					
    					
    					
    					lstTeacherDetail =  criteria.list();
    					TestTool.getTraceTime("518 lstTeacherDetail "+lstTeacherDetail.size());
    				}
    				if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
    					criteria.add(criterionSchool);
    				}
    			} 
    			catch (Exception e) {
    				e.printStackTrace();
    			}		
    			TestTool.getTraceTime("599");
    			return lstTeacherDetail;
    		}
    
 
    	 // Anurag for Optimization replacement of Method == >    getAllQQFinalizedTeacherListByJobCategory	 
    	 
    	 
    	 @Transactional(readOnly=false)
    		public List<TeacherDetail> getAllQQFinalizedTeacherListByJobCategoryOp(List<TeacherDetail> lstTeacherDetails,JobCategoryMaster jobCat)
    		{
    			PrintOnConsole.getJFTPrint("JFT:105");
    			List<TeacherDetail> lstTeacher=new ArrayList<TeacherDetail>();
    			
    			if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
    			{
    				try{
    					Session session = getSession();
    					Criteria criteria = session.createCriteria(getPersistentClass());
    					 long start = new Date().getTime();
    					 
    					List<JobOrder> lstJobOrder =  jobOrderDAO.findJobIdByJobCategoryMasterAll(jobCat);
    					 
    					 
    					if(lstTeacherDetails!=null && lstTeacherDetails.size()>0)
    						criteria.add(Restrictions.in("teacherId",lstTeacherDetails));
    					    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
    					    criteria.addOrder(Order.desc("isDistrictSpecificNoteFinalize"));
    					    criteria.add(Restrictions.in("jobId.jobId",lstJobOrder));
    					  //  criteria.createCriteria("jobId").add(Restrictions.eq("jobCategoryMaster", jobCat));
    					    criteria.setProjection(Projections.groupProperty("teacherId"));
    					    
    					    System.out.println("Criteria ======"+criteria);
    					    lstTeacher =  criteria.list();
    					    
    					    
    					   
    					    
    					     long end = new Date().getTime();
    					     System.out.println("total time taken ==="+(end-start)+"ms");
    					     
    					     
    					     
    					    
    					    System.out.println(" getAllQQFinalizedTeacherListByJobCategory ::::::::::  list Size == "+lstTeacher.size());
    				} 
    				catch (Exception e) {
    					e.printStackTrace();
    				}
    			}
    			return lstTeacher;
    		}
    	 
	
  @Transactional(readOnly=false)
  @SuppressWarnings("unchecked")
  public Map<Integer,String> countJobsByTeachers(List<TeacherDetail> teacherDetails,UserMaster userMaster) 
  {
	  Session session = getSession();
	  String sql ="";
	  Query query = null;
	  if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){
		 sql = "SELECT jft.teacherId, COUNT(*) AS appliedCnt from jobforteacher jft, joborder jo where jft.teacherId IN (:teacherDetails) and jft.jobId=jo.jobId and jo.headQuarterId=(:id) group by jft.teacherId";
		 query = session.createSQLQuery(sql);
		 query.setParameter("id", userMaster.getHeadQuarterMaster());
	  }
	  else if(userMaster.getBranchMaster()!=null){
		  sql = "SELECT jft.teacherId, COUNT(*) AS appliedCnt from jobforteacher jft, joborder jo where jft.teacherId IN (:teacherDetails) and jft.jobId=jo.jobId and jo.branchId=(:id) group by jft.teacherId";
		  query = session.createSQLQuery(sql);
		  query.setParameter("id", userMaster.getBranchMaster());
	  }
	  else if(userMaster.getDistrictId()!=null){
		  sql ="SELECT jft.teacherId, COUNT(*) AS appliedCnt from jobforteacher jft, joborder jo where jft.teacherId IN (:teacherDetails) and jft.jobId=jo.jobId and jo.districtId=(:id) group by jft.teacherId";
		  query = session.createSQLQuery(sql);
		  query.setParameter("id", userMaster.getDistrictId());
	  }
	  else{
		  sql = "SELECT jft.teacherId, COUNT(*) AS appliedCnt from jobforteacher jft where jft.teacherId IN (:teacherDetails) group by jft.teacherId";
		  query = session.createSQLQuery(sql);
	  }

	  query.setParameterList("teacherDetails", teacherDetails);

	  List<Object[]> rows = query.list();
	  Map<Integer,String> map = new HashMap<Integer, String>();
	  if(rows.size()>0)
	  {
		  for(Object oobz: rows){

			  Object obj[] = (Object[])oobz;
			  map.put((Integer)obj[0], obj[0]+"###"+obj[1]);
		  }
	  }
	  return map;
  }
  @Transactional(readOnly=false)
	public List<JobForTeacher> findJFTForKellyDashboard(List<TeacherDetail> teacherDetail,List<StatusMaster> statusMasterList,UserMaster userMaster,boolean globalFlag,List<JobOrder> listjJobOrders,boolean statusFlag , boolean ksnId)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		StatusMaster statusMaster = WorkThreadServlet.statusMap.get("vlt");
		try 
		{
			Session session 		= 	getSession();
			Criteria criteria 		= 	session.createCriteria(getPersistentClass());
			Criteria jobOrderSet= criteria.createCriteria("jobId");
			if(globalFlag)
			{
				if((teacherDetail!=null && teacherDetail.size()>0) )
			    {
				  criteria.add(Restrictions.in("teacherId",teacherDetail));			 
				}
				else
					return lstJobForTeacher;
			}
		    
		    if(listjJobOrders.size()>0)
		      criteria.add(Restrictions.in("jobId",listjJobOrders));
		    	
		    if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null)
		    	jobOrderSet.add(Restrictions.eq("headQuarterMaster",userMaster.getHeadQuarterMaster()));
		    else if(userMaster.getBranchMaster()!=null)
		    	jobOrderSet.add(Restrictions.eq("branchMaster",userMaster.getBranchMaster()));
		    if((statusMasterList.size()>0 || statusFlag) && !ksnId){
			    criteria.add(Restrictions.in("status", statusMasterList)).add((Restrictions.in("statusMaster", statusMasterList)));
		    }else if(ksnId){
		    	criteria.add(Restrictions.not(Restrictions.in("status", statusMasterList)));
		    	criteria.add(Restrictions.not(Restrictions.eq("status", statusMaster)));
		    	/*criteria.add(Restrictions.not(Restrictions.in("statusMaster", statusMasterList)));
		    	criteria.add(Restrictions.ne("status",statusMasterList.get(0)));*/	
		    }
			lstJobForTeacher = criteria.list();
			
			System.out.println(" Size Onboarding :"+lstJobForTeacher.size());
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
  @Transactional(readOnly=false)
  public List<String[]> getJobForTeacherByJobAndTeachersListNew(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
  {
  	PrintOnConsole.getJFTPrint("JFT:92");	
  	 List<String[]> lstJobForTeacher=new ArrayList<String[]>();
  	 if(lstTeacherDetails.size()>0){
	         try{
	               Session session = getSession();
	               Criteria criteria = session.createCriteria(getPersistentClass());             
	               criteria.setProjection( Projections.projectionList()
	                       .add( Projections.property("teacherId.teacherId"), "teachId" )
	                       .add( Projections.count("teacherId") )              
	                       .add( Projections.groupProperty("teacherId") )
	                   );
	               Criterion criterionTeacher = Restrictions.in("teacherId",lstTeacherDetails);
	              /* if(districtMaster!=null && entityID!=1){
	            	     criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
	                     //criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
	               }*/
	               
	               if(entityID!=1){
						if(headQuarterMaster!=null && headQuarterMaster.getHeadQuarterId()==1)
						{
							Criteria jobOrderSet= criteria.createCriteria("jobId");
							if(districtMaster!=null){
								jobOrderSet.add(Restrictions.eq("districtMaster",districtMaster));
							}else{
								//jobOrderSet.add(Restrictions.isNull("districtMaster"));
							}
							if(branchMaster!=null){
								jobOrderSet.add(Restrictions.eq("branchMaster",branchMaster));
							}else{
								//jobOrderSet.add(Restrictions.isNull("branchMaster"));
							}
							if(headQuarterMaster!=null){
								if(headQuarterMaster.getHeadQuarterId()==1){
								   jobOrderSet.add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
								}
							}else{
								//jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
							}
						}else
							criteria.add(Restrictions.eq("districtId",districtMaster.getDistrictId()));
						}
	               if(lstTeacherDetails.size()>0){
	                     criteria.add(criterionTeacher);
	               }
	              // criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
	               criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
	               lstJobForTeacher = criteria.list() ;
	         }catch (Throwable e) {
	               e.printStackTrace();
	         }   
  	 }
       return lstJobForTeacher;
  }

  
  	@Transactional(readOnly=false)
	 public List<TeacherDetail> getAllTeacherByStatus(List<StatusMaster> statusMasterList,List<SecondaryStatus> secondaryStatusList,DistrictMaster districtMaster,List<TeacherDetail> teacherDetail) 
	 {
		 
  		List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
		 
		 try{
			 	Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				Criterion criterion1 = null;
				Criterion criterion2 = null;
				Criterion criterion3 = null;
				Criterion criterion4 = null;
				Criterion criterion5 = null;
				
				if(statusMasterList!=null && statusMasterList.size()>0){
					criterion1 = Restrictions.in("status", statusMasterList);
				}
				
				if(secondaryStatusList!=null && secondaryStatusList.size()>0){
					criterion2 = Restrictions.in("secondaryStatus", secondaryStatusList);
				}

				criterion3 = Restrictions.eq("districtId",districtMaster.getDistrictId());
				criterion4 = Restrictions.not(Restrictions.in("teacherId", teacherDetail));
				
				criterion5 = Restrictions.or(criterion1,criterion2);
				
				criteria.add(criterion3);
				criteria.add(criterion4);

				if((statusMasterList!=null && statusMasterList.size()>0) && (secondaryStatusList!=null && secondaryStatusList.size()>0)){
					criteria.add(criterion5);
				}else if(statusMasterList!=null && statusMasterList.size()>0){
					criteria.add(criterion1);
				} else if(secondaryStatusList!=null && secondaryStatusList.size()>0){
					criteria.add(criterion2);
				}
				
				criteria.setProjection(Projections.groupProperty("teacherId"));
				teacherDetailList = criteria.list();
			 
		 } catch(Exception exception) {
			 exception.printStackTrace();
		 }
		 
		 return teacherDetailList;
	 }
  	
  	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherInDesc(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				lstJobForTeacher = findByCriteria(Order.desc("jobForTeacherId"),criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
  	
  	@Transactional(readOnly=false)
	 public List<JobForTeacher> getTeacherByStatusAndSecondaryStatus(List<TeacherDetail> teacherDetailsList,DistrictMaster districtMaster,List<SecondaryStatus> secondaryStatusList) 
	 {
  		List<JobForTeacher> jobForTeacherList = new ArrayList<JobForTeacher>();
  		
  		String[] statuss = {"hird","widrw","hide","vcomp"};
  		//List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
  		List<StatusMaster> statusMasters = Utility.getStaticMasters(statuss);
  		try{
  			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.in("teacherId",teacherDetailsList);
			Criterion criterion2 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion3 = Restrictions.not(Restrictions.in("status", statusMasters));
			Criterion criterion4 = Restrictions.not(Restrictions.in("secondaryStatus", secondaryStatusList));
  			
			if(teacherDetailsList!=null && teacherDetailsList.size()>0)
				criteria.add(criterion1);
			
			criteria.add(criterion2);
			criteria.add(criterion3);
			criteria.add(criterion4);
			
			//criteria.setProjection(Projections.groupProperty("teacherId"));
			jobForTeacherList = criteria.list();
			
  		} catch(Exception exception){
  			exception.printStackTrace();
  		}
  		
  		return jobForTeacherList;
	 }
  	
  	@Transactional(readOnly=false)
	 public List<TeacherDetail> getTeacherByStatusAndSecondaryStatusBackUp(DistrictMaster districtMaster,List<SecondaryStatus> secondaryStatusList) 
	 {
 		
 		List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
 		
 		String[] statuss = {"hird","widrw","hide","vcomp"};
 		//List<StatusMaster> statusMasters =  Utility.getStaticMasters(statuss);
 		List<StatusMaster> statusMasters = Utility.getStaticMasters(statuss);
 		try{
 			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			
			Criterion criterion1 = Restrictions.eq("districtId",districtMaster.getDistrictId());
			Criterion criterion2 = Restrictions.not(Restrictions.in("status", statusMasters));
			Criterion criterion3 = Restrictions.not(Restrictions.in("secondaryStatus", secondaryStatusList));
 			
			criteria.add(criterion1);
			criteria.add(criterion2);
			criteria.add(criterion3);
			
			//criteria.setProjection(Projections.groupProperty("teacherId"));
			teacherDetailList = criteria.list();
			
 		} catch(Exception exception){
 			exception.printStackTrace();
 		}
 		
 		return teacherDetailList;
	 }
  	
  	@Transactional(readOnly=false)
    public Map<Integer,String> getJobForTeacherByJobAndTeachersListWithHQandBRSQL(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,DistrictMaster districtMaster,int entityID)
    {
      System.out.println(":::::::::::::;getJobForTeacherByJobAndTeachersListWithHQandBR  method::::::::::::::::");
      PrintOnConsole.getJFTPrint("JFT:92");     
       
      Session session = getSession();
	  String sql ="";
	  Query query = null;
	
	  
          if(districtMaster!=null){
        	  sql = "SELECT jft.teacherId,COUNT(IF(jft.districtId="+districtMaster.getDistrictId()+",1,NULL)) AS districtApplied ,COUNT(jft.teacherId) allApplied from jobforteacher jft where " +
        	  		" jft.teacherId IN (:teacherDetails) and jft.status IN(:statuss) group by jft.teacherId";
          }else if(headQuarterMaster!=null){
        	  sql = "SELECT jft.teacherId, COUNT(IF(jo.headQuarterId="+headQuarterMaster.getHeadQuarterId()+",1,NULL)) AS districtApplied ,COUNT(jft.teacherId) allApplied " +
        	  		" from jobforteacher jft,joborder jo WHERE jo.jobId = jft.jobId  and " +
  	  		" jft.teacherId IN (:teacherDetails) and jft.status IN(:statuss) group by jft.teacherId";
          }else if(branchMaster!=null && headQuarterMaster!=null){
        	  sql = "SELECT jft.teacherId, COUNT(IF(branch.branchId="+branchMaster.getBranchId()+",1,NULL)) AS districtApplied ,COUNT(IF(jo.headQuarterId="+headQuarterMaster.getHeadQuarterId()+",1,NULL)) allApplied  " +
        	  		" from jobforteacher jft,joborder jo,branchmaster branch WHERE jo.jobId = jft.jobId AND branch.branchId=jo.branchId and " +
  	  		" jft.teacherId IN (:teacherDetails) and jft.status IN(:statuss) and jo.headQuarterId="+headQuarterMaster.getHeadQuarterId()+" group by jft.teacherId";
          }else
          {
          	sql = "SELECT jft.teacherId,COUNT(jft.teacherId) AS districtApplied ,COUNT(jft.teacherId) allApplied from jobforteacher jft where " +
        		" jft.teacherId IN (:teacherDetails) and jft.status IN(:statuss) group by jft.teacherId";
          }
          
	  query = session.createSQLQuery(sql);
	  
	  query.setParameterList("teacherDetails", lstTeacherDetails);
	  query.setParameterList("statuss", statusMasterList);
	  System.out.println("sql::::::::::::::: "+sql);
	  List<Object[]> rows = query.list();
	  Map<Integer,String> map = new HashMap<Integer, String>();
	  if(rows.size()>0)
	  {
		  for(Object oobz: rows){

			  Object obj[] = (Object[])oobz;
			  //System.out.println(obj[0]+" "+obj[1]+"###"+obj[2]);
			  map.put((Integer)obj[0], obj[1]+"###"+obj[2]);
		  }
	  }
	  return map;
    }
  	
 // for Optimization  pagination of CG    	 
	 @Transactional(readOnly=false)
		public List<JobForTeacher> findByJobOrderForCGOp(UserMaster userMaster,JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate,String firstName,String lastName,String emailAddress,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean all,boolean teachersOnly,List<Long> selectedTeachers,int pageNo, int pageSize,String orderColumn,String sortingOrder)
		{
			PrintOnConsole.getJFTPrint("JFT:19");
			List<JobForTeacher> lstJobForTeacher=  new ArrayList<JobForTeacher>();
			try{
				Session session = getSession();
				List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
				Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
				for (StatusMaster sMaster : statusMasterLst) {
					mapStatus.put(sMaster.getStatusShortName(),sMaster);
				}

				Criterion criterionSchool =null;
	            if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
	                criterionSchool=Restrictions.eq("schoolMaster",userMaster.getSchoolId());
	            }	            
				
				StatusMaster statusMaster= mapStatus.get("hide");
				StatusMaster internalStatus= mapStatus.get("icomp");
				Criterion criterion10 = Restrictions.ne("internalStatus",internalStatus);
				Criterion criterion14 = Restrictions.isNull("internalStatus");
				Criterion criterion15 = Restrictions.or(criterion10,criterion14);
				
				Criterion criterion11 = Restrictions.eq("internalStatus",internalStatus);
				Criterion criterion1 = Restrictions.in("status", lstStatusMasters);		
				Criterion criterion13 = Restrictions.or(criterion11,criterion1);
				
				Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
				Criterion criterion3 = Restrictions.ne("status",statusMaster);
				Criterion criterion4 = Restrictions.eq("cgUpdated",true);
				Criterion criterion8 = Restrictions.isNull("cgUpdated");
				Criterion criterion5 = Restrictions.eq("cgUpdated",false);
				
				Criterion criterion6 = Restrictions.or(criterion5, criterion8);
				Criterion criterion9 = Restrictions.or(criterion6, criterion4);
				
				Criteria criteria = session.createCriteria(getPersistentClass());
				
				if(teachersOnly)
				{
					System.out.println("teachersOnly :"+teachersOnly+" ====== selectedTeachers.size() :: "+selectedTeachers.size());
					if(selectedTeachers.size()>0)
					{
						criteria.add(Restrictions.in("jobForTeacherId", selectedTeachers));
					}
				}
				
				if(cgUpdate==0){
					criteria.add(criterion13);
					criteria.add(criterion2);
					criteria.add(criterion3);
					criteria.add(criterion9);
				}else if(cgUpdate==1){
					criteria.add(criterion15);
					criteria.add(criterion1);
					criteria.add(criterion2);
					criteria.add(criterion3);
					/*boolean epiInternal=false;
					try{
						if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
							epiInternal=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					if(jobOrder.getJobCategoryMaster()!=null){
						if(jobOrder.getJobCategoryMaster().getBaseStatus() && jobOrder.getJobCategoryMaster().getStatus().equalsIgnoreCase("A")){
							if(epiInternal==false){
								Criterion criterion7 = Restrictions.eq("isAffilated",1);
								Criterion criterion12 =Restrictions.or(criterion7, criterion4) ;
								criteria.add(criterion12);
							}else{
								criteria.add(criterion4);
							}
						}
					}*/
				}else if(cgUpdate==7){
					criteria.add(criterion15);
					criteria.add(criterion1);
					criteria.add(criterion2);
					criteria.add(criterion3);
				}else if(cgUpdate==2){
					criteria.add(criterion1);
					criteria.add(criterion2);
					criteria.add(criterion3);
				}else if(cgUpdate==5){
					criteria.add(criterion15);
					criteria.add(criterion1);
					criteria.add(criterion2);
					criteria.add(criterion3);
				}else if(cgUpdate==3){
					criteria.add(criterion2);
					criteria.add(criterion3);
				}else{
					criteria.add(criterion2);
					criteria.add(criterion3);
				}
				
				 if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
		                criteria.add(criterionSchool);
		           }
				
				if(teacherFlag){
					criteria.add(Restrictions.in("teacherId", filterTeacherList));
				}
	
				 
				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
			/*	if(orderColumn!=null && orderColumn.equalsIgnoreCase("candidate"))
				{
					if(sortingOrder.equalsIgnoreCase("0"))
						criteria.addOrder(Order.desc("lastName"));
					else{
						criteria.addOrder(Order.asc("lastName"));
					}
				}
			 */
				
				//criteria.addAlias("teacherNormScore","joined_alias",CriteriaSpecification.LEFT_JOIN);
				
				 criteria.setFirstResult(pageNo * pageSize);
				 criteria.setMaxResults(pageSize);
				lstJobForTeacher =  criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			System.out.println("lstJobForTeacher    "+lstJobForTeacher.size()+"   Gourav");
			return lstJobForTeacher;
		}
	 
	 	@Transactional(readOnly=false)
		public List<JobForTeacher> findByJobOrderForCGOpSql(UserMaster userMaster,JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate,String firstName,String lastName,String emailAddress,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean all,boolean teachersOnly,List<Long> selectedTeachers,int pageNo, int pageSize,String orderColumn,String sortingOrder)
		{
			PrintOnConsole.getJFTPrint("JFT:19 sql query   "+orderColumn);
			List<JobForTeacher> lstJobForTeacher=  new ArrayList<JobForTeacher>();
			//
			try{
		
				List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
				Map<Integer, StatusMaster> mapStatus = new HashMap<Integer, StatusMaster>();
				for (StatusMaster sMaster : statusMasterLst) {
					mapStatus.put(sMaster.getStatusId(),sMaster);
				}
				
				
				String criterionSchool ="";
				if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
					criterionSchool=" and jft.hiredBySchool="+userMaster.getSchoolId()+" ";
	            }
	            
				if(teachersOnly)
				{
					System.out.println("teachersOnly :"+teachersOnly+" ====== selectedTeachers.size() :: "+selectedTeachers.size());
					if(selectedTeachers.size()>0)
					{						
						String jftIdLst="";
						for (Long jftId : selectedTeachers) {
							if(jftIdLst.equalsIgnoreCase(""))
								jftIdLst+=jftId+"";
							else
								jftIdLst+=","+jftId;
						}
						criterionSchool=" and jft.jobForTeacherId in("+jftIdLst+")";
					}
				}
				
				if(teacherFlag){
					String tidslst="";
					for (TeacherDetail tIds : filterTeacherList) {
						if(tidslst.equalsIgnoreCase("")){
							tidslst+=tIds.getTeacherId()+"";
						}else{
							//tidslst+=tIds.getTeacherId()+",";
							tidslst+=","+tIds.getTeacherId();
						}
					}
					criterionSchool+=" and jft.teacherId in ("+tidslst+") ";
				}
				String statusMaster= "";//mapStatus.get("hide");
				
				String ststuaList=""; 
				for (StatusMaster lstStatusMasterss : lstStatusMasters) {
					if(!ststuaList.equalsIgnoreCase(""))
						ststuaList+=","+lstStatusMasterss.getStatusId();
					else
						ststuaList+=lstStatusMasterss.getStatusId();
					if(lstStatusMasterss.getStatusShortName().equalsIgnoreCase("hide")){
						statusMaster=lstStatusMasterss.getStatusId()+""; 
					}
					
				}
				String criterion15 = " and (jft.internalStatus!=3 or jft.internalStatus IS NULL) ";//Restrictions.or(criterion10,criterion14);

				
				String criterion13 = " and (jft.internalStatus!=3 or jft.status in ("+ststuaList+")) ";//Restrictions.or(criterion11,criterion1);				
				String criterion1 = " and jft.status in ("+ststuaList+") ";
				String criterion3 = " and jft.status!=8 ";
			
				String allCondidtions="jft.jobId="+jobOrder.getJobId()+" ";
				if(cgUpdate==0){
					allCondidtions+= criterion13+criterion3;
				}else if(cgUpdate==1){
					allCondidtions+=criterion15+criterion1+criterion3;
					boolean epiInternal=false;
					try{
						if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
							epiInternal=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					if(jobOrder.getJobCategoryMaster()!=null){/*
						if(jobOrder.getJobCategoryMaster().getBaseStatus() && jobOrder.getJobCategoryMaster().getStatus().equalsIgnoreCase("A")){
							if(epiInternal==false){
								Criterion criterion7 = Restrictions.eq("isAffilated",1);
								Criterion criterion12 =Restrictions.or(criterion7, criterion4) ;
								criteria.add(criterion12);
								//String criterion4 = Restrictions.eq("cgUpdated",true);
								allCondidtions+=" and (jft.isAffilated=1 or jft.cgUpdated=1) ";
							}else{
								allCondidtions+=" and jft.cgUpdated=1 ";
							}
						}
					*/}
				}else if(cgUpdate==7){					
					allCondidtions+=criterion15+criterion1+criterion3;
				}else if(cgUpdate==2){
					allCondidtions+=criterion1+criterion3;
				}else if(cgUpdate==5){
					allCondidtions+=criterion15+criterion1+criterion3;
				}else if(cgUpdate==3){
					allCondidtions+=criterion3;
				}else{
					allCondidtions+=criterion3;
				}

				String start =""+(pageNo * pageSize);
				String end =""+pageSize;
				
				String limit="";
				if(pageSize!=0){
					limit=" limit "+start+","+end;
				}
				
				allCondidtions+=" and firstName like '%"+firstName.trim()+"%' and lastName like '%"+lastName.trim()+"%' and emailAddress like '%"+emailAddress.trim()+"%'";
				System.out.println("allCondidtionsallCondidtions    "+allCondidtions);
				
				String sortOrderStr="";
				boolean baseStatusFlag=false;
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
					baseStatusFlag=true;
				}
				
				if(orderColumn!=null && orderColumn.equalsIgnoreCase("normScore") && baseStatusFlag)
				{
					if(sortingOrder.equalsIgnoreCase("0"))
						sortOrderStr = " Order By "+orderColumn+" desc"; 
					else{
						sortOrderStr = " Order By "+orderColumn+" asc";
					}
				}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("normScore") && sortingOrder.equalsIgnoreCase("0") && sortingOrder.equalsIgnoreCase("0")){					
						sortOrderStr = " Order By lastName asc";					
				}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("fScore")){					
					if(sortingOrder.equalsIgnoreCase("0"))
						sortOrderStr = " Order By "+orderColumn+" desc"; 
					else{
						sortOrderStr = " Order By "+orderColumn+" asc";
					}					
				}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("status")){
					if(sortingOrder.equalsIgnoreCase("0"))
						sortOrderStr = " Order By currentStatus asc"; 
					else{
						sortOrderStr = " Order By currentStatus  desc";
					}					
				}else{
					
					if(sortingOrder.equalsIgnoreCase("0"))					
						sortOrderStr = " Order By lastName desc"; 
					else{
						sortOrderStr = " Order By lastName asc";
					}
				}
				Connection connection =null;
				String sql="SELECT jft.*,tdl.firstName as firstName,tdl.lastName as lastName,tdl.emailAddress,tdl.isPortfolioNeeded,tns.teacherNormScore as normScore,"+
				"status1.statusShortName,status1.status as statusFeildName,"+
				"CASE WHEN jft.displayStatusId IS NOT NULL THEN (CASE WHEN jft.status IN (16,17,18) THEN (SELECT secondaryStatusName FROM secondarystatus WHERE parentStatusId=jft.status AND districtId=jft.districtId AND jobCategoryId="+jobOrder.getJobCategoryMaster().getJobCategoryId()+") ELSE  status1.status END) ELSE secondarystatus1.secondaryStatusName END AS currentStatus, "+
				"secondarystatus1.secondaryStatusName,"+
				"jobwiseconteascore.jobWiseConsolidatedScore as fScore "+
				" FROM jobforteacher jft "+ 
		        " LEFT JOIN teachernormscore tns ON tns.teacherId = jft.teacherId "+
		        " INNER JOIN teacherdetail tdl ON tdl.teacherId = jft.teacherId "+
		        " INNER JOIN statusmaster status1 ON status1.statusId = jft.status "+		        
		        " LEFT JOIN secondarystatus secondarystatus1 ON secondarystatus1.secondaryStatusId = jft.displaySecondaryStatusId "+
		        " LEFT JOIN jobwiseconsolidatedteacherscore jobwiseconteascore ON jobwiseconteascore.teacherId = jft.teacherId and jobwiseconteascore.jobId = jft.jobId"+
		        " WHERE "+allCondidtions+" "+criterionSchool+" group by jft.teacherId "+sortOrderStr+limit;
		        //" limit "+start+","+end;
				
				//jobwiseconsolidatedteacherscore
				System.out.println("sql  "+sql);
				SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
			      ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			        connection = connectionProvider.getConnection();
				PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE); 
				 
				  ResultSet rs=ps.executeQuery();
				  rs.last();
				  int rowcount=rs.getRow();
				  rs.beforeFirst();
				  if(rs.next()){				  
					  
					    do{
					     final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					     JobForTeacher tempJft = new JobForTeacher();
					     	tempJft.setJobForTeacherId(Long.parseLong(rs.getString(rs.findColumn("jobForTeacherId"))));//jobForTeacherId
					     	if(rs.getString("districtId")!=null){
					     		tempJft.setDistrictId(Integer.parseInt(rs.getString(rs.findColumn("districtId"))));//districtId
					     	}
					     	TeacherDetail tDetail = new TeacherDetail();
					     	tDetail.setTeacherId(Integer.parseInt(rs.getString(rs.findColumn("teacherId"))));
					     	tDetail.setFirstName(rs.getString("firstName"));
					     	tDetail.setLastName(rs.getString("lastName"));
					     	tDetail.setEmailAddress(rs.getString("emailAddress"));
					     	boolean isPortfolioNeeded = false;
					     	if(rs.getString("tdl.isPortfolioNeeded")!=null){
					     		isPortfolioNeeded=rs.getBoolean("tdl.isPortfolioNeeded");
					     		
					     	}
					     	tDetail.setIsPortfolioNeeded(isPortfolioNeeded);
					     	//System.out.println("rs.getString()    "+rs.getString("firstName"));
					     	tempJft.setTeacherId(tDetail);//teacherId
					     /*	JobOrder tempJobOrder = new JobOrder();
					     	tempJobOrder.setJobId(Integer.parseInt(rs.getString(rs.findColumn("jobId"))));*/
					     	tempJft.setJobId(jobOrder);
					     	tempJft.setRedirectedFromURL(rs.getString("redirectedFromURL"));
					     	tempJft.setJobBoardReferralURL(rs.getString("jobBoardReferralURL"));
					     	StatusMaster tempStatus = null;					     	
					     	if(rs.getString("status")!=null){
					     		tempStatus = mapStatus.get(Integer.parseInt(rs.getString("status"))); 
					     	}
					     	tempJft.setStatus(tempStatus);
					     	StatusMaster tempStatus2 = null;
					     	if(rs.getString("displayStatusId")!=null){
					     		tempStatus2 = mapStatus.get(Integer.parseInt(rs.getString("displayStatusId")));
					     	}
					     	tempJft.setStatusMaster(tempStatus2);
					     	
					     	if(rs.getString("displaySecondaryStatusId")!=null){
					     		SecondaryStatus secondaryStatus = new SecondaryStatus();
					     		secondaryStatus.setSecondaryStatusId(Integer.parseInt(rs.getString("displaySecondaryStatusId")));
					     		secondaryStatus.setSecondaryStatusName(rs.getString("secondarystatus1.secondaryStatusName"));					     		
					     		tempJft.setSecondaryStatus(secondaryStatus);
					     	}else{
					     		tempJft.setSecondaryStatus(null);
					     	}
					     		
					     		
					     	StatusMaster tempStatus3 = null;
					     	if(rs.getString("internalStatus")!=null)
					     		tempStatus3 = mapStatus.get(Integer.parseInt(rs.getString("internalStatus")));					     		
					     	tempJft.setInternalStatus(tempStatus3);
					     	
					     	
					     	if(rs.getString("internalSecondaryStatusId")!=null){
					     		SecondaryStatus secondaryStatus2 = new SecondaryStatus();
					     		secondaryStatus2.setSecondaryStatusId(Integer.parseInt(rs.getString("internalSecondaryStatusId")));
					     		tempJft.setInternalSecondaryStatus(secondaryStatus2);
					     	}else{
					     		tempJft.setInternalSecondaryStatus(null);
					     	}
					     	if(rs.getString("cgUpdated")!=null)
					     		tempJft.setCgUpdated(rs.getBoolean("cgUpdated"));
					     	tempJft.setCoverLetter(rs.getString("coverLetter"));
					     	tempJft.setCoverLetterFileName(rs.getString("coverLetterFileName"));
					     	if(rs.getString("isAffilated")!=null)
					     		tempJft.setIsAffilated(Integer.parseInt(rs.getString("isAffilated")));
					     	tempJft.setStaffType(rs.getString("staffType"));
					     	if(rs.getString("flagForDistrictSpecificQuestions")!=null)
					     		tempJft.setFlagForDistrictSpecificQuestions(rs.getBoolean("flagForDistrictSpecificQuestions"));
					     	tempJft.setNoteForDistrictSpecificQuestions(rs.getString("noteForDistrictSpecificQuestions"));
					     	if(rs.getString("isDistrictSpecificNoteFinalize")!=null)
					     		tempJft.setIsDistrictSpecificNoteFinalize(rs.getBoolean("isDistrictSpecificNoteFinalize"));
					     	
					     	if(rs.getString("districtSpecificNoteFinalizeBy")!=null){
					     		UserMaster finalizeBY = new UserMaster();
					     		finalizeBY.setUserId(Integer.parseInt(rs.getString("districtSpecificNoteFinalizeBy")));
					     		tempJft.setDistrictSpecificNoteFinalizeBy(finalizeBY);
					     	}else{
					     		tempJft.setDistrictSpecificNoteFinalizeBy(null);
					     	}
					     	if(rs.getString("flagForDspq")!=null)
					     		tempJft.setFlagForDspq(rs.getBoolean("flagForDspq"));
					     	tempJft.setRequisitionNumber(rs.getString("requisitionNumber"));
					     	
					     	if(rs.getString("updatedBy")!=null){
					     		UserMaster usrMas = new UserMaster();
					     		usrMas.setUserId(Integer.parseInt(rs.getString("updatedBy")));
					     		tempJft.setUpdatedBy(usrMas);
					     	}else{
					     		tempJft.setUpdatedBy(null);
					     	}
					     	if(rs.getString("updatedByEntity")!=null)
					     		tempJft.setUpdatedByEntity(Integer.parseInt(rs.getString("updatedByEntity")));
					     	tempJft.setLastActivity(rs.getString("lastActivity"));
					     	
					     	if(rs.getString("lastActivityDoneBy")!=null){
					     		UserMaster usrMas2 = new UserMaster();
					     		usrMas2.setUserId(Integer.parseInt(rs.getString("lastActivityDoneBy")));
					     		tempJft.setUserMaster(usrMas2);
					     	}else{
					     		tempJft.setUserMaster(null);
					     	}
					     	
					     	if(rs.getString("hiredBySchool")!=null){
					     		SchoolMaster hiredSchool = new SchoolMaster();
					     		hiredSchool.setSchoolId(Long.parseLong(rs.getString("hiredBySchool")));
					     		tempJft.setSchoolMaster(hiredSchool);
					     	}else{
					     		tempJft.setSchoolMaster(null);
					     	}
					     	if(rs.getString("offerReady")!=null)
					     		tempJft.setOfferReady(rs.getBoolean("offerReady"));
					     	if(rs.getString("offerAccepted")!=null)
					     		tempJft.setOfferAccepted(rs.getBoolean("offerAccepted"));
					     	if(rs.getString("noResponseEmail")!=null)
					     		tempJft.setNoResponseEmail(rs.getBoolean("noResponseEmail"));
					     	if(rs.getString("candidateConsideration")!=null)
					     		tempJft.setCandidateConsideration(rs.getBoolean("candidateConsideration"));
					     	if(rs.getString("noOfReminderSent")!=null)
					     		tempJft.setNoOfReminderSent(Integer.parseInt(rs.getString("noOfReminderSent")));
					     	if(rs.getString("noofTries")!=null)
					     		tempJft.setNoofTries(Integer.parseInt(rs.getString("noofTries")));
					     	if(rs.getString("branchcontact")!=null)
					     		tempJft.setBranchcontact(rs.getBoolean("branchcontact"));
					     	if(rs.getString("jobCompleteDate")!=null)
					     		tempJft.setJobCompleteDate(rs.getDate("jobCompleteDate"));
					     //JobCompleteDate
					     	tempJft.setCreatedDateTime(rs.getDate("createdDateTime"));
					     	if(rs.getString("notReviewedFlag")!=null){
					     		tempJft.setNotReviewedFlag(rs.getBoolean("notReviewedFlag"));
					     	}
					     	if(rs.getString("normScore")!=null)
					     		tempJft.setNormScore(Double.parseDouble(rs.getString("normScore")));
					     	
					     	if(rs.getString("fScore")!=null){
					     		tempJft.setFitScore(rs.getDouble("fScore"));
					     	}
					     	lstJobForTeacher.add(tempJft);
					     
					    }while(rs.next());
					   }
					   else{
					    System.out.println("Record not found.");
					   }
				} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}
	 
	 /* Candidates Not Reviewed by Job Summary Report*/
		@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		//public  List<String[]> candidatesNotReviewedList(Integer districtId,String jobStatus, int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,String startDate,String enddate,String jobendstartDate,String jobendendDate)
		public  List<String[]> candidatesNotReviewedList(Integer districtId,String jobStatus,Integer jobCategory, int sortingcheck, String sortOrderStrVal ,int start, int noOfRow,boolean report,String sortColomnName,String startDate,String enddate,String jobendstartDate,String jobendendDate)
		{
			List<String[]> lst=new ArrayList<String[]>(); 
			Session session = getSession();
			String sql = "";
			Connection connection = null;
			System.out.println(startDate+"::::::::::::: HANZALA ::::::::::::::"+enddate);
			try{
				 SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
				 ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
				 connection = connectionProvider.getConnection();
				 
				   Date startD = new Date("1/1/2000");
		           Date endD = new Date("12/31/9999"); 

		           if(startDate!=null && !startDate.equals(""))
		        	   startD =  Utility.getCurrentDateFormart(startDate);

		           if(enddate!=null && !enddate.equals(""))
		        	   endD = Utility.getCurrentDateFormart(enddate);
		           
		           Date jobendstartD = new Date("1/1/2000");
		           Date jobendendD = new Date("12/31/9999"); 

		           if(jobendstartDate!=null && !jobendstartDate.equals(""))
		         	  jobendstartD =  Utility.getCurrentDateFormart(jobendstartDate);

		           if(jobendendDate!=null && !jobendendDate.equals(""))
		         	  jobendendD = Utility.getCurrentDateFormart(jobendendDate);
				 
		           if(sortColomnName.equalsIgnoreCase("districtName"))
						sortColomnName = "District";
					else if(sortColomnName.equalsIgnoreCase("jobTitle"))
						sortColomnName = "Job_Title";
					else if(sortColomnName.equalsIgnoreCase("jobCategory"))
						sortColomnName = "Job_Category";
					else if(sortColomnName.equalsIgnoreCase("jobId"))
						sortColomnName = "Job_ID";
					else if(sortColomnName.equalsIgnoreCase("jobStatus"))
						sortColomnName = "Job_Status";
					else if(sortColomnName.equalsIgnoreCase("jobPostDate"))
						sortColomnName = "Job_Post_Date";
					else if(sortColomnName.equalsIgnoreCase("jobEndDate"))
						sortColomnName = "Job_End_Date";
					else if(sortColomnName.equalsIgnoreCase("noOfNotReviewed"))
						sortColomnName = "N_of_Not_Reviewed";
		           
		           String statusValue="and jo.status = ?";
		           if(jobStatus!=null)
		           {
		        	   jobStatus = jobStatus.trim();
		        	   if(!jobStatus.equals("") && jobStatus.equalsIgnoreCase("All"))
		        		   statusValue="";
		        		   
		           }
		           
		           String jobCategoryValue="and jcm.jobcategoryid= ? ";
		           if(jobCategory == 0)
		           {
		        		   jobCategoryValue="";
		           }
		           
					String orderby = " order by "+sortColomnName+" "+sortOrderStrVal ;
		           
				//districtId = 1200390;
				 System.out.println(jobCategoryValue+":::::::::"+jobCategory+"::::::"+districtId);
				 sql="select dm.districtname District, " +
				 	 " jo.jobtitle Job_Title," +
				 	 " jcm.jobcategoryname Job_Category," +
				 	 " jft.jobid Job_ID, " +
				 	 " jo.status Job_Status," +
				 	 " jo.jobstartdate Job_Post_Date," +
				 	 " jo.jobenddate Job_End_Date, count(*) N_of_Not_Reviewed " +
				 	 " from jobforteacher jft left join statusmaster sm " +
				 	 " on sm.statusid=jft.status " +
				 	 " left join teachersecondarystatus tss on jft.teacherid=tss.teacherid " +
				 	 " and jft.jobid=tss.jobid and jft.districtid=tss.districtid " +
				 	 " left join secondarystatusmaster ssm on ssm.secstatusid=tss.secStatusId " +
				 	 " left join secondarystatus ss on ss.secondarystatusId=jft.displaysecondarystatusid " +
				 	 " left join joborder jo on jo.jobid=jft.jobid" +
				 	 " left join jobcategorymaster jcm on jo.jobcategoryid=jcm.jobcategoryid " +
				 	 " left join districtmaster dm on dm.districtid=jft.districtid " +
				 	 " left join democlassschedule dcs on dcs.teacherid=jft.teacherid and dcs.jobid=jft.jobid " +
				 	 " left join panelschedule ps on ps.teacherid=jft.teacherid and ps.jobid=jft.jobid " +
				 	 " left join teacherphonecallhistory tpch on tpch.teacherid=jft.teacherid and tpch.jobid=jft.jobid " +
				 	 " left join onlineactivitynotes oan on oan.teacherid=jft.teacherid and oan.jobid=jft.jobid " +
				 	 " left join referencenotes rn on rn.teacherid=jft.teacherid and rn.jobid=jft.jobid " +
				 	 " left join teacherstatusnotes tsn on tsn.teacherid=jft.teacherid and tsn.jobid=jft.jobid " +
				 	 " left join jsicommunicationlog jcl on jcl.teacherid=jft.teacherid and jcl.jobid=jft.jobid " +
				 	 " left join jobforteacher hired on hired.teacherid=jft.teacherid and hired.status=6 " +
				 	 " where jft.status=4 " +
				 	 " and jft.lastactivity is null " +
				 	 " and ssm.secstatusname is null " +
				 	 " and jft.displaysecondarystatusid is null" +
				 	 " and dcs.demoid is null " +
				 	 " and ps.panelid is null " +
				 	 " and tpch.teacherphonecallid is null " +
				 	 " and tpch.teacherid is null " +
				 	 " and oan.teacherid is null " +
				 	 " and rn.teacherid is null " +
				 	 " and tsn.teacherid is null " +
				 	 " and jcl.teacherid is null " +
				 	 " and hired.teacherid is null " +
				 	 " and jft.districtid= ? " +statusValue +" " +
				 	 " " +jobCategoryValue+" " +
				 	 " and jo.jobStartDate >= ? and jo.jobStartDate <= ?  " +
				 	 " and jo.jobEndDate >= ? and jo.jobEndDate <= ? " +
				 	 " group by jft.districtid, jft.jobid " +
				     " "+orderby;
				 
				 // " order by dm.districtname, jo.jobtitle ";
				 
				  java.sql.Date startD1 = new java.sql.Date(startD.getTime());
				  java.sql.Date endD1= new java.sql.Date(endD.getTime());
				  
				  java.sql.Date jobendstartD1 = new java.sql.Date(jobendstartD.getTime());
				  java.sql.Date jobendendD1= new java.sql.Date(jobendendD.getTime());
				  
				 PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);		
				 ps.setString(1, districtId.toString());
				
				 if(!statusValue.equals("") || !jobCategory.equals(0))
					{
					 	if(statusValue.equals("")) {
					 		if(!jobCategory.equals(0)){
					 			ps.setInt(2, jobCategory );
								ps.setDate(3, startD1 );
								ps.setDate(4, endD1);
								ps.setDate(5, jobendstartD1 );
							    ps.setDate(6, jobendendD1);
					 		}
					 	} else {
					 		ps.setString(2, jobStatus);
					 		if(!jobCategory.equals(0)){
					 			ps.setInt(3, jobCategory );
								ps.setDate(4, startD1 );
								ps.setDate(5, endD1);
								ps.setDate(6, jobendstartD1 );
							    ps.setDate(7, jobendendD1);
					 		} else {
								ps.setDate(3, startD1 );
								ps.setDate(4, endD1);
								ps.setDate(5, jobendstartD1 );
							    ps.setDate(6, jobendendD1);
					 		}
					 	}
					 }
					else
					{
						ps.setDate(2, startD1 );
						ps.setDate(3, endD1);
						ps.setDate(4, jobendstartD1);
						ps.setDate(5, jobendendD1);
					}
				
				 ResultSet rs=ps.executeQuery();
				 if(rs.next()){
						do{
							final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
							for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
							   {
								allInfo[i-1]=rs.getString(i);
							   }
							lst.add(allInfo);
						}while(rs.next());
					
				 } else {
					 System.out.println("Record not found.");
				 } 
				 
				 return lst;
				
			}catch(Exception exception){
				exception.printStackTrace();
			} finally{
				if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return null;
		}
		
	 // Optimization purpose get only teacherid for specefic destrict
	 
	 @Transactional(readOnly=false)
		public List<Integer> findTeacersIdByDistrict(UserMaster userMaster)
		{
			PrintOnConsole.getJFTPrint("JFT:38");
			List<Integer> lstJobForTeacher=new ArrayList<Integer>();
			List<Integer> jobsId = new ArrayList<Integer>();
			Integer entityType = 0;
			if(userMaster!=null)
				entityType = userMaster.getEntityType();
			
			Criterion mainCriterion = null;
			
			switch (entityType) {
			
			case 1:
			{
				// for TM Admin
				 
				
				
			}
				 break;
			case 2:
			{
				// for District Admin
				mainCriterion = Restrictions.eq("districtId", userMaster.getDistrictId().getDistrictId());
			}
				 break;
			case 3:
			{
				// For School Admin
				jobsId = schoolInJobOrderDAO.findJobsIdByCriterion(Restrictions.eq("schoolId", userMaster.getSchoolId()));
				if(jobsId!=null){
				jobsId.add(0);
				mainCriterion = Restrictions.in("jobId.jobId", jobsId);
				}
				
			}
				 break;
			
			case 5:
			{
				// for HeadQuarter Admin
				jobsId = jobOrderDAO.findJobsIdByCriterion(Restrictions.eq("headQuarterMaster", userMaster.getHeadQuarterMaster()));
				if(jobsId!=null){
				jobsId.add(0);
				mainCriterion = Restrictions.in("jobId.jobId", jobsId);
				}
			}
				 break;
				 
			case 6:
			{
				// For Branch Admin
				
				jobsId = jobOrderDAO.findJobsIdByCriterion(Restrictions.eq("branchMaster", userMaster.getBranchMaster()));
				if(jobsId!=null){
				jobsId.add(0);
				mainCriterion = Restrictions.in("jobId.jobId", jobsId);
				}
				
			}
				 break;
				
	
				

			default:
				break;
			}
			
			try 
			{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				if(mainCriterion!=null)
				criteria.add(mainCriterion); 
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
				criteria.setCacheable(true); 
				lstJobForTeacher = criteria.list();
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}
	 
	 // Optimization for Candiadte Pool search replacement of getAllQQFinalizeTeacherList
	 @Transactional(readOnly=false)
		public List<Integer> getAllQQFinalizeTeacherList_Op(DistrictMaster districtMaster,List<Integer> teachersId)
		{
	    	PrintOnConsole.getJFTPrint("JFT:96");	
			List<Integer> teacherDetailList = new ArrayList<Integer>();
			if(teachersId!=null && teachersId.size()>0)
				if(districtMaster!=null){
					try{
						Session session = getSession();
						Criteria criteria = session.createCriteria(getPersistentClass());
					    criteria.add(Restrictions.eq("isDistrictSpecificNoteFinalize",true));
					    criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					    
					    criteria.add(Restrictions.in("teacherId.teacherId", teachersId));
						criteria.setProjection(Projections.property("teacherId.teacherId")); 
						criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
					    teacherDetailList = criteria.list();
					}
					catch (Exception e) {
						e.printStackTrace();
					}
				}
			return teacherDetailList;
		}

	 // Optimization for Candiadte Pool search replacement of findTeachersBySubjectList

	 @Transactional(readOnly=false)
		public List<Integer> findTeachersBySubjectList_Op(List<SubjectMaster> subjectMasters,List<Integer> teachersId)
		{
			PrintOnConsole.getJFTPrint("JFT:31");
			List<Integer> results = new ArrayList<Integer>();
			
			if(teachersId!=null && teachersId.size()>0)
				try 
				{
					Session session = getSession();
					
					if(subjectMasters.size()>0)
					{
						Criteria criteria= session.createCriteria(getPersistentClass()); 
						StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
						criteria.add(Restrictions.ne("status",statusMaster));
						
						Criteria incriteria = criteria.createCriteria("jobId");
						incriteria.add(Restrictions.in("subjectMaster",subjectMasters));
						 criteria.add(Restrictions.in("teacherId.teacherId", teachersId));
							criteria.setProjection(Projections.property("teacherId.teacherId")); 
							criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId"))); 
						results = criteria.list();
					}
					 
				}catch (Exception e) {
					e.printStackTrace();
				}		
			return results;
		}
	 
	 // Optimization for Candiadte Pool search replacement of getJobForTeacherByStatus
	 
	 @Transactional(readOnly=false)
		public List<Integer> getJobForTeacherByStatus_Op(List<Integer> lstJobOrder)
		{
			List<Integer> lstJobForTeacher= new ArrayList<Integer>();
			
			if(lstJobOrder!=null && lstJobOrder.size()>0)
			try 
			{
				Session session = getSession();
				Criteria criteria= session.createCriteria(getPersistentClass()); 
			//	Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
				Criterion criterion1 = Restrictions.ne("status",statusMaster);
				Criterion criterion2 = Restrictions.in("jobId.jobId",lstJobOrder);
	            
				criteria.add(criterion1);
				criteria.add(criterion2);
				
				criteria.setProjection(Projections.property("teacherId.teacherId")); 
				criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId"))); 
				lstJobForTeacher = criteria.list();
				//lstJobForTeacher = findByCriteria(criterion1,criterion2);		
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		}

	 // Optimization for Candidate pool search
	 
		@Transactional(readOnly=false)
		public List<Integer> getTeacherDetailByDASARecordsBeforeDateNew_Op(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<Integer> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<Integer> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,StatusMaster statusMaster)
		{
			PrintOnConsole.getJFTPrint("JFT:73");	
			List<Integer> lstTeacherDetail= new ArrayList<Integer>();
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
				try{ 
					if(Utility.getValueOfPropByKey("basePath").contains("platform")){
						//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
						JobOrder jobOrder = new JobOrder();
						 jobOrder.setJobId(11136);
						 
						if(jobOrder!=null)
						criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
					}
				}catch(Exception e){
					e.printStackTrace();
			}
			String[] candidatetyp=null;
			if(internalCandVal!=null){
					candidatetyp=internalCandVal.split("|");
			}
			
			List<Integer> candidatetypec=new ArrayList<Integer>();
			if(candidatetyp!=null){
				for(String s:candidatetyp){
					if(s.equalsIgnoreCase("0")){
						candidatetypec.add(1);
						candidatetypec.add(5);
					}else{
						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
							int val=Integer.parseInt(s);
							candidatetypec.add(val);
						}
						
					}
				}
			}
			System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
			if(candidatetypec.size()>0){
				criteria.add(Restrictions.in("isAffilated",candidatetypec));
			}
			
			
			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}*/
			boolean flag=true;
			boolean dateDistrictFlag=false;
			
			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
					criteria.add(criterion1);
				}
				
				if((entityID==3 || entityID==4)){
					if(lstJobOrder.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
						criteria.add(criterion1);
					}
					if(entityID==4 && dateFlag==false){
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
					}else if(entityID==4 && dateFlag){
						dateDistrictFlag=true;
						if(fDate!=null && tDate!=null){
							criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
							criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
						}else if(fDate!=null && tDate==null){
							criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
						}else if(fDate==null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
						}
					}
				}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(dateFlag && districtMaster!=null &&  entityID==2){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
					dateDistrictFlag=true;
				}
				if(dateDistrictFlag==false && dateFlag){
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
				if(status)
					criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				else
					criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
				
				//job applied flag
				
				/*if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
					criteria.add(Restrictions.le("createdDateTime",appliedfDate));
				else if(daysVal==5 && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
				else if(appliedfDate!=null)
					criteria.add(Restrictions.le("createdDateTime",appliedfDate));*/
				
				if(appliedfDate!=null && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
				else if(appliedfDate!=null && appliedtDate==null)
					criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
				else if(appliedtDate!=null && appliedfDate==null)
					criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				
				if(teacherFlag && filterTeacherList!=null && filterTeacherList.size() >0)
					criteria.add(Restrictions.in("teacherId.teacherId", filterTeacherList));	
				
				if(nByAflag && NbyATeacherList!=null &&NbyATeacherList.size()>0)
					criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", NbyATeacherList)));	
				
				if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
					if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
					{
						criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
						.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
						.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
					}
					criteria.setProjection(Projections.property("teacherId.teacherId")); 
					criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
					lstTeacherDetail =  criteria.list();
				}
				System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstTeacherDetail;
		}

		
		// Optimization for candidate pool
		
		@Transactional(readOnly=false)
		public List<Integer> getTeacherDetailByDASARecordsL1_Op(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<Integer> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<Integer> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,StatusMaster statusMaster,List<Integer> lstTeacherDetailAllFilter)
		{
			PrintOnConsole.getJFTPrint("JFT:74");	
			List<Integer> lstTeacherDetail= new ArrayList<Integer>();
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());

			
			try{
				if(Utility.getValueOfPropByKey("basePath").contains("platform")){
					//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
					JobOrder jobOrder = new JobOrder();
					 jobOrder.setJobId(11136);
					 
					if(jobOrder!=null)
					criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			String[] candidatetyp=null;
			if(internalCandVal!=null){
					candidatetyp=internalCandVal.split("|");
			}
			List<Integer> candidatetypec=new ArrayList<Integer>();
			if(candidatetyp!=null){
				for(String s:candidatetyp){
					if(s.equalsIgnoreCase("0")){
						candidatetypec.add(1);
						candidatetypec.add(5);
					}else{
						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
							int val=Integer.parseInt(s);
							candidatetypec.add(val);
						}
						
					}
				}
			}
			System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
			if(candidatetypec.size()>0){
				criteria.add(Restrictions.in("isAffilated",candidatetypec));
			}
			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}*/
			boolean flag=true;
				boolean dateDistrictFlag=false;
				
				if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
				{
					Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
					criteria.add(criterion1);
				}
				
				if((entityID==3 || entityID==4)){
					if(lstJobOrder.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
						criteria.add(criterion1);
					}
					if(entityID==4 && dateFlag==false){
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
					}else if(entityID==4 && dateFlag){
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						dateDistrictFlag=true;
						if(fDate!=null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
						}else if(fDate!=null && tDate==null){
							criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
						}else if(fDate==null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
						}
					}
				}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(dateFlag && districtMaster!=null &&  entityID==2){
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
					dateDistrictFlag=true;
				}
				if(dateDistrictFlag==false && dateFlag){
					if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}
				}
				if(status)
					criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
				else
					criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
				
				//job applied flag
				
				if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
					criteria.add(Restrictions.le("createdDateTime",appliedfDate));
				else if(daysVal==5 && appliedtDate!=null)
					criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
				else if(appliedfDate!=null)
					criteria.add(Restrictions.le("createdDateTime",appliedfDate));
				
				filterTeacherList.addAll(lstTeacherDetailAllFilter);
				
				//if(teacherFlag && filterTeacherList!=null && filterTeacherList.size() >0)
				if(filterTeacherList!=null && filterTeacherList.size() >0)
					criteria.add(Restrictions.in("teacherId.teacherId", filterTeacherList));	
				
				if(nByAflag && NbyATeacherList!=null &&NbyATeacherList.size()>0)
					criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", NbyATeacherList)));	
				
				if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
					if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
					{
						criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
						.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
						.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
					}
					criteria.setProjection(Projections.property("teacherId.teacherId")); 
					criteria.setProjection(Projections.distinct(Projections.property("teacherId.teacherId")));
					lstTeacherDetail =  criteria.list();
				}
				System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstTeacherDetail;
		}

		//Optimization for candidate pool search
		 @Transactional(readOnly=false)
			public List<TeacherDetail> getTeacherDetailByDASARecordsDataOp_Op(Order sortOrderStrVal,int start,int end,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<Integer> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<Integer> lstTeacherDetailAllFilter,boolean nByAflag,List<Integer> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,boolean normScoreFlag,List<StatusMaster> lstStatusMastersNew)
			{

				
				PrintOnConsole.getJFTPrint("JFT:71");
				
				List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
				try{
					Session session = getSession();
					Criteria criteria = session.createCriteria(getPersistentClass());
			
					try{
						if(Utility.getValueOfPropByKey("basePath").contains("platform")){
							//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
							JobOrder jobOrder = new JobOrder();
							 jobOrder.setJobId(11136);
							 
							if(jobOrder!=null)
							criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					System.out.println("inside 1");
			String[] candidatetyp=null;
			if(internalCandVal!=null){
					candidatetyp=internalCandVal.split("|");
			}
			
			List<Integer> candidatetypec=new ArrayList<Integer>();
			if(candidatetyp!=null){
				for(String s:candidatetyp){
					if(s.equalsIgnoreCase("0")){
						candidatetypec.add(1);
						candidatetypec.add(5);
					}else{
						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
							int val=Integer.parseInt(s);
							candidatetypec.add(val);
						}
						
					}
				}
			}
			System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
			if(candidatetypec.size()>0){
				criteria.add(Restrictions.in("isAffilated",candidatetypec));
			}
			
			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
			if(internalCandVal==1){
				criteria.add(criterionInterCand);
			}*/
			System.out.println("inside 2");
					Criterion criterionSchool =null;
					if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
						criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
					}
					
					criteria.add(Restrictions.ne("status",statusMaster));
					if(lstStatusMastersNew.size()>0)
						criteria.add(Restrictions.in("status",lstStatusMastersNew));
					
					
					if(fDate!=null && tDate!=null)
						criteria.add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					else if(fDate!=null && tDate==null)
						criteria.add(Restrictions.ge("createdDateTime",fDate));
					else if(tDate!=null && fDate==null)
						criteria.add(Restrictions.le("createdDateTime",tDate));
					

					boolean flag=false;
					boolean dateDistrictFlag=false;
					if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
						criteria.add(criterion1);
					}
					System.out.println("inside 3");
					if((entityID==3 || entityID==4)){
						flag=true;
						if(lstJobOrder.size()>0)
						{
							Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
							criteria.add(criterion1);
						}
						if(entityID==4 && dateFlag==false){
							criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						}else if(entityID==4 && dateFlag){
							dateDistrictFlag=true;
							/*if(fDate!=null && tDate!=null){
								criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
							}else if(fDate!=null && tDate==null){
								criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
							}else if(fDate==null && tDate!=null){
								criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
							}*/
							if(lstJobOP!=null && lstJobOP.size()>0)
							{
								Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
								criteria.add(criterion1);
							}
						}
					}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
						flag=true;
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
					}else if(dateFlag && districtMaster!=null &&  entityID==2){
						flag=true;
						/*if(fDate!=null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
						}else if(fDate!=null && tDate==null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
						}else if(fDate==null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
						}*/
						
						if(lstJobOP!=null && lstJobOP.size()>0)
						{
							Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
							criteria.add(criterion1);
						}
						
						dateDistrictFlag=true;
					}
					else if(entityID==5)
					{

						System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
						flag=true;
						if(branchMaster!=null)
							criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
						else
							criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));

							}
							else if(entityID==6)
							{
								System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
								flag=true;
								criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
							}
							if(dateDistrictFlag==false && dateFlag)
							{
								/*if(fDate!=null && tDate!=null){
									criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
								}else if(fDate!=null && tDate==null){
									criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
								}else if(fDate==null && tDate!=null){
									criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
								}*/
								
								if(lstJobOP!=null && lstJobOP.size()>0)
								{
									Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
									criteria.add(criterion1);
								}
								
								flag=true;
							}
							System.out.println("inside 6");
							if(status){
								criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
								if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
									criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
							}
							System.out.println("inside 7");
							//job applied flag
							if(newcandidatesonlyNew)
							{
								if(appliedfDate!=null && appliedtDate!=null)
								{
									System.out.println("############### newcandidatesonlyNew FT #######################");
									criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
								}
								else if(appliedfDate!=null && appliedtDate==null)
								{
									System.out.println("############### newcandidatesonlyNew F #######################");
									//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
									criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
								}
								else if(appliedtDate!=null && appliedfDate==null)
								{
									System.out.println("############### newcandidatesonlyNew T #######################");
									criteria.add(Restrictions.le("createdDateTime",appliedtDate));
								}
							}
							else
							{
								System.out.println("inside 8");
								if(appliedfDate!=null && appliedtDate!=null)
									criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
								else if(appliedfDate!=null && appliedtDate==null)
									criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
								else if(appliedtDate!=null && appliedfDate==null)
									criteria.add(Restrictions.le("createdDateTime",appliedtDate));
								System.out.println("inside 9");
							}
							
							flag=true;
							if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
							{
								criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", lstTeacherDetailAllFilter))); // filter first time
							}
							System.out.println("inside 10");
							if(teacherFlag){
								flag=true;
								criteria.add(Restrictions.in("teacherId.teacherId", filterTeacherList));	
							}
							if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size() >0){
								criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", NbyATeacherList)));	
							}
							System.out.println("inside 11");
							if(districtMaster!=null  && utype==3)
							{
								StatusMaster master = districtMaster.getStatusMaster();
								SecondaryStatus smaster = districtMaster.getSecondaryStatus();
								if(master!=null)
								{
									System.out.println("master: "+master.getStatus());
									//String sts = master.getStatus();
									String sts = master.getStatusShortName();
									if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
									{
										List<StatusMaster> sList = new ArrayList<StatusMaster>();
										sList.add(master);
										if(sts.equals("scomp"))
										{
											StatusMaster st1 = new StatusMaster();
											st1.setStatusId(17);
											sList.add(st1);
											
											StatusMaster st2 = new StatusMaster();
											st2.setStatusId(18);
											sList.add(st2);
											
										}else if(sts.equals("vcomp"))
										{
											StatusMaster st2 = new StatusMaster();
											st2.setStatusId(18);
											sList.add(st2);
										}
										
										/*
										6 - Hired
										19 - Declined
										10 - rejected
										7 -- Withdrew
										*/
										
										StatusMaster stHird = new StatusMaster();
										stHird.setStatusId(6);
										sList.add(stHird);

								StatusMaster stDeclined = new StatusMaster();
								stDeclined.setStatusId(19);
								sList.add(stDeclined);
								
								StatusMaster stRejected = new StatusMaster();
								stRejected.setStatusId(10);
								sList.add(stRejected);
								
								StatusMaster stWithdrew = new StatusMaster();
								stWithdrew.setStatusId(7);
								sList.add(stWithdrew);
								
								criteria.add(Restrictions.in("status",sList));
							}else
							{
								List<StatusMaster> sList = new ArrayList<StatusMaster>();
								sList.add(master);
								if(sts.equals("hird"))
								{
									StatusMaster stDeclined = new StatusMaster();
									stDeclined.setStatusId(19);
									sList.add(stDeclined);
									
									StatusMaster stRejected = new StatusMaster();
									stRejected.setStatusId(10);
									sList.add(stRejected);
									
									StatusMaster stWithdrew = new StatusMaster();
									stWithdrew.setStatusId(7);
									sList.add(stWithdrew);
								}else if(sts.equals("dcln"))
								{
									StatusMaster stRejected = new StatusMaster();
									stRejected.setStatusId(10);
									sList.add(stRejected);
									
									StatusMaster stWithdrew = new StatusMaster();
									stWithdrew.setStatusId(7);
									sList.add(stWithdrew);

								}else if(sts.equals("rem"))
								{
									StatusMaster stWithdrew = new StatusMaster();
									stWithdrew.setStatusId(7);
									sList.add(stWithdrew);
								}
								criteria.add(Restrictions.in("status",sList));
								//criteria.add(Restrictions.eq("status",master));
							}
						}
						if(smaster!=null)
						{
							System.out.println("smaster: "+smaster.getSecondaryStatusName());
							Criteria c = criteria.createCriteria("secondaryStatus");
							
							Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
							if(!stq.equals(""))
							{
								Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
								Criterion c3 = Restrictions.or(c2, criterion1);
								c.add(c3);
							}else
								c.add(c2);
								}
							}
							System.out.println("inside 12  ");
							if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
								if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
								{
									criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
									.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
									.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
									 
								}
								criteria.setProjection(Projections.groupProperty("teacherId"));
								if(!normScoreFlag){
									criteria.setFirstResult(start);
									criteria.setMaxResults(end);	
								}
								
						try {
							criteria.setFetchMode("teacherId", FetchMode.SELECT);
							criteria.setFetchMode("jobId", FetchMode.SELECT);
							criteria.setFetchMode("status", FetchMode.SELECT);
							criteria.setFetchMode("updatedBy", FetchMode.SELECT);					
							criteria.setFetchMode("districtSpecificNoteFinalizeONBBy", FetchMode.SELECT);
							criteria.setFetchMode("userMaster", FetchMode.SELECT);
							criteria.setFetchMode("statusMaster", FetchMode.SELECT);
							criteria.setFetchMode("secondaryStatus", FetchMode.SELECT);
							criteria.setFetchMode("internalStatus", FetchMode.SELECT);
							criteria.setFetchMode("internalSecondaryStatus", FetchMode.SELECT);
							criteria.setFetchMode("schoolMaster", FetchMode.SELECT);
							
						
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("13");
						
						lstTeacherDetail =  criteria.list();
					}else{
						lstTeacherDetail=new ArrayList<TeacherDetail>();
					}
					System.out.println("inside 15");
					if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
						criteria.add(criterionSchool);
					}
				} 
				catch (Exception e) {
					e.printStackTrace();
				}		
				return lstTeacherDetail;
			
}   
		    
		 // Optimization for candidate pool search
		 
    	 @Transactional(readOnly=false)
    		public List<Integer> getTeacherDetailByDASARecordsCountOp_Op(Order sortOrderStrVal,int entityID,UserMaster userMaster,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<Integer> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<Integer> lstTeacherDetailAllFilter,boolean nByAflag,List<Integer> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster,StatusMaster statusMaster,boolean newcandidatesonlyNew,List<JobOrder> lstJobOP,List<StatusMaster> lstStatusMastersNew)
    		{
 			PrintOnConsole.getJFTPrint("JFT:72");	
			TestTool.getTraceTime("500");
			List<Integer> lstTeacherDetail= new ArrayList<Integer>();
			try{
				Session session = getSession();
				Criteria criteria = session.createCriteria(getPersistentClass());
    				
				try{
					if(Utility.getValueOfPropByKey("basePath").contains("platform")){
						//JobOrder jobOrder=jobOrderDAO.findById(11136,false,false);
						JobOrder jobOrder = new JobOrder();
						 jobOrder.setJobId(11136);
						 
						if(jobOrder!=null)
						criteria.add(Restrictions.not(Restrictions.eq("jobId",jobOrder)));
					}
				}catch(Exception e){
					e.printStackTrace();
				}
    				String[] candidatetyp=null;
    				if(internalCandVal!=null){
    						candidatetyp=internalCandVal.split("|");
    				}
    				
    				List<Integer> candidatetypec=new ArrayList<Integer>();
    				if(candidatetyp!=null){
    					for(String s:candidatetyp){
    						if(s.equalsIgnoreCase("0")){
    							candidatetypec.add(1);
    							candidatetypec.add(5);
    						}else{
    							if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
    								int val=Integer.parseInt(s);
    								candidatetypec.add(val);
    							}
    							
    						}
    					}
    				}
    				System.out.println("size of the candidatetype String array>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  "+candidatetypec.size());
    				if(candidatetypec.size()>0){
    					criteria.add(Restrictions.in("isAffilated",candidatetypec));
    				}
    				
    				
    				
    				/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
    				if(internalCandVal==1){
    					TestTool.getTraceTime("501");
    					criteria.add(criterionInterCand);
    				}*/

    				criteria.add(Restrictions.ne("status",statusMaster));
				if(lstStatusMastersNew.size()>0)
					criteria.add(Restrictions.in("status",lstStatusMastersNew));
				
				Criterion criterionSchool =null;
				if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
					criterionSchool = Restrictions.eq("schoolMaster",userMaster.getSchoolId());
				}
				
				boolean flag=false;
				boolean dateDistrictFlag=false;
				if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
				{
					TestTool.getTraceTime("502");
					Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
					criteria.add(criterion1);
				}
				
				if((entityID==3 || entityID==4)){
					TestTool.getTraceTime("503");
					flag=true;
					if(lstJobOrder.size()>0)
					{
						TestTool.getTraceTime("504");
						Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
						criteria.add(criterion1);
					}
					if(entityID==4 && dateFlag==false){
						TestTool.getTraceTime("505");
						criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
						//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
					}else if(entityID==4 && dateFlag){
						TestTool.getTraceTime("506");
						dateDistrictFlag=true;
						/*if(fDate!=null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
						}else if(fDate!=null && tDate==null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
						}else if(fDate==null && tDate!=null){
							criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
						}*/
						
						if(lstJobOP!=null && lstJobOP.size()>0)
						{
							Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
							criteria.add(criterion1);
						}
						
					}
				}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
					TestTool.getTraceTime("507");
					flag=true;
					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
				}else if(dateFlag && districtMaster!=null &&  entityID==2){
					TestTool.getTraceTime("508");
					flag=true;
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster)).add(Restrictions.le("createdDateTime",tDate));
					}*/
					
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
					
					dateDistrictFlag=true;
				}
				else if(entityID==5)
				{
					System.out.println("::::::::::::::::::::::::::::::::headQuarterMaster::::::"+headQuarterMaster.getHeadQuarterId());
					flag=true;
					criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
				}
				else if(entityID==6)
				{
					System.out.println("::::::::::::::::::::::::::::::::branchMaster::::::"+branchMaster.getBranchId());
					flag=true;
					criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));
				}
				if(dateDistrictFlag==false && dateFlag){
					TestTool.getTraceTime("509");
					/*if(fDate!=null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
					}else if(fDate!=null && tDate==null){
						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
					}else if(fDate==null && tDate!=null){
						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
					}*/
					
					if(lstJobOP!=null && lstJobOP.size()>0)
					{
						Criterion criterion1 = Restrictions.in("jobId",lstJobOP);
						criteria.add(criterion1);
					}
					
					flag=true;
				}
				if(status){
					TestTool.getTraceTime("510");
					criteria.createCriteria("status").add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
					if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
						criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
				}
				
				//job applied flag
				//job applied flag
				if(newcandidatesonlyNew)
				{
					if(appliedfDate!=null && appliedtDate!=null)
					{
						System.out.println("############### newcandidatesonlyNew Count FT #######################");
						criteria.add(Restrictions.between("createdDateTime", appliedfDate, appliedtDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
					}
					else if(appliedfDate!=null && appliedtDate==null)
					{
						System.out.println("############### newcandidatesonlyNew Count F #######################");
						//criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
						criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.not(Restrictions.lt("createdDateTime", appliedfDate)));
					}
					else if(appliedtDate!=null && appliedfDate==null)
					{
						System.out.println("############### newcandidatesonlyNew Count T #######################");
						criteria.add(Restrictions.le("createdDateTime",appliedtDate));
					}
				}
				else
				{
					if(appliedfDate!=null && appliedtDate!=null)
						criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
					else if(appliedfDate!=null && appliedtDate==null)
						criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
					else if(appliedtDate!=null && appliedfDate==null)
						criteria.add(Restrictions.le("createdDateTime",appliedtDate));
				}
				
				
				flag=true;
				if(lstTeacherDetailAllFilter!=null && lstTeacherDetailAllFilter.size()>0)
				{
					TestTool.getTraceTime("511 T");
					criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", lstTeacherDetailAllFilter)));
				}

				if(teacherFlag){
					if(filterTeacherList!=null && filterTeacherList.size() >0)
					{
						TestTool.getTraceTime("512 T");
						flag=true; //
						criteria.add(Restrictions.in("teacherId.teacherId", filterTeacherList)); //
					}
				}
				if(nByAflag && NbyATeacherList!=null && NbyATeacherList.size()>0) //
				{
					TestTool.getTraceTime("513 T");
					criteria.add(Restrictions.not(Restrictions.in("teacherId.teacherId", NbyATeacherList)));
				}
				
				if(districtMaster!=null  && utype==3)
				{
					TestTool.getTraceTime("514");
					StatusMaster master = districtMaster.getStatusMaster();
					SecondaryStatus smaster = districtMaster.getSecondaryStatus();
					if(master!=null)
					{
						TestTool.getTraceTime("515");
						System.out.println("master: "+master.getStatus());
						String sts = master.getStatusShortName();
						if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
						{
							List<StatusMaster> sList = new ArrayList<StatusMaster>();
							sList.add(master);
							if(sts.equals("scomp"))
							{
								StatusMaster st1 = new StatusMaster();
								st1.setStatusId(17);
								sList.add(st1);
								
								StatusMaster st2 = new StatusMaster();
								st2.setStatusId(18);
								sList.add(st2);
								
							}else if(sts.equals("vcomp"))
							{
								StatusMaster st2 = new StatusMaster();
								st2.setStatusId(18);
								sList.add(st2);
							}
							
							/*
							6 - Hired
							19 - Declined
							10 - rejected
							7 -- Withdrew
							*/
							
							StatusMaster stHird = new StatusMaster();
							stHird.setStatusId(6);
							sList.add(stHird);

							StatusMaster stDeclined = new StatusMaster();
							stDeclined.setStatusId(19);
							sList.add(stDeclined);
							
							StatusMaster stRejected = new StatusMaster();
							stRejected.setStatusId(10);
							sList.add(stRejected);
							
							StatusMaster stWithdrew = new StatusMaster();
							stWithdrew.setStatusId(7);
							sList.add(stWithdrew);
							
							criteria.add(Restrictions.in("status",sList));
						}else
						{
							List<StatusMaster> sList = new ArrayList<StatusMaster>();
							sList.add(master);
							if(sts.equals("hird"))
							{
								StatusMaster stDeclined = new StatusMaster();
								stDeclined.setStatusId(19);
								sList.add(stDeclined);
								
								StatusMaster stRejected = new StatusMaster();
								stRejected.setStatusId(10);
								sList.add(stRejected);
								
								StatusMaster stWithdrew = new StatusMaster();
								stWithdrew.setStatusId(7);
								sList.add(stWithdrew);
							}else if(sts.equals("dcln"))
							{
								StatusMaster stRejected = new StatusMaster();
								stRejected.setStatusId(10);
								sList.add(stRejected);
								
								StatusMaster stWithdrew = new StatusMaster();
								stWithdrew.setStatusId(7);
								sList.add(stWithdrew);

							}else if(sts.equals("rem"))
							{
								StatusMaster stWithdrew = new StatusMaster();
								stWithdrew.setStatusId(7);
								sList.add(stWithdrew);
							}
							criteria.add(Restrictions.in("status",sList));
						}
					}
					if(smaster!=null)
					{
						System.out.println("smaster: "+smaster.getSecondaryStatusName());
						Criteria c = criteria.createCriteria("secondaryStatus");
						
						Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
						if(!stq.equals(""))
						{
							Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
							Criterion c3 = Restrictions.or(c2, criterion1);
							c.add(c3);
						}else
							c.add(c2);
					}
				}
				System.out.println("*****************************************************");
				if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
					TestTool.getTraceTime("516");
					if(!firstName.trim().equals("") || !lastName.trim().equals("") || !emailAddress.trim().equals(""))
					{
						TestTool.getTraceTime("517");
						criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
						.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
						.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
					}				
					//criteria.setProjection(Projections.countDistinct("teacherId"));
					String countRow= " count(distinct this_.teacherId)";
					criteria.setProjection( Projections.projectionList().add(Projections.sqlProjection(""+countRow+" as countRow", new String[]{"countRow"}, new Type[]{ Hibernate.INTEGER}),"countRow"));
					lstTeacherDetail =  criteria.list();
					
					
					TestTool.getTraceTime("518 lstTeacherDetail "+lstTeacherDetail.size());
				}
				if(userMaster != null && (userMaster.getEntityType() == 3 || userMaster.getEntityType() == 6) && (districtMaster!=null && districtMaster.getDistrictId() == 1200390)){
					criteria.add(criterionSchool);
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			TestTool.getTraceTime("599");
			return lstTeacherDetail;
		}

    	@Transactional(readOnly=false)
 		public int noOfHiresSql(JobOrder jobOrder, List<StatusMaster> lstStatusMasters)
 		{
 			int  noOfHires=0;
 			
 			try{
 				
 				Session session = getSessionFactory().openSession(); 
 				Criteria criteria = session.createCriteria(getPersistentClass())
 				.setProjection( Projections.projectionList()
 	    	          .add( Projections.count("jobForTeacherId"), "jobForTeacherId" )    	        
 	    	      );			
 				criteria.add(Restrictions.eq("jobId",jobOrder));
 				criteria.add(Restrictions.in("status", lstStatusMasters)); 				
 				noOfHires=(Integer)criteria.list().get(0);				
 			} 
 			catch (Exception e) {
 				e.printStackTrace();
 			}		
 			return noOfHires;
 		}
    	
    	@Transactional(readOnly=false)
		@SuppressWarnings("unchecked")
		public int checkCandidatesNotReviewed(Integer districtId,Integer jobId,Integer teacherId)
		{
			System.out.println("::: checkCandidatesNotReviewed :::");
			Session session = getSession();
			String sql = "";
			Connection connection = null;
			
			try{
				 SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
				 ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
				 connection = connectionProvider.getConnection();
				 
				 sql="select count(jft.jobForTeacherId) numberOfRecord " +
				 	 " from jobforteacher jft left join statusmaster sm " +
				 	 " on sm.statusid=jft.status " +
				 	 " left join teachersecondarystatus tss on jft.teacherid=tss.teacherid " +
				 	 " and jft.jobid=tss.jobid and jft.districtid=tss.districtid " +
				 	 " left join secondarystatusmaster ssm on ssm.secstatusid=tss.secStatusId " +
				 	 " left join secondarystatus ss on ss.secondarystatusId=jft.displaysecondarystatusid " +
				 	 " left join joborder jo on jo.jobid=jft.jobid" +
				 	 " left join jobcategorymaster jcm on jo.jobcategoryid=jcm.jobcategoryid " +
				 	 " left join districtmaster dm on dm.districtid=jft.districtid " +
				 	 " left join democlassschedule dcs on dcs.teacherid=jft.teacherid and dcs.jobid=jft.jobid " +
				 	 " left join panelschedule ps on ps.teacherid=jft.teacherid and ps.jobid=jft.jobid " +
				 	 " left join teacherphonecallhistory tpch on tpch.teacherid=jft.teacherid and tpch.jobid=jft.jobid " +
				 	 " left join onlineactivitynotes oan on oan.teacherid=jft.teacherid and oan.jobid=jft.jobid " +
				 	 " left join referencenotes rn on rn.teacherid=jft.teacherid and rn.jobid=jft.jobid " +
				 	 " left join teacherstatusnotes tsn on tsn.teacherid=jft.teacherid and tsn.jobid=jft.jobid " +
				 	 " left join jsicommunicationlog jcl on jcl.teacherid=jft.teacherid and jcl.jobid=jft.jobid " +
				 	 " left join jobforteacher hired on hired.teacherid=jft.teacherid and hired.status=6 " +
				 	 " where jft.status=4 " +
				 	 " and jft.lastactivity is null " +
				 	 " and ssm.secstatusname is null " +
				 	 " and jft.displaysecondarystatusid is null" +
				 	 " and dcs.demoid is null " +
				 	 " and ps.panelid is null " +
				 	 " and tpch.teacherphonecallid is null " +
				 	 " and tpch.teacherid is null " +
				 	 " and oan.teacherid is null " +
				 	 " and rn.teacherid is null " +
				 	 " and tsn.teacherid is null " +
				 	 " and jcl.teacherid is null " +
				 	 " and hired.teacherid is null " +
				 	 " and jft.districtid= ? " +
				 	 " and jft.jobId= ? " +
				 	 " and jft.teacherId= ? " +
				 	 " ";
				 	//System.out.println(sql);
					PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);
					if(districtId!=null){
						ps.setString(1, districtId.toString());
					}
					ps.setInt(2, jobId );
					ps.setInt(3, teacherId );
				 
					ResultSet rs=ps.executeQuery();
					
					int rowcount = 0;
					while (rs.next()) {
						rowcount = rs.getInt("numberOfRecord");
			        }
				 return rowcount;
				
			}catch(Exception exception){
				exception.printStackTrace();
			} finally{
				if(connection!=null)
					try {
						connection.close();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			}
			return 0;
		}
    	
    	@Transactional(readOnly=false)
    	public List findRecordForNotReviewdCandidate(List<TeacherDetail> teacherDetailsist)
    	{
    		List lstJobForTeacher= new ArrayList();
    		try 
    		{
    			Session session = getSession();
    			Criterion criterion = Restrictions.in("teacherId", teacherDetailsist);		
    			Criteria c= session.createCriteria(getPersistentClass());

    			c.add(criterion).setProjection( Projections.projectionList()
    					.add(Projections.property("teacherId.teacherId"))
    					.add(Projections.property("notReviewedFlag"))
    					.add(Projections.property("lastActivity"))
    					.add(Projections.property("secondaryStatus.secondaryStatusId")));
    		
    			lstJobForTeacher = c.list();
    			
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobForTeacher;
    	}

    	@Transactional(readOnly=false)
    	public List<JobForTeacher> findAllJobForTeacherByDistrictIdONROP(List<Long> jftIds,List<StatusMaster> lstStatusMasters,DistrictMaster districtMaster,TeacherDetail teacherDetail,boolean ssnFlag,String teacherFNames,String teacherLNames,String position,Order order,int startPos,int limit,ArrayList<Integer> listJobOrderIds)
    	{

    		PrintOnConsole.getJFTPrint("JFT:39xxxxxxxxxxxxxxxxxxx");
    		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    		
    		List<String []> lstJobForTeacherTemp=new ArrayList<String []>();
    		try 
    		{
    			Session session 		= 	getSession();
    			Criteria criteria = getSession().createCriteria(this.getPersistentClass());
    			  			
    			
    			criteria.createAlias("jobId", "jo").createAlias("teacherId", "td").createAlias("jo.districtMaster", "dm").createAlias("jo.jobCategoryMaster", "jobCategoryMaster")
    								.createAlias("secondaryStatus", "secondaryStatus",Criteria.LEFT_JOIN).createAlias("status", "statusmaster")
    					.setProjection( Projections.projectionList()
    							.add( Projections.property("jobForTeacherId"), "jobForTeacherId1" )    					
    					.add( Projections.property("statusmaster.statusId"), "statusId" )
    					.add( Projections.property("requisitionNumber"), "requisitionNumber1" )
    					.add( Projections.property("createdDateTime"),"createdDateTime132")    					
    					.add( Projections.property("offerAcceptedDate"), "offerAcceptedDate" )
			    		.add( Projections.property("dm.districtName"), "districtName" )
			    		.add( Projections.property("dm.districtId"), "districtId1" )
			    		.add( Projections.property("td.teacherId"), "teacherId" )
				        .add( Projections.property("td.firstName"), "firstName" )
				        .add( Projections.property("td.lastName"), "lastName" )
				        .add( Projections.property("td.emailAddress"), "emailAddress" )			        
				        .add( Projections.property("jo.jobTitle"), "jobTitle" )			       	
				        .add( Projections.property("jo.status"), "status2" )
				        .add( Projections.property("jo.jobId"), "jobId11" )
				        .add( Projections.property("secondaryStatus.secondaryStatusId"), "secondaryStatusId" )
				        .add( Projections.property("jobCategoryMaster.jobCategoryId"), "jobCategoryId" )
				        .add( Projections.property("jobCategoryMaster.jobCategoryName"), "jobCategoryName" )
				        .add( Projections.property("td.createdDateTime"), "teacherCreatedDateTime" )
				        .add( Projections.property("statusmaster.statusShortName"), "statusShortName" )
				        .add( Projections.property("isAffilated"), "isAffilated" )
    					);    			    			
    			
    			criteria.add(Restrictions.or(Restrictions.isNull("secondaryStatus"), Restrictions.isNotNull("secondaryStatus")));
    			if(jftIds.size()>0){
    				criteria.add(Restrictions.not(Restrictions.in("jobForTeacherId",jftIds)));
    			}
    			if(districtMaster!=null && districtMaster.getDistrictId()!=806900 && !(districtMaster.getHeadQuarterMaster()!=null && districtMaster.getHeadQuarterMaster().getHeadQuarterId().equals(2)))
    			criteria.add(Restrictions.isNotNull("requisitionNumber"));
    			criteria.add(Restrictions.in("status",lstStatusMasters));
    			criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    			 if(listJobOrderIds!=null && listJobOrderIds.size()>0)
    		         criteria.add(Restrictions.in("jo.jobId", listJobOrderIds));
    			//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster",districtMaster));
    			
    			if(teacherDetail!=null || ssnFlag){
    				System.out.println("=============================  check ssn record  ====================");
    				criteria.add(Restrictions.eq("td.teacherId",teacherDetail.getTeacherId()));
    			}
    			if((teacherFNames!= null && !teacherFNames.equals(""))&&(teacherLNames!= null && !teacherLNames.equals(""))){
    				criteria.add(Restrictions.like("td.firstName","%"+teacherFNames+"%")).add(Restrictions.like("td.lastName","%"+teacherLNames+"%"));
    			}else if(teacherFNames!= null && !teacherFNames.equals("")){
    				criteria.add(Restrictions.like("td.firstName","%"+teacherFNames+"%"));
    			}else if(teacherLNames!= null && !teacherLNames.equals("")){
    				criteria.add(Restrictions.like("td.lastName","%"+teacherLNames+"%"));
    			}	
    			if(position!= null && !position.equals("")){
    				criteria.add(Restrictions.like("requisitionNumber","%"+position+"%"));
    			}	
    				
    			criteria.addOrder(order);
    			criteria.addOrder(Order.asc("createdDateTime"));
    			
    			lstJobForTeacherTemp = criteria.list();
    			
    			int time=0;
    			for (Iterator it = lstJobForTeacherTemp.iterator(); it.hasNext();)
				{
    				Object[] row = (Object[]) it.next();
    				time++;
    				 if(row[0]!=null)
 		            {
    					 JobForTeacher tempJObForTeacher = new JobForTeacher();
    					 
    					 SecondaryStatus secondaryStatus = new SecondaryStatus();
    					 if(row[14]!=null)
    						 secondaryStatus.setSecondaryStatusId(Integer.parseInt(row[14].toString()));
    					 tempJObForTeacher.setSecondaryStatus(secondaryStatus);
    					 StatusMaster statusMaster = new StatusMaster();
    					 if(row[1]!=null)
    					 	statusMaster.setStatusId(Integer.parseInt(row[1].toString()));
    					 	statusMaster.setStatusShortName(row[18].toString());
    					 tempJObForTeacher.setStatus(statusMaster);
    					 if(row[2]!=null)
    					 tempJObForTeacher.setRequisitionNumber(row[2].toString());
    					 
    					 Date createdDateTime=null;
    					 if(row[3]!=null)    					 
    					 createdDateTime =   (Date)row[3];    					 
    					 tempJObForTeacher.setCreatedDateTime(createdDateTime);
    					 
    					 Date offerAcceptedDate =null;
    					 if(row[4]!=null)
    					 offerAcceptedDate = (Date)row[4];
    					 tempJObForTeacher.setOfferAcceptedDate(offerAcceptedDate);
    					 
    					 DistrictMaster districtMaster2 = new DistrictMaster();
    					 if(row[6]!=null)
    					 districtMaster2.setDistrictId(Integer.parseInt(row[6].toString()));
    					 if(row[6]!=null)
    					 tempJObForTeacher.setDistrictId(Integer.parseInt(row[6].toString()));
    					 TeacherDetail teacherDetail2 = new TeacherDetail();
    					 teacherDetail2.setTeacherId(Integer.parseInt(row[7].toString()));
    					 teacherDetail2.setFirstName(row[8].toString());
    					 teacherDetail2.setLastName(row[9].toString());
    					 teacherDetail2.setEmailAddress(row[10].toString());
    					 Date teacherCreatedDateTime=null;
    					 if(row[17]!=null)    					 
    						 teacherCreatedDateTime =   (Date)row[17];
    					 teacherDetail2.setCreatedDateTime(teacherCreatedDateTime);
    					 
    					 tempJObForTeacher.setTeacherId(teacherDetail2);
    					 JobOrder jobOrder = new JobOrder();
    					 JobCategoryMaster jobCategoryMaster = new JobCategoryMaster();
    					 jobCategoryMaster.setJobCategoryId(Integer.parseInt(row[15].toString()));
    					 jobCategoryMaster.setJobCategoryName(row[16].toString());
    					 jobOrder.setJobCategoryMaster(jobCategoryMaster);
    					 jobOrder.setDistrictMaster(districtMaster2);
    					 jobOrder.setJobTitle(row[11].toString());
    					 jobOrder.setStatus(row[12].toString());
    					 jobOrder.setJobId(Integer.parseInt(row[13].toString()));
    					 tempJObForTeacher.setJobId(jobOrder);
    					 tempJObForTeacher.setJobForTeacherId(Long.parseLong(row[0].toString()));
    					 tempJObForTeacher.setIsAffilated(Integer.parseInt(row[19].toString()));
    					 lstJobForTeacher.add(tempJObForTeacher);
    					 
    					// System.out.println(tempJObForTeacher);
 		            }
				}
    			
    			//System.out.println("lstJobForTeacher re   "+lstJobForTeacher.size()+"    time   "+time);
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobForTeacher;

    	}
    	
    	@Transactional(readOnly=false)
    	public List<JobForTeacher> findJobByTeacherAndSatusFormJobIdOp(TeacherDetail teacherDetail, JobOrder jobOrder)
    	{
    		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    		List<JobForTeacher> lstJobForTeacherFinal= null;
    		List<String []> lstJobForTeacherTemp=new ArrayList<String []>();
    		try 
    		{	    			
    			Session session 		= 	getSession();   		
    			
    			Criteria cr = session.createCriteria(JobForTeacher.class);
    			cr.createAlias("jobId", "jo").createAlias("teacherId", "td").createAlias("jo.districtMaster", "dm",Criteria.LEFT_JOIN)
    			//.createAlias("jo.headQuarterMaster", "hm",Criteria.LEFT_JOIN).createAlias("jo.branchMaster", "bm",Criteria.LEFT_JOIN)
    			//.createAlias("jo.jobCategoryMaster", "jobCategoryMaster",Criteria.LEFT_JOIN)
    			.createAlias("secondaryStatus", "secondaryStatus",Criteria.LEFT_JOIN).createAlias("status", "statusmaster")
    		    .setProjection(Projections.projectionList()
    		    		.add( Projections.property("jobForTeacherId"), "jobForTeacherId" )
    					.add( Projections.property("offerReady"), "offerReady" )    					
    					.add( Projections.property("offerAccepted"), "offerAccepted" )    					
    					.add( Projections.property("requisitionNumber"), "requisitionNumber" )
    					.add( Projections.property("isAffilated"), "isAffilated" )
    					.add( Projections.property("statusmaster.statusId"), "statusId" )
    					.add( Projections.property("statusmaster.statusShortName"), "statusShortName" )
						.add( Projections.property("dm.districtId"), "districtId1" )						        
						.add( Projections.property("secondaryStatus.secondaryStatusId"), "secondaryStatusId" )
						
					
    		      );
    			cr.add(Restrictions.eq("td.teacherId",teacherDetail.getTeacherId())).add(Restrictions.eq("jo.jobId",jobOrder.getJobId()));
    			lstJobForTeacherTemp = cr.list();
    			System.out.println("lstJobForTeacherTemp   "+lstJobForTeacherTemp.size());
    			for (Iterator it = lstJobForTeacherTemp.iterator(); it.hasNext();)
				{
    				JobForTeacher jobForTeacher = new JobForTeacher();
    				Object[] row = (Object[]) it.next();
    				if(row[0]!=null){
    					jobForTeacher.setJobForTeacherId(Long.parseLong(row[0].toString()));
    					if(row[1]!=null)
    					jobForTeacher.setOfferReady(Boolean.parseBoolean(row[1].toString()));
    					if(row[2]!=null)
    					jobForTeacher.setOfferAccepted(Boolean.parseBoolean(row[2].toString()));
    					if(row[3]!=null)
    					jobForTeacher.setRequisitionNumber(row[3].toString());
    					if(row[4]!=null)
    					jobForTeacher.setIsAffilated(Integer.parseInt(row[4].toString()));
    					StatusMaster statusMaster = new StatusMaster();
    					if(row[5]!=null)
    					statusMaster.setStatusId(Integer.parseInt(row[5].toString()));
    					if(row[6]!=null)
    					statusMaster.setStatusShortName(row[6].toString());
    					jobForTeacher.setStatus(statusMaster);
    					if(row[7]!=null)
    					jobForTeacher.setDistrictId(Integer.parseInt(row[7].toString()));
    					jobForTeacher.setTeacherId(teacherDetail);
    					jobForTeacher.setJobId(jobOrder);
    					lstJobForTeacher.add(jobForTeacher);
    				}
				}
    		  //System.out.println("lstJobForTeacher   gourav   "+lstJobForTeacher.size());
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobForTeacher;
    	}
    	
    	@Transactional(readOnly=false)
    	public List<JobForTeacher> findByJobOrderForCGWithSSN(UserMaster userMaster,JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate,String firstName,String lastName,String emailAddress,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean all,boolean teachersOnly,List<Long> selectedTeachers,TeacherDetail teacherDetail)
    	{
    		PrintOnConsole.getJFTPrint("JFT:19");
    		List<JobForTeacher> lstJobForTeacher=  new ArrayList<JobForTeacher>();
    		try{
    			Session session = getSession();
    			List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
    			Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
    			for (StatusMaster sMaster : statusMasterLst) {
    				mapStatus.put(sMaster.getStatusShortName(),sMaster);
    			}

    			Criterion criterionSchool =null;
                if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
                    criterionSchool=Restrictions.eq("schoolMaster",userMaster.getSchoolId());
                }
    			
    			StatusMaster statusMaster= mapStatus.get("hide");
    			StatusMaster internalStatus= mapStatus.get("icomp");
    			Criterion criterion10 = Restrictions.ne("internalStatus",internalStatus);
    			Criterion criterion14 = Restrictions.isNull("internalStatus");
    			Criterion criterion15 = Restrictions.or(criterion10,criterion14);
    			Criterion criterion11 = Restrictions.eq("internalStatus",internalStatus);
    			Criterion criterion1 = Restrictions.in("status", lstStatusMasters);		
    			Criterion criterion13 = Restrictions.or(criterion11,criterion1);
    			Criterion criterion2 = Restrictions.eq("jobId",jobOrder);
    			Criterion criterion3 = Restrictions.ne("status",statusMaster);
    			Criterion criterion4 = Restrictions.eq("cgUpdated",true);
    			Criterion criterion8 = Restrictions.isNull("cgUpdated");
    			Criterion criterion5 = Restrictions.eq("cgUpdated",false);
    			Criterion criterion6 = Restrictions.or(criterion5, criterion8);
    			Criterion criterion9 = Restrictions.or(criterion6, criterion4);
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			if(teachersOnly)
    			{
    				System.out.println("teachersOnly :"+teachersOnly+" ====== selectedTeachers.size() :: "+selectedTeachers.size());
    				if(selectedTeachers.size()>0)
    				{
    					criteria.add(Restrictions.in("jobForTeacherId", selectedTeachers));
    				}
    			}
    			
    			if(cgUpdate==0){
    				criteria.add(criterion13);
    				criteria.add(criterion2);
    				criteria.add(criterion3);
    				criteria.add(criterion9);
    			}else if(cgUpdate==1){
    				criteria.add(criterion15);
    				criteria.add(criterion1);
    				criteria.add(criterion2);
    				criteria.add(criterion3);
    				
    			}else if(cgUpdate==7){
    				criteria.add(criterion15);
    				criteria.add(criterion1);
    				criteria.add(criterion2);
    				criteria.add(criterion3);
    			}else if(cgUpdate==2){
    				criteria.add(criterion1);
    				criteria.add(criterion2);
    				criteria.add(criterion3);
    			}else if(cgUpdate==5){
    				criteria.add(criterion15);
    				criteria.add(criterion1);
    				criteria.add(criterion2);
    				criteria.add(criterion3);
    			}else if(cgUpdate==3){
    				criteria.add(criterion2);
    				criteria.add(criterion3);
    			}else{
    				criteria.add(criterion2);
    				criteria.add(criterion3);
    			}
    			
    			 if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
    	                criteria.add(criterionSchool);
    	           }
    			
    			if(teacherFlag){
    				criteria.add(Restrictions.in("teacherId", filterTeacherList));
    			}
    			
    			if(teacherDetail!=null){
    				criteria.add(Restrictions.eq("teacherId",teacherDetail));
    			}
    			
    			criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
    			.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
    			.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE));
    			lstJobForTeacher =  criteria.list();
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobForTeacher;
    	}
    	
    	@Transactional(readOnly=false)
		public List<JobForTeacher> findByJobOrderForKellyCGOpSql(UserMaster userMaster,JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate,String firstName,String lastName,String emailAddress,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean all,boolean teachersOnly,List<Long> selectedTeachers,int pageNo, int pageSize,String orderColumn,String sortingOrder,int callType)
		{
			PrintOnConsole.getJFTPrint("JFT:101 findByJobOrderForKellyCGOpSql sql query   "+orderColumn);
			List<JobForTeacher> lstJobForTeacher=  new ArrayList<JobForTeacher>();
			//
			try{
		
				List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
				Map<Integer, StatusMaster> mapStatus = new HashMap<Integer, StatusMaster>();
				for (StatusMaster sMaster : statusMasterLst) {
					mapStatus.put(sMaster.getStatusId(),sMaster);
				}
				
				
				String criterionSchool ="";
				if(callType==1){
					criterionSchool=" and tdl.KSNID is null ";
				}
				if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
					criterionSchool=" and jft.hiredBySchool="+userMaster.getSchoolId()+" ";
	            }
	            
				if(teachersOnly)
				{
				//	System.out.println("teachersOnly :"+teachersOnly+" ====== selectedTeachers.size() :: "+selectedTeachers.size());
					if(selectedTeachers.size()>0)
					{						
						String jftIdLst="";
						for (Long jftId : selectedTeachers) {
							if(jftIdLst.equalsIgnoreCase(""))
								jftIdLst+=jftId+"";
							else
								jftIdLst+=jftId+",";
						}
						criterionSchool=" and jft.jobForTeacherId in("+jftIdLst+")";
					}
				}
				
				if(teacherFlag){
					String tidslst="";
					for (TeacherDetail tIds : filterTeacherList) {
						if(tidslst.equalsIgnoreCase("")){
							tidslst+=tIds.getTeacherId()+"";
						}else{
							tidslst+=","+tIds.getTeacherId();
						}
					}
					criterionSchool+=" and jft.teacherId in ("+tidslst+") ";
				}
				String statusMaster= "";//mapStatus.get("hide");
				
				String ststuaList=""; 
				for (StatusMaster lstStatusMasterss : lstStatusMasters) {
					if(!ststuaList.equalsIgnoreCase(""))
						ststuaList+=","+lstStatusMasterss.getStatusId();
					else
						ststuaList+=lstStatusMasterss.getStatusId();
					if(lstStatusMasterss.getStatusShortName().equalsIgnoreCase("hide")){
						statusMaster=lstStatusMasterss.getStatusId()+""; 
					}
					
				}
				String criterion15 = " and (jft.internalStatus!=3 or jft.internalStatus IS NULL) ";//Restrictions.or(criterion10,criterion14);

				
				String criterion13 = " and (jft.internalStatus!=3 or jft.status in ("+ststuaList+")) ";//Restrictions.or(criterion11,criterion1);				
				String criterion1 = " and jft.status in ("+ststuaList+") ";
				String criterion3 = " and jft.status!=3 ";
			
				String allCondidtions="jft.jobId="+jobOrder.getJobId()+" ";
				if(cgUpdate==0){
					allCondidtions+= criterion13;
				}else if(cgUpdate==1){
					allCondidtions+=criterion15+criterion1;
					boolean epiInternal=false;
					try{
						if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
							epiInternal=true;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					if(jobOrder.getJobCategoryMaster()!=null){/*
						if(jobOrder.getJobCategoryMaster().getBaseStatus() && jobOrder.getJobCategoryMaster().getStatus().equalsIgnoreCase("A")){
							if(epiInternal==false){
								Criterion criterion7 = Restrictions.eq("isAffilated",1);
								Criterion criterion12 =Restrictions.or(criterion7, criterion4) ;
								criteria.add(criterion12);
								//String criterion4 = Restrictions.eq("cgUpdated",true);
								allCondidtions+=" and (jft.isAffilated=1 or jft.cgUpdated=1) ";
							}else{
								allCondidtions+=" and jft.cgUpdated=1 ";
							}
						}
					*/}
				}else if(cgUpdate==7){					
					allCondidtions+=criterion15+criterion1;
				}else if(cgUpdate==2){
					allCondidtions+=criterion1;
				}else if(cgUpdate==5){
					allCondidtions+=criterion15+criterion1;
				}

				String start =""+(pageNo * pageSize);
				String end =""+pageSize;
				
				allCondidtions+=" and firstName like '%"+firstName.trim()+"%' and lastName like '%"+lastName.trim()+"%' and emailAddress like '%"+emailAddress.trim()+"%'";
				System.out.println("allCondidtionsallCondidtions    "+allCondidtions);
				
				String sortOrderStr="";
				boolean baseStatusFlag=false;
				if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
					baseStatusFlag=true;
				}
				
				if(orderColumn!=null && orderColumn.equalsIgnoreCase("normScore") && baseStatusFlag)
				{
					if(sortingOrder.equalsIgnoreCase("0"))
						sortOrderStr = " Order By "+orderColumn+" desc"; 
					else{
						sortOrderStr = " Order By "+orderColumn+" asc";
					}
				}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("normScore") && sortingOrder.equalsIgnoreCase("0") && sortingOrder.equalsIgnoreCase("0")){					
						sortOrderStr = " Order By lastName asc";					
				}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("fScore")){					
					if(sortingOrder.equalsIgnoreCase("0"))
						sortOrderStr = " Order By "+orderColumn+" desc"; 
					else{
						sortOrderStr = " Order By "+orderColumn+" asc";
					}					
				}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("status")){
					if(sortingOrder.equalsIgnoreCase("0"))
						sortOrderStr = " Order By currentStatus asc"; 
					else{
						sortOrderStr = " Order By currentStatus  desc";
					}					
				}else{
					
					if(sortingOrder.equalsIgnoreCase("0"))					
						sortOrderStr = " Order By lastName desc"; 
					else{
						sortOrderStr = " Order By lastName asc";
					}
				}
				int jobCategoryId=jobOrder.getJobCategoryMaster().getJobCategoryId();
				if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
					jobCategoryId=jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();
				}
				String limitTxt="";
				/*if(callType==1){
					limitTxt=" limit "+start+","+end;
				}else if(callType==2){
					
				}*/
				Connection connection =null;
				String sql="SELECT jft.*,tdl.firstName as firstName,tdl.lastName as lastName,tdl.emailAddress,tdl.KSNID,tdl.isPortfolioNeeded,tns.teacherNormScore as normScore,"+
				"status1.statusShortName,status1.status as statusFeildName,"+
				"CASE WHEN jft.displayStatusId IS NOT NULL THEN (CASE WHEN jft.status IN (3,4,9,16,17,18) THEN (SELECT secondaryStatusName FROM secondarystatus WHERE parentStatusId=jft.status AND jobCategoryId="+jobCategoryId+") ELSE  status1.status END) ELSE secondarystatus1.secondaryStatusName END AS currentStatus, "+
				"secondarystatus1.secondaryStatusName,"+
				"jobwiseconteascore.jobWiseConsolidatedScore as fScore "+
				" FROM jobforteacher jft "+ 
		        " LEFT JOIN teachernormscore tns ON tns.teacherId = jft.teacherId "+
		        " INNER JOIN teacherdetail tdl ON tdl.teacherId = jft.teacherId "+
		        " INNER JOIN statusmaster status1 ON status1.statusId = jft.status "+		        
		        " LEFT JOIN secondarystatus secondarystatus1 ON secondarystatus1.secondaryStatusId = jft.displaySecondaryStatusId "+
		        " LEFT JOIN jobwiseconsolidatedteacherscore jobwiseconteascore ON jobwiseconteascore.teacherId = jft.teacherId and jobwiseconteascore.jobId = jft.jobId"+
		        " WHERE "+allCondidtions+" "+criterionSchool+sortOrderStr+limitTxt;
				
				//jobwiseconsolidatedteacherscore
				System.out.println("sql  "+sql);
				SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
			      ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			        connection = connectionProvider.getConnection();
				PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE); 
				 
				  ResultSet rs=ps.executeQuery();
				  rs.last();
				  int rowcount=rs.getRow();
				  rs.beforeFirst();
				  if(rs.next()){				  
					  
					    do{
					     final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
					     JobForTeacher tempJft = new JobForTeacher();
					     	tempJft.setJobForTeacherId(Long.parseLong(rs.getString(rs.findColumn("jobForTeacherId"))));//jobForTeacherId
					     	if(rs.getString("districtId")!=null){
					     		tempJft.setDistrictId(Integer.parseInt(rs.getString(rs.findColumn("districtId"))));//districtId
					     	}
					     	TeacherDetail tDetail = new TeacherDetail();
					     	tDetail.setTeacherId(Integer.parseInt(rs.getString(rs.findColumn("teacherId"))));
					     	tDetail.setFirstName(rs.getString("firstName"));
					     	if(rs.getString("KSNID")!=null){
					     		tDetail.setKSNID(new BigInteger(rs.getString("KSNID")));
					     	}
					     	tDetail.setLastName(rs.getString("lastName"));
					     	tDetail.setEmailAddress(rs.getString("emailAddress"));
					     	boolean isPortfolioNeeded = false;
					     	if(rs.getString("tdl.isPortfolioNeeded")!=null){
					     		isPortfolioNeeded=rs.getBoolean("tdl.isPortfolioNeeded");
					     		
					     	}
					     	tDetail.setIsPortfolioNeeded(isPortfolioNeeded);
					     	//System.out.println("rs.getString()    "+rs.getString("firstName"));
					     	tempJft.setTeacherId(tDetail);//teacherId
					     /*	JobOrder tempJobOrder = new JobOrder();
					     	tempJobOrder.setJobId(Integer.parseInt(rs.getString(rs.findColumn("jobId"))));*/
					     	tempJft.setJobId(jobOrder);
					     	tempJft.setRedirectedFromURL(rs.getString("redirectedFromURL"));
					     	tempJft.setJobBoardReferralURL(rs.getString("jobBoardReferralURL"));
					     	StatusMaster tempStatus = null;					     	
					     	if(rs.getString("status")!=null){
					     		tempStatus = mapStatus.get(Integer.parseInt(rs.getString("status"))); 
					     	}
					     	tempJft.setStatus(tempStatus);
					     	StatusMaster tempStatus2 = null;
					     	if(rs.getString("displayStatusId")!=null){
					     		tempStatus2 = mapStatus.get(Integer.parseInt(rs.getString("displayStatusId")));
					     	}
					     	tempJft.setStatusMaster(tempStatus2);
					     	
					     	if(rs.getString("displaySecondaryStatusId")!=null){
					     		SecondaryStatus secondaryStatus = new SecondaryStatus();
					     		secondaryStatus.setSecondaryStatusId(Integer.parseInt(rs.getString("displaySecondaryStatusId")));
					     		secondaryStatus.setSecondaryStatusName(rs.getString("secondarystatus1.secondaryStatusName"));					     		
					     		tempJft.setSecondaryStatus(secondaryStatus);
					     	}else{
					     		tempJft.setSecondaryStatus(null);
					     	}
					     		
					     		
					     	StatusMaster tempStatus3 = null;
					     	if(rs.getString("internalStatus")!=null)
					     		tempStatus3 = mapStatus.get(Integer.parseInt(rs.getString("internalStatus")));					     		
					     	tempJft.setInternalStatus(tempStatus3);
					     	
					     	
					     	if(rs.getString("internalSecondaryStatusId")!=null){
					     		SecondaryStatus secondaryStatus2 = new SecondaryStatus();
					     		secondaryStatus2.setSecondaryStatusId(Integer.parseInt(rs.getString("internalSecondaryStatusId")));
					     		tempJft.setInternalSecondaryStatus(secondaryStatus2);
					     	}else{
					     		tempJft.setInternalSecondaryStatus(null);
					     	}
					     	if(rs.getString("cgUpdated")!=null)
					     		tempJft.setCgUpdated(rs.getBoolean("cgUpdated"));
					     	tempJft.setCoverLetter(rs.getString("coverLetter"));
					     	tempJft.setCoverLetterFileName(rs.getString("coverLetterFileName"));
					     	if(rs.getString("isAffilated")!=null)
					     		tempJft.setIsAffilated(Integer.parseInt(rs.getString("isAffilated")));
					     	tempJft.setStaffType(rs.getString("staffType"));
					     	if(rs.getString("flagForDistrictSpecificQuestions")!=null)
					     		tempJft.setFlagForDistrictSpecificQuestions(rs.getBoolean("flagForDistrictSpecificQuestions"));
					     	tempJft.setNoteForDistrictSpecificQuestions(rs.getString("noteForDistrictSpecificQuestions"));
					     	if(rs.getString("isDistrictSpecificNoteFinalize")!=null)
					     		tempJft.setIsDistrictSpecificNoteFinalize(rs.getBoolean("isDistrictSpecificNoteFinalize"));
					     	
					     	if(rs.getString("districtSpecificNoteFinalizeBy")!=null){
					     		UserMaster finalizeBY = new UserMaster();
					     		finalizeBY.setUserId(Integer.parseInt(rs.getString("districtSpecificNoteFinalizeBy")));
					     		tempJft.setDistrictSpecificNoteFinalizeBy(finalizeBY);
					     	}else{
					     		tempJft.setDistrictSpecificNoteFinalizeBy(null);
					     	}
					     	if(rs.getString("flagForDspq")!=null)
					     		tempJft.setFlagForDspq(rs.getBoolean("flagForDspq"));
					     	tempJft.setRequisitionNumber(rs.getString("requisitionNumber"));
					     	
					     	if(rs.getString("updatedBy")!=null){
					     		UserMaster usrMas = new UserMaster();
					     		usrMas.setUserId(Integer.parseInt(rs.getString("updatedBy")));
					     		tempJft.setUpdatedBy(usrMas);
					     	}else{
					     		tempJft.setUpdatedBy(null);
					     	}
					     	if(rs.getString("updatedByEntity")!=null)
					     		tempJft.setUpdatedByEntity(Integer.parseInt(rs.getString("updatedByEntity")));
					     	tempJft.setLastActivity(rs.getString("lastActivity"));
					     	
					     	if(rs.getString("lastActivityDoneBy")!=null){
					     		UserMaster usrMas2 = new UserMaster();
					     		usrMas2.setUserId(Integer.parseInt(rs.getString("lastActivityDoneBy")));
					     		tempJft.setUserMaster(usrMas2);
					     	}else{
					     		tempJft.setUserMaster(null);
					     	}
					     	
					     	if(rs.getString("hiredBySchool")!=null){
					     		SchoolMaster hiredSchool = new SchoolMaster();
					     		hiredSchool.setSchoolId(Long.parseLong(rs.getString("hiredBySchool")));
					     		tempJft.setSchoolMaster(hiredSchool);
					     	}else{
					     		tempJft.setSchoolMaster(null);
					     	}
					     	if(rs.getString("offerReady")!=null)
					     		tempJft.setOfferReady(rs.getBoolean("offerReady"));
					     	if(rs.getString("offerAccepted")!=null)
					     		tempJft.setOfferAccepted(rs.getBoolean("offerAccepted"));
					     	if(rs.getString("noResponseEmail")!=null)
					     		tempJft.setNoResponseEmail(rs.getBoolean("noResponseEmail"));
					     	if(rs.getString("candidateConsideration")!=null)
					     		tempJft.setCandidateConsideration(rs.getBoolean("candidateConsideration"));
					     	if(rs.getString("noOfReminderSent")!=null)
					     		tempJft.setNoOfReminderSent(Integer.parseInt(rs.getString("noOfReminderSent")));
					     	if(rs.getString("noofTries")!=null)
					     		tempJft.setNoofTries(Integer.parseInt(rs.getString("noofTries")));
					     	if(rs.getString("branchcontact")!=null)
					     		tempJft.setBranchcontact(rs.getBoolean("branchcontact"));
					     	if(rs.getString("jobCompleteDate")!=null)
					     		tempJft.setJobCompleteDate(rs.getDate("jobCompleteDate"));
					     //JobCompleteDate
					     	tempJft.setCreatedDateTime(rs.getDate("createdDateTime"));
					     	if(rs.getString("notReviewedFlag")!=null){
					     		tempJft.setNotReviewedFlag(rs.getBoolean("notReviewedFlag"));
					     	}
					     	if(rs.getString("normScore")!=null)
					     		tempJft.setNormScore(Double.parseDouble(rs.getString("normScore")));
					     	
					     	if(rs.getString("fScore")!=null){
					     		tempJft.setFitScore(rs.getDouble("fScore"));
					     	}
					     	lstJobForTeacher.add(tempJft);
					     
					    }while(rs.next());
					   }
					   else{
					    System.out.println("Record not found ......");
					   }
				} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			return lstJobForTeacher;
		} 

    	// for Optimization of candidate pool search
    	
    	
    	@Transactional(readOnly=false)
    	public List<Object> findInternalCandidateOp(List<TeacherDetail> lstteacherDetail , Integer districtId)
    	{
    		List<Object> lstJobForTeacher=new ArrayList<Object>();
    		try 
    		{
    			if(lstteacherDetail.size()>0)
    			{
    				List<Integer> ids = new ArrayList<Integer>(lstteacherDetail.size());
    				for(TeacherDetail td : lstteacherDetail){ids.add(td.getTeacherId());}
    				
    				
    				Criteria criteria = this.getSession().createCriteria(this.getPersistentClass());
    				criteria.createAlias("jobId", "jo").createAlias("teacherId", "td").createAlias("jo.districtMaster", "dm",criteria.LEFT_JOIN);
    				if(districtId!=null && districtId!=0){
    					criteria.add(Restrictions.eq("districtId", districtId));
    				}
    				
    				Criterion criterion_tId = Restrictions.in("td.teacherId",ids);
    				criteria.setProjection(Projections.distinct(
    						Projections.projectionList()
    				           .add( Projections.property("td.teacherId"), "teacherId" )
    				           .add( Projections.property("dm.districtId"), "districtId" )
    				            .add( Projections.property("jo.jobId"), "jid" )
    				            .add( Projections.property("isAffilated"), "isAffilated" )
    				         ));
    				lstJobForTeacher = criteria.list();
    				
    				
//    				System.out.println("lstJobForTeacher:::::  size "+lstJobForTeacher.size());
    			/*	for(Iterator data =lstJobForTeacher.iterator() ; data.hasNext();){
    					Object []row = (Object[])data.next();
    					System.out.println("teacherId :::: "+ (Integer.parseInt(row[0].toString())) +" DistrictId::::::  "+(Integer.parseInt(row[1].toString()))+" jobid::::::  "+(Integer.parseInt(row[2].toString())));
    					
    				}*/
    			}
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}
    		finally
    		{
    			return lstJobForTeacher;
    		}
    	}
 
    	
    	@Transactional(readOnly=false)
    	public List<JobForTeacher> findJobByTeacherForOfferOp(TeacherDetail teacherDetail)
    	{
    		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    		try 
    		{
    			
    			Session session 		= 	getSession();   		
    			
    			Criteria cr = session.createCriteria(JobForTeacher.class);
    			cr.createAlias("jobId", "jo")
    		    .setProjection(Projections.projectionList()
    		    		.add( Projections.property("jobForTeacherId"), "jobForTeacherId" )
    		    		.add( Projections.property("requisitionNumber"), "requisitionNumber" )
    		    		.add( Projections.property("jo.jobId"), "jobId" )
    		    		);
    			
    			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
    			Criterion criterion2 = Restrictions.eq("offerReady",true);
    			Criterion criterion3 = Restrictions.eq("offerReady",false);
    			Criterion criterion4 = Restrictions.or(criterion3,criterion2);
    			//lstJobForTeacher = findByCriteria(criterion1,criterion4);
    			
    			cr.add(criterion4).add(criterion1);
    			
    			List<String []> jobForTeacherLst=new ArrayList<String []>();
    			jobForTeacherLst = cr.list();
    			
    			for (Iterator it = jobForTeacherLst.iterator(); it.hasNext();)
				{
    				//System.out.println("call");
    				JobForTeacher jobForTeacher = new JobForTeacher();
    				Object[] row = (Object[]) it.next();    				
    				if(row[0]!=null){
    					jobForTeacher.setJobForTeacherId(Long.parseLong(row[0].toString()));
    					if(row[1]!=null)
    						jobForTeacher.setRequisitionNumber(row[1].toString());
    					if(row[2]!=null){
    						JobOrder jobOrder = new JobOrder();
    						jobOrder.setJobId(Integer.parseInt(row[2].toString()));
    						jobForTeacher.setJobId(jobOrder);
    					}
    					lstJobForTeacher.add(jobForTeacher);
    				}
    			}
//    			System.out.println("lstJobForTeacher    "+lstJobForTeacher.size());
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobForTeacher;
    	}
 
    	
    	@Transactional(readOnly=false)
    	public List<Object> getJobStatusByJobCategory(JobCategoryMaster jobCategoryMaster,Integer isAffilated,Integer districtId)
    	{
    		List<Object> lstJobAndTchr= null;
    		try{
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			criteria.createAlias("jobId", "jo");
    			
   				if(jobCategoryMaster!=null)
   					criteria.add(Restrictions.eq("jo.jobCategoryMaster", jobCategoryMaster));
   				
   				if(isAffilated.equals(1))
   				{
   					Criterion criterion1 = Restrictions.eq("isAffilated",isAffilated);
   	    			criteria.add(criterion1);
   				}
   				else if(isAffilated.equals(0))
   				{
   					Criterion criterion1 = Restrictions.ne("isAffilated",1);
   	    			criteria.add(criterion1);
   				}
    			
   				Criterion criterion2 = Restrictions.eq("districtId",districtId);
    			criteria.add(criterion2);
   				
    			/*StatusMaster statusMaster=new StatusMaster();
    			statusMaster.setStatusId(3);*/
    			criteria.createAlias("statusMaster", "st");
    			Criterion criterion3 = Restrictions.ne("st.statusId",3);
    			criteria.add(criterion3);
    			
   				criteria.setProjection( Projections.projectionList()
   						.add(Projections.groupProperty("jo.jobId"))
   						.add(Projections.property("jo.jobTitle"))
   						.add(Projections.count("jo.jobId"))) ;
   				
    			lstJobAndTchr =  criteria.list();
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobAndTchr;
    		
    	}
    	
    	@Transactional(readOnly=false)
    	public List<JobForTeacher> findJobByTeacherListForOffersOp(List<TeacherDetail> teacherDetails,DistrictMaster districtMaster,StatusMaster statusMaster,StatusMaster secondaryStatus)
    	{
    		PrintOnConsole.getJFTPrint("JFT:69");	
    		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    		try 
    		{
    			if(teacherDetails.size()>0){
    				
    				Session session = getSession();
        			Criteria criteria = session.createCriteria(getPersistentClass());
        			criteria.createAlias("jobId", "jo").createAlias("teacherId", "td")
        			.setProjection(Projections.projectionList()
        					.add( Projections.property("jobForTeacherId"))
        					.add( Projections.property("requisitionNumber"))
        					.add( Projections.property("jo.jobId"), "jobId" )
        					.add( Projections.property("td.teacherId") )
        					);
    				Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
    				Criterion criterion2 = Restrictions.not(Restrictions.isNull("requisitionNumber"));	
    				Criterion criterion3 = Restrictions.not(Restrictions.eq("requisitionNumber",""));
    				Criterion criterion4 = Restrictions.or(criterion3,criterion2);
    				Criterion criterion5 = Restrictions.eq("status",statusMaster);
    				List<String []> jobForTeacherLst=new ArrayList<String []>();
    				if(districtMaster!=null && districtMaster.getDistrictId()==1200390){
    					Criterion criterion6 = Restrictions.eq("statusMaster",secondaryStatus);
    					Criterion criterion7 = Restrictions.or(criterion5, criterion6);
    					criteria.add(criterion1).add(criterion4).add(criterion7);
    	    			
    				}else{    					
    					criteria.add(criterion1).add(criterion4).add(criterion5);
    					//lstJobForTeacher = findByCriteria(criterion1,criterion4,criterion5);
    				}
    				
    				jobForTeacherLst = criteria.list();
	    			
	    			for (Iterator it = jobForTeacherLst.iterator(); it.hasNext();)
					{
	    				//System.out.println("call");
	    				JobForTeacher jobForTeacher = new JobForTeacher();
	    				Object[] row = (Object[]) it.next();    				
	    				if(row[0]!=null){
	    					jobForTeacher.setJobForTeacherId(Long.parseLong(row[0].toString()));
	    					if(row[1]!=null)
	    						jobForTeacher.setRequisitionNumber(row[1].toString());
	    					if(row[2]!=null){
	    						JobOrder jobOrder = new JobOrder();
	    						jobOrder.setJobId(Integer.parseInt(row[2].toString()));
	    						jobForTeacher.setJobId(jobOrder);
	    					}
	    					if(row[3]!=null){
	    						TeacherDetail teacherDetail = new TeacherDetail();
	    						teacherDetail.setTeacherId(Integer.parseInt(row[3].toString()));
	    						jobForTeacher.setTeacherId(teacherDetail);
	    					}
	    					lstJobForTeacher.add(jobForTeacher);
	    				}
	    			}
    			}
    		}catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobForTeacher;
    	}

    	//optimization for candidate pool
    	
    @Transactional(readOnly=false)
    public List<String[]> hireTimeBySchool(int districtOrSchoolId,Long schoolId,String jobOrderId,int categoryId,String noOfRow,String pageNo,String sortOrder,String sortOrderType,boolean exportcheck,String sortColomnName,String sortOrderStrVal,int start,boolean istotal,int end)
    	{
    	PrintOnConsole.getJFTPrint("JFT:101");
    	  List<String[]> lst=new ArrayList<String[]>(); 
    	  Session session = getSession();
    	  String sql = "";
    	  String add="";
    	  List <String>condition =new ArrayList<String>();
    	   Connection connection =null;
      	try
      	
      	{       		
      		SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) session.getSessionFactory();
    	      	ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
    	        connection = connectionProvider.getConnection();
    	      SchoolMaster schoolMaster=null;
    	        if(schoolId!=null&& !schoolId.equals(""))
    	        {
    	        	schoolMaster=    schoolMasterDAO.findById(schoolId, false, false);	
    	        }
    	
      	int districtid=districtOrSchoolId; 
      	String schoolName="";
      	if(schoolMaster!=null)
      	{
      		schoolName=schoolMaster.getSchoolName();
      	}
      		
      		int categoryid=categoryId;
      		
      		int jobid=0;
      		String jobTitle="";
      		if(jobOrderId!=null&& !jobOrderId.equals("0"))
      		{
      			try{
      				jobid = Integer.parseInt(jobOrderId);
      			}catch (Exception e) {jobid=0;}
      			if(jobid==0)
          		{
          			jobTitle=jobOrderId;
          		}
      		}
      		
      		
      		System.out.println("sortColomnName ::::::::::"+sortColomnName);
      	/*	int jobid=0;
      		if(jobOrderId!=null || jobOrderId !="")
      			try{
      				jobid = Integer.parseInt(jobOrderId);
      			}catch (Exception e) {}*/
      		if(sortColomnName.equalsIgnoreCase("districtName"))
				sortColomnName = "districtName";
			else if(sortColomnName.equalsIgnoreCase("School"))
				sortColomnName = "schoolname";
			else if(sortColomnName.equalsIgnoreCase("Job_ID"))
				sortColomnName = "jobid";
			else if(sortColomnName.equalsIgnoreCase("Job_Title"))
				sortColomnName = "jobtitle";
			else if(sortColomnName.equalsIgnoreCase("Job_Category"))
				sortColomnName = "jobcategoryname";
			else if(sortColomnName.equalsIgnoreCase("N_Hired"))
				sortColomnName = "N_Hired";
			else if(sortColomnName.equalsIgnoreCase("Averege_Hire_Days"))
				sortColomnName = "Average_Hire_Days";
      		String orderby =sortColomnName+" "+sortOrderStrVal  ;
      	 		if(istotal){
      	 			sql=" select count(*)";
      	 		}else{
      	 			sql=" select "+ 
                    "districtname,"+ 
                    "schoolname,"+
                    "jobid,"+ 
                    " jobtitle, "+ 
                    " jobcategoryname, "+ 
                    " count(distinct teacherid) As N_Hired,"+ 
                    "round(avg(hiretime),2) As Average_Hire_Days";
      	 				}
      	 		if(istotal)
      	 		{
      	 			sql=" select "+ 
                    "districtname,"+ 
                    "schoolname,"+
                    "jobid,"+ 
                    " jobtitle, "+ 
                    " jobcategoryname, "+ 
                    " count(distinct teacherid)As  N_Hired,"+ 
                    "round(avg(hiretime),2) As Average_Hire_Days";
      	 		}
      	 		sql+=" from "+
                " ( "+
            	" select "+ 
            		" jft.districtid, "+ 
                    " sm.schoolid,"+
            		" jft.teacherid,"+ 
            		" jo.jobcategoryid,"+ 
            		" jft.jobid,"+ 
            		" jo.jobtitle,"+ 
            		" jft.status,"+ 
            		" jo.noofhires,"+ 
            		" jft.createddatetime applicationdate,"+ 
            		" jft.jobcompletedate applcompletedate,"+ 
            		" jft.hiredbydate `hireddate`,"+ 
            		" sm.schoolname,"+
            		" dm.districtname,"+
            		" jcm.jobcategoryname,"+
            		" datediff(jft.hiredbydate, jft.createddatetime) hiretime "+
            		" from jobforteacher jft "+
          		" inner join schoolmaster sm on jft.hiredBySchool=sm.schoolid "+
          		" inner join joborder jo on jo.jobid=jft.jobid "+
          		" inner join districtmaster dm on dm.districtid=jft.districtid "+
          		" inner join jobcategorymaster jcm on jcm.jobCategoryId=jo.jobcategoryid "+
          		" where jft.status=6 "+
          		" ) as schooljobs ";
      	 	
      	 	if(districtid !=0)
      	 		condition.add(" schooljobs.districtid="+districtid);
      	 	 
      	 	
      	 	if(schoolName!="")
      	 		condition.add(" schooljobs.schoolname='"+schoolName+"'");
      	 	 
      	 	
      	 	if(categoryid != 0) 
      	 		condition.add(" schooljobs.jobcategoryid="+categoryid);
      	 		 
      	 	
      	 	if(jobid != 0)
      	 		condition.add("  schooljobs.jobid="+jobid);
      	 	if(!jobTitle.equals(""))
      	 		condition.add("  schooljobs.jobtitle LIKE '%"+jobTitle+"%'");
      	 		
      	 	 
                     
          
      	 	 String other = " group by schooljobs.districtname, schooljobs.schoolname, schooljobs.jobid"+
            " order by "; 
      	 	
            String conditions ="";
            for(int i=0;i<condition.size();i++){
          	  
          	  if(i==0)
          		  conditions+=" where " + condition.get(i);
          	  else
          		  conditions += " and " +condition.get(i);
          	  
          	  
            }
            
            
            sql+=  " " + conditions+" " +other+" "+orderby;
            if(!istotal)
            sql+=" limit "+start+","+end;    
            
      		  Statement ps=connection.createStatement(); 
      			 
      		  ResultSet rs=ps.executeQuery(sql);
      		 
      		   if(rs.next()){    
      		    do{
      		     final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
      		     for(Integer i=1;i<=rs.getMetaData().getColumnCount();i++)
      		        {
      		      allInfo[i-1]=rs.getString(i);
      		        }
      		     lst.add(allInfo);      		    
      		     
      		    }while(rs.next());
      		   }
      		   else{
      		    System.out.println("Record not found.");
      		   }
      		   
      		   return lst;
      		
      	}catch(Exception e){e.printStackTrace();}
      	finally{
      		   if(connection!=null)
      		    try {
      		     connection.close();
      		    } catch (SQLException e) {
      		     // TODO Auto-generated catch block
      		     e.printStackTrace();
      		    }
      		  }    	
      	
      return null;
}

    	//optimization for candidate pool
    	
    	
    	
      @Transactional(readOnly=false)
		public List<Object> findJobByTeacherForOffersOp(List<TeacherDetail> teacherDetails)
		{
			List<Object> lstJobForTeacher= new ArrayList<Object>();
			try 
			{
				if(teacherDetails!=null && teacherDetails.size() >0)
				{
					//StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
					StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
					Criterion criterion1 = Restrictions.in("teacherId",teacherDetails);
					Criterion criterion2 = Restrictions.not(Restrictions.isNull("requisitionNumber"));	
					Criterion criterion3 = Restrictions.not(Restrictions.eq("requisitionNumber",""));
					Criterion criterion4 = Restrictions.or(criterion3,criterion2);
					Criterion criterion5 = Restrictions.eq("status",statusMaster);
					
					Criteria criteria = getSession().createCriteria(this.getPersistentClass()).createAlias("jobId", "jo").createAlias("teacherId", "teacher");
					criteria.add(criterion1).add(criterion4).add(criterion5);
					criteria.setProjection(Projections.projectionList()
							.add(Projections.property("requisitionNumber"))
							.add(Projections.property("jo.jobId"))
							.add(Projections.property("teacher.teacherId")));
					lstJobForTeacher = criteria.list();
				}
			} 
			catch (Exception e) {
				e.printStackTrace();
			}		
			
			return lstJobForTeacher;
		}

    	//brajesh
    	@Transactional(readOnly=false)
    	public List<Object> findInternalCandidate1(List<TeacherDetail> lstteacherDetail,Integer districtId)
    	{
    		List<Object> lstJobForTeacher= new ArrayList<Object>();
    		List<Object> teacherIds =new ArrayList<Object>();
    		try 
    		{
    			if(lstteacherDetail.size()>0)
    			{
    				for(TeacherDetail td :lstteacherDetail)
    					teacherIds.add(td.getTeacherId());
    				 
    				 String sql =  "SELECT t2.jobForTeacherId ,t2.teacherId, t2.districtId,t2.jobId,t2.isAffilated FROM jobforteacher t2 INNER JOIN(SELECT MAX(t1.jobForTeacherId) as jobForTeacherId  FROM jobforteacher t1 where t1.teacherId in (:tids)   GROUP BY t1.teacherId    ) t1 ON  t1.jobForTeacherId = t2.jobForTeacherId";
    				if(districtId!=null && districtId!=0)
    					sql =  "SELECT t2.jobForTeacherId ,t2.teacherId, t2.districtId,t2.jobId,t2.isAffilated FROM jobforteacher t2 INNER JOIN(SELECT MAX(t1.jobForTeacherId) as jobForTeacherId  FROM jobforteacher t1 where t1.teacherId in (:tids) and districtId =:distId  GROUP BY t1.teacherId    ) t1 ON  t1.jobForTeacherId = t2.jobForTeacherId";
    				 Query query = session.createSQLQuery(sql);
    				query.setParameterList("tids", teacherIds);
    				if(districtId!=null && districtId!=0)
    					query.setInteger("distId", districtId);
    				
    				 				 
    				 lstJobForTeacher = query.list();
    			}
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}
    		finally
    		{
    			return lstJobForTeacher;
    		}
    	}
    	
    	public List<TeacherDetail> getTeacherDetailByDASARecordsBeforeDate1(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,boolean newcandidatesonly,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds)
    	{
    		PrintOnConsole.getJFTPrint("JFT:17");
    		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
    		try{
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
    			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
    			if(internalCandVal==1){
    				criteria.add(criterionInterCand);
    			}*/
    			String[] candidatetyp=null;
    			if(internalCandVal!=null){
    					candidatetyp=internalCandVal.split("|");
    			}
    			List<Integer> candidatetypec=new ArrayList<Integer>();
    			if(candidatetyp!=null){
    				for(String s:candidatetyp){
    					if(s.equalsIgnoreCase("0")){
    						candidatetypec.add(1);
    						candidatetypec.add(5);
    					}else{
    						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
    							int val=Integer.parseInt(s);
    							candidatetypec.add(val);
    						}
    					}
    				}
    			}
    			if(candidatetypec.size()>0){
    				criteria.add(Restrictions.in("isAffilated",candidatetypec));
    			}
    			
    			boolean flag=false;
    			boolean dateDistrictFlag=false;
    			
    			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
    			{
    				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
    				criteria.add(criterion1);
    			}
    			
    			if((entityID==3 || entityID==4)){
    				flag=true;
    				if(lstJobOrder.size()>0)
    				{
    					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
    					criteria.add(criterion1);
    				}
    				if(entityID==4 && dateFlag==false){
    					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    				}else if(entityID==4 && dateFlag){
    					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    					dateDistrictFlag=true;
    					if(fDate!=null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    					}else if(fDate!=null && tDate==null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    					}else if(fDate==null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    					}
    				}
    			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
    				flag=true;
    				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			}else if(dateFlag && districtMaster!=null &&  entityID==2){
    				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    				flag=true;
    				if(fDate!=null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    				}else if(fDate!=null && tDate==null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    				}else if(fDate==null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    				}
    				dateDistrictFlag=true;
    			}
    			if(dateDistrictFlag==false && dateFlag){
    				if(fDate!=null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    				}else if(fDate!=null && tDate==null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    				}else if(fDate==null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    				}
    				flag=true;
    			}
    			if(status){
    				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
    			}else{
    				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
    			}

    			Calendar cal1 = Calendar.getInstance();
    			cal1.set(Calendar.HOUR_OF_DAY, 0);
    			cal1.set(Calendar.MINUTE, 0);
    			cal1.set(Calendar.SECOND, 0);
    			cal1.set(Calendar.MILLISECOND, 0);

    			Calendar cal2 = Calendar.getInstance();
    			cal2.add(Calendar.HOUR,23);
    			cal2.add(Calendar.MINUTE,59);
    			cal2.add(Calendar.SECOND,59);

    			if(daysVal==0)
    			{
    				cal1.add(Calendar.DATE, -7);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}
    			else if(daysVal==1)
    			{
    				cal1.add(Calendar.DATE, -15);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}else if(daysVal==2)
    			{
    				cal1.add(Calendar.DATE, -30);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}else if(daysVal==3)
    			{
    				cal1.add(Calendar.DATE, -60);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}
    			else if(daysVal==4)
    			{
    				cal1.add(Calendar.DATE, -90);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			} 
    			//job applied flag
    			
    			flag=true;
    			if(daysVal==5 && appliedtDate!=null && appliedfDate!=null)
    				//criteria.add(Restrictions.or( Restrictions.le("createdDateTime",appliedfDate),Restrictions.ge("createdDateTime",appliedtDate)));
    				criteria.add(Restrictions.le("createdDateTime",appliedfDate));
    			else if(daysVal==5 && appliedtDate!=null)
    				criteria.add(Restrictions.ge("createdDateTime",appliedtDate));
    			else if(appliedfDate!=null)
    				criteria.add(Restrictions.le("createdDateTime",appliedfDate));


    			if(teacherFlag){
    				flag=true;
    				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
    			}
    			if(nByAflag){
    				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
    			}
    			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
    				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
    				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
    				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
    				.addOrder(sortOrderStrVal);
    				criteria.setProjection(Projections.groupProperty("teacherId"));
    				lstTeacherDetail =  criteria.list();
    			}else{
    				lstTeacherDetail=new ArrayList<TeacherDetail>();
    			}
    			System.out.println("lstTeacherDetail.size()::: "+lstTeacherDetail.size());
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstTeacherDetail;
    	}
    	
    	@Transactional(readOnly=false)
    	public List<TeacherDetail> getTeacherDetailByDASA1(Order sortOrderStrVal,int start,int end,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> notTeacherDetails,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster)
    	{
    		PrintOnConsole.getJFTPrint("JFT:13");
    		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
    		try{
    			Session session = getSession();
    			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
    			if(internalCandVal==1){
    				criteria.add(criterionInterCand);
    			}*/
    			String[] candidatetyp=null;
    			if(internalCandVal!=null){
    					candidatetyp=internalCandVal.split("|");
    			}
    			
    			List<Integer> candidatetypec=new ArrayList<Integer>();
    			if(candidatetyp!=null){
    				for(String s:candidatetyp){
    					if(s.equalsIgnoreCase("0")){
    						candidatetypec.add(1);
    						candidatetypec.add(5);
    					}else{
    						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
    							int val=Integer.parseInt(s);
    							candidatetypec.add(val);
    						}
    					}
    				}
    			}
    			if(candidatetypec.size()>0){
    				criteria.add(Restrictions.in("isAffilated",candidatetypec));
    			}

    			criteria.add(Restrictions.ne("status",statusMaster));

    			boolean flag=false;
    			boolean dateDistrictFlag=false;
    			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
    			{
    				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
    				criteria.add(criterion1);
    			}
    			
    			if((entityID==3 || entityID==4)){
    				flag=true;
    				if(lstJobOrder.size()>0)
    				{
    					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
    					criteria.add(criterion1);
    				}
    				if(entityID==4 && dateFlag==false){
    					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    				}else if(entityID==4 && dateFlag){
    					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    					dateDistrictFlag=true;
    					if(fDate!=null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    					}else if(fDate!=null && tDate==null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    					}else if(fDate==null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    					}
    				}
    			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
    				flag=true;
    				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			}else if(dateFlag && districtMaster!=null &&  entityID==2){
    				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    				flag=true;
    				if(fDate!=null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    				}else if(fDate!=null && tDate==null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    				}else if(fDate==null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    				}
    				dateDistrictFlag=true;
    			}
    			if(dateDistrictFlag==false && dateFlag){
    				if(fDate!=null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    				}else if(fDate!=null && tDate==null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    				}else if(fDate==null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    				}
    				flag=true;
    			}
    			if(status){
    				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
    				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
    					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
    			}else{
    				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
    			}
    			if(teacherFlag){
    				flag=true;
    				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
    			}
    			if(nByAflag){
    				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
    			}

    			Calendar cal1 = Calendar.getInstance();
    			cal1.set(Calendar.HOUR_OF_DAY, 0);
    			cal1.set(Calendar.MINUTE, 0);
    			cal1.set(Calendar.SECOND, 0);
    			cal1.set(Calendar.MILLISECOND, 0);

    			Calendar cal2 = Calendar.getInstance();
    			cal2.add(Calendar.HOUR,23);
    			cal2.add(Calendar.MINUTE,59);
    			cal2.add(Calendar.SECOND,59);

    			if(daysVal==0)
    			{
    				cal1.add(Calendar.DATE, -7);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}
    			else if(daysVal==1)
    			{
    				cal1.add(Calendar.DATE, -15);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}else if(daysVal==2)
    			{
    				cal1.add(Calendar.DATE, -30);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}else if(daysVal==3)
    			{
    				cal1.add(Calendar.DATE, -60);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}
    			else if(daysVal==4)
    			{
    				cal1.add(Calendar.DATE, -90);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}

    			// job applied flag
    			if(appliedfDate!=null && appliedtDate!=null){
    				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
    			}else if(appliedfDate!=null && appliedtDate==null){
    				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
    			}else if(appliedtDate!=null && appliedfDate==null){
    				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
    			}


    			if(notTeacherDetails.size()>0)
    				criteria.add(Restrictions.not(Restrictions.in("teacherId", notTeacherDetails)));	
    			//new change fresh candidates
    			flag=true;
    			
    			if(districtMaster!=null  && utype==3)
    			{
    				StatusMaster master = districtMaster.getStatusMaster();
    				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
    				if(master!=null)
    				{
    					System.out.println("master: "+master.getStatus());
    					//String sts = master.getStatus();
    					String sts = master.getStatusShortName();
    					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
    					{
    						List<StatusMaster> sList = new ArrayList<StatusMaster>();
    						sList.add(master);
    						if(sts.equals("scomp"))
    						{
    							StatusMaster st1 = new StatusMaster();
    							st1.setStatusId(17);
    							sList.add(st1);
    							
    							StatusMaster st2 = new StatusMaster();
    							st2.setStatusId(18);
    							sList.add(st2);
    							
    						}else if(sts.equals("vcomp"))
    						{
    							StatusMaster st2 = new StatusMaster();
    							st2.setStatusId(18);
    							sList.add(st2);
    						}
    						
    						/*
    						6 - Hired
    						19 - Declined
    						10 - rejected
    						7 -- Withdrew
    						*/
    						
    						StatusMaster stHird = new StatusMaster();
    						stHird.setStatusId(6);
    						sList.add(stHird);

    						StatusMaster stDeclined = new StatusMaster();
    						stDeclined.setStatusId(19);
    						sList.add(stDeclined);
    						
    						StatusMaster stRejected = new StatusMaster();
    						stRejected.setStatusId(10);
    						sList.add(stRejected);
    						
    						StatusMaster stWithdrew = new StatusMaster();
    						stWithdrew.setStatusId(7);
    						sList.add(stWithdrew);
    						
    						criteria.add(Restrictions.in("status",sList));
    					}else
    					{
    						List<StatusMaster> sList = new ArrayList<StatusMaster>();
    						sList.add(master);
    						if(sts.equals("hird"))
    						{
    							StatusMaster stDeclined = new StatusMaster();
    							stDeclined.setStatusId(19);
    							sList.add(stDeclined);
    							
    							StatusMaster stRejected = new StatusMaster();
    							stRejected.setStatusId(10);
    							sList.add(stRejected);
    							
    							StatusMaster stWithdrew = new StatusMaster();
    							stWithdrew.setStatusId(7);
    							sList.add(stWithdrew);
    						}else if(sts.equals("dcln"))
    						{
    							StatusMaster stRejected = new StatusMaster();
    							stRejected.setStatusId(10);
    							sList.add(stRejected);
    							
    							StatusMaster stWithdrew = new StatusMaster();
    							stWithdrew.setStatusId(7);
    							sList.add(stWithdrew);

    						}else if(sts.equals("rem"))
    						{
    							StatusMaster stWithdrew = new StatusMaster();
    							stWithdrew.setStatusId(7);
    							sList.add(stWithdrew);
    						}
    						criteria.add(Restrictions.in("status",sList));
    						//criteria.add(Restrictions.eq("status",master));
    					}
    				}
    				if(smaster!=null)
    				{
    					System.out.println("smaster: "+smaster.getSecondaryStatusName());
    					Criteria c = criteria.createCriteria("secondaryStatus");
    					
    					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
    					if(!stq.equals(""))
    					{
    						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
    						Criterion c3 = Restrictions.or(c2, criterion1);
    						c.add(c3);
    					}else
    						c.add(c2);
    				}
    			}
    			
    			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
    				criteria.createCriteria("teacherId")
    				.add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
    				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
    				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
    				.addOrder(sortOrderStrVal);
    				criteria.setProjection(Projections.groupProperty("teacherId"));
    				criteria.setFirstResult(start);
    				criteria.setMaxResults(end);

    				lstTeacherDetail =  criteria.list();
    			}else{
    				lstTeacherDetail=new ArrayList<TeacherDetail>();
    			}
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}	
    		return lstTeacherDetail;
    	}
    	
    	@Transactional(readOnly=false)
    	public List<TeacherDetail> getTeacherDetailByDASARecords1(Order sortOrderStrVal,int entityID,List<JobOrder> lstJobOrder,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,boolean dateFlag,Date fDate,Date tDate,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean status,StatusMaster statusMasterObj,String internalCandVal,boolean applieddateFlag,Date appliedfDate,Date appliedtDate,int daysVal,List<TeacherDetail> notTeacherDetails,boolean nByAflag,List<TeacherDetail> NbyATeacherList,int utype,boolean certTypeFlag,List<JobOrder> jobOrderIds,String stq,SchoolMaster schoolMaster)
    	{
    		PrintOnConsole.getJFTPrint("JFT:15");
    		List<TeacherDetail> lstTeacherDetail= new ArrayList<TeacherDetail>();
    		try{
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
    			/*Criterion criterionInterCand = Restrictions.eq("isAffilated",1);
    			if(internalCandVal==1){
    				criteria.add(criterionInterCand);
    			}*/
    			String[] candidatetyp=null;
    			if(internalCandVal!=null){
    					candidatetyp=internalCandVal.split("|");
    			}
    			List<Integer> candidatetypec=new ArrayList<Integer>();
    			if(candidatetyp!=null){
    				for(String s:candidatetyp){
    					if(s.equalsIgnoreCase("0")){
    						candidatetypec.add(1);
    						candidatetypec.add(5);
    					}else{
    						if(!s.equalsIgnoreCase("") && !s.equalsIgnoreCase("|")){
    							int val=Integer.parseInt(s);
    							candidatetypec.add(val);
    						}
    					}
    				}
    			}
    			if(candidatetypec.size()>0){
    				criteria.add(Restrictions.in("isAffilated",candidatetypec));
    			}

    			criteria.add(Restrictions.ne("status",statusMaster));

    			boolean flag=false;
    			boolean dateDistrictFlag=false;
    			if(entityID==2 && certTypeFlag && jobOrderIds.size()>0)
    			{
    				Criterion criterion1 = Restrictions.in("jobId",jobOrderIds);
    				criteria.add(criterion1);
    			}
    			
    			if((entityID==3 || entityID==4)){
    				flag=true;
    				if(lstJobOrder.size()>0)
    				{
    					Criterion criterion1 = Restrictions.in("jobId",lstJobOrder);
    					criteria.add(criterion1);
    				}
    				if(entityID==4 && dateFlag==false){
    					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    					//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    				}else if(entityID==4 && dateFlag){
    					criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    					dateDistrictFlag=true;
    					if(fDate!=null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    					}else if(fDate!=null && tDate==null){
    						criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    					}else if(fDate==null && tDate!=null){
    						criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    					}
    				}
    			}else if(dateFlag==false && districtMaster!=null &&  entityID==2){
    				flag=true;
    				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    				//criteria.createCriteria("jobId").add(Restrictions.eq("districtMaster", districtMaster));
    			}else if(dateFlag && districtMaster!=null &&  entityID==2){
    				flag=true;
    				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    				if(fDate!=null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    				}else if(fDate!=null && tDate==null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    				}else if(fDate==null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    				}
    				dateDistrictFlag=true;
    			}
    			if(dateDistrictFlag==false && dateFlag){
    				if(fDate!=null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate)).add(Restrictions.le("createdDateTime",tDate));
    				}else if(fDate!=null && tDate==null){
    					criteria.createCriteria("jobId").add(Restrictions.ge("createdDateTime",fDate));
    				}else if(fDate==null && tDate!=null){
    					criteria.createCriteria("jobId").add(Restrictions.le("createdDateTime",tDate));
    				}
    				flag=true;
    			}
    			if(status){
    				//criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
    				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId())).add(Restrictions.eq("statusId",statusMasterObj.getStatusId()));
    				if(schoolMaster!=null && statusMasterObj.getStatusShortName().equalsIgnoreCase("hird"))
    					criteria.add(Restrictions.eq("schoolMaster", schoolMaster));
    			}else{
    				criteria.createCriteria("status").add(Restrictions.ne("statusId",statusMaster.getStatusId()));
    			}

    			Calendar cal1 = Calendar.getInstance();
    			cal1.set(Calendar.HOUR_OF_DAY, 0);
    			cal1.set(Calendar.MINUTE, 0);
    			cal1.set(Calendar.SECOND, 0);
    			cal1.set(Calendar.MILLISECOND, 0);

    			Calendar cal2 = Calendar.getInstance();
    			cal2.add(Calendar.HOUR,23);
    			cal2.add(Calendar.MINUTE,59);
    			cal2.add(Calendar.SECOND,59);

    			if(daysVal==0)
    			{
    				cal1.add(Calendar.DATE, -7);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}
    			else if(daysVal==1)
    			{
    				cal1.add(Calendar.DATE, -15);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}else if(daysVal==2)
    			{
    				cal1.add(Calendar.DATE, -30);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}else if(daysVal==3)
    			{
    				cal1.add(Calendar.DATE, -60);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}
    			else if(daysVal==4)
    			{
    				cal1.add(Calendar.DATE, -90);
    				appliedfDate = cal1.getTime();
    				appliedtDate = cal2.getTime();
    			}
    			//job applied flag
    			if(appliedfDate!=null && appliedtDate!=null){
    				criteria.add(Restrictions.ge("createdDateTime",appliedfDate)).add(Restrictions.le("createdDateTime",appliedtDate));
    			}else if(appliedfDate!=null && appliedtDate==null){
    				criteria.add(Restrictions.ge("createdDateTime",appliedfDate));
    			}else if(appliedtDate!=null && appliedfDate==null){
    				criteria.add(Restrictions.le("createdDateTime",appliedtDate));
    			}
    			flag=true;
    			if(notTeacherDetails.size()>0)
    				criteria.add(Restrictions.not(Restrictions.in("teacherId", notTeacherDetails)));

    			if(teacherFlag){
    				flag=true;
    				criteria.add(Restrictions.in("teacherId", filterTeacherList));	
    			}
    			if(nByAflag){
    				criteria.add(Restrictions.not(Restrictions.in("teacherId", NbyATeacherList)));	
    			}
    			
    			if(districtMaster!=null  && utype==3)
    			{
    				StatusMaster master = districtMaster.getStatusMaster();
    				SecondaryStatus smaster = districtMaster.getSecondaryStatus();
    				if(master!=null)
    				{
    					System.out.println("master: "+master.getStatus());
    					//String sts = master.getStatus();
    					String sts = master.getStatusShortName();
    					if(sts.equals("scomp") || sts.equals("vcomp") || sts.equals("ecomp"))
    					{
    						List<StatusMaster> sList = new ArrayList<StatusMaster>();
    						sList.add(master);
    						if(sts.equals("scomp"))
    						{
    							StatusMaster st1 = new StatusMaster();
    							st1.setStatusId(17);
    							sList.add(st1);
    							
    							StatusMaster st2 = new StatusMaster();
    							st2.setStatusId(18);
    							sList.add(st2);
    							
    						}else if(sts.equals("vcomp"))
    						{
    							StatusMaster st2 = new StatusMaster();
    							st2.setStatusId(18);
    							sList.add(st2);
    						}
    						
    						/*
    						6 - Hired
    						19 - Declined
    						10 - rejected
    						7 -- Withdrew
    						*/
    						
    						StatusMaster stHird = new StatusMaster();
    						stHird.setStatusId(6);
    						sList.add(stHird);

    						StatusMaster stDeclined = new StatusMaster();
    						stDeclined.setStatusId(19);
    						sList.add(stDeclined);
    						
    						StatusMaster stRejected = new StatusMaster();
    						stRejected.setStatusId(10);
    						sList.add(stRejected);
    						
    						StatusMaster stWithdrew = new StatusMaster();
    						stWithdrew.setStatusId(7);
    						sList.add(stWithdrew);
    						
    						criteria.add(Restrictions.in("status",sList));
    					}else
    					{
    						List<StatusMaster> sList = new ArrayList<StatusMaster>();
    						sList.add(master);
    						if(sts.equals("hird"))
    						{
    							StatusMaster stDeclined = new StatusMaster();
    							stDeclined.setStatusId(19);
    							sList.add(stDeclined);
    							
    							StatusMaster stRejected = new StatusMaster();
    							stRejected.setStatusId(10);
    							sList.add(stRejected);
    							
    							StatusMaster stWithdrew = new StatusMaster();
    							stWithdrew.setStatusId(7);
    							sList.add(stWithdrew);
    						}else if(sts.equals("dcln"))
    						{
    							StatusMaster stRejected = new StatusMaster();
    							stRejected.setStatusId(10);
    							sList.add(stRejected);
    							
    							StatusMaster stWithdrew = new StatusMaster();
    							stWithdrew.setStatusId(7);
    							sList.add(stWithdrew);

    						}else if(sts.equals("rem"))
    						{
    							StatusMaster stWithdrew = new StatusMaster();
    							stWithdrew.setStatusId(7);
    							sList.add(stWithdrew);
    						}
    						criteria.add(Restrictions.in("status",sList));
    						//criteria.add(Restrictions.eq("status",master));
    					}
    				}
    				if(smaster!=null)
    				{
    					System.out.println("smaster: "+smaster.getSecondaryStatusName());
    					Criteria c = criteria.createCriteria("secondaryStatus");
    					
    					Criterion c2 = Restrictions.eq("secondaryStatusName",smaster.getSecondaryStatusName());
    					if(!stq.equals(""))
    					{
    						Criterion criterion1 = Restrictions.sqlRestriction("("+stq+")");
    						Criterion c3 = Restrictions.or(c2, criterion1);
    						c.add(c3);
    					}else
    						c.add(c2);
    				}
    			}
    			
    			if(flag && (teacherFlag && filterTeacherList.size()>0)||teacherFlag==false){
    				criteria.createCriteria("teacherId").add(Restrictions.like("firstName", firstName.trim(),MatchMode.ANYWHERE))
    				.add(Restrictions.like("lastName", lastName.trim(),MatchMode.ANYWHERE))
    				.add(Restrictions.like("emailAddress", emailAddress.trim(),MatchMode.ANYWHERE))
    				.addOrder(sortOrderStrVal);
    				criteria.setProjection(Projections.groupProperty("teacherId"));
    				lstTeacherDetail =  criteria.list();
    			}else{
    				lstTeacherDetail=new ArrayList<TeacherDetail>();
    			}
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstTeacherDetail;
    	}
    	
    	@Transactional(readOnly=false)
    	public List<Object> getJobStatusByJobCategorys(List<JobCategoryMaster> jobCategoryMasters,Integer districtId)
    	{
    		List<Object> lstJobAndTchr= null;
    		try{
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			criteria.createAlias("jobId", "jo");
   
   				if(jobCategoryMasters!=null)
   					criteria.add(Restrictions.in("jo.jobCategoryMaster", jobCategoryMasters));

   				Criterion criterion2 = Restrictions.eq("districtId",districtId);
    			criteria.add(criterion2);

    			criteria.createAlias("statusMaster", "st");
    			Criterion criterion3 = Restrictions.ne("st.statusId",3);
    			criteria.add(criterion3);
    			criteria.createAlias("jo.jobCategoryMaster", "jc");
   				criteria.setProjection( Projections.projectionList()
   						.add(Projections.groupProperty("jo.jobId"))
   						.add(Projections.property("jo.jobTitle"))
   						.add(Projections.count("jo.jobId"))
   						.add(Projections.property("jc.jobCategoryName"))   						
   						);
   				criteria.addOrder(Order.asc("jc.jobCategoryName"))
   						//.setResultTransformer(Transformers.aliasToBean(JobForTeacher.class)) 
   						;
    			lstJobAndTchr =  criteria.list();
    			//System.out.println("Criteria :::::: "+criteria);
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobAndTchr;
    		
    	}
    	@Transactional(readOnly=false)
    	//public List<JobForTeacher> findJFTForKellyDashboard_OP(String jobId,String fname,String lname,String email,BigInteger ksn,int actionSearchId,List<TeacherDetail> teacherDetailList,List<StatusMaster> statusMasterList,UserMaster userMaster,boolean globalFlag,List<JobOrder> listjJobOrders,boolean statusFlag , boolean ksnId,boolean qflag,int columnName,String sortOrderType,int start,int noOfRowInPage){
    	public List<JobForTeacher> findJFTForKellyDashboard_OP(String jobId,String fname,String lname,String email,BigInteger ksn,int actionSearchId,List<TeacherDetail> teacherDetailList,List<StatusMaster> statusMasterList,UserMaster userMaster,boolean globalFlag,List<JobOrder> listjJobOrders,boolean statusFlag , boolean ksnId,boolean qflag,Integer branchId,boolean branchFlag,int columnName,String sortOrderType){
    			
    		System.out.println("::::::::::::::::findJFTForKellyDashboard_OP:::::::::::::::::::::::");
    		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    			try 
    			{
    				Session session 		= 	getSession();
    				Criteria criteria 		= 	session.createCriteria(getPersistentClass());
    				String teacherIdList="";
    				if(globalFlag)
    				{/*
    					if((teacherDetailList!=null && teacherDetailList.size()>0) ){
    						System.out.println("globalFlag Inside ::::::::::::::");
    						for (TeacherDetail teacherDetail : teacherDetailList) {
        						if(!teacherIdList.equalsIgnoreCase(""))
        							teacherIdList+=","+teacherDetail.getTeacherId();
        						else
        							teacherIdList+=teacherDetail.getTeac
        							herId();
    						}
    					}else{
    						System.out.println("GlobalFlag Else ::::::::::::::");
    						//return lstJobForTeacher;
    					}
    				*/}
    				System.out.println("globalFlag:::::::::::::::::::::::::::::::::::"+globalFlag);
    				try{
    					List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
    					Map<Integer, StatusMaster> mapStatus = new HashMap<Integer, StatusMaster>();
    					for (StatusMaster sMaster : statusMasterLst) {
    						mapStatus.put(sMaster.getStatusId(),sMaster);
    					}
    					
    					String allCondidtions="";
    					if(userMaster.getHeadQuarterMaster()!=null && userMaster.getBranchMaster()==null){
    						allCondidtions+="job.headQuarterId="+userMaster.getHeadQuarterMaster().getHeadQuarterId();
    					}else if(userMaster.getBranchMaster()!=null){
    				    	allCondidtions+="job.branchId="+userMaster.getBranchMaster().getBranchId();
    					}else{
    						allCondidtions+="jft.jobId IS NOT NULL ";
    					}
    					
    					if(branchFlag)
    				    	allCondidtions+=" and job.branchId="+branchId;
    					
    					
    						
    					
    					if(statusMasterList.size()>0){
    						String statusList="";
        					for (StatusMaster statusMasterObj : statusMasterList) {
        						if(!statusList.equalsIgnoreCase(""))
        							statusList+=","+statusMasterObj.getStatusId();
        						else
        							statusList+=statusMasterObj.getStatusId();
    						}
        					if((statusMasterList.size()>0 || statusFlag) && (!ksnId | !qflag)){
        						//allCondidtions+= " and (jft.status in ("+statusList+"))";
        				    }else if(ksnId && qflag){
        				    	//allCondidtions+= " and (jft.status not in ("+statusList+")) ";
        				    }
    					}
    					allCondidtions+= " and jft.status!=8 ";
    					if(teacherIdList!=""){
    						allCondidtions+= " and jft.teacherId in ("+teacherIdList+")";
    					}
    					System.out.println("globalFlag:: "+globalFlag);
    					if(globalFlag==false){
    						if(listjJobOrders.size()>0){
        						String jobList="";
            					for (JobOrder jobOrder : listjJobOrders) {
            						if(!jobList.equalsIgnoreCase(""))
            							jobList+=","+jobOrder.getJobId();
            						else
            							jobList+=jobOrder.getJobId();
        						}
        						allCondidtions+= " and jft.jobId in ("+jobList+")";
        					}
    					}else{
    							String jobList="";
            					for (JobOrder jobOrder : listjJobOrders) {
            						if(!jobList.equalsIgnoreCase(""))
            							jobList+=","+jobOrder.getJobId();
            						else
            							jobList+=jobOrder.getJobId();
            					}
            					allCondidtions+= " and jft.jobId in ("+jobList+")";
    					}
    					
    					
    					Connection connection =null;
    					String sql="SELECT jft.*,job.jobTitle AS jobTitle,job.createdForEntity as createdForEntity,tdl.firstName as firstName,tdl.lastName as lastName,tdl.emailAddress,tdl.KSNID,job.jobCategoryId as jobCategoryId,jcat.parentJobCategoryId as parentJobCategoryId,jcat.preHireSmartPractices as preHireSmartPractices,jcat.JobCategoryName as JobCategoryName,jcat2.JobCategoryName as parentJobCategoryName,"+ 
    					"status1.statusShortName,status1.status as statusFeildName";
    					
    					/*if(actionSearchId==2){
    						sql+=",passCnt,failCnt";
    					}*/
    					
    					sql+=" FROM jobforteacher jft "+ 
    			        " INNER JOIN statusmaster status1 ON status1.statusId = jft.status "+	
    			        " INNER JOIN teacherdetail tdl ON tdl.teacherId = jft.teacherId "+
    			        " INNER JOIN joborder job ON job.jobId = jft.jobId "+
    			        " INNER JOIN jobcategorymaster jcat ON jcat.jobCategoryId=job.jobCategoryId "+
    			        " LEFT JOIN jobcategorymaster jcat2 ON jcat.parentJobCategoryId=jcat2.jobCategoryId ";

    					/*if(actionSearchId==2){
    						sql+=" Left join (SELECT COUNT(IF(t.pass='P',1,NULL)) AS passCnt,COUNT(IF(t.pass='F',1,NULL)) AS failCnt,t.teacherId "+
    			        		 " FROM `teacherassessmentstatus` t group by t.teacherId) tt on tt.teacherId=jft.teacherId";
    					}*/
    			        
    					sql+=" WHERE "+allCondidtions;
    					// fname,lname,email,ksn
    					//////////////
    					if((fname!=null && !fname.equals("")) || (lname!=null && !lname.equals("")) || (email!=null && !email.equals("")) || (ksn!=null && !ksn.equals(""))){
    						
    							if((fname!=null && !fname.equals(""))&&(lname!=null && !lname.equals(""))){
    	    						//criteria.add(Restrictions.like("firstName","%"+teacherFNames+"%")).add(Restrictions.like("lastName","%"+teacherLNames+"%"));
    	    						sql+=" and tdl.firstName like '%"+fname+"%' and tdl.lastName like '%"+lname+"%' ";
    	    					}else if(fname!=null && !fname.equals("")){
    	    						//criteria.add(Restrictions.like("firstName","%"+fname+"%"));
    	    						sql+=" and tdl.firstName like '%"+fname+"%' ";
    	    					}else if(lname!=null && !lname.equals("")){
    	    						//criteria.add(Restrictions.like("lastName","%"+lname+"%"));
    	    						sql+=" and tdl.lastName like '%"+lname+"%' ";
    	    					}	
    	    					if(email!= null && !email.equals("")){
    	    					//	criteria.add(Restrictions.like("emailAddress","%"+email+"%"));
    	    						sql+=" and tdl.emailAddress like '%"+email+"%' ";
    	    					}
    	    					if(ksn!= null && !ksn.equals("")){
    	    						//criteria.add(Restrictions.eq("KSNID", ksnId));
    	    						sql+=" and tdl.KSNID like '%"+ksn+"%' ";
    	    					}		
    					}
    					if(!jobId.equals("") && !jobId.equals("0")){
    						sql+=" and jft.jobId ="+jobId+" ";
    					}
    					
    					/*
    					 * for sorting column
    					 * 0 -> Teacher Last Name
    					 * 1 -> KSNID
    					 * 2 -> Job Title
    					 * 3 -> Job Category Master
    					 * 4 -> Job Id 
    					 * */
    					if(columnName==0)
    						sql+=" ORDER BY  tdl.lastName "; 
    					else if(columnName==1)
    						sql+=" ORDER BY  tdl.KSNID ";
    					else if(columnName==2)
    						sql+=" ORDER BY  job.jobTitle ";
    					else if(columnName==3)
    						sql+=" ORDER BY  jcat.JobCategoryName ";
    					else if(columnName==4)
    						sql+=" ORDER BY  jft.jobId ";
    					else if(columnName==5)
    						sortOrderType = "";
    					
    					
    					if(sortOrderType!=null && !sortOrderType.equals("")){
    						if(sortOrderType.equalsIgnoreCase("0"))
    							sql+="ASC";
    						else if(sortOrderType.equalsIgnoreCase("1"))
    							sql+="DESC";
    					}
    						
    				//	sql+=" limit "+start+","+noOfRowInPage+" ";
    					
    					//////////////
    					/*if(actionSearchId==2){
    						sql+=" and (passCnt=0 or passCnt IS NULL)";
    					}*/
    					System.out.println("sql  "+sql);
    					SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
    				    ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
    				    connection = connectionProvider.getConnection();
    					PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE); 
    					  ResultSet rs=ps.executeQuery();
    					  rs.last();
    					  int rowcount=rs.getRow();
    					  rs.beforeFirst();
    					  if(rs.next()){				  
    						    do{
    						     final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
    						     JobForTeacher tempJft = new JobForTeacher();
    						     	tempJft.setJobForTeacherId(Long.parseLong(rs.getString(rs.findColumn("jobForTeacherId"))));//jobForTeacherId
    						     	TeacherDetail tDetail = new TeacherDetail();
    						     	tDetail.setTeacherId(Integer.parseInt(rs.getString(rs.findColumn("teacherId"))));
    						     	tDetail.setFirstName(rs.getString("firstName"));
    						     	if(rs.getString("KSNID")!=null){
    						     		tDetail.setKSNID(new BigInteger(rs.getString("KSNID")));
    						     	}
    						     	tDetail.setLastName(rs.getString("lastName"));
    						     	tDetail.setEmailAddress(rs.getString("emailAddress"));
    						     	tempJft.setTeacherId(tDetail);
    						     	JobOrder tempJobOrder = new JobOrder();
    						     	tempJobOrder.setJobId(Integer.parseInt(rs.getString(rs.findColumn("jobId"))));
    						     	tempJobOrder.setJobTitle(rs.getString(rs.findColumn("jobTitle")));
    						     	tempJobOrder.setCreatedForEntity(rs.getInt(rs.findColumn("createdForEntity")));
    						     	JobCategoryMaster jobCategoryMaster = new JobCategoryMaster();
    						     	jobCategoryMaster.setJobCategoryId(Integer.parseInt(rs.getString(rs.findColumn("jobCategoryId"))));					     			
    						     	if(rs.getString(rs.findColumn("parentJobCategoryId"))!=null){
    						     		JobCategoryMaster parJobCategoryMaster = new JobCategoryMaster();
    						     		parJobCategoryMaster.setJobCategoryId(Integer.parseInt(rs.getString(rs.findColumn("parentJobCategoryId"))));
    						     		parJobCategoryMaster.setJobCategoryName(rs.getString(rs.findColumn("parentJobCategoryName")));
    						     		jobCategoryMaster.setParentJobCategoryId(parJobCategoryMaster);
    						     		jobCategoryMaster.setPreHireSmartPractices(rs.getBoolean("preHireSmartPractices"));    						     	
    						     	}
    						     	jobCategoryMaster.setJobCategoryName(rs.getString("jobCategoryName"));
    						     	tempJobOrder.setJobCategoryMaster(jobCategoryMaster);
    						     	tempJft.setRedirectedFromURL(rs.getString("redirectedFromURL"));
    						     	tempJft.setJobId(tempJobOrder);
    						     	tempJft.setRedirectedFromURL(rs.getString("redirectedFromURL"));
    						     	tempJft.setJobBoardReferralURL(rs.getString("jobBoardReferralURL"));
    						     	StatusMaster tempStatus = null;					     	
    						     	if(rs.getString("status")!=null){
    						     		tempStatus = mapStatus.get(Integer.parseInt(rs.getString("status"))); 
    						     	}
    						     	tempJft.setStatus(tempStatus);
    						     	StatusMaster tempStatus2 = null;
    						     	if(rs.getString("displayStatusId")!=null){
    						     		tempStatus2 = mapStatus.get(Integer.parseInt(rs.getString("displayStatusId")));
    						     	}
    						     	tempJft.setStatusMaster(tempStatus2);
    						     	if(rs.getString("cgUpdated")!=null)
    						     		tempJft.setCgUpdated(rs.getBoolean("cgUpdated"));
    						     	tempJft.setCoverLetter(rs.getString("coverLetter"));
    						     	tempJft.setCoverLetterFileName(rs.getString("coverLetterFileName"));
    						     	if(rs.getString("isAffilated")!=null)
    						     		tempJft.setIsAffilated(Integer.parseInt(rs.getString("isAffilated")));
    						     	
    						     	if(rs.getString("applicationStatus")!=null)
    						     		tempJft.setApplicationStatus(Integer.parseInt(rs.getString("applicationStatus")));
    						     	
    						     	lstJobForTeacher.add(tempJft);
    						     
    						    }while(rs.next());
    						   }
    						   else{
    						    System.out.println("Record not found ......");
    						   }
    					} 
    				catch (Exception e) {
    					e.printStackTrace();
    				}		
    				System.out.println("lstJobForTeacher:::::::::::::::::"+lstJobForTeacher.size());
    			} 
    			catch (Exception e) {
    				e.printStackTrace();
    			}		
    			return lstJobForTeacher;
    		}
    	
    	//SWADESH
    	@Transactional(readOnly=false)
    	public List<JobForTeacher> getReasonAndTiming(DistrictMaster districtMaster,String searchbystatus)
    	{
    		List<JobForTeacher> lstJobAndTchr= null;
    		StatusMaster statusMaster=new StatusMaster();
    		StatusMaster statusMaster1=new StatusMaster();
    		statusMaster= WorkThreadServlet.statusMap.get("widrw");
    		statusMaster1=WorkThreadServlet.statusMap.get("dcln");
    		try{
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			Criterion criterion=null;
    			Criterion criterion1=null;
    			if(districtMaster!=null)
   				criterion = Restrictions.eq("districtId",districtMaster.getDistrictId());
    			if(searchbystatus.equals("0"))
    				criterion1=Restrictions.or(Restrictions.eq("statusMaster",statusMaster), Restrictions.eq("statusMaster",statusMaster1));
    	    	else if(searchbystatus.equals("7"))
    	    		criterion1=Restrictions.eq("statusMaster",statusMaster);
    	    	else if(searchbystatus.equals("19"))
    	    		criterion1=Restrictions.eq("statusMaster",statusMaster1);
   				if(districtMaster!=null)
   				criteria.add(criterion);
    			criteria.add(criterion1);
    			lstJobAndTchr =  criteria.list();
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobAndTchr;
    	}
    	
    	@Transactional(readOnly=false)
    	public List<TeacherDetail> findRecordForNotReviewdCandidateByFlag(DistrictMaster districtMaster,JobOrder jobOrder)
    	{
    		List<TeacherDetail> lstJobForTeacher= new ArrayList<TeacherDetail>();
    		try 
    		{
    			Session session = getSession();
    			Criteria criteria = session.createCriteria(getPersistentClass());
    			
    			if(districtMaster!=null)
    				criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
    			
    			Criterion criterion1 = Restrictions.eq("jobId",jobOrder);
    			criteria.add(criterion1);
    			
    			Criterion criterion2 = Restrictions.eq("notReviewedFlag",true);
    			criteria.add(criterion2);
    			
    			Criterion criterion3 = Restrictions.isNull("lastActivity");
    			criteria.add(criterion3);
    			
    			Criterion criterion4 = Restrictions.isNull("secondaryStatus");
    			criteria.add(criterion4);
    			
    			criteria.setProjection( Projections.projectionList()
    					.add(Projections.property("teacherId"))
    					//.add(Projections.property("notReviewedFlag"))
    					);
    		
    			lstJobForTeacher =  criteria.list();
    			
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return lstJobForTeacher;
    	} 
    	
    	// Optimization of UserDashboard
    	
    	@Transactional(readOnly=false)
    	public Integer findJobByTeacherOneYear_Op(TeacherDetail teacherDetail)
    	{
    		 
    		List<Object> result =new ArrayList<Object>();
    		try 
    		{
    			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
    			Date sDate= null;
    			Date eDate= null;
    			
    			/*Calendar cal = Calendar.getInstance();
    			cal.set(cal.get(cal.YEAR),0,1,0,0,0);
    			sDate=cal.getTime();
    			cal.set(cal.get(cal.YEAR),11,31,23,59,59);
    			eDate = cal.getTime();*/
    			
    			
    			Calendar cal1 = Calendar.getInstance();
    			cal1.set(Calendar.HOUR_OF_DAY, 0);
    			cal1.set(Calendar.MINUTE, 0);
    			cal1.set(Calendar.SECOND, 0);
    			cal1.set(Calendar.MILLISECOND, 0);

    			Calendar cal2 = Calendar.getInstance();
    			cal2.add(Calendar.HOUR,23);
    			cal2.add(Calendar.MINUTE,59);
    			cal2.add(Calendar.SECOND,59);

    			cal1.add(Calendar.DATE, -365);
    			sDate = cal1.getTime();
    			eDate = cal2.getTime();
    			
    			Criterion criterion2 = Restrictions.between("createdDateTime", sDate, eDate);
    			
    		  Criteria criteria = this.getSession().createCriteria(this.getPersistentClass());
    		  criteria.add(criterion1).add(criterion2);
    		  criteria.setProjection(Projections.count("jobForTeacherId"));
    		  result = criteria.list();
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return (Integer) result.get(0);
    	}
    	
    	// Optimization for userDashboard
    	
    	@Transactional(readOnly=false)
    	public Integer countCandidateConsideration(TeacherDetail teacherDetail)
    	{
    		Integer result= 0;
    		try 
    		{
    			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
    			Criteria criteria = this.getSession().createCriteria(this.getPersistentClass());
    			criteria.add(criterion1);
    			criteria.add(Restrictions.isNotNull("candidateConsideration"));
    			criteria.setProjection(Projections.count("jobForTeacherId"));
    			result =(Integer) criteria.list().get(0);		
    		} 
    		catch (Exception e) {
    			e.printStackTrace();
    		}		
    		return result;
    	}
    	
    	//userdashboard
    	
    	   @Transactional(readOnly=false)
    		public Integer findJobByTeacherNotKES_Op(TeacherDetail teacherDetail)
    		{
    			Integer result = 0;
    	
    			try 
    			{
    				Session session = getSession();
    				Criteria criteria = session.createCriteria(getPersistentClass());
    				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
    				Criteria jobOrderSet= criteria.createCriteria("jobId");
    				jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
    				criteria.add(criterion1);
    				criteria.setProjection(Projections.count("jobForTeacherId"));
    				result = (Integer) criteria.list().get(0);
    			} 
    			catch (Exception e) {
    				e.printStackTrace();
    			}		
    			return result;
    		}
    	
// UserDash board controller
    	   
    	   @Transactional(readOnly=false)
    		public Integer findJobByTeacherForKES_Op(TeacherDetail teacherDetail,boolean flag)
    		{
    		Integer result  =  0;
    			try 
    			{
    				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
    				Session session = getSession();
    				Criteria criteria = session.createCriteria(getPersistentClass());
    				Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
    				Criteria jobOrderSet= criteria.createCriteria("jobId");
    				
    				jobOrderSet.add(Restrictions.isNotNull("headQuarterMaster"));
    				criteria.add(criterion1);
    				if(flag)
    					criteria.add(Restrictions.eq("statusMaster",statusMaster));
    				
    				criteria.setProjection(Projections.count("jobForTeacherId"));
    				result = (Integer) criteria.list().get(0);
    			} 
    			catch (Exception e) {
    				e.printStackTrace();
    			}		
    			return result;
    		}
    	
 
    	
    	@Transactional(readOnly=false)
    	public JobForTeacher jobForTeacherByTeacherAndJob(TeacherDetail teacherDetail,JobOrder jobOrder){
    		JobForTeacher jobForTeacher = null;
    		try {
    			Session session = getSession();
				Criteria criteria = session.createCriteria(JobForTeacher.class);
				
				if(teacherDetail!=null)
					criteria.add(Restrictions.eq("teacherId", teacherDetail));
				if(jobOrder!=null)
					criteria.add(Restrictions.eq("jobId", jobOrder));
				
				criteria.createAlias("secondaryStatus", "secondarystatus",criteria.LEFT_JOIN).createAlias("schoolMaster", "schoolmaster",criteria.LEFT_JOIN)
				.setProjection( Projections.projectionList()
						.add(Projections.property("jobForTeacherId"))
    					.add(Projections.property("requisitionNumber"))
    					.add(Projections.property("startDate"))
    					.add(Projections.property("secondarystatus.secondaryStatusId"))
    					.add(Projections.property("secondarystatus.secondaryStatusName"))
    					.add(Projections.property("schoolmaster.schoolId"))
    					.add(Projections.property("schoolmaster.schoolName"))
    					);
				JobForTeacher jftObj = new JobForTeacher();
				List results = criteria.list();
				if(results.size()>0){
					Object[] row = (Object[]) results.get(0);
					jftObj.setJobForTeacherId(new Long(row[0].toString()));
					jftObj.setTeacherId(teacherDetail);
					jftObj.setJobId(jobOrder);
					if(jobOrder!=null && jobOrder.getDistrictMaster()!=null)
					jftObj.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
					if(row[1]!=null)
					jftObj.setRequisitionNumber(row[1].toString());
	
					if(row[2]!=null){
						DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
						Date newDate = df.parse(row[2].toString());
						jftObj.setStartDate(newDate);
					}
					if(row[3]!=null){
						SecondaryStatus secStatus = new SecondaryStatus();
						secStatus.setSecondaryStatusId(Integer.parseInt(row[3].toString()));
						secStatus.setSecondaryStatusName(row[4].toString());
						jftObj.setSecondaryStatus(secStatus);
					}
					if(row[5]!=null){
						SchoolMaster scm = new SchoolMaster();
						scm.setSchoolId(new Long(row[5].toString()));
						scm.setSchoolName(row[6].toString());
						jftObj.setSchoolMaster(scm);
					}
					jobForTeacher =jftObj; 
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
    		return jobForTeacher;
    	}

    	@Transactional(readOnly=false)
    	public List<JobForTeacher> getAllJobByGeoZone(Integer districtId,String jobTitle,TeacherDetail teacherDetail){
    		
    		List<JobForTeacher> jobForTeacherList = new ArrayList<JobForTeacher>();
    		try {
    				Session session = getSession();
    				Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo");
    				
    				 jobTitle = jobTitle.replace("(Mid County)","");
    				 jobTitle = jobTitle.replace("(North County)","");
    				 jobTitle = jobTitle.replace("(South County)","");
    				
    				if(districtId!=null)
    					criteria.add(Restrictions.eq("districtId", districtId));
    				
    				Criterion criterion1 = Restrictions.like("jo.jobTitle",jobTitle,MatchMode.ANYWHERE);
    				criteria.add(criterion1);
    				
    				Criterion criterion2 = Restrictions.eq("teacherId",teacherDetail);
    				criteria.add(criterion2);
    				
    				jobForTeacherList =  criteria.list();
    			
    		} catch(Exception exception){
    			exception.printStackTrace();
    		}
    		
    		return jobForTeacherList;
    	}
    	
    	//Optimized function for personal planning  indrajeet
    	 @Transactional(readOnly=false)
    		public List<Object> findJobListForJobSpecificInventory_Op(TeacherDetail teacherDetail,Boolean assesmentStatus)
    		{
    			PrintOnConsole.getJFTPrint("JFT:last");
    			List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
    			List<Object> lstJobForTeacher1= null;
    			try 
    			{
    				Date dateWithoutTime = Utility.getDateWithoutTime();
    				
    				Session session = getSession();
    				Criteria criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo").createAlias("jo.jobCategoryMaster", "joj");
    				criteria.add(Restrictions.eq("teacherId", teacherDetail));
    				criteria.add(Restrictions.eq("jo.isJobAssessment", assesmentStatus))
    				.add(Restrictions.le("jo.jobStartDate",dateWithoutTime))
    				.add(Restrictions.ge("jo.jobEndDate",dateWithoutTime))
    				.add(Restrictions.eq("jo.status", "A"));
    				criteria.setProjection(Projections.projectionList()
    						.add(Projections.property("jo.jobId")) //0
    						.add(Projections.property("joj.baseStatus"))); //1
    				lstJobForTeacher1 = criteria.list();

    				System.out.println("lstJobForTeacher opt..."+lstJobForTeacher1.size());
    				/*for(Object ta:lstJobForTeacher1)
    				{
    					System.out.println("JobId....."+ta.toString());
    				  try{Object  row[]= (Object[])ta;
    				  System.out.println("JobId....."+row[0]+"sm.statusShortName....."+row[1]);
    				  }catch(Exception e){e.printStackTrace();}
    				}*/
    			} 
    			catch (Exception e) {
    				e.printStackTrace();
    			}		
    			return lstJobForTeacher1;
    		}
    	 
    	 // Optimized function for Personal Planning
    	 	@Transactional(readOnly=false)
    	   public   Integer findAllAppliedJobsOfTeacher_Op(Integer teacherId)
    	   {
    	 		Integer appliedJobCount=0;
    			try{
    				 Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
    				 Criteria crit = getSession().createCriteria(getPersistentClass());
    				 crit.add(Restrictions.eq("teacherId.teacherId",teacherId));
    				 crit.setProjection(Projections.rowCount());
    				 appliedJobCount=(Integer) crit.uniqueResult();
    				 // lstJobForTeacher2=findByCriteria(criterion);
    				// lstJobForTeacher2=crit.list();
    				 System.out.println("Active Job List optmized.."+appliedJobCount);
    			}catch(Exception e){
    				
    					e.printStackTrace();
    			}
    			
    		  return appliedJobCount;
    	   }
    	 	 
    	 // Optimized Function For personal planning indrajeet	
    	 	@Transactional(readOnly=false)
    	 	   public  Integer  getEpiStatusByTeacher_Op(Integer teacherId)
    	 	   {
    	 		   PrintOnConsole.getJFTPrint("JFT:97");	
    	 	      List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
    	 	      Integer Epistatuscount=null;
    	 			try{
    	 				
    	 				      Session session = getSession();
    	 				      
    	 				      Criteria criteria = session.createCriteria(getPersistentClass());
    	 				      Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
    	 				      Criterion criterion1 =Restrictions.eq("baseStatus", true);
    	 				      Criterion criterion2 =Restrictions.eq("epiForFullTimeTeachers",true);
    	 				      Criterion criterion3 =Restrictions.or(criterion1, criterion2);
    	 				      criteria.add(criterion);
    	 				      Criteria c1 = criteria.createCriteria("jobId").add(Restrictions.eq("status","A"));
    	 				      c1.createCriteria("jobCategoryMaster").add(criterion3);
    	 				      criteria.setProjection(Projections.rowCount());
    	    				 Epistatuscount=(Integer) criteria.uniqueResult();
    	    				   // lstJobForTeacher2 = criteria.list();
    	    				  System.out.println("EPI Status Count optmized..."+Epistatuscount);
    	 				}catch(Exception e){
    	 				
    	 					e.printStackTrace();
    	 			}
    	 			
    	 		  return Epistatuscount;
    	 	   }
    	 	
    	//Optimized function for personal planning 	
    	 	@Transactional(readOnly=false)
    	    public   List <JobForTeacher>  getSPStatusByTeacher_Op(Integer teacherId)
    	    {
    	       List<JobForTeacher> lstJobForTeacher2= new ArrayList<JobForTeacher>();
    	 		try{
    	 			
    	 		      Session session = getSession();
    	 		      
    	 		      Criteria criteria = session.createCriteria(getPersistentClass());
    	 		      Criterion criterion =Restrictions.eq("teacherId.teacherId",teacherId);
    	 		      criteria.add(criterion);
    	 		      Criteria c1 = criteria.createCriteria("jobId").add(Restrictions.eq("status","A"))
    	 		      .createCriteria("jobCategoryMaster").add(Restrictions.eq("preHireSmartPractices", true))
    	 		      //.createCriteria("districtMaster").add(Restrictions.eq("districtId", 5509600))
    	 		      .setFirstResult(0)
    	 		      .setMaxResults(1);
    	 			
    	 			lstJobForTeacher2 = criteria.list();
    	 			System.out.println("getSPStatus Size..."+lstJobForTeacher2.size());
    	 			}catch(Exception e){
    	 			
    	 				e.printStackTrace();
    	 		}
    	 		
    	 	  return lstJobForTeacher2;
    	    }
    	 	//nadeem
    		 @Transactional(readOnly=false)
    	 	public JobForTeacher findJobByTeacherAndJobCategoryna(TeacherDetail teacherDetail,JobOrder jobOrder)
    	 	{
    	    		JobForTeacher jobForTeacher = null;
    	    		try {
    	    			Session session = getSession();
    					Criteria criteria = session.createCriteria(JobForTeacher.class);    					
    					if(teacherDetail!=null)
    						criteria.add(Restrictions.eq("teacherId", teacherDetail));
    					if(jobOrder!=null)
    						criteria.add(Restrictions.eq("jobId", jobOrder));    					
    					criteria.setProjection( Projections.projectionList()
    							.add(Projections.property("jobForTeacherId"))
    							.add(Projections.property("status")) 
    							.add(Projections.property("statusMaster"))    	    					
    	    					.add(Projections.property("secondaryStatus"))      	    					
    	    					.add(Projections.property("updatedBy"))
    	    					.add(Projections.property("updatedByEntity"))
    	    					.add(Projections.property("updatedDate"))    	    			
    	    					.add(Projections.property("userMaster"))//lastActivityDoneBy
    	    					.add(Projections.property("createdDateTime"))
    	    					);
    					JobForTeacher jftObj = new JobForTeacher();
    					List results = criteria.list();
    					if(results.size()>0){
    						Object[] row = (Object[]) results.get(0);
    						jftObj.setJobForTeacherId(new Long(row[0].toString()));
    						jftObj.setTeacherId(teacherDetail);
    						jftObj.setJobId(jobOrder);  
    						if(row[1]!=null){
    							jftObj.setStatus((StatusMaster) row[1]);    						
    						}
    						if(row[2]!=null){
    							jftObj.setStatusMaster((StatusMaster) row[2]);    						
    						}
    						if(row[3]!=null){    						
    							jftObj.setSecondaryStatus((SecondaryStatus) row[3]);
    						}    					
    						if(row[4]!=null){
    							jftObj.setUpdatedBy((UserMaster) row[4]);
    						}
    						if(row[5]!=null){
    							jftObj.setUpdatedByEntity(Integer.parseInt(row[5].toString()));
    						}
    						if(row[6]!=null){
    							jftObj.setLastActivity(row[6].toString());
    						}
    						if(row[7]!=null){
    							jftObj.setUserMaster((UserMaster) row[7]);
    						}
    						if(row[8]!=null){
    							DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    							Date newDate = df.parse(row[8].toString());
    							jftObj.setCreatedDateTime(newDate);
    						}
    						jobForTeacher =jftObj; 
    					}
    				} catch (Exception e) {
    					e.printStackTrace();
    				}
    	    		return jobForTeacher;
    	    	}
    	    	    
    	    @Transactional(readOnly=false)
	public List<JobForTeacher> findJFTApplicantbyJobOrder_OP(JobOrder jobOrder)
	{
		List<JobForTeacher> jobForTeacherList= new ArrayList<JobForTeacher>();
		try 
		{
  			Query query1=null;
			if(jobOrder!=null){
 				Session session = getSession();
 				StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
 				String hql=	"SELECT   jft.jobForTeacherId,jft.requisitionNumber,td.teacherId,td.firstName,td.lastName  FROM  jobforteacher jft "
			 	    +   "left join joborder jo on jft.jobId= jo.jobId left join teacherdetail td on td.teacherId=jft.teacherId "
				    + " WHERE   jft.jobId=? and jft.status!=? ";
 				    hql=hql+ "order by jft.createdDateTime ASC ";
 				  
				query1=session.createSQLQuery(hql);
 				query1.setParameter(0, jobOrder.getJobId());
 				query1.setParameter(1, statusMaster);
 				List<Object[]> objList = query1.list(); 
 				JobForTeacher obj=null;
				if(objList.size()>0){
				for (int j = 0; j < objList.size(); j++) {
						Object[] objArr2 = objList.get(j);
						obj = new JobForTeacher();
					 	obj.setJobForTeacherId(objArr2[0] == null ? 0 : Long.parseLong(objArr2[0].toString()));
					    obj.setRequisitionNumber(objArr2[1] == null ? "NA" :  objArr2[1].toString());
					   int  teacherId=objArr2[2] == null ? 0 : Integer.parseInt(objArr2[2].toString());
					    TeacherDetail teacherIdObj=new TeacherDetail();
					    if(teacherId!=0){
					    	teacherIdObj.setTeacherId(teacherId);
					    	obj.setTeacherId(teacherIdObj);
					     }
					    obj.setFirstName(objArr2[3] == null ? "NA" :  objArr2[3].toString());
					    obj.setLastName(objArr2[4] == null ? "NA" :  objArr2[4].toString());
					    jobForTeacherList.add(obj);
				}
				}
			 
			}
		} 
	 	catch(Exception e){
			   
		         e.printStackTrace(); 
		}finally{
			// session.close(); 
		}
		return jobForTeacherList;
	}
    	 	/*
    	 	 * Created by deepak 
    	 	 * for the Candidate Pool
    	 	 */
    		@Transactional(readOnly=false)
    		public List<String[]> findDistrictByTeacherIds(List<TeacherDetail> teacherDetailsist)
    		{
    			List<String[]> lstJobForTeacher = new ArrayList<String[]>();
    			Session session = getSession();
    			try{
    				if(teacherDetailsist.size()>0){
    					Criteria  criteria = session.createCriteria(getPersistentClass()).createAlias("jobId", "jo").createAlias("jo.jobCategoryMaster", "jc");
     					criteria.setProjection(Projections.projectionList()
    							.add(Projections.groupProperty("districtId"))
    							.add(Projections.property("teacherId.teacherId"))
    							.add(Projections.property("jobId.jobId"))
     							.add(Projections.groupProperty("jc.jobCategoryId")));
    					lstJobForTeacher = criteria.add(Restrictions.in("teacherId",teacherDetailsist)) 
    					.list();
    				}
    				/*for (Iterator it = lstJobForTeacher.iterator(); it.hasNext();){ 
						Object[] row = (Object[]) it.next(); 
						if(row[3]!=null){
							System.out.println("Job Category ID ::::"+row[3]);
						}
					}*/
    			} 
    			catch (Exception e) {
    				e.printStackTrace();
    			}		
    			return lstJobForTeacher;
    		}
    		
    		/**
    		 * for kelly API
    		 * @param teacherDetail
    		 * @param headQuarterMaster
    		 * @param branchMaster
    		 * @return
    		 */
    		@Transactional(readOnly=false)	
    	 	public List<JobForTeacher> findByTeacherIdHQAndBranch_OP(TeacherDetail teacherDetail,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster) throws Exception 
    	 	{	
    	 		PrintOnConsole.getJFTPrint("findByTeacherIdHQAndBranch_OP :::: JFT ");
    	 		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();

    	 		try
    	 		{
    	 			Session session = getSession();
    	 			Criteria criteria = session.createCriteria(getPersistentClass());
    	 			Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);	
    	 			criteria.add(criterion1);
    	 			if(headQuarterMaster!=null && branchMaster!=null)
    	 				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
    	 			else if(headQuarterMaster!=null)
    	 				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
    	 			else if(branchMaster!=null)
    	 			    criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));

    	 			criteria.addOrder(Order.desc("createdDateTime"));
    	 			//lstJobForTeachers = criteria.list();
    	 			
    	 			criteria.createAlias("status", "stMaster").createAlias("secondaryStatus", "secondarystatus",criteria.LEFT_JOIN)
    				.setProjection( Projections.projectionList()
    						.add(Projections.property("jobForTeacherId"))
    						.add(Projections.property("stMaster.statusId"))
        					.add(Projections.property("secondarystatus.secondaryStatusId"))
        					.add(Projections.property("secondarystatus.secondaryStatusName"))
        					.add(Projections.property("jobId.jobId"))
        					.add(Projections.property("stMaster.statusShortName"))
        					.add(Projections.property("createdDateTime"))
        					.add(Projections.property("applicationStatus"))
        					);
    				
    				List results = criteria.list();
    				if(results!=null && results.size()>0){
    					for(Iterator it =results.iterator();it.hasNext();){
    						JobForTeacher jftObj = new JobForTeacher();
    						Object[] row = (Object[]) it.next();
    						jftObj.setJobForTeacherId(new Long(row[0].toString()));
    						if(row[1]!=null){
    							StatusMaster status = new StatusMaster();
    							status.setStatusId(Integer.parseInt(row[1].toString()));
    							status.setStatusShortName(row[5].toString());
    							jftObj.setStatus(status);
    						}
    						if(row[2]!=null){
    							SecondaryStatus secStatus = new SecondaryStatus();
    							secStatus.setSecondaryStatusId(Integer.parseInt(row[2].toString()));
    							secStatus.setSecondaryStatusName(row[3].toString());
    							jftObj.setSecondaryStatus(secStatus);
    						}
    						jftObj.setTeacherId(teacherDetail);
    						if(row[4]!=null){
    							JobOrder jobOrder = new JobOrder();
    							jobOrder.setJobId(Integer.parseInt(row[4].toString()));
    							jftObj.setJobId(jobOrder);
    						}
    						if(row[6]!=null){
    							DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    							Date newDate = df.parse(row[6].toString());
    							jftObj.setCreatedDateTime(newDate);
    						}
    						if(row[7]!=null){
    							jftObj.setApplicationStatus(Integer.parseInt(row[7].toString()));
    						}
    						else
    							jftObj.setApplicationStatus(null);
    						
    						lstJobForTeachers.add(jftObj);
    					}
    				} 

    	 		}catch(Exception e ){
    	 			e.printStackTrace();
    	 		}

    	 		return lstJobForTeachers;
    	 	}

    		   //Added by kumar avinash 		
    		@Transactional(readOnly=false)	
    	 	public List<JobForTeacher>  findJobByTeacherAndSatus_Op(TeacherDetail teacherDetail, StatusMaster statusMaster) throws Exception 
    	 	{	  List<Object[]> objList=null;
    	 	 	List<JobForTeacher>  lstJobForTeachers = new ArrayList<JobForTeacher>();
     	 		 try{
    	 		     Query  query=null;
    	 	 	     Session session = getSession();
    	 	  	    if(teacherDetail!=null&& statusMaster!=null){ 
    	 	     	String   hql = "SELECT   jft.jobForTeacherId,jo.isJobAssessment,jft.jobId,jcm.baseStatus  FROM  jobforteacher jft "
    	 		         +   "left join teacherdetail td on td.teacherId= jft.teacherId "
    	 		         +   "left join statusmaster sm on sm.statusId= jft.status "
    	 		        +   "left join joborder jo on jo.jobId= jft.jobId "
    	 		       +   "left join jobcategorymaster jcm on jcm.jobCategoryId=jo.jobCategoryId "
     	 		        + " WHERE   jft.teacherId=? and  jft.status=? order by jft.createdDateTime desc ";
    	 		        
    	 		       query=session.createSQLQuery(hql);
    	 		      query.setParameter(0, teacherDetail.getTeacherId());
    	 		      query.setParameter(1, statusMaster.getStatusId() );
    	 		     objList  = query.list(); 
    	 	 	         
    	 		      JobForTeacher jobForTeacher=null;
    	 		        if(objList.size()>0){
    	 		           for (int j = 0; j < objList.size(); j++) {
    	 		       	    Object[] objArr2 = objList.get(j);
    	 		         	jobForTeacher = new JobForTeacher();
    	 		         	jobForTeacher.setJobForTeacherId(objArr2[0] == null ? null : Long.parseLong(objArr2[0].toString()));
    	 		         	JobOrder jobOrder=null;
    	 		         	jobOrder=new JobOrder();
    	 		         	jobOrder.setIsJobAssessment(Boolean.parseBoolean(objArr2[1].toString()));
    	 		         	jobOrder.setJobId(objArr2[2] == null ? null : Integer.parseInt(objArr2[2].toString()));
    	 		         	if(objArr2[2]!=null){	    	 		         	
	    	 		         	JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();    	 		         	
	    	 		         	jobCategoryMaster.setBaseStatus(Boolean.parseBoolean(objArr2[3].toString()));
	    	 		         	jobOrder.setJobCategoryMaster(jobCategoryMaster);
    	 		         	}
    	 		         	jobForTeacher.setJobId(jobOrder);
    	 		         	
    	 		         	 
    	 		         	lstJobForTeachers.add(jobForTeacher );
    	 		             }
    	 		            }
    	 		          }
    	 		        }
    	 		   catch(Exception e){
    	 		    e.printStackTrace();
    	 		  }
    	 		   return lstJobForTeachers;
    	 	}

    		 //Added by kumar avinash 		
     		@Transactional(readOnly=false)	
     	 	public List<JobForTeacher>  findAllAppliedJobsOfTeacher_Opp(Integer teacherId) throws Exception 
     	 	{	  List<Object[]> objList=null;
     	 	 	List<JobForTeacher>  lstJobForTeachers = new ArrayList<JobForTeacher>();
      	 		 try{
     	 		     Query  query=null;
     	 	 	     Session session = getSession();
     	 	  	    if(teacherId!=0){ 
     	 	     	String   hql = "SELECT   jft.jobForTeacherId,hqm.headQuarterId  FROM  jobforteacher jft "
     	 		         +   "left join teacherdetail td on td.teacherId= jft.teacherId "
     	 		       +   "left join joborder jo on jo.jobId= jft.jobId "
     	 		     +   "left join headquartermaster hqm on hqm.headQuarterId= jo.headQuarterId "
     	 		         + " WHERE   jft.teacherId=? order by jft.jobForTeacherId asc ";
     	 		        query=session.createSQLQuery(hql);
     	 		      query.setParameter(0, teacherId);
     	 		     
     	 		     objList  = query.list(); 
     	 	 	         
     	 		     JobForTeacher jobForTeacher=null;
     	 		        if(objList.size()>0){
     	 		           for (int j = 0; j < objList.size(); j++) {
     	 		       	    Object[] objArr2 = objList.get(j);
     	 		         	jobForTeacher = new JobForTeacher();
     	 		         	if(objArr2[0]!=null){
     	 		         	jobForTeacher.setJobForTeacherId(objArr2[0] == null ? null : Long.parseLong(objArr2[0].toString()));
     	 		         	}
     	 		         	HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster(); 
     	 		         	if(objArr2[1]!=null){
     	 		         	int headQuarterMasterId=objArr2[1] == null ? null : Integer.parseInt(objArr2[1].toString());
     	 		         	headQuarterMaster.setHeadQuarterId(headQuarterMasterId);
     	 		         	JobOrder joborder=new JobOrder();
     	 		         	joborder.setHeadQuarterMaster(headQuarterMaster);
     	 		         	
     	 		         	jobForTeacher.setJobId(joborder);
     	 		         	}
        	 		         lstJobForTeachers.add(jobForTeacher );
     	 		             }
     	 		            }
     	 		          }
     	 		        }
     	 		   catch(Exception e){
     	 		    e.printStackTrace();
     	 		  }
     	 		   return lstJobForTeachers;
     	 	}
 @Transactional(readOnly=false)		
     public List<JobForTeacher> getJobForTeachersByJobAndTeacher(List<JobOrder> jobOrdersList,List<TeacherDetail> listTeachers){
    	List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();
		Session session = getSession();
		try{
		Criteria criteria = session.createCriteria(getPersistentClass());
		Criterion criterion1 = Restrictions.in("teacherId", listTeachers);	
		Criterion criterion2 = Restrictions.in("jobId", jobOrdersList);	
		lstJobForTeachers    = findByCriteria(criterion1,criterion2);
		}catch(Exception e){
			e.printStackTrace();
		}
    	return lstJobForTeachers;
     }

 //nadeem........
 @Transactional(readOnly=false)	
 public List<JobForTeacher> getjftByTalentId(List<TeacherDetail> allTalentId){
	 List<JobForTeacher> compJFT=new ArrayList<JobForTeacher>();
	 try
	 {
	 			Session session = getSession();
	 			Criteria criteria = session.createCriteria(getPersistentClass());
	 			Criterion criterion1 = Restrictions.in("teacherId", allTalentId);	
	 			criteria.add(criterion1);
	 			compJFT = criteria.list();
	 			
	 			/*criteria.setProjection( Projections.projectionList()
					.add(Projections.property("jobForTeacherId"))
					.add(Projections.property("status"))
 					.add(Projections.property("statusMaster")) 					
 					.add(Projections.property("jobId")) 									
 					);				
				List results = criteria.list();
				if(results!=null && results.size()>0){
					for(Iterator it =results.iterator();it.hasNext();){
						JobForTeacher jftObj = new JobForTeacher();
						Object[] row = (Object[]) it.next();						
						jftObj.setJobForTeacherId(new Long(row[0].toString()));
						
						if(row[1]!=null){
							StatusMaster status = new StatusMaster();
							status.setStatus(row[1].toString());							
							jftObj.setStatus(status);
						}
						if(row[2]!=null){
							SecondaryStatus secStatus = new SecondaryStatus();
						//	secStatus.setSecondaryStatusId(Integer.parseInt(row[2].toString()));
							secStatus.setSecondaryStatusName(row[3].toString());
							jftObj.setSecondaryStatus(secStatus);
						}
						if(row[3]!=null){
								JobCategoryMaster jobCategoryMaster = new JobCategoryMaster();
								jobCategoryMaster
								JobOrder jbordr=new JobOrder();
								jbordr.setjobi
								jobCategoryMaster.setJobCategory(row[3].toString());
								jftObj.setJobId(jbordr);
						}
						compJFT.add(jftObj);
					}
				} */
	 }catch (Exception e) {
		 e.printStackTrace();
	 }
	 return compJFT;
 } 
 
 public JSONArray[] getHiredGrid(int schoolId, DistrictMaster districtMaster, int sortingcheck, String sortOrderStrVal,Date hiredStartdate,Date hiredEndDtae,int start, int end,String noOfRow){

		JSONArray jsonArray = new JSONArray();
		JSONArray jsonCountArray = new JSONArray();

		JSONArray[] returnJsonArray = new JSONArray[2];
		Connection connection =null;
		int totalCount=0;
		try {		
			String limit="";
			if(Integer.parseInt(noOfRow)!=0){
				limit=" limit "+start+","+noOfRow;
			}
			String startDate = null, endDate = null;
			if(hiredStartdate !=null && hiredEndDtae!=null){
				SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				startDate = newFormat.format(hiredStartdate);
				endDate = newFormat.format(hiredEndDtae);
			}

			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hird");
			String where="";
			String   hql = "SELECT SQL_CALC_FOUND_ROWS DISTINCT jft.jobForteacherId,jft.teacherId,jft.jobId,jft.districtId,"
			+ "CASE WHEN jft.requisitionNumber!=0  THEN jft.requisitionNumber ELSE 'N/A' END AS   requisitionNumber, "
			+ "CASE WHEN jft.hiredByDate IS NOT NULL THEN jft.hiredByDate ELSE 'N/A' END AS   hiredByDate, " 
			+ "CASE WHEN jft.hiredBySchool!=0  THEN jft.hiredBySchool ELSE 'N/A' END AS   hiredBySchool, "
			+ "CONCAT(td.firstName,' ',td.lastName) AS teacherName, "
			+ "CASE WHEN td.emailAddress!=''  THEN td.emailAddress ELSE 'N/A' END AS   emailAddress, " 
			+ "CASE WHEN jo.jobTitle!=''  THEN jo.jobTitle ELSE 'N/A' END AS   jobTitle, "
			+ "CASE WHEN scm.schoolName!=''  THEN scm.schoolName ELSE 'N/A' END AS   schoolName, "
			+ "CASE WHEN scm.locationCode!=0  THEN scm.locationCode ELSE 'N/A' END AS   locationCode, "
			+ "CASE WHEN scm.division !=''  THEN scm.division ELSE 'N/A' END AS  division,"            
			+ "CASE WHEN drn.posType  = 'F' THEN 'Full Time' WHEN drn.posType  = 'P' THEN 'Part Time' ELSE 'N/A' END AS postType,"
			+ "IFNULL(tns.teacherNormScore,'N/A') AS normScore"+
	  	      				" FROM  jobforteacher jft "
	  		        +   "inner join teacherdetail td on td.teacherId= jft.teacherId "
	 		        +   " inner join joborder jo on jo.jobId= jft.jobId "
	  		        +  " left join schoolmaster scm ON scm.schoolId=jft.hiredBySchool   "
	 		        +   " left join districtrequisitionnumbers drn ON drn.districtId=jft.districtId AND drn.requisitionNumber=jft.requisitionNumber "
	 		          + " left join teachernormscore tns ON tns.teacherNorScoreId=(SELECT tmnscr.teacherNorScoreId FROM teachernormscore tmnscr WHERE tmnscr.teacherId=jft.teacherId ORDER BY tmnscr.teacherAssessmentId DESC LIMIT 1)  "
	 		          + " WHERE jft.status="+statusMaster.getStatusId()+" ";
	 	     	if(startDate!=null && endDate!=null)
	 	     		hql=hql+ "and jft.hiredByDate>='"+startDate+"' and jft.hiredByDate <='"+endDate+"' ";
	 	     	else if(startDate!=null && endDate==null)
	 	         	hql=hql+ "and jft.hiredByDate>='"+startDate+"' ";
	 	     	else if(endDate!=null && startDate==null)
	 	     		hql=hql+ "and jft.hiredByDate<='"+endDate+"' ";
	 	     	
	 	     	if(districtMaster!=null)
	 	     	   hql=hql+ " and jft.districtId="+districtMaster.getDistrictId()+" ";
	 	     	 if(schoolId!=0)
	 	     		hql=hql+ "and scm.schoolId="+schoolId+" ";
/*	 	   	if(sortingcheck==1){
	 	   		hql=hql+" order by td."+sortOrderStrVal;
			}else if(sortingcheck==2){
	  			hql=hql+" order by jo."+sortOrderStrVal;
			}else if(sortingcheck==3){
		 		hql=hql+" order by jft."+sortOrderStrVal;
		    }else if(sortingcheck==4){
		 		hql=hql+" order by tns."+sortOrderStrVal;
		     }*/
	 	   	
	 	   	if(sortOrderStrVal!=null || !sortOrderStrVal.equals(""))
	 	   	{
	 	   		hql=hql+" order by "+sortOrderStrVal;
	 	   	}
			/*if(Integer.parseInt(noOfRow)!=0){
				limit=" limit "+start+","+end;
			}*/
			
			
			System.out.println("limit   "+limit);
			hql=hql+" "+limit;
			
			System.out.println("hql    "+hql);
			SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
			ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			connection = connectionProvider.getConnection();
			PreparedStatement ps=connection.prepareStatement(hql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);

			PreparedStatement psCount=connection.prepareStatement("SELECT FOUND_ROWS()",ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);

			ResultSet rs=ps.executeQuery();
			ResultSet rsCount=psCount.executeQuery();	
			while(rsCount.next()){
				totalCount=rsCount.getInt(1);
				System.out.println("count ::::"+totalCount);
			}
			
			
			rs.last();
			int rowcount=rs.getRow();
			
			System.out.println("rowcount   "+rowcount);
			rs.beforeFirst();
			int cnt=0;
			ResultSetMetaData rsmd = rs.getMetaData();
			if(rs.next()){
				do{  	
					JSONObject jsonRec = new JSONObject();					
					for (int i = 1; i <=rsmd.getColumnCount(); i++) {
						String name = rsmd.getColumnName(i);
						jsonRec.put(name, rs.getString(name));
					}

					jsonArray.add(jsonRec);
				}while(rs.next());
			}

			JSONObject jsonCount = new JSONObject();
			jsonCount.put("totalCount", totalCount);
			jsonCountArray.add(jsonCount);
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			if(connection!=null)
				try {connection.close();} catch (SQLException e) {	e.printStackTrace();}
		}

		returnJsonArray[0] = jsonArray;
		returnJsonArray[1] = jsonCountArray;
		return returnJsonArray;
	}

 /*
  * 
  * pawan kumar
  * 
  * */
 @Transactional(readOnly=false)
 	public List<JobForTeacher> getJobIdByTeacherAndDistrict(Integer districtId,TeacherDetail teacherDetail)
 	{
 		List<JobForTeacher> listForJobId=null;
 		try
 		{
 			Criterion c1= Restrictions.eq("districtId", districtId);
 			Criterion c2=Restrictions.eq("teacherId", teacherDetail);
 			listForJobId=findByCriteria(c1,c2);
 		}
 		catch (Exception e) {
 			e.printStackTrace();
		}
 		return listForJobId;
 	}

 
 	@Transactional(readOnly=false)	
	public List<JobForTeacher>  getEpiStatusByTeacher_Opp(Integer teacherId) throws Exception 
	{	
 		List<Object[]> objList=null;
	 	List<JobForTeacher>  lstJobForTeachers = new ArrayList<JobForTeacher>();
		 try{
		     Query  query=null;
	 	     Session session = getSession();
	  	    if(teacherId!=0){ 
	     	String   hql = "SELECT   jft.jobForTeacherId,jft.jobId  FROM  jobforteacher jft "
		        +   "inner join joborder jo on jo.jobId= jft.jobId "
		     +   "inner join jobcategorymaster jcm on jcm.jobCategoryId= jo.jobCategoryId "
		        + " WHERE   jft.teacherId=? and jo.status=? and (jcm.baseStatus=? or jcm.epiForFullTimeTeachers=?) ";
		        query=session.createSQLQuery(hql);
		        query.setParameter(0, teacherId);
		        query.setParameter(1, "A");
		        query.setParameter(2, 1);
		         query.setParameter(3, 1);
		          objList  = query.list(); 
	 	          JobForTeacher jobForTeacher=null;
		        if(objList.size()>0){
		           for (int j = 0; j < objList.size(); j++) {
		       	    Object[] objArr2 = objList.get(j);
		         	jobForTeacher = new JobForTeacher();
		         	HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster(); 
		         	JobOrder joborder=new JobOrder();
		         	if(objArr2[0]!=null){
		         	jobForTeacher.setJobForTeacherId(objArr2[0] == null ? null : Long.parseLong(objArr2[0].toString()));
		         	}
		          if(objArr2[1]!=null){
		         	int jobId=objArr2[1] == null ? null : Integer.parseInt(objArr2[1].toString());
		          	joborder.setJobId(jobId);
		         	jobForTeacher.setJobId(joborder);
		            }	
		             lstJobForTeachers.add(jobForTeacher);
		             }
		            }
		          }
		        }
		   catch(Exception e){
		    e.printStackTrace();
		  }
		   return lstJobForTeachers;
	}	 
 	@Transactional(readOnly=false)
 	  public List<JobForTeacher> findJobListForJobSpecificInventoryOnlYjobIds(TeacherDetail teacherDetail,Boolean assesmentStatus)
 	  {
 	   PrintOnConsole.getJFTPrint("JFT:last");
 	   List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
 	   try 
 	   {
 	    Date dateWithoutTime = Utility.getDateWithoutTime();
 	    
 	    Criterion criterion1 = Restrictions.eq("teacherId", teacherDetail);
 	    Session session = getSession();
 	    Criteria criteria = session.createCriteria(getPersistentClass());
 	    criteria.add(criterion1) .setProjection(Projections.projectionList() .add(Projections.property("jobId.jobId"))
	    		.add(Projections.property("jobForTeacherId")));
	    criteria.createCriteria("jobId").add(Restrictions.eq("isJobAssessment", assesmentStatus))
 	    .add(Restrictions.le("jobStartDate",dateWithoutTime))
	    .add(Restrictions.ge("jobEndDate",dateWithoutTime))
	    .add(Restrictions.eq("status", "A"));
 	    List<String[]> results= criteria.list();
 	    
 	    for (Iterator it = results.iterator(); it.hasNext();)
 	   {System.out.println("test");
 	    
 	    Object[] row = (Object[]) it.next();
 	    if(row[0]!=null){
 	     JobForTeacher jft =new JobForTeacher();
 	     JobOrder jo = new JobOrder();
 	    System.out.println("test"+Integer.parseInt(row[0].toString()));
 	    if(row[0]!=null){
 	     jo.setJobId(Integer.parseInt(row[0].toString()));
 	     jft.setJobId(jo);
 	    }
 	     lstJobForTeacher.add(jft);
 	    }
 	  }
 	   } 
 	   catch (Exception e) {
 	    e.printStackTrace();
 	   }  
 	   return lstJobForTeacher;
 	  }
//Added by Kumar Avinash 	
 	@Transactional(readOnly=false)	
	public List<JobForTeacher> findJobListForJobSpecificInventory_Opp(TeacherDetail teacherDetail,Boolean assesmentStatus) throws Exception 
	{	List<Object[]> objList=null;
	  List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		 try{
			  Date dateWithoutTime = Utility.getDateWithoutTime();
		     Query  query=null;
	 	     Session session = getSession();
	  	    if(teacherDetail!=null){ 
	     	String   hql = "SELECT   jft.jobForTeacherId,jft.jobId,jcm.baseStatus  FROM  jobforteacher jft "
		        +   "left join joborder jo on jo.jobId= jft.jobId "
		        +    "inner join jobcategorymaster jcm on jcm.jobCategoryId= jo.jobCategoryId "
	 	        + " WHERE   jft.teacherId=? and jo.isJobAssessment=? and jo.jobStartDate <=? and jo.jobEndDate>=?  and jo.status=? ";
		        query=session.createSQLQuery(hql);
		        query.setParameter(0, teacherDetail.getTeacherId());
		        query.setParameter(1, assesmentStatus);
		        query.setParameter(2, dateWithoutTime);
		         query.setParameter(3, dateWithoutTime);
		        query.setParameter(4, "A");
		         objList  = query.list(); 
	 	        JobForTeacher jobForTeacher=null;
		        if(objList.size()>0){
		           for (int j = 0; j < objList.size(); j++) {
		       	    Object[] objArr2 = objList.get(j);
		         	jobForTeacher = new JobForTeacher();
		         	HeadQuarterMaster headQuarterMaster=new HeadQuarterMaster(); 
		         	JobOrder joborder=new JobOrder();
		         	if(objArr2[0]!=null){
		         	jobForTeacher.setJobForTeacherId(objArr2[0] == null ? null : Long.parseLong(objArr2[0].toString()));
		         	}
		          if(objArr2[1]!=null){
		         	int jobId=objArr2[1] == null ? null : Integer.parseInt(objArr2[1].toString());
		          	joborder.setJobId(jobId);
		         	jobForTeacher.setJobId(joborder);
		            
		          }
		          if(objArr2[2]!=null){
		        	  JobCategoryMaster jobCategoryMaster=new JobCategoryMaster();
		        	  jobCategoryMaster.setBaseStatus(Boolean.parseBoolean(objArr2[2].toString()));
		        	  joborder.setJobCategoryMaster(jobCategoryMaster);
		        	  jobForTeacher.setJobId(joborder);
		          }
		          
		            lstJobForTeacher.add(jobForTeacher);
		             }
		            }
		          }
		        }
		   catch(Exception e){
		    e.printStackTrace();
		  }
		   return lstJobForTeacher;
	}	 

 	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherActiveJob_Op(TeacherDetail teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= new ArrayList<JobForTeacher>();
		try 
		{
			Session session = getSession();
			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
			
			criteria.createAlias("jobId", "jo").createAlias("status", "sm").createAlias("jo.jobCategoryMaster", "jcm").createAlias("jo.districtMaster", "dm");
			criteria.setProjection(Projections.projectionList()
					.add(Projections.property("jobForTeacherId"))//0
					.add(Projections.property("jo.jobId"))//1
					.add(Projections.property("jo.jobTitle"))//2
					.add(Projections.property("jo.isInviteOnly"))//3
					.add(Projections.property("isAffilated"))//4
					.add(Projections.property("sm.statusId"))//5
					.add(Projections.property("sm.statusShortName"))//6
					.add(Projections.property("jcm.jobCategoryId"))//7
					.add(Projections.property("jcm.baseStatus"))//8
					.add(Projections.property("jcm.epiForFullTimeTeachers"))//9
					.add(Projections.property("jcm.offerJSI"))//10
					.add(Projections.property("jcm.offerPortfolioNeeded"))//11
					.add(Projections.property("dm.districtId"))//12
					.add(Projections.property("jo.isPortfolioNeeded"))//13
					.add(Projections.property("jo.isJobAssessment"))//14
					.add(Projections.property("jo.jobAssessmentStatus"))//15
					.add(Projections.property("jcm.jobCategoryName"))//16
					);
		
			List<String[]> jftList = new ArrayList<String[]>();
			jftList = criteria.list();

			if(jftList.size()>0){
			for (int j = 0; j < jftList.size(); j++) {				
				Object[] row = (Object[]) jftList.get(j);
				
				if(row[0]!=null){
					JobForTeacher jft = new JobForTeacher();
					JobOrder jo = new JobOrder();
					
					jft.setTeacherId(teacherDetail);
					jo.setJobId(Integer.parseInt(row[1].toString()));
					jo.setJobTitle(row[2].toString());
					if(row[12]!=null){
						DistrictMaster dm = new DistrictMaster();
						dm.setDistrictId(Integer.parseInt(row[12].toString()));
						jo.setDistrictMaster(dm);							
					}
					
					if(row[3]!=null){
						jo.setIsInviteOnly(Boolean.parseBoolean(row[3].toString()));
					}
					if(row[13]!=null){
						jo.setIsPortfolioNeeded(Boolean.parseBoolean(row[13].toString()));
					}
					if(row[14]!=null){
						if(row[14].toString().equalsIgnoreCase("true"))
							jo.setIsJobAssessment(Boolean.TRUE);
						else
							jo.setIsJobAssessment(Boolean.TRUE);
					}
					if(row[15]!=null){
						jo.setJobAssessmentStatus(Integer.parseInt(row[15].toString()));
					}
					
					if(row[4]!=null)
					 jft.setIsAffilated(Integer.parseInt(row[4].toString()));

					StatusMaster sm = new StatusMaster();
					sm.setStatusId(Integer.parseInt(row[5].toString()));
					sm.setStatusShortName(row[6].toString());
					jft.setStatus(sm);						
					
					if(row[7]!=null){
						JobCategoryMaster jcm = new JobCategoryMaster();
						jcm.setJobCategoryId(Integer.parseInt(row[7].toString()));
						jcm.setBaseStatus(Boolean.parseBoolean(row[8].toString()));
						jcm.setEpiForFullTimeTeachers(Boolean.parseBoolean(row[9].toString()));
						jcm.setOfferJSI(Integer.parseInt(row[10].toString()));
						jcm.setOfferPortfolioNeeded(Boolean.parseBoolean(row[11].toString()));
						jcm.setJobCategoryName(row[16].toString());
						jo.setJobCategoryMaster(jcm);
					}
					jft.setJobId(jo);
					lstJobForTeacher.add(jft);
				}
			}
			}
		
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		
		return lstJobForTeacher;
		
	}
 	
 	@Transactional(readOnly=false)
	public JSONArray[] getQuestTeacherDetailRecords_OP(Order sortOrderStrVal,DistrictMaster districtMaster,String firstName,String lastName,String emailAddress,Date appliedfDate,Date appliedtDate,int daysVal,int start, int end,String noOfRow)
	{
	 	JSONArray jsonArray = new JSONArray();
		JSONArray jsonCountArray = new JSONArray();
		JSONArray[] returnJsonArray = new JSONArray[2];
		Connection connection =null;
		int totalCount=0;
		try{
			Calendar cal1 = Calendar.getInstance();
			cal1.set(Calendar.HOUR_OF_DAY, 0);
			cal1.set(Calendar.MINUTE, 0);
			cal1.set(Calendar.SECOND, 0);
			cal1.set(Calendar.MILLISECOND, 0);
	
			Calendar cal2 = Calendar.getInstance();
			cal2.add(Calendar.HOUR,23);
			cal2.add(Calendar.MINUTE,59);
			cal2.add(Calendar.SECOND,59);
	
			if(daysVal==0)
			{
				cal1.add(Calendar.DATE, -7);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==1)
			{
				cal1.add(Calendar.DATE, -15);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==2)
			{
				cal1.add(Calendar.DATE, -30);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}else if(daysVal==3)
			{
				cal1.add(Calendar.DATE, -60);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			else if(daysVal==4)
			{
				cal1.add(Calendar.DATE, -90);
				appliedfDate = cal1.getTime();
				appliedtDate = cal2.getTime();
			}
			
			String startDate = null, endDate = null;
			SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			if(appliedfDate !=null)
				startDate = newFormat.format(appliedfDate);
			if(appliedtDate!=null)
				endDate = newFormat.format(appliedtDate);
			
			String limit="";
			if(Integer.parseInt(noOfRow)!=0)
				limit=" limit "+start+","+noOfRow;
			
			StatusMaster statusMaster= WorkThreadServlet.statusMap.get("hide");
			String queryString = "SELECT SQL_CALC_FOUND_ROWS DISTINCT jft.teacherId AS teacherId, jft.isAffilated AS isAffilated, " +
				" CASE WHEN td.firstName!=''  THEN td.firstName ELSE 'N/A' END AS teacherFirstName, "+
				" CASE WHEN td.lastName!=''  THEN td.lastName ELSE 'N/A' END AS teacherLastName,  "+
				" CASE WHEN td.emailAddress!=''  THEN td.emailAddress ELSE 'N/A' END AS emailAddress,  "+
				" IFNULL(tns.teacherNormScore,'N/A') AS normScore, "+
				" CASE WHEN tns.decileColor!=''  THEN tns.decileColor ELSE 'white' END AS decileColor,  "+
				" (SELECT COUNT(*) AS jobApply FROM jobforteacher WHERE teacherId=td.teacherId AND STATUS IN (3,4,6,7,9,10,16,17,18,19,21,22)) AS jobApply, "+
				"(SELECT COUNT(*) AS jobApply1 FROM jobforteacher WHERE teacherId=td.teacherId AND DistrictID="+districtMaster.getDistrictId()+" AND STATUS IN (3,4,6,7,9,10,16,17,18,19,21,22)) AS jobApply1 "+
				" FROM  jobforteacher jft  "+
				" LEFT JOIN teacherdetail td ON td.teacherId= jft.teacherId  "+ 
				" LEFT JOIN teachernormscore tns ON tns.teacherNorScoreId=(SELECT tmnscr.teacherNorScoreId FROM teachernormscore tmnscr WHERE tmnscr.teacherId=jft.teacherId ORDER BY tmnscr.teacherAssessmentId DESC LIMIT 1) "+   
	 		    " WHERE jft.status!="+statusMaster.getStatusId()+" ";
				if(districtMaster!=null)
		     	   queryString=queryString+ " and jft.districtId="+districtMaster.getDistrictId()+" ";
				
				if(startDate!=null && endDate!=null)
	 	     		queryString=queryString+ "and jft.createdDateTime>='"+startDate+"' and jft.createdDateTime <='"+endDate+"' ";
	 	     	else if(startDate!=null && endDate==null)
	 	         	queryString=queryString+ "and jft.createdDateTime>='"+startDate+"' ";
	 	     	else if(endDate!=null && startDate==null)
	 	     		queryString=queryString+ "and jft.createdDateTime<='"+endDate+"' ";
	
				if(firstName!=null && !firstName.equals(""))
					queryString=queryString+ "and td.firstName like '%"+firstName+"%' ";
				if(lastName!=null && !lastName.equals(""))
					queryString=queryString+ "and td.lastName like '%"+lastName+"%' ";
				if(emailAddress!=null && !emailAddress.equals(""))
					queryString=queryString+ "and td.emailAddress like '%"+emailAddress+"%' ";
	 	   	
	 	   		if(sortOrderStrVal!=null || !sortOrderStrVal.equals(""))
	 	   			queryString=queryString+" order by "+sortOrderStrVal;
	 	   		
	 	   		queryString=queryString+" "+limit;
	 	   	
			System.out.println("queryString    "+queryString);
			SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
			ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
			connection = connectionProvider.getConnection();
			PreparedStatement ps=connection.prepareStatement(queryString,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);
	
			PreparedStatement psCount=connection.prepareStatement("SELECT FOUND_ROWS()",ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE);
	
			ResultSet rs=ps.executeQuery();
			ResultSet rsCount=psCount.executeQuery();	
			while(rsCount.next()){
				totalCount=rsCount.getInt(1);
				System.out.println("count ::::"+totalCount);
			}
			
			
			rs.last();
			int rowcount=rs.getRow();
			
			System.out.println("rowcount   "+rowcount);
			rs.beforeFirst();
			int cnt=0;
			ResultSetMetaData rsmd = rs.getMetaData();
			if(rs.next()){
				do{  	
					JSONObject jsonRec = new JSONObject();					
					for (int i = 1; i <=rsmd.getColumnCount(); i++) {
						String name = rsmd.getColumnName(i);
						jsonRec.put(name, rs.getString(name));
					}
	
					jsonArray.add(jsonRec);
				}while(rs.next());
			}
	
			JSONObject jsonCount = new JSONObject();
			jsonCount.put("totalCount", totalCount);
			jsonCountArray.add(jsonCount);
			}catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				if(connection!=null)
					try {connection.close();} catch (SQLException e) {	e.printStackTrace();}
			}

			returnJsonArray[0] = jsonArray;
			returnJsonArray[1] = jsonCountArray;
			return returnJsonArray;
 		}
 	@Transactional(readOnly=false)	
 	public List<JobForTeacher> findByTeacherListHQAndBranch(List<TeacherDetail> teacherDetails,HeadQuarterMaster headQuarterMaster,BranchMaster branchMaster) 
 	{	
 		PrintOnConsole.getJFTPrint("JFT:201");
 		List<JobForTeacher> lstJobForTeachers = new ArrayList<JobForTeacher>();
 		try
 		{
 			Session session = getSession();
 			Criteria criteria = session.createCriteria(getPersistentClass());
 			Criterion criterion1 = Restrictions.in("teacherId", teacherDetails);	
 			criteria.add(criterion1);
 			if(headQuarterMaster!=null && branchMaster!=null)
 				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster)).add(Restrictions.eq("branchMaster", branchMaster));
 			else if(headQuarterMaster!=null)
 				criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster", headQuarterMaster));
 			else if(branchMaster!=null)
 			    criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster", branchMaster));

 			criteria.addOrder(Order.desc("createdDateTime"));
 			lstJobForTeachers = criteria.list();

 		}catch(Exception e ){
 			e.printStackTrace();
 		}

 		return lstJobForTeachers;
 	}  
 	@Transactional(readOnly=false)
 	public int findIsAffilatedByJFT(Long jobForTeacherId){
 		int isAffilated=0;
 		Session session = getSession();
		Criteria criteria = session.createCriteria(getPersistentClass());
		Criterion criterion1 = Restrictions.eq("jobForTeacherId", jobForTeacherId);
		criteria.setProjection(Projections.property("isAffilated"));
		try{
			criteria.add(criterion1);
			List result = criteria.list();
			if(result!=null && result.size()>0){
				isAffilated = (Integer) result.get(0);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
 		return isAffilated;
 	}
 	
 	@Transactional(readOnly=false)
	public List<JobForTeacher> findJobByTeacherInDesc(List<TeacherDetail> teacherDetail)
	{
		List<JobForTeacher> lstJobForTeacher= null;
		try 
		{
				Criterion criterion1 = Restrictions.in("teacherId",teacherDetail);
				lstJobForTeacher = findByCriteria(Order.desc("jobForTeacherId"),criterion1);
		} 
		catch (Exception e) {
			e.printStackTrace();
		}		
		return lstJobForTeacher;
	}
 	
	@Transactional(readOnly=false)
 	public List<Integer> getJobCategoryIdByTeacherId(TeacherDetail teacherDetail)
 	{
 		List<Integer> jobCategoryId=null;
 		Session session = getSession();
 		try{
 			Criteria criteria = session.createCriteria(getPersistentClass());
			Criterion criterion1 = Restrictions.eq("teacherId",teacherDetail);
			criteria.add(criterion1);
			
			criteria.createAlias("jobId", "jo").createAlias("jo.jobCategoryMaster", "jcm");
			
			criteria.setProjection(Projections.projectionList()
					.add(Projections.distinct(Projections.property("jcm.jobCategoryId")))
				);
		jobCategoryId = criteria.list();
 		}catch (Exception e) {
			e.printStackTrace();
		}
 		return jobCategoryId;
 	}
	@Transactional(readOnly=false)
	public Map newfindByJobOrderForKellyCGOpSql(UserMaster userMaster,JobOrder jobOrder, List<StatusMaster> lstStatusMasters,int cgUpdate,String firstName,String lastName,String emailAddress,boolean teacherFlag,List<TeacherDetail> filterTeacherList,boolean all,boolean teachersOnly,List<Long> selectedTeachers,int pageNo, int pageSize,String orderColumn,String sortingOrder,int callType)
	{

		PrintOnConsole.getJFTPrint("JFT:101 findByJobOrderForKellyCGOpSql sql query   "+orderColumn);
		List<JobForTeacher> lstJobForTeacher=  new ArrayList<JobForTeacher>();
		Map returnMap=new HashMap();
		//
		try{
	
			List <StatusMaster> statusMasterLst=WorkThreadServlet.statusMasters;
			Map<Integer, StatusMaster> mapStatus = new HashMap<Integer, StatusMaster>();
			for (StatusMaster sMaster : statusMasterLst) {
				mapStatus.put(sMaster.getStatusId(),sMaster);
			}
			
			
			String criterionSchool ="";
			if(callType==1){
				criterionSchool=" and tdl.KSNID is null ";
			}
			if(userMaster!=null && (userMaster.getEntityType()==3 || userMaster.getEntityType()==6) && (jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId()==1200390)){
				criterionSchool=" and jft.hiredBySchool="+userMaster.getSchoolId()+" ";
            }
            
			if(teachersOnly)
			{
			//	System.out.println("teachersOnly :"+teachersOnly+" ====== selectedTeachers.size() :: "+selectedTeachers.size());
				if(selectedTeachers.size()>0)
				{						
					String jftIdLst="";
					for (Long jftId : selectedTeachers) {
						if(jftIdLst.equalsIgnoreCase(""))
							jftIdLst+=jftId+"";
						else
							jftIdLst+=jftId+",";
					}
					criterionSchool=" and jft.jobForTeacherId in("+jftIdLst+")";
				}
			}
			
			if(teacherFlag){
				String tidslst="";
				for (TeacherDetail tIds : filterTeacherList) {
					if(tidslst.equalsIgnoreCase("")){
						tidslst+=tIds.getTeacherId()+"";
					}else{
						tidslst+=","+tIds.getTeacherId();
					}
				}
				criterionSchool+=" and jft.teacherId in ("+tidslst+") ";
			}
			String statusMaster= "";//mapStatus.get("hide");
			
			String ststuaList=""; 
			for (StatusMaster lstStatusMasterss : lstStatusMasters) {
				if(!ststuaList.equalsIgnoreCase(""))
					ststuaList+=","+lstStatusMasterss.getStatusId();
				else
					ststuaList+=lstStatusMasterss.getStatusId();
				if(lstStatusMasterss.getStatusShortName().equalsIgnoreCase("hide")){
					statusMaster=lstStatusMasterss.getStatusId()+""; 
				}
				
			}
			String criterion15 = " and (jft.internalStatus!=3 or jft.internalStatus IS NULL) ";//Restrictions.or(criterion10,criterion14);

			
			String criterion13 = " and (jft.internalStatus!=3 or jft.status in ("+ststuaList+")) ";//Restrictions.or(criterion11,criterion1);				
			String criterion1 = " and jft.status in ("+ststuaList+") ";
			String criterion3 = " and jft.status!=3 ";
		
			String allCondidtions="jft.jobId="+jobOrder.getJobId()+" ";
			if(cgUpdate==0){
				allCondidtions+= criterion13;
			}else if(cgUpdate==1){
				allCondidtions+=criterion15+criterion1;
				boolean epiInternal=false;
				try{
					if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
						epiInternal=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobOrder.getJobCategoryMaster()!=null){/*
					if(jobOrder.getJobCategoryMaster().getBaseStatus() && jobOrder.getJobCategoryMaster().getStatus().equalsIgnoreCase("A")){
						if(epiInternal==false){
							Criterion criterion7 = Restrictions.eq("isAffilated",1);
							Criterion criterion12 =Restrictions.or(criterion7, criterion4) ;
							criteria.add(criterion12);
							//String criterion4 = Restrictions.eq("cgUpdated",true);
							allCondidtions+=" and (jft.isAffilated=1 or jft.cgUpdated=1) ";
						}else{
							allCondidtions+=" and jft.cgUpdated=1 ";
						}
					}
				*/}
			}else if(cgUpdate==7){					
				allCondidtions+=criterion15+criterion1;
			}else if(cgUpdate==2){
				allCondidtions+=criterion1;
			}else if(cgUpdate==5){
				allCondidtions+=criterion15+criterion1;
			}

			String start =""+(pageNo * pageSize);
			String end =""+pageSize;
			
			allCondidtions+=" and firstName like '%"+firstName.trim()+"%' and lastName like '%"+lastName.trim()+"%' and emailAddress like '%"+emailAddress.trim()+"%'";
			System.out.println("allCondidtionsallCondidtions    "+allCondidtions);
			
			String sortOrderStr="";
			boolean baseStatusFlag=false;
			if(jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getStatus().equals("A") && jobOrder.getJobCategoryMaster().getBaseStatus()){
				baseStatusFlag=true;
			}
			
			if(orderColumn!=null && orderColumn.equalsIgnoreCase("normScore") && baseStatusFlag)
			{
				if(sortingOrder.equalsIgnoreCase("0"))
					sortOrderStr = " Order By "+orderColumn+" desc"; 
				else{
					sortOrderStr = " Order By "+orderColumn+" asc";
				}
			}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("normScore") && sortingOrder.equalsIgnoreCase("0") && sortingOrder.equalsIgnoreCase("0")){					
					sortOrderStr = " Order By lastName asc";					
			}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("fScore")){					
				if(sortingOrder.equalsIgnoreCase("0"))
					sortOrderStr = " Order By "+orderColumn+" desc"; 
				else{
					sortOrderStr = " Order By "+orderColumn+" asc";
				}					
			}else if(orderColumn!=null && orderColumn.equalsIgnoreCase("status")){
				if(sortingOrder.equalsIgnoreCase("0"))
					sortOrderStr = " Order By currentStatus asc"; 
				else{
					sortOrderStr = " Order By currentStatus  desc";
				}					
			}else{
				
				if(sortingOrder.equalsIgnoreCase("0"))					
					sortOrderStr = " Order By lastName desc"; 
				else{
					sortOrderStr = " Order By lastName asc";
				}
			}
			int jobCategoryId=jobOrder.getJobCategoryMaster().getJobCategoryId();
			if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null){
				jobCategoryId=jobOrder.getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();
			}
			String limitTxt="";
			/*if(callType==1){
				limitTxt=" limit "+start+","+end;
			}else if(callType==2){
				
			}*/
			Connection connection =null;
			String sql2="SELECT jft.teacherId ";
			String sql="SELECT jft.*,tdl.firstName as firstName,tdl.lastName as lastName,tdl.emailAddress,tdl.KSNID,tdl.isPortfolioNeeded,tns.teacherNormScore as normScore,"+
			"status1.statusShortName,status1.status as statusFeildName,"+
			"CASE WHEN jft.displayStatusId IS NOT NULL THEN (CASE WHEN jft.status IN (3,4,9,16,17,18) THEN (SELECT secondaryStatusName FROM secondarystatus WHERE parentStatusId=jft.status AND jobCategoryId="+jobCategoryId+") ELSE  status1.status END) ELSE secondarystatus1.secondaryStatusName END AS currentStatus, "+
			"secondarystatus1.secondaryStatusName,"+
			"jobwiseconteascore.jobWiseConsolidatedScore as fScore ";
			
			String table = " FROM jobforteacher jft "+ 
	        " LEFT JOIN teachernormscore tns ON tns.teacherId = jft.teacherId "+
	        " INNER JOIN teacherdetail tdl ON tdl.teacherId = jft.teacherId "+
	        " INNER JOIN statusmaster status1 ON status1.statusId = jft.status "+		        
	        " LEFT JOIN secondarystatus secondarystatus1 ON secondarystatus1.secondaryStatusId = jft.displaySecondaryStatusId "+
	        " LEFT JOIN jobwiseconsolidatedteacherscore jobwiseconteascore ON jobwiseconteascore.teacherId = jft.teacherId and jobwiseconteascore.jobId = jft.jobId"+
	        " WHERE "+allCondidtions+" "+criterionSchool;
			sql+=table+sortOrderStr+limitTxt;
			sql2+=table;
			
			
			//jobwiseconsolidatedteacherscore
			System.out.println("sql  "+sql);
			returnMap.put("query", sql2);
			SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor) getSessionFactory();
		      ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
		        connection = connectionProvider.getConnection();
			PreparedStatement ps=connection.prepareStatement(sql,ResultSet.CONCUR_READ_ONLY,ResultSet.CONCUR_UPDATABLE); 
			 
			  ResultSet rs=ps.executeQuery();
			  rs.last();
			  int rowcount=rs.getRow();
			  rs.beforeFirst();
			  if(rs.next()){				  
				  
				    do{
				     final String[] allInfo=new String[rs.getMetaData().getColumnCount()];
				     JobForTeacher tempJft = new JobForTeacher();
				     	tempJft.setJobForTeacherId(Long.parseLong(rs.getString(rs.findColumn("jobForTeacherId"))));//jobForTeacherId
				     	if(rs.getString("districtId")!=null){
				     		tempJft.setDistrictId(Integer.parseInt(rs.getString(rs.findColumn("districtId"))));//districtId
				     	}
				     	TeacherDetail tDetail = new TeacherDetail();
				     	tDetail.setTeacherId(Integer.parseInt(rs.getString(rs.findColumn("teacherId"))));
				     	tDetail.setFirstName(rs.getString("firstName"));
				     	if(rs.getString("KSNID")!=null){
				     		tDetail.setKSNID(new BigInteger(rs.getString("KSNID")));
				     	}
				     	tDetail.setLastName(rs.getString("lastName"));
				     	tDetail.setEmailAddress(rs.getString("emailAddress"));
				     	boolean isPortfolioNeeded = false;
				     	if(rs.getString("tdl.isPortfolioNeeded")!=null){
				     		isPortfolioNeeded=rs.getBoolean("tdl.isPortfolioNeeded");
				     		
				     	}
				     	tDetail.setIsPortfolioNeeded(isPortfolioNeeded);
				     	//System.out.println("rs.getString()    "+rs.getString("firstName"));
				     	tempJft.setTeacherId(tDetail);//teacherId
				     /*	JobOrder tempJobOrder = new JobOrder();
				     	tempJobOrder.setJobId(Integer.parseInt(rs.getString(rs.findColumn("jobId"))));*/
				     	tempJft.setJobId(jobOrder);
				     	tempJft.setRedirectedFromURL(rs.getString("redirectedFromURL"));
				     	tempJft.setJobBoardReferralURL(rs.getString("jobBoardReferralURL"));
				     	StatusMaster tempStatus = null;					     	
				     	if(rs.getString("status")!=null){
				     		tempStatus = mapStatus.get(Integer.parseInt(rs.getString("status"))); 
				     	}
				     	tempJft.setStatus(tempStatus);
				     	StatusMaster tempStatus2 = null;
				     	if(rs.getString("displayStatusId")!=null){
				     		tempStatus2 = mapStatus.get(Integer.parseInt(rs.getString("displayStatusId")));
				     	}
				     	tempJft.setStatusMaster(tempStatus2);
				     	
				     	if(rs.getString("displaySecondaryStatusId")!=null){
				     		SecondaryStatus secondaryStatus = new SecondaryStatus();
				     		secondaryStatus.setSecondaryStatusId(Integer.parseInt(rs.getString("displaySecondaryStatusId")));
				     		secondaryStatus.setSecondaryStatusName(rs.getString("secondarystatus1.secondaryStatusName"));					     		
				     		tempJft.setSecondaryStatus(secondaryStatus);
				     	}else{
				     		tempJft.setSecondaryStatus(null);
				     	}
				     		
				     		
				     	StatusMaster tempStatus3 = null;
				     	if(rs.getString("internalStatus")!=null)
				     		tempStatus3 = mapStatus.get(Integer.parseInt(rs.getString("internalStatus")));					     		
				     	tempJft.setInternalStatus(tempStatus3);
				     	
				     	
				     	if(rs.getString("internalSecondaryStatusId")!=null){
				     		SecondaryStatus secondaryStatus2 = new SecondaryStatus();
				     		secondaryStatus2.setSecondaryStatusId(Integer.parseInt(rs.getString("internalSecondaryStatusId")));
				     		tempJft.setInternalSecondaryStatus(secondaryStatus2);
				     	}else{
				     		tempJft.setInternalSecondaryStatus(null);
				     	}
				     	if(rs.getString("cgUpdated")!=null)
				     		tempJft.setCgUpdated(rs.getBoolean("cgUpdated"));
				     	tempJft.setCoverLetter(rs.getString("coverLetter"));
				     	tempJft.setCoverLetterFileName(rs.getString("coverLetterFileName"));
				     	if(rs.getString("isAffilated")!=null)
				     		tempJft.setIsAffilated(Integer.parseInt(rs.getString("isAffilated")));
				     	tempJft.setStaffType(rs.getString("staffType"));
				     	if(rs.getString("flagForDistrictSpecificQuestions")!=null)
				     		tempJft.setFlagForDistrictSpecificQuestions(rs.getBoolean("flagForDistrictSpecificQuestions"));
				     	tempJft.setNoteForDistrictSpecificQuestions(rs.getString("noteForDistrictSpecificQuestions"));
				     	if(rs.getString("isDistrictSpecificNoteFinalize")!=null)
				     		tempJft.setIsDistrictSpecificNoteFinalize(rs.getBoolean("isDistrictSpecificNoteFinalize"));
				     	
				     	if(rs.getString("districtSpecificNoteFinalizeBy")!=null){
				     		UserMaster finalizeBY = new UserMaster();
				     		finalizeBY.setUserId(Integer.parseInt(rs.getString("districtSpecificNoteFinalizeBy")));
				     		tempJft.setDistrictSpecificNoteFinalizeBy(finalizeBY);
				     	}else{
				     		tempJft.setDistrictSpecificNoteFinalizeBy(null);
				     	}
				     	if(rs.getString("flagForDspq")!=null)
				     		tempJft.setFlagForDspq(rs.getBoolean("flagForDspq"));
				     	tempJft.setRequisitionNumber(rs.getString("requisitionNumber"));
				     	
				     	if(rs.getString("updatedBy")!=null){
				     		UserMaster usrMas = new UserMaster();
				     		usrMas.setUserId(Integer.parseInt(rs.getString("updatedBy")));
				     		tempJft.setUpdatedBy(usrMas);
				     	}else{
				     		tempJft.setUpdatedBy(null);
				     	}
				     	if(rs.getString("updatedByEntity")!=null)
				     		tempJft.setUpdatedByEntity(Integer.parseInt(rs.getString("updatedByEntity")));
				     	tempJft.setLastActivity(rs.getString("lastActivity"));
				     	
				     	if(rs.getString("lastActivityDoneBy")!=null){
				     		UserMaster usrMas2 = new UserMaster();
				     		usrMas2.setUserId(Integer.parseInt(rs.getString("lastActivityDoneBy")));
				     		tempJft.setUserMaster(usrMas2);
				     	}else{
				     		tempJft.setUserMaster(null);
				     	}
				     	
				     	if(rs.getString("hiredBySchool")!=null){
				     		SchoolMaster hiredSchool = new SchoolMaster();
				     		hiredSchool.setSchoolId(Long.parseLong(rs.getString("hiredBySchool")));
				     		tempJft.setSchoolMaster(hiredSchool);
				     	}else{
				     		tempJft.setSchoolMaster(null);
				     	}
				     	if(rs.getString("offerReady")!=null)
				     		tempJft.setOfferReady(rs.getBoolean("offerReady"));
				     	if(rs.getString("offerAccepted")!=null)
				     		tempJft.setOfferAccepted(rs.getBoolean("offerAccepted"));
				     	if(rs.getString("noResponseEmail")!=null)
				     		tempJft.setNoResponseEmail(rs.getBoolean("noResponseEmail"));
				     	if(rs.getString("candidateConsideration")!=null)
				     		tempJft.setCandidateConsideration(rs.getBoolean("candidateConsideration"));
				     	if(rs.getString("noOfReminderSent")!=null)
				     		tempJft.setNoOfReminderSent(Integer.parseInt(rs.getString("noOfReminderSent")));
				     	if(rs.getString("noofTries")!=null)
				     		tempJft.setNoofTries(Integer.parseInt(rs.getString("noofTries")));
				     	if(rs.getString("branchcontact")!=null)
				     		tempJft.setBranchcontact(rs.getBoolean("branchcontact"));
				     	if(rs.getString("jobCompleteDate")!=null)
				     		tempJft.setJobCompleteDate(rs.getDate("jobCompleteDate"));
				     //JobCompleteDate
				     	tempJft.setCreatedDateTime(rs.getDate("createdDateTime"));
				     	if(rs.getString("notReviewedFlag")!=null){
				     		tempJft.setNotReviewedFlag(rs.getBoolean("notReviewedFlag"));
				     	}
				     	if(rs.getString("normScore")!=null)
				     		tempJft.setNormScore(Double.parseDouble(rs.getString("normScore")));
				     	
				     	if(rs.getString("fScore")!=null){
				     		tempJft.setFitScore(rs.getDouble("fScore"));
				     	}
				     	lstJobForTeacher.add(tempJft);
				     
				    }while(rs.next());
				   }
				   else{
				    System.out.println("Record not found ......");
				   }
			} 
		catch (Exception e) {
			e.printStackTrace();
		}	
		returnMap.put("list", lstJobForTeacher);
		return returnMap;
	}
	
	@Transactional(readOnly=false)
	  public List<String[]> getJobForTeacherByJobAndTeachersListWithHQandBR(List<Integer> statusMasterList,List<TeacherDetail> lstTeacherDetails,List<JobOrder> lstJobOrder,HeadQuarterMaster headQuarterMaster, BranchMaster branchMaster,DistrictMaster districtMaster,int entityID,String query)
	  {
	    System.out.println("getJobForTeacherByJobAndTeachersListWithHQandBR  method::::::::::::::::");
	    PrintOnConsole.getJFTPrint("JFT:92");     
	     List<String[]> lstJobForTeacher=new ArrayList<String[]>();
	     if(lstTeacherDetails.size()>0){
	             try{
	                   Session session = getSession();
	                  
	                   Criteria criteria = session.createCriteria(getPersistentClass());             
	                   criteria.setProjection( Projections.projectionList()
	                           .add( Projections.property("teacherId.teacherId"), "teachId" )
	                           .add( Projections.count("teacherId") )              
	                           .add( Projections.groupProperty("teacherId") )
	                       );
	                  
	                   Criterion criterionTeacher=Restrictions.sqlRestriction("this_.teacherId in("+query+")");
	                   if(entityID!=1){                                   
	                                  if(districtMaster!=null){
	                                        criteria.add(Restrictions.eq("districtId", districtMaster.getDistrictId()));
	                                  }else{
	                                        //jobOrderSet.add(Restrictions.isNull("districtMaster"));
	                                  }
	                                  if(branchMaster!=null){
	                                        criteria.createCriteria("jobId").add(Restrictions.eq("branchMaster",branchMaster));
	                                  }else{
	                                        //jobOrderSet.add(Restrictions.isNull("branchMaster"));
	                                  }
	                                  if(headQuarterMaster!=null){
	                                        if(headQuarterMaster.getHeadQuarterId()==1){
	                                              criteria.createCriteria("jobId").add(Restrictions.eq("headQuarterMaster",headQuarterMaster));
	                                        }
	                                  }else{
	                                        //jobOrderSet.add(Restrictions.isNull("headQuarterMaster"));
	                                  }
	                            }
	                  
	               if(lstTeacherDetails.size()>0){
	                            criteria.add(Restrictions.in("status",getObjectStatus(statusMasterList)));
	                            criteria.add(criterionTeacher);
	                      }
	                      
	                      //  System.out.println(criteria);
	                            //criteria.createCriteria("status").add(Restrictions.in("statusId",statusMasterList));
	                   
	                   
	                   lstJobForTeacher = criteria.list() ;
	                               }catch (Throwable e) {
	                   e.printStackTrace();
	             }   
	     }
	       return lstJobForTeacher;
	  }
}
