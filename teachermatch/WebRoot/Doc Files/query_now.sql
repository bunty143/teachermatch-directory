-- Vishwanath 15-4-2015
ALTER TABLE  `questionspool` ADD  `itemCode` VARCHAR( 45 ) NULL AFTER  `fieldLength` ;
-- Sanavvar Khan 15-4-2015
ALTER TABLE  `questionuploadtemp` ADD  `itemCode` VARCHAR( 45 ) NULL AFTER  `rank6` ;

ALTER TABLE `teachermatch`.`universitymaster`
ADD COLUMN `stateId` INT(3) NULL DEFAULT NULL AFTER `collegeCode`;
ALTER TABLE `teachermatch`.`universitymaster`
ADD COLUMN `cityId` INT(5) NULL DEFAULT NULL AFTER `stateId`;
ALTER TABLE `teachermatch`.`universitymaster`
ADD COLUMN `countryId` INT(11) NULL DEFAULT NULL AFTER `cityId`;
ALTER TABLE `teachermatch`.`universitymaster`
ADD COLUMN `ipedsCode` varchar(6) NULL DEFAULT NULL AFTER `countryId`;
----------------------------------- Amit 17-4-2015 -----------------------------------
ALTER TABLE `districtspecificportfolioanswers` ADD `fileName` TEXT NULL AFTER `insertedText` ;
ALTER TABLE `teacherpersonalinfo` ADD `noLongerEmployed` TEXT NULL AFTER `expectedSalary` ;
ALTER TABLE `teachergeneralknowledgeexam` ADD `generalExamNote` TEXT NULL AFTER `generalKnowledgeScoreReport`;
ALTER TABLE `teachersubjectareaexam` ADD `examNote` TEXT NULL AFTER `scoreReport`;


--------------------------------- Ankit 17-04-2015------------------------------------ (Exists in Titan & Cloud)
ALTER TABLE `centermaster` ADD `timeZoneId` INT( 2 ) NOT NULL COMMENT 'Foreign Key to "timeZoneId" in table "timezone"' AFTER `noOfSeatsAvailable` ;

-------------------------- vishwanath 17-04-2015 ----------------------------

CREATE TABLE `scorelookupmaster` (
 `lookupId` int(10) NOT NULL auto_increment,
 `name` varchar(100) character set utf8 collate utf8_unicode_ci NOT NULL,
 `description` varchar(1000) character set utf8 collate utf8_unicode_ci NOT NULL,
 `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP,
 `updatedDateTime` timestamp NULL default NULL,
 `createdBy` int(5) default NULL,
 PRIMARY KEY  (`lookupId`),
 KEY `createdBy` (`createdBy`),
 CONSTRAINT `scorelookupmaster_ibfk_1` FOREIGN KEY (`createdBy`) REFERENCES `usermaster` (`userId`)
) ENGINE=InnoDB;

CREATE TABLE `scorelookup` (
 `scorelookupId` int(10) NOT NULL auto_increment,
 `lookupId` int(10) default NULL,
 `domainId` int(2) default NULL,
 `competencyId` int(3) default NULL,
 `totalscore` double default NULL,
 `percentile` double default NULL,
 `zscore` double default NULL,
 `tscore` double default NULL,
 `ncevalue` double default NULL,
 `normscore` double default NULL,
 `ritvalue` double default NULL,
 `ritse` double default NULL,
 `pass` varchar(5) default NULL,
 `createdDateTime` timestamp NULL default CURRENT_TIMESTAMP,
 `createdBy` int(5) default NULL,
 PRIMARY KEY  (`scorelookupId`),
 KEY `lookupId` (`lookupId`),
 KEY `domainId` (`domainId`),
 KEY `competencyId` (`competencyId`),
 KEY `createdBy` (`createdBy`),
 CONSTRAINT `scorelookup_ibfk_1` FOREIGN KEY (`lookupId`) REFERENCES `scorelookupmaster` (`lookupId`),
 CONSTRAINT `scorelookup_ibfk_2` FOREIGN KEY (`domainId`) REFERENCES `domainmaster` (`domainId`),
 CONSTRAINT `scorelookup_ibfk_3` FOREIGN KEY (`competencyId`) REFERENCES `competencymaster` (`competencyId`),
 CONSTRAINT `scorelookup_ibfk_4` FOREIGN KEY (`createdBy`) REFERENCES `usermaster` (`userId`)
) ENGINE=InnoDB;

CREATE TABLE `assessmentdomainscore` (
 `assessmentdomainscoreId` int(10) NOT NULL auto_increment,
 `teacherId` int(10) NOT NULL,
 `domainId` int(2) NOT NULL,
 `percentile` double default NULL,
 `zscore` double default NULL,
 `tscore` double default NULL,
 `ncevalue` double default NULL,
 `normscore` double default NULL,
 `ritvalue` double default NULL,
 `lookupId` int(10) NOT NULL,
 PRIMARY KEY  (`assessmentdomainscoreId`),
 KEY `teacherId` (`teacherId`),
 CONSTRAINT `assessmentdomainscore_ibfk_1` FOREIGN KEY (`teacherId`) REFERENCES `teacherdetail` (`teacherId`)
) ENGINE=InnoDB;

CREATE TABLE `assessmentcompetencyscore` (
 `assessmentcompetencyscoreId` int(10) NOT NULL auto_increment,
 `teacherId` int(10) NOT NULL,
 `competencyId` int(3) NOT NULL,
 `percentile` double default NULL,
 `zscore` double default NULL,
 `tscore` double default NULL,
 `ncevalue` double default NULL,
 `normscore` double default NULL,
 `ritvalue` double default NULL,
 `lookupId` int(10) NOT NULL,
 PRIMARY KEY  (`assessmentcompetencyscoreId`),
 KEY `teacherId` (`teacherId`),
 KEY `competencyId` (`competencyId`),
 CONSTRAINT `assessmentcompetencyscore_ibfk_1` FOREIGN KEY (`teacherId`) REFERENCES `teacherdetail` (`teacherId`),
 CONSTRAINT `assessmentcompetencyscore_ibfk_2` FOREIGN KEY (`competencyId`) REFERENCES `competencymaster` (`competencyId`)
) ENGINE=InnoDB; 

ALTER TABLE `assessmentdetail` ADD `lookupId` INT( 10 ) NULL AFTER `jobCategoryId` ;
ALTER TABLE `assessmentdetail` ADD FOREIGN KEY ( lookupId ) REFERENCES scorelookupmaster( lookupId );
ALTER TABLE `teacherassessmentdetails` ADD `lookupId` INT( 10 ) NULL AFTER `assessmentId` ;
ALTER TABLE `teacherassessmentdetails` ADD FOREIGN KEY ( lookupId ) REFERENCES scorelookupmaster( lookupId ) ;

-------------------------------------------------Amit 20-04-2015----------------------------------------------------------------
CREATE TABLE `sdpcodeswithcertificates` (
  `sdpCodeCertId` int(10) NOT NULL auto_increment,
  `sdpCode` varchar(50) default NULL,
  `sdpCodeTitle` varchar(250) default NULL,
  `stateId` int(3) NOT NULL COMMENT 'Foreign Key to "stateId" in table "stateMaster" ',
  `certTypeId` int(11) default NULL COMMENT 'Foreign Key to "certTypeId" in table "certificateTypeMaster" ',
  `status` varchar(1) default 'A',
  PRIMARY KEY  (`sdpCodeCertId`),
  KEY `stateId` (`stateId`),
  KEY `certTypeId` (`certTypeId`)
) ENGINE=InnoDB;

ALTER TABLE `sdpcodeswithcertificates`
  ADD CONSTRAINT `sdpCodesWithCertificates_ibfk_1` FOREIGN KEY (`certTypeId`) REFERENCES `certificatetypemaster` (`certTypeId`),
  ADD CONSTRAINT `sdpCodesWithCertificates_ibfk_2` FOREIGN KEY (`stateId`) REFERENCES `statemaster` (`stateId`);
--------------------------------Gourav 20-4-2015---------------------------------
ALTER TABLE `employeemaster` ADD `position` VARCHAR(250) NULL AFTER `alum`, 
ADD `certificationCode` VARCHAR(250) NULL AFTER `position`, 
ADD `eligibleToTransfer` VARCHAR(5) NULL AFTER `certificationCode`, 
ADD `classification` VARCHAR(250) NULL AFTER `eligibleToTransfer`;

ALTER TABLE `joborder` ADD `classification` VARCHAR(250) NULL AFTER `isVacancyJob`;
--------------------------------Sonu 20-4-2015---------------------------------
 DROP TABLE  `teacheranswerdetailsfordistrictspecificquestionshistory`;
 
CREATE TABLE `teacheranswerhistory` (
  `answerId` int(10) NOT NULL auto_increment,
  `teacherId` int(10) default NULL COMMENT 'Foreign Key to "teacherId" in table "teacherDetail"',
  `jobId` int(6) default NULL COMMENT 'Foreign Key to "jobId" in table "jobOrder"',
  `districtId` int(11) default NULL COMMENT '''Foreign key to districtmaster''',
  `questionId` int(10) default NULL COMMENT 'Foreign Key to "questionId" in table "districtSpecificQuestions"',
  `question` text,
  `questionTypeId` int(2) default NULL COMMENT 'Foreign Key to "questionTypeId" in table "questionTypeMaster"',
  `questionType` varchar(5) default NULL,
  `selectedOptions` varchar(50) default NULL,
  `questionOption` text,
  `insertedText` text,
  `teacherAssessmentQuestionId` bigint(16) default NULL COMMENT 'Foreign Key to "teacherAssessmentQuestionId" in table "teacherassessmentquestions"',
  `assessmentId` int(5) default NULL COMMENT 'Foreign Key to "assessmentId" in table "assessmentDetail"',
  `teacherAssessmentId` int(10) default NULL COMMENT 'Foreign Key to "teacherAssessmentId" in table "teacherassessmentdetails"',
  `typeId` int(2) default NULL COMMENT '1 = QQ 2 = JSI',
  `isValidAnswer` tinyint(1) default NULL COMMENT '0 = Invalid 1 = Valid',
  `isActive` tinyint(4) default '0',
  `createdDateTime` datetime NOT NULL,
  PRIMARY KEY  (`answerId`),
  KEY `districtId` (`districtId`),
  KEY `teacherId` (`teacherId`),
  KEY `jobId` (`jobId`),
  KEY `questionTypeId` (`questionTypeId`),
  KEY `questionId` (`questionId`),
  KEY `teacheranswerhistory_ibfk_6` (`assessmentId`),
  KEY `teacheranswerhistory_ibfk_7` (`teacherAssessmentQuestionId`),
  KEY `teacheranswerhistoryl_ibfk_8` (`teacherAssessmentId`)
) ENGINE=InnoDB;

ALTER TABLE `teacheranswerhistory`
  ADD CONSTRAINT `teacheranswerhistory_ibfk_1` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`),
  ADD CONSTRAINT `teacheranswerhistory_ibfk_2` FOREIGN KEY (`teacherId`) REFERENCES `teacherdetail` (`teacherId`),
  ADD CONSTRAINT `teacheranswerhistory_ibfk_3` FOREIGN KEY (`jobId`) REFERENCES `joborder` (`jobId`),
  ADD CONSTRAINT `teacheranswerhistory_ibfk_4` FOREIGN KEY (`questionTypeId`) REFERENCES `questiontypemaster` (`questionTypeId`),
  ADD CONSTRAINT `teacheranswerhistory_ibfk_5` FOREIGN KEY (`questionId`) REFERENCES `districtspecificquestions` (`questionId`),
  ADD CONSTRAINT `teacheranswerhistory_ibfk_6` FOREIGN KEY (`assessmentId`) REFERENCES `assessmentdetail` (`assessmentId`),
  ADD CONSTRAINT `teacheranswerhistory_ibfk_7` FOREIGN KEY (`teacherAssessmentQuestionId`) REFERENCES `teacherassessmentquestions` (`teacherAssessmentQuestionId`),
  ADD CONSTRAINT `teacheranswerhistoryl_ibfk_8` FOREIGN KEY (`teacherAssessmentId`) REFERENCES `teacherassessmentdetails` (`teacherAssessmentId`);
  
  --------------------------------Shadab Ansari 22-4-2015---------------------------------
  CREATE TABLE  `districtapprovalgroups` (
 `districtApprovalGroupsId` INT( 10 ) NOT NULL AUTO_INCREMENT ,
 `districtId` INT( 10 ) NOT NULL COMMENT  '''Foreign Key to districtId in table districtmaster ''',
 `jobCategoryId` INT( 10 ) DEFAULT NULL COMMENT  '''Foreign Key to jobCategoryId in table jobCategoryMaster''',
 `jobId` INT( 10 ) DEFAULT NULL COMMENT  '''Foreign Key to jobId in table jobOrder''',
 `groupName` VARCHAR( 40 ) NOT NULL ,
 `groupMembers` VARCHAR( 40 ) DEFAULT NULL ,
 `noOfApprovals` INT( 10 ) DEFAULT NULL ,
 `approvalGroupCreatedDateTime` DATETIME NOT NULL ,
 `status` VARCHAR( 5 ) DEFAULT  'A',
PRIMARY KEY (  `districtApprovalGroupsId` )
) ENGINE = INNODB;


ALTER TABLE districtapprovalgroups ADD CONSTRAINT FOREIGN KEY ( districtId ) REFERENCES districtmaster( districtId );

ALTER TABLE districtapprovalgroups ADD CONSTRAINT FOREIGN KEY ( jobCategoryId ) REFERENCES jobcategorymaster( jobCategoryId );

ALTER TABLE districtapprovalgroups ADD CONSTRAINT FOREIGN KEY ( jobId ) REFERENCES joborder( jobId );

alter table districtmaster add column buildApprovalGroup boolean default false;

alter table jobapprovalhistory add column districtApprovalGroupsId  int default null;

ALTER TABLE jobapprovalhistory ADD CONSTRAINT FOREIGN KEY ( districtApprovalGroupsId ) REFERENCES districtapprovalgroups( districtApprovalGroupsId ) ;


---------------------------------------------------sandeep singh(22-04-2015)--------------------------------------------

ALTER TABLE `districtmaster` ADD `displayCommunication` INT( 10 ) ;

-------------------------------------------------Amit 23-04-2015----------------------------------------------------------------
ALTER TABLE `jobforteacher` ADD `flagForDspq` tinyint(4) NULL default NULL AFTER `districtSpecificNoteFinalizeDate` ;
-------------------------------------------------ankit saini 23-04-2015----------------------------------------------------------------
ALTER TABLE  `menumaster` ADD  `subMenuId` INT( 5 ) NULL AFTER  `parentMenuId` ;
ALTER TABLE  `menumaster` ADD  `districtId` VARCHAR(200) NULL AFTER  `subMenuId` ;
ALTER TABLE  `menumaster` ADD  `menuArrow` INT( 5 ) NULL AFTER  `districtId` ;
------------------ vishwanath 24-4-2015 ----------------------
ALTER TABLE  `assessmentquestions` ADD  `questionWeightage` DOUBLE NOT NULL DEFAULT  '1' COMMENT  'By Default "1" ' AFTER  `status` ;

CREATE TABLE IF NOT EXISTS `assessmentgroupdetails` (
  `assessmentGroupId` int(5) NOT NULL AUTO_INCREMENT,
  `assessmentGroupName` varchar(100) NOT NULL DEFAULT '0',
  `assessmentType` int(1) NOT NULL DEFAULT '0' COMMENT '"1" -  Base, "2" - Job Specific',
  `createdBy` int(10) NOT NULL DEFAULT '0' COMMENT 'Foreign Key to " in userId" table "userMaster"',
  `status` varchar(1) NOT NULL DEFAULT '0' COMMENT '"A" - Active, "I" - Inactive',
  `createdDateTime` datetime NOT NULL COMMENT 'Current Date and Time ',
  `ipaddress` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`assessmentGroupId`),
  UNIQUE KEY `assessmentGroupId` (`assessmentGroupId`),
  KEY `FK_assessmentgroupdetails_usermaster` (`createdBy`),
  CONSTRAINT `FK_assessmentgroupdetails_usermaster` FOREIGN KEY (`createdBy`) REFERENCES `usermaster` (`userId`)
) ENGINE=InnoDB;

 alter table assessmentdetail ADD `assessmentGroupId` int(5) default NULL;

 alter table assessmentdetail ADD foreign key (assessmentGroupId) references assessmentgroupdetails(assessmentGroupId);

update assessmentquestions INNER JOIN questionspool ON assessmentquestions.`questionId`=questionspool.`questionId` SET assessmentquestions.`questionWeightage`=questionspool.`questionWeightage`;

--- do not run on titan -------------------------------------------------------------------
INSERT INTO `scorelookupmaster` (`lookupId`, `name`, `description`, `createdDateTime`, `updatedDateTime`, `createdBy`) VALUES
(1, 'EPI Form 1 Scores 2013-14', 'EPI Form 1 Scores 2013-14', '2015-03-12 05:00:00', '2015-03-12 05:00:00', 1);

INSERT INTO `scorelookup` (`scorelookupId`, `lookupId`, `domainId`, `competencyId`, `totalscore`, `percentile`, `zscore`, `tscore`, `ncevalue`, `normscore`, `ritvalue`, `ritse`, `pass`, `createdDateTime`, `createdBy`) VALUES
(1, 1, 1, NULL, 0, 0, -19.17, 0, 1, 0, 0, 0, '', '2015-03-31 14:32:30', 1),
(2, 1, 1, NULL, 25, 0, -19.17, 0, 1, 0, -7.47, 1.85, '', '2015-03-31 14:32:30', 1),
(3, 1, 1, NULL, 26, 0, -15.97, 0, 1, 0, -6.19, 1.05, '', '2015-03-31 14:32:30', 1),
(4, 1, 1, NULL, 27, 0, -13.995, 0, 1, 0, -5.4, 0.77, '', '2015-03-31 14:32:30', 1),
(5, 1, 1, NULL, 28, 0, -12.745, 0, 1, 0, -4.9, 0.65, '', '2015-03-31 14:32:30', 1),
(6, 1, 1, NULL, 29, 0, -11.795, 0, 1, 0, -4.52, 0.58, '', '2015-03-31 14:32:30', 1),
(7, 1, 1, NULL, 30, 0, -11.02, 0, 1, 0, -4.21, 0.53, '', '2015-03-31 14:32:30', 1),
(8, 1, 1, NULL, 31, 0, -10.37, 0, 1, 0, -3.95, 0.49, '', '2015-03-31 14:32:30', 1),
(9, 1, 1, NULL, 32, 0, -9.795, 0, 1, 0, -3.72, 0.46, '', '2015-03-31 14:32:30', 1),
(10, 1, 1, NULL, 33, 0, -9.295, 0, 1, 0, -3.52, 0.44, '', '2015-03-31 14:32:30', 1),
(11, 1, 1, NULL, 34, 0, -8.82, 0, 1, 0, -3.33, 0.42, '', '2015-03-31 14:32:30', 1),
(12, 1, 1, NULL, 35, 0, -8.395, 0, 1, 0, -3.16, 0.4, '', '2015-03-31 14:32:30', 1),
(13, 1, 1, NULL, 36, 0, -7.995, 0, 1, 0, -3, 0.39, '', '2015-03-31 14:32:30', 1),
(14, 1, 1, NULL, 37, 0, -7.645, 0, 1, 0, -2.86, 0.38, '', '2015-03-31 14:32:30', 1),
(15, 1, 1, NULL, 38, 0, -7.295, 0, 1, 0, -2.72, 0.36, '', '2015-03-31 14:32:30', 1),
(16, 1, 1, NULL, 39, 0, -6.97, 0, 1, 0, -2.59, 0.35, '', '2015-03-31 14:32:30', 1),
(17, 1, 1, NULL, 40, 0, -6.67, 0, 1, 0, -2.47, 0.35, '', '2015-03-31 14:32:30', 1),
(18, 1, 1, NULL, 41, 0, -6.37, 0, 1, 0, -2.35, 0.34, '', '2015-03-31 14:32:30', 1),
(19, 1, 1, NULL, 42, 0, -6.095, 0, 1, 0, -2.24, 0.33, '', '2015-03-31 14:32:30', 1),
(20, 1, 1, NULL, 43, 0, -5.845, 0, 1, 0, -2.14, 0.32, '', '2015-03-31 14:32:30', 1),
(21, 1, 1, NULL, 44, 0, -5.595, 0, 1, 0, -2.04, 0.32, '', '2015-03-31 14:32:30', 1),
(22, 1, 1, NULL, 45, 0, -5.345, 0, 1, 0, -1.94, 0.31, '', '2015-03-31 14:32:30', 1),
(23, 1, 1, NULL, 46, 0, -5.095, 0, 1, 0, -1.84, 0.31, '', '2015-03-31 14:32:30', 1),
(24, 1, 1, NULL, 47, 0, -4.87, 1.3, 1, 1, -1.75, 0.3, '', '2015-03-31 14:32:30', 1),
(25, 1, 1, NULL, 48, 0, -4.645, 3.55, 1, 4, -1.66, 0.3, '', '2015-03-31 14:32:30', 1),
(26, 1, 1, NULL, 49, 0, -4.445, 5.55, 1, 6, -1.58, 0.29, '', '2015-03-31 14:32:30', 1),
(27, 1, 1, NULL, 50, 0, -4.22, 7.8, 1, 8, -1.49, 0.29, '', '2015-03-31 14:32:30', 1),
(28, 1, 1, NULL, 51, 0, -4.02, 9.8, 1, 10, -1.41, 0.28, '', '2015-03-31 14:32:30', 1),
(29, 1, 1, NULL, 52, 0, -3.82, 11.8, 1, 12, -1.33, 0.28, '', '2015-03-31 14:32:30', 1),
(30, 1, 1, NULL, 53, 0.01, -3.62, 13.8, 1, 14, -1.25, 0.28, '', '2015-03-31 14:32:30', 1),
(31, 1, 1, NULL, 54, 0.01, -3.62, 13.8, 1, 14, -1.18, 0.27, '', '2015-03-31 14:32:30', 1),
(32, 1, 1, NULL, 55, 0.01, -3.62, 13.8, 1, 14, -1.1, 0.27, '', '2015-03-31 14:32:30', 1),
(33, 1, 1, NULL, 56, 0.02, -3.49, 15.1, 1, 15, -1.03, 0.27, '', '2015-03-31 14:32:30', 1),
(34, 1, 1, NULL, 57, 0.05, -3.27, 17.3, 1, 17, -0.96, 0.27, '', '2015-03-31 14:32:30', 1),
(35, 1, 1, NULL, 58, 0.12, -3.03, 19.7, 1, 20, -0.88, 0.26, '', '2015-03-31 14:32:30', 1),
(36, 1, 1, NULL, 59, 0.22, -2.85, 21.5, 1, 22, -0.82, 0.26, '', '2015-03-31 14:32:30', 1),
(37, 1, 1, NULL, 60, 0.36, -2.69, 23.1, 1, 23, -0.75, 0.26, '', '2015-03-31 14:32:30', 1),
(38, 1, 1, NULL, 61, 0.57, -2.53, 24.7, 1, 25, -0.68, 0.26, '', '2015-03-31 14:32:30', 1),
(39, 1, 1, NULL, 62, 0.9, -2.37, 26.3, 1, 26, -0.61, 0.26, '', '2015-03-31 14:32:30', 1),
(40, 1, 1, NULL, 63, 1.44, -2.19, 28.1, 1, 28, -0.55, 0.26, '', '2015-03-31 14:32:30', 1),
(41, 1, 1, NULL, 64, 2.29, -2, 30, 5, 30, -0.48, 0.25, '', '2015-03-31 14:32:30', 1),
(42, 1, 1, NULL, 65, 3.58, -1.8, 32, 10, 32, -0.42, 0.25, '', '2015-03-31 14:32:30', 1),
(43, 1, 1, NULL, 66, 5.53, -1.6, 34, 14, 34, -0.35, 0.25, '', '2015-03-31 14:32:30', 1),
(44, 1, 1, NULL, 67, 8.35, -1.38, 36.2, 19, 36, -0.29, 0.25, '', '2015-03-31 14:32:30', 1),
(45, 1, 1, NULL, 68, 12.18, -1.17, 38.3, 23, 38, -0.23, 0.25, '', '2015-03-31 14:32:30', 1),
(46, 1, 1, NULL, 69, 17.23, -0.95, 40.5, 28, 41, -0.17, 0.25, '', '2015-03-31 14:32:30', 1),
(47, 1, 1, NULL, 70, 23.47, -0.72, 42.8, 32, 43, -0.1, 0.25, '', '2015-03-31 14:32:30', 1),
(48, 1, 1, NULL, 71, 30.95, -0.5, 45, 37, 45, -0.04, 0.25, '', '2015-03-31 14:32:30', 1),
(49, 1, 1, NULL, 72, 39.44, -0.27, 47.3, 41, 47, 0.02, 0.25, '', '2015-03-31 14:32:30', 1),
(50, 1, 1, NULL, 73, 48.55, -0.04, 49.6, 46, 50, 0.08, 0.25, '', '2015-03-31 14:32:30', 1),
(51, 1, 1, NULL, 74, 57.75, 0.2, 52, 50, 52, 0.14, 0.25, '', '2015-03-31 14:32:30', 1),
(52, 1, 1, NULL, 75, 66.38, 0.42, 54.2, 54, 54, 0.2, 0.25, '', '2015-03-31 14:32:30', 1),
(53, 1, 1, NULL, 76, 74.24, 0.65, 56.5, 59, 57, 0.26, 0.24, '', '2015-03-31 14:32:30', 1),
(54, 1, 1, NULL, 77, 81.15, 0.88, 58.8, 63, 59, 0.32, 0.24, '', '2015-03-31 14:32:30', 1),
(55, 1, 1, NULL, 78, 86.74, 1.11, 61.1, 68, 61, 0.38, 0.24, '', '2015-03-31 14:32:30', 1),
(56, 1, 1, NULL, 79, 90.73, 1.32, 63.2, 72, 63, 0.44, 0.24, '', '2015-03-31 14:32:30', 1),
(57, 1, 1, NULL, 80, 93.69, 1.53, 65.3, 77, 65, 0.5, 0.24, '', '2015-03-31 14:32:30', 1),
(58, 1, 1, NULL, 81, 95.95, 1.74, 67.4, 81, 67, 0.56, 0.24, '', '2015-03-31 14:32:30', 1),
(59, 1, 1, NULL, 82, 97.44, 1.95, 69.5, 86, 70, 0.62, 0.25, '', '2015-03-31 14:32:30', 1),
(60, 1, 1, NULL, 83, 98.4, 2.14, 71.4, 90, 71, 0.68, 0.25, '', '2015-03-31 14:32:30', 1),
(61, 1, 1, NULL, 84, 98.98, 2.32, 73.2, 95, 73, 0.74, 0.25, '', '2015-03-31 14:32:30', 1),
(62, 1, 1, NULL, 85, 99.32, 2.47, 74.7, 99, 75, 0.8, 0.25, '', '2015-03-31 14:32:30', 1),
(63, 1, 1, NULL, 86, 99.55, 2.61, 76.1, 99, 76, 0.86, 0.25, '', '2015-03-31 14:32:30', 1),
(64, 1, 1, NULL, 87, 99.68, 2.73, 77.3, 99, 77, 0.92, 0.25, '', '2015-03-31 14:32:30', 1),
(65, 1, 1, NULL, 88, 99.76, 2.82, 78.2, 99, 78, 0.98, 0.25, '', '2015-03-31 14:32:30', 1),
(66, 1, 1, NULL, 89, 99.82, 2.92, 79.2, 99, 79, 1.05, 0.25, '', '2015-03-31 14:32:30', 1),
(67, 1, 1, NULL, 90, 99.88, 3.04, 80.4, 99, 80, 1.11, 0.25, '', '2015-03-31 14:32:30', 1),
(68, 1, 1, NULL, 91, 99.9, 3.1, 81, 99, 81, 1.17, 0.25, '', '2015-03-31 14:32:30', 1),
(69, 1, 1, NULL, 92, 99.92, 3.17, 81.7, 99, 82, 1.24, 0.25, '', '2015-03-31 14:32:30', 1),
(70, 1, 1, NULL, 93, 99.95, 3.32, 83.2, 99, 83, 1.3, 0.25, '', '2015-03-31 14:32:30', 1),
(71, 1, 1, NULL, 94, 99.98, 3.61, 86.1, 99, 86, 1.36, 0.26, '', '2015-03-31 14:32:30', 1),
(72, 1, 1, NULL, 96, 99.98, 3.61, 86.1, 99, 86, 1.5, 0.26, '', '2015-03-31 14:32:30', 1),
(73, 1, 1, NULL, 97, 99.99, 3.89, 88.9, 99, 89, 1.57, 0.26, '', '2015-03-31 14:32:30', 1),
(74, 1, 1, NULL, 98, 100, 4.04, 90.4, 99, 90, 1.63, 0.26, '', '2015-03-31 14:32:30', 1),
(75, 1, 1, NULL, 99, 100, 4.215, 92.15, 99, 92, 1.7, 0.27, '', '2015-03-31 14:32:30', 1),
(76, 1, 1, NULL, 100, 100, 4.415, 94.15, 99, 94, 1.78, 0.27, '', '2015-03-31 14:32:30', 1),
(77, 1, 1, NULL, 101, 100, 4.59, 95.9, 99, 96, 1.85, 0.27, '', '2015-03-31 14:32:30', 1),
(78, 1, 1, NULL, 102, 100, 4.765, 97.65, 99, 98, 1.92, 0.27, '', '2015-03-31 14:32:30', 1),
(79, 1, 1, NULL, 103, 100, 4.965, 99.65, 99, 100, 2, 0.28, '', '2015-03-31 14:32:30', 1),
(80, 1, 1, NULL, 104, 100, 5.165, 100, 99, 100, 2.08, 0.28, '', '2015-03-31 14:32:30', 1),
(81, 1, 1, NULL, 105, 100, 5.365, 100, 99, 100, 2.16, 0.29, '', '2015-03-31 14:32:30', 1),
(82, 1, 1, NULL, 106, 100, 5.565, 100, 99, 100, 2.24, 0.29, '', '2015-03-31 14:32:30', 1),
(83, 1, 1, NULL, 107, 100, 5.79, 100, 99, 100, 2.33, 0.29, '', '2015-03-31 14:32:30', 1),
(84, 1, 1, NULL, 108, 100, 5.99, 100, 99, 100, 2.41, 0.3, '', '2015-03-31 14:32:30', 1),
(85, 1, 1, NULL, 109, 100, 6.24, 100, 99, 100, 2.51, 0.31, '', '2015-03-31 14:32:30', 1),
(86, 1, 1, NULL, 110, 100, 6.465, 100, 99, 100, 2.6, 0.31, '', '2015-03-31 14:32:30', 1),
(87, 1, 1, NULL, 111, 100, 6.715, 100, 99, 100, 2.7, 0.32, '', '2015-03-31 14:32:30', 1),
(88, 1, 1, NULL, 112, 100, 6.99, 100, 99, 100, 2.81, 0.33, '', '2015-03-31 14:32:30', 1),
(89, 1, 1, NULL, 113, 100, 7.265, 100, 99, 100, 2.92, 0.34, '', '2015-03-31 14:32:30', 1),
(90, 1, 1, NULL, 114, 100, 7.565, 100, 99, 100, 3.04, 0.35, '', '2015-03-31 14:32:30', 1),
(91, 1, 1, NULL, 115, 100, 7.865, 100, 99, 100, 3.16, 0.36, '', '2015-03-31 14:32:30', 1),
(92, 1, 1, NULL, 116, 100, 8.215, 100, 99, 100, 3.3, 0.38, '', '2015-03-31 14:32:30', 1),
(93, 1, 1, NULL, 117, 100, 8.59, 100, 99, 100, 3.45, 0.39, '', '2015-03-31 14:32:30', 1),
(94, 1, 1, NULL, 118, 100, 8.99, 100, 99, 100, 3.61, 0.42, '', '2015-03-31 14:32:30', 1),
(95, 1, 1, NULL, 119, 100, 9.465, 100, 99, 100, 3.8, 0.44, '', '2015-03-31 14:32:30', 1),
(96, 1, 1, NULL, 120, 100, 9.99, 100, 99, 100, 4.01, 0.48, '', '2015-03-31 14:32:30', 1),
(97, 1, 1, NULL, 121, 100, 10.615, 100, 99, 100, 4.26, 0.53, '', '2015-03-31 14:32:30', 1),
(98, 1, 1, NULL, 122, 100, 11.415, 100, 99, 100, 4.58, 0.6, '', '2015-03-31 14:32:30', 1),
(99, 1, 1, NULL, 123, 100, 12.515, 100, 99, 100, 5.02, 0.73, '', '2015-03-31 14:32:30', 1),
(100, 1, 1, NULL, 124, 100, 14.315, 100, 99, 100, 5.74, 1.02, '', '2015-03-31 14:32:30', 1),
(101, 1, 1, NULL, 125, 100, 17.39, 100, 99, 100, 6.97, 1.83, '', '2015-03-31 14:32:30', 1),
(102, 1, 1, NULL, 126, 100, 17.39, 100, 99, 100, 0, 0, '', '2015-03-31 14:32:30', 1),
(103, 1, 2, NULL, 0, 0.24, -2.82, 21.8, 1, 22, -3.89, 1.91, '', '2015-03-31 14:32:30', 1),
(104, 1, 2, NULL, 1, 1.74, -2.11, 28.9, 13, 29, -2.45, 1.15, '', '2015-03-31 14:32:30', 1),
(105, 1, 2, NULL, 2, 6.64, -1.5, 35, 26, 35, -1.42, 0.91, '', '2015-03-31 14:32:30', 1),
(106, 1, 2, NULL, 3, 17.47, -0.94, 40.6, 38, 41, -0.68, 0.83, '', '2015-03-31 14:32:30', 1),
(107, 1, 2, NULL, 4, 34.71, -0.39, 46.1, 50, 46, -0.01, 0.81, '', '2015-03-31 14:32:30', 1),
(108, 1, 2, NULL, 5, 55.64, 0.14, 51.4, 62, 51, 0.66, 0.84, '', '2015-03-31 14:32:30', 1),
(109, 1, 2, NULL, 6, 75.92, 0.7, 57, 75, 57, 1.42, 0.93, '', '2015-03-31 14:32:30', 1),
(110, 1, 2, NULL, 7, 90.96, 1.34, 63.4, 87, 63, 2.47, 1.16, '', '2015-03-31 14:32:30', 1),
(111, 1, 2, NULL, 8, 98.31, 2.12, 71.2, 99, 71, 3.91, 1.91, '', '2015-03-31 14:32:30', 1),
(112, 1, 3, NULL, 0, 0, -6.105, 0, 1, 0, -5.35, 1.84, '', '2015-03-31 14:32:30', 1),
(113, 1, 3, NULL, 1, 0, -4.989, 0.11, 1, 0, -4.11, 1.03, '', '2015-03-31 14:32:30', 1),
(114, 1, 3, NULL, 2, 0, -4.314, 6.86, 1, 7, -3.36, 0.74, '', '2015-03-31 14:32:30', 1),
(115, 1, 3, NULL, 3, 0, -3.9, 11, 1, 11, -2.9, 0.62, '', '2015-03-31 14:32:30', 1),
(116, 1, 3, NULL, 4, 0.01, -3.62, 13.8, 1, 14, -2.57, 0.55, '', '2015-03-31 14:32:30', 1),
(117, 1, 3, NULL, 6, 0.02, -3.49, 15.1, 1, 15, -2.29, 0.5, '', '2015-03-31 14:32:30', 1),
(118, 1, 3, NULL, 7, 0.06, -3.22, 17.8, 1, 18, -2.06, 0.47, '', '2015-03-31 14:32:30', 1),
(119, 1, 3, NULL, 8, 0.12, -3.03, 19.7, 1, 20, -1.85, 0.44, '', '2015-03-31 14:32:30', 1),
(120, 1, 3, NULL, 9, 0.22, -2.85, 21.5, 1, 22, -1.67, 0.42, '', '2015-03-31 14:32:30', 1),
(121, 1, 3, NULL, 10, 0.37, -2.68, 23.2, 1, 23, -1.5, 0.4, '', '2015-03-31 14:32:30', 1),
(122, 1, 3, NULL, 11, 0.56, -2.54, 24.6, 1, 25, -1.34, 0.39, '', '2015-03-31 14:32:30', 1),
(123, 1, 3, NULL, 12, 0.8, -2.41, 25.9, 1, 26, -1.2, 0.38, '', '2015-03-31 14:32:30', 1),
(124, 1, 3, NULL, 13, 1.08, -2.3, 27, 1, 27, -1.06, 0.37, '', '2015-03-31 14:32:30', 1),
(125, 1, 3, NULL, 14, 1.43, -2.19, 28.1, 5, 28, -0.92, 0.36, '', '2015-03-31 14:32:30', 1),
(126, 1, 3, NULL, 15, 1.91, -2.07, 29.3, 8, 29, -0.79, 0.36, '', '2015-03-31 14:32:30', 1),
(127, 1, 3, NULL, 16, 2.5, -1.96, 30.4, 12, 30, -0.67, 0.35, '', '2015-03-31 14:32:30', 1),
(128, 1, 3, NULL, 17, 3.2, -1.85, 31.5, 16, 32, -0.55, 0.35, '', '2015-03-31 14:32:30', 1),
(129, 1, 3, NULL, 18, 4.06, -1.74, 32.6, 19, 33, -0.43, 0.34, '', '2015-03-31 14:32:30', 1),
(130, 1, 3, NULL, 19, 5.23, -1.62, 33.8, 23, 34, -0.32, 0.34, '', '2015-03-31 14:32:30', 1),
(131, 1, 3, NULL, 20, 6.64, -1.5, 35, 26, 35, -0.2, 0.34, '', '2015-03-31 14:32:30', 1),
(132, 1, 3, NULL, 21, 8.34, -1.38, 36.2, 30, 36, -0.09, 0.33, '', '2015-03-31 14:32:30', 1),
(133, 1, 3, NULL, 22, 10.38, -1.26, 37.4, 34, 37, 0.02, 0.33, '', '2015-03-31 14:32:30', 1),
(134, 1, 3, NULL, 23, 12.82, -1.13, 38.7, 37, 39, 0.13, 0.33, '', '2015-03-31 14:32:30', 1),
(135, 1, 3, NULL, 24, 15.82, -1, 40, 41, 40, 0.25, 0.33, '', '2015-03-31 14:32:30', 1),
(136, 1, 3, NULL, 25, 19.21, -0.87, 41.3, 45, 41, 0.36, 0.34, '', '2015-03-31 14:32:30', 1),
(137, 1, 3, NULL, 26, 23.12, -0.73, 42.7, 48, 43, 0.47, 0.34, '', '2015-03-31 14:32:30', 1),
(138, 1, 3, NULL, 27, 27.88, -0.59, 44.1, 52, 44, 0.59, 0.34, '', '2015-03-31 14:32:30', 1),
(139, 1, 3, NULL, 28, 33.49, -0.43, 45.7, 55, 46, 0.7, 0.34, '', '2015-03-31 14:32:30', 1),
(140, 1, 3, NULL, 29, 39.65, -0.26, 47.4, 59, 47, 0.82, 0.35, '', '2015-03-31 14:32:30', 1),
(141, 1, 3, NULL, 30, 46.28, -0.09, 49.1, 63, 49, 0.95, 0.35, '', '2015-03-31 14:32:30', 1),
(142, 1, 3, NULL, 31, 53.56, 0.09, 50.9, 66, 51, 1.08, 0.36, '', '2015-03-31 14:32:30', 1),
(143, 1, 3, NULL, 32, 61.34, 0.29, 52.9, 70, 53, 1.21, 0.37, '', '2015-03-31 14:32:30', 1),
(144, 1, 3, NULL, 33, 69.2, 0.5, 55, 74, 55, 1.35, 0.38, '', '2015-03-31 14:32:30', 1),
(145, 1, 3, NULL, 34, 76.68, 0.73, 57.3, 77, 57, 1.5, 0.39, '', '2015-03-31 14:32:30', 1),
(146, 1, 3, NULL, 35, 83.6, 0.98, 59.8, 81, 60, 1.66, 0.41, '', '2015-03-31 14:32:30', 1),
(147, 1, 3, NULL, 36, 89.49, 1.25, 62.5, 84, 63, 1.84, 0.43, '', '2015-03-31 14:32:30', 1),
(148, 1, 3, NULL, 37, 93.88, 1.54, 65.4, 88, 65, 2.04, 0.46, '', '2015-03-31 14:32:30', 1),
(149, 1, 3, NULL, 38, 96.89, 1.86, 68.6, 92, 69, 2.26, 0.49, '', '2015-03-31 14:32:30', 1),
(150, 1, 3, NULL, 39, 98.73, 2.23, 72.3, 95, 72, 2.52, 0.54, '', '2015-03-31 14:32:30', 1),
(151, 1, 3, NULL, 40, 99.62, 2.67, 76.7, 99, 77, 2.85, 0.61, '', '2015-03-31 14:32:30', 1),
(152, 1, 3, NULL, 41, 99.92, 3.17, 81.7, 99, 82, 3.29, 0.73, '', '2015-03-31 14:32:30', 1),
(153, 1, 3, NULL, 42, 99.99, 3.89, 88.9, 99, 89, 4.02, 1.02, '', '2015-03-31 14:32:30', 1),
(154, 1, 3, NULL, 57, 100, 3.9, 89, 99, 89, 5.25, 1.84, '', '2015-03-31 14:32:30', 1),
(211, 1, NULL, 9, 0, 0.05, -3.27, 17.3, 1, 17, 0, 0, '', '2015-03-31 14:38:49', 1),
(212, 1, NULL, 9, 1, 0.35, -2.7, 23, 1, 23, 0, 0, '', '2015-03-31 14:38:49', 1),
(213, 1, NULL, 9, 2, 1.47, -2.18, 28.2, 1, 28, 0, 0, '', '2015-03-31 14:38:49', 1),
(214, 1, NULL, 9, 3, 4.65, -1.68, 33.2, 8, 33, 0, 0, '', '2015-03-31 14:38:49', 1),
(215, 1, NULL, 9, 4, 12.17, -1.17, 38.3, 15, 38, 0, 0, '', '2015-03-31 14:38:49', 1),
(216, 1, NULL, 9, 5, 25.46, -0.66, 43.4, 22, 43, 0, 0, '', '2015-03-31 14:38:49', 1),
(217, 1, NULL, 9, 6, 41.53, -0.21, 47.9, 29, 48, 0, 0, '', '2015-03-31 14:38:49', 1),
(218, 1, NULL, 9, 7, 55.24, 0.13, 51.3, 36, 51, 0, 0, '', '2015-03-31 14:38:49', 1),
(219, 1, NULL, 9, 8, 65.74, 0.41, 54.1, 43, 54, 0, 0, '', '2015-03-31 14:38:49', 1),
(220, 1, NULL, 9, 9, 73.89, 0.64, 56.4, 50, 56, 0, 0, '', '2015-03-31 14:38:49', 1),
(221, 1, NULL, 9, 10, 79.59, 0.83, 58.3, 57, 58, 0, 0, '', '2015-03-31 14:38:49', 1),
(222, 1, NULL, 9, 11, 83.37, 0.97, 59.7, 64, 60, 0, 0, '', '2015-03-31 14:38:49', 1),
(223, 1, NULL, 9, 12, 86.48, 1.1, 61, 71, 61, 0, 0, '', '2015-03-31 14:38:49', 1),
(224, 1, NULL, 9, 13, 89.93, 1.28, 62.8, 78, 63, 0, 0, '', '2015-03-31 14:38:49', 1),
(225, 1, NULL, 9, 14, 93.44, 1.51, 65.1, 85, 65, 0, 0, '', '2015-03-31 14:38:49', 1),
(226, 1, NULL, 9, 15, 96.44, 1.8, 68, 92, 68, 0, 0, '', '2015-03-31 14:38:49', 1),
(227, 1, NULL, 9, 16, 98.55, 2.18, 71.8, 99, 72, 0, 0, '', '2015-03-31 14:38:49', 1),
(228, 1, NULL, 9, 17, 99.64, 2.69, 76.9, 99, 77, 0, 0, '', '2015-03-31 14:38:49', 1),
(229, 1, NULL, 9, 18, 99.96, 3.38, 83.8, 99, 84, 0, 0, '', '2015-03-31 14:38:49', 1),
(230, 1, NULL, 10, 0, 0.09, -3.19, 18.1, 1, 18, 0, 0, '', '2015-03-31 14:38:49', 1),
(231, 1, NULL, 10, 1, 0.62, -2.5, 25, 1, 25, 0, 0, '', '2015-03-31 14:38:49', 1),
(232, 1, NULL, 10, 2, 2.57, -1.95, 30.5, 13.25, 31, 0, 0, '', '2015-03-31 14:38:49', 1),
(233, 1, NULL, 10, 3, 8.18, -1.39, 36.1, 25.5, 36, 0, 0, '', '2015-03-31 14:38:49', 1),
(234, 1, NULL, 10, 4, 21.49, -0.79, 42.1, 37.75, 42, 0, 0, '', '2015-03-31 14:38:49', 1),
(235, 1, NULL, 10, 5, 44.6, -0.14, 48.6, 50, 49, 0, 0, '', '2015-03-31 14:38:49', 1),
(236, 1, NULL, 10, 6, 70.72, 0.55, 55.5, 62.25, 56, 0, 0, '', '2015-03-31 14:38:49', 1),
(237, 1, NULL, 10, 7, 88.95, 1.22, 62.2, 74.5, 62, 0, 0, '', '2015-03-31 14:38:49', 1),
(238, 1, NULL, 10, 8, 97.48, 1.96, 69.6, 86.75, 70, 0, 0, '', '2015-03-31 14:38:49', 1),
(239, 1, NULL, 10, 9, 100, 3.9, 89, 99, 89, 0, 0, '', '2015-03-31 14:38:49', 1),
(240, 1, NULL, 11, 0, 0.06, -3.22, 17.8, 1, 18, 0, 0, '', '2015-03-31 14:38:49', 1),
(241, 1, NULL, 11, 1, 0.42, -2.64, 23.6, 1, 24, 0, 0, '', '2015-03-31 14:38:49', 1),
(242, 1, NULL, 11, 2, 1.75, -2.11, 28.9, 7.125, 29, 0, 0, '', '2015-03-31 14:38:49', 1),
(243, 1, NULL, 11, 3, 5.56, -1.59, 34.1, 13.25, 34, 0, 0, '', '2015-03-31 14:38:49', 1),
(244, 1, NULL, 11, 4, 14.62, -1.05, 39.5, 19.375, 40, 0, 0, '', '2015-03-31 14:38:49', 1),
(245, 1, NULL, 11, 5, 30.34, -0.51, 44.9, 25.5, 45, 0, 0, '', '2015-03-31 14:38:49', 1),
(246, 1, NULL, 11, 6, 48.26, -0.04, 49.6, 31.625, 50, 0, 0, '', '2015-03-31 14:38:49', 1),
(247, 1, NULL, 11, 7, 61.25, 0.29, 52.9, 37.75, 53, 0, 0, '', '2015-03-31 14:38:49', 1),
(248, 1, NULL, 11, 8, 68.26, 0.47, 54.7, 43.875, 55, 0, 0, '', '2015-03-31 14:38:49', 1),
(249, 1, NULL, 11, 9, 71.85, 0.58, 55.8, 50, 56, 0, 0, '', '2015-03-31 14:38:49', 1),
(250, 1, NULL, 11, 10, 74.42, 0.66, 56.6, 56.125, 57, 0, 0, '', '2015-03-31 14:38:49', 1),
(251, 1, NULL, 11, 11, 77.83, 0.77, 57.7, 62.25, 58, 0, 0, '', '2015-03-31 14:38:49', 1),
(252, 1, NULL, 11, 12, 81.97, 0.91, 59.1, 68.375, 59, 0, 0, '', '2015-03-31 14:38:49', 1),
(253, 1, NULL, 11, 13, 86.58, 1.11, 61.1, 74.5, 61, 0, 0, '', '2015-03-31 14:38:49', 1),
(254, 1, NULL, 11, 14, 91.25, 1.36, 63.6, 80.625, 64, 0, 0, '', '2015-03-31 14:38:49', 1),
(255, 1, NULL, 11, 15, 95.25, 1.67, 66.7, 86.75, 67, 0, 0, '', '2015-03-31 14:38:49', 1),
(256, 1, NULL, 11, 16, 98.06, 2.07, 70.7, 92.875, 71, 0, 0, '', '2015-03-31 14:38:49', 1),
(257, 1, NULL, 11, 17, 99.52, 2.59, 75.9, 99, 76, 0, 0, '', '2015-03-31 14:38:49', 1),
(258, 1, NULL, 11, 18, 99.94, 3.26, 82.6, 99, 83, 0, 0, '', '2015-03-31 14:38:49', 1),
(259, 1, NULL, 12, 0, 0.13, -3, 20, 1, 20, 0, 0, '', '2015-03-31 14:38:49', 1),
(260, 1, NULL, 12, 1, 0.87, -2.38, 26.2, 1, 26, 0, 0, '', '2015-03-31 14:38:49', 1),
(261, 1, NULL, 12, 2, 3.55, -1.81, 31.9, 17.33333333, 32, 0, 0, '', '2015-03-31 14:38:49', 1),
(262, 1, NULL, 12, 3, 11.34, -1.21, 37.9, 33.66666667, 38, 0, 0, '', '2015-03-31 14:38:49', 1),
(263, 1, NULL, 12, 4, 29.8, -0.53, 44.7, 50, 45, 0, 0, '', '2015-03-31 14:38:49', 1),
(264, 1, NULL, 12, 5, 60.15, 0.26, 52.6, 66.33333333, 53, 0, 0, '', '2015-03-31 14:38:49', 1),
(265, 1, NULL, 12, 6, 88.87, 1.22, 62.2, 82.66666667, 62, 0, 0, '', '2015-03-31 14:38:49', 1),
(266, 1, NULL, 12, 7, 100, 3.9, 89, 99, 89, 0, 0, '', '2015-03-31 14:38:49', 1);
------------------------------------------------------------------------------------------------
-----------------------------------ankit saini 25-04-2015-------------------------------------------------------------

ALTER TABLE  `usermaster` ADD  `tableauUserStatus` VARCHAR( 1 ) CHARACTER SET swe7 COLLATE swe7_swedish_ci NOT NULL DEFAULT  'A' COMMENT  '"I" - Inactive, "A" - Active (By Default)' AFTER  `createdDateTime` ;
ALTER TABLE  `tableauusermaster` ADD  `entityType` INT( 5 ) NULL AFTER  `createdDateTime` ;
-------------------------------------------------Amit 25-04-2015----------------------------------------------------------------
ALTER TABLE `teacherassessmentstatus` ADD `assessmentCompletedDateTime` DATETIME NULL DEFAULT NULL AFTER `updatedDate` ;

UPDATE `teacherassessmentstatus` t SET t.assessmentCompletedDateTime = ( SELECT max( t1.assessmentEndTime ) FROM `teacherassessmentattempt` t1 WHERE t1.teacherId = t.teacherId AND t1.assessmentId = t.assessmentId );
------------------------vishwanath 25-4-2015-------------------------------------------------------------
UPDATE  `assessmentdetail` SET  `lookupId` =  '1' WHERE  `assessmentdetail`.`assessmentId` =110;
UPDATE `teachermatch`.`teacherassessmentdetails` SET `lookupId` = '1' WHERE assessmentId=110;
UPDATE `teachermatch`.`teacherassessmentdetails` SET `lookupId` = '1' WHERE `assessmentType` =1;
 
ALTER TABLE  `teacherdistrictstrikelog` CHANGE  `ipAddress`  `ipAddress` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE  `teacherstrikelog` CHANGE  `ipAddress`  `ipAddress` VARCHAR( 100 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL


---------------------sandeep singh(25-04-2015)--------------------------------------------
 ALTER TABLE `districtmaster` CHANGE `displayCommunication` `displayCommunication` INT( 10 ) NULL DEFAULT '0'; 
 
 ----*********************************************************************************************************
 ----**********************  Query is updated on Cloud, Demo and Platform 27-04-2015 *************************
 ----*********************************************************************************************************
-------------------------------Shadab Ansari(27-04-2015)-----------------------------------------------------
Alter table jobcategorymaster add column schoolSelection boolean default true;

-------------------------------Shadab Ansari(29-04-2015)-----------------------------------------------------
ALTER TABLE districtmaster ADD COLUMN sendNotificationOnNegativeQQ VARCHAR( 100 ) DEFAULT NULL ;

---------------------Gourav 5-5-2015-------------------------------------------
CREATE TABLE `qqquestionsets` (
  `ID` int(10) NOT NULL auto_increment,
  `DistrictID` int(11) NOT NULL,
  `QuestionSetText` varchar(100) NOT NULL,
  `QQQuestionSetID` varchar(10) default NULL,
  `DateCreated` datetime NOT NULL,
  `Status` varchar(1) NOT NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB; 

CREATE TABLE `qqquestionsetquestions` (
  `ID` int(10) NOT NULL auto_increment,
  `QuestionSetID` int(10) NOT NULL,
  `QuestionID` int(10) NOT NULL,
  `QuestionSequence` int(5) default NULL,
  PRIMARY KEY  (`ID`),
  KEY `QuestionSetID` (`QuestionSetID`),
  KEY `QuestionID` (`QuestionID`)
) ENGINE=InnoDB;
 
ALTER TABLE `qqquestionsetquestions`
  ADD CONSTRAINT `qqquestionsetquestions_ibfk_1` FOREIGN KEY (`QuestionSetID`) REFERENCES `qqquestionsets` (`ID`),
  ADD CONSTRAINT `qqquestionsetquestions_ibfk_2` FOREIGN KEY (`QuestionID`) REFERENCES `districtspecificquestions` (`questionId`);

ALTER TABLE `teacheranswerdetailsfordistrictspecificquestions` ADD COLUMN questionSetID int( 11 ) DEFAULT NULL after districtId;
ALTER TABLE `teacheranswerdetailsfordistrictspecificquestions` ADD FOREIGN KEY ( QuestionSetID ) REFERENCES `qqquestionsets` (`ID`);

ALTER TABLE jobcategorymaster ADD COLUMN questionSetID int( 11 ) DEFAULT NULL ;
ALTER TABLE `jobcategorymaster` ADD FOREIGN KEY ( questionSetID ) REFERENCES `qqquestionsets` (`ID`);



ALTER TABLE `districtmaster` ADD qqsetid int(10)  DEFAULT NULL AFTER sendNotificationOnNegativeQQ;
ALTER TABLE `districtmaster` ADD FOREIGN KEY ( qqsetid ) REFERENCES qqquestionsets( ID );


--------------------- AShish 06/05/2014 -----------------------------------------
ALTER TABLE  `jobcategorymaster` ADD  `parentJobCategoryId` INT( 11 ) NULL AFTER  `districtId` ;

--------------------- mukesh 07/05/2014 -----------------------------------------
 ALTER TABLE `districtspecificdocuments` DROP `entityType` ;
 ALTER TABLE `districtspecificdocuments` ADD `ipAddress` VARCHAR( 50 ) NULL DEFAULT NULL AFTER `status` ;
 
 
 ----------------------------------Shadab Ansari (07-05-2015)-------------------------------------

alter table jobforteacher add column `jobCompleteDate` datetime;

-------------------------- Ram Nath 08-05-2015 ----------------------------
alter table districtmaster add `qqThumbShowOrNot` tinyint(1) default '0';

--------------------------------------  Hanzala 08-05-2015 -----------------------------------------------------
ALTER TABLE districtmaster ADD statusIdForeReferenceFinalize INT( 2 ) NULL AFTER  sendReferenceOnJobComplete,
ADD secondaryStatusIdForeReferenceFinalize INT( 11 ) NULL AFTER statusIdForeReferenceFinalize;

ALTER TABLE  `districtmaster` ADD  `showReferenceToSA` INT( 11 ) NULL AFTER  `secondaryStatusIdForeReferenceFinalize`;

----------------------------------- Ravindra 08-05-2015 -----------------------------------------------------

   CREATE TABLE `portfolioreminders` (
  `portfolioremindersId` int(11) NOT NULL auto_increment,
  `districtId` int(11) default NULL COMMENT 'Foreign Key to "districtId" in table "districtmaster"',
  `salutation` int(2) default NULL,
  `firstName` int(2) default NULL,
  `lastName` int(2) default NULL,
  `ethnicOriginId` int(2) default NULL,
  `ethnicityId` int(2) default NULL,
  `raceId` int(2) default NULL,
  `genderId` int(2) default NULL,
  `addressLine1` int(2) default NULL,
  `addressLine2` int(2) default NULL,
  `countryId` int(2) default NULL,
  `zipCode` int(2) default NULL,
  `stateId` int(2) default NULL,
  `cityId` int(2) default NULL,
  `phoneNumber` int(2) default NULL,
  `mobileNumber` int(2) default NULL,
  `SSN` int(2) default NULL,
  `dob` int(2) default NULL,
  `retirementnumber` int(2) default NULL,
  `retirementdate` int(2) default NULL,
  `veteran` int(2) default NULL,
  `academics` int(2) default NULL,
  `resume` int(2) default NULL,
  `honors` int(2) default NULL,
  `videoLink` int(2) default NULL,
  `highNeedSchool` int(2) default NULL,
  `coverLetter` int(2) default NULL,
  `academicTranscript` int(2) default NULL,
  `certification` int(2) default NULL,
  `proofOfCertification` int(2) default NULL,
  `referenceLettersOfRecommendation` int(2) default NULL,
  `tfaAffiliate` int(2) default NULL,
  `willingAsSubstituteTeacher` int(2) default NULL,
  `candidateType` int(2) default NULL,
  `expCertTeacherTraining` int(2) default NULL,
  `nationalBoardCert` int(2) default NULL,
  `affidavit` int(2) default NULL,
  `employment` int(2) default NULL,
  `formeremployee` int(2) default NULL,
  `generalKnowledgeExam` int(2) default NULL,
  `subjectAreaExam` int(2) default NULL,
  `involvement` int(2) default NULL,
  `createdDateTime` datetime default NULL,
  PRIMARY KEY  (`portfolioremindersId`),
  KEY `districtId` (`districtId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

CREATE TABLE `portfolioremindershistory` (
  `portfolioremindersId` int(11) NOT NULL auto_increment,
  `districtId` int(11) default NULL COMMENT 'Foreign Key to "districtId" in table "districtmaster"',
  `salutation` int(2) default NULL,
  `firstName` int(2) default NULL,
  `lastName` int(2) default NULL,
  `ethnicOriginId` int(2) default NULL,
  `ethnicityId` int(2) default NULL,
  `raceId` int(2) default NULL,
  `genderId` int(2) default NULL,
  `addressLine1` int(2) default NULL,
  `addressLine2` int(2) default NULL,
  `countryId` int(2) default NULL,
  `zipCode` int(2) default NULL,
  `stateId` int(2) default NULL,
  `cityId` int(2) default NULL,
  `phoneNumber` int(2) default NULL,
  `mobileNumber` int(2) default NULL,
  `SSN` int(2) default NULL,
  `dob` int(2) default NULL,
  `retirementnumber` int(2) default NULL,
  `retirementdate` int(2) default NULL,
  `veteran` int(2) default NULL,
  `academics` int(2) default NULL,
  `resume` int(2) default NULL,
  `honors` int(2) default NULL,
  `videoLink` int(2) default NULL,
  `highNeedSchool` int(2) default NULL,
  `coverLetter` int(2) default NULL,
  `academicTranscript` int(2) default NULL,
  `certification` int(2) default NULL,
  `proofOfCertification` int(2) default NULL,
  `referenceLettersOfRecommendation` int(2) default NULL,
  `tfaAffiliate` int(2) default NULL,
  `willingAsSubstituteTeacher` int(2) default NULL,
  `candidateType` int(2) default NULL,
  `expCertTeacherTraining` int(2) default NULL,
  `nationalBoardCert` int(2) default NULL,
  `affidavit` int(2) default NULL,
  `employment` int(2) default NULL,
  `formeremployee` int(2) default NULL,
  `generalKnowledgeExam` int(2) default NULL,
  `subjectAreaExam` int(2) default NULL,
  `involvement` int(2) default NULL,
  `createdDateTime` datetime default NULL,
  PRIMARY KEY  (`portfolioremindersId`),
  KEY `districtId` (`districtId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

  ALTER TABLE `portfolioreminders`
  ADD CONSTRAINT `portfolioreminders_ibfk_1` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`);
  
  ALTER TABLE `portfolioremindershistory`
  ADD CONSTRAINT `portfolioremindershistory_ibfk_1` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`);


 ----*********************************************************************************************************
 ----**********************  Query is updated on Cloud, Demo and Platform 11-05-2015 *************************
 ----*********************************************************************************************************

------------------- Hanzala 12-05-2015 ----------------------
CREATE TABLE `portfolioremindersdspq` (
  `portfolioremindersdspqId` int(11) NOT NULL auto_increment,
  `questionId` int(11) NOT NULL COMMENT 'Foreign Key to "questionId" in table "districtspecificportfolioquestions"',
  `districtId` int(11) NOT NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`portfolioremindersdspqId`),
  KEY `questionId` (`questionId`),
  KEY `districtId` (`districtId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER TABLE `portfolioremindersdspq`
  ADD CONSTRAINT `portfolioremindersdspq_ibfk_2` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`),
  ADD CONSTRAINT `portfolioremindersdspq_ibfk_1` FOREIGN KEY (`questionId`) REFERENCES `districtspecificportfolioquestions` (`questionId`);

---------------------- Hanzala 13-05-2015 --------------------------------------
ALTER TABLE  `portfolioreminders` ADD  `certificationPortfolio` INT( 2 ) NULL AFTER  `highNeedSchool` ,
ADD  `employmentHistory` INT( 2 ) NULL AFTER  `certificationPortfolio` ,
ADD  `additionalDocuments` INT( 2 ) NULL AFTER  `employmentHistory` ,
ADD  `references` INT( 2 ) NULL AFTER  `additionalDocuments` ,
ADD  `tfaAffiliatePortfolio` INT( 2 ) NULL AFTER  `references` ,
ADD  `substituteTeacher` INT( 2 ) NULL AFTER  `tfaAffiliatePortfolio` ;


ALTER TABLE  `portfolioremindershistory` ADD  `certificationPortfolio` INT( 2 ) NULL AFTER  `highNeedSchool` ,
ADD  `employmentHistory` INT( 2 ) NULL AFTER  `certificationPortfolio` ,
ADD  `additionalDocuments` INT( 2 ) NULL AFTER  `employmentHistory` ,
ADD  `references` INT( 2 ) NULL AFTER  `additionalDocuments` ,
ADD  `tfaAffiliatePortfolio` INT( 2 ) NULL AFTER  `references` ,
ADD  `substituteTeacher` INT( 2 ) NULL AFTER  `tfaAffiliatePortfolio` ;

ALTER TABLE  `portfolioreminders` CHANGE  `references`  `referencesPortfolio` INT( 2 ) NULL DEFAULT NULL;
ALTER TABLE  `portfolioremindershistory` CHANGE  `references`  `referencesPortfolio` INT( 2 ) NULL DEFAULT NULL;
 ----*********************************************************************************************************
 ----**********************  Query is updated on Canada Server 14-05-2015 *************************
 ----*********************************************************************************************************
----------------------- Hanzala 14-05-2015 -------------------------------
ALTER TABLE  `portfolioreminders` ADD  `academicDspq` INT( 2 ) NULL AFTER  `coverLetter` ;
ALTER TABLE  `portfolioremindershistory` ADD  `academicDspq` INT( 2 ) NULL AFTER  `coverLetter` ;
ALTER TABLE  `portfolioreminders` ADD  `referenceDspq` INT( 2 ) NULL AFTER  `proofOfCertification` ;
ALTER TABLE  `portfolioremindershistory` ADD  `referenceDspq` INT( 2 ) NULL AFTER  `proofOfCertification` ;


-----------------gourav 16-5-2015-----------------------------------
create table `octdetails` (
    `octId` int (11),
    `teacherId` int (11),
    `octOption` tinyint (1),
    `OctNumber` varchar (300),
    `octFileName` varchar (750),
    `octText` text ,
    `createdDate` datetime 
); 

ALTER TABLE `octdetails` ADD FOREIGN KEY ( teacherId ) REFERENCES teacherdetail( teacherId );

-----------------gourav 19-5-2015-----------------------------------
ALTER TABLE `joborder`  ADD `jobInternalNotes` MEDIUMTEXT NULL AFTER `jobQualification`;


-----------------Vikas 20-5-2015-----------------------------------
ALTER TABLE districtrequisitionnumbers ADD postingNo VARCHAR( 45 ) NULL AFTER requisitionNumber;

-------------------------- Deepak 20 may 2015 -------------------------
ALTER TABLE  `schoolmaster` CHANGE  `zip`  `zip` VARCHAR( 20 ) NOT NULL;

--------------------Gourav 20-5-2015 --------------------------------
ALTER TABLE `joborder` ADD `notificationToschool` TINYINT(1) NULL DEFAULT '0' AFTER `apiJobId`;
--------------------Gourav 21-5-2015 --------------------------------
CREATE TABLE `jobwiseteacherdetails` (
  `jobWiseTeacherDetailsId` int(11) NOT NULL auto_increment,
  `teacherId` int(11) NOT NULL,
  `jobId` int(11) NOT NULL,
  `districtId` int(11) NOT NULL,
  `userUpdatedBy` int(11) default NULL,
  `seniorityNumber` varchar(50) NOT NULL,
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`jobWiseTeacherDetailsId`)
) ENGINE=InnoDB;
--------------------Amit Chaudhary 21-5-2015 --------------------------------
ALTER TABLE `assessmentdomainscore` ADD `createdDateTime` DATETIME NULL COMMENT 'Current Date and Time ' AFTER `lookupId` ;
ALTER TABLE `assessmentcompetencyscore` ADD `createdDateTime` DATETIME NULL COMMENT 'Current Date and Time ' AFTER `lookupId` ;
------------------ Vishwanath 25-5-2015 --------------------
ALTER TABLE  `stageonestatus` ADD  `shortName` VARCHAR( 10 ) NULL AFTER  `name` ;
ALTER TABLE  `stagetwostatus` ADD  `shortName` VARCHAR( 10 ) NULL AFTER  `name` ;
ALTER TABLE  `stagethreestatus` ADD  `shortName` VARCHAR( 10 ) NULL AFTER  `name`;

UPDATE  `stagethreestatus` SET  `shortName` =  'None' WHERE  `stagethreestatus`.`stage3Id` =1 LIMIT 1 ;
UPDATE  `stagethreestatus` SET  `shortName` =  'Active' WHERE  `stagethreestatus`.`stage3Id` =2 LIMIT 1 ;
UPDATE  `stagethreestatus` SET  `shortName` =  'Rtrd' WHERE  `stagethreestatus`.`stage3Id` =3 LIMIT 1 ;

UPDATE  `stagetwostatus` SET  `shortName` =  'None' WHERE  `stagetwostatus`.`stage2Id` =1 LIMIT 1 ;
UPDATE  `stagetwostatus` SET  `shortName` =  'Exptl' WHERE  `stagetwostatus`.`stage2Id` =2 LIMIT 1 ;
UPDATE  `stagetwostatus` SET  `shortName` =  'AP' WHERE  `stagetwostatus`.`stage2Id` =3 LIMIT 1 ;
UPDATE  `stagetwostatus` SET  `shortName` =  'Rjctd' WHERE  `stagetwostatus`.`stage2Id` =4 LIMIT 1 ;
UPDATE  `stagetwostatus` SET  `shortName` =  'Revise' WHERE  `stagetwostatus`.`stage2Id` =5 LIMIT 1 ;
UPDATE  `stagetwostatus` SET  `shortName` =  'Valdtd' WHERE  `stagetwostatus`.`stage2Id` =6 LIMIT 1 ;
UPDATE  `stagetwostatus` SET  `shortName` =  'Replcd' WHERE  `stagetwostatus`.`stage2Id` =7 LIMIT 1 ;

UPDATE  `stageonestatus` SET  `shortName` =  'None' WHERE  `stageonestatus`.`stage1Id` =1 LIMIT 1 ;
UPDATE  `stageonestatus` SET  `shortName` =  'New' WHERE  `stageonestatus`.`stage1Id` =2 LIMIT 1 ;
UPDATE  `stageonestatus` SET  `shortName` =  'UD' WHERE  `stageonestatus`.`stage1Id` =3 LIMIT 1 ;
UPDATE  `stageonestatus` SET  `shortName` =  'IR' WHERE  `stageonestatus`.`stage1Id` =4 LIMIT 1 ;
UPDATE  `stageonestatus` SET  `shortName` =  'SR' WHERE  `stageonestatus`.`stage1Id` =5 LIMIT 1 ;
UPDATE  `stageonestatus` SET  `shortName` =  'CE' WHERE  `stageonestatus`.`stage1Id` =6 LIMIT 1 ;
UPDATE  `stageonestatus` SET  `shortName` =  'AR' WHERE  `stageonestatus`.`stage1Id` =7 LIMIT 1 ;
UPDATE  `stageonestatus` SET  `shortName` =  'Rjct' WHERE  `stageonestatus`.`stage1Id` =8 LIMIT 1 ;
UPDATE  `stageonestatus` SET  `shortName` =  'Rdy' WHERE  `stageonestatus`.`stage1Id` =9 LIMIT 1 ;

ALTER TABLE  `stageonestatus` CHANGE  `shortName`  `shortName` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE  `stagetwostatus` CHANGE  `shortName`  `shortName` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;
ALTER TABLE  `stagethreestatus` CHANGE  `shortName`  `shortName` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

------------------ Mukesh 26-5-2015 --------------------
ALTER TABLE `joborder` ADD `hiddenJob` TINYINT( 1 ) NULL DEFAULT NULL COMMENT '0-"Invite job ",1-"hide job"' AFTER `isInviteOnly` ;

--------------------------------- vishwanath 27-5-2015 --------------------------------------
ALTER TABLE  `questionuploadtemp` CHANGE  `domain_id`  `domain_id` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE  `questionuploadtemp` CHANGE  `competency_id`  `competency_id` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `objective_id`  `objective_id` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `stage1`  `stage1` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `stage2`  `stage2` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `stage3`  `stage3` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE  `questionuploadtemp` CHANGE  `domain_name`  `domain_name` VARCHAR( 222 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
CHANGE  `competency_name`  `competency_name` VARCHAR( 222 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `objective_name`  `objective_name` VARCHAR( 222 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `stage1_name`  `stage1_name` VARCHAR( 222 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `stage2_name`  `stage2_name` VARCHAR( 222 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `stage3_name`  `stage3_name` VARCHAR( 222 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE  `questionuploadtemp` CHANGE  `question`  `question` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `question_weightage`  `question_weightage` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `max_marks`  `max_marks` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `instructions`  `instructions` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `question_type`  `question_type` VARCHAR( 50 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `question_type_name`  `question_type_name` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `assessmentId`  `assessmentId` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `sectionId`  `sectionId` VARCHAR( 10 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;

ALTER TABLE `questionuploadtemp` CHANGE `option1` `option1` VARCHAR(750) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `score1` `score1` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `rank1` `rank1` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `option2` `option2` VARCHAR(750) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `score2` `score2` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `rank2` `rank2` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `option3` `option3` VARCHAR(750) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `score3` `score3` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `rank3` `rank3` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE `questionuploadtemp` CHANGE `option4` `option4` VARCHAR(750) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `score4` `score4` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `rank4` `rank4` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `option5` `option5` VARCHAR(750) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `score5` `score5` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `rank5` `rank5` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `option6` `option6` VARCHAR(750) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `score6` `score6` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL, CHANGE `rank6` `rank6` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
ALTER TABLE  `questionuploadtemp` CHANGE  `itemCode`  `itemCode` VARCHAR( 45 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `errortext`  `errortext` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL ,
CHANGE  `sessionid`  `sessionid` VARCHAR( 200 ) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL;
------------------------------------------------------------------------------------------------------

-------------------------------------------------Ram Nath 27-05-2015 ----------------------------------------
alter table districtspecificrefchkanswers ADD `answeredBy` int(10) default NULL COMMENT '(if enter answer by district) Foreign Key to " in userId" table "userMaster"';
-------------------------------------------------------------------------------------------------------------
----------------------- Gourav 27-5-2015 -------------------------------------
alter table districtmaster add column displaySeniorityNumber boolean default false; 
--------------------Amit Chaudhary 29-5-2015 --------------------------------
ALTER TABLE `scorelookupmaster` ADD `shortName` VARCHAR( 10 ) NULL COMMENT '"EPI" , "IPI" , "SP"' AFTER `description`;
ALTER TABLE `scorelookupmaster` ADD `status` VARCHAR( 1 ) NULL COMMENT '"A" - Active, "I" - Inactive' AFTER `shortName`;

ALTER TABLE `teacherelectronicreference` ADD `canContOnOffer` TINYINT( 1 ) NULL DEFAULT NULL AFTER `rdcontacted` ;

----------- Gourav 29-5-2015 -----------------------------------------------
ALTER TABLE `teacherpersonalinfo` ADD `presentAddressLine1` VARCHAR(50) NULL AFTER `otherState`, ADD `presentAddressLine2` VARCHAR(50) NULL AFTER `presentAddressLine1`, ADD `presentCountryId` INT(11) NULL AFTER `presentAddressLine2`, ADD `presentZipCode` VARCHAR(10) NULL AFTER `presentCountryId`, ADD `presentStateId` INT(3) NULL AFTER `presentZipCode`, ADD `presentCityId` INT(5) NULL AFTER `presentStateId`, ADD `persentOtherCity` VARCHAR(100) NULL AFTER `presentCityId`, ADD `persentOtherState` VARCHAR(100) NULL AFTER `persentOtherCity`, ADD `drivingLicState` VARCHAR(50) NULL AFTER `persentOtherCity`,ADD `drivingLicNum` VARCHAR(50) NULL AFTER `drivingLicState`;


-------------------------------------------------Ram Nath 29-05-2015 ----------------------------------------
CREATE TABLE `employmentservicestechnician` (
  `estechnicianId` int(10) NOT NULL auto_increment,
  `employeeId` int(10) NOT NULL,
  `primaryESTech` text NOT NULL COMMENT 'Primary Employement Service Technician',
  `eSBackupId` int(10) UNIQUE NOT NULL,
  `backupESTech` text NOT NULL COMMENT 'Backup Employement Service Technician',
   PRIMARY KEY  (`estechnicianId`)
);

-------------------------------------------------Ram Nath 29-05-2015 ----------------------------------------
ALTER TABLE  `joborder` ADD  `positionStartDate` DATE NULL DEFAULT NULL AFTER  `jobEndDate` ,
ADD  `positionEndDate` DATE NULL DEFAULT NULL AFTER  `positionStartDate` ,
ADD  `tempType` TINYINT NULL DEFAULT NULL AFTER  `positionEndDate` ,
ADD  `jobApplicationStatus` TINYINT NULL DEFAULT NULL AFTER  `tempType` ;

-------------------------------------------------Ram Nath 29-05-2015 ----------------------------------------
alter table joborder add salaryRange text default NULL ;
alter table joborder add additionalDocumentPath text default NULL ;
alter table joborder add estechnicianId int(10) default NULL ;
ALTER TABLE `joborder` ADD FOREIGN KEY ( estechnicianId ) REFERENCES employmentservicestechnician( estechnicianId );
INSERT INTO `employmentservicestechnician` (`estechnicianId`, `primaryESTech`, `eSBackupId`, `backupESTech`, `employeeId`) VALUES
(1, 'Stephanie', 134764, 'Diane Narvaez', 114071),
(2, 'Sonia Schwartz', 112309, 'Kimberley Bradley', 137502),
(3, 'Diane Narvaez', 114071, 'Stephanie Guenther', 134764),
(4, 'Diane Stepp', 125133, 'Glenda Gomez', 136354),
(5, 'Glenda Gomez', 136354, 'Diane Stepp', 125233),
(6, 'Kimberley Bradley', 137502, 'Sonia Schwartz', 112309),
(7, 'Dorothy Lewis', 145785, 'Courtney Henderson', 125922);
-------------------------------------------------------------------------------------------------------------
---------------------------------Sekhar 29-05-2015-------------------------------
ALTER TABLE districtportfolioconfig ADD FOREIGN KEY (jobCategoryId) REFERENCES jobcategorymaster (jobCategoryId);

---------------------------------Ravindra 30-05-2015----------------------------------
ALTER TABLE  `jobforteacher` ADD  `districtSpecificNoteFinalizeBy` INT( 10 ) NULL AFTER  `districtSpecificNoteFinalizeDate` ;

--------------------- Gourav 4-6-2015 -----------------------------
ALTER TABLE `teacherinvolvement` CHANGE `organization` `organization` VARCHAR(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, 
CHANGE `orgTypeId` `orgTypeId` INT(2) NULL COMMENT 'Foreign Key to "orgTypeId" in table "orgTypeMaster" ', 
CHANGE `totalNoOfPeople` `totalNoOfPeople` INT(3) NULL, CHANGE `leadNoOfPeople` `leadNoOfPeople` INT(4) NULL DEFAULT NULL;

ALTER TABLE `teacherrole` CHANGE `fieldId` `fieldId` INT(2) NULL COMMENT 'Foreign Key to "fieldId" in table "fieldMaster" '

-----------------------------------Sandeep singh 05-06-2015-----------------------------------------------
ALTER TABLE `joborder` ADD `noofhires` int(5) default null;
Update joborder SET `joborder`.`noofhires`= `joborder`.`noOfExpHires` Where noofhires is null;
----------------------------------------------------------------------------------------------------------

---------------------------------Ravindra 06-06-2015----------------------------------
ALTER TABLE  `districtmaster` ADD  `qqSetIdForOnboard` INT( 10 ) NULL DEFAULT NULL AFTER  `qqsetid` ;
ALTER TABLE  `jobcategorymaster` ADD  `QuestionSetIDForOnboard` INT( 11 ) NULL DEFAULT NULL AFTER  `questionSetID` ;
--------------------------------------------------------------------------------------

--------------------Amit Chaudhary 08-06-2015 --------------------------------

CREATE TABLE `inventoryinvitationstemp` (
`inventoryInvitationsTempId` INT( 10 ) NOT NULL AUTO_INCREMENT ,
`candidateFirstName` VARCHAR( 100 ) NULL DEFAULT NULL ,
`candidateLastName` VARCHAR( 100 ) NULL DEFAULT NULL ,
`candidateEmail` VARCHAR( 100 ) NULL DEFAULT NULL ,
`sessionId` VARCHAR( 200 ) NULL DEFAULT NULL ,
`errorText` VARCHAR( 400 ) NULL DEFAULT NULL ,
`existTeacherId` INT( 10 ) NULL DEFAULT '0',
`date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
PRIMARY KEY ( `inventoryInvitationsTempId` )
) ENGINE = InnoDB;

ALTER TABLE `inventoryinvitationstemp` ADD `orgType` VARCHAR( 1 ) NOT NULL AFTER `existTeacherId` , ADD `inventoryType` VARCHAR( 1 ) NOT NULL AFTER `orgType` , ADD `groupName` VARCHAR( 20 ) NOT NULL AFTER `inventoryType` , ADD `districtId` INT( 11 ) NOT NULL AFTER `groupName` ;

CREATE TABLE `inventoryinvitations` (
`inventoryInvitationsId` INT( 10 ) NOT NULL AUTO_INCREMENT ,
`orgType` VARCHAR( 1 ) NOT NULL COMMENT '"D" or "S"',
`inventoryType` VARCHAR( 1 ) NOT NULL COMMENT '"1" -  Base, "2" - Job Specific, "3" - SP, "4" - IPI',
`groupName` VARCHAR( 20 ) NOT NULL ,
`districtId` INT( 11 ) NOT NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster" ',
`candidateFirstName` VARCHAR( 50 ) NOT NULL ,
`candidateLastName` VARCHAR( 50 ) NOT NULL ,
`candidateEmail` VARCHAR( 75 ) NOT NULL ,
`invitationLink` VARCHAR( 200 ) NOT NULL ,
`invitationParams` VARCHAR( 200 ) NOT NULL ,
`createdBy` INT( 5 ) NOT NULL COMMENT 'Foreign Key to "userId" in table "userMaster" ',
`createdDateTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
`ipaddress` VARCHAR( 50 ) NULL ,
PRIMARY KEY ( `inventoryInvitationsId` )
) ENGINE = InnoDB;

ALTER TABLE `inventoryinvitations` ADD FOREIGN KEY ( `createdBy` ) REFERENCES `usermaster` ( `userId` ) ;
ALTER TABLE `inventoryinvitations` ADD FOREIGN KEY ( `districtId` ) REFERENCES `districtmaster` ( `districtId` ) ;

ALTER TABLE `inventoryinvitations` CHANGE `invitationLink` `invitationLink` LONGTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
ALTER TABLE `inventoryinvitations` CHANGE `invitationParams` `invitationParams` LONGTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL ;
--------------------------------------------------------------------------------------------
----------------- Updated Query on Platform 09.06.2015 -------------------------------------
--------------------------------------------------------------------------------------------

------------------ ----- Anurag Kumar 09/06/2015  (Add 'All Uk Region' in regionmaster table)-------- ---------

insert into regionmaster ( regionName,parentRegionId, status ) values ('All UK Regions',0,'A');

--------------- Please run Select with Insert Query---------------------------------Below
SELECT @regid := regionId FROM regionmaster where regionName ='All UK Regions';
insert into regionmaster ( regionName,parentRegionId, status ) values ('Essex and East Anglia',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('Herts, Beds and Bucks',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('London',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('Midlands',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('North East',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('North West',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('Scotland',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('South',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('South East',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('South West',@regid,'A');
insert into regionmaster ( regionName,parentRegionId, status ) values ('Surrey and Thames Valley',@regid,'A');

--------------------Sadaab Ahmad 11-06-2015 --------------------------------
ALTER TABLE  `districtmaster` ADD  `latitude` VARCHAR( 50 ) NULL AFTER  `textForReferenceInstruction` ,
ADD  `longitude` VARCHAR( 50 ) NULL AFTER  `latitude` ;

--------------------Amit Chaudhary 11-06-2015 --------------------------------
--1
ALTER TABLE `rawdatafordomain` ADD `assessmentId` INT( 5 ) NULL COMMENT 'Foreign Key to "assessmentId" in table "assessmentDetail" ' AFTER `maxMarks` ;
ALTER TABLE `rawdatafordomain` ADD `assessmentType` INT( 1 ) NULL COMMENT '"1" -  Base, "2" - Job Specific, "3" - SP, "4" - IPI ' AFTER `assessmentId` ;
ALTER TABLE `rawdatafordomain` ADD CONSTRAINT `rawdatafordomain_ibfk_1` FOREIGN KEY ( `assessmentId` ) REFERENCES `assessmentdetail` ( `assessmentId` ) ;
--2
ALTER TABLE `rawdataforcompetency` ADD `assessmentId` INT( 5 ) NULL COMMENT 'Foreign Key to "assessmentId" in table "assessmentDetail" ' AFTER `maxMarks` ;
ALTER TABLE `rawdataforcompetency` ADD `assessmentType` INT( 1 ) NULL COMMENT '"1" -  Base, "2" - Job Specific, "3" - SP, "4" - IPI ' AFTER `assessmentId` ;
ALTER TABLE `rawdataforcompetency` ADD CONSTRAINT `rawdataforcompetency_ibfk_1` FOREIGN KEY ( `assessmentId` ) REFERENCES `assessmentdetail` ( `assessmentId` ) ;
--3
ALTER TABLE `assessmentdomainscore` ADD `assessmentId` INT( 5 ) NULL COMMENT 'Foreign Key to "assessmentId" in table "assessmentDetail" ' AFTER `lookupId` ;
ALTER TABLE `assessmentdomainscore` ADD `assessmentType` INT( 1 ) NULL COMMENT '"1" -  Base, "2" - Job Specific, "3" - SP, "4" - IPI ' AFTER `assessmentId` ;
ALTER TABLE `assessmentdomainscore` ADD CONSTRAINT FOREIGN KEY ( `assessmentId` ) REFERENCES `assessmentdetail` ( `assessmentId` ) ;
--4
ALTER TABLE `assessmentcompetencyscore` ADD `assessmentId` INT( 5 ) NULL COMMENT 'Foreign Key to "assessmentId" in table "assessmentDetail" ' AFTER `lookupId` ;
ALTER TABLE `assessmentcompetencyscore` ADD `assessmentType` INT( 1 ) NULL COMMENT '"1" -  Base, "2" - Job Specific, "3" - SP, "4" - IPI ' AFTER `assessmentId` ;
ALTER TABLE `assessmentcompetencyscore` ADD CONSTRAINT FOREIGN KEY ( `assessmentId` ) REFERENCES `assessmentdetail` ( `assessmentId` ) ;
--5
ALTER TABLE `teacherassessmentdetails` ADD `assessmentTakenCount` INT( 3 ) NULL AFTER `isDone` ;
ALTER TABLE `teacherassessmentstatus` ADD `assessmentTakenCount` INT( 3 ) NULL AFTER `cgUpdated` ;
ALTER TABLE `teacherassessmentattempt` ADD `assessmentTakenCount` INT( 3 ) NULL AFTER `jobId` ;
--6
ALTER TABLE `rawdatafordomain` ADD `assessmentTakenCount` INT( 3 ) NULL AFTER `assessmentType` ;
ALTER TABLE `rawdataforcompetency` ADD `assessmentTakenCount` INT( 3 ) NULL AFTER `assessmentType` ;
ALTER TABLE `assessmentdomainscore` ADD `assessmentTakenCount` INT( 3 ) NULL AFTER `assessmentType` ;
ALTER TABLE `assessmentcompetencyscore` ADD `assessmentTakenCount` INT( 3 ) NULL AFTER `assessmentType` ;
--7
ALTER TABLE `teachernormscore` ADD `teacherAssessmentId` INT( 10 ) NULL COMMENT '''Foreign Key to "teacherAssessmentId" in table "teacherassessmentdetails" ''' AFTER `teacherId` ;
ALTER TABLE `teachernormscore` ADD FOREIGN KEY ( `teacherAssessmentId` ) REFERENCES `teacherassessmentdetails` ( `teacherAssessmentId` ) ;
--8
ALTER TABLE `rawdatafordomain` ADD `teacherAssessmentId` INT( 10 ) NULL COMMENT '''Foreign Key to "teacherAssessmentId" in table "teacherassessmentdetails" ''' AFTER `teacherId` ;
ALTER TABLE `rawdatafordomain` ADD FOREIGN KEY ( `teacherAssessmentId` ) REFERENCES `teacherassessmentdetails` ( `teacherAssessmentId` ) ;
--9
ALTER TABLE `rawdataforcompetency` ADD `teacherAssessmentId` INT( 10 ) NULL COMMENT '''Foreign Key to "teacherAssessmentId" in table "teacherassessmentdetails" ''' AFTER `teacherId` ;
ALTER TABLE `rawdataforcompetency` ADD FOREIGN KEY ( `teacherAssessmentId` ) REFERENCES `teacherassessmentdetails` ( `teacherAssessmentId` ) ;
--10
ALTER TABLE `assessmentdomainscore` ADD `rawDataId` INT( 10 ) NULL COMMENT '''Foreign Key to "rawDataId" in table "rawdatafordomain" ''' AFTER `teacherId` ;
ALTER TABLE `assessmentdomainscore` ADD FOREIGN KEY ( `rawDataId` ) REFERENCES `rawdatafordomain` ( `rawDataId` ) ;
--11
ALTER TABLE `assessmentcompetencyscore` ADD `rawDataId` INT( 10 ) NULL COMMENT '''Foreign Key to "rawDataId" in table "rawdataforcompetency" ''' AFTER `teacherId` ;
ALTER TABLE `assessmentcompetencyscore` ADD FOREIGN KEY ( `rawDataId` ) REFERENCES `rawdataforcompetency` ( `rawDataId` ) ;

----------------------------------------Mukesh 15-06-2015----------------------
ALTER TABLE `teacherassessmentstatus` ADD `pass` VARCHAR( 2 ) NULL DEFAULT NULL COMMENT '"P"  means pass and "F" means fail' AFTER `statusId` ;

--------------------------------------------------sandeep Singh(16-06-2015) --------------------------------------

ALTER TABLE jobcategorymaster ADD COLUMN minDaysJobWillDisplay int( 5 ) DEFAULT 8 ;

-----------------------------------Gourav 16-6-2015--------------------------------------------------------------------------
ALTER TABLE `joborder` ADD COLUMN `hoursPerDay` varchar(3) DEFAULT NULL

---------------------Sandeep singh 18-6-2015----------------------------------------------------------------
ALTER TABLE  `districtmaster` ADD  `sACreateDistJOb` tinyint( 1 ) NULL;

-----------------------------------Gourav 18-6-2015--------------------------------------------------------------------------
ALTER TABLE  `districtrequisitionnumbers` ADD  `schoolId` INT( 11 ) NULL AFTER  `requisitionNumber` ;
ALTER TABLE `districtrequisitionnumbers` ADD FOREIGN KEY ( `schoolId` ) REFERENCES `schoolmaster` ( `schoolId` ) ;

---------------------------------Shadab Ansari 18-06-2015-----------------------------------------------------
Alter table jobcategorymaster add column approvalBeforeGoLive tinyint(4) DEFAULT '0' COMMENT 'approval process for positions prior to them going live';
Alter table jobcategorymaster add column noOfApprovalNeeded int(2) DEFAULT '0' COMMENT 'No. of Approval needed';
Alter table jobcategorymaster add column buildApprovalGroup tinyint(1) DEFAULT '0';

---------------------------------Ankit saini 19-06-2015------------------------------------------------------------
ALTER TABLE `districtrequisitionnumbers` ADD `requisitionDescription` VARCHAR(250) NULL AFTER `uploadFrom`, 
ADD `continuingOrTemporaryCode` VARCHAR(1) NULL COMMENT 'C = Continuing, T = Temporary' AFTER `requisitionDescription`,
ADD `startDate` DATE NULL AFTER `continuingOrTemporaryCode`,
ADD `endDate` DATE NULL AFTER `startDate`,
ADD `postionTerm` FLOAT NULL AFTER `endDate`, 
ADD `payMethod` VARCHAR(1) NULL COMMENT 'S = Salaried, H = Hourly, F = Flat Dollar' AFTER `postionTerm`,
ADD `postionStatus` VARCHAR(2) NULL COMMENT '01 = Staffed  Fully, 02 = Staffed  Partially, 51 = Vacant, 60 = Frozen, 00 = Inactive Position, PV = Pending Vacant' AFTER `payMethod`, 
ADD `commDriverLicence` VARCHAR(1) NULL AFTER `postionStatus`,
ADD `positionPercentFunded` FLOAT NULL AFTER `commDriverLicence`, 
ADD `postionHoursWeekFunded` FLOAT NULL AFTER `positionPercentFunded`,
ADD `standardHoursWeek` FLOAT NULL AFTER `postionHoursWeekFunded`;

ALTER TABLE  `districtrequisitionnumbers` ADD  `requisitionTypeCode` VARCHAR( 1 ) NULL COMMENT  'L = Licensed, C = Classified,  B = Bus Driver, S = Substitute' AFTER  `requisitionDescription` ;

---------------------------------Ram Nath 19-06-2015------------------------------------------------------------
alter table jobcategorywisestatusprivilege add internalExternalOrBothCandidate int(1) default 1 comment "(Required Node on candidate: '0'- no required for both internal and external candidate, '1'- requied for both external and internal candidate, '2'-requied for internal candidate, '3'-requied for external candidate)";

---------------------------------- Ashish 20-06-2015 ----------------------------------------------
ALTER TABLE  `jobcategorywisestatusprivilege` ADD  `autoNotifyAll` TINYINT( 1 ) NULL DEFAULT NULL AFTER  `overrideUserSettings`;
UPDATE jobcategorywisestatusprivilege SET autoNotifyAll =0 WHERE autoNotifyAll IS NULL;

--------------------------------------------------------------------------------------------
----------------- Updated Query on Platform 22.06.2015 -------------------------------------
--------------------------------------------------------------------------------------------


--------------------------------Ankit saini 22-06-2015------------------------------------------------------------
ALTER TABLE  `districtrequisitionnumbers` ADD  `updateTimestamp` TIMESTAMP NULL AFTER  `standardHoursWeek` ;

-------------------------------------sandeep Singh(23-06-2015)-------------------------------------------------------

CREATE TABLE IF NOT EXISTS `jobtitlewithdescription` (
  `jobTitleWithDescritionId` INT(11) NOT NULL AUTO_INCREMENT,
  `districtId` INT(5) NOT NULL COMMENT 'Foreign Key to "districtId" in table "districtMaster"',
  `jobCode` VARCHAR(10) COLLATE utf8_unicode_ci NULL,
  `jobTitle` VARCHAR(200) COLLATE utf8_unicode_ci NOT NULL,
  `jobDescription` MEDIUMTEXT COLLATE utf8_unicode_ci,
  `status` VARCHAR(1) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'A' COMMENT '"I" - Inactive, "A" - Active By default "A"',
  `createdDateTime` DATETIME NOT NULL COMMENT 'Current Date Time',
  PRIMARY KEY (`jobTitleWithDescritionId`),
  UNIQUE KEY `jobTitleWithDescritionId` (`jobTitleWithDescritionId`),
  KEY `districtId` (`districtId`)
)

----------------------------------------------------------------------------------------------------------------------------------------

--------------------------------Ram nath 23-06-2015------------------------------------------------------------
UPDATE  `menumaster` SET  `districtId` =  '|Hide#804800|Hide#7800055|' WHERE  `menumaster`.`menuId` =22 and `menumaster`.menuName='School Job Orders' LIMIT 1 ;

-----------------------Sekhar 28-06-2015----------------------------
ALTER TABLE  `jobforteacher` ADD  `districtId` INT( 10 ) NOT NULL AFTER  `jobForTeacherId` ;
UPDATE  `jobforteacher` jt,joborder j SET  jt.`districtId` =j.districtId   WHERE j.jobId=jt.jobId;
ALTER TABLE `jobforteacher` ADD FOREIGN KEY ( `districtId` ) REFERENCES `districtmaster` ( `districtId` ) ;

--------------------Amit Chaudhary 29-06-2015 --------------------------------
ALTER TABLE `teacherassessmentdetails` CHANGE `ipaddress` `ipaddress` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `teacherassessmentattempt` CHANGE `ipAddress` `ipAddress` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;
ALTER TABLE `teachersectiondetails` CHANGE `ipAddress` `ipAddress` VARCHAR(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

-------------------- Ashish 02-062015 --------------------------------------------
ALTER TABLE  `jobcategorymaster` ADD  `attachDSPQFromJC` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `questionSetID` ,
ADD  `attachQQFromJC` TINYINT( 1 ) NOT NULL DEFAULT  '0' AFTER  `attachDSPQFromJC` ;
ALTER TABLE  `jobcategorymaster` CHANGE  `attachQQFromJC`  `attachSLCFromJC` TINYINT( 1 ) NOT NULL DEFAULT  '0';



-------------------- Vikas Kumar 02-07-2015 --------------------------------------------
ALTER TABLE `joborder` ADD COLUMN `grantFlag` varchar(1) DEFAULT NULL AFTER `hoursPerDay`;
ALTER TABLE `joborder` ADD COLUMN `specialEdFlag` varchar(1) DEFAULT NULL AFTER `grantFlag`;

-------------------- Ramesh 02-07-2015 --------------------------------------------
ALTER TABLE  `teacherrole` ADD  `noWorkExp` INT( 1 ) NULL AFTER  `reasonForLeaving` ;
ALTER TABLE  `teacherrole` CHANGE  `noWorkExp`  `noWorkExp` TINYINT( 1 ) NULL DEFAULT NULL;
ALTER TABLE  `teacherrole` CHANGE  `role`  `role` VARCHAR( 200 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE  `teacherrole` CHANGE  `position`  `position` TINYINT( 1 ) NULL;
ALTER TABLE  `teacherrole` CHANGE  `roleStartMonth`  `roleStartMonth` INT( 2 ) NULL COMMENT  '"1" - January, "2" - February, etc. ';
ALTER TABLE  `teacherrole` CHANGE  `roleStartYear`  `roleStartYear` VARCHAR( 4 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
ALTER TABLE  `teacherrole` CHANGE  `currentlyWorking`  `currentlyWorking` TINYINT( 4 ) NULL;

--------------------------Shadab Ansari 03-07-2015 ---------------------------------------
alter table districtmaster add approvalByPredefinedGroups tinyint(1) default NULL;
alter table jobcategorymaster add approvalByPredefinedGroups tinyint(1) default NULL;

CREATE TABLE `districtspecificapprovalflow` (
  `districtSpecificApprovalFlowId` int(10) NOT NULL AUTO_INCREMENT,
  `districtId` int(11) NOT NULL COMMENT 'Foreign Key to districtId in table districtmaster',
  `grantFlag` varchar(1) NOT NULL COMMENT 'Y/N (Y= Yes, N= No)',
  `specialEdFlag` varchar(1) NOT NULL COMMENT 'Y/N (Y= Yes, N= No)',
  `approvalFlow` varchar(50) NOT NULL COMMENT 'districtApprovalGroupsId seperated by # from DistrictApprovalGroups',
  `createdDateTime` datetime NOT NULL,
  `status` varchar(5) DEFAULT 'A',
  PRIMARY KEY (`districtSpecificApprovalFlowId`),
  KEY `districtId` (`districtId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

ALTER TABLE `districtspecificapprovalflow`
  ADD CONSTRAINT `districtspecificapprovalflow_ibfk_1` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`);

drop table jeffcoSpecificApprovalFlow;
INSERT INTO `districtspecificapprovalflow` (`districtSpecificApprovalFlowId`, `districtId`, `grantFlag`, `specialEdFlag`, `approvalFlow`, `createdDateTime`, `status`) VALUES
(1, 804800, 'N', 'N', '#SA#ES#', '2015-07-03 16:47:50', 'A'),
(2, 804800, 'N', 'Y', '#SPED#SA#ES#', '2015-07-03 16:49:02', 'A'),
(3, 804800, 'Y', 'N', '#GRNT#SA#ES#', '2015-07-03 16:49:47', 'A'),
(4, 804800, 'Y', 'Y', '#GRNT#SPED#SA#ES#', '2015-07-03 16:50:39', 'A');
--------------------------Ram Nath 03-07-2015 ---------------------------------------
 CREATE TABLE `districtwiseapprovalgroup` (
  `approvalId` int(11) NOT NULL auto_increment,
  `districtId` int(11) NOT NULL,
  `groupName` varchar(255) ,
  `groupShortName` varchar(255) NOT NULL ,
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `status` varchar(1) NOT NULL,
   PRIMARY KEY  (`approvalId`),
   KEY `districtId` (`districtId`),
   CONSTRAINT `districtId_ibfk_1`  FOREIGN KEY ( districtId ) REFERENCES districtmaster( districtId )
);
INSERT INTO `districtwiseapprovalgroup` (`approvalId`, `districtId`, `groupName`, `groupShortName`, `createdDateTime`, `status`) VALUES
(1, 804800, 'All SAs', 'SA', '2015-07-03 00:00:00', 'A'),
(2, 804800, 'ES Tech and backup ES Tech', 'ES', '2015-07-03 00:00:00', 'A'),
(3, 804800, 'Grant Group', 'GRNT', '2015-07-03 00:00:00', 'A'),
(4, 804800, 'Special Ed Approval Group', 'SPED', '2015-07-03 00:00:00', 'A');

----------------------- Hanzala 03-07-2015 ------------------------------
ALTER TABLE  `teachercertificate` ADD  `certiExpiration` VARCHAR( 100 ) NULL AFTER  `ieinNumber` ;

-------------------------------Sekhar 04-07-2015------------------------------
CREATE TABLE `tempusermaster` (
  `tempuserId` int(5) NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(200) NOT NULL,
  `entityType` int(1) NOT NULL,
  `districtId` int(11) DEFAULT NULL,
  `location` varchar(25) DEFAULT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `emailAddress` varchar(150) NOT NULL,
  `role` varchar(50) NOT NULL,
  `errortext` varchar(200) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tempuserId`)
) ENGINE=InnoDB;

-------------------------------Shadab Ansari 04-07-2015 -------------------------
alter table employmentservicestechnician add column emailAddress varchar(50);
ALTER TABLE  `employmentservicestechnician` CHANGE  `emailAddress`  `primaryEmailAddress` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;
ALTER TABLE  `employmentservicestechnician` Add column `backupEmailAddress` VARCHAR( 50 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL;

update employmentservicestechnician set primaryEmailAddress='sguenthe@jeffco.k12.co.us',backupEmailAddress='enarvaez@jeffco.k12.co.us' where primaryESTech='Stephanie Guenther' and employeeId='114071'; 
update employmentservicestechnician set primaryEmailAddress='sschwart@jeffco.k12.co.us',backupEmailAddress='kbradley@jeffco.k12.co.us' where primaryESTech='Sonia Schwartz' and employeeId='137502';
update employmentservicestechnician set primaryEmailAddress='enarvaez@jeffco.k12.co.us',backupEmailAddress='sguenthe@jeffco.k12.co.us' where primaryESTech='Diane Narvaez' and employeeId='134764';
update employmentservicestechnician set primaryEmailAddress='jstepp@jeffco.k12.co.us',backupEmailAddress='ggomez@jeffco.k12.co.us' where primaryESTech='Diane Stepp' and employeeId='136354';
update employmentservicestechnician set primaryEmailAddress='ggomez@jeffco.k12.co.us',backupEmailAddress='jstepp@jeffco.k12.co.us' where primaryESTech='Glenda Gomez' and employeeId='125233';
update employmentservicestechnician set primaryEmailAddress='kbradley@jeffco.k12.co.us',backupEmailAddress='sschwart@jeffco.k12.co.us' where primaryESTech='Kimberley Bradley' and employeeId='112309';
update employmentservicestechnician set primaryEmailAddress='dglewis@jeffco.k12.co.us',backupEmailAddress='chenders@jeffco.k12.co.us' where primaryESTech='Dorothy Lewis' and employeeId='125922';

update employmentservicestechnician set primaryESTech='Stephanie Guenther',primaryEmailAddress='sguenthe@jeffco.k12.co.us',backupEmailAddress='enarvaez@jeffco.k12.co.us' where employeeId='114071';


--------------------------------------------------------------------------------------------
----------------- Updated Query on Platform 06.07.2015 -------------------------------------
--------------------------------------------------------------------------------------------

---------------------------Sekhar 6-7-2015----------------------------
INSERT INTO  `menumaster` (
`menuId` ,
`menuName` ,
`parentMenuId` ,
`subMenuId` ,
`districtId` ,
`menuArrow` ,
`pageName` ,
`pageNameWithSubPage` ,
`orderBy` ,
`status`
)
VALUES (
NULL ,  'Groups',  '2', NULL , NULL , NULL ,  'jobapprovalgroups.do',  '|jobapprovalgroups.do|',  '22',  'A'
);

INSERT INTO  `menusectionmaster` (
`menuSectionId` ,
`menuId` ,
`menuSectionName`
)
VALUES (
NULL ,  '102',  'Groups'
);
INSERT INTO `roleaccesspermission` (`roleId`, `menuId`, `menuSectionId`, `optionId`, `status`) VALUES
(1, 102, 99, '|1|2|4|5|7|', 'A'),
(2, 102, 99, '|1|2|4|5|7|', 'A');

INSERT INTO  `menumaster` (
`menuId` ,
`menuName` ,
`parentMenuId` ,
`subMenuId` ,
`districtId` ,
`menuArrow` ,
`pageName` ,
`pageNameWithSubPage` ,
`orderBy` ,
`status`
)
VALUES (
NULL ,  'Users',  '48', NULL , NULL , NULL ,  'importuserdetails.do',  '|importuserdetails.do|usertemplist.do|',  '3',  'A'
);

INSERT INTO  `menusectionmaster` (
`menuSectionId` ,
`menuId` ,
`menuSectionName`
)
VALUES (
NULL ,  '103',  'Users'
);

INSERT INTO `roleaccesspermission` (`roleId`, `menuId`, `menuSectionId`, `optionId`, `status`) VALUES
(1, 103, 100, '|1|2|4|5|7|', 'A')
