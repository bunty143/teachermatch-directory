package tm.bean.user;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.FlagTerm;
import javax.servlet.ServletContext;

import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.TeacherDetail;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.MessageToTeacherDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.user.PersonalEmailTrackDAO;
import tm.dao.user.UserMasterDAO;
 
public class JavaMailPop3Reader {
	
    private String saveDirectory;
 
    public void setSaveDirectory(String dir) {
        this.saveDirectory = dir;
    }
  
 @Autowired
 MessageToTeacherDAO messageToTeacherDAO;
 
 @Autowired
 UserMasterDAO userMasterDAO;
 UserMaster senderIdByEmail=null;
 
 @Autowired
 TeacherDetailDAO teacherDetailDAO;
 TeacherDetail teacherIdbyEmail=null;
  
 @Autowired
 PersonalEmailTrackDAO personalEmailTrackDAO;
 
 @Autowired
 JobForTeacherDAO jobForTeacherDAO;
 JobForTeacher jobForTeacherJobId=null;
 
 @Autowired
 JobOrderDAO jobOrderDAO;
 JobOrder jobOrder=null;
    
 List<PersonalEmailTrack> communicationMailList=null;
 List<UserMaster> lstUserMAster=null;
 List<TeacherDetail> lastTeacherDetailsComp=null;
    
    public void downloadEmailAttachments(MessageToTeacherDAO messageToTeacherDAO1,ServletContext servletContext,UserMasterDAO userMasterDAO,TeacherDetailDAO teacherDetailDAO,PersonalEmailTrackDAO personalEmailTrackDAO,JobForTeacherDAO jobForTeacherDAO,JobOrderDAO jobOrderDAO)
    {
    	try {
    		communicationMailList=personalEmailTrackDAO.getEmailIdWithPassword("A");
    		List<String> teacherDetails=new ArrayList<String>();
    		List<Integer> userMasters=new ArrayList<Integer>();
    		if(communicationMailList!=null && communicationMailList.size()>0){
    			for(PersonalEmailTrack pet:communicationMailList){    		
		    		System.out.println("Email Idddd===="+pet.getPersonalEmailAddress());    		
		    		System.out.println("Email Host===="+pet.getMailHost());
		    		System.out.println("Host Port===="+pet.getMailPort());
		    		
		    		Date temp=null;   		    		
		    		temp=pet.getLastmailreadtime();
		    		System.out.println("time=================="+temp);    		   		
		    		
		    		String host = pet.getMailHost();
		            String userName = pet.getPersonalEmailAddress();
		            String password = pet.getPersonalEmailPassword();
		     
				    Properties props = new Properties();
			        props.setProperty("mail.store.protocol", "imaps");			        			        

			        Session session = Session.getInstance(props, null);
			        Store store = session.getStore();
			        Message[] arrayMessages=null;
			        Folder folderInbox=null;
			        try{
				        store.connect(host,userName, password);	
			            // opens the inbox folder	        
				        
				        if(userName.contains("gmail")){
				        	 folderInbox = store.getFolder("[Gmail]/All Mail");	
				        	 folderInbox.open(Folder.READ_ONLY); 
				        }else if(userName.contains("yahoo")){			        	 
			        		 folderInbox = store.getFolder("Inbox"); 
			        		 folderInbox.open(Folder.READ_ONLY); 		        	
				        }else{	    
				        	folderInbox = store.getFolder("Inbox");
						    folderInbox.open(Folder.READ_ONLY);
				        }
			            
			            arrayMessages = folderInbox.getMessages();   
			            FetchProfile fp = new FetchProfile();
			            fp.add(FetchProfile.Item.ENVELOPE);
			            fp.add(FetchProfile.Item.CONTENT_INFO);
			            folderInbox.fetch(arrayMessages, fp);
					 }catch (Exception e) {
						 e.printStackTrace();
					 }
		         
		             if(arrayMessages!=null)
		            System.out.println("arrayMessages length==="+arrayMessages.length);
		             
		            if(arrayMessages!=null)
		            for (int i = 0; i < arrayMessages.length; i++) { //messsage for loop 
		                Message message = arrayMessages[i];
		                Address[] fromAddress = message.getFrom();
		                String from="";                
		                if(fromAddress!=null)
		                {
		                 from = fromAddress[0].toString();
		                }
		                
		                Address[] toAddress = message.getAllRecipients();               
		                String toMail="";              
		                if(toAddress!=null)
		                {              
		                 toMail=toAddress[0].toString();
		                }                
		                
		                Date sentDate = message.getSentDate();
		                 // store attachment file name, separated by comma
		                if(sentDate!=null && temp.compareTo(sentDate)<0){                
			                if(toMail.contains("<"))
			                {
			                	toMail = toMail.substring(toMail.indexOf("<")+1, toMail.indexOf(">"));
			                	System.out.println("toMail====="+toMail);
			                	if(toMail.contains("@"))
			                	{
			                		toMail=toMail.trim();
			                		System.out.println("toMail="+toMail);
			                	}               
			                }
			                else if(toMail.contains("@"))
			                {
			                	toMail=toMail.trim();
			            		System.out.println("toMail="+toMail);
			                }
			                String swapTemp="";                
			               
			                if(toMail.equalsIgnoreCase(userName))
			                {
			                	swapTemp=from;
			                	from=toMail;
			                	toMail=swapTemp;
			                	         	
			                }           
			                teacherDetails.add(toMail.toLowerCase());
			                userMasters.add(pet.getUserId());
		               }
		            }//Message for loop
		    	}//End for loop
    			//////////////////////////////////////////////////////////////////////////////////
    			
    			Map<String, TeacherDetail> teacherDetailMap=new HashMap<String, TeacherDetail>();
        		Map<Integer, UserMaster> userMasterMap=new HashMap<Integer, UserMaster>();
        		if(communicationMailList!=null && communicationMailList.size()>0){
    	    		lstUserMAster=userMasterDAO.getUsersByUserIds(userMasters);
    	    		lastTeacherDetailsComp=teacherDetailDAO.findByEmails(teacherDetails);
        			if(lastTeacherDetailsComp!=null && lastTeacherDetailsComp.size()>0){
        				for(TeacherDetail td:lastTeacherDetailsComp){    					
        					teacherDetailMap.put(td.getEmailAddress().trim().toLowerCase(), td);    			
        				}
        			}
        			if(lstUserMAster!=null && lstUserMAster.size()>0){    		
        				for(UserMaster userMap: lstUserMAster){
        					userMasterMap.put(userMap.getUserId(), userMap);
        				}
        			}
        		} 
    			////////////////////////
        		for(PersonalEmailTrack pet:communicationMailList){    		
		    		System.out.println("Email Idddd===="+pet.getPersonalEmailAddress());    		
		    		System.out.println("Email Host===="+pet.getMailHost());
		    		System.out.println("Host Port===="+pet.getMailPort());
		    		
		    		Date temp=null;   		    		
		    		temp=pet.getLastmailreadtime();
		    		System.out.println("time=================="+temp);    		   		
		    		
		    		String host = pet.getMailHost();
		            String port = pet.getMailPort(); 
		            String userName = pet.getPersonalEmailAddress();
		            String password = pet.getPersonalEmailPassword();
		     
		            String target = servletContext.getRealPath("/")+"/"+"/Attachment/";
					System.out.println("Real target Path "+target);
		            boolean folderFlag;
						
		            File targetDir = new File(target);
						if(!targetDir.exists())
						{
							targetDir.mkdirs();
							folderFlag=true;
						} 
						else
						{
							folderFlag=true;
						}
						
		           		String saveDirectory="";
						if(folderFlag)
						 saveDirectory = target;
		    		   	
						    Properties props = new Properties();
					        props.setProperty("mail.store.protocol", "imaps");			        			        
		
					        Session session = Session.getInstance(props, null);
					        Store store = session.getStore();
					        Message[] arrayMessages=null;
					        Folder folderInbox=null;
					        try
					        {
					        store.connect(host,userName, password);	
					        
					        Folder[] f = store.getDefaultFolder().list();
				            for(Folder fd:f)
				                System.out.println(">> "+fd.getName());
		            
		            // opens the inbox folder	        
					        
					        if(userName.contains("gmail"))
					        {
					        	 folderInbox = store.getFolder("[Gmail]/All Mail");	
					        	 folderInbox.open(Folder.READ_ONLY); 
					        }
					        else if(userName.contains("yahoo"))
					        {			        	 
					        		 folderInbox = store.getFolder("Inbox"); 
					        		 folderInbox.open(Folder.READ_ONLY); 		        	
					        }
					        else 
					        {	    
					        	folderInbox = store.getFolder("Inbox");
							    folderInbox.open(Folder.READ_ONLY);
					         }
					        
					       
		           /* // search for all "unseen" messages
		            Flags seen = new Flags(Flags.Flag.SEEN);
		            FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
		            Message[] arrayMessages = folderInbox.search(unseenFlagTerm);*/
		            
		            arrayMessages = folderInbox.getMessages();   
		            
		            FetchProfile fp = new FetchProfile();
		            fp.add(FetchProfile.Item.ENVELOPE);
		            fp.add(FetchProfile.Item.CONTENT_INFO);
		            folderInbox.fetch(arrayMessages, fp);
					        }
					        catch (Exception e) {
								e.printStackTrace();
							}
		            // fetches new messages from server
		                   
		            String fileName="";
		         
		            SessionFactory sessionFactory = messageToTeacherDAO1.getSessionFactory();
		    		StatelessSession statelesSsession 	= sessionFactory.openStatelessSession();
		    		Transaction txOpen =statelesSsession.beginTransaction();
		    		
		    		
		    		 DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
		             Calendar cal = Calendar.getInstance();                     
		             String n=dateFormat.format(cal.getTime());
		             
		             if(arrayMessages!=null)
		            System.out.println("arrayMessages length==="+arrayMessages.length);
		             
		            if(arrayMessages!=null)
		            for (int i = 0; i < arrayMessages.length; i++) { //messsage for loop 
		                Message message = arrayMessages[i];
		                Address[] fromAddress = message.getFrom();
		                String from="";                
		                if(fromAddress!=null)
		                {
		                 from = fromAddress[0].toString();
		                }
		                
		                Address[] toAddress = message.getAllRecipients();               
		                String toMail="";              
		                if(toAddress!=null)
		                {              
		                 toMail=toAddress[0].toString();
		                }                
		                
		                String subject = message.getSubject();
		                Date sentDate = message.getSentDate();
		               
		                String contentType = message.getContentType();
		                String messageContent = "";
		                
		                 // store attachment file name, separated by comma
		                String attachFiles = "";               
		                
		                MessageToTeacher messageToTeacher = new MessageToTeacher();
		                PersonalEmailTrack personalEmailTrack=new PersonalEmailTrack();
		                                                  
		              if(sentDate!=null && temp.compareTo(sentDate)<0)
		              {                
		                if (contentType.contains("multipart"))
		                {
		                	// content may contain attachments
		                    Multipart multiPart = (Multipart) message.getContent();
		                    int numberOfParts = multiPart.getCount();
		                    for (int partCount = 0; partCount < numberOfParts; partCount++) 
		                    {
		                        MimeBodyPart part = (MimeBodyPart) multiPart.getBodyPart(partCount);
		                                             
		                        if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition()))
		                         {
		                            // this part is attachment
		                        	                        	
		                        	fileName = part.getFileName();                             
		                            
		                             if(fileName.contains("."))
		                             {
		                            	 String firstNameWithDate = (fileName.substring(0, fileName.lastIndexOf(".")))+"_"+n;                            	
		                            	 fileName = firstNameWithDate + (fileName.substring(fileName.lastIndexOf(".")));
		                            	 fileName = fileName.replaceAll(" ","_");
		                            	 fileName = fileName.replaceAll(":","_");
		                            	                        	
		                             }                            
		                                                      
		                            if(attachFiles.equals(""))
		                            {
		                            	attachFiles = attachFiles + fileName;
		                            }
		                            else
		                            {
		                            	attachFiles = attachFiles+", "+ fileName;
		                            }
		                            part.saveFile(saveDirectory + File.separator + fileName);
		                            System.out.println("File path========="+saveDirectory + File.separator + fileName);
		                        } else  {
		                        	String ss=getText(message);
		                        	messageContent=ss;
		                        }
		                    }
		                  
		                } else if (contentType.contains("text/plain")|| contentType.contains("text/html"))
		                {
		                    Object content = message.getContent();
		                    if (content != null) {
		                        messageContent = content.toString();
		                    }
		                }               
		              
		                if(toMail.contains("<"))
		                {
		                	toMail = toMail.substring(toMail.indexOf("<")+1, toMail.indexOf(">"));
		                	System.out.println("toMail====="+toMail);
		                	if(toMail.contains("@"))
		                	{
		                		toMail=toMail.trim();
		                		System.out.println("toMail="+toMail);
		                	}               
		                }
		                else if(toMail.contains("@"))
		                {
		                	toMail=toMail.trim();
		            		System.out.println("toMail="+toMail);
		                }
		                	                
		                if(from.contains("<"))
		                {
		                	from = from.substring(from.indexOf("<")+1, from.indexOf(">"));
		                	System.out.println("Frommmm====="+from);
		                	if(from.contains("@"))
		                	{
		                		from=from.trim();
		                		System.out.println("from="+from);
		                	}
		                	
		                }
		                else if(from.contains("@"))
		                {
		                	from=from.trim();
		            		System.out.println("from="+from);
		                }
		                
		                String swapTemp="";                
		               
		                if(toMail.equalsIgnoreCase(userName))
		                {
		                	swapTemp=from;
		                	from=toMail;
		                	toMail=swapTemp;
		                	messageToTeacher.setTeacherEmailFlag("1");
		                	         	
		                } else
		                {                	
		                	messageToTeacher.setTeacherEmailFlag("0");  
		                	
		                }              
		                
		                List<TeacherDetail> lstEmailTeacherId=new ArrayList<TeacherDetail>(); 
		                if(teacherDetailMap!=null && teacherDetailMap.size()>0)
		                {
		                	if(teacherDetailMap.containsKey(toMail))
		                	{                		
		                		lstEmailTeacherId.add(teacherDetailMap.get(toMail.toLowerCase().trim()));                	
		                	}
		                	
		                }                
		                
		                messageToTeacher.setTeacherEmailAddress(toMail);
		                messageToTeacher.setMessageSubject(subject);
		                messageToTeacher.setMessageSend(messageContent);
		                messageToTeacher.setSenderEmailAddress(from);
		                messageToTeacher.setCreatedDateTime(sentDate);
		                
		                if(attachFiles!=null && !attachFiles.equals(""))
		                {
		                messageToTeacher.setMessageFileName(attachFiles);
		                }
		                else
		                {
		                	 messageToTeacher.setMessageFileName(null);
		                }
		                
		               
		                
		                if(pet.getUserId()!=null && userMasterMap!=null)
		                {
		                	if(userMasterMap.containsKey(pet.getUserId()))
		                	{
		                		senderIdByEmail=userMasterMap.get(pet.getUserId());	
		                	}                	
		                	//senderIdByEmail=userMasterDAO.findById(pet.getUserId(), false, false);
		                	if(senderIdByEmail!=null)
		                	{   
		                      messageToTeacher.setSenderId(senderIdByEmail);
		                      messageToTeacher.setEntityType(senderIdByEmail.getEntityType());
		                	}
		                }
		                else
		                {
		                	 messageToTeacher.setEntityType(1);
		                }
		                
		                /*if((lstEmail!=null)&&(lstEmail.size()>0))
		                {
		                senderIdByEmail=lstEmail.get(0);
		                messageToTeacher.setSenderId(senderIdByEmail);
		                messageToTeacher.setEntityType(lstEmail.get(0).getEntityType());
		                }*/
		                
		                if((lstEmailTeacherId!=null)&&(lstEmailTeacherId.size()>0))
		                {  
		                   	teacherIdbyEmail=lstEmailTeacherId.get(0);
		                	messageToTeacher.setTeacherId(teacherIdbyEmail);                	
		                }
		                
		                /*if((lstJobForTeacherJobId!=null)&&(lstJobForTeacherJobId.size()>0))
		                {
		                	jobOrder=lstJobForTeacherJobId.get(0).getJobId();
		                	messageToTeacher.setJobId(jobOrder);
		                }*/
		                messageToTeacher.setJobId(null);                
		                statelesSsession.insert(messageToTeacher);
		                
		                personalEmailTrack.setLastmailreadtime(sentDate);
		                personalEmailTrack.setPersonalID(pet.getPersonalID());
		                personalEmailTrack.setMailHost(host);
		                personalEmailTrack.setMailPort(port);
		                personalEmailTrack.setPersonalEmailAddress(userName);
		                personalEmailTrack.setPersonalEmailPassword(password);
		                personalEmailTrack.setPersonalStatus("A"); 
		                personalEmailTrack.setUserId(pet.getUserId());
		                personalEmailTrack.setDistrictId(pet.getDistrictId());
		                statelesSsession.update(personalEmailTrack);                
		                 }
		            }//Message for loop
		            
		             try{
			            //code here for cmt
			            txOpen.commit();
			    		statelesSsession.close();
			            // disconnect
			            folderInbox.close(false);
			            store.close();
		             }catch (Exception e) {
						e.printStackTrace();
					}
		    	}//End for loop
    			////////////////////////////////////////////////////////////////////////////////
    		}
        } catch (NoSuchProviderException ex) {
            System.out.println("No provider for IMAP.");
            ex.printStackTrace();
        } catch (MessagingException ex) {
            System.out.println("Could not connect to the message store");
            ex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private boolean textIsHtml = false;
    private String getText(Part p)
    {
    	try
    	{
    		if (p.isMimeType("text/*")) 
    		{
    	            String s = (String)p.getContent();
    	            textIsHtml = p.isMimeType("text/html");
    	            return s;
    	        }

    	        if (p.isMimeType("multipart/alternative")) 
    		{
    	            // prefer html text over plain text
    	            Multipart mp = (Multipart)p.getContent();
    	            String text = null;
    	            for (int i = 0; i < mp.getCount(); i++)
    	             {
    	                Part bp = mp.getBodyPart(i);
    	                if (bp.isMimeType("text/plain")) 
    			{
    	                    if (text == null)
    	                        text = getText(bp);
    	                    continue;
    	                } else if (bp.isMimeType("text/html"))
    			  {
    	                    String s = getText(bp);
    	                    if (s != null)
    	                        return s;
    	                } else {
    	                    return getText(bp);
    	                }
    	            }
    	            return text;
    	        } else if (p.isMimeType("multipart/*"))
    		    {
    	            Multipart mp = (Multipart)p.getContent();
    	            for (int i = 0; i < mp.getCount(); i++) 
    		    {
    	                String s = getText(mp.getBodyPart(i));
    	                if (s != null)
    	                    return s;
    	            }
    	        }
    	}catch (Exception e) {
			e.printStackTrace();
		}
    	return null;
    }
 
}