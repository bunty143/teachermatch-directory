System.out.println(Utility.decodeBase64("MDUxNTg0Mzg3"));

Need a report of all applicants from TeacherMatch that have accepted an "Offer"

SELECT J.createdDateTime,J.status,J.districtId,P.firstName,P.lastName,P.dob,P.ssn,P.employeeNumber,P.addressLine1,C.cityName,S.stateName,P.zipCode,P.phoneNumber,T.emailAddress,J.isAffilated 
FROM jobforteacher J, teacherdetail T, teacherpersonalinfo P, citymaster C, statemaster S
WHERE J.districtId=1200390 AND (J.status = 6 OR J.status=18)
AND J.teacherId = T.teacherId
AND T.teacherId = P.teacherId
AND P.cityId = C.cityId
AND P.stateId = S.stateId
GROUP BY J.teacherId


SELECT J.createdDateTime,J.offerAcceptedDate,J.hiredByDate,J.status,J.districtId,P.firstName,P.lastName,P.dob,P.ssn,P.employeeNumber,P.addressLine1,C.cityName,S.stateName,P.zipCode,P.phoneNumber,T.emailAddress,J.isAffilated 
FROM jobforteacher J, teacherdetail T, teacherpersonalinfo P, citymaster C, statemaster S
WHERE J.districtId=1200390 AND (J.status = 6 OR J.status=18)
AND J.teacherId = T.teacherId
AND T.teacherId = P.teacherId
AND P.cityId = C.cityId
AND P.stateId = S.stateId
AND (J.offerAcceptedDate >= "2015-10-16 00:00:00" AND J.offerAcceptedDate <= "2016-02-24 00:00:00")
GROUP BY J.teacherId



SELECT J.createdDateTime,J.offerAcceptedDate,J.hiredByDate,J.status,J.districtId,P.firstName,P.lastName,P.dob,P.ssn,P.employeeNumber,P.addressLine1,C.cityName,S.stateName,P.zipCode,P.phoneNumber,T.emailAddress,J.isAffilated 
FROM jobforteacher J, teacherdetail T, teacherpersonalinfo P, citymaster C, statemaster S
WHERE J.districtId=1200390 AND (J.status = 6 OR J.status=18)
AND J.teacherId = T.teacherId
AND T.teacherId = P.teacherId
AND P.cityId = C.cityId
AND P.stateId = S.stateId
AND J.offerAcceptedDate IS NULL
AND J.jobforteacherId IN (633092,704135,691071,689624,700661,694542,700626,689508,700624,687332,587905)
GROUP BY J.teacherId



SELECT * FROM jobforteacher WHERE districtId=1200390 AND (status = 6 OR status=18) AND offerAcceptedDate IS NULL


update jobforteacher inner join (select teacherstatushistoryforjob.teacherId as teacherId, teacherstatushistoryforjob.jobId as jobId, teacherstatushistoryforjob.createdDateTime as createdDateTime, joborder.districtId as districtId from teacherstatushistoryforjob inner join joborder on joborder.jobId=teacherstatushistoryforjob.jobId where teacherstatushistoryforjob.statusId=18 and teacherstatushistoryforjob.status='S' and joborder.districtId=1200390) as update_tableDATA on update_tableDATA.teacherId=jobforteacher.teacherId and update_tableDATA.jobId=jobforteacher.jobId and update_tableDATA.districtId=jobforteacher.districtId set offerAcceptedDate=update_tableDATA.createdDateTime where jobforteacher.offerAcceptedDate is null and jobforteacher.status in (6,18);

======================================================================================================================================================
(MDCPS is requesting the following information:  Can you please inquire on how many or a list of candidates that 
are coded in Tm as internal (hired) but for which TM does not have an employee number?)

SELECT T.teacherId,P.firstName,P.lastName,P.dob,P.employeeNumber,T.emailAddress,J.isAffilated 
FROM jobforteacher J, teacherdetail T, teacherpersonalinfo P
WHERE J.districtId = 1200390 
AND J.status = 6
AND J.teacherId = T.teacherId
AND T.teacherId = P.teacherId
AND J.isAffilated = 1
AND (P.employeeNumber IS NULL OR P.employeeNumber = "")
GROUP BY J.teacherId
======================================================================================================================================================
1. Create a list of all active, current Miami applicants that have triggered the RED thumbs down
Select firstName,lastName,emailAddress, status  from teacherdetail where status='A' and teacherId in (SELECT teacherId  FROM `jobforteacher` WHERE `districtId` = 1200390 AND `flagForDistrictSpecificQuestions` = 0 AND flagForDistrictSpecificQuestions IS NOT NULL group by teacherId);

Find list of Green thumb and cross verify in master list.

2. Create a list of all HIRED Miami applicants that have triggered the RED thumbs down
SELECT distinct td.teacherId ,td.firstName,td.lastName,td.emailAddress,jft.jobId,jft.flagForDistrictSpecificQuestions 
FROM jobforteacher jft
INNER JOIN teacherdetail td on td.teacherId=jft.teacherId 
WHERE jft.districtId = 1200390 AND jft.flagForDistrictSpecificQuestions = 0 and jft.flagForDistrictSpecificQuestions IS NOT NULL and jft.status=6 group by jft.teacherId;

===================================================================================
UPDATE offerAccepted Date from JFTHistory 
select * from jobforteacher inner join (select teacherstatushistoryforjob.teacherId as teacherId, teacherstatushistoryforjob.jobId as jobId, teacherstatushistoryforjob.createdDateTime as createdDateTime, joborder.districtId as districtId from teacherstatushistoryforjob inner join joborder on joborder.jobId=teacherstatushistoryforjob.jobId where teacherstatushistoryforjob.statusId=18 and teacherstatushistoryforjob.status='S' and joborder.districtId=1200390) as update_tableDATA on update_tableDATA.teacherId=jobforteacher.teacherId and update_tableDATA.jobId=jobforteacher.jobId and update_tableDATA.districtId=jobforteacher.districtId where jobforteacher.offerAcceptedDate is null and jobforteacher.status in (6,18);

update jobforteacher inner join (select teacherstatushistoryforjob.teacherId as teacherId, teacherstatushistoryforjob.jobId as jobId, teacherstatushistoryforjob.createdDateTime as createdDateTime, joborder.districtId as districtId from teacherstatushistoryforjob inner join joborder on joborder.jobId=teacherstatushistoryforjob.jobId where teacherstatushistoryforjob.statusId=18 and teacherstatushistoryforjob.status='S' and joborder.districtId=1200390) as update_tableDATA on update_tableDATA.teacherId=jobforteacher.teacherId and update_tableDATA.jobId=jobforteacher.jobId and update_tableDATA.districtId=jobforteacher.districtId set offerAcceptedDate=update_tableDATA.createdDateTime where jobforteacher.offerAcceptedDate is null and jobforteacher.status in (6,18);
======================================================================================================================================================================================================================================================================================================
======================================================================================================================================================================================================================================================================================================

SELECT dm.districtname District, 
jo.jobtitle Job_Title,
jcm.jobcategoryname Job_Category, 
jft.jobid Job_ID, 
jft.jobforteacherId jobforteacherId,
jft.notReviewedFlag notReviewedFlag,
jo.status Job_Status, 
jo.jobstartdate Job_Post_Date 
from jobforteacher jft
left join statusmaster sm on sm.statusid=jft.status
left join teachersecondarystatus tss on jft.teacherid=tss.teacherid 
and jft.jobid=tss.jobid and jft.districtid=tss.districtid
left join secondarystatusmaster ssm on ssm.secstatusid=tss.secStatusId
left join secondarystatus ss on ss.secondarystatusId=jft.displaysecondarystatusid
left join joborder jo on jo.jobid=jft.jobid
left join jobcategorymaster jcm on jo.jobcategoryid=jcm.jobcategoryid
left join districtmaster dm on dm.districtid=jft.districtid
left join democlassschedule dcs on dcs.teacherid=jft.teacherid and dcs.jobid=jft.jobid
left join panelschedule ps on ps.teacherid=jft.teacherid and ps.jobid=jft.jobid
left join teacherphonecallhistory tpch on tpch.teacherid=jft.teacherid and tpch.jobid=jft.jobid
left join onlineactivitynotes oan on oan.teacherid=jft.teacherid and oan.jobid=jft.jobid
left join referencenotes rn on rn.teacherid=jft.teacherid and rn.jobid=jft.jobid
left join teacherstatusnotes tsn on tsn.teacherid=jft.teacherid and tsn.jobid=jft.jobid
left join jsicommunicationlog jcl on jcl.teacherid=jft.teacherid and jcl.jobid=jft.jobid
left join jobforteacher hired on hired.teacherid=jft.teacherid and hired.status=6
where jft.status=4
and jft.lastactivity is null
and ssm.secstatusname is null
and jft.displaysecondarystatusid is null
and dcs.demoid is null
and ps.panelid is null
and tpch.teacherphonecallid is null
and tpch.teacherid is null
and oan.teacherid is null
and rn.teacherid is null
and tsn.teacherid is null
and jcl.teacherid is null
and hired.teacherid is null
and jft.districtId =1200390
and jft.notReviewedFlag !=1


UPDATE jobforteacher SET notReviewedFlag = 1 WHERE jobforteacherId IN(733066,757714,759497,760259,787964,788411,800618,801669,801861,802200,802438,802719,802944,803083,804185,805343,805481,805586,805916)



SELECT J.jobForTeacherId,J.districtId,J.teacherId,J.jobId,J.status 
FROM jobforteacher J,sapcandidatedetails S
WHERE J.districtId=1200390
AND J.status!=6
AND J.status=18
AND J.teacherId=S.teacherId
AND J.jobId=S.jobId
AND S.sapStatus = "S"
======================================================================================================================================================================================================================================================================================================
======================================================================================================================================================================================================================================================================================================

								SQLcandidateWithdrawnReason
===================================================================================================
select w.districtname District, jobtitle Job, 
withdrawnBy Withdrawn_By, WithdrawTime Withdraw_Time, 
withdrawnReason Withdrawn_Reason, count(*) N_Of_Withdraws
from 
(
select dm.districtname, jo.jobtitle, jft.teacherid, jft.districtid, 
jft.jobid, sm.status, jft.withdrawndatetime, 
case when ActiontakenByCandidate=1 then 'Candidate' else rm.rolename end `withdrawnBy`,
tshfj.ActiontakenByCandidate, 
case when tshfj.timingforDclwrdw is null then 'Candidate Withdraw' else dst.timing end WithdrawTime, 
case when ActiontakenByCandidate=1 then wrm.reason
	else dsr.reason end `withdrawnReason`
from jobforteacher jft
left join teacherstatushistoryforjob tshfj on jft.teacherid=tshfj.teacherid
	and jft.status=tshfj.statusid and jft.jobid=tshfj.jobid
left join statusmaster sm on sm.statusid=jft.status
left join withdrawnreasonmaster wrm on wrm.withdrawnreasonmasterid=tshfj.withdrawnreasonmasterid
left join districtmaster dm on dm.districtid=jft.districtid
left join joborder jo on jo.jobid=jft.jobid
left join usermaster um on um.userid=tshfj.createdBy
left join rolemaster rm on rm.roleid=um.roleid
left join districtspecificreason dsr on dsr.reasonid=tshfj.reasonforDclWrdw
left join districtspecifictiming dst on dst.timingid=tshfj.timingforDclwrdw
where jft.status=7 and (case when ActiontakenByCandidate=1 then wrm.reason else dsr.reason end is not null) and jft.districtid=1200390
) w
group by w.districtname, jobid, withdrawnBy, WithdrawTime, withdrawnreason;