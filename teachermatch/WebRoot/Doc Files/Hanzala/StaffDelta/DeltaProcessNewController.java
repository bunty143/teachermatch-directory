package tm.controller.master;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EmployeeMaster;
import tm.bean.FileUploadHistory;
import tm.bean.JobForTeacher;
import tm.bean.JobForTeacherHistory;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.SapCandidateDetails;
import tm.bean.SapCandidateDetailsHistory;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.TeacherStatusHistoryForJobHistory;
import tm.bean.UserUploadFolderAccess;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScoreHistory;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.TeacherStatusNotesHistory;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.cgreport.TeacherStatusScoresHistory;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.PanelSchedule;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EmployeeMasterDAO;
import tm.dao.FileUploadHistoryDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SapCandidateDetailsDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.UserUploadFolderAccessDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.PanelScheduleDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.ScheduleMailThreadWithAttachment;
import tm.services.report.CandidateGridService;
import tm.utility.Utility;

@Controller
public class DeltaProcessNewController 
{
	
	String locale = Utility.getValueOfPropByKey("locale");
	  String TM_MDCPS_staff_delta1= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta1", locale);
	  String msgNotFoundFileUpload1= Utility.getLocaleValuePropByKey("msgNotFoundFileUpload1", locale);
	  String MSGsdpstaffmasterFilefound2= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound2", locale);
	  String MSGsdpstaffmasterFilefound3= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound3", locale);
	  String MSGsdpstaffmasterFilefound4= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound4", locale);
	  String MSGsdpstaffmasterFilefound5= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound5", locale);
	  String MSGsdpstaffmasterFilefound6= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound6", locale);
	  String MSGsdpstaffmasterFilefound7= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound7", locale);
	  String MSGsdpstaffmasterFilefoundf= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefoundf", locale);
	  String msgLocColumnNotFound1= Utility.getLocaleValuePropByKey("msgLocColumnNotFound1", locale);
	  String MSGsdpstaffmasterFilefound8= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound8", locale);
	  String MSGsdpstaffmasterFilefound9= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound9", locale);
	  String TM_MDCPS_staff_delta2= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta3", locale);
	  String TM_MDCPS_staff_delta3= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta1", locale);
	  String MSGsdpstaffmasterFilefound11= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound11", locale);
	  String MSGsdpstaffmasterFilefound12= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound12", locale);
	  String TM_MDCPS_staff_delta4= Utility.getLocaleValuePropByKey("TM_MDCPS_staff_delta4", locale);
	  String MSGsdpstaffmasterFilefound13= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound13", locale);
	  String MSGsdpstaffmasterFilefound14= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound14", locale);
	  String MSGsdpstaffmasterFilefound15= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound15", locale);
	  String lblFileName1 = Utility.getLocaleValuePropByKey("lblFileName1", locale);
	  String lblNewRecords1 = Utility.getLocaleValuePropByKey("lblNewRecords1", locale);
	  String lblUpdateRecords1 = Utility.getLocaleValuePropByKey("lblUpdateRecords1  ", locale);
	  String lblUploadDateTime1 = Utility.getLocaleValuePropByKey("lblUploadDateTime1 ", locale);
	  String MSGsdpstaffmasterFilefound16= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound16", locale);
	  String ProcessingDateis= Utility.getLocaleValuePropByKey("ProcessingDateis", locale);
	  String MSGsdpstaffmasterFilefound17= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound17", locale);
	  String NoUpdateforOC= Utility.getLocaleValuePropByKey("NoUpdateforOC", locale);
	  String MSGsdpstaffmasterFilefound18= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound18", locale);
	  String MSGsdpstaffmasterFilefound19= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound19", locale);
	  String MSGsdpstaffmasterFilefound20= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound20", locale);
	  String MSGsdpstaffmasterFilefound21= Utility.getLocaleValuePropByKey("MSGsdpstaffmasterFilefound21", locale);
	  String LBLJobForTeacherId1= Utility.getLocaleValuePropByKey("LBLJobForTeacherId1", locale);
	  String LBLTeacherId1= Utility.getLocaleValuePropByKey("LBLTeacherId1", locale);
	  String EmailAddressD= Utility.getLocaleValuePropByKey("EmailAddressD", locale);
	  String lblNotes= Utility.getLocaleValuePropByKey("lblNotes", locale);
	  String LBLInternalToExternal1= Utility.getLocaleValuePropByKey("LBLInternalToExternal1", locale);
	  String LBLOldStatus1= Utility.getLocaleValuePropByKey("LBLOldStatus1", locale);
	  String LBLOldSecStatus1= Utility.getLocaleValuePropByKey("LBLOldSecStatus1", locale);
	  String LBLNewStatus1= Utility.getLocaleValuePropByKey("LBLNewStatus1", locale);
	  String LBLNewSecStName1= Utility.getLocaleValuePropByKey("LBLNewSecStName1", locale);
	  String LBLOldRequisitionNumber1= Utility.getLocaleValuePropByKey("LBLOldRequisitionNumber1", locale);
	  String LBLResetJobRequisitionNumberId1= Utility.getLocaleValuePropByKey("LBLResetJobRequisitionNumberId1", locale);
	  String lblAlumEmpId1= Utility.getLocaleValuePropByKey("lblAlumEmpId1", locale);
	  String LBLJobDetails1= Utility.getLocaleValuePropByKey("LBLJobDetails1", locale);
	  String lblCancelPanelId1= Utility.getLocaleValuePropByKey("lblCancelPanelId1", locale);
	  String msgRejectedTheCandidate9= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate9", locale);
	  String lblPCProcessing1= Utility.getLocaleValuePropByKey("lblPCProcessing1", locale);
	  String lblOCProcessing1= Utility.getLocaleValuePropByKey("lblOCProcessing1", locale);
	 
	  String msgRejectedTheCandidate1= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate1", locale);
	  String msgRejectedTheCandidate2= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate2", locale);
	  String msgRejectedTheCandidate3= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate3", locale);
	  String msgRejectedTheCandidate4= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate4", locale);
	  String msgRejectedTheCandidate5= Utility.getLocaleValuePropByKey("msgRejectedTheCandidate5", locale);
	
	@Autowired
	private UserUploadFolderAccessDAO userUploadFolderAccessDAO;
	
	@Autowired
	private FileUploadHistoryDAO fileUploadHistoryDAO;
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
        this.emailerService = emailerService;
    }
	
	@Autowired
	private EmployeeMasterDAO employeeMasterDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	
	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	
	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	
	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private TeacherStatusScoresDAO teacherStatusScoresDAO;  
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;
	
	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;
	
	@Autowired
	private PanelScheduleDAO panelScheduleDAO;
	
	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private SapCandidateDetailsDAO sapCandidateDetailsDAO;
	
	@RequestMapping(value="/staffdeltaprocess.do", method=RequestMethod.GET)
	public String getallactivedistrict(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println("Start Staff Delta File Process ...");
		List<UserUploadFolderAccess> uploadFolderAccessesrec = null;
		try {
			Criterion functionality = Restrictions.eq("functionality", "employeeUpload");
			Criterion active = Restrictions.eq("status", "A");
			uploadFolderAccessesrec = userUploadFolderAccessDAO.findByCriteria(functionality,active);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		int i=0;
		for (UserUploadFolderAccess getRec : uploadFolderAccessesrec) {
			i++;
			DistrictMaster districtMaster=getRec.getDistrictId();
			File filesList = findLatestFilesByPath(getRec.getFolderPath());
			
			if(filesList==null){
				String to= "gourav@netsutra.com";
				String subject =TM_MDCPS_staff_delta1;
				String msg = msgNotFoundFileUpload1;						
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom("admin@netsutra.com");
				dsmt.setMailto(to);
				dsmt.setMailsubject(subject);
				dsmt.setMailcontent(msg);
				try {
					dsmt.start();	
				} catch (Exception e) {}
				
			}else{
				try {
					String FileName = filesList.getName();
					Integer rowId = getRec.getUseuploadFolderId();
					String folderPath = getRec.getFolderPath();
					String lastUploadDateTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new java.util.Date(filesList.lastModified()));
					
					savejob(FileName,districtMaster,rowId,folderPath,lastUploadDateTime);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} 
			
			if(i==1){
				break;
			}
		}
		
		System.out.println("End Delta File Process ...");
		
		return null;
	}
	
	public String savejob(String fileName,DistrictMaster districtMaster,Integer rowId,String folderPath,String lastUploadDateTime)
	{							
    try{
    	 
    	 Vector vectorDataExcelXLSX = new Vector();
    	 String filePath=folderPath+fileName;
    	 if(fileName.contains(".csv") || fileName.contains(".CSV")){
    		 vectorDataExcelXLSX = readDataExcelCSV(filePath);
    	 }
	 	int first_name_count=11;
        int middle_name_count=11;
        int last_name_count=11;	 	        
        int employee_number_count=11;
        int last4_count=11;
        int pc_count=11;
        int rr_count=11;
        int oc_count=11;
        int staff_type_count=11;
        int status_count=11;
        int email_count=11;
        
       
        SessionFactory sessionFactory=employeeMasterDAO.getSessionFactory();
		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
 	    Transaction txOpen =statelesSsession.beginTransaction();
 	    
        int mapCount=0;
        int updateCount=0;
        int createCount=0;
        int numOfError=0;
        
        String Employee_Number_store="";
        String final_error_store="";
        Criterion criterion= Restrictions.eq("districtMaster", districtMaster);
		List<EmployeeMaster> lstEmployeeMaster = employeeMasterDAO.findByCriteria(criterion);
		Map<String,EmployeeMaster> mapEmployeeMaster=new HashMap<String, EmployeeMaster>();
		for(EmployeeMaster employeeMaster:lstEmployeeMaster)
			mapEmployeeMaster.put(employeeMaster.getEmployeeCode(), employeeMaster);
		 
		List<EmployeeMaster> lstEmployeeMasterForProcessPC=new ArrayList<EmployeeMaster>();
		List<EmployeeMaster> lstEmployeeMasterForProcessOC=new ArrayList<EmployeeMaster>();
		List<EmployeeMaster> lstEmpMstrExToIn=new ArrayList<EmployeeMaster>();
		List<EmployeeMaster> lstEmpMstrInToEx=new ArrayList<EmployeeMaster>();
		
			 boolean row_error=false;
				 for(int i=0; i<vectorDataExcelXLSX.size(); i++) {
		            Vector vectorCellEachRowData = (Vector) vectorDataExcelXLSX.get(i);
		            String first_name="";
		            String middle_name="";
		            String last_name="";	 	        
		            String employee_number="";
		            String last4="";
		            String pc="";
		            String rr="";
		            String oc="";
		            String staff_type="";
		            String status="";
		            String email="";
		            
		  	      String errorText="";
		  	    String rowErrorText="";
		  	        for(int j=0; j<vectorCellEachRowData.size(); j++) {
		  	        	try{
		  	        		
		  	        		if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("first_name")){
		  	        			first_name_count=j;
			            	}
			            	if(first_name_count==j)
			            		first_name=vectorCellEachRowData.get(j).toString().trim();
		
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("middle_name")){
			            		middle_name_count=j;
			            	}
			            	if(middle_name_count==j)
			            		middle_name=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("last_name")){
										last_name_count=j;
									}
			            	if(last_name_count==j)
			            		last_name=vectorCellEachRowData.get(j).toString().trim();
			            				            	
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("employee_number")){
			            		employee_number_count=j;
			            	}
			            	if(employee_number_count==j)
			            		employee_number=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("last4")){
			            		last4_count=j;
			            	}
			            	if(last4_count==j)
			            		last4=vectorCellEachRowData.get(j).toString().trim();
			            	
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("pc")){
			            		pc_count=j;
			            	}
			            	if(pc_count==j)
			            		pc=vectorCellEachRowData.get(j).toString().trim();

			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("rr")){
			            		rr_count=j;
			            	}
			            	if(rr_count==j)
			            		rr=vectorCellEachRowData.get(j).toString().trim();
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("oc")){
			            		oc_count=j;
			            	}
			            	if(oc_count==j)
			            		oc=vectorCellEachRowData.get(j).toString().trim();
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("staff_type")){
			            		staff_type_count=j;
			            	}
			            	if(staff_type_count==j)
			            		staff_type=vectorCellEachRowData.get(j).toString().trim();
						if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("status")){
			            		status_count=j;
			            	}
			            	if(status_count==j)
			            		status=vectorCellEachRowData.get(j).toString().trim();
			            	if(vectorCellEachRowData.get(j).toString().equalsIgnoreCase("email")){
			            		email_count=j;
			            	}
			            	if(email_count==j)
			            		email=vectorCellEachRowData.get(j).toString().trim();
		            	}catch(Exception e){}
		  	        }
		  	        
		  	        if(i==0){
			  		  
			  		  if(!first_name.equalsIgnoreCase("first_name")){
			  			rowErrorText=MSGsdpstaffmasterFilefound2;
			  			row_error=true;
			  		  }
			  		  if(!middle_name.equalsIgnoreCase("middle_name")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound3;
			  			row_error=true;
			  		  }
			  		  if(!last_name.equalsIgnoreCase("last_name")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound4;
			  			row_error=true;  
			  		  }
			  		if(!employee_number.equalsIgnoreCase("employee_number")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound5;
			  			row_error=true;  
			  		  }
					  if(!last4.equalsIgnoreCase("last4")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound6;
			  			row_error=true;  
			  		  }
					  if(!pc.equalsIgnoreCase("PC")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound7;
			  			row_error=true;  
			  		  }
					  if(!rr.equalsIgnoreCase("RR")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefoundf;
			  			row_error=true;  
			  		  }
					  if(!oc.equalsIgnoreCase("OC")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=msgLocColumnNotFound1;
			  			row_error=true;  
			  		  }
					  if(!staff_type.equalsIgnoreCase("staff_type")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound8;
			  			row_error=true;  
			  		  }
					  if(!status.equalsIgnoreCase("status")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=MSGsdpstaffmasterFilefound9;
			  			row_error=true;  
			  		  }
					  if(!email.equalsIgnoreCase("email")){
			  			if(row_error){
			  				rowErrorText+=",";
			  			  }
			  			rowErrorText+=TM_MDCPS_staff_delta2;
			  			row_error=true;  
			  		  }
			  		 }
		  	      if(row_error){
				  		File file = new File(folderPath+"/excelUploadError.txt");
						if (!file.exists()) {
							file.createNewFile();
						}
							    				
						FileWriter fw = new FileWriter(file.getAbsoluteFile());
						BufferedWriter bw = new BufferedWriter(fw);
						bw.write(rowErrorText);
		
						bw.close();
				  		
						String to= "gourav@netsutra.com";
						String subject = TM_MDCPS_staff_delta3;
						String msg = "Error in "+fileName+" file";
						String errorFileName="excelUploadError.txt";			

						ScheduleMailThreadWithAttachment smtwa = new ScheduleMailThreadWithAttachment();
						smtwa.setEmailerService(emailerService);
						smtwa.setMailfrom("admin@netsutra.com");
						smtwa.setMailto(to);
						smtwa.setMailsubject(subject);
						smtwa.setMailcontent(msg);
						smtwa.setFileName(errorFileName);
						smtwa.setFilePath(folderPath);
						try {
							smtwa.start();	
						} catch (Exception e) {}
						
						
						numOfError++;
				  		 break;
				  	 }
		  	        
		  	      
		  	        if(i!=0)
		  	        {
		  	        		EmployeeMaster employeeMaster=null;
			  	        	boolean errorFlag=false;
			  	        	boolean updateFlag=false;
			  	        	if(first_name.equalsIgnoreCase("")){
			  	    			errorText+=MSGsdpstaffmasterFilefound11;
			  	    			errorFlag=true;
			  	    		}		  	    		
			  	    		if(last_name.equalsIgnoreCase("")){
			  	    			if(errorFlag){
		            	 			errorText+=",";	
		            	 		}
			  	    			errorText+=MSGsdpstaffmasterFilefound12;
			  	    			errorFlag=true;
			  	    		}
			  	    		
			  	    		if(employee_number.equalsIgnoreCase("")){
			  	    			if(errorFlag){
		            	 			errorText+=",";	
		            	 		}
			  	    			errorText+=TM_MDCPS_staff_delta4;
			  	    			errorFlag=true;
			  	    		}else if(!employee_number.equalsIgnoreCase("")){
			  	    			if(Employee_Number_store.contains("||"+employee_number+"||")){
			  	    				
			  	    				if(errorFlag){
			  	    				errorText+=",";	
			  	    				}
			  	    				errorText+=MSGsdpstaffmasterFilefound13;
			  	    				errorFlag=true;
			  	    			}
			  	    			else
			  	    			{			  	    				
				  	    			if(mapEmployeeMaster!=null && mapEmployeeMaster.size()>0)
				  	    			{
				  	    				if(mapEmployeeMaster.get(employee_number)!=null)
						  	    		{
				  	    					updateFlag=true;
				  	    					employeeMaster=mapEmployeeMaster.get(employee_number);
				  							if(employeeMaster!=null)
				  							{
				  								if(employeeMaster.getPCCode()!=null && employeeMaster.getPCCode().equalsIgnoreCase("N") && pc!=null && pc.equalsIgnoreCase("Y"))
				  									lstEmployeeMasterForProcessPC.add(employeeMaster);
				  								else if(employeeMaster.getOCCode()!=null && employeeMaster.getOCCode().equalsIgnoreCase("N") && oc!=null && oc.equalsIgnoreCase("Y"))
				  									lstEmployeeMasterForProcessOC.add(employeeMaster);
				  								
				  								if((( employeeMaster.getFECode()==null || employeeMaster.getFECode().equalsIgnoreCase("") ) || (employeeMaster.getFECode()!=null && ( employeeMaster.getFECode().equalsIgnoreCase("1") || employeeMaster.getFECode().equalsIgnoreCase("3")))) && status!=null && status.equalsIgnoreCase("0"))
							            	 		lstEmpMstrInToEx.add(employeeMaster);
							            	 	else if((( employeeMaster.getFECode()==null || employeeMaster.getFECode().equalsIgnoreCase("") ) || (employeeMaster.getFECode()!=null && employeeMaster.getFECode().equalsIgnoreCase("0"))) && (status==null || status.equalsIgnoreCase("") || status.equalsIgnoreCase("1") || status.equalsIgnoreCase("3")))
							            	 		lstEmpMstrExToIn.add(employeeMaster);
				  							}
				  	    					
						  	    			if(errorFlag)
					  							errorText+=",";	
						  	    		}
				  	    			}
				  	    			
			  	    			}
			  	    		}


			  	    		if(!employee_number.equalsIgnoreCase("")){
			  	    			Employee_Number_store+="||"+employee_number+"||";
			  	    		}
			  	    		
			  	    		if(!errorText.equals("")){
			  	    			int row = i+1;
			  	    			final_error_store+="Row "+row+" : "+errorText+"\r\n"+first_name+","+middle_name+","+last_name+","+employee_number+","+last4+","+pc+","+rr+","+oc+","+staff_type+","+status+","+email+"<>";
			  	    			numOfError++;
			  	    		}
			  	    		if(errorText.equals(""))
			  	    		{
				  	    			if(updateFlag==false)
				  	    			{
				  	    				employeeMaster=new EmployeeMaster();
				  	    				employeeMaster.setDistrictMaster(districtMaster);
				  	    				employeeMaster.setFirstName(first_name);
				  	    				employeeMaster.setMiddleName(middle_name);
				  	    				employeeMaster.setLastName(last_name);
				  	    				employeeMaster.setEmailAddress(email);
				  	    				employeeMaster.setSSN(last4);
				  	    				employeeMaster.setEmployeeCode(employee_number);
				  	    				employeeMaster.setFECode(status);
				  	    				employeeMaster.setRRCode(rr);
				  	    				employeeMaster.setOCCode(oc);
				  	    				employeeMaster.setPCCode(pc);
				  	    				employeeMaster.setStaffType(staff_type);
				  	    				employeeMaster.setSAPId("");
				  	    				employeeMaster.setStatus("A");			  	    			
				  	    				employeeMaster.setCreatedDateTime(new Date());
				  	    				employeeMaster.setAlum("N");
				  	    				
					  	    			mapCount++;
					  	    			createCount++;
					  	    			
					  	    			txOpen =statelesSsession.beginTransaction();
					            	 	statelesSsession.insert(employeeMaster);
					            	 	txOpen.commit();
					            	 	
					            	 	if(pc!=null && pc.equalsIgnoreCase("Y"))
					            	 		lstEmployeeMasterForProcessPC.add(employeeMaster);
					            	 	else if(oc!=null && oc.equalsIgnoreCase("Y"))
					            	 		lstEmployeeMasterForProcessOC.add(employeeMaster);
					            	 	
					            	 	if(status!=null && !status.equalsIgnoreCase("") && status.equalsIgnoreCase("0"))
					            	 		lstEmpMstrInToEx.add(employeeMaster);
					            	 	else
					            	 		lstEmpMstrExToIn.add(employeeMaster);
					            	 	
				  	    			}
				  	    			else
				  	    			{
				  	    				//String sAlum="N";
				  	    				//if(status!=null && !status.equalsIgnoreCase("") && status.equalsIgnoreCase("0") && employeeMaster.getFECode()!=null && !employeeMaster.getFECode().equalsIgnoreCase("") && (employeeMaster.getFECode().equalsIgnoreCase("1") || employeeMaster.getFECode().equalsIgnoreCase("3")))
				  	    					//sAlum="Y";
				  	    				
				  	    				employeeMaster.setDistrictMaster(districtMaster);
				  	    				employeeMaster.setFirstName(first_name);
				  	    				employeeMaster.setMiddleName(middle_name);
				  	    				employeeMaster.setLastName(last_name);
				  	    				employeeMaster.setEmailAddress(email);
				  	    				employeeMaster.setSSN(last4);
				  	    				employeeMaster.setEmployeeCode(employee_number);
				  	    				employeeMaster.setFECode(status);
				  	    				employeeMaster.setRRCode(rr);
				  	    				employeeMaster.setOCCode(oc);
				  	    				employeeMaster.setPCCode(pc);
				  	    				employeeMaster.setStaffType(staff_type);
				  	    				employeeMaster.setSAPId("");
				  	    				employeeMaster.setStatus("A");			  	    			
				  	    				employeeMaster.setCreatedDateTime(new Date());
				  	    				//employeeMaster.setAlum(sAlum);
				  	    				
					  	    			mapCount++;	
					  	    			updateCount++;
					  	    			
					  	    			txOpen =statelesSsession.beginTransaction();
					            	 	statelesSsession.update(employeeMaster);
					            	 	txOpen.commit();
				  	    			}
			  	    		}
		  	        	}
			  	    }
				 
	     	 	 statelesSsession.close();	     	 	
     	    
	     	 	UserUploadFolderAccess b1 = new UserUploadFolderAccess();
		 	 	b1.setUseuploadFolderId(rowId);
				b1.setDistrictId(districtMaster);
				b1.setFunctionality("employeeUpload");
				b1.setFolderPath(folderPath);
				b1.setNameOfLastFileUploaded(fileName);
				b1.setLastUploadDateTime(getCurrentDateFormart2(lastUploadDateTime));
				b1.setStatus("A");
				userUploadFolderAccessDAO.makePersistent(b1);
				
				FileUploadHistory fileUploadHistory = new FileUploadHistory();
				fileUploadHistory.setUseuploadFolderId(b1);
				fileUploadHistory.setInsertRecord(createCount);
				fileUploadHistory.setUpdateRecord(updateCount);
				fileUploadHistory.setNumOfError(numOfError);
				fileUploadHistory.setDateTime(new Date());
				fileUploadHistoryDAO.makePersistent(fileUploadHistory);

				deleteXlsAndXlsxFile(filePath);// Alpha
				
				if(final_error_store.equalsIgnoreCase(""))
				{
		 	 		String to= "gourav@netsutra.com";
							
					String subject = fileName+" "+MSGsdpstaffmasterFilefound14;
					String msg = MSGsdpstaffmasterFilefound15;
							msg+=lblFileName1+" : "+fileName+"\n";
							msg+=lblNewRecords1+" : "+createCount+"\n";
							msg+=lblUpdateRecords1+" : "+updateCount+"\n";
							msg+=lblUploadDateTime1+" : "+new Date()+"\n";
					System.out.println(msg);
					
					DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
					dsmt.setEmailerService(emailerService);
					dsmt.setMailfrom("admin@netsutra.com");
					dsmt.setMailto(to);
					dsmt.setMailsubject(subject);
					dsmt.setMailcontent(msg);
					try {
						dsmt.start();	
					} catch (Exception e) {}
		 	 	}
				
	     	 	if(!final_error_store.equalsIgnoreCase("")){
	     	 		String content = final_error_store;		    				
    				File file = new File(folderPath+"excelUploadError.txt");
    				if (!file.exists()) {
    					file.createNewFile();
    				}
    				String[] parts = content.split("<>");	    				
    				FileWriter fw = new FileWriter(file.getAbsoluteFile());
    				BufferedWriter bw = new BufferedWriter(fw);
    				bw.write("first_name,middle_name,last_name,employee_number,last4,pc,rr,oc,staff_type,status,email");
    				bw.write("\r\n\r\n");
    				int k =0;
    				for(String cont :parts) {
    					bw.write(cont+"\r\n\r\n");
    				    k++;
    				}
    				bw.close();
    			
    				String to= "gourav@netsutra.com";
    				String subject = fileName+" Upload Error";
    				String msg = "Error in "+fileName+" file";
    				String errorFileName="excelUploadError.txt";
    				
					ScheduleMailThreadWithAttachment smtwa = new ScheduleMailThreadWithAttachment();
					smtwa.setEmailerService(emailerService);
					smtwa.setMailfrom("admin@netsutra.com");
					smtwa.setMailto(to);
					smtwa.setMailsubject(subject);
					smtwa.setMailcontent(msg);
					smtwa.setFileName(errorFileName);
					smtwa.setFilePath(folderPath);
					try {
						smtwa.start();	
					} catch (Exception e) {}
					
    				System.out.println("complete text file create");
	     	 	}
	     	 	
	     	 	
	     	 	//************ process for TPI and JFT
	     	 	System.out.println("Start update process for PC/OC");
	     	 	
	     	 	int iPCCount=0,iOCCount=0,iExToInCount=0,iIntToExCount=0;
	     	 	boolean bPC=false,bOC=false,bExToIn=false,bIntToEx=false;
	     	 	Map<String, Integer> mapTPI=new HashMap<String, Integer>();
	     	 	
	     	 	if(lstEmployeeMasterForProcessPC!=null && lstEmployeeMasterForProcessPC.size() > 0)
	     	 	{
	     	 		iPCCount=lstEmployeeMasterForProcessPC.size();
	     	 		bPC=true;
	     	 	}
	     	 	
	     	 	if(lstEmployeeMasterForProcessOC!=null && lstEmployeeMasterForProcessOC.size() > 0)
	     	 	{
	     	 		iOCCount=lstEmployeeMasterForProcessOC.size();
	     	 		bOC=true;
	     	 	}
	     	 	
	     	 	if(lstEmpMstrExToIn!=null && lstEmpMstrExToIn.size() > 0)
	     	 	{
	     	 		iExToInCount=lstEmpMstrExToIn.size();
	     	 		bExToIn=true;
	     	 	}
	     	 	
	     	 	if(lstEmpMstrInToEx!=null && lstEmpMstrInToEx.size() > 0)
	     	 	{
	     	 		iIntToExCount=lstEmpMstrInToEx.size();
	     	 		bIntToEx=true;
	     	 	}
	     	 	
	     	 	int iCount=iPCCount+iOCCount+iExToInCount+iIntToExCount;
	     	 	if( iCount > 0)
	     	 	{
	     	 		List<TeacherPersonalInfo> lstTPI_FNLNSSN=new ArrayList<TeacherPersonalInfo>();
	     	 		List<TeacherPersonalInfo> lstTPI_FNLNSSNEMPCode=new ArrayList<TeacherPersonalInfo>();
	     	 		List<TeacherPersonalInfo> lstTPI_FNLNEMPCode=new ArrayList<TeacherPersonalInfo>();
	     	 		
	     	 		lstTPI_FNLNEMPCode=teacherPersonalInfoDAO.getTPIEMPCode();
	     	 		lstTPI_FNLNSSN=teacherPersonalInfoDAO.getTPISSN();
	     	 		lstTPI_FNLNSSNEMPCode=teacherPersonalInfoDAO.getTPISSNEMPCode();
	     	 		
	     	 		if(lstTPI_FNLNEMPCode!=null)
	     	 		{
	     	 			for(TeacherPersonalInfo personalInfo:lstTPI_FNLNEMPCode) {
	     	 				if(personalInfo!=null) {
	     	 					String sTempMapKey="";
	     	 					
	     	 					if(personalInfo.getEmployeeNumber()!=null && !personalInfo.getEmployeeNumber().equalsIgnoreCase(""))
	     	 						sTempMapKey=sTempMapKey+"_"+personalInfo.getEmployeeNumber();
	     	 					
	     	 					if(sTempMapKey!=null && !sTempMapKey.equalsIgnoreCase(""))
	     	 					{
	     	 						sTempMapKey=sTempMapKey.toUpperCase();
	     	 						if(mapTPI.get(sTempMapKey)==null)
	     	 							mapTPI.put(sTempMapKey,personalInfo.getTeacherId());
	     	 					}
	     	 				}
	     	 			}
	     	 		}
	     	 		
	     	 		if(lstTPI_FNLNSSN!=null)
	     	 		{
	     	 			for(TeacherPersonalInfo personalInfo:lstTPI_FNLNSSN)
	     	 			{
	     	 				if(personalInfo!=null)
	     	 				{
	     	 					String sTempMapKey="",sTempSSN="";
	     	 					if(personalInfo.getFirstName()!=null && !personalInfo.getFirstName().equalsIgnoreCase(""))
	     	 						sTempMapKey=personalInfo.getFirstName();
	     	 					if(personalInfo.getLastName()!=null && !personalInfo.getLastName().equalsIgnoreCase(""))
	     	 						sTempMapKey=sTempMapKey+"_"+personalInfo.getLastName();
	     	 					
	     	 					if(personalInfo.getSSN()!=null && !personalInfo.getSSN().equalsIgnoreCase(""))
	     	 					{
	     	 						sTempSSN=Utility.decodeBase64(personalInfo.getSSN());
	     	 						String sSSN4Digits=sTempSSN.substring(sTempSSN.length()-4, sTempSSN.length());
	     	 						sTempMapKey=sTempMapKey+"_"+sSSN4Digits;
	     	 					}
	     	 					if(sTempMapKey!=null && !sTempMapKey.equalsIgnoreCase(""))
	     	 					{
	     	 						sTempMapKey=sTempMapKey.toUpperCase();
	     	 						if(mapTPI.get(sTempMapKey)==null)
	     	 							mapTPI.put(sTempMapKey,personalInfo.getTeacherId());
	     	 					}
	     	 				}
	     	 			}
	     	 		}
	     	 		
	     	 		if(lstTPI_FNLNSSNEMPCode!=null)
	     	 		{
	     	 			for(TeacherPersonalInfo personalInfo:lstTPI_FNLNSSNEMPCode)
	     	 			{
	     	 				if(personalInfo!=null)
	     	 				{
	     	 					String sTempMapKey="",sTempSSN="";
	     	 					if(personalInfo.getFirstName()!=null && !personalInfo.getFirstName().equalsIgnoreCase(""))
	     	 						sTempMapKey=personalInfo.getFirstName();
	     	 					if(personalInfo.getLastName()!=null && !personalInfo.getLastName().equalsIgnoreCase(""))
	     	 						sTempMapKey=sTempMapKey+"_"+personalInfo.getLastName();
	     	 					
	     	 					if(personalInfo.getSSN()!=null && !personalInfo.getSSN().equalsIgnoreCase(""))
	     	 					{
	     	 						sTempSSN=Utility.decodeBase64(personalInfo.getSSN());
	     	 						String sSSN4Digits=sTempSSN.substring(sTempSSN.length()-4, sTempSSN.length());
	     	 						sTempMapKey=sTempMapKey+"_"+sSSN4Digits;
	     	 					}
	     	 					
	     	 					if(personalInfo.getEmployeeNumber()!=null && !personalInfo.getEmployeeNumber().equalsIgnoreCase(""))
	     	 						sTempMapKey=sTempMapKey+"_"+personalInfo.getEmployeeNumber();
	     	 					
	     	 					if(sTempMapKey!=null && !sTempMapKey.equalsIgnoreCase(""))
	     	 					{
	     	 						sTempMapKey=sTempMapKey.toUpperCase();
	     	 						if(mapTPI.get(sTempMapKey)==null)
		     	 						mapTPI.put(sTempMapKey,personalInfo.getTeacherId());
	     	 					}
	     	 					
	     	 					
	     	 				}
	     	 			}
	     	 		}
	     	 		
	     	 		
	     	 	}
	     	 	else
	     	 	{
	     	 		System.out.println("No any JFT is updated for PC/OC");
	     	 	}
	     	 	
	     	 	StringBuffer sbEmail=new StringBuffer("");
	     	 	String sEmailSubject=MSGsdpstaffmasterFilefound16;
	     	 	sbEmail.append("<P>"+ProcessingDateis+" "+Utility.getUSformatDateTime(new Date())+"</P>"); 
	     	 	String sNoUpdatePc="<BR>"+MSGsdpstaffmasterFilefound17+"<BR>";
	     	 	
	     	 	if(bPC)
	     	 	{
	     	 		List<TeacherDetail> lstTeacherPC=new ArrayList<TeacherDetail>();
	     	 		lstTeacherPC=getTeacherList(lstEmployeeMasterForProcessPC, mapTPI);
	     	 		
	     	 		Map<Integer, EmployeeMaster> mapTIDEmpPC=new HashMap<Integer, EmployeeMaster>();
	     	 		mapTIDEmpPC=getEmployeeMasterFromTID(lstEmployeeMasterForProcessPC, mapTPI);
	     	 		
	     	 		if(lstTeacherPC!=null & lstTeacherPC.size()>0)
	     	 		{
	     	 			String statusShortNames[]={"widrw","dcln"};
			     	 	List<StatusMaster> listStatusMaster= new ArrayList<StatusMaster>();
			     	 	listStatusMaster=statusMasterDAO.findStatusByStatusByShortNames(statusShortNames);
	     	 			List<JobForTeacher> lstJFTPC=new ArrayList<JobForTeacher>();
	     	 			lstJFTPC=jobForTeacherDAO.getJFTByTeacherListPC(lstTeacherPC, districtMaster,listStatusMaster);
	     	 			if(lstJFTPC!=null && lstJFTPC.size() > 0)
	     	 			{
	     	 				List<String> lstRequisitionNumbers=new ArrayList<String>();
	     	 				for(JobForTeacher forTeacher :lstJFTPC)
	     	 				{
	     	 					if(forTeacher.getRequisitionNumber()!=null && !forTeacher.getRequisitionNumber().equals(""))
	     	 						lstRequisitionNumbers.add(forTeacher.getRequisitionNumber());
	     	 				}
	     	 				Map<String, JobRequisitionNumbers> mapReqPC = getReqMap(districtMaster, lstRequisitionNumbers);
	     	 				//String sReturnValuePC=setRejectPCOC(lstJFTPC, 0, mapReqPC, districtMaster, lstTeacherPC,mapTIDEmpPC,null);
	     	 				String sReturnValuePC= setRejectStatus(lstJFTPC, 0, mapReqPC);
	     	 				sbEmail.append(sReturnValuePC);
	     	 			}
	     	 			else
	     	 			{
	     	 				sbEmail.append(sNoUpdatePc);
	     	 			}
	     	 		}
	     	 		else
	     	 		{
	     	 			sbEmail.append(sNoUpdatePc);
	     	 		}
	     	 		
	     	 	}
	     	 	else
	     	 	{
	     	 		sbEmail.append(sNoUpdatePc);
	     	 	}
	     	 	
	     	 	String sNoUpdateOC="<BR>"+NoUpdateforOC+"<BR>";
	     	 	if(bOC)
	     	 	{
	     	 		List<TeacherDetail> lstTeacherOC=new ArrayList<TeacherDetail>();
	     	 		lstTeacherOC=getTeacherList(lstEmployeeMasterForProcessOC, mapTPI);
	     	 		
	     	 		Map<Integer, EmployeeMaster> mapTIDEmpOC=new HashMap<Integer, EmployeeMaster>();
	     	 		mapTIDEmpOC=getEmployeeMasterFromTID(lstEmployeeMasterForProcessOC, mapTPI);
	     	 		
	     	 		if(lstTeacherOC!=null & lstTeacherOC.size()>0)
	     	 		{
	     	 			String statusShortNames[]={"widrw","dcln","hird"};
			     	 	List<StatusMaster> listStatusMasterOC= new ArrayList<StatusMaster>();
			     	 	listStatusMasterOC=statusMasterDAO.findStatusByStatusByShortNames(statusShortNames);
	     	 			List<JobForTeacher> lstJFTOC=new ArrayList<JobForTeacher>();
		     	 		lstJFTOC=jobForTeacherDAO.getJFTByTeacherListOC(lstTeacherOC,districtMaster,listStatusMasterOC);
	     	 			if(lstJFTOC!=null && lstJFTOC.size() > 0)
	     	 			{
	     	 				Map<Integer, Boolean> mapFullTimeJFT=new HashMap<Integer, Boolean>();
	     	 				List<JobForTeacher> lstFullTimeJFTOC=new ArrayList<JobForTeacher>();
	     	 				lstFullTimeJFTOC=jobForTeacherDAO.getFullTimeDetla(lstTeacherOC, districtMaster);
	     	 				if(lstFullTimeJFTOC!=null && lstFullTimeJFTOC.size()>0)
	     	 					for(JobForTeacher forTeacher1 :lstFullTimeJFTOC)
	     	 						mapFullTimeJFT.put(forTeacher1.getTeacherId().getTeacherId(), true);
	     	 				
	     	 				List<String> lstRequisitionNumbers=new ArrayList<String>();
	     	 				for(JobForTeacher forTeacher :lstJFTOC)
	     	 				{
	     	 					if(forTeacher.getRequisitionNumber()!=null && !forTeacher.getRequisitionNumber().equals(""))
	     	 						lstRequisitionNumbers.add(forTeacher.getRequisitionNumber());
	     	 				}
	     	 				Map<String, JobRequisitionNumbers> mapReqOC = getReqMap(districtMaster, lstRequisitionNumbers);
	     	 				String sReturnValuePC=setRejectPCOC(lstJFTOC, 1, mapReqOC, districtMaster, lstTeacherOC,mapTIDEmpOC,mapFullTimeJFT);
	     	 				sbEmail.append(sReturnValuePC);
	     	 			}
	     	 			else
	     	 			{
	     	 				sbEmail.append(sNoUpdateOC);
	     	 			}
	     	 		}
	     	 		else
	     	 		{
	     	 			sbEmail.append(sNoUpdateOC);
	     	 		}
	     	 	}
	     	 	else
	     	 	{
	     	 		sbEmail.append(sNoUpdateOC);
	     	 	}
	     	 	
	     	 	System.out.println("End update process for PC/OC");
	     	 	
	     	 	System.out.println("Start update process for Internal or External");
	     	 	//Status
 	 			List <StatusMaster> statusMasterList=statusMasterDAO.findAllStatusMaster();
 				Map<String, StatusMaster> mapStatus = new HashMap<String, StatusMaster>();
 				for (StatusMaster statusMaster : statusMasterList) 
 					mapStatus.put(statusMaster.getStatusShortName(),statusMaster);
 				
 				String sNoUpdateExToIn="<BR>"+MSGsdpstaffmasterFilefound18+"<BR>";
	     	 	if(bExToIn)
	     	 	{
	     	 		List<TeacherDetail> lstTeacherExToIn=new ArrayList<TeacherDetail>();
	     	 		lstTeacherExToIn=getTeacherList(lstEmpMstrExToIn, mapTPI);
	     	 		
	     	 		if(lstTeacherExToIn!=null & lstTeacherExToIn.size()>0)
	     	 		{
	     	 			//EPI
	     	 			Map<Integer, TeacherAssessmentStatus> mapTAS=getMapTAS(lstTeacherExToIn);
	     	 			//TeacherPortfolioStatus
	     	 			Map<Integer, Boolean> mapTeacherPortfolioStatus=teacherPortfolioStatusDAO.findActivePortfolioStatus(lstTeacherExToIn);
	     	 			
	     	 			List<JobForTeacher> lstJFTExToIn=new ArrayList<JobForTeacher>();
	     	 			lstJFTExToIn=jobForTeacherDAO.getJFTByTeacherListExIn(lstTeacherExToIn, districtMaster);
	     	 			if(lstJFTExToIn!=null && lstJFTExToIn.size() > 0)
	     	 			{
	     	 				// Need to work for External To Internal
	     	 				String sReturnValueExToIn=processForExternalOrInternal(lstTeacherExToIn,lstJFTExToIn,mapTAS,mapTeacherPortfolioStatus,mapStatus,1,districtMaster,null,null);
	     	 				sbEmail.append(sReturnValueExToIn);
	     	 			}
	     	 			else
	     	 			{
	     	 				sbEmail.append(sNoUpdateExToIn);
	     	 			}
	     	 		}
	     	 		else
	     	 		{
	     	 			sbEmail.append(sNoUpdateExToIn);
	     	 		}
	     	 	}
	     	 	else
	     	 	{
	     	 		sbEmail.append(sNoUpdateExToIn);
	     	 	}
	     	 	
	     	 	
	     	 	
	     	 	String sNoIntToEx="<BR>"+MSGsdpstaffmasterFilefound19+"<BR>";
	     	 	if(bIntToEx)
	     	 	{
	     	 		List<TeacherDetail> lstTeacherInToEx=new ArrayList<TeacherDetail>();
	     	 		lstTeacherInToEx=getTeacherList(lstEmpMstrInToEx, mapTPI);
	     	 		
	     	 		Map<Integer, EmployeeMaster> mapTIDEmpInToEx=new HashMap<Integer, EmployeeMaster>();
	     	 		mapTIDEmpInToEx=getEmployeeMasterFromTID(lstEmpMstrInToEx, mapTPI);
	     	 		
	     	 		if(lstTeacherInToEx!=null & lstTeacherInToEx.size()>0)
	     	 		{
	     	 			//EPI
	     	 			Map<Integer, TeacherAssessmentStatus> mapTAS=getMapTAS(lstTeacherInToEx);
	     	 			//TeacherPortfolioStatus
	     	 			Map<Integer, Boolean> mapTeacherPortfolioStatus=teacherPortfolioStatusDAO.findActivePortfolioStatus(lstTeacherInToEx);
	     	 			
	     	 			List<JobForTeacher> lstJFTInToEx=new ArrayList<JobForTeacher>();
	     	 			lstJFTInToEx=jobForTeacherDAO.getJFTByTeacherListExIn(lstTeacherInToEx, districtMaster);
	     	 			if(lstJFTInToEx!=null && lstJFTInToEx.size() > 0)
	     	 			{
	     	 				// Need to work for Internal To External
	     	 				List<String> lstRequisitionNumbers=new ArrayList<String>();
	     	 				for(JobForTeacher forTeacher :lstJFTInToEx)
	     	 				{
	     	 					if(forTeacher.getRequisitionNumber()!=null && !forTeacher.getRequisitionNumber().equals(""))
	     	 						lstRequisitionNumbers.add(forTeacher.getRequisitionNumber());
	     	 				}
	     	 				Map<String, JobRequisitionNumbers> mapReqIntToEx = getReqMap(districtMaster, lstRequisitionNumbers);
	     	 				String sReturnValueIntToEx=processForExternalOrInternal(lstTeacherInToEx,lstJFTInToEx,mapTAS,mapTeacherPortfolioStatus,mapStatus,0,districtMaster,mapReqIntToEx,mapTIDEmpInToEx);
	     	 				sbEmail.append("<BR><BR>"+sReturnValueIntToEx);
	     	 			}
	     	 			else
	     	 			{
	     	 				sbEmail.append(sNoIntToEx);
	     	 			}
	     	 		}
	     	 		else
	     	 		{
	     	 			sbEmail.append(sNoIntToEx);
	     	 		}
	     	 	}
	     	 	else
	     	 	{
	     	 		sbEmail.append(sNoIntToEx);
	     	 	}
	     	 	
	     	 	String sEmailBodyText=sbEmail.toString();
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom("admin@netsutra.com");
				dsmt.setMailto("teachermatchemaillog@gmail.com");
				dsmt.setMailsubject(sEmailSubject);
				dsmt.setMailcontent(sEmailBodyText);
				
				System.out.println("sEmailSubject "+sEmailSubject);
				System.out.println("");
				System.out.println("sEmailBodyText "+sEmailBodyText);
				System.out.println("");
				
				try {
					dsmt.start();	
				} catch (Exception e) {}
				
	     	 	System.out.println("End update process for Internal or External");
	     	 	// End ... process ...
	     	 	
			}catch (Exception e){
				e.printStackTrace();
			}
				return null;
			}
		
		 public static Vector readDataExcelCSV(String fileName) throws FileNotFoundException
		 {
				
				Vector vectorData = new Vector();
				String filepathhdr=fileName;
				System.out.println("read csv");
				int rowshdr=0;
				int first_name_count=11;
	            int middle_name_count=11;
	 	        int last_name_count=11;	 	        
	 	        int employee_number_count=11;
	 	        int last4_count=11;
	 	        int pc_count=11;
	 	        int rr_count=11;
	 	        int oc_count=11;
	 	        int staff_type_count=11;
	 	        int status_count=11;
	 	        int email_count=11;
				
			        BufferedReader brhdr=new BufferedReader(new FileReader(filepathhdr));
			        
			        String strLineHdr="";	        
			        String hdrstr="";
			        
			        StringTokenizer sthdr=null;
			        StringTokenizer stdtl=null;
			        int lineNumberHdr=0;
			        try{
			            String indexCheck="";
			            while((strLineHdr=brhdr.readLine())!=null){               
			            	Vector vectorCellEachRowData = new Vector();
			                int cIndex=0;
			                boolean cellFlag=false,dateFlag=false;
			                Map<String,String> mapCell = new TreeMap<String, String>();
			                int i=1;
			                
			                String[] parts = strLineHdr.split(",");
			                
			                for(int j=0; j<parts.length; j++) {
			                	hdrstr=parts[j];
			                	cIndex=j;
			                	
		                        
			                       if(hdrstr.toString().trim().equalsIgnoreCase("first_name")){
				                    	first_name_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(first_name_count==cIndex){
					            		cellFlag=true;
					            		dateFlag=true;
					            	}
					            	
					            	if(hdrstr.toString().trim().equalsIgnoreCase("middle_name")){
					            		middle_name_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(middle_name_count==cIndex){
					            		cellFlag=true;
					            	}
					            	  
					            	if(hdrstr.toString().trim().equalsIgnoreCase("last_name")){
					            		last_name_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(last_name_count==cIndex){
					            		cellFlag=true;
					            	}
					            	
					            	if(hdrstr.toString().trim().equalsIgnoreCase("employee_number")){
					            		employee_number_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(employee_number_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("last4")){
					            		last4_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(last4_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("pc")){
					            		pc_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(pc_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("rr")){
					            		rr_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(rr_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("oc")){
					            		oc_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(oc_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("staff_type")){
					            		staff_type_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(staff_type_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("status")){
					            		status_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(status_count==cIndex){
					            		cellFlag=true;
					            	}
					            	if(hdrstr.toString().trim().equalsIgnoreCase("email")){
					            		email_count=cIndex;
					            		indexCheck+="||"+cIndex+"||";
					            	}
					            	if(email_count==cIndex){
					            		cellFlag=true;
					            	} 
			                        
			                    	if(cellFlag){
			                    		if(!dateFlag){
			                    			try{
			                    				mapCell.put(cIndex+"",hdrstr);
			                    			}catch(Exception e){
			                    				mapCell.put(cIndex+"",hdrstr);
			                    			}
			                    		}else{
			                    			mapCell.put(cIndex+"",hdrstr);
			                    		}
			                    	}
			                }
			                    
			                    vectorCellEachRowData=cellValuePopulate(mapCell);
			                    vectorData.addElement(vectorCellEachRowData);
			                lineNumberHdr++;
			            }
			            brhdr.close();
			        }
			        catch(Exception e){			        	
			            System.out.println(e.getMessage());
			        }
			        //brhdr.close();
				return vectorData;
			}
		public static Vector cellValuePopulate(Map<String, String> mapCell)
		{
			 Vector vectorCellEachRowData = new Vector();
			 Map<String,String> mapCellTemp = new TreeMap<String, String>();
			 boolean flag0=false,flag1=false,flag2=false,flag3=false,flag4=false,flag5=false,flag6=false,flag7=false,flag8=false,flag9=false,flag10=false;
			 for(Map.Entry<String, String> entry : mapCell.entrySet()){
				 String key=entry.getKey();
				String cellValue=null;
				if(entry.getValue()!=null)
					cellValue=entry.getValue().trim();
				
				if(key.equals("0")){
					mapCellTemp.put(key, cellValue);
					flag0=true;
				}
				if(key.equals("1")){
					flag1=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("2")){
					flag2=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("3")){
					flag3=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("4")){
					flag4=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("5")){
					flag5=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("6")){
					flag6=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("7")){
					flag7=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("8")){
					flag8=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("9")){
					flag9=true;
					mapCellTemp.put(key, cellValue);
				}
				if(key.equals("10")){
					flag10=true;
					mapCellTemp.put(key, cellValue);
				}
			 }
			 if(flag0==false){
				 mapCellTemp.put(0+"", "");
			 }
			 if(flag1==false){
				 mapCellTemp.put(1+"", "");
			 }
			 if(flag2==false){
				 mapCellTemp.put(2+"", "");
			 }
			 if(flag3==false){
				 mapCellTemp.put(3+"", "");
			 }
			 if(flag4==false){
				 mapCellTemp.put(4+"", "");
			 }
			 if(flag5==false){
				 mapCellTemp.put(5+"", "");
			 }
			 if(flag6==false){
				 mapCellTemp.put(6+"", "");
			 }
			 if(flag7==false){
				 mapCellTemp.put(7+"", "");
			 }
			 if(flag8==false){
				 mapCellTemp.put(8+"", "");
			 }
			 if(flag9==false){
				 mapCellTemp.put(9+"", "");
			 }
			 if(flag10==false){
				 mapCellTemp.put(10+"", "");
			 }
			 			 
			 for(Map.Entry<String, String> entry : mapCellTemp.entrySet()){
				 vectorCellEachRowData.addElement(entry.getValue());
			 }
			 return vectorCellEachRowData;
}
		
		public String deleteXlsAndXlsxFile(String fileName)
		{
			
			String filePath="";
			File file=null;
			String returnDel="";
			try{			
				filePath=fileName;
				file = new File(filePath);
				file.delete();
				returnDel="deleted";
			}catch(Exception e){}
			return returnDel;
		}
		
		public static Date getCurrentDateFormart2(String oldDateString) throws ParseException
		{

			String OLD_FORMAT = "yyyy-MM-dd HH:mm:ss";
			String NEW_FORMAT = "yyyy-MM-dd HH:mm:ss";
			String newDateString;
			SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
			Date d = sdf.parse(oldDateString);
			sdf.applyPattern(NEW_FORMAT);
			newDateString = sdf.format(d);

			SimpleDateFormat stodate = new SimpleDateFormat(NEW_FORMAT);
			Date date = stodate.parse(newDateString);

			return date;
		}
		

		public List<TeacherDetail> getTeacherList(List<EmployeeMaster> lstEmployeeMaster,Map<String, Integer> mapTPI)
		{
			List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
			for(EmployeeMaster employeeMaster:lstEmployeeMaster)
     	 	{
     	 		if(employeeMaster!=null)
     	 		{
     	 			String sTempMapKey1="",sTempMapKey2="";
     	 			if(employeeMaster.getEmployeeCode()!=null && !employeeMaster.getEmployeeCode().equalsIgnoreCase("")){
 						sTempMapKey2=sTempMapKey1+"_"+employeeMaster.getEmployeeCode();
     	 			} else {
     	 				if(employeeMaster.getFirstName()!=null && !employeeMaster.getFirstName().equalsIgnoreCase(""))
	 						sTempMapKey1=employeeMaster.getFirstName();
	 					if(employeeMaster.getLastName()!=null && !employeeMaster.getLastName().equalsIgnoreCase(""))
	 						sTempMapKey1=sTempMapKey1+"_"+employeeMaster.getLastName();
	 					if(employeeMaster.getSSN()!=null && !employeeMaster.getSSN().equalsIgnoreCase(""))
	 						sTempMapKey1=sTempMapKey1+"_"+employeeMaster.getSSN();
	 					if(employeeMaster.getEmployeeCode()!=null && !employeeMaster.getEmployeeCode().equalsIgnoreCase(""))
	 						sTempMapKey2=sTempMapKey1+"_"+employeeMaster.getEmployeeCode();
     	 			}
     	 			
     	 			if(sTempMapKey1!=null && !sTempMapKey1.equalsIgnoreCase(""))
	 					sTempMapKey1=sTempMapKey1.toUpperCase();
	 					
	 				if(sTempMapKey2!=null && !sTempMapKey2.equalsIgnoreCase(""))
	 					sTempMapKey2=sTempMapKey2.toUpperCase();
	 				
     	 			Integer teacherID=null;
     	 			if(sTempMapKey1!=null && !sTempMapKey1.equalsIgnoreCase(""))
     	 			{
     	 				if(mapTPI!=null)
     	 				{
     	 					if(mapTPI.get(sTempMapKey1)!=null)
     	 						teacherID=mapTPI.get(sTempMapKey1);
     	 					else if(sTempMapKey2!=null && !sTempMapKey2.equalsIgnoreCase(""))
    	     	 			{
     	 						if(mapTPI.get(sTempMapKey2)!=null)
	     	 						teacherID=mapTPI.get(sTempMapKey2);
    	     	 			}
     	 					
     	 					if(teacherID!=null)
 	 						{
 	 							TeacherDetail detail=new TeacherDetail();
 	 							detail.setTeacherId(teacherID);
 	 							lstTeacherDetails.add(detail);
 	 						}
     	 				}
     	 			}
     	 			
     	 		}
     	 	}
			return lstTeacherDetails;
		}
		
		public String processForExternalOrInternal(List<TeacherDetail> teacherDetails, List<JobForTeacher> lstJFT,Map<Integer, TeacherAssessmentStatus> mapTAS,Map<Integer, Boolean> mapTeacherPortfolioStatus,Map<String, StatusMaster> mapStatus,int isAffilated,DistrictMaster districtMaster,Map<String, JobRequisitionNumbers> listOfReqMap,Map<Integer, EmployeeMaster> mapTIDEmpInToEx)
		{
			System.out.println("Start Calling processForExternalOrInternal "+isAffilated);
			StringBuffer sb=new StringBuffer("");
			if(lstJFT!=null && lstJFT.size() > 0)
	 		{
				SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		 	    Transaction txOpen =statelesSsession.beginTransaction();
		 	    
	 			//JSI
	 			List<JobOrder> jobOrders=new ArrayList<JobOrder>();
	 			for(JobForTeacher jobForTeacher:lstJFT)
	 				jobOrders.add(jobForTeacher.getJobId());
	 			
	 			TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
	 			Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
	 			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
	 			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>(); 
	 			List<AssessmentJobRelation> assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobOrders);
	 			List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
	 			if(assessmentJobRelations1.size()>0){
	 				for(AssessmentJobRelation ajr: assessmentJobRelations1){
	 					mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
	 					adList.add(ajr.getAssessmentId());
	 				}
	 				if(adList.size()>0)
	 				lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentListAndTIDS(teacherDetails,adList);
	 			}
	 			for(TeacherAssessmentStatus ts : lstJSI){
	 				mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
	 			}
	 			//History Hire Status
	 			List<TeacherStatusHistoryForJob> hiredTSHFJList = new ArrayList<TeacherStatusHistoryForJob>();
	 			hiredTSHFJList=teacherStatusHistoryForJobDAO.findHireTeachersWithDistrictListAndTeacherList(teacherDetails, districtMaster);
	 			Map<String,TeacherStatusHistoryForJob> mapTSHFJ=new HashMap<String, TeacherStatusHistoryForJob>();
				if(hiredTSHFJList!=null && hiredTSHFJList.size() > 0)
				{
					for(TeacherStatusHistoryForJob tSHObj : hiredTSHFJList)
						if(tSHObj!=null)
						{
							mapTSHFJ.put(tSHObj.getTeacherDetail().getTeacherId()+"_"+tSHObj.getJobOrder().getJobId(),tSHObj);
						}
				}
	 			//
				UserMaster userMaster=new UserMaster();
		 	    userMaster.setUserId(1);
	 			
	 			CandidateGridService cgService=new CandidateGridService();
	 			
	 			if(isAffilated==0)
		 	   		sb.append("<BR>"+MSGsdpstaffmasterFilefound20+"</BR>");
		 	   	else if(isAffilated==1 || isAffilated==3)
		 	   		sb.append("<BR>"+MSGsdpstaffmasterFilefound21+"</BR>");
	 			
	 		   sb.append("<Table border='1' cellpadding='5' cellspacing='0'>");
		 	   sb.append("<TR>");
		 	   sb.append("<TD>SNo.</TD>");
		 	   sb.append("<TD>"+LBLJobForTeacherId1+"</TD>");
		 	   sb.append("<TD>"+LBLTeacherId1+"</TD>");
		 	   sb.append("<TD>"+EmailAddressD+"</TD>");
		 	   sb.append("<TD>"+LBLJobDetails1+"</TD>");
		 	   sb.append("<TD>"+lblNotes+"</TD>");
		 	   sb.append("</TR>");
			 	   
		 	   int iCounter=0;
		 	   
		 	   Map<Integer, SecondaryStatus> mapSecStJID=new HashMap<Integer, SecondaryStatus>();
		 	   Map<Integer, SecondaryStatus> mapSecStSID=new HashMap<Integer, SecondaryStatus>();
		 	   if(isAffilated==0 && districtMaster.getDistrictId()==1200390)
		 	   {
		 		  List<SecondaryStatus> lstSecondaryStatus=new ArrayList<SecondaryStatus>();
		 		  lstSecondaryStatus=secondaryStatusDAO.getSSDelta(districtMaster, "Ofcl Trans / Veteran Pref");
		 		  for(SecondaryStatus ss :lstSecondaryStatus)
		 		  {
		 			 mapSecStJID.put(ss.getJobCategoryMaster().getJobCategoryId(), ss);
		 			 mapSecStSID.put(ss.getSecondaryStatusId(), ss);
		 		  }
		 	   }
		 	   
		 	   	List<JobOrder> lstJobsPanel=new ArrayList<JobOrder>();
		 	   	List<TeacherDetail> lstTeachersPanel=new ArrayList<TeacherDetail>();
		 	   	Map<Integer, JobWisePanelStatus> mapJobWisePanelStatus=new HashMap<Integer, JobWisePanelStatus>();
	 			for(JobForTeacher jobForTeacher:lstJFT)
	 			{
	 				lstJobsPanel.add(jobForTeacher.getJobId());
	 				lstTeachersPanel.add(jobForTeacher.getTeacherId());
	 			}
	 			List<JobWisePanelStatus> listJobWisePanelStatus= new ArrayList<JobWisePanelStatus>();
	 			listJobWisePanelStatus=jobWisePanelStatusDAO.findJobWisePanelStatusList(lstJobsPanel);
	 			
	 			for(JobWisePanelStatus jwp:listJobWisePanelStatus)
	 				mapJobWisePanelStatus.put(jwp.getJobOrder().getJobId(),jwp);
	 			
	 			Map<String, PanelSchedule> mapPanelSchedule=new HashMap<String, PanelSchedule>();
	 			List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();
	 			panelScheduleList=panelScheduleDAO.findPanelScheduleDelta(lstTeachersPanel,lstJobsPanel,listJobWisePanelStatus);
	 			
	 			for(PanelSchedule panelSchedule:panelScheduleList)
	 				mapPanelSchedule.put(panelSchedule.getJobOrder().getJobId()+"_"+panelSchedule.getTeacherDetail().getTeacherId()+"_"+panelSchedule.getJobWisePanelStatus().getJobPanelStatusId(), panelSchedule);
		 	   
		 	   	List<JobOrder> lstHireJO=new ArrayList<JobOrder>();
		 	   	List<TeacherDetail> lstHireTeacherList=new ArrayList<TeacherDetail>();
	 			for(JobForTeacher jobForTeacher:lstJFT)
	 			{
	 				String sOldStatus="",sOldSecStatus="";
	 				if(jobForTeacher.getStatus()!=null)
	 					sOldStatus=jobForTeacher.getStatus().getStatus();
	 				if(jobForTeacher.getSecondaryStatus()!=null)
	 					sOldSecStatus=jobForTeacher.getSecondaryStatus().getSecondaryStatusName();
	 				
	 				iCounter++;
	 				boolean bPortfolioStatus=false;
	 				if(mapTeacherPortfolioStatus.get(jobForTeacher.getTeacherId().getTeacherId())!=null)
	 					bPortfolioStatus=mapTeacherPortfolioStatus.get(jobForTeacher.getTeacherId().getTeacherId());
	 				
	 				TeacherAssessmentStatus teacherAssessmentStatus=null;
	 				if(mapTAS.get(jobForTeacher.getTeacherId().getTeacherId())!=null)
	 					teacherAssessmentStatus=mapTAS.get(jobForTeacher.getTeacherId().getTeacherId());
	 				
	 					StatusMaster statusMasterCIV=getLatestUpdate(jobForTeacher, mapAssess, teacherAssessmentStatusJSI, mapJSI, bPortfolioStatus, mapStatus, teacherAssessmentStatus,isAffilated);
	 				
	 					if(isAffilated==0 && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
	 					{
	 						JobForTeacherHistory copyJFT=new JobForTeacherHistory();
	 			 			 try {
	 							BeanUtils.copyProperties(copyJFT, jobForTeacher);
	 							copyJFT.setJobForTeacherId(null);
	 							copyJFT.setNotes("Hired-Internal To External");
	 						} catch (IllegalAccessException e) {
	 							e.printStackTrace();
	 						} catch (InvocationTargetException e) {
	 							e.printStackTrace();
	 						}
	 						
	 			 			 try {
	 							txOpen =statelesSsession.beginTransaction();
	 							 statelesSsession.insert(copyJFT);
	 							 txOpen.commit();
	 						} catch (HibernateException e) {
	 							e.printStackTrace();
	 						}
	 						
	 						lstHireJO.add(jobForTeacher.getJobId());
	 						lstHireTeacherList.add(jobForTeacher.getTeacherId());
		 					
		 					StatusMaster statusMaster=null;
							if(districtMaster.getDistrictId()==1200390)
							{
								String sSecStName="";
								int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
								statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
								
								jobForTeacher.setOfferReady(null);
								jobForTeacher.setOfferAccepted(null);
								jobForTeacher.setOfferMadeDate(null);
								jobForTeacher.setOfferAcceptedDate(null);
								jobForTeacher.setStatus(statusMaster);
								if(mapSecStJID.get(jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId())!=null)
								{
									SecondaryStatus secondaryStatus=mapSecStJID.get(jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId());
									jobForTeacher.setStatusMaster(null);
									jobForTeacher.setSecondaryStatus(secondaryStatus);
									sSecStName=secondaryStatus.getSecondaryStatusName();
								}
								else
								{
									jobForTeacher.setStatusMaster(statusMaster);
									jobForTeacher.setSecondaryStatus(null);
								}
								
								jobForTeacher.setUpdatedBy(userMaster);
								jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
								jobForTeacher.setUpdatedDate(new Date());
								jobForTeacher.setLastActivityDate(new Date());
								jobForTeacher.setUserMaster(userMaster);
								jobForTeacher.setSchoolMaster(null);
								jobForTeacher.setIsAffilated(isAffilated);
								
								// St ... Requisition reset
								String sRequisitionNumber="",sResetjobRequisitionNumberId="";
								if(jobForTeacher.getRequisitionNumber()!=null && jobForTeacher.getRequisitionNumber().length()>0)
								{
									sRequisitionNumber=jobForTeacher.getRequisitionNumber();
									jobForTeacher.setRequisitionNumber(null);
									if(listOfReqMap.containsKey(jobForTeacher.getJobId().getJobId()+"##"+sRequisitionNumber)){
										JobRequisitionNumbers jobRequisitionNumbers = listOfReqMap.get(jobForTeacher.getJobId().getJobId()+"##"+sRequisitionNumber);
										jobRequisitionNumbers.setStatus(0);
										
										try {
											txOpen =statelesSsession.beginTransaction();
											statelesSsession.update(jobRequisitionNumbers);
											txOpen.commit();
											sResetjobRequisitionNumberId=jobRequisitionNumbers.getJobRequisitionId()+"";
										} catch (HibernateException e) {
											e.printStackTrace();
										}
									}
									
								}
								//End ... Requisition reset
								
								txOpen =statelesSsession.beginTransaction();
								statelesSsession.update(jobForTeacher);
								txOpen.commit();
								
								// Start Marked As Alum
								String sAlumEmpId="";
								if(mapTIDEmpInToEx.get(jobForTeacher.getTeacherId().getTeacherId())!=null)
								{
									EmployeeMaster employeeMaster=mapTIDEmpInToEx.get(jobForTeacher.getTeacherId().getTeacherId());
									employeeMaster.setAlum("Y");
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.update(employeeMaster);
									txOpen.commit();
									sAlumEmpId=employeeMaster.getEmployeeId()+"";
								}
								// End Marked As Alum
								
								//Panel Cancel
								String sPanelId="";
								String sJID_TID=jobForTeacher.getJobId().getJobId()+"_"+jobForTeacher.getTeacherId().getTeacherId();
								if(mapJobWisePanelStatus!=null && mapJobWisePanelStatus.get(jobForTeacher.getJobId().getJobId())!=null)
								{
									JobWisePanelStatus jobWisePanelStatus= mapJobWisePanelStatus.get(jobForTeacher.getJobId().getJobId());
									if(jobWisePanelStatus!=null)
									{
										sJID_TID=sJID_TID+"_"+jobWisePanelStatus.getJobPanelStatusId();
										if(mapPanelSchedule!=null && mapPanelSchedule.get(sJID_TID)!=null)
										{
											PanelSchedule panelSchedule=mapPanelSchedule.get(sJID_TID);
											if(panelSchedule!=null)
											{
												panelSchedule.setPanelStatus("Cancelled");
												txOpen =statelesSsession.beginTransaction();
												statelesSsession.update(panelSchedule);
												txOpen.commit();
												sPanelId=panelSchedule.getPanelId()+"";
											}
										}
									}
								}
								
								sb.append("<TR>");
								sb.append("<TD>"+iCounter+"</TD>");
						 	   	sb.append("<TD>"+jobForTeacher.getJobForTeacherId()+"</TD>");
						 	   	sb.append("<TD>"+jobForTeacher.getTeacherId().getTeacherId()+"</TD>");
						 	   	sb.append("<TD>"+jobForTeacher.getTeacherId().getEmailAddress()+"</TD>");
						 	   	sb.append("<TD>"+jobForTeacher.getJobId()+"</TD>");
						 	   	
						 	   	String sNotes=LBLInternalToExternal1;
						 	   	if(sOldStatus!=null && !sOldStatus.equals(""))
						 	   		sNotes=sNotes+", "+LBLOldStatus1+sOldStatus;
						 	   	if(sOldSecStatus!=null && !sOldSecStatus.equals(""))
						 	   		sNotes=sNotes+",  "+LBLOldSecStatus1+sOldSecStatus;
						 	   	if(statusMaster.getStatus()!=null && !statusMaster.getStatus().equals(""))
						 	   		sNotes=sNotes+",  "+LBLNewStatus1+statusMaster.getStatus();
						 	   	if(sSecStName!=null && !sSecStName.equals(""))
						 	   		sNotes=sNotes+", "+LBLNewSecStName1+sSecStName;
						 	   	if(sRequisitionNumber!=null && !sRequisitionNumber.equals(""))
						 	   		sNotes=sNotes+",  "+LBLOldRequisitionNumber1+sRequisitionNumber;
						 	   	if(sResetjobRequisitionNumberId!=null && !sResetjobRequisitionNumberId.equals(""))
						 	   		sNotes=sNotes+",  "+LBLResetJobRequisitionNumberId1+sResetjobRequisitionNumberId;
						 	   	if(sAlumEmpId!=null && !sAlumEmpId.equals(""))
						 	   		sNotes=sNotes+",  "+lblAlumEmpId1+sAlumEmpId;
						 		if(sPanelId!=null && !sPanelId.equals(""))
						 	   		sNotes=sNotes+",  "+lblCancelPanelId1+sPanelId;
						 	   	sb.append("<TD>"+sNotes+"</TD>");
						 	   
						 	   	sb.append("</TR>");
						 	   	
							}
							
	 					}
	 					else
	 					{
	 						String sStatusShortName="";
		 					jobForTeacher.setIsAffilated(isAffilated);
		 					if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("icomp") || jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("vlt"))
		 					{
		 						jobForTeacher.setStatus(statusMasterCIV);
			 					if(jobForTeacher.getSecondaryStatus()!=null && cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),jobForTeacher.getSecondaryStatus()))
									jobForTeacher.setStatusMaster(null);
								else
									jobForTeacher.setStatusMaster(statusMasterCIV);
			 					
			 					sStatusShortName=statusMasterCIV.getStatusShortName();
		 					}
							txOpen =statelesSsession.beginTransaction();
							statelesSsession.update(jobForTeacher);
							txOpen.commit();
							
							sb.append("<TR>");
							sb.append("<TD>"+iCounter+"</TD>");
					 	   	sb.append("<TD>"+jobForTeacher.getJobForTeacherId()+"</TD>");
					 	   	sb.append("<TD>"+jobForTeacher.getTeacherId().getTeacherId()+"</TD>");
					 	   	sb.append("<TD>"+jobForTeacher.getTeacherId().getEmailAddress()+"</TD>");
					 	   	sb.append("<TD>"+jobForTeacher.getJobId()+"</TD>");
					 	   	
					 	   sb.append("<TD>");
					 	   	if(isAffilated==0)
					 	   		sb.append(LBLInternalToExternal1);
					 	   	else if(isAffilated==1 || isAffilated==3)
					 	   		sb.append(msgRejectedTheCandidate9);
					 	   	
					 	   	if(!sStatusShortName.equalsIgnoreCase(""))
					 	   		sb.append(" , "+sStatusShortName);
					 	   sb.append("</TD>");
					 	   
					 	   	sb.append("</TR>");
					 	   	
	 					}
	 			}
	 			
	 			//copy and delete
				System.out.println("lstHireTeacherList "+lstHireTeacherList.size());
				System.out.println("lstHireJO "+lstHireJO.size());
				if(isAffilated==0 && districtMaster.getDistrictId()==1200390)
				{
		 		   List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs=new ArrayList<TeacherStatusHistoryForJob>();
		 		   List<TeacherStatusNotes> teacherStatusNotes=new ArrayList<TeacherStatusNotes>();
		 		   List<TeacherStatusScores> teacherStatusScores=new ArrayList<TeacherStatusScores>();
		 		   List<JobWiseConsolidatedTeacherScore> consolidatedTeacherScores=new ArrayList<JobWiseConsolidatedTeacherScore>();
		 		   
		 		   if(lstHireTeacherList!=null && lstHireTeacherList.size()>0)
		 			   teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.getTSHDelta(lstHireTeacherList, districtMaster,lstHireJO);
		 		   if(teacherStatusHistoryForJobs!=null && teacherStatusHistoryForJobs.size() >0)
		 		   {
		 			  for(TeacherStatusHistoryForJob tsh:teacherStatusHistoryForJobs) 
			 		   {
			 			 if(mapSecStSID==null || tsh.getSecondaryStatus()==null || mapSecStSID.get(tsh.getSecondaryStatus().getSecondaryStatusId())==null) 
			 			 {
			 				TeacherStatusHistoryForJobHistory forJobHistory=new TeacherStatusHistoryForJobHistory();
				 			 try {
								BeanUtils.copyProperties(forJobHistory, tsh);
								forJobHistory.setTeacherStatusHistoryForJobId(null);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
							
				 			 try {
								txOpen =statelesSsession.beginTransaction();
								 statelesSsession.insert(forJobHistory);
								 txOpen.commit();
							} catch (HibernateException e) {
								e.printStackTrace();
							}
				 			 
				 			 try {
								txOpen =statelesSsession.beginTransaction();
								statelesSsession.delete(tsh);
								txOpen.commit();
							} catch (HibernateException e) {
								e.printStackTrace();
							}
			 			 }
			 		  }
		 		   }
		 		  
		 		  
		 		 if(lstHireTeacherList!=null && lstHireTeacherList.size()>0)
		 			 teacherStatusNotes=teacherStatusNotesDAO.getTSNDelta(lstHireTeacherList, districtMaster,lstHireJO);
		 		 
		 		 if(teacherStatusNotes!=null && teacherStatusNotes.size()>0)
		 		 {
		 			for(TeacherStatusNotes tsn:teacherStatusNotes)
			 		 {
			 			if(mapSecStSID==null || tsn.getSecondaryStatus()==null || mapSecStSID.get(tsn.getSecondaryStatus().getSecondaryStatusId())==null) 
			 			 {
			 				TeacherStatusNotesHistory teacherStatusNotesHistory=new TeacherStatusNotesHistory();
				 			 try {
									BeanUtils.copyProperties(teacherStatusNotesHistory, tsn);
									teacherStatusNotesHistory.setTeacherStatusNoteId(null);
								} catch (IllegalAccessException e) {
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									e.printStackTrace();
								}
					 			 
					 			 try {
									txOpen =statelesSsession.beginTransaction();
									 statelesSsession.insert(teacherStatusNotesHistory);
									 txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
								
								 try {
										txOpen =statelesSsession.beginTransaction();
										statelesSsession.delete(tsn);
										txOpen.commit();
									} catch (HibernateException e) {
										e.printStackTrace();
									}
			 			 }
			 		 }
		 		 }
		 		 
		 		if(lstHireTeacherList!=null && lstHireTeacherList.size()>0)
		 			teacherStatusScores=teacherStatusScoresDAO.getTSSDelta(lstHireTeacherList, districtMaster,lstHireJO);
		 		if(teacherStatusScores!=null && teacherStatusScores.size()>0)
		 		{
		 			for(TeacherStatusScores tss :teacherStatusScores)
			 		{
			 			if(mapSecStSID==null || tss.getSecondaryStatus()==null || mapSecStSID.get(tss.getSecondaryStatus().getSecondaryStatusId())==null) 
			 			{
			 				TeacherStatusScoresHistory teacherStatusScoresHistory=new TeacherStatusScoresHistory();
				 			try {
								BeanUtils.copyProperties(teacherStatusScoresHistory, tss);
								teacherStatusScoresHistory.setTeacherStatusScoreId(null);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
				 			 
				 			 try {
								txOpen =statelesSsession.beginTransaction();
								 statelesSsession.insert(teacherStatusScoresHistory);
								 txOpen.commit();
							} catch (HibernateException e) {
								e.printStackTrace();
							}
							
							 try {
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.delete(tss);
									txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
			 			}
			 			
			 		}
		 		}
		 		
		 		if(lstHireTeacherList!=null && lstHireTeacherList.size()>0)
		 			consolidatedTeacherScores=jobWiseConsolidatedTeacherScoreDAO.getJCTDelta(lstHireTeacherList, districtMaster,lstHireJO);
		 		if(consolidatedTeacherScores!=null && consolidatedTeacherScores.size()>0)
		 		{
		 			for(JobWiseConsolidatedTeacherScore jwcts :consolidatedTeacherScores)
			 		{
			 				JobWiseConsolidatedTeacherScoreHistory jobWiseConsolidatedTeacherScoreHistory=new JobWiseConsolidatedTeacherScoreHistory();
				 			try {
								BeanUtils.copyProperties(jobWiseConsolidatedTeacherScoreHistory, jwcts);
								jobWiseConsolidatedTeacherScoreHistory.setTeacherConsolidatedScoreId(null);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
				 			 
				 			 try {
								txOpen =statelesSsession.beginTransaction();
								 statelesSsession.insert(jobWiseConsolidatedTeacherScoreHistory);
								 txOpen.commit();
							} catch (HibernateException e) {
								e.printStackTrace();
							}
							
							 try {
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.delete(jwcts);
									txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
			 		}
		 		}
		 		
		 		JobWiseConsolidatedTeacherScore jwScoreObj=new JobWiseConsolidatedTeacherScore();
		 		List result=null;
		 		if(lstHireTeacherList!=null && lstHireTeacherList.size()>0)
		 			result= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvgDelta(lstHireTeacherList, lstHireJO);
		 		if(result!=null)
		 		{
			 		Iterator itr = result.iterator();
					while(itr.hasNext())
					{
						Object[] obj = (Object[]) itr.next();
						JobOrder jobOrder2=(JobOrder)obj[0];
						TeacherDetail teacherDetail=(TeacherDetail)obj[1];
						Double val1= (Double) obj[2];
						Double val2= (Double) obj[3];
						jwScoreObj=new JobWiseConsolidatedTeacherScore();
						jwScoreObj.setTeacherDetail(teacherDetail);
						jwScoreObj.setJobOrder(jobOrder2);
						jwScoreObj.setJobWiseConsolidatedScore(val1);
						jwScoreObj.setJobWiseMaxScore(val2);
						jwScoreObj.setUpdatedDateTime(new Date());
						
						try {
							txOpen =statelesSsession.beginTransaction();
							statelesSsession.insert(jwScoreObj);
							txOpen.commit();
						} catch (HibernateException e) {
							e.printStackTrace();
						}
						
						System.out.println(teacherDetail.getTeacherId() +" "+jobOrder2.getJobId()+" "+jwScoreObj.getJobWiseConsolidatedScore()+"/"+jwScoreObj.getJobWiseMaxScore());
						
					}
			 	  }
				}
				
	 			statelesSsession.close();
	 			sb.append("</Table>");
	 		}
			System.out.println("End Calling processForExternalOrInternal");
			return sb.toString();
		}
		
		
		public String setRejectPCOC(List<JobForTeacher> lstJFT,int iPCOC, Map<String, JobRequisitionNumbers> listOfReqMap ,DistrictMaster districtMaster,List<TeacherDetail> lstAllTeachers,Map<Integer, EmployeeMaster> mapTIDEmpOC,Map<Integer, Boolean> mapFullTimeJFT)
		{
			System.out.println("Start Calling setRejectPCOC method "+iPCOC);
			StringBuffer sb=new StringBuffer("");
			if(lstJFT!=null && lstJFT.size() > 0)
	 		{
				SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		 	    Transaction txOpen =statelesSsession.beginTransaction();
		 	    
				UserMaster userMaster=new UserMaster();
		 	    userMaster.setUserId(1);
		 	    if(iPCOC==0)
		 	    	sb.append("<BR>"+lblPCProcessing1+"<BR>");
		 	    else
		 	    	sb.append("<BR>"+lblOCProcessing1+"<BR>");
		 	    
	 		   sb.append("<Table border='1' cellpadding='5' cellspacing='0'>");
		 	   sb.append("<TR>");
		 	   sb.append("<TD>SNo.</TD>");
		 	   sb.append("<TD>"+LBLJobForTeacherId1+"</TD>");
		 	   sb.append("<TD>"+LBLTeacherId1+"</TD>");
		 	   sb.append("<TD>"+EmailAddressD+"</TD>");
		 	   sb.append("<TD>"+LBLJobDetails1+"</TD>");
		 	   sb.append("<TD>"+lblNotes+"</TD>");
		 	   sb.append("</TR>");
			 	   
		 	   int iCounter=0;
		 	   
		 	   Map<Integer, SecondaryStatus> mapSecStJID=new HashMap<Integer, SecondaryStatus>();
		 	   Map<Integer, SecondaryStatus> mapSecStSID=new HashMap<Integer, SecondaryStatus>();
		 	   List<SecondaryStatus> lstSecondaryStatus=new ArrayList<SecondaryStatus>();
		 	   if(districtMaster.getDistrictId()==1200390)
		 	   {
		 		  lstSecondaryStatus=secondaryStatusDAO.getSSDelta(districtMaster, "Ofcl Trans / Veteran Pref");
		 		  for(SecondaryStatus ss :lstSecondaryStatus)
		 		  {
		 			 mapSecStJID.put(ss.getJobCategoryMaster().getJobCategoryId(), ss);
		 			 mapSecStSID.put(ss.getSecondaryStatusId(), ss);
		 		  }
		 	   }
		 	   
		 	  Map<String, SecondaryStatus> mapSecStSIDAll=new HashMap<String, SecondaryStatus>(); 
		 	  List<TeacherStatusHistoryForJob> lstTSHJSecSt=new ArrayList<TeacherStatusHistoryForJob>();
		 	  lstTSHJSecSt=teacherStatusHistoryForJobDAO.getAllTSHDelta(lstAllTeachers, districtMaster, lstSecondaryStatus);
		 	  for(TeacherStatusHistoryForJob tsh :lstTSHJSecSt)
	 		  {
		 		  if(tsh.getSecondaryStatus()!=null)
		 			 mapSecStSIDAll.put(tsh.getTeacherDetail().getTeacherId()+"_"+tsh.getJobOrder().getJobId()+"_"+tsh.getSecondaryStatus().getSecondaryStatusId(), tsh.getSecondaryStatus());
	 		  }
	 		  
		 	  
		 	  	List<JobOrder> lstJobsPanel=new ArrayList<JobOrder>();
		 	   	List<TeacherDetail> lstTeachersPanel=new ArrayList<TeacherDetail>();
		 	   	Map<Integer, JobWisePanelStatus> mapJobWisePanelStatus=new HashMap<Integer, JobWisePanelStatus>();
	 			for(JobForTeacher jobForTeacher:lstJFT)
	 			{
	 				lstJobsPanel.add(jobForTeacher.getJobId());
	 				lstTeachersPanel.add(jobForTeacher.getTeacherId());
	 			}
	 			List<JobWisePanelStatus> listJobWisePanelStatus= new ArrayList<JobWisePanelStatus>();
	 			listJobWisePanelStatus=jobWisePanelStatusDAO.findJobWisePanelStatusList(lstJobsPanel);
	 			for(JobWisePanelStatus jwp:listJobWisePanelStatus)
	 				mapJobWisePanelStatus.put(jwp.getJobOrder().getJobId(),jwp);
	 			
	 			Map<String, PanelSchedule> mapPanelSchedule=new HashMap<String, PanelSchedule>();
	 			List<PanelSchedule> panelScheduleList= new ArrayList<PanelSchedule>();
	 			panelScheduleList=panelScheduleDAO.findPanelScheduleDelta(lstTeachersPanel,lstJobsPanel,listJobWisePanelStatus);
	 			for(PanelSchedule panelSchedule:panelScheduleList)
	 				mapPanelSchedule.put(panelSchedule.getJobOrder().getJobId()+"_"+panelSchedule.getTeacherDetail().getTeacherId()+"_"+panelSchedule.getJobWisePanelStatus().getJobPanelStatusId(), panelSchedule);
	 				
		 	   	List<JobOrder> lstJobs=new ArrayList<JobOrder>();
		 	   	List<TeacherDetail> lstTeachers=new ArrayList<TeacherDetail>();
		 	   	
		 	   //SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
		 	   	
	 			for(JobForTeacher jobForTeacher:lstJFT)
	 			{
	 				
	 				String sOldStatus="",sOldSecStatus="";
	 				if(jobForTeacher.getStatus()!=null)
	 					sOldStatus=jobForTeacher.getStatus().getStatus();
	 				if(jobForTeacher.getSecondaryStatus()!=null)
	 					sOldSecStatus=jobForTeacher.getSecondaryStatus().getSecondaryStatusName();
	 				
	 				JobForTeacherHistory copyJFT=new JobForTeacherHistory();
		 			 try {
						BeanUtils.copyProperties(copyJFT, jobForTeacher);
						copyJFT.setJobForTeacherId(null);
						if(iPCOC==0)
							copyJFT.setNotes("PC");
				 	    else
				 	    	copyJFT.setNotes("OC");
						
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
					
		 			 try {
						txOpen =statelesSsession.beginTransaction();
						 statelesSsession.insert(copyJFT);
						 txOpen.commit();
					} catch (HibernateException e) {
						e.printStackTrace();
					}
					
	 				String sStatusName="",sSecStatusName="";
	 				iCounter++;
					lstJobs.add(jobForTeacher.getJobId());
					lstTeachers.add(jobForTeacher.getTeacherId());
		 			
					boolean bHired=false;
					boolean bHiredForFullTimeJob=false;
					if(jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
					{
						bHired=true;
						if(mapFullTimeJFT!=null && mapFullTimeJFT.get(jobForTeacher.getTeacherId().getTeacherId())!=null)
							bHiredForFullTimeJob=true;
					}
					
		 			StatusMaster statusMaster=null;
					int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
					statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);
								
					jobForTeacher.setOfferReady(null);
					jobForTeacher.setOfferAccepted(null);
					jobForTeacher.setOfferMadeDate(null);
					jobForTeacher.setOfferAcceptedDate(null);
					jobForTeacher.setStatus(statusMaster);
					sStatusName=statusMaster.getStatus();
					
					boolean bSecStatus=false;
					SecondaryStatus secondaryStatus=null;
					if(mapSecStJID.get(jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId())!=null)
					{
						secondaryStatus=mapSecStJID.get(jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId());
						String sKey=jobForTeacher.getTeacherId().getTeacherId()+"_"+jobForTeacher.getJobId().getJobId()+"_"+secondaryStatus.getSecondaryStatusId();
						if(mapSecStSIDAll.get(sKey)!=null)
							bSecStatus=true;
					}
					
					if(bSecStatus)
					{
						jobForTeacher.setStatusMaster(null);
						jobForTeacher.setSecondaryStatus(secondaryStatus);
						sSecStatusName=secondaryStatus.getSecondaryStatusName();
					}
					else
					{
						jobForTeacher.setStatusMaster(statusMaster);
						jobForTeacher.setSecondaryStatus(null);
					}
					
					jobForTeacher.setUpdatedBy(userMaster);
					jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
					jobForTeacher.setUpdatedDate(new Date());
					jobForTeacher.setLastActivityDate(new Date());
					jobForTeacher.setUserMaster(userMaster);
					jobForTeacher.setSchoolMaster(null);
					
					// Start ... Requisition reset
					String sRequisitionNumber="",sResetjobRequisitionNumberId="";
					if(jobForTeacher.getRequisitionNumber()!=null && jobForTeacher.getRequisitionNumber().length()>0)
					{
						sRequisitionNumber=jobForTeacher.getRequisitionNumber();
						jobForTeacher.setRequisitionNumber(null);
						if(listOfReqMap.containsKey(jobForTeacher.getJobId().getJobId()+"##"+sRequisitionNumber)){
							JobRequisitionNumbers jobRequisitionNumbers = listOfReqMap.get(jobForTeacher.getJobId().getJobId()+"##"+sRequisitionNumber);
							jobRequisitionNumbers.setStatus(0);
							try {
								txOpen =statelesSsession.beginTransaction();
								statelesSsession.update(jobRequisitionNumbers);
								txOpen.commit();
								sResetjobRequisitionNumberId=jobRequisitionNumbers.getJobRequisitionId()+"";
							} catch (HibernateException e) {
								e.printStackTrace();
							}
						}
						
					}
					//End ... Requisition reset
					
					txOpen =statelesSsession.beginTransaction();
					statelesSsession.update(jobForTeacher);
					txOpen.commit();
					
					// Start Marked As Alum ... 0=> PC(All) and 1=>OC(Partial)
					String sAlumEmpId="";
					if(bHired && (iPCOC==0 || (iPCOC==1 && !bHiredForFullTimeJob)))
					{
						if(mapTIDEmpOC.get(jobForTeacher.getTeacherId().getTeacherId())!=null)
						{
							EmployeeMaster employeeMaster=mapTIDEmpOC.get(jobForTeacher.getTeacherId().getTeacherId());
							employeeMaster.setAlum("Y");
							txOpen =statelesSsession.beginTransaction();
							statelesSsession.update(employeeMaster);
							txOpen.commit();
							sAlumEmpId=employeeMaster.getEmployeeId()+"";
						}
					}
					// End Marked As Alum
					
					//Panel Cancel
					String sPanelId="";
					String sJID_TID=jobForTeacher.getJobId().getJobId()+"_"+jobForTeacher.getTeacherId().getTeacherId();
					if(mapJobWisePanelStatus!=null && mapJobWisePanelStatus.get(jobForTeacher.getJobId().getJobId())!=null)
					{
						JobWisePanelStatus jobWisePanelStatus= mapJobWisePanelStatus.get(jobForTeacher.getJobId().getJobId());
						if(jobWisePanelStatus!=null)
						{
							sJID_TID=sJID_TID+"_"+jobWisePanelStatus.getJobPanelStatusId();
							if(mapPanelSchedule!=null && mapPanelSchedule.get(sJID_TID)!=null)
							{
								PanelSchedule panelSchedule=mapPanelSchedule.get(sJID_TID);
								if(panelSchedule!=null)
								{
									panelSchedule.setPanelStatus("Cancelled");
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.update(panelSchedule);
									txOpen.commit();
									sPanelId=panelSchedule.getPanelId()+"";
								}
							}
						}
					}
					
					sb.append("<TR>");
					sb.append("<TD>"+iCounter+"</TD>");
			 	   	sb.append("<TD>"+jobForTeacher.getJobForTeacherId()+"</TD>");
			 	   	sb.append("<TD>"+jobForTeacher.getTeacherId().getTeacherId()+"</TD>");
			 	   	sb.append("<TD>"+jobForTeacher.getTeacherId().getEmailAddress()+"</TD>");
			 	   	sb.append("<TD>"+jobForTeacher.getJobId()+"</TD>");
			 	   	
			 	   String sNotes="";
			 	   	if(sOldStatus!=null && !sOldStatus.equals(""))
			 	   		sNotes=sNotes+" "+LBLOldStatus1+sOldStatus;
			 	   	if(sOldSecStatus!=null && !sOldSecStatus.equals(""))
			 	   		sNotes=sNotes+",  "+LBLOldSecStatus1+sOldSecStatus;
			 	   	if(sStatusName!=null && !sStatusName.equals(""))
			 	   		sNotes=sNotes+",  "+LBLNewStatus1+sStatusName;
			 	   	if(sSecStatusName!=null && !sSecStatusName.equals(""))
			 	   		sNotes=sNotes+",  "+LBLNewSecStName1+sSecStatusName;
			 	   	if(sRequisitionNumber!=null && !sRequisitionNumber.equals(""))
			 	   		sNotes=sNotes+",  "+LBLOldRequisitionNumber1+sRequisitionNumber;
			 	   	if(sResetjobRequisitionNumberId!=null && !sResetjobRequisitionNumberId.equals(""))
			 	   		sNotes=sNotes+",  "+LBLResetJobRequisitionNumberId1+sResetjobRequisitionNumberId;
			 	   	if(sAlumEmpId!=null && !sAlumEmpId.equals(""))
			 	   		sNotes=sNotes+",  "+lblAlumEmpId1+sAlumEmpId;
			 		if(sPanelId!=null && !sPanelId.equals(""))
			 	   		sNotes=sNotes+",  "+lblCancelPanelId1+sPanelId;
			 	   	sb.append("<TD>"+sNotes+"</TD>");
			 	   	sb.append("</TR>");
	 			}
	 			
	 			//copy and delete
				if(districtMaster.getDistrictId()==1200390)
				{
		 		   List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs=new ArrayList<TeacherStatusHistoryForJob>();
		 		   List<TeacherStatusNotes> teacherStatusNotes=new ArrayList<TeacherStatusNotes>();
		 		   List<TeacherStatusScores> teacherStatusScores=new ArrayList<TeacherStatusScores>();
		 		   List<JobWiseConsolidatedTeacherScore> consolidatedTeacherScores=new ArrayList<JobWiseConsolidatedTeacherScore>();
		 		   
		 		   System.out.println("####### lstJobs ######### "+lstJobs.size());
		 		   
		 		  teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.getTSHDelta(lstTeachers, districtMaster,lstJobs);
		 		  if(teacherStatusHistoryForJobs!=null && teacherStatusHistoryForJobs.size()>0)
		 		  {
		 			 for(TeacherStatusHistoryForJob tsh:teacherStatusHistoryForJobs) 
			 		  {
			 			 if(mapSecStSID==null || tsh.getSecondaryStatus()==null || mapSecStSID.get(tsh.getSecondaryStatus().getSecondaryStatusId())==null) 
			 			 {
			 				TeacherStatusHistoryForJobHistory forJobHistory=new TeacherStatusHistoryForJobHistory();
				 			 try {
								BeanUtils.copyProperties(forJobHistory, tsh);
								forJobHistory.setTeacherStatusHistoryForJobId(null);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
							
				 			 try {
								txOpen =statelesSsession.beginTransaction();
								 statelesSsession.insert(forJobHistory);
								 txOpen.commit();
							} catch (HibernateException e) {
								e.printStackTrace();
							}
				 			 
				 			 try {
								txOpen =statelesSsession.beginTransaction();
								statelesSsession.delete(tsh);
								txOpen.commit();
							} catch (HibernateException e) {
								e.printStackTrace();
							}
			 			 }
			 		  }  
		 		  }
		 		  
		 		  
		 		 teacherStatusNotes=teacherStatusNotesDAO.getTSNDelta(lstTeachers, districtMaster,lstJobs);
		 		 if(teacherStatusNotes!=null && teacherStatusNotes.size()>0)
		 		 {
		 			for(TeacherStatusNotes tsn:teacherStatusNotes)
			 		 {
			 			if(mapSecStSID==null || tsn.getSecondaryStatus()==null || mapSecStSID.get(tsn.getSecondaryStatus().getSecondaryStatusId())==null) 
			 			 {
			 				TeacherStatusNotesHistory teacherStatusNotesHistory=new TeacherStatusNotesHistory();
				 			 try {
									BeanUtils.copyProperties(teacherStatusNotesHistory, tsn);
									teacherStatusNotesHistory.setTeacherStatusNoteId(null);
								} catch (IllegalAccessException e) {
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									e.printStackTrace();
								}
					 			 
					 			 try {
									txOpen =statelesSsession.beginTransaction();
									 statelesSsession.insert(teacherStatusNotesHistory);
									 txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
								
								 try {
										txOpen =statelesSsession.beginTransaction();
										statelesSsession.delete(tsn);
										txOpen.commit();
									} catch (HibernateException e) {
										e.printStackTrace();
									}
			 			 }
			 		 } 
		 		 }
		 		 
		 		 
		 		teacherStatusScores=teacherStatusScoresDAO.getTSSDelta(lstTeachers, districtMaster,lstJobs);
		 		if(teacherStatusScores!=null && teacherStatusScores.size()>0)
		 		{
		 			for(TeacherStatusScores tss :teacherStatusScores)
			 		{
			 			if(mapSecStSID==null || tss.getSecondaryStatus()==null || mapSecStSID.get(tss.getSecondaryStatus().getSecondaryStatusId())==null) 
			 			{
			 				TeacherStatusScoresHistory teacherStatusScoresHistory=new TeacherStatusScoresHistory();
				 			try {
								BeanUtils.copyProperties(teacherStatusScoresHistory, tss);
								teacherStatusScoresHistory.setTeacherStatusScoreId(null);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
				 			 
				 			 try {
								txOpen =statelesSsession.beginTransaction();
								 statelesSsession.insert(teacherStatusScoresHistory);
								 txOpen.commit();
							} catch (HibernateException e) {
								e.printStackTrace();
							}
							
							 try {
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.delete(tss);
									txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
			 			}
			 			
			 		}
		 		}
		 		
		 		consolidatedTeacherScores=jobWiseConsolidatedTeacherScoreDAO.getJCTDelta(lstTeachers, districtMaster,lstJobs);
		 		if(consolidatedTeacherScores!=null && consolidatedTeacherScores.size()>0)
		 		{
		 			for(JobWiseConsolidatedTeacherScore jwcts :consolidatedTeacherScores)
			 		{
			 				JobWiseConsolidatedTeacherScoreHistory jobWiseConsolidatedTeacherScoreHistory=new JobWiseConsolidatedTeacherScoreHistory();
				 			try {
								BeanUtils.copyProperties(jobWiseConsolidatedTeacherScoreHistory, jwcts);
								jobWiseConsolidatedTeacherScoreHistory.setTeacherConsolidatedScoreId(null);
							} catch (IllegalAccessException e) {
								e.printStackTrace();
							} catch (InvocationTargetException e) {
								e.printStackTrace();
							}
				 			 
				 			 try {
								txOpen =statelesSsession.beginTransaction();
								 statelesSsession.insert(jobWiseConsolidatedTeacherScoreHistory);
								 txOpen.commit();
							} catch (HibernateException e) {
								e.printStackTrace();
							}
							
							 try {
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.delete(jwcts);
									txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
			 		}
		 		}
		 		
		 		/*
		 		 * 	Remove data from table "sapcandidatedetails" and copy to "sapcandidatedetailshistory" 
		 		 * */
		 		List <SapCandidateDetails> sapCandidateDetailsList = sapCandidateDetailsDAO.checkSAPdataByTeacherAndJobList(lstTeachers,lstJobs);
		 		System.out.println(":::::::::::::::: sapCandidateDetailsList ::::::::::::::::::::::"+sapCandidateDetailsList.size());
		 		if(sapCandidateDetailsList !=null && sapCandidateDetailsList.size()>0){
		 			for(SapCandidateDetails sapCandidateDetails : sapCandidateDetailsList){
		 				SapCandidateDetailsHistory copySapHistory = new SapCandidateDetailsHistory();
		 				try{
							BeanUtils.copyProperties(copySapHistory, sapCandidateDetails);
							copySapHistory.setSapCandidateId(null);
							copySapHistory.setHistoryDate(new Date());
						} catch (IllegalAccessException e) {
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							e.printStackTrace();
						}
						// Copy Record
						try {
							 txOpen =statelesSsession.beginTransaction();
							 statelesSsession.insert(copySapHistory);
							 txOpen.commit();
						} catch (HibernateException e) {
							e.printStackTrace();
						}
						// Delete Record
						try {
							txOpen =statelesSsession.beginTransaction();
							statelesSsession.delete(sapCandidateDetails);
							txOpen.commit();
						} catch (HibernateException e) {
							e.printStackTrace();
						}
		 			}
		 		}
		 		
		 		/*
		 		 * 	END SECTION
		 		 * 
		 		 * */
		 		
		 		
		 		JobWiseConsolidatedTeacherScore jwScoreObj=new JobWiseConsolidatedTeacherScore();
		 		List result= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvgDelta(lstTeachers, lstJobs);
		 		if(result!=null)
		 		{
			 		Iterator itr = result.iterator();
					while(itr.hasNext())
					{
						Object[] obj = (Object[]) itr.next();
						JobOrder jobOrder2=(JobOrder)obj[0];
						TeacherDetail teacherDetail=(TeacherDetail)obj[1];
						Double val1= (Double) obj[2];
						Double val2= (Double) obj[3];
						
						jwScoreObj.setTeacherDetail(teacherDetail);
						jwScoreObj.setJobOrder(jobOrder2);
						jwScoreObj.setJobWiseConsolidatedScore(val1);
						jwScoreObj.setJobWiseMaxScore(val2);
						jwScoreObj.setUpdatedDateTime(new Date());
						
						try {
							txOpen =statelesSsession.beginTransaction();
							statelesSsession.insert(jwScoreObj);
							txOpen.commit();
						} catch (HibernateException e) {
							e.printStackTrace();
						}
						System.out.println(teacherDetail.getTeacherId() +" "+jobOrder2.getJobId()+" "+jwScoreObj.getJobWiseConsolidatedScore()+"/"+jwScoreObj.getJobWiseMaxScore());
					}
			 	  }
				}
				
	 			statelesSsession.close();
	 			sb.append("</Table>");
	 		}
			System.out.println("Start Calling setRejectPCOC method ");
			
			return sb.toString();
		}
		
		public Map<Integer, TeacherAssessmentStatus> getMapTAS(List<TeacherDetail> lstTeacherDetails)
		{
			Map<Integer, TeacherAssessmentStatus> mapTAS=new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstTAS = new ArrayList<TeacherAssessmentStatus>();
 			lstTAS=teacherAssessmentStatusDAO.findByAllTeacherAssessStatusByTeacher(lstTeacherDetails);
 			if(lstTAS!=null && lstTAS.size()>0)
 			{
 				for(TeacherAssessmentStatus teacherAssessmentStatus:lstTAS)
 				{
					if(teacherAssessmentStatus!=null)
					{
						if(mapTAS.get(teacherAssessmentStatus.getTeacherDetail().getTeacherId())==null)
							mapTAS.put(teacherAssessmentStatus.getTeacherDetail().getTeacherId(),teacherAssessmentStatus);
					}
 				}
 			}
 			return mapTAS;
		}
		
		public StatusMaster getLatestUpdate(JobForTeacher jobForTeacher,Map<Integer, Integer> mapAssess,TeacherAssessmentStatus teacherAssessmentStatusJSI,Map<Integer, TeacherAssessmentStatus>mapJSI,boolean tPortfolioStatus,Map<String, StatusMaster>mapStatus,TeacherAssessmentStatus teacherBaseAssessmentStatus,int isAffilated) 
		{
			
			jobForTeacher.setIsAffilated(isAffilated);
			TeacherDetail teacherDetail = jobForTeacher.getTeacherId();
			JobOrder jobOrder =  jobForTeacher.getJobId();
			StatusMaster statusMaster =  mapStatus.get("icomp");
			try{
				if(mapAssess!=null){
					Integer assessmentId=mapAssess.get(jobForTeacher.getJobId().getJobId());
					teacherAssessmentStatusJSI = mapJSI.get(assessmentId);
				}
				 boolean epiInternal=false;
				 int isPortfolio=0;
				 if(jobOrder.getIsPortfolioNeeded()!=null){
					 if(jobOrder.getIsPortfolioNeeded())
						 isPortfolio=1;
				 }
				int offerEPI=0;
				int offerJSI=0;
				int internalFlag=0;
				
				try{
					 if(jobOrder.getJobCategoryMaster()!=null){
							if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()!=null){
								if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
									epiInternal=true;
								}
								if(jobForTeacher.getIsAffilated()!=null){
									 isAffilated=jobForTeacher.getIsAffilated();
								}
								if(internalFlag==0 && isAffilated==1){
									if(jobOrder.getJobCategoryMaster().getEpiForFullTimeTeachers()){
										offerEPI=1;
									}else{
										offerEPI=0;
									}
									if(jobOrder.getJobCategoryMaster().getOfferJSI()!=0){
										offerJSI=1;
									}else{
										offerJSI=0;
									}
									if(jobOrder.getJobCategoryMaster().getOfferPortfolioNeeded()){
										isPortfolio=1;
									}else{
										isPortfolio=0;
									}
								}
							}
					 }
				}catch(Exception e){
					e.printStackTrace();
				}
				if(jobForTeacher.getIsAffilated()!=null){
					 if(epiInternal==false){
						 isAffilated=jobForTeacher.getIsAffilated();
					 }else{
						 isAffilated=0;
					 }
				}
				
				if((teacherDetail.getIsPortfolioNeeded()) && !tPortfolioStatus && internalFlag==0 && isPortfolio==1)
				{
					statusMaster = mapStatus.get("icomp");
				}else{
					if(isPortfolio==0 || (isPortfolio==1 && tPortfolioStatus  && teacherDetail.getIsPortfolioNeeded()))
					{
						statusMaster = mapStatus.get("comp");
						if(!jobOrder.getJobCategoryMaster().getBaseStatus()  || (internalFlag==1 && offerEPI==0 )||(internalFlag==0 && offerEPI==0 && isAffilated==1)){
							if(jobOrder.getIsJobAssessment() && ((offerJSI==1 && internalFlag==1 )||(internalFlag==0 && isAffilated==1 && offerJSI==1)|| (internalFlag==0 && isAffilated==0))){
								if(teacherAssessmentStatusJSI!=null){
									statusMaster=teacherAssessmentStatusJSI.getStatusMaster();
								}else{
									statusMaster = mapStatus.get("icomp");
								}
							}else{
								statusMaster = mapStatus.get("comp");
							}
						}                                                                        
						else if(jobOrder.getIsJobAssessment()&& ((offerJSI==1 && internalFlag==1 )|| (internalFlag==0 && isAffilated==1 && offerJSI==1)|| (internalFlag==0 && isAffilated==0))){
							if(teacherBaseAssessmentStatus!=null){
								if(jobForTeacher.getIsAffilated()!=null && jobOrder.getJobCategoryMaster().getOfferJSI()==0 && jobForTeacher.getIsAffilated()==1)
								{
									statusMaster = mapStatus.get("comp");
								}else{
								if(teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp")){
									if(teacherAssessmentStatusJSI!=null){
										statusMaster=teacherAssessmentStatusJSI.getStatusMaster();
									}else{
										statusMaster = mapStatus.get("icomp");
									}
								}else{
									statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
								}
							}
							}else{
								statusMaster = mapStatus.get("icomp");
							}
						}
						else{
							boolean  evDistrict=false;
							try{
								if(jobOrder.getDistrictMaster()!=null){
									if(jobOrder.getDistrictMaster().getDistrictId()==5513170){
										evDistrict=true;
									}
								}
							}catch(Exception e){}
							if((isAffilated!=1 || evDistrict )&&((offerEPI==1 && internalFlag==1 )|| ( internalFlag==0 && isAffilated==0) || ( internalFlag==0 && isAffilated==1 && offerEPI==1))) //if block will execute only if Integer.parseInt(isAffilated) value is 0 or null
							{	
								if(teacherBaseAssessmentStatus!=null){
									statusMaster=teacherBaseAssessmentStatus.getStatusMaster();
								}else{
									statusMaster = mapStatus.get("icomp");
								}
							}
							else // isAffilated else block
							{
								statusMaster = mapStatus.get("comp");
							}
						}
					}
					else
					{
						statusMaster = mapStatus.get("icomp");
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			return statusMaster;
		}
		
		
		public Map<String, JobRequisitionNumbers> getReqMap(DistrictMaster districtMaster,List<String> requisitionNumbers)
		{
			Map<String, JobRequisitionNumbers> mapJobRequisitionNumbers = new HashMap<String, JobRequisitionNumbers>();
			List<DistrictRequisitionNumbers> listDistrictRequisitionNumbers =new ArrayList<DistrictRequisitionNumbers>();
			List<JobRequisitionNumbers> lstJobRequisitionNumbers = new ArrayList<JobRequisitionNumbers>();
			
			if(requisitionNumbers!=null && requisitionNumbers.size()>0)
			{
				listDistrictRequisitionNumbers=districtRequisitionNumbersDAO.getRequisitionNumbersWithAndWithoutDistrict(requisitionNumbers, districtMaster);
				if(listDistrictRequisitionNumbers!=null && listDistrictRequisitionNumbers.size()>0)
				{
					lstJobRequisitionNumbers=jobRequisitionNumbersDAO.findReqsObjInJRN(listDistrictRequisitionNumbers);
					if(lstJobRequisitionNumbers!=null && lstJobRequisitionNumbers.size()>0)
					{
						for (JobRequisitionNumbers jobRequisitionNumbers : lstJobRequisitionNumbers) 
						{
							String jobReq = jobRequisitionNumbers.getJobOrder().getJobId()+"##"+jobRequisitionNumbers.getDistrictRequisitionNumbers().getRequisitionNumber();
							mapJobRequisitionNumbers.put(jobReq,jobRequisitionNumbers);
						}
					}
				}
			}
			return mapJobRequisitionNumbers;
		}
		
		
		public Map<Integer, EmployeeMaster> getEmployeeMasterFromTID(List<EmployeeMaster> lstEmployeeMaster,Map<String, Integer> mapTPI)
		{
			Map<Integer, EmployeeMaster> mapTIDEmp=new HashMap<Integer, EmployeeMaster>();
			for(EmployeeMaster employeeMaster:lstEmployeeMaster)
     	 	{
     	 		if(employeeMaster!=null)
     	 		{
     	 			String sTempMapKey1="",sTempMapKey2="";
     	 			if(employeeMaster.getEmployeeCode()!=null && !employeeMaster.getEmployeeCode().equalsIgnoreCase("")){
 						sTempMapKey2=sTempMapKey1+"_"+employeeMaster.getEmployeeCode();
     	 			} else {
     	 				if(employeeMaster.getFirstName()!=null && !employeeMaster.getFirstName().equalsIgnoreCase(""))
	 						sTempMapKey1=employeeMaster.getFirstName();
	 					if(employeeMaster.getLastName()!=null && !employeeMaster.getLastName().equalsIgnoreCase(""))
	 						sTempMapKey1=sTempMapKey1+"_"+employeeMaster.getLastName();
	 					if(employeeMaster.getSSN()!=null && !employeeMaster.getSSN().equalsIgnoreCase(""))
	 						sTempMapKey1=sTempMapKey1+"_"+employeeMaster.getSSN();
	 					if(employeeMaster.getEmployeeCode()!=null && !employeeMaster.getEmployeeCode().equalsIgnoreCase(""))
	 						sTempMapKey2=sTempMapKey1+"_"+employeeMaster.getEmployeeCode();
     	 			}
     	 			
	 				if(sTempMapKey1!=null && !sTempMapKey1.equalsIgnoreCase(""))
	 					sTempMapKey1=sTempMapKey1.toUpperCase();
	 					
	 				if(sTempMapKey2!=null && !sTempMapKey2.equalsIgnoreCase(""))
	 					sTempMapKey2=sTempMapKey2.toUpperCase();
	 				
     	 			Integer teacherID=null;
     	 			if(sTempMapKey1!=null && !sTempMapKey1.equalsIgnoreCase(""))
     	 			{
     	 				if(mapTPI!=null)
     	 				{
     	 					if(mapTPI.get(sTempMapKey1)!=null)
     	 						teacherID=mapTPI.get(sTempMapKey1);
     	 					else if(sTempMapKey2!=null && !sTempMapKey2.equalsIgnoreCase(""))
    	     	 			{
     	 						if(mapTPI.get(sTempMapKey2)!=null)
	     	 						teacherID=mapTPI.get(sTempMapKey2);
    	     	 			}
     	 					
     	 					if(teacherID!=null)
     	 						mapTIDEmp.put(teacherID, employeeMaster);
     	 				}
     	 			}
     	 		}
     	 	}
			return mapTIDEmp;
		}
		
		public File findLatestFilesByPath(String path)
	    {
	        String pathToScan = path;
	        File dir = new File(pathToScan);
	        
	        File[] files2 = dir.listFiles( new FilenameFilter() {
	            public boolean accept( File dir,String name ) {
	            	return name.endsWith("TM_MDCPS_staff_delta.CSV");            	
	            }
	        } );

	        if(files2.length == 0)
	            return null;       
	        
	            File lastModifiedFile = files2[0];
	        for(int i = 1; i < files2.length; i++){
		            if(lastModifiedFile.lastModified() < files2[i].lastModified()){	            	
		                lastModifiedFile = files2[i];	            	
		            }
	        }       
	        		return lastModifiedFile;
	    }
		
		public String setRejectStatus(List<JobForTeacher> lstJFT,int iPCOC, Map<String, JobRequisitionNumbers> listOfReqMap)
		{
			StringBuffer sb=new StringBuffer("");
			String sEmailSubject="",sEmailBodyText="";
			if(lstJFT!=null && lstJFT.size() >0)
			{
				SessionFactory sessionFactory=jobForTeacherDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
		 	    Transaction txOpen =statelesSsession.beginTransaction();
		 	    
		 	    UserMaster userMaster=new UserMaster();
		 	    userMaster.setUserId(1);
		 	    
		 	    StatusMaster statusMaster=new StatusMaster();
		 	    statusMaster=statusMasterDAO.findStatusByShortName("rem");
		 	    
		 	    String sStatusNote=msgRejectedTheCandidate1;
		 	    if(iPCOC==0)
		 	    {
		 	    	sStatusNote=msgRejectedTheCandidate2;
		 	    	sEmailSubject=msgRejectedTheCandidate3;
		 	    }
		 	    else if(iPCOC==1)
		 	    {
		 	    	sStatusNote=msgRejectedTheCandidate4;
		 	    	sEmailSubject=msgRejectedTheCandidate5;
		 	    }
		 	   
		 	   sb.append("<BR>"+sEmailSubject+"<BR>");
		 	    
		 	   sb.append("<Table border='1' cellpadding='5' cellspacing='0'>");
		 	   
		 	   sb.append("<TR>");
		 	   sb.append("<TD>SNo.</TD>");
		 	   sb.append("<TD>"+LBLJobForTeacherId1+"</TD>");
		 	   sb.append("<TD>"+LBLTeacherId1+"</TD>");
		 	   sb.append("<TD>"+EmailAddressD+"</TD>");
		 	   sb.append("<TD>"+LBLJobDetails1+"</TD>");
		 	   sb.append("</TR>");
		 	   List<Long> jobForTeacherIdList= new ArrayList<Long>();
		 	   List<Integer> teacherDetailIds= new ArrayList<Integer>();
		 	   int iCounter=0;
		 	   int count=0;
		 	   String queryString="";
			   for(JobForTeacher jft:lstJFT){
					if(jft!=null && jft.getStatus()!=null && jft.getStatus().getStatusShortName()!=null && !jft.getStatus().getStatusShortName().equalsIgnoreCase("hird") && !jft.getStatus().getStatusShortName().equalsIgnoreCase("rem"))
					{
						iCounter++;
						TeacherDetail teacherDetail=jft.getTeacherId();
						JobOrder jobOrder=jft.getJobId();
						DistrictMaster districtMaster=jft.getJobId().getDistrictMaster();
						
						//TSHFJ
						TeacherStatusHistoryForJob objTSHFJ =new TeacherStatusHistoryForJob();
						objTSHFJ.setTeacherDetail(teacherDetail);
						objTSHFJ.setJobOrder(jobOrder);
						objTSHFJ.setStatusMaster(statusMaster);
						objTSHFJ.setStatus("A");
						objTSHFJ.setCreatedDateTime(new Date());
						objTSHFJ.setUserMaster(userMaster);
						
						txOpen =statelesSsession.beginTransaction();
						statelesSsession.insert(objTSHFJ);
						txOpen.commit();
						
						//TSN
						TeacherStatusNotes tsnObj = new TeacherStatusNotes();
						tsnObj.setTeacherDetail(teacherDetail);
						tsnObj.setJobOrder(jobOrder);
						tsnObj.setUserMaster(userMaster);
						tsnObj.setDistrictId(districtMaster.getDistrictId());
						if(statusMaster!=null)
							tsnObj.setStatusMaster(statusMaster);
						tsnObj.setStatusNotes(sStatusNote);
						tsnObj.setFinalizeStatus(true);
						
						txOpen =statelesSsession.beginTransaction();
						statelesSsession.insert(tsnObj);
						txOpen.commit();
						
						// St ... Requisition reset
						String sRequisitionNumber="";
						if(jft.getRequisitionNumber()!=null && jft.getRequisitionNumber().length()>0)
						{
							sRequisitionNumber=jft.getRequisitionNumber();
							jft.setRequisitionNumber("");
							if(listOfReqMap.containsKey(jft.getJobId().getJobId()+"##"+sRequisitionNumber)){
								JobRequisitionNumbers jobRequisitionNumbers = listOfReqMap.get(jft.getJobId().getJobId()+"##"+sRequisitionNumber);
								jobRequisitionNumbers.setStatus(0);
								
								txOpen =statelesSsession.beginTransaction();
								statelesSsession.update(jobRequisitionNumbers);
								txOpen.commit();
							}
							
						}
						
						
						
						//Ed ... Requisition reset
						
						jft.setOfferReady(null);
						jft.setOfferAccepted(null);
						jft.setRequisitionNumber(null);
						if(statusMaster!=null)
						{
							jft.setStatus(statusMaster);
							jft.setStatusMaster(statusMaster);
							jft.setLastActivity(statusMaster.getStatus());
						}
						jft.setUpdatedBy(userMaster);
						jft.setUpdatedByEntity(userMaster.getEntityType());		
						jft.setUpdatedDate(new Date());
						jft.setLastActivityDate(new Date());
						jft.setUserMaster(userMaster);
						
						txOpen =statelesSsession.beginTransaction();
						statelesSsession.update(jft);
						txOpen.commit();
						
						
						
						sb.append("<TR>");
						sb.append("<TD>"+iCounter+"</TD>");
				 	   	sb.append("<TD>"+jft.getJobForTeacherId()+"</TD>");
				 	   	sb.append("<TD>"+jft.getTeacherId().getTeacherId()+"</TD>");
				 	   	sb.append("<TD>"+jft.getTeacherId().getEmailAddress()+"</TD>");
				 	   	sb.append("<TD>"+jft.getJobId()+"</TD>");
				 	   	sb.append("</TR>");
					   	try{
					 	   	jobForTeacherIdList.add(jft.getJobForTeacherId());
							teacherDetailIds.add(jft.getTeacherId().getTeacherId());
							try{
								String jobTitle=jft.getJobId().getJobTitle();
								if(jobTitle.indexOf("(")>0){
									jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
									System.out.println("jobTitle:::::::::"+jobTitle);
								}
								System.out.println("jobTitle::::::"+jobTitle);
								if(count==1){
									count=0;
									queryString+=" or ";	
								}
								queryString+="( jo.districtId="+districtMaster.getDistrictId()+" and jo.jobTitle like '%"+jobTitle+"%' and jo.jobCategoryId="+jft.getJobId().getJobCategoryMaster().getJobCategoryId()+" and jft.jobForTeacherId not in ("+jft.getJobForTeacherId()+") and jft.teacherId="+jft.getTeacherId().getTeacherId()+")";
								count++;
							}catch(Exception e){
								e.printStackTrace();
							}
							System.out.println("queryString::::::"+queryString);
				 	   	}catch(Exception e){
				 	   		e.printStackTrace();
				 	   	}
				 	   	
				 	   	
				 	   	/*
				 		 * 	Remove data from table "sapcandidatedetails" and copy to "sapcandidatedetailshistory" 
				 		 * */
				 	   List <SapCandidateDetails> sapCandidateDetailsList = sapCandidateDetailsDAO.checkSAPdataByTeacherAndJob(jft.getTeacherId(),jft.getJobId());
				 	   if(sapCandidateDetailsList !=null && sapCandidateDetailsList.size()>0){
				 		  for(SapCandidateDetails sapCandidateDetails : sapCandidateDetailsList){
				 				SapCandidateDetailsHistory copySapHistory = new SapCandidateDetailsHistory();
				 				try{
									BeanUtils.copyProperties(copySapHistory, sapCandidateDetails);
									copySapHistory.setSapCandidateId(null);
									copySapHistory.setHistoryDate(new Date());
								} catch (IllegalAccessException e) {
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									e.printStackTrace();
								}
								// Copy Record
								try {
									 txOpen =statelesSsession.beginTransaction();
									 statelesSsession.insert(copySapHistory);
									 txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
								// Delete Record
								try {
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.delete(sapCandidateDetails);
									txOpen.commit();
								} catch (HibernateException e) {
									e.printStackTrace();
								}
				 			}
				 	     }
				 	   	/*	End
				 	   	 * 	
				 	   	 * */
				 	   	
					}
				}
				sb.append("</Table>");
				statelesSsession.close();
				/////////////////////////////////Add Rejected Zone Wise///////////////////
				try{
					List<JobForTeacher> jobForTeacherList= jobForTeacherDAO.findJobsByTeacherListAndJobTitles(queryString);
					List<JobOrder> joblist=new ArrayList<JobOrder>();
					List<TeacherDetail> teacherList =new ArrayList<TeacherDetail>();
					System.out.println("jobForTeacherList:::><><::"+jobForTeacherList.size());
					try{
						for (JobForTeacher jobForTeacher : jobForTeacherList) {
							joblist.add(jobForTeacher.getJobId());
							teacherList.add(jobForTeacher.getTeacherId());
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					List<TeacherStatusHistoryForJob> forJobs=teacherStatusHistoryForJobDAO.findByTeachersAndJobsStatus(teacherList, joblist);
					
					Map<String,TeacherStatusHistoryForJob> mapTSHFJ=new HashMap<String, TeacherStatusHistoryForJob>();
					if(forJobs!=null && forJobs.size() > 0)
					{
						for(TeacherStatusHistoryForJob tSHObj : forJobs){
							mapTSHFJ.put(tSHObj.getTeacherDetail().getTeacherId()+"#"+tSHObj.getJobOrder().getJobId(),tSHObj);
						}
					}
					DistrictMaster districtMaster=null;
					if(jobForTeacherList.size()>0)
						for (JobForTeacher jobForObj : jobForTeacherList){
							TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
							String sTID_JID=jobForObj.getTeacherId().getTeacherId()+"#"+jobForObj.getJobId().getJobId();
							districtMaster=jobForObj.getJobId().getDistrictMaster();
							try{
								if(mapTSHFJ.get(sTID_JID)!=null){
										teacherStatusHistoryForJob=mapTSHFJ.get(sTID_JID);
								}
								if(teacherStatusHistoryForJob!=null)
								{
									teacherStatusHistoryForJob.setStatus("I");
									teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
									teacherStatusHistoryForJob.setUserMaster(userMaster);
									
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.update(teacherStatusHistoryForJob);
									txOpen.commit();
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
									TeacherStatusHistoryForJob forJob=new TeacherStatusHistoryForJob();
									forJob.setTeacherDetail(jobForObj.getTeacherId());
									forJob.setJobOrder(jobForObj.getJobId());
									forJob.setStatusMaster(statusMaster);
									forJob.setStatus("A");
									forJob.setUserMaster(userMaster);
									forJob.setCreatedDateTime(new Date());
									//teacherStatusHistoryForJobDAO.makePersistent(forJob);
									txOpen =statelesSsession.beginTransaction();
									statelesSsession.insert(forJob);
									txOpen.commit();
							}catch(Exception e){
								e.printStackTrace();
							}	
							
							try{
								TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
								teacherStatusNoteObj.setTeacherDetail(jobForObj.getTeacherId());
								teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
								teacherStatusNoteObj.setStatusMaster(statusMaster);
								teacherStatusNoteObj.setUserMaster(userMaster);
								teacherStatusNoteObj.setDistrictId(districtMaster.getDistrictId());
								teacherStatusNoteObj.setStatusNotes("Job of this Candidate is Rejected because of External status found in Staff Delta file");
								teacherStatusNoteObj.setEmailSentTo(1);
								teacherStatusNoteObj.setFinalizeStatus(true);
								//teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
								txOpen =statelesSsession.beginTransaction();
								statelesSsession.insert(teacherStatusNoteObj);
								txOpen.commit();
							}catch(Exception ee){
								ee.printStackTrace();
							}
							jobForObj.setOfferReady(null);
							jobForObj.setOfferAccepted(null);
							jobForObj.setRequisitionNumber(null);
							jobForObj.setStatus(statusMaster);
							jobForObj.setStatusMaster(statusMaster);
							jobForObj.setUpdatedBy(userMaster);
							jobForObj.setUpdatedByEntity(userMaster.getEntityType());		
							jobForObj.setUpdatedDate(new Date());
							jobForObj.setLastActivity("Rejected");
							jobForObj.setLastActivityDate(new Date());
							jobForObj.setUserMaster(userMaster);
							//jobForTeacherDAO.makePersistent(jobForObj);
							txOpen =statelesSsession.beginTransaction();
							statelesSsession.update(jobForObj);
							txOpen.commit();
						}
				}catch(Exception e){
					e.printStackTrace();
				}
				/////////////////////////////////End Rejected Zone Wise///////////////////
				/*sEmailBodyText=sb.toString();
				
				DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
				dsmt.setEmailerService(emailerService);
				dsmt.setMailfrom("admin@netsutra.com");
				dsmt.setMailto("teachermatchemaillog@gmail.com");
				dsmt.setMailsubject(sEmailSubject);
				dsmt.setMailcontent(sEmailBodyText);
				
				System.out.println("sEmailSubject "+sEmailSubject);
				System.out.println("sEmailBodyText "+sEmailBodyText);
				
				try {
					dsmt.start();	
				} catch (Exception e) {}*/
			}
			
			return sb.toString();
		}
}
