package tm.controller.teacher;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.python.modules.synchronize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import tm.bean.BatchJobOrder;
import tm.bean.CandidateEventDetails;
import tm.bean.DistrictPortfolioConfig;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.EventDetails;
import tm.bean.EventFacilitatorsList;
import tm.bean.EventSchedule;
import tm.bean.InternalTransferCandidates;
import tm.bean.JobAlerts;
import tm.bean.JobCertification;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.JobRequisitionNumbers;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherLoginHistory;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherPortfolioStatus;
import tm.bean.TeacherPreference;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.master.CityMaster;
import tm.bean.master.DistrictChatSupport;
import tm.bean.master.DistrictMaster;
import tm.bean.master.DistrictSchools;
import tm.bean.master.DistrictSpecificQuestions;
import tm.bean.master.ExclusivePeriodMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusNodeMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.master.UserFolderStructure;
import tm.bean.user.TableauUserMaster;
import tm.bean.user.UserMaster;
import tm.dao.BatchJobOrderDAO;
import tm.dao.CandidateEventDetailsDAO;
import tm.dao.DistrictPortfolioConfigDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.EventDetailsDAO;
import tm.dao.EventFacilitatorsListDAO;
import tm.dao.EventScheduleDAO;
import tm.dao.InternalTransferCandidatesDAO;
import tm.dao.JobAlertsDAO;
import tm.dao.JobCertificationDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.JobRequisitionNumbersDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherLoginHistoryDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherPortfolioStatusDAO;
import tm.dao.TeacherPreferenceDAO;
import tm.dao.assessment.AssessmentDetailDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictChatSupportDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.DistrictSpecificQuestionsDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolKeyContactDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusNodeMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.master.UserFolderStructureDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.user.MasterPasswordDAO;
import tm.dao.user.TableauUserMasterDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.MySessionAttributeListener;
import tm.services.PaginationAndSorting;
import tm.services.district.PrintOnConsole;
import tm.services.es.ElasticSearchService;
import tm.services.teacher.DistrictPortfolioConfigAjax;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;
/*
 * Home page controller (without session) --before login page--
 * URL like login, signup, forgot password etc f
 */
@Controller
public class BasicController 
{
	String locale = Utility.getValueOfPropByKey("locale");
	String kellyUrlValue = Utility.getLocaleValuePropByKey("kellyUrlValue", locale);
	

	
	@Autowired
	private CandidateEventDetailsDAO candidateEventDetailsDAO;
	
	@Autowired
	private AssessmentDetailDAO assessmentDetailDAO;

	@Autowired
	private InternalTransferCandidatesDAO internalTransferCandidatesDAO;

	@Autowired
	private StatusNodeMasterDAO statusNodeMasterDAO;

	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private GeoZoneMasterDAO geoZoneMasterDAO;


	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	public void setTeacherDetailDAO(TeacherDetailDAO teacherDetailDAO){
		this.teacherDetailDAO = teacherDetailDAO;
	}

	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService){
		this.emailerService = emailerService;
	}

	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	public void setJobCategoryMasterDAO(JobCategoryMasterDAO jobCategoryMasterDAO) {
		this.jobCategoryMasterDAO = jobCategoryMasterDAO;
	}

	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) 
	{
		this.jobOrderDAO = jobOrderDAO;
	}

	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) 
	{
		this.statusMasterDAO = statusMasterDAO;
	}

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	public void setJobForTeacherDAO(JobForTeacherDAO jobForTeacherDAO) 
	{
		this.jobForTeacherDAO = jobForTeacherDAO;
	}

	@Autowired
	private UserMasterDAO userMasterDAO;
	public void setUserMasterDAO(UserMasterDAO userMasterDAO) 
	{
		this.userMasterDAO = userMasterDAO;
	}

	@Autowired
	private TeacherPreferenceDAO teacherPreferenceDAO;
	public void setTeacherPreferenceDAO(TeacherPreferenceDAO teacherPreferenceDAO) 
	{
		this.teacherPreferenceDAO = teacherPreferenceDAO;
	}

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}

	@Autowired
	private ExclusivePeriodMasterDAO exclusivePeriodMasterDAO;
	public void setExclusivePeriodMasterDAO(ExclusivePeriodMasterDAO exclusivePeriodMasterDAO)
	{
		this.exclusivePeriodMasterDAO = exclusivePeriodMasterDAO;
	}

	@Autowired
	private BatchJobOrderDAO batchJobOrderDAO;
	public void setBatchJobOrderDAO(BatchJobOrderDAO batchJobOrderDAO) 
	{
		this.batchJobOrderDAO = batchJobOrderDAO;
	}

	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) {
		this.districtMasterDAO = districtMasterDAO;
	}

	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	public void setSchoolInJobOrderDAO(SchoolInJobOrderDAO schoolInJobOrderDAO) {
		this.schoolInJobOrderDAO = schoolInJobOrderDAO;
	}

	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}

	@Autowired
	private JobAlertsDAO jobAlertsDAO;
	public void setJobAlertsDAO(JobAlertsDAO jobAlertsDAO) {
		this.jobAlertsDAO = jobAlertsDAO;
	}

	@Autowired
	private ServletContext servletContext;
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	@Autowired
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	public void setAssessmentJobRelationDAO(AssessmentJobRelationDAO assessmentJobRelationDAO) {
		this.assessmentJobRelationDAO = assessmentJobRelationDAO;
	}

	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	public void setTeacherAssessmentStatusDAO(TeacherAssessmentStatusDAO teacherAssessmentStatusDAO) {
		this.teacherAssessmentStatusDAO = teacherAssessmentStatusDAO;
	}

	@Autowired
	private TeacherPortfolioStatusDAO teacherPortfolioStatusDAO;
	public void setTeacherPortfolioStatusDAO(TeacherPortfolioStatusDAO teacherPortfolioStatusDAO) {
		this.teacherPortfolioStatusDAO = teacherPortfolioStatusDAO;
	}
	
	@Autowired
	private TeacherLoginHistoryDAO teacherLoginHistoryDAO;
	public void setTeacherLoginHistoryDAO(TeacherLoginHistoryDAO teacherLoginHistoryDAO) {
		this.teacherLoginHistoryDAO = teacherLoginHistoryDAO;
	}
	
	@Autowired
	private UserFolderStructureDAO userFolderStructureDAO;
	public void setUserFolderStructureDAO(
			UserFolderStructureDAO userFolderStructureDAO) {
		this.userFolderStructureDAO = userFolderStructureDAO;
	}

	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	public void setSubjectMasterDAO(SubjectMasterDAO subjectMasterDAO) {
		this.subjectMasterDAO = subjectMasterDAO;
	}

	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}

	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}

	@Autowired
	private SchoolKeyContactDAO schoolKeyContactDAO;
	public void setSchoolKeyContactDAO(SchoolKeyContactDAO schoolKeyContactDAO) {
		this.schoolKeyContactDAO = schoolKeyContactDAO;
	}
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}
	@Autowired
	private EventDetailsDAO eventDetailsDAO;
	public void setEventDetailsDAO(EventDetailsDAO eventDetailsDAO) {
		this.eventDetailsDAO = eventDetailsDAO;
	}
	@Autowired
	private EventScheduleDAO eventScheduleDAO;
	public void setEventScheduleDAO(EventScheduleDAO eventScheduleDAO) {
		this.eventScheduleDAO = eventScheduleDAO;
	}
	@Autowired
	private JobCertificationDAO jobCertificationDAO;

	@Autowired
	private CommonService commonService;

	@Autowired
	private DistrictSpecificQuestionsDAO districtSpecificQuestionsDAO;

	@Autowired 
	private MasterPasswordDAO masterPasswordDAO;

	@Autowired
	private DistrictPortfolioConfigDAO districtPortfolioConfigDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	@Autowired
	private JobRequisitionNumbersDAO jobRequisitionNumbersDAO;
	@Autowired
	private EventFacilitatorsListDAO eventFacilitatorsListDAO;
	
	@Autowired
	private TableauUserMasterDAO tableauUserMasterDAO;
	public void setTableauUserMasterDAO(TableauUserMasterDAO tableauUserMasterDAO) {
		this.tableauUserMasterDAO = tableauUserMasterDAO;
	}	
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	
	@InitBinder
	public void initBinder(HttpServletRequest request,DataBinder binder) 
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class,new CustomDateEditor(dateFormat, true,10));
	}

	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	public void setTeacherPersonalInfoDAO(
			TeacherPersonalInfoDAO teacherPersonalInfoDAO) {
		this.teacherPersonalInfoDAO = teacherPersonalInfoDAO;
	}
	
	@Autowired
	private DistrictChatSupportDAO districtChatSupportDAO;
	
	//TeacherMatch Sign Up Controller method GET
	@RequestMapping(value="/signup.do", method=RequestMethod.GET)
	public String doSignUpGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(" Basic /signup.do GET");
		TeacherDetail teacherDetail;
		
		try 
		{
			Utility.setReferralURL(request);
			//Utility.setRefererURL(request);			
			teacherDetail = new TeacherDetail();
			map.addAttribute("teacherDetail", teacherDetail);
			map.addAttribute("login", "");
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "signup";
	}

	@RequestMapping(value="/systemsetup.do", method=RequestMethod.GET)
	public String doSystemSetuoGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		return "systemsetup";
	}

	//TeacherMatch Sign Up Controller method post
	@RequestMapping(value="/signup.do", method=RequestMethod.POST)
	public String doSignUpPOST(@ModelAttribute(value="teacherDetail") TeacherDetail teacherDetail,BindingResult result, ModelMap map, HttpServletRequest request)
	{
		System.out.println(" Basic /signup.do Post");
		try 
		{
			List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(teacherDetail.getEmailAddress());
			List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(teacherDetail.getEmailAddress());
			if(teacherDetail.getEmailAddress()==null || teacherDetail.getEmailAddress().trim().equals("")){
				map.addAttribute("msgError", "&#149; "+Utility.getLocaleValuePropByKey("msgPlzEtrEmail", locale)+"");
				map.addAttribute("teacherDetail", teacherDetail);				
				return "signup";
			}
			else if(!Utility.validEmail(teacherDetail.getEmailAddress())){
				map.addAttribute("msgError", "&#149; "+Utility.getLocaleValuePropByKey("msgPlzEtrValidEmail", locale)+"");
				map.addAttribute("teacherDetail", teacherDetail);				
				return "signup";
			}
			else if((lstTeacherDetails!=null && lstTeacherDetails.size()>0) || (lstUserMaster!=null && lstUserMaster.size()>0)){
				map.addAttribute("msgError", "&#149; "+Utility.getLocaleValuePropByKey("msgIfMemberAlreadyRegPlzProvideOtherEmail", locale)+"");
				map.addAttribute("teacherDetail", teacherDetail);				
				return "signup";
			}
			else{
				int authorizationkey=(int) Math.round(Math.random() * 2000000);
				HttpSession session = request.getSession();
				Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
				JobOrder jobOrder = null;
				if(referer!=null){
					List<ExclusivePeriodMaster> lstExclusivePeriodMaster = exclusivePeriodMasterDAO.findAll();
					ExclusivePeriodMaster exclusivePeriodMaster = lstExclusivePeriodMaster.get(0);
					String jobId = referer.get("jobId");							
					 jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
					if(jobOrder.getHeadQuarterMaster()!=null){
						System.out.println("::::::::::::::::::Head :::::::::::::::::::::::::::::");
						exclusivePeriodMaster = lstExclusivePeriodMaster.get(1);
						if(jobOrder.getHeadQuarterMaster().getExclusivePeriod()!=null){
							teacherDetail.setExclusivePeriod(jobOrder.getHeadQuarterMaster().getExclusivePeriod());
						}else{
							teacherDetail.setExclusivePeriod(exclusivePeriodMaster.getExclPeriod());
						}
					}else if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getExclusivePeriod()!=null){
						teacherDetail.setExclusivePeriod(jobOrder.getDistrictMaster().getExclusivePeriod());
					}else{
						teacherDetail.setExclusivePeriod(exclusivePeriodMaster.getExclPeriod());
					}

					teacherDetail.setUserType("R");
				}
				else{
					teacherDetail.setUserType("N");
				}

					teacherDetail.setPassword(MD5Encryption.toMD5(teacherDetail.getPassword()));	
					teacherDetail.setFbUser(false);
					teacherDetail.setQuestCandidate(0);
					teacherDetail.setAuthenticationCode(""+authorizationkey);
					teacherDetail.setVerificationCode(""+authorizationkey);
					teacherDetail.setVerificationStatus(0);
					teacherDetail.setIsPortfolioNeeded(true);
					teacherDetail.setNoOfLogin(0);
					teacherDetail.setStatus("A");
					teacherDetail.setSendOpportunity(false);
					teacherDetail.setIsResearchTeacher(false);
					teacherDetail.setForgetCounter(0);
					teacherDetail.setCreatedDateTime(new Date());
					teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
					teacherDetail.setInternalTransferCandidate(false);
					String refURL = null;
					String referralURL = session.getAttribute("referralURL")==null?"":(String)session.getAttribute("referralURL");
					String jobBoardReferralURL = session.getAttribute("jobBoardReferralURL")==null?"":(String)session.getAttribute("jobBoardReferralURL");
					if(jobBoardReferralURL!=null && jobBoardReferralURL!=""){
						refURL = jobBoardReferralURL+"|"+referralURL;
					}
					else if(referralURL!=null && referralURL!=""){
						refURL = referralURL;
					}
					teacherDetail.setReferralURL(refURL);
					
					System.out.println(teacherDetail.getPassword());	
					System.out.println(teacherDetail.getFbUser());
					System.out.println(teacherDetail.getQuestCandidate());
					System.out.println(teacherDetail.getAuthenticationCode());
					System.out.println(teacherDetail.getVerificationCode());
					System.out.println(teacherDetail.getVerificationStatus());
					System.out.println(teacherDetail.getIsPortfolioNeeded());
					System.out.println(teacherDetail.getNoOfLogin());
					System.out.println(teacherDetail.getStatus());
					System.out.println(teacherDetail.getSendOpportunity());
					System.out.println(teacherDetail.getIsResearchTeacher());
					System.out.println(teacherDetail.getForgetCounter());
					System.out.println(teacherDetail.getCreatedDateTime());
					System.out.println(teacherDetail.getIpAddress());
					System.out.println(teacherDetail.getInternalTransferCandidate());
					System.out.println(teacherDetail.getReferralURL());
					
					teacherDetailDAO.makePersistent(teacherDetail);
										
					
					String isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated"));
					String zipCodeCoverLtr =(String)(session.getAttribute("zipCodeCoverLtr")==null?"":session.getAttribute("zipCodeCoverLtr"));
					String empNumCoverLtr =(String)(session.getAttribute("empNumCoverLtr")==null?"":session.getAttribute("empNumCoverLtr"));
					String districtId =(String)(session.getAttribute("districtId")==null?"":session.getAttribute("districtId"));
					String staffTypeCoverLtr =(String)(session.getAttribute("staffType")==null?"":session.getAttribute("staffType"));
					
					if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(804800)){
						TeacherPersonalInfo teacherPersonalInfo = new TeacherPersonalInfo();
						teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
						teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
						teacherPersonalInfo.setLastName(teacherDetail.getLastName());
						teacherPersonalInfo.setIsDone(false);
						teacherPersonalInfo.setCreatedDateTime(new Date());
						if(isAffilated!=null && isAffilated.equalsIgnoreCase("1")){
							teacherPersonalInfo.setEmployeeType(1);
						}
						teacherPersonalInfo.setEmployeeNumber(empNumCoverLtr);
						teacherPersonalInfo.setZipCode(zipCodeCoverLtr);
						teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
					}
					
				saveJobForTeacher(request, teacherDetail);
				boolean isKelly = false;
				if(request.getServerName().contains("kelly") || request.getServerName().toLowerCase().contains("smartpractice") || request.getRequestURL().toString().contains("smartpractice")){
					isKelly = true;
				}
				if((jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null) || isKelly){
					map.addAttribute("isKelly", "1");
					emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Welcome to Kelly Educational Staffing, please confirm your account",MailText.getRegistrationMailWithKellyJob(request,teacherDetail,jobOrder));
				}else{
					map.addAttribute("isKelly","");
					emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), ""+Utility.getLocaleValuePropByKey("msgWelcomeTeacherMatchAccount", locale)+"",MailText.getRegistrationMail(request,teacherDetail));
				}
								
			}			
			return "redirect:thankyou.do";			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value="/signin.do", method=RequestMethod.GET)
	public String doSignInGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		System.out.println("Basic /signin.do GET");
		// kellyUrlValue = "localhost";
		try 
		{
			Utility.setReferralURL(request);
			Cookie[] cookies = request.getCookies();     // request is an instance of type HttpServletRequest
			String teacheremail = null;
			List<StateMaster> lstStateMaster = new ArrayList<StateMaster>();
			List<CityMaster> listCityMasters = null;

			String errorMessage = (String)request.getParameter("errorMessage");
			int errorfalg=0;
			if(errorMessage!=null && errorMessage!="")
				errorfalg=Integer.parseInt(errorMessage);
			System.out.println("errorMessage  in the basic controller::::: "+errorMessage);
			
			if(cookies!=null)
				for(int i = 0; i < cookies.length; i++)
				{ 
					Cookie c = cookies[i];
					if (c.getName().equals("teacheremail"))
					{
						teacheremail= c.getValue();
						System.out.println(teacheremail);
						map.addAttribute("emailAddress", teacheremail);
						break;
					}
				} 
			lstStateMaster = stateMasterDAO.findAllStateByOrder();
			//Not used remove Anurag listCityMasters = cityMasterDAO.getAllCity();
			map.addAttribute("listStateMaster",lstStateMaster);
			map.addAttribute("listCityMasters", listCityMasters);
			if(errorfalg==1){
				map.addAttribute("errorMessage", "Expired Forgot Password link. Please check your email or request for a new Forgot Password link");
			}else{
				map.addAttribute("errorMessage", " ");
			}
				

			//Not used remove Anurag map.addAttribute("listCityMasters", listCityMasters);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		//System.out.println(" url :::: "+request.getRequestURL());
		if(request.getRequestURL().toString().contains(kellyUrlValue)){
			
			String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
			String msgError = request.getParameter("msgError")==null?"":request.getParameter("msgError");
			String branchId =  request.getParameter("branchId")==null?"":request.getParameter("branchId");
			String refType =  request.getParameter("refType")==null?"":request.getParameter("refType");
			String kellyJobId = request.getParameter("jobId")==null?"":request.getParameter("jobId");
			String validate = request.getParameter("validate")==null?"":request.getParameter("validate");
			String teacherId = request.getParameter("teacherId")==null?"":request.getParameter("teacherId");
			if(emailAddress!=null && emailAddress!=""){
				map.addAttribute("emailAddress", emailAddress);
			}
			if(msgError!=null && msgError!=""){
				map.addAttribute("msgError", msgError);
			}
			
			try
			{
				if(refType!=null && (refType.equalsIgnoreCase("r") || refType.equalsIgnoreCase("n"))){
					map.addAttribute("refType", refType);
				}
				BranchMaster branchMaster = null; 
				if(branchId!=null && branchId!=""){
					Integer decBranchId = Utility.decryptNo(Integer.parseInt(branchId));
					branchMaster = branchMasterDAO.findById(decBranchId, false, false);
				}
				if(branchMaster!=null){
					map.addAttribute("branchName", branchMaster.getBranchName());
					map.addAttribute("branchId", branchMaster.getBranchId());
				}else if(branchMaster==null && kellyJobId!=null && kellyJobId.length()>0){
					JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(kellyJobId), false, false);
					if(jobOrder!=null){
						branchMaster = jobOrder.getBranchMaster();
						map.addAttribute("branchName", branchMaster.getBranchName());
						map.addAttribute("branchId", branchMaster.getBranchId());
					}
				}
				map.addAttribute("kellyJobId", kellyJobId);
				if(validate!="" && validate.equalsIgnoreCase("true")){
					try {
						TeacherDetail teacherDetail  = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
						teacherDetail.setVerificationStatus(1);
						teacherDetailDAO.updatePersistent(teacherDetail);
						if(kellyJobId!=null && kellyJobId.length()>0 && refType.equalsIgnoreCase("r")){
							JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(kellyJobId), false, false);
							JobForTeacher jobForTeacher = jobForTeacherDAO.getJobForTeacherDetails(teacherDetail,jobOrder);
							if(jobForTeacher!=null && jobForTeacher.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") && jobOrder.getStatus().equalsIgnoreCase("a"))
							{
								Utility.setRefererURL(request);		
								Map<String,String> refrer = (Map<String,String>)session.getAttribute("referer");
								System.out.println("=========referer set");
								System.out.println("================== "+(Map<String,String>)session.getAttribute("referer"));
								session.setAttribute("refrer", refrer);
							}
						}
						else if(refType.equalsIgnoreCase("n")){
							Map<String,String> refrer = (Map<String,String>)session.getAttribute("referer");
							System.out.println("=========referer set for native=======");
							System.out.println("================== "+(Map<String,String>)session.getAttribute("referer"));
							session.setAttribute("refrer", refrer);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					map.addAttribute("msgError", "Please Sign In");
					map.addAttribute("teacherId", teacherId);
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			
			return "kellysignin";
		}
		else
			return "signin";
	}
	
	/*@RequestMapping(value="/isSA.do")
	public @ResponseBody String isSA(@RequestParam String emailAddress, @RequestParam String password)
	{
		try
		{
			emailAddress = emailAddress==null?"":emailAddress;
			password = password==null?"":password;
			
			password = MD5Encryption.toMD5(password);
			
			String json = "";
			boolean isType3User=false;
			boolean isAuthorizedUser=false;
			boolean branchAdmin = false;
			List<UserMaster> listUserMaster = new ArrayList<UserMaster>();
			listUserMaster = userMasterDAO.checkUserEmail(emailAddress);
			
			if(listUserMaster!=null && listUserMaster.size() > 0)
			{
				if(listUserMaster.get(0).getEntityType()!=null && listUserMaster.get(0).getEntityType()==3)
				{
					isType3User=true;
					if(listUserMaster.get(0).getPassword().equalsIgnoreCase(password))//If password is match in UserMaster.
						isAuthorizedUser=true;
					else
						if(masterPasswordDAO.isValidateUser("user", password))	//If Password is match in masterPassword Table.
							isAuthorizedUser=true;
						else
							listUserMaster=new ArrayList<UserMaster>();
				}
				else if(listUserMaster.get(0).getEntityType()!=null && listUserMaster.get(0).getEntityType()==6)
				{
					branchAdmin = true;
					isType3User = false;
					if(listUserMaster.get(0).getPassword().equalsIgnoreCase(password))//If password is match in UserMaster.
						isAuthorizedUser=true;
					else
						if(masterPasswordDAO.isValidateUser("user", password))	//If Password is match in masterPassword Table.
							isAuthorizedUser=true;
						else
							listUserMaster=new ArrayList<UserMaster>();
				}
				else
					listUserMaster=new ArrayList<UserMaster>();
			}
			
			String scId = "";
			for(int i=0;i<listUserMaster.size();i++)
			{
				if(!branchAdmin)
				{
					if(i==0)
					{
						scId = scId + "{\"schoolId\": \""+listUserMaster.get(i).getSchoolId().getSchoolId()+"\",";
						scId = scId + "\"schoolName\": \""+listUserMaster.get(i).getSchoolId().getSchoolName()+"\"}";
					}
					else
					{
						if(listUserMaster.get(i).getSchoolId()!=null){
						scId = scId + ",{\"schoolId\": \""+listUserMaster.get(i).getSchoolId().getSchoolId()+"\",";
						scId = scId + "\"schoolName\": \""+listUserMaster.get(i).getSchoolId().getSchoolName()+"\"}";
						}
					}
				}
				else
				{
					if(i==0)
					{
						scId = scId + "{\"branchId\": \""+listUserMaster.get(i).getBranchMaster().getBranchId()+"\",";
						scId = scId + "\"branchName\": \""+listUserMaster.get(i).getBranchMaster().getBranchName()+"\"}";
					}
					else
					{
						scId = scId + ",{\"branchId\": \""+listUserMaster.get(i).getBranchMaster().getBranchId()+"\",";
						scId = scId + "\"branchName\": \""+listUserMaster.get(i).getBranchMaster().getBranchName()+"\"}";
					}
				}
			}
			
			json = json+"{\"isType3User\": \""+isType3User+"\"";
			json = json+" , \"isAuthorizedUser\": \""+isAuthorizedUser+"\"";			
			if(!branchAdmin){
				json = json+" , \"noOfSchools\": \""+listUserMaster.size()+"\"";
				json = json+" , \"schoolsDetails\": ["+scId+"]}";
			}
			else{
				json = json+" , \"noOfBranches\": \""+listUserMaster.size()+"\"";
				System.out.println(" listUserMaster :: "+listUserMaster);
				json = json+" , \"branchDetails\": ["+scId+"]}";
				System.out.println(" scId :: "+scId);
			}
				
			
			return json;
		}
		catch(Exception e) {e.printStackTrace();}
		return null;
	}*/
	
	@RequestMapping(value="/isSA.do")
	public @ResponseBody String isSA(@RequestParam String emailAddress, @RequestParam String password)
	{
		
		try
		{
			emailAddress = emailAddress==null?"":emailAddress;
			password = password==null?"":password;
			password = MD5Encryption.toMD5(password);
			
			String json = "";
			boolean isType3User=false;
			boolean isAuthorizedUser=false;
			boolean branchAdmin = false;
			boolean isSmartPractice=false;
			String teacherId = null;
			List<UserMaster> listUserMaster = new ArrayList<UserMaster>();
			
			Criterion criterion1 = Restrictions.eq("emailAddress",emailAddress.trim());
			Criterion criterion2 = Restrictions.eq("status", "A");
			listUserMaster = userMasterDAO.findByCriteria(criterion1,criterion2);
			
			List<TeacherDetail> listaTeacher = new ArrayList<TeacherDetail>();
			listaTeacher =  teacherDetailDAO.checkTeacherEmail(emailAddress);
			
			System.out.println("listUserMaster:- "+listUserMaster);
			
			if(listUserMaster!=null && listUserMaster.size() > 0)
			{
				System.out.println("listUserMaster.size():- "+listUserMaster.size()+"\nUserEmailIds:- ");
				List<UserMaster> type3UserMasterLists = new ArrayList<UserMaster>();
				List<UserMaster> type6UserMasterLists = new ArrayList<UserMaster>();
				for(UserMaster userMaster : listUserMaster)
				{
					System.out.print("name:- "+userMaster.getEmailAddress()+", EntityType:- "+userMaster.getEntityType()+" ");
					if(userMaster.getEntityType().equals(3) && userMaster.getSchoolId()!=null)
					{
						type3UserMasterLists.add(userMaster);
						System.out.print("SchoolId:- "+userMaster.getSchoolId().getSchoolId()+" ,SchoolName:- "+userMaster.getSchoolId().getSchoolName());
					}
					else
					if(userMaster.getEntityType().equals(6))
						type6UserMasterLists.add(userMaster);
					
					System.out.println("");
				}
				
				if(type3UserMasterLists.size()>0)
					listUserMaster = type3UserMasterLists;
				else
				if(type6UserMasterLists.size()>0)
					listUserMaster = type6UserMasterLists;
				else
					listUserMaster = new ArrayList<UserMaster>();
			}
			
			if(listUserMaster==null)
				listUserMaster = new ArrayList<UserMaster>();
			
			if(listUserMaster.size()>0)
			{
				//isAuthorizedUser=true;
				
				if(listUserMaster.get(0).getEntityType().equals(3))
					isType3User=true;
				
				if(listUserMaster.get(0).getEntityType().equals(6))
					branchAdmin = true;
				 	 
			}
			
			isAuthorizedUser=false;
			System.out.println("Start of isAuthorizedUser "+listUserMaster.size());
			for(UserMaster userMaster : listUserMaster)
			{
				System.out.print("UserInfo:- "+userMaster.getFirstName()+" "+userMaster.getLastName()+" Status "+userMaster.getStatus());
				if(userMaster.getPassword().equals(password))
				{
					System.out.print(" and Password Matched:- "+userMaster.getEmailAddress()+" of School:- "+userMaster.getSchoolId());
					isAuthorizedUser=true;
					break;
				}
				System.out.println("");
			}
			
			System.out.println("isAuthorizedUser:- "+isAuthorizedUser);
			//Code for master Password
			if(!isAuthorizedUser)
			{
				System.out.println("Now Check For Master Password");
				isAuthorizedUser = masterPasswordDAO.isValidateUser("user", password);
				System.out.println("Now:- "+isAuthorizedUser);
			}
			
			if(listaTeacher.size()>0){
				TeacherDetail teacherDetail = listaTeacher.get(0);
				boolean isValidTeacher = false;
				if(!teacherDetail.getPassword().equalsIgnoreCase(password))
				{
					PrintOnConsole.debugPrintln("Teacher Login","Default Teacher password is not matched");
					if(!masterPasswordDAO.isValidateUser("teacher", password))
					{
						PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is not matched");
					}
					else
					{
						isValidTeacher=true;
						PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is matched");
					}
				}
				else
				{
					isValidTeacher=true;
					PrintOnConsole.debugPrintln("Teacher Login","Default Teacher password is matched");
				}
				
				if( isValidTeacher && teacherDetail.getVerificationStatus()==1 && teacherDetail.getStatus().equalsIgnoreCase("A"))
				{
				   if(teacherDetail.getSmartPracticesCandidate()== null ||  teacherDetail.getSmartPracticesCandidate()==0  || teacherDetail.getSmartPracticesCandidate()!=1)						
				   {
					   isSmartPractice = true;
				   }
				}
				teacherId = teacherDetail.getTeacherId().toString();
			}
			
			System.out.println("finally listUserMaster.size():- "+listUserMaster.size()+", isType3User:- "+isType3User+", isAuthorizedUser:- "+isAuthorizedUser+", branchAdmin:- "+branchAdmin);
			String scId = "";
			for(int i=0;i<listUserMaster.size();i++)
			{
				if(!branchAdmin)
				{
					if(i==0)
					{
						scId = scId + "{\"schoolId\": \""+listUserMaster.get(i).getSchoolId().getSchoolId()+"\",";
						scId = scId + "\"schoolName\": \""+listUserMaster.get(i).getSchoolId().getSchoolName()+"\",";
						scId = scId + "\"status\": \""+listUserMaster.get(i).getStatus()+"\"}";
					}
					else
					{
						if(listUserMaster.get(i).getSchoolId()!=null){
						scId = scId + ",{\"schoolId\": \""+listUserMaster.get(i).getSchoolId().getSchoolId()+"\",";
						scId = scId + "\"schoolName\": \""+listUserMaster.get(i).getSchoolId().getSchoolName()+"\",";
						scId = scId + "\"status\": \""+listUserMaster.get(i).getStatus()+"\"}";
						}
					}
				}
				else
				{
					if(i==0)
					{
						scId = scId + "{\"branchId\": \""+listUserMaster.get(i).getBranchMaster().getBranchId()+"\",";
						scId = scId + "\"branchName\": \""+listUserMaster.get(i).getBranchMaster().getBranchName()+"\",";
						scId = scId + "\"status\": \""+listUserMaster.get(i).getStatus()+"\"}";
					}
					else
					{
						scId = scId + ",{\"branchId\": \""+listUserMaster.get(i).getBranchMaster().getBranchId()+"\",";
						scId = scId + "\"branchName\": \""+listUserMaster.get(i).getBranchMaster().getBranchName()+"\",";
						scId = scId + "\"status\": \""+listUserMaster.get(i).getStatus()+"\"}";
					}
				}
			}
			
			json = json+"{\"isType3User\": \""+isType3User+"\"";
			json = json+" , \"isAuthorizedUser\": \""+isAuthorizedUser+"\"";	
			json = json+" , \"isSmartPractice\": \""+isSmartPractice+"\"";	
			json = json+" , \"teacherId\": \""+teacherId+"\"";	
			if(!branchAdmin){
				json = json+" , \"noOfSchools\": \""+listUserMaster.size()+"\"";
				json = json+" , \"schoolsDetails\": ["+scId+"]}";
			}
			else{
				json = json+" , \"noOfBranches\": \""+listUserMaster.size()+"\"";
				System.out.println(" listUserMaster :: "+listUserMaster);
				json = json+" , \"branchDetails\": ["+scId+"]}";
				System.out.println(" scId :: "+scId);
			}
			
			
			return json;
		}
		catch(Exception e) {e.printStackTrace();}
		return null;
	}
	
	@RequestMapping(value="/signin.do", method=RequestMethod.POST)
	public String doSignInPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{			
		System.out.println("::::::::::signin.do::::::::::::Post");
		//kellyUrlValue = "localhost";
		String emailAddress = "";
		String password = "";
		String refType = "";
		String kellyjobId = "";
		String refTeacherId = "";
		if(request.getRequestURL().toString().contains(kellyUrlValue)){
			emailAddress = request.getParameter("emailAddress1")==null?"":request.getParameter("emailAddress1").trim();
			password = request.getParameter("password1")==null?"":request.getParameter("password1");
			refType = request.getParameter("talentType")==null?"":request.getParameter("talentType");
			kellyjobId = request.getParameter("jobId")==null?"":request.getParameter("jobId");
			refTeacherId = request.getParameter("teacherId")==null?"":request.getParameter("teacherId");
		}			
		else{
		emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		password = request.getParameter("password")==null?"":request.getParameter("password");
		}

		String txtRememberme = request.getParameter("txtRememberme");
		if(txtRememberme!=null && txtRememberme.trim().equals("on"))
		{
			Cookie cookie = new Cookie ("teacheremail",emailAddress);
			cookie.setMaxAge(365 * 24 * 60 * 60);
			response.addCookie(cookie);			
		}
		
		TeacherDetail teacherDetail = null;
		TeacherDetail teacherDetail1 = null;
		UserMaster userMaster = null;
		UserMaster userdetail = null;
		HttpSession session = null;
		HttpSession session1 = null;
		String pageRedirect= "redirect:userdashboard.do";
		String flag="epiFlag=on";
		String key = null;
		String id = null;
		List<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
		List<TeacherDetail> lstteacherEmail = null;
		List<UserMaster> listUserMaster = new ArrayList<UserMaster>();
		List<UserMaster> listUserEmail = null;
		List<TableauUserMaster> lsttableauusername = null;

		try 
		{
			boolean teacherloggedIn=false;
			
			/*session1 = request.getSession(false);	//get the previous session if it is exists.
			UserMaster userSessionObject 		= null;
			TeacherDetail teacherSessionObject 	= null;
			String sessionEmailAddress 			= "";
			try{
			
				if(session1.getAttribute("userMaster")!=null){
					userSessionObject 	= 	(UserMaster) session1.getAttribute("userMaster");
					sessionEmailAddress = 	userSessionObject.getEmailAddress();
				} else if(session1.getAttribute("teacherDetail")!=null){
					teacherSessionObject=	(TeacherDetail) session1.getAttribute("teacherDetail");
					sessionEmailAddress	=	teacherSessionObject.getEmailAddress();
				}
				if(!emailAddress.equalsIgnoreCase(sessionEmailAddress)){
					session1.invalidate();
				}
			} catch(Exception exception){
				exception.printStackTrace();
			}
			*/
			session = request.getSession();		//if previous session is not exists then create new session.
							
			
			session.removeAttribute("epiStandalone");
			password = MD5Encryption.toMD5(password);
			

			//listTeacherDetails = teacherDetailDAO.getLogin(emailAddress,password);
			//listUserMaster = userMasterDAO.getLogin(emailAddress,password);
			
			PrintOnConsole.debugPrintln("Login","Try to login for email Id is "+emailAddress);
			listTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
			if(listTeacherDetails!=null && listTeacherDetails.size() > 0)
			{
				PrintOnConsole.debugPrintln("Teacher Login","Teacher is in our TeacherDB, TeacherId: "+listTeacherDetails.get(0).getTeacherId());
				if(!listTeacherDetails.get(0).getPassword().equalsIgnoreCase(password))
				{
					PrintOnConsole.debugPrintln("Teacher Login","Default Teacher password is not matched");
					if(!masterPasswordDAO.isValidateUser("teacher", password))
					{
						listTeacherDetails=new ArrayList<TeacherDetail>();
						PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is not matched");
					}
					else
					{
						teacherloggedIn=true;
						PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is matched");
					}
				}
				else
				{
					teacherloggedIn=true;
					PrintOnConsole.debugPrintln("Teacher Login","Default Teacher password is matched");
				}
			}
			else
			{
				PrintOnConsole.debugPrintln("Teacher Login","Teacher Email "+emailAddress +" is not in our TeacherDB");
			}
			
			if(!teacherloggedIn)
			{
				boolean isAuthorizedUser=false;
				listUserMaster = userMasterDAO.checkUserEmail(emailAddress);
				
				//Only for user entityType=3
				if(listUserMaster!=null && listUserMaster.size()>1)
				{
					if(listUserMaster.get(0).getEntityType()!=null && listUserMaster.get(0).getEntityType()==3)
					{
						String sid = request.getParameter("schoolID")==null? "" : request.getParameter("schoolID").trim();
						Long schoolId = 0L;
						if(!sid.equals(""))
							try{schoolId = Long.parseLong(request.getParameter("schoolID"));}catch(Exception e){}
						
						String schoolName = request.getParameter("schoolName")==null?"":request.getParameter("schoolName");
						
						//Check The Password.
						
						System.out.println("isAuthorizedUser:- "+isAuthorizedUser);
						for(UserMaster master: listUserMaster)
						{
							System.out.println("UserPassword:- "+master.getFirstName()+" "+master.getLastName()+"user Password:- "+master.getPassword()+"and InputPOassword:- "+password+", School:- "+master.getSchoolId().getSchoolName());
							if(master.getPassword().equals(password))
							{
								if(master.getStatus().equalsIgnoreCase("A")){
									System.out.println(":::::::::::Password is matched:::::::");
									isAuthorizedUser=true;
									break;
								}
							}
						}
						
						//Code For Master Password
						if(!isAuthorizedUser)
						{
							System.out.println("Now Check For Master Password");
							isAuthorizedUser = masterPasswordDAO.isValidateUser("user", password);
							System.out.println("Now:- "+isAuthorizedUser);
						}
						
						//login into the Account
						
						boolean isInactive=false;
						if(schoolId!=null && schoolId!=0 && schoolName!=null && !schoolName.equals("") && isAuthorizedUser)
						{
							for(UserMaster master: listUserMaster)
							{
								if(master.getSchoolId().getSchoolId().equals(schoolId) && !isInactive)
								{
									listUserMaster.set(0, master);
									password = master.getPassword();
									break;
								}
							}
						}
					}//End of user entityType=3
					//For Branch Admin check
					else if(listUserMaster.get(0).getEntityType()!=null && listUserMaster.get(0).getEntityType()==6)
					{
						for(UserMaster master: listUserMaster)
						{
							if(master!=null){
								//System.out.println("UserPassword:- "+master.getFirstName()+" "+master.getLastName()+"user Password:- "+master.getPassword()+"and InputPOassword:- "+password+", School:- "+master.getSchoolId().getSchoolName());
								
								System.out.println("*******Branch Name"+master.getBranchMaster().getBranchName()+"**********");
								if(master.getPassword().equals(password))
								{
									System.out.println("******* Required Branch Name"+master.getBranchMaster().getBranchName()+",status="+master.getStatus()+"**********");
									if(master.getStatus().equalsIgnoreCase("A")){
										System.out.println("*&&&&&&Branch User status="+master.getStatus()+"**********");
										System.out.println("%%%%%%%%%%%%%%%%%%%Password is matched%%%%%%%%%%%%%%%%%%%%");
										isAuthorizedUser=true;
										break;
									}
								}
							}
						}
						
						//Code For Master Password
						if(!isAuthorizedUser)
						{
							System.out.println("Now Check For Master Password");
							isAuthorizedUser = masterPasswordDAO.isValidateUser("user", password);
							System.out.println("Now:- "+isAuthorizedUser);
						}
						
						System.out.println("Check for Authorized before Brach admin block=="+isAuthorizedUser);
						long branchId = request.getParameter("branchID")==null?0:Long.parseLong(request.getParameter("branchID"));
						String branchName = request.getParameter("brName")==null?"":request.getParameter("brName");
						
						if(branchId!=0 && !branchName.equals("") && isAuthorizedUser)
						{
							for(UserMaster master: listUserMaster)
							{
								if(master.getBranchMaster().getBranchId()==branchId)
								{
									listUserMaster.set(0, master);
									password = master.getPassword();
									break;
								}
							}
						}
					}//End of user entityType=6
				}
				
				if(listUserMaster!=null && listUserMaster.size() > 0)
				{
					PrintOnConsole.debugPrintln("User Login","User(TM/DA/SA) is in our UserDB, UserId: "+listUserMaster.get(0).getUserId());
					if(!listUserMaster.get(0).getPassword().equalsIgnoreCase(password))
					{
						PrintOnConsole.debugPrintln("User Login","Default user(TM/DA/SA) password is not matched");
						if(listUserMaster.get(0).getEntityType()!=null && listUserMaster.get(0).getEntityType()==1)
						{
							if(!masterPasswordDAO.isValidateUser("tmadmin", password))
							{
								listUserMaster=new ArrayList<UserMaster>();
								PrintOnConsole.debugPrintln("User Login","Master password for user(TM) is not matched");
							}
							else
							{
								PrintOnConsole.debugPrintln("User Login","Master password for user(TM) is matched");
							}
						}
						else
						if(listUserMaster.get(0).getEntityType()!=null && ( listUserMaster.get(0).getEntityType()==2 || listUserMaster.get(0).getEntityType()==3 || listUserMaster.get(0).getEntityType()==5 || listUserMaster.get(0).getEntityType()==6))
						{
							if(!masterPasswordDAO.isValidateUser("user", password))
							{
								listUserMaster=new ArrayList<UserMaster>();
								PrintOnConsole.debugPrintln("User Login","Master password for user(DA/SA) is not matched");
							}
							else
							{
								PrintOnConsole.debugPrintln("User Login","Master password for user(DA/SA) is matched");
							}
						}
					}
					else
					{
						PrintOnConsole.debugPrintln("User Login","Default user(TM/DA/SA) password is matched");
					}
				}
				else
				{
					PrintOnConsole.debugPrintln("User Login","User(TM DA SA) Email "+emailAddress +" is not in our UserDB");
				}
			}


			//System.out.println(" ========== listTeacherDetails  ============"+listTeacherDetails.size()+"==== User master Size rtyrt===== "+listUserMaster.size());
			//System.out.println(" Gagan:===== Teacher Id:"+teacherDetail.getTeacherId()+" Email Address "+teacherDetail.getEmailAddress());
			if(listTeacherDetails.size()>0)
			{
				
				/**
				 *  add by ankit for remove jobId in refrer session attributes for kelly.teachermatch.org url if another teacher is logged In
				 *  Start
				 */
				
				if(listTeacherDetails!=null && listTeacherDetails.size()>0 && listTeacherDetails.get(0).getVerificationStatus()==1 && listTeacherDetails.get(0).getStatus().equals("A")){
					if(request.getRequestURL().toString().contains(kellyUrlValue) && refType.equalsIgnoreCase("R") && (kellyjobId.length()>0 && !kellyjobId.equals("0"))){
						JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(kellyjobId), false, false);
						JobForTeacher jobForTeacher = jobForTeacherDAO.getJobForTeacherDetails(listTeacherDetails.get(0), jobOrder);
						if(jobForTeacher!=null && jobForTeacher.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") && jobOrder.getStatus().equalsIgnoreCase("a") && !(refTeacherId.equalsIgnoreCase(listTeacherDetails.get(0).getTeacherId()+"")))
						{
							System.out.println("========================= "+session.getAttribute("refrer"));
							session.setAttribute("referer", null);
							session.setAttribute("refrer", null);
							System.out.println("============== refrer removed=======");
						}
						else if(jobForTeacher!=null && jobForTeacher.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp") && jobOrder.getStatus().equalsIgnoreCase("a") && (refTeacherId.equalsIgnoreCase(listTeacherDetails.get(0).getTeacherId()+"")))
							map.addAttribute("pShowFlg", 1);
						else if(jobOrder!=null && jobForTeacher==null){
							System.out.println(" teacher exists and try to apply job :: "+kellyjobId);
							Utility.setRefererURL(request);		
							Map<String,String> refrer = (Map<String,String>)session.getAttribute("referer");
							System.out.println("=========referer set");
							System.out.println("================== "+(Map<String,String>)session.getAttribute("referer"));
							session.setAttribute("refrer", refrer);
						}
					}
					else if(request.getRequestURL().toString().contains(kellyUrlValue) && refType.equalsIgnoreCase("n")){
						//if(jobOrder.getStatus().equalsIgnoreCase("a") && (refTeacherId.equalsIgnoreCase(listTeacherDetails.get(0).getTeacherId()+"")))
							map.addAttribute("pShowFlg", 1);
					}
				}
				
				/**
				 * End
				 */
				/***  Session set for Menu List( Teacher ) populate ***/
				
				session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuListForTeacher(request));
				teacherDetail = listTeacherDetails.get(0);
				try{
					//List<TeacherLoginHistory> teacherLoginHistorylst =teacherLoginHistoryDAO.findByTeacherLoginHistory(session.getId());
					//if(teacherLoginHistorylst.size())
					TeacherLoginHistory teacherLoginHistory = new TeacherLoginHistory();
					teacherLoginHistory.setTeacherDetail(teacherDetail);
					teacherLoginHistory.setSessionId(session.getId());
					teacherLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
					teacherLoginHistory.setLoginTime(new Date());
					teacherLoginHistoryDAO.makePersistent(teacherLoginHistory);
				}catch(Exception e){}
				//---------
				key = request.getParameter("key")==null?"":request.getParameter("key");
				id = request.getParameter("id")==null?"":request.getParameter("id");

				//	System.out.println("key"+key);
				//System.out.println("id"+id);

				//---------


				/*if(!key.trim().equals("") && !id.trim().equals(""))
				{

					List<TeacherDetail> lstTDetail = teacherDetailDAO.getTeacherByKeyAndID(key, new Integer(id));
					if(lstTDetail!=null && lstTDetail.size()>0)
					{
						 TeacherDetail tDeatil = lstTDetail.get(0);						 
						 if(tDeatil.getTeacherId().equals(teacherDetail.getTeacherId()))
						 {
							 teacherDetail.setVerificationStatus(1);
							 teacherDetailDAO.makePersistent(teacherDetail);
							 emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Successfully authenticated your TeacherMatch account",MailText.getRegConfirmationEmailToTeacher(request,teacherDetail));
						 }
					 }

				}*/



				//System.out.println(" Gagan:===== Teacher Id:"+teacherDetail.getTeacherId()+" Email Address "+teacherDetail.getEmailAddress());

				if(teacherDetail.getVerificationStatus().equals(0))
				{ 
					map.addAttribute("authmail",false);
					teacherDetail.setPassword(password);
					map.addAttribute("emailAddress",emailAddress);
					map.addAttribute("msgError",""+Utility.getLocaleValuePropByKey("msgAuthContr1", locale)+" <a onclick='return divOn();' href='"+Utility.getBaseURL(request)+"authmail.do?emailAddress="+emailAddress+"'>"+Utility.getLocaleValuePropByKey("lnkClickHere", locale)+"</a> "+Utility.getLocaleValuePropByKey("msgAuthContr2", locale)+"");
					if(request.getRequestURL().toString().contains(kellyUrlValue))
						return "kellysignin";
					else
						return "signin";
				}
				else if(!teacherDetail.getStatus().equalsIgnoreCase("A"))
				{
					teacherDetail.setPassword(password);
					map.addAttribute("authmail",false);
					map.addAttribute("emailAddress",emailAddress);
					map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgAcountInactive", locale));
					if(request.getRequestURL().toString().contains(kellyUrlValue))
						return "kellysignin";
					else
						return "signin";

				}
				else
				{
					//____________________________
					if(teacherDetail!=null && !teacherDetail.getIsPortfolioNeeded()){
						Map<String, String> referer_Job = (HashMap<String, String>) session.getAttribute("referer");
						if(referer_Job!=null){
							String jobId = referer_Job.get("jobId");
							JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
							boolean flagCount=false;
							if(jobOrder!=null){
								if(jobOrder.getJobCategoryMaster()!=null){
									JobCategoryMaster cat=jobOrder.getJobCategoryMaster();
									if(cat.getOfferJSI()!=null && cat.getOfferJSI()==1){
										flagCount=true;
									}
									if(cat.getOfferQualificationItems()!=null && cat.getOfferQualificationItems()){
										flagCount=true;
									}
									if(cat.getOfferDistrictSpecificItems()!=null && cat.getOfferQualificationItems()){
										flagCount=true;
									}
									if(flagCount){
										teacherDetail.setIsPortfolioNeeded(true);
										teacherDetailDAO.makePersistent(teacherDetail);
									}
								}
						    }
					    }
					}
					String loginType = teacherDetail.getUserType()==null?"":teacherDetail.getUserType();
					if(loginType.equals("R"))
					{						
						try 
						{
							Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
							if(referer!=null)
							{
								String jobId = referer.get("jobId");							
								JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
								List<JobForTeacher> lstJFT = jobForTeacherDAO.findJobByTeacher(teacherDetail);

								if(lstJFT!=null && lstJFT.size()!=0)
								{
									if(jobOrder.getDistrictMaster()!=null && lstJFT.get(0).getJobId().getDistrictMaster()!=null && !lstJFT.get(0).getJobId().getDistrictMaster().getDistrictId().equals(jobOrder.getDistrictMaster().getDistrictId()))
									{
										teacherDetail.setUserType("N");
									}
								}								
							}
							else
							{
								//if(!teacherDetail.getNoOfLogin().equals(0))
								//	teacherDetail.setUserType("N");
							}

							Calendar c = Calendar.getInstance();
							c.setTime(teacherDetail.getCreatedDateTime());
							c.add(Calendar.DATE, 30);
							Date exclDate = c.getTime();
							Date currentDate = new Date();
							if(currentDate.compareTo(exclDate)>0 || currentDate.compareTo(exclDate)==0)
							{
								teacherDetail.setUserType("N");
							}
							else
							{

							}
						} 
						catch (Exception e) 
						{
							teacherDetail.setUserType("R");
							e.printStackTrace();
						}
					}

					/*Cookie[] cookies = request.getCookies();
					for(int i=0;i<cookies.length;i++)
					{									
						if(cookies[i].getName().equals("slotUrl") && cookies[i].getValue()!=null)
						{	
							String forMated = Utility.decodeBase64(cookies[i].getValue());
							String arr[] = forMated.split("###");						
							String emailAddresss=arr[1];
							if(emailAddresss.equalsIgnoreCase(emailAddress))
							{
								System.out.println(" Cookie value for slot url :: "+cookies[i].getValue());
								String slotVal=cookies[i].getValue();				
								map.addAttribute("id", slotVal);
								pageRedirect= "redirect:slotselection.do";
							}
							else
							{
								pageRedirect= "redirect:userdashboard.do";
							}
							
						}
					}*/
					
					String isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated"));
					String staffType =(String)(session.getAttribute("staffType")==null?"N":session.getAttribute("staffType"));
					String zipCodeCoverLtr =(String)(session.getAttribute("zipCodeCoverLtr")==null?"":session.getAttribute("zipCodeCoverLtr"));
					String empNumCoverLtr =(String)(session.getAttribute("empNumCoverLtr")==null?"":session.getAttribute("empNumCoverLtr"));
					
					String jobId = null;
					int isaffilatedstatus=0;
					if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded()){
						Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
						System.out.println("referer:: 111 "+referer);
						if(session.getAttribute("referer")!=null)
						{
							jobId = referer.get("jobId");
							System.out.println("referer:: 222 "+referer);
							isaffilatedstatus=checkIsAffilated(request,teacherDetail,Integer.parseInt(jobId), Integer.parseInt(isAffilated));
							map.addAttribute("savejobFlag",1); // savejobFlag is only for saving this job record in job for teacher table. 
							// ok_cancelflag is the value isAffilated value for latest applied job.
							map.addAttribute("ok_cancelflag", Integer.parseInt(isAffilated));
							map.addAttribute("jobId", Integer.parseInt(jobId));
							map.addAttribute("stp", staffType);
							
														
							JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
							
							if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(804800)){
							TeacherPersonalInfo teacherPersonalInfo = teacherPersonalInfoDAO.findById(teacherDetail.getTeacherId(), false, false);
							
							if(teacherPersonalInfo==null){
								teacherPersonalInfo = new TeacherPersonalInfo();
							}							
							
								teacherPersonalInfo.setTeacherId(teacherDetail.getTeacherId());
								teacherPersonalInfo.setFirstName(teacherDetail.getFirstName());
								teacherPersonalInfo.setLastName(teacherDetail.getLastName());
								teacherPersonalInfo.setIsDone(false);
								teacherPersonalInfo.setCreatedDateTime(new Date());
								if(isAffilated!=null && isAffilated.equalsIgnoreCase("1")){
									teacherPersonalInfo.setEmployeeType(1);
								}
								teacherPersonalInfo.setEmployeeNumber(empNumCoverLtr);
								teacherPersonalInfo.setZipCode(zipCodeCoverLtr);
								teacherPersonalInfoDAO.makePersistent(teacherPersonalInfo);
							}
							
							
						}
					}
					else if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded().equals(false) && session.getAttribute("referer")!=null){
						Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
						jobId = referer.get("jobId");
						JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
						if(jobOrder!=null && jobOrder.getIsPortfolioNeeded().equals(false)){
						}				
					}

					//____________________________
					TeacherPreference preference = teacherPreferenceDAO.findPrefByTeacher(teacherDetail);

					/*======= Checkin forgetCounter flag to restrict Teacher ===== */
					boolean isKellyJobApply = false;
					boolean isJobApply=true;
					if(teacherDetail.getForgetCounter()<3) 
					{
				      try{
				         isKellyJobApply = jobForTeacherDAO.getJFTByTeachers(teacherDetail);
				      }catch (Exception e) {
				          e.printStackTrace();
				      }
						
						try
						{
							Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
							if(referer!=null)
							{
								jobId = referer.get("jobId");
								JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
								
								if(!isKellyJobApply){
									 isJobApply=jobForTeacherDAO.isJobApply(teacherDetail);
								}
								if(!isJobApply && jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null){
									isKellyJobApply=true; 
								}
								try
                                {
									int districtId=0;
									try{
										districtId=Integer.parseInt(Utility.getValueOfPropByKey("Dallas_ISD_ID"));
										//System.out.println("job D :: "+jobOrder.getDistrictMaster().getDistrictId() +" districtId :: "+districtId);
									}catch(Exception e){e.printStackTrace();}
                                    if(jobOrder.getDistrictMaster()!=null && (jobOrder.getDistrictMaster().getDistrictId()==1200390 || jobOrder.getDistrictMaster().getDistrictId()==7800043 || jobOrder.getDistrictMaster().getDistrictId()==640740 || jobOrder.getDistrictMaster().getDistrictId()==806240 || jobOrder.getDistrictMaster().getDistrictId()==7800047 || jobOrder.getDistrictMaster().getDistrictId()==1303840 || jobOrder.getDistrictMaster().getDistrictId()==1200420 || jobOrder.getDistrictMaster().getDistrictId()==districtId))
                                    {
                                        if(preference==null)
                                        {
                                            preference = new TeacherPreference();
                                            preference.setTeacherId(teacherDetail);
                                            if(jobOrder.getDistrictMaster().getDistrictId()==districtId){//Dallas ISD
                                            	preference.setGeoId("11");
                                                preference.setRegionId("10");
                                                preference.setSchoolTypeId("2");
                                            }else{
                                            	preference.setGeoId("12");
                                                preference.setRegionId("8");
                                                preference.setSchoolTypeId("1");
                                            }
                                            
                                            preference.setCreatedDateTime(new Date());
                                            teacherPreferenceDAO.makePersistent(preference);
 
                                            if(!jobId.trim().equals(""))
                                            {
                                                if(jobId.trim().length()>0)
                                                    if(Integer.parseInt(jobId)>0)
                                                    {
                                                        JobForTeacher jobForTeacher =  jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
                                                        if(jobForTeacher!=null)
                                                        {
                                                            map.addAttribute("pShowFlg", 1);
                                                            map.addAttribute("savejobFlag", 1);
                                                        }
                                                    }
                                            }
                                        }
                                    }
                                }
                                catch(Exception e)
                                {
                                    e.printStackTrace();
                                }
 							}
						}catch(Exception e)
						{
							e.printStackTrace();
						}
						
						session.setAttribute("isKelly", isKellyJobApply);
						
						if(!teacherDetail.getIsPortfolioNeeded())
						{
							session.setAttribute("epiStandalone", true);
							pageRedirect="redirect:portaldashboard.do";	
							if(isaffilatedstatus==1)
								map.addAttribute("isaffilatedstatus",isaffilatedstatus);
						}
						else if(preference==null)
						{
							session.setAttribute("redirectTopref", true);
							pageRedirect="redirect:userpreference.do";
						}
						else if(isKellyJobApply && ((preference.getStateId()==null || preference.getStateId().trim().equals(""))&&(preference.getJobCategoryId()==null || preference.getJobCategoryId().trim().equals(""))))
						{
							session.setAttribute("redirectTopref", true);
							pageRedirect="redirect:userpreference.do";
						}
						else if(!isKellyJobApply && ((preference.getGeoId()==null || preference.getGeoId().trim().equals(""))&&(preference.getRegionId()==null || preference.getRegionId().trim().equals(""))&&(preference.getSchoolTypeId()==null || preference.getSchoolTypeId().trim().equals(""))))
						{
							session.setAttribute("redirectTopref", true);
							pageRedirect="redirect:userpreference.do";
						}
						if(checkBaseComplete(request,teacherDetail))
						{
							List<JobForTeacher> lstJobForTeacher= null;
							lstJobForTeacher = jobForTeacherDAO.findIncompleteJoborderforwhichBaseIsRequired(teacherDetail,statusMasterDAO.findStatusByShortName("icomp"));
							if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
							{
								if(Integer.parseInt(isAffilated)==0  && session.getAttribute("referer")!=null) 
								{
									Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
									jobId = referer.get("jobId");
									JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
									if(jobOrder!=null && jobOrder.getJobCategoryMaster().getBaseStatus())
									{
										map.addAttribute("epimsjstatus", "icomp");
									}
								}
								else
								{
									if(session.getAttribute("referer")==null && Integer.parseInt(isAffilated)==1)
									{
										map.addAttribute("epimsjstatus", "icomp");
									}
								}
							}

							if(teacherDetail.getIsPortfolioNeeded()==true)
							{
								TeacherPortfolioStatus portfolioStatus = null;
								portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

								if(portfolioStatus!=null && portfolioStatus.getIsPersonalInfoCompleted() && portfolioStatus.getIsAcademicsCompleted() && portfolioStatus.getIsCertificationsCompleted() && portfolioStatus.getIsExperiencesCompleted() && portfolioStatus.getIsAffidavitCompleted())
								{
									map.addAttribute("portfolioStatus", "");
									if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
									{
										if(Integer.parseInt(isAffilated)==0  && session.getAttribute("referer")!=null) 
										{
											pageRedirect=pageRedirect+"?"+flag;
											map.addAttribute("epimsjstatus", "icomp");
										}
										else
										{
											if(session.getAttribute("referer")==null)
											{
												pageRedirect=pageRedirect+"?"+flag;
												map.addAttribute("epimsjstatus", "icomp");
											}
										}
									}
								}
								else
								{
									map.addAttribute("portfolioStatus", "required");
									pageRedirect=pageRedirect+"?"+flag;
									if(lstJobForTeacher.size()>0) /*===== Total No of Job in current financial year whose status  is incomplete ==== */
									{
										if(Integer.parseInt(isAffilated)==0  && session.getAttribute("referer")!=null) 
										{
											map.addAttribute("epimsjstatus", "icomp");
										}
										else
										{
											if(session.getAttribute("referer")==null)
											{
												map.addAttribute("epimsjstatus", "icomp");
											}
										}
									}

								}
								if(isaffilatedstatus==1)
									map.addAttribute("isaffilatedstatus",isaffilatedstatus);

							}
						}
						else
						{
							if(teacherDetail.getIsPortfolioNeeded()==true)
							{
								TeacherPortfolioStatus portfolioStatus = null;
								portfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

								if(portfolioStatus!=null && portfolioStatus.getIsPersonalInfoCompleted() && portfolioStatus.getIsAcademicsCompleted() && portfolioStatus.getIsCertificationsCompleted() && portfolioStatus.getIsExperiencesCompleted() && portfolioStatus.getIsAffidavitCompleted())
								{
								}
								else
								{
								}
							}

							if(isaffilatedstatus==1)
								map.addAttribute("isaffilatedstatus",isaffilatedstatus);
						}
						teacherDetail.setNoOfLogin((teacherDetail.getNoOfLogin())+1);
						teacherDetail.setForgetCounter(0);
						teacherDetailDAO.makePersistent(teacherDetail);
						/// internal candiate check
						List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
						if(itcList.size()>0){
							InternalTransferCandidates itcObj=itcList.get(0);
							if(itcObj.getDistrictMaster()!=null){
								session.setAttribute("districtIdForTeacher",Utility.encryptNo(itcObj.getDistrictMaster().getDistrictId()));
							}
							if(itcList.get(0).isVerificationStatus()){
								session.setAttribute("InterTeacherDetail",true);
							}
						}
						//shadab
						System.out.println(teacherDetail.getTeacherId());
						List<JobForTeacher> jobForTeacherList= jobForTeacherDAO.findJobByTeacherNotKES(teacherDetail);
						List<JobForTeacher> jobForTeacherListForKES= jobForTeacherDAO.findJobByTeacherForKES(teacherDetail,false);
						System.out.println(jobForTeacherList.size()>0);
						System.out.println(jobForTeacherListForKES.size()==0);
						System.out.println(!(jobForTeacherList.size()>0 || jobForTeacherListForKES.size()==0));
						if(!(jobForTeacherList.size()>0 || jobForTeacherListForKES.size()==0))
						{
							request.getSession().setAttribute("powerProfile", "hide");
						}
						
							
						//shadab end
						session.setAttribute("teacherDetail", teacherDetail);
						session.setAttribute("teacherName",	getTeacherName(teacherDetail));
						System.out.println(" Name :: "+	getTeacherName(teacherDetail));
						System.out.println("Teacher Session Details Set====");
						String teacherAssessmentStatus=teacherAssessmentStatusDAO.epiStatus(teacherDetail);
						//System.out.println(" Epi Status :: "+teacherAssessmentStatus);
						session.setAttribute("epistatus", teacherAssessmentStatus);
						
						Cookie[] cookies = request.getCookies();
						for(int i=0;i<cookies.length;i++)
						{									
							if(cookies[i].getName().equals("slotUrl") && cookies[i].getValue()!=null)
							{	
								String forMated = Utility.decodeBase64(cookies[i].getValue());
								String arr[] = forMated.split("###");						
								String emailAddresss=arr[1];
								if(emailAddresss!=null &&  emailAddresss.equalsIgnoreCase(emailAddress))
								{
									System.out.println(" Cookie value for slot url :: "+cookies[i].getValue());
									String slotVal=cookies[i].getValue();				
									map.addAttribute("id", slotVal);
									pageRedirect= "redirect:slotselection.do";
									
									if(preference==null){
										session.removeAttribute("redirectTopref");
									}
									break;
								}
							}
							else if(cookies[i].getName().equals("examSlotUrl") && cookies[i].getValue()!=null)
							{
								String forMated = Utility.decodeBase64(cookies[i].getValue());
								String arr[] = forMated.split("###");	
								String emailAddresss=arr[0];
								if(emailAddresss!=null &&  emailAddresss.equalsIgnoreCase(emailAddress))
								{
									System.out.println(" Cookie value for slot url :: "+cookies[i].getValue());
									String slotVal=cookies[i].getValue();				
									map.addAttribute("id", slotVal);
									pageRedirect= "redirect:examslotselection.do";
									
									if(preference==null){
										session.removeAttribute("redirectTopref");
									}
									break;
								}
							}
							else if(cookies[i].getName().equals("PortfolioUrl") && cookies[i].getValue()!=null)
							{	
								//String forMated = Utility.decodeBase64(cookies[i].getValue());
								//String arr[] = forMated.split("###");						
								//String emailAddresss=arr[1];
								

								Integer districtId = 0;
								Integer tId = 0;
								String p = Utility.decodeBase64(cookies[i].getValue());
								System.out.println("p-->"+p);
								String array[]		=	p.split("&");
								String arrayDist[]	=	array[0].split("=");
								String arrayTeach[]	=	array[1].split("=");
								
								districtId		= 	Utility.decryptNo(Integer.valueOf(arrayDist[1]));
								tId		= 	Utility.decryptNo(Integer.valueOf(arrayTeach[1]));
								System.out.println("districtId-->"+districtId);
								System.out.println("tId-->"+tId);
								TeacherDetail teacher = 	teacherDetailDAO.findById(tId, false, false);
								String emailAddresss=teacher.getEmailAddress().trim();
								System.out.println("emailAddresss-->"+teacher.getEmailAddress());
								
								if(emailAddresss!=null &&  emailAddresss.equalsIgnoreCase(emailAddress))
								{
									System.out.println(" Cookie value for PortfolioUrl  :: "+cookies[i].getValue());
									String slotVal=cookies[i].getValue();				
									map.addAttribute("p", slotVal);
									pageRedirect= "redirect:personalinforeminder.do";
									
									if(preference==null){
										session.removeAttribute("redirectTopref");
									}
									break;
								}
							}	
						}
						
						return pageRedirect;
					}
					else
					{
						map.addAttribute("teacherblockflag", 1);
						if(request.getRequestURL().toString().contains(kellyUrlValue))
							return "kellysignin";
						else
							return "signin";
					}

				}
			}//// user start
			else if(listUserMaster.size()>0)
			{
					userMaster = null;//listUserMaster.get(0);
					
					for(UserMaster user : listUserMaster){
						if(user.getStatus().equalsIgnoreCase("A")){
							userMaster=user;
							break;
						}
					}
					if(userMaster ==null){
						userMaster = listUserMaster.get(0);
					}
					
					
					SchoolMaster schoolMaster = userMaster.getSchoolId();
					if(userMaster.getEntityType()==2)
						userMaster.setSchoolId(null);
					if(userMaster.getIsQuestCandidate()==null || !userMaster.getIsQuestCandidate())
					{
						session.setAttribute("userMaster", userMaster);
						/***  Session set for Menu List populate ***/
						session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuList(request,userMaster));
					}

					if(userMaster.getForgetCounter()<3) 
					{
						if(!userMaster.getStatus().equalsIgnoreCase("A"))
						{
							userMaster.setPassword(password);
							map.addAttribute("authmail",false);
							map.addAttribute("emailAddress",emailAddress);
							map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgAcountInactive", locale));
							if(request.getRequestURL().toString().contains(kellyUrlValue))
								return "kellysignin";
							else
								return "signin";

						}					
						userMaster.setUserId(userMaster.getUserId());
						userMaster.setForgetCounter(0);
						if(userMaster.getEntityType()==1 || userMaster.getEntityType()==5)//TM(admin)
						{
							userMasterDAO.makePersistent(userMaster);
							
							/********add by ankit **********/
							
						    int entityType=userMaster.getEntityType();
							lsttableauusername=tableauUserMasterDAO.getTableauAdminUserNameByEntityType(entityType);
							if(lsttableauusername!=null && lsttableauusername.size()>0)
							{
							System.out.println("Distict user name====="+lsttableauusername.get(0).getTableauUserName());
	    
							HttpSession ses=request.getSession(true);
							ses.setAttribute("email", lsttableauusername.get(0).getTableauUserName());
							String tableau=getTableauUrl(request,response);
							System.out.println("url is="+tableau);
							session.setAttribute("tablueURL", tableau);
							}
						
						/********add end by ankit **********/
							
							if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId().equals(2)&& userMaster.getEntityType()==5)
								return "redirect:tmdashboard.do";
							if(userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId().equals(1) && userMaster.getEntityType()==5)
								return "redirect:kesdashboard.do";
							else
								return "redirect:admindashboard.do";
						}
						else if(userMaster.getEntityType()==2 || userMaster.getRoleId().getRoleId()==6) //District
						{
							if(userMaster.getIsQuestCandidate()==null || !userMaster.getIsQuestCandidate())
							{
								userMaster.setSchoolId(schoolMaster);
								userMasterDAO.makePersistent(userMaster);
								if(userMaster.getEntityType()==2)
									userMaster.setSchoolId(null);
								
								int responseFolder =	 createHomeFolder(request);
								if(userMaster.getEntityType()!=1 && userMaster.getRoleId().getRoleId()==6)
									createDistrictStatusHome(request);
								
								/********add by ankit **********/
								int distId = userMaster.getDistrictId()!=null?userMaster.getDistrictId().getDistrictId():0;
								String tableauStatus=userMaster.getTableauUserStatus();
								System.out.println("District User Status========="+tableauStatus);
								if(tableauStatus != null)
								{
								tableauStatus = tableauStatus.trim();
								if(tableauStatus.equalsIgnoreCase("A"))
								{
									System.out.println("District Id========="+distId);
								lsttableauusername=tableauUserMasterDAO.getTableauDistrictIdByEntityType(distId);
								if(lsttableauusername!=null && lsttableauusername.size()>0)
								{
								System.out.println("Distict user name====="+lsttableauusername.get(0).getTableauUserName());
		
								HttpSession ses=request.getSession(true);
								ses.setAttribute("email", lsttableauusername.get(0).getTableauUserName());
								String tableau=getTableauUrl(request,response);
								System.out.println("url is="+tableau);
								session.setAttribute("tablueURL", tableau);
								}
								}
								}
							   /********add end by ankit **********/
								
								try {
									DistrictChatSupport districtChatSupport=districtChatSupportDAO.getDistrictChatSupportByDistrictId(userMaster.getDistrictId());
									session.setAttribute("districtChatSupport", districtChatSupport);
								} catch (Exception e) {
									e.printStackTrace();
								}
								
								return "redirect:dashboard.do";	
							}
							else
							{
								map.addAttribute("authmail",false);
								map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
								if(request.getRequestURL().toString().contains(kellyUrlValue))
									return "kellysignin";
								else
									return "signin";
							}

						}if(userMaster.getEntityType()==6){
							if(userMaster.getIsQuestCandidate()==null || !userMaster.getIsQuestCandidate())
							{
								userMasterDAO.makePersistent(userMaster);
								int responseFolder =	 createHomeFolder(request);
								return "redirect:kesdashboard.do";	
							}
							else
							{
								map.addAttribute("authmail",false);
								map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
								if(request.getRequestURL().toString().contains(kellyUrlValue))
									return "kellysignin";
								else
									return "signin";
							}

						}
						else if(userMaster.getEntityType()==3)//School
						{
							userMaster.setSchoolId(schoolMaster);
							userMasterDAO.makePersistent(userMaster);
							int responseFolder =	 createHomeFolder(request);
							if(userMaster.getEntityType()!=1)
								createDistrictStatusHome(request);
							
							return "redirect:dashboard.do";
						}
						else
						{
							map.addAttribute("authmail",false);
							map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
							map.addAttribute("teacherblockflag", 1);
							if(request.getRequestURL().toString().contains(kellyUrlValue))
								return "kellysignin";
							else
								return "signin";
						}
					}
					else
					{
						map.addAttribute("teacherblockflag", 1);
						if(request.getRequestURL().toString().contains(kellyUrlValue))
							return "kellysignin";
						else
							return "signin";
					}
				//}
			}
			else
			{
				int forgetCounter=0;
				//password = MD5Encryption.toMD5(password);
				lstteacherEmail = teacherDetailDAO.checkTeacherEmail(emailAddress);
				listUserEmail = userMasterDAO.checkUserEmail(emailAddress);
				if(lstteacherEmail.size()>0)
				{
					try 
					{
						teacherDetail1=teacherDetailDAO.findById(lstteacherEmail.get(0).getTeacherId(), false, false);
						if(teacherDetail1.getForgetCounter()<3)
						{
							forgetCounter=(teacherDetail1.getForgetCounter()+1);
							teacherDetail1.setTeacherId(teacherDetail1.getTeacherId());
							teacherDetail1.setForgetCounter(forgetCounter);
							teacherDetailDAO.makePersistent(teacherDetail1);

							if(forgetCounter==3)
								map.addAttribute("teacherblockflag", 1);
						}
						else
						{
							map.addAttribute("teacherblockflag", 1);
						}
					} catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
				else
				{
					if(listUserEmail.size()>0)
					{
						try 
						{
							userdetail=userMasterDAO.findById(listUserEmail.get(0).getUserId(), false, false);
							if(userdetail.getForgetCounter()<3)
							{
								forgetCounter=(userdetail.getForgetCounter()+1);
								userdetail.setUserId(userdetail.getUserId());
								userdetail.setForgetCounter(forgetCounter);
								userMasterDAO.makePersistent(userdetail);

								if(forgetCounter==3)
									map.addAttribute("teacherblockflag", 1);
							}
							else
							{
								map.addAttribute("teacherblockflag", 1);
							}
						} catch (Exception e) 
						{
							e.printStackTrace();
						}
					}
				}


				map.addAttribute("authmail",false);
				map.addAttribute("emailAddress", emailAddress);
				map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
				if(request.getRequestURL().toString().contains(kellyUrlValue))
					return "kellysignin";
				else
					return "signin";
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("authmail",false);
		map.addAttribute("emailAddress", emailAddress);
		map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
		if(request.getRequestURL().toString().contains(kellyUrlValue))
			return "kellysignin";
		else
			return "signin";
	}
	
	/*****************************/
	public String getTableauUrl(HttpServletRequest request,HttpServletResponse response)
	{
		HttpSession ses=request.getSession(true);
		String email=ses.getAttribute("email").toString();
		//String email="vsharma@teachermatch.org";
		
		System.out.println("Email="+email);
		OutputStreamWriter tableauOutWriter= null;
		BufferedReader in = null;
		String tableauUrl="";
		try
		{
			final String user = email;
			//final String wgserver = "192.168.0.15:8000";
			//final String dst = "site2";
			//final String params = ":embed=yes&:toolbar=yes";
			final String remoteAddr= request.getRemoteAddr();
			 
			System.out.println("inside try method");
			
			StringBuffer data = new StringBuffer();
            data.append(URLEncoder.encode("username", "UTF-8"));
            data.append("=");
            data.append(URLEncoder.encode(user, "UTF-8"));
                       
            System.out.println("data="+data.toString());
         
            URL url = new URL("https://tableau.teachermatch.org/trusted");	
           
                    
            System.out.println("url="+url.toString());
            URLConnection conn =url.openConnection();
            System.out.println("connection has open.");
            
            conn.setDoOutput(true);
            tableauOutWriter = new OutputStreamWriter(conn.getOutputStream());
            System.out.println("data="+data.toString());
            tableauOutWriter.write(data.toString());
            tableauOutWriter.flush();
            
            // Read the response
            StringBuffer rsp = new StringBuffer();
            in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ( (line = in.readLine()) != null) {
                rsp.append(line);
            } 
            System.out.println("response================="+rsp.toString());
            
            if(!rsp.toString().equals("-1"))
            tableauUrl="https://tableau.teachermatch.org/trusted/" + rsp.toString() + "/views";
            else
            	tableauUrl="";
                       
            
		} catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            try {
                if (in != null)
                	in.close();
                if (tableauOutWriter != null)
                	tableauOutWriter.close();
            }
            catch (IOException e) {}
        }
		
	return ""+tableauUrl;
	}
	/*******************/
	
	
	

	public int createHomeFolder(HttpServletRequest request)
	{
		System.out.println(" Basic createHomeFolder ::::::::::::");
		HttpSession session = request.getSession();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		DistrictMaster districtMaster			=	null;
		SchoolMaster schoolMaster				=	null;
		UserFolderStructure userFolder = new UserFolderStructure();
		UserFolderStructure userShareFolder = new UserFolderStructure();
		UserFolderStructure userSavedCandidates = new UserFolderStructure();
		List<UserFolderStructure> lstIsUserHomeCreated=	null;
		try 
		{
			UserMaster userMaster 				=	userMasterDAO.findById(userSession.getUserId(), false, false);
			Criterion criterion1				=	Restrictions.eq("usermaster", userMaster);
			lstIsUserHomeCreated				=	userFolderStructureDAO.findByCriteria(criterion1);
			if(lstIsUserHomeCreated.size()==0)
			{
				userFolder.setUsermaster(userMaster);
				if(userMaster.getBranchMaster()!=null)
					userFolder.setBranchMaster(userMaster.getBranchMaster());
				
				if(userMaster.getHeadQuarterMaster()!=null)
					userFolder.setHeadQuarterMaster(userMaster.getHeadQuarterMaster());
				else if(userMaster.getDistrictId()!=null && userMaster.getEntityType()==2)
					userFolder.setDistrictMaster(userMaster.getDistrictId());
				
				else if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
				{
					userFolder.setDistrictMaster(userMaster.getDistrictId());
					userFolder.setSchoolMaster(userMaster.getSchoolId());
				}
				    /*if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
					 {
				    	
						 userFolder.setSchoolMaster(userMaster.getSchoolId());
					 }
					*/
				
				userFolder.setUserFolderStructure(null);
				userFolder.setIsDefault(1);
				userFolder.setFolderName("My Folders");
				userFolderStructureDAO.makePersistent(userFolder);
            /*==================== Creating First Time shared folder To User =========================================*/				 
				userShareFolder.setUsermaster(userMaster);
				
				if(userMaster.getBranchMaster()!=null)
					userShareFolder.setBranchMaster(userMaster.getBranchMaster());
				else if(userMaster.getHeadQuarterMaster()!=null)
					userShareFolder.setHeadQuarterMaster(userMaster.getHeadQuarterMaster());
				else if(userMaster.getDistrictId()!=null){
					userShareFolder.setDistrictMaster(userMaster.getDistrictId());
				}
				
				if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
				{
					userShareFolder.setSchoolMaster(userMaster.getSchoolId());
				}
				userShareFolder.setUserFolderStructure(userFolder);

				userShareFolder.setIsDefault(1);
				userShareFolder.setFolderName("Received Candidates");

				userFolderStructureDAO.makePersistent(userShareFolder);

				userSavedCandidates.setUsermaster(userMaster);
				
				if(userMaster.getBranchMaster()!=null)
					userSavedCandidates.setBranchMaster(userMaster.getBranchMaster());
				else if(userMaster.getHeadQuarterMaster()!=null)
					userSavedCandidates.setHeadQuarterMaster(userMaster.getHeadQuarterMaster());
				else if(userMaster.getDistrictId()!=null)
					userSavedCandidates.setDistrictMaster(userMaster.getDistrictId());
				
				if(userMaster.getEntityType()==3 && userMaster.getSchoolId()!=null)
				{
					userSavedCandidates.setSchoolMaster(userMaster.getSchoolId());
				}
				userSavedCandidates.setUserFolderStructure(userFolder);

				userSavedCandidates.setIsDefault(1);
				userSavedCandidates.setFolderName("Saved Candidates");

				userFolderStructureDAO.makePersistent(userSavedCandidates);

				return 2;
			}

		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 1;
	}



	public int createDistrictStatusHome(HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		SecondaryStatus secondaryStatus = new SecondaryStatus();
		System.out.println(" Basic createDistrictStatusHome ::::::::::::");

		try 
		{
			Criterion criterion1	=	Restrictions.eq("districtMaster", userSession.getDistrictId());
			int lstIsStatusHomeCreated=	secondaryStatusDAO.checkSecondaryStatus(userSession.getDistrictId());
			if(lstIsStatusHomeCreated==0){
				SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();

				List<StatusNodeMaster> statusNodeMasterList= new ArrayList<StatusNodeMaster>();
				statusNodeMasterList=statusNodeMasterDAO.findStatusNodeList();

				List<StatusMaster> statusMasterList= new ArrayList<StatusMaster>();
				statusMasterList=statusMasterDAO.findStatusNodeList();

				for(StatusNodeMaster statusNodeMaster :statusNodeMasterList){
					secondaryStatus=new SecondaryStatus();
					secondaryStatus.setUsermaster(userSession);
					secondaryStatus.setDistrictMaster(userSession.getDistrictId());
					if(userSession.getEntityType()==3 && userSession.getSchoolId()!=null){
						secondaryStatus.setSchoolMaster(userSession.getSchoolId());
					}
					secondaryStatus.setSecondaryStatusName(statusNodeMaster.getStatusNodeName());
					secondaryStatus.setOrderNumber(statusNodeMaster.getOrderNumber());
					secondaryStatus.setStatus("A");
					secondaryStatus.setStatusNodeMaster(statusNodeMaster);
					secondaryStatus.setCreatedDateTime(new Date());
					statelesSsession.insert(secondaryStatus);
				}
				txOpen.commit();
				statelesSsession.close(); 

				statelesSsession = sessionFactory.openStatelessSession();
				txOpen =statelesSsession.beginTransaction();
				List<SecondaryStatus>	secondaryStatusList=	secondaryStatusDAO.findByCriteria(criterion1);

				SecondaryStatus secondaryStatusObj=new SecondaryStatus();
				for(StatusMaster statusMaster :statusMasterList){
					secondaryStatusObj=getSecondaryStatus(secondaryStatusList,statusMaster.getStatusNodeMaster());
					secondaryStatus=new SecondaryStatus();
					secondaryStatus.setUsermaster(userSession);
					secondaryStatus.setDistrictMaster(userSession.getDistrictId());
					if(userSession.getEntityType()==3 && userSession.getSchoolId()!=null){
						secondaryStatus.setSchoolMaster(userSession.getSchoolId());
					}
					secondaryStatus.setSecondaryStatusName(statusMaster.getStatus());
					secondaryStatus.setOrderNumber(statusMaster.getOrderNumber());
					secondaryStatus.setStatus("A");
					secondaryStatus.setStatusMaster(statusMaster);
					secondaryStatus.setSecondaryStatus(secondaryStatusObj);
					secondaryStatus.setCreatedDateTime(new Date());
					statelesSsession.insert(secondaryStatus);
				}
				txOpen.commit();
				statelesSsession.close(); 
				return 2;
			}

		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 1;
	}

	public SecondaryStatus getSecondaryStatus(List<SecondaryStatus> secondaryStatusList,StatusNodeMaster statusNodeMaster)
	{
		SecondaryStatus secondaryStatusObj= new SecondaryStatus();
		try {
			if(secondaryStatusList.size()>0)
				for(SecondaryStatus secondaryStatus: secondaryStatusList){
					if(secondaryStatus.getStatusNodeMaster().getStatusNodeId().equals(statusNodeMaster.getStatusNodeId())){
						secondaryStatusObj=secondaryStatus;
					}
				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatusObj;
	}
	@RequestMapping(value="/authmail.do", method=RequestMethod.GET)
	public String authorizationMail(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		request.getSession();
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress");
		List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
		if(lstTeacherDetails!=null)
			emailerService.sendMailAsHTMLText(lstTeacherDetails.get(0).getEmailAddress(), "Welcome to TeacherMatch, please confirm your account",MailText.getRegistrationMail(request,lstTeacherDetails.get(0)));
		request.getSession().setAttribute("authmsg", "Please check your email for a new authentication link.");
		map.addAttribute("authmail",true);
		return "signin";
	}

	@RequestMapping(value="/chkemail.do", method=RequestMethod.POST)
 public String isEmailExist(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
 {
		  request.getSession();
		  String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		  List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
		  List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(emailAddress);    
		  response.setContentType("text/html");
		  PrintWriter pw = response.getWriter();

		  if((lstTeacherDetails!=null && lstTeacherDetails.size()>0) || (lstUserMaster!=null && lstUserMaster.size()>0))
		  {
			  if(lstUserMaster.size()>0){
				  if(lstUserMaster.get(0).getEntityType()!=2&&lstUserMaster.get(0).getEntityType()!=3)
				  {
					  pw.println("1");
				  }
				  else if(lstUserMaster.get(0).getEntityType()==2||lstUserMaster.get(0).getEntityType()==3)
				  {
					  pw.println("2");
				  }
			  }
			  
			  if(lstTeacherDetails.size()>0)
				  pw.println("1");
		  }
		  else
		  {
		   pw.println("0");
		  }
		  return null;}

	@RequestMapping(value="/checkemail.do", method=RequestMethod.POST)
	public String isQuestEmailExist(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		HttpSession session = request.getSession();
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		System.out.println("emailAddress :: "+emailAddress);
		List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
		List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(emailAddress);				
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		//String status = "";
		System.out.println(" lstTeacherDetails :: "+lstTeacherDetails.size());
		if((lstTeacherDetails!=null && lstTeacherDetails.size()>0))
		{
			pw.println("1");
			session.setAttribute("teacherDetail", lstTeacherDetails.get(0));
			//status = "1";
			System.out.println("== teacher found===");
		}
		else if((lstUserMaster!=null && lstUserMaster.size()>0))
		{
			pw.println("2");
			//status = "2";
			System.out.println("== user found===");
		}
		else
		{
			pw.println("0");
			//status = "0";
			System.out.println("== no found===");
		}
		return null;
	}

	@RequestMapping(value="/chkCaptcha.do", method=RequestMethod.POST)
	public String chkCaptcha(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();

		String verificationCode="";
		String sumOfCaptchaText = request.getParameter("sumOfCaptchaText")==null?"":request.getParameter("sumOfCaptchaText");

		HttpSession session = request.getSession();
		if(session!=null && session.getAttribute("verification.code")!=null )
		{
			verificationCode = (String)	 session.getAttribute("verification.code");			
			if(verificationCode.trim().equals(sumOfCaptchaText.trim()))
			{
				pw.print("0");
			}
			else
			{
				pw.print("1");
			}
		}
		else
		{
			pw.print("0");
		}
		return null;
	}

	@RequestMapping(value="/verify.do", method=RequestMethod.GET)
	public String doVerifyGET(ModelMap map,HttpServletRequest request){	
		HttpSession ses = request.getSession();
		System.out.println(" Basic verify.do ::::::::::::");
		String reDirectUrl="";
		String url="";
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			String quest = request.getParameter("quest")==null?"":request.getParameter("quest").trim();
			String smartpractices = request.getParameter("smartpractices")==null?"":request.getParameter("smartpractices").trim();
			System.out.println("@@@@@@@@@@@@         "+quest);

			if(quest=="" || quest==null)
			{
				reDirectUrl="redirect:index.jsp";
				url="signin";
			}
			else
			{
				reDirectUrl="redirect:quest.jsp";	
				url="quest";
			}
			if((smartpractices!="" || smartpractices!=null) && smartpractices.equals("true"))
			{
				reDirectUrl="redirect:smartpractices.do";	
				url="redirect:smartpractices.do";
			}
			Integer id = new Integer(request.getParameter("id"));			
			String iId=null;
			try{
				iId = request.getParameter("iId")==null?"":request.getParameter("iId").trim();
			}catch(Exception e){
				e.printStackTrace();
			}

			TeacherDetail teacherDetail = null;
			if(!key.trim().equals("") && id!=0){
				List<TeacherDetail> listTeDetails = teacherDetailDAO.getTeacherByKeyAndIDWithStatus(key, id);
				boolean validFlag=false;
				if(listTeDetails!=null && listTeDetails.size()>0 && (iId==null || iId=="")){	
					validFlag=true;
					teacherDetail = listTeDetails.get(0);						 
					teacherDetail.setVerificationStatus(1);
					teacherDetailDAO.makePersistent(teacherDetail);
					if((smartpractices!="" || smartpractices!=null) && smartpractices.equals("true"))
						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Successfully authenticated your TeacherMatch account",MailText.getRegConfirmationEmailToTeacherForSmartPractices(request,teacherDetail));
					else
						emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Successfully authenticated your TeacherMatch account",MailText.getRegConfirmationEmailToTeacher(request,teacherDetail));
					if(ses!=null)ses.setAttribute("authmsg","Welcome to TeacherMatch! </br></br> Your account has been confirmed. Now you can login to continue.");
					map.addAttribute("authmail",true);
				}
				listTeDetails = teacherDetailDAO.getTeacherByKeyAndID(key, id);
				if(listTeDetails!=null && listTeDetails.size()>0 && iId!=null && iId.equals("543210")){
					teacherDetail = listTeDetails.get(0);
					List<InternalTransferCandidates> interList = internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
					InternalTransferCandidates interObj=null;
					if(interList.size()>0){
						interObj=interList.get(0);
						if(interObj.isVerificationStatus()==false){
							validFlag=true;
							if(teacherDetail.getVerificationStatus()!=null && teacherDetail.getVerificationStatus()==0){
								teacherDetail.setVerificationStatus(1);
								teacherDetailDAO.makePersistent(teacherDetail);							 	
								map.addAttribute("authmail",true);
								if(ses!=null)ses.setAttribute("authmsg","Welcome to TeacherMatch! </br></br> Your account has been confirmed. Now you can login to continue.");
							}else{
								if(ses!=null)ses.setAttribute("authmsg","Welcome to TeacherMatch! </br></br> Your authentication as Internal Transfer Candidate has been confirmed. Now we will send you job opportunities that match the interests you specified during the internal transfer opportunities registration process.");								
								map.addAttribute("authmail",true);
							}
							interObj.setVerificationStatus(true);
							internalTransferCandidatesDAO.makePersistent(interObj);
							// emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Successfully authenticated your TeacherMatch account",MailText.getRegConfirmationEmailToTeacher(request,teacherDetail));
							map.addAttribute("authmail",true);
						}
					}
				}			
				if(validFlag==false){
					map.addAttribute("authmail",false);
				}
				return url;
			}else{				
				return reDirectUrl;
			}

		}catch (Exception e){
			e.printStackTrace();
		}
		return reDirectUrl;
	}

	@RequestMapping(value="/thankyou.do", method=RequestMethod.GET)
	public String doThankyou(ModelMap map,HttpServletRequest request)
	{	
		request.getSession();
		String smartpractices = request.getParameter("smartpractices")==null?"":request.getParameter("smartpractices").trim();
		if(smartpractices!="")
			map.addAttribute("smartpractices", 1);
		
		String isKelly = (request.getParameter("isKelly")==null?"":request.getParameter("isKelly"));
		if(isKelly!="")
			map.addAttribute("isKelly", isKelly);
		
		return "thankyou";
	}
	@RequestMapping(value="/candidate-thank-you.do", method=RequestMethod.GET)
	public String docandidateThankyou(ModelMap map,HttpServletRequest request)
	{	
		request.getSession();
		return "questthankyou";
	}
	@RequestMapping(value="/employer-thank-you.do", method=RequestMethod.GET)
	public String doEmployerThankyou(ModelMap map,HttpServletRequest request)
	{	
		request.getSession();
		return "questthankyou";
	}

	public void saveJobForTeacher(HttpServletRequest request, TeacherDetail teacherDetail)
	{
		System.out.println(" Basic saveJobForTeacher ::::::::::::");
		HttpSession session = request.getSession();

		Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
		String jobBoardReferralURL = session.getAttribute("jobBoardReferralURL")==null?"":(String)session.getAttribute("jobBoardReferralURL");
		if(referer!=null)
		{			
			String jobId = referer.get("jobId");
			String coverLetter = (String)(session.getAttribute("coverLetter")==null?"":session.getAttribute("coverLetter"));
			String isAffilated =(String)(session.getAttribute("isAffilated")==null?"0":session.getAttribute("isAffilated"));
			String staffType =(String)(session.getAttribute("staffType")==null?"N":session.getAttribute("staffType"));			
			jobOrderDAO.findById(new Integer(jobId), false, false);
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false); 
			
			JobForTeacher jobForTeacher = jobForTeacherDAO.findJobByTeacherAndJob(teacherDetail, jobOrder);
			boolean completeFlag=false;						
			if(jobForTeacher==null)
			{
				//TeacherPortfolioStatus teacherPortfolioStatus = teacherPortfolioStatusDAO.findPortfolioStatusByTeacher(teacherDetail);

				jobForTeacher = new JobForTeacher();
				jobForTeacher.setTeacherId(teacherDetail);
				jobForTeacher.setJobId(jobOrder);
				if(jobOrder.getDistrictMaster()!=null){
					jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());	
				}
				jobForTeacher.setCoverLetter(coverLetter);
				jobForTeacher.setStaffType(staffType);
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(614730) && staffType.equalsIgnoreCase("PI")){
					jobForTeacher.setIsAffilated(0);
				}else{
					jobForTeacher.setIsAffilated(Integer.parseInt(isAffilated));
				}
				jobForTeacher.setCreatedDateTime(new Date());
				StatusMaster statusMaster = null;

				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher) ;
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

				List<DistrictSpecificQuestions> districtSpecificQuestions = districtSpecificQuestionsDAO.getDistrictSpecificQuestionBYDistrict(jobForTeacher.getJobId().getDistrictMaster());
				if(districtSpecificQuestions.size()>0)
				{
					statusMaster = statusMasterDAO.findStatusByShortName("icomp");
				}
				try{
					if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}

				jobForTeacher.setRedirectedFromURL(referer.get("refererURL"));
				jobForTeacher.setJobBoardReferralURL(jobBoardReferralURL);
				jobForTeacher.setStatus(statusMaster);
				jobForTeacher.setStatusMaster(statusMaster);
				jobForTeacherDAO.makePersistent(jobForTeacher);	

				try{
					if(completeFlag && jobForTeacher.getStatus()!=null && jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("comp")){
						commonService.futureJobStatus(jobOrder, teacherDetail,jobForTeacher);
					}
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}
	}

	@RequestMapping(value="/forgotpassword.do", method=RequestMethod.GET)
	public String doForgotpwdGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		if(request.getParameter("smartpractices")!=null)
		{
			String smartpractices = request.getParameter("smartpractices")==null?"":request.getParameter("smartpractices").trim();
			session.setAttribute("smartpractices", smartpractices);
		}
		return "forgotpassword";
	}

	@RequestMapping(value="/forgotpassword.do", method=RequestMethod.POST)
	public String doForgotpwdPOST(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession();
		String smartpractices = null;
		try 
		{	
			String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress");

			List<TeacherDetail> lsteacherDetail = teacherDetailDAO.findByEmail(emailAddress);

			List<UserMaster> lstUserMaster = userMasterDAO.findByEmail(emailAddress);
			
			if(session.getAttribute("smartpractices")!=null)
				smartpractices = String.valueOf(session.getAttribute("smartpractices")).trim();

			if( lsteacherDetail.size()==0 && lstUserMaster.size()==0)
			{
				map.addAttribute("emailAddress",emailAddress );
				if(smartpractices!=null && smartpractices!="" && smartpractices.equals("1"))
					map.addAttribute("msgError","We have no registered member with the email address, you entered!<br/> Please check your email address and try again.<br/> If you would like to Sign Up, please <a href='smartpractices.do'>click here</a>" );
				else
					map.addAttribute("msgError","We have no registered member with the email address, you entered!<br/> Please check your email address and try again.<br/> If you would like to Sign Up, please <a href='signup.do'>click here</a>" );
				return "forgotpassword";
			}
			else
			{
				int authorizationkey=(int) Math.round(Math.random() * 2000000);
				if(lsteacherDetail.size()>0)
				{

					TeacherDetail teacherDetail = lsteacherDetail.get(0);
					if(teacherDetail.getVerificationStatus()==1)
					{
						teacherDetail.setVerificationCode(""+authorizationkey);
					}
					teacherDetail.setFgtPwdDateTime(new Date());
					teacherDetailDAO.makePersistent(teacherDetail);
					emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Forgot your TeacherMatch password?",MailText.getFgPwdMailToTeacher(request,teacherDetail));
				}
				else
				{
					UserMaster userMaster  = lstUserMaster.get(0);					
					//userMaster.setAuthenticationCode(""+authorizationkey);
					userMaster.setVerificationCode(""+authorizationkey);
					userMasterDAO.makePersistent(userMaster);

					/* ====== Gagan : Calling HrDetail method to get Hr Information ====*/
					String[] arrHrDetail = commonService.getHrDetailToUser(request,userMaster);

					emailerService.sendMailAsHTMLText(userMaster.getEmailAddress(), "Forgot your TeacherMatch password?",MailText.getFgPwdMailToOtUser(request,userMaster,arrHrDetail));							
				}
				if(smartpractices!=null && smartpractices!="" && smartpractices.equals("1"))
					return "redirect:forgotmsg.do?smartpractices=1";
				else
					return "forgotmsg";
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			if(smartpractices!=null && smartpractices!="" && smartpractices.equals("1"))
				map.addAttribute("msgError","We have no registered member with the email address, you entered!<br/> Please check your email address and try again.<br/> If you would like to Sign Up, please <a href='smartpractices.do'>click here</a>" );
			else
				map.addAttribute("msgError","We have no registered member with the email address, you entered!<br/> Please check your email address and try again.<br/> If you would like to Sign Up, please <a href='signup.do'>click here</a>" );
			return "forgotpassword";
		}


	}
	
	@RequestMapping(value="/forgotmsg.do", method=RequestMethod.GET)
	public String doForgotMsgGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		if(request.getParameter("smartpractices")!=null)
		{
			String smartpractices = request.getParameter("smartpractices")==null?"":request.getParameter("smartpractices").trim();
			map.addAttribute("smartpractices", smartpractices);
		}
		return "forgotmsg";
	}

	@RequestMapping(value="/resetpassword.do", method=RequestMethod.GET)
	public String doRestPasswordGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		try 
		{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			String id = request.getParameter("id")==null?"":request.getParameter("id").trim();

			List<TeacherDetail> listTeacherDetail = teacherDetailDAO.getTeacherByKeyAndID(key, new Integer(id));
			List<UserMaster> listUserMasters = userMasterDAO.getUserByKeyAndID(key, new Integer(id));
			int errorflag=0;
			if(listTeacherDetail.size()==0 && listUserMasters.size()==0)
			{
				errorflag=1;
				System.out.println("errorflag   "+errorflag);
				 //errorMessage="Expired Forgot Password link. Please check your email or request for a new Forgot Password link";
				return "redirect:signin.do?errorMessage="+errorflag+"";
			}
			else
			{
				if(listTeacherDetail.size()>0)
				{
					TeacherDetail tDetail = listTeacherDetail.get(0);

					Date fgtDate = DateUtils.addHours(tDetail.getFgtPwdDateTime(), 72);

					if(fgtDate.compareTo(new Date())>0 )
					{	
						return "resetpassword";					
					}
					else
					{
						return "resetexpmsg";					
					}
				}
				else
				{
					return "resetpassword";
				}
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return "redirect:signin.do";
		}
	}

	@RequestMapping(value="/resetpassword.do", method=RequestMethod.POST)
	public String doRestPasswordPOST(ModelMap map,HttpServletRequest request){
		request.getSession();
		try{
			String key = request.getParameter("key")==null?"":request.getParameter("key").trim();
			String id = request.getParameter("id")==null?"":request.getParameter("id").trim();
			String txtPwd =  request.getParameter("txtPwd")==null?"":request.getParameter("txtPwd");
			String encryptedPWD = MD5Encryption.toMD5(txtPwd);

			TeacherDetail teacherDetail = null;
			UserMaster userMaster = null;

			List<TeacherDetail> listTeacherDetail = teacherDetailDAO.getTeacherByKeyAndID(key, new Integer(id));
			List<UserMaster> listUserMasters = userMasterDAO.getUserByKeyAndID(key, new Integer(id));

			if(listTeacherDetail.size()==0 && listUserMasters.size()==0){	
				return "signin.do";
			}else{
				String emailAddress="";
				if(listTeacherDetail.size()>0){
					teacherDetail = listTeacherDetail.get(0);
					teacherDetail.setPassword(encryptedPWD);
					teacherDetail.setVerificationStatus(1);
					teacherDetail.setForgetCounter(0);
					emailAddress=teacherDetail.getEmailAddress();
					teacherDetailDAO.makePersistent(teacherDetail);
				}else{
						    userMaster =  listUserMasters.get(0);
						    emailAddress= userMaster.getEmailAddress();
						    List<UserMaster> listUsers=new ArrayList<UserMaster>();
						    if(emailAddress!=null)
						      listUsers = userMasterDAO.findByCriteria(Restrictions.eq("emailAddress", emailAddress));
						     if(listUsers!=null && listUsers.size()>0){
						    	    SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
									StatelessSession statelesSsession = sessionFactory.openStatelessSession();
									Transaction txOpen =statelesSsession.beginTransaction();
							      for(UserMaster userData:listUsers){
							    	  userData.setPassword(encryptedPWD);
							    	  userData.setForgetCounter(0);
									 // userMasterDAO.makePersistent(userData);
							    	  statelesSsession.update(userData);
							      }
							    txOpen.commit();
								statelesSsession.close(); 
							      
						     }
				}
				map.addAttribute("emailAddress",emailAddress);
				map.addAttribute("password",txtPwd);
				return "resetsuccess";
			}

		}catch (Exception e){
			e.printStackTrace();
			return null;
		}
	}
	@RequestMapping(value="/applyteacherjobfrommain.do", method=RequestMethod.GET)
	public String doApplyJobForTeacherFormMain(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		System.out.println(" Basic applyteacherjobfrommain.do ::::::::::::");
		String returnData = "";
		PrintWriter pw = null;
		try 
		{	
			pw = response.getWriter();
			HttpSession session = request.getSession();
			String jobId = request.getParameter("jobId")==null?"0":request.getParameter("jobId");
            if(request.getParameter("jobld")!=null)
                return "redirect:applyteacherjob.do?jobId="+request.getParameter("jobld");
			//String batchJobId = request.getParameter("batchJobId")==null?"0":request.getParameter("batchJobId");

			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			//BatchJobOrder batchJobOrder = batchJobOrderDAO.findById(new Integer(batchJobId), false, false);
			String jobStatusFlag = "I";
			if(jobOrder!=null )
			{
				jobStatusFlag = jobOrder.getStatus();

				if(jobStatusFlag.equalsIgnoreCase("A"))
				{
					Date currTime = Utility.getDateWithoutTime();
					if(currTime.equals(jobOrder.getJobStartDate()) || currTime.equals(jobOrder.getJobEndDate()))
						jobStatusFlag="A";
					else if(Utility.between(currTime,jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
					{
						jobStatusFlag="A";
					}
					else 
					{
						if(currTime.before(jobOrder.getJobStartDate()))
						{
							System.out.println("Inactive");
							jobStatusFlag="I";
						}else if(!Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
						{
							System.out.println("expired");
							jobStatusFlag="E";
						}
					}
				}
				System.out.println("jobStatusFlag:::: "+jobStatusFlag);
			}

			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded())
			{
				returnData = "applyjob.do?jobId="+jobOrder.getJobId();
			}

			if(jobStatusFlag.equalsIgnoreCase("a"))
			{
				Utility.setRefererURL(request);		
				Map<String,String> refrer = (Map<String,String>)session.getAttribute("referer");
				System.out.println("=========referer set");
				session.setAttribute("refrer", refrer);
			}else
				session.setAttribute("refrer", null);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		pw.print(returnData);
		//return "applyteacherjob";
		return null;
	}
	@RequestMapping(value="/applyteacherjob.do", method=RequestMethod.GET)
	public String doApplyJobForTeacher(ModelMap map,HttpServletRequest request)
	{
		System.out.println(" Basic applyteacherjob.do ::::::::::::");
		HeadQuarterMaster hQ= null;
		
		try{		
			hQ= headQuarterMasterDAO.findById(2, false, false);
		    if(hQ!=null)
		    {
			System.out.println("Pavan Test "+hQ.getHeadQuarterId());
			map.addAttribute("HQID", hQ.getHeadQuarterId());
		    }
			}
			catch (Exception e) {
				// TODO: handle exception
			}
		try 
		{	
			HttpSession session = request.getSession();
			// kellyUrlValue = "localhost";
			Boolean kellyUrl = false;
			String teacherId="";
			String kellyJobId = "";
			String refType="";
			String branchId = "";
			TeacherDetail teacherDetail1 = null;
			if(request.getRequestURL().toString().contains(kellyUrlValue)){
				kellyUrl = true;
				kellyJobId = request.getParameter("jobId")==null?"0":request.getParameter("jobId");
				map.addAttribute("jobId", kellyJobId);
				refType = request.getParameter("refType")==null?"":request.getParameter("refType");
				map.addAttribute("refType", refType);
				if(refType.length()==0)
					map.addAttribute("refType", "R");
				
				branchId = request.getParameter("branchId")==null?"0":request.getParameter("branchId");
				map.addAttribute("branchId", branchId);
				
				if(branchId.length()==0){
					try {
						JobOrder jobOrder = jobOrderDAO.findById(Integer.parseInt(kellyJobId), false, false);
						if(jobOrder!=null && jobOrder.getBranchMaster()!=null)
							map.addAttribute("branchId", jobOrder.getBranchMaster().getBranchId());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
			
			if(request.getParameterMap().containsKey("teacherId")){
				teacherId = request.getParameter("teacherId");
				if(teacherId.length()>0) {
				try{teacherDetail1 = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
				
				}
				catch (Exception e) {e.printStackTrace();}
				}
			}
			
			map.addAttribute("kellyUrl", kellyUrl);
			map.addAttribute("teacherId", teacherId);
			map.addAttribute("teacherDetail", teacherDetail1);
			String jobId = request.getParameter("jobId")==null?"0":request.getParameter("jobId");
			
			if(request.getParameter("jobld")!=null)
                return "redirect:applyteacherjob.do?jobId="+request.getParameter("jobld");
			
			String batchJobId = request.getParameter("batchJobId")==null?"0":request.getParameter("batchJobId");
			if(jobId.trim().equals("") && batchJobId.trim().equals(""))
			{
				return "redirect:signin.do";
			}		
			String JobBoardURL=null;
			String startDate="";
			String endDate="";
			String positionStart="";

			try{
				JobBoardURL=Utility.getBaseURL(request)+"jobboard.do";
			}catch (Exception e) {
				// TODO: handle exception
			}
			map.addAttribute("JobBoardURL",JobBoardURL);
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			BatchJobOrder batchJobOrder = batchJobOrderDAO.findById(new Integer(batchJobId), false, false);
			String jobStatusFlag = "I";
			
			if(jobOrder!=null )
			{
				startDate = Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobStartDate());
				endDate   = Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate());
				if(jobOrder.getPositionStart()!=null)
				positionStart = jobOrder.getPositionStart();
				else 
					positionStart = "N/A";
				
				map.addAttribute("jobOrder", jobOrder);
				map.addAttribute("startDate",startDate);
				map.addAttribute("endDate",endDate);
				map.addAttribute("positionStart",positionStart);
				boolean isMiami = false;
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(1200390))
					isMiami = true;
				
				List<DistrictPortfolioConfig> lstdistrictPortfolioConfig = districtPortfolioConfigDAO.getPortfolioConfig("E",jobOrder);
				DistrictPortfolioConfig districtPortfolioConfig=null;
				if(lstdistrictPortfolioConfig!=null && lstdistrictPortfolioConfig.size()>0)
				{
					districtPortfolioConfig=lstdistrictPortfolioConfig.get(0);
				}
				boolean isDspqReqForKelly=false;
				boolean isKelly=false;
				boolean isCoverLetterNeeded = false;
				if(districtPortfolioConfig!=null)
				{
					if(districtPortfolioConfig.getCoverLetter())
						isCoverLetterNeeded = true;
					
					if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1 && jobOrder.getIsInviteOnly()!=null && jobOrder.getIsInviteOnly())
						isCoverLetterNeeded = false;
					else if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1)
						isCoverLetterNeeded = true;
					
					if(jobOrder!=null && jobOrder.getHeadQuarterMaster()!=null)
					{
						isKelly=true;
						isDspqReqForKelly=jobOrder.getJobCategoryMaster().getOfferDSPQ();
					}
				}
				
				System.out.println("isCoverLetterNeeded:: "+isCoverLetterNeeded);
				map.addAttribute("isCoverLetterNeeded", isCoverLetterNeeded);
				map.addAttribute("isKelly", isKelly);
				map.addAttribute("isDspqReqForKelly", isDspqReqForKelly);
				

				String jeffcoSpecificSchool="";
				if(jobOrder!=null && jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800")){					
					if(jobOrder.getRequisitionNumber()!=null && !jobOrder.getRequisitionNumber().equalsIgnoreCase("")){
						DistrictRequisitionNumbers districtRequisitionNumbers = districtRequisitionNumbersDAO.getDistrictRequisitionNumber(jobOrder.getDistrictMaster(), jobOrder.getRequisitionNumber());
						if(districtRequisitionNumbers!=null){
							JobRequisitionNumbers jobRequisitionNumbers = jobRequisitionNumbersDAO.findJobReqNumbersObj(districtRequisitionNumbers);
								if(jobRequisitionNumbers!=null){
									jeffcoSpecificSchool = jobRequisitionNumbers.getSchoolMaster().getSchoolName()+"</br>"+getSchoolFullAddress(jobOrder);
								}
							
						}
					}
					
					map.addAttribute("jeffcoSpecificSchool", jeffcoSpecificSchool);
				}
				if(jobOrder!=null && jobOrder.getCreatedForEntity()!=null && jobOrder.getCreatedForEntity()==2 && jobOrder.getDistrictMaster()!=null && !jobOrder.getDistrictMaster().getDistrictId().toString().equalsIgnoreCase("804800"))
				{
					if(jobOrder.getSchool()!=null && jobOrder.getSchool().size()>1)
					{

						String schoolListIds = jobOrder.getSchool().get(0).getSchoolId().toString();
						for(int i=1;i<jobOrder.getSchool().size();i++)
						{
							schoolListIds = schoolListIds+","+jobOrder.getSchool().get(i).getSchoolId().toString();
						}
						jeffcoSpecificSchool="<td> <a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>";
						map.addAttribute("jeffcoSpecificSchool", jeffcoSpecificSchool);
					}
					else if(jobOrder.getSchool().size()==1)
					{
						//jeffcoSpecificSchool=jobOrder.getSchool().get(0).getSchoolName()+"<br>"+jobOrder.getSchool().get(0).getAddress();
						jeffcoSpecificSchool=jobOrder.getSchool().get(0).getSchoolName()+"<br>"+getSchoolFullAddress(jobOrder);
					}
					map.addAttribute("jeffcoSpecificSchool", jeffcoSpecificSchool);
				}
				
				jobStatusFlag = jobOrder.getStatus();
				if(jobStatusFlag.equalsIgnoreCase("A"))
				{
					Date currTime = Utility.getDateWithoutTime();
					if(currTime.equals(jobOrder.getJobStartDate()) || currTime.equals(jobOrder.getJobEndDate()))
						jobStatusFlag="A";
					else if(Utility.between(currTime,jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
					{
						jobStatusFlag="A";
					}
					else 
					{
						if(currTime.before(jobOrder.getJobStartDate()))
						{
							System.out.println("Inactive");
							jobStatusFlag="I";
						}else if(!Utility.checkDateBetweenTwoDate(jobOrder.getJobStartDate(), jobOrder.getJobEndDate()))
						{
							System.out.println("expired");
							jobStatusFlag="E";
						}
					}
				}
				System.out.println("jobStatusFlag:::: "+jobStatusFlag);
			}
			else if(batchJobOrder!=null)
			{
				map.addAttribute("batchJobOrder", batchJobOrder);
			}
			else
			{
					return "redirect:signin.do";
			}	

			DistrictMaster districtMaster = null;
			String source = "";
			String target = "";
			String path = "";

		

// Anurag for display logo for kelly------ strat------
			
			if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId().equals(1)){
				
				boolean isLogoExist=false;
				
				try {
					
					
				 	HeadQuarterMaster headQuarterMaster = jobOrder.getHeadQuarterMaster(); 
					BranchMaster branchMaster = jobOrder.getBranchMaster(); 
					if(branchMaster!=null){
						
						map.addAttribute("branchMaster", branchMaster);

						if(branchMaster.getLogoPath()!=null ){							
							source = Utility.getValueOfPropByKey("branchRootPath")+branchMaster.getBranchId()+"/"+branchMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/branch/"+branchMaster.getBranchId()+"/";

							File targetDir = new File(target);
							if(!targetDir.exists())
								targetDir.mkdirs();
							File sourceFile = new File(source);					        
							File targetFile = new File(targetDir+"/"+sourceFile.getName());

							if(sourceFile.exists())
							{
								FileUtils.copyFile(sourceFile, targetFile);
								path = Utility.getValueOfPropByKey("contextBasePath")+"/branch/"+branchMaster.getBranchId()+"/"+sourceFile.getName();	
								isLogoExist =true;
							}
							map.addAttribute("logoPath", path);
						}
					 
					else{		}
						
					}
					if(!isLogoExist){
					
					map.addAttribute("headQuarterMaster", headQuarterMaster);

						if(headQuarterMaster!=null && headQuarterMaster.getLogoPath()!=null){							
							source = Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/"+headQuarterMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/headquarter/"+headQuarterMaster.getHeadQuarterId()+"/";

							File targetDir = new File(target);
							if(!targetDir.exists())
								targetDir.mkdirs();
							File sourceFile = new File(source);					        
							File targetFile = new File(targetDir+"/"+sourceFile.getName());

							if(sourceFile.exists())
							{
								FileUtils.copyFile(sourceFile, targetFile);
								path = Utility.getValueOfPropByKey("contextBasePath")+"/headquarter/"+headQuarterMaster.getHeadQuarterId()+"/"+sourceFile.getName();						        
							   isLogoExist=true;
							}
							map.addAttribute("logoPath", path);
						}
					 
					else{		}
					}


				} 
				catch (Exception e) {
				}
				
				if(!isLogoExist){
					path = "images/applyfor-job.png";
					map.addAttribute("logoPath", path);
				}
				
			}
		//// Anurag for display logo for kelly------ end------	
			else{
			if(jobOrder.getCreatedForEntity().equals(3)){
				try {
					SchoolMaster schoolMaster = jobOrder.getSchool().get(0);
					map.addAttribute("schoolMaster", schoolMaster);
					map.addAttribute("schoolFullAddress", getSchoolFullAddress(jobOrder));
					if(schoolMaster!=null){
						if(schoolMaster!=null){							
							source = Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/"+schoolMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/school/"+schoolMaster.getSchoolId()+"/";

							File targetDir = new File(target);
							if(!targetDir.exists())
								targetDir.mkdirs();
							File sourceFile = new File(source);					        
							File targetFile = new File(targetDir+"/"+sourceFile.getName());

							if(sourceFile.exists())
							{
								FileUtils.copyFile(sourceFile, targetFile);
								path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+schoolMaster.getSchoolId()+"/"+sourceFile.getName();						        						        
							}
							map.addAttribute("logoPath", path);
						}
					}
					else{

					}
				} 
				catch (Exception e) {
				}
			}
			else{
				try{
					districtMaster = jobOrder.getDistrictMaster();
					map.addAttribute("districtMaster", districtMaster);
					map.addAttribute("districtFullAddress", getDistrictFullAddress(jobOrder));

					if(districtMaster!=null){

						source = Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"/"+"/district/"+districtMaster.getDistrictId()+"/";

						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();

						File targetFile = new File(targetDir+"/"+sourceFile.getName());

						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtMaster.getDistrictId()+"/"+sourceFile.getName();
						}
						map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
				}
			}
			}

			if(!Utility.existsURL(path)){
				path="images/applyfor-job.png";
				map.addAttribute("logoPath", path);
			}
			map.addAttribute("jobOrder", jobOrder);
			TeacherDetail teacherDetail = (TeacherDetail)session.getAttribute("teacherDetail");
			if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded())
			{
				return "redirect:applyjob.do?jobId="+jobOrder.getJobId();				
			}

			////////////////////////////////////////////////////////////////////////////////////////////


			////////////////////////////////////////////////////////////////////////////////////////////
			if(jobStatusFlag.equalsIgnoreCase("a"))
			{
				Utility.setRefererURL(request);		
				Map<String,String> refrer = (Map<String,String>)session.getAttribute("referer");
		//		System.out.println("============== refrer set"+refrer);
				session.setAttribute("refrer", refrer);
			}else
				session.setAttribute("refrer", null);


			StringBuffer jobCertifications = new StringBuffer();
			List<JobCertification> jobCertificationList = jobCertificationDAO.findCertificationByJobOrder(jobOrder);
			if(jobCertificationList.size()>0)
			{
				for (JobCertification jobCertification : jobCertificationList) {
					if(jobCertification.getCertificateTypeMaster()!=null)
						jobCertifications.append(jobCertification.getCertificateTypeMaster().getCertType()+"</br>");
				}
			}

			map.addAttribute("jobCertifications", jobCertifications.toString());
			System.out.println("================== "+(Map<String,String>)session.getAttribute("referer"));
			if(jobOrder!=null && jobOrder.getGradeLevel()!=null)
				map.addAttribute("schoolType", jobOrder.getGradeLevel());
			else
				map.addAttribute("schoolType", null);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "applyteacherjob";
	}
	@RequestMapping(value="/setcl.do", method=RequestMethod.POST)
	public String setCoverLetter(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		String coverLetter = request.getParameter("coverLetter")==null?"":request.getParameter("coverLetter");
		String isAffilated = request.getParameter("isAffilated")==null?"0":request.getParameter("isAffilated");
		String staffType = request.getParameter("staffType")==null?"N":request.getParameter("staffType");
		String empNumber = request.getParameter("empNumCoverLtr")==null?"":request.getParameter("empNumCoverLtr");
		String zipCodeCoverLtr = request.getParameter("zipCodeCoverLtr")==null?"":request.getParameter("zipCodeCoverLtr");
		String dobDay1 = request.getParameter("dobDay1")==null?"":request.getParameter("dobDay1");
		String dobMonth1 = request.getParameter("dobMonth1")==null?"0":request.getParameter("dobMonth1");
		String dobYear1 = request.getParameter("dobYear1")==null?"N":request.getParameter("dobYear1");
		String last4SSN = request.getParameter("last4SSN")==null?"":request.getParameter("last4SSN");
		String dID = request.getParameter("dID")==null?"":request.getParameter("dID");
		
		String kellyJobId = "";
		String refType="";
		String branchId = "";
		try {	
			HttpSession session = request.getSession();
			session.setAttribute("dobDay1", dobDay1);
			session.setAttribute("dobMonth1", dobMonth1);
			session.setAttribute("dobYear1", dobYear1);
			session.setAttribute("last4SSN", last4SSN);
			String empDOB=((dobMonth1.trim().length()==1?("0"+dobMonth1):dobMonth1)+dobDay1+dobYear1);
			session.setAttribute("dob", empDOB);
			String[] empInfo=null;
			boolean flag =true;
			System.out.println("dID========="+dID);
			if(dID!=null && !dID.equals("") && dID.trim().equals("804800")){
				String[] result=new DistrictPortfolioConfigAjax().getDoNotHireResponseForJeffco(empDOB,last4SSN.trim());
				if(result!=null){
					if(result[4]!=null && !result[4].trim().equals("") && result[4].trim().equals("A")){
						flag=false;
						session.setAttribute("isAffilated", "1");
						isAffilated="1";
						session.setAttribute("staffType", "I");
						staffType="I";
						session.setAttribute("empNumCoverLtr", result[3].trim());
						empNumber=result[3].trim();
					}
				}
			}
			session.setAttribute("coverLetter", coverLetter);
			if(flag){
				session.setAttribute("isAffilated", isAffilated);
				session.setAttribute("staffType", staffType);
				session.setAttribute("empNumCoverLtr", empNumber);
			}
			
			session.setAttribute("applyByExUser", true);
			session.setAttribute("zipCodeCoverLtr", zipCodeCoverLtr);
			System.out.println("isAffilated  "+isAffilated);
			System.out.println("zip "+zipCodeCoverLtr);
			System.out.println("emp "+empNumber);
			System.out.println("dobDay1  "+dobDay1);
			System.out.println("dobMonth1 "+dobMonth1);
			System.out.println("dobYear1 "+dobYear1);
			System.out.println("last4SSN "+last4SSN);
			map.addAttribute("y",1);
			
			if(request.getRequestURL().toString().contains(kellyUrlValue)){
				kellyJobId = request.getParameter("kellyJobId")==null?"0":request.getParameter("kellyJobId");
				map.addAttribute("jobId", kellyJobId);
				
				branchId = request.getParameter("branchId")==null?"0":request.getParameter("branchId");
				map.addAttribute("branchId", branchId);
				
				refType = request.getParameter("refType")==null?"N":request.getParameter("refType");
				map.addAttribute("refType", refType);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:signin.do";
	}
	/**
	 * 	added by ankit
	 * @param map
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/chkQuestJobDetails.do", method=RequestMethod.GET)
	public String chkQuestJobDetails(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(" Basic chkQuestJobDetails.do :::::::::::GET:");
		try{
			PrintWriter out = response.getWriter();
			boolean canApply = false;
			String msgCode = "";
			HttpSession session = request.getSession();
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherDetail==null)
			{
				System.out.println(" teacherDetail not available");
				msgCode = "5";
			}
			else
			{
				String jobId = request.getParameter("jobId")==null?"":request.getParameter("jobId");
				JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);


				if(teacherDetail==null){
					canApply=true;
				}
				else if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded()){
					canApply=true;
				}
				else if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded().equals(false) && session.getAttribute("referer")!=null){
					Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
					jobId = referer.get("jobId");
					jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
					if(jobOrder!=null && jobOrder.getIsPortfolioNeeded().equals(false)){
						canApply=true;
					}				
				}
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
				Date dateWithoutTime = sdf.parse(sdf.format(new Date()));

				if(!canApply){
					msgCode="4";
				}
				else if(!jobOrder.getStatus().equalsIgnoreCase("a")){
					msgCode="1";
				}
				else if(jobOrder.getJobStartDate().compareTo(dateWithoutTime)>0){
					msgCode="2";
				}
				else if(jobOrder.getJobEndDate().compareTo(dateWithoutTime)<0){
					msgCode="3";
				}
			}
			out.println(msgCode);	
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="/chkJobDetails.do", method=RequestMethod.GET)
	public String checkJobDetails(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(" Basic chkJobDetails.do :::::::::::GET:");
		try{
			PrintWriter out = response.getWriter();

			HttpSession session = request.getSession();
			TeacherDetail teacherDetail = (TeacherDetail) session.getAttribute("teacherDetail");
			boolean canApply = false;
			String jobId = request.getParameter("jobId")==null?"":request.getParameter("jobId");
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);

			String msgCode = "";
			if(teacherDetail==null){
				canApply=true;
			}
			else if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded()){
				canApply=true;
			}
			else if(teacherDetail!=null && teacherDetail.getIsPortfolioNeeded().equals(false) && session.getAttribute("referer")!=null){
				Map<String, String> referer = (HashMap<String, String>) session.getAttribute("referer");
				jobId = referer.get("jobId");
				jobOrder = jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
				if(jobOrder!=null && jobOrder.getIsPortfolioNeeded().equals(false)){
					canApply=true;
				}				
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");      
			Date dateWithoutTime = sdf.parse(sdf.format(new Date()));

			if(!canApply){
				msgCode="4";
			}
			else if(!jobOrder.getStatus().equalsIgnoreCase("a")){
				msgCode="1";
			}
			else if(jobOrder.getJobStartDate().compareTo(dateWithoutTime)>0){
				msgCode="2";
			}
			else if(jobOrder.getJobEndDate().compareTo(dateWithoutTime)<0){
				msgCode="3";
			}

			out.println(msgCode);	
		} 
		catch (Exception e){
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value="/jobdetails.do", method=RequestMethod.GET)
	public String jobDetails(ModelMap map,HttpServletRequest request)
	{
		System.out.println(" Basic jobdetails.do :::::::::::GET:");
		try 
		{		
			HttpSession session = request.getSession();
			String jobId = request.getParameter("jobId")==null?"0":request.getParameter("jobId");
			String batchJobId = request.getParameter("batchJobId")==null?"0":request.getParameter("batchJobId");
			if(jobId.trim().equals("") && batchJobId.trim().equals(""))
			{
				return "redirect:signin.do";
			}
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobId), false, false);
			BatchJobOrder batchJobOrder = batchJobOrderDAO.findById(new Integer(batchJobId), false, false);
			if(jobOrder!=null )
			{
				map.addAttribute("jobOrder", jobOrder);				
			}
			else if(batchJobOrder!=null)
			{
				map.addAttribute("batchJobOrder", batchJobOrder);
			}
			else
			{
				return "redirect:signin.do";
			}			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}	
		return "jobdetails";
	}


	@RequestMapping(value="/termsofuse.do", method=RequestMethod.GET)
	public String doTermsOfUse(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		return "termsofuse";
	}


	@RequestMapping(value="/teachermatchfaq.do", method=RequestMethod.GET)
	public String faq(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		return "teachermatchfaq";
	}

	@RequestMapping(value="/sendinvitemail.do", method=RequestMethod.GET)
	public String senmail(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		return "sendinvitemail";
	}

	@RequestMapping(value="/prospectsignin.do", method=RequestMethod.GET)
	public String prospectsignin(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		return "prospectsignin";
	}

	@RequestMapping(value="/employer-sign-in.do", method=RequestMethod.GET)
	public String districtsignin(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		return "districtsignin";
	}

	@RequestMapping(value="/jobsboard.do", method=RequestMethod.GET)
	public String doSearchJob(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(" >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> jobsboard.do <<<<<<<<<<<<<<<");
		
		request.getSession();
		Utility.setRefererURLByJobsBoard(request);
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		DistrictMaster districtMaster = null;
		SchoolMaster schoolMaster = null;
		HttpSession session = request.getSession(false);	
		try 
		{	
			String jobBoardSource="";
			String source = "";
			String target = "";
			String path = "";


			String districtId =  "";			
			String schoolId = "";
			String headQuarterId = "";
			String branchId = "";
			
			String talentRef = "";
			String teacherId="";
			String jobsubcategoryy="0";
			
			try {							
				headQuarterId = request.getParameter("hqId")==null?"":request.getParameter("hqId");
				headQuarterId = ""+ Utility.decryptNo(Integer.parseInt(headQuarterId));
			} catch (Exception e) {
			}
			
			try {							
				branchId = request.getParameter("branchId")==null?"":request.getParameter("branchId");
				branchId = ""+ Utility.decryptNo(Integer.parseInt(branchId));
			} catch (Exception e) {
			}
			
			
			try {
				districtId = request.getParameter("districtId")==null?"":request.getParameter("districtId");			
				districtId = ""+ Utility.decryptNo(Integer.parseInt(districtId));	
			} catch (Exception e) {

			}
			try {							
				schoolId = request.getParameter("schoolId")==null?"":request.getParameter("schoolId");
				schoolId = ""+ Utility.decryptNo(Integer.parseInt(schoolId));
			} catch (Exception e) {
			}
			
			try {							
				talentRef = request.getParameter("talentRef")==null?"":request.getParameter("talentRef");
				map.addAttribute("talentRef", talentRef);
				
				teacherId= request.getParameter("teacherId")==null?"":request.getParameter("teacherId");
				map.addAttribute("teacherId", teacherId);
				
				
			} catch (Exception e) {
			}

			session.removeAttribute("headQuarterMaster");
			session.removeAttribute("branchMaster");
			
			session.removeAttribute("schoolMaster");
			session.removeAttribute("districtMaster");
				jobsubcategoryy=(String)session.getAttribute("subjobCategoryIdCheck");
				map.addAttribute("subjobCategoryIdCheck", jobsubcategoryy);
			
			if((schoolId.equals("0")||schoolId.equals("")) && (districtId.equals("0")||districtId.equals("")) && (session.getAttribute("teacherDetail")==null)){
			}
			if(!schoolId.trim().equals("")){
				try {
					schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);
					if(schoolMaster!=null){
						districtMaster = schoolMaster.getDistrictId();
						if(schoolMaster!=null){
							jobBoardSource="1";
							map.addAttribute("schoolMaster", schoolMaster);
							map.addAttribute("districtMaster", districtMaster);
							session.setAttribute("schoolMaster", schoolMaster);
							session.setAttribute("districtMaster", districtMaster);					

							source = Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/"+schoolMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/school/"+schoolMaster.getSchoolId()+"/";

							File targetDir = new File(target);
							if(!targetDir.exists())
								targetDir.mkdirs();
							File sourceFile = new File(source);

							File targetFile = new File(targetDir+"/"+sourceFile.getName());

							if(sourceFile.exists())
							{
								FileUtils.copyFile(sourceFile, targetFile);
								path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+schoolMaster.getSchoolId()+"/"+sourceFile.getName();

							}
							map.addAttribute("logoPath", path);
						}
					}
					else{
					}

				} 
				catch (Exception e) {
				}
			}
			else if(!districtId.equals("")){
				try {
					districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
					if(districtMaster!=null){
						jobBoardSource="2";
						map.addAttribute("districtMaster", districtMaster);
						session.setAttribute("districtMaster", districtMaster);					

						source = Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"/"+"/district/"+districtMaster.getDistrictId()+"/";

						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();

						File targetFile = new File(targetDir+"/"+sourceFile.getName());

						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtMaster.getDistrictId()+"/"+sourceFile.getName();
						}
						map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
				}
			}
			else if(!headQuarterId.equals("")){
				try {
					headQuarterMaster = headQuarterMasterDAO.findById(new Integer(headQuarterId), false, false);
					if(headQuarterMaster!=null){
						jobBoardSource="3";
						map.addAttribute("headQuarterMaster", headQuarterMaster);
						//session.setAttribute("headQuarterMaster", headQuarterMaster);					

						source = Utility.getValueOfPropByKey("headQuarterRootPath")+headQuarterMaster.getHeadQuarterId()+"/"+headQuarterMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"headquarter/"+headQuarterMaster.getHeadQuarterId()+"/";
						System.out.println(" target ::: "+target);

						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();

						File targetFile = new File(targetDir+"/"+sourceFile.getName());

						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							path = Utility.getValueOfPropByKey("contextBasePath")+"/headquarter/"+headQuarterMaster.getHeadQuarterId()+"/"+sourceFile.getName();
						}

						map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
				}
			}
			else if(!branchId.equals("")){
				try {
					branchMaster = branchMasterDAO.findById(new Integer(branchId), false, false);
					if(branchMaster!=null){
						jobBoardSource="4";
						map.addAttribute("branchMaster", branchMaster);
						//session.setAttribute("branchMaster", branchMaster);					
						if(branchMaster.getLogoPath()!=null){
						source = Utility.getValueOfPropByKey("branchRootPath")+branchMaster.getBranchId()+"/"+branchMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"branch/"+branchMaster.getBranchId()+"/";
						}
						else{
						source = Utility.getValueOfPropByKey("headQuarterRootPath")+branchMaster.getHeadQuarterMaster().getHeadQuarterId()+"/"+branchMaster.getHeadQuarterMaster().getLogoPath();
						target = servletContext.getRealPath("/")+"headquarter/"+branchMaster.getHeadQuarterMaster().getHeadQuarterId()+"/";
						}
						
						System.out.println(" target ::: "+target);

						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();

						File targetFile = new File(targetDir+"/"+sourceFile.getName());

						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							if(branchMaster.getLogoPath()!=null){
							path = Utility.getValueOfPropByKey("contextBasePath")+"/branch/"+branchMaster.getBranchId()+"/"+sourceFile.getName();
							}
							else{
								path = Utility.getValueOfPropByKey("contextBasePath")+"/headquarter/"+branchMaster.getHeadQuarterMaster().getHeadQuarterId()+"/"+sourceFile.getName();
							}
						}

						map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
				}
			}
			if(session.getAttribute("logoPath")!=null && (!session.getAttribute("logoPath").equals("")))
				path=(String)session.getAttribute("logoPath");

			if(!Utility.existsURL(path))
			{
				path="images/applyfor-job.png";
				map.addAttribute("logoPath", path);
			}
			List<JobCategoryMaster> jobCategoryMasterlst = null;
			List<JobCategoryMaster> finalList = new ArrayList<JobCategoryMaster>();
			Set<JobCategoryMaster> finalSet = new HashSet<JobCategoryMaster>();
			
			if(jobBoardSource.equals("1") || jobBoardSource.equals("2")){
			//	jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
				jobCategoryMasterlst = jobOrderDAO.findUniqueCateogryByJobOrder(districtMaster,false);
			
				/*for(JobCategoryMaster jj:jobCategoryMasterlst){
					if(jj.getParentJobCategoryId()!=null)
						finalList.add(jj.getParentJobCategoryId());
				}*/
				
				for(JobCategoryMaster jj:jobCategoryMasterlst){
					if(jj.getParentJobCategoryId()==null)
						finalSet.add(jj);
					else if(jj.getParentJobCategoryId()!=null && jj.getParentJobCategoryId().getJobCategoryId()!=null)
					{
						finalSet.add(jj.getParentJobCategoryId());
					}
				}
				
				//	finalList	= jobCategoryMasterDAO.findJobCategoryListByHQAandBranchAndDistrict(jobCategoryMasterlst, null, null, districtMaster);
				if(finalSet!=null && finalSet.size()>0)
				{
					jobCategoryMasterlst.clear();
					jobCategoryMasterlst.addAll(finalSet);
				}
					
			}
			else if(jobBoardSource.equals("3") || jobBoardSource.equals("4")){
				if(branchMaster!=null)
				{
					headQuarterMaster=branchMaster.getHeadQuarterMaster();
				}
			//	jobCategoryMasterlst = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster, null, null);
				jobCategoryMasterlst = jobOrderDAO.findUniqueCateogryByHeadQuarter(headQuarterMaster,branchMaster);
				for(JobCategoryMaster jj:jobCategoryMasterlst){
					if(jj.getParentJobCategoryId()!=null && !finalList.contains(jj.getParentJobCategoryId()))
						finalList.add(jj.getParentJobCategoryId());
				}
				//finalList	= jobCategoryMasterDAO.findJobCategoryListByHQAandBranchAndDistrict(jobCategoryMasterlst,headQuarterMaster,null,null);
				if(finalList!=null && finalList.size()>0)
					jobCategoryMasterlst = finalList;
			}
			/*else if(jobBoardSource.equals("4")){
			//	jobCategoryMasterlst = jobCategoryMasterDAO.findJobCategoryByHQAandBranchAndDistrict(headQuarterMaster, branchMaster, null);
				jobCategoryMasterlst = jobOrderDAO.findJobCategoryByHQAandBranch(headQuarterMaster, branchMaster);
				for(JobCategoryMaster jj:jobCategoryMasterlst){
					if(jj.getParentJobCategoryId()!=null)
						finalList.add(jj.getParentJobCategoryId());
				}
				//finalList	= jobCategoryMasterDAO.findJobCategoryListByHQAandBranchAndDistrict(jobCategoryMasterlst, headQuarterMaster, branchMaster, null);
				if(finalList!=null && finalList.size()>0)
					jobCategoryMasterlst = finalList;
			}*/
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	

			// @ASHish :: List of subject
			List<SubjectMaster> lstSubjectMasters =null;
			if(districtMaster!=null)
			{
				lstSubjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);
			}

			// @AShish :: List of State

			String districtDName="";
			String districtAddress="";
			String address="";
			String state="";
			String city = "";
			String zipcode="";
			String districtPnNo="";
			
			String sHQName="";
			String sBRName="";
			
			if(districtMaster!=null){
				if(districtMaster.getDisplayName()!=null && !districtMaster.getDisplayName().equals("")){
					districtDName=districtMaster.getDisplayName();
				}else{
					districtDName=districtMaster.getDistrictName();
				}
			}
			
			if(headQuarterMaster!=null){
				/*if(headQuarterMaster.getHeadQuarterName()!=null && !headQuarterMaster.getHeadQuarterName().equals("")){
					districtDName=headQuarterMaster.getHeadQuarterName();
				}else{
					districtDName=headQuarterMaster.getHeadQuarterName();
				}
				sHQName=districtDName;*/
			}
			
			if(branchMaster!=null){
				if(branchMaster.getBranchName()!=null && !branchMaster.getBranchName().equals("")){
					districtDName=branchMaster.getBranchName();
				}else{
					districtDName=branchMaster.getBranchName();
				}
				sBRName=districtDName;
			}

			/* @Start
			 * @Ashish Kumar
			 * @Description :: Address*/


			if(districtMaster!=null)
			{
				if(districtMaster.getAddress()!=null && !districtMaster.getAddress().equals(""))
				{
					if(!districtMaster.getCityName().equals(""))
					{
						districtAddress = districtMaster.getAddress()+", ";
					}
					else
					{
						districtAddress = districtMaster.getAddress();
					}
				}
				if(!districtMaster.getCityName().equals(""))
				{
					if(districtMaster.getStateId()!=null && !districtMaster.getStateId().getStateName().equals(""))
					{
						city = districtMaster.getCityName()+", ";
					}else{
						city = districtMaster.getCityName();
					}
				}
				if(districtMaster.getStateId()!=null && !districtMaster.getStateId().getStateName().equals(""))
				{
					if(!districtMaster.getZipCode().equals(""))
					{
						state = districtMaster.getStateId().getStateName()+", ";
					}
					else
					{
						state = districtMaster.getStateId().getStateName();
					}	
				}
				if(!districtMaster.getZipCode().equals(""))
				{
					if(districtMaster.getPhoneNumber()!=null && !districtMaster.getPhoneNumber().equals(""))
					{
						zipcode = districtMaster.getZipCode()+", Phone #: ";
					}
					else
					{
						zipcode = districtMaster.getZipCode();
					}
				}

				if(districtMaster.getPhoneNumber()!=null && !districtMaster.getPhoneNumber().equals("")){
					districtPnNo = districtMaster.getPhoneNumber();
				}

				if(districtAddress !="" || city!="" ||state!="" || zipcode!="" || districtPnNo!=""){
					address = districtAddress+city+state+zipcode+districtPnNo;
				}else{
					address="";
				}
			}


			/* @End 
			 * @Ashish Kumar
			 * @Description :: Address*/
			
			
			/* @Start
			 * @Ankit Sharma
			 * @Description :: HeadQuarter Address*/
			if(headQuarterMaster!=null)
			{
				// comment as per new requirement for kelly
				
				/*if(headQuarterMaster.getAddress()!=null && !headQuarterMaster.getAddress().equals(""))
				{
					if(!headQuarterMaster.getCityName().equals(""))
					{
						districtAddress = headQuarterMaster.getAddress()+", ";
					}
					else
					{
						districtAddress = headQuarterMaster.getAddress();
					}
				}
				if(!headQuarterMaster.getCityName().equals(""))
				{
					if(headQuarterMaster.getStateId()!=null && !headQuarterMaster.getStateId().getStateName().equals(""))
					{
						city = headQuarterMaster.getCityName()+", ";
					}else{
						city = headQuarterMaster.getCityName();
					}
				}
				if(headQuarterMaster.getStateId()!=null && !headQuarterMaster.getStateId().getStateName().equals(""))
				{
					if(!headQuarterMaster.getZipCode().equals(""))
					{
						state = headQuarterMaster.getStateId().getStateName()+", ";
					}
					else
					{
						state = headQuarterMaster.getStateId().getStateName();
					}	
				}
				if(!headQuarterMaster.getZipCode().equals(""))
				{
					if(headQuarterMaster.getPhoneNumber()!=null && !headQuarterMaster.getPhoneNumber().equals(""))
					{
						zipcode = headQuarterMaster.getZipCode()+", Phone #: ";
					}
					else
					{
						zipcode = headQuarterMaster.getZipCode();
					}
				}

				if(headQuarterMaster.getPhoneNumber()!=null && !headQuarterMaster.getPhoneNumber().equals("")){
					districtPnNo = headQuarterMaster.getPhoneNumber();
				}

				if(districtAddress !="" || city!="" ||state!="" || zipcode!="" || districtPnNo!=""){
					address = districtAddress+city+state+zipcode+districtPnNo;
				}else{
					address="";
				}*/
				
			}
			/* @End
			 * @Ankit Sharma
			 * @Description :: HeadQuarter Address*/
			
			
			/* @Start
			 * @Ankit Sharma
			 * @Description :: Branch Address*/
			if(branchMaster!=null)
			{
				headQuarterMaster=branchMaster.getHeadQuarterMaster();
				map.addAttribute("headQuarterMaster", headQuarterMaster);
				
				if(headQuarterMaster.getHeadQuarterName()!=null && !headQuarterMaster.getHeadQuarterName().equals("")){
					sHQName=headQuarterMaster.getHeadQuarterName();
				}else{
					sHQName=headQuarterMaster.getHeadQuarterName();
				}
				
				if(branchMaster.getAddress()!=null && !branchMaster.getAddress().equals(""))
				{
					if(!branchMaster.getCityName().equals(""))
					{
						districtAddress = branchMaster.getAddress()+", ";
					}
					else
					{
						districtAddress = branchMaster.getAddress();
					}
				}
				if(!branchMaster.getCityName().equals(""))
				{
					if(branchMaster.getStateMaster()!=null && !branchMaster.getStateMaster().getStateName().equals(""))
					{
						city = branchMaster.getCityName()+", ";
					}else{
						city = branchMaster.getCityName();
					}
				}
				if(branchMaster.getStateMaster()!=null && !branchMaster.getStateMaster().getStateName().equals(""))
				{
					if(!branchMaster.getZipCode().equals(""))
					{
						state = branchMaster.getStateMaster().getStateName()+", ";
					}
					else
					{
						state = headQuarterMaster.getStateId().getStateName();
					}	
				}
				if(!branchMaster.getZipCode().equals(""))
				{
					if(branchMaster.getPhoneNumber()!=null && !branchMaster.getPhoneNumber().equals(""))
					{
						zipcode = branchMaster.getZipCode()+", Phone #: ";
					}
					else
					{
						zipcode = branchMaster.getZipCode();
					}
				}

				if(branchMaster.getPhoneNumber()!=null && !branchMaster.getPhoneNumber().equals("")){
					districtPnNo = branchMaster.getPhoneNumber();
				}

				if(districtAddress !="" || city!="" ||state!="" || zipcode!="" || districtPnNo!=""){
					address = districtAddress+city+state+zipcode+districtPnNo;
				}else{
					address="";
				}
			}
			/* @End
			 * @Ankit Sharma
			 * @Description :: Branch Address*/
			List<StateMaster> lstState= new ArrayList<StateMaster>();
			lstState = stateMasterDAO.findAllStateByOrder();
			String positionStart="";

			
			map.addAttribute("address", address);
			map.addAttribute("districtDName", districtDName);
			map.addAttribute("lstJobCategoryMasters", jobCategoryMasterlst);
			map.addAttribute("lstSubjectMasters", lstSubjectMasters);
			//	map.addAttribute("lstStateMasters", lstStateMasters);
			map.addAttribute("jobBoardSource", jobBoardSource);
			map.addAttribute("sHQName", sHQName);
			map.addAttribute("sBRName", sBRName);
			map.addAttribute("lstState", lstState);
			
			//Added by Gaurav Kumar
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			String dateStr = format.format(date).toString().substring(0,4);

			dateStr = (Integer.parseInt(dateStr)+1)+"-"+(Integer.parseInt(dateStr)+2)+" "+Utility.getLocaleValuePropByKey("lblSchoolYear", locale);
			List<String> positionStartList = new ArrayList<String>();
			positionStartList.add(Utility.getLocaleValuePropByKey("lblCurrentImmediate", locale));
			positionStartList.add(dateStr);
			//End
			map.addAttribute("positionStartList", positionStartList);
			System.out.println("jobBoardSource "+jobBoardSource);
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		String paramText="";
		if(districtMaster==null && schoolMaster==null && headQuarterMaster==null && branchMaster==null){
			map.addAttribute("redirectTo", "1");			
		}
		return "searchjob";
	}
	// mukesh for searching	
	@RequestMapping(value="/getsearchjob.do", method=RequestMethod.POST)
	public String doSearchJobFilter(ModelMap modelmap,HttpServletRequest request, HttpServletResponse response)
	{	
		System.out.println("Calling getsearchjob.do ");
		StringBuffer sb = new StringBuffer();
		request.getSession();
		try
		{
			Integer geoZoneId=0;
			String zoneId   =  request.getParameter("geoZoneId");
			if(zoneId!=null && zoneId!="")
				geoZoneId =  Integer.parseInt(zoneId);
			
			String headQuarterId =  request.getParameter("headQuarterId")==null?"0":request.getParameter("headQuarterId").trim();
			String branchId =  request.getParameter("branchId")==null?"0":request.getParameter("branchId").trim();
			String districtId =  request.getParameter("districtId")==null?"0":request.getParameter("districtId").trim();
			System.out.println("headQuarterId "+headQuarterId +" branchId "+branchId +" districtId "+districtId);
			
			String schoolId   =  request.getParameter("schoolId")==null?"0":request.getParameter("schoolId").trim();			
			String jobCategoryId =  request.getParameter("jobCategoryId")==null?"0":request.getParameter("jobCategoryId").trim();
			String zipCode =  request.getParameter("zipCode")==null?"0":request.getParameter("zipCode").trim();
			
			String stateId =  request.getParameter("stateId")==null?"0":request.getParameter("stateId").trim();
			
			String noOfRow			=	request.getParameter("noOfRows");
			String pageNo			=	request.getParameter("page");
			String sortOrder		=	request.getParameter("sortOrderStr");
			String sortOrderType	=	request.getParameter("sortOrderType");
			String subjectId		=   request.getParameter("subjectIdList");
			String cityName 		=	request.getParameter("cityName");
			String pageName			=	request.getParameter("pageName");


			//-- get no of record in grid,
			//-- set start and end position

			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			//	int start 			= 	0;
			//	int end 			= 	1000;
			int totalRecord 	=	0;
			//------------------------------------

			/* @Start
			 * @AShish
			 * @Description:: Get subject Ids and subject Object List according ids
			 * */
			ArrayList<Integer> subjectIdList=  new ArrayList<Integer>();
			List<SubjectMaster> ObjSubjectList = new ArrayList<SubjectMaster>();	
			List<String> zipCOdeList	=	new ArrayList<String>();

			//boolean flagForJobList=false;

			List<JobOrder> finalJobOrderList = new ArrayList<JobOrder>();
			//List<JobOrder> finalPoolJobOrderList = new ArrayList<JobOrder>();
			try
			{
				/*if(!districtId.equals("0"))
				{
					DistrictMaster districtMaster = districtMasterDAO.findById(Integer.parseInt(districtId), false, false);*/
				if(pageName.equalsIgnoreCase("internaljobsboard.do")){
					finalJobOrderList = jobOrderDAO.findAllJobForInternalJobBoard();
					System.out.println(" ****** A ****** ");
				}
				else
				{
					System.out.println(" ****** B ****** ");
					
					//finalJobOrderList = jobOrderDAO.findAllJobWithPoolCondition();
					
					try 
					{
						Date dateWithoutTime = Utility.getDateWithoutTime();
						
						Criterion criterion1 = Restrictions.le("jobStartDate",dateWithoutTime);
						Criterion criterion2 = Restrictions.ge("jobEndDate",dateWithoutTime);
						//Criterion criterion3 = Restrictions.ne("isPoolJob",1); 	
						Criterion criterion4 = Restrictions.eq("status","A");
						//Criterion criterion5 = Restrictions.eq("approvalBeforeGoLive",1);
						//Criterion criterion6 = Restrictions.ne("isInviteOnly",true);
						//Criterion criterion7 = Restrictions.ne("isVacancyJob",true);
						
						Criterion criterion8=null;
						Criterion criterion9=null;
						
						if(districtId!=null && !districtId.equals("0") && !districtId.equals(""))
						{
							finalJobOrderList = jobOrderDAO.findAllJobWithPoolCondition();
							System.out.println("*********** D **************");
						}
						else if(branchId!=null && !branchId.equals("0") && !branchId.equals(""))
						{
							int iBranchId=Utility.getIntValue(branchId);
							BranchMaster branchMaster=branchMasterDAO.findById(iBranchId, false, false);
							criterion8=Restrictions.eq("branchMaster",branchMaster);

							//finalJobOrderList = jobOrderDAO.findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6,criterion7,criterion8);
							finalJobOrderList = jobOrderDAO.findByCriteria(criterion1,criterion2,criterion4,criterion8);
							System.out.println("*********** B **************");

							
						}
						else if(headQuarterId!=null && !headQuarterId.equals("0") && !headQuarterId.equals(""))
						{
							int iHeadQuarterId=Utility.getIntValue(headQuarterId);
							HeadQuarterMaster headQuarterMaster=headQuarterMasterDAO.findById(iHeadQuarterId, false, false);
							criterion9=Restrictions.eq("headQuarterMaster",headQuarterMaster);

							//finalJobOrderList = jobOrderDAO.findByCriteria(criterion1,criterion2,criterion3,criterion4,criterion5,criterion6,criterion7,criterion9);
							finalJobOrderList = jobOrderDAO.findByCriteria(criterion1,criterion2,criterion4,criterion9);
							System.out.println("*********** H **************");

						}
						
					} 
					catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
				/*}
				finalJobOrderList.addAll(finalPoolJobOrderList);*/

			}catch(Exception e)
			{
				e.printStackTrace();
			}


			if(!subjectId.equals(null) && !subjectId.equals("")){
				String subIdList[] = subjectId.split(",");
				if(Integer.parseInt(subIdList[0])!=0){
					for(int i=0;i<subIdList.length;i++){
						subjectIdList.add(Integer.parseInt(subIdList[i]));
					}
					try{
						ObjSubjectList = subjectMasterDAO.getSubjectMasterlist(subjectIdList);

					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}

			zipCOdeList = cityMasterDAO.findCityByName(cityName);


		
			/* @End
			 * @AShish
			 * @Description:: Get subject Ids and subject Object List according ids
			 * */			

			SortedMap map = new TreeMap();
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"jobId";
			String sortOrderNoField		=	"jobId";

			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("jobId")&& !sortOrder.equals("jobTitle")&& !sortOrder.equals("schoolName") && !sortOrder.equals("location") && !sortOrder.equals("geoZoneName") && !sortOrder.equals("subjectName") && !sortOrder.equals("jobEndDate")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("jobId"))
				{
					sortOrderNoField="jobId";
				}
				if(sortOrder.equals("jobTitle"))
				{
					sortOrderNoField="jobTitle";
				}
				if(sortOrder.equals("districtName"))
				{
					sortOrderNoField="districtName";
				}
				if(sortOrder.equals("schoolName"))
				{
					sortOrderNoField="schoolName";
				}
				if(sortOrder.equals("location"))
				{
					sortOrderNoField="location";
				}
				if(sortOrder.equals("geoZoneName"))
				{
					sortOrderNoField="geoZoneName";
				}
				if(sortOrder.equals("subjectName"))
				{
					sortOrderNoField="subjectName";
				}
				if(sortOrder.equals("jobEndDate"))
				{
					sortOrderNoField="jobEndDate";
				}				
				
			}	
	
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal="1";
				sortOrderStrVal=Order.desc(sortOrderFieldName);
			}		
			/**End ------------------------------------**/
			
		
			List<JobCategoryMaster> jobcateList = new ArrayList<JobCategoryMaster>();
			//System.out.println("\n ********************************   sortOrderStrVal "+sortOrderStrVal+" ***************************\n");
			PrintWriter out = response.getWriter();
			List<JobOrder> lstJobOrder = null;
			DistrictMaster districtMaster = null;
			SchoolMaster schoolMaster = null;
			boolean isAllCategory = false;
			JobCategoryMaster jobCategoryMaster = null;
			if(jobCategoryId==null || jobCategoryId.equals("") || jobCategoryId.equals("0")){
				isAllCategory = true;				
			} 
			else{
					DistrictMaster disMaster = districtMasterDAO.findByDistrictId(districtId);
					jobcateList = jobCategoryMasterDAO.findAllJobSubCategoryByJobCateIdForList(Integer.parseInt(jobCategoryId), disMaster);
			}

			if((schoolId.equals("0")||schoolId.equals("")) && (districtId.equals("0")||districtId.equals("")) && ((zipCode.equals("0")) || (zipCode.equals(""))) && (cityName.equals("0") || (cityName.equals("")) && ((subjectId.equals("")) ||(subjectId.equals(null)) ))){

				if(isAllCategory){
					lstJobOrder = jobOrderDAO.findSortedJobtoShow(sortOrderStrVal);
				}else{				
					//lstJobOrder = jobOrderDAO.findSortedJobOrderbyJobCategory(sortOrderStrVal,jobCategoryMaster);
					lstJobOrder = jobOrderDAO.findSortedJobOrderbyJobCategory(sortOrderStrVal,jobcateList);
				}
			}
			else if(!zipCode.equals("0") && !zipCode.equals("") && (zipCOdeList==null || zipCOdeList.size()==0)) //@AShish:: Add two condition {zipCOdeList}
			{

				lstJobOrder= new ArrayList<JobOrder>();

				if((schoolId.equals("0")||schoolId.equals("")) && (districtId.equals("0")||districtId.equals(""))){
					List<DistrictMaster> lstDistrictMasters = null;
					List<SchoolMaster> lstSchoolMasters = new ArrayList<SchoolMaster>();		
					List<DistrictSchools> lstDistrictSchool = null;

					lstDistrictMasters = districtMasterDAO.findDistrictByZip(zipCode);
					lstDistrictSchool = districtSchoolsDAO.findByZip(zipCode);
					/*for(DistrictMaster d: lstDistrictMasters){
					}*/

					for(DistrictSchools ds: lstDistrictSchool){

						if(!lstDistrictMasters.contains(ds.getDistrictMaster())){
							lstSchoolMasters.add(ds.getSchoolMaster());
						}	
					}
					for(DistrictMaster district: lstDistrictMasters){
						if(isAllCategory){					
							lstJobOrder.addAll(jobOrderDAO.findSortedJobOrderbyDistrict(sortOrderStrVal,district,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter							
						}
						else{					
							lstJobOrder.addAll(jobOrderDAO.findSortedJobOrderbyDistrict(sortOrderStrVal,district,jobcateList,ObjSubjectList));							
						}
					}
					for(SchoolMaster school: lstSchoolMasters){
						if(isAllCategory)
							lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,school, null,ObjSubjectList));
						else				
							lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,school, jobcateList,ObjSubjectList));
					}
				}
				else if((!schoolId.equals("0")) && (!schoolId.equals(""))){
					try	{
						schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);
						if(zipCode.equals(""+schoolMaster.getZip())){
							if(isAllCategory) 
								lstJobOrder = schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
							else				
								lstJobOrder = schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, jobcateList,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
						}
						else{
							lstJobOrder = new ArrayList<JobOrder>();
						}

					} 
					catch (Exception e) {
						lstJobOrder = new ArrayList<JobOrder>();
					}
				}
				else if((!districtId.equals("0")) || (!districtId.equals(""))){
					List<SchoolMaster> lstSchoolMasters = new ArrayList<SchoolMaster>();		
					List<DistrictSchools> lstDistrictSchool = null;

					try {
						districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
						if(districtMaster.getZipCode().equals(zipCode)){
							if(isAllCategory){					
								lstJobOrder = jobOrderDAO.findSortedJobOrderbyDistrict(sortOrderStrVal,districtMaster,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
							}
							else{					
								lstJobOrder = jobOrderDAO.findSortedJobOrderbyDistrict(sortOrderStrVal,districtMaster,jobcateList,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
							}
						}
						else{
							lstDistrictSchool = districtSchoolsDAO.findByZip(zipCode);
							for(DistrictSchools ds: lstDistrictSchool){
								lstSchoolMasters.add(ds.getSchoolMaster());	
							}
							for(SchoolMaster school: lstSchoolMasters){
								if(isAllCategory)
									lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,school, null,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
								else				
									lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,school, jobcateList,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
							}
						}

					} catch (Exception e) {
						lstJobOrder = new ArrayList<JobOrder>();
					}
				}
			}

			/* @Start
			 * @Ashish Kumar
			 * @Descriptiion :: Search By ZipCode List (City Name) and ZipCode
			 * */

			// Search By Only ZipCode List 
			else if(zipCOdeList!=null && zipCOdeList.size()>0 && (zipCode.equals("0") || zipCode.equals("")))
			{

				lstJobOrder= new ArrayList<JobOrder>();

				if((schoolId.equals("0")||schoolId.equals("")) && (districtId.equals("0")||districtId.equals(""))){
					List<DistrictMaster> lstDistrictMasters = null;
					List<SchoolMaster> lstSchoolMasters = new ArrayList<SchoolMaster>();		
					List<DistrictSchools> lstDistrictSchool = null;

					lstDistrictMasters = districtMasterDAO.findDistrictByZipCodeList(zipCOdeList);
					// Get list by ZipcodeList And DistrictList
					lstDistrictSchool = districtSchoolsDAO.findByZipCodeListAndDistrictList(zipCOdeList,lstDistrictMasters);

					for(DistrictSchools ds: lstDistrictSchool){
						lstSchoolMasters.add(ds.getSchoolMaster());
					}
					for(SchoolMaster school: lstSchoolMasters){
						if(isAllCategory)
							lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,school, null,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
						else				
							lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,school, jobcateList,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
					}
				}
				else if((!schoolId.equals("0")) && (!schoolId.equals(""))){
					try	{
						schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);

						boolean checkForSchool = false; // Check for zipCode 

						for(int i=0;i<zipCOdeList.size();i++){
							if(zipCOdeList.get(i).equals(""+schoolMaster.getZip())){
								checkForSchool = true;
							}
						}

						if(checkForSchool){
							if(isAllCategory)
								lstJobOrder = schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
							else				
								lstJobOrder = schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, jobcateList,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
						}
						else{
							lstJobOrder = new ArrayList<JobOrder>();
						}

					} 
					catch (Exception e) {
						lstJobOrder = new ArrayList<JobOrder>();
					}
				}
				else if((!districtId.equals("0")) || (!districtId.equals(""))){
					List<SchoolMaster> lstSchoolMasters = new ArrayList<SchoolMaster>();		
					List<DistrictSchools> lstDistrictSchool = null;

					try {

						districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);

						lstDistrictSchool = districtSchoolsDAO.findByZipCodeList(zipCOdeList,districtMaster);

						for(DistrictSchools ds: lstDistrictSchool){
							lstSchoolMasters.add(ds.getSchoolMaster());	
						}
						if(isAllCategory)
							lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategoryForZip(sortOrderStrVal,lstSchoolMasters, null,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
						else				
							lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategoryForZip(sortOrderStrVal,lstSchoolMasters, jobcateList,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
					} catch (Exception e) {
						lstJobOrder = new ArrayList<JobOrder>();
					}
				}
			}

			//Search By ZipCode List (City Name) and ZipCode
			else if(zipCOdeList!=null && zipCOdeList.size()>0 && !zipCode.equals("0") && !zipCode.equals(""))
			{

				lstJobOrder= new ArrayList<JobOrder>();
				boolean chechForZipCodeMatch = false; 

				for(int i=0;i<zipCOdeList.size();i++){
					if(zipCode.equals(zipCOdeList.get(i))){
						chechForZipCodeMatch = true;
					}
				}

				if(chechForZipCodeMatch){

					if((schoolId.equals("0")||schoolId.equals("")) && (districtId.equals("0")||districtId.equals(""))){
						List<DistrictMaster> lstDistrictMasters = null;
						List<SchoolMaster> lstSchoolMasters = new ArrayList<SchoolMaster>();		
						List<DistrictSchools> lstDistrictSchool = null;

						lstDistrictSchool = districtSchoolsDAO.findByZip(zipCode);

						for(DistrictSchools ds: lstDistrictSchool){
							lstSchoolMasters.add(ds.getSchoolMaster());
						}
						if(isAllCategory)
							lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategoryForZip(sortOrderStrVal,lstSchoolMasters, null,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
						else				
							lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategoryForZip(sortOrderStrVal,lstSchoolMasters, jobcateList,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
					}
					else if((!schoolId.equals("0")) && (!schoolId.equals(""))){
						try	{

							schoolMaster = schoolMasterDAO.findBySchoolIdAndZipCode(zipCode,new Long(schoolId.trim()));

							if(schoolMaster!=null){
								if(isAllCategory)
									lstJobOrder = schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
								else				
									lstJobOrder = schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, jobcateList,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
							}
						} 
						catch (Exception e) {
							lstJobOrder = new ArrayList<JobOrder>();
						}
					}
					else if((!districtId.equals("0")) || (!districtId.equals(""))){
						List<SchoolMaster> lstSchoolMasters = new ArrayList<SchoolMaster>();		
						List<DistrictSchools> lstDistrictSchool = null;

						try {

							districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
							//System.out.println(" District Name............................"+districtMaster.getDistrictName()+" "+districtMaster.getDistrictId());

							lstDistrictSchool = districtSchoolsDAO.findByZipAndDistrict(districtMaster, zipCode);
							for(DistrictSchools ds: lstDistrictSchool){
								lstSchoolMasters.add(ds.getSchoolMaster());	
							}
							if(isAllCategory)
								lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategoryForZip(sortOrderStrVal,lstSchoolMasters, null,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
							else				
								lstJobOrder.addAll(schoolInJobOrderDAO.findSortedJobBySchoolAndCategoryForZip(sortOrderStrVal,lstSchoolMasters, jobcateList,ObjSubjectList)); //@Ashish :: add  ObjSubjectList parameter
						} catch (Exception e) {
							//System.out.println(e.getMessage());
							lstJobOrder = new ArrayList<JobOrder>();
						}
					}
				}
			}
			/* @End
			 * @Ashish Kumar
			 * @Descriptiion :: Search By ZipCode List (City Name) and ZipCOde
			 * */

			else if((!schoolId.equals("0")) && (!schoolId.equals(""))){
				//System.out.println("If block 3 Search by (SCHOOL)");
				try	{
					schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);
					if(isAllCategory)
						lstJobOrder = schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, null,ObjSubjectList);
					else				
						lstJobOrder = schoolInJobOrderDAO.findSortedJobBySchoolAndCategory(sortOrderStrVal,schoolMaster, jobcateList,ObjSubjectList);
				} 
				catch (Exception e) {
					//System.out.println(e.getMessage());
					lstJobOrder = new ArrayList<JobOrder>();
				}
			}
			else if((!districtId.equals("0")) || (!districtId.equals(""))){
				try {
					//System.out.println("If block 4 Serach by District");
					districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
					//System.out.println("districtMaster>"+districtMaster);
					if(isAllCategory){					
						lstJobOrder = jobOrderDAO.findSortedJobOrderbyDistrict(sortOrderStrVal,districtMaster,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
					}
					else{					
						lstJobOrder = jobOrderDAO.findSortedJobOrderbyDistrict(sortOrderStrVal,districtMaster,jobcateList,ObjSubjectList); //@Ashish :: add  ObjSubjectList parameter
						//System.out.println("Inside else 4");
					}
				} catch (Exception e) {
					//System.out.println(e.getMessage());
					lstJobOrder = new ArrayList<JobOrder>();
				}
			}

			if(lstJobOrder.size()>0)
			{
				//	flagForJobList=true;
				finalJobOrderList.retainAll(lstJobOrder);
			}
			else
			{
				finalJobOrderList = new ArrayList<JobOrder>();
			}
			// @ mukesh sorting Jobs according to the Zone.

			List<JobOrder> listjobOrdersgeoZone = new ArrayList<JobOrder>();
			if(geoZoneId>0)
			{
				GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
				listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);

			}

			if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
			{
				finalJobOrderList.retainAll(listjobOrdersgeoZone);
			}
			if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
			{
				finalJobOrderList.addAll(listjobOrdersgeoZone);

			}
			else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
			{
				finalJobOrderList.retainAll(listjobOrdersgeoZone);
			}	  

			// filter Statewise.

			List<JobOrder> listjobOrdersState = new ArrayList<JobOrder>();
			int iStateId=Utility.getIntValue(stateId);
			if(iStateId>0)
			{
				StateMaster master=stateMasterDAO.findById((long)iStateId, false, false);
				//listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(master);
			}

			if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
			{
				finalJobOrderList.retainAll(listjobOrdersgeoZone);
			}
			if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
			{
				finalJobOrderList.addAll(listjobOrdersgeoZone);

			}
			else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
			{
				finalJobOrderList.retainAll(listjobOrdersgeoZone);
			}
			

			List<JobOrder> sortedlstJobOrder		=	new ArrayList<JobOrder>();
			SortedMap<String,JobOrder>	sortedMap 	= 	new TreeMap<String,JobOrder>();			
			if(sortOrderNoField.equals("jobId"))
			{
				sortOrderFieldName	=	"jobId";
			}
			if(sortOrderNoField.equals("jobTitle"))
			{
				sortOrderFieldName	=	"jobTitle";
			}			
			if(sortOrderNoField.equals("schoolName"))
			{
				sortOrderFieldName	=	"schoolName";
			}
			if(sortOrderNoField.equals("location"))
			{
				sortOrderFieldName	=	"location";
			}
			if(sortOrderNoField.equals("geoZoneName"))
			{
				sortOrderFieldName	=	"geoZoneName";
			}
			if(sortOrderNoField.equals("subjectName"))
			{
				sortOrderFieldName	=	"subjectName";
			}
			if(sortOrderNoField.equals("jobEndDate"))
			{
				sortOrderFieldName	=	"jobEndDate";
			}		
	
			int mapFlag=2;
			for (JobOrder jobOrder : finalJobOrderList){
				String orderFieldName=jobOrder.getStatus();

				if(sortOrderFieldName.equals("jobId")){/*
					orderFieldName=jobOrder.getJobId()+"||"+jobOrder.getJobId();
					sortedMap.put(orderFieldName+"||",jobOrder);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				*/}
				if(sortOrderFieldName.equals("jobTitle")){
					orderFieldName=jobOrder.getJobTitle().toUpperCase()+"||"+jobOrder.getJobId();
					sortedMap.put(orderFieldName+"||",jobOrder);
					if(sortOrderTypeVal.equals("0")){
						mapFlag=0;
					}else{
						mapFlag=1;
					}
				}							
				if(jobOrder.getGeoZoneMaster()!=null){
					orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName();
					if(sortOrderFieldName.equals("geoZoneName")){
						orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("geoZoneName")){
						orderFieldName="0||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}			
				if(sortOrderFieldName.equals("schoolName")){
					if(jobOrder.getCreatedForEntity().equals(3)){
						orderFieldName=jobOrder.getSchool().get(0).getSchoolName()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
					else{
						if(jobOrder.getSchool().size()==1){
							orderFieldName=jobOrder.getSchool().get(0).getSchoolName()+"||"+jobOrder.getJobId();
						}else if(jobOrder.getSchool().size()>1){
							orderFieldName="Many Schools||"+jobOrder.getJobId();
						}else{
							orderFieldName=""+"0000||"+jobOrder.getJobId();
						}
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}				
				if(jobOrder.getDistrictMaster()!=null){
					orderFieldName=jobOrder.getDistrictMaster().getAddress();
					if(sortOrderFieldName.equals("location")){
						orderFieldName=jobOrder.getDistrictMaster().getAddress()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("location")){
						orderFieldName="0||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}


				if(jobOrder.getSubjectMaster()!=null){
					orderFieldName=jobOrder.getSubjectMaster().getSubjectName();
					if(sortOrderFieldName.equals("subjectName")){
						orderFieldName=jobOrder.getSubjectMaster().getSubjectName()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("subjectName")){
						orderFieldName="0||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}	
				if(jobOrder.getJobEndDate()!=null){
					orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jobOrder.getJobEndDate());
					if(sortOrderFieldName.equals("jobEndDate")){
						orderFieldName=jobOrder.getJobEndDate()+"||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}else{
					if(sortOrderFieldName.equals("jobEndDate")){
						orderFieldName="0||"+jobOrder.getJobId();
						sortedMap.put(orderFieldName+"||",jobOrder);
						if(sortOrderTypeVal.equals("0")){
							mapFlag=0;
						}else{
							mapFlag=1;
						}
					}
				}
			}
			
			
			if(mapFlag==1){
				NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
				for (Iterator iter=navig.iterator();iter.hasNext();) {  
					Object key = iter.next(); 
					sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
				} 
			}else if(mapFlag==0){
				Iterator iterator = sortedMap.keySet().iterator();
				while (iterator.hasNext()) {
					Object key = iterator.next();
					sortedlstJobOrder.add((JobOrder) sortedMap.get(key));
				}
			}else{
				sortedlstJobOrder=finalJobOrderList;
			}
			
			totalRecord =sortedlstJobOrder.size();
			
			/**
			 *  for pool jobs 
			 */
			if(sortOrderFieldName.equals("jobId"))
			if(sortedlstJobOrder!=null && sortedlstJobOrder.size()>0){
				for (JobOrder job : sortedlstJobOrder) {
					job.setFitScore(job.getJobId());
				}
				if(sortOrderTypeVal.equalsIgnoreCase("1") && sortOrderNoField.equals("jobId"))
					Collections.sort(sortedlstJobOrder,JobOrder.jobOrderComparatorFitScoreDesc );
				else if(sortOrderTypeVal.equalsIgnoreCase("0") && sortOrderNoField.equals("jobId"))
					Collections.sort(sortedlstJobOrder,JobOrder.jobOrderComparatorFitScoreAsc );	
			}
			
			Collections.sort(sortedlstJobOrder,JobOrder.jobOrderComparatorPoolJob);
	
			if(totalRecord<end)
				end=totalRecord;


			List<JobOrder> lstsortedJobOrder =	sortedlstJobOrder.subList(start,end);
			if(!schoolId.equals(null) && !schoolId.equals(""))
			{
				sb.append("<div style='padding-bottom:10px;'><label>Address</label><div style='line-height: 10px;'>"+schoolMaster.getAddress()+"</div></div>");
			}			
			sb.append("<table width='100%' border='0' id='tblGrid'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");		
			String responseText="";
			responseText=PaginationAndSorting.responseSortingLink("Job ID",sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink("Title",sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink("Zone",sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink("Subject",sortOrderFieldName,"subjectName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>"); 

			responseText=PaginationAndSorting.responseSortingLink("School",sortOrderFieldName,"schoolName",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");			

			responseText=PaginationAndSorting.responseSortingLink("Address",sortOrderFieldName,"location",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");

			responseText=PaginationAndSorting.responseSortingLink("End Date",sortOrderFieldName,"jobEndDate",sortOrderTypeVal,pgNo);
			sb.append("<th  valign='top'>"+responseText+"</th>");

			sb.append("<th valign='top'>Actions/Apply</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			int rowCount = 0;

			String redirectTo = request.getParameter("redirectTo")==null?"":request.getParameter("redirectTo");			
			if(redirectTo.equals("1")){
				finalJobOrderList = new ArrayList<JobOrder>();
			}
			Set<JobOrder> setJobOrder= new LinkedHashSet<JobOrder>(lstsortedJobOrder);
			lstsortedJobOrder = new ArrayList<JobOrder>(new LinkedHashSet<JobOrder>(setJobOrder));
			for(JobOrder jbOrder : lstsortedJobOrder){
				rowCount++;			
				String subName =null;
				if(jbOrder.getSubjectMaster()!=null){
					subName=jbOrder.getSubjectMaster().getSubjectName();
				}else{
					subName="";
				}				
				sb.append("<tr>");
				sb.append("<td>"+jbOrder.getJobId()+"</td>");
				sb.append("<td>"+jbOrder.getJobTitle()+"</td>");

				if(jbOrder.getGeoZoneMaster()!=null){

					if(jbOrder.getGeoZoneMaster().getDistrictMaster()!=null && jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990){
						sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						//sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}else if(jbOrder.getGeoZoneMaster().getDistrictMaster()!=null && jbOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
						//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						sb.append("<td><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}else{
						 int districtIdForBoard=0;
						 if(jbOrder.getDistrictMaster()!=null){
							 districtIdForBoard=jbOrder.getDistrictMaster().getDistrictId();
						 }
						sb.append("<td><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jbOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"',"+districtIdForBoard+");\">"+jbOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
					}
				}else
				{
					sb.append("<td></td>");
				}				
				sb.append("<td>"+subName+"</td>");

				if(jbOrder!=null && jbOrder.getDistrictMaster()!=null && jbOrder.getDistrictMaster().getDistrictId()==1200390)
				{
					sb.append("<td>&nbsp;</td>");	
				}
				else
				{
					if(jbOrder.getCreatedForEntity().equals(3)){
						if(jbOrder.getIsPoolJob()==2){
							sb.append("<td>&nbsp;</td>");	
						}else{
							sb.append("<td>"+jbOrder.getSchool().get(0).getSchoolName()+"</td>");
						}
					}
					else{
						if(jbOrder.getSchool().size()==1){
							if(jbOrder.getIsPoolJob()==2){
								sb.append("<td>&nbsp;</td>");	
							}else{
								sb.append("<td>"+jbOrder.getSchool().get(0).getSchoolName()+"</td>");
							}
						}else if(jbOrder.getSchool().size()>1)
						{

							String schoolListIds = jbOrder.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jbOrder.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jbOrder.getSchool().get(i).getSchoolId().toString();
							}
							sb.append("<td> <a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Schools</a></td>");
						}else
							sb.append("<td>&nbsp;</td>");
					}

				}

				if(jbOrder!=null && jbOrder.getDistrictMaster()!=null && jbOrder.getDistrictMaster().getDistrictId()==1200390)
				{
					sb.append("<td>"+jbOrder.getDistrictMaster().getAddress()+"</td>");	
				}
				else
				{
					if(jbOrder.getSchool().size()>0)
					{
						String schoolAddress="";
						String sAddress="";
						String sstate="";
						String scity = "";
						String szipcode="";

						// Get School address
						if(jbOrder.getSchool().size()==1){
							if(jbOrder.getSchool()!=null)
							{
								if(jbOrder.getSchool().get(0).getAddress()!=null && !jbOrder.getSchool().get(0).getAddress().equals(""))
								{
									if(!jbOrder.getSchool().get(0).getCityName().equals(""))
									{
										schoolAddress = jbOrder.getSchool().get(0).getAddress()+", ";
									}
									else
									{
										schoolAddress = jbOrder.getSchool().get(0).getAddress();
									}
								}
								if(!jbOrder.getSchool().get(0).getCityName().equals(""))
								{
									if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
									{
										scity = jbOrder.getSchool().get(0).getCityName()+", ";
									}else{
										scity = jbOrder.getSchool().get(0).getCityName();
									}
								}
								if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
								{
									if(!jbOrder.getSchool().get(0).getZip().equals(""))
									{
										sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName()+", ";
									}
									else
									{
										sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName();
									}	
								}
								if(!jbOrder.getSchool().get(0).getZip().equals(""))
								{
									szipcode = jbOrder.getSchool().get(0).getZip().toString();
								}

								if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
									sAddress = schoolAddress+scity+sstate+szipcode;
								}else{
									sAddress="";
								}
								if(jbOrder.getIsPoolJob()==2){

									sb.append("<td>"+getDistrictFullAddress(jbOrder)+"</td>");
								}else{
									sb.append("<td>"+sAddress+"</td>");
								}

							}
						}
						else
						{
							String schoolListIds = jbOrder.getSchool().get(0).getSchoolId().toString();
							for(int i=1;i<jbOrder.getSchool().size();i++)
							{
								schoolListIds = schoolListIds+","+jbOrder.getSchool().get(i).getSchoolId().toString();
							}
							sb.append("<td><a href='javascript:void(0);' onclick=showSchoolList('"+schoolListIds+"')>Many Address</a></td>");
						}
					}
					else
					{

						String districtAddress="";
						String address="";
						String state="";
						String city = "";
						String zipcode="";

						//Get district address
						if(jbOrder.getDistrictMaster()!=null)
						{
							if(jbOrder.getDistrictMaster().getAddress()!=null && !jbOrder.getDistrictMaster().getAddress().equals(""))
							{
								if(!jbOrder.getDistrictMaster().getCityName().equals(""))
								{
									districtAddress = jbOrder.getDistrictMaster().getAddress()+", ";
								}
								else
								{
									districtAddress = jbOrder.getDistrictMaster().getAddress();
								}
							}
							if(!jbOrder.getDistrictMaster().getCityName().equals(""))
							{
								if(jbOrder.getDistrictMaster().getStateId()!=null && !jbOrder.getDistrictMaster().getStateId().getStateName().equals(""))
								{
									city = jbOrder.getDistrictMaster().getCityName()+", ";
								}else{
									city = jbOrder.getDistrictMaster().getCityName();
								}
							}
							if(jbOrder.getDistrictMaster().getStateId()!=null && !jbOrder.getDistrictMaster().getStateId().getStateName().equals(""))
							{
								if(!jbOrder.getDistrictMaster().getZipCode().equals(""))
								{
									state = jbOrder.getDistrictMaster().getStateId().getStateName()+", ";
								}
								else
								{
									state = jbOrder.getDistrictMaster().getStateId().getStateName();
								}	
							}
							if(!jbOrder.getDistrictMaster().getZipCode().equals(""))
							{
								zipcode = jbOrder.getDistrictMaster().getZipCode();
							}

							if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
								address = districtAddress+city+state+zipcode;
							}else{
								address="";
							}
						}
						if(jbOrder.getIsPoolJob()==2){
							//address
							sb.append("<td>"+getDistrictFullAddress(jbOrder)+"</td>");
						}else{
							sb.append("<td>"+address+"</td>");
						}
					}
				}

				if(Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate()).equals("Dec 25, 2099"))
					sb.append("<td style='font-size:11px;'>Until filled</td>");
				else
					sb.append("<td style='font-size:11px;'>"+Utility.convertDateAndTimeToUSformatOnlyDate(jbOrder.getJobEndDate())+",11:59 PM CST</td>");

				sb.append("<td>");
				sb.append("<a data-original-title=' Apply Now' rel='tooltip' id='tpApply"+rowCount+"' href='applyteacherjob.do?jobId="+jbOrder.getJobId()+"' ><img src='images/option01.png' style='width:30px; height:25px;'></a>");
				sb.append("<a data-original-title='Share' rel='tooltip' id='tpShare"+rowCount+"' href='sharejob.do?jobId="+jbOrder.getJobId()+"'><img src='images/option04.png' style='width:30px; height:25px;'></a>");
				sb.append("</td>");

				sb.append("</tr>");

			}
			if(finalJobOrderList.size()==0)
			{
				sb.append("<tr>");
				sb.append("<td colspan='6'>");

				sb.append("No record found.");
				sb.append("</td>");	   				
				sb.append("</tr>");
			}			

			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationStringForPanelAjax(request, totalRecord, noOfRow, pageNo));
			out.write(sb.toString());

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value="/saveJobAlert.do", method=RequestMethod.POST)
	public String doSaveJobAlerts(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println(" Basic saveJobAlert.do :::::::::::GET:");
		System.out.println("Controller:BasicController; URL:saveJobAlert.do; RequestedMethodPOST: ;ControllerMethod:doSaveJobAlerts(); --start--");
		System.out.println();
		StringBuffer sb = new StringBuffer();

		try {
			PrintWriter out = response.getWriter();
			String senderName = request.getParameter("senderName")==null?"":request.getParameter("senderName");
			String senderEmail = request.getParameter("senderEmail")==null?"":request.getParameter("senderEmail");
			String districtId = request.getParameter("districtId")==null?"":request.getParameter("districtId");
			String schoolId = request.getParameter("schoolId")==null?"":request.getParameter("schoolId");
			String jobCategoryId = request.getParameter("jobCategoryId")==null?"":request.getParameter("jobCategoryId");
			String zipCode = request.getParameter("zipCode")==null?"":request.getParameter("zipCode");

			SchoolMaster schoolMaster = null;
			DistrictMaster districtMaster = null;
			JobCategoryMaster jobCategoryMaster = null;

			if((!districtId.equals(""))&&(!districtId.equals("0") )){
				districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
			}
			if((!schoolId.equals(""))&&(!schoolId.equals("0") )){
				schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);
			}
			if((!jobCategoryId.equals(""))&&(!jobCategoryId.equals("0") )){
				jobCategoryMaster = jobCategoryMasterDAO.findById(new Integer(jobCategoryId), false, false);
			}

			JobAlerts jobAlerts = new JobAlerts();
			jobAlerts.setSenderName(senderName);
			jobAlerts.setSenderEmail(senderEmail);
			jobAlerts.setDistrictMaster(districtMaster);
			jobAlerts.setSchoolMaster(schoolMaster);
			jobAlerts.setJobCategoryMaster(jobCategoryMaster);
			jobAlerts.setZipCode(zipCode);
			jobAlerts.setIpAddress(IPAddressUtility.getIpAddress(request));
			jobAlerts.setStatus("A");
			jobAlerts.setCreatedtDateTime(new Date());

			jobAlertsDAO.makePersistent(jobAlerts);


			//System.out.println(">senderName"+senderName);
			//System.out.println(">senderEmail"+senderEmail);
			//System.out.println(">districtId"+districtId);
			//System.out.println(">schoolId"+schoolId);
			//System.out.println(">jobCategoryId"+jobCategoryId);
			//System.out.println(">zipCode"+zipCode);
			sb.append("senderName"+senderName);

			out.println(sb.toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;

	}

	@RequestMapping(value="/jobalerts.do", method=RequestMethod.GET)
	public String doJobAlerts(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		request.getSession();
		try {
			System.out.println(" Basic jobalerts.do :::::::::::GET:");

			//System.out.println("Controller:BasicController; URL:jobalerts.do; RequestedMethod POST: ;ControllerMethod:doJobAlerts(); --start--");
			PrintWriter pw= response.getWriter();
			pw.print("Hello Test");

			String emailAddress = "";

			List<JobAlerts> userListJobAlerts = new ArrayList<JobAlerts>();
			JobAlerts userJobAlerts = null;


			List<JobAlerts> lstJobAlerts = jobAlertsDAO.findActiveJobAlert();
			////System.out.println("lstJobAlerts.size()>"+lstJobAlerts.size());
			List<JobOrder> lstJobOrders = jobOrderDAO.getOnlyTodayAciveJob();
			////System.out.println("lstJobOrders.size()>"+lstJobOrders.size());


			if(lstJobAlerts.size()!=0){
				emailAddress = lstJobAlerts.get(0).getSenderEmail();
				userJobAlerts = lstJobAlerts.get(0);
			}
			for(JobAlerts jobAlerts: lstJobAlerts){
				if(emailAddress.equals(jobAlerts.getSenderEmail())){
					userListJobAlerts.add(jobAlerts);
				}
				else{
					sendJobAlertMail(lstJobOrders,userListJobAlerts,request);
					userListJobAlerts = new ArrayList<JobAlerts>();
					userListJobAlerts.add(jobAlerts);
					emailAddress = jobAlerts.getSenderEmail();
					userJobAlerts = jobAlerts;
				}
			}
			if(lstJobAlerts.size()>0){
				sendJobAlertMail(lstJobOrders,userListJobAlerts, request);
			}

			//System.out.println("Controller:BasicController; URL:jobalerts.do; RequestedMethod POST: ;ControllerMethod:doJobAlerts(); --End--");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void sendJobAlertMail(List<JobOrder> lstJobOrders, List<JobAlerts> lstJobAlerts, HttpServletRequest request ){
		//System.out.println("\nMail Send to:");
		//System.out.println("lstJobAlerts.size()"+lstJobAlerts.size());
		//System.out.println("lstJobOrders.size()"+lstJobOrders.size());
		//System.out.println(""+lstJobAlerts.get(0).getSenderEmail()+"");

		List<JobOrder> userListJobOrders = new ArrayList<JobOrder>();

		topLoop:
			for(JobAlerts jobAlerts : lstJobAlerts){
				/*System.out.println("\njobAlerts.getDistrictMaster()"+jobAlerts.getDistrictMaster());
			System.out.println("jobAlerts.getSchoolMaster()"+jobAlerts.getSchoolMaster());
			System.out.println("jobAlerts.getZipCode()"+jobAlerts.getZipCode());
			System.out.println("jobAlerts.getJobCategoryMaster()"+jobAlerts.getJobCategoryMaster());*/

				for(JobOrder jobOrder:lstJobOrders){
					//System.out.println(">>>"+((jobAlerts.getDistrictMaster()==null) && (jobAlerts.getSchoolMaster()==null) && (jobAlerts.getJobCategoryMaster()==null) && (jobAlerts.getZipCode() ==null || jobAlerts.getZipCode().trim().equals(""))));
					if((jobAlerts.getDistrictMaster()==null) && (jobAlerts.getSchoolMaster()==null) && (jobAlerts.getJobCategoryMaster()==null) && (jobAlerts.getZipCode() ==null || jobAlerts.getZipCode().trim().equals(""))){
						//System.out.println("Block 1 All");
						userListJobOrders.addAll(lstJobOrders);
						break topLoop;
					}
					if(jobAlerts.getJobCategoryMaster()==null || jobOrder.getJobCategoryMaster().equals(jobAlerts.getJobCategoryMaster())){
						if((jobAlerts.getSchoolMaster()!=null)){
							//System.out.println("Block 2 School");
							if(jobOrder.getSchool()!=null)
								for(SchoolMaster schoolMaster: jobOrder.getSchool()){
									if(jobAlerts.getSchoolMaster().equals(schoolMaster))
										userListJobOrders.add(jobOrder);
								}
						}
						else if((jobAlerts.getZipCode()!=null && (!jobAlerts.getZipCode().trim().equals("")))){
							//System.out.println("Block 3 Zipcode");

							if(jobAlerts.getDistrictMaster()==null || jobAlerts.getDistrictMaster().equals(jobOrder.getDistrictMaster())){
								//System.out.println("District equals");							
								if( jobOrder.getDistrictMaster().getZipCode().equals(""+jobAlerts.getZipCode())){
									userListJobOrders.add(jobOrder);
								}

								//System.out.println(jobOrder.getSchool().size());
								if(jobOrder.getSchool()!=null)
									for(SchoolMaster schoolMaster: jobOrder.getSchool()){
										//System.out.println(">>>"+schoolMaster.getZip()+"  >> "+jobAlerts.getZipCode());
										if(jobAlerts.getZipCode().trim().equals(""+schoolMaster.getZip())){
											//System.out.println("OK");
											userListJobOrders.add(jobOrder);
										}

									}
							}
						}
						else if((jobAlerts.getDistrictMaster()!=null)){
							//System.out.println("Block 4 District");
							if(jobAlerts.getDistrictMaster().equals(jobOrder.getDistrictMaster()))
								userListJobOrders.add(jobOrder);										
						}
					}				
				}
			}

		//System.out.println("userListJobOrders.size()"+userListJobOrders.size());		
		Set<JobOrder> setJobOrders= new LinkedHashSet<JobOrder>(userListJobOrders);
		userListJobOrders = new ArrayList<JobOrder>(new LinkedHashSet<JobOrder>(setJobOrders));

		//System.out.println("after removing duplicates userListJobOrders.size()"+userListJobOrders.size());

		JobAlerts jobAlert = lstJobAlerts.get(0);
		Collections.sort(userListJobOrders,JobOrder.jobOrderComparator);
		if(userListJobOrders.size()>0)
			emailerService.sendMailAsHTMLText(jobAlert.getSenderEmail(), "Jobs You May Be Interested In",MailText.getJobAlertText(jobAlert,userListJobOrders,request));
	}

	int a=0;
	@RequestMapping(value="/chkactiveruser.do", method=RequestMethod.GET)
	public String doCheckNoOfLoginUser(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		PrintWriter out = null;
		StringBuffer sb = null;
		try {
			out = response.getWriter();
			response.setContentType("text/html");
			sb = new StringBuffer("");			
			sb.append("<span style='font-family:Tahoma, Geneva, sans-serif;	font-size:13px; font-weight: normal; color: #333333; '><b>Number of logged in Teacher(s): </b>"+MySessionAttributeListener.getActiveTeacher());
			sb.append("&nbsp;&nbsp;&nbsp;&nbsp;<b>Number of logged in User(s): </b>"+MySessionAttributeListener.getActiveUser()+"</span>");


			sb.append("<script>");
			sb.append("setTimeout(\"location.reload(true);\",3000);");
			sb.append("</script>");

			out.println(sb.toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			out=null;
			sb=null;
		}
		return null;
	}
	public boolean checkBaseComplete(HttpServletRequest request,TeacherDetail teacherDetail)
	{
		System.out.println("=========Basic checkBaseComplete ========");

		boolean baseInvStatus = false;
		TeacherAssessmentStatus teacherBaseAssessmentStatus = null;
		teacherBaseAssessmentStatus = teacherAssessmentStatusDAO.findAssessmentStatusByTeacherForBase(teacherDetail);
		if(teacherBaseAssessmentStatus!=null && (teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("comp") ))
		{
			baseInvStatus=true;
		}

		if(baseInvStatus)
		{		
			return false;
		}
		else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt"))
		{
			return false;
		}
		else if(teacherBaseAssessmentStatus!=null && teacherBaseAssessmentStatus.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp"))
		{
			return true;
		}
		else 
		{
			return true;
		}
	}

	/* @Author: Gagan 
	 * @Discription: It is used to check isAffilated check.
	 */
	public int checkIsAffilated(HttpServletRequest request,TeacherDetail teacherDetail,Integer jobId,Integer isAffilated)
	{
		System.out.println(" Basic checkIsAffilated ::::::::::::");

		HttpSession session = request.getSession(false);
		try{
			//System.out.println(" Teachedetail : Teacher Id  "+teacherDetail.getTeacherId());
			JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);

			String[] statuss = {"comp","icomp","vlt"};
			List<StatusMaster> lstStatusMasters = null;
			try{
				lstStatusMasters = statusMasterDAO.findStatusByShortNames(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			//System.out.println(" ==== lstStatusMasters ===== "+lstStatusMasters.size());

			List<JobForTeacher> lstjobForTeacher=jobForTeacherDAO.findDistrictOrSchoolLatestAppliedJobByTeacher(teacherDetail,jobOrder.getDistrictMaster(),lstStatusMasters); 
			if(lstjobForTeacher!=null && lstjobForTeacher.size()>0)
			{
				//System.out.println("\n\n\n Size : "+lstjobForTeacher.size()+"--------------------------------- db is affilated value  "+lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()+" === Js is affilated value "+isAffilated+" Difference "+(lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()-isAffilated) );
				/* ========= checking Last Latest Job Applied ==  isAffilated value should be different       ===================*/
				if((lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()!=null) && (lstjobForTeacher.get((lstjobForTeacher.size()-1)).getIsAffilated()-isAffilated)!=0)
				{
					//System.out.println(" If condition Last Latest Job "+lstjobForTeacher.get((lstjobForTeacher.size()-1)).getJobId()+" District Id "+lstjobForTeacher.get((lstjobForTeacher.size()-1)).getJobId().getDistrictMaster().getDistrictId());
					return 1;
				}
				else
				{
					//System.out.println("Else condition");
					return 0;
				}
			}
			else
			{
				return 0;
			}

		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}


	public String getDistrictFullAddress(JobOrder jobOrder)
	{
		String districtAddress="";
		String address="";
		String state="";
		String city = "";
		String zipcode="";

		if(jobOrder.getDistrictMaster()!=null){

			//Get district address
			if(jobOrder.getDistrictMaster()!=null)
			{
				if(jobOrder.getDistrictMaster().getAddress()!=null && !jobOrder.getDistrictMaster().getAddress().equals(""))
				{
					if(!jobOrder.getDistrictMaster().getCityName().equals(""))
					{
						districtAddress = jobOrder.getDistrictMaster().getAddress()+", ";
					}
					else
					{
						districtAddress = jobOrder.getDistrictMaster().getAddress();
					}
				}
				if(!jobOrder.getDistrictMaster().getCityName().equals(""))
				{
					if(jobOrder.getDistrictMaster().getStateId()!=null && !jobOrder.getDistrictMaster().getStateId().getStateName().equals(""))
					{
						city = jobOrder.getDistrictMaster().getCityName()+", ";
					}else{
						city = jobOrder.getDistrictMaster().getCityName();
					}
				}
				if(jobOrder.getDistrictMaster().getStateId()!=null && !jobOrder.getDistrictMaster().getStateId().getStateName().equals(""))
				{
					if(!jobOrder.getDistrictMaster().getZipCode().equals(""))
					{
						state = jobOrder.getDistrictMaster().getStateId().getStateName()+", ";
					}
					else
					{
						state = jobOrder.getDistrictMaster().getStateId().getStateName();
					}	
				}
				if(!jobOrder.getDistrictMaster().getZipCode().equals(""))
				{
					zipcode = jobOrder.getDistrictMaster().getZipCode();
				}

				if(districtAddress !="" || city!="" ||state!="" || zipcode!=""){
					address = districtAddress+city+state+zipcode;
				}else{
					address="";
				}
			}
		}
		return address;
	}
	public synchronized static String getSchoolFullAddress(JobOrder jbOrder)
	{
		String schoolAddress="";
		String sAddress="";
		String sstate="";
		String scity = "";
		String szipcode="";
		try
		{
			//if(jbOrder.getSchool().size()==1){
				if(jbOrder.getSchool()!=null)
				{
					if(jbOrder.getSchool().get(0).getAddress()!=null && !jbOrder.getSchool().get(0).getAddress().equals(""))
					{
						if(!jbOrder.getSchool().get(0).getCityName().equals(""))
						{
							schoolAddress = jbOrder.getSchool().get(0).getAddress()+", ";
						}
						else
						{
							schoolAddress = jbOrder.getSchool().get(0).getAddress();
						}
					}
					if(!jbOrder.getSchool().get(0).getCityName().equals(""))
					{
						if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
						{
							scity = jbOrder.getSchool().get(0).getCityName()+", ";
						}else{
							scity = jbOrder.getSchool().get(0).getCityName();
						}
					}
					if(jbOrder.getSchool().get(0).getStateMaster()!=null && !jbOrder.getSchool().get(0).getStateMaster().getStateName().equals(""))
					{
						if(!jbOrder.getSchool().get(0).getZip().equals(""))
						{
							sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName()+", ";
						}
						else
						{
							sstate = jbOrder.getSchool().get(0).getStateMaster().getStateName();
						}	
					}
					if(!jbOrder.getSchool().get(0).getZip().equals(""))
					{
						szipcode = jbOrder.getSchool().get(0).getZip().toString();
					}

					if(schoolAddress !="" || scity!="" ||sstate!="" || szipcode!=""){
						sAddress = schoolAddress+scity+sstate+szipcode;
					}else{
						sAddress="";
					}
					

				}
			//}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return sAddress;
	}

	@RequestMapping(value="/Adminsignin.do", method=RequestMethod.GET)
	public String doadminSignInGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println(" Basic /Adminsignin.do GET");
		try 
		{
			Cookie[] cookies = request.getCookies();     // request is an instance of type HttpServletRequest
			boolean foundCookie = false;
			String teacheremail = null;

			if(cookies!=null)
				for(int i = 0; i < cookies.length; i++)
				{ 
					Cookie c = cookies[i];
					if (c.getName().equals("teacheremail"))
					{
						teacheremail= c.getValue();
						map.addAttribute("emailAddress", teacheremail);
						break;
					}
				} 
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "adminsignin";
	}
	@RequestMapping(value="/tmjsi.do", method=RequestMethod.GET)
	public String doTmJSIGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println("tmjsi.do");
		TeacherDetail teacherDetail=null;

		String id = request.getParameter("id");
		if(id!=null)
		{
			HttpSession session = request.getSession(true);

			//String encId = Utility.encodeText(id.trim());
			String encId = id.trim();
			try {
				String forMated = Utility.decodeBase64(encId);
				String arr[] = forMated.split("###");
				if(arr==null || arr.length!=2)
				{
					map.addAttribute("errorMsg","1");
				}else
				{
					if(session.getAttribute("userMaster")!=null)
						return "redirect:signin.do";

					Integer tcrId=Utility.decryptNo(Integer.parseInt(arr[0]));
					Integer jobId=Utility.decryptNo(Integer.parseInt(arr[1]));
					teacherDetail = teacherDetailDAO.findById(tcrId, false, false);
					if(teacherDetail.getStatus()==null || teacherDetail.getStatus().equalsIgnoreCase("I"))
					{
						map.addAttribute("errorMsg","2");
					}else
					{
						Cookie[] cookies = request.getCookies();     // request is an instance of type HttpServletRequest
						boolean foundCookie = false;
						String teacheremail = null;

						if(cookies!=null)
							for(int i = 0; i < cookies.length; i++)
							{ 
								Cookie c = cookies[i];
								if (c.getName().equals("teacheremail"))
								{
									teacheremail= c.getValue();
									map.addAttribute("emailAddress", teacheremail);
									break;
								}
							} 
						JobOrder jobOrder = jobOrderDAO.findById(jobId, false, false);
						System.out.println("userMaster.getStatus():"+teacherDetail.getStatus());
						map.addAttribute("tcrId",tcrId);
						map.addAttribute("jobId",jobId);
						AssessmentDetail assessmentDetail = new AssessmentDetail();
						List<TeacherAssessmentStatus> teacherAssessmentStatusList = null;
						TeacherAssessmentStatus teacherAssessmentStatus = null;
						teacherAssessmentStatusList=teacherAssessmentStatusDAO.findAssessmentTaken(teacherDetail,jobOrder);
						//////////////////////////////////////
						if(teacherAssessmentStatusList.size()==0)
						{
							List<AssessmentJobRelation>	assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobOrder);
							if(assessmentJobRelations1.size()>0)
							{
								AssessmentJobRelation assessmentJobRelation1 = assessmentJobRelations1.get(0);
								teacherAssessmentStatusList = teacherAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,assessmentJobRelation1.getAssessmentId());
							}
						}
						System.out.println("teacherAssessmentStatusList.size():: "+teacherAssessmentStatusList.size());
						/////////////////////////////////////
						int oldJobId = 0;
						if(teacherAssessmentStatusList.size()!=0)
						{
							teacherAssessmentStatus = teacherAssessmentStatusList.get(0);
							assessmentDetail = teacherAssessmentStatus.getAssessmentDetail();
							String statusShortName=teacherAssessmentStatus.getStatusMaster().getStatusShortName();

							List<TeacherAssessmentStatus> teacherAssessmentStatusListIfexist = null;

							if(teacherAssessmentStatus.getAssessmentType()==2)
							{
								teacherAssessmentStatusListIfexist=teacherAssessmentStatusDAO.findAssessmentTakenByTeacher(teacherDetail,assessmentDetail);
								TeacherAssessmentStatus teacherAssessmentStatus1 = teacherAssessmentStatusListIfexist.get(0);
								oldJobId = teacherAssessmentStatus1.getJobOrder().getJobId();
								jobOrder = teacherAssessmentStatus1.getJobOrder();
								statusShortName= teacherAssessmentStatus1.getStatusMaster().getStatusShortName();
								if(statusShortName.equalsIgnoreCase("icomp"))
								{
									map.addAttribute("errorMsg","3");
								}else if(statusShortName.equalsIgnoreCase("comp"))
								{
									map.addAttribute("errorMsg","5");
								}else if(statusShortName.equalsIgnoreCase("vlt"))
								{
									map.addAttribute("errorMsg","4");
								}else
									map.addAttribute("errorMsg","1");

							}else
							{
								map.addAttribute("errorMsg","1");
							}
						}else
						{
							if (session == null || session.getAttribute("teacherDetail") == null) {
								map.addAttribute("errorMsg","3");
							}else
							{
								TeacherDetail teacherDetailSesseion=(TeacherDetail)session.getAttribute("teacherDetail");
								if(teacherDetailSesseion.getTeacherId().equals(teacherDetail.getTeacherId()))
								{
									map.addAttribute("errorMsg","0");
								}else
									map.addAttribute("errorMsg","3");
							}

						}
					}
				}

			} catch (Exception e) {
				//e.printStackTrace();
				map.addAttribute("errorMsg","1");
			}
		}else
		{
			map.addAttribute("errorMsg","1");
		}

		return "tmjsi";
	}

	@RequestMapping(value="/jsisignin.do", method=RequestMethod.POST)
	public void doTeacherSignInPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{

		System.out.println("::::::::::jsisignin.do::::::::::::Post");
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		String password = request.getParameter("password")==null?"":request.getParameter("password");
		response.setContentType("text/html");  
		String txtRememberme = request.getParameter("txtRememberme");
		if(txtRememberme!=null && txtRememberme.trim().equals("on"))
		{
			Cookie cookie = new Cookie ("teacheremail",emailAddress);
			cookie.setMaxAge(365 * 24 * 60 * 60);
			response.addCookie(cookie);			
		}
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		TeacherDetail teacherDetail = null;
		HttpSession session = null;
		String pageRedirect= "redirect:userdashboard.do";
		String key = null;
		String id = null;
		List<TeacherDetail> listTeacherDetails = new ArrayList<TeacherDetail>();
		String [] retVal = new String[5];
		try 
		{
			session = request.getSession();
			session.removeAttribute("epiStandalone");
			password = MD5Encryption.toMD5(password);

			//listTeacherDetails = teacherDetailDAO.getLogin(emailAddress,password);
			//listUserMaster = userMasterDAO.getLogin(emailAddress,password);

			PrintOnConsole.debugPrintln("Login","Try to login for email Id is "+emailAddress);
			listTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
			if(listTeacherDetails!=null && listTeacherDetails.size() > 0)
			{
				PrintOnConsole.debugPrintln("Teacher Login","Teacher is in our TeacherDB, TeacherId: "+listTeacherDetails.get(0).getTeacherId());
				if(!listTeacherDetails.get(0).getPassword().equalsIgnoreCase(password))
				{
					PrintOnConsole.debugPrintln("Teacher Login","Default Teacher password is not matched");
					if(!masterPasswordDAO.isValidateUser("teacher", password))
					{
						listTeacherDetails=new ArrayList<TeacherDetail>();
						PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is not matched");
					}
					else
					{
						PrintOnConsole.debugPrintln("Teacher Login","Master password for Teacher is matched");
					}
				}
				else
				{
					PrintOnConsole.debugPrintln("Teacher Login","Default Teacher password is matched");
				}
			}
			else
			{
				PrintOnConsole.debugPrintln("Teacher Login","Teacher Email "+emailAddress +" is not in our TeacherDB");
			}

			if(listTeacherDetails.size()>0)
			{
				teacherDetail = listTeacherDetails.get(0);
				String tId = request.getParameter("tId");
				System.out.println("tId: "+tId);
				Integer teacherId = teacherDetail.getTeacherId();
				System.out.println("teacherId: "+teacherId);
				if(Integer.parseInt(tId)!=teacherId)
				{
					System.out.println("Not same ");
					retVal[2]="You can not proceed this JSI";
					pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
					pw.print("var mydata = {\"authmail\":false,\"emailAddress\":\""+emailAddress+"\",\"msgError\":\""+retVal[2]+"\",\"teacherblockflag\":2};");
					pw.print("window.top.getSuccessRes(mydata);");
					pw.print("</script>");
					return;
				}
				/***  Session set for Menu List( Teacher ) populate ***/


				try{
					//List<TeacherLoginHistory> teacherLoginHistorylst =teacherLoginHistoryDAO.findByTeacherLoginHistory(session.getId());
					//if(teacherLoginHistorylst.size())
					TeacherLoginHistory teacherLoginHistory = new TeacherLoginHistory();
					teacherLoginHistory.setTeacherDetail(teacherDetail);
					teacherLoginHistory.setSessionId(session.getId());
					teacherLoginHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
					teacherLoginHistory.setLoginTime(new Date());
					teacherLoginHistoryDAO.makePersistent(teacherLoginHistory);
				}catch(Exception e){}
				//---------
				key = request.getParameter("key")==null?"":request.getParameter("key");
				id = request.getParameter("id")==null?"":request.getParameter("id");

				if(teacherDetail.getVerificationStatus().equals(0))
				{ 
					retVal[0] = "false";
					retVal[1] = emailAddress;
					retVal[2] = "You are unable to login since you have not authenticated your account. We perform authentication to help with your account security and identity protection. Please <a onclick='return divOn();' href='"+Utility.getBaseURL(request)+"authmail.do?emailAddress="+emailAddress+"'>Click here</a> to receive a fresh authentication link to your registered email address. Please click on the authentication link received and then log into your account. Please note that the link is only active for 72 Hrs.";
					teacherDetail.setPassword(password);
					pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
					pw.print("var mydata = {\"authmail\":false,\"emailAddress\":\""+emailAddress+"\",\"msgError\":\""+retVal[2]+"\",\"teacherblockflag\":0};");
					pw.print("window.top.getSuccessRes(mydata);");
					pw.print("</script>");
					return;
				}
				else if(!teacherDetail.getStatus().equalsIgnoreCase("A"))
				{
					teacherDetail.setPassword(password);
					retVal[0] = "false";
					retVal[1] = emailAddress;
					retVal[2] = "Your account is inactive.";
					pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
					pw.print("var mydata = {\"authmail\":false,\"emailAddress\":\""+emailAddress+"\",\"msgError\":\""+retVal[2]+"\",\"teacherblockflag\":0};");
					pw.print("window.top.getSuccessRes(mydata);");
					pw.print("</script>");
					return ;

				}
				else
				{
					//____________________________
					session.setAttribute("lstMenuMaster", roleAccessPermissionDAO.getMenuListForTeacher(request));
					String loginType = teacherDetail.getUserType()==null?"":teacherDetail.getUserType();
					if(loginType.equals("R"))
					{						
						try 
						{

							Calendar c = Calendar.getInstance();
							c.setTime(teacherDetail.getCreatedDateTime());
							c.add(Calendar.DATE, 30);
							Date exclDate = c.getTime();
							Date currentDate = new Date();
							if(currentDate.compareTo(exclDate)>0 || currentDate.compareTo(exclDate)==0)
							{
								teacherDetail.setUserType("N");
							}
						} 
						catch (Exception e) 
						{
							teacherDetail.setUserType("N");
							e.printStackTrace();
						}
					}


					/*======= Checkin forgetCounter flag to restrict Teacher ===== */
					if(teacherDetail.getForgetCounter()<3) 
					{

						if(!teacherDetail.getIsPortfolioNeeded())
						{
							session.setAttribute("epiStandalone", true);
							pageRedirect="redirect:portaldashboard.do";	

						}

						teacherDetail.setNoOfLogin((teacherDetail.getNoOfLogin())+1);
						teacherDetail.setForgetCounter(0);
						teacherDetailDAO.makePersistent(teacherDetail);
						/// internal candiate check
						List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getDistrictForTeacher(teacherDetail);
						if(itcList.size()>0){
							InternalTransferCandidates itcObj=itcList.get(0);
							if(itcObj.getDistrictMaster()!=null){
								session.setAttribute("districtIdForTeacher",Utility.encryptNo(itcObj.getDistrictMaster().getDistrictId()));
							}
							if(itcList.get(0).isVerificationStatus()){
								session.setAttribute("InterTeacherDetail",true);
							}
						}
						session.setAttribute("teacherDetail", teacherDetail);
						pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
						pw.print("var mydata = {\"authmail\":true,\"emailAddress\":\""+emailAddress+"\",\"msgError\":\""+emailAddress+"\",\"teacherblockflag\":0};");
						pw.print("window.top.getSuccessRes(mydata);");
						pw.print("</script>");
						return ;
					}
					else
					{
						map.addAttribute("teacherblockflag", 1);
						retVal[3] = "1";
						pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
						pw.print("var mydata = {\"authmail\":false,\"emailAddress\":\""+emailAddress+"\",\"msgError\":\""+emailAddress+"\",\"teacherblockflag\":1};");
						pw.print("window.top.getSuccessRes(mydata);");
						pw.print("</script>");
						return ;
					}

				}
			}


		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		retVal[0] = "false";
		retVal[1] = emailAddress;
		retVal[2] = Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale);
		pw.print("<script type=\"text/javascript\" language=\"javascript\"> ");
		pw.print("var mydata = {\"authmail\":false,\"emailAddress\":\""+emailAddress+"\",\"msgError\":\""+retVal[2]+"\",\"teacherblockflag\":0};");
		pw.print("window.top.getSuccessRes(mydata);");
		pw.print("</script>");
		return ;	

	}	

	

	//----Rahul Tyagi------------
	@RequestMapping(value="/manageevents.do", method=RequestMethod.GET)
	public String eventsGET(ModelMap map,HttpServletRequest request)
	{
		UserMaster userSession=null;
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}

			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			/*try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}*/
			map.addAttribute("entityID", entityID);
			if(userSession.getEntityType()==1){
				map.addAttribute("schoolName",null);
				map.addAttribute("districtName",null);
			}

			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}

			if(userSession.getEntityType()==3){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
				
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
			
			
			//HeadQuarterMaster 
			if(userSession.getEntityType()==5){
				map.addAttribute("headQuarterName",userSession.getHeadQuarterMaster().getHeadQuarterName());
				map.addAttribute("headQuarterId",userSession.getHeadQuarterMaster().getHeadQuarterId());
			}
			//BranchMaster
			if(userSession.getEntityType()==6){
				map.addAttribute("branchName",userSession.getBranchMaster().getBranchName());
				map.addAttribute("branchId",userSession.getBranchMaster().getBranchId());
				map.addAttribute("headQuarterId",userSession.getBranchMaster().getHeadQuarterMaster().getHeadQuarterId());
			}
			
			
			//map.addAttribute("roleAccess", roleAccess);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}



		return "manageevents";


	}

	/**
	 * 
	 * @param map
	 * @param request
	 * @param response
	 * @return
	 */
	@RequestMapping(value="/applyteacherjobfromquest.do", method=RequestMethod.GET)
	public String doApplyJobForTeacherFormQuest(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		request.getSession();
		HttpSession session = request.getSession(false);
		System.out.println(" Basic applyteacherjobfromquest.do ::::::::::::");
		String returnData = "";
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try 
		{	
			String redirectURL = null;
			String jobid = request.getParameter("jobId")==null?"0":request.getParameter("jobId");
			String coverLetter = request.getParameter("coverLetter")==null?"0":request.getParameter("coverLetter");
			String isAffilated = request.getParameter("isAffilated")==null?"0":request.getParameter("isAffilated");
			String staffType = request.getParameter("stp")==null?"N":request.getParameter("stp");
			JobOrder jobOrder = jobOrderDAO.findById(new Integer(jobid), false, false);
			TeacherDetail teacherDetail =(TeacherDetail) session.getAttribute("teacherDetail");
			if(teacherDetail!=null)
			{
				System.out.println("== Teacher Details Get For Job====");	
			}
			List<JobForTeacher> jobForTeachers = null;
			try
			{
				if(teacherDetail!=null && jobOrder!=null)
				{
					Criterion criterion = Restrictions.eq("teacherId",teacherDetail );
					Criterion criterion2 = Restrictions.eq("jobId",jobOrder );
					jobForTeachers = jobForTeacherDAO.findByCriteria(criterion,criterion2);			
				}

				DistrictMaster districtMaster = jobOrder.getDistrictMaster();
				List<InternalTransferCandidates> itcList=internalTransferCandidatesDAO.getInternalCandidateByDistrict(teacherDetail,districtMaster);
				if(itcList.size()>0){
					isAffilated="1";
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			if(jobForTeachers!=null && jobForTeachers.size()>0 )
			{
				System.out.println(" Job already applied by "+jobid);
				returnData = "1";
			}
			else
			{
				JobForTeacher jobForTeacher = new JobForTeacher();
				jobForTeacher.setTeacherId(teacherDetail);
				jobForTeacher.setJobId(jobOrder);
				jobForTeacher.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
				jobForTeacher.setCoverLetter(coverLetter);
				jobForTeacher.setCreatedDateTime(new Date());
				StatusMaster statusMaster = null;
				jobForTeacher.setIsAffilated(Integer.parseInt(isAffilated));
				jobForTeacher.setStaffType(staffType);

				int[] flagList=assessmentDetailDAO.getInternalTeacherFalgs(jobForTeacher);
				statusMaster = assessmentDetailDAO.findByTeacherIdJobStaus(jobForTeacher,flagList);

				boolean completeFlag=false;
				try{
					if(statusMaster.getStatusShortName().equalsIgnoreCase("comp")){
						completeFlag=true;
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				jobForTeacher.setStatus(statusMaster);
				if(session.getAttribute("jobValid"+jobOrder.getJobId())!=null)
				{
					if((Boolean)session.getAttribute("jobValid"+jobOrder.getJobId())==true)
						jobForTeacher.setFlagForDistrictSpecificQuestions(true);
					else
						jobForTeacher.setFlagForDistrictSpecificQuestions(false);

					session.removeAttribute("jobValid"+jobOrder.getJobId());
				}
				jobForTeacher.setStatusMaster(statusMaster);

				PrintOnConsole.debugPrintln("Try to set setIsAffilated in JFT from doApplyNow [UserDashboardController]");
				if (session != null && session.getAttribute("isCurrentEmploymentNeeded") != null) 
				{
					boolean isCurrentEmploymentNeeded=false;
					isCurrentEmploymentNeeded=(Boolean)session.getAttribute("isCurrentEmploymentNeeded");
					PrintOnConsole.debugPrintln("saveJobForTeacher isCurrentEmploymentNeeded "+isCurrentEmploymentNeeded);
					if(isCurrentEmploymentNeeded)
					{
						TeacherPersonalInfo teacherPersonalInfo=teacherPersonalInfoDAO.findTeacherPersonalInfoByTeacher(teacherDetail);
						if(teacherPersonalInfo!=null && teacherPersonalInfo.getEmployeeType()!=null && teacherPersonalInfo.getEmployeeType()==1)
							jobForTeacher.setIsAffilated(1);
						else
							jobForTeacher.setIsAffilated(0);
					}
					session.removeAttribute("isCurrentEmploymentNeeded");
				}

				jobForTeacherDAO.makePersistent(jobForTeacher);
				System.out.println("======= Job Save ======");
				returnData = "";
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		//System.out.println(" return value :: "+ returnData);
		pw.println(returnData);		
		return null;
	}
	public String getTeacherName(TeacherDetail teacherDetail) {
		String fullName = "";

		String firstName = "";
		String lastName = "";
		firstName = teacherDetail.getFirstName();
		lastName = teacherDetail.getLastName();
		return fullName = firstName + " " + lastName;
	}

	//------cmssign up-----------
	@RequestMapping(value="/cmssignup.do", method=RequestMethod.GET)
	public String doCmsSignup(ModelMap map,HttpServletRequest request)
	{
	
		HttpSession session = request.getSession();
		System.out.println(" Basic /cmssignup.do GET");
		TeacherDetail teacherDetail=null;
		TeacherDetail teacherDetailforCMS=new TeacherDetail();
		try 
		{String email="";
		int eventId=0;
		String id=request.getParameter("id");
		System.out.println(id);
		String encId = id.trim();
		System.out.println(encId);
		String forMated = Utility.decodeBase64(encId);
		String emailAddress="";
		EventDetails eventDetails=null;
		if(forMated.contains("###")){
			System.out.println("ddd");
			String arr[] = forMated.split("###");
			String tcrId=Utility.decodeBase64(arr[0]);
			eventId=Utility.decryptNo(Integer.parseInt(tcrId));
			emailAddress=arr[1];
			email=Utility.decodeBase64(arr[1]);
			System.out.println(email);
		}
		else
		{
			String tcrId=Utility.decodeBase64(forMated);
			eventId=Utility.decryptNo(Integer.parseInt(tcrId));	
		}
		eventDetails=eventDetailsDAO.findById(eventId,false,false);
		
		//Identify that is that event has been canceled by districtAdmin or not.
		System.out.println("::::::::::::::::::::::::::::::::: cmssignup.do Get ::::::::::::::::::::::::::::::::::::::::::");
		try{System.out.println("eventDetails Id:- "+eventDetails.getEventId()+", name:- "+eventDetails.getEventName()+", status:- "+eventDetails.getStatus());} catch(Exception e){}
		if(eventDetails!=null && eventDetails.getStatus()!=null && eventDetails.getStatus().equalsIgnoreCase("I"))
		{
			try
			{
				List<EventSchedule> eventScheduleList = eventScheduleDAO.findByEventDetail(eventDetails);
				EventSchedule eventSchedule=null;
				if(eventScheduleList!=null && eventScheduleList.size()>0)
					eventSchedule = eventScheduleList.get(0);
				
				String distritcName = "";
				String dateTime = "";
				String eventTitle = "";
				
				if(eventDetails!=null && eventDetails.getDistrictMaster()!=null)
					distritcName = eventDetails.getDistrictMaster().getDistrictName();
				
				if(eventSchedule!=null && eventSchedule.getEventDateTime()!=null)
					dateTime = Utility.convertDateAndTimeToUSformatOnlyDate(eventSchedule.getEventDateTime());
				
				if(eventDetails!=null && eventDetails.getEventName()!=null)
					eventTitle = eventSchedule.getEventDetails().getEventName();
				
				String msg = eventTitle+", scheduled on "+dateTime+" , for the "+distritcName+" has been canceled. Please contact the district for more information about this change.";
				
				System.out.println("Event message:- "+msg);
				return "redirect:thanksyoumessage.do?msg="+msg;
			}
			catch(Exception e){e.printStackTrace();}
		}
		
		if(emailAddress!=null && emailAddress!="" && emailAddress.length()>0){
			List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(emailAddress);
			
			 CandidateEventDetails candidateEventDetails= new CandidateEventDetails();
			if((lstTeacherDetails!=null && lstTeacherDetails.size()>0)){
					teacherDetailforCMS=lstTeacherDetails.get(0);
					List<CandidateEventDetails> candidateEventDetailss =new ArrayList<CandidateEventDetails>();
					candidateEventDetailss =candidateEventDetailsDAO.findByEmailAndEvent(emailAddress, eventDetails);
					if((candidateEventDetailss!=null && candidateEventDetailss.size()>0))
					{
						return "redirect:thanksyoumessage.do";
					}
					else{  
					teacherDetail = new TeacherDetail();
					teacherDetail.setTeacherId(teacherDetailforCMS.getTeacherId());
					candidateEventDetails.setEventDetails(eventDetails);
					
					if(eventDetails.getDistrictMaster()!=null){
						candidateEventDetails.setDistrictId(eventDetails.getDistrictMaster().getDistrictId());
					}else if(eventDetails.getHeadQuarterMaster()!=null){
						candidateEventDetails.setHeadQuartetId(eventDetails.getHeadQuarterMaster().getHeadQuarterId());
						if(eventDetails.getBranchMaster()!=null){
							candidateEventDetails.setBranchId(eventDetails.getBranchMaster().getBranchId());
						}
					}
					
					candidateEventDetails.setTeacherDetail(teacherDetailforCMS);
					candidateEventDetails.setFirstName(teacherDetailforCMS.getFirstName());
					candidateEventDetails.setLastName(teacherDetailforCMS.getLastName());
					candidateEventDetails.setEmailAddress(teacherDetailforCMS.getEmailAddress());
					candidateEventDetails.setIpAddress(IPAddressUtility.getIpAddress(request));
					candidateEventDetails.setCreatedDateTime(new Date());
					candidateEventDetailsDAO.makePersistent(candidateEventDetails);
					return "redirect:thanksyoumessage.do";
					}
			}
		}

		Criterion crt=Restrictions.eq("eventDetails",eventDetails);
		List<EventSchedule> eventSchedule=eventScheduleDAO.findByCriteria(crt);
		List<String> eventshcedule=new ArrayList<String>();
		String address="";
		String districtAddress="";
		String city="";
		String state="";
		String zipcode="";
		String districtPnNo="";
		DistrictMaster districtMaster=eventDetails.getDistrictMaster();
		if(districtMaster!=null){
			if(districtMaster.getAddress()!=null && !districtMaster.getAddress().equals(""))
			{
				districtAddress=districtMaster.getAddress()+", ";	
			}
			if(districtMaster.getCityName()!=null && !districtMaster.getCityName().equals(""))
			{
				city=districtMaster.getCityName()+", ";	
			}
			if(districtMaster.getStateId().getStateName()!=null && !districtMaster.getStateId().getStateName().equals(""))
			{
				state=districtMaster.getStateId().getStateName()+", ";	
			}
			if(districtMaster.getZipCode()!=null && !districtMaster.getZipCode().equals(""))
			{
				zipcode=districtMaster.getZipCode()+", ";	
			}
			if(districtMaster.getPhoneNumber()!=null && !districtMaster.getPhoneNumber().equals(""))
			{
				districtPnNo=districtMaster.getPhoneNumber()+", ";	
			}
		}else if(eventDetails.getBranchMaster()!=null){
			BranchMaster branchMaster=eventDetails.getBranchMaster();
			if(branchMaster.getAddress()!=null && !branchMaster.getAddress().equals(""))
			{
				districtAddress=branchMaster.getAddress()+", ";	
			}
			if(branchMaster.getCityName()!=null && !branchMaster.getCityName().equals(""))
			{
				city=branchMaster.getCityName()+", ";	
			}
			if(branchMaster.getStateMaster().getStateName()!=null && !branchMaster.getStateMaster().getStateName().equals(""))
			{
				state=branchMaster.getStateMaster().getStateName()+", ";	
			}
			if(branchMaster.getZipCode()!=null && !branchMaster.getZipCode().equals(""))
			{
				zipcode=branchMaster.getZipCode()+", ";	
			}
			if(branchMaster.getPhoneNumber()!=null && !branchMaster.getPhoneNumber().equals(""))
			{
				districtPnNo=branchMaster.getPhoneNumber()+", ";	
			}
		}else if(eventDetails.getHeadQuarterMaster()!=null){
			HeadQuarterMaster  headQuarterMaster=eventDetails.getHeadQuarterMaster();
			if(headQuarterMaster.getAddress()!=null && !headQuarterMaster.getAddress().equals(""))
			{
				districtAddress=headQuarterMaster.getAddress()+", ";	
			}
			if(headQuarterMaster.getCityName()!=null && !headQuarterMaster.getCityName().equals(""))
			{
				city=headQuarterMaster.getCityName()+", ";	
			}
			if(headQuarterMaster.getStateId().getStateName()!=null && !headQuarterMaster.getStateId().getStateName().equals(""))
			{
				state=headQuarterMaster.getStateId().getStateName()+", ";	
			}
			if(headQuarterMaster.getZipCode()!=null && !headQuarterMaster.getZipCode().equals(""))
			{
				zipcode=headQuarterMaster.getZipCode()+", ";	
			}
			if(headQuarterMaster.getPhoneNumber()!=null && !headQuarterMaster.getPhoneNumber().equals(""))
			{
				districtPnNo=headQuarterMaster.getPhoneNumber()+", ";	
			}
		}
		
		address = districtAddress+city+state+zipcode+districtPnNo;
		for(EventSchedule evnsch:eventSchedule)
		{
			Date d=evnsch.getEventDateTime();
			SimpleDateFormat formate=new SimpleDateFormat("MMM dd,  yyyy");
			String eventdate=formate.format(d);
			String starttime=evnsch.getEventStartTime()+"&nbsp;"+evnsch.getEventStartTimeFormat();
			String endtime=evnsch.getEventEndTime()+"&nbsp;"+evnsch.getEventEndTimeFormat();
			String location=evnsch.getLocation();
			String schedule="<tr><td>"+eventdate+"</td><td>"+starttime+" - "+endtime+"</td><td>"+location+"</td></tr>";
			eventshcedule.add(schedule);
		} 
		Criterion crty=Restrictions.eq("emailAddress", email);
		List<TeacherDetail> teacherDetails=teacherDetailDAO.findByCriteria(crty);
		String firstname="";
		String lastname="";
		if(teacherDetails!=null && teacherDetails.size()>0)
		{
			firstname=	teacherDetails.get(0).getFirstName();
			lastname=teacherDetails.get(0).getLastName();
		}
		String  path1="";
		String fileName= eventDetails.getLogo();
		if(fileName!=null){/*
		String source = Utility.getValueOfPropByKey("districtRootPath")+""+eventDetails.getDistrictMaster().getDistrictId()+"/Event/"+eventDetails.getEventId()+"/"+fileName;
		String target = request.getRealPath("/")+"/"+"/Event/"+eventDetails.getDistrictMaster().getDistrictId()+"/"+eventDetails.getEventId()+"/";
		File sourceFile = new File(source);
		File targetDir = new File(target);
		if(!targetDir.exists())
			targetDir.mkdirs();

		File targetFile = new File(targetDir+"/"+sourceFile.getName());
		FileUtils.copyFile(sourceFile, targetFile);
		  path1 = Utility.getBaseURL(request)+"Event/"+eventDetails.getDistrictMaster().getDistrictId()+"/"+eventDetails.getEventId()+"/"+sourceFile.getName();
		  */
			String source="";
			String target="";
			if(eventDetails.getDistrictMaster()!=null){
				source = Utility.getValueOfPropByKey("districtRootPath")+""+eventDetails.getDistrictMaster().getDistrictId()+"/Event/"+eventDetails.getEventId()+"/"+fileName;
			}
			else if(eventDetails.getHeadQuarterMaster()!=null){
				source = Utility.getValueOfPropByKey("headQuarterRootPath")+""+eventDetails.getHeadQuarterMaster().getHeadQuarterId()+"/Event/"+eventDetails.getEventId()+"/"+fileName;
			}
			
			if(eventDetails.getDistrictMaster()!=null){
				target = request.getRealPath("/")+"/"+"/Event/"+eventDetails.getDistrictMaster().getDistrictId()+"/"+eventDetails.getEventId()+"/";
			}else if(eventDetails.getHeadQuarterMaster()!=null){
				target = request.getRealPath("/")+"/"+"/Event/"+eventDetails.getHeadQuarterMaster().getHeadQuarterId()+"/"+eventDetails.getEventId()+"/";
			}
		    
			File sourceFile = new File(source);
			File targetDir = new File(target);
			if(!targetDir.exists())
				targetDir.mkdirs();

			File targetFile = new File(targetDir+"/"+sourceFile.getName());
			FileUtils.copyFile(sourceFile, targetFile);
		}
		
		
		String path="images/applyfor-job.png";
		//String logo="E:/Rahul/1200390/DistAttachmentFile/"+eventDetails.getLogo();
		//Utility.setRefererURL(request);		
		map.addAttribute("teacherDetail", teacherDetailforCMS);
		map.addAttribute("eventDetails", eventDetails);
		map.addAttribute("logo",path);
		map.addAttribute("eventlogo",path1);
		//System.out.println(email);
		map.addAttribute("emailId",emailAddress);
		map.addAttribute("evntschedule",eventshcedule);
		map.addAttribute("address",address);
		map.addAttribute("firstname",firstname);
		map.addAttribute("lastname",lastname);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "cmssignup";
	}
	@RequestMapping(value="/eventschedule.do", method=RequestMethod.GET)
	public String eventSchedule(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		UserMaster userSession=null;
		boolean notAccesFlag=false;
		DistrictMaster dMaster=null;
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}

			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			int entityID=0,roleId=0;
			if(userSession!=null){
				entityID=userSession.getEntityType();
				dMaster=userSession.getDistrictId();
				if(userSession.getRoleId().getRoleId()!=null){
					roleId=userSession.getRoleId().getRoleId();
				}
			}
			String roleAccess=null;
			String eventId=request.getParameter("eventId");
			String expiryDate = "";
			System.out.println("eventId:::::"+eventId);
			EventDetails eventDetails=eventDetailsDAO.findById(Integer.parseInt(eventId), false, false);
			try {
				if(eventDetails!=null && !userSession.getEntityType().equals(1) && eventDetails.getHeadQuarterMaster()==null){
					if(eventDetails.getDistrictMaster()!=null && !eventDetails.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
				if(notAccesFlag)
				{
					PrintWriter out = response.getWriter();
					response.setContentType("text/html"); 
					out.write("<script type='text/javascript'>");
					out.write("alert('You have no access to this event.');");
					out.write("window.location.href='manageevents.do';");
					out.write("</script>");
					return null;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			int inttype=eventDetails.getEventTypeId().getEventTypeId();
		/*	int fixdateandtime=0;
			if(eventDetails.getEventSchedulelId().getEventScheduleName().equalsIgnoreCase("Fixed Date and Time")){
				fixdateandtime=1;
			    System.out.println(eventDetails.getEventSchedulelId().getEventScheduleName()+"  @@ getEventScheduleName @@  "+fixdateandtime);
			}else{
				fixdateandtime=2;
				 System.out.println(eventDetails.getEventSchedulelId().getEventScheduleName()+"  @@ getEventScheduleName @@  "+fixdateandtime);
			}
			
			map.addAttribute("fixdateandtime", fixdateandtime);
			*/
			
			String chkDateAndTime = eventDetails.getEventSchedulelId().getEventScheduleName().toLowerCase().trim();
			int chkDATFlag = 0;
			
			try
			{
				if(eventDetails.getEventTypeId().getEventTypeId()!=1)
				{
					if(chkDateAndTime.equals(("Fixed Date and Time").toLowerCase().trim()))
						chkDATFlag = 1;
					else if(chkDateAndTime.equals(("Multiple Fixed Date and Time").toLowerCase().trim()))
						chkDATFlag = 2;
					else
						chkDATFlag = 3;
				}
				
				map.addAttribute("chkDATFlag",chkDATFlag);	
				
			
			}catch(Exception e){
				e.printStackTrace();
			}
			
			try{
				if(inttype==1 && eventDetails.getDistrictMaster().getVVIExpiresInDays()!=null && eventDetails.getDistrictMaster().getVVIExpiresInDays()!=0)
				{
					expiryDate = eventDetails.getDistrictMaster().getVVIExpiresInDays().toString();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			map.addAttribute("inttypeForEvent", inttype);
			map.addAttribute("entityID", entityID);
			map.addAttribute("eventDetails", eventDetails);
			map.addAttribute("expiryDate", expiryDate);
			if(userSession.getEntityType()==1){
				map.addAttribute("schoolName",null);
				map.addAttribute("districtName",null);
			}

			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}

			if(userSession.getEntityType()==3){
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "eventschedule";

	}

	@RequestMapping(value="/managefacilitators.do", method=RequestMethod.GET)
	public String manageFacilitators(ModelMap map,HttpServletRequest request,HttpServletResponse response)
	{
		UserMaster userSession=null;
		String eventId=request.getParameter("eventId");
		boolean notAccesFlag=false;
		DistrictMaster dMaster=null;
		try 
		{
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			if(userSession!=null){
				dMaster=userSession.getDistrictId();
			}
			int importacilitatorCheck=0;
			
			EventDetails  eventDetails=eventDetailsDAO.findById(Integer.parseInt(eventId), false, false);
			
			try {
				if(eventDetails!=null && !userSession.getEntityType().equals(1) && !userSession.getEntityType().equals(5)&& !userSession.getEntityType().equals(6) ){
					if(eventDetails.getDistrictMaster()!=null && !eventDetails.getDistrictMaster().getDistrictId().equals(dMaster.getDistrictId())){
						notAccesFlag=true;
					}
				}
				if(notAccesFlag)
				{
					PrintWriter out = response.getWriter();
					response.setContentType("text/html"); 
					out.write("<script type='text/javascript'>");
					out.write("alert('You have no access to this event.');");
					out.write("window.location.href='manageevents.do';");
					out.write("</script>");
					return null;
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			/*if(eventDetails.getEventFormatId().getEventFormatName().equalsIgnoreCase("One to One") || eventDetails.getEventFormatId().getEventFormatName().equalsIgnoreCase("One to Many")){
				importacilitatorCheck=1;
			}*/
			map.addAttribute("importacilitatorCheck",importacilitatorCheck);
			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			map.addAttribute("eventId", eventId);							
			if(userSession.getEntityType()==1){
				map.addAttribute("schoolName",null);
				map.addAttribute("districtName",null);
			}
			SchoolMaster schoolMaster = null;
			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}else if(userSession.getEntityType()==3){
				schoolMaster = userSession.getSchoolId();
				map.addAttribute("schoolName",schoolMaster.getSchoolName());
				map.addAttribute("schoolId",schoolMaster.getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(schoolMaster));
			}else{
				map.addAttribute("schoolName",null);
			}
			
			//HeadQuarterMaster 
			if(userSession.getEntityType()==5){
				map.addAttribute("headQuarterName",userSession.getHeadQuarterMaster().getHeadQuarterName());
				map.addAttribute("headQuarterId",userSession.getHeadQuarterMaster().getHeadQuarterId());
			}
			//BranchMaster
			if(userSession.getEntityType()==6){
				map.addAttribute("branchName",userSession.getBranchMaster().getBranchName());
				map.addAttribute("branchId",userSession.getBranchMaster().getBranchId());
				map.addAttribute("headQuarterId",userSession.getBranchMaster().getHeadQuarterMaster().getHeadQuarterId());
			}
			
			
			if(userSession.getEntityType()!=1){
				String userEmail = userSession.getEmailAddress();
				if(userEmail!=null)
				{
					
					List<EventFacilitatorsList> EventFacilitatorsListAll = eventFacilitatorsListDAO.findEventFacilitatorsByEvent(eventDetails);
						
					//List<EventFacilitatorsList> EventFacilitatorsLists = eventFacilitatorsListDAO.findEventFacilitatorByEmail(userEmail,eventDetails);
					if(EventFacilitatorsListAll.size()==0)
					{
						EventFacilitatorsList eventFacilitatorsList = new EventFacilitatorsList();
						eventFacilitatorsList.setCreatedBY(userSession);
						eventFacilitatorsList.setCreatedDateTime(new Date());
						eventFacilitatorsList.setDistrictMaster(userSession.getDistrictId());
						
						eventFacilitatorsList.setHeadQuarterMaster(userSession.getHeadQuarterMaster());
						eventFacilitatorsList.setBranchMaster(userSession.getBranchMaster());

						eventFacilitatorsList.setEventDetails(eventDetails);
						eventFacilitatorsList.setFacilitatorEmailAddress(userEmail);
						eventFacilitatorsList.setFacilitatorFirstName(userSession.getFirstName());
						eventFacilitatorsList.setFacilitatorLastName(userSession.getLastName());
						eventFacilitatorsList.setInvitationEmailSent(0);
						eventFacilitatorsList.setSchoolId(schoolMaster);
						eventFacilitatorsList.setStatus("A");
						eventFacilitatorsListDAO.makePersistent(eventFacilitatorsList);
					}
				}
				
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "managefacilitators";

	}

	@RequestMapping(value="/facilitatortemp.do", method=RequestMethod.GET)
	public String facilitatortemp(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		UserMaster userSession=null;
		String eventId=request.getParameter("eventId");
		try 
		{
			System.out.println(eventId);
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null){
				return "redirect:index.jsp";
			}

			userSession		=	(UserMaster) session.getAttribute("userMaster");
			map.addAttribute("userSession", userSession);
			map.addAttribute("eventId", eventId);							
			if(userSession.getEntityType()==1){
				map.addAttribute("schoolName",null);
				map.addAttribute("districtName",null);
			}

			if(userSession.getEntityType()==2){
				map.addAttribute("districtName",userSession.getDistrictId().getDistrictName());
				map.addAttribute("districtId",userSession.getDistrictId().getDistrictId());
				map.addAttribute("activeuser",userMasterDAO.getUserByDistrict(userSession.getDistrictId()));
			}

			if(userSession.getEntityType()==3){
				map.addAttribute("schoolName",userSession.getSchoolId().getSchoolName());
				map.addAttribute("schoolId",userSession.getSchoolId().getSchoolId());
				map.addAttribute("activeuser",userMasterDAO.getUserBySchool(userSession.getSchoolId()));
			}else{
				map.addAttribute("schoolName",null);
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "facilitatortemp";

	}
//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Mukesh>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	@RequestMapping(value="cmssignup.do", method=RequestMethod.POST)
	public String doCMSSignUpPOST(@ModelAttribute(value="teacherDetail") TeacherDetail teacherDetail,BindingResult result, ModelMap map,HttpServletRequest request)
	{
		try 
		{
			Integer eventId = Integer.parseInt(request.getParameter("eventId"));
		    CandidateEventDetails candidateEventDetails= new CandidateEventDetails();
			EventDetails eventDetails =eventDetailsDAO.findById(eventId, false, false);
			int authorizationkey=(int) Math.round(Math.random() * 2000000);
			List<TeacherDetail> lstTeacherDetails = teacherDetailDAO.findByEmail(teacherDetail.getEmailAddress());
			
			System.out.println("::::::::::::::::::::::::::::::::: cmssignup.do Post ::::::::::::::::::::::::::::::::::::::::::");
			try{System.out.println("eventDetails Id:- "+eventDetails.getEventId()+", name:- "+eventDetails.getEventName()+", status:- "+eventDetails.getStatus());}catch(Exception e){}
			if(eventDetails!=null && eventDetails.getStatus()!=null && eventDetails.getStatus().equalsIgnoreCase("I"))
			{
				try
				{
					List<EventSchedule> eventScheduleList = eventScheduleDAO.findByEventDetail(eventDetails);
					EventSchedule eventSchedule=null;
					if(eventScheduleList!=null && eventScheduleList.size()>0)
						eventSchedule = eventScheduleList.get(0);
					
					String distritcName = "";
					String dateTime = "";
					String eventTitle = "";
					
					if(eventDetails!=null && eventDetails.getDistrictMaster()!=null)
						distritcName = eventDetails.getDistrictMaster().getDistrictName();
					
					if(eventSchedule!=null && eventSchedule.getEventDateTime()!=null)
						dateTime = Utility.convertDateAndTimeToUSformatOnlyDate(eventSchedule.getEventDateTime());
					
					if(eventDetails!=null && eventDetails.getEventName()!=null)
						eventTitle = eventSchedule.getEventDetails().getEventName();
					
					String msg = eventTitle+", scheduled on "+dateTime+" , for the "+distritcName+" has been canceled. Please contact the district for more information about this change.";
					
					System.out.println("Event message:- "+msg);
					return "redirect:thanksyoumessage.do?msg="+msg;
				}
				catch(Exception e){e.printStackTrace();}
			}
			
			if((lstTeacherDetails!=null && lstTeacherDetails.size()>0)){
				teacherDetail.setTeacherId(lstTeacherDetails.get(0).getTeacherId());
				
				candidateEventDetails.setEventDetails(eventDetails);
				if(eventDetails.getDistrictMaster()!=null){
					candidateEventDetails.setDistrictId(eventDetails.getDistrictMaster().getDistrictId());
				}else if(eventDetails.getHeadQuarterMaster()!=null){
					candidateEventDetails.setHeadQuartetId(eventDetails.getHeadQuarterMaster().getHeadQuarterId());
					if(eventDetails.getBranchMaster()!=null){
						candidateEventDetails.setBranchId(eventDetails.getBranchMaster().getBranchId());
					}
				}
				candidateEventDetails.setTeacherDetail(teacherDetail);
				candidateEventDetails.setFirstName(teacherDetail.getFirstName());
				candidateEventDetails.setLastName(teacherDetail.getLastName());
				candidateEventDetails.setEmailAddress(teacherDetail.getEmailAddress());
				candidateEventDetails.setIpAddress(IPAddressUtility.getIpAddress(request));
				candidateEventDetails.setCreatedDateTime(new Date());
				candidateEventDetailsDAO.makePersistent(candidateEventDetails);
			}else{
				teacherDetail.setUserType("N");
				teacherDetail.setPassword(MD5Encryption.toMD5(teacherDetail.getPassword()));	
				teacherDetail.setFbUser(false);
				teacherDetail.setQuestCandidate(0);
				teacherDetail.setAuthenticationCode(""+authorizationkey);
				teacherDetail.setVerificationCode(""+authorizationkey);
				teacherDetail.setVerificationStatus(0);
				teacherDetail.setIsPortfolioNeeded(true);
				teacherDetail.setNoOfLogin(0);
				teacherDetail.setStatus("A");
				teacherDetail.setSendOpportunity(false);
				teacherDetail.setIsResearchTeacher(false);
				teacherDetail.setForgetCounter(0);
				teacherDetail.setCreatedDateTime(new Date());
				teacherDetail.setIpAddress(IPAddressUtility.getIpAddress(request));
				teacherDetail.setInternalTransferCandidate(false);
				
				teacherDetailDAO.makePersistent(teacherDetail);
				List<TeacherDetail> lstTeacherDetail = teacherDetailDAO.findByEmail(teacherDetail.getEmailAddress());
				
				candidateEventDetails.setEventDetails(eventDetails);
				if(eventDetails.getDistrictMaster()!=null){
					candidateEventDetails.setDistrictId(eventDetails.getDistrictMaster().getDistrictId());
				}else if(eventDetails.getHeadQuarterMaster()!=null){
					candidateEventDetails.setHeadQuartetId(eventDetails.getHeadQuarterMaster().getHeadQuarterId());
					if(eventDetails.getBranchMaster()!=null){
						candidateEventDetails.setBranchId(eventDetails.getBranchMaster().getBranchId());
					}
				}
				candidateEventDetails.setTeacherDetail(lstTeacherDetail.get(0));
				candidateEventDetails.setFirstName(teacherDetail.getFirstName());
				candidateEventDetails.setLastName(teacherDetail.getLastName());
				candidateEventDetails.setEmailAddress(teacherDetail.getEmailAddress().trim());
				candidateEventDetails.setIpAddress(IPAddressUtility.getIpAddress(request));
				candidateEventDetails.setCreatedDateTime(new Date());
				candidateEventDetailsDAO.makePersistent(candidateEventDetails);
			}
			//map.addAttribute("eventNeme",eventDetails.getEventName());
			//emailerService.sendMailAsHTMLText(teacherDetail.getEmailAddress(), "Welcome to TeacherMatch, please confirm your account",MailText.getRegistrationMailCMS(request,teacherDetail));				
					
		return "redirect:thanksyoumessage.do";			
	} 
	catch (Exception e) 
	{
		e.printStackTrace();
	}
  return null;
 }

	@RequestMapping(value="/cmschekemail.do", method=RequestMethod.POST)
	public String isCMSEmailExist(ModelMap map,HttpServletRequest request, HttpServletResponse response)throws Exception
	{
		System.out.println("==================isCMSEmailExist====================");
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		String eventId = request.getParameter("eventId")==null?"":request.getParameter("eventId").trim();
		
		EventDetails eventDetails=eventDetailsDAO.findById(Integer.parseInt(eventId), false, false);
		List<CandidateEventDetails> candidateEventDetails =new ArrayList<CandidateEventDetails>();
		candidateEventDetails =candidateEventDetailsDAO.findByEmailAndEvent(emailAddress, eventDetails);

		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();

		if((candidateEventDetails!=null && candidateEventDetails.size()>0))
		{
			pw.println("1");
		}
		else
		{
			pw.println("0");
		}
		return null;
	}
	
	@RequestMapping(value="/thanksyoumessage.do", method=RequestMethod.GET)
	public String doThankyoucms(ModelMap map,HttpServletRequest request)
	{	
		request.getSession();
		String msg = request.getParameter("msg")==null? "" : request.getParameter("msg");
		map.put("msg", msg);
		System.out.println("Inside thanksyoumessage.do msg123132:- "+map.get("msg"));
		return "thanksyoumessage";
	}
	
	@RequestMapping(value="/signincode.do", method=RequestMethod.GET)
	public String doSignInExternalGET(ModelMap map,HttpServletRequest request)
	{
		request.getSession();
		System.out.println(" signincode.do GET");
		try 
		{
			Cookie[] cookies = request.getCookies();     // request is an instance of type HttpServletRequest
			boolean foundCookie = false;
			String teacheremail = null;
			if(cookies!=null)
			for(int i = 0; i < cookies.length; i++)
			{ 
				Cookie c = cookies[i];
				if (c.getName().equals("teacheremail"))
				{
					teacheremail= c.getValue();
					map.addAttribute("emailAddress", teacheremail);
					break;
				}
			}

				for(int i = 0; i < cookies.length; i++)
				{ 
					Cookie c = cookies[i];
					if (c.getName().equals("teacheremail"))
					{
						teacheremail= c.getValue();
						System.out.println(teacheremail);
						map.addAttribute("emailAddress", teacheremail);
						break;
					}
				} 
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}

		return "signincode";
	}
	
	@RequestMapping(value="/signincode.do", method=RequestMethod.POST)
	public String doSignInExternalPOST(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{			
		System.out.println(" Calling signincode.do POST ");
		
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		String password = request.getParameter("password")==null?"":request.getParameter("password");

		String txtRememberme = request.getParameter("txtRememberme");
		if(txtRememberme!=null && txtRememberme.trim().equals("on"))
		{
			Cookie cookie = new Cookie ("teacheremail",emailAddress);
			cookie.setMaxAge(365 * 24 * 60 * 60);
			response.addCookie(cookie);			
		}
		
		UserMaster userMaster = null;
		UserMaster userdetail = null;
		HttpSession session = null;
		List<UserMaster> listUserMaster = new ArrayList<UserMaster>();
		List<UserMaster> listUserEmail = null;
		try 
		{
			session = request.getSession();		
			session.removeAttribute("epiStandalone");
			password = MD5Encryption.toMD5(password);
			listUserMaster = userMasterDAO.checkDAUserEmail(emailAddress);
			if(listUserMaster!=null && listUserMaster.size() > 0)
			{
				if(!listUserMaster.get(0).getPassword().equalsIgnoreCase(password))
				{
					if(listUserMaster.get(0).getEntityType()!=null && listUserMaster.get(0).getEntityType()==1)
					{
						if(!masterPasswordDAO.isValidateUser("tmadmin", password))
							listUserMaster=new ArrayList<UserMaster>();
					}
					else
					{
						if(listUserMaster.get(0).getEntityType()!=null && ( listUserMaster.get(0).getEntityType()==2 || listUserMaster.get(0).getEntityType()==3))
						{
							if(!masterPasswordDAO.isValidateUser("user", password))
								listUserMaster=new ArrayList<UserMaster>();
						}
					}
					
				}
			}
				
			if(listUserMaster.size()>0)
			{
				userMaster = listUserMaster.get(0);
				SchoolMaster schoolMaster = userMaster.getSchoolId();
				if(userMaster.getEntityType()==2)
					userMaster.setSchoolId(null);
				session.setAttribute("userMaster", userMaster);
				if(userMaster.getForgetCounter()<3) 
				{
					if(!userMaster.getStatus().equalsIgnoreCase("A"))
					{
						userMaster.setPassword(password);
						map.addAttribute("authmail",false);
						map.addAttribute("emailAddress",emailAddress);
						map.addAttribute("msgError","Your account is inactive.");
						return "signincode";
					}
					userMaster.setUserId(userMaster.getUserId());
					userMaster.setForgetCounter(0);
					if(userMaster.getEntityType()==2)
					{
						userMaster.setSchoolId(schoolMaster);
						userMasterDAO.makePersistent(userMaster);
						if(userMaster.getEntityType()!=1)
							createDistrictStatusHome(request);
						return "redirect:externaldshboard.do";
					}
				}
				else
				{
					map.addAttribute("teacherblockflag", 1);
					return "signincode";
				}
			}
			else
			{
				int forgetCounter=0;
				listUserEmail = userMasterDAO.checkUserEmail(emailAddress);
				if(listUserEmail.size()>0)
				{
					try 
					{
						userdetail=userMasterDAO.findById(listUserEmail.get(0).getUserId(), false, false);
						if(userdetail.getForgetCounter()<3)
						{
							forgetCounter=(userdetail.getForgetCounter()+1);
							userdetail.setUserId(userdetail.getUserId());
							userdetail.setForgetCounter(forgetCounter);
							userMasterDAO.makePersistent(userdetail);

							if(forgetCounter==3)
								map.addAttribute("teacherblockflag", 1);
						}
						else
						{
							map.addAttribute("teacherblockflag", 1);
						}
					} catch (Exception e) 
					{
						e.printStackTrace();
					}
				}
				map.addAttribute("authmail",false);
				map.addAttribute("emailAddress", emailAddress);
				map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
				return "signincode";
			}

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.addAttribute("authmail",false);
		map.addAttribute("emailAddress", emailAddress);
		map.addAttribute("msgError",Utility.getLocaleValuePropByKey("msgLogingAuthenticationYouerAccout3", locale));
		return "signincode";
	}
	
	
	@RequestMapping(value="/externaldshboard.do", method=RequestMethod.GET)
	public String doExternalDshboardGET(ModelMap map,HttpServletRequest request)
	{
		System.out.println(" externaldshboard.do GET");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:signincode.do";
		}
		return "externaldshboard";
	}
	
	@RequestMapping(value="/internaljobsboard.do", method=RequestMethod.GET)
	public String doInternalSearchJob(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{		
		Utility.setRefererURLByJobsBoard(request);
		DistrictMaster districtMaster = null;
		SchoolMaster schoolMaster = null;
		HttpSession session = request.getSession();	
		try 
		{	
			String source = "";
			String target = "";
			String path = "";


			String districtId =  "";			
			String schoolId = "";

			try {
				districtId = request.getParameter("districtId")==null?"":request.getParameter("districtId");			
				districtId = ""+ Utility.decryptNo(Integer.parseInt(districtId));	
			} catch (Exception e) {

			}
			try {							
				schoolId = request.getParameter("schoolId")==null?"":request.getParameter("schoolId");
				schoolId = ""+ Utility.decryptNo(Integer.parseInt(schoolId));
			} catch (Exception e) {
				// TODO: handle exception
			}

			session.removeAttribute("schoolMaster");
			session.removeAttribute("districtMaster");

			if((schoolId.equals("0")||schoolId.equals("")) && (districtId.equals("0")||districtId.equals("")) && (session.getAttribute("teacherDetail")==null)){
			}
			if(!schoolId.trim().equals("")){
				try {
					schoolMaster = schoolMasterDAO.findById(new Long(schoolId), false, false);
					if(schoolMaster!=null){
						districtMaster = schoolMaster.getDistrictId();
						if(schoolMaster!=null){
							map.addAttribute("schoolMaster", schoolMaster);
							map.addAttribute("districtMaster", districtMaster);
							session.setAttribute("schoolMaster", schoolMaster);
							session.setAttribute("districtMaster", districtMaster);					

							source = Utility.getValueOfPropByKey("schoolRootPath")+schoolMaster.getSchoolId()+"/"+schoolMaster.getLogoPath();
							target = servletContext.getRealPath("/")+"/"+"/school/"+schoolMaster.getSchoolId()+"/";

							File targetDir = new File(target);
							if(!targetDir.exists())
								targetDir.mkdirs();
							File sourceFile = new File(source);

							File targetFile = new File(targetDir+"/"+sourceFile.getName());

							if(sourceFile.exists())
							{
								FileUtils.copyFile(sourceFile, targetFile);
								path = Utility.getValueOfPropByKey("contextBasePath")+"/school/"+schoolMaster.getSchoolId()+"/"+sourceFile.getName();

							}
							map.addAttribute("logoPath", path);
						}
					}
					else{
					}

				} 
				catch (Exception e) {
				}
			}
			else if(!districtId.equals("")){
				try {
					districtMaster = districtMasterDAO.findById(new Integer(districtId), false, false);
					if(districtMaster!=null){

						map.addAttribute("districtMaster", districtMaster);
						session.setAttribute("districtMaster", districtMaster);					

						source = Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/"+districtMaster.getLogoPath();
						target = servletContext.getRealPath("/")+"/"+"/district/"+districtMaster.getDistrictId()+"/";

						File sourceFile = new File(source);
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();

						File targetFile = new File(targetDir+"/"+sourceFile.getName());

						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
							path = Utility.getValueOfPropByKey("contextBasePath")+"/district/"+districtMaster.getDistrictId()+"/"+sourceFile.getName();
						}
						map.addAttribute("logoPath", path);					
					}
					else{
						//response.sendRedirect("signin.do");
					}	
				} 
				catch (Exception e) {
				}
			}
			if(session.getAttribute("logoPath")!=null && (!session.getAttribute("logoPath").equals("")))
				path=(String)session.getAttribute("logoPath");

			if(!Utility.existsURL(path))
			{
				path="images/applyfor-job.png";
				map.addAttribute("logoPath", path);
			}

			
			//List<JobCategoryMaster> jobCategoryMasterlst = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			List<JobCategoryMaster> jobCategoryMasterlst = jobOrderDAO.findUniqueCateogryByJobOrder(districtMaster,true);
			map.addAttribute("jobCategoryMasterlst",jobCategoryMasterlst);	

			// @ASHish :: List of subject
			List<SubjectMaster> lstSubjectMasters = subjectMasterDAO.findActiveSubjectByDistrict(districtMaster);

			// @AShish :: List of State

			String districtDName="";
			String districtAddress="";
			String address="";
			String state="";
			String city = "";
			String zipcode="";
			String districtPnNo="";

			if(districtMaster!=null){
				if(districtMaster.getDisplayName()!=null && !districtMaster.getDisplayName().equals("")){
					districtDName=districtMaster.getDisplayName();
				}else{
					districtDName=districtMaster.getDistrictName();
				}
			}

			/* @Start
			 * @Ashish Kumar
			 * @Description :: Address*/


			if(districtMaster!=null)
			{
				if(districtMaster.getAddress()!=null && !districtMaster.getAddress().equals(""))
				{
					if(!districtMaster.getCityName().equals(""))
					{
						districtAddress = districtMaster.getAddress()+", ";
					}
					else
					{
						districtAddress = districtMaster.getAddress();
					}
				}
				if(!districtMaster.getCityName().equals(""))
				{
					if(districtMaster.getStateId()!=null && !districtMaster.getStateId().getStateName().equals(""))
					{
						city = districtMaster.getCityName()+", ";
					}else{
						city = districtMaster.getCityName();
					}
				}
				if(districtMaster.getStateId()!=null && !districtMaster.getStateId().getStateName().equals(""))
				{
					if(!districtMaster.getZipCode().equals(""))
					{
						state = districtMaster.getStateId().getStateName()+", ";
					}
					else
					{
						state = districtMaster.getStateId().getStateName();
					}	
				}
				if(!districtMaster.getZipCode().equals(""))
				{
					if(districtMaster.getPhoneNumber()!=null && !districtMaster.getPhoneNumber().equals(""))
					{
						zipcode = districtMaster.getZipCode()+", Phone #: ";
					}
					else
					{
						zipcode = districtMaster.getZipCode();
					}
				}

				if(districtMaster.getPhoneNumber()!=null && !districtMaster.getPhoneNumber().equals("")){
					districtPnNo = districtMaster.getPhoneNumber();
				}

				if(districtAddress !="" || city!="" ||state!="" || zipcode!="" || districtPnNo!=""){
					address = districtAddress+city+state+zipcode+districtPnNo;
				}else{
					address="";
				}
			}


			/* @End 
			 * @Ashish Kumar
			 * @Description :: Address*/

			map.addAttribute("address", address);
			map.addAttribute("districtDName", districtDName);
			map.addAttribute("lstJobCategoryMasters", jobCategoryMasterlst);
			map.addAttribute("lstSubjectMasters", lstSubjectMasters);
			//	map.addAttribute("lstStateMasters", lstStateMasters);

		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		String paramText="";
		if(districtMaster==null && schoolMaster==null ){
			map.addAttribute("redirectTo", "1");			
		}
		return "searchjob";
	}
	
	//shadab search for districtjobboard
	@RequestMapping(value="/esSearchdistrictjobboard.do", method=RequestMethod.POST)
	public String esSearchdistrictjobboard(ModelMap modelmap,HttpServletRequest request, HttpServletResponse response)
	{
		//schoolInJobOrderDAO.getSchoolRequisitionNumber(null);
		System.out.println("/esSearchdistrictjobboard.do");
		try
		{
			ElasticSearchService elasticSearchService=new ElasticSearchService();
			String districtId =  request.getParameter("districtId")==null?"0":request.getParameter("districtId").trim();
			String schoolName   =  request.getParameter("schoolName")==null?"0":request.getParameter("schoolName").trim();			
			String geoZoneName =request.getParameter("geoZoneName");
			String jobCategoryName   =  request.getParameter("jobCategoryName")==null?"0":request.getParameter("jobCategoryName").trim();
			String jobCategoryId   =  request.getParameter("jobCategoryId")==null?"0":request.getParameter("jobCategoryId").trim();
			String jobSubCategoryId   =  request.getParameter("jobSubCategoryId")==null?"0":request.getParameter("jobSubCategoryId").trim();
			System.out.println(jobCategoryId);
			System.out.println(jobCategoryName);
			String zipcode =  request.getParameter("zipCode")==null?"0":request.getParameter("zipCode").trim();
			String headQuarterId =  request.getParameter("headQuarterId")==null?"0":request.getParameter("headQuarterId").trim();
			String branchId =  request.getParameter("branchId")==null?"0":request.getParameter("branchId").trim();
			String positionStart=request.getParameter("positionStart")==null?"":request.getParameter("positionStart");
			String talentRef = request.getParameter("talentRef")==null?"":request.getParameter("talentRef");
			String teacherId = request.getParameter("teacherId")==null?"":request.getParameter("teacherId");
			String gradeLevel=request.getParameter("gradeLevel")==null?"":request.getParameter("gradeLevel");
			String requestType=request.getParameter("requestType")==null?"2":request.getParameter("requestType");
			
			//dd-mm-yyyy	to yyyy-mm-dd
			
			//positionStartDate="2015-09-24";
			HttpSession session = request.getSession(false);
			
			if(!jobSubCategoryId.equalsIgnoreCase("0")){
				session.setAttribute("subjobCategoryIdCheck", jobSubCategoryId);
			}else{
				session.setAttribute("subjobCategoryIdCheck", "0");
			}
			int stateId=0;
			try
			{
				stateId=Integer.parseInt(request.getParameter("stateId"));
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			

			String noOfRow			=	request.getParameter("noOfRows");
			String pageNo			=	request.getParameter("page");
			String sortOrder		=	request.getParameter("sortOrderStr");
			String sortOrderType	=	request.getParameter("sortOrderType");
			String subjectName		=   request.getParameter("subjectNameList");
			int	from 				=	0;//Integer.parseInt(request.getParameter("from"));
			
			int noOfRowInPage	=	Integer.parseInt(noOfRow);
			int pgNo			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			//	int start 			= 	0;
			//	int end 			= 	1000;
			int totalRecord 	=	0;
			
			
			//===========================================
			String sortOrderFieldName	=	"jobId";
			String sortOrderNoField		=	"jobId";
			if(districtId.equals("1200390"))
			{
				sortOrderFieldName	=	"jobTitle";
				sortOrderNoField		=	"jobTitle";
			}

			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("jobId")&& !sortOrder.equals("jobTitle")&& !sortOrder.equals("schoolName") && !sortOrder.equals("location") && !sortOrder.equals("geoZoneName") && !sortOrder.equals("subjectName") && !sortOrder.equals("jobEndDate")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("jobId"))
				{
					sortOrderNoField="jobId";
				}
				if(sortOrder.equals("jobTitle"))
				{
					sortOrderNoField="jobTitle";
				}
				if(sortOrder.equals("districtName"))
				{
					sortOrderNoField="districtName";
				}
				if(sortOrder.equals("schoolName"))
				{
					sortOrderNoField="schoolName";
				}
				if(sortOrder.equals("location"))
				{
					sortOrderNoField="location";
				}
				if(sortOrder.equals("geoZoneName"))
				{
					sortOrderNoField="geoZoneName";
				}
				if(sortOrder.equals("subjectName"))
				{
					sortOrderNoField="subjectName";
				}
				if(sortOrder.equals("jobEndDate"))
				{
					sortOrderNoField="jobEndDate";
				}
				if(sortOrder.equals("gradeLevel"))
				{
					sortOrderNoField="gradeLevel";
				}	
				
			}	

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderType="asc";
					//sortOrderStrVal=Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderType="desc";
					//sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				if(districtId.equals("1200390")){
					sortOrderType="asc";
				}else{
					sortOrderType="desc";	
				}
			}	
			
			
			
			
			if(sortOrderNoField.equals("jobId"))
			{
				sortOrderFieldName	=	"jobId";
			}
			if(sortOrderNoField.equals("jobTitle"))
			{
				sortOrderFieldName	=	"jobTitle";
			}			
			if(sortOrderNoField.equals("schoolName"))
			{
				sortOrderFieldName	=	"schoolName";
			}
			if(sortOrderNoField.equals("location"))
			{
				sortOrderFieldName	=	"address";
			}
			if(sortOrderNoField.equals("geoZoneName"))
			{
				sortOrderFieldName	=	"geoZoneName";
			}
			if(sortOrderNoField.equals("subjectName"))
			{
				sortOrderFieldName	=	"subjectName";
			}
			if(sortOrderNoField.equals("jobEndDate"))
			{
				sortOrderFieldName	=	"jobEndDate";
			}	
			if(sortOrderNoField.equals("gradeLevel"))
			{
				sortOrderFieldName	=	"gradeLevel";
			}	
			//===========================================
			
			
			
			int iBranchId=0;
			int iHeadQuarterId=0;
			
			if(branchId!=null && !branchId.equals("0") && !branchId.equals(""))
			{
				 iBranchId=Utility.getIntValue(branchId);
				System.out.println("*********** B **************"+iBranchId);
				
			}
			if(headQuarterId!=null && !headQuarterId.equals("0") && !headQuarterId.equals(""))
			{
				 iHeadQuarterId=Utility.getIntValue(headQuarterId);
				 System.out.println("*********** H **************"+iHeadQuarterId);
			}
			if(districtId.trim().equals(""))
				districtId="0";
			
			DistrictMaster districtMaster =null;
			try{
				districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false,false);
			}catch(Exception e){
				e.printStackTrace();
			}
			String data= elasticSearchService.esForDistrictJobBoard(districtMaster,iBranchId,iHeadQuarterId,Integer.parseInt(districtId), "districtName", schoolName, zipcode, geoZoneName,jobCategoryName, subjectName, from,request,noOfRow,
					pageNo,pgNo,sortOrderFieldName,sortOrderType,request.getParameter("searchTerm"),stateId,jobCategoryId,jobSubCategoryId,positionStart,talentRef,teacherId,gradeLevel,requestType);
		//	System.out.println("return data is*************************");
			//System.out.println(data);
			try {
				PrintWriter out = response.getWriter();
				out.write(data);
				out.flush();
				//System.out.println("flushed");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		
		return "";
	}
	
	
	//shadab end
	
	@RequestMapping(value="/approvejobsignin.do", method=RequestMethod.POST)
	public String approvejobsignin(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("::::::::::approvejobsignin.do::::::::::::Post");
		String emailAddress = request.getParameter("emailAddress")==null?"":request.getParameter("emailAddress").trim();
		String password = request.getParameter("password")==null?"":request.getParameter("password");
		String id = request.getParameter("redirectURL")==null?"":request.getParameter("redirectURL");
		String schoolId = request.getParameter("schoolID")==null?"":request.getParameter("schoolID");
				
		response.setContentType("text/html");  
		String txtRememberme = request.getParameter("txtRememberme");
		if(txtRememberme!=null && txtRememberme.trim().equals("on"))
		{
			Cookie cookie = new Cookie ("user",emailAddress);
			cookie.setMaxAge(365 * 24 * 60 * 60);
			response.addCookie(cookie);			
		}
		PrintWriter pw = null;
		try {
			pw = response.getWriter();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		HttpSession session = null;
		
		try 
		{
			password = MD5Encryption.toMD5(password);
			System.out.println("emailAdress:- "+emailAddress);
			System.out.println("password:- "+password);
			System.out.println("SchoolId:- "+schoolId);
			System.out.println("id:- "+id);
			if(!id.equals(""))
			{
				UserMaster userMaster=null;
				emailAddress = emailAddress.trim();
				List<UserMaster> userMasterList = userMasterDAO.findByEmail(emailAddress);
				
				if(!schoolId.equals(""))	//login SA with SchoolID
				{
					Long sid=null;
					Boolean ispasswordMatch=false;
					
					try{sid = Long.parseLong(schoolId);}catch(Exception e){e.printStackTrace();}
					System.out.println("sid:- "+sid);
					
					if(sid!=null)
					for(UserMaster master : userMasterList)
					{
						if(!ispasswordMatch)
							if(master.getPassword().equalsIgnoreCase(password) )
								ispasswordMatch=true;
						if(master.getSchoolId()!=null && master.getSchoolId().getSchoolId()!=null && master.getSchoolId().getSchoolId().equals(sid))
							userMaster=master;
					}
					
					if(userMaster!=null && !ispasswordMatch)	//If  Password is not matched
					{
						if(!masterPasswordDAO.isValidateUser("user", password))	//If Password is not master password
						{
							userMaster=null;
						}
					}
				}
				else	//login DA and SA with without SchoolID
				{
					if(userMasterList!=null && userMasterList.size()>0)
						userMaster = userMasterList.get(0);
					
					if(userMaster!=null)
					{
						if(!userMaster.getPassword().equalsIgnoreCase(password))
						{
							if(!masterPasswordDAO.isValidateUser("user", password))
							{
								userMaster=null;
							}
						}
					}
				}
				
				String msg="";
				if(userMaster!=null)
				{
					session = request.getSession();
					session.setAttribute("userMaster", userMaster);
				}
				else
					msg = "*Invalid Email or Password. Please check the Email and Password and try again. Password is case sensitive.";
				
				//request.removeAttribute("msg");
				
				if(msg.equals(""))
				{
					System.out.println("inside if 1");
					return "redirect:approveJob.do?id="+id;
				}
				else
				{
					System.out.println("inside if 2");
					return "redirect:approvejobsignin.do?id="+id+"&msg="+msg;
				}
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "";
	}
	
	@RequestMapping(value="/approvejobsignin.do", method=RequestMethod.GET)
	public String approvejobsigninGET(ModelMap map,HttpServletRequest request, HttpServletResponse response)
	{
		System.out.println("::::::::::approvejobsignin.do::::::::::::GET");
		String id = request.getParameter("id")==null? "" : request.getParameter("id"); 
		String msg = request.getParameter("msg")==null? "" : request.getParameter("msg"); 
		map.put("id", id);
		map.put("msg", msg);
		return "approvejobsignin";
	}
	
	/*@RequestMapping(value="/applyteacherjob.do", method=RequestMethod.POST)
	public String doApplyJobForTeacherPOST(ModelMap map,HttpServletRequest request)
	{
		System.out.println("::::::::::applyteacherjob.do::::::::::::POST");
		HttpSession session = request.getSession(true);
		
		String kellyJobId = request.getParameter("kellyJobId")==null? "" : request.getParameter("kellyJobId"); 
		String kellyBranchId = request.getParameter("kellyBranchId")==null? "" : request.getParameter("kellyBranchId");
		
		
		System.out.println(" kellyJobId :: "+kellyJobId);
		System.out.println(" kellyBranchId :: "+kellyBranchId);
		
		session.setAttribute("kellyJobId", kellyJobId);
		session.setAttribute("kellyBranchId", kellyBranchId);
		session.setAttribute("kellyTalentType", "N");
		session.setAttribute("kellyTalentType", "R");
		//map.addAttribute("kellyJobId", kellyJobId);
		//map.addAttribute("kellyBranchId", kellyBranchId);
		
		return "redirect:signin.do";
	}*/
}
