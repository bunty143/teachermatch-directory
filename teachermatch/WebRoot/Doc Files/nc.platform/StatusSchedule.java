package tm.services.quartz;



import java.io.PrintStream;
import java.util.Date;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdSchedulerFactory;


import tm.services.quartz.ElasticSearchDistrictJobBoardUpdateService;
import tm.services.quartz.ElasticSearchIndexUpdateService;
import tm.services.quartz.ElasticSearchJobsOfInterestNotCandidateUpdateService;
import tm.services.quartz.ElasticSearchSchoolJobOrderUpdateService;
import tm.utility.Utility;

public class StatusSchedule {
    Scheduler sche = null;

    public StatusSchedule() throws Exception {
        StdSchedulerFactory sf = new StdSchedulerFactory();
        this.sche = sf.getScheduler();
        this.sche.start();
        int hour = Integer.parseInt(Utility.getValueOfPropByKey((String)"jobAlertHour"));
        int minutes = Integer.parseInt(Utility.getValueOfPropByKey((String)"jobAlertMinutes"));
        //document
        JobDetail elUpdataIndex = new JobDetail("elasticSearchIndexUpdateService", "DEFAULT", ElasticSearchIndexUpdateService.class);
		CronTrigger cronTriggerelUpdataIndex = new CronTrigger("Cron elasticSearchIndexUpdateService",Scheduler.DEFAULT_GROUP,"0 0/56 * * * ?");
        this.sche.scheduleJob(elUpdataIndex, (Trigger)cronTriggerelUpdataIndex);
        //jobboard
        JobDetail elasticSearchDistrictJobBoardUpdateService = new JobDetail("elasticSearchDistrictJobBoardUpdateService", "DEFAULT", ElasticSearchDistrictJobBoardUpdateService.class);
		CronTrigger elasticSearchDistrictJobBoardUpdateServiceCron = new CronTrigger("Cron elasticSearchDistrictJobBoardUpdateService",Scheduler.DEFAULT_GROUP,"0 0/59 * * * ?");
        this.sche.scheduleJob(elasticSearchDistrictJobBoardUpdateService, (Trigger)elasticSearchDistrictJobBoardUpdateServiceCron);
        //managejoborder
        JobDetail elasticSearchSchoolJobOrderUpdateService = new JobDetail("elasticSearchSchoolJobOrderUpdateService", "DEFAULT", ElasticSearchSchoolJobOrderUpdateService.class);
		CronTrigger schoolJobOrder = new CronTrigger("Cron elasticSearchSchoolJobOrderUpdateService",Scheduler.DEFAULT_GROUP,"0 0/2 * * * ?");
       this.sche.scheduleJob(elasticSearchSchoolJobOrderUpdateService, (Trigger)schoolJobOrder);
        
        
		JobDetail hrmsPositionsText = new JobDetail("hrmsPositionsText", "DEFAULT", HrmsPostionsTextSchedular.class);
		CronTrigger HrmsPostionsTextSchedular = new CronTrigger("Cron HrmsPostionsTextSchedular",Scheduler.DEFAULT_GROUP,"0 0 0/1 1/1 * ? *");
		this.sche.scheduleJob(hrmsPositionsText, (Trigger)HrmsPostionsTextSchedular);
       
       try
		{
			String applicantCronExp = Utility.getValueOfPropByKey("applicantCronExp");
		JobDetail applicantHire = new JobDetail("applicantHire",Scheduler.DEFAULT_GROUP,ApplicantHire.class);                                
       CronTrigger applicantHireCron = new CronTrigger("Cron Applicant Hire",Scheduler.DEFAULT_GROUP,applicantCronExp);
       sche.scheduleJob(applicantHire, applicantHireCron);
		}catch (Exception e) {
			e.printStackTrace();
		}
		
    }

    public void shutDownScheduler() {
        try {
            this.sche.shutdown();
            if (this.sche.isShutdown()) {
                System.out.println("Scheduler is shutdown!");
                System.out.println("Job cann't be executed here.");
            } else {
                System.out.println("Scheduler isn't shutdown!");
                System.out.println("Job is executed here.");
            }
        }
        catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}