------------------------------- Sonu Gupta 23-06-2015 ----------------------------------------------------------------

CREATE TABLE `dspqgroupmaster` (
  `groupId` int(10) NOT NULL auto_increment,
  `groupName` varchar(200) NOT NULL,
  `status` varchar(1) NOT NULL,
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`groupId`)
) ENGINE=InnoDB ;

INSERT INTO `dspqgroupmaster` (`groupName`, `status`, `createdDateTime`) VALUES
('Personal', 'A', '2015-06-12 16:16:47'),
('Academics', 'A', '2015-06-12 16:16:47'),
('Credentials', 'A', '2015-06-12 16:17:21'),
('Experiences', 'A', '2015-06-12 16:17:21');

CREATE TABLE `dspqsectionmaster` (
  `sectionId` int(6) NOT NULL auto_increment,
  `groupId` int(6) NOT NULL COMMENT 'Foreign Key to "grouprId" in table "dspqgroupmaster"',
  `sectionName` varchar(200) NOT NULL,
  `status` varchar(1) NOT NULL default 'A',
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`sectionId`),
  KEY `groupId` (`groupId`)
) ENGINE=InnoDB;

ALTER TABLE `dspqsectionmaster`
  ADD CONSTRAINT `dspqsectionmaster_ibfk_1` FOREIGN KEY (`groupId`) REFERENCES `dspqgroupmaster` (`groupId`);

INSERT INTO `dspqsectionmaster` (`groupId`, `sectionName`, `status`, `createdDateTime`) VALUES
(1, 'Cover Letter', 'A', '2015-07-03 12:09:12'),
(1, 'Personal Information', 'A', '2015-06-12 17:01:59'),
(1, 'EEOC', 'A', '2015-06-12 16:26:29'),
(1, 'Permanent Address', 'A', '2015-07-03 14:11:32'),
(1, 'Expected Salary', 'A', '2015-06-12 17:00:37'),
(2, 'Academics', 'A', '2015-06-12 19:40:09'),
(3, 'Credentials', 'A', '2015-06-12 19:41:19'),
(3, 'Certification/Licensure', 'A', '2015-06-12 19:41:50'),
(3, 'References', 'A', '2015-06-12 19:42:31'),
(3, 'Video Links', 'A', '2015-06-12 19:42:55'),
(3, 'Additional Documents', 'A', '2015-06-12 19:46:28'),
(4, 'Resume(PDF preferred)', 'A', '2015-07-03 11:40:52'),
(4, 'Employment History', 'A', '2015-06-12 19:47:49'),
(4, 'Involvement/Volunteer Work/Student Teaching', 'A', '2015-06-12 19:47:49'),
(4, 'Honors', 'A', '2015-06-12 19:49:48'),
(1, 'Date of Birth (DOB)', 'A', '2015-07-01 12:37:48'),
(1, 'Present Address', 'A', '2015-06-17 19:35:50'),
(1, 'Language', 'A', '2015-06-17 19:48:08'),
(1, 'Phone Number', 'A', '2015-07-03 11:29:57'),
(1, 'Is Veteran ', 'A', '2015-07-03 14:28:28'),
(2, 'Substitute Teacher', 'A', '2015-07-03 11:40:25'),
(2, 'Drivers License', 'A', '2015-07-03 14:16:01'),
(2, 'TFA', 'A', '2015-07-03 11:40:04'),
(4, 'Teacher Retirement', 'A', '2015-07-06 16:44:03');


CREATE TABLE `dspqfieldmaster` (
  `dspqFieldId` int(6) NOT NULL auto_increment,
  `sectionId` int(6) default NULL COMMENT 'Foreign Key to "sectionId" in table "dspqsectionmaster"',
  `dspqFieldName` varchar(100) NOT NULL,
  `isRequired` tinyint(1) NOT NULL default '0',
  `numberRequired` tinyint(1) NOT NULL default '0',
  `isAdditionalField` tinyint(1) default '0' COMMENT '"0" -TM Default , "1" -Additional',
  `status` varchar(1) NOT NULL,
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`dspqFieldId`)
) ENGINE=InnoDB ;

INSERT INTO `dspqfieldmaster` (`sectionId`, `dspqFieldName`, `isRequired`, `numberRequired`, `isAdditionalField`, `status`, `createdDateTime`) VALUES
(1, 'I do not want to add a cover letter', 0, 0, 0, 'A', '2015-07-03 12:09:35'),
(1, 'Please type in your cover letter', 0, 0, 0, 'A', '2015-07-03 12:10:00'),
(1, 'I have already submitted a cover letter for another job at this District/School, so please use that ', 0, 0, 0, 'A', '2015-07-03 12:10:03'),
(1, 'I am currently an employee of this District', 0, 0, 0, 'A', '2015-07-03 12:10:05'),
(2, 'Salutation', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(2, 'First Name', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(2, 'Last Name', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(3, 'Ethnic Origin', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(3, 'Ethnicity', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(3, 'Race', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(3, 'Gender', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(4, 'Address Line 1', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(4, 'Address Line 2', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(4, 'Country', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(4, 'Zip Code', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(4, 'State', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(4, 'City', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(4, 'Mobile', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(5, 'Expected Salary  $', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Degree', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'School', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Field of Study', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Dates Attended', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Transcript/Diploma/Certificate', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Freshman', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Sophomore', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Junior', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Senior', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(6, 'Cumulative', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(7, 'Years of Certified Teaching Experience', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(7, 'National Board Certification/Licensure', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Certification/Licensure Status', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Certification Type', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'State', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Year Received', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Year Expires', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Certification/Licensure Name', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'DOE Number', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Certification/Licensure Url', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Grade Level(s)', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Praxis I Reading: 5710(0710)', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Praxis I Writing: 5720(0720)', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Praxis I Mathematics: 5730(0730)', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(8, 'Certification/Licensure Letter', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'Salutation', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'First Name', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'Last Name', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'Title', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'Organization/Emp.', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'Contact Number', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'Email', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'Recommendation Letter', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(9, 'Can this person be directly contacted by the hiring authority?', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(10, 'Video Link', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(10, 'Video', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(11, 'Additional Documents', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(11, 'Document(please upload a file)', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(12, 'Resume', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Position', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Role', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Field', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Name of Organization/Emp.', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Duration', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'City', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'State Of Organization', 1, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'From', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'To', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Annual Salary Amount (in $)', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Type of Role', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Primary Responsibilities in this Role', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Most Significant Contributions in this Role', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(13, 'Reason for leaving', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(14, 'Organization', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(14, 'Type of Organization', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(14, 'Number of people in this Organization', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(14, 'Did you lead people in this organization?', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(14, 'How many people ?', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(15, 'Award', 0, 0, 0, 'A', '2015-06-18 12:37:36'),
(2, 'Middle Name', 0, 0, 1, 'A', '2015-06-18 12:37:36'),
(2, 'Another Name', 0, 0, 1, 'A', '2015-06-18 12:37:36'),
(2, 'Social Security Number', 0, 0, 1, 'A', '2015-07-03 14:21:25'),
(4, 'Other City', 0, 0, 1, 'A', '2015-06-27 17:20:26'),
(4, 'Other State', 0, 0, 1, 'A', '2015-06-27 17:20:26'),
(16, 'Month', 1, 0, 1, 'A', '2015-07-04 11:40:56'),
(16, 'Day', 1, 0, 1, 'A', '2015-07-06 17:05:40'),
(16, 'Year', 1, 0, 1, 'A', '2015-07-06 17:05:44'),
(17, 'Address Line 1', 0, 0, 1, 'A', '2015-06-18 12:40:20'),
(17, 'Address Line 2', 0, 0, 1, 'A', '2015-06-18 12:40:20'),
(17, 'Country', 0, 0, 1, 'A', '2015-06-18 12:40:20'),
(17, 'ZipCode', 0, 0, 1, 'A', '2015-06-18 12:40:20'),
(17, 'State', 0, 0, 1, 'A', '2015-06-18 12:40:20'),
(17, 'City', 0, 0, 1, 'A', '2015-06-18 12:40:20'),
(17, 'Other City', 0, 0, 1, 'A', '2015-06-18 12:40:20'),
(17, 'Other State', 0, 0, 1, 'A', '2015-06-27 17:22:37'),
(18, 'Language', 0, 0, 1, 'A', '2015-06-18 12:41:06'),
(19, 'Phone', 0, 0, 1, 'A', '2015-07-06 17:04:29'),
(20, 'I am a veteran or disabled veteran or spouse of a disabled veteran of the US Armed Forces*', 0, 0, 1, 'A', '2015-07-03 14:28:38'),
(21, 'If required, would you be willing to serve as a substitute/part-time teacher?', 0, 0, 1, 'A', '2015-07-03 12:18:45'),
(22, 'Driving Licence State', 0, 0, 1, 'A', '2015-07-03 14:16:48'),
(22, 'Driving Licence Number', 0, 0, 1, 'A', '2015-07-03 14:35:05'),
(23, 'TFA', 0, 0, 1, 'A', '2015-07-06 17:05:13'),
(23, 'Corps Year', 1, 0, 1, 'A', '2015-07-06 17:05:18'),
(23, 'TFA Region', 1, 0, 1, 'A', '2015-07-06 17:05:22'),
(24, 'Teacher Retirement Number', 1, 0, 1, 'A', '2015-07-03 11:46:39'),
(24, 'Retired From State', 1, 0, 1, 'A', '2015-07-03 11:45:39'),
(24, 'Retired From District', 1, 0, 1, 'A', '2015-07-03 11:46:00'),
(24, 'Money Withdrawal Date', 0, 0, 1, 'A', '2015-07-03 14:20:57'),
(24, 'Retirement Code', 0, 0, 1, 'A', '2015-07-03 14:21:04'),
(24, 'Seniority Number', 0, 0, 1, 'A', '2015-07-03 14:21:33'),
(24, 'Are you retired from district?', 0, 0, 1, 'A', '2015-07-03 11:45:04');

CREATE TABLE `dspqportfolioname` (
  `dspqPortfolioNameId` int(6) NOT NULL auto_increment,
  `portfolioName` varchar(200) NOT NULL,
  `districtId` int(11) NOT NULL,
  `jobCategoryId` int(4) default NULL,
  `status` varchar(1) NOT NULL,
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`dspqPortfolioNameId`),
  KEY `dspqportfolioname _ibfk_1` (`districtId`),
  KEY `dspqportfolioname _ibfk_2` (`jobCategoryId`)
) ENGINE=InnoDB ;

ALTER TABLE `dspqportfolioname`
  ADD CONSTRAINT `dspqportfolioname _ibfk_1` FOREIGN KEY (`districtId`) REFERENCES `districtmaster` (`districtId`),
  ADD CONSTRAINT `dspqportfolioname _ibfk_2` FOREIGN KEY (`jobCategoryId`) REFERENCES `jobcategorymaster` (`jobCategoryId`);

CREATE TABLE `dspqrouter` (
  `dspqRouterId` int(11) NOT NULL auto_increment,
  `sectionId` int(5) default NULL COMMENT 'Foreign Key to "sectionId" in table "dspqsectionmaster"',
  `dspqFieldId` int(11) default NULL COMMENT 'Foreign Key to "dspqFieldId" in table "dspqfieldmaster"',
  `dspqPortfolioNameId` int(11) NOT NULL COMMENT 'Foreign Key to "dspqPortfolioNameId" in table "dspqportfolioname"',
  `displayName` varchar(222) default NULL,
  `isRequired` tinyint(1) default NULL,
  `applicantType` varchar(15) NOT NULL,
  `numberRequired` tinyint(2) default NULL,
  `instructions` varchar(500) default NULL,
  `tooltip` varchar(500) default NULL,
  `status` varchar(1) NOT NULL,
  `createdDateTime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`dspqRouterId`),
  KEY `dspqFieldId` (`dspqFieldId`),
  KEY `dspqPortfolioNameId` (`dspqPortfolioNameId`),
  KEY `sectionId` (`sectionId`)
) ENGINE=InnoDB;

ALTER TABLE `dspqrouter`
  ADD CONSTRAINT `dspqrouter_ibfk_1` FOREIGN KEY (`dspqPortfolioNameId`) REFERENCES `dspqportfolioname` (`dspqPortfolioNameId`),
  ADD CONSTRAINT `dspqrouter_ibfk_2` FOREIGN KEY (`sectionId`) REFERENCES `dspqsectionmaster` (`sectionId`);
  
----------------------------------- Sonu Gupta 08/07/2015 -----------------------------------
  
  INSERT INTO  `dspqfieldmaster` (
`dspqFieldId` ,
`sectionId` ,
`dspqFieldName` ,
`isRequired` ,
`numberRequired` ,
`isAdditionalField` ,
`status` ,
`createdDateTime`
)
VALUES (
NULL ,  '1',  'I am currently a full time instructional employee (Teacher)',  '0',  '0',  '0',  'A', NOW( )
);
  
  INSERT INTO  `dspqfieldmaster` (
`dspqFieldId` ,
`sectionId` ,
`dspqFieldName` ,
`isRequired` ,
`numberRequired` ,
`isAdditionalField` ,
`status` ,
`createdDateTime`
)
VALUES (
NULL ,  '1',  'I am currently a part-time instructional employee (Teacher)',  '0',  '0',  '0',  'A', NOW( )
);
  
  INSERT INTO  `dspqfieldmaster` (
`dspqFieldId` ,
`sectionId` ,
`dspqFieldName` ,
`isRequired` ,
`numberRequired` ,
`isAdditionalField` ,
`status` ,
`createdDateTime`
)
VALUES (
NULL ,  '1',  'I am currently a non instructional employee',  '0',  '0',  '0',  'A', NOW( )
);
  
  UPDATE  `dspqsectionmaster` SET  `status` =  'I',
`createdDateTime` = NOW( ) WHERE  `dspqsectionmaster`.`sectionId` =5 LIMIT 1 ;

UPDATE  `dspqfieldmaster` SET  `sectionId` =  '2',
`createdDateTime` = NOW( ) WHERE  `dspqfieldmaster`.`dspqFieldId` =19 LIMIT 1 ;

----------------------------------- Sonu Gupta 22/07/2015 -----------------------------------

UPDATE  `dspqsectionmaster` SET  `groupId` =  '3',
`createdDateTime` = NOW( ) WHERE  `sectionId` =21 ;

UPDATE  `dspqsectionmaster` SET  `groupId` =  '3',
`createdDateTime` = NOW( ) WHERE `sectionId` =23;

UPDATE  `dspqsectionmaster` SET  `groupId` =  '1',
`createdDateTime` = NOW( ) WHERE  `sectionId` =22;