package tm.services.hqbranches;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NavigableSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.Alignment;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.connection.ConnectionProvider;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.SessionFactoryImplementor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import tm.bean.CommunicationsDetail;
import tm.bean.DistrictRequisitionNumbers;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.SchoolWiseCandidateStatus;
import tm.bean.TalentKSNDetail;
import tm.bean.TeacherAssessmentStatus;
import tm.bean.TeacherDetail;
import tm.bean.TeacherExperience;
import tm.bean.TeacherNormScore;
import tm.bean.TeacherSecondaryStatus;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.assessment.AssessmentDetail;
import tm.bean.assessment.AssessmentJobRelation;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.hqbranchesmaster.ReasonsForWaived;
import tm.bean.hqbranchesmaster.StatusNodeColorHistory;
import tm.bean.hqbranchesmaster.WorkFlowStatus;
import tm.bean.master.ApplitrackDistricts;
import tm.bean.master.DistrictMaster;
import tm.bean.master.GeoZoneMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SecondaryStatusMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.SubjectMaster;
import tm.bean.mq.MQEvent;
import tm.bean.teacher.SpInboundAPICallRecord;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.AdminActivityDetailDAO;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.SchoolWiseCandidateStatusDAO;
import tm.dao.TalentKSNDetailDAO;
import tm.dao.TeacherAssessmentStatusDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherExperienceDAO;
import tm.dao.TeacherNormScoreDAO;
import tm.dao.TeacherSecondaryStatusDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.assessment.AssessmentJobRelationDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.ReasonsForWaivedDAO;
import tm.dao.hqbranchesmaster.StatusNodeColorHistoryDAO;
import tm.dao.hqbranchesmaster.WorkFlowStatusDAO;
import tm.dao.master.DemoClassScheduleDAO;
import tm.dao.master.GeoZoneMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.JobCategoryMasterHistoryDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SecondaryStatusMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.SubjectMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.mq.MQEventDAO;
import tm.dao.teacher.SpInboundAPICallRecordDAO;
import tm.services.CommonService;
import tm.services.EmailerService;
import tm.services.HqJobCategoryAjax;
import tm.services.MailText;
import tm.services.ManageJobOrdersAjax;
import tm.services.PaginationAndSorting;
import tm.services.ScheduleMailThreadWithAttachment;
import tm.services.district.GlobalServices;
import tm.services.district.ManageStatusAjax;
import tm.services.mq.MQService;
import tm.services.report.CandidateGridService;
import tm.services.teacher.MQEventThread;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.TestTool;
import tm.utility.Utility;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

import edu.emory.mathcs.backport.java.util.concurrent.ExecutorService;
import edu.emory.mathcs.backport.java.util.concurrent.Executors;

public class KellyONRAjax {
	
private static final Logger logger = Logger.getLogger(KellyONRAjax.class.getName());
static String locale = Utility.getValueOfPropByKey("locale");
private static final int MYTHREADS = 1000;
	public static List<String> nodeList=new ArrayList<String>();
	private static Map<Integer,String> OPTION_MAP=new LinkedHashMap<Integer,String> ();
	static {
		nodeList.add(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
		nodeList.add(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
		nodeList.add(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
		OPTION_MAP.put(1, "Need INV to WF1");
		OPTION_MAP.put(2, "Smart Practices Incomplete");
		OPTION_MAP.put(3, "Workflow 1 Incomplete");
		OPTION_MAP.put(4, "Need INV to WF2");
		OPTION_MAP.put(5, "Workflow 2 Incomplete");
		OPTION_MAP.put(6, "Unlinked");
	}
	
	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private SchoolWiseCandidateStatusDAO schoolWiseCandidateStatusDAO;

	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;
	
	@Autowired
	private TeacherDetailDAO teacherDetailDAO;
	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;
	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	
	
	@Autowired
	private TeacherNormScoreDAO teacherNormScoreDAO;
	
	private AssessmentJobRelationDAO assessmentJobRelationDAO;
	@Autowired
	private TeacherAssessmentStatusDAO teacherAssessmentStatusDAO;
	@Autowired
	private DemoClassScheduleDAO demoClassScheduleDAO;

	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;

	private GeoZoneMasterDAO geoZoneMasterDAO;
	@Autowired
	private SubjectMasterDAO subjectMasterDAO;
	@Autowired
	private TeacherExperienceDAO teacherExperienceDAO;
	@Autowired
	private MQEventDAO mqEventDAO;
	@Autowired
	private WorkFlowStatusDAO workFlowStatusDAO;
	
	@Autowired
	private EmailerService emailerService;
	
	@Autowired
	private ManageStatusAjax manageStatusAjax;
	
	@Autowired
	private TalentKSNDetailDAO talentKSNDetailDAO;
	
	@Autowired
    private SpInboundAPICallRecordDAO spInboundAPICallRecordDAO;
	
	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;
	
	@Autowired
	private StatusNodeColorHistoryDAO statusNodeColorHistoryDAO;
	
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private ReasonsForWaivedDAO reasonsForWaivedDAO;
	
	@Autowired
	private ManageJobOrdersAjax manageJobOrdersAjax;
	
	@Autowired
	private AdminActivityDetailDAO adminActivityDetailDAO;
	
	@Autowired
	private JobCategoryMasterHistoryDAO jobCategoryMasterHistoryDAO;
	
	@Autowired
	private HqJobCategoryAjax hqJobCategoryAjax;
	
	public void setEmailerService(EmailerService emailerService) {
		this.emailerService = emailerService;
	}
	
	@Autowired
	private CommonService commonService;
	
	@Autowired
	private TeacherSecondaryStatusDAO teacherSecondaryStatusDAO;
	
	@Autowired
	private SecondaryStatusMasterDAO secondaryStatusMasterDAO;
	public String displayKellyONBGrid(BigInteger ksn,String fname,String lname,String email,String jobcategoryId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean ksnId,boolean onb,boolean hired,boolean tTIE,boolean Withdrawn,boolean hidden,Integer actionFlag,String[] TeacherIds,Integer actionSearchId,String jobId,Integer branchId) throws WriteException, IOException
	{
		
		System.out.println("ksnId : "+ksnId+" onb:"+onb+" hired:"+hired+" tTIE:"+tTIE+" Withdrawn:"+Withdrawn+" hidden :"+hidden+" actionFlag:"+actionFlag+" TeacherIds:"+TeacherIds+" actionSearchId:"+actionSearchId+" jobId:"+jobId+" branchId:"+branchId);
		
		boolean qflag=false;
		boolean osFlag = true;
		boolean statusDependency = false;
		logger.info(" displayKellyONBGrid called ");
		try{
			fname		=	fname.trim()==null?"":fname.trim();
			lname		=	lname.trim()==null?"":lname.trim();
			email	    =	email.trim()==null?"":email.trim();
		}catch(Exception e){}

		UserMaster userMaster 			= null;
		HeadQuarterMaster headQuarterMaster 	= null;
		BranchMaster branchMaster = null;
		RoleMaster roleMaster 			= null;
		WebContext context;
        context = WebContextFactory.get();
        HttpServletRequest request = context.getHttpServletRequest();
        HttpSession session1 = request.getSession(false);
        if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
              throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
        }
        StringBuffer dmRecords = new StringBuffer();
	        try{
	        	 boolean loginUserType = true;
	 	        if (session1 == null || session1.getAttribute("userMaster") == null) {
	 	        }else{
	 	        	userMaster=(UserMaster)session1.getAttribute("userMaster");
	 	        	if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null){
	 	        		headQuarterMaster=userMaster.getHeadQuarterMaster();
	 	        		if(userMaster!=null && userMaster.getBranchMaster()!=null)
		 	        		branchMaster=userMaster.getBranchMaster();
		 	        		
	 	        		loginUserType=true;
	 	        		
	 	        		if(userMaster.getRoleId().getRoleId()==12 || userMaster.getRoleId().getRoleId()==13){
	 	        			loginUserType=false;
	 	        		}
	 	        	}
	 	        		
	 	        	if(userMaster.getRoleId()!=null){
	 	        		roleMaster = userMaster.getRoleId();
	 	        	}
	 	        }
	        Map<Integer,List<SecondaryStatus>> secStatusListByCat = new HashMap<Integer, List<SecondaryStatus>>();
	        Integer userRoleId	=	roleMaster.getRoleId();
	        Integer userId 		= 	userMaster.getUserId();
	        
	        /*============= For Sorting ===========*/
			int noOfRowInPage 	= 	Integer.parseInt(noOfRow);
			int pgNo 			= 	Integer.parseInt(pageNo);
			int start 			= 	((pgNo-1)*noOfRowInPage);
			int end 			= 	((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord		= 	0;
			
			System.out.println("noOfRowInPage:: "+noOfRowInPage+" pgNo: "+pgNo+" start:"+start+" noOfRowInPage: "+noOfRowInPage+" end:"+end);
			//------------------------------------
			
			/*====== set default sorting fieldName ====== **/
			/** set default sorting fieldName **/
			String sortOrderFieldName	=	"tLastName";
			String sortOrderNoField		=	"tLastName";
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal		=	null;
			if(sortOrder!=null){
				 if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("teacherId"))
				 {
					 sortOrderFieldName=sortOrder;
					 sortOrderNoField=sortOrder;
				 }
				 if(sortOrder.equals("createdDateTime"))
				 {
					 sortOrderNoField="createdDateTime";
				 }
				 
			}
	
			if(!sortOrder.equals("") && !sortOrder.equals(null))
			{
				sortOrderFieldName		=	sortOrder;
			}
			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null))
			{
				if(sortOrderType.equals("0"))
				{
					sortOrderStrVal		=	Order.asc(sortOrderFieldName);
				}
				else
				{
					sortOrderTypeVal	=	"1";
					sortOrderStrVal		=	Order.desc(sortOrderFieldName);
				}
			}
			else
			{
				sortOrderTypeVal		=	"0";
				sortOrderStrVal			=	Order.asc(sortOrderFieldName);
			}
			List<TeacherDetail> lstAllTeacher =new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstAllJFTTeacher =new ArrayList<TeacherDetail>();
			List<JobForTeacher> listJobForTeacher =	 new ArrayList<JobForTeacher>();
			List<JobCategoryMaster> jobCategorySubList = new ArrayList<JobCategoryMaster>();
			Map<JobCategoryMaster,JobCategoryMaster> allCategMap = new HashMap<JobCategoryMaster,JobCategoryMaster>();
			Map<TeacherDetail,TeacherDetail> allTeacher = new HashMap<TeacherDetail,TeacherDetail>();
			Map<JobOrder,JobOrder> allJob = new HashMap<JobOrder,JobOrder>();
			List<JobOrder> lstAllJob =new ArrayList<JobOrder>();
			boolean globalFlag=false;
			boolean filtFlag=false;
			String fileName = null;
	
				
	
	
			// Job category
				List<TeacherDetail> jobCateTeacherList = new ArrayList<TeacherDetail>();
				List<JobOrder> listjJobOrders = new ArrayList<JobOrder>();
				boolean jobcategFlag=false;
				if(!jobcategoryId.equals("") && !jobcategoryId.equals("0"))
				{
					JobCategoryMaster jobcatIds =jobCategoryMasterDAO.findById(Integer.parseInt(jobcategoryId), false, false);
					jobcategFlag=true;
					
					 if(jobcatIds!=null){
					   jobCategorySubList = jobCategoryMasterDAO.getJobSubCategoryByParentJobCategory(jobcatIds);
					 }
					
					 if(jobCategorySubList!=null){
						listjJobOrders = jobOrderDAO.findJobOrderByJobCategories(jobCategorySubList); 
					 }
					 if(listjJobOrders!=null && listjJobOrders.size()>0){
						 jobCateTeacherList = jobForTeacherDAO.findUniqueApplicantsbyJobOrders(listjJobOrders);
					 }
				}
				if(jobcategFlag){
					if(globalFlag)
					lstAllTeacher.retainAll(jobCateTeacherList);
					else
					lstAllTeacher.addAll(jobCateTeacherList);				
				}
				if(jobcategFlag)
					globalFlag=true;
				
				// JOB ID
				List<TeacherDetail> jobOrderTeacherList = new ArrayList<TeacherDetail>();
				List<JobOrder> listjJob = new ArrayList<JobOrder>();
				JobOrder jJobOrder = new JobOrder();
				/*boolean jobIdFlag=false;
				if(!jobId.equals("") && !jobId.equals("0"))
				{
					jobIdFlag=true;
					jJobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
					if(jJobOrder!=null)
					listjJob.add(jJobOrder);
					 if(listjJob!=null && listjJob.size()>0){
						 jobOrderTeacherList = jobForTeacherDAO.findUniqueApplicantsbyJobOrders(listjJob);
					 }
				}
				if(jobIdFlag){
					if(globalFlag)
					lstAllTeacher.retainAll(jobOrderTeacherList);
					else
					lstAllTeacher.addAll(jobOrderTeacherList);				
				}
				if(jobIdFlag)
					globalFlag=true;*/
				
				// Branch
				List<TeacherDetail> branchTeacherList = new ArrayList<TeacherDetail>();
				List<JobOrder> listBranchJob = new ArrayList<JobOrder>();
				boolean branchFlag=false;
				System.out.println("branchId:"+branchId);
				if(branchId!=null && !branchId.equals("0"))
				{				
					branchFlag=true;
					
					BranchMaster branch = branchMasterDAO.findById(branchId, false, false);
					 if(branch!=null){
						listBranchJob = jobOrderDAO.getAllJobsByBranch(branch); 
					 }
					 if(listBranchJob!=null && listBranchJob.size()>0){
						 branchTeacherList = jobForTeacherDAO.findUniqueApplicantsbyJobOrders(listBranchJob);
					 }
				}
				if(branchFlag){
					if(globalFlag)
					lstAllTeacher.retainAll(branchTeacherList);
					else
					lstAllTeacher.addAll(branchTeacherList);				
				}
				if(branchFlag)
					globalFlag=true;
				
				//Action Search
				List<MQEvent> mqEvents = new ArrayList<MQEvent>();
				List<MQEvent> tepmMQEvents = new ArrayList<MQEvent>();
				Map<Integer,TeacherDetail> linkToKSNMap = new HashMap<Integer, TeacherDetail>();
				List<TeacherDetail> actionSearchTeacherList = new ArrayList<TeacherDetail>();
				List<TeacherDetail> teacherAssessemntList = new ArrayList<TeacherDetail>();
				List<TeacherDetail> spInboundTeacherList =new ArrayList<TeacherDetail>();
				Set<TeacherDetail> spInboundTeacher =new HashSet<TeacherDetail>();
				boolean actionSearchFlag=false;
	            
				if(actionSearchId!=0)
				{
				  actionSearchFlag=true;
				  if(actionSearchId==6)
				  {
					  actionSearchTeacherList=teacherDetailDAO.getAllNotLinkedToKSNTeacherList();
					  System.out.println("actionSearchTeacherList:"+actionSearchTeacherList.size());
				  }else if(actionSearchId==2)
				  {
					/*  teacherAssessemntList=teacherAssessmentStatusDAO.findSmartPracticesIncompTeachers();
					  spInboundTeacherList= spInboundAPICallRecordDAO.getAllSpInboundTeacherList();
					  spInboundTeacher.addAll(teacherAssessemntList);
					  spInboundTeacher.addAll(spInboundTeacherList);
					  
					  if(spInboundTeacher!=null)
					  actionSearchTeacherList.addAll(spInboundTeacher);
					  
					  System.out.println("teacherAssessemntList:"+teacherAssessemntList.size());
					  System.out.println("spInboundTeacherList:"+spInboundTeacherList.size());
					  System.out.println("spInboundTeacher:"+spInboundTeacher.size());
					  System.out.println("actionSearchTeacherList:"+actionSearchTeacherList.size()); */
					  
				  }else{
					  mqEvents = mqEventDAO.findEventTypeStatus(actionSearchId);
				  }			 
				  if(mqEvents!=null && mqEvents.size()>0){				  
					  
					   if(actionSearchId==1)
					  {
					    for(MQEvent mq : mqEvents){
						  if(mq.getEventType().equals(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)) && mq.getStatus()!=null && mq.getStatus().equals("C")){
							  linkToKSNMap.put(mq.getTeacherdetail().getTeacherId(), mq.getTeacherdetail());
							  tepmMQEvents.add(mq);
						  }
					    }
					    mqEvents.removeAll(tepmMQEvents);
					    for(MQEvent mq : mqEvents){
					    	if(linkToKSNMap.size()>0 && linkToKSNMap.containsValue(mq.getTeacherdetail()))
					    	{
					    	  if(mq.getEventType().equals(Utility.getLocaleValuePropByKey("lblINVWF1", locale))){	
					    		  linkToKSNMap.remove(mq.getTeacherdetail().getTeacherId());
					    	   }
					    	}
					    }
					    if(linkToKSNMap.size()>0)
						    actionSearchTeacherList.addAll(linkToKSNMap.values());	
					  }else if(actionSearchId==3)
					  {
	
						    for(MQEvent mq : mqEvents){
							  if(mq.getEventType().equals(Utility.getLocaleValuePropByKey("lblINVWF1", locale)) && mq.getStatus()!=null &&  mq.getStatus().equals("R")){
								  linkToKSNMap.put(mq.getTeacherdetail().getTeacherId(), mq.getTeacherdetail());
								  tepmMQEvents.add(mq);
							  }
						    }
						    mqEvents.removeAll(tepmMQEvents);
						    for(MQEvent mq : mqEvents){
						    	if(linkToKSNMap.size()>0 && linkToKSNMap.containsValue(mq.getTeacherdetail()))
						    	{
						    	  if(mq.getEventType().equals(Utility.getLocaleValuePropByKey("lblINVWF1", locale)) && mq.getStatus()!=null && mq.getStatus().equals("C")){
						    		  linkToKSNMap.remove(mq.getTeacherdetail().getTeacherId());
						    	   }
						    	}
						    }
						    if(linkToKSNMap.size()>0)
						    actionSearchTeacherList.addAll(linkToKSNMap.values());					  
					  }else if(actionSearchId==4)
					  {
						    for(MQEvent mq : mqEvents){
								  if(mq.getEventType().equals(Utility.getLocaleValuePropByKey("lblINVWF1", locale)) && mq.getStatus()!=null && mq.getStatus().equals("C")){
									  linkToKSNMap.put(mq.getTeacherdetail().getTeacherId(), mq.getTeacherdetail());
									  tepmMQEvents.add(mq);
								  }
							    }
						     mqEvents.removeAll(tepmMQEvents);
							    for(MQEvent mq : mqEvents){
							    	if(linkToKSNMap.size()>0 && linkToKSNMap.containsValue(mq.getTeacherdetail()))
							    	{
							    	  if(mq.getEventType().equals(Utility.getLocaleValuePropByKey("lblINVWF2", locale))){	
							    		  linkToKSNMap.remove(mq.getTeacherdetail().getTeacherId());
							    	   }
							    	}
							    }
							    if(linkToKSNMap.size()>0)
								    actionSearchTeacherList.addAll(linkToKSNMap.values());	
					  }else if(actionSearchId==5)
					  {
	
						    for(MQEvent mq : mqEvents){
							  if(mq.getEventType().equals(Utility.getLocaleValuePropByKey("lblINVWF2", locale)) && mq.getStatus()!=null && mq.getStatus().equals("R")){
								  linkToKSNMap.put(mq.getTeacherdetail().getTeacherId(), mq.getTeacherdetail());
								  tepmMQEvents.add(mq);
							  }
						    }
						    mqEvents.removeAll(tepmMQEvents);
						    for(MQEvent mq : mqEvents){
						    	if(linkToKSNMap.size()>0 && linkToKSNMap.containsValue(mq.getTeacherdetail()))
						    	{
						    	  if(mq.getEventType().equals(Utility.getLocaleValuePropByKey("lblINVWF2", locale)) && mq.getStatus()!=null && mq.getStatus().equals("C")){
						    		  linkToKSNMap.remove(mq.getTeacherdetail().getTeacherId());
						    	   }
						    	}
						    }
						    if(linkToKSNMap.size()>0)
						    actionSearchTeacherList.addAll(linkToKSNMap.values());					  
					  }
				  }
				}
				if(actionSearchFlag){
					if(globalFlag)
					lstAllTeacher.retainAll(actionSearchTeacherList);
					else
					lstAllTeacher.addAll(actionSearchTeacherList);				
				}
				if(actionSearchFlag)
					globalFlag=true;
				
				System.out.println("actionSearchTeacherList:::::::::::::::"+actionSearchTeacherList.size());
				  
				List<TeacherDetail> listOfSPInboundTeacherDetail  = new ArrayList<TeacherDetail>();
				if(ksnId){
						
				    //listOfSPInboundTeacherDetail = spInboundAPICallRecordDAO.getAllSpInboundTeacherList();
				    //System.out.println("listOfSPInboundTeacherDetail  >>>>"+listOfSPInboundTeacherDetail);
				}
				
				if(ksnId){
					if(globalFlag)
					lstAllTeacher.retainAll(listOfSPInboundTeacherDetail);
					else
					lstAllTeacher.addAll(listOfSPInboundTeacherDetail);				
				}
				if(ksnId)
					globalFlag=true;
				
				
				
				logger.info("All teacher after filter:"+lstAllTeacher.size());
				
				//filterTeacherList=jobForTeacherDAO.findAllJFTForKellyDashboard(lstAllTeacher,userMaster,globalFlag);
			
			if(actionFlag==0){
			dmRecords.append("<table id='onbGridTable' border='0'>");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			String selectHeader="";
			
			if(loginUserType)
				selectHeader="<span style='margin:0px;padding:0px'><input type='checkbox' name='SelectAllCandidate' style=' vertical-align: middle;' id='select_all_candidate' onclick='selectAllCandidate()'><label style=' vertical-align: top;'><span style='color:white'>&nbsp;"+Utility.getLocaleValuePropByKey("lblCheckUncheck", locale)+"</span></label></span>";
			//else
				//selectHeader="<span style='margin:0px;padding:0px'><input type='checkbox' name='SelectAllCandidate' style=' vertical-align: middle;' id='select_all_candidate' disabled ><label style=' vertical-align: top;'><span style='color:white'>&nbsp;"+Utility.getLocaleValuePropByKey("lblCheckUncheck", locale)+"</span></label></span>";
			
			
			String responseText="";
			    responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNm", locale),sortOrderFieldName,"tLastName",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top'>"+responseText+selectHeader+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblKSNID", locale),sortOrderFieldName,"ksnid",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top'>"+responseText+"</th>");
				
				if(Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale).equalsIgnoreCase("yes")){
					responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJoTil", locale),sortOrderFieldName,"jobtitle",sortOrderTypeVal,pgNo);
					dmRecords.append("<th valign='top'>"+responseText+"</th>");
					}else{
						responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblMASTERJOBCATEGORY", locale),sortOrderFieldName,"jobcategoryname",sortOrderTypeVal,pgNo);
						dmRecords.append("<th valign='top'>"+responseText+"</th>");
					}
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblJOBID", locale),sortOrderFieldName,"jobid",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top'>"+responseText+"</th>");
			
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblEPI", locale),sortOrderFieldName,"normscore",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Educators Professional Inventory'>"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblAPP", locale),sortOrderFieldName,"app",sortOrderTypeVal,pgNo);
			    dmRecords.append("<th valign='top' title='Application'>"+responseText+"</th>");
			    responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSPS", locale),sortOrderFieldName,"sps",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Smart Practices Status'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),sortOrderFieldName,"linktoksn",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)+"'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblINVWF1", locale),sortOrderFieldName,"nvwf1",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title=' Invite to eRegistration Workflow 1'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblWF1COM", locale),sortOrderFieldName,"wf1com",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Workflow 1 Complete'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblINVWF2", locale),sortOrderFieldName,"invwf2",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title=' Invite to eRegistration Workflow 2'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblWF2COM", locale),sortOrderFieldName,"wf2com",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Workflow 2 Complete'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblINVWF3", locale),sortOrderFieldName,"invwf3",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title=' Invite to eRegistration Workflow 3'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblWF3COM", locale),sortOrderFieldName,"wf3com",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Workflow 3 Complete'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblSCR", locale),sortOrderFieldName,"scr",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Screening'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblTALTYP", locale),sortOrderFieldName,"taltyp",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Talent Type'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblWTHD", locale),sortOrderFieldName,"wthd",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Withdrawn'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblINE", locale),sortOrderFieldName,"ine",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Inactive Eligible'>"+responseText+"</th>");
				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblINIE", locale),sortOrderFieldName,"inie",sortOrderTypeVal,pgNo);
				dmRecords.append("<th valign='top' title='Inactive Not Eligible'>"+responseText+"</th>");
//				responseText=PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblOS", locale),sortOrderFieldName,"os",sortOrderTypeVal,pgNo);
//				dmRecords.append("<th valign='top' title='Overall Status'>OS</th>");
	
			dmRecords.append("<th valign='top'>");
			//dmRecords.append("&nbsp;<span class=\"btn-group\"><a data-toggle=\"dropdown\" href=\"javascript:void(0);\"><span class=\"icon-collapse  iconcolorOnr icon-large\"></span></a>");
			dmRecords.append("<ul class=\"dropdown-menu pull-left\" style=\"margin-left:-135px;\">");
			String statusType="";
			if(statusType.equals("A")){
				dmRecords.append("<li class=\"active-link\"><a href=javascript:void(0); onclick='getStatusType(\"A\");'>"+Utility.getLocaleValuePropByKey("optAll", locale)+"</a></li>");
			} else {
				dmRecords.append("<li><a href=javascript:void(0); onclick='getStatusType(\"A\");'>"+Utility.getLocaleValuePropByKey("optAll", locale)+"</a></li>");
			}
			if(statusType.equals("F")){
				dmRecords.append("<li class=\"active-link\"><a href=javascript:void(0); onclick='getStatusType(\"F\");'>"+Utility.getLocaleValuePropByKey("lblFailed", locale)+"</a></li>");
			} else {
				dmRecords.append("<li><a href=javascript:void(0); onclick='getStatusType(\"F\");'>"+Utility.getLocaleValuePropByKey("lblFailed", locale)+"</a></li>");
			}
			if(statusType.equals("Q")){
				dmRecords.append("<li class=\"active-link\"><a href=javascript:void(0); onclick='getStatusType(\"Q\");'>"+Utility.getLocaleValuePropByKey("msgIntheQueue", locale)+"</a></li>");
			} else {
				dmRecords.append("<li><a href=javascript:void(0); onclick='getStatusType(\"Q\");'>"+Utility.getLocaleValuePropByKey("msgIntheQueue", locale)+"</a></li>");
			}
			if(statusType.equals("N")){
				dmRecords.append("<li class=\"active-link\"><a href=javascript:void(0); onclick='getStatusType(\"N\");'>"+Utility.getLocaleValuePropByKey("msgNew", locale)+"</a></li>");
			} else {
				dmRecords.append("<li><a href=javascript:void(0); onclick='getStatusType(\"N\");'>"+Utility.getLocaleValuePropByKey("msgNew", locale)+"</a></li>");
			}
			dmRecords.append("</ul>");
			dmRecords.append("</span></span>");
	
			dmRecords.append("</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			}
			
			List<String> nodeList = new ArrayList<String>();
			nodeList.add(Utility.getLocaleValuePropByKey("lblAPP", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblSPS", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblSCR", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblTALTYP", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblWTHD", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblINE", locale));
			nodeList.add(Utility.getLocaleValuePropByKey("lblINIE", locale));
		//	nodeList.add(Utility.getLocaleValuePropByKey("lblOS", locale));
			
			logger.info("hired:"+hired+"--Withdrawn:"+Withdrawn+"--hidden:"+hidden+"--tTIE:"+tTIE);	
			List<StatusMaster> lstStatusMasters = new ArrayList<StatusMaster>();
			HashSet<String> statuss=new HashSet<String>();
			boolean statusFlag=false;
			if(hired || Withdrawn || hidden || tTIE || ksnId){
				if(hired&&!ksnId){
					statuss.add("hird");
				}if(Withdrawn&&!ksnId){
					 statuss.add("widrw");
				}if(hidden&&!ksnId){
					 statuss.add("cghide");
				}if(tTIE&&!ksnId){
					 statuss.add("ielig");
					 statuss.add("iielig");	
				}if(ksnId&&!(hired | Withdrawn | hidden | tTIE)){
					 qflag=true;
					 statuss.add("hird");
					 statuss.add("widrw");
					 statuss.add("cghide");
					 statuss.add("ielig");
					 statuss.add("iielig");			 
				 }if(ksnId &&(hired | Withdrawn | hidden | tTIE)){
					 if(!hired){	
						 statuss.add("hird");
					 }if(!Withdrawn){					 
						 statuss.add("widrw");
					 }if(!hidden){
						 statuss.add("cghide");
					 }if(!tTIE){
						 statuss.add("ielig");
						 statuss.add("iielig");	
					 }
					 qflag=true;
				 }
				 statusFlag=true;
				}
			
			logger.info("statuss::"+statuss.size());
			
			try{
				if(statuss.size()>0)
				lstStatusMasters =	statusMasterDAO.findStatusByShortNameHashSet(statuss);
			}catch (Exception e) {
				e.printStackTrace();
			}
			logger.info("lstStatusMasters::"+lstStatusMasters.size());
			
			System.out.println(" sortOrder >>>>>>>> "+sortOrder);
			////////////////////////////// Start :: Sorting////////////////////////////////////////////////////////////////////////////////
			int columnName=0; //default sorting with teacher last name
				
			if(sortOrder.equalsIgnoreCase("tLastName"))
				columnName=0;
			else if(sortOrder.equalsIgnoreCase("ksnid"))
				columnName = 1;
			else if(sortOrder.equalsIgnoreCase("jobtitle"))
				columnName = 2;
			else if(sortOrder.equalsIgnoreCase("jobcategoryname"))
				columnName = 3;
			else if(sortOrder.equalsIgnoreCase("jobid"))
				columnName = 4;
			else{
				columnName=5;
			}
			///////////////////////////////End ::  Sorting/////////////////////////////////////////////////////////////////////////////////
			if(listjJobOrders!=null && listjJobOrders.size()>0)
			{
				globalFlag = true;
			}else
				globalFlag = false;
			
			if(ksnId){
			   listJobForTeacher=jobForTeacherDAO.findJFTForKellyDashboard_OP(jobId,fname,lname,email,ksn,actionSearchId,lstAllTeacher,lstStatusMasters,userMaster,globalFlag,listjJobOrders,statusFlag , ksnId,qflag,branchId,branchFlag,columnName,sortOrderType);
			}else{
				listJobForTeacher=jobForTeacherDAO.findJFTForKellyDashboard_OP(jobId,fname,lname,email,ksn,actionSearchId,lstAllTeacher,lstStatusMasters,userMaster,globalFlag,listjJobOrders,statusFlag , ksnId,qflag,branchId,branchFlag,columnName,sortOrderType);
			}
			
			List<SecondaryStatusMaster> lstSecondaryStatusMaster = null;
			if(userMaster.getBranchMaster()!=null)
				lstSecondaryStatusMaster = secondaryStatusMasterDAO.findTagsWithBranchInCandidatePool(branchMaster.getBranchId());
			else if(userMaster.getHeadQuarterMaster()!=null)
				lstSecondaryStatusMaster = secondaryStatusMasterDAO.findTagsWithHeadQuarterInCandidatePool(headQuarterMaster.getHeadQuarterId());
			for(JobForTeacher jobForTeacher : listJobForTeacher){
				if(jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
					allCategMap.put(jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId(), jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId());
				
				allTeacher.put(jobForTeacher.getTeacherId(), jobForTeacher.getTeacherId());
				allJob.put(jobForTeacher.getJobId(), jobForTeacher.getJobId());
			}
			
			 if(allTeacher.size()>0)
			 {
			   lstAllJFTTeacher =new ArrayList<TeacherDetail>(allTeacher.values());
			 }
			 if(allJob.size()>0)
			 {
				 lstAllJob =new ArrayList<JobOrder>(allJob.values());
			 }
			
			List<JobCategoryMaster> allCategory = new ArrayList<JobCategoryMaster>();
			if(allCategMap.size()>0)
			{
			   allCategory = new ArrayList<JobCategoryMaster>(allCategMap.values());
			}
			List<TeacherNormScore> teacherNormScoreList=teacherNormScoreDAO.findNormScoreByTeacherList(lstAllJFTTeacher);
			Map<Integer,TeacherNormScore> normMap = new HashMap<Integer, TeacherNormScore>();
			try {
				for (TeacherNormScore teacherNormScore : teacherNormScoreList) {
					normMap.put(teacherNormScore.getTeacherDetail().getTeacherId(), teacherNormScore);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
			//////////////////EPI Sorting ////////////////
			if(sortOrder.equalsIgnoreCase("normscore")){
				if(listJobForTeacher!=null){
					for(JobForTeacher jft : listJobForTeacher){
						//normMap.get(jft.getTeacherId().getTeacherId()!=null ? normMap.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore():(0));
						Double norm=0.0;
						if(normMap.get(jft.getTeacherId().getTeacherId())!=null){
							norm=Double.parseDouble(normMap.get(jft.getTeacherId().getTeacherId()).getTeacherNormScore()+"");
						}
						//norm
						jft.setNormScore(norm);
					}
				}
			}
			
			if(sortOrder.equalsIgnoreCase("normscore")){
				if(sortOrderType.equals("1"))
		 			Collections.sort(listJobForTeacher,JobForTeacher.jobForTeacherComparatorNormScore);
				else
					Collections.sort(listJobForTeacher,JobForTeacher.jobForTeacherComparatorNormScoreDesc);
			}
			
			
			
			
			List<SecondaryStatus> secondaryStatusList = null;
			try {
				secondaryStatusList = secondaryStatusDAO.findSecondaryStatusByJobCategory_Op(allCategory);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			try {
				if(secondaryStatusList!=null && secondaryStatusList.size()>0){
					List<SecondaryStatus> temp = null;
					for(SecondaryStatus s : secondaryStatusList){
							if(secStatusListByCat.get(s.getJobCategoryMaster().getJobCategoryId())!=null)
								temp = secStatusListByCat.get(s.getJobCategoryMaster().getJobCategoryId());
							else{
								temp = new ArrayList<SecondaryStatus>();
							}
							temp.add(s);
							secStatusListByCat.put(s.getJobCategoryMaster().getJobCategoryId(), temp);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			Map<String,TeacherStatusHistoryForJob> nodeMap = new HashMap<String, TeacherStatusHistoryForJob>();
			List<TeacherStatusHistoryForJob> tshfjList = teacherStatusHistoryForJobDAO.findByTeacherSecondaryStatus_Op(lstAllJFTTeacher,secondaryStatusList);
			for(TeacherStatusHistoryForJob tshj : tshfjList)
			{
			 nodeMap.put(tshj.getTeacherDetail().getTeacherId()+"--"+tshj.getJobOrder().getJobId()+"-"+tshj.getSecondaryStatus().getSecondaryStatusName(), tshj);	
			}
			
			//node color for SP
			List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
			Map<String,String> mapHistrory = new HashMap<String,String>();
			List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
			Map<Integer,Integer> spStatusMap=new HashMap<Integer, Integer>();
		    List<TeacherAssessmentStatus>  teacherAssessmentStatusList=new ArrayList<TeacherAssessmentStatus>();
		    Map<Integer,SecondaryStatus> categoryStatusMap=new HashMap<Integer, SecondaryStatus>();
			
			lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findHistoryByTeachersAndJobs_Op(lstAllJFTTeacher,lstAllJob);
			
			if(lstTeacherStatusHistoryForJob.size()>0){
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob :lstTeacherStatusHistoryForJob) {
					if(teacherStatusHistoryForJob.getStatusMaster()!=null){
						String statusM=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatus();
						mapHistrory.put(statusM, teacherStatusHistoryForJob.getStatus());
					}else{
						String sStatus=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatus();
						mapHistrory.put(sStatus, teacherStatusHistoryForJob.getStatus());
					}
				}
			}
			List<TeacherSecondaryStatus> teacherSecondaryStatus =new ArrayList<TeacherSecondaryStatus>();
			Map<String, TeacherSecondaryStatus> teachersTagsExistMap = new HashMap<String, TeacherSecondaryStatus>();
			if(lstAllJFTTeacher.size()>0){
			      teacherAssessmentStatusList=teacherAssessmentStatusDAO.findSmartPracticesTakenByTeachers_Op(lstAllJFTTeacher);
			      logger.info("teacherAssessmentStatusList:::"+teacherAssessmentStatusList.size());
			      if(teacherAssessmentStatusList.size()>0)
			      for (TeacherAssessmentStatus teacherAssessmentStatusObj : teacherAssessmentStatusList) {
			       int keyAss=teacherAssessmentStatusObj.getTeacherDetail().getTeacherId();
			       if(spStatusMap.get(keyAss)==null){
				        if(teacherAssessmentStatusObj.getPass()!=null){
				         if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
				          spStatusMap.put(keyAss,1);
//				          break;
				         }else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F")){
				          spStatusMap.put(keyAss,2);
				         }
				        }else if(teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
				         spStatusMap.put(keyAss,2);
				        }else{
				         spStatusMap.put(keyAss,0);
				        }
			       }else{
				        int prevValue=spStatusMap.get(keyAss);
				        if(teacherAssessmentStatusObj.getPass()!=null){
				         if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
				          spStatusMap.put(keyAss,1);
//				          break;
				         }else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F") && prevValue!=0){
				          spStatusMap.put(keyAss,++prevValue);
				         }
				        }else if(prevValue!=0 && teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
				         spStatusMap.put(keyAss,++prevValue);
				        }else{
				         spStatusMap.put(keyAss,0);
				        }
			       }
			      }      
					teacherSecondaryStatus = teacherSecondaryStatusDAO.findTSSListByTeacherAndUserMaster(lstAllJFTTeacher,userMaster);				
					if(teacherSecondaryStatus!=null &&  teacherSecondaryStatus.size()>0){
						for (TeacherSecondaryStatus tss : teacherSecondaryStatus) {
							String teacherTagKey = tss.getTeacherDetail().getTeacherId()+"##"+tss.getSecondaryStatusMaster().getSecStatusId();
							teachersTagsExistMap.put(teacherTagKey, tss);
						}
					}
			}
			System.out.println(spStatusMap.get(1)+"<<<:::::::::::::::::::::spStatusMap::::::::::::::::::::::::::::::::::::::::::::::"+spStatusMap.size());
			lstTreeStructure = secondaryStatusDAO.findByNameAndJobCats_OP(Utility.getLocaleValuePropByKey("lblSPStatus", locale),allCategory);
			logger.info("secondary status size ONR :"+lstTreeStructure.size());
			List<MQEvent> mqEventList=new ArrayList<MQEvent>();
			Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();
			Map<TeacherDetail,Integer> spInboundMap=new HashMap<TeacherDetail, Integer>();
			List<JobForTeacher> filteredList = new ArrayList<JobForTeacher>();
			try{
			     if(lstTreeStructure!=null){
			    	 for(SecondaryStatus sc : lstTreeStructure)
			    	 {
			    		 if(sc.getJobCategoryMaster()!=null)
			    		 categoryStatusMap.put(sc.getJobCategoryMaster().getJobCategoryId(), sc);
			    	 }
			     }
			   
			     List<SpInboundAPICallRecord> spInboundAPICallRecordList= spInboundAPICallRecordDAO.getDetailsByTeacherList(lstAllJFTTeacher);
			     if(spInboundAPICallRecordList.size()>0){
			     for(SpInboundAPICallRecord sIACR : spInboundAPICallRecordList)
			     {
			    	 spInboundMap.put(sIACR.getTeacherDetail(), 1);
			     }
			     }
			   //end node color for SP
				
				
				//node color
			
				if(lstAllJFTTeacher!=null && lstAllJFTTeacher.size()>0)
				{
				 mqEventList=mqEventDAO.findMQEventByTeacherList_Op(lstAllJFTTeacher);
				 if(mqEventList.size()>0){			 
					 for (MQEvent mqEvent : mqEventList) {				 
							 mapNodeStatus.put(mqEvent.getTeacherdetail()+"-"+mqEvent.getEventType() , mqEvent); 				 
					}
				 }
				}
				List<JobForTeacher> lstJFTremove = new ArrayList<JobForTeacher>(listJobForTeacher);
				logger.info("actionSearchId::::::::::::::::::::::::::::::::"+actionSearchId);
				logger.info("Before ::: listJobForTeacher.size ():::::::::::"+listJobForTeacher.size());
				System.out.println(" actionSearchId :: "+actionSearchId);
				System.out.println("ksnId : "+ksnId);
				System.out.println("onb:"+onb);
				System.out.println("hired:"+hired);
				System.out.println("tTIE:"+tTIE);
				System.out.println("Withdrawn:"+Withdrawn);
				System.out.println("hidden :"+hidden);
				System.out.println("actionFlag:"+actionFlag);
				System.out.println("TeacherIds:"+TeacherIds);
				System.out.println("actionSearchId:"+actionSearchId);
				System.out.println("jobId:"+jobId);
				System.out.println("branchId:"+branchId);
				Boolean linkToKSN = false;
				  Boolean spPass = false;
				  Boolean talType = true;
				  Boolean talStatus = false;
				  Boolean withraw = false;
				  Boolean hiddenStatus = false;
				  
				for(JobForTeacher jobForTeacherObj : lstJFTremove){

					MQEvent mqEventKSN = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
					MQEvent mqEventTalTypeKSN = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale));
					MQEvent mqEventTalStatusKSN = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
					withraw = false;
					hiddenStatus = false;
					
					if(jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("widrw"))
						  withraw =true;
					
					if(jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("cghide"))
						hiddenStatus =true;
					
					if(ksnId){
						linkToKSN = false;
						spPass = false;
						talType = true;
						talStatus = false;
						
						if(Withdrawn)
							withraw = false;
						if(hidden)
							hiddenStatus = false;

						if(mqEventKSN!=null){
							if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
								linkToKSN=true;
							}
						}
						
						if(mqEventTalTypeKSN!=null){
							if(mqEventTalTypeKSN.getWorkFlowStatusId()!=null && !(mqEventTalTypeKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale)))){
								talType = false;
								linkToKSN=false;
							}
							if(mqEventTalTypeKSN.getWorkFlowStatusId()==null){
								talType = false;
								linkToKSN=false;
							}
						}
						else if(mqEventTalTypeKSN==null){
							talType = false;
							linkToKSN=false;
						}
						
						if(jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("hird")){
							talType=true;
						}else{
							talType=false;
						}
						
						
						String sStatus=jobForTeacherObj.getTeacherId().getTeacherId()+"##"+jobForTeacherObj.getJobId().getJobId()+"##W";
						Integer spMapVal = spStatusMap.get(jobForTeacherObj.getTeacherId().getTeacherId());
						if((spMapVal!=null && (spMapVal==1))){
							spPass=true;
						}
						if((spMapVal!=null && (spMapVal>1))){
							spPass=false;
						}
						boolean SPFlag=false;
						if(spMapVal== null && (jobForTeacherObj.getJobId().getJobCategoryMaster().getPreHireSmartPractices()==null || !jobForTeacherObj.getJobId().getJobCategoryMaster().getPreHireSmartPractices())){
							spPass=false;
							SPFlag=true;
						}
										  
						if(mapHistrory.get(sStatus)!=null)
							spPass = true;
						//System.out.println("spPass :::"+spPass);
						//System.out.println("talType :::"+talType);
						
						
						if((!spPass && talType) || (spPass && !talType)){
							spPass=false;
							if(!SPFlag){
								talType=false;
							}
							linkToKSN=false;
						}
						
						if(mqEventTalStatusKSN!=null){
							if(mqEventTalStatusKSN.getWorkFlowStatusId()!=null && (mqEventTalStatusKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale)) || mqEventTalStatusKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale)))){
								talStatus=true;
							}
						}
						//System.out.println(" linkToKSN :: "+linkToKSN+" spPass :: "+spPass+" talType :: "+talType+" talStatus :: "+talStatus+" withraw :: "+withraw+" hiddenStatus :: "+hiddenStatus);
						if(!(linkToKSN || spPass || talType || talStatus) && !withraw && !hiddenStatus){
							if(!filteredList.contains(jobForTeacherObj))
								filteredList.add(jobForTeacherObj);
						}
						else
							listJobForTeacher.remove(jobForTeacherObj);
					}
					
					if(hired){
						spPass = false;
						talType = false;
						Integer spMapVal = spStatusMap.get(jobForTeacherObj.getTeacherId().getTeacherId());
						String sStatus=jobForTeacherObj.getTeacherId().getTeacherId()+"##"+jobForTeacherObj.getJobId().getJobId()+"##W";
						
						if(spMapVal!=null){
					  		if(spMapVal==1){
					  				  spPass = true;
					  		}
						}
						if(spMapVal== null && (jobForTeacherObj.getJobId().getJobCategoryMaster().getPreHireSmartPractices()==null || !jobForTeacherObj.getJobId().getJobCategoryMaster().getPreHireSmartPractices()))
			  			{
			  				spPass = true;
			  			}
						if(mapHistrory.get(sStatus)!=null)
			  			{
			  				spPass = true;
			  			}
			  				
					
					if(mqEventTalTypeKSN!=null){
				    if(mqEventTalTypeKSN.getWorkFlowStatusId()!=null && (mqEventTalTypeKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHired", locale)))){
							talType = true;
						}
					}
					
					if((talType && spPass) && jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("hird")){
						if(!filteredList.contains(jobForTeacherObj))
							filteredList.add(jobForTeacherObj);
					}
					else
						listJobForTeacher.remove(jobForTeacherObj);
					}
				
					if(hidden){
						if(jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("cghide")){
							if(!filteredList.contains(jobForTeacherObj))
								filteredList.add(jobForTeacherObj);
						}
						else
							listJobForTeacher.remove(jobForTeacherObj);
					}
					
					if(Withdrawn){
						if(jobForTeacherObj.getStatus()!=null && jobForTeacherObj.getStatus().getStatusShortName().equalsIgnoreCase("widrw")){
							if(!filteredList.contains(jobForTeacherObj))
								filteredList.add(jobForTeacherObj);
						}
						else
							listJobForTeacher.remove(jobForTeacherObj);
					}
					
					if(tTIE){
						if(mqEventTalStatusKSN!=null){
							if(mqEventTalStatusKSN.getWorkFlowStatusId()!=null && (mqEventTalStatusKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale)) || mqEventTalStatusKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale)))){
								if(!filteredList.contains(jobForTeacherObj))
									filteredList.add(jobForTeacherObj);
							}
							//else
								//listJobForTeacher.remove(jobForTeacherObj);
						}
					}
				}
				
				
			}catch(Exception e){
				e.printStackTrace();
			}
			
			if(!(hired || ksnId || Withdrawn || tTIE || hidden))
			{
				filteredList.addAll(listJobForTeacher);
			}
			//filteredList
			listJobForTeacher = new ArrayList<JobForTeacher>(filteredList) ;
			getListSubFilter(listJobForTeacher,mapNodeStatus,actionSearchId,spStatusMap,mapHistrory);
			
			totalRecord=listJobForTeacher.size();
			logger.info("After ::: listJobForTeacher.size():::::::::::::::"+listJobForTeacher.size());
			 String sCheckBoxValue = "";
			 TeacherNormScore teacherNormScore = null;
			 Map<String,String> nodeColorMap = new HashMap<String, String>();
			 List<JobForTeacher> lstJFT =new ArrayList<JobForTeacher>();
			 MQEvent mqEventKSN = null;
			 
			 if(totalRecord<end)
				 end=totalRecord;
			 
			 if(listJobForTeacher!=null && listJobForTeacher.size()>0)
			 {
				 if(end>start){
					lstJFT=listJobForTeacher.subList(start,end);
				}else{
					lstJFT=listJobForTeacher;
				}
			 }
			// listJobForTeacher = null;
			 filteredList = null;
			 logger.info("List after pagination:"+lstJFT.size()	);
			 
			 ////////////Start ::  For Defalt load Onboarding /////////////////////
			// boolean spsDefault = false;
			 try{
				 if(ksnId== true && actionSearchId==0){/*
					 boolean ksnDefault = true;
					 boolean statusDefault = true;
					 List<JobForTeacher> jstMidList = new ArrayList<JobForTeacher>();
					 if(lstJFT.size() > 0){
						 String nodeColor = "";
						 String nodeColorMsg = "";
						 
						 for(JobForTeacher teacher : lstJFT){
							//Link to KSN
								mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
								int KSNDivFlag=0;
								 nodeColor = "7F7F7F"; //grey
								 nodeColorMsg="Not initiated";
								if(mqEventKSN!=null){
									if(mqEventKSN.getCheckEmailAPI()!=null && mqEventKSN.getCheckEmailAPI()){
										nodeColor = "7F7F7F"; //grey
										nodeColorMsg="Not initiated";
								    }else
									if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
										KSNDivFlag=1;
										nodeColor="009900"; //green
										nodeColorMsg="Linked to KSN";
									}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()==null){
										nodeColor="fcd914"; //yellow
										nodeColorMsg="Initiated, waiting for msg";
									}
									else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
										nodeColor="ff0000"; //red
										nodeColorMsg="Error";
									}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
										nodeColor="243a6a"; //blue
										nodeColorMsg="Initiated confirmation msg";
									}
									
								}
								
								if(nodeColor.equals("7F7F7F") || nodeColor.equals("ff0000"))
									ksnDefault = false;
						 
								if(teacher.getStatus().getStatusShortName().equals("widrw") || teacher.getStatus().getStatusShortName().equals("cghide"))
									statusDefault = false;
								
								
								if(!ksnDefault && statusDefault)
									jstMidList.add(teacher);
						 }
						 if(jstMidList.size()>0)
							 lstJFT=jstMidList;
					 }
				 */}
			 }catch(Exception e){
				 e.printStackTrace();
			 }
			 //////////End :: For Defalt load Onboarding /////////////////////	
			 
			 
			 
			 ////////////sorting for APP
			 if(sortOrder!=null && sortOrder.equalsIgnoreCase("app"))
			 {
				 if(lstJFT.size() > 0){
					 for(JobForTeacher teacher : lstJFT){
						 String nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;
						 if(teacher.getApplicationStatus()!=null && teacher.getApplicationStatus().equals(WorkThreadServlet.statusMap.get("icomp").getStatusId())){
							 nodeColor="fcd914"; //Yellow
							 sortvaulue = 4;
						 }else{
							 nodeColor="009900"; //Green
							 sortvaulue = 5;						 }
						 	teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			}
			 
			 ////////////sorting for SPS
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("sps"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = ""; // grey
					 for(JobForTeacher teacher : lstJFT){
						 int spNodeStatus=0;
						 int sortvaulue = 1;
						 nodeColor = "7F7F7F"; // grey
						 if(spInboundMap!=null && spInboundMap.get(teacher.getTeacherId())!=null){
						 	spNodeStatus=1;
						 }
						 if(spStatusMap.size()>0){
						       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())!=null){
						        if(spStatusMap.get(teacher.getTeacherId().getTeacherId())>2){
						 	spNodeStatus=2;
						        }
						        if(spStatusMap.get(teacher.getTeacherId().getTeacherId())==1){
						 	spNodeStatus=3;
						        }
						       }
						    }
						 SecondaryStatus subfL =null;
						 if(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
						 	subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
						 }else{
						 	subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getJobCategoryId());
						 }
						 if(subfL!=null){
						     String statusStr=teacher.getTeacherId().getTeacherId()+"##"+teacher.getJobId().getJobId()+"##W";
						     String waived=mapHistrory.get(statusStr);
						    if(waived!=null && waived.equalsIgnoreCase("W")){
						 		nodeColor="243a6a"; //blue
						 		sortvaulue = 3;
						 	}else if(subfL.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)) && spNodeStatus!=0){
						 	 if(spNodeStatus==1){
						 		 nodeColor="fcd914"; //yellow
						 		 sortvaulue = 4;
						 	 }else if(spNodeStatus==2){
						 		 nodeColor="ff0000"; //red
						 		 sortvaulue = 2;
						 	 }else if(spNodeStatus==3){
						 		 nodeColor="009900"; //green
						 		 sortvaulue = 5;
						 	 } 
						 	}
						 }
						 
						 if(nodeColor.equalsIgnoreCase("7F7F7F") && teacher.getJobId().getJobCategoryMaster()!=null && teacher.getJobId().getJobCategoryMaster().getPreHireSmartPractices()!=null && !teacher.getJobId().getJobCategoryMaster().getPreHireSmartPractices())
						 {sortvaulue = 0;}
						 
						 teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			}
			 
			 /////////// Link to KSN
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("linktoksn"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
						  nodeColor = "7F7F7F"; //grey
						  int sortvaulue = 1;
						 if(mqEventKSN!=null){
						 	if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}
						 	else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()==null){
						 		nodeColor="fcd914"; //yellow
						 		sortvaulue = 4;
						 	}
						 	else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().equalsIgnoreCase("Received")){
						 		nodeColor="fcd914"; //yellow
						 		sortvaulue = 4;
						 	}
						 	else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
						 		nodeColor="ff0000"; //red
						 		sortvaulue = 2;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
						 		nodeColor="243a6a"; //blue
						 		sortvaulue = 3;
						 	}
						 }	 	
						teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 ////////////sorting for NVWF1
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("nvwf1"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
						 nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;
						 if(mqEventKSN!=null){
						 	MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
						 	if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
						 		nodeColor="ff0000"; //red
						 		sortvaulue = 2;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
						 		nodeColor="ff0000"; //red
						 		sortvaulue = 2;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
						 		nodeColor="fcd914"; //yellow
						 		sortvaulue = 4;
						 	}
						 }
						teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 ////////////sorting for WF1COM
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("wf1com"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
						 nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;

						 MQEvent lblWF1COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
						 if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						 	nodeColor="009900"; //green
						 	sortvaulue = 5;
						 }
						 else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						 	nodeColor="009900"; //green
						 	sortvaulue = 5;
						 }
						 else if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
						 	nodeColor="ff0000"; //Red
						 	sortvaulue = 2;
						 }
						
						 teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 
			 ////////////sorting for INVWF2
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("invwf2"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						 nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;
						 if(mqEventKSN!=null){
						 	MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						 	if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
						 		nodeColor="ff0000"; //red
						 		sortvaulue = 2;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
						 		nodeColor="ff0000"; //red
						 		sortvaulue = 2;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
						 		nodeColor="fcd914"; //yellow
						 		sortvaulue = 4;
						 	}
						 }
						 teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }


			 ////////////sorting for WF2COM
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("wf2com"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor="";
					 for(JobForTeacher teacher : lstJFT){
						 mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						 nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;
						 MQEvent lblWF2COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						 if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						 	nodeColor="009900"; //green
						 	sortvaulue = 5;
						 }else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null &&  lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						 	nodeColor="009900"; //green
						 	sortvaulue = 5;
						 }else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getStatus().equalsIgnoreCase("R") && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
						 	nodeColor="ff0000"; //red
						 	sortvaulue = 2;
						 }
						 else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
						 	nodeColor="ff0000"; //red
						 	sortvaulue = 2;
						 }
						 
						 teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 ////////////sorting for INV WF3
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("invwf3"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						 nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;
						 if(mqEventKSN!=null){
						 	MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						 	if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
						 		nodeColor="ff0000"; //red
						 		sortvaulue = 2;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
						 		nodeColor="ff0000"; //red
						 		sortvaulue = 2;
						 	}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
						 		nodeColor="fcd914"; //yellow
						 		sortvaulue = 4;
						 	}
						 }
						 teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 ////////////sorting for WF3COM
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("wf3com"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						 nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;
						 MQEvent lblWF3COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						 if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null &&  lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						 	nodeColor="009900"; //green
						 	sortvaulue = 5;
						 }
						 else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						 	nodeColor="009900"; //green
						 	sortvaulue = 5;
						 }
						 else if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null && lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
						 	nodeColor="ff0000"; //green
						 	sortvaulue = 5;
						 }
						 teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 ////////////sorting for SCR
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("scr"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblScreening", locale));
						 nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;
						 if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null){
						 	if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))){
						 		nodeColor="fcd914"; //yellow
						 		sortvaulue = 4;
						 	}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale))){
						 		nodeColor="243a6a"; //blue
						 		sortvaulue = 3;
						 	}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
						 		nodeColor="009900"; //green
						 		sortvaulue = 5;
						 	}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
						 		nodeColor="ff0000"; //red
						 		sortvaulue = 2;
						 	}
						 }
						teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 ////////////sorting for TALTYP
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("taltyp"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 	nodeColor = "7F7F7F"; //grey
							int sortvaulue = 1;
							if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale))!=null)
							{
								mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale));
								if(mqEventKSN!=null && teacher.getStatus()!=null && teacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
								{
									if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
										nodeColor="009900"; //green
										sortvaulue = 5;
									}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANDIDATE", locale))){
										nodeColor="fcd914"; //yellow
										sortvaulue = 4;
									}
									else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCONDITIONALTEOFFER", locale))){
										nodeColor="fcd914"; //yellow
										sortvaulue = 4;
									}
								}
							}
							teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 
			 ////////////sorting for WTHD
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("wthd"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
						 nodeColor = "7F7F7F"; //grey
						 int sortvaulue = 1;
						 if(teacher.getStatus()!=null && teacher.getStatusMaster()!=null && teacher.getStatus().getStatusShortName().equals("widrw") && teacher.getStatusMaster().getStatusShortName().equals("widrw")){
						 	nodeColor="FFA500"; //Orange
						 	sortvaulue = 6;
						}
						teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 ////////////sorting for INE
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("ine"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
							nodeColor = "7F7F7F"; //grey
							int sortvaulue = 1;
							if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
							{
								mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
								if(mqEventKSN!=null)
								{
									if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
										if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
											nodeColor="C46E1C"; //green		
											sortvaulue = 5;
										}else{
											nodeColor="7F7F7F"; //grey
											sortvaulue = 1;
										}
									}
								}
							}
							teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 ////////////sorting for INIE
			 else if(sortOrder!=null && sortOrder.equalsIgnoreCase("inie"))
			 {
				 if(lstJFT.size() > 0){
					 String nodeColor = "";
					 for(JobForTeacher teacher : lstJFT){
							nodeColor = "7F7F7F"; //grey
							int sortvaulue = 1;
							if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
							{
								mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
								if(mqEventKSN!=null)
								{
									if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale)))
									{
										if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
											nodeColor="C46E1C"; //green
											sortvaulue = 5;
										}else{
											nodeColor="7F7F7F"; //grey
											sortvaulue = 1;
										}
									}
								}
							}
							teacher.setVVI(sortvaulue);
					 }
					 
					 if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("0"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVI);
					 else if(sortOrderType!=null && sortOrderType.equalsIgnoreCase("1"))
						 Collections.sort(lstJFT,JobForTeacher.jobForTeacherComparatorVVIDesc);
				 }
			 }
			 
			 
			 if(actionFlag==0)             //to only display grid actionFlag=0
			 {
			   if(lstJFT.size() > 0){
						
				String ccsName		=	"nobground1";
				List<SecondaryStatus> secListByEachJobCat = null;
				Map<String,Integer> statusNameWithId = new HashMap<String, Integer>();
				String wfsCssClass = "";
				Integer noOfRecordCheck=0;
				
				for(JobForTeacher teacher : lstJFT){
					/*if(ksnId){
						Boolean spSnodeFlag = true;
						for(SpInboundAPICallRecord  spInboundAPICallRecord : listOfSPInboundAPICallRecord){
							if(spInboundAPICallRecord.getTeacherDetail().getTeacherId().equals(teacher.getTeacherId().getTeacherId()))
								spSnodeFlag = false;
						}
						if(spSnodeFlag)
						continue;
					}	*/
	
					teacherNormScore = normMap.get(teacher.getTeacherId().getTeacherId());
					int normScore = 0;
					String color = "";
					Integer parentCategoryId = 0;
					
					if(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
						parentCategoryId = teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId();
					
					if(secStatusListByCat!=null && secStatusListByCat.size()>0 && secStatusListByCat.get(parentCategoryId)!=null){
						secListByEachJobCat = secStatusListByCat.get(parentCategoryId);
						if(secListByEachJobCat!=null && secListByEachJobCat.size()>0)
						{
							for(SecondaryStatus sec :secListByEachJobCat){
									statusNameWithId.put(sec.getSecondaryStatusName(), sec.getSecondaryStatusId());
							}
						}
							
					}
					
					if(teacherNormScore!=null)
					{
						normScore = teacherNormScore.getTeacherNormScore();
						 color = teacherNormScore.getDecileColor();
					}
					
					for(String str : nodeList)
					{
						if(nodeMap.get(teacher.getTeacherId().getTeacherId()+"--"+teacher.getJobId().getJobId()+"-"+str)!=null)
						{
							nodeColorMap.put(str, "009900");	//green
						}else{
							nodeColorMap.put(str, "7F7F7F");  //grey
						}
						
					}
					
					String fullName=teacher.getTeacherId().getFirstName()+" "+teacher.getTeacherId().getLastName();
					noOfRecordCheck++;
					
					if(loginUserType)
						sCheckBoxValue="<input type=\"checkbox\"  name='chkMassTP'  id='chkMassTP"+teacher.getTeacherId().getTeacherId()+"' onclick=\"actionMenuTP();\"   value='"+teacher.getTeacherId().getTeacherId()+"-"+teacher.getJobForTeacherId()+"'>&nbsp&nbsp;";
					//else
						//sCheckBoxValue="<input type=\"checkbox\"  name='chkMassTP'  id='chkMassTP"+teacher.getTeacherId().getTeacherId()+"' disabled   value='"+teacher.getTeacherId().getTeacherId()+"-"+teacher.getJobForTeacherId()+"'>&nbsp&nbsp;";
					
					
					String emailAddress = teacher.getTeacherId().getEmailAddress();
					
					if(emailAddress.length()>0 && emailAddress.length()>20){
						emailAddress = emailAddress.substring(0, 20)+"..";
					}
				
					dmRecords.append("<tr height='40px'>");				
					dmRecords.append("<td style='word-wrap: break-word;'>");
					if(Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale).equalsIgnoreCase("yes")){
					dmRecords.append(sCheckBoxValue);
					if(userMaster!=null && userMaster.getHeadQuarterMaster()!=null && userMaster.getHeadQuarterMaster().getHeadQuarterId()==1)
						dmRecords.append("<a class='profile' data-placement='above' href='javascript:void(0);' onclick=\"showProfileContentForKelly(this,"+teacher.getTeacherId().getTeacherId()+","+noOfRecordCheck+",'PNR Dashboard',event)\">"+fullName+"</a><span><br><a href='#' data-original-title='"+teacher.getTeacherId().getEmailAddress()+"' rel='tooltip' id='cndNotReviewed"+noOfRecordCheck+"' onclick=\"getMessageDiv('"+teacher.getTeacherId().getTeacherId()+"','"+teacher.getTeacherId().getEmailAddress()+"','"+teacher.getJobId().getJobId()+"',0);\">"+emailAddress+"</a></span>");
					StringBuffer tagContent =	new StringBuffer();
					tagContent.append("<div>");
					int k3=0;
					tagContent.append("<table border=\"0\" class=\"tablecgtbl\">");				
					for(SecondaryStatusMaster secondaryStatusMaster : lstSecondaryStatusMaster){
						
						if(lstSecondaryStatusMaster.size()>15)
						{
							if (k3 % 3 == 0 && k3!= 0) {
								tagContent.append("</tr><tr>");
							}
							k3++;						
							//sb.append("<td width='20px' "+csscoloumns+" >");			
							if(teachersTagsExistMap.containsKey(teacher.getTeacherId().getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId())){
								tagContent.append("<td width=\"30px\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" checked=\"checked\"  name=\"tagsValue"+teacher.getTeacherId().getTeacherId()+"\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
							}else{
								tagContent.append("<td width=\"30px\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" name=\"tagsValue"+teacher.getTeacherId().getTeacherId()+"\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
							}
							//tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
							
							if(secondaryStatusMaster.getPathOfTagsIcon()!=null){
								tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\"><img src=\""+showTagIconFile(secondaryStatusMaster)+"\" width=\"20px\" height=\"20px\">&nbsp;&nbsp;"+secondaryStatusMaster.getSecStatusName()+"</td>");
								
							}else{
								tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
							}				
							
						}else{
						
							tagContent.append("<tr>");					
							if(teachersTagsExistMap.containsKey(teacher.getTeacherId().getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId())){
								tagContent.append("<td width=\"10%\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" checked=\"checked\"  name=\"tagsValue"+teacher.getTeacherId().getTeacherId()+"\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
							}else{
								tagContent.append("<td width=\"10%\" style=\"padding-left:14px;line-height:0px;\"><input type=\"checkbox\" name=\"tagsValue"+teacher.getTeacherId().getTeacherId()+"\" id=\"tagsValue\" value="+secondaryStatusMaster.getSecStatusId()+"></td>");
							}
							//tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
							
							if(secondaryStatusMaster.getPathOfTagsIcon()!=null){
								tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\"><img src=\""+showTagIconFile(secondaryStatusMaster)+"\" width=\"20px\" height=\"20px\">&nbsp;&nbsp;"+secondaryStatusMaster.getSecStatusName()+"</td>");
								
							}else{
								tagContent.append("<td style=\"padding:0px 4px 4px 4px;line-height:19px;\" class=\"tdAct\">"+secondaryStatusMaster.getSecStatusName()+"</td>");
							}
							tagContent.append("</tr>");
						}
					}
					
					tagContent.append("</table>");
					tagContent.append("</div>");
					tagContent.append("<div class=\"modal-footer\">");
					tagContent.append("<span><button  class=\"btn btn-large btn-primary\" onclick=\"saveTags("+teacher.getTeacherId().getTeacherId()+");\"  >"+Utility.getLocaleValuePropByKey("btnSave", locale)+" <i class=\"icon\"></i></button>  <button id=\"closeTag"+noOfRecordCheck+"\" onclick=\"closeActActionForKelly("+noOfRecordCheck+");\" class=\"btn\" data-dismiss=\"modal\" aria-hidden=\"true\"  >"+Utility.getLocaleValuePropByKey("btnClr", locale)+"</button></span>");						 
					tagContent.append("</div>");
					
					StringBuffer tagNames =	new StringBuffer();
					for(SecondaryStatusMaster secondaryStatusMaster : lstSecondaryStatusMaster){
						if(teachersTagsExistMap.containsKey(teacher.getTeacherId().getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId())){						
							String tagName = teachersTagsExistMap.get(teacher.getTeacherId().getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId()).getSecondaryStatusMaster().getSecStatusName();						
							if(teachersTagsExistMap.get(teacher.getTeacherId().getTeacherId()+"##"+secondaryStatusMaster.getSecStatusId()).getSecondaryStatusMaster().getPathOfTagsIcon()!=null){
								tagNames.append(" <img src='"+showTagIconFile(secondaryStatusMaster)+"' width=\"20px\" height=\"20px\">");
							}else{
								tagNames.append(" <span class=\"txtbgroundTagStatusCPool\" width=\"5%\">"+tagName+"</span>");
							}
						}
					}
					int roleId = userMaster.getRoleId().getRoleId();
					if(roleId != 1)
					{
						if(roleId != 3 )
						{
							//tmRecords.append("<a href='javascript:void(0);' onclick=\"closeProfileOpenTagsTP("+teacherDetail.getTeacherId()+","+noOfRecordCheck+");\" id='tagPreview"+noOfRecordCheck+"'>"+tagNames.toString()+"</a>");
							
							// Gour Code
							String tagContentChange = StringEscapeUtils.unescapeHtml(tagContent.toString()).replace("'","&#39;");						
							dmRecords.append(" <a id='tagPreview"+noOfRecordCheck+"' class=\"allPopOver\" href='javascript:void(0);' rel='popover' data-content='"+tagContentChange+"'");
							//tmRecords.append(tagContent.toString());
							dmRecords.append(" data-html='true' onmouseover=\"closeProfileOpenTags("+teacher.getTeacherId().getTeacherId()+","+noOfRecordCheck+");\">");
							dmRecords.append(tagNames.toString());
							dmRecords.append("</a>");
							
							dmRecords.append("<script type='text/javascript'>$('#tagPreview"+noOfRecordCheck+"').popover();</script>");
						}
						else
							dmRecords.append(tagNames.toString());
					}
					dmRecords.append("<br/>");
					}
					else{
						dmRecords.append(sCheckBoxValue+fullName);
					}
					dmRecords.append("</td>");
					
					//logger.info("Name:"+fullName+"---teacherId:"+teacher.getTeacherId().getTeacherId()+"---Job:"+teacher.getJobId().getJobId()+"---Category:"+teacher.getJobId().getJobCategoryMaster().getJobCategoryId());
					
					dmRecords.append("<td>");
					if(teacher.getTeacherId().getKSNID()!=null)
					dmRecords.append(teacher.getTeacherId().getKSNID());
					else
					dmRecords.append("N/A");
					dmRecords.append("</td>");
					
					// job title. nadeem
					if(Utility.getLocaleValuePropByKey("kellyupdatecodelive", locale).equalsIgnoreCase("yes"))
					{
					dmRecords.append("<td style='word-wrap: break-word;'>");
					dmRecords.append("<div class='featureInfo ellipsis'>");
					if(teacher.getJobId().getJobTitle().length()>30){			
					dmRecords.append("<a href='candidategridkelly.do?jobId="+teacher.getJobId().getJobId()+"&JobOrderType="+teacher.getJobId().getCreatedForEntity()+"' target='_blank' id='jobTitle' title='"+teacher.getJobId().getJobTitle()+"'><div id='h6'>"+teacher.getJobId().getJobTitle()+"</div></a>");
					dmRecords.append("</div>");
					}else{
						dmRecords.append("<a href='candidategridkelly.do?jobId="+teacher.getJobId().getJobId()+"&JobOrderType="+teacher.getJobId().getCreatedForEntity()+"' target='_blank' id='jobTitle' title='"+teacher.getJobId().getJobTitle()+"'>"+teacher.getJobId().getJobTitle()+"</a>");
					}
					dmRecords.append("</td>");
					}else{
						dmRecords.append("<td style='word-wrap: break-word;'>");
						dmRecords.append("<div class='featureInfo ellipsis'>");
						if(teacher.getJobId().getJobTitle().length()>30){			
						dmRecords.append("<a href='candidategrid.do?jobId="+teacher.getJobId().getJobId()+"&JobOrderType="+teacher.getJobId().getCreatedForEntity()+"' target='_blank' id='jobTitle' title='"+teacher.getJobId().getJobTitle()+"'><div id='h6'>"+teacher.getJobId().getJobTitle()+"</div></a>");
						dmRecords.append("</div>");
						}else{
							dmRecords.append("<a href='candidategrid.do?jobId="+teacher.getJobId().getJobId()+"&JobOrderType="+teacher.getJobId().getCreatedForEntity()+"' target='_blank' id='jobTitle' title='"+teacher.getJobId().getJobTitle()+"'>"+teacher.getJobId().getJobTitle()+"</a>");
						}
						dmRecords.append("</td>");
					}
					// end..........
					
					
					//int jobList=jobListByTeacherAndCategory(teacher.getTeacherId().getTeacherId(),teacher.getJobId().getJobCategoryMaster().getJobCategoryId(),jftMap);
					
					//job ID
					dmRecords.append("<td>");
					if(teacher.getJobId()!=null){                                                                                                   
						dmRecords.append("<a href='hqbrjoborder.do?jobId="+teacher.getJobId().getJobId()+"' target='_blank' id='jobTitle' title=''>"+teacher.getJobId().getJobId()+"</a>");
					}else{
						dmRecords.append(Utility.getLocaleValuePropByKey("lblNA", locale));
					}
					dmRecords.append("</td>");
					dmRecords.append("<input type='hidden' id='tId' value='"+teacher.getTeacherId()+"'>");
					dmRecords.append("<input type='hidden' id='jcategId' value='"+teacher.getJobId().getJobCategoryMaster().getJobCategoryId()+"'>");
					dmRecords.append("<input type='hidden' id='jobId' value='"+teacher.getJobId().getJobId()+"'>");
					dmRecords.append("<input type='hidden' id='fullName' value='"+fullName+"'>");
					
					//EPI
					String ccsName1="";
					if(normScore==0)
					{
						dmRecords.append("<td>"+Utility.getLocaleValuePropByKey("lblNA", locale)+"</td>");
					}else
					{			
						String normScoreLen=normScore+"";
						if(normScoreLen.length()==1){
							ccsName1="nobground1";
						}else if(normScoreLen.length()==2){
							ccsName1="nobground2";
						}else{
							ccsName1="nobground3";
						}
						dmRecords.append("<td><span class=\""+ccsName1+"\" style='font-size: 11px;font-weight: bold;background-color: #"+color+";'>"+normScore+"</span></td>");
					}
					Integer statusSecId = 0;
							statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblAPP", locale));
					//APP
							
					String nodeColor = "7F7F7F"; //grey
	 				String	nodeColorMsg="Not started";
	 				if(teacher.getApplicationStatus()!=null && teacher.getApplicationStatus().equals(WorkThreadServlet.statusMap.get("icomp").getStatusId())){
	 					nodeColor="fcd914"; //Yellow
					    nodeColorMsg="In Progress";
	 				}else{
	 					nodeColor="009900"; //Green
					    nodeColorMsg="Complete";
	 				}
					//nodeColorMsg="Application not started";
					dmRecords.append("<td>");
					//if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					//else
						//dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
	
					//SPS
					int spNodeStatus=0;
					nodeColorMsg="Not started";
					nodeColor = "7F7F7F"; // grey
					String nodeColorNameW="Grey";
					if(spInboundMap!=null && spInboundMap.get(teacher.getTeacherId())!=null){
						spNodeStatus=1;
					}
					if(spStatusMap.size()>0){
					      if(spStatusMap.get(teacher.getTeacherId().getTeacherId())!=null){
					       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())>2){
					        spNodeStatus=2;
					       }
					       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())==1){
					        spNodeStatus=3;
					       }
					      }
					   }
					SecondaryStatus subfL =null;
					if(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
						subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
					}else{
						subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getJobCategoryId());
					}
					/*if(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
						System.out.println("Parent::::::::::::"+teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
					}else{
						System.out.println("Child::::::::::::"+teacher.getJobId().getJobCategoryMaster().getJobCategoryId());
					}*/
					//logger.info(teacher.getTeacherId().getTeacherId()+":spNodeStatus:"+spNodeStatus);
					//logger.info(teacher.getTeacherId().getTeacherId()+":subfL:"+subfL.getSecondaryStatusId());
					if(subfL!=null){
					    String statusStr=teacher.getTeacherId().getTeacherId()+"##"+teacher.getJobId().getJobId()+"##W";
					    String waived=mapHistrory.get(statusStr);
					    //logger.info(teacher.getTeacherId().getTeacherId()+":waived:"+waived);
					    	if(waived!=null && waived.equalsIgnoreCase("W")){
						    	nodeColor="243a6a"; //blue
						    	nodeColorMsg="SP Assessment waived";
						    	nodeColorNameW="Blue";
					        }else if(subfL.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)) && spNodeStatus!=0){
					         if(spNodeStatus==1){
					        	 nodeColor="fcd914"; //yellow
					        	 nodeColorMsg="SP Assessment in progress";
					        	 nodeColorNameW="Yellow";
					         }else if(spNodeStatus==2){
					        	 nodeColor="ff0000"; //red
					        	 nodeColorMsg="SP Assessment failed twice";
					        	 nodeColorNameW="Red";
					         }else if(spNodeStatus==3){
					        	 nodeColor="009900"; //green
					        	 nodeColorMsg="Passed SP assessment";
					        	 nodeColorNameW="Green";
					         } 
					        }/*else if(waived1!=null && waived1.equalsIgnoreCase("S")){
					        	nodeColor="009900"; //green
					        	nodeColorMsg="Passed SP assessment";
					        }*/
					    }
					
						if(!nodeColor.equals("009900"))
							osFlag = false;
				//	statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblSPStatus", locale));
					
					statusSecId = WorkThreadServlet.statusMap.get("scomp").getStatusId();
				
					dmRecords.append("<td>");
					if(nodeColor.equalsIgnoreCase("7F7F7F") && teacher.getJobId().getJobCategoryMaster()!=null && teacher.getJobId().getJobCategoryMaster().getPreHireSmartPractices()!=null && !teacher.getJobId().getJobCategoryMaster().getPreHireSmartPractices()){
						dmRecords.append("N/A");
					}else{
						//if(loginUserType)
							dmRecords.append("<a href='javascript:void(0);' onclick=\"doSPSWaived("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+fullName+"',"+statusSecId+",'"+Utility.getLocaleValuePropByKey("lblSPS", locale)+"','"+nodeColorNameW+"');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
						//else
						//dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					}
					
					dmRecords.append("</td>");
					
					//Link to KSN
					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
					int KSNDivFlag=0;
					 nodeColor = "7F7F7F"; //grey
					 nodeColorMsg="Not initiated";
					if(mqEventKSN!=null){
						/*if(mqEventKSN.getCheckEmailAPI()!=null && mqEventKSN.getCheckEmailAPI()){
							nodeColor = "7F7F7F"; //grey
							nodeColorMsg="Not initiated";
					    }else*/
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
							KSNDivFlag=1;
							nodeColor="009900"; //green
							nodeColorMsg="Linked to KSN";
						}
						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()==null){
							nodeColor="fcd914"; //yellow
							nodeColorMsg="Initiated, waiting for msg";
						}
						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().equalsIgnoreCase("Received")){
							nodeColor="fcd914"; //yellow
							nodeColorMsg="Initiated, waiting for msg";
						}
						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
							nodeColor="ff0000"; //red
							nodeColorMsg="Error";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
							nodeColor="243a6a"; //blue
							nodeColorMsg="Initiated confirmation msg";
						}
						
					}
					if(!nodeColor.equals("009900")){
						osFlag = false;
						statusDependency = false;
					}else{
						statusDependency = true;
					}
					
					statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
					
					dmRecords.append("<td>");
					//if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)+"',"+statusSecId+","+KSNDivFlag+",'');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+teacher.getTeacherId().getTeacherId()+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					//else
						//dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+teacher.getTeacherId().getTeacherId()+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					
					dmRecords.append("</td>");
					dmRecords.append("<script>applyCSS('"+teacher.getTeacherId().getTeacherId()+"');</script>");
					//INV WF1
					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not initiated";
					if(mqEventKSN!=null){
						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf1";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf1";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf1";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							nodeColor="ff0000"; //red
							nodeColorMsg="Error";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
							nodeColor="ff0000"; //red
							nodeColorMsg="Error";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
							nodeColor="fcd914"; //yellow
							nodeColorMsg="Initiated, waiting for msg";
						}
					}
					if(!nodeColor.equals("009900")){
						osFlag = false;
						statusDependency = false;
					}else{
						statusDependency = true;
					}
					
					statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblINVWF1", locale));
					wfsCssClass = teacher.getTeacherId().getTeacherId()+Utility.getLocaleValuePropByKey("lblINVWF1", locale).split(" ")[1];
					dmRecords.append("<td>");
				//	if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("lblINVWF1", locale)+"',"+statusSecId+","+KSNDivFlag+",'');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+wfsCssClass+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
				//	else
				//		dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+wfsCssClass+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					dmRecords.append("<script>applyCSS('"+wfsCssClass+"');</script>");
					//WF1 COM
					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not initiated";
					
					MQEvent lblWF1COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
					if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						nodeColor="009900"; //green
						nodeColorMsg="WF1 Complete";
					}
					else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						nodeColor="009900"; //green
						nodeColorMsg="WF1 Complete";
					}
					else if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
						nodeColor="ff0000"; //Red
						nodeColorMsg="Error";
					}
					
					if(!nodeColor.equals("009900")){
						osFlag = false;
						statusDependency = false;
					}else{
						statusDependency = true;
					}
					
					statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblWF1COM", locale));
					dmRecords.append("<td>");
		//			if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("lblWF1COM", locale)+"',"+statusSecId+","+KSNDivFlag+",'');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
		//			else
		//				dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					
					//INV WF2
					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not initiated";
					if(mqEventKSN!=null){
						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf2";
						}/*else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
							nodeColor="7F7F7F"; //gray
							nodeColorMsg="Not initiated";
						}else */if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf2";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf2";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							nodeColor="ff0000"; //red
							nodeColorMsg="Error";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
							nodeColor="ff0000"; //red
							nodeColorMsg="Error";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
							nodeColor="fcd914"; //yellow
							nodeColorMsg="Initiated, waiting for msg";
						}
					}
					if(!nodeColor.equals("009900")){
						osFlag = false;
						statusDependency = false;
					}else{
						statusDependency = true;
					}
					
					statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblINVWF2", locale));
					wfsCssClass = teacher.getTeacherId().getTeacherId()+Utility.getLocaleValuePropByKey("lblINVWF2", locale).split(" ")[1];
					dmRecords.append("<td>");
			//		if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("lblINVWF2", locale)+"',"+statusSecId+","+KSNDivFlag+",'');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+wfsCssClass+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
			//		else
			//			dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+wfsCssClass+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					dmRecords.append("<script>applyCSS('"+wfsCssClass+"');</script>");
					//WF2 COM
					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not initiated";
					MQEvent lblWF2COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
					if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						nodeColor="009900"; //green
						nodeColorMsg="WF2 Complete";
					}else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null &&  lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						nodeColor="009900"; //green
						nodeColorMsg="WF2 Complete";
					}else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getStatus().equalsIgnoreCase("R") && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
						nodeColor="ff0000"; //red
						nodeColorMsg="Awaiting Review";
					}
					else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
						nodeColor="ff0000"; //red
						nodeColorMsg="Error";
					}
					if(!nodeColor.equals("009900")){
						osFlag = false;
						statusDependency = false;
					}else{
						statusDependency = true;
					}
					
					statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblWF2COM", locale));
					dmRecords.append("<td>");
			//		if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("lblWF2COM", locale)+"',"+statusSecId+","+KSNDivFlag+",'');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
			//		else
			//			dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					
					//INV WF3
					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not initiated";
					if(mqEventKSN!=null){
						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf3";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf3";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="Invited to Wf3";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
							nodeColor="ff0000"; //red
							nodeColorMsg="Error";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
							nodeColor="ff0000"; //red
							nodeColorMsg="Error";
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
							nodeColor="fcd914"; //yellow
							nodeColorMsg="Manually initiated";
						}
					}
					if(!nodeColor.equals("009900")){
						osFlag = false;
						statusDependency = false;
					}else{
						statusDependency = true;
					}
					
					statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblINVWF3", locale));
					wfsCssClass = teacher.getTeacherId().getTeacherId()+Utility.getLocaleValuePropByKey("lblINVWF3", locale).split(" ")[1];
					dmRecords.append("<td>");
			//		if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("lblINVWF3", locale)+"',"+statusSecId+","+KSNDivFlag+",'');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+wfsCssClass+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
			//		else
			//			dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+wfsCssClass+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					dmRecords.append("<script>applyCSS('"+wfsCssClass+"');</script>");
					
					//WF3 COM
					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not initiated";
				
					MQEvent lblWF3COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
					if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null &&  lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						nodeColor="009900"; //green
						nodeColorMsg="WF3 Complete";
					}
					else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
						nodeColor="009900"; //green
						nodeColorMsg="WF3 Complete";
					}
					else if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null && lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
						nodeColor="ff0000"; //green
						nodeColorMsg="Error";
					}
					
					if(!nodeColor.equals("009900"))
						osFlag = false;
					
					statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblWF3COM", locale));
					dmRecords.append("<td>");
			//		if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("lblWF3COM", locale)+"',"+statusSecId+","+KSNDivFlag+",'');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
			//		else
			//			dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					
					//SCR
					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblScreening", locale));
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="No screening record";
					if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null){
						if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))){
							nodeColor="fcd914"; //yellow
							nodeColorMsg="Pending results";
						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale))){
							nodeColor="243a6a"; //blue
							nodeColorMsg="Review required";
						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="Favorable results";
						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
							nodeColor="ff0000"; //red
							nodeColorMsg="Unfavorable results";
						}
					}
					if(!nodeColor.equals("009900"))
						osFlag = false;
					
					statusSecId = statusNameWithId.get(Utility.getLocaleValuePropByKey("lblScreening", locale));
					dmRecords.append("<td>");
				//	if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("lblScreening", locale)+"',"+statusSecId+","+KSNDivFlag+",'');\" id='' title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
				//	else
				//		dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					
					//TAL TYP
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not linked to KSN";
					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale))!=null)
					{
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale));
						if(mqEventKSN!=null && teacher.getStatus()!=null && teacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
						{
						if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="Hired";
						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANDIDATE", locale))){
							nodeColor="fcd914"; //yellow
							nodeColorMsg="Candidate";
						}
						else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCONDITIONALTEOFFER", locale))){
							nodeColor="fcd914"; //yellow
							nodeColorMsg="Conditional Offer";
						}
						}
					}
					if(!nodeColor.equals("009900"))
						osFlag = false;
					
					statusSecId = WorkThreadServlet.statusMap.get("hird").getStatusId();
					dmRecords.append("<td>");
			//		if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' id='' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+Utility.getLocaleValuePropByKey("msgHired2", locale)+"','',"+KSNDivFlag+",'"+statusSecId+"');\" title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
			//		else
			//			dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					
					//WTHD
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not withdrawn";
					if(teacher.getStatus()!=null && teacher.getStatusMaster()!=null && teacher.getStatus().getStatusShortName().equals("widrw") && teacher.getStatusMaster().getStatusShortName().equals("widrw")){
						nodeColor="FFA500"; //Orange
						nodeColorMsg="Withdrawn";
					}
					if(!nodeColor.equals("009900"))
						osFlag = false;
					
					statusSecId = WorkThreadServlet.statusMap.get("widrw").getStatusId();
					dmRecords.append("<td>");
			//		if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' id='' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+WorkThreadServlet.statusMap.get("widrw").getStatus()+"','',"+KSNDivFlag+",'"+statusSecId+"');\" title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
			//		else
			//			dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					
					//IN-E
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not Inactive Eligible";
					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
					{
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
						if(mqEventKSN!=null)
						{
							if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
								if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
									nodeColor="C46E1C"; //green		
								    nodeColorMsg="Inactive Eligible";}
								else{
									nodeColor="7F7F7F"; //grey
									nodeColorMsg="Not Inactive Eligible";
								}
							}
						}
					}
					if(!nodeColor.equals("009900"))
						osFlag = false;
					
					statusSecId = WorkThreadServlet.statusMap.get("ielig").getStatusId();
					dmRecords.append("<td>");
			//		if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' id='' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+WorkThreadServlet.statusMap.get("ielig").getStatus()+"','',"+KSNDivFlag+",'"+statusSecId+"');\" title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
			//		else
			//			dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					
					//IN-IE
					nodeColor = "7F7F7F"; //grey
					nodeColorMsg="Not Inactive Not Eligible";
					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
					{
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
						if(mqEventKSN!=null)
						{
							if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale))){
								if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
									nodeColor="C46E1C"; //green
									nodeColorMsg="Inactive Not Eligible";}
								else{
									nodeColor="7F7F7F"; //grey
									nodeColorMsg="Inactive Not Eligible";
								}
							}
						}
					}
					if(!nodeColor.equals("009900"))
						osFlag = false;
					
					statusSecId = WorkThreadServlet.statusMap.get("iielig").getStatusId();
					dmRecords.append("<td>");
				//	if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' id='' onclick=\"showNotesDiv("+teacher.getTeacherId().getTeacherId()+","+teacher.getJobForTeacherId()+",'"+WorkThreadServlet.statusMap.get("iielig").getStatus()+"','',"+KSNDivFlag+",'"+statusSecId+"');\" title='"+nodeColorMsg+"'>" + "<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
				//	else
				//		dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" +"<span class=\""+ccsName+"\" style='font-size: 7px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					dmRecords.append("</td>");
					
					//OS
					
					if(osFlag){
						nodeColor = "009900"; //green
						nodeColorMsg = "Complete";
					}else{
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg = "InComplete";
					}
					/*dmRecords.append("<td>");
					if(loginUserType)
						dmRecords.append("<a href='javascript:void(0);' id='' title='"+nodeColorMsg+"'>" + "<span class='nobground1' style='font-size: 10px;font-weight: bold;background-color: #"+nodeColor+";'></span></a>");
					else
						dmRecords.append("<span class='nobground1' style='font-size: 10px;font-weight: bold;background-color: #"+nodeColor+";'></span>");
					dmRecords.append("</td>");*/
					
					dmRecords.append("<td>");
					dmRecords.append("</td>");
					
					dmRecords.append("</tr>");
				}
				
			} else {
			dmRecords.append("<tr>");
			dmRecords.append("<td colspan=3>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td>");
			dmRecords.append("</tr>");
		}
			dmRecords.append("</table>");
			dmRecords.append(PaginationAndSorting.getPaginationForKellyOnboarding(request,totalRecord,noOfRow, pageNo));
			lstJFT = null;
		}else if(actionFlag==1 || actionFlag==2)    //for excel export to selected candidates(actionFlag=1)/all candidates(actionFlag=2) 
		{
			 Map<Long,Long> checkedJFTMap = new HashMap<Long, Long>();
			 String time = String.valueOf(System.currentTimeMillis()).substring(6);
	         String basePath = request.getSession().getServletContext().getRealPath ("/")+"/"+Utility.getLocaleValuePropByKey("headonboarding", locale)+"";
	         fileName =Utility.getLocaleValuePropByKey("headonboarding", locale)+time+".xls";
	         
	         File file = new File(basePath);
	         if(!file.exists())
	               file.mkdirs();
	
	         Utility.deleteAllFileFromDir(basePath);
	
	         file = new File(basePath+"/"+fileName);
	
	         WorkbookSettings wbSettings = new WorkbookSettings();
	
	         wbSettings.setLocale(new Locale("en", "EN"));
	
	         WritableCellFormat timesBoldUnderline;
	         WritableCellFormat header;
	         WritableCellFormat headerBold;
	         WritableCellFormat times;
	
	         WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
	         workbook.createSheet(Utility.getLocaleValuePropByKey("headonboarding", locale), 0);
	         WritableSheet excelSheet = workbook.getSheet(0);
	
	         WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
	         // Define the cell format
	         times = new WritableCellFormat(times10pt);
	         // Lets automatically wrap the cells
	         times.setWrap(true);
	
	         // Create create a bold font with unterlines
	         WritableFont times20ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 20, WritableFont.BOLD, false);
	         WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD, false);
	
	         timesBoldUnderline = new WritableCellFormat(times20ptBoldUnderline);
	         timesBoldUnderline.setAlignment(Alignment.CENTRE);
	         // Lets automatically wrap the cells
	         timesBoldUnderline.setWrap(true);
	
	         header = new WritableCellFormat(times10ptBoldUnderline);
	         headerBold = new WritableCellFormat(times10ptBoldUnderline);
	         CellView cv = new CellView();
	         cv.setFormat(times);
	         cv.setFormat(timesBoldUnderline);
	         cv.setAutosize(true);
	
	         header.setBackground(Colour.GRAY_25);
	         
	         int numberOfRow=20;
	         
	         excelSheet.mergeCells(0, 0, numberOfRow-1, 1);
	         Label label;
	         label = new Label(0, 0, Utility.getLocaleValuePropByKey("headonboarding", locale), timesBoldUnderline);
	         excelSheet.addCell(label);
	         excelSheet.mergeCells(0, 3, numberOfRow-1, 3);
	         label = new Label(0, 3, "");
	         excelSheet.addCell(label);
	         excelSheet.getSettings().setDefaultColumnWidth(18);
	         
	         int k=4;
	         int col=0;
	               label = new Label(0, k, Utility.getLocaleValuePropByKey("lblNm", locale),header); 
	               excelSheet.addCell(label);
	         
	       	       label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblKSNID", locale),header); 
	               excelSheet.addCell(label); 
	                
	         
	               label = new Label(++col, k,Utility.getLocaleValuePropByKey("lblJoTil", locale),header);
	               excelSheet.addCell(label);
	               
	       	       label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblJOBID", locale),header);
	               excelSheet.addCell(label); 
	                
	         
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblEPI", locale),header); 
	               excelSheet.addCell(label);
	
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblAPP", locale),header); 
	               excelSheet.addCell(label);
	         
	         
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblSPS", locale),header); 
	               excelSheet.addCell(label);
	         
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),header); 
	               excelSheet.addCell(label);
	
	        
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblINVWF1", locale),header); 
	               excelSheet.addCell(label);
	         
	         
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblWF1COM", locale),header); 
	               excelSheet.addCell(label);
	         
	         
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblINVWF2", locale),header); 
	               excelSheet.addCell(label);
	               
	        
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblWF2COM", locale),header); 
	               excelSheet.addCell(label);
	               
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblINVWF3", locale),header); 
	               excelSheet.addCell(label);
	               
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblWF3COM", locale),header); 
	               excelSheet.addCell(label);
	               
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblSCR", locale),header); 
	               excelSheet.addCell(label);
	               
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblTALTYP", locale),header); 
	               excelSheet.addCell(label);
	               
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblWTHD", locale),header); 
	               excelSheet.addCell(label);
	               
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblINE", locale),header); 
	               excelSheet.addCell(label);
	               
	               label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblINIE", locale),header); 
	               excelSheet.addCell(label);
	               
	          //     label = new Label(++col, k, Utility.getLocaleValuePropByKey("lblOS", locale),header); 
	          //    excelSheet.addCell(label);       
	         
	         k=k+1;
	         if(listJobForTeacher.size()==0)
	         {     
	               excelSheet.mergeCells(0, k, 8, k);
	               label = new Label(0, k, Utility.getLocaleValuePropByKey("msgNorecordfound", locale)); 
	               excelSheet.addCell(label);
	         }
	         if(listJobForTeacher.size()>0){
	        	 
	        	 if(actionFlag==1 && TeacherIds!=null){
	        	        if(TeacherIds.length>0)
	        	 		{
	        	 			for(String id:TeacherIds)
	        	 			{
	        	 				String[] teacherArray=id.split("-");
	        	 				//jftIds.add(Long.parseLong(teacherArray[1]));
	        	 				checkedJFTMap.put(Long.parseLong(teacherArray[1]), Long.parseLong(teacherArray[1]));
	        	 			}
	        	 		}
	        	 }
	        	        
	        	        List<JobForTeacher> iteratingJFTList = null;
	        	        if(actionFlag==1)
	        	        	iteratingJFTList=lstJFT;
	        	        else if(actionFlag==2)
	        	        	iteratingJFTList=listJobForTeacher;
	        	        
	        	        for(JobForTeacher teacher : iteratingJFTList)
	        	        {
	        	          col=0;
	        	          boolean flag1=false;
	        	          if(checkedJFTMap.containsKey(teacher.getJobForTeacherId()) && actionFlag==1)
	        	        	  flag1=true; //for selected talent
	        	          if(flag1 || actionFlag==2){
	        	        	  
	
	        					teacherNormScore = normMap.get(teacher.getTeacherId().getTeacherId());
	        					int normScore = 0;
	        					String color = "";
	        					if(teacherNormScore!=null)
	        					{
	        						normScore = teacherNormScore.getTeacherNormScore();
	        						 color = teacherNormScore.getDecileColor();
	        					}
	        					
	        					for(String str : nodeList)
	        					{
	        						if(nodeMap.get(teacher.getTeacherId().getTeacherId()+"--"+teacher.getJobId().getJobId()+"-"+str)!=null)
	        						{
	        							nodeColorMap.put(str, "Complete");	//green
	        						}else{
	        							nodeColorMap.put(str, "Pending");  //grey
	        						}
	        						
	        					}
	        					
	        					
	        					String fullName=teacher.getTeacherId().getFirstName()+" "+teacher.getTeacherId().getLastName();
	        					
	        					
	        					label = new Label(0, k, fullName); 
	     	                    excelSheet.addCell(label);
	        					
	        					if(teacher.getTeacherId().getKSNID()!=null)
	        					{
	        					label = new Label(++col, k, teacher.getTeacherId().getKSNID()+""); 
		                        excelSheet.addCell(label);
	        					}
	        					else
	        					{
	        					label = new Label(++col, k, Utility.getLocaleValuePropByKey("optN/A", locale)); 
	    	                    excelSheet.addCell(label);
	        					}
	        					
	        					if(teacher.getJobId()!=null && teacher.getJobId().getJobTitle()!=null)
	        					{
	        						label = new Label(++col, k, teacher.getJobId().getJobTitle()); 
	        	                    excelSheet.addCell(label);
	        					}else{
	        						label = new Label(++col, k, Utility.getLocaleValuePropByKey("optN/A", locale)); 
	        	                    excelSheet.addCell(label);
	        					}
	        					
	        					if(teacher.getJobId()!=null){                                                                                                   
	        						label = new Label(++col, k, teacher.getJobId().getJobId()+""); 
	        	                    excelSheet.addCell(label);
	        					}else{
	        						label = new Label(++col, k, Utility.getLocaleValuePropByKey("optN/A", locale)); 
	        	                    excelSheet.addCell(label);
	        					}
	        					if(normScore==0)
	        					{
	        						label = new Label(++col, k, Utility.getLocaleValuePropByKey("optN/A", locale)); 
	    	                        excelSheet.addCell(label);
	        					}
	        					else
	        					{
	        						label = new Label(++col, k,normScore+""); 
		                            excelSheet.addCell(label);
		                        }
	        					
	        					//APP
	        					String nodeColor = "7F7F7F"; //grey
	             				String	nodeColorMsg="Not started";
	        					
	             				if(teacher.getApplicationStatus()!=null && teacher.getApplicationStatus().equals(WorkThreadServlet.statusMap.get("icomp").getStatusId())){
	        	 					nodeColor="fcd914"; //Yellow
	        					    nodeColorMsg="In Progress";
	        	 				}else{
	        	 					nodeColor="009900"; //Green
	        					    nodeColorMsg="Complete";
	        	 				}
	             				
	             				
	        					label = new Label(++col, k,nodeColorMsg); 
	                            excelSheet.addCell(label);
	        					
	        				  //SPS
	        					int spNodeStatus=0;
	        					nodeColorMsg="Not started";
	        					nodeColor = "7F7F7F"; // grey
	        					String nodeColorNameW="Grey";
	        					if(spInboundMap!=null && spInboundMap.get(teacher.getTeacherId())!=null){
	        						spNodeStatus=1;
	        					}
	        					if(spStatusMap.size()>0){
	        					      if(spStatusMap.get(teacher.getTeacherId().getTeacherId())!=null){
	        					       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())>2){
	        					        spNodeStatus=2;
	        					       }
	        					       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())==1){
	        					        spNodeStatus=3;
	        					       }
	        					      }
	        					   }
	        					SecondaryStatus subfL =null;
	        					if(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
	        						subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
	        					}else{
	        						subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getJobCategoryId());
	        					}
	        					if(subfL!=null){
	        					    String statusStr=teacher.getTeacherId().getTeacherId()+"##"+teacher.getJobId().getJobId()+"##W";
	        					    String waived=mapHistrory.get(statusStr);
	        					    	if(waived!=null && waived.equalsIgnoreCase("W")){
	        						    	nodeColor="243a6a"; //blue
	        						    	nodeColorMsg="SP Assessment waived";
	        						    	nodeColorNameW="Blue";
	        					        }else if(subfL.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)) && spNodeStatus!=0){
	        					         if(spNodeStatus==1){
	        					        	 nodeColor="fcd914"; //yellow
	        					        	 nodeColorMsg="SP Assessment in progress";
	        					        	 nodeColorNameW="Yellow";
	        					         }else if(spNodeStatus==2){
	        					        	 nodeColor="ff0000"; //red
	        					        	 nodeColorMsg="SP Assessment failed twice";
	        					        	 nodeColorNameW="Red";
	        					         }else if(spNodeStatus==3){
	        					        	 nodeColor="009900"; //green
	        					        	 nodeColorMsg="Passed SP assessment";
	        					        	 nodeColorNameW="Green";
	        					         } 
	        					        }/*else if(waived1!=null && waived1.equalsIgnoreCase("S")){
	        					        	nodeColor="009900"; //green
	        					        	nodeColorMsg="Passed SP assessment";
	        					        }*/
	        					    }
	        					
	        					if(nodeColor.equalsIgnoreCase("7F7F7F") && teacher.getJobId().getJobCategoryMaster()!=null && teacher.getJobId().getJobCategoryMaster().getPreHireSmartPractices()!=null && !teacher.getJobId().getJobCategoryMaster().getPreHireSmartPractices()){
	        						label = new Label(++col, k,Utility.getLocaleValuePropByKey("optN/A", locale)); 
		                            excelSheet.addCell(label);
	        						//dmRecords.append("N/A");
	        					}else{
	        						label = new Label(++col, k,nodeColorMsg); 
		                            excelSheet.addCell(label);
	        					}
	             				
	         					//Link to KSN
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
	        					int KSNDivFlag=0;
	        					 nodeColor = "7F7F7F"; //grey
	        					 nodeColorMsg="Not initiated";
	        					if(mqEventKSN!=null){
	        						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	        							KSNDivFlag=1;
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Linked to KSN";
	        						}
	        						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()==null){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Initiated, waiting for msg";
	        						}
	        						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().equalsIgnoreCase("Received")){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Initiated, waiting for msg";
	        						}
	        						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
	        							nodeColor="243a6a"; //blue
	        							nodeColorMsg="Initiated confirmation msg";
	        						}
	        						
	        					}
	
	             				label = new Label(++col, k,nodeColorMsg); 
	                            excelSheet.addCell(label);
	             				
	         					//INV WF1
	                            mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        					if(mqEventKSN!=null){
	        						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
	        						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf1";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf1";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf1";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Initiated, waiting for msg";
	        						}
	        					}
	             			
	             				label = new Label(++col, k,nodeColorMsg); 
	                            excelSheet.addCell(label);
	             				
	                          //WF1 COM
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        					
	        					MQEvent lblWF1COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
	        					if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF1 Complete";
	        					}
	        					else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF1 Complete";
	        					}
	        					else if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
	        						nodeColor="ff0000"; //Red
	        						nodeColorMsg="Error";
	        					}
	
	             				label = new Label(++col, k,nodeColorMsg); 
	                             excelSheet.addCell(label);
	             			
	         					
	                           //INV WF2
	         					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
	         					nodeColor = "7F7F7F"; //grey
	         					nodeColorMsg="Not initiated";
	         					if(mqEventKSN!=null){
	         						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
	         						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	         							nodeColor="009900"; //green
	         							nodeColorMsg="Invited to Wf2";
	         						}if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	         							nodeColor="009900"; //green
	         							nodeColorMsg="Invited to Wf2";
	         						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	         							nodeColor="009900"; //green
	         							nodeColorMsg="Invited to Wf2";
	         						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
	         							nodeColor="ff0000"; //red
	         							nodeColorMsg="Error";
	         						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
	         							nodeColor="ff0000"; //red
	         							nodeColorMsg="Error";
	         						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
	         							nodeColor="fcd914"; //yellow
	         							nodeColorMsg="Initiated, waiting for msg";
	         						}
	         					}
	
	 	        				 label = new Label(++col, k,nodeColorMsg); 
	 	                         excelSheet.addCell(label);
	             				
	                             
	                             
	 	                       //WF2 COM
	 	    					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
	 	    					nodeColor = "7F7F7F"; //grey
	 	    					nodeColorMsg="Not initiated";
	 	    					MQEvent lblWF2COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
	 	    					if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	 	    						nodeColor="009900"; //green
	 	    						nodeColorMsg="WF2 Complete";
	 	    					}else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null &&  lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	 	    						nodeColor="009900"; //green
	 	    						nodeColorMsg="WF2 Complete";
	 	    					}else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getStatus().equalsIgnoreCase("R") && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
	 	    						nodeColor="ff0000"; //red
	 	    						nodeColorMsg="Awaiting Review";
	 	    					}
	 	    					else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
	 	    						nodeColor="ff0000"; //red
	 	    						nodeColorMsg="Error";
	 	    					}
	
	             				label = new Label(++col, k,nodeColorMsg); 
	                            excelSheet.addCell(label);
	                             
	                             
	                             
	                             
	                          //INV WF3
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        					if(mqEventKSN!=null){
	        						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
	        						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf3";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf3";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf3";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Manually initiated";
	        						}
	        					}
	
	             				label = new Label(++col, k,nodeColorMsg); 
	                            excelSheet.addCell(label);
	                             
	                             
	                          //WF3 COM
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        				
	        					MQEvent lblWF3COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
	        					if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null &&  lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF3 Complete";
	        					}
	        					else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF3 Complete";
	        					}
	        					else if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null && lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
	        						nodeColor="ff0000"; //green
	        						nodeColorMsg="Error";
	        					}
	
	             				label = new Label(++col, k,nodeColorMsg); 
	             				excelSheet.addCell(label);
	             				
	         					
	             				//SCR
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblScreening", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="No screening record";
	        					if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null){
	        						if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Pending results";
	        						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale))){
	        							nodeColor="243a6a"; //blue
	        							nodeColorMsg="Review required";
	        						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Favorable results";
	        						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Unfavorable results";
	        						}
	        					}
	             				
	        					label = new Label(++col, k,nodeColorMsg); 
	        					excelSheet.addCell(label);
	           					
	        					//TAL TYP
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not linked to KSN";
	        					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale))!=null)
	        					{
	        						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale));
	        						if(mqEventKSN!=null && teacher.getStatus()!=null && teacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
	        						{
	        						if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Hired";
	        						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANDIDATE", locale))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Candidate";
	        						}
	        						else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCONDITIONALTEOFFER", locale))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Conditional Offer";
	        						}
	        						}
	        					}
	
	 	          				label = new Label(++col, k,nodeColorMsg); 
	 	                        excelSheet.addCell(label);
	                           
		 	                    //WTHD
		 	   					nodeColor = "7F7F7F"; //grey
		 	   					nodeColorMsg="Not withdrawn";
		 	   					if(teacher.getStatus()!=null && teacher.getStatusMaster()!=null && teacher.getStatus().getStatusShortName().equals("widrw") && teacher.getStatusMaster().getStatusShortName().equals("widrw")){
		 	   						nodeColor="FFA500"; //Orange
		 	   						nodeColorMsg="Withdrawn";
		 	   					}
	
	 	        				 label = new Label(++col, k,nodeColorMsg); 
	 	        	             excelSheet.addCell(label);
	 	                        
	 	        	            //IN-E
	 	    					nodeColor = "7F7F7F"; //grey
	 	    					nodeColorMsg="Not Inactive Eligible";
	 	    					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
	 	    					{
	 	    						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
	 	    						if(mqEventKSN!=null)
	 	    						{
	 	    							if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
	 	    								if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	 	    									nodeColor="C46E1C"; //green		
	 	    								    nodeColorMsg="Inactive Eligible";}
	 	    								else{
	 	    									nodeColor="7F7F7F"; //grey
	 	    									nodeColorMsg="Not Inactive Eligible";
	 	    								}
	 	    							}
	 	    						}
	 	    					}
	
	             				 label = new Label(++col, k,nodeColorMsg); 
	                             excelSheet.addCell(label);
	                             
	                           //IN-IE
	         					nodeColor = "7F7F7F"; //grey
	         					nodeColorMsg="Not Inactive Not Eligible";
	         					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
	         					{
	         						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
	         						if(mqEventKSN!=null)
	         						{
	         							if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale))){
	         								if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	         									nodeColor="C46E1C"; //green
	         									nodeColorMsg="Inactive Not Eligible";}
	         								else{
	         									nodeColor="7F7F7F"; //grey
	         									nodeColorMsg="Inactive Not Eligible";
	         								}
	         							}
	         						}
	         					}
	
	              			 label = new Label(++col, k,nodeColorMsg); 
	                         excelSheet.addCell(label);
	                         k++; 
	         	          }
	         	          
	         	        }
	        	        workbook.write();
	    	            workbook.close();
	    	            iteratingJFTList=null;
	        	 
	         }
	         logger.info(fileName);
		      return fileName;
	         
		}else if(actionFlag==3)    //for export to pdf to all candidates actionFlag=3
		{
			System.out.println(" action flag 3 >>>>>>>>  ");
			int cellSize = 0;
			Document document=null;
			FileOutputStream fos = null;
			PdfWriter writer = null;
			Paragraph footerpara = null;
			HeaderFooter headerFooter = null;
			
			Font font8 = null;
			Font font8Green = null;
			Font font8bold = null;
			Font font9 = null;
			Font font9bold = null;
			Font font10 = null;
			Font font10_10 = null;
			Font font10bold = null;
			Font font11 = null;
			Font font11bold = null;
			Font font20bold = null;
			Font font11b   =null;
			Color bluecolor =null;
			Font font8bold_new = null;
			
			try{
				String time = String.valueOf(System.currentTimeMillis()).substring(6);
				String basePath = context.getServletContext().getRealPath("/")+"/user/"+userId+"/";
				fileName =time+"Report.pdf";
				String path = basePath+fileName;
				String realPath = context.getServletContext().getRealPath("/");
				String fontPath = realPath;
				
				try {
					
					BaseFont.createFont(fontPath+"fonts/4637.ttf",BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
					BaseFont tahoma = BaseFont.createFont(fontPath+"fonts/4637.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
					font8 = new Font(tahoma, 8);
	
					font8Green = new Font(tahoma, 8);
					font8Green.setColor(Color.BLUE);
					font8bold = new Font(tahoma, 8, Font.NORMAL);
	
	
					font9 = new Font(tahoma, 9);
					font9bold = new Font(tahoma, 9, Font.BOLD);
					font10 = new Font(tahoma, 10);
					
					font10_10 = new Font(tahoma, 10);
					font10_10.setColor(Color.white);
					font10bold = new Font(tahoma, 10, Font.BOLD);
					font11 = new Font(tahoma, 11);
					font11bold = new Font(tahoma, 11,Font.BOLD);
					bluecolor =  new Color(0,122,180); 
					
					//Color bluecolor =  new Color(0,122,180); 
					
					font20bold = new Font(tahoma, 20,Font.BOLD,bluecolor);
					//font20bold.setColor(Color.BLUE);
					font11b = new Font(tahoma, 11, Font.BOLD, bluecolor);
	
	
				} 
				catch (DocumentException e1) 
				{
					e1.printStackTrace();
				} 
				catch (IOException e1) 
				{
					e1.printStackTrace();
				}
				
				System.out.println(" path :: "+path);
				
				File file = new File(path);
				
				if(!file.exists())
					file.mkdirs();
				
				Utility.deleteAllFileFromDir(basePath);
				System.out.println("user/"+userId+"/"+fileName);
				
	
				
				document = new Document(PageSize.A4.rotate(),10f,10f,10f,10f);		
				document.addAuthor("TeacherMatch");
				document.addCreator("TeacherMatch Inc.");
				document.addSubject(Utility.getLocaleValuePropByKey("headonboarding", locale));
				document.addCreationDate();
				document.addTitle(Utility.getLocaleValuePropByKey("headonboarding", locale));
	
				fos = new FileOutputStream(path);
				PdfWriter.getInstance(document, fos);
			
				document.open();
				PdfPTable mainTable = new PdfPTable(1);
				mainTable.setWidthPercentage(90);
	
				Paragraph [] para = null;
				PdfPCell [] cell = null;
	
	
				para = new Paragraph[3];
				cell = new PdfPCell[3];
				para[0] = new Paragraph(" ",font20bold);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_RIGHT);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				
				Image logo = Image.getInstance (fontPath+"/images/Logo with Beta300.png");
				logo.scalePercent(75);
				
				cell[1]= new PdfPCell(logo);
				cell[1].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[1].setBorder(0);
				mainTable.addCell(cell[1]);
				document.add(new Phrase("\n"));
				
				para[2] = new Paragraph("",font20bold);
				cell[2]= new PdfPCell(para[2]);
				cell[2].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[2].setBorder(0);
				mainTable.addCell(cell[2]);
				document.add(mainTable);
				document.add(new Phrase("\n"));
				
				float[] tblwidthz={.15f};
				
				mainTable = new PdfPTable(tblwidthz);
				mainTable.setWidthPercentage(100);
				para = new Paragraph[1];
				cell = new PdfPCell[1];
	
				
				para[0] = new Paragraph(Utility.getLocaleValuePropByKey("headonboarding", locale),font20bold);
				cell[0]= new PdfPCell(para[0]);
				cell[0].setHorizontalAlignment(Element.ALIGN_CENTER);
				cell[0].setBorder(0);
				mainTable.addCell(cell[0]);
				document.add(mainTable);
				document.add(new Phrase("\n"));
	
				 float[] tblwidths={.15f,.20f};
					
					mainTable = new PdfPTable(tblwidths);
					mainTable.setWidthPercentage(100);
					para = new Paragraph[2];
					cell = new PdfPCell[2];
	
					String name1 = userMaster.getFirstName()+" "+userMaster.getLastName();
					
					para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblCreatedBy", locale)+": "+name1,font10);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					cell[0].setBorder(0);
					mainTable.addCell(cell[0]);
					
					para[1] = new Paragraph(""+""+Utility.getLocaleValuePropByKey("lblCreatedOn", locale)+": "+Utility.convertDateAndTimeToUSformatOnlyDate(new Date()),font10);
					cell[1]= new PdfPCell(para[1]);
					cell[1].setHorizontalAlignment(Element.ALIGN_RIGHT);
					cell[1].setBorder(0);
					mainTable.addCell(cell[1]);
					
					document.add(mainTable);
					
					document.add(new Phrase("\n"));
					
					float[] tblwidth={.22f,.10f,.40f,.20f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.10f,.08f,.08f};
					
					mainTable = new PdfPTable(tblwidth);
					mainTable.setWidthPercentage(100);
					para = new Paragraph[19];
					cell = new PdfPCell[19];
				
					// header
					para[0] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblNm", locale),font10_10);
					cell[0]= new PdfPCell(para[0]);
					cell[0].setBackgroundColor(bluecolor);
					cell[0].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[0]);
					
					para[1] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblKSNID", locale),font10_10);
					cell[1]= new PdfPCell(para[1]);
					cell[1].setBackgroundColor(bluecolor);
					cell[1].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[1]);
					
					para[2] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJoTil", locale),font10_10);
					cell[2]= new PdfPCell(para[2]);
					cell[2].setBackgroundColor(bluecolor);
					cell[2].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[2]);
					
					//Hired by date Swadesh
					para[3] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblJOBID", locale),font10_10);
					cell[3]= new PdfPCell(para[3]);
					cell[3].setBackgroundColor(bluecolor);
					cell[3].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[3]);
	
					para[4] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblEPI", locale),font10_10);
					cell[4]= new PdfPCell(para[4]);
					cell[4].setBackgroundColor(bluecolor);
					cell[4].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[4]);
					
					para[5] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblAPP", locale),font10_10);
					cell[5]= new PdfPCell(para[5]);
					cell[5].setBackgroundColor(bluecolor);
					cell[5].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[5]);
					
					para[6] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblSPS", locale),font10_10);
					cell[6]= new PdfPCell(para[6]);
					cell[6].setBackgroundColor(bluecolor);
					cell[6].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[6]);
					
					para[7] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),font10_10);
					cell[7]= new PdfPCell(para[7]);
					cell[7].setBackgroundColor(bluecolor);
					cell[7].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[7]);
					
					para[8] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblINVWF1", locale),font10_10);
					cell[8]= new PdfPCell(para[8]);
					cell[8].setBackgroundColor(bluecolor);
					cell[8].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[8]);
					
					para[9] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblWF1COM", locale),font10_10);
					cell[9]= new PdfPCell(para[9]);
					cell[9].setBackgroundColor(bluecolor);
					cell[9].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[9]);
					
					
					para[10] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblINVWF2", locale),font10_10);
					cell[10]= new PdfPCell(para[10]);
					cell[10].setBackgroundColor(bluecolor);
					cell[10].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[10]);
					
					
					para[11] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblWF2COM", locale),font10_10);
					cell[11]= new PdfPCell(para[11]);
					cell[11].setBackgroundColor(bluecolor);
					cell[11].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[11]);
					
					
					para[12] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblINVWF3", locale),font10_10);
					cell[12]= new PdfPCell(para[12]);
					cell[12].setBackgroundColor(bluecolor);
					cell[12].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[12]);
					
					
					para[13] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblWF3COM", locale),font10_10);
					cell[13]= new PdfPCell(para[13]);
					cell[13].setBackgroundColor(bluecolor);
					cell[13].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[13]);
					
					
					para[14] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblSCR", locale),font10_10);
					cell[14]= new PdfPCell(para[14]);
					cell[14].setBackgroundColor(bluecolor);
					cell[14].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[14]);
					
					
					para[15] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblTALTYP", locale),font10_10);
					cell[15]= new PdfPCell(para[15]);
					cell[15].setBackgroundColor(bluecolor);
					cell[15].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[15]);
					
					
					para[16] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblWTHD", locale),font10_10);
					cell[16]= new PdfPCell(para[16]);
					cell[16].setBackgroundColor(bluecolor);
					cell[16].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[16]);
					
					
					para[17] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblINE", locale),font10_10);
					cell[17]= new PdfPCell(para[17]);
					cell[17].setBackgroundColor(bluecolor);
					cell[17].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[17]);
					
					
					para[18] = new Paragraph(""+Utility.getLocaleValuePropByKey("lblINIE", locale),font10_10);
					cell[18]= new PdfPCell(para[18]);
					cell[18].setBackgroundColor(bluecolor);
					cell[18].setHorizontalAlignment(Element.ALIGN_LEFT);
					mainTable.addCell(cell[18]);
					
					
					document.add(mainTable);
				
					for(JobForTeacher teacher : listJobForTeacher){
						 int index=0;
						 float[] tblwidth1={.22f,.10f,.40f,.20f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.08f,.10f,.08f,.08f};
								
						  mainTable = new PdfPTable(tblwidth1);
						  mainTable.setWidthPercentage(100);
						  para = new Paragraph[19];
						  cell = new PdfPCell[19];
						
						
						teacherNormScore = normMap.get(teacher.getTeacherId().getTeacherId());
						int normScore = 0;
						String color = "";
						if(teacherNormScore!=null)
						{
							normScore = teacherNormScore.getTeacherNormScore();
							 color = teacherNormScore.getDecileColor();
						}
						
						for(String str : nodeList)
						{
							if(nodeMap.get(teacher.getTeacherId().getTeacherId()+"--"+teacher.getJobId().getJobId()+"-"+str)!=null)
							{
								nodeColorMap.put(str, "Complete");	//green
							}else{
								nodeColorMap.put(str, "Pending");  //grey
							}
						}
						
						// Teacher Name
						String fullName=teacher.getTeacherId().getFirstName()+" "+teacher.getTeacherId().getLastName();
						para[index] = new Paragraph(fullName,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						
						if(teacher.getTeacherId().getKSNID()!=null)
							para[index] = new Paragraph(""+teacher.getTeacherId().getKSNID(),font8bold);
						else
							para[index] = new Paragraph(Utility.getLocaleValuePropByKey("optN/A", locale),font8bold);
						
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						
						//Job Title
						if(teacher.getJobId()!=null && teacher.getJobId().getJobTitle()!=null)
							para[index] = new Paragraph(teacher.getJobId().getJobTitle(),font8bold);
						else
							para[index] = new Paragraph(Utility.getLocaleValuePropByKey("optN/A", locale),font8bold);
						
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						
						
						// Job Id
						if(teacher.getJobId()!=null)
							para[index] = new Paragraph(""+teacher.getJobId().getJobId(),font8bold);
						else
							para[index] = new Paragraph(""+Utility.getLocaleValuePropByKey("optN/A", locale),font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						
						//Norm Score
						if(normScore==0)
							para[index] = new Paragraph(""+Utility.getLocaleValuePropByKey("optN/A", locale),font8bold);
						else
							para[index] = new Paragraph(""+normScore,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
				
						//App
						String nodeColor = "7F7F7F"; //grey
	     				String	nodeColorMsg="Not started";
						
	     				if(teacher.getStatusMaster()!=null && teacher.getStatus().getStatusShortName().equals("comp")){
	     					nodeColor="009900"; //Green
						    nodeColorMsg="Complete";
	     				}else{
	     					nodeColor="fcd914"; //Yellow
						    nodeColorMsg="In Progress";
	     				}
						para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						
						//SPS
						int spNodeStatus=0;
						nodeColorMsg="Not started";
						nodeColor = "7F7F7F"; // grey
						String nodeColorNameW="Grey";
						if(spInboundMap!=null && spInboundMap.get(teacher.getTeacherId())!=null){
							spNodeStatus=1;
						}
						if(spStatusMap.size()>0){
						      if(spStatusMap.get(teacher.getTeacherId().getTeacherId())!=null){
						       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())>2){
						        spNodeStatus=2;
						       }
						       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())==1){
						        spNodeStatus=3;
						       }
						      }
						   }
						SecondaryStatus subfL =null;
						if(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
							subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
						}else{
							subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getJobCategoryId());
						}
						if(subfL!=null){
						    String statusStr=teacher.getTeacherId().getTeacherId()+"##"+teacher.getJobId().getJobId()+"##W";
						    String waived=mapHistrory.get(statusStr);
						    	if(waived!=null && waived.equalsIgnoreCase("W")){
							    	nodeColor="243a6a"; //blue
							    	nodeColorMsg="SP Assessment waived";
							    	nodeColorNameW="Blue";
						        }else if(subfL.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)) && spNodeStatus!=0){
						         if(spNodeStatus==1){
						        	 nodeColor="fcd914"; //yellow
						        	 nodeColorMsg="SP Assessment in progress";
						        	 nodeColorNameW="Yellow";
						         }else if(spNodeStatus==2){
						        	 nodeColor="ff0000"; //red
						        	 nodeColorMsg="SP Assessment failed twice";
						        	 nodeColorNameW="Red";
						         }else if(spNodeStatus==3){
						        	 nodeColor="009900"; //green
						        	 nodeColorMsg="Passed SP assessment";
						        	 nodeColorNameW="Green";
						         } 
						        }
						    }
	
	     				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						//////////////////////////////////////
				
						///////// Link TO KSN
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
						int KSNDivFlag=0;
						 nodeColor = "7F7F7F"; //grey
						 nodeColorMsg="Not initiated";
						if(mqEventKSN!=null){
							if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
								KSNDivFlag=1;
								nodeColor="009900"; //green
								nodeColorMsg="Linked to KSN";
							}
							else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()==null){
								nodeColor="fcd914"; //yellow
								nodeColorMsg="Initiated, waiting for msg";
							}
							else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().equalsIgnoreCase("Received")){
								nodeColor="fcd914"; //yellow
								nodeColorMsg="Initiated, waiting for msg";
							}
							else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
								nodeColor="ff0000"; //red
								nodeColorMsg="Error";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
								nodeColor="243a6a"; //blue
								nodeColorMsg="Initiated confirmation msg";
							}
						}
	
	      				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////
				
				
						//INV WF1
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not initiated";
						if(mqEventKSN!=null){
							MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
							if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf1";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf1";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf1";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
								nodeColor="ff0000"; //red
								nodeColorMsg="Error";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
								nodeColor="ff0000"; //red
								nodeColorMsg="Error";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
								nodeColor="fcd914"; //yellow
								nodeColorMsg="Initiated, waiting for msg";
							}
						}
	
	     				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						//WF1 COM
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not initiated";
						
						MQEvent lblWF1COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
						if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="WF1 Complete";
						}
						else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="WF1 Complete";
						}
						else if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
							nodeColor="ff0000"; //Red
							nodeColorMsg="Error";
						}
	
	     				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						//INV WF2
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not initiated";
						if(mqEventKSN!=null){
							MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
							if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf2";
							}if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf2";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf2";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
								nodeColor="ff0000"; //red
								nodeColorMsg="Error";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
								nodeColor="ff0000"; //red
								nodeColorMsg="Error";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
								nodeColor="fcd914"; //yellow
								nodeColorMsg="Initiated, waiting for msg";
							}
						}
	
	        			para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						//WF2 COM
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not initiated";
						MQEvent lblWF2COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
						if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="WF2 Complete";
						}else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null &&  lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="WF2 Complete";
						}else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getStatus().equalsIgnoreCase("R") && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
							nodeColor="ff0000"; //red
							nodeColorMsg="Awaiting Review";
						}
						else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
							nodeColor="ff0000"; //red
							nodeColorMsg="Error";
						}
	
	      				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						
						//INV WF3
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not initiated";
						if(mqEventKSN!=null){
							MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
							if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf3";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf3";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
								nodeColor="009900"; //green
								nodeColorMsg="Invited to Wf3";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
								nodeColor="ff0000"; //red
								nodeColorMsg="Error";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
								nodeColor="ff0000"; //red
								nodeColorMsg="Error";
							}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
								nodeColor="fcd914"; //yellow
								nodeColorMsg="Manually initiated";
							}
						}
	       				
	       				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						
						//WF3 COM
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not initiated";
					
						MQEvent lblWF3COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
						if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null &&  lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="WF3 Complete";
						}
						else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="009900"; //green
							nodeColorMsg="WF3 Complete";
						}
						else if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null && lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
							nodeColor="ff0000"; //green
							nodeColorMsg="Error";
						}
	
	     				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						//SCR
						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblScreening", locale));
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="No screening record";
						if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null){
							if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))){
								nodeColor="fcd914"; //yellow
								nodeColorMsg="Pending results";
							}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale))){
								nodeColor="243a6a"; //blue
								nodeColorMsg="Review required";
							}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
								nodeColor="009900"; //green
								nodeColorMsg="Favorable results";
							}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
								nodeColor="ff0000"; //red
								nodeColorMsg="Unfavorable results";
							}
						}
	
	    				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						//TAL TYP
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not linked to KSN";
						if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale))!=null)
						{
							mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale));
							if(mqEventKSN!=null  && teacher.getStatus()!=null && teacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
							{
							if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
								nodeColor="009900"; //green
								nodeColorMsg="Hired";
							}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANDIDATE", locale))){
								nodeColor="fcd914"; //yellow
								nodeColorMsg="Candidate";
							}
							else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCONDITIONALTEOFFER", locale))){
								nodeColor="fcd914"; //yellow
								nodeColorMsg="Conditional Offer";
							}
							}
						}
	
	      				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						
						//WTHD
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not withdrawn";
						if(teacher.getStatus()!=null && teacher.getStatusMaster()!=null && teacher.getStatus().getStatusShortName().equals("widrw") && teacher.getStatusMaster().getStatusShortName().equals("widrw")){
							nodeColor="FFA500"; //Orange
							nodeColorMsg="Withdrawn";
						}
	
	    				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						//IN-E
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not Inactive Eligible";
						if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
						{
							mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
							if(mqEventKSN!=null)
							{
								if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
									if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
										nodeColor="C46E1C"; //green		
									    nodeColorMsg="Inactive Eligible";}
									else{
										nodeColor="7F7F7F"; //grey
										nodeColorMsg="Not Inactive Eligible";
									}
								}
							}
						}
	
	    				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						
						
						//IN-IE
						nodeColor = "7F7F7F"; //grey
						nodeColorMsg="Not Inactive Not Eligible";
						if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
						{
							mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
							if(mqEventKSN!=null)
							{
								if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale))){
									if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
										nodeColor="C46E1C"; //green
										nodeColorMsg="Inactive Not Eligible";}
									else{
										nodeColor="7F7F7F"; //grey
										nodeColorMsg="Inactive Not Eligible";
									}
								}
							}
						}
	
	      				para[index] = new Paragraph(nodeColorMsg,font8bold);
						cell[index]= new PdfPCell(para[index]);
						cell[index].setHorizontalAlignment(Element.ALIGN_LEFT);
						mainTable.addCell(cell[index]);
						index++;
						/////////////////////////////////////////////////
						
						document.add(mainTable);
					}
				document.close();
				
				return "user/"+userId+"/"+fileName;
				
			}
					catch(Exception e){
				e.printStackTrace();
			}finally
			{
				if(document != null && document.isOpen())
					document.close();
				if(writer!=null){
					writer.flush();
					writer.close();
				}
			}
			
		}else  if(actionFlag==4){
	
			
			dmRecords.append("<table  id='tblGridOfferReady' width='100%' align='center'>");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			dmRecords.append("<th  style='text-align:center; font-size:22px;' valign='top'>"+Utility.getLocaleValuePropByKey("headonboarding", locale)+"</th>");
			dmRecords.append("</tr>");
			
			dmRecords.append("<table  id='tblGridOfferReady' width='100%' border='1'>");
			dmRecords.append("<thead class='bg'>");
			dmRecords.append("<tr>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblNm", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblKSNID", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblJoTil", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblJOBID", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblEPI", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblAPP", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblSPS", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblINVWF1", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblWF1COM", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblINVWF2", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblWF2COM", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblINVWF3", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblWF3COM", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblSCR", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblTALTYP", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblWTHD", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblINE", locale)+"</th>");
			dmRecords.append("<th  style='text-align:left' valign='top'>"+Utility.getLocaleValuePropByKey("lblINIE", locale)+"</th>");
			dmRecords.append("</tr>");
			dmRecords.append("</thead>");
			
			 Map<Long,Long> checkedJFTMap = new HashMap<Long, Long>();
			 
	         
	         
	        if(listJobForTeacher.size()>0){
	        	        for(JobForTeacher teacher : listJobForTeacher)
	        	        {
	        	        	dmRecords.append("<tr>");	
	        	        	  teacherNormScore = normMap.get(teacher.getTeacherId().getTeacherId());
	        					int normScore = 0;
	        					String color = "";
	        					if(teacherNormScore!=null)
	        					{
	        						normScore = teacherNormScore.getTeacherNormScore();
	        						 color = teacherNormScore.getDecileColor();
	        					}
	        					
	        					for(String str : nodeList)
	        					{
	        						if(nodeMap.get(teacher.getTeacherId().getTeacherId()+"--"+teacher.getJobId().getJobId()+"-"+str)!=null)
	        						{
	        							nodeColorMap.put(str, "Complete");	//green
	        						}else{
	        							nodeColorMap.put(str, "Pending");  //grey
	        						}
	        						
	        					}
	        					
	        					String fullName=teacher.getTeacherId().getFirstName()+" "+teacher.getTeacherId().getLastName();
	        					
	        					
	        					dmRecords.append("<td style='font-size:12px;'>"+fullName+"</td>");
	        					
	        					if(teacher.getTeacherId().getKSNID()!=null){
	        						dmRecords.append("<td style='font-size:12px;'>"+teacher.getTeacherId().getKSNID()+"</td>"); 
		                        }else{
		                        	dmRecords.append("<td style='font-size:12px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
		                        }
	        					
	        					if(teacher.getJobId()!=null && teacher.getJobId().getJobTitle()!=null){
	        						dmRecords.append("<td style='font-size:12px;'>"+teacher.getJobId().getJobTitle()+"</td>");
	        					}
	        					else{
	        						dmRecords.append("<td style='font-size:12px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>");
	        					}
	        					
	        					if(teacher.getJobId()!=null){                                                                                                   
	        						dmRecords.append("<td style='font-size:12px;'>"+teacher.getJobId().getJobId()+"</td>"); 
	        					}else{
	        						dmRecords.append("<td style='font-size:12px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>"); 
	        					}
	        					if(normScore==0)
	        					{
	        						dmRecords.append("<td style='font-size:12px;'>"+Utility.getLocaleValuePropByKey("optN/A", locale)+"</td>"); 
	        					}
	        					else
	        					{
	        						dmRecords.append("<td style='font-size:12px;'>"+normScore+"</td>"); 
	        		            }
	        					
	        					//APP
	        					String nodeColor = "7F7F7F"; //grey
	        	 				String	nodeColorMsg="Not started";
	        	 				if(teacher.getApplicationStatus()!=null && teacher.getApplicationStatus().equals(WorkThreadServlet.statusMap.get("icomp").getStatusId())){
	        	 					nodeColor="fcd914"; //Yellow
	        					    nodeColorMsg="In Progress";
	        	 				}else{
	        	 					nodeColor="009900"; //Green
	        					    nodeColorMsg="Complete";
	        	 				}
	        					
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	        					
	             				//SPS
	        					int spNodeStatus=0;
	        					nodeColorMsg="Not started";
	        					nodeColor = "7F7F7F"; // grey
	        					String nodeColorNameW="Grey";
	        					if(spInboundMap!=null && spInboundMap.get(teacher.getTeacherId())!=null){
	        						spNodeStatus=1;
	        					}
	        					if(spStatusMap.size()>0){
	        					      if(spStatusMap.get(teacher.getTeacherId().getTeacherId())!=null){
	        					       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())>2){
	        					        spNodeStatus=2;
	        					       }
	        					       if(spStatusMap.get(teacher.getTeacherId().getTeacherId())==1){
	        					        spNodeStatus=3;
	        					       }
	        					      }
	        					   }
	        					SecondaryStatus subfL =null;
	        					if(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
	        						subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
	        					}else{
	        						subfL = categoryStatusMap.get(teacher.getJobId().getJobCategoryMaster().getJobCategoryId());
	        					}
	        					if(subfL!=null){
	        					    String statusStr=teacher.getTeacherId().getTeacherId()+"##"+teacher.getJobId().getJobId()+"##W";
	        					    String waived=mapHistrory.get(statusStr);
	        					    	if(waived!=null && waived.equalsIgnoreCase("W")){
	        						    	nodeColor="243a6a"; //blue
	        						    	nodeColorMsg="SP Assessment waived";
	        						    	nodeColorNameW="Blue";
	        					        }else if(subfL.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)) && spNodeStatus!=0){
	        					         if(spNodeStatus==1){
	        					        	 nodeColor="fcd914"; //yellow
	        					        	 nodeColorMsg="SP Assessment in progress";
	        					        	 nodeColorNameW="Yellow";
	        					         }else if(spNodeStatus==2){
	        					        	 nodeColor="ff0000"; //red
	        					        	 nodeColorMsg="SP Assessment failed twice";
	        					        	 nodeColorNameW="Red";
	        					         }else if(spNodeStatus==3){
	        					        	 nodeColor="009900"; //green
	        					        	 nodeColorMsg="Passed SP assessment";
	        					        	 nodeColorNameW="Green";
	        					         } 
	        					        }
	        					    }
	
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	             				
	             				//Link to KSN
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
	        					int KSNDivFlag=0;
	        					 nodeColor = "7F7F7F"; //grey
	        					 nodeColorMsg="Not initiated";
	        					if(mqEventKSN!=null){
	        						/*if(mqEventKSN.getCheckEmailAPI()!=null && mqEventKSN.getCheckEmailAPI()){
	        							nodeColor = "7F7F7F"; //grey
	        							nodeColorMsg="Not initiated";
	        					    }else*/
	        						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	        							KSNDivFlag=1;
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Linked to KSN";
	        						}
	        						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()==null){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Initiated, waiting for msg";
	        						}
	        						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().equalsIgnoreCase("Received")){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Initiated, waiting for msg";
	        						}
	        						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
	        							nodeColor="243a6a"; //blue
	        							nodeColorMsg="Initiated confirmation msg";
	        						}
	        					}
	
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	             			
	             				//INV WF1
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        					if(mqEventKSN!=null){
	        						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
	        						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf1";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf1";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf1";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Initiated, waiting for msg";
	        						}
	        					}
	
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	             				
	             				//WF1 COM
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        					
	        					MQEvent lblWF1COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
	        					if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF1 Complete";
	        					}
	        					else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF1 Complete";
	        					}
	        					else if(lblWF1COM!=null && lblWF1COM.getStatus()!=null && lblWF1COM.getWorkFlowStatusId()!=null && lblWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
	        						nodeColor="ff0000"; //Red
	        						nodeColorMsg="Error";
	        					}
	
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	             				
	             				//INV WF2
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        					if(mqEventKSN!=null){
	        						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
	        						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf2";
	        						}/*else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
	        							nodeColor="7F7F7F"; //gray
	        							nodeColorMsg="Not initiated";
	        						}else */if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf2";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf2";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Initiated, waiting for msg";
	        						}
	        					}
	
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	 	        			     
	             				//WF2 COM
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        					MQEvent lblWF2COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
	        					if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF2 Complete";
	        					}else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null &&  lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF2 Complete";
	        					}else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getStatus().equalsIgnoreCase("R") && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
	        						nodeColor="ff0000"; //red
	        						nodeColorMsg="Awaiting Review";
	        					}
	        					else if(lblWF2COM!=null && lblWF2COM.getStatus()!=null && lblWF2COM.getWorkFlowStatusId()!=null && lblWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
	        						nodeColor="ff0000"; //red
	        						nodeColorMsg="Error";
	        					}
	             				
	        					dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	             			     
	        					//INV WF3
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        					if(mqEventKSN!=null){
	        						MQEvent tempMqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
	        						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf3";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf3";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && tempMqEventKSN!=null && tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Invited to Wf3";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().contains("Success")) && tempMqEventKSN!=null &&  tempMqEventKSN.getWorkFlowStatusId()!=null && tempMqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblN", locale))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure")) && mqEventKSN.getWorkFlowStatusId()==null){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Error";
	        						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate()))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Manually initiated";
	        						}
	        					}
	             				
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	             			
	             				//WF3 COM
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not initiated";
	        				
	        					MQEvent lblWF3COM = mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
	        					if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null &&  lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF3 Complete";
	        					}
	        					else if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	        						nodeColor="009900"; //green
	        						nodeColorMsg="WF3 Complete";
	        					}
	        					else if(lblWF3COM!=null && lblWF3COM.getStatus()!=null && lblWF3COM.getWorkFlowStatusId()!=null && lblWF3COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale))){
	        						nodeColor="ff0000"; //green
	        						nodeColorMsg="Error";
	        					}
	
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	             		 		
	             				//SCR
	        					mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblScreening", locale));
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="No screening record";
	        					if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null){
	        						if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINPR", locale))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Pending results";
	        						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblRERE", locale))){
	        							nodeColor="243a6a"; //blue
	        							nodeColorMsg="Review required";
	        						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblFAVO", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Favorable results";
	        						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblUNFA", locale))){
	        							nodeColor="ff0000"; //red
	        							nodeColorMsg="Unfavorable results";
	        						}
	        					}
	
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	         				 	
	             				//TAL TYP
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not linked to KSN";
	        					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale))!=null)
	        					{
	        						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgHired2", locale));
	        						if(mqEventKSN!=null  && teacher.getStatus()!=null && teacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
	        						{
	        						if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblHIRED", locale))){
	        							nodeColor="009900"; //green
	        							nodeColorMsg="Hired";
	        						}else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCANDIDATE", locale))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Candidate";
	        						}
	        						else if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCONDITIONALTEOFFER", locale))){
	        							nodeColor="fcd914"; //yellow
	        							nodeColorMsg="Conditional Offer";
	        						}
	        						}
	        					}
	
	 	          				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	 	          				
		 	          			//WTHD
		 	   					nodeColor = "7F7F7F"; //grey
		 	   					nodeColorMsg="Not withdrawn";
		 	   					if(teacher.getStatus()!=null && teacher.getStatusMaster()!=null && teacher.getStatus().getStatusShortName().equals("widrw") && teacher.getStatusMaster().getStatusShortName().equals("widrw")){
		 	   						nodeColor="FFA500"; //Orange
		 	   						nodeColorMsg="Withdrawn";
		 	   					}
	
	 	        				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	 	        		
	 	        				//IN-E
		 	   					nodeColor = "7F7F7F"; //grey
		 	   					nodeColorMsg="Not Inactive Eligible";
		 	   					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
		 	   					{
		 	   						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
		 	   						if(mqEventKSN!=null)
		 	   						{
		 	   							if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVEELIG", locale))){
		 	   								if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
		 	   									nodeColor="C46E1C"; //green		
		 	   								    nodeColorMsg="Inactive Eligible";}
		 	   								else{
		 	   									nodeColor="7F7F7F"; //grey
		 	   									nodeColorMsg="Not Inactive Eligible";
		 	   								}
		 	   							}
		 	   						}
		 	   					}
	
	             				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	             				 
	             				//IN-IE
	        					nodeColor = "7F7F7F"; //grey
	        					nodeColorMsg="Not Inactive Not Eligible";
	        					if(mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale))!=null)
	        					{
	        						mqEventKSN=mapNodeStatus.get(teacher.getTeacherId()+"-"+Utility.getLocaleValuePropByKey("msgTalentStatus", locale));
	        						if(mqEventKSN!=null)
	        						{
	        							if(mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINACTIVENELIG", locale))){
	        								if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
	        									nodeColor="C46E1C"; //green
	        									nodeColorMsg="Inactive Not Eligible";}
	        								else{
	        									nodeColor="7F7F7F"; //grey
	        									nodeColorMsg="Inactive Not Eligible";
	        								}
	        							}
	        						}
	        					}
	
	              				dmRecords.append("<td style='font-size:12px;'>"+nodeColorMsg+"</td>"); 
	              		dmRecords.append("</tr>"); 
	        	   }
	        	        dmRecords.append("</table>");
	         	}
	        	return dmRecords.toString();
	         
		}
	}catch(Exception e){
		e.printStackTrace();
	}
	return dmRecords.toString();
	}
	public int jobListByTeacherAndCategory(int teacherId,int jobCategoryId,SortedMap<Long,JobForTeacher> jftMap)
	{
		int jobList=0;
		try{
			Iterator iterator = jftMap.keySet().iterator();
			JobForTeacher jobForTeacher=null;
			while (iterator.hasNext()) {
				Object key = iterator.next();
				jobForTeacher=jftMap.get(key);
				int teacherKey=Integer.parseInt(jobForTeacher.getTeacherId().getTeacherId()+"");
				//int categoryKey=Integer.parseInt(jobForTeacher.getJobId().getJobCategoryMaster().getJobCategoryId()+"");
				if(teacherKey==teacherId){
					jobList++;
				}
			}
		}catch (Exception e) {
			// TODO: handle exception
		}
		return jobList;
	}
	public String getJobListByTeacherAndJobCategory(int teacherId,int jobOrderId,String noOfRow, String pageNo,String sortOrder,String sortOrderType,boolean showCommunication)
	{
	
		
		TestTool.getTraceTime("Job List 200");
		String visitLocation="";
		int subjectId=0;
		String zoneIdd="";
		Integer geoZoneId=0;
		if(zoneIdd!=null && zoneIdd!="")
			geoZoneId =  Integer.parseInt(zoneIdd);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		UserMaster userMaster=null;
		DistrictMaster districtMaster=null;
		HeadQuarterMaster headQuarterMaster = null;
		BranchMaster branchMaster = null;
		SchoolMaster schoolMaster=null;
		int entityID=0;
		int roleId=0;
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}else{
			userMaster=	(UserMaster) session.getAttribute("userMaster");
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
			}
			if(userMaster.getBranchMaster()!=null){
				branchMaster = userMaster.getBranchMaster();
			}
			if(userMaster.getHeadQuarterMaster()!=null){
				headQuarterMaster = userMaster.getHeadQuarterMaster();
			}
			if(userMaster.getDistrictId()!=null){
				districtMaster=userMaster.getDistrictId();
			}
			if(userMaster.getSchoolId()!=null){
				schoolMaster =userMaster.getSchoolId();
			}
			entityID = userMaster.getEntityType();
		}

		//boolean achievementScore=false,tFA=false,demoClass=false,JSI=false,teachingOfYear =false,expectedSalary=false,fitScore=false;
		boolean demoClass=false,JSI=false,fitScore=false,isNoble=false,isFlutron=false;
		
		if(entityID==1)
		{
			//achievementScore=true;tFA=true;demoClass=true;JSI=true;teachingOfYear =true;expectedSalary=true;fitScore=true;
			demoClass=true;JSI=true;fitScore=true;
		}
		else if(entityID==5)
		{
			if(headQuarterMaster.getDisplayDemoClass()!=null && headQuarterMaster.getDisplayDemoClass()){
				demoClass=true;
			}
			if(headQuarterMaster.getDisplayJSI()!=null && headQuarterMaster.getDisplayJSI()){
				JSI=true;
			}
			if(headQuarterMaster.getDisplayFitScore()!=null && headQuarterMaster.getDisplayFitScore()){
				fitScore=true;
			}
		}
		else if(entityID==6)
		{
			if(branchMaster.getDisplayDemoClass()!=null && branchMaster.getDisplayDemoClass()){
				demoClass=true;
			}
			if(branchMaster.getDisplayJSI()!=null && branchMaster.getDisplayJSI()){
				JSI=true;
			}
			if(branchMaster.getDisplayFitScore()!=null && branchMaster.getDisplayFitScore()){
				fitScore=true;
			}			
		}
		else
		{
			try{
				DistrictMaster districtMasterObj=districtMaster;
				if( districtMaster !=null && districtMaster.getDistrictId().equals(7800038))
			    {
				  isNoble = true;
			    }
				if( districtMaster !=null && districtMaster.getDistrictId().equals(614730))
			    {
					isFlutron = true;
			    }
				
				/*if(districtMasterObj.getDisplayAchievementScore()){
					achievementScore=true;
				}
				if(districtMasterObj.getDisplayTFA()){
					tFA=true;
				}*/
				if(districtMasterObj.getDisplayDemoClass()){
					demoClass=true;
				}
				if(districtMasterObj.getDisplayJSI()){
					JSI=true;
				}
				/*if(districtMasterObj.getDisplayYearsTeaching()){
					teachingOfYear=true;
				}*/
				/*if(districtMasterObj.getDisplayExpectedSalary()){
					expectedSalary=true;
				}*/
				if(districtMasterObj.getDisplayFitScore()){
					fitScore=true;
				}
			}catch(Exception e){ e.printStackTrace();}
		}
		TestTool.getTraceTime("Job List 201");
		StatusMaster status = null;
		StringBuffer sb = new StringBuffer();	
		int noOfRecordCheck = 0;
		try 
		{	
			//String locale = Utility.getValueOfPropByKey("locale");
			String lblZone  = Utility.getLocaleValuePropByKey("lblZone", locale);
			String lblSubject  = Utility.getLocaleValuePropByKey("lblSubject", locale);
			String lblNoRecord = Utility.getLocaleValuePropByKey("lblNoRecord", locale);
			String lblId  = Utility.getLocaleValuePropByKey("lblId", locale);
			String lblType  = Utility.getLocaleValuePropByKey("lblType", locale);
			String lblCategory  = Utility.getLocaleValuePropByKey("lblCategory", locale);
			String lblDateApplied  = Utility.getLocaleValuePropByKey("lblDateApplied", locale);
			String lblApplication  = Utility.getLocaleValuePropByKey("lblApplication ", locale);
			String lblDemoLesson   = Utility.getLocaleValuePropByKey("lblDemoLesson", locale);
			String lblDetails   = Utility.getLocaleValuePropByKey("lblDetails", locale);
			String lblAppliedOn  = Utility.getLocaleValuePropByKey("lblAppliedOn", locale);
			String lblClickToViewCG  = Utility.getLocaleValuePropByKey("lblClickToViewCG", locale);
			String lblClickToViewJO  = Utility.getLocaleValuePropByKey("lblClickToViewJO", locale);
			String lblCandidateNotCompletedJSI  = Utility.getLocaleValuePropByKey("lblCandidateNotCompletedJSI", locale);
			String lbllblCandidateTimedOut   = Utility.getLocaleValuePropByKey("lbllblCandidateTimedOut", locale);
			String lblNoCoverLetter   = Utility.getLocaleValuePropByKey("lblNoCoverLetter ", locale);
			String lblAvailable   = Utility.getLocaleValuePropByKey("lblAvailable", locale);
			String lblWithdrew   = Utility.getLocaleValuePropByKey("lblWithdrew", locale);
			String lblCompleted   = Utility.getLocaleValuePropByKey("lblCompleted", locale);
			String lblIncomplete   = Utility.getLocaleValuePropByKey("lblIncomplete", locale);
			String lblHired    = Utility.getLocaleValuePropByKey("lblHired", locale);
			String lblTimedOut    = Utility.getLocaleValuePropByKey("lblTimedOut", locale);
			String lblJobTitle    = Utility.getLocaleValuePropByKey("lblJobTitle", locale);
			String lblStatus    = Utility.getLocaleValuePropByKey("lblStatus", locale);
			String lblFitScore    = Utility.getLocaleValuePropByKey("lblFitScore", locale);
			String lblDeclined    = Utility.getLocaleValuePropByKey("lblDeclined", locale);
			String lblChangeStatus    = Utility.getLocaleValuePropByKey("lblChangeStatus", locale);
			String lblCommunications    = Utility.getLocaleValuePropByKey("headCom", locale);
			
			
			int noOfRowInPage = Integer.parseInt(noOfRow);
			int pgNo = Integer.parseInt(pageNo);
			int start =((pgNo-1)*noOfRowInPage);
			int end =((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
			int totalRecord = 0;
			//------------------------------------

			/** set default sorting fieldName **/
			String sortOrderFieldName="jobId";
			String sortOrderNoField="jobId";

			boolean deafultFlag=true;
			/**Start set dynamic sorting fieldName **/
			Order  sortOrderStrVal=null;
			if(sortOrder!=null){
				if(!sortOrder.equals("") && !sortOrder.equals(null) && !sortOrder.equals("JobType") && !sortOrder.equals("jobCategoryId")&& !sortOrder.equals("applicationStatus") && !sortOrder.equals("dateApplied") && !sortOrder.equals("subjectId") && !sortOrder.equals("geoZoneName")){
					sortOrderFieldName=sortOrder;
					sortOrderNoField=sortOrder;
				}
				if(sortOrder.equals("JobType")){
					sortOrderNoField="JobType";
				}
				if(sortOrder.equals("jobCategoryId")){
					sortOrderNoField="jobCategoryId";
				}
				if(sortOrder.equals("applicationStatus")){
					sortOrderNoField="applicationStatus";
				}
				if(sortOrder.equals("dateApplied")){
					sortOrderNoField="dateApplied";
				}
				if(sortOrder.equals("subjectId")){
					sortOrderNoField="subjectId";
				}
				if(sortOrder.equals("geoZoneName"))
				{
					sortOrderNoField="geoZoneName";
				}
			}
			if(sortOrderNoField.equals("demoClass") || sortOrderNoField.equals("fitScore")){
				sortOrderFieldName="jobId";
			}

			String sortOrderTypeVal="0";
			if(!sortOrderType.equals("") && !sortOrderType.equals(null)){
				if(sortOrderType.equals("0")){
					sortOrderStrVal=Order.asc(sortOrderFieldName);
				}else{
					sortOrderTypeVal="1";
					sortOrderStrVal=Order.desc(sortOrderFieldName);
				}
			}else{
				sortOrderTypeVal="0";
				sortOrderStrVal=Order.asc(sortOrderFieldName);
			}

			/**End ------------------------------------**/
			String roleAccess=null;
			try{
				roleAccess = roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			TestTool.getTraceTime("Job List 202");
			JobCategoryMaster jobCategoryMaster=null;
			JobOrder job=null;
			SubjectMaster subjectMaster=null;
			//if(jobCategoryId > 0)
				//jobCategoryMaster=jobCategoryMasterDAO.findById(jobCategoryId, false, false);
			if(subjectId > 0)
				subjectMaster=subjectMasterDAO.findById(subjectId, false, false);
			TestTool.getTraceTime("Job List 203");
			List<JobOrder> lstJobOrder = new ArrayList<JobOrder>();
			TeacherDetail teacherDetail =teacherDetailDAO.findById(teacherId, false, false);
			List<SecondaryStatus> lstSecondaryStatus =new ArrayList<SecondaryStatus>();
			List<JobForTeacher> lstjft= jobForTeacherDAO.findJobByTeacher(teacherDetail);
			
			TestTool.getTraceTime("Job List 204");
			Map<Integer,JobForTeacher> jftList = new HashMap<Integer, JobForTeacher>();
			List<JobCategoryMaster> jobCategoryMastersDP=new ArrayList<JobCategoryMaster>();
			List<StatusMaster> statusMastersDP=new ArrayList<StatusMaster>();
			for(JobForTeacher jftObj:lstjft){
				jftList.put(jftObj.getJobId().getJobId(),jftObj);
				if(jftObj!=null && jftObj.getSecondaryStatus()!=null)
					lstSecondaryStatus.add(jftObj.getSecondaryStatus());
				
				if(jftObj.getStatusMaster()!=null && (jftObj.getStatusMaster().getStatusShortName().equalsIgnoreCase("scomp") || jftObj.getStatusMaster().getStatusShortName().equalsIgnoreCase("ecomp") || jftObj.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp")))
				{
					if(jftObj.getJobId().getHeadQuarterMaster()!=null && jftObj.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
						jobCategoryMastersDP.add(jftObj.getJobId().getJobCategoryMaster().getParentJobCategoryId());
					}
					else{
						jobCategoryMastersDP.add(jftObj.getJobId().getJobCategoryMaster());
					}
					statusMastersDP.add(jftObj.getStatusMaster());
				}
			}
			
			List<SecondaryStatus> lstSecondaryStatusDPoint=new ArrayList<SecondaryStatus>();
			if(jobCategoryMastersDP!=null && jobCategoryMastersDP.size()>0 && statusMastersDP!=null && statusMastersDP.size()>0)
			{
				Criterion criterionSS2 =Restrictions.eq("status","A");
				Criterion criterionSS3 =Restrictions.in("jobCategoryMaster",jobCategoryMastersDP);
				Criterion criterionSS4 =Restrictions.in("statusMaster",statusMastersDP);
				lstSecondaryStatusDPoint =	secondaryStatusDAO.findByCriteria(criterionSS2,criterionSS3,criterionSS4);
			}
			logger.info("lstSecondaryStatus "+lstSecondaryStatus.size());
			if(lstSecondaryStatus!=null)
				lstSecondaryStatus.addAll(lstSecondaryStatusDPoint);
			else
				lstSecondaryStatus=lstSecondaryStatusDPoint;
			
			
			TestTool.getTraceTime("Job List 205");
			Criterion criterion4= Restrictions.eq("districtMaster",districtMaster);
			List<JobOrder> lstJobOrderList = new ArrayList<JobOrder>();//schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
//			if(jobOrderId>0)
//			job=jobOrderDAO.findById(jobOrderId, false, false);
//			if(job!=null)
//			lstJobOrderList.add(job);
			Map<Integer,JobOrder> schoolJobMap = new HashMap<Integer, JobOrder>();  
			if(schoolMaster!=null)
			{
				List<JobOrder> lstSchoolJobOrderList = schoolInJobOrderDAO.jobOrderPostedBySchool(schoolMaster);
				for (JobOrder jobOrder : lstSchoolJobOrderList) {
					schoolJobMap.put(jobOrder.getJobId(), jobOrder);
				}
			}
			TestTool.getTraceTime("Job List 206");
			List<Integer> statusIdList = new ArrayList<Integer>();
			String[] statusShrotName ={"comp","icomp","vlt","hird","scomp","ecomp","vcomp","dcln","rem","widrw","cghide"};
			try{
				statusIdList = statusMasterDAO.findStatusIdsByStatusByShortNames(statusShrotName);
			}catch (Exception e) {
				e.printStackTrace();
			}
			Map<Integer,StatusMaster> swcsMap= new HashMap<Integer,StatusMaster>();
			lstJobOrder=jobForTeacherDAO.getJobOrderByTeacher(statusIdList,sortOrderStrVal,teacherDetail,lstJobOrderList,headQuarterMaster,branchMaster,districtMaster,jobCategoryMaster,subjectMaster);
			
			TestTool.getTraceTime("Job List 207");
			///////////////Start show school panel status/////////////// 
			List<SchoolWiseCandidateStatus> lstSchoolWiseCandidateStatus= new ArrayList<SchoolWiseCandidateStatus>();
			if(userMaster!=null)
				if(userMaster.getEntityType()==3){	
					lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusListHistoryForSchoolByJob(teacherDetail,lstJobOrder,userMaster.getSchoolId());
				}else if(userMaster.getEntityType()==2){
					lstSchoolWiseCandidateStatus=schoolWiseCandidateStatusDAO.findByTeacherStatusListHistoryForUser(teacherDetail, lstJobOrder,userMaster);
				}
			if(lstSchoolWiseCandidateStatus.size()>0)
				for (SchoolWiseCandidateStatus schoolWiseCandidateStatus : lstSchoolWiseCandidateStatus) {
					swcsMap.put(schoolWiseCandidateStatus.getJobOrder().getJobId(),schoolWiseCandidateStatus.getStatusMaster());
				}
			TestTool.getTraceTime("Job List 208");
			///////////////End show school panel status///////////////			
			SortedMap<String,JobOrder>	sortedMap = new TreeMap<String,JobOrder>();
			List<JobOrder> lstJob=new ArrayList<JobOrder>();

			if(sortOrderNoField.equals("JobType")){
				sortOrderFieldName="JobType";
			}
			if(sortOrderNoField.equals("jobCategoryId")){
				sortOrderFieldName="jobCategoryId";
			}
			if(sortOrderNoField.equals("dateApplied")){
				sortOrderFieldName="dateApplied";
			}
			if(sortOrderNoField.equals("applicationStatus")){
				sortOrderFieldName="applicationStatus";
			}
			if(sortOrderNoField.equals("fitScore")){
				sortOrderFieldName="fitScore";
			}
			if(sortOrderNoField.equals("demoClass")){
				sortOrderFieldName="demoClass";
			}
			if(sortOrderNoField.equals("subjectId"))
			{
				sortOrderFieldName	="subjectId";
			}
			if(sortOrderNoField.equals("geoZoneName"))
			{
				sortOrderFieldName	=	"geoZoneName";
			}			
			List<DistrictMaster> districtMasterList= new ArrayList<DistrictMaster>();
			int mapFlag=2;
			try{
				
				TestTool.getTraceTime("Job List 250");
				
				for (JobOrder jobOrder : lstJobOrder){
					
					districtMasterList.add(jobOrder.getDistrictMaster());
					String orderFieldName=jobOrder.getCreatedDateTime()+"||";
					if(sortOrder!=null){
						if(sortOrderFieldName.equals("JobType")){
							deafultFlag=false;
							String jobType="";
							 if(jobOrder.getCreatedForEntity()==3){
								jobType="SJO";
							}
							else
							{
								jobType = "DJO";
							}
							logger.info(" jobType ::: "+jobType);
							orderFieldName=jobType+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}if(sortOrderFieldName.equals("jobCategoryId")){
							deafultFlag=false;
							orderFieldName=jobOrder.getJobCategoryMaster().getJobCategoryName()+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
						
						if(jobOrder.getSubjectMaster()!=null){
							orderFieldName=jobOrder.getSubjectMaster().getSubjectName();
							if(sortOrderFieldName.equals("subjectId")){
								orderFieldName=jobOrder.getSubjectMaster().getSubjectName()+"||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}else{
							if(sortOrderFieldName.equals("subjectId")){
								orderFieldName="0||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}						
						if(sortOrderFieldName.equals("dateApplied")){
							deafultFlag=false;
							//orderFieldName=getDateApplied(jobOrder.getJobId(),jft,teacherDetail)+jobOrder.getJobId();;
							JobForTeacher jftObj=jftList.get(jobOrder.getJobId());
							if(teacherDetail.equals(jftObj.getTeacherId())){
								orderFieldName=Utility.convertDateAndTimeToUSformatOnlyDate(jftObj.getCreatedDateTime())+jobOrder.getJobId();
							}
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}if(sortOrderFieldName.equals("applicationStatus")){
							deafultFlag=false;
							JobForTeacher jobFTObj=jftList.get(jobOrder.getJobId());
							status =jobFTObj.getStatusMaster();
							
							if(swcsMap.get(jobOrder.getJobId())!=null){
								status=swcsMap.get(jobOrder.getJobId());
							}
							String statusVal="";
							if(status!=null){	
								if(status.getStatusShortName().equalsIgnoreCase("comp")){
									statusVal=lblCompleted;
								}else if(status.getStatusShortName().equalsIgnoreCase("icomp")){
									statusVal=lblIncomplete;
								}else if(status.getStatusShortName().equalsIgnoreCase("rem")){
									statusVal=status.getStatus();
								}else if(status.getStatusShortName().equalsIgnoreCase("hird")){
									statusVal=lblHired;
								}else if(status.getStatusShortName().equalsIgnoreCase("vlt")){
									statusVal=lblTimedOut;
								}else if(status.getStatusShortName().equalsIgnoreCase("widrw")){
									statusVal=lblWithdrew; 
								}else if(status.getStatusShortName().equalsIgnoreCase("cghide")){
									statusVal="Hidden"; 
								}
							}else {
								if(jobFTObj.getSecondaryStatus()!=null){
									statusVal=jobFTObj.getSecondaryStatus().getSecondaryStatusName();
								}
							}
							
							orderFieldName=statusVal+jobOrder.getJobId();
							sortedMap.put("||"+orderFieldName, jobOrder);
							if(sortOrderTypeVal.equals("0")){
								mapFlag=0;
							}else{
								mapFlag=1;
							}
						}
						
						
						if(jobOrder.getGeoZoneMaster()!=null){
							orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName();
							if(sortOrderFieldName.equals("geoZoneName")){
								orderFieldName=jobOrder.getGeoZoneMaster().getGeoZoneName()+"||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}else{
							if(sortOrderFieldName.equals("geoZoneName")){
								orderFieldName="0||"+jobOrder.getJobId();
								sortedMap.put(orderFieldName+"||",jobOrder);
								if(sortOrderTypeVal.equals("0")){
									mapFlag=0;
								}else{
									mapFlag=1;
								}
							}
						}
					}
				}
				TestTool.getTraceTime("Job List 251");
				if(mapFlag==1){
					NavigableSet<String> navig = ((TreeMap)sortedMap ).descendingKeySet();  
					for (Iterator iter=navig.iterator();iter.hasNext();) {  
						Object key = iter.next(); 
						lstJob.add((JobOrder) sortedMap.get(key));
					} 
				}else if(mapFlag==0){
					Iterator iterator = sortedMap.keySet().iterator();
					
					while (iterator.hasNext()) {
						Object key = iterator.next();
						lstJob.add((JobOrder) sortedMap.get(key));
					}
				}else{
					
					lstJob=lstJobOrder;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			TestTool.getTraceTime("Job List 252");
			List<JobOrder> listjobOrdersgeoZone = new ArrayList<JobOrder>();
			 if(geoZoneId>0)
				{
					GeoZoneMaster geoZoneMaster=geoZoneMasterDAO.findById(geoZoneId, false, false);
					if(userMaster.getRoleId().getRoleId().equals(3))
						listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZoneRole(geoZoneMaster,userMaster);
					else
						listjobOrdersgeoZone = jobOrderDAO.getJobOrderBygeoZone(geoZoneMaster);
				}
			 TestTool.getTraceTime("Job List 253");
			  if(listjobOrdersgeoZone!=null && listjobOrdersgeoZone.size()>0)
			  {
				  if(deafultFlag==true){
					  lstJobOrder.retainAll(listjobOrdersgeoZone);
					}else{
						lstJobOrder.addAll(listjobOrdersgeoZone);
					}
	
			  }	
			
			  if(listjobOrdersgeoZone.size()==0 && geoZoneId==-1 ||geoZoneId==0)
			  {
				  lstJobOrder.addAll(listjobOrdersgeoZone);
			  
			  }
			  else if(listjobOrdersgeoZone.size()==0 &&geoZoneId!=-1)
			  {
				  lstJobOrder.retainAll(listjobOrdersgeoZone);
			  }
			  
			totalRecord =lstJob.size();

			if(totalRecord<end)
				end=totalRecord;

			Map<Integer,String> scoremap = new HashMap<Integer, String>();
			Map<Integer,String> demoClassmap = new HashMap<Integer, String>();
			if(fitScore)
			{
				scoremap =  jobWiseConsolidatedTeacherScoreDAO.getFitScoreTeacherWise(teacherDetail,lstJob);
				String fit = "0";
				for (JobOrder jobOrder2 : lstJob) {
					fit = scoremap.get(jobOrder2.getJobId());
					if(fit==null)
						jobOrder2.setFitScore(0);
					else
						jobOrder2.setFitScore(Integer.parseInt(fit.split("/")[0]));
				}
				if(sortOrderTypeVal.equalsIgnoreCase("1") && sortOrderNoField.equals("fitScore"))
					Collections.sort(lstJob,JobOrder.jobOrderComparatorFitScoreDesc );
				else if(sortOrderTypeVal.equalsIgnoreCase("0") && sortOrderNoField.equals("fitScore"))
					Collections.sort(lstJob,JobOrder.jobOrderComparatorFitScoreAsc );
			}
			TestTool.getTraceTime("Job List 254");
			if(demoClass)
			{
				demoClassmap = demoClassScheduleDAO.findDemoClassSchedulesTeacherWise(teacherDetail,lstJob);
				String demo = "";
				for (JobOrder jobOrder2 : lstJob) {
					demo = demoClassmap.get(jobOrder2.getJobId());
					if(demo==null)
						jobOrder2.setDemoClass("N/A");
					else
						jobOrder2.setDemoClass(demo);
				}
				if(sortOrderTypeVal.equalsIgnoreCase("1") && sortOrderNoField.equals("demoClass"))
					Collections.sort(lstJob,JobOrder.jobOrderComparatorDemoDesc );
				else if(sortOrderTypeVal.equalsIgnoreCase("0") && sortOrderNoField.equals("demoClass"))
					Collections.sort(lstJob,JobOrder.jobOrderComparatorDemoAsc );
			}
			TestTool.getTraceTime("Job List 255");

			List<JobOrder> jobLst=new ArrayList<JobOrder>();
			if(lstJob!=null)
			jobLst=lstJob.subList(start,end);
			//List<SecondaryStatus> lstSecondaryStatus =	secondaryStatusDAO.findSecondaryStatusWithDistrictList(districtMasterList);
			//logger.info("lstSecondaryStatus.size() "+lstSecondaryStatus.size());
			TestTool.getTraceTime("Job List 255.1");
			Map<Integer, Integer> mapAssess = new HashMap<Integer, Integer>();
			Map<Integer, TeacherAssessmentStatus> mapJSI = new HashMap<Integer, TeacherAssessmentStatus>();
			List<TeacherAssessmentStatus> lstJSI =new ArrayList<TeacherAssessmentStatus>();
			logger.info("jobLst::"+jobLst.size());
			List<AssessmentJobRelation> assessmentJobRelations1=new ArrayList<AssessmentJobRelation>();
			TestTool.getTraceTime("Job List 255.3");
			List<AssessmentDetail> adList=new ArrayList<AssessmentDetail>();
			//assessmentJobRelations1=assessmentJobRelationDAO.findRelationByJobOrder(jobLst);
			if(assessmentJobRelations1.size()>0){
				for(AssessmentJobRelation ajr: assessmentJobRelations1){
					mapAssess.put(ajr.getJobId().getJobId(),ajr.getAssessmentId().getAssessmentId());
					adList.add(ajr.getAssessmentId());
				}
				if(adList.size()>0)
					lstJSI = teacherAssessmentStatusDAO.findJSITakenByAssessMentList(teacherDetail,adList);
				
				TestTool.getTraceTime("Job List 255.4");
			}
			TestTool.getTraceTime("Job List 256");
			for(TeacherAssessmentStatus ts : lstJSI){
				mapJSI.put(ts.getAssessmentDetail().getAssessmentId(), ts);
			}
			int[] headerwidth;
			
			if(entityID==1)
				headerwidth = new int[] {40,55,205,70,110,120,80,100,65,70,110};
			else if(fitScore && demoClass)
				headerwidth = new int[] {40,55,205,70,110,130,80,100,65,70,110}; // fitScore and demoClass
			else if(!fitScore && !demoClass)
				headerwidth = new int[] {40,55,310,70,110,130,80,110,0,0,120}; // ! fitScore and ! demoClass
			else{
				//headerwidth = new int[] {40,55,250,110,130,80,100,80,110}; // else
				headerwidth = new int[] {40,55,205,70,110,130,80,100,65,70,110};
			}						
			String responseText="";
			String tablePadding_Data="";
			if(visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))
			{
				tablePadding_Data="padding-left:10px;vertical-align:top;text-align:left;font-weight:normal;line-height:30px;";

				String tablePadding="style='padding-left:10px;vertical-align:top;color:#FFFFFF;line-height:25px;'";
				sb.append("<table id='jobTable' border='0'  width='900' class='table-bordered' onmouseover=\"javascript:setGridProfileVariable('job')\">");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");

				responseText=PaginationAndSorting.responseSortingLink(lblId,sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[0]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblType,sortOrderNoField,"JobType",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[1]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblJobTitle,sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[2]+"' "+tablePadding+" >"+responseText+"</th>");
				
				responseText=PaginationAndSorting.responseSortingLink(lblZone,sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[3]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblSubject,sortOrderFieldName,"subjectId",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[4]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblCategory,sortOrderNoField,"jobCategoryId",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[5]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblDateApplied,sortOrderNoField,"dateApplied",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[6]+"' "+tablePadding+" >"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblApplication+"<br>"+lblStatus+"",sortOrderNoField,"applicationStatus",sortOrderTypeVal,pgNo);
				sb.append("<th align=\"left\" width='"+headerwidth[7]+"' "+tablePadding+" >"+responseText+"</th>");

				if(fitScore)
				{
					responseText=PaginationAndSorting.responseSortingLink(lblFitScore,sortOrderNoField,"fitScore",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='"+headerwidth[8]+"' "+tablePadding+" >"+responseText+"</th>");
				}
				if(demoClass)
				{
					responseText=PaginationAndSorting.responseSortingLink(lblDemoLesson,sortOrderNoField,"demoClass",sortOrderTypeVal,pgNo);
					sb.append("<th align=\"left\" width='"+headerwidth[9]+"' "+tablePadding+" >"+responseText+"</th>");
				}

				sb.append("<th align=\"left\" width='"+headerwidth[10]+"' "+tablePadding+" >"+lblDetails+"</th>");

			}
			else
			{
				tablePadding_Data="";
				sb.append("<table id='jobTable' width='100%' border='0' class='table table-striped'>");
				sb.append("<thead class='bg'>");
				sb.append("<tr>");

				responseText=PaginationAndSorting.responseSortingLink(lblId,sortOrderFieldName,"jobId",sortOrderTypeVal,pgNo);
				sb.append("<th width='50px' valign='top'>"+responseText+"</th>");

				//responseText=PaginationAndSorting.responseSortingLink("Job Type",sortOrderNoField,"JobType",sortOrderTypeVal,pgNo);
				responseText=PaginationAndSorting.responseSortingLink(lblType,sortOrderNoField,"JobType",sortOrderTypeVal,pgNo);
				sb.append("<th width='60px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblJobTitle,sortOrderFieldName,"jobTitle",sortOrderTypeVal,pgNo);
				sb.append("<th width='140px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblZone,sortOrderFieldName,"geoZoneName",sortOrderTypeVal,pgNo);
				sb.append("<th width='70px' valign='top'>"+responseText+"</th>");
			
				responseText=PaginationAndSorting.responseSortingLink(lblSubject,sortOrderFieldName,"subjectId",sortOrderTypeVal,pgNo);
				sb.append("<th width='90px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblCategory,sortOrderNoField,"jobCategoryId",sortOrderTypeVal,pgNo);
				sb.append("<th width='110px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblAppliedOn,sortOrderNoField,"dateApplied",sortOrderTypeVal,pgNo);
				sb.append("<th width='80px' valign='top'>"+responseText+"</th>");

				responseText=PaginationAndSorting.responseSortingLink(lblApplication+"<br>"+lblStatus+"",sortOrderNoField,"applicationStatus",sortOrderTypeVal,pgNo);
				sb.append("<th width='110px' valign='top'>"+responseText+"</th>");

				if(fitScore)
				{
					//sb.append("<th width='60px' valign='top'>Fit Score</th>");
					responseText=PaginationAndSorting.responseSortingLink(lblFitScore,sortOrderNoField,"fitScore",sortOrderTypeVal,pgNo);
					sb.append("<th width='60px' valign='top'>"+responseText+"</th>");
				}
				if(demoClass)
				{
					//sb.append("<th width='60px' valign='top'>Demo Class</th>");
					responseText=PaginationAndSorting.responseSortingLink(lblDemoLesson,sortOrderNoField,"demoClass",sortOrderTypeVal,pgNo);
					sb.append("<th width='80px' valign='top'>"+responseText+"</th>");
				}

				sb.append("<th width='100px;' valign='top'>");
				sb.append(lblDetails);
				sb.append("</th>");	
			}

			sb.append("</tr>");
			sb.append("</thead>");

			TeacherAssessmentStatus teacherAssessmentStatusJSI = null;
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			String fitScore1 = "";
			
			boolean isDisplayCheckBox=false;
			if((jobCategoryMaster!=null || jobLst.size()==1) && entityID==2 && visitLocation.equalsIgnoreCase("TeacherPool"))
			{
				isDisplayCheckBox=true;
			}
			sb.append("<input type='hidden' id='txtJobLstNo' value='"+jobLst.size()+"'>");
			
			int iRecordCount=1;
			CandidateGridService cgService= new CandidateGridService();
			boolean schoolFlag=false;
			//int iDisplayCount=0;
			
			boolean bChangeStatusFromTP=false;
			if(visitLocation.equalsIgnoreCase("TeacherPool"))
				bChangeStatusFromTP=true;
			TestTool.getTraceTime("Job List 257");
			boolean bLoggedInSchool=false;
			boolean bSchoolPrivilege=false;
			StatusMaster statusMasterSP=null;
			SecondaryStatus secondaryStatusSP=null;
			Map<Integer, Integer> mapTSHFJ=new HashMap<Integer, Integer>(); 
			if(entityID==3 && (roleId==3 || roleId==6))
			{
				
				bLoggedInSchool=true;
				if(districtMaster!=null)
				{
					if(districtMaster.getStatusMaster()==null && districtMaster.getSecondaryStatus()==null)
						bSchoolPrivilege=false;
					else
					{
						bSchoolPrivilege=true;
						if(districtMaster.getStatusMaster()!=null && districtMaster.getSecondaryStatus()==null)
							statusMasterSP=districtMaster.getStatusMaster();
						else if(districtMaster.getStatusMaster()==null && districtMaster.getSecondaryStatus()!=null)
							secondaryStatusSP=districtMaster.getSecondaryStatus();
						List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob =new ArrayList<TeacherStatusHistoryForJob>();
						lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findHistory(teacherDetail, jobLst, statusMasterSP, secondaryStatusSP);
						if(lstTeacherStatusHistoryForJob!=null && lstTeacherStatusHistoryForJob.size() >0)
							for(TeacherStatusHistoryForJob forJob:lstTeacherStatusHistoryForJob)
								if(forJob!=null)
									mapTSHFJ.put(forJob.getJobOrder().getJobId(), 0);
					}
				}
				
			}
			TestTool.getTraceTime("Job List 258");
			///////////////////////Add Hat for Job List ( Sekhar )//////////////////////////////////////////
			List<StatusMaster> statusMasters=statusMasterDAO.findStatusByStatusByShortNames(statusShrotName);
			Map<String,StatusMaster> statusMap=new HashMap<String, StatusMaster>();
			for (StatusMaster statusMasterObj : statusMasters) {
				statusMap.put(statusMasterObj.getStatusShortName(),statusMasterObj);
			}
			TestTool.getTraceTime("Job List 259");
			List<TeacherDetail>lstTeacherDetails =new ArrayList<TeacherDetail>();
			lstTeacherDetails.add(teacherDetail);
			Map<Integer,List<TeacherStatusHistoryForJob>> mapForHiredTeachers = new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			//List<TeacherStatusHistoryForJob> historyForJobList = teacherStatusHistoryForJobDAO.findHireTeachersWithDistrictListAndTeacherList(lstTeacherDetails,districtMaster);
			//List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherForOffers(lstTeacherDetails);
			List<TeacherStatusHistoryForJob> historyForJobList = teacherStatusHistoryForJobDAO.findHireAndVcompTeacherList(lstTeacherDetails,districtMaster,statusMap.get("hird"),statusMap.get("vcomp"));
			List<JobForTeacher> jftOffers=jobForTeacherDAO.findJobByTeacherListForOffers(lstTeacherDetails,districtMaster,statusMap.get("hird"),statusMap.get("vcomp"));
			TestTool.getTraceTime("Job List 260");
			List<String> posList=new ArrayList<String>();
			Map<String,String> reqMap=new HashMap<String, String>();
			Map<String,List<SchoolMaster>> hiredSchol=new HashMap<String,List<SchoolMaster>>();
			TestTool.getTraceTime("Job List 261");
			for (JobForTeacher jobForTeacher : jftOffers){
			try{

					if(jobForTeacher.getRequisitionNumber()!=null){	
						if(jobForTeacher.getSchoolMaster()!=null){	
							String teacherIdAndJobId=jobForTeacher.getTeacherId().getTeacherId()+"#"+jobForTeacher.getJobId().getJobId();	
							List<SchoolMaster> allList=hiredSchol.get(teacherIdAndJobId);	
							if(hiredSchol.get(teacherIdAndJobId)!=null){	
							allList.add(jobForTeacher.getSchoolMaster());	
							hiredSchol.put(teacherIdAndJobId,allList);	
							}else{	
							List<SchoolMaster> tList=new ArrayList<SchoolMaster>();	
							tList.add(jobForTeacher.getSchoolMaster());	
							hiredSchol.put(teacherIdAndJobId,tList);	
							}	
						}			
						posList.add(jobForTeacher.getRequisitionNumber());			
						reqMap.put(jobForTeacher.getJobId().getJobId()+"#"+jobForTeacher.getTeacherId().getTeacherId(),jobForTeacher.getRequisitionNumber());		
					}
			}catch(Exception e){

			e.printStackTrace();

			}

			}
			TestTool.getTraceTime("Job List 262");
			List<DistrictRequisitionNumbers> districtRequisitionNumbers=districtRequisitionNumbersDAO.getDistrictRequisitionNumbers(districtMaster,posList);
			Map<String,DistrictRequisitionNumbers> dReqNoMap = new HashMap<String,DistrictRequisitionNumbers>();
			try{
				for (DistrictRequisitionNumbers districtRequisitionNumbers2 : districtRequisitionNumbers) {
					dReqNoMap.put(districtRequisitionNumbers2.getRequisitionNumber(),districtRequisitionNumbers2);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			TestTool.getTraceTime("Job List 263");
			////////////////////////////////End Hat for Job List ( Sekhar )////////////////////////////////////
			for(JobOrder jobOrder:jobLst)
			{
				TestTool.getTraceTime("Job List 263.1");
				if(historyForJobList.size()>0){
					mapForHiredTeachers=cgService.getHiredList(reqMap,dReqNoMap,lstTeacherDetails,historyForJobList,jobOrder);
				}
				String sDidplayStatusName="";
				JobForTeacher jobFTObj=jftList.get(jobOrder.getJobId());	
				status =jobFTObj.getStatusMaster();
				if(swcsMap.get(jobOrder.getJobId())!=null){
					status=swcsMap.get(jobOrder.getJobId());
				}
				if(status!=null)
				{
					if(status.getStatusShortName().equalsIgnoreCase("comp"))
						sDidplayStatusName=lblAvailable; 
					else if(status.getStatusShortName().equalsIgnoreCase("rem") || status.getStatusShortName().equalsIgnoreCase("icomp") || status.getStatusShortName().equalsIgnoreCase("vlt"))
						sDidplayStatusName=status.getStatus(); 
					else if(status.getStatusShortName().equalsIgnoreCase("scomp") || status.getStatusShortName().equalsIgnoreCase("ecomp") || status.getStatusShortName().equalsIgnoreCase("vcomp"))
						sDidplayStatusName=cgService.getSecondaryStatusNameByDistrictAndJobCategory(lstSecondaryStatus, status,jobOrder); //sb.append(cgService.getSecondaryStatusNameByDistrictAndJobCategory(lstSecondaryStatus, status,jobOrder));
					else if(status.getStatusShortName().equalsIgnoreCase("dcln"))
						sDidplayStatusName=lblDeclined; 
					else if(status.getStatusShortName().equalsIgnoreCase("widrw"))
						sDidplayStatusName=lblWithdrew; 
					else if(status.getStatusShortName().equalsIgnoreCase("cghide"))
						sDidplayStatusName="Hidden"; 
					else if(status.getStatusShortName().equalsIgnoreCase("hird"))
						if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1){
							sDidplayStatusName="Completed Onboarding Process";
						}else{
							sDidplayStatusName=lblHired;
						}
				}
				else
				{
					if(jobFTObj.getSecondaryStatus()!=null)
						sDidplayStatusName=jobFTObj.getSecondaryStatus().getSecondaryStatusName(); //sb.append(jobFTObj.getSecondaryStatus().getSecondaryStatusName());
				}
				boolean bDisplayRow=true;
				
				
				if(bDisplayRow)
				{
					if(schoolMaster!=null)
					{
						if(schoolJobMap.get(jobOrder.getJobId())==null && (jobOrder.getWritePrivilegeToSchool()==null || !jobOrder.getWritePrivilegeToSchool())  )
							schoolFlag=true;
						else
							schoolFlag=false;
					}

					iRecordCount++;
					String gridColor="";
					if(iRecordCount%2==0 && ( visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
						gridColor="class='bggrid'";
					}

					teacherAssessmentStatusJSI = null;
					noOfRecordCheck++;
					sb.append("<tr "+gridColor+">");

					String jobType="DJO",jobTitle="",jobId="";
					if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getBranchMaster()==null){
						jobType="HJO";
					}
					else if(jobOrder.getHeadQuarterMaster()!=null && jobOrder.getBranchMaster()!=null){
						jobType="BJO";
					}
					if(jobOrder.getCreatedForEntity()==3){
						jobType="SJO";

						if(schoolFlag)
						{	
							jobTitle=jobOrder.getJobTitle();
							jobId=""+jobOrder.getJobId();
						}else
						{
							jobTitle="<a target='blank' data-original-title='"+lblClickToViewCG+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=3'>"+jobOrder.getJobTitle()+"</a>";
							if(jobOrder.getHeadQuarterMaster()!=null)
								jobId="<a target='blank'  data-original-title='"+lblClickToViewJO+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"'  href='hqbrjoborder.do?jobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
							else
								jobId="<a target='blank'  data-original-title='"+lblClickToViewJO+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"'  href='editjoborder.do?JobOrderType=3&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
						}

					}else{
						
						logger.info("schoolFlag:- "+schoolFlag+", jobOrder.getIsPoolJob():- "+jobOrder.getIsPoolJob());
						if(schoolFlag)
						{	
							jobTitle=jobOrder.getJobTitle();
							jobId=""+jobOrder.getJobId();
						}else
						{
							if(jobOrder.getHeadQuarterMaster()!=null){
								jobTitle="<a target='blank' data-original-title='"+lblClickToViewCG+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=5'>"+jobOrder.getJobTitle()+"</a>";
								jobId="<a target='blank' data-original-title='"+lblClickToViewJO+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"' href='hqbrjoborder.do?jobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
							}
							else{
								jobTitle="<a target='blank' data-original-title='"+lblClickToViewCG+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=2'>"+jobOrder.getJobTitle()+"</a>";
							jobId="<a target='blank' data-original-title='"+lblClickToViewJO+"' rel='tooltip'  id='jobJobId"+noOfRecordCheck+"' href='editjoborder.do?JobOrderType=2&JobId="+jobOrder.getJobId()+"'>"+jobOrder.getJobId()+"</a>";
							}
						}
					}
					
					int iJobId=jobOrder.getJobId();
					String sCheckBox="<input type=\"checkbox\" name='chkjobId_msu' id='chkjobId_msu"+iJobId+"' value='"+iJobId+"'>&nbsp;";
					if(isDisplayCheckBox)
						jobId="<div>"+sCheckBox+"</div><div style='margin-left:20px;margin-top:-28px;'>"+jobId+"</div>";
					
					sb.append("<td style='text-align: left; vertical-align: middle;width:50px;"+tablePadding_Data+"'>"+jobId+"</td>");

					sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'>"+jobType+"</td>");
					if(jobOrder.getIsPoolJob()==2)
					{
						//jobTitle="<a target='blank' data-original-title='"+lblClickToViewCG+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=2'>"+jobOrder.getJobTitle()+"</a>";
						sb.append("<td style='text-align: left; vertical-align: middle;width:150px;'><span class='requiredlarge'>*</span>"+jobTitle+"</td>");						
					}
					else
					{	//jobTitle="<a target='blank' data-original-title='"+lblClickToViewCG+"' rel='tooltip'  id='jobJobTitle"+noOfRecordCheck+"' href='candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType=2'>"+jobOrder.getJobTitle()+"</a>";
						sb.append("<td style='text-align: left; vertical-align: middle;width:150px;'>"+jobTitle+"</td>");
					}	
					String zone = "";
					if(jobOrder.getGeoZoneMaster()!=null)
					{
						if(jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==4218990)
						{
							sb.append("<td><a href='javascript:void(0);' onclick=\"showPhiladelphiaPdf();\" return false;>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}
						else if(jobOrder.getGeoZoneMaster().getDistrictMaster().getDistrictId()==1200390){
							//sb.append("<td><a href='javascript:void(0);' onclick=\"showMiamiPdf();\" return false;>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
						}else{
							if(jobOrder.getHeadQuarterMaster()!=null){
								sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'><a href='http://jobs.dadeschools.net/teachers/pdf/Geo_Zones_With_Schools.pdf' target='_blank'>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}else{
								sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'><a href='javascript:void(0);' onclick=\"return showZoneSchoolList("+jobOrder.getGeoZoneMaster().getGeoZoneId()+",'"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"',"+jobOrder.getDistrictMaster().getDistrictId()+")\";>"+jobOrder.getGeoZoneMaster().getGeoZoneName()+"</a></td>");
							}
						}
					}
					else
					{
						sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'>");
						sb.append(zone);
						sb.append("</td>");
					}					
					if(jobOrder.getSubjectMaster()!=null)
						sb.append("<td style='text-align: left; vertical-align: middle;width:90px;"+tablePadding_Data+"'>"+jobOrder.getSubjectMaster().getSubjectName()+"</td>");
					else
						sb.append("<td style='text-align: left; vertical-align: middle;width:90px;"+tablePadding_Data+"'>&nbsp;</td>");

					sb.append("<td style='text-align: left; vertical-align: middle;width:110px;"+tablePadding_Data+"'>"+jobOrder.getJobCategoryMaster().getJobCategoryName()+"</td>");
					JobForTeacher jftOb=jftList.get(jobOrder.getJobId());
					String dateVal="";
					if(teacherDetail.equals(jftOb.getTeacherId())){
						dateVal=Utility.convertDateAndTimeToUSformatOnlyDate(jftOb.getCreatedDateTime());
					}
					sb.append("<td style='text-align: left; vertical-align: middle;width:80px;"+tablePadding_Data+"' nowrap>"+dateVal+"</td>");
					sb.append("<td style='text-align: left; vertical-align: middle;width:110px;"+tablePadding_Data+"'>");
					boolean isWithdrew=true;
					try{
						if(sDidplayStatusName.equals("Withdrew") && jobOrder.getHeadQuarterMaster()!=null && jobOrder.getHeadQuarterMaster().getHeadQuarterId()==1){
							isWithdrew=true;
						}else if(sDidplayStatusName.equals("Withdrew")){
							isWithdrew=false;
						}else if(sDidplayStatusName.equals("Hidden")){
							isWithdrew=false;
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					
					if(( (mapTSHFJ!=null && mapTSHFJ.get(jobOrder.getJobId())!=null) || (!bLoggedInSchool || !bSchoolPrivilege)) && bChangeStatusFromTP && isWithdrew)
					{
						sb.append("<a href='javascript:void(0);'  onclick=\"getJobListStatusChange('"+teacherId+"','"+jobOrder.getJobId()+"');\">");
						sb.append(sDidplayStatusName);
						sb.append("</a>");
					}
					else
						sb.append(sDidplayStatusName);
				
					if((isNoble|| isFlutron) && visitLocation.equalsIgnoreCase("TeacherPool"))
	    			  	sb.append("&nbsp;<a data-html='true' data-original-title='"+lblChangeStatus+"' rel='tooltip' id='hireTp"+noOfRecordCheck+"' onclick=\"changeJobCanidateStatus('"+jobFTObj.getTeacherId().getTeacherId()+"','"+jobFTObj.getJobId().getJobId()+"','"+sDidplayStatusName+"');\"><i class='icon-instagram icon-large'></i></a>");

					/////////////////////////Add Hat for Job ( Sekhar )//////////////////////
					int fullAndPartTime=1;
					try{
						String reqNo=reqMap.get(jobOrder.getJobId()+"#"+teacherDetail.getTeacherId());
						if(reqNo!=null){
							DistrictRequisitionNumbers drnObj=dReqNoMap.get(reqNo);
							if(drnObj!=null && drnObj.getPosType()!=null){
								if(drnObj.getPosType().equalsIgnoreCase("P")){
									fullAndPartTime=2;
								}
							}
						}
					}catch(Exception e){
						e.printStackTrace();
					}
					try{
						if(mapForHiredTeachers.get(teacherDetail.getTeacherId())!=null && mapForHiredTeachers.get(teacherDetail.getTeacherId()).size()>0)
						{
							String offerText="Offer Accepted";
							try{
								offerText=cgService.getSecondaryStatusNameByDistrictAndJobCategory(lstSecondaryStatus,statusMap.get("vcomp"),jobOrder);
							}catch(Exception e){
								e.printStackTrace();
							}
							if(fullAndPartTime==2){
								boolean offerAccepted=cgService.displayOfferAccepted(mapForHiredTeachers,teacherDetail.getTeacherId(),hiredSchol);
								if(offerAccepted){
									sb.append("&nbsp;<a data-html='true' data-original-title=\""+cgService.displayHiredVcompTeacherListToolTip(mapForHiredTeachers,teacherDetail.getTeacherId(),hiredSchol,offerText).replaceAll("\"","'")+"\" rel='tooltip' id='hireTp"+noOfRecordCheck+"'><span class='fa-list-alt icon-large iconcolor' style='color:#555555;'></span></a>");
								}else{
									sb.append("&nbsp;<a data-html='true' data-original-title=\""+cgService.displayHiredVcompTeacherListToolTip(mapForHiredTeachers,teacherDetail.getTeacherId(),hiredSchol,offerText).replaceAll("\"","'")+"\" rel='tooltip' id='hireTp"+noOfRecordCheck+"'><span class='fa-graduation-cap icon-large iconcolor' style='color:#555555;'></span></a>");
								}
							}else{
								boolean offerAccepted=cgService.displayOfferAccepted(mapForHiredTeachers,teacherDetail.getTeacherId(),hiredSchol);
								if(offerAccepted){
									sb.append("&nbsp;<a data-html='true' data-original-title=\""+cgService.displayHiredVcompTeacherListToolTip(mapForHiredTeachers,teacherDetail.getTeacherId(),hiredSchol,offerText).replaceAll("\"","'")+"\" rel='tooltip' id='hireTp"+noOfRecordCheck+"'><span class='fa-list-alt icon-large iconcolor'></span></a>");
								}else{
									sb.append("&nbsp;<a data-html='true' data-original-title=\""+cgService.displayHiredVcompTeacherListToolTip(mapForHiredTeachers,teacherDetail.getTeacherId(),hiredSchol,offerText).replaceAll("\"","'")+"\" rel='tooltip' id='hireTp"+noOfRecordCheck+"'><span class='fa-graduation-cap icon-large iconcolor'></span></a>");
								}
							}
						}
					}
					catch(Exception e)
					{
						e.printStackTrace();
					}
					////////////////End Hat for Job ( Sekhar )//////////////
					
					sb.append("</td>");
					if(fitScore)
					{
						fitScore1 = scoremap.get(jobOrder.getJobId());
						sb.append("<td style='text-align: left; vertical-align: middle;width:60px;"+tablePadding_Data+"'>"+(fitScore1==null?"N/A":fitScore1)+"</td>");
					}
					if(demoClass)
					{
						sb.append("<td style='text-align: left; vertical-align: middle;width:80px;"+tablePadding_Data+"'>"+(jobOrder.getDemoClass()==null?"N/A":jobOrder.getDemoClass())+"</td>");
					}

					sb.append("<td style='vertical-align: middle;width:110px;"+tablePadding_Data+"'>");				
					//school checking
					if(!schoolFlag)
					{
						sb.append("&nbsp;");
						if(JSI)
						{
							if(roleAccess.indexOf("|4|")!=-1){
								//teacherAssessmentStatusJSI =getJSIStatus(lstJSI,jobOrder);
								Integer assessmentId=mapAssess.get(jobOrder.getJobId());
								
								teacherAssessmentStatusJSI = mapJSI.get(assessmentId);
								if(teacherAssessmentStatusJSI!=null){
									if(teacherAssessmentStatusJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("icomp")){
										sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblCandidateNotCompletedJSI", locale)+"' rel='tooltip' id='tpJSA"+noOfRecordCheck+"'><img src='images/jsi_hover.png'  width='24px'></a>");
									}
									else if(teacherAssessmentStatusJSI.getStatusMaster().getStatusShortName().equalsIgnoreCase("vlt")){
										sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lbllblCandidateTimedOut", locale)+"' rel='tooltip' id='tpJSA"+noOfRecordCheck+"'><img src='images/jsi_hover.png'  width='24px'></a>");
									}
									else
									{
										sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("lblJSI", locale)+"' rel='tooltip' id='tpJSA"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"generatePilarReport('"+teacherId+"','"+jobOrder.getJobId()+"','tpJSA"+noOfRecordCheck+"');"+windowFunc+"\"><img src='images/jsi.png'  width='24px'></a>");
									}
								}
								else{
									sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("msgNoJSI3", locale)+"' rel='tooltip' id='tpJSA"+noOfRecordCheck+"'><img src='images/jsi_hover.png'  width='24px'></a>");
								}
							}else{
								sb.append("&nbsp;");
							}
						}
						sb.append("&nbsp;");

						if(roleAccess.indexOf("|4|")!=-1  && showCommunication){
							String coverLetter=null;
							JobForTeacher jftObj=jftList.get(jobOrder.getJobId());
							if(jftObj!=null){
								if(jftObj.getCoverLetter()!=null){
									coverLetter=jftObj.getCoverLetter();
								}
							}
							
							if(coverLetter==null || coverLetter.trim().equals("")){
								sb.append("<a data-original-title='"+lblNoCoverLetter+"' rel='tooltip' id='jobCL"+noOfRecordCheck+"'><span class='fa-file-text-o icon-large iconcolorhover'></span></a>");
							}
							else{
								sb.append("<a data-original-title='"+Utility.getLocaleValuePropByKey("headCoverLetr", locale)+"' rel='tooltip' id='jobCL"+noOfRecordCheck+"'href='javascript:void(0);' onclick=\"showCoverLetter('"+teacherId+"','"+jobOrder.getJobId()+"');\"><span class='fa-file-text-o icon-large iconcolor'></span></span></a>");
							}
						}else{
							sb.append("&nbsp;");
						}
						sb.append("&nbsp;");
						sb.append("&nbsp;");
						if(roleAccess.indexOf("|4|")!=-1 && showCommunication){
							int jobforTeacherId=0;
							try{
								jobforTeacherId=Integer.parseInt(jftList.get(jobOrder.getJobId()).getJobForTeacherId()+"");
							}catch(Exception e){}
							sb.append("<a href='javascript:void(0);'data-original-title='"+lblCommunications+"' rel='tooltip'  id='jobCom"+noOfRecordCheck+"' onclick=\"getCommunicationsDiv(0,'"+jobforTeacherId+"','"+teacherDetail.getTeacherId()+"','"+jobOrder.getJobId()+"');\" ><span class='icon-dropbox icon-large iconcolor'></span></a>");
						}else{
							sb.append("&nbsp;");
						}
						sb.append("&nbsp;");
					}
					sb.append("</td>");
					sb.append("</tr>");
				}	
					
				TestTool.getTraceTime("Job List 263.2");				
			}
			TestTool.getTraceTime("Job List 264");
			if(totalRecord>10 && (visitLocation.equalsIgnoreCase("My Folders") || visitLocation.equalsIgnoreCase("Mosaic"))){
				sb.append("<tr><td colspan='13' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
				sb.append(PaginationAndSorting.getPagination_pageSizeVariableName(request,totalRecord,noOfRow, pageNo,"pageSize1","950"));
				sb.append("</td></tr>");
			}
						
			if(jobLst.size()==0)
			{
				sb.append("<tr><td colspan='13' style='border-top: 1px solid #cccccc;padding-bottom:0px;padding-top:5px;line-height:30px;'>");
				sb.append(Utility.getLocaleValuePropByKey("msgNorecordfound", locale));
				sb.append("</td></tr>");
			}
			
			sb.append("</table>");


			if(!visitLocation.equalsIgnoreCase("My Folders") && !visitLocation.equalsIgnoreCase("Mosaic"))
			{
				sb.append(PaginationAndSorting.getPaginationDoubleString(request,totalRecord,noOfRow, pageNo));
			}

		}catch (Exception e){
			e.printStackTrace();
		}
		TestTool.getTraceTime("Job List 265");
		return sb.toString();	
	
	}

	public String getEmailAddressMassTalent(String[] teacher)
	{
		
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		
		List<TeacherDetail> teacherDetailList=new ArrayList<TeacherDetail>();
		Set<Integer> teacherIds=new HashSet<Integer>();
		if(teacher.length>0)
		{
			for(String id:teacher)
			{
				logger.info("id:"+id);
				String[] teacherArray=id.split("-");
				teacherIds.add(Integer.parseInt(teacherArray[0]));
				logger.info("teacher::"+teacherIds);
			}
		
		if(teacherIds.size()>0)
			teacherDetailList=teacherDetailDAO.getActiveKellyTeacherList(teacherIds);
		}
		
		String sEmailAddress="";
		if(teacherDetailList.size()>0)
			for(TeacherDetail teacherDetail:teacherDetailList)
				if(teacherDetail!=null)
					if(teacherDetail.getEmailAddress()!=null && !teacherDetail.getEmailAddress().equalsIgnoreCase(""))
						sEmailAddress=sEmailAddress+teacherDetail.getEmailAddress()+",";
		
		if(!sEmailAddress.equalsIgnoreCase("") && sEmailAddress.length()>1)
			sEmailAddress=sEmailAddress.substring(0, sEmailAddress.length()-1);
		
		String[] sEmailArray=sEmailAddress.split(","); 
		Arrays.sort(sEmailArray);
		
		return Arrays.toString(sEmailArray).replace("[", "").replace("]", "");
	}
	public Boolean SendMessageToAllTalent(String[] teacher,String messageSubject,String messageSendBody,String sSourceFileName,String sSourceFilePath)
	{
		logger.info("hello");
		WebContext context; 
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));		
		}
		
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		String messageSend1=messageSendBody;
		try{
			
			SessionFactory sessionFactory=teacherDetailDAO.getSessionFactory();
			StatelessSession statelesSsessionMassTP = sessionFactory.openStatelessSession();
        	Transaction transactionMassTP =statelesSsessionMassTP.beginTransaction();
        	
			
					
			
       	    String toEmailId="";
			int iTeacherCount=0;
			List<TeacherDetail> teacherDetailList=new ArrayList<TeacherDetail>();
			Set<Integer> teacherIds=new HashSet<Integer>();
			if(teacher.length>0)
			{
								
					for(String id:teacher)
					{
						logger.info("id:"+id);
						String[] teacherArray=id.split("-");
						teacherIds.add(Integer.parseInt(teacherArray[0]));
						logger.info("teacher::"+teacherIds);
					}
				
				if(teacherIds.size()>0)
					teacherDetailList=teacherDetailDAO.getActiveKellyTeacherList(teacherIds);
				
				
				
				for(TeacherDetail teacherDetail:teacherDetailList)
				{
					if(messageSendBody.contains("&lt; Teacher First Name &gt;") || messageSendBody.contains("&lt; Teacher Last Name &gt;")){
    					messageSend1	=	messageSendBody.replaceAll("&lt; Teacher First Name &gt;",teacherDetail.getFirstName());
    					messageSend1	=	messageSend1.replaceAll("&lt; Teacher Last Name &gt;",teacherDetail.getLastName());
    					messageSend1	=	messageSend1.replaceAll("&lt; Teacher Email &gt;",teacherDetail.getEmailAddress());
    				}
					
					transactionMassTP=statelesSsessionMassTP.beginTransaction();
					String sEmailBodyText="";
    				MessageToTeacher  messageToTeacher= new MessageToTeacher();
    				messageToTeacher.setTeacherId(teacherDetail);
    				messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
    				messageToTeacher.setMessageSubject(messageSubject);
    				messageToTeacher.setMessageSend(messageSend1);
    				messageToTeacher.setSenderId(userMaster);
    				messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
    				messageToTeacher.setEntityType(userMaster.getEntityType());
    				messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
    				if(sSourceFileName!=null && !sSourceFileName.equals(""))
    				{
    					messageToTeacher.setMessageFileName(sSourceFileName);
    				}
    					
    					
    				logger.info(messageSend1);
    				sEmailBodyText=MailText.messageForDefaultFont(messageSend1,userMaster);
    				
    				statelesSsessionMassTP.insert(messageToTeacher);
        			transactionMassTP.commit();
        			
        			if(sSourceFileName!=null && !sSourceFileName.equals(""))
    				{
	        			try {
	        				String SourceFilePathTemp=Utility.getValueOfPropByKey("communicationRootPath")+"/message/"+sSourceFilePath+"/";
	        				String sourceFilePath =SourceFilePathTemp+"/"+sSourceFileName;
	    					String targetFilePath=Utility.getValueOfPropByKey("communicationRootPath")+"/message/"+teacherDetail.getTeacherId()+"/";
	    					File sourceFile = new File(sourceFilePath);
	    					File targetDir = new File(targetFilePath);
	    					if(!targetDir.exists())
	    						targetDir.mkdirs();
	    		
	    					File targetFile = new File(targetDir+"/"+sourceFile.getName());
	    					
	    					if(!targetFile.exists())
	    						FileUtils.copyFile(sourceFile, targetFile);
	    					
						} catch (Exception e)
						{
							e.printStackTrace();
						}
    				}
        			
        			
        			toEmailId=teacherDetail.getEmailAddress();
        			String emailFilePath=Utility.getValueOfPropByKey("communicationRootPath")+"/message/"+teacherDetail.getTeacherId()+"/"+sSourceFileName;
    				ScheduleMailThreadWithAttachment sMTWA = new ScheduleMailThreadWithAttachment();
        			sMTWA.setEmailerService(emailerService);        			
        			
        			      			
        			sMTWA.setMailfrom("noreply@teachermatch.net");
        			
        			
        			sMTWA.setMailto(toEmailId);
        			sMTWA.setMailsubject(messageSubject);
        			sMTWA.setMailcontent(sEmailBodyText);
        			sMTWA.setFileName(sSourceFileName);
        			sMTWA.setFilePath(emailFilePath);
					
					try {
						sMTWA.start();	
					} catch (Exception e) {}
					
					
					iTeacherCount++;
					logger.info("Sending email "+iTeacherCount +" of "+teacherDetailList.size());
				}
			}
			
			}catch(Exception e){
			e.printStackTrace();
		}
		return true;
	}
	public String printResume(String teacherIds)
	{
		WebContext context;
		context = WebContextFactory.get();
		UserMaster userMaster;
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String path="";
		userMaster = (UserMaster)session.getAttribute("userMaster");
		
		try 
		{
			if(teacherIds!=null && !teacherIds.equals(""))
			{
				List<String> teacherIdsList =  new ArrayList<String>();
				Set<Integer> teacherId=new HashSet<Integer>();
				
				
				for(String id : teacherIds.split(","))
				{
					try
					{ 
						teacherIdsList.add(id);
					}
					catch(Exception e){}
				}
				for(String id:teacherIdsList)
				{
					logger.info("id:"+id);
					String[] teacherArray=id.split("-");
					teacherId.add(Integer.parseInt(teacherArray[0]));
					logger.info("teacher::"+teacherId);
				}
			
				
				List<TeacherDetail> teacherDetailList = new ArrayList<TeacherDetail>();
				List<TeacherExperience> teacherExperienceList = new ArrayList<TeacherExperience>();
				if(teacherId!=null && teacherId.size()>0)
				{
					teacherDetailList = teacherDetailDAO.findByCriteria(Restrictions.and(Restrictions.in("teacherId", teacherId), Restrictions.eq("status", "A")));
					if(teacherDetailList!=null && teacherDetailList.size()>0)
						teacherExperienceList = teacherExperienceDAO.findByCriteria(Restrictions.in("teacherDetail", teacherDetailList));
				}
				List<String> pdfFilesPath = new ArrayList<String>();
				if(teacherExperienceList.size()>0)
				{
					for(TeacherExperience teacherExperience : teacherExperienceList)
					{
						String source = Utility.getValueOfPropByKey("teacherRootPath")+teacherExperience.getTeacherId().getTeacherId()+"/"+teacherExperience.getResume();
						String target = context.getServletContext().getRealPath("/")+"/"+"/teacher/"+teacherExperience.getTeacherId().getTeacherId()+"/";
						
						String fileExtension = teacherExperience.getResume().substring( teacherExperience.getResume().lastIndexOf(".") );
						String fileNameWithoutExtension = teacherExperience.getResume().substring( 0,teacherExperience.getResume().lastIndexOf(".") );
						
						File targetDir = new File(target);
						if(!targetDir.exists())
							targetDir.mkdirs();
						
						File sourceFile = new File(source);
						File targetFile = new File(targetDir+"/"+fileNameWithoutExtension+".pdf");
						
						if(sourceFile.exists())
						{
							FileUtils.copyFile(sourceFile, targetFile);
						
							if(fileExtension.equalsIgnoreCase(".doc") || fileExtension.equalsIgnoreCase(".docx"))
							{
								if( Utility.convertDocToPDF(sourceFile, targetFile) )
								{
									pdfFilesPath.add(targetFile.toString());
									teacherDetailList.remove(teacherExperience.getTeacherId());
								}
							}
							else
							if( fileExtension.equalsIgnoreCase(".jpg") || fileExtension.equalsIgnoreCase(".jpeg") || fileExtension.equalsIgnoreCase(".png") || fileExtension.equalsIgnoreCase(".gif"))
							{
								if( Utility.convertImageToPDF(sourceFile, targetFile) )
								{
									pdfFilesPath.add(targetFile.toString());
									teacherDetailList.remove(teacherExperience.getTeacherId());
								}
							}
							else
							if(fileExtension.equalsIgnoreCase(".pdf"))
							{
								pdfFilesPath.add(targetFile.toString());
								teacherDetailList.remove(teacherExperience.getTeacherId());
							}
						}
					}
				}
				
				if(pdfFilesPath!=null && pdfFilesPath.size()>0)
				{
					Integer districtId = null;
					if(userMaster.getDistrictId()!=null)
						districtId = userMaster.getDistrictId().getDistrictId();
					
					String fileLocation = context.getServletContext().getRealPath("/")+ File.separator +"district"+ File.separator +districtId+ File.separator + userMaster.getUserId() +".pdf";
					if(Utility.mergePDF(pdfFilesPath, new File(fileLocation)))
						path = Utility.getValueOfPropByKey("contextBasePath")+ File.separator +"district"+ File.separator + districtId +File.separator +userMaster.getUserId()+".pdf";
					
					for(String filePath : pdfFilesPath)
						new File(filePath).delete();
				}
				
				String message="";
				if(teacherDetailList!=null && teacherDetailList.size()>0)
				for(TeacherDetail teacherDetail : teacherDetailList)
				{
					if(message.equals(""))
						message = message + teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
					else
						message = message + ", " +teacherDetail.getFirstName()+" "+teacherDetail.getLastName();
				}
				
				if(!message.equals(""))
					message = "Resume is not uploaded by the "+message+".";
				
				path = message+"##"+path;
			}	
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			path="";
		}
		return path;
	}
	public String callMessageQueueAPI(String[] teacher, Integer callerId, Boolean duplicateCheck)
	{
		GlobalServices.IS_PRINT=false;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		List<TeacherDetail> teacherDetailList=new ArrayList<TeacherDetail>();
		Set<Integer> teacherIds=new HashSet<Integer>();
		List<Long> jftIds=new ArrayList<Long>();
		List<MQEvent> mqEvents = new ArrayList<MQEvent>();
		Map<String,String> mqMap = new HashMap<String, String>();
		String msg="";
		String alreadyLinkToKsn = "true";
		if(teacher.length>0)
		{
			for(String id:teacher)
			{
				String[] teacherArray=id.split("-");
				teacherIds.add(Integer.parseInt(teacherArray[0]));
				jftIds.add(Long.parseLong(teacherArray[1]));
			}
		
		if(teacherIds.size()>0)
			teacherDetailList=teacherDetailDAO.getActiveKellyTeacherList_OP(teacherIds);
		}
		
		if(teacherDetailList.size()>0){
			//System.out.println(teacherDetailList);
			HashSet<TeacherDetail> set = new HashSet<TeacherDetail>(teacherDetailList);
			teacherDetailList.clear();
			teacherDetailList.addAll(set);
			set.clear();
			//System.out.println("after duplicate remove ::: "+teacherDetailList);
		}
		if(callerId==1){
			String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
			List<MQEvent> alreadyLinkedList = mqEventDAO.findExistStatusByTeacherListAndNode_Op(teacherDetailList,LinkToKsn);
			if(alreadyLinkedList!=null && alreadyLinkedList.size()>0){
				msg="1";
			}
			else if(!duplicateCheck){
				/**
				 * API Change for bulk LINK TO KSN - CheckEmailAddress
				 * modified Date: Feb 12, 2016 
				 */
				ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("CheckForTalentEmail");
				if(applitrackDistricts == null){
					System.out.println("applitrackDistricts Object Can Not Be Null");
					//return null;
				}else{
					System.out.println("======================================================================||");  
					CandidateGridService candidateGridService = new CandidateGridService();
					Map<Integer,String> mapExistLinkToKsn=new HashMap<Integer, String>();
					mapExistLinkToKsn=candidateGridService.saveMQEvents(teacherDetailList,userMaster,mapExistLinkToKsn);
					System.out.println("================================ 1 End======================================||");
					
					Map<Integer,MQEvent> mQEventMAP=new HashMap<Integer,MQEvent>();
					List<MQEvent> mqEventList=mqEventDAO.findMQEventByTeacherList_Op(teacherDetailList,LinkToKsn);
					for (MQEvent mqEvent : mqEventList) {
						String prevClour=null;
						if(mapExistLinkToKsn.get(mqEvent.getTeacherdetail().getTeacherId())!=null){
							prevClour=mapExistLinkToKsn.get(mqEvent.getTeacherdetail().getTeacherId());
						}
						candidateGridService.saveStatusNodeColorForLinkToKSN(mqEvent,prevClour);
						mQEventMAP.put(mqEvent.getTeacherdetail().getTeacherId(), mqEvent);
					}
					System.out.println("================================ 2 End======================================");
					msg="8";
					LinkedHashMap<Integer,LinkedList> teacherJFTandSSmap=new LinkedHashMap<Integer,LinkedList>();
					LinkedHashMap<Integer,JobOrder> allTeacherJFTandSSmap=new LinkedHashMap<Integer,JobOrder>();
					List listObj = new ArrayList();
					try {
						if (teacherDetailList != null && teacherDetailList.size() > 0)
							listObj= setJFTandSSagainstTD(jobForTeacherDAO, secondaryStatusDAO,teacherDetailList);
							teacherJFTandSSmap =(LinkedHashMap<Integer, LinkedList>) listObj.get(0);
							allTeacherJFTandSSmap= (LinkedHashMap<Integer, JobOrder>) listObj.get(1);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					Map<TeacherDetail,List<StatusNodeColorHistory>> teacherListByTeacherObj=new HashMap<TeacherDetail,List<StatusNodeColorHistory>>();
		    		List<StatusNodeColorHistory> list =statusNodeColorHistoryDAO.getStatusColorHistoryByTeacherList(teacherDetailList);
		    		for(StatusNodeColorHistory snch:list)
		    			if(teacherListByTeacherObj.get(snch.getTeacherDetail())!=null){
		    				List<StatusNodeColorHistory> snchListForTeacher=teacherListByTeacherObj.get(snch.getTeacherDetail());
		    				snchListForTeacher.add(snch);
		    				teacherListByTeacherObj.put(snch.getTeacherDetail(), snchListForTeacher);
		    			}else{
		    				List<StatusNodeColorHistory> snchListForTeacher=new ArrayList<StatusNodeColorHistory>();
		    				snchListForTeacher.add(snch);
		    				teacherListByTeacherObj.put(snch.getTeacherDetail(), snchListForTeacher);
		    			}
		    		
					//end
					new Thread(new CallSOAPAndRESTServiceThread(teacher, teacherDetailList, userMaster,teacherJFTandSSmap,teacherListByTeacherObj,allTeacherJFTandSSmap,mQEventMAP, request)).start();
				}
			}
		}
		else{
			String msg1 = "";
			try{
				if(callerId==2 || callerId==3){
					if(teacherDetailList!=null && teacherDetailList.size()>0){
						CandidateGridService candidateGridService = new CandidateGridService();
						
						String str = candidateGridService.getNodeColorWithMqEventforFlag(teacherDetailList, callerId);
						
						String str1[] = str.split("##");
						System.out.println(" str1 :: "+str1);
						alreadyLinkToKsn = 	str1[0];
						msg1 = str1[1];
						if(msg1.equalsIgnoreCase("no"))
							msg1 = "";
						System.out.println(" alreadyLinkToKsn :: "+alreadyLinkToKsn+"  msg1 :: "+msg1);
					}
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			
			
			if(!alreadyLinkToKsn.equals("") && alreadyLinkToKsn.equalsIgnoreCase("true"))
			{
				boolean ksnFlag=false;
				boolean wf1Flag=false;
				boolean wf2Flag=false;
				boolean wf3Flag=false;
				boolean ksnFlag1=false;
				boolean wf1Flag1=false;
				boolean wf2Flag1=false;
				boolean wf3Flag1=false;
				String LinkToKsn = Utility.getLocaleValuePropByKey("lblLinkToKsn", locale);
				String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
				String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
				String INVWF3 = Utility.getLocaleValuePropByKey("lblINVWF3", locale);
				List<String> statusList = new ArrayList<String>();
				Map<JobCategoryMaster,JobCategoryMaster> allCategMap = new HashMap<JobCategoryMaster,JobCategoryMaster>();
				Map<String,SecondaryStatus> secStatusMap = new HashMap<String, SecondaryStatus>();
				Map<TeacherDetail,JobForTeacher> jftMap = new HashMap<TeacherDetail, JobForTeacher>();
				StatusMaster statusMaster=null;
				List<SecondaryStatus> secondaryStatuses =null;
				SecondaryStatus secondaryStatus = null;
				try{
					if(LinkToKsn!=null)
						statusList.add(LinkToKsn);
					if(INVWF1!=null)
						statusList.add(INVWF1);
					if(INVWF2!=null)
						statusList.add(INVWF2);
					if(INVWF3!=null)
						statusList.add(INVWF3);

					if(teacherDetailList.size()>0){
						WorkFlowStatus workFlowStatus = workFlowStatusDAO.findById(2, false, false);

						mqEvents = mqEventDAO.findWFStatusByTeacherList(teacherDetailList,workFlowStatus);

						if(mqEvents!=null && mqEvents.size()>0)
						{
							for(MQEvent mqevent : mqEvents){
								mqMap.put(mqevent.getTeacherdetail().getTeacherId()+"-"+mqevent.getEventType(), mqevent.getStatus());
							}
						}

						List<JobForTeacher> jfteacher =jobForTeacherDAO.getJobForTeacherList(jftIds);
						if(jfteacher.size()>0){
							for(JobForTeacher jobForTeacher : jfteacher){
								jftMap.put(jobForTeacher.getTeacherId(), jobForTeacher);
								allCategMap.put(jobForTeacher.getJobId().getJobCategoryMaster(), jobForTeacher.getJobId().getJobCategoryMaster().getParentJobCategoryId());
							}

						}
						List<JobCategoryMaster> allCategory = new ArrayList<JobCategoryMaster>();
						if(allCategMap.size()>0)
						{
							allCategory = new ArrayList<JobCategoryMaster>(allCategMap.values());
						}
						if(allCategory!=null && allCategory.size()>0)
							secondaryStatuses = secondaryStatusDAO.findByNameListAndJobCats(statusList,allCategory);
						if(secondaryStatuses!=null && secondaryStatuses.size()>0){
							for(SecondaryStatus sec : secondaryStatuses){
								if(sec.getJobCategoryMaster()!=null){
									secStatusMap.put(sec.getJobCategoryMaster().getJobCategoryId()+"+"+sec.getSecondaryStatusName(), sec);
									logger.info("KEY::"+sec.getJobCategoryMaster().getJobCategoryId()+"+"+sec.getSecondaryStatusName());
								}
							}
						}
						logger.info("secStatusMap:"+secStatusMap.size()); 

						if(callerId==2){
							if(wf1Flag){    		
								msg="2"; //one or more talent is already completed WF1 Registration
							}else{
								if(ksnFlag1){
									msg="3";//one or more talent is not linked to KSN
								}else if(!duplicateCheck){
									if(jfteacher!=null && jfteacher.size()>0){
										for(JobForTeacher jft : jfteacher){
											if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
												logger.info(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF1);
												if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()!=null){
													secondaryStatus = secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF1);
													if(secondaryStatus!=null)
														logger.info("callerId:"+callerId+"----secondaryStatus:"+secondaryStatus.getSecondaryStatusId());
													break;
												}
											}
										}
									}
									for(TeacherDetail td : teacherDetailList)
									{
										Boolean message=manageStatusAjax.messageQueueForOnboarding(statusMaster,secondaryStatus,jftMap.get(td),userMaster,"","");

									}
									msg="22";
								}
							}

						}
						else if(callerId==3){
							if(wf2Flag){    		
								msg="4"; //one or more talent is already completed WF2 Registration
							}else if(ksnFlag1){
								msg="3";//one or more talent is not linked to KSN
							}else if(wf1Flag1){
								msg="5"; //one or more talent is not completed WF1 Registration
							}else if(!duplicateCheck){		
								if(jfteacher!=null && jfteacher.size()>0){
									for(JobForTeacher jft : jfteacher){
										if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
											logger.info(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF2);
											if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()!=null){
												secondaryStatus = secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF2);
												if(secondaryStatus!=null)
													logger.info("callerId:"+callerId+"----secondaryStatus:"+secondaryStatus.getSecondaryStatusId());
												break;
											}
										}
									}
								}
								for(TeacherDetail td : teacherDetailList)
								{
									Boolean message=manageStatusAjax.messageQueueForOnboarding(statusMaster,secondaryStatus,jftMap.get(td),userMaster,"","");

								}
								msg="33";

							}

						}
						else if(callerId==4){
							if(wf3Flag){    		
								msg="6"; //one or more talent is already completed WF3 Registration
							}else if(ksnFlag1){
								msg="3";//one or more talent is not linked to KSN
							}else if(wf1Flag1){
								msg="5"; //one or more talent is not completed WF1 Registration
							}else if(wf2Flag1){
								msg="7"; //one or more talent is not completed WF2 Registration
							}else if(!duplicateCheck){
								if(jfteacher!=null && jfteacher.size()>0){
									for(JobForTeacher jft : jfteacher){
										if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
											logger.info(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF3);
											if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()!=null){
												secondaryStatus = secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()+"+"+INVWF3);
												if(secondaryStatus!=null)
													logger.info("callerId:"+callerId+"----secondaryStatus:"+secondaryStatus.getSecondaryStatusId());
												break;
											}
										}
									}
								}
								for(TeacherDetail td : teacherDetailList)
								{
									Boolean message=manageStatusAjax.messageQueueForOnboarding(statusMaster,secondaryStatus,jftMap.get(td),userMaster,"","");

								}
								msg="44";
							}

						}
					}
				}
				catch(Exception ex){
					ex.printStackTrace();
				}
			}else{
				msg = msg1;
			}
		}
		return msg;
	}
	
	public String callLinkToKSNFromOnboarding(Integer teacherId,Long jobForTeacherId,String nodeName,String statusNotes,String secStatusId){
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		StringBuffer sb = new StringBuffer();
		TeacherDetail teacherDetail = null;
		JobForTeacher jobForTeacher = null;
		JobOrder jobOrder = null;
		SecondaryStatus secStatus = null;
		String hideFlag="";
		logger.info(" teacherId get :: "+teacherId);
		
		if(teacherId!=null)
			teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		
		if(jobForTeacherId!=null){
			jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);
			jobOrder = jobForTeacher.getJobId();
		}
		
		if(secStatusId!=null && secStatusId.length()>0)
			secStatus = secondaryStatusDAO.findById(Integer.parseInt(secStatusId), false, false);
		
		if(statusNotes!=null && statusNotes.length()>0){
			TeacherStatusNotes tsn = new TeacherStatusNotes();
			tsn.setTeacherDetail(teacherDetail);
			tsn.setUserMaster(userMaster);
			tsn.setJobOrder(jobForTeacher.getJobId());
			if(secStatus!=null && secStatus.getStatusMaster()!=null){
				tsn.setStatusMaster(secStatus.getStatusMaster());
			}
			else {
				tsn.setSecondaryStatus(secStatus);
			}
			if(statusNotes!=null){
				tsn.setStatusNotes(statusNotes);
			}
			tsn.setCreatedDateTime(new Date());
			tsn.setFinalizeStatus(true);
			teacherStatusNotesDAO.makePersistent(tsn);
		}
		
		JobCategoryMaster jobCategoryMaster = null;
		if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null)
			jobCategoryMaster = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
		else
			jobCategoryMaster = jobOrder.getJobCategoryMaster();
		SecondaryStatus secondaryStatus = secondaryStatusDAO.findByNameAndJobCategory(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),jobCategoryMaster);
		logger.info(" jobForTeacherId get :: "+jobForTeacherId);
		StatusMaster statusMaster = null;
		SecondaryStatus sec = null;
		if(secondaryStatus!=null && secondaryStatus.getStatusMaster()!=null){
			statusMaster = secondaryStatus.getStatusMaster();
			logger.info(" statusMaster ::: "+statusMaster.getStatusId());
		}
		else{
			sec = secondaryStatus;
			if(sec!=null)
				logger.info(" sec :: "+sec.getSecondaryStatusId());
		}
		
		/*try{
			commonService.resetStatusForJobs(jobOrder,teacherDetail,jobForTeacher,Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
		}catch(Exception ex){
			ex.printStackTrace();
		}*/
			
		
		
			List<TalentKSNDetail> talentksndList = null;
			// inactive previous records
	    	 List<TalentKSNDetail> talentksnList = talentKSNDetailDAO.getListTalentKSNDetail(teacherDetail, "A");
	    	 if(talentksnList!=null && talentksnList.size()>0){
	    		 for(TalentKSNDetail tsn : talentksnList){
	    			 if(!tsn.getIsSelected().equalsIgnoreCase("y"))
	    				 tsn.setStatus("I");
	    				 talentKSNDetailDAO.updatePersistent(tsn);
	    		 }
	    	 }
	    	 System.out.println(" nodeName :: "+nodeName);
	    	 List<MQEvent> mqEvent=mqEventDAO.findEventByEventType(teacherDetail, nodeName);//findNodeStatusByTeacher(teacherDetail,null,sec);
			int counter=0;
			String result ="";
			boolean disableAccept = false;
			try {
				if((mqEvent==null || mqEvent.size()==0) || (mqEvent!=null && mqEvent.size()>0 && ((mqEvent.get(0).getStatus()!=null && mqEvent.get(0).getStatus().equalsIgnoreCase("R"))|| (mqEvent.get(0).getStatus()==null)) )){
					try {
						manageStatusAjax.messageQueue(statusMaster, sec, jobForTeacher, userMaster, "KSNID", "");
						logger.info("Time Start :::::::::::::::::::::::::::::::: "+new Date());
						while((talentksndList==null || talentksndList.size()==0) && counter<8){
							ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
							logger.info("Time Mid "+counter+": "+new Date());
							//talentksndList = talentKSNDetailDAO.getListTalentKSNDetail(teacherDetail, "A");
							logger.info(counter+"::::::::::talentksndList :::>>>>>>>>>>>: "+talentksndList);
							counter++;
							try {
								Runnable worker = new MQEventThread();
								executor.execute(worker);
							} catch (Exception e) {
							} 
							executor.shutdown();
							while (!executor.isTerminated()){}
						}
						
						mqEvent=mqEventDAO.findEventByEventType(teacherDetail, nodeName);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
					talentksndList = talentKSNDetailDAO.getListTalentKSNDetail(teacherDetail, "A");
					hideFlag= String.valueOf(talentksndList.size());
					if(talentksndList!=null && talentksndList.size()>0){
					
					sb.append("<div class='row' style='margin-left:4px;margin-bottom:-19px;'>* "+Utility.getLocaleValuePropByKey("lblTalentRecordFound", locale)+"</div><br/>");
					
					sb.append("<table border='0' class='table' id='tblKSNdetails'>");
					sb.append("<thead class='bg' >");
					sb.append("<tr>");
					sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th>");
					sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblKSNID", locale)+"</th>");
					sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblTeacherName1", locale)+"</th>");
					sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblTeacherEmail1", locale)+"</th>");
					sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblLastFourSSN", locale)+"</th>");
					//sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblStatus", locale)+"</th>");
					sb.append("</tr>");
					sb.append("</thead>");

					boolean newTalent = false;
					
						logger.info(" talentKSNDetail size :: "+talentksndList.size());
						String teacherIDD="";
						for(TalentKSNDetail talentKSNDetail : talentksndList){
							teacherIDD=talentKSNDetail.getTeacherId().getTeacherId().toString();
							sb.append("<tr>");
							sb.append("<td>");
							if(teacherDetail!=null && teacherDetail.getKSNID()!=null && (talentKSNDetail.getKSNID()!=null && Integer.parseInt(teacherDetail.getKSNID().toString())== talentKSNDetail.getKSNID()) || (talentKSNDetail.getIsSelected().equalsIgnoreCase("y"))){
								sb.append("<span><input type='radio' name='teacherStatus' onclick=\"varifyTchIdANDTmtlntID("+talentKSNDetail.getTeacherId().getTeacherId()+","+talentKSNDetail.getTmTalentId()+");\" id='tcher"+talentKSNDetail.getKsnDetailId()+"' value='"+talentKSNDetail.getKSNID()+"'  checked='checked'></span>");
								disableAccept =true;
							}else{
								sb.append("<span><input type='radio' name='teacherStatus' id='tcher"+talentKSNDetail.getKsnDetailId()+"' onclick=\"varifyTchIdANDTmtlntID("+talentKSNDetail.getTeacherId().getTeacherId()+","+talentKSNDetail.getTmTalentId()+");\"  value='"+talentKSNDetail.getKSNID()+"' ></span>");
							}
							sb.append("</td>");
							sb.append("<td>"+talentKSNDetail.getKSNID()+"</td>");
							String name=""+talentKSNDetail.getFirstName()+" "+talentKSNDetail.getLastName();
							sb.append("<td>"+name+"</td>");

							sb.append("<td>"+talentKSNDetail.getEmailAddress()+"</td>");
							if(talentKSNDetail.getLastFourSSN() != null){
								sb.append("<td>"+talentKSNDetail.getLastFourSSN()+"</td>");
							}else{
								sb.append("<td>"+""+"</td>");	
							}
							//sb.append("<td>"+talentKSNDetail.getStatus()+"</td>");

							sb.append("</tr>");
						}
						sb.append("<table  border='0' class='' style='float: right;margin-top:5px;'>");
						sb.append("<tr style='float: left; margin-left: 8cm;'><td>");

						/*if(disableAccept){ // hide as per new changes
							sb.append("<div id='onlyDec' style='display:none;'><button class='btn btn-primary' type='button' onclick=\"return declinedKSNID('"+teacherIDD+"','D')\">"+Utility.getLocaleValuePropByKey("lblDecline", locale)+"</div>");
							sb.append("<div id='appDec' style='display:none;'><button class='btn btn-primary' type='button' onclick='return acceptedmethod("+teacherDetail.getTeacherId()+")'>"+Utility.getLocaleValuePropByKey("lblAccepte", locale)+"</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-primary' type='button' onclick=\"return declinedKSNID('"+teacherIDD+"','D')\">"+Utility.getLocaleValuePropByKey("lblDecline", locale)+"</div>");
						}
						else{
							sb.append("<div id='onlyDec' style='display:none;'><button class='btn btn-primary' type='button' onclick=\"return declinedKSNID('"+teacherIDD+"','D')\">"+Utility.getLocaleValuePropByKey("lblDecline", locale)+"</div>");
							sb.append("<div id='appDec'><button class='btn btn-primary' type='button' onclick='return acceptedmethod("+teacherDetail.getTeacherId()+")'>"+Utility.getLocaleValuePropByKey("lblAccepte", locale)+"</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-primary' type='button' onclick=\"return declinedKSNID('"+teacherIDD+"','D')\">"+Utility.getLocaleValuePropByKey("lblDecline", locale)+"</div>");
							//sb.append("<div id='appDec'><button class='btn btn-primary' type='button' onclick='return acceptedmethod("+teacherDetail.getTeacherId()+")'>"+Utility.getLocaleValuePropByKey("lblAccepte", locale)+"<div>");
						}*/
						sb.append("<div class='row' id='appDec' style=''><button class='btn btn-primary' type='button' onclick='return acceptedmethod("+teacherDetail.getTeacherId()+")'>"+Utility.getLocaleValuePropByKey("lblSelectedRecords", locale)+"</button>&nbsp;&nbsp;&nbsp;<button class='btn btn-primary' type='button' onclick=\"return declinedKSNID('"+teacherIDD+"','D')\">"+Utility.getLocaleValuePropByKey("lblDeclineResult", locale)+"</div>");

						sb.append("</td></tr>");
						sb.append("</table>");
					}
					
					/*if((talentksndList==null || talentksndList.size()==0) &&  mqEvent!=null && mqEvent.size()>0 &&  (mqEvent.get(0).getStatus()==null || mqEvent.get(0).getStatus().equalsIgnoreCase("R"))){
						sb.append("<tr>");				
						sb.append("<td colspan=5>"+Utility.getLocaleValuePropByKey("msgNoKSNfound", locale)+"</td>");
						sb.append("</tr>");
					}*/
					
					
					/*else if(talentksndList.size()==0){
						sb.append("<tr>");				
						sb.append("<td colspan=5>"+Utility.getLocaleValuePropByKey("msgNorecordfound", locale)+"</td>");
						sb.append("</tr>");

					}*/
					
				// continue button is hide here ..
			/*	if(mqEvent!=null && mqEvent.size()>0 && (talentksndList==null || talentksndList.size()==0) && (mqEvent.get(0).getStatus()==null || mqEvent.get(0).getStatus().equalsIgnoreCase("R"))){
					if(mqEvent.get(0).getAckStatus()==null || mqEvent.get(0).getAckStatus()!=null){
						sb.append("<table  border='0' class=''  style='float: left; margin-left: 16cm;'>");
						sb.append("<tr style='float: right;margin-top: 7px;'><td><button class='btn btn-primary' type='button' onclick=\"return declinedKSNID('"+teacherDetail.getTeacherId()+"','C')\">"+Utility.getLocaleValuePropByKey("lblContinue", locale)+"</td></tr>");
						sb.append("</table>");
					}
				}*/
				
				
				}
					
				sb.append("</table>");
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return sb.toString()+"####"+hideFlag;
	}
	
	
	public String getLatestNodeColor(String teacherId,String node){
		
		logger.info(" node :: "+node);
		logger.info(" teacherId :: "+teacherId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		
		String nodeColor = "#7F7F7F"; //grey
		logger.info(" nodeColor :: "+nodeColor);
		if(teacherId!=null && teacherId.length()>0){
			
		TeacherDetail teacher = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
		List<MQEvent> mqEventList=new ArrayList<MQEvent>();
		Map<String,MQEvent> mapNodeStatus= new HashMap<String, MQEvent>();
		mqEventList=mqEventDAO.findMQEventByTeacher(teacher);
		 if(mqEventList.size()>0){
			 for (MQEvent mqEvent : mqEventList) {
				 mapNodeStatus.put(mqEvent.getTeacherdetail().getTeacherId()+"-"+mqEvent.getEventType() , mqEvent);
				 logger.info(" ------------- "+mqEvent.getTeacherdetail().getTeacherId()+"-"+mqEvent.getEventType());
			 }
		 }
		 MQEvent mqEventKSN = mapNodeStatus.get(teacher.getTeacherId()+"-"+node);
		 if(mqEventKSN!=null){
				if(mqEventKSN.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale))){
					/*if(mqEventKSN.getCheckEmailAPI()!=null && mqEventKSN.getCheckEmailAPI()){
						nodeColor = "7F7F7F";
				    }else */if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
						nodeColor="#009900"; //green
					}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && (mqEventKSN.getAckStatus()==null)){
						nodeColor="#fcd914"; //yellow
					}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && (mqEventKSN.getAckStatus()!=null && mqEventKSN.getAckStatus().equalsIgnoreCase("Received"))){
							nodeColor="#fcd914"; //yellow
					}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
						nodeColor="#ff0000"; //red
					}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))){
						nodeColor="#243a6a"; //blue
					}
				}
				//INV WF1
				else if(mqEventKSN.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF1", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEventKSN.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF1COM", locale));
					if(mqEventKSN!=null){
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}
						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
							nodeColor="Red"; // ff0000
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()==null && (((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate())) || (mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))))){
							nodeColor="Yellow"; // fcd914
						}

					}
				}
				//WF1 COM
				else if(mqEventKSN.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF1COM", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEventKSN.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblINVWF1", locale));
					if(mqEventKSN!=null){
						if(mqEventKSN.getStatus()!=null && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getStatus()!=null &&  mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="Green"; // 009900
						}
					}
				}
				//INV WF2
				else if(mqEventKSN.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF2", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEventKSN.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF2COM", locale));
					if(mqEventKSN!=null){
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}
						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
							nodeColor="Red"; // ff0000
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate())) && (mqEventKSN.getAckStatus()==null || (mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))))){
							nodeColor="Yellow"; // fcd914
						}
					}
				}
				//WF2 COM
				else if(mqEventKSN.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF2COM", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEventKSN.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblINVWF2", locale));
					if(mqEventKSN!=null){
						if(mqEventKSN!=null && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getStatus()!=null && mqEventtemp.get(0).getStatus().equalsIgnoreCase("C") && mqEventtemp.get(0).getWorkFlowStatusId()!=null  && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="Green"; // 009900
						}/*else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
							nodeColor="Red"; // ff0000
						}*/
					}
				}
				//INV WF3
				else if(mqEventKSN.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblINVWF3", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEventKSN.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblWF3COM", locale));
					if(mqEventKSN!=null){
						if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C") && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success")) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getWorkFlowStatusId()!=null && mqEventKSN.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblERR", locale)) && mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
							nodeColor="Green"; // 009900
						}
						else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Failed") || mqEventKSN.getAckStatus().equalsIgnoreCase("Failure"))){
							nodeColor="Red"; // ff0000
						}else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && ((mqEventKSN.getWorkFlowStatusId()==null) || (mqEventKSN.getTmInitiate()!=null && mqEventKSN.getTmInitiate())) && (mqEventKSN.getAckStatus()==null || (mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))))){
							nodeColor="Yellow"; // fcd914
						}
					}
				}
				//WF3 COM
				else if(mqEventKSN.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblWF3COM", locale))){
					nodeColor = "Grey"; // 7F7F7F
					List<MQEvent> mqEventtemp = mqEventDAO.findEventByEventType(mqEventKSN.getTeacherdetail(), Utility.getLocaleValuePropByKey("lblINVWF2", locale));
					if(mqEventKSN!=null){
						if(mqEventKSN.getStatus()!=null &&  mqEventtemp!=null && mqEventtemp.size()>0 && mqEventtemp.get(0).getStatus()!=null && mqEventtemp.get(0).getStatus().equalsIgnoreCase("C") && mqEventtemp.get(0).getWorkFlowStatusId()!=null && mqEventtemp.get(0).getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
							nodeColor="Green"; // 009900
						}
					}
				}
				
		 }
		
		}
		
		logger.info(" latest nodeColor :: "+nodeColor);
		return nodeColor;
	}
    public String getSPStatus(String teacherId,Long jf){
		
		logger.info(" teacherId :: "+teacherId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
		Map<String,String> mapHistrory = new HashMap<String,String>();
		SecondaryStatus lstTreeStructure=	new SecondaryStatus();
		int spNodeStatus=0; 
		String color = "0"; //grey
		try{
			JobForTeacher jft = jobForTeacherDAO.findById(jf, false, false);
			TeacherDetail teacherDetail = jft.getTeacherId();
			JobOrder jobOrder = jft.getJobId();
			JobCategoryMaster jobCategoryMaster = null;
			SecondaryStatus secondaryStatus = null;
			StatusMaster statusMaster = null;
			
			if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
				jobCategoryMaster = jft.getJobId().getJobCategoryMaster().getParentJobCategoryId();
			else
				jobCategoryMaster = jft.getJobId().getJobCategoryMaster();
			
			List<SecondaryStatus> lstSec = secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(jft.getJobId().getHeadQuarterMaster(),jft.getJobId().getBranchMaster(),null,jobCategoryMaster);
			if(lstSec!=null && lstSec.size()>0){
				for(SecondaryStatus sec : lstSec){
					if(sec.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale))){
						if(sec.getStatusMaster()!=null)
							statusMaster = sec.getStatusMaster();
						else
							secondaryStatus = sec;
					}
				}
			}
			
			lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findByTeacherStatusHistorySelected(teacherDetail,jobOrder);
			
			if(lstTeacherStatusHistoryForJob.size()>0){
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob :lstTeacherStatusHistoryForJob) {
					if(teacherStatusHistoryForJob.getStatusMaster()!=null && statusMaster!=null && teacherStatusHistoryForJob.getStatusMaster().getStatus().equalsIgnoreCase(statusMaster.getStatus())){
						String statusM=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatus();
						mapHistrory.put(statusM, teacherStatusHistoryForJob.getStatus());
					}else if(teacherStatusHistoryForJob.getSecondaryStatus()!=null && secondaryStatus!=null && teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName().equalsIgnoreCase(secondaryStatus.getSecondaryStatusName())){
						String sStatus=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatus();
						mapHistrory.put(sStatus, teacherStatusHistoryForJob.getStatus());
					}
				}
			}
		     List<TeacherDetail> teacherList=new ArrayList<TeacherDetail>();
		     teacherList.add(teacherDetail);
		     List<SpInboundAPICallRecord> spInboundAPICallRecordList= spInboundAPICallRecordDAO.getDetailsByTeacherList(teacherList);
		     if(spInboundAPICallRecordList.size()>0){
		      spNodeStatus=1;
		     }
		     Map<Integer,Integer> spStatusMap=new HashMap<Integer, Integer>();
		     List<TeacherAssessmentStatus>  teacherAssessmentStatusList=new ArrayList<TeacherAssessmentStatus>();
		     if(teacherList.size()>0){
		      teacherAssessmentStatusList=teacherAssessmentStatusDAO.findSmartPracticesTakenByTeachers(teacherList);
		      if(teacherAssessmentStatusList.size()>0)
		      for (TeacherAssessmentStatus teacherAssessmentStatusObj : teacherAssessmentStatusList) {
		       int keyAss=teacherAssessmentStatusObj.getTeacherDetail().getTeacherId();
		       if(spStatusMap.get(keyAss)==null){
		        if(teacherAssessmentStatusObj.getPass()!=null){
		         if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
		          spStatusMap.put(keyAss,1);
		          break;
		         }else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F")){
		          spStatusMap.put(keyAss,2);
		         }
		        }else if(teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
		         spStatusMap.put(keyAss,2);
		        }else{
		         spStatusMap.put(keyAss,0);
		        }
		       }else{
		        int prevValue=spStatusMap.get(keyAss);
		        if(teacherAssessmentStatusObj.getPass()!=null){
		         if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
		          spStatusMap.put(keyAss,1);
		          break;
		         }else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F") && prevValue!=0){
		          spStatusMap.put(keyAss,++prevValue);
		         }
		        }else if(prevValue!=0 && teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
		         spStatusMap.put(keyAss,++prevValue);
		        }else{
		         spStatusMap.put(keyAss,0);
		        }
		       }
		      }
		     }
		     if(spStatusMap.size()>0){
		      if(spStatusMap.get(teacherDetail.getTeacherId())!=null){
		       if(spStatusMap.get(teacherDetail.getTeacherId())>2){
		        spNodeStatus=2;
		       }
		       if(spStatusMap.get(teacherDetail.getTeacherId())==1){
		        spNodeStatus=3;
		       }
		      }
		     }
		    
		     lstTreeStructure = secondaryStatusDAO.findSecondaryStatusObjByJobCategoryHeadAndBranch(jobOrder,Utility.getLocaleValuePropByKey("lblSPStatus", locale));
		     SecondaryStatus subfL=null;
		     if(lstTreeStructure!=null){
		    	 if(lstTreeStructure.getSecondaryStatus()!=null && lstTreeStructure.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)))
					{		    	
		    			 subfL=lstTreeStructure;		    		 
					}
		     }
				
		     
		    if(subfL!=null){
		    String statusStr=jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId()+"##W";
		    String statusStr1=jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId()+"##S";
		    String waived1=mapHistrory.get(statusStr1);
		    String waived=mapHistrory.get(statusStr);
		    if(waived!=null && waived.equalsIgnoreCase("W")){
		    	     color="1"; //blue
		        }else if(subfL.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)) && spNodeStatus!=0){
		         if(spNodeStatus==1){
		        	 color="2"; //yellow
		         }else if(spNodeStatus==2){
		        	 color="3"; //red
		         }else if(spNodeStatus==3){
		        	 color="4"; //green
		         } 
		        }else if(waived1!=null && waived1.equalsIgnoreCase("S")){
		        	color="4"; //green
		        }
		    }
		    
		    
		    
		}catch(Exception e){
		     e.printStackTrace();
		    } 
		
		return color;
	}
    public void saveSPStatusWaived(Long jobForTeacherId, Integer statusNotes,String secStatusId,String priviosNodeColor)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		try
		{
			SecondaryStatus secondaryStatus = null;
			StatusMaster statusMaster = null;
			JobCategoryMaster jobCategoryMaster = null;
			JobForTeacher jft = jobForTeacherDAO.findById(jobForTeacherId, false, false);
			if(jft!=null)
			{
				if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null)
					jobCategoryMaster = jft.getJobId().getJobCategoryMaster().getParentJobCategoryId();
				else
					jobCategoryMaster = jft.getJobId().getJobCategoryMaster();
				
				List<SecondaryStatus> lstSec = secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategoryAndHead(jft.getJobId().getHeadQuarterMaster(),jft.getJobId().getBranchMaster(),null,jobCategoryMaster);
				if(lstSec!=null && lstSec.size()>0){
					for(SecondaryStatus sec : lstSec){
						if(sec.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale))){
							if(sec.getStatusMaster()!=null)
								statusMaster = sec.getStatusMaster();
							else
								secondaryStatus = sec;
						}
					}
				}
				TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
				tSHJ.setTeacherDetail(jft.getTeacherId());
				tSHJ.setJobOrder(jft.getJobId());
				tSHJ.setStatus("W");
				if(statusMaster!=null){
					tSHJ.setStatusMaster(statusMaster);
				}
				if(secondaryStatus!=null){
					tSHJ.setSecondaryStatus(secondaryStatus);
				}
				tSHJ.setCreatedDateTime(new Date());
				tSHJ.setUserMaster(userMaster);
				teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
				
				if(secondaryStatus!=null || statusMaster!=null){
					if(secondaryStatus!=null){
						jft.setSecondaryStatus(secondaryStatus);
						jft.setStatusMaster(null);
					}else if(statusMaster!=null){
						jft.setStatus(statusMaster);
						jft.setStatusMaster(statusMaster);
					}
					jobForTeacherDAO.updatePersistent(jft);
				}
				
				StatusNodeColorHistory nodeColorHistory = new StatusNodeColorHistory();
				
				nodeColorHistory.setTeacherDetail(jft.getTeacherId());
				nodeColorHistory.setUserMaster(userMaster);
				nodeColorHistory.setMilestoneName("SP Status");
				if(userMaster.getHeadQuarterMaster()!=null)
				nodeColorHistory.setHeadQuarterMaster(userMaster.getHeadQuarterMaster());
				if(userMaster.getBranchMaster()!=null)
				nodeColorHistory.setBranchMaster(userMaster.getBranchMaster());
				nodeColorHistory.setPrevColor(priviosNodeColor);
				nodeColorHistory.setCurrentColor("Blue");
				nodeColorHistory.setCreatedDateTime(new Date());
				if(secondaryStatus!=null)
					nodeColorHistory.setSecondaryStatusId(secondaryStatus.getSecondaryStatusId());
				else if(statusMaster!=null)
					nodeColorHistory.setStatusId(statusMaster.getStatusId());
				nodeColorHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				statusNodeColorHistoryDAO.makePersistent(nodeColorHistory);				
				
				if(statusNotes!=null && statusNotes!=0)
				{
				ReasonsForWaived reasonsForWaived=reasonsForWaivedDAO.findById(statusNotes, false, false);	
				TeacherStatusNotes tsn = new TeacherStatusNotes();
				
				tsn.setTeacherDetail(jft.getTeacherId());
				tsn.setUserMaster(userMaster);
				tsn.setJobOrder(jft.getJobId());
				if(statusMaster!=null){
					tsn.setStatusMaster(statusMaster);
				}
				else if(secondaryStatus!=null){
					tsn.setSecondaryStatus(secondaryStatus);
				}
				if(reasonsForWaived!=null && reasonsForWaived.getReasonName()!=null){
					tsn.setStatusNotes("Waived - "+reasonsForWaived.getReasonName());
				}
				tsn.setCreatedDateTime(new Date());
				teacherStatusNotesDAO.makePersistent(tsn);
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
    public void saveSPStatusWaivedMultiple(String[] teacher, String statusNotes)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		List<TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJob= new ArrayList<TeacherStatusHistoryForJob>();
		Map<String,String> mapHistrory = new HashMap<String,String>();
		List<Long> jftIds=new ArrayList<Long>();
		Set<Integer> teacherIds=new HashSet<Integer>();
		List<JobForTeacher> jfteacher = new ArrayList<JobForTeacher>();
		List<TeacherDetail> teacherDetailList=new ArrayList<TeacherDetail>();
		List<JobOrder> jobOrder=new ArrayList<JobOrder>();
		List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
		Map<Integer,Integer> spStatusMap=new HashMap<Integer, Integer>();
		List<TeacherAssessmentStatus>  teacherAssessmentStatusList=new ArrayList<TeacherAssessmentStatus>();
		Map<Integer,SecondaryStatus> categoryStatusMap=new HashMap<Integer, SecondaryStatus>();
		Map<JobCategoryMaster,JobCategoryMaster> allCategMap = new HashMap<JobCategoryMaster,JobCategoryMaster>();

		try
		{
			
			if(teacher.length>0)
			{
				for(String id:teacher)
				{
					String[] teacherArray=id.split("-");
					teacherIds.add(Integer.parseInt(teacherArray[0]));
					jftIds.add(Long.parseLong(teacherArray[1]));
				}		
				teacherDetailList=teacherDetailDAO.getActiveKellyTeacherList(teacherIds);
				jfteacher =jobForTeacherDAO.getJobForTeacherList(jftIds);
			}
			
			if(jfteacher.size()>0){
				for(JobForTeacher jf : jfteacher){
					jobOrder.add(jf.getJobId());
					allCategMap.put(jf.getJobId().getJobCategoryMaster().getParentJobCategoryId(), jf.getJobId().getJobCategoryMaster().getParentJobCategoryId());
				}				
			}
			List<JobCategoryMaster> allCategory = new ArrayList<JobCategoryMaster>();
			if(allCategMap.size()>0)
			{
			   allCategory = new ArrayList<JobCategoryMaster>(allCategMap.values());
			}
			
			lstTeacherStatusHistoryForJob=teacherStatusHistoryForJobDAO.findHistoryByTeachersAndJobs(teacherDetailList,jobOrder);
			
			if(lstTeacherStatusHistoryForJob.size()>0){
				for (TeacherStatusHistoryForJob teacherStatusHistoryForJob :lstTeacherStatusHistoryForJob) {
					if(teacherStatusHistoryForJob.getStatusMaster()!=null){
						String statusM=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatus();
						mapHistrory.put(statusM, teacherStatusHistoryForJob.getStatus());
					}else{
						String sStatus=teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"##"+teacherStatusHistoryForJob.getJobOrder().getJobId()+"##"+teacherStatusHistoryForJob.getStatus();
						mapHistrory.put(sStatus, teacherStatusHistoryForJob.getStatus());
					}
				}
			}
			if(teacherDetailList.size()>0){
			      teacherAssessmentStatusList=teacherAssessmentStatusDAO.findSmartPracticesTakenByTeachers(teacherDetailList);
			      if(teacherAssessmentStatusList.size()>0)
			      for (TeacherAssessmentStatus teacherAssessmentStatusObj : teacherAssessmentStatusList) {
			       int keyAss=teacherAssessmentStatusObj.getTeacherDetail().getTeacherId();
			       if(spStatusMap.get(keyAss)==null){
			        if(teacherAssessmentStatusObj.getPass()!=null){
			         if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
			          spStatusMap.put(keyAss,1);
			          break;
			         }else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F")){
			          spStatusMap.put(keyAss,2);
			         }
			        }else if(teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
			         spStatusMap.put(keyAss,2);
			        }else{
			         spStatusMap.put(keyAss,0);
			        }
			       }else{
			        int prevValue=spStatusMap.get(keyAss);
			        if(teacherAssessmentStatusObj.getPass()!=null){
			         if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("P")){
			          spStatusMap.put(keyAss,1);
			          break;
			         }else if(teacherAssessmentStatusObj.getPass().equalsIgnoreCase("F") && prevValue!=0){
			          spStatusMap.put(keyAss,++prevValue);
			         }
			        }else if(prevValue!=0 && teacherAssessmentStatusObj.getStatusMaster()!=null && teacherAssessmentStatusObj.getStatusMaster().getStatusShortName().equals("vlt")){
			         spStatusMap.put(keyAss,++prevValue);
			        }else{
			         spStatusMap.put(keyAss,0);
			        }
			       }
			      }
			     }
			
			lstTreeStructure = secondaryStatusDAO.findByNameAndJobCats(Utility.getLocaleValuePropByKey("lblSPStatus", locale),allCategory);
		     if(lstTreeStructure!=null){
		    	 for(SecondaryStatus sc : lstTreeStructure)
		    	 {
		    	 if(sc.getSecondaryStatus()!=null && sc.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)))
					{	
		    		 if(sc.getJobCategoryMaster()!=null)
		    			 categoryStatusMap.put(sc.getJobCategoryMaster().getJobCategoryId(), sc);
					}
		    	 }
		     }
		     Map<TeacherDetail,Integer> spInboundMap=new HashMap<TeacherDetail, Integer>();
		     List<SpInboundAPICallRecord> spInboundAPICallRecordList= spInboundAPICallRecordDAO.getDetailsByTeacherList(teacherDetailList);
		     if(spInboundAPICallRecordList.size()>0){
		     for(SpInboundAPICallRecord sIACR : spInboundAPICallRecordList)
		     {
		    	 spInboundMap.put(sIACR.getTeacherDetail(), 1);
		     }
		     }
		     
			if(jfteacher.size()>0){
				for(JobForTeacher jft : jfteacher)
				{
					String nodeColor = "0";
					int spNodeStatus=0;
					
					if(spInboundMap!=null && spInboundMap.get(jft.getTeacherId())!=null){
						spNodeStatus=1;
					}			
					if(spStatusMap.size()>0){
					      if(spStatusMap.get(jft.getTeacherId().getTeacherId())!=null){
					       if(spStatusMap.get(jft.getTeacherId().getTeacherId())>2){
					        spNodeStatus=2;
					       }
					       if(spStatusMap.get(jft.getTeacherId().getTeacherId())==1){
					        spNodeStatus=3;
					       }
					      }
					     }
						SecondaryStatus subfL =null;
						if(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
							subfL = categoryStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId());
						}else{
							subfL = categoryStatusMap.get(jft.getJobId().getJobCategoryMaster().getJobCategoryId());
						}					if(subfL!=null){
					    String statusStr=jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId()+"##W";
					    String statusStr1=jft.getTeacherId().getTeacherId()+"##"+jft.getJobId().getJobId()+"##S";
					    String waived=mapHistrory.get(statusStr);
					    String waived1=mapHistrory.get(statusStr1);
					    if(waived!=null && waived.equalsIgnoreCase("W")){
					    	nodeColor="1"; //blue
					        }else if(subfL.getSecondaryStatusName().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPStatus", locale)) && spNodeStatus!=0){
					         if(spNodeStatus==1){
					        	 nodeColor="2"; //yellow
					         }else if(spNodeStatus==2){
					        	 nodeColor="3"; //red
					         }else if(spNodeStatus==3){
					        	 nodeColor="4"; //green
					         } 
					        }else if(waived1!=null && waived1.equalsIgnoreCase("S")){
					        	 nodeColor="4"; //green
					        }
					    }

				if(nodeColor.equals("0")){					
				TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
				tSHJ.setTeacherDetail(jft.getTeacherId());
				tSHJ.setJobOrder(jft.getJobId());
				tSHJ.setStatus("W");
				if(jft.getStatus()!=null){
					tSHJ.setStatusMaster(jft.getStatus());
				}
				if(jft.getSecondaryStatus()!=null){
					tSHJ.setSecondaryStatus(jft.getSecondaryStatus());
				}
				tSHJ.setCreatedDateTime(new Date());
				tSHJ.setUserMaster(userMaster);
				teacherStatusHistoryForJobDAO.makePersistent(tSHJ);
				
                StatusNodeColorHistory nodeColorHistory = new StatusNodeColorHistory();
				
				nodeColorHistory.setTeacherDetail(jft.getTeacherId());
				nodeColorHistory.setUserMaster(userMaster);
				nodeColorHistory.setMilestoneName("SP Status");
				if(userMaster.getHeadQuarterMaster()!=null)
				nodeColorHistory.setHeadQuarterMaster(userMaster.getHeadQuarterMaster());
				if(userMaster.getBranchMaster()!=null)
				nodeColorHistory.setBranchMaster(userMaster.getBranchMaster());
				nodeColorHistory.setPrevColor("Grey");
				nodeColorHistory.setCurrentColor("Blue");
				nodeColorHistory.setCreatedDateTime(new Date());
				/*if(jft.getSecondaryStatus()!=null)
				nodeColorHistory.setSecondaryStatusId(jft.getSecondaryStatus().getSecondaryStatusId());
				if(jft.getStatus()!=null)
				nodeColorHistory.setStatusId(jft.getStatus().getStatusId());*/
				nodeColorHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				statusNodeColorHistoryDAO.makePersistent(nodeColorHistory);	
				
				if(statusNotes!=null && statusNotes!="")
				{
				TeacherStatusNotes tsn = new TeacherStatusNotes();
				
				tsn.setTeacherDetail(jft.getTeacherId());
				tsn.setUserMaster(userMaster);
				tsn.setJobOrder(jft.getJobId());
				if(jft.getStatus()!=null){
					tsn.setStatusMaster(jft.getStatus());
				}
				if(jft.getSecondaryStatus()!=null){
					tsn.setSecondaryStatus(jft.getSecondaryStatus());
				}
				if(statusNotes!=null){
					tsn.setStatusNotes(statusNotes);
				}
				tsn.setCreatedDateTime(new Date());
				teacherStatusNotesDAO.makePersistent(tsn);
				}
			}
		   }			
		 }
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
    public void saveSPStatusUndo(Long jobForTeacherId, Integer statusNotes,String secStatusId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}

		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		try
		{
		//	SecondaryStatus secondaryStatus = null;
			StatusMaster statusMaster = null;
			if(secStatusId!=null && secStatusId.length()>0){
				//secondaryStatus = secondaryStatusDAO.findById(Integer.parseInt(secStatusId), false, false);
				statusMaster = statusMasterDAO.findById(Integer.parseInt(secStatusId), false, false);
			}
			
			JobForTeacher jft = jobForTeacherDAO.findById(jobForTeacherId, false, false);
			if(jft!=null)
			{
				
				TeacherStatusHistoryForJob tSHJ= new TeacherStatusHistoryForJob();
				StatusMaster statusMasterObj  = WorkThreadServlet.statusMap.get("hird");
				tSHJ=teacherStatusHistoryForJobDAO.findWaivedTeacherStatusHistory(jft.getTeacherId(),jft.getJobId(),"W");
				
				tSHJ.setStatus("I");
				tSHJ.setHiredByDate(null);
				tSHJ.setUpdatedDateTime(new Date());
				tSHJ.setUserMaster(userMaster);
				teacherStatusHistoryForJobDAO.updatePersistent(tSHJ);
				
				try{
					commonService.undoJobStatus(jft);
				}catch(Exception ex){
					ex.printStackTrace();
				}
				
				List<StatusNodeColorHistory> priviosColorSPS= statusNodeColorHistoryDAO.getPreviosColorSPS(jft.getTeacherId(),Utility.getLocaleValuePropByKey("lblSPStatus", locale));
				 
				
                StatusNodeColorHistory nodeColorHistory = new StatusNodeColorHistory();
				
				nodeColorHistory.setTeacherDetail(jft.getTeacherId());
				nodeColorHistory.setUserMaster(userMaster);
				nodeColorHistory.setMilestoneName("SP Status");
				if(userMaster.getHeadQuarterMaster()!=null)
				nodeColorHistory.setHeadQuarterMaster(userMaster.getHeadQuarterMaster());
				if(userMaster.getBranchMaster()!=null)
				nodeColorHistory.setBranchMaster(userMaster.getBranchMaster());
				if(priviosColorSPS.size()>0 && priviosColorSPS!=null)
					nodeColorHistory.setCurrentColor(priviosColorSPS.get(0).getPrevColor());
				else
					nodeColorHistory.setCurrentColor("Grey");
				nodeColorHistory.setPrevColor("Blue");
				nodeColorHistory.setCreatedDateTime(new Date());
				/*if(jft.getSecondaryStatus()!=null)
				nodeColorHistory.setSecondaryStatusId(jft.getSecondaryStatus().getSecondaryStatusId());
				*/
				if(secStatusId!=null && !secStatusId.equals(""))
					nodeColorHistory.setStatusId(Integer.parseInt(secStatusId));
				
				nodeColorHistory.setIpAddress(IPAddressUtility.getIpAddress(request));
				statusNodeColorHistoryDAO.makePersistent(nodeColorHistory);
				
				//if(statusNotes!=null && statusNotes!=0)
				//{
					//ReasonsForWaived reasonsForWaived=reasonsForWaivedDAO.findById(statusNotes, false, false);
					TeacherStatusNotes tsn = new TeacherStatusNotes();

					tsn.setTeacherDetail(jft.getTeacherId());
					tsn.setUserMaster(userMaster);
					tsn.setJobOrder(jft.getJobId());
					/*if(secondaryStatus.getStatusMaster()!=null){
						tsn.setStatusMaster(secondaryStatus.getStatusMaster());
					}
					else if(secondaryStatus!=null){
						tsn.setSecondaryStatus(secondaryStatus);
					}*/
					if(statusMaster!=null){
						tsn.setStatusMaster(statusMaster);
					}
					/*else if(secondaryStatus!=null){
						tsn.setSecondaryStatus(secondaryStatus);
					}*/
					
					//if(reasonsForWaived!=null && reasonsForWaived.getReasonName()!=null){
						tsn.setStatusNotes("Reset");
					//}
					tsn.setCreatedDateTime(new Date());
					teacherStatusNotesDAO.makePersistent(tsn);
				//}
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
	}
    private boolean  getResetShowOrNot(String nodeName, String teacherId){
    	try{
    	if(nodeList.contains(nodeName)){
    		List<MQEvent> list=mqEventDAO.findMQEventByTeacher_Op(teacherDetailDAO.findById(new Integer(teacherId), false, false),nodeName);
    		if(list!=null && list.size()>0){
    			MQEvent mqevent=list.get(0);
    			if(mqevent!=null && mqevent.getStatus()!=null)
    				return true;
    				else
    				return false;
    		}else
    		return false;
    	}else{
    		return false;
    	}
    	}catch(Exception  e){e.printStackTrace();return false;}
    
    }
    public String getNodeStatusLog(String nodeName, String teacherId,String jobForTeacherId,String secStatusId,String statusId){

    	StringBuffer sb =	new StringBuffer();
    	int counter =0;
    	int showHideResetButton=getResetShowOrNot(nodeName,teacherId)?1:0;
    	TeacherDetail teacherDetail = null;
    	JobForTeacher jobForTeacher = null;
    	JobOrder jobOrder = null;
    	Integer jobCategoryId = 0;
    	List<CommunicationsDetail> communicationDetailList = new ArrayList<CommunicationsDetail>();
    	teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
    	//jobForTeacher = jobForTeacherDAO.findById(Long.parseLong(jobForTeacherId), false, false);
    	/*if(jobForTeacher!=null){
    		jobOrder = jobForTeacher.getJobId();
    		jobCategoryId = jobOrder.getJobCategoryMaster().getJobCategoryId();
    	}*/
    	//System.out.println(" jobCategoryId ::::::>>>>>>>>>>>>>>>>> "+jobCategoryId);
    	List<StatusNodeColorHistory> statusNodeColorList = statusNodeColorHistoryDAO.getStatusColorHistoryByTeacher(teacherDetail);
    	//List<AdminActivityDetail> adminActivityDetails = null;
    	//List <JobCategoryMasterHistory> data= jobCategoryMasterHistoryDAO.listJobCategoryHistoryByPrevId(jobCategoryId);
    	/*List<String> noCheckLabels=new ArrayList<String>();
    		noCheckLabels.add(StringUtils.capitalize("JobCategoryHistoryId"));
    		noCheckLabels.add(StringUtils.capitalize("updateTime"));
    		noCheckLabels.add(StringUtils.capitalize("updateAction"));*/
		
		/*try {
			adminActivityDetails = adminActivityDetailDAO.findByEntityIdNdType(jobForTeacher.getJobId().getJobId());
		} catch (Exception e1) {
			e1.printStackTrace();
		}*/
		
		try {
			System.out.println(" nodeName ::: "+nodeName);
			if(statusNodeColorList.size()>0){
				for(StatusNodeColorHistory snch : statusNodeColorList){
					if(snch.getMqEvent()!=null && 
							((nodeName.equalsIgnoreCase(snch.getMqEvent().getEventType()) || (snch.getMqEvent().getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale)))) || 
									(statusId!=null && statusId.length()>0 && 
											((nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblInElig", locale)) || 
													nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblInInlig", locale))) && 
														(snch.getMqEvent().getStatusMaster()!=null && 
															snch.getMqEvent().getStatusMaster().getStatusId()==Integer.parseInt(statusId)))))) { 
						CommunicationsDetail comm = new CommunicationsDetail();
						comm.setCommType("Kelly API");
						if(snch.getUpdateDateTime()!=null)
							comm.setCreatedDateTime(snch.getUpdateDateTime());
						else
							comm.setCreatedDateTime(snch.getCreatedDateTime());
						comm.setLastUpdateDateTime(snch.getCreatedDateTime());
						comm.setUpdateDateTime(snch.getUpdateDateTime());
						
						if(snch.getUserMaster()!=null){
							comm.setUserFirstName(snch.getUserMaster().getFirstName());
							comm.setUserLastName(snch.getUserMaster().getFirstName());
						}
						comm.setCurrentColor(snch.getCurrentColor());
						comm.setPrevColor(snch.getPrevColor());
						comm.setMqEvent(snch.getMqEvent());
						communicationDetailList.add(comm);
					}else  if(statusId!=null && statusId.length()>0 && nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPS", locale))){
						CommunicationsDetail comm = new CommunicationsDetail();
						comm.setCommType("SPS");
						if(snch.getUpdateDateTime()!=null)
							comm.setCreatedDateTime(snch.getUpdateDateTime());
						else
							comm.setCreatedDateTime(snch.getCreatedDateTime());
						comm.setLastUpdateDateTime(snch.getCreatedDateTime());
						comm.setUpdateDateTime(snch.getUpdateDateTime());
						
						if(snch.getUserMaster()!=null){
							comm.setUserFirstName(snch.getUserMaster().getFirstName());
							comm.setUserLastName(snch.getUserMaster().getFirstName());
						}
						comm.setCurrentColor(snch.getCurrentColor());
						comm.setPrevColor(snch.getPrevColor());
						//comm.setMqEvent(snch.getMqEvent());
						communicationDetailList.add(comm);
					}
				}
			}
			String fields="";
			//LinkedHashMap<String, String> columnVsUI  = manageJobOrdersAjax.getColumnVsUIMap();
			
			/*if(adminActivityDetails!=null && adminActivityDetails.size()>0){
				for(AdminActivityDetail activity : adminActivityDetails){
					CommunicationsDetail comm = new CommunicationsDetail();
					comm.setCommType("Job Order");
					comm.setUserFirstName(activity.getUserMaster().getFirstName());
					comm.setUserLastName(activity.getUserMaster().getLastName());
					comm.setCreatedDateTime(activity.getCreatedDate());
					comm.setActivityType(activity.getActivityType());
					if(activity.getActivityType().equalsIgnoreCase("update")){
						try {
							fields= manageJobOrdersAjax.getDiffJobValueKeys(activity , columnVsUI);
							if(fields == null) continue;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						if(activity.getActivityType().equalsIgnoreCase("create"))
						fields="<span style='color:green'>New job Order created.</style>";
						else
							fields="N/A";						
						}
					comm.setLogColumn(fields);
					communicationDetailList.add(comm);
				}
			}*/
			
			/*if(data!=null && data.size()>0){
				columnVsUI.clear();
				columnVsUI = hqJobCategoryAjax.getColumnVsUIMap();
				for(int i=0;i<data.size();i++){
					fields = "";
					CommunicationsDetail comm = new CommunicationsDetail();
					comm.setCommType("Job Category");
					comm.setUserFirstName(data.get(i).getUserMaster().getFirstName());
					comm.setUserLastName(data.get(i).getUserMaster().getLastName());
					comm.setCreatedDateTime(data.get(i).getUpdateTime());
					comm.setActivityType(data.get(i).getUpdateAction().equalsIgnoreCase("add")==true?"Create":"Update");
					
					
					JobCategoryMasterHistory jobCatHis = data.get(i);
					// hide preAdd Flag object from UI ( preAdd - while old job category is updated first time )
					if(jobCatHis.getUpdateAction()!=null && jobCatHis.getUpdateAction().equalsIgnoreCase("preAdd")) continue;
 
					if(data.size()>i+1){
						try {
							Map<String,Object> first =hqJobCategoryAjax.ConvertObjectToMap(jobCatHis);
							Map<String,Object> second =hqJobCategoryAjax.ConvertObjectToMap(data.get(i+1));
							fields= hqJobCategoryAjax.getDiffValueKeys(first, second ,noCheckLabels , columnVsUI);
							if(fields == null) continue;
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else{
						if(jobCatHis.getUpdateAction().equalsIgnoreCase("add"))
						fields="<span style='color:green'>New job category created.</style>";
						else
							fields="N/A";						
						}
					
					comm.setLogColumn(fields);
					communicationDetailList.add(comm);
				}
			}*/
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
    	String year="";
    	String preYear="";
    	sb.append("<table border='0' width='760' class='tablecgtbl'>");
    	sb.append("<tr><td>");
    	sb.append("<table border='0' style='padding:0px;' cellpadding=0 cellspacing=0  width='100%' class='tablecgtbl'>");
    	
    	/////////////////////////// Start :: LinkTOKSN ////////////////////////////////////////
    	try {
    		if(communicationDetailList.size()>0){
    			Collections.sort(communicationDetailList,CommunicationsDetail.createdDate);
    			for(CommunicationsDetail commDetail : communicationDetailList){
    				if(commDetail.getUpdateDateTime()!=null)
    					year= convertUSformatOnlyYear(commDetail.getUpdateDateTime());
    				else
    					year= convertUSformatOnlyYear(commDetail.getCreatedDateTime());
    				counter++;
    				if(!preYear.equals(year)){
    					sb.append("<tr>");
    					sb.append("<td colspan=3 style='text-align:right;padding-right:8px;padding-bottom:5px;verical-align:bottom;'><span class=\"txtyearbg\">"+year+"</span></td><td>&nbsp;</td>");
    					sb.append("<tr>");
    					sb.append("<tr>");
    					sb.append("<td width='15%' style='height:10px;'>&nbsp;</td>");
    					sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=10>");
    					sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
    					sb.append("</tr>");
    				}
    				preYear=year;
    				sb.append("<tr>");
    				if(commDetail.getCommType().equalsIgnoreCase("Kelly API") && commDetail.getUpdateDateTime()!=null)
    					sb.append("<td width='23%' style='text-align:right;vertical-align:middle;position:relative;top:-3px;'>"+Utility.convertDateAndTimeFormatForCommunication(commDetail.getUpdateDateTime()));
    				else
    					sb.append("<td width='23%' style='text-align:right;vertical-align:middle;position:relative;top:-3px;'>"+Utility.convertDateAndTimeFormatForCommunication(commDetail.getCreatedDateTime()));
    				
    				if(commDetail.getActivityType()!=null && commDetail.getActivityType().equalsIgnoreCase("create"))
						sb.append("<span class='note-com'><img src=\"images/add_circle.png\" style=\"width:20px;    margin-left: 3px; height:auto;\"></span>");
					else
						sb.append("<span class='note-com'><img src=\"images/edit_circle.png\" style=\"width:20px;    margin-left: 3px; height:auto;\"></span>");
    				
    				sb.append("</td><td style='text-align:right;vertical-align:middle;padding-left:12px;' width='8px;' height='10'>");
    				sb.append("<span class=\"vertical-line\"></span></td>");
    				sb.append("<td  style='text-align:right;vertical-align:middle;' width=20>");;
    				sb.append("<hr style=\"color: #f00; background:#007AB4; width: 100%; height: 3px;\"/></td>");
    				sb.append("<td class='commbgcolor'  style='padding:5px;vertical-align:middle;'>");
    				if(commDetail.getCommType().equalsIgnoreCase("Kelly API")){
    					sb.append("<span><b>Action:</b> Integration Update </span><br/>");
    					sb.append("<span><b>User:</b> KSN </span><br/>");
    					if(commDetail.getMqEvent()!=null)
    						sb.append("<span><b>Updated For: </b>"+commDetail.getMqEvent().getEventType()+"</span><br/>");
    					if(commDetail.getMqEvent()!=null && (!commDetail.getMqEvent().getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("msgDisconnectTalent", locale)))){
    						sb.append("<span><b>Updated Field Details: </b></span><br/>");

    						sb.append("<span><table class='tablecgtbl' style='width:100%;'>");
    						sb.append("<tr class=\"txtyearbg\" style='height:15px;'><td>#</td>");
    						sb.append("<td>Update</td>");
    						sb.append("<td>Previous Value</td>");
    						sb.append("<td>Current Value</td></tr>");

    						sb.append("<tr>");
    						sb.append("<td>1.</td>");
    						sb.append("<td>Date/Time</td>");
    						sb.append("<td>"+commDetail.getLastUpdateDateTime()+"</td>");
    						if(commDetail.getUpdateDateTime()!=null)
    							sb.append("<td>"+commDetail.getUpdateDateTime()+"</td>");
    						else
    							sb.append("<td>"+commDetail.getCreatedDateTime()+"</td>");
    						sb.append("</tr>");

    						sb.append("<tr>");
    						sb.append("<td>2.</td>");
    						sb.append("<td>Status</td>");
    						sb.append("<td>"+commDetail.getPrevColor()+"</td>");
    						sb.append("<td>"+commDetail.getCurrentColor()+"</td>");
    						sb.append("</tr>");
    						sb.append("</table></span>");
    						
    					}
    					else{
    						sb.append("<span><b>Date:</b> "+commDetail.getCreatedDateTime()+"</span><br/>");
    					}
    					
    				}
    				else /*if(commDetail.getCommType().equalsIgnoreCase("Job Order"))*/{
    					sb.append("<span class='com-user' style='line-height:14px;'>Action: "+(commDetail.getActivityType().equalsIgnoreCase("create")==true?"Create":"Update") +"</span><br/>");
						sb.append("<span class='com-user' style='line-height:14px;'>User: "+commDetail.getUserFirstName()+" "+commDetail.getUserLastName()+"</span><br/>");
						sb.append("<span class='com-user' style='line-height:14px;'>Fields that were updated: "+commDetail.getLogColumn()+"</span>");
    				}
    				/*else if(commDetail.getCommType().equalsIgnoreCase("Job Category")){
    					
    				}*/
    				sb.append("</td>");
    				sb.append("</tr>");
    				if(counter<communicationDetailList.size()){
    					sb.append("<tr>");
    					sb.append("<td width='15%' style='height:20px;'>&nbsp;</td>");
    					sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=20>");
    					sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
    					sb.append("</tr>");
    				}
    			}
    			sb.append("</td></tr>");
    		}
    		//logger.info(" jobForTeacher.getJobId().getJobId() ::::::::::: "+jobForTeacher.getJobId().getJobId());
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	////////// End:: SPS ////////////////////////////////////////////////////
    	
    	
    	sb.append("<input type='hidden' value='"+showHideResetButton+"' id='showHideResetButtonONB' name='showHideResetButtonONB' />");
    	sb.append("</table>");

    	return sb.toString();
    }
    
    
    
    public String saveNotesForOnBoardingNodes(String teacherId,String jobForTeacherId,String nodeName, String statusNotes,String secStatusId,String statusId)
	{
    	WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		String result = "";
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		JobForTeacher jobForTeacher = null;
		TeacherDetail teacherDetail = null;
		SecondaryStatus secondaryStatus = null;
		StatusMaster statusMaster = null;
		try{
			logger.info(" ::::::::::: nodeName ::::::::: "+nodeName);
			logger.info("  :::::: statusNotes :::: "+statusNotes);
			
			if(jobForTeacherId!=null && jobForTeacherId.length()>0)
				jobForTeacher = jobForTeacherDAO.findById(Long.valueOf(jobForTeacherId), false, false);
			
			if(jobForTeacher!=null)
				teacherDetail = jobForTeacher.getTeacherId();
			
			if(secStatusId!=null && secStatusId.length()>0)
				secondaryStatus = secondaryStatusDAO.findById(Integer.parseInt(secStatusId), false, false);
			else if(statusId!=null && statusId.length()>0)
				statusMaster = WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
			if(statusNotes!=null && statusNotes.length()>0){
				TeacherStatusNotes tsn = new TeacherStatusNotes();
				tsn.setTeacherDetail(teacherDetail);
				tsn.setUserMaster(userMaster);
				tsn.setJobOrder(jobForTeacher.getJobId());
				if(secondaryStatus!=null){
					if(secondaryStatus.getStatusMaster()!=null){
						tsn.setStatusMaster(secondaryStatus.getStatusMaster());
					}
					else {
						tsn.setSecondaryStatus(secondaryStatus);
					}
				}
				else if(statusMaster!=null){
					tsn.setStatusMaster(statusMaster);
				}
				if(statusNotes!=null){
					tsn.setStatusNotes(statusNotes);
				}
				tsn.setCreatedDateTime(new Date());
				tsn.setFinalizeStatus(true);
				teacherStatusNotesDAO.makePersistent(tsn);
			}
			result = "success";
			
		}
		catch(Exception ex){
			ex.printStackTrace();
			result = "failed";
		}
		
    	return "";
	}
    
    public String getStatusNotesDiv(String teacherId,String jobForTeacherId,String nodeName,String secStatusId,String pageNo, String noOfRow,String sortOrder,String sortOrderType,String statusId){
    	
    	logger.info(" ::::::::::::::::::: getStatusNotesDiv :::::::::::::: "+statusId);
    	WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
    	StringBuffer sb = new StringBuffer();
    	
    	UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		JobForTeacher jobForTeacher = null;
		TeacherDetail teacherDetail = null;

		int noOfRowInPage = Integer.parseInt(noOfRow);
		int pgNo = Integer.parseInt(pageNo);
		int start = ((pgNo-1)*noOfRowInPage);
		int end = ((pgNo-1)*noOfRowInPage) +noOfRowInPage ;
		int totaRecord = 0;
		
		List<TeacherStatusNotes> lstTeacherStatusNotes = new ArrayList<TeacherStatusNotes>();
		List<TeacherStatusNotes> lstTeacherStatusNotesAll = new ArrayList<TeacherStatusNotes>();
		SecondaryStatus secondaryStatus = null;
		StatusMaster statusMaster =null;
		String sortOrderFieldName	=	"createdDateTime";
		Order  sortOrderStrVal		=	null;
		String sortOrderTypeVal="0";
		if((secStatusId!=null && secStatusId.length()>0) || (statusId!=null && statusId.length()>0))
		{
			try {
				if(jobForTeacherId!=null && jobForTeacherId.length()>0)
					jobForTeacher = jobForTeacherDAO.findById(Long.valueOf(jobForTeacherId), false, false);
				
				if(jobForTeacher!=null)
					teacherDetail = jobForTeacher.getTeacherId();
				
				if(secStatusId!=null && secStatusId.length()>0)
					secondaryStatus = secondaryStatusDAO.findById(Integer.parseInt(secStatusId), false, false);
				else if(statusId!=null && statusId.length()>0)
					statusMaster = WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
				
				Criterion criterion1 = Restrictions.eq("teacherDetail", teacherDetail);
				Criterion criterion2 = Restrictions.eq("jobOrder", jobForTeacher.getJobId());
				Criterion criterion3=null;
				if(secondaryStatus!=null){
					if(secondaryStatus.getStatusMaster()!=null)
						criterion3 = Restrictions.eq("statusMaster", secondaryStatus.getStatusMaster());
					else
						criterion3 = Restrictions.eq("secondaryStatus", secondaryStatus);
				}
				else if(statusMaster!=null){
					criterion3 = Restrictions.eq("statusMaster", statusMaster);
				}

				if(!sortOrder.equals("") && !sortOrder.equals(null))
				{
					sortOrderFieldName		=	sortOrder;
				}
				if(!sortOrderType.equals("") && !sortOrderType.equals(null))
				{
					if(sortOrderType.equals("0"))
					{
						sortOrderStrVal		=	Order.asc(sortOrderFieldName);
					}
					else
					{
						sortOrderTypeVal	=	"1";
						sortOrderStrVal		=	Order.desc(sortOrderFieldName);
					}
				}
				else
				{
					sortOrderTypeVal		=	"1";
					sortOrderStrVal			=	Order.desc(sortOrderFieldName);
				}

				lstTeacherStatusNotes=teacherStatusNotesDAO.findWithLimit(sortOrderStrVal,start,noOfRowInPage,criterion1,criterion2,criterion3);
				lstTeacherStatusNotesAll=teacherStatusNotesDAO.findWithAll(sortOrderStrVal,criterion1,criterion2,criterion3);

				totaRecord=lstTeacherStatusNotesAll.size();
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}
		String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			sb.append("<table border='0' class='table tblStatusNote' id='tblStatusNote'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblNotes", locale),sortOrderFieldName,"statusNotes",sortOrderTypeVal,pgNo)+"</th>");
			sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("hdAttachment", locale)+"</th>");
			sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedBy", locale),sortOrderFieldName,"userMaster",sortOrderTypeVal,pgNo)+"</th>");
			sb.append("<th valign=top>"+PaginationAndSorting.responseSortingLink(Utility.getLocaleValuePropByKey("lblCreatedDate", locale),sortOrderFieldName,"createdDateTime",sortOrderTypeVal,pgNo)+"</th>");
			//sb.append("<th valign=top>"+Utility.getLocaleValuePropByKey("lblActions", locale)+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			int counter=0,countSave=0;
			if(lstTeacherStatusNotes !=null && lstTeacherStatusNotes.size()>0)
			{
				for(TeacherStatusNotes tsDetails : lstTeacherStatusNotes){
					sb.append("<tr>");
					sb.append("<td>");
					String statusNote=tsDetails.getStatusNotes();
					if(statusNote!=null){
						String statusNotes=statusNote.replaceAll("\\<[^>]*>","");
						int iTurncateIndex=90;
						if(statusNotes.length()>iTurncateIndex)
						{
							int sSpaceIndex=statusNotes.indexOf(" ", iTurncateIndex);
							if(sSpaceIndex >0)
							{
								statusNotes=statusNotes.substring(0,sSpaceIndex);
								sb.append("<a href='javascript:void(0);' id='teacherNotesDetails"+tsDetails.getTeacherStatusNoteId()+"' rel='tooltip' data-original-title='"+statusNote+"' >"+statusNotes+"</a>");
								sb.append("<script>$('#teacherNotesDetails"+tsDetails.getTeacherStatusNoteId()+"').tooltip({placement : 'right'});</script>");
							}
							else
								sb.append(statusNotes);
						}
						else
							sb.append(statusNotes);
					}
					sb.append("</td>");
					sb.append("<td>");
					String fileName=tsDetails.getStatusNoteFileName();
					String filePath="statusNote/"+teacherDetail.getTeacherId();
					if(fileName!=null && fileName!=""){
						counter++;
						sb.append("<a href='javascript:void(0);' id='commfile"+counter+"' onclick=\"downloadCommunication('"+filePath+"','"+fileName+"','commfile"+counter+"');"+windowFunc+"\"><span class='icon-paper-clip icon-large iconcolor'></span></a>");
					}
					sb.append("</td>");
					String name="";
					if(tsDetails.getUserMaster()!=null){
						UserMaster um=tsDetails.getUserMaster();
						if(um.getMiddleName()!=null){
							name=um.getFirstName()+" "+um.getMiddleName()+" "+um.getLastName();
						}else{
							name=um.getFirstName()+" "+um.getLastName();
						}
					}
					sb.append("<td>"+name+"</td>");
					sb.append("<td>"+Utility.convertDateAndTimeToUSformatOnlyDate(tsDetails.getCreatedDateTime())+"</td>");
					/*sb.append("<td>");
					//if(!tsDetails.isFinalizeStatus()&& userMaster.getEntityType()!=1){
					if((tsDetails.isFinalizeStatus()) && userMaster.getEntityType()!=1){
						countSave++;
							sb.append("<a  href='javascript:void(0);' onclick=\"return editShowStatusNote('"+tsDetails.getTeacherStatusNoteId()+"')\" >"+Utility.getLocaleValuePropByKey("lblEdit", locale)+"</a> | <a  href='javascript:void(0);' onclick=\"return deleteStatusNoteChk('"+tsDetails.getTeacherStatusNoteId()+"','note')\" >"+Utility.getLocaleValuePropByKey("lnkDlt", locale)+"</a>");
					}else{
						sb.append("&nbsp");
					}
					sb.append("</td>");*/
					sb.append("</tr>");
				}
			}
	
			if(lstTeacherStatusNotes == null || lstTeacherStatusNotes.size()==0){
				sb.append("<tr>");				
				sb.append("<td colspan=4>"+Utility.getLocaleValuePropByKey("lblNoNoteAdd", locale)+"</td>");
				sb.append("</tr>");
			}
			sb.append("</table>");
			sb.append(PaginationAndSorting.getPaginationTrippleGrid2(request,totaRecord,noOfRow, pageNo));
			sb.append("</td></tr></table>");
		
    	return sb.toString();
    }
    
    
    public static String convertUSformatOnlyYear(Date dat)
	{
		try {
			SimpleDateFormat sdf=new SimpleDateFormat("yyyy");
			return sdf.format(dat);
		} catch (Exception e) {
			return "";
		}
	}
    
    public String callWorkFlowFromOnboarding(Integer teacherId,Long jobForTeacherId,String nodeName,String statusNotes,String secStatusId)
    {

		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		
		String msgStatus = "";
		UserMaster userMaster = (UserMaster)session.getAttribute("userMaster");
		TeacherDetail teacherDetail = null;
		JobForTeacher jobForTeacher = null;
		JobOrder jobOrder = null;
		SecondaryStatus secStatus = null;
		logger.info(" teacherId get :: "+teacherId);
		
		if(teacherId!=null)
			teacherDetail = teacherDetailDAO.findById(teacherId, false, false);
		
		if(jobForTeacherId!=null){
			jobForTeacher = jobForTeacherDAO.findById(jobForTeacherId, false, false);
			jobOrder = jobForTeacher.getJobId();
		}
		
		if(secStatusId!=null && secStatusId.length()>0)
			secStatus = secondaryStatusDAO.findById(Integer.parseInt(secStatusId), false, false);
		
		if(statusNotes!=null && statusNotes.length()>0){
			TeacherStatusNotes tsn = new TeacherStatusNotes();
			tsn.setTeacherDetail(teacherDetail);
			tsn.setUserMaster(userMaster);
			tsn.setJobOrder(jobForTeacher.getJobId());
			if(secStatus!=null && secStatus.getStatusMaster()!=null){
				tsn.setStatusMaster(secStatus.getStatusMaster());
			}
			else {
				tsn.setSecondaryStatus(secStatus);
			}
			if(statusNotes!=null){
				tsn.setStatusNotes(statusNotes);
			}
			tsn.setCreatedDateTime(new Date());
			tsn.setFinalizeStatus(true);
			teacherStatusNotesDAO.makePersistent(tsn);
		}
		
		JobCategoryMaster jobCategoryMaster = null;
		if(jobOrder.getJobCategoryMaster().getParentJobCategoryId()!=null)
			jobCategoryMaster = jobOrder.getJobCategoryMaster().getParentJobCategoryId();
		else
			jobCategoryMaster = jobOrder.getJobCategoryMaster();
		SecondaryStatus secondaryStatus = secondaryStatusDAO.findByNameAndJobCategory(nodeName,jobCategoryMaster);
		logger.info(" jobForTeacherId get :: "+jobForTeacherId);
		StatusMaster statusMaster = null;
		SecondaryStatus sec = null;
		if(secondaryStatus!=null && secondaryStatus.getStatusMaster()!=null){
			statusMaster = secondaryStatus.getStatusMaster();
			logger.info(" statusMaster ::: "+statusMaster.getStatusId());
		}
		else{
			sec = secondaryStatus;
			if(sec!=null)
				logger.info(" sec :: "+sec.getSecondaryStatusId());
		}
			 List<MQEvent> mqEventData = mqEventDAO.findMQEventByTeacher(teacherDetail);
			 
	    	 boolean callApi = false;
	    	 boolean invWf1Flag = false;
	    	 boolean invWf2Flag = false;
	    	// boolean invWf3Flag = false;
	    	 boolean wf1Com = false;
	    	 boolean wf2Com = false;
	    	 //boolean wf3Com = false;
	    	 
	    	 if(teacherDetail!=null && teacherDetail.getKSNID()!=null)
	    		 callApi = true;
	    	 
	    	 	String INVWF1 = Utility.getLocaleValuePropByKey("lblINVWF1", locale);
				String WF1COM = Utility.getLocaleValuePropByKey("lblWF1COM", locale);
				String INVWF2 = Utility.getLocaleValuePropByKey("lblINVWF2", locale);
				String WF2COM = Utility.getLocaleValuePropByKey("lblWF2COM", locale);
				String INVWF3 = Utility.getLocaleValuePropByKey("lblINVWF3", locale);
	    	 if(callApi && nodeName.equalsIgnoreCase(INVWF1)){
	    		 try {
						saveStatusResetINF(jobForTeacherId,0,secStatusId);
					} catch (Exception e) {
						e.printStackTrace();
					}
	    	 }				
	    	 else if(callApi && nodeName.equalsIgnoreCase(INVWF2)){
	    		 if(mqEventData!=null && mqEventData.size()>0){
	    			 for(MQEvent mq : mqEventData){
	    				 if(mq.getEventType().equalsIgnoreCase(INVWF1) && mq.getWorkFlowStatusId()!=null && mq.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
	    					 invWf1Flag = true;
	    				 if(mq.getEventType().equalsIgnoreCase(WF1COM) && mq.getWorkFlowStatusId()!=null && (mq.getWorkFlowStatusId()==null || mq.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
	    					 wf1Com = true;
	    			 }
	    			 
	    			 if(invWf1Flag && wf1Com){
	    				 callApi = true;
	    				 try {
							saveStatusResetINF(jobForTeacherId,0,secStatusId);
						} catch (Exception e) {
							e.printStackTrace();
						}
	    			 }
	    			 else{
	    				 callApi = false;
	    				 msgStatus="This workflow cannot be selected because the 1 - Initial Application Forms workflow has not been completed.";
	    			 }
	    		 }
	    	 }
	    	 else if(callApi && nodeName.equalsIgnoreCase(INVWF3)){
	    		 if(mqEventData!=null && mqEventData.size()>0){
	    			 for(MQEvent mq : mqEventData){
	    				 if(mq.getEventType().equalsIgnoreCase(INVWF1) && mq.getWorkFlowStatusId()!=null && mq.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
	    					 invWf1Flag = true;
	    				 if(mq.getEventType().equalsIgnoreCase(WF1COM) && mq.getWorkFlowStatusId()!=null && (mq.getWorkFlowStatusId()==null || mq.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
	    					 wf1Com = true;
	    				 if(mq.getEventType().equalsIgnoreCase(INVWF2) && mq.getWorkFlowStatusId()!=null && mq.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblAWAIT", locale))){
	    					 invWf2Flag = true;
	    					 wf2Com = true;
	    				 }
	    				 else{
	    					 if(mq.getEventType().equalsIgnoreCase(INVWF2) && mq.getStatus()!=null && mq.getWorkFlowStatusId()!=null && mq.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale)))
	    						 invWf2Flag = true;
	    					 if(mq.getEventType().equalsIgnoreCase(WF2COM) && mq.getStatus()!=null && mq.getWorkFlowStatusId()!=null && (mq.getWorkFlowStatusId()==null || mq.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))))
	    						 wf2Com = true;
	    					 if(mq.getEventType().equalsIgnoreCase(WF2COM) && mq.getWorkFlowStatusId()!=null && mq.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblCOM", locale))){
	    						 wf2Com = true;
	    					 	invWf2Flag = true;
	    					 }
	    				 }
	    			 }
	    			 
	    			 if(invWf1Flag && wf1Com && invWf2Flag && wf2Com)
	    			 {
	    				 callApi = true;
	    				 try {
								saveStatusResetINF(jobForTeacherId,0,secStatusId);
							} catch (Exception e) {
								e.printStackTrace();
							}
	    			 }
	    			 else{
	    				 callApi = false;
	    				 if(!invWf1Flag || !wf1Com)
	    					 msgStatus="This workflow cannot be selected because the 1 - Initial Application Forms workflow has not been completed.";
	    				 else if(!invWf2Flag || !wf2Com)
	    					 msgStatus="This workflow cannot be selected because the 2 - Conditional Offer workflow has not been completed.";
	    			 }
	    		 }
	    	 }
	    	 
			int counter=0;
			String result ="";
			if(callApi){
				try {
					manageStatusAjax.messageQueue(statusMaster, sec, jobForTeacher, userMaster, "KSNID", "");
					logger.info("Time Start :::::::::::::::::::::::::::::::: "+new Date());
					while(counter<8){
						ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
						logger.info("Time Mid "+counter+": "+new Date());
						counter++;
						try {
							Runnable worker = new MQEventThread();
							executor.execute(worker);
						} catch (Exception e) {
						} 
						executor.shutdown();
						while (!executor.isTerminated()){}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				
				MQEvent mqEventWF=mqEventDAO.findEventByEventType(teacherDetail, nodeName).get(0);
				msgStatus="Data sent to Kelly but no response received";
				if(mqEventWF!=null && mqEventWF.getMsgStatus()!=null){
					msgStatus=mqEventWF.getMsgStatus();
					if(msgStatus.contains("Success")){
					      msgStatus = Utility.getLocaleValuePropByKey("lblWorkFlowInitiated", locale);
					}
				}else{
					logger.info("Inside MqEvent::: Else");
				}
				
			}
			else if(!callApi && ( nodeName.equalsIgnoreCase(INVWF1)|| nodeName.equalsIgnoreCase(INVWF3) || nodeName.equalsIgnoreCase(INVWF2))){
				try {
					manageStatusAjax.callMessageQueue_dummy(statusMaster, sec, jobForTeacher, userMaster,nodeName);									
				} catch (Exception e) {
					e.printStackTrace();
				}				
				MQEvent mqEventWF=mqEventDAO.findEventByEventType(teacherDetail, nodeName).get(0);
				if(nodeName.equalsIgnoreCase(INVWF2))
					msgStatus="This workflow cannot be selected because the 1 - Initial Application Forms workflow has not been completed.";
				else if(nodeName.equalsIgnoreCase(INVWF3))
					msgStatus="This workflow cannot be selected because the 2 - Initial Application Forms workflow has not been completed.";
				else if(nodeName.equalsIgnoreCase(INVWF1))
					msgStatus="INV WF1 can not be initiate until Link to KSN is complete for the Talent.";
				if(mqEventWF!=null && mqEventWF.getMsgStatus()!=null){
					msgStatus=mqEventWF.getMsgStatus();
				}else{
					logger.info("Inside MqEvent::: Else");
				}
			}
					
		return msgStatus;
    }
    
    public String showTagIconFile(SecondaryStatusMaster secondaryStatusMaster)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		String path="";
		String source="";
		String target="";
		try 
		{
			if(secondaryStatusMaster.getDistrictMaster()!=null){
				
				source = Utility.getValueOfPropByKey("districtRootPath")+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();
				target = context.getServletContext().getRealPath("/")+"/district/"+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/";
			}else{
				source = Utility.getValueOfPropByKey("rootPath")+"tags/"+secondaryStatusMaster.getPathOfTagsIcon();
				target = context.getServletContext().getRealPath("/")+"/tags/";
			}
			
			
		    File sourceFile = new File(source);
	        File targetDir = new File(target);
	        if(!targetDir.exists())
	        	targetDir.mkdirs();
	        
	        File targetFile = new File(targetDir+"/"+sourceFile.getName());
	        FileUtils.copyFile(sourceFile, targetFile);
	        
	        String ext = null;
	        String s = sourceFile.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	        }
	        
	        /*if(ext.equals("jpeg") || ext.equals("png") || ext.equals("gif") || ext.equals("jpg")){
	        	System.out.println("tag path >>>>>> "+targetDir+"/"+sourceFile.getName());
	        	ImageResize.resizeTag(targetDir+"/"+sourceFile.getName());
	        }*/
	        if(secondaryStatusMaster.getDistrictMaster()!=null){
	        		path = Utility.getValueOfPropByKey("contextBasePath")+"district/"+secondaryStatusMaster.getDistrictMaster().getDistrictId()+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();	
	        }else{
	        	path = Utility.getValueOfPropByKey("contextBasePath")+"/tags/"+secondaryStatusMaster.getPathOfTagsIcon();
	        }
	       	 
		}catch (Exception e){
			//e.printStackTrace();
		}		
		return path;
	}
    
    public String createPDFForKellyOnboarding(List<JobForTeacher> jft)
    {
    	WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)){
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
    	
		String fileName = "";
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) {
			//return "false";
		}else
			userMaster = (UserMaster)session.getAttribute("userMaster");

		
		Document document = new Document(); 
		
		try{
			File file = new File("D:/adddd.pdf");
			FileOutputStream fileout = new FileOutputStream(file);
			PdfWriter writer = PdfWriter.getInstance(document, fileout);
			document.open();
			document.add(new Paragraph("A Hello World PDF document."));
			document.close();
	        writer.close();
			
		}catch(Exception e){e.printStackTrace();}
		
    	return fileName;
    }
    
    public void saveStatusResetINF(Long jobForTeacherId, Integer statusNotes,String secStatusId)
	{
    	System.out.println(":::::::::::::;saveStatusResetINF >>>>:::::::::::");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		try
		{
			CandidateGridService cgService=new CandidateGridService();
			JobForTeacher jft = jobForTeacherDAO.findById(jobForTeacherId, false, false);
			SecondaryStatus secondaryStatus = null;
			if(secStatusId!=null && secStatusId.length()>0){
				secondaryStatus = secondaryStatusDAO.findById(Integer.parseInt(secStatusId), false, false);
			}
			if(jft!=null){
				List<SecondaryStatus> lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jft.getJobId());
				Map<String,Integer> statusHistMap=new HashMap<String, Integer>();
				Map<String,StatusMaster> statusMasterMap=new HashMap<String,StatusMaster>();
				Map<String,SecondaryStatus> secondaryStatusMap=new HashMap<String,SecondaryStatus>();
				LinkedHashMap<String,String> statusNameMap=new LinkedHashMap<String,String>();
				try{
					int counter=0; 
					for(SecondaryStatus tree: lstTreeStructure)
					{
						if(tree.getSecondaryStatus()==null)
						{
							if(tree.getChildren().size()>0){
								counter=cgService.getAllStatusForHistory(statusMasterMap,secondaryStatusMap,tree.getChildren(),statusHistMap,statusNameMap,counter);
							}
						}
					}
				}catch(Exception e){}
				System.out.println("statusNameMap:::::::::::::::::::::::::>>>>>::::::::::::::::::::::::::::::::"+statusNameMap.size());
				
				List<String> statusNameList=new ArrayList<String>(); 
				List<StatusMaster> statusMasters=new ArrayList<StatusMaster>();
				
				/*statusMasters.add(WorkThreadServlet.statusMap.get("hird"));
				statusMasters.add(WorkThreadServlet.statusMap.get("widrw"));
				statusMasters.add(WorkThreadServlet.statusMap.get("w"));
				statusMasters.add(WorkThreadServlet.statusMap.get("hird"));*/
				List<SecondaryStatus> secondaryStatusList=new ArrayList<SecondaryStatus>();
				boolean nextStatus=false;
				for (Map.Entry<String,String> entry : statusNameMap.entrySet()) {
					  String key = entry.getKey();
					  String value = entry.getValue();
					  if(value!=null && value.equalsIgnoreCase(secondaryStatus.getSecondaryStatusName())){
						  nextStatus=true;
						  System.out.println("secondaryStatus.getSecondaryStatusName()::::>>>>>>>>>>>>::::::::::::::::"+secondaryStatus.getSecondaryStatusName());
					  }
					  
					  if(key!=null && nextStatus){
						  if(!value.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblScreening", locale))){
							  statusNameList.add(value);
							  if(key.contains("##0")){
								  statusMasters.add(statusMasterMap.get(key));
							  }else{
								  secondaryStatusList.add(secondaryStatusMap.get(key));
							  }
						  }
					  }
					  System.out.println("Key ::::::>>>::::::"+key);
					  System.out.println("Value ::::>>>:::::::"+value);
				}
				System.out.println("statusNameList:::::::::::::::"+statusNameList.size());
				System.out.println("statusMasters:::::::::::::"+statusMasters.size());
				System.out.println("secondaryStatusList:::::::::::::"+secondaryStatusList.size());
				//*********Undo Status ************//*
				try{
					List<TeacherStatusHistoryForJob>  statusHistoryObjList=teacherStatusHistoryForJobDAO.findByTeacherStatusListForUndo(jft.getTeacherId(),jft.getJobId(),statusMasters,secondaryStatusList);
					System.out.println("statusHistoryObjList:::::::::::::::;"+statusHistoryObjList.size());
					for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : statusHistoryObjList) {
						teacherStatusHistoryForJob.setStatus("I");
						teacherStatusHistoryForJob.setHiredByDate(null);
						teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
						teacherStatusHistoryForJob.setUserMaster(userMaster);
						teacherStatusHistoryForJobDAO.updatePersistent(teacherStatusHistoryForJob);
					}

				}catch(Exception e){
					e.printStackTrace();
				}
				commonService.undoJobStatus(jft);
				try{
					if(statusNameList.size()>0){  
					  List<MQEvent>  mqEventList=mqEventDAO.findNodeStatusByTeacherUndoINF(jft.getTeacherId(),statusNameList);
				      System.out.println("mqEventList::::::::::::::::"+mqEventList.size());
				      List<StatusNodeColorHistory> statusNodeLogHist = statusNodeColorHistoryDAO.getStatusColorHistoryByTeacher_OP(jft.getTeacherId());
				      Map<String,String> colorWithTeacher = new HashMap<String, String>();
				      if(statusNodeLogHist.size()>0){
				    	  for(StatusNodeColorHistory s : statusNodeLogHist){
				    		  colorWithTeacher.put(jft.getTeacherId().getTeacherId()+"##"+s.getMqEvent().getEventType(), s.getCurrentColor());
				    	  }
				      }
				      CandidateGridService candidateGridService = new CandidateGridService();
				      for (MQEvent mQEvent : mqEventList) {

				    	  if(mQEvent.getEventType()!=null && !mQEvent.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblScreening", locale))){
				    		  String color = "Grey";
				    		  StatusNodeColorHistory statusNodeColorHistory = new StatusNodeColorHistory();
				    		  HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
				    		  headQuarterMaster.setHeadQuarterId(1);
				    		  statusNodeColorHistory.setHeadQuarterMaster(headQuarterMaster);
				    		  statusNodeColorHistory.setCreatedDateTime(new Date());
				    		  statusNodeColorHistory.setTeacherDetail(mQEvent.getTeacherdetail());

				    		  // color = candidateGridService.getNodeColorWithMqEvent(mQEvent, null);
				    		  if(colorWithTeacher.size()>0)
				    			  color = colorWithTeacher.get(mQEvent.getTeacherdetail().getTeacherId()+"##"+mQEvent.getEventType());

				    		  if(color==null || color.length()==0)
				    			  color = "Grey";

				    		  mQEvent.setStatus(null);
				    		  mQEvent.setWorkFlowStatusId(null);
				    		  mQEvent.setSecondaryStatus(null);
				    		  mQEvent.setStatusMaster(null);
				    		  mQEvent.setAckStatus(null);
				    		  mQEvent.setTmInitiate(null);
				    		  mqEventDAO.updatePersistent(mQEvent);

				    		  statusNodeColorHistory.setMqEvent(mQEvent);
				    		  statusNodeColorHistory.setPrevColor(color);
				    		  statusNodeColorHistory.setCurrentColor("Grey");
				    		  statusNodeColorHistory.setUserMaster(userMaster);
				    		  statusNodeColorHistoryDAO.makePersistent(statusNodeColorHistory);
				    	  }
				      }
				      }
				}catch(Exception e){
					e.printStackTrace();
				}
					
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
    
    private void getListSubFilter(List<JobForTeacher> listJFT,Map<String,MQEvent> mapNodeStatus,Integer subFilterId,Map<Integer,Integer> spStatusMap,Map<String,String> mapHistrory){
    	List<JobForTeacher> lstJFTremove=new ArrayList<JobForTeacher> (listJFT);
    	
    	if(subFilterId!=null && subFilterId>0 && subFilterId<=6)
    	for(JobForTeacher jobForTeacherObj : lstJFTremove){
    		MQEvent mqEventKSN = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
    		MQEvent mqEventSPS = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblSPStatus", locale));
    		MQEvent mqEventINVWF1 = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF1", locale));
    		MQEvent mqEventINVWF2 = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF2", locale));
    		MQEvent mqEventINVWF3 = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblINVWF3", locale));
    		MQEvent mqEventWF1COM = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF1COM", locale));
    		MQEvent mqEventWF2COM = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF2COM", locale));
    		MQEvent mqEventWF3COM = mapNodeStatus.get(jobForTeacherObj.getTeacherId().getTeacherId()+"-"+Utility.getLocaleValuePropByKey("lblWF3COM", locale));
    		  Boolean linkToKSN = false;
			  Boolean INVWF1 = false;
			  Boolean INVWF2 = false;
			  Boolean INVWF3 = false;
			  Boolean WF1COM = false;
			  Boolean WF2COM = false;
			  Boolean WF3COM = false;
			  Boolean spComp=false;
			  
    		if(OPTION_MAP.get(subFilterId).equals("Need INV to WF1")){
				   linkToKSN = false;
				   INVWF1 = true;
				   INVWF2 = false;
				   INVWF3 = false;
				   WF1COM = false;
				   WF2COM = false;
				   WF3COM = false;
						if(mqEventKSN!=null){
							if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
								linkToKSN=true;
							}
						}
						if(mqEventINVWF1!=null){
							if(mqEventINVWF1.getStatus()!=null && mqEventINVWF1.getAckStatus()!=null && (mqEventINVWF1.getAckStatus().equals("Failure") || mqEventINVWF1.getAckStatus().equals("Failed"))){								INVWF1 = false;
								INVWF1 = false;
							}
						}
						else
							INVWF1 = false;
						
						if(mqEventINVWF2==null)
							INVWF2 = true;
						if(mqEventINVWF3==null)
							INVWF3 = true;
						if(mqEventWF1COM==null)
							WF1COM = true;
						if(mqEventWF2COM==null)
							WF2COM = true;
						if(mqEventWF3COM==null)
							WF3COM = true;
						
						if(!linkToKSN || INVWF1 || !WF1COM || !INVWF2 || !WF2COM || !INVWF3 || !WF3COM)
							listJFT.remove(jobForTeacherObj);
					
				}
			  
			  if(OPTION_MAP.get(subFilterId).equals("Smart Practices Incomplete")){
				  
				  String sStatus=jobForTeacherObj.getTeacherId().getTeacherId()+"##"+jobForTeacherObj.getJobId().getJobId()+"##W";
				  Integer spMapVal = spStatusMap.get(jobForTeacherObj.getTeacherId().getTeacherId());
				  if((spMapVal!=null && (spMapVal>=1))){
					  spComp=true;
				  }
				  if(spMapVal== null && (jobForTeacherObj.getJobId().getJobCategoryMaster().getPreHireSmartPractices()==null || !jobForTeacherObj.getJobId().getJobCategoryMaster().getPreHireSmartPractices()))
					  spComp=true;
				  
				  if(mapHistrory.get(sStatus)!=null)
					  spComp = true;
				  
				  if(spComp)
					  listJFT.remove(jobForTeacherObj);
			  }
			  
    		if(OPTION_MAP.get(subFilterId).equals("Workflow 1 Incomplete")){
    			   linkToKSN = true;
				   INVWF1 = false;
				   INVWF2 = false;
				   INVWF3 = false;
				   WF1COM = false;
				   WF2COM = false;
				   WF3COM = false;
    			
    			if(mqEventKSN!=null){
					if(mqEventKSN.getStatus()!=null && !mqEventKSN.getStatus().equalsIgnoreCase("C")){
						linkToKSN=false;
					}
				}
    			
    			if(mqEventKSN==null || mqEventINVWF1==null)
   	        	 	INVWF1 = false;
   	        	 
   	        	 	if(mqEventINVWF1!=null){
   	        	 		if(mqEventINVWF1.getAckStatus()!=null && ((mqEventINVWF1.getWorkFlowStatusId()==null || (mqEventINVWF1.getWorkFlowStatusId()!=null && mqEventINVWF1.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase("INIT")))) && mqEventWF1COM!=null &&  mqEventWF1COM.getWorkFlowStatusId()!=null && mqEventWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
   	        	 				INVWF1 = true;
   	        	 		}
   	        	 	}
   	        	 	
   	        	 if(mqEventWF1COM!=null && mqEventWF1COM.getWorkFlowStatusId()!=null && (mqEventWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equals("COM") ))
						INVWF1 = false;
   	        	 if((mqEventINVWF2!=null && (mqEventINVWF2.getAckStatus()==null || mqEventINVWF2.getAckStatus()!=null)))
   	        		INVWF1 = false;
   	        	 if(mqEventWF2COM!=null)
   	        		INVWF1 = false;
   	        	if( (mqEventINVWF3!=null && (mqEventINVWF3.getAckStatus()==null || mqEventINVWF3.getAckStatus()!=null)))
   	        		INVWF1 = false;
   	        	 if(mqEventWF3COM!=null)
   	        		INVWF1 = false;
   	        	 	
    	         if(!(linkToKSN && INVWF1))
					 listJFT.remove(jobForTeacherObj);	
    		}
    		if(OPTION_MAP.get(subFilterId).equals("Need INV to WF2")){
    			linkToKSN = true;
    			INVWF2 = false;
    			

    			if(mqEventKSN!=null){
    				if(mqEventKSN.getStatus()!=null && !mqEventKSN.getStatus().equalsIgnoreCase("C")){
    					linkToKSN=false;
    				}
    			}

    			if(mqEventKSN==null || mqEventINVWF1==null || mqEventWF1COM==null)
    				INVWF2 = false;

    			if(mqEventINVWF1!=null){
    				if(mqEventINVWF1.getAckStatus()!=null && (mqEventINVWF1.getWorkFlowStatusId()!=null && mqEventINVWF1.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase("COM")) && mqEventWF1COM!=null &&  mqEventWF1COM.getWorkFlowStatusId()!=null && mqEventWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
    					INVWF2 = true;
    				}
    			}
    			if(mqEventWF1COM!=null && mqEventWF1COM.getWorkFlowStatusId()!=null && mqEventWF1COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase("COM"))
    				INVWF2 = true;

    			
    			if(mqEventWF2COM!=null)
    				INVWF2 = false;
    			if( (mqEventINVWF3!=null && (mqEventINVWF3.getAckStatus()==null || mqEventINVWF3.getAckStatus()!=null)))
    				INVWF2 = false;
    			if(mqEventWF3COM!=null)
    				INVWF2 = false;

    			if(!(linkToKSN && INVWF2))
    				listJFT.remove(jobForTeacherObj);	
    		}
			  
    		if(OPTION_MAP.get(subFilterId).equals("Workflow 2 Incomplete")){

    			linkToKSN = true;
    			INVWF2 = false;


    			if(mqEventKSN!=null){
    				if(mqEventKSN.getStatus()!=null && !mqEventKSN.getStatus().equalsIgnoreCase("C")){
    					linkToKSN=false;
    				}
    			}

    			if(mqEventKSN==null || mqEventINVWF1==null || mqEventWF1COM==null)
    				INVWF2 = false;

    			if(mqEventINVWF2!=null){
	        	 		if(mqEventINVWF2.getAckStatus()!=null && ((mqEventINVWF2.getWorkFlowStatusId()==null || (mqEventINVWF2.getWorkFlowStatusId()!=null && mqEventINVWF2.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase("INIT")))) && mqEventWF2COM!=null &&  mqEventWF2COM.getWorkFlowStatusId()!=null && mqEventWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblY", locale))){
	        	 			INVWF2 = true;
	        	 		}
	        	 	}
	        	 	
	        	 if(mqEventWF2COM!=null && mqEventWF2COM.getWorkFlowStatusId()!=null && (mqEventWF2COM.getWorkFlowStatusId().getWorkFlowStatus().equals("COM") ))
					INVWF1 = false;
	        	 if((mqEventINVWF3!=null && (mqEventINVWF3.getAckStatus()==null || mqEventINVWF3.getAckStatus()!=null)))
	        		INVWF1 = false;
	        	 if(mqEventWF3COM!=null)
	        		INVWF1 = false;
	        	 	
	         if(!(linkToKSN && INVWF2))
				 listJFT.remove(jobForTeacherObj);

    		}
			  
			  if(OPTION_MAP.get(subFilterId).equals("Unlinked")){
				   linkToKSN = false;
						if(mqEventKSN!=null){
							if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("C")){
								linkToKSN=true;
							}
							else if(mqEventKSN.getStatus()!=null && mqEventKSN.getStatus().equalsIgnoreCase("R") && (mqEventKSN.getAckStatus()==null || (mqEventKSN.getAckStatus()!=null && (mqEventKSN.getAckStatus().equalsIgnoreCase("Success") || mqEventKSN.getAckStatus().equalsIgnoreCase("Successful"))))){
								linkToKSN=true;
							}
						}					
					 if(linkToKSN)
						 listJFT.remove(jobForTeacherObj);	
				}
			}
    
    }
    
    public boolean executeRestCallForAllTeacher(UserMaster userMaster,JobForTeacher jft, SecondaryStatus ss,TeacherDetail teacherDetail, Map<TeacherDetail,List<StatusNodeColorHistory>> teacherListByTeacherObj , Map<Integer,MQEvent> mQEventMAP, Connection connection,String ipRemoteAddress, ApplicationContext APPLICATIONCONTEXT) throws Exception
    {
	    	if(teacherDetail!=null){
	    				StatusMaster statusMaster=null;
	    				if(ss.getStatusMaster()!=null){
	    					statusMaster = ss.getStatusMaster();
	    					ss=null;
	    				}
	    				try {
	    					manageStatusAjax.messageQueueForOnboarding(true,statusMaster,ss,jft,userMaster,"","",connection,teacherListByTeacherObj,mQEventMAP,ipRemoteAddress,APPLICATIONCONTEXT);
	    				} catch (Exception e) {
	    					e.printStackTrace();
	    				} 
	    	}
    	return true;
    }
    
    public String getNodeStatusLogForSPS(String nodeName, String teacherId,String jobForTeacherId,String statusId){

    	StringBuffer sb =	new StringBuffer();
    	int counter =0;
    	int showHideResetButton=getResetShowOrNot(nodeName,teacherId)?1:0;
    	TeacherDetail teacherDetail = null;
    	JobForTeacher jobForTeacher = null;
    	JobOrder jobOrder = null;
    	Integer jobCategoryId = 0;
    	List<CommunicationsDetail> communicationDetailList = new ArrayList<CommunicationsDetail>();
    	teacherDetail = teacherDetailDAO.findById(Integer.parseInt(teacherId), false, false);
    
    	List<StatusNodeColorHistory> statusNodeColorList = new ArrayList<StatusNodeColorHistory>();
    
		
		try {
			statusNodeColorList = statusNodeColorHistoryDAO.getStatusColorHistoryByTeacherAndStatus(teacherDetail,Integer.parseInt(statusId));
			
			System.out.println(" nodeName ::: "+nodeName);
			if(statusNodeColorList.size()>0){
				for(StatusNodeColorHistory snch : statusNodeColorList){
					if(statusId!=null && statusId.length()>0 && nodeName.equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblSPS", locale))){
						CommunicationsDetail comm = new CommunicationsDetail();
						comm.setCommType("SPS");
						if(snch.getUpdateDateTime()!=null)
							comm.setCreatedDateTime(snch.getUpdateDateTime());
						else
							comm.setCreatedDateTime(snch.getCreatedDateTime());
						comm.setLastUpdateDateTime(snch.getCreatedDateTime());
						comm.setUpdateDateTime(snch.getUpdateDateTime());
						
						if(snch.getUserMaster()!=null){
							comm.setUserFirstName(snch.getUserMaster().getFirstName());
							comm.setUserLastName(snch.getUserMaster().getFirstName());
						}
						comm.setCurrentColor(snch.getCurrentColor());
						comm.setPrevColor(snch.getPrevColor());
						//comm.setMqEvent(snch.getMqEvent());
						communicationDetailList.add(comm);
					}
				}
			}
			String fields="";
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
    	String year="";
    	String preYear="";
    	sb.append("<table border='0' width='760' class='tablecgtbl'>");
    	sb.append("<tr><td>");
    	sb.append("<table border='0' style='padding:0px;' cellpadding=0 cellspacing=0  width='100%' class='tablecgtbl'>");
    	
    	//////////////////////////// Start :: SPS /////////////////////////////////////
    	try {
    		if(communicationDetailList.size()>0 && communicationDetailList.get(0).getCommType().equalsIgnoreCase("SPS")){
    			if(communicationDetailList.size()>0){
        			Collections.sort(communicationDetailList,CommunicationsDetail.createdDate);
        			for(CommunicationsDetail commDetail : communicationDetailList){
        				if(commDetail.getUpdateDateTime()!=null)
        					year= convertUSformatOnlyYear(commDetail.getUpdateDateTime());
        				else
        					year= convertUSformatOnlyYear(commDetail.getCreatedDateTime());
        				counter++;
        				if(!preYear.equals(year)){
        					sb.append("<tr>");
        					sb.append("<td colspan=3 style='text-align:right;padding-right:8px;padding-bottom:5px;verical-align:bottom;'><span class=\"txtyearbg\">"+year+"</span></td><td>&nbsp;</td>");
        					sb.append("<tr>");
        					sb.append("<tr>");
        					sb.append("<td width='15%' style='height:10px;'>&nbsp;</td>");
        					sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=10>");
        					sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
        					sb.append("</tr>");
        				}
        				preYear=year;
        				sb.append("<tr>");
        				if(commDetail.getCommType().equalsIgnoreCase("SPS") && commDetail.getUpdateDateTime()!=null)
        					sb.append("<td width='23%' style='text-align:right;vertical-align:middle;position:relative;top:-3px;'>"+Utility.convertDateAndTimeFormatForCommunication(commDetail.getUpdateDateTime()));
        				else
        					sb.append("<td width='23%' style='text-align:right;vertical-align:middle;position:relative;top:-3px;'>"+Utility.convertDateAndTimeFormatForCommunication(commDetail.getCreatedDateTime()));
        				
        				if(commDetail.getActivityType()!=null && commDetail.getActivityType().equalsIgnoreCase("create"))
    						sb.append("<span class='note-com'><img src=\"images/add_circle.png\" style=\"width:20px;    margin-left: 3px; height:auto;\"></span>");
    					else
    						sb.append("<span class='note-com'><img src=\"images/edit_circle.png\" style=\"width:20px;    margin-left: 3px; height:auto;\"></span>");
        				
        				sb.append("</td><td style='text-align:right;vertical-align:middle;padding-left:12px;' width='8px;' height='10'>");
        				sb.append("<span class=\"vertical-line\"></span></td>");
        				sb.append("<td  style='text-align:right;vertical-align:middle;' width=20>");;
        				sb.append("<hr style=\"color: #f00; background:#007AB4; width: 100%; height: 3px;\"/></td>");
        				sb.append("<td class='commbgcolor'  style='padding:5px;vertical-align:middle;'>");
        				
        				if(commDetail.getCommType().equalsIgnoreCase("SPS")){
        					sb.append("<span><b>Action:</b> Integration Update </span><br/>");
        					sb.append("<span><b>User:</b> SPS </span><br/>");
        					
        						sb.append("<span><b>Updated Field Details: </b></span><br/>");

        						sb.append("<span><table class='tablecgtbl' style='width:100%;'>");
        						sb.append("<tr class=\"txtyearbg\" style='height:15px;'><td>#</td>");
        						sb.append("<td>Update</td>");
        						sb.append("<td>Previous Value</td>");
        						sb.append("<td>Current Value</td></tr>");

        						sb.append("<tr>");
        						sb.append("<td>1.</td>");
        						sb.append("<td>Date/Time</td>");
        						sb.append("<td>"+commDetail.getLastUpdateDateTime()+"</td>");
        						if(commDetail.getUpdateDateTime()!=null)
        							sb.append("<td>"+commDetail.getUpdateDateTime()+"</td>");
        						else
        							sb.append("<td>"+commDetail.getCreatedDateTime()+"</td>");
        						sb.append("</tr>");

        						sb.append("<tr>");
        						sb.append("<td>2.</td>");
        						sb.append("<td>Status</td>");
        						sb.append("<td>"+commDetail.getPrevColor()+"</td>");
        						sb.append("<td>"+commDetail.getCurrentColor()+"</td>");
        						sb.append("</tr>");
        						sb.append("</table></span>");
        				}
        				else {
        					sb.append("<span class='com-user' style='line-height:14px;'>Action: "+(commDetail.getActivityType().equalsIgnoreCase("create")==true?"Create":"Update") +"</span><br/>");
    						sb.append("<span class='com-user' style='line-height:14px;'>User: "+commDetail.getUserFirstName()+" "+commDetail.getUserLastName()+"</span><br/>");
    						sb.append("<span class='com-user' style='line-height:14px;'>Fields that were updated: "+commDetail.getLogColumn()+"</span>");
        				}
        				
        				sb.append("</td>");
        				sb.append("</tr>");
        				if(counter<communicationDetailList.size()){
        					sb.append("<tr>");
        					sb.append("<td width='15%' style='height:20px;'>&nbsp;</td>");
        					sb.append("<td style='text-align:right;vertical-align:middle;padding-left:12px;'  height=20>");
        					sb.append("<span class=\"vertical-line\"></span></td><td ></td>");
        					sb.append("</tr>");
        				}
        			}
        			sb.append("</td></tr>");
        		}
    		}
    		//logger.info(" jobForTeacher.getJobId().getJobId() ::::::::::: "+jobForTeacher.getJobId().getJobId());
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	////////// End:: SPS ////////////////////////////////////////////////////
    	
    	
    	sb.append("<input type='hidden' value='"+showHideResetButton+"' id='showHideResetButtonONB' name='showHideResetButtonONB' />");
    	sb.append("</table>");

    	return sb.toString();
    }
    
    
    
    public class CallSOAPAndRESTServiceThread implements Runnable{
    	private String[] teacher=null;
    	private List<TeacherDetail> teacherDetailList=null;
    	private UserMaster userMaster=null;
    	private String ipRemoteAddress=null;
    	private ApplicationContext APPLICATIONCONTEXT =null;
    	private LinkedHashMap<Integer,LinkedList> teacherJFTandSSmap=null;
    	Map<TeacherDetail,List<StatusNodeColorHistory>> teacherListByTeacherObj=null;
    	Map<Integer,MQEvent> mQEventMAP=null;
    	private LinkedHashMap<Integer,JobOrder> allTeacherJFTandSSmap =null;
		public CallSOAPAndRESTServiceThread(String[] teacher,List<TeacherDetail> teacherDetailList, UserMaster userMaster,LinkedHashMap<Integer,LinkedList> teacherJFTandSSmap, Map<TeacherDetail,List<StatusNodeColorHistory>> teacherListByTeacherObj, LinkedHashMap<Integer,JobOrder> allTeacherJFTandSSmap , Map<Integer,MQEvent> mQEventMAP,HttpServletRequest request){
			this.teacher=teacher;
			this.teacherDetailList=teacherDetailList;
			this.userMaster=userMaster;
			this.ipRemoteAddress=request.getRemoteAddr();
			this.APPLICATIONCONTEXT = WebApplicationContextUtils.getWebApplicationContext(request.getSession().getServletContext());
			this.teacherJFTandSSmap=teacherJFTandSSmap;
			this.teacherListByTeacherObj=teacherListByTeacherObj;
			this.mQEventMAP=mQEventMAP;
			this.allTeacherJFTandSSmap=allTeacherJFTandSSmap;
		}
		public void run() {
				try{
					/**
					 * API Change for bulk LINK TO KSN - CheckEmailAddress
					 * modified Date: Feb 12, 2016 
					 */
					ApplitrackDistricts applitrackDistricts = WorkThreadServlet.applitrackDisMap.get("CheckForTalentEmail");
					if(applitrackDistricts == null){
						System.out.println("applitrackDistricts Object Can Not Be Null");
						//return null;
					}else{

					String status = null;
					String tmTransactionId = "";

					if(teacherDetailList.size()>0){
						List<MQEvent> mqEventList = mqEventDAO.findMQEventByTeacherList_Op(teacherDetailList,Utility.getLocaleValuePropByKey("lblLinkToKsn", locale));
						Map<Integer,MQEvent> teacherWiseMQData = new HashMap<Integer, MQEvent>();
						Map<Integer,List<TalentKSNDetail>> teacherWiseKSNData = new HashMap<Integer, List<TalentKSNDetail>>();
						if(mqEventList!=null && mqEventList.size()>0){
							for(MQEvent mq : mqEventList){
								//if(mq.getEventType().equalsIgnoreCase(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale)))
									teacherWiseMQData.put(mq.getTeacherdetail().getTeacherId(), mq);
							}
						}

						List<TalentKSNDetail> talentksndList = talentKSNDetailDAO.getListTalentKSNDetail_Op(teacherDetailList);
						if(talentksndList!=null && talentksndList.size()>0){
							List<TalentKSNDetail> tempList = null;
							for(TalentKSNDetail tal : talentksndList){
								tempList = null;
								if(teacherWiseKSNData.get(tal.getTeacherId().getTeacherId())!=null){
									tempList = teacherWiseKSNData.get(tal.getTeacherId().getTeacherId());
								}
								else{
									tempList = new ArrayList<TalentKSNDetail>();
								}
								tempList.add(tal);
								teacherWiseKSNData.put(tal.getTeacherId().getTeacherId(), tempList);
							}
						}	
						mqEventDAO = (MQEventDAO)APPLICATIONCONTEXT.getBean("mqEventDAO");
						MQService mqServiceObj=new MQService();
						List<TeacherDetail> teacherRestAPICall = new ArrayList<TeacherDetail>();
						SessionFactoryImplementor sessionFactoryImplementation = (SessionFactoryImplementor)mqEventDAO.getSessionFactory();
						ConnectionProvider connectionProvider = sessionFactoryImplementation.getConnectionProvider();
						Connection connection = connectionProvider.getConnection();
						int counter =1;
						for(TeacherDetail t : teacherDetailList){
							try {

								System.out.println(" count :: "+counter);
								MQEvent mq = teacherWiseMQData.get(t.getTeacherId());
								List<TalentKSNDetail> tList = teacherWiseKSNData.get(t.getTeacherId());
								if(tList!=null && tList.size()>0)
									System.out.println(" t.getTeacherId()"+t.getTeacherId()+" ksn data :: "+tList.size());

								try {
									mqServiceObj.callCheckEmailAPI(t, applitrackDistricts, mq, userMaster, tList, mqEventDAO,connection,teacherRestAPICall,teacherJFTandSSmap,allTeacherJFTandSSmap);
									if(teacherRestAPICall!=null && teacherRestAPICall.size()>0){
										LinkedList jftANDss=teacherJFTandSSmap.get(teacherRestAPICall.get(0).getTeacherId());
										if(jftANDss!=null){
											System.out.println("jftANDss size-====="+jftANDss.size());
											JobForTeacher jftObj=(JobForTeacher)jftANDss.get(0);
											SecondaryStatus ssObj=(SecondaryStatus)jftANDss.get(1);
											System.out.println("jftANDss  jftObj============"+jftObj.getJobForTeacherId());
											System.out.println("jftANDss  ssObj============"+ssObj.getSecondaryStatusId());
											if(executeRestCallForAllTeacher(userMaster,jftObj,ssObj,teacherRestAPICall.get(0),teacherListByTeacherObj,mQEventMAP,connection,ipRemoteAddress,APPLICATIONCONTEXT))
											teacherRestAPICall.clear();
										}
									}
									if(counter%5==0){
										connection.close();
										connection = connectionProvider.getConnection();
									}
								} catch (Exception e) {
									e.printStackTrace();
								}

								counter++;
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
						 if(connection!=null)
						 connection.close();
						if(teacherWiseMQData!=null && teacherWiseMQData.size()>0)
							teacherWiseMQData.clear();
					}
				 }
				}catch(Exception ex){
					ex.printStackTrace();
				}
			}
    }
    
    @SuppressWarnings("unchecked")
	private static List setJFTandSSagainstTD(JobForTeacherDAO jobForTeacherDAO,SecondaryStatusDAO secondaryStatusDAO, List<TeacherDetail> teacherDetail) throws Exception
    {
    	List list=new ArrayList();
    	LinkedHashMap<Integer,JobOrder> allTeacherJFTandSSmap=new LinkedHashMap<Integer,JobOrder>();
    	 logger.info(" kellymapping yesssssssssssssssssssss multiple object call --=================================================================");
    	LinkedHashMap<Integer,LinkedList> teacherJFTandSSmap=new LinkedHashMap<Integer,LinkedList>();
    	   List<JobForTeacher> kellyAppliedJobs = jobForTeacherDAO.findJobByTeacherInDesc(teacherDetail);
		   List<JobCategoryMaster> jobCategories =new ArrayList<JobCategoryMaster>();
		   if(kellyAppliedJobs!=null && kellyAppliedJobs.size()>0){
			   for(JobForTeacher jft : kellyAppliedJobs){
				   if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
					   jobCategories.add(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId());
				   }
			   }
		   }
		   Map<Integer,SecondaryStatus> secStatusMap=new HashMap<Integer,SecondaryStatus>();
		   List<SecondaryStatus> secondaryStatuses =null;
		   if(jobCategories!=null && jobCategories.size()>0)
			   secondaryStatuses = secondaryStatusDAO.findByNameAndJobCats(Utility.getLocaleValuePropByKey("lblLinkToKsn", locale),jobCategories);
		   if(secondaryStatuses!=null && secondaryStatuses.size()>0){
			   for(SecondaryStatus sec : secondaryStatuses){
				   if(sec.getJobCategoryMaster()!=null){
					   secStatusMap.put(sec.getJobCategoryMaster().getJobCategoryId(), sec);
				   }
			   }
		   }
		   
		   if(kellyAppliedJobs!=null && kellyAppliedJobs.size()>0){
			   for(JobForTeacher jft : kellyAppliedJobs){
				   allTeacherJFTandSSmap.put(jft.getTeacherId().getTeacherId(), jft.getJobId());
				   if(jft.getJobId().getHeadQuarterMaster()!=null && jft.getJobId().getJobCategoryMaster().getParentJobCategoryId()!=null){
					   if(secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId())!=null){
						   LinkedList obj=new LinkedList();
						   obj.add(jft);
						   obj.add(secStatusMap.get(jft.getJobId().getJobCategoryMaster().getParentJobCategoryId().getJobCategoryId()));
						   teacherJFTandSSmap.put(jft.getTeacherId().getTeacherId(), obj);
						   //break;
					   }
				   }
			   }
		   }
		   list.add(teacherJFTandSSmap);
		   list.add(allTeacherJFTandSSmap);
		   kellyAppliedJobs.clear();
		   secStatusMap.clear();
		   jobCategories.clear();
		   kellyAppliedJobs=null;
		   secStatusMap=null;
		   jobCategories=null;
	   return list;
    }
}