package tm.services.report;

import static tm.services.district.GlobalServices.IS_ALWAYS_REQUIRED_SCH_MAP;
import static tm.services.district.GlobalServices.OFFER_READY_MAP;
import static tm.services.district.GlobalServices.println;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.directwebremoting.WebContext;
import org.directwebremoting.WebContextFactory;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import tm.bean.DistrictRequisitionNumbers;
import tm.bean.DistrictSpecificReason;
import tm.bean.DistrictSpecificTiming;
import tm.bean.JobForTeacher;
import tm.bean.JobOrder;
import tm.bean.MessageToTeacher;
import tm.bean.SchoolInJobOrder;
import tm.bean.StatusSpecificEmailTemplates;
import tm.bean.StatusWiseAutoEmailSend;
import tm.bean.StatusWiseEmailSection;
import tm.bean.StatusWiseEmailSubSection;
import tm.bean.TeacherAcademics;
import tm.bean.TeacherDetail;
import tm.bean.TeacherPersonalInfo;
import tm.bean.TeacherStatusHistoryForJob;
import tm.bean.cgreport.DistrictMaxFitScore;
import tm.bean.cgreport.JobCategoryWiseStatusPrivilege;
import tm.bean.cgreport.JobWiseConsolidatedTeacherScore;
import tm.bean.cgreport.TeacherStatusNotes;
import tm.bean.cgreport.TeacherStatusScores;
import tm.bean.cgreport.UserMailSend;
import tm.bean.master.DistrictKeyContact;
import tm.bean.master.DistrictMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.JobWisePanelStatus;
import tm.bean.master.RaceMaster;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusPrivilegeForSchools;
import tm.bean.master.StatusSpecificQuestions;
import tm.bean.master.StatusSpecificScore;
import tm.bean.teacher.OnlineActivityQuestionSet;
import tm.bean.teacher.OnlineActivityStatus;
import tm.bean.user.RoleMaster;
import tm.bean.user.UserMaster;
import tm.dao.DistrictRequisitionNumbersDAO;
import tm.dao.DistrictSpecificReasonDAO;
import tm.dao.DistrictSpecificTimingDAO;
import tm.dao.JobForTeacherDAO;
import tm.dao.JobOrderDAO;
import tm.dao.SchoolInJobOrderDAO;
import tm.dao.StatusSpecificEmailTemplatesDAO;
import tm.dao.StatusWiseAutoEmailSendDAO;
import tm.dao.StatusWiseEmailSectionDAO;
import tm.dao.StatusWiseEmailSubSectionDAO;
import tm.dao.TeacherAcademicsDAO;
import tm.dao.TeacherDetailDAO;
import tm.dao.TeacherPersonalInfoDAO;
import tm.dao.TeacherStatusHistoryForJobDAO;
import tm.dao.cgreport.DistrictMaxFitScoreDAO;
import tm.dao.cgreport.JobCategoryWiseStatusPrivilegeDAO;
import tm.dao.cgreport.JobWiseConsolidatedTeacherScoreDAO;
import tm.dao.cgreport.TeacherStatusNotesDAO;
import tm.dao.cgreport.TeacherStatusScoresDAO;
import tm.dao.master.DistrictKeyContactDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.JobWisePanelStatusDAO;
import tm.dao.master.RaceMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusPrivilegeForSchoolsDAO;
import tm.dao.master.StatusSpecificQuestionsDAO;
import tm.dao.master.StatusSpecificScoreDAO;
import tm.dao.master.UserEmailNotificationsDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.dao.teacher.OnlineActivityQuestionSetDAO;
import tm.dao.teacher.OnlineActivityStatusDAO;
import tm.dao.user.UserMasterDAO;
import tm.services.DemoScheduleMailThread;
import tm.services.EmailerService;
import tm.services.MailText;
import tm.services.PATSHiredPostThread;
import tm.services.district.ManageStatusAjax;
import tm.services.district.PrintOnConsole;
import tm.services.teacher.MailSendSchedulerThread;
import tm.services.teacher.OnlineActivityAjax;
import tm.services.teacher.SchoolSelectionAjax;
import tm.services.teacher.SchoolSelectionThread;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.Utility;

public class MassStatusUpdateCGAjax 
{

	String locale = Utility.getValueOfPropByKey("locale");
	String lblScr=Utility.getLocaleValuePropByKey("lblScr", locale);
    String headQues=Utility.getLocaleValuePropByKey("headQues", locale);
    String lblAttribute1=Utility.getLocaleValuePropByKey("lblAttribute1", locale);
    String lblAddNote1=Utility.getLocaleValuePropByKey("lblAddNote1", locale);
    String lblClickToViewInst=Utility.getLocaleValuePropByKey("lblClickToViewInst", locale);
    String lblNote=Utility.getLocaleValuePropByKey("lblNote", locale);
    String hdAttachment=Utility.getLocaleValuePropByKey("hdAttachment", locale);
    String lblCreatedBy=Utility.getLocaleValuePropByKey("lblCreatedBy", locale);
    String lblCreatedDate=Utility.getLocaleValuePropByKey("lblCreatedDate", locale);
    String lblAct=Utility.getLocaleValuePropByKey("lblAct", locale);
    String lblNoNoteAdded=Utility.getLocaleValuePropByKey("lblNoNoteAdded", locale);
    String msgStatusOfCandidateCouldNotSet=Utility.getLocaleValuePropByKey("msgStatusOfCandidateCouldNotSet", locale);
    String msgStatusOfCandidateCouldNotSet1=Utility.getLocaleValuePropByKey("msgStatusOfCandidateCouldNotSet1", locale);
    String lblCandidatealreadyhired=Utility.getLocaleValuePropByKey("lblNoNoteAdded", locale);
    String lblCandidatealreadyhired1=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired1", locale);
    String lblCandidatealreadyhired2=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired2", locale);
    String lblCandidatealreadyhired3=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired3", locale);
    String lblCandidatealreadyhired4=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired4", locale);
    String lblCandidatealreadyhired5=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired5", locale);
    String lblCandidatealreadyhired6=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired6", locale);
    String lblCandidatealreadyhired7=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired7", locale);
    String lblCandidatealreadyhired8=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired8", locale);
    String lblCandidatealreadyhired9=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired9", locale);
    String lblCandidatealreadyhired10=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired10", locale);
    String lblCandidatealreadyhired11=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired11", locale);
    String lblCandidatealreadyhired12=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired12", locale);
    String lblCandidatealreadyhired13=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired13", locale);
    String lblCandidatealreadyhired14=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired14", locale);
    String lblCandidatealreadyhired15=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired15", locale);
    String lblCandidatealreadyhired16=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired16", locale);
    String lblCandidatealreadyhired17=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired17", locale);
   
    String lblSelectSubTemplate1=Utility.getLocaleValuePropByKey("lblSelectSubTemplate1", locale);
    String mailAcceotOrDeclineMailHtmal11=Utility.getLocaleValuePropByKey("mailAcceotOrDeclineMailHtmal11", locale);
    String lblTeacherName1=Utility.getLocaleValuePropByKey("lblTeacherName1", locale);
    String lblUserName=Utility.getLocaleValuePropByKey("lblUserName", locale);
    String lblJoTil=Utility.getLocaleValuePropByKey("lblJoTil", locale);
    String lblStatusName=Utility.getLocaleValuePropByKey("lblStatusName", locale);
    String lblURL1=Utility.getLocaleValuePropByKey("lblURL1", locale);
    String lblHrContact1=Utility.getLocaleValuePropByKey("lblHrContact1", locale);
    String lblUserEmailAddress1=Utility.getLocaleValuePropByKey("lblUserEmailAddress1", locale);
    String lblCandidatealreadyhired18=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired18", locale);
    String lblCandidatealreadyhired19=Utility.getLocaleValuePropByKey("lblCandidatealreadyhired19", locale);
    String lblCopiesOfficialTran=Utility.getLocaleValuePropByKey("lblCopiesOfficialTran", locale);
    String msgTrySendEmail1=Utility.getLocaleValuePropByKey("msgTrySendEmail1", locale);
    String msgTrySendEmail2=Utility.getLocaleValuePropByKey("msgTrySendEmail2", locale);
    private static String slcStartDateOnOrOff=Utility.getValueOfPropByKey("basePath").contains("platform")?"ON":"ON";

	@Autowired
	private OnlineActivityStatusDAO onlineActivityStatusDAO;
	
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	
	@Autowired
	private OnlineActivityQuestionSetDAO onlineActivityQuestionSetDAO;
	
	@Autowired
	private StatusPrivilegeForSchoolsDAO statusPrivilegeForSchoolsDAO;

	@Autowired
	private UserEmailNotificationsDAO userEmailNotificationsDAO;

	@Autowired
	private OnlineActivityAjax onlineActivityAjax;
	public void setOnlineActivityAjax(
			OnlineActivityAjax onlineActivityAjax) {
		this.onlineActivityAjax = onlineActivityAjax;
	}
	
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}

	@Autowired
	private TeacherStatusScoresDAO teacherStatusScoresDAO;

	@Autowired
	private JobWiseConsolidatedTeacherScoreDAO jobWiseConsolidatedTeacherScoreDAO;

	@Autowired
	private TeacherStatusNotesDAO teacherStatusNotesDAO;

	@Autowired
	private TeacherStatusHistoryForJobDAO teacherStatusHistoryForJobDAO;

	@Autowired
	private StatusMasterDAO statusMasterDAO;

	@Autowired
	private SchoolInJobOrderDAO schoolInJobOrderDAO;

	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;

	@Autowired
	private JobOrderDAO jobOrderDAO;

	@Autowired
	private JobForTeacherDAO jobForTeacherDAO;

	@Autowired
	private TeacherDetailDAO teacherDetailDAO;

	@Autowired
	private DistrictMaxFitScoreDAO districtMaxFitScoreDAO;

	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;

	@Autowired
	private StatusWiseEmailSectionDAO statusWiseEmailSectionDAO;
	
	@Autowired
	private StatusWiseEmailSubSectionDAO statusWiseEmailSubSectionDAO;
	
	@Autowired
	private TeacherPersonalInfoDAO teacherPersonalInfoDAO;
	
	@Autowired
	private DistrictSpecificTimingDAO districtSpecificTimingDAO;
	
	@Autowired
	private DistrictSpecificReasonDAO districtSpecificReasonDAO;

	public void setDistrictSpecificTimingDAO(
			DistrictSpecificTimingDAO districtSpecificTimingDAO) {
		this.districtSpecificTimingDAO = districtSpecificTimingDAO;
	}

	public void setDistrictSpecificReasonDAO(
			DistrictSpecificReasonDAO districtSpecificReasonDAO) {
		this.districtSpecificReasonDAO = districtSpecificReasonDAO;
	}
	
	@Autowired
	private StatusSpecificQuestionsDAO statusSpecificQuestionsDAO;
	public void setStatusSpecificQuestionsDAO(
			StatusSpecificQuestionsDAO statusSpecificQuestionsDAO) {
		this.statusSpecificQuestionsDAO = statusSpecificQuestionsDAO;
	}

	@Autowired
	private StatusSpecificScoreDAO statusSpecificScoreDAO;
	public void setStatusSpecificScoreDAO(
			StatusSpecificScoreDAO statusSpecificScoreDAO) {
		this.statusSpecificScoreDAO = statusSpecificScoreDAO;
	}

	@Autowired
	private JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO;
	public void setJobCategoryWiseStatusPrivilegeDAO(
			JobCategoryWiseStatusPrivilegeDAO jobCategoryWiseStatusPrivilegeDAO) {
		this.jobCategoryWiseStatusPrivilegeDAO = jobCategoryWiseStatusPrivilegeDAO;
	}

	@Autowired
	private DistrictKeyContactDAO districtKeyContactDAO;
	public void setDistrictKeyContactDAO(DistrictKeyContactDAO districtKeyContactDAO) {
		this.districtKeyContactDAO = districtKeyContactDAO;
	}

	@Autowired
	private StatusSpecificEmailTemplatesDAO statusSpecificEmailTemplatesDAO;
	public void setStatusSpecificEmailTemplatesDAO(
			StatusSpecificEmailTemplatesDAO statusSpecificEmailTemplatesDAO) {
		this.statusSpecificEmailTemplatesDAO = statusSpecificEmailTemplatesDAO;
	}

	@Autowired
	private JobWisePanelStatusDAO jobWisePanelStatusDAO;

	@Autowired
	private TeacherAcademicsDAO teacherAcademicsDAO;

	@Autowired
	private StatusWiseAutoEmailSendDAO statusWiseAutoEmailSendDAO;

	@Autowired
	private ManageStatusAjax manageStatusAjax;
	public void setManageStatusAjax(ManageStatusAjax manageStatusAjax) {
		this.manageStatusAjax = manageStatusAjax;
	}
	
	@Autowired
	private RaceMasterDAO raceMasterDAO;
	
	@Autowired
	private UserMasterDAO userMasterDAO;
	
	@Autowired
	private DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO;
	public void setDistrictRequisitionNumbersDAO(
			DistrictRequisitionNumbersDAO districtRequisitionNumbersDAO) {
		this.districtRequisitionNumbersDAO = districtRequisitionNumbersDAO;
	}
	
	//added
	@Autowired
	private SchoolSelectionAjax schoolSelectionAjax;
	public void setSchoolSelectionAjax(
			SchoolSelectionAjax schoolSelectionAjax) {
		this.schoolSelectionAjax = schoolSelectionAjax;
	}
	//ended
	
	
	public String printChildStatus(HttpServletRequest request,List<SecondaryStatus> lstuserChildren,SecondaryStatus tree,Map<Integer,StatusPrivilegeForSchools> mapSschool,Map<Integer,StatusPrivilegeForSchools> mapSecSchool,Map<Integer,JobCategoryWiseStatusPrivilege> mapJobSecSchool)
	{
		StringBuffer sbchild=new StringBuffer();
		boolean priviFlag=false;
		Collections.sort(lstuserChildren,SecondaryStatus.secondaryStatusOrder);
		sbchild.append("<table  border=0 bordercolor='green'>");
		int banchmarkStatus=0;
		for(SecondaryStatus subfL: lstuserChildren ){
			if(subfL.getStatusMaster()!=null){
				if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
					banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
				}
			}
			if(banchmarkStatus==0){	
				if(subfL.getStatusMaster()!=null){
					if(subfL.getStatusMaster().getStatusShortName().equals("apl")  && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){
						sbchild.append("<tr><td style='text-align:center;height:100px;'>");
						sbchild.append("<img src=\"images/green.png\"/><br/>"+subfL.getSecondaryStatusName());
						sbchild.append("</td></tr>");
					}
				}
			}else if(subfL.getStatusMaster()==null && subfL.getStatus().equalsIgnoreCase("A") && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){
				
				SecondaryStatus subfLTmp=new SecondaryStatus();
				if(subfL.getSecondaryStatus_copy()!=null)
					subfLTmp=subfL.getSecondaryStatus_copy();
				else
					subfLTmp=subfL;
				
				StatusPrivilegeForSchools sPFSObj=mapSecSchool.get(subfLTmp.getSecondaryStatusId());
				priviFlag=false;
				JobCategoryWiseStatusPrivilege jobSchool=mapJobSecSchool.get(subfL.getSecondaryStatusId());
				if(jobSchool!=null && !jobSchool.isStatusPrivilegeForSchools())
					priviFlag=true;
				else if(jobSchool==null && sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0)
					priviFlag=true;
				
				sbchild.append("<tr><td style='text-align:center;height:100px;'>");
				if(!priviFlag)
				{
					sbchild.append("<a href='#' onclick=\"showStatusDetails_mass('"+subfL.getSecondaryStatusName()+"','0','"+subfL.getSecondaryStatusId()+"');\">");
					sbchild.append("<img src=\"images/grey.png\"/>");
					sbchild.append("</a><br/>"+subfL.getSecondaryStatusName());
				}
				else
					sbchild.append("<img src=\"images/grey.png\"/><br/>"+subfL.getSecondaryStatusName());
				sbchild.append("</td></tr>");
			}
		}
		for(SecondaryStatus subfL: lstuserChildren){
			if(subfL.getStatusMaster()!=null && subfL.getStatus().equalsIgnoreCase("A") && subfL.getJobCategoryMaster()==tree.getJobCategoryMaster()){
				if(subfL.getStatusMaster().getStatusShortName().equals("hcomp")|| subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp")){
					priviFlag=false;
					StatusPrivilegeForSchools sPFSObj=mapSschool.get(subfL.getStatusMaster().getStatusId());

					JobCategoryWiseStatusPrivilege jobSchool=mapJobSecSchool.get(subfL.getSecondaryStatusId());//jobSchool=mapJobSschool.get(subfL.getStatusMaster().getStatusId());
					if(jobSchool!=null && !jobSchool.isStatusPrivilegeForSchools()){
						priviFlag=true;
					}else if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
						priviFlag=true;
					}
					
					sbchild.append("<tr><td align='center' style='height:100px;width:150px;'>");
					if(!priviFlag)
					{
						sbchild.append("<a href='#' onclick=\"showStatusDetails_mass('"+subfL.getSecondaryStatusName()+"','"+subfL.getStatusMaster().getStatusId()+"','"+subfL.getSecondaryStatusId()+"');\">");
						sbchild.append("<table><tr><td class='txtbgroundstatus'>"+subfL.getSecondaryStatusName()+"</td></tr></table>");
						sbchild.append("</a>");
					}
					else
						sbchild.append("<table><tr><td class='txtbgroundstatus'>"+subfL.getSecondaryStatusName()+"</td></tr></table>");
					
					sbchild.append("</td></tr>");
				} 
			}
		}
		sbchild.append("</table>");
		return sbchild.toString();
	}
	
	public String displayStatusDashboard_CGMass(Integer jobId)
	{
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null || session.getAttribute("userMaster")==null) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		boolean isMiami=false;
		
		UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
		int roleId=0;
		int entityType=0;
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
			entityType=userMaster.getEntityType();
		}
		
		StringBuffer rdiv =	new StringBuffer();
		try 
		{
			JobOrder jobOrder=null;
			DistrictMaster districtMaster=null;
			JobCategoryMaster jobCategoryMaster=null;
			if(jobId!=null)
			{
				jobOrder=jobOrderDAO.findById(jobId, false, false);
				if(jobOrder!=null)
				{
					districtMaster=jobOrder.getDistrictMaster();
					jobCategoryMaster=jobOrder.getJobCategoryMaster();
				}
			}
			
			if(districtMaster!=null && districtMaster.getDistrictId().equals(1200390))
				isMiami=true;
			
			Map<Integer,StatusPrivilegeForSchools> mapSschool = new HashMap<Integer, StatusPrivilegeForSchools>();
			Map<Integer,StatusPrivilegeForSchools> mapSecSchool = new HashMap<Integer, StatusPrivilegeForSchools>();
			Map<Integer,JobCategoryWiseStatusPrivilege> mapJobSecSchool = new HashMap<Integer,JobCategoryWiseStatusPrivilege>();
			List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchools= new ArrayList<StatusPrivilegeForSchools>();
			List<JobCategoryWiseStatusPrivilege> lstJobCategoryWiseStatusPrivilege= new ArrayList<JobCategoryWiseStatusPrivilege>();
			
			if(entityType==3){
				lstStatusPrivilegeForSchools=statusPrivilegeForSchoolsDAO.findStatusPrivilege(jobOrder.getDistrictMaster());

				lstJobCategoryWiseStatusPrivilege=jobCategoryWiseStatusPrivilegeDAO.findStatusPrivilegeSchool(jobOrder);
				if(lstJobCategoryWiseStatusPrivilege!=null)
					for(JobCategoryWiseStatusPrivilege jobSchool:lstJobCategoryWiseStatusPrivilege){
						if(jobSchool.getSecondaryStatus()!=null)
							mapJobSecSchool.put(jobSchool.getSecondaryStatus().getSecondaryStatusId(), jobSchool);
					}

				if(lstStatusPrivilegeForSchools!=null)
					for(StatusPrivilegeForSchools spFSchool: lstStatusPrivilegeForSchools ){
						if(spFSchool.getStatusMaster()!=null){
							mapSschool.put(spFSchool.getStatusMaster().getStatusId(), spFSchool);
						}else{
							if(spFSchool.getSecondaryStatus()!=null)
								mapSecSchool.put(spFSchool.getSecondaryStatus().getSecondaryStatusId(), spFSchool);
						}
					}	
			}
			
			List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
	
			//if(districtMaster!=null && jobCategoryMaster!=null)
			//	lstTreeStructure =	secondaryStatusDAO.findSecondaryStatusByDistrictAndJobCategory(districtMaster,jobCategoryMaster);
			
			List<SecondaryStatus> lstSecondaryStatus=secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
			if(lstSecondaryStatus.size()==0){
				secondaryStatusDAO.copyDataFromDistrictToJobCategoryBanchAndHead(jobOrder.getHeadQuarterMaster(),jobOrder.getBranchMaster(),jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(),userMaster);
			}
			lstTreeStructure = secondaryStatusDAO.findSecondaryStatusByJobCategoryHeadAndBranch(jobOrder);
	
			System.out.println("lstTreeStructure::::::::::::>>>>>>:::::::::"+lstTreeStructure.size());
			String tempArray[]={"hird","dcln","rem","widrw"};
			List<StatusMaster> lstStatusMaster=Utility.getStaticMasters(tempArray);
			
			StatusMaster statusHired=findStatusByShortName(lstStatusMaster,"hird");
			StatusMaster statusDeclined=findStatusByShortName(lstStatusMaster,"dcln");
			StatusMaster statusRemoved=findStatusByShortName(lstStatusMaster,"rem");
			StatusMaster statusWithdrew=findStatusByShortName(lstStatusMaster,"widrw");
			boolean hiredFlag=false,declineFlag=false,rejectFlag=false,withdrewFlag=false;
			if(entityType==3)
			{
				System.out.println("School    3333333333333333");
				StatusPrivilegeForSchools sPFSObj=null;
				JobCategoryWiseStatusPrivilege jobSchool=null;
				if(mapSschool.size()>0)
					sPFSObj=mapSschool.get(statusHired.getStatusId());
				if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
					hiredFlag=true;
				}
				if(mapSschool.size()>0)
					sPFSObj=mapSschool.get(statusDeclined.getStatusId());
				if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
					declineFlag=true;
				}
				if(mapSschool.size()>0)
					sPFSObj=mapSschool.get(statusRemoved.getStatusId());
				if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
					rejectFlag=true;
				}
				if(mapSschool.size()>0)
					sPFSObj=mapSschool.get(statusWithdrew.getStatusId());
				if(sPFSObj!=null && sPFSObj.getCanSchoolSetStatus()!=null && sPFSObj.getCanSchoolSetStatus()==0){
					withdrewFlag=true;
				}
			}
			else
			{
				System.out.println("Not School    3333333333333333");
			}
			
			Map<String, Integer> mapStatus=new HashMap<String, Integer>();
			if(lstStatusMaster!=null)
				for(StatusMaster master:lstStatusMaster)
					mapStatus.put(master.getStatusShortName(), master.getStatusId());

			int iHird=0,iDcln=0,iRem=0,iWidrw=0;
			if(mapStatus.get("hird")!=null)
				iHird=mapStatus.get("hird");
			
			if(mapStatus.get("dcln")!=null)
				iDcln=mapStatus.get("dcln");
			
			if(mapStatus.get("rem")!=null)
				iRem=mapStatus.get("rem");
			
			if(mapStatus.get("widrw")!=null)
				iWidrw=mapStatus.get("widrw");
			
			rdiv.append("<table border=0 bordercolor='red'>");
			rdiv.append("<tr>");
			for(SecondaryStatus tree: lstTreeStructure)
			{
				if(tree.getSecondaryStatus()==null)
				{
					rdiv.append("<td style='vertical-align:bottom;'> ");
					if(tree.getChildren().size()>0){
						rdiv.append(printChildStatus(request, tree.getChildren(),tree,mapSschool,mapSecSchool,mapJobSecSchool));
					}
					rdiv.append("</td>");
				}
			}
			rdiv.append("<td style='vertical-align:top;'>");
			rdiv.append("<table  border=0 bordercolor='green'>");
			rdiv.append("<tr><td style='text-align:center;height:100px;'>");
			if((isMiami && roleId==7) || (!isMiami && !hiredFlag))
				rdiv.append("<a href='#' onclick=\"showStatusDetails_mass('Hired','"+iHird+"','0');\">");
			rdiv.append("<img src=\"images/hire.png\"/>");
			rdiv.append("</a><br/>Hired");
			rdiv.append("</td></tr>");
			rdiv.append("</table>");
			rdiv.append("<table  border=0 bordercolor='green'>");
			rdiv.append("<tr><td style='text-align:center;height:100px;'>");
			if(!declineFlag)
			{
				rdiv.append("<a href='#' onclick=\"showStatusDetails_mass('Declined','"+iDcln+"','0');\">");
				rdiv.append("<img src=\"images/decline.png\"/>");
				rdiv.append("</a><br/>Declined");
			}
			else
				rdiv.append("<img src=\"images/decline.png\"/><br/>Declined");
			
			rdiv.append("</td></tr>");
			rdiv.append("</table>");
			rdiv.append("<table  border=0 bordercolor='green'>");
			rdiv.append("<tr><td style='text-align:center;height:100px;'>");
			
			if(!rejectFlag)
			{
				rdiv.append("<a href='#' onclick=\"showStatusDetails_mass('Rejected','"+iRem+"','0');\">");
				rdiv.append("<img src=\"images/remove.png\"/>");
				rdiv.append("</a><br/>Rejected");
			}
			else
				rdiv.append("<img src=\"images/remove.png\"/><br/>Rejected");
			
			rdiv.append("</td></tr>");
			rdiv.append("</table>");
			rdiv.append("<table  border=0 bordercolor='green'>");
			rdiv.append("<tr><td style='text-align:center;height:100px;'>");
			if(!withdrewFlag)
			{
			rdiv.append("<a href='#' onclick=\"showStatusDetails_mass('Withdrew','"+iWidrw+"','0');\">");
			rdiv.append("<img src=\"images/withdrew.png\"/></a><br/>Withdrawn");
			}
			rdiv.append("</td></tr>");
			rdiv.append("</table>");
			rdiv.append("</td>");
			rdiv.append("</tr>");
			rdiv.append("</table>");
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return rdiv.toString();
	}
	
	
	public String[] getStatusDetailsInfo_CGMass(String teacherIds,String jobId,String statusId,String secondaryStatusId)
	{
		System.out.println("::::::::::::::::::::::::::::::::::::::>>>>>>>Yes<<<<<<<<<::::::::::::::::::::::::::::::::::::");
		StringBuffer sb = new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		
		String sDoActivity="0",sPanel="0",sStatus="0",sDistrictRequisitionNumber="0",sMultipleRequisitionNumber="0",sAllDPointSetStatus="0",sAllSetStatusForOfferReady="0",sAlreadyOffer="0",sDisplayStatusName="";
		String sSchoolCount="0",sAutoEmailSend="0",sQuestionCount="0",sQuestionIDMatch="0",sOfclTransVeteranPref="0";
		String sAllDPointSetStatus_partial="0",sAllSetStatusForOfferReady_partial="0",sOfferReady_NonInst="0",sJSI="0",sHWRD_Partial="0",sHWRD_All="0";
		boolean offerAndAccepted=false;
		try {
			HttpSession session = request.getSession(false);
			if(session == null || session.getAttribute("userMaster")==null) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			
			List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
			if(teacherIds!=null && teacherIds.length() > 0)
			{
				String teacherIDsArray[]=teacherIds.split(",");
				List<Integer> lstTeacherIds=new ArrayList<Integer>();
				for(String temp:teacherIDsArray)
					lstTeacherIds.add(Integer.parseInt(temp));
				
				lstTeacherDetails=teacherDetailDAO.findByTeacherIds(lstTeacherIds);
			}
			
			DistrictMaster districtMaster = null;
			JobCategoryMaster jobCategoryMaster=null;
			JobOrder jobOrder =jobOrderDAO.findById(new Integer(jobId), false, false);
			if(jobOrder!=null)
			{
				if(jobOrder.getDistrictMaster()!=null)
					districtMaster=jobOrder.getDistrictMaster();
				if(jobOrder.getJobCategoryMaster()!=null)
					jobCategoryMaster=jobOrder.getJobCategoryMaster();
			}
			
			boolean isMiami = false;
			try{
				if(districtMaster!=null && districtMaster.getDistrictId().equals(1200390))
					isMiami=true;
			}catch(Exception e){}
			
			boolean isSubstituteInstructionalJob = false;
			try{
				if(isMiami && jobOrder!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
					isSubstituteInstructionalJob=true;
			}catch(Exception e){}
			
			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatus=null;
			String recentStatusName="";

			//added by 12-06-2015
			if(statusId!=null && !statusId.equals("0")){
				statusMaster=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
				recentStatusName=statusMaster.getStatus();
				try{
					SecondaryStatus secondaryStatusObj=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
					recentStatusName=secondaryStatusObj.getSecondaryStatusName();
					if(secondaryStatusObj.getSecondaryStatusName().equalsIgnoreCase("jsi"))
						recentStatusName="JSI";
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			
			if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
				recentStatusName=secondaryStatus.getSecondaryStatusName();
				if(secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("jsi"))
					recentStatusName="JSI";
			}
			//ended by 12-06-2015
			
			if((statusMaster!=null && secondaryStatus!=null) || secondaryStatus!=null)
				sDisplayStatusName=secondaryStatus.getSecondaryStatusName();
			else if(statusMaster!=null)
				sDisplayStatusName=statusMaster.getStatus();
			
			SecondaryStatus temp_secStatus=null;
			StatusMaster temp_status=null;
			if(secondaryStatus!=null)
			{
				if(secondaryStatus.getStatusMaster()!=null)
					temp_status=secondaryStatus.getStatusMaster();
				else if(secondaryStatus.getSecondaryStatus_copy()!=null)
					temp_secStatus=secondaryStatus.getSecondaryStatus_copy();
				else
					temp_secStatus=secondaryStatus;
			}
			else if(statusMaster!=null)
				temp_status=statusMaster;
			
			if(isMiami && sDisplayStatusName.equalsIgnoreCase("Ofcl Trans / Veteran Pref"))
				sOfclTransVeteranPref="1";
			
			if(sDisplayStatusName.equalsIgnoreCase("JSI"))
				sJSI="1";
			
			boolean bOfferReady=false;
			if(isMiami && secondaryStatus!=null && !secondaryStatus.getSecondaryStatusName().equals("") && secondaryStatus.getSecondaryStatusName().equals("Offer Ready"))
				bOfferReady=true;
			
			if(!isSubstituteInstructionalJob && bOfferReady)
				sOfferReady_NonInst="1";
			try{
				if(isMiami && isSubstituteInstructionalJob && ( bOfferReady || (statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")))){
					offerAndAccepted=true;
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			SchoolInJobOrder schoolInJobOrder = null;
			if(userMaster.getEntityType()==3){
				SchoolMaster schoolMaster = userMaster.getSchoolId();				
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster);
			}
			
			CandidateGridService cgService=new CandidateGridService();
			boolean doActivity=cgService.isVisible(jobOrder,userMaster,schoolInJobOrder);
			if(doActivity)
				sDoActivity="1";
			
			List<JobWisePanelStatus> jobWisePanelStatusList=new ArrayList<JobWisePanelStatus>();
			try {
				jobWisePanelStatusList=jobWisePanelStatusDAO.getPanelCGView(jobOrder, secondaryStatus);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
			if(jobWisePanelStatusList!=null && jobWisePanelStatusList.size() > 0)
				sPanel="1";
			
			if(sPanel.equalsIgnoreCase("0"))
			{
				if(districtMaster.getIsReqNoRequired())
					if(jobOrder.getIsExpHireNotEqualToReqNo()==null || !jobOrder.getIsExpHireNotEqualToReqNo())
						if(sDisplayStatusName.equalsIgnoreCase("Offer Ready") || sDisplayStatusName.equalsIgnoreCase("Hired"))
							sMultipleRequisitionNumber="1";
			}
			
			/////
			List<TeacherDetail> lstTeacherDetails_Green=new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstTeacherDetails_NotAllSetDPoint=new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstTeacherDetails_NotSetAllBeforeOfferReady=new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstTeacherDetails_AlreadyOffer=new ArrayList<TeacherDetail>();
			
			List <TeacherStatusHistoryForJob> lstTSHForJob =new ArrayList<TeacherStatusHistoryForJob>();
			lstTSHForJob=teacherStatusHistoryForJobDAO.getStatus(lstTeacherDetails, jobOrder, statusMaster, secondaryStatus);
			if(lstTSHForJob!=null && lstTSHForJob.size() > 0)
			{
				PrintOnConsole.debugPrintlnCGMass(" ************************ Set Status Check ****************************** ");
				PrintOnConsole.debugPrintlnCGMass(" *** lstTSHForJob Size "+lstTSHForJob.size());
				lstTeacherDetails_Green=getTeacherListFromTSHForJob(lstTSHForJob);
				PrintOnConsole.debugPrintlnCGMass("*** lstTeacherDetails_Green Size "+lstTeacherDetails_Green.size());
			}
			
			
			
			PrintOnConsole.debugPrintlnCGMass(" ************************ All Set DPoint Check ****************************** ");
			lstTeacherDetails_NotAllSetDPoint=NotAllSetDPointTeacherList(jobOrder, statusMaster, lstTeacherDetails);
			PrintOnConsole.debugPrintlnCGMass(" *** lstTeacherDetails_NotAllSetDPoint Size "+lstTeacherDetails_NotAllSetDPoint.size());
			
			
			if(lstTeacherDetails_NotAllSetDPoint!=null && lstTeacherDetails_NotAllSetDPoint.size() > 0)
			{
				if(lstTeacherDetails_NotAllSetDPoint.size()==lstTeacherDetails.size())
					sAllDPointSetStatus="1";
				else if(lstTeacherDetails_NotAllSetDPoint.size()!=lstTeacherDetails.size())
					sAllDPointSetStatus_partial="1";
			}
			
			
			if(sDisplayStatusName.equalsIgnoreCase("Offer Ready"))
			{
				PrintOnConsole.debugPrintlnCGMass(" ************************ Set All Before OfferReady ****************************** ");
				lstTeacherDetails_NotSetAllBeforeOfferReady=NotSetAllBeforeOfferReadyTeacherList(jobOrder, lstTeacherDetails);
				PrintOnConsole.debugPrintlnCGMass(" *** lstTeacherDetails_NotSetAllBeforeOfferReady Size "+lstTeacherDetails_NotSetAllBeforeOfferReady.size());
				
				if(lstTeacherDetails_NotSetAllBeforeOfferReady!=null && lstTeacherDetails_NotSetAllBeforeOfferReady.size() > 0)
				{
					if(lstTeacherDetails_NotSetAllBeforeOfferReady.size()==lstTeacherDetails.size())
						sAllSetStatusForOfferReady="1";
					else if(lstTeacherDetails_NotSetAllBeforeOfferReady.size()!=lstTeacherDetails.size())
						sAllSetStatusForOfferReady_partial="1";
				}
				
				
				PrintOnConsole.debugPrintlnCGMass(" ************************ Already Offer Ready ****************************** ");
				List<JobForTeacher> jftOffer=jobForTeacherDAO.OfferCandidateList(lstTeacherDetails);
				PrintOnConsole.debugPrintlnCGMass(" *** jobforteacherOffer Size "+jftOffer.size());
				lstTeacherDetails_AlreadyOffer=getAlreadyOfferTeacherList(jftOffer);
				PrintOnConsole.debugPrintlnCGMass(" *** lstTeacherDetails_AlreadyOffer Size "+lstTeacherDetails_AlreadyOffer.size());
			}
			
			String statusShortNames[]={"hird","widrw","rem","dcln"};
			List<StatusMaster> lstStatusMaster=Utility.getStaticMasters(statusShortNames);
			List<JobForTeacher> jftStatus=jobForTeacherDAO.getJFTByTIDsAndSID_CGMass(lstTeacherDetails, lstStatusMaster, jobOrder);
			if(jftStatus!=null && jftStatus.size() >0)
			{
				sHWRD_Partial="1";
				if(lstTeacherDetails!=null && lstTeacherDetails.size()==jftStatus.size())
					sHWRD_All="1";
			}
			
			/////
			
			
			if(userMaster.getEntityType()!=3 && jobOrder!=null && ((statusMaster!=null && statusMaster.getStatusShortName().equalsIgnoreCase("hird")) || (sDisplayStatusName.equalsIgnoreCase("Offer Ready"))))
			{
				List<SchoolInJobOrder> listSchoolInJobOrder	= schoolInJobOrderDAO.findJobOrder(jobOrder);
				if(listSchoolInJobOrder!=null && listSchoolInJobOrder.size() > 1)
					sSchoolCount=""+listSchoolInJobOrder.size();
			}

			List<StatusWiseAutoEmailSend> lstStatusWiseAutoEmailSends=new ArrayList<StatusWiseAutoEmailSend>();
			lstStatusWiseAutoEmailSends=statusWiseAutoEmailSendDAO.getAutoEmail(districtMaster, statusMaster, secondaryStatus);
			
			if(lstStatusWiseAutoEmailSends!=null && lstStatusWiseAutoEmailSends.size() > 0)
				sAutoEmailSend="1";
			
			List<DistrictMaxFitScore> lstDistrictMaxFitScores= new ArrayList<DistrictMaxFitScore>();
			lstDistrictMaxFitScores=districtMaxFitScoreDAO.getMaxFitScoreByDID_SM_SS_msu(districtMaster, temp_status, temp_secStatus);
			List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges=new ArrayList<JobCategoryWiseStatusPrivilege>();
			jobCategoryWiseStatusPrivileges=jobCategoryWiseStatusPrivilegeDAO.getJobCWSP(jobOrder, secondaryStatus,statusMaster);
			
			//ShowInstructionAttacheFileName 
			boolean bShowInstructionAttacheFileName=false;
			String strShowInstructionAttacheFileName="";
			
			if(jobCategoryWiseStatusPrivileges.size()>0)
			{
				strShowInstructionAttacheFileName=jobCategoryWiseStatusPrivileges.get(0).getInstructionAttachFileName();
				if(userMaster.getEntityType()!=1 && strShowInstructionAttacheFileName!=null && !strShowInstructionAttacheFileName.equalsIgnoreCase(""))
					bShowInstructionAttacheFileName=true;
			}
			
			List<StatusSpecificQuestions> lstStatusSpecificQuestions = new ArrayList<StatusSpecificQuestions>();
			lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findQuestions_msu(districtMaster, jobCategoryMaster, secondaryStatus, statusMaster);
			Double iScoreMaxSum=0.0;
			if(lstStatusSpecificQuestions!=null && lstStatusSpecificQuestions.size() > 0)
			{
				for(StatusSpecificQuestions questions:lstStatusSpecificQuestions)
				{
					if(questions!=null && questions.getMaxScore() > 0)
						iScoreMaxSum=iScoreMaxSum+questions.getMaxScore();
				}
			}
			
			int iTopSliderDisable=1;
			boolean isDispalyTopSlider=false;
			Double iMaxFitScore=0.0;
			int iScoreProvided=0;
			Double iTopSliderMaxValue=0.0;
			Double iSliderInterval=10.0;
			
			if(lstDistrictMaxFitScores.size()==1)
			{
				iMaxFitScore=lstDistrictMaxFitScores.get(0).getMaxFitScore();
				if(iMaxFitScore > 0.0)
					isDispalyTopSlider=true;
				else
					iTopSliderDisable=0;
			}
			else
			{
				iTopSliderDisable=0;	
			}
			
			
			if(iScoreMaxSum > 0.0)
			{
				isDispalyTopSlider=true;
				iTopSliderMaxValue=iScoreMaxSum;
				iTopSliderDisable=0;
			}
			else
			{
				iTopSliderMaxValue=iMaxFitScore;
			}
			iSliderInterval=getSliderInterval(iTopSliderMaxValue);
			
			
			sb.append("<table border='0' style='width:670px;'>");
			
			if(isDispalyTopSlider)
			{
					sb.append("<tr><td align=left id='sliderStatusNote'>");
					sb.append("<table border=0><tr><td style='vertical-align:center;height:33px;padding-left:8px'><label >"+lblScr+" </label></td>");
					sb.append("<td style='padding-left:5px;vertical-align:bottom;height:33px;'>&nbsp;<iframe id=\"ifrmTopSliderCGMass\"  src=\"slideract.do?name=topSliderCGMass&tickInterval="+iSliderInterval+"&max="+iTopSliderMaxValue+"&swidth=575&dslider="+iTopSliderDisable+"&svalue="+iScoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:620px;margin-top:-11px;\"></iframe></td>");
					
					sb.append("<td valign=top style='padding-top:4px;'>");					
					
					if(bShowInstructionAttacheFileName)
					{
						String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
						String fileName=strShowInstructionAttacheFileName;
						String filePath=""+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusId();
						sb.append("&nbsp;<a data-original-title='"+lblClickToViewInst+"' rel='tooltip' id='attachInstructionFileName' href='javascript:void(0);' onclick=\"downloadAttachInstructionFileName_msu('"+filePath+"','"+fileName+"','"+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusName()+"');"+windowFunc+"\"><span class='icon-info-sign icon-large iconcolor'></span></a>");
					}
					
					sb.append("</td></tr>");
					sb.append("</table>");
					sb.append("</td></tr>");
			}
			else
			{
				sb.append(OnlyShowInstructionAttacheFileNameWithOutSlider(bShowInstructionAttacheFileName, strShowInstructionAttacheFileName, jobCategoryWiseStatusPrivileges));
			}
			
			//************* Start ... Display Question and List
			String sQNoteId="";
			int iQsliderDisable=1;
			int iCountScoreSlider=0;
			if(userMaster.getEntityType()!=1)
			{
				iCountScoreSlider=0;
				if(lstStatusSpecificQuestions.size()>0)
				{
					sb.append("<tr><td align=left>");
					int questionNumber=1;
					
					
					int iQuetionScoreProvided=0;
					for(StatusSpecificQuestions ssq:lstStatusSpecificQuestions)
					{
						if(ssq.getQuestion()!=null && !ssq.getQuestion().equalsIgnoreCase(""))
							sb.append("<div class='row question_padding_left '><span><label class='labletxt'><B>"+headQues+":</B> "+ssq.getQuestion()+"</label></span></div>");
							
						if(ssq.getMaxScore()!=null)
							iTopSliderMaxValue=ssq.getMaxScore();
							
						iSliderInterval=getSliderInterval(iTopSliderMaxValue);
							
						if(ssq.getSkillAttributesMaster()!=null)
							sb.append("<div class='row question_padding_left'><span><label  class='lableAtxt'><B>"+lblAttribute1+":</B> "+ssq.getSkillAttributesMaster().getSkillName()+"</label></span></div>");
							
						if(ssq.getMaxScore()!=null && ssq.getMaxScore()>0)
						{
							iCountScoreSlider++;
							sb.append("<div class='row mt10' style='padding-left: 22px;'>");
							sb.append("<div class='span5 slider_question'><iframe id=\"ifrmQuestion_msu_"+iCountScoreSlider+"\" src=\"slideract.do?name=questionFrm_msu&tickInterval="+iSliderInterval+"&max="+ssq.getMaxScore()+"&dslider="+iQsliderDisable+"&swidth=575&step=1&answerId_msu="+ssq.getQuestionId()+"&svalue="+iQuetionScoreProvided+"\" scrolling=\"no\" frameBorder=\"0\" style=\"border: 0px; padding: 0px; margin: 0px; overflow: hidden; text-align: top; vertical-align: top;height:40px;width:615px;margin-top:-11px;\"></iframe></div>");
							sb.append("</div>");
						}
						
						if(ssq.getQuestion()!=null && !ssq.getQuestion().equals(""))
						{
							int queId=ssq.getQuestionId();
							sb.append("<div class='row mt5'>");
							sb.append("<div class='left25' id='questionNotes"+queId+"' >");
							sb.append("<label >"+lblNote+"</label>");
							sb.append("<textarea id='questionNotes"+queId+"' name='questionNotes"+queId+"' class='span6' rows='2'></textarea>");
							sb.append("</div>");
							sb.append("</div>");
							
							
							
							if(!sQNoteId.equals(""))
								sQNoteId = sQNoteId+"#"+queId;
							else
								sQNoteId=queId+"";
							
						}
						questionNumber++;	
					}
					
					if(questionNumber>1)
						sb.append("<div class='row mt10'></div>");
					
					sb.append("</td></tr>");
				}
				else
				{
					iQsliderDisable=0;
				}
			}
			
			
			//added by 12-06-2015
			int requiredNotesForUno=0; //added by 04-04-2015
				int isSchoolOnJobId=0;
					List<SchoolInJobOrder> listSIJO=new ArrayList<SchoolInJobOrder>();
					Boolean checkSchoolOnJobIdSetOrNot=OFFER_READY_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
					if(checkSchoolOnJobIdSetOrNot!=null && checkSchoolOnJobIdSetOrNot){
					try{
						listSIJO=schoolInJobOrderDAO.findByCriteria(Restrictions.eq("jobId", jobOrder));
						if(listSIJO.size()>0)
							isSchoolOnJobId=1;					
				}catch(Exception e){e.printStackTrace();}	
			}
					int isRequiredSchoolIdCheck=0;
					int alreadyExistsSchoolInStatus=0;
					String schoolNameValue="";
					String schoolIdValue="";
					println("listSIJO size========================================================================="+listSIJO.size());
					Boolean checkFlagForOFFER_READY=OFFER_READY_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
					if(checkFlagForOFFER_READY!=null && checkFlagForOFFER_READY){
						/*Boolean schoolInputBoxHideOnStatus=SCH_INPUTBOX_HIDE.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
						if(schoolInputBoxHideOnStatus!=null && schoolInputBoxHideOnStatus){
							isRequiredSchoolIdCheck=0;
							}else*/
						isRequiredSchoolIdCheck=1;
						println("Enter================");
						if(listSIJO!=null && listSIJO.size()==1){
							alreadyExistsSchoolInStatus=1;
							SchoolInJobOrder sijo=listSIJO.get(0);
							schoolNameValue = sijo.getSchoolId().getSchoolName().replace(" ", "!!");
							schoolIdValue=sijo.getSchoolId().getSchoolId().toString();
							println(schoolIdValue+"================nnnnnnn============================="+schoolNameValue);
						}						
					}
					Boolean isAlwaysRequiredSchool=IS_ALWAYS_REQUIRED_SCH_MAP.get(jobOrder.getDistrictMaster().getDistrictId()+"##"+userMaster.getEntityType()+"##"+recentStatusName.trim());
					if(isAlwaysRequiredSchool!=null && isAlwaysRequiredSchool){
					isSchoolOnJobId=1;
					}
					
					sb.append("<input type='hidden' name='isSchoolOnJobId' id='isSchoolOnJobId' value="+isSchoolOnJobId+">");
					sb.append("<input type='hidden' name='isRequiredSchoolIdCheck' id='isRequiredSchoolIdCheck' value="+isRequiredSchoolIdCheck+">");
					sb.append("<input type='hidden' name='alreadyExistsSchoolInStatus' id='alreadyExistsSchoolInStatus' value="+alreadyExistsSchoolInStatus+">");
					sb.append("<input type='hidden' name='schoolNameValue' id='schoolNameValue' value="+schoolNameValue+">");
					sb.append("<input type='hidden' name='schoolIdValue' id='schoolIdValue' value="+schoolIdValue+">");
					sb.append("<input type='hidden' name='slcStartDateOnOrOff' value="+slcStartDateOnOrOff+">");
					System.out.println("uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu");
			//added by 12-06-2015
			
			
			boolean bStatusNotes=false; 
			if(districtMaster.getStatusNotes()!=null)
				bStatusNotes=districtMaster.getStatusNotes();
			
			//System.out.println(districtMaster.getDistrictId() +" bStatusNotes "+bStatusNotes);
			
			sb.append("<input type='hidden' name='topSliderDisable_CGMass' id='topSliderDisable_CGMass' value="+iTopSliderDisable+">");
			sb.append("<input type='hidden' name='countQuestionSlider_CGMass' id='countQuestionSlider_CGMass' value="+iCountScoreSlider+">");
			sb.append("<input type='hidden' name='statusNoteflag_CGMass' id='statusNoteflag_CGMass' value="+bStatusNotes+">");
			
			sb.append("<input type='hidden' id='masterQuestionId' value='"+sQNoteId+"'>");
			
			System.out.println("sQNoteId "+sQNoteId);
			// End ... Display Question list from statusSpecificScore
			List<DistrictSpecificTiming> districtSpecificTiming=null;
			List<DistrictSpecificReason> districtSpecificReason=null;
			districtSpecificTiming=districtSpecificTimingDAO.getAllTimingForDistrict(districtMaster);
			districtSpecificReason=districtSpecificReasonDAO.getAllReasonForDistrict(districtMaster);
			if (districtSpecificTiming.size()==0) {
				districtSpecificTiming=districtSpecificTimingDAO.getAllTimingDefault();
			}
			if (districtSpecificReason.size()==0) {
				districtSpecificReason=districtSpecificReasonDAO.getAllReasonDefault();
			}
			
			if(userMaster.getEntityType()==1){
				sb.append("<tr ><td >");
				sb.append("<div class='col-sm-10 hide' id=\"timeAndReason_mass\" style='margin-top: -30px;margin-bottom: 28px;'>");
				}else if(userMaster.getEntityType()!=1){
					sb.append("<tr ><td >");
					sb.append("<div class='col-sm-10 hide' id=\"timeAndReason_mass\">");
				}
				sb.append("<div class='col-sm-5'>");
				sb.append("<label>Timing:</label>");
				sb.append("<select id=\"timingforDclWrdw_mass\" class='form-control'>");
				sb.append("<option value='0'>Select time</option>");
				for (DistrictSpecificTiming districtSpecificTiming2 : districtSpecificTiming) {
					sb.append("<option value='"+districtSpecificTiming2.getTimingId()+"'>"+districtSpecificTiming2.getTiming()+"</option>");
				}
				sb.append("</select>");
				sb.append("</div>");
				sb.append("<div class='col-sm-5'>");
				sb.append("<label>Reason:</label>");
				sb.append("<select id=\"reasonforDclWrdw_mass\" class='form-control'>");
				sb.append("<option value='0'>Select time</option>");
				for (DistrictSpecificReason districtSpecificReason2 : districtSpecificReason) {
					sb.append("<option value='"+districtSpecificReason2.getReasonId()+"'>"+districtSpecificReason2.getReason()+"</option>");
				}
				sb.append("</select>");
				sb.append("</div>");
				sb.append("</div>");
				sb.append("</td></tr>");
			
			if(userMaster.getEntityType()!=1){
				sb.append("<tr><td align=right style='padding-right:15px;'>");
				sb.append("<span id=\"statusAddNote_mass\"><a href=\"#\" onclick=\"openNoteDiv_mass("+userMaster.getDistrictId().getDistrictId()+")\">"+lblAddNote1+"</a></span>");
				sb.append("</td></tr>");
			}
			
			//String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			sb.append("<tr><td>");
			sb.append("<table border='0' class='table' id='tblStatusNote_mass'>");
			sb.append("<thead class='bg'>");
			sb.append("<tr>");
			sb.append("<th valign=top>"+lblNote+"</th>");
			sb.append("<th valign=top>"+hdAttachment+"</th>");
			sb.append("<th valign=top>"+lblCreatedBy+"</th>");
			sb.append("<th valign=top>"+lblCreatedDate+"</th>");
			sb.append("<th valign=top>"+lblAct+"</th>");
			sb.append("</tr>");
			sb.append("</thead>");
			sb.append("<tr>");				
			sb.append("<td colspan=4>"+lblNoNoteAdded+"</td>");
			sb.append("</tr>");
			sb.append("</table>");
			sb.append("</td></tr>");
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		sb.append("</table>");
		
		String sCGInfoWindowShow="0";
		String sCGInfoMessage="";
		
		String sCGInfoNotification="";
		
		if(offerAndAccepted || sOfclTransVeteranPref.equals("1") || sPanel.equalsIgnoreCase("1") || sMultipleRequisitionNumber.equalsIgnoreCase("1") || sAllDPointSetStatus.equals("1") || sAllSetStatusForOfferReady.equals("1") || sAllDPointSetStatus_partial.equals("1") || sAllSetStatusForOfferReady_partial.equals("1") || sOfferReady_NonInst.equals("1") || sHWRD_Partial.equals("1") || sHWRD_All.equals("1") || sJSI.equals("1"))
		{
			sCGInfoWindowShow="1";
			if(offerAndAccepted){
				sCGInfoMessage=sCGInfoMessage+"&#149; "+msgStatusOfCandidateCouldNotSet+" "+sDisplayStatusName+""+msgStatusOfCandidateCouldNotSet1+"<BR>";
			}
			if(sHWRD_All.equalsIgnoreCase("1"))
			{
				//sCGInfoMessage=sCGInfoMessage+"&#149; Status of these Candidate could not be set to GREEN because of any of the following reason:<BR>&#149;	Candidate is already hired<BR>&#149; Candidate is already rejected by Admin<BR>&#149; Candidate has already declined the job<BR>&#149; Candidate has already withdrew from the job<BR>";
				sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired+"<BR>&#149; "+lblCandidatealreadyhired1+"<BR>&#149; "+lblCandidatealreadyhired2+"<BR>&#149; "+lblCandidatealreadyhired3+"<BR>";
			}
			if(sPanel.equalsIgnoreCase("1") || sOfferReady_NonInst.equalsIgnoreCase("1"))
				sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired4+" '"+sDisplayStatusName+"' "+lblCandidatealreadyhired5+".<BR>";
			if(sOfclTransVeteranPref.equalsIgnoreCase("1") || sJSI.equalsIgnoreCase("1"))
				sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired6+" '"+sDisplayStatusName+"'.<BR>";
			if(sMultipleRequisitionNumber.equalsIgnoreCase("1"))
				sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired7+".<BR>";
			if(sAllDPointSetStatus.equalsIgnoreCase("1"))
				sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired8+"'"+sDisplayStatusName+"' "+lblCandidatealreadyhired9+".<BR>";
			if(sAllSetStatusForOfferReady.equalsIgnoreCase("1"))
				sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired9+" '"+sDisplayStatusName+"' "+lblCandidatealreadyhired10+".<BR>";
			
			if(sOfclTransVeteranPref.equals("1") || sPanel.equalsIgnoreCase("1") || sMultipleRequisitionNumber.equalsIgnoreCase("1") || (sAllSetStatusForOfferReady.equals("1") && sOfferReady_NonInst.equals("0")) || sHWRD_All.equals("1") || sJSI.equals("1") || sAllDPointSetStatus.endsWith("1"))
			{
				sCGInfoNotification="0";
				sCGInfoMessage=""+lblCandidatealreadyhired11+":<BR>"+sCGInfoMessage;
			}
			else
			{
				if(sAllDPointSetStatus_partial.equals("1") || (sAllSetStatusForOfferReady_partial.equals("1") && sOfferReady_NonInst.equals("0")) || sHWRD_Partial.equals("1"))
				{
					sCGInfoNotification="1";
					if(sAllDPointSetStatus_partial.equalsIgnoreCase("1"))
						sCGInfoMessage=sCGInfoMessage+"&#149;"+lblCandidatealreadyhired12+" '"+sDisplayStatusName+"' "+lblCandidatealreadyhired13+".<BR>";
					else if(sAllSetStatusForOfferReady_partial.equalsIgnoreCase("1"))
						sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired14+" '"+sDisplayStatusName+"' "+lblCandidatealreadyhired13+".<BR>";
					else if(sHWRD_Partial.equalsIgnoreCase("1"))
						sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired15+"<BR>";
					sCGInfoMessage=sCGInfoMessage+""+lblCandidatealreadyhired16+" '"+sDisplayStatusName+"' "+lblCandidatealreadyhired17+"";
				}
			}
			
		}
		
		String sReturnArray[]=new String[13];
		sReturnArray[0]=sb.toString();
		sReturnArray[1]=sCGInfoWindowShow;
		sReturnArray[2]=sCGInfoMessage;
		sReturnArray[3]=sDoActivity;
		sReturnArray[4]=sPanel;
		sReturnArray[5]=sStatus;
		sReturnArray[6]=sMultipleRequisitionNumber;
		sReturnArray[7]=sAllDPointSetStatus;
		sReturnArray[8]=sAllSetStatusForOfferReady;
		sReturnArray[9]=sAlreadyOffer;
		sReturnArray[10]=sSchoolCount;
		sReturnArray[11]=sAutoEmailSend;
		sReturnArray[12]=sCGInfoNotification;
		
		return sReturnArray;
	}
	
	public Double getSliderInterval(Double iMaxSliderValue)
	{
		Double interval=10.0;
		if(iMaxSliderValue%10.0==0)
			interval=10.0;
		else if(iMaxSliderValue%5.0==0)
			interval=5.0;
		else if(iMaxSliderValue%3.0==0)
			interval=3.0;
		else if(iMaxSliderValue%2.0==0)
			interval=2.0;
		return interval; 
	}
	
	public String OnlyShowInstructionAttacheFileNameWithOutSlider(boolean bShowInstructionAttacheFileName,String strShowInstructionAttacheFileName,List<JobCategoryWiseStatusPrivilege> jobCategoryWiseStatusPrivileges)
	{
		StringBuffer sb=new StringBuffer();
		if(bShowInstructionAttacheFileName)
		{
			sb.append("<tr><td align=left bgcolor=white>");
			sb.append("<table border=0><tr><td style='vertical-align:top;width:580px;'>&nbsp;</td>");
			sb.append("<td style='padding-left:5px;vertical-align:bottom;width:42px;'>&nbsp;</td>");
			sb.append("<td valign=top style='padding-top:4px;' align='right'>");
			String windowFunc="if(this.href!='javascript:void(0);')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			String fileName=strShowInstructionAttacheFileName;
			String filePath=""+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusId();
			sb.append("&nbsp;<a data-original-title='"+lblClickToViewInst+"' rel='tooltip' id='attachInstructionFileName' href='javascript:void(0);' onclick=\"downloadAttachInstructionFileName_msu('"+filePath+"','"+fileName+"','"+jobCategoryWiseStatusPrivileges.get(0).getSecondaryStatus().getSecondaryStatusName()+"');"+windowFunc+"\"><span class='icon-info-sign icon-large iconcolor'></span></a>");
			sb.append("</td></tr>");
			sb.append("</table>");
			sb.append("</td></tr>");
		}
		else
		{
			sb.append("&nbsp;");
		}
		return sb.toString();
	}
	
	
	public String getSectionWiseTemaplteslist_CGMass(Integer sectionEmailId){
		try{
			StatusWiseEmailSection statusWiseEmailSection = statusWiseEmailSectionDAO.findById(sectionEmailId, false, false);
			Criterion criteria =Restrictions.eq("statusWiseEmailSection",statusWiseEmailSection);
			List<StatusWiseEmailSubSection> sectionWiseTemaplteslist = statusWiseEmailSubSectionDAO.findByCriteria(Order.asc("subSectionName"),criteria);

			String statuswisesectiontemplateslist="<select class='form-control' id='statuswisesectiontemplateslist_mass' onchange='setTemplateByLIstId_mass(this.value)'><option value='0'>"+lblSelectSubTemplate1+"</option>";

			for (StatusWiseEmailSubSection statusWiseEmailSubSection : sectionWiseTemaplteslist)
				statuswisesectiontemplateslist+="<option value="+statusWiseEmailSubSection.getEmailSubSectionId()+">"+statusWiseEmailSubSection.getSubSectionName()+"</option>";

			statuswisesectiontemplateslist+="</select>";
			return statuswisesectiontemplateslist;
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public StatusSpecificEmailTemplates getStatusWiseEmailForAdmin_CGMass(String jobId,String statusId,String secondaryStatusId)
	{
		
		System.out.println("Calling getStatusWiseEmailForAdmin_CGMass");
		
		UserMaster userMaster =null;
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session = request.getSession(false);
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		else{
			userMaster = (UserMaster) session.getAttribute("userMaster");
		}
		try{

			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatus=null;
			
			String statusShortName="oth";
			String statusName="";
			
			if((statusId!=null && !statusId.equals("0") && secondaryStatusId!=null && !secondaryStatusId.equals("0")) || (secondaryStatusId!=null && !secondaryStatusId.equals("0")) )
			{
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
				statusName=secondaryStatus.getSecondaryStatusName();
			}
			else if(statusId!=null && !statusId.equals("0"))
			{
				statusMaster=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
				statusName=statusMaster.getStatus();
				statusShortName=statusMaster.getStatusShortName();
			}
			
			if(statusShortName!=null && !statusShortName.equalsIgnoreCase("hird") && !statusShortName.equalsIgnoreCase("rem") && !statusShortName.equalsIgnoreCase("dcln"))
				statusShortName="oth";
			
			if(jobId!=null && !jobId.equals("0"))
			{
				JobOrder jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false,false);
				if(jobOrder!=null)
				{
					String[] arrHrDetail = manageStatusAjax.getHrDetailToTeacher(request,jobOrder);
					
					StringBuffer sb =new StringBuffer();
					String hrContactFirstName = arrHrDetail[0];
					String hrContactLastName = arrHrDetail[1];
					String hrContactEmailAddress = arrHrDetail[2];
					String phoneNumber			 = arrHrDetail[3];
					String title		 = arrHrDetail[4];
					int isHrContactExist = Integer.parseInt(arrHrDetail[5]);
					String schoolDistrictName =	arrHrDetail[6];
					String dmName 			 = arrHrDetail[7];
					String dmEmailAddress = arrHrDetail[8];
					String dmPhoneNumber	 = arrHrDetail[9];

					sb.append("<table>");
					sb.append("<tr>");
					sb.append("<td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>");
					if(isHrContactExist==0)
						sb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
					else 
						if(isHrContactExist==1)
						{
							sb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
							if(title!="")
								sb.append(title+"<br/>");
							if(phoneNumber!="")
								sb.append(phoneNumber+"<br/>");
							sb.append(schoolDistrictName+"<br/><br/>");
						}
						else
						{
							if(isHrContactExist==2)
							{
								sb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+dmName+"<br/>");
								if(dmPhoneNumber!="")
									sb.append(dmPhoneNumber+"<br/>");
								sb.append(schoolDistrictName+"<br/><br/>");
							}
						}
					sb.append("</td>");
					sb.append("</tr>");
					sb.append("</table>");

					String url = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity();
					Criterion criterion2=Restrictions.eq("statusShortName",statusShortName);
					List<StatusSpecificEmailTemplates> lstStatusSpecificEmailTemplates	=	statusSpecificEmailTemplatesDAO.findByCriteria(criterion2);

					String mailBody	="";

					if(lstStatusSpecificEmailTemplates.size()>0)
					{
						mailBody	=	lstStatusSpecificEmailTemplates.get(0).getTemplateBody();
						if(mailBody.contains("&lt; "+lblTeacherName1+" &gt;") || mailBody.contains("&lt; "+lblUserName+" &gt;") || mailBody.contains("&lt; "+lblJoTil+" &gt;")|| mailBody.contains("&lt;"+lblStatusName+" &gt;"))
						{
							//System.out.println("\n\n =================================================================================================");
							//mailBody	=	mailBody.replaceAll("&lt; Teacher Name &gt;",jbforteacher.getTeacherId().getFirstName()+" "+jbforteacher.getTeacherId().getLastName());
							mailBody	=	mailBody.replaceAll("&lt;"+lblUserName+"&gt;",userMaster.getFirstName()+" "+userMaster.getLastName());
							mailBody	=	mailBody.replaceAll("&lt; "+lblJoTil+" &gt;",jobOrder.getJobTitle());
							mailBody	=	mailBody.replaceAll("&lt; "+lblStatusName+" &gt;",statusName);
							mailBody	=	mailBody.replaceAll("&lt; "+lblURL1+" &gt;",url);
							mailBody	=	mailBody.replaceAll("&lt; "+lblHrContact1+" &gt;",sb.toString());
							mailBody	=	mailBody.replaceAll("&lt; "+lblUserEmailAddress1+" &gt;",userMaster.getEmailAddress());
						}
						lstStatusSpecificEmailTemplates.get(0).setTemplateBody(mailBody);	
						return lstStatusSpecificEmailTemplates.get(0);
					}
				}
			}
			
			
		}catch (Exception e) 
		{
			e.printStackTrace();
		}
		return null;

	}
	
	
	public boolean isAllDPointSet(JobOrder jobOrder,StatusMaster statusMaster,List<TeacherDetail> lstTeacherDetail)
	{
		boolean bAllDPointSet=true;
		if(jobOrder.getDistrictMaster()!=null && statusMaster!=null && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")))
		{
			String secondaryStatusNames="";
			if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null){
				if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()){
					String accessDPoints="";
					if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
						accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();

					if(accessDPoints.contains(""+statusMaster.getStatusId())){
						List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs=new ArrayList<TeacherStatusHistoryForJob>();
						if(lstTeacherDetail!=null && lstTeacherDetail.size() > 0 && jobOrder!=null)
						{
							teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.massStatusSelected(lstTeacherDetail, jobOrder);
							List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatus(jobOrder,statusMaster);

							Map<String,SecondaryStatus> secMap=new HashMap<String, SecondaryStatus>();
							if(teacherStatusHistoryForJobs!=null && teacherStatusHistoryForJobs.size() > 0)
							{
								for(TeacherStatusHistoryForJob tSHObj : teacherStatusHistoryForJobs){
									if(tSHObj.getSecondaryStatus()!=null)
										secMap.put(tSHObj.getTeacherDetail().getTeacherId()+"_"+tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
								}
							}
							for(TeacherDetail teacherDetail:lstTeacherDetail)
							{
								if(teacherDetail!=null)
								{
									if(listSecondaryStatus!=null && listSecondaryStatus.size()>0)
									{
										secondaryStatusNames="";
										int counter=0;
										for(SecondaryStatus  obj : listSecondaryStatus.get(0).getChildren())
										{
											if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null)
											{
												SecondaryStatus sec=secMap.get(teacherDetail.getTeacherId()+"_"+obj.getSecondaryStatusId());
												if(sec==null)
												{
													boolean isJobAssessment=false;
													if(jobOrder.getIsJobAssessment()!=null)
													{
														if(jobOrder.getIsJobAssessment())
														{
															isJobAssessment=true;
														}
													}
													if((isJobAssessment && obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))|| !obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))
													{
														if(counter==1){
															secondaryStatusNames+=", ";
														}
														counter=0;
														secondaryStatusNames+=obj.getSecondaryStatusName();
														counter++;
														bAllDPointSet=false;
														break;
													}
												}
											}
										}
									}
								}
								
								if(secondaryStatusNames!=null && secondaryStatusNames.length() > 0)
									break;
							}
						}
					}
				}
			}
		}
		return bAllDPointSet;
	}
	
	
	public boolean isValidateForOfferReady(JobOrder jobOrder,List<TeacherDetail> lstTeacherDetail)
	{
		boolean bAllSetStatusForOfferReady=true;
		List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs=new ArrayList<TeacherStatusHistoryForJob>();
		if(lstTeacherDetail!=null && lstTeacherDetail.size() > 0 && jobOrder!=null)
		{
			Map<String,Integer> allStsMap = new HashMap<String, Integer>();
			teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.massStatusSelected(lstTeacherDetail, jobOrder);
			List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithOutStatus(jobOrder);
			
			Map<String,Integer> cmpMap = new HashMap<String, Integer>();
			for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : teacherStatusHistoryForJobs) 
			{
				if(teacherStatusHistoryForJob.getSecondaryStatus()!=null)
				{
					cmpMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"_"+teacherStatusHistoryForJob.getSecondaryStatus().getSecondaryStatusName(),1);
				}
				if(teacherStatusHistoryForJob.getStatusMaster()!=null && !teacherStatusHistoryForJob.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp"))
				{
					cmpMap.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+"_"+teacherStatusHistoryForJob.getStatusMaster().getStatusShortName(),1);
				}
			}
			
			
			for(TeacherDetail teacherDetail:lstTeacherDetail)
			{
				if(teacherDetail!=null)
				{
					if(listSecondaryStatus!=null && listSecondaryStatus.size()>0)
					{
						
						for (SecondaryStatus secondaryStatus : listSecondaryStatus) 
						{
							if(secondaryStatus.getStatusMaster()!=null)
							{
								allStsMap.put(teacherDetail.getTeacherId()+"_"+secondaryStatus.getStatusMaster().getStatusShortName(), 1);
							}
							else
							{
								allStsMap.put(teacherDetail.getTeacherId()+"_"+secondaryStatus.getSecondaryStatusName(), 1);
							}
						}
					}
				}
			}
			
			for(TeacherDetail teacherDetail:lstTeacherDetail)
			{
				if(teacherDetail!=null)
				{
					int teacherId=teacherDetail.getTeacherId();
					if(allStsMap.size()>0)
					{
						allStsMap.remove(teacherId+"_vcomp");
						allStsMap.remove(teacherId+"_apl");
						allStsMap.remove(teacherId+"_Offer Ready");
						allStsMap.remove(teacherId+"_icomp");
						allStsMap.remove(teacherId+"_vlt");
						allStsMap.remove(teacherId+"_hird");
						allStsMap.remove(teacherId+"_dcln");
						allStsMap.remove(teacherId+"_rem");
						allStsMap.remove(teacherId+"_widrw");
						cmpMap.remove(teacherId+"_Offer Ready");
						cmpMap.remove(teacherId+"_hird");
						cmpMap.remove(teacherId+"_dcln");
						cmpMap.remove(teacherId+"_rem");
						cmpMap.remove(teacherId+"_widrw");
						cmpMap.remove(teacherId+"_vcomp");
						cmpMap.remove(teacherId+"_icomp");
						cmpMap.remove(teacherId+"_vlt");
						cmpMap.remove(teacherId+"_apl");
					}
				}
			}
			
			if(cmpMap.size()!=allStsMap.size())
				bAllSetStatusForOfferReady=false;
		}	
		return bAllSetStatusForOfferReady;
	}
	
	public String[] massPersistentData(int OverrideOrSkip,boolean bFinalize,String teacherIds,String jobId,String statusId,String secondaryStatusId,boolean bTopSliderEnable,Double topScoreProvided,boolean bQuestionSliderCount,int[] questionId_arrary,String[] scoreValue_arrary,int[] QuestionNoteIds_arrary,String[] sQuestionIds_Note_arrary,String statusEditorNotes,String statusNoteFileName,String requisitionNumberID,String requisitionNumberText,long schoolId,boolean bSentEmailToDASA,int isEmailTemplateChanged,String aDASAMsgSubject,String sAdminMailBody,Long schoolId_mass,String startDate,Integer timingforDclWrdw,Integer reasonforDclWrdw)
	{
		PrintOnConsole.debugPrintlnCGMass("Calling massPersistentData");
		try { PrintOnConsole.debugPrintlnCGMass("Parameters :: OverrideOrSkip "+OverrideOrSkip+" bFinalize "+bFinalize+" teacherIds "+teacherIds+" jobId "+jobId+" statusId "+statusId+" secondaryStatusId "+secondaryStatusId+" bTopSliderEnable "+bTopSliderEnable+" topScoreProvided "+topScoreProvided+" bQuestionSliderCount "+bQuestionSliderCount+" questionId_arrary "+questionId_arrary+" scoreValue_arrary "+scoreValue_arrary+" QuestionNoteIds_arrary "+QuestionNoteIds_arrary+" sQuestionIds_Note_arrary "+sQuestionIds_Note_arrary+" statusEditorNotes "+statusEditorNotes+" statusNoteFileName "+statusNoteFileName+" requisitionNumberID "+requisitionNumberID+" requisitionNumberText "+requisitionNumberText+" schoolId "+schoolId+" bSentEmailToDASA "+bSentEmailToDASA +" isEmailTemplateChanged "+isEmailTemplateChanged+" aDASAMsgSubject "+aDASAMsgSubject+" sAdminMailBody "+sAdminMailBody); } catch (Exception e) {}
		
		StringBuffer sb = new StringBuffer("");
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		
		String sDoActivity="0",sPanel="0",sStatus="0",sDistrictRequisitionNumber="0",sMultipleRequisitionNumber="0",sAllDPointSetStatus="0",sAllSetStatusForOfferReady="0",sAlreadyOffer="0",sDisplayStatusName="";
		String sSchoolCount="0",sAutoEmailSend="0",sQuestionCount="0",sQuestionIDMatch="0";
		Double iMaxFitScore=0.0;
		
		String sStatusShortCode="oth";
		String sTSS_Finalize="0";
		
		String sCGInfoWindowShow="0";
		String sCGInfoMessage="";
		int finalizeForEvent=0;
		try 
		{
			HttpSession session = request.getSession(false);
			if(session == null || session.getAttribute("userMaster")==null) 
			{
				throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
			}
			UserMaster userMaster = (UserMaster) session.getAttribute("userMaster");
			int roleId=0;
			int entityType=0;
			if(userMaster.getRoleId().getRoleId()!=null){
				roleId=userMaster.getRoleId().getRoleId();
				entityType=userMaster.getEntityType();
			}
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,38,"candidatereport.do",0);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			
			/////////////////////////////
			
			List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
			if(teacherIds!=null && teacherIds.length() > 0)
			{
				String teacherIDsArray[]=teacherIds.split(",");
				List<Integer> lstTeacherIds=new ArrayList<Integer>();
				for(String temp:teacherIDsArray)
					lstTeacherIds.add(Integer.parseInt(temp));
				
				lstTeacherDetails=teacherDetailDAO.findByTeacherIds(lstTeacherIds);
			}
			
			DistrictMaster districtMaster = null;
			JobCategoryMaster jobCategoryMaster=null;
			boolean isJeffcoDistrict=false;
			JobOrder jobOrder =jobOrderDAO.findById(new Integer(jobId), false, false);
			if(jobOrder!=null)
			{
				if(jobOrder.getDistrictMaster()!=null && jobOrder.getDistrictMaster().getDistrictId().equals(804800))
					isJeffcoDistrict=true;
				if(jobOrder.getDistrictMaster()!=null)
					districtMaster=jobOrder.getDistrictMaster();
				if(jobOrder.getJobCategoryMaster()!=null)
					jobCategoryMaster=jobOrder.getJobCategoryMaster();
			}
			
			boolean isMiami = false;
			try{
				if(districtMaster!=null && districtMaster.getDistrictId().equals(1200390))
					isMiami=true;
			}catch(Exception e){}

			String sDistrictName=districtMaster.getDistrictName();
			
			boolean isSubstituteInstructionalJob = false;
			try{
				if(isMiami && jobOrder!=null && jobOrder.getJobCategoryMaster().getJobCategoryName().equalsIgnoreCase("Substitute Instructional Positions"))
					isSubstituteInstructionalJob=true;
			}catch(Exception e){}
			
			SchoolMaster schoolMaster =null;
			if(jobOrder.getCreatedForEntity()==3){
				try{
					schoolMaster=jobOrder.getSchool().get(0);
				}catch(Exception e){}
			}
			
			StatusMaster statusMaster=null;
			SecondaryStatus secondaryStatus=null;

			if(statusId!=null && !statusId.equals("0")){
				statusMaster=WorkThreadServlet.statusIdMap.get(Integer.parseInt(statusId));
			}
			
			if(secondaryStatusId!=null && !secondaryStatusId.equals("0")){
				secondaryStatus=secondaryStatusDAO.findById(Integer.parseInt(secondaryStatusId),false,false);
			}
			String sStatusShortCodeEmail="oth";
			if((statusMaster!=null && secondaryStatus!=null) || secondaryStatus!=null)
			{
				sDisplayStatusName=secondaryStatus.getSecondaryStatusName();
				
				if(statusMaster!=null && (statusMaster.getStatusShortName().equals("scomp") || statusMaster.getStatusShortName().equals("ecomp") || statusMaster.getStatusShortName().equals("vcomp")))
					sStatusShortCode=statusMaster.getStatusShortName();
			}
			else if(statusMaster!=null)
			{
				sDisplayStatusName=statusMaster.getStatus();
				if(statusMaster!=null && (statusMaster.getStatusShortName().equals("hird") || statusMaster.getStatusShortName().equals("rem") || statusMaster.getStatusShortName().equals("dcln")))
					sStatusShortCodeEmail=statusMaster.getStatusShortName();
			}
			
			SecondaryStatus temp_secStatus=null;
			StatusMaster temp_status=null;
			if(secondaryStatus!=null)
			{
				if(secondaryStatus.getStatusMaster()!=null)
					temp_status=secondaryStatus.getStatusMaster();
				else if(secondaryStatus.getSecondaryStatus_copy()!=null)
					temp_secStatus=secondaryStatus.getSecondaryStatus_copy();
				else
					temp_secStatus=secondaryStatus;
			}
			else if(statusMaster!=null)
				temp_status=statusMaster;
			// OfferReady
			boolean bOfferReady=false;
			if(isMiami && secondaryStatus!=null && !secondaryStatus.getSecondaryStatusName().equals("") && secondaryStatus.getSecondaryStatusName().equals("Offer Ready"))
				bOfferReady=true;	
			
			SecondaryStatus secondaryStatus_temp_OR=null;
			StatusMaster statusMaster_temp_OR=null;
			List<SecondaryStatus> lstSecondaryStatus_temp_OR=new ArrayList<SecondaryStatus>();

			if(statusMaster!=null)
			{
				lstSecondaryStatus_temp_OR=secondaryStatusDAO.getSecondaryStatusForPanel(jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster(), statusMaster);
				if(lstSecondaryStatus_temp_OR.size() ==1)
				{
					secondaryStatus_temp_OR=lstSecondaryStatus_temp_OR.get(0);
					statusMaster_temp_OR=null;
				}
				else
				{
					statusMaster_temp_OR=statusMaster;
				}
			}
			else
			{
				secondaryStatus_temp_OR=secondaryStatus;
			}
			
			String statusIds="";
			try{
				if(statusMaster_temp_OR!=null){
					statusIds="s##"+statusMaster_temp_OR.getStatusId();
				}else{
					statusIds="ss##"+secondaryStatus_temp_OR.getSecondaryStatusId();
				}
			}catch(Exception e){}

			int userId=0;
			try{
				if(userMaster!=null){
					userId=userMaster.getUserId();
				}
			}catch(Exception e){}
			// End OfferReady
			
			// Offer Accepted
			boolean bOfferAccepted=false;
			if(isMiami && secondaryStatus!=null && !secondaryStatus.getSecondaryStatusName().equals("") && secondaryStatus.getSecondaryStatusName().equals("Offer Accepted"))
				bOfferAccepted=true;
			//End Offer Accepted
			
			// Email
			String[] arrHrDetail =manageStatusAjax.getHrDetailToTeacher(request,jobOrder);
			String sGridURL = Utility.getValueOfPropByKey("basePath")+"/candidategrid.do?jobId="+jobOrder.getJobId()+"&JobOrderType="+jobOrder.getCreatedForEntity();
			
			String sUserFullName="";
			if(userMaster.getLastName()!=null)
				sUserFullName=userMaster.getFirstName()+" "+userMaster.getLastName();	 
			else
				sUserFullName=userMaster.getFirstName();

			String sJobTitle=jobOrder.getJobTitle();
			
			/*List<UserMaster> lstUserMasters=new ArrayList<UserMaster>();
			lstUserMasters=getUserMasterListForEmail_CGMass(jobOrder, userMaster, sStatusShortCode, districtMaster);*/

			//Criterion criterion_StatusShortCode=Restrictions.eq("statusShortName",sStatusShortCode);
			System.out.println("sStatusShortCodeEmail "+sStatusShortCodeEmail);
			Criterion criterion_StatusShortCode=Restrictions.eq("statusShortName",sStatusShortCodeEmail);
			List<StatusSpecificEmailTemplates> lstStatusSpecificEmailTemplates=statusSpecificEmailTemplatesDAO.findByCriteria(criterion_StatusShortCode);
			
			//End Email
			
			/*SchoolInJobOrder schoolInJobOrder = null;
			if(userMaster.getEntityType()==3){
				SchoolMaster schoolMaster_User = userMaster.getSchoolId();				
				schoolInJobOrder = schoolInJobOrderDAO.findSchoolInJobBySchoolAndJob(jobOrder, schoolMaster_User);
			}*/
			CandidateGridService cgService=new CandidateGridService();
			//boolean doActivity=cgService.isVisible(jobOrder,userMaster,schoolInJobOrder);
			
			List<DistrictMaxFitScore> lstDistrictMaxFitScores= new ArrayList<DistrictMaxFitScore>();
			lstDistrictMaxFitScores=districtMaxFitScoreDAO.getMaxFitScoreByDID_SM_SS_msu(districtMaster, temp_status, temp_secStatus);
			if(lstDistrictMaxFitScores.size()==1)
				iMaxFitScore=lstDistrictMaxFitScores.get(0).getMaxFitScore();
			
			List<TeacherStatusScores> lstTSS_V=new ArrayList<TeacherStatusScores>();
			if(bFinalize)
			{
				if(bTopSliderEnable && topScoreProvided >0)
				{
					lstTSS_V= teacherStatusScoresDAO.getFinalizeTeacherStatusScoreCGMass(lstTeacherDetails,jobOrder,statusMaster,secondaryStatus,userMaster);
				}
			}
			
			//Question
			
			System.out.println("scoreValue_arrary "+scoreValue_arrary.length);
			boolean bQValueFromUser=false;
			if(scoreValue_arrary!=null && scoreValue_arrary.length > 0)
				bQValueFromUser=true;
			
			//boolean bCopyAllQuestionListToSSS=false;
			//List<TeacherDetail> lstTeacherDetails_delete_SSS=new ArrayList<TeacherDetail>();
			//List<TeacherDetail> lstTeacherDetails_delete_TSN=new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstTeacherDetails_Skip_SSS=new ArrayList<TeacherDetail>();
			
			int iMasterQuestionCount=0;
			boolean bIsMasterQuestionList=false;
			boolean bMaxScoreForMasterQuestion=false;
			Map<Integer, StatusSpecificQuestions> mapMasterQuestion=new HashMap<Integer, StatusSpecificQuestions>();
			List<StatusSpecificQuestions> lstStatusSpecificQuestions= new ArrayList<StatusSpecificQuestions>();
			
			//System.out.println("      ------------------          "+districtMaster+"     "+ jobCategoryMaster+"     "+ secondaryStatus+"     "+ statusMaster);
			lstStatusSpecificQuestions=statusSpecificQuestionsDAO.findQuestions_msu(districtMaster, jobCategoryMaster, secondaryStatus, statusMaster);
			if(lstStatusSpecificQuestions!=null && lstStatusSpecificQuestions.size() >0)
			{
				PrintOnConsole.debugPrintlnCGMass("statusSpecificQuestions have "+lstStatusSpecificQuestions.size());
				bIsMasterQuestionList=true;
				iMasterQuestionCount=lstStatusSpecificQuestions.size();
				for(StatusSpecificQuestions questions:lstStatusSpecificQuestions)
				{
					if(questions!=null)
					{
						if(mapMasterQuestion.get(questions.getQuestionId())==null)
							mapMasterQuestion.put(questions.getQuestionId(), questions);
						
						if(questions.getMaxScore()!=null && questions.getMaxScore() > 0)
							bMaxScoreForMasterQuestion=true;
						
					}
				}
			}
			else
			{
				bIsMasterQuestionList=false;
				PrintOnConsole.debugPrintlnCGMass("No statusSpecificQuestions ");
			}
			
			boolean bIsStatusSpecificScoreQuestionList=false;
			List<StatusSpecificScore> lstStatusSpecificScore_First=new ArrayList<StatusSpecificScore>();
			lstStatusSpecificScore_First=statusSpecificScoreDAO.getQuestionList_CGMass(jobOrder, statusMaster, secondaryStatus, lstTeacherDetails, userMaster);
			if(lstStatusSpecificScore_First!=null && lstStatusSpecificScore_First.size() > 0)
				bIsStatusSpecificScoreQuestionList=true;
			
			Map<Integer, List<StatusSpecificScore>> mapStatusSpecificScoreList=new HashMap<Integer, List<StatusSpecificScore>>();
			mapStatusSpecificScoreList=getSSSListMap(lstStatusSpecificScore_First);
			
			//System.out.println("lstStatusSpecificScore_First "+lstStatusSpecificScore_First.size());
			boolean bScoreProvidedTemp=false;
			boolean bIsFinalizeSSSQList=false;
			List<StatusSpecificScore> lstStatusSpecificScore_First_Finalize=new ArrayList<StatusSpecificScore>();
			lstStatusSpecificScore_First_Finalize=getFinalizeSSSList(lstStatusSpecificScore_First);
			if(lstStatusSpecificScore_First_Finalize!=null && lstStatusSpecificScore_First_Finalize.size() > 0)
			{	
				bIsFinalizeSSSQList=true;
				
				for(StatusSpecificScore score:lstStatusSpecificScore_First_Finalize)
				{
					if(score!=null && score.getScoreProvided()!=null && score.getScoreProvided() > 0)
					{
						bScoreProvidedTemp=true;
						break;
					}
				}
				
			}
			
			//System.out.println("lstStatusSpecificScore_First_Finalize "+lstStatusSpecificScore_First_Finalize.size());
			
			Map<Integer, List<StatusSpecificScore>> mapStatusSpecificScoreList_F=new HashMap<Integer, List<StatusSpecificScore>>();
			mapStatusSpecificScoreList_F=getSSSListMap(lstStatusSpecificScore_First_Finalize);
			
			String sMasterNoQ_CopiedAndFinalize="0";
			if(!bIsMasterQuestionList && bIsFinalizeSSSQList && bScoreProvidedTemp)
			{
				//Call O/S
				sMasterNoQ_CopiedAndFinalize="1";
			}
			
			String sAlreadyFinalizeQuestion="0";
			if(bQValueFromUser && bIsMasterQuestionList && bIsFinalizeSSSQList)
			{
				sAlreadyFinalizeQuestion="1";
			}
			
			if(OverrideOrSkip==0)
			{
				if(lstTSS_V!=null & lstTSS_V.size() > 0)
					sTSS_Finalize="1";
				
				sCGInfoWindowShow="0";
				sCGInfoMessage="";
				
				if(sTSS_Finalize.equalsIgnoreCase("1") || sMasterNoQ_CopiedAndFinalize.equals("1") || sAlreadyFinalizeQuestion.equals("1") )
				{
					sCGInfoWindowShow="1";
					if(sMasterNoQ_CopiedAndFinalize.equalsIgnoreCase("1"))
					{
						sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired18+"<BR>";
						System.out.println("sMasterNoQ_CopiedAndFinalize "+sMasterNoQ_CopiedAndFinalize);
					}
					else if(sTSS_Finalize.equalsIgnoreCase("1") || sAlreadyFinalizeQuestion.equalsIgnoreCase("1"))
					{
						sCGInfoMessage=sCGInfoMessage+"&#149; "+lblCandidatealreadyhired19+"<BR>";
						System.out.println("sTSS_Finalize "+sTSS_Finalize);
					}
					
					
					/*if(sAlreadyFinalizeQuestion.equalsIgnoreCase("1"))
					{
						sCGInfoMessage=sCGInfoMessage+"&#149; You have already scored one or more selected candidate for this status. Please click on button 'Override' if you want to ignore the previous score or click on button 'Skip' if you want to keep the and keep the previous score for such Candidate(s).<BR>";
						System.out.println("sAlreadyFinalizeQuestion "+sAlreadyFinalizeQuestion);
					}*/
				}
				
				if(sCGInfoWindowShow.equalsIgnoreCase("1") && !sCGInfoMessage.equalsIgnoreCase(""))
				{
					String sReturnArray[]=new String[2];
					sReturnArray[0]=sCGInfoMessage;// message info
					sReturnArray[1]=sCGInfoWindowShow; // confirmation DIV (1 - Need to Conform , 0 - OK)
					return sReturnArray;
				}
			}
			
			System.out.println(" *******************************************************************");
			System.out.println(" ****************** Start Processing for after user confirmation *****************");
			System.out.println(" *******************************************************************");
			////////////////////////////
			
			List<TeacherDetail> lstTeacherDetails_Skip_TSS=new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstTeacherDetails_Skip_TSN=new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstTeacherDetails_NotAllSetDPoint=new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstTeacherDetails_NotSetAllBeforeOfferReady=new ArrayList<TeacherDetail>();
			List<TeacherDetail> lstTeacherDetails_AlreadyOffer=new ArrayList<TeacherDetail>();
			
			if(bFinalize)
			{
				if(OverrideOrSkip==2)// for Skip (2) 
				{
					lstTeacherDetails_Skip_TSS=getFinalizeTSSTeacherList(lstTSS_V);
					lstTeacherDetails_Skip_SSS=getTeacherListFromSSS(lstStatusSpecificScore_First_Finalize);
				}
				
				lstTeacherDetails_NotAllSetDPoint=NotAllSetDPointTeacherList(jobOrder, statusMaster, lstTeacherDetails);
				
				if(sDisplayStatusName.equalsIgnoreCase("Offer Ready"))
				{
					lstTeacherDetails_NotSetAllBeforeOfferReady=NotSetAllBeforeOfferReadyTeacherList(jobOrder, lstTeacherDetails);
					List<JobForTeacher> jftOffer=jobForTeacherDAO.OfferCandidateList(lstTeacherDetails);
					lstTeacherDetails_AlreadyOffer=getAlreadyOfferTeacherList(jftOffer);
				}
				
				boolean isSkipAdded=false;
				List<TeacherDetail> skipedTeacherList=new ArrayList<TeacherDetail>();
				if(lstTeacherDetails_Skip_SSS!=null && lstTeacherDetails_Skip_SSS.size()>0)
				{
					skipedTeacherList.addAll(lstTeacherDetails_Skip_SSS);
					isSkipAdded=true;
				}
				
				/*if(isSkipAdded)
				{
					skipedTeacherList.retainAll(lstTeacherDetails_Skip_TSN);
				}
				else
				{
					skipedTeacherList.addAll(lstTeacherDetails_Skip_TSN);
				}*/
				if(lstTeacherDetails_NotAllSetDPoint!=null && lstTeacherDetails_NotAllSetDPoint.size() >0)
				{
					if(isSkipAdded)
					{
						skipedTeacherList.retainAll(lstTeacherDetails_NotAllSetDPoint);
					}
					else
					{
						skipedTeacherList.addAll(lstTeacherDetails_NotAllSetDPoint);
						isSkipAdded=true;
					}
				}
				
				
				if(lstTeacherDetails_NotSetAllBeforeOfferReady!=null && lstTeacherDetails_NotSetAllBeforeOfferReady.size() >0)
				{
					if(isSkipAdded)
					{
						skipedTeacherList.retainAll(lstTeacherDetails_NotSetAllBeforeOfferReady);
					}
					else
					{
						skipedTeacherList.addAll(lstTeacherDetails_NotSetAllBeforeOfferReady);
						isSkipAdded=true;
					}
				}
				
				
				if(lstTeacherDetails_AlreadyOffer!=null && lstTeacherDetails_AlreadyOffer.size() >0)
				{
					if(isSkipAdded)
					{
						skipedTeacherList.retainAll(lstTeacherDetails_AlreadyOffer);
					}
					else
					{
						skipedTeacherList.addAll(lstTeacherDetails_AlreadyOffer);
						isSkipAdded=true;
					}
				}
				
				
				if(lstTeacherDetails_Skip_TSS!=null && lstTeacherDetails_Skip_TSS.size() >0)
				{
					if(isSkipAdded)
					{
						skipedTeacherList.retainAll(lstTeacherDetails_Skip_TSS);
					}
					else
					{
						skipedTeacherList.addAll(lstTeacherDetails_Skip_TSS);
						isSkipAdded=true;
					}
				}
				
				boolean bSkip=false;
				if(OverrideOrSkip==2)
					bSkip=true;
				boolean bOverride=false;
				if(OverrideOrSkip==1)
					bOverride=true;
				boolean bNormal=false;
				if(OverrideOrSkip==0)
					bNormal=true;
				
				List<JobForTeacher> jobForTeachers=new ArrayList<JobForTeacher>();
				jobForTeachers=jobForTeacherDAO.getJFTByTIDsAndJID(lstTeacherDetails, jobOrder);
				Map<Integer, JobForTeacher> mapJFT=new HashMap<Integer, JobForTeacher>();
				mapJFT=getJFTMapCGMass(jobForTeachers);
				
				List<TeacherStatusScores> lstTSS_Master=new ArrayList<TeacherStatusScores>();
				Map<Integer, List<TeacherStatusScores>> mapLstTSSMaster=new HashMap<Integer, List<TeacherStatusScores>>();
				lstTSS_Master= teacherStatusScoresDAO.getStatusScore_CGMass(lstTeacherDetails,jobOrder,statusMaster,secondaryStatus,userMaster);
				mapLstTSSMaster=getTSSListMap(lstTSS_Master);
				
				List <TeacherStatusHistoryForJob> lstTeacherStatusHistoryForJobMaster =new ArrayList<TeacherStatusHistoryForJob>();
				lstTeacherStatusHistoryForJobMaster=teacherStatusHistoryForJobDAO.findByTeacherStatus_CGMass(lstTeacherDetails,jobOrder,statusMaster,secondaryStatus);
				Map<Integer, List <TeacherStatusHistoryForJob>> mapListTeacherStatusHistoryForJobMaster=new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
				mapListTeacherStatusHistoryForJobMaster=getMapForTSHForJob(lstTeacherStatusHistoryForJobMaster);
				
				List<DistrictKeyContact> districtKeyContactList = districtKeyContactDAO.findOfferAccepted(districtMaster);
				
				Map<Integer, List<JobWiseConsolidatedTeacherScore>> mapLstJWCTSMaster=new HashMap<Integer, List<JobWiseConsolidatedTeacherScore>>();
				List<JobWiseConsolidatedTeacherScore> lstJWCTSMaster= new ArrayList<JobWiseConsolidatedTeacherScore>();
				lstJWCTSMaster=jobWiseConsolidatedTeacherScoreDAO.getJWCTScore_CGMass(lstTeacherDetails, jobOrder);
				mapLstJWCTSMaster=getJWCTSMap(lstJWCTSMaster);
				
				Map<Integer, TeacherPersonalInfo> mapTeacherPersonalInfo=new HashMap<Integer, TeacherPersonalInfo>();
				List<TeacherPersonalInfo> teacherPersonalInfoList  = new ArrayList<TeacherPersonalInfo>();
				teacherPersonalInfoList=teacherPersonalInfoDAO.findTeacherPInfoForTeacher(lstTeacherDetails);
				mapTeacherPersonalInfo=getTeacherPersonalInfoCGMass(teacherPersonalInfoList);
				
				List<TeacherDetail> lstHWRDTeacher=new ArrayList<TeacherDetail>();
				lstHWRDTeacher=getHWRDTeacherList(jobForTeachers);
				if(lstHWRDTeacher!=null && lstHWRDTeacher.size() >0)
				{
					if(isSkipAdded)
					{
						skipedTeacherList.retainAll(lstHWRDTeacher);
					}
					else
					{
						skipedTeacherList.addAll(lstHWRDTeacher);
						isSkipAdded=true;
					}
				}
				
				Map<Integer, Boolean> mapSkipTeachers=new HashMap<Integer, Boolean>();
				if(skipedTeacherList!=null && skipedTeacherList.size() > 0)
				{
					for(TeacherDetail detail:skipedTeacherList)
					{
						if(detail!=null && mapSkipTeachers.get(detail.getTeacherId())==null)
						{
							mapSkipTeachers.put(detail.getTeacherId(), true);
						}
					}
				}
				
				boolean bIsTeacherStatusNotesList=false;
				List<TeacherStatusNotes> lstTeacherStatusNotes_First=new ArrayList<TeacherStatusNotes>();
				lstTeacherStatusNotes_First=teacherStatusNotesDAO.getTeacheStatusNotesList_CGMass(lstTeacherDetails, jobOrder, statusMaster, secondaryStatus, userMaster);
				if(lstTeacherStatusNotes_First!=null && lstTeacherStatusNotes_First.size() > 0)
					bIsTeacherStatusNotesList=true;
				
				Map<Integer, List<TeacherStatusNotes>> mapTeacherStatusNotesList=new HashMap<Integer, List<TeacherStatusNotes>>();
				if(bIsTeacherStatusNotesList)
					mapTeacherStatusNotesList=getTSNListMap(lstTeacherStatusNotes_First);
				
				
				PrintOnConsole.debugPrintlnCGMass("Start processing ... Total Teacher Count "+lstTeacherDetails.size());
				int iTeacherCount=0;
				int iSkipTeacherCount=0;
				int iProcessTeacherCount=0;
				String sTeacherEmail="";
				SessionFactory factory_tss=teacherStatusScoresDAO.getSessionFactory();
				StatelessSession statelessSession_tss=factory_tss.openStatelessSession();
				Transaction transaction_tss=statelessSession_tss.beginTransaction();
				
				//Delete Question
				System.out.println("bIsMasterQuestionList "+bIsMasterQuestionList +" bIsFinalizeSSSQList "+bIsFinalizeSSSQList +" bIsStatusSpecificScoreQuestionList "+bIsStatusSpecificScoreQuestionList);
				if(!bIsMasterQuestionList && !bIsFinalizeSSSQList && bIsStatusSpecificScoreQuestionList)
				{
					System.out.println("1");
					for(StatusSpecificScore specificScoreTemp:lstStatusSpecificScore_First)
					 {
						 if(specificScoreTemp!=null)
						 {
							 transaction_tss=statelessSession_tss.beginTransaction();
							 statelessSession_tss.delete(specificScoreTemp);
							 transaction_tss.commit(); 
						 }
					 }
				}
				
				Map<Integer, Boolean> mapUpdateFitScore=new HashMap<Integer, Boolean>();
				if(OverrideOrSkip!=2)// Override or Normal 
				{
					if(!bIsMasterQuestionList && bIsFinalizeSSSQList && bIsStatusSpecificScoreQuestionList)
					{
						System.out.println("2");
						for(StatusSpecificScore specificScoreTemp:lstStatusSpecificScore_First)
						 {
							 if(specificScoreTemp!=null)
							 {
								 transaction_tss=statelessSession_tss.beginTransaction();
								 statelessSession_tss.delete(specificScoreTemp);
								 transaction_tss.commit(); 
								 
								 //delete from TSS and TSN
								 try {
								 	if(lstTSS_Master!=null && lstTSS_Master.size() > 0)
								 	{
								 		for(TeacherStatusScores scores:lstTSS_Master)
								 		{
								 			transaction_tss=statelessSession_tss.beginTransaction();
											 statelessSession_tss.delete(scores);
											 transaction_tss.commit(); 
											 if(mapUpdateFitScore.get(scores.getTeacherDetail().getTeacherId())==null)
												 mapUpdateFitScore.put(scores.getTeacherDetail().getTeacherId(), true);
								 		}
								 	}
								 	
								 	if(lstTeacherStatusNotes_First!=null && lstTeacherStatusNotes_First.size() > 0)
								 	{
								 		for(TeacherStatusNotes notes:lstTeacherStatusNotes_First)
								 		{
								 			transaction_tss=statelessSession_tss.beginTransaction();
											 statelessSession_tss.delete(notes);
											 transaction_tss.commit(); 
								 		}
								 	}
								 	
								 } catch (Exception e) {}
								 
							 }
						 }
					}
				}
				
				//**************************************************
				
				lstStatusSpecificScore_First=statusSpecificScoreDAO.getQuestionList_CGMass(jobOrder, statusMaster, secondaryStatus, lstTeacherDetails, userMaster);
				if(lstStatusSpecificScore_First!=null && lstStatusSpecificScore_First.size() > 0)
					bIsStatusSpecificScoreQuestionList=true;
				mapStatusSpecificScoreList=getSSSListMap(lstStatusSpecificScore_First);
				
				lstTSS_Master= teacherStatusScoresDAO.getStatusScore_CGMass(lstTeacherDetails,jobOrder,statusMaster,secondaryStatus,userMaster);
				mapLstTSSMaster=getTSSListMap(lstTSS_Master);
				
				lstTeacherStatusNotes_First=teacherStatusNotesDAO.getTeacheStatusNotesList_CGMass(lstTeacherDetails, jobOrder, statusMaster, secondaryStatus, userMaster);
				if(lstTeacherStatusNotes_First!=null && lstTeacherStatusNotes_First.size() > 0)
				{
					bIsTeacherStatusNotesList=true;
					mapTeacherStatusNotesList=getTSNListMap(lstTeacherStatusNotes_First);
				}
				//sss
				
				//**************************************************
				
				
				//Validate Question and Copy
				for(TeacherDetail teacherDetail:lstTeacherDetails)
				{
					if(teacherDetail!=null)
					{
						if(mapSkipTeachers!=null && mapSkipTeachers.get(teacherDetail.getTeacherId())!=null)
						{
						}
						else
						{
							List<StatusSpecificScore> scoreslst=new ArrayList<StatusSpecificScore>();
							if(mapStatusSpecificScoreList.get(teacherDetail.getTeacherId())!=null)
							{
								Map<Integer, Boolean> mapSSSProcessTemp=new HashMap<Integer, Boolean>();
								Map<Integer, StatusSpecificScore> mapSSSProcessDetailsTemp=new HashMap<Integer, StatusSpecificScore>();
								
								scoreslst=mapStatusSpecificScoreList.get(teacherDetail.getTeacherId());
								if(scoreslst!=null && scoreslst.size() > 0)
								{
									mapSSSProcessTemp=getMapSSS(scoreslst);
									mapSSSProcessDetailsTemp=getMapSSSDetails(scoreslst);
								}
								
								System.out.println("****** Question Master "+iMasterQuestionCount+" ***********");
								if(iMasterQuestionCount > 0)
								{
									for(StatusSpecificQuestions masterQuestions:lstStatusSpecificQuestions)
									{
										if(masterQuestions!=null)
										{
											if(mapSSSProcessTemp.get(masterQuestions.getQuestionId())!=null)
											{
												mapSSSProcessTemp.put(masterQuestions.getQuestionId(), true);
												
												StatusSpecificScore scoreTemp=new StatusSpecificScore();
												scoreTemp=mapSSSProcessDetailsTemp.get(masterQuestions.getQuestionId());
												if(scoreTemp!=null)
												{
													if(!masterQuestions.getMaxScore().equals(scoreTemp.getMaxScore()) || ( masterQuestions.getSkillAttributesMaster()!=null && scoreTemp.getSkillAttributesMaster()==null ) || (masterQuestions.getSkillAttributesMaster()!=null && scoreTemp.getSkillAttributesMaster()!=null && !masterQuestions.getSkillAttributesMaster().equals(scoreTemp.getSkillAttributesMaster())) )
													{
														System.out.println("No Match "+ masterQuestions.getMaxScore()+" == "+scoreTemp.getMaxScore());
														// Need to Update MaxScore
														scoreTemp.setMaxScore(masterQuestions.getMaxScore());
														
														if(masterQuestions.getSkillAttributesMaster()!=null){
															scoreTemp.setSkillAttributesMaster(masterQuestions.getSkillAttributesMaster());
															scoreTemp.setScoringCaption(masterQuestions.getSkillAttributesMaster().getSkillName());
														}
														
														scoreTemp.setQuestion(masterQuestions.getQuestion());
														
														transaction_tss=statelessSession_tss.beginTransaction();
														statelessSession_tss.update(scoreTemp);
														transaction_tss.commit();
													}
												}
											}
											else
											{
												// Copy Question into tbl StatusSpecificScore
												StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
												statusSpecificScore.setDistrictId(masterQuestions.getDistrictId());
												
												statusSpecificScore.setJobCategoryId(masterQuestions.getJobCategoryId());
												statusSpecificScore.setJobOrder(jobOrder);
												
												if(masterQuestions.getStatusId()!=null)
													statusSpecificScore.setStatusId(masterQuestions.getStatusId());
												if(masterQuestions.getSecondaryStatusId()!=null)
													statusSpecificScore.setSecondaryStatusId(masterQuestions.getSecondaryStatusId());
												
												statusSpecificScore.setStatusSpecificQuestions(masterQuestions);
												statusSpecificScore.setQuestion(masterQuestions.getQuestion());
												if(masterQuestions.getSkillAttributesMaster()!=null){
													statusSpecificScore.setSkillAttributesMaster(masterQuestions.getSkillAttributesMaster());
													statusSpecificScore.setScoringCaption(masterQuestions.getSkillAttributesMaster().getSkillName());
												}
												statusSpecificScore.setScoreProvided(0);
												statusSpecificScore.setMaxScore(masterQuestions.getMaxScore());
												statusSpecificScore.setUserMaster(userMaster);
												statusSpecificScore.setCreatedDateTime(new Date());
												
												statusSpecificScore.setTeacherDetail(teacherDetail);
												statusSpecificScore.setFinalizeStatus(0);
												
												if(userMaster.getSchoolId()!=null)
													statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());
												
												transaction_tss=statelessSession_tss.beginTransaction();
												statelessSession_tss.insert(statusSpecificScore);
												transaction_tss.commit();
											}
										}
									}
									
									
									//delete extra Question
									Iterator iter = mapSSSProcessTemp.entrySet().iterator();
									while (iter.hasNext()) 
									{
										Map.Entry mEntry = (Map.Entry) iter.next();
										if(mEntry.getValue().equals(new Boolean(false)))
										{
											Integer iQuestionId=(Integer)mEntry.getKey();
											StatusSpecificScore scoreTempDelete=mapSSSProcessDetailsTemp.get(iQuestionId);
											transaction_tss=statelessSession_tss.beginTransaction();
											
											System.out.println("Try to delete getAnswerId "+scoreTempDelete.getAnswerId());
											statelessSession_tss.delete(scoreTempDelete);
											transaction_tss.commit();
										}
											
									}
									
								}
								else
								{
									System.out.println("****** No Question Master ***********");
								}
								
							}
							else
							{
								System.out.println("****** Need to copy all Question for TID "+teacherDetail.getTeacherId()+" *********** iMasterQuestionCount "+iMasterQuestionCount);
								
								if(iMasterQuestionCount > 0)
								{
									for(StatusSpecificQuestions masterQuestions:lstStatusSpecificQuestions)
									{
										if(masterQuestions!=null)
										{
												// Copy Question into tbl StatusSpecificScore
												StatusSpecificScore statusSpecificScore=new StatusSpecificScore();
												statusSpecificScore.setDistrictId(masterQuestions.getDistrictId());
												
												statusSpecificScore.setJobCategoryId(masterQuestions.getJobCategoryId());
												
												statusSpecificScore.setJobOrder(jobOrder);
												
												if(masterQuestions.getStatusId()!=null)
													statusSpecificScore.setStatusId(masterQuestions.getStatusId());
												if(masterQuestions.getSecondaryStatusId()!=null)
													statusSpecificScore.setSecondaryStatusId(masterQuestions.getSecondaryStatusId());
												
												statusSpecificScore.setStatusSpecificQuestions(masterQuestions);
												statusSpecificScore.setQuestion(masterQuestions.getQuestion());
												if(masterQuestions.getSkillAttributesMaster()!=null){
													statusSpecificScore.setSkillAttributesMaster(masterQuestions.getSkillAttributesMaster());
													statusSpecificScore.setScoringCaption(masterQuestions.getSkillAttributesMaster().getSkillName());
												}
												statusSpecificScore.setScoreProvided(0);
												statusSpecificScore.setMaxScore(masterQuestions.getMaxScore());
												statusSpecificScore.setUserMaster(userMaster);
												statusSpecificScore.setCreatedDateTime(new Date());
												
												statusSpecificScore.setTeacherDetail(teacherDetail);
												statusSpecificScore.setFinalizeStatus(0);
												
												if(userMaster.getSchoolId()!=null)
													statusSpecificScore.setSchoolId(userMaster.getSchoolId().getSchoolId());
												
												transaction_tss=statelessSession_tss.beginTransaction();
												statelessSession_tss.insert(statusSpecificScore);
												transaction_tss.commit();
										}
									}
								}
								
								
							}
							
						}
					}
				}
				
				
				//Get Final Question
				boolean bIsSSSList_ForProcess=false;
				List<StatusSpecificScore> lstStatusSpecificScore_Process=new ArrayList<StatusSpecificScore>();
				lstStatusSpecificScore_Process=statusSpecificScoreDAO.getQuestionList_CGMass(jobOrder, statusMaster, secondaryStatus, lstTeacherDetails, userMaster);
				if(lstStatusSpecificScore_Process!=null && lstStatusSpecificScore_Process.size() > 0)
					bIsSSSList_ForProcess=true;
				
				Map<Integer, List<StatusSpecificScore>> mapSSSListForProcess=new HashMap<Integer, List<StatusSpecificScore>>();
				mapSSSListForProcess=getSSSListMap(lstStatusSpecificScore_Process);
				
				System.out.println("lstStatusSpecificScore_Process "+lstStatusSpecificScore_Process.size());
				
				
				boolean bAnyQuestionZeroValue=false;
				Map<Integer, Integer> mapQV=new HashMap<Integer, Integer>();
				Double iTotalQuestionValue=0.0;
				if(questionId_arrary!=null && questionId_arrary.length > 0)
				{
					for(int i=0;i < questionId_arrary.length;i++)
					{
						if(mapQV.get(questionId_arrary[i])==null)
						{
							int iTemp=0;
							try {
								iTemp=Integer.parseInt(scoreValue_arrary[i]);
							} catch (Exception e) {
								// TODO: handle exception
							}
							iTotalQuestionValue=iTotalQuestionValue+iTemp;
							mapQV.put(questionId_arrary[i], iTemp);
							
							if(iTemp==0)
								bAnyQuestionZeroValue=true;
							
							System.out.println(questionId_arrary[i]+" :: "+iTemp);
						}
					}
				}
				
				Map<Integer, String> mapQNote=new HashMap<Integer, String>();
				if(QuestionNoteIds_arrary!=null && QuestionNoteIds_arrary.length > 0)
				{
					for(int i=0;i < QuestionNoteIds_arrary.length;i++)
					{
						if(mapQNote.get(QuestionNoteIds_arrary[i])==null)
						{
							String sTempQuestionNote="";
							try {
								sTempQuestionNote=sQuestionIds_Note_arrary[i];
							} catch (Exception e) {
								// TODO: handle exception
							}
							mapQNote.put(QuestionNoteIds_arrary[i], sTempQuestionNote);
							System.out.println(QuestionNoteIds_arrary[i]+" :: "+sTempQuestionNote);
						}
					}
				}
				

				List<RaceMaster> lstRace= null;
				lstRace = raceMasterDAO.findAllRaceByOrder();
				Map<String,RaceMaster> raceMap = new HashMap<String, RaceMaster>();
				for (RaceMaster raceMaster : lstRace) {
					raceMap.put(""+raceMaster.getRaceId(), raceMaster);
				}
				/////////////////Add Zone Wise Rejected By Sekhar/////////////////////
				List<Long> jobForTeacherIdList= new ArrayList<Long>();
				try{
					if(jobForTeachers.size()>0){
						for (JobForTeacher jobForTeacher : jobForTeachers) {
							jobForTeacherIdList.add(jobForTeacher.getJobForTeacherId());
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				String jobTitle=jobOrder.getJobTitle();
				try{
					if(jobTitle.indexOf("(")>0){
						jobTitle=jobTitle.substring(0,jobTitle.lastIndexOf("(")).trim();
						System.out.println("jobTitle:::::::::"+jobTitle);
					}
					System.out.println("jobTitle::::::"+jobTitle);
				}catch(Exception e){
					e.printStackTrace();
				}
				List<JobForTeacher> jobForTeacherList= jobForTeacherDAO.findJobByTeacherListAndJobTitle(lstTeacherDetails,districtMaster,jobCategoryMaster,jobForTeacherIdList,jobTitle);
				System.out.println("jobForTeacherList:::::::>>>:::::"+jobForTeacherList.size());
				
				List<JobOrder> joblist=new ArrayList<JobOrder>();
				List<TeacherDetail> teacherList =new ArrayList<TeacherDetail>();
				System.out.println("jobForTeacherList:::><><::"+jobForTeacherList.size());
				try{
					for (JobForTeacher jobForTeacher : jobForTeacherList) {
						joblist.add(jobForTeacher.getJobId());
						teacherList.add(jobForTeacher.getTeacherId());
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				List<TeacherStatusHistoryForJob> forJobs=teacherStatusHistoryForJobDAO.findByTeachersAndJobsStatus(teacherList, joblist);
				Map<String,TeacherStatusHistoryForJob> mapTSHFJ=new HashMap<String, TeacherStatusHistoryForJob>();
				try{
					if(forJobs!=null && forJobs.size() > 0)
					{
						for(TeacherStatusHistoryForJob tSHObj : forJobs){
							mapTSHFJ.put(tSHObj.getTeacherDetail().getTeacherId()+"#"+tSHObj.getJobOrder().getJobId(),tSHObj);
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				/////////////////End Zone Wise Rejected By Sekhar/////////////////////
				
				//Status process
				List<TeacherDetail> teacherDetailLst=new ArrayList<TeacherDetail>();//added
				for(TeacherDetail teacherDetail:lstTeacherDetails)
				{
					if(teacherDetail!=null)
					{
						sTeacherEmail=teacherDetail.getEmailAddress();
						boolean bUpdateFitScore=false;
						iTeacherCount++;
						PrintOnConsole.debugPrintlnCGMass("**************************************************************");
						PrintOnConsole.debugPrintlnCGMass(iTeacherCount+" Start Processing for TID "+teacherDetail.getTeacherId() +" :: "+teacherDetail.getEmailAddress() +" :: "+teacherDetail.getFirstName());
						
						System.out.println(" >>>>>>>>>>>>>>>>> teacherDetail >>>>>>>>>>>>>>>>> "+teacherDetail);
						if(mapSkipTeachers!=null && mapSkipTeachers.get(teacherDetail.getTeacherId())!=null)
						{
							System.out.println("01");
							iSkipTeacherCount++;
							PrintOnConsole.debugPrintlnCGMass("Ignore/Skip "+iSkipTeacherCount);
						}
						else
						{
							System.out.println("02");
							teacherDetailLst.add(teacherDetail);//added
							iProcessTeacherCount++;
							PrintOnConsole.debugPrintlnCGMass("Ready to process "+iProcessTeacherCount); 													
							// Start process here ....
							
							// Teacher Status Note process
							Map<Long, TeacherStatusNotes> mapTempTSN=new HashMap<Long, TeacherStatusNotes>();
							if(mapTeacherStatusNotesList!=null && mapTeacherStatusNotesList.get(teacherDetail.getTeacherId())!=null)
							{
								List<TeacherStatusNotes> lstTempTSN=new ArrayList<TeacherStatusNotes>();
								lstTempTSN=mapTeacherStatusNotesList.get(teacherDetail.getTeacherId());
								if(lstTempTSN!=null && lstTempTSN.size() > 0)
									mapTempTSN=getMapTSN(lstTempTSN);
							}
							
							Double iTotalQuestionMaxScore=0.0;
							List<StatusSpecificScore> scoresLst_P=new ArrayList<StatusSpecificScore>();
							if(mapSSSListForProcess.get(teacherDetail.getTeacherId())!=null)
							{
								scoresLst_P=mapSSSListForProcess.get(teacherDetail.getTeacherId());
								if(scoresLst_P!=null)
								{
									for(StatusSpecificScore statusSpecificScore:scoresLst_P)
									{
										if(statusSpecificScore!=null)
										{
											int iQuestionUserValue=0;
											if(statusSpecificScore.getStatusSpecificQuestions()!=null && mapQV.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId())!=null)
											{
												iQuestionUserValue=mapQV.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId());
											}
											if(iQuestionUserValue > 0 && iTotalQuestionValue > 0 && !bAnyQuestionZeroValue)
											{
												statusSpecificScore.setScoreProvided(iQuestionUserValue);
												statusSpecificScore.setFinalizeStatus(1);
												
												transaction_tss=statelessSession_tss.beginTransaction();
												statelessSession_tss.update(statusSpecificScore);
												transaction_tss.commit();
												bUpdateFitScore=true;
												
												iTotalQuestionMaxScore=iTotalQuestionMaxScore+statusSpecificScore.getMaxScore();
											}
										
										//	Teacher Status Note :: Add / Update
											if(mapTempTSN!=null && mapTempTSN.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId())!=null)
											{
												TeacherStatusNotes notes=mapTempTSN.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId());
												
												String sQuestionUserNoteValue="";
												if(statusSpecificScore.getStatusSpecificQuestions()!=null && mapQNote.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId())!=null)
												{
													sQuestionUserNoteValue=mapQNote.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId());
												}
												
												if(notes!=null && sQuestionUserNoteValue.trim().length() > 0)
												{
													notes.setStatusNotes(sQuestionUserNoteValue);
													notes.setFinalizeStatus(new Boolean(true));
													
													transaction_tss=statelessSession_tss.beginTransaction();
													statelessSession_tss.update(notes);
													transaction_tss.commit();
												}
											}
											else
											{
												// Need to add new
												String sQuestionUserNoteValue="";
												if(statusSpecificScore.getStatusSpecificQuestions()!=null && mapQNote.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId())!=null)
												{
													sQuestionUserNoteValue=mapQNote.get(statusSpecificScore.getStatusSpecificQuestions().getQuestionId());
												}
												
												if(sQuestionUserNoteValue.trim().length() > 0)
												{
													TeacherStatusNotes tsnObj = new TeacherStatusNotes();
													tsnObj.setTeacherDetail(teacherDetail);
													tsnObj.setUserMaster(userMaster);
													tsnObj.setJobOrder(jobOrder);
													tsnObj.setDistrictId(districtMaster.getDistrictId());
													if(statusMaster!=null)
														tsnObj.setStatusMaster(statusMaster);
													if(secondaryStatus!=null)
														tsnObj.setSecondaryStatus(secondaryStatus);
													tsnObj.setStatusNotes(sQuestionUserNoteValue);
													tsnObj.setStatusNoteFileName(null);
													if(schoolMaster!=null)
														tsnObj.setSchoolId(schoolMaster.getSchoolId());
													tsnObj.setEmailSentTo(0);
													if(statusSpecificScore.getStatusSpecificQuestions()!=null)
														tsnObj.setTeacherAssessmentQuestionId(new Long(statusSpecificScore.getStatusSpecificQuestions().getQuestionId()));
													tsnObj.setFinalizeStatus(true);
													transaction_tss=statelessSession_tss.beginTransaction();
													statelessSession_tss.insert(tsnObj);
													transaction_tss.commit();
												}
												
											}
											
											
										}
									}
								}
							}
							
							boolean bQuestionSlider=false;
							if(iTotalQuestionValue > 0 && iTotalQuestionMaxScore > 0 && bUpdateFitScore)
							{
								bQuestionSlider=true;
								topScoreProvided=iTotalQuestionValue;
								iMaxFitScore=iTotalQuestionMaxScore;
							}
							System.out.println("iTotalQuestionValue "+iTotalQuestionValue +" of "+iTotalQuestionMaxScore);
							
							PrintOnConsole.debugPrintlnCGMass("Top Slider "+bTopSliderEnable+" :: topScoreProvided "+topScoreProvided +" :: iMaxFitScore "+iMaxFitScore);
							PrintOnConsole.debugPrintlnCGMass("Notes "+statusEditorNotes+" :: "+statusNoteFileName);
							List<TeacherStatusScores> lstTSS_P=new ArrayList<TeacherStatusScores>();
							if( ( bTopSliderEnable && topScoreProvided >0) || bQuestionSlider )
							{
								try {
									if(mapLstTSSMaster!=null && mapLstTSSMaster.get(teacherDetail.getTeacherId())!=null)
										lstTSS_P=mapLstTSSMaster.get(teacherDetail.getTeacherId());
									
								} catch (Exception e) {}
								
								TeacherStatusScores statusScores=null;
								if(lstTSS_P==null || lstTSS_P.size()==0)
								{
									//Add
									PrintOnConsole.debugPrintlnCGMass("Need To Add TeacherStatusScores");
									statusScores=new TeacherStatusScores();

									statusScores.setTeacherDetail(teacherDetail);
									statusScores.setUserMaster(userMaster);
									statusScores.setJobOrder(jobOrder);
									statusScores.setDistrictId(districtMaster.getDistrictId());
									if(statusMaster!=null)
										statusScores.setStatusMaster(statusMaster);
									else if(secondaryStatus!=null)
										statusScores.setSecondaryStatus(secondaryStatus);

									if(schoolMaster!=null)
										statusScores.setSchoolId(schoolMaster.getSchoolId());	
									statusScores.setScoreProvided(topScoreProvided);
									statusScores.setMaxScore(iMaxFitScore);
									statusScores.setFinalizeStatus(true);
									statusScores.setEmailSentTo(0);
									transaction_tss=statelessSession_tss.beginTransaction();
									statelessSession_tss.insert(statusScores);
									transaction_tss.commit();
									
									bUpdateFitScore=true;
								}
								else if(lstTSS_P!=null && lstTSS_P.size()==1)
								{
									//Update
									PrintOnConsole.debugPrintlnCGMass("Need To Update TeacherStatusScores");
									statusScores=lstTSS_P.get(0);
									if(statusScores!=null)
									{
										statusScores.setScoreProvided(topScoreProvided);
										statusScores.setMaxScore(iMaxFitScore);
										statusScores.setFinalizeStatus(true);
										statusScores.setEmailSentTo(0);
										transaction_tss=statelessSession_tss.beginTransaction();
										statelessSession_tss.update(statusScores);
										transaction_tss.commit();
										bUpdateFitScore=true;
									}
								}
							}
							
							
							if(statusEditorNotes!=null && statusEditorNotes.trim().length() > 0)
							{
								PrintOnConsole.debugPrintlnCGMass("Need To Add TeacherStatusNotes");
								TeacherStatusNotes tsnObj = new TeacherStatusNotes();
								tsnObj.setTeacherDetail(teacherDetail);
								tsnObj.setUserMaster(userMaster);
								tsnObj.setJobOrder(jobOrder);
								tsnObj.setDistrictId(districtMaster.getDistrictId());
								
								if(statusMaster!=null)
									tsnObj.setStatusMaster(statusMaster);
								
								if(secondaryStatus!=null)
									tsnObj.setSecondaryStatus(secondaryStatus);
								
								tsnObj.setStatusNotes(statusEditorNotes.trim());
								
								if(statusNoteFileName!=null && !statusNoteFileName.equals(""))
								{
									try {
										String sfileNameArray[]=statusNoteFileName.split("_");
										String teacherIdTemp=teacherDetail.getTeacherId()+"";
										String fileNameTemp="";
										String sSourceTeacherId="";
										if(sfileNameArray.length ==2)
										{
											sSourceTeacherId=sfileNameArray[0];
											fileNameTemp=sfileNameArray[1];
										}
										
										String filePath=Utility.getValueOfPropByKey("communicationRootPath")+"/statusNote/"+sSourceTeacherId+"/";
										String source =filePath+"/"+statusNoteFileName;
										String target=Utility.getValueOfPropByKey("communicationRootPath")+"/statusNote/"+teacherIdTemp+"/";
										
										File sourceFile = new File(source);
										File targetDir = new File(target);
										if(!targetDir.exists())
											targetDir.mkdirs();
										
										String sTargetFileName=teacherIdTemp+"_"+fileNameTemp;
										
										try {
											File targetFile = new File(targetDir+"/"+sTargetFileName);
											FileUtils.copyFile(sourceFile, targetFile);
										} catch (Exception e) {}
										tsnObj.setStatusNoteFileName(sTargetFileName);
									} catch (Exception e) {}
								}
								
								if(schoolMaster!=null)
									tsnObj.setSchoolId(schoolMaster.getSchoolId());
								
								tsnObj.setEmailSentTo(0);
								tsnObj.setTeacherAssessmentQuestionId(null);
								tsnObj.setFinalizeStatus(true);
								
								transaction_tss=statelessSession_tss.beginTransaction();
								statelessSession_tss.insert(tsnObj);
								transaction_tss.commit();
							}
							
							JobForTeacher jobForTeacher=mapJFT.get(teacherDetail.getTeacherId());
							
							boolean IsUpdateJFTStatus=false;
							boolean selectedstatus=false;
							boolean selectedSecondaryStatus=false;
							if(isJeffcoDistrict && startDate!=null && !startDate.trim().equalsIgnoreCase("")){
								jobForTeacher.setStartDate(new SimpleDateFormat("MM-dd-yyyy").parse(startDate));
							}
							
							try{
								if(jobForTeacher.getStatus()!=null && statusMaster!=null){
									selectedstatus=cgService.selectedStatusCheck(statusMaster,jobForTeacher.getStatus());
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							try{
								if(secondaryStatus!=null && jobForTeacher.getStatus()!=null &&  statusMaster!=null){// && (jobForTeacherObj.getStatus().getStatusShortName().equals("scomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("ecomp") || jobForTeacherObj.getStatus().getStatusShortName().equals("vcomp"))
									if(jobForTeacher.getSecondaryStatus()!=null){
										selectedSecondaryStatus=cgService.selectedPriSecondaryStatusCheck(secondaryStatus,jobForTeacher.getSecondaryStatus());
										if(selectedSecondaryStatus==false){
											selectedstatus=false;
										}
									}
								}else if(jobForTeacher.getSecondaryStatus()!=null && secondaryStatus!=null && statusMaster==null){
									if(jobForTeacher.getStatus()!=null){
										if(cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),secondaryStatus)){
											selectedSecondaryStatus=cgService.selectedNotPriSecondaryStatusCheck(secondaryStatus,jobForTeacher.getSecondaryStatus());
										}
									}
								}else if(jobForTeacher.getSecondaryStatus()==null && secondaryStatus!=null && statusMaster==null){
									if(jobForTeacher.getStatus()!=null){
										if(cgService.priSecondaryStatusCheck(jobForTeacher.getStatus(),secondaryStatus)){
											selectedSecondaryStatus=true;
										}
									}
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							
							if(selectedstatus || selectedSecondaryStatus)
								IsUpdateJFTStatus=true;
							
							boolean bSentEmailToCandidate=false;
							boolean bSentEmailForOfferReady=false;
							if(jobForTeacher!=null)
							{
								PrintOnConsole.debugPrintlnCGMass("jobForTeacher Id "+jobForTeacher.getJobForTeacherId());
								if(IsUpdateJFTStatus)
								{
									// Need to Update jobForTeacher
									PrintOnConsole.debugPrintlnCGMass("Need to Update jobForTeacher");
									if(statusMaster!=null)
										jobForTeacher.setStatus(statusMaster);
									if(statusMaster!=null)
									{
										jobForTeacher.setStatusMaster(statusMaster);
										jobForTeacher.setLastActivity(statusMaster.getStatus());
									}
									else
									{
										jobForTeacher.setStatusMaster(null);
										jobForTeacher.setSecondaryStatus(secondaryStatus);
										jobForTeacher.setLastActivity(secondaryStatus.getSecondaryStatusName());
										
										if(isMiami)
										{
											if(secondaryStatus!=null && secondaryStatus.getSecondaryStatusName().equalsIgnoreCase("Ofcl Trans / Veteran Pref"))
											{
												try{ 
														sendEmailOfficialTranscripts_CGMass(teacherDetail, sDistrictName);
												} 
												catch (Exception e) 
												{}
											}
										}
									}
									
									jobForTeacher.setUpdatedBy(userMaster);
									jobForTeacher.setUpdatedByEntity(userMaster.getEntityType());		
									jobForTeacher.setUpdatedDate(new Date());
									jobForTeacher.setLastActivityDate(new Date());
									jobForTeacher.setUserMaster(userMaster);
									
									if(requisitionNumberText!=null && !requisitionNumberText.equalsIgnoreCase(""))
										jobForTeacher.setRequisitionNumber(requisitionNumberText);
									
									if(bOfferReady && isSubstituteInstructionalJob)
									{
										jobForTeacher.setOfferReady(true);
										jobForTeacher.setOfferMadeDate(new Date());
										bSentEmailForOfferReady=true;
									}
									
									transaction_tss=statelessSession_tss.beginTransaction();
									statelessSession_tss.update(jobForTeacher);
									transaction_tss.commit();
									/////////////////Add Zone Wise Rejected By Sekhar/////////////////////
									try{
										if(isMiami && jobForTeacherList.size()>0 && statusMaster!=null && statusMaster.getStatusShortName().equals("rem")){
											System.out.println(":::::::::::::::::::Mass Rejected::::::::::::::");
											TeacherStatusNotes noteObj=teacherStatusNotesDAO.getFinalizeNotesByOrder(teacherDetail, jobOrder, statusMaster, secondaryStatus, userMaster);
											for (JobForTeacher jobForObj : jobForTeacherList){
												TeacherStatusHistoryForJob teacherStatusHistoryForJob=null;
												String sTID_JID=jobForObj.getTeacherId().getTeacherId()+"#"+jobForObj.getJobId().getJobId();
												try{
													if(mapTSHFJ.get(sTID_JID)!=null){
														teacherStatusHistoryForJob=mapTSHFJ.get(sTID_JID);
													}
													if(teacherStatusHistoryForJob!=null)
													{
														teacherStatusHistoryForJob.setStatus("I");
														teacherStatusHistoryForJob.setUpdatedDateTime(new Date());
														teacherStatusHistoryForJob.setUserMaster(userMaster);
														transaction_tss=statelessSession_tss.beginTransaction();
														statelessSession_tss.update(teacherStatusHistoryForJob);
														transaction_tss.commit();
													}
												}catch(Exception e){
													e.printStackTrace();
												}
												try{
														TeacherStatusHistoryForJob forJob=new TeacherStatusHistoryForJob();
														forJob.setTeacherDetail(jobForObj.getTeacherId());
														forJob.setJobOrder(jobForObj.getJobId());
														forJob.setStatusMaster(statusMaster);
														forJob.setStatus("A");
														forJob.setUserMaster(userMaster);
														forJob.setCreatedDateTime(new Date());
														//teacherStatusHistoryForJobDAO.makePersistent(forJob);
														transaction_tss=statelessSession_tss.beginTransaction();
														statelessSession_tss.insert(forJob);
														transaction_tss.commit();
												}catch(Exception e){
													e.printStackTrace();
												}	
												if(noteObj!=null){
													try{
														TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
														teacherStatusNoteObj.setTeacherDetail(noteObj.getTeacherDetail());
														teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
														if(noteObj.getStatusMaster()!=null)
														teacherStatusNoteObj.setStatusMaster(noteObj.getStatusMaster());
														if(noteObj.getSecondaryStatus()!=null){
															teacherStatusNoteObj.setSecondaryStatus(noteObj.getSecondaryStatus());
														}
														teacherStatusNoteObj.setUserMaster(noteObj.getUserMaster());
														teacherStatusNoteObj.setDistrictId(noteObj.getDistrictId());
														teacherStatusNoteObj.setStatusNotes(noteObj.getStatusNotes());
														teacherStatusNoteObj.setStatusNoteFileName(noteObj.getStatusNoteFileName());
														teacherStatusNoteObj.setEmailSentTo(noteObj.getEmailSentTo());
														teacherStatusNoteObj.setFinalizeStatus(noteObj.isFinalizeStatus());
														//teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
														transaction_tss=statelessSession_tss.beginTransaction();
														statelessSession_tss.insert(teacherStatusNoteObj);
														transaction_tss.commit();
													}catch(Exception ee){
														ee.printStackTrace();
													}
												}else{
													try{
														TeacherStatusNotes teacherStatusNoteObj=new TeacherStatusNotes();
														teacherStatusNoteObj.setTeacherDetail(jobForObj.getTeacherId());
														teacherStatusNoteObj.setJobOrder(jobForObj.getJobId());
														teacherStatusNoteObj.setStatusMaster(statusMaster);
														teacherStatusNoteObj.setUserMaster(userMaster);
														teacherStatusNoteObj.setDistrictId(districtMaster.getDistrictId());
														try{
															if(statusEditorNotes!=null){
																teacherStatusNoteObj.setStatusNotes(statusEditorNotes);
															}else{
																teacherStatusNoteObj.setStatusNotes("Rejected");
															}
														}catch(Exception e){
															teacherStatusNoteObj.setStatusNotes("Rejected");
															e.printStackTrace();
														}
														teacherStatusNoteObj.setEmailSentTo(1);
														teacherStatusNoteObj.setFinalizeStatus(true);
														//teacherStatusNotesDAO.makePersistent(teacherStatusNoteObj);
														transaction_tss=statelessSession_tss.beginTransaction();
														statelessSession_tss.insert(teacherStatusNoteObj);
														transaction_tss.commit();
													}catch(Exception ee){
														ee.printStackTrace();
													}
												}
												jobForObj.setOfferReady(null);
												jobForObj.setOfferAccepted(null);
												jobForObj.setRequisitionNumber(null);
												jobForObj.setStatus(statusMaster);
												jobForObj.setStatusMaster(statusMaster);
												jobForObj.setUpdatedBy(userMaster);
												jobForObj.setUpdatedByEntity(userMaster.getEntityType());		
												jobForObj.setUpdatedDate(new Date());
												jobForObj.setLastActivity("Rejected");
												jobForObj.setLastActivityDate(new Date());
												jobForObj.setUserMaster(userMaster);
												//jobForTeacherDAO.makePersistent(jobForObj);
												transaction_tss=statelessSession_tss.beginTransaction();
												statelessSession_tss.update(jobForObj);
												transaction_tss.commit();
										    }
									    }
									}catch(Exception e){
										e.printStackTrace();
									}
									/////////////////End Zone Wise Rejected By Sekhar/////////////////////
									int live=Integer.parseInt(Utility.getValueOfPropByKey("isAPILive"));
									if(districtMaster!=null &&  districtMaster.getDistrictId().equals(3628590) && live==1)
									{
										System.out.println("================== Hired by vishwanath");
										//// Vishwanath (For Hired Status SCSD candidates)
										try {
											System.out.println("555555555:: "+jobForTeacher.getTeacherId().getTeacherId());
											if(jobForTeacher.getStatus().getStatusShortName().equalsIgnoreCase("hird"))
											{
												TeacherPersonalInfo teacherPersonalInfo = mapTeacherPersonalInfo.get(jobForTeacher.getTeacherId().getTeacherId());
												System.out.println("PPPPPPPPPPP teacherPersonalInfo:: "+teacherPersonalInfo);
												
												
												List<DistrictRequisitionNumbers> lstDistrictRequisitionNumbers = new ArrayList<DistrictRequisitionNumbers>();
												String requisitionNumber1 = jobForTeacher.getRequisitionNumber();
												
												Criterion criteria5 = Restrictions.eq("requisitionNumber", requisitionNumber1);
												lstDistrictRequisitionNumbers = districtRequisitionNumbersDAO.findByCriteria(criteria5);
												
												System.out.println("lstDistrictRequisitionNumbers**inside ManageStatusAjax*******************: "+lstDistrictRequisitionNumbers.size());
												
												String postingNo = "";
												if(lstDistrictRequisitionNumbers.size()>0)
												{
													postingNo = lstDistrictRequisitionNumbers.get(0).getPostingNo();
												}
												
												
												if(teacherPersonalInfo!=null)
												{
													PATSHiredPostThread phpt = new PATSHiredPostThread();
													phpt.setRaceMap(raceMap);
													phpt.setJobForTeacher(jobForTeacher);
													phpt.setTeacherPersonalInfo(teacherPersonalInfo);
													phpt.setPostingNo(postingNo);
													//TMCommonUtil.updateSDSCHiredCandidate(jobForTeacher, teacherPersonalInfo, raceMap);
													phpt.start();
												}
											}
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
									
									bSentEmailToCandidate=true;
									
									if(bSentEmailForOfferReady)
									{
										
										String sSubjectOR="Offer Letter";
										String decline = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"0##"+statusIds+"##"+userId);
										String accept = Utility.encodeInBase64(teacherDetail.getTeacherId()+"##"+jobOrder.getJobId()+"##"+"1##"+statusIds+"##"+userId);
										
										String sOfferAcceptURL=Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+accept;
										String sOfferDeclineURL=Utility.getValueOfPropByKey("basePath")+"/verifyoffer.do?key="+decline;
										
										String schoolLocation="";
										
										String sEmailBodyText=MailText.getSecondOfferLetter(teacherDetail,sJobTitle,schoolLocation,sOfferAcceptURL,sOfferDeclineURL,isSubstituteInstructionalJob);
										
										DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
										dsmt.setEmailerService(emailerService);
										dsmt.setMailfrom("admin@netsutra.com");
										dsmt.setMailto(sTeacherEmail);
										dsmt.setMailsubject(sSubjectOR);
										dsmt.setMailcontent(sEmailBodyText);
										try {
											dsmt.start();	
										} catch (Exception e) {
											// TODO: handle exception
										}
										
										
										PrintOnConsole.debugPrintlnCGMass("<HR>************* Offer Ready Email ********<BR>");
										PrintOnConsole.debugPrintlnCGMass(" sTeacherEmail "+sTeacherEmail+" <BR>sSubjectOR "+sSubjectOR+"<BR>"+sEmailBodyText);
										PrintOnConsole.debugPrintlnCGMass("************* End Offer Ready Email ********<HR>");
									}
									
									int offerReady=2;
									try{
										if(jobForTeacher.getOfferReady()!=null && jobForTeacher.getOfferReady())
											offerReady=1;
										else
											offerReady=0;
									
									}catch(Exception e){}
									
									if(offerReady==1 && bOfferAccepted){
										String schoolLocation="";
										try{
											
											List<UserMailSend> userMailSendListFotTeacher =new ArrayList<UserMailSend>();
											UserMailSend userMailSendForTeacher= new UserMailSend();
											userMailSendForTeacher.setEmail(teacherDetail.getEmailAddress());
											userMailSendForTeacher.setTeacherDetail(teacherDetail);

											String bccAndSchoolUser[]=new String[1];
											userMailSendForTeacher.setBccEmail(bccAndSchoolUser);
											userMailSendForTeacher.setSubject("Offer Accepted");
											userMailSendForTeacher.setIsUserOrTeacherFlag(6);

											userMailSendForTeacher.setSchoolLocation(schoolLocation);

											userMailSendForTeacher.setJobTitle(jobOrder.getJobTitle());
											userMailSendForTeacher.setUserMaster(userMaster);
											userMailSendListFotTeacher.add(userMailSendForTeacher);
											
											TeacherPersonalInfo teacherPersonalInfos=null;
											if(mapTeacherPersonalInfo!=null && mapTeacherPersonalInfo.get(teacherDetail.getTeacherId())!=null)
											{
												teacherPersonalInfos=mapTeacherPersonalInfo.get(teacherDetail.getTeacherId());
											}
											
											TeacherDetail teacherPersonalInfoTemp = teacherDetail;							
											teacherPersonalInfoTemp.setFgtPwdDateTime(teacherPersonalInfos.getDob());//Dob		
											
											String teacherAddress="";
											try{
												if(teacherPersonalInfos.getAddressLine1()!=null){
													teacherAddress=teacherPersonalInfos.getAddressLine1().trim();
												}
												if(teacherPersonalInfos.getAddressLine2()!=null && !teacherPersonalInfos.getAddressLine2().equals("")){
													teacherAddress+=" "+teacherPersonalInfos.getAddressLine2().trim();
												}
												if(teacherPersonalInfos.getCityId()!=null){
													teacherAddress+=", "+teacherPersonalInfos.getCityId().getCityName();
												}
												if(teacherPersonalInfos.getStateId()!=null){
													teacherAddress+=", "+teacherPersonalInfos.getStateId().getStateName();
												}
												if(teacherPersonalInfos.getCountryId()!=null){
													teacherAddress+=", "+teacherPersonalInfos.getCountryId().getName();
												}
												if(teacherPersonalInfos.getZipCode()!=null){
													teacherAddress+=", "+teacherPersonalInfos.getZipCode();
												}
												teacherPersonalInfoTemp.setPhoneNumber(teacherPersonalInfos.getPhoneNumber());//Phone
												
											}catch(Exception e){}
											teacherPersonalInfoTemp.setAuthenticationCode(teacherAddress);//Address
											String emailAddresses="";

											if(districtKeyContactList!=null){
												int count=0;
												for (DistrictKeyContact districtKeyContact : districtKeyContactList) {
													if(count==1){
														emailAddresses+=",";
														count=0;
													}
													emailAddresses+=districtKeyContact.getKeyContactEmailAddress();
													count++;
												}
												UserMailSend userMailSendForTeacher3= new UserMailSend();
												userMailSendForTeacher3.setEmail(emailAddresses);
												userMailSendForTeacher3.setTeacherDetail(teacherPersonalInfoTemp);
												userMailSendForTeacher3.setSubject("Notification of Offer Accepted");
												userMailSendForTeacher3.setIsUserOrTeacherFlag(7);
												userMailSendForTeacher3.setSchoolLocation(schoolLocation);
												userMailSendForTeacher3.setJobTitle(jobOrder.getJobTitle());
												userMailSendListFotTeacher.add(userMailSendForTeacher3);
											}
											try {
												manageStatusAjax.mailSendToUserByThread(userMailSendListFotTeacher,null,null);
											} catch (Exception e) {}
											
											
										}catch(Exception e){
											e.printStackTrace();
										}
									}
									
								}
								else
								{
									PrintOnConsole.debugPrintlnCGMass("No need to Update jobForTeacher");
								}
							}
							
							TeacherStatusHistoryForJob statusHistory=null;
							if(mapListTeacherStatusHistoryForJobMaster!=null && mapListTeacherStatusHistoryForJobMaster.get(teacherDetail.getTeacherId())!=null)
							{
								List <TeacherStatusHistoryForJob> lstTssTemp =new ArrayList<TeacherStatusHistoryForJob>();
								lstTssTemp=mapListTeacherStatusHistoryForJobMaster.get(teacherDetail.getTeacherId());
								if(lstTssTemp!=null && lstTssTemp.size() > 0)
									statusHistory=lstTssTemp.get(0);
							}
							
							if(statusHistory==null)
							{
								// Need to add in TeacherStatusHistoryForJob
								PrintOnConsole.debugPrintlnCGMass("Need to add in TeacherStatusHistoryForJob");
								statusHistory=new TeacherStatusHistoryForJob();
								statusHistory.setTeacherDetail(teacherDetail);
								statusHistory.setJobOrder(jobOrder);
								DistrictSpecificTiming timingforDclWrdw1=new DistrictSpecificTiming();
								DistrictSpecificReason reasonforDclWrdw1=new DistrictSpecificReason();
								timingforDclWrdw1.setTimingId(timingforDclWrdw);
								reasonforDclWrdw1.setReasonId(reasonforDclWrdw);
								if(timingforDclWrdw!=null){
									statusHistory.setTimingforDclWrdw(timingforDclWrdw1);
									}
								if(reasonforDclWrdw!=null){
									statusHistory.setReasonforDclWrdw(reasonforDclWrdw1);
									}
								
								if(statusMaster!=null)
								{
									statusHistory.setStatusMaster(statusMaster);
									statusHistory.setSecondaryStatus(null);
								}
								else
								{
									statusHistory.setStatusMaster(null);
									statusHistory.setSecondaryStatus(secondaryStatus);
								}
								
								if(secondaryStatus!=null)
									statusHistory.setStatus("S");
								else
									statusHistory.setStatus("A");	
								
								statusHistory.setCreatedDateTime(new Date());
								statusHistory.setUserMaster(userMaster);
								
								transaction_tss=statelessSession_tss.beginTransaction();
								statelessSession_tss.insert(statusHistory);
								transaction_tss.commit();
								
							}
							else
							{
								PrintOnConsole.debugPrintlnCGMass("Aready have record in TeacherStatusHistoryForJob");
							}
							
							
							if(mapUpdateFitScore.get(teacherDetail.getTeacherId())!=null)
							{
								bUpdateFitScore=true;
							}
							
							if(bUpdateFitScore)
							{
								boolean isInsert=false;
								Double cScore=0.0;
								Double maxScore=0.0;
								JobWiseConsolidatedTeacherScore jwScoreObj=null;
								if(mapLstJWCTSMaster.get(teacherDetail.getTeacherId())!=null)
								{
									List<JobWiseConsolidatedTeacherScore> lstJWCTSMasterTemp= new ArrayList<JobWiseConsolidatedTeacherScore>();
									lstJWCTSMasterTemp=mapLstJWCTSMaster.get(teacherDetail.getTeacherId());
									if(lstJWCTSMasterTemp!=null && lstJWCTSMasterTemp.size() >0)
										jwScoreObj=lstJWCTSMasterTemp.get(0);
								}
								
								if(jwScoreObj==null)
								{
									cScore=topScoreProvided;
									maxScore=iMaxFitScore;
									jwScoreObj= new JobWiseConsolidatedTeacherScore();
									isInsert=true;
								}
								else
								{
									try{
										Double[] teacherStatusScoresAvg= teacherStatusScoresDAO.getAllFinalizeStatusScoreAvg(teacherDetail,jobOrder);
										cScore=teacherStatusScoresAvg[0];
										maxScore=Double.parseDouble(teacherStatusScoresAvg[1]+"");
										isInsert=false;
										}catch(Exception e){ e.printStackTrace();}

								}
								
								jwScoreObj.setTeacherDetail(teacherDetail);
								jwScoreObj.setJobOrder(jobOrder);
								jwScoreObj.setJobWiseConsolidatedScore(cScore);
								if(maxScore!=0.0)
									jwScoreObj.setJobWiseMaxScore(maxScore);
								
								if(isInsert)
								{
									transaction_tss=statelessSession_tss.beginTransaction();
									statelessSession_tss.insert(jwScoreObj);
									transaction_tss.commit();	
								}
								else
								{
									transaction_tss=statelessSession_tss.beginTransaction();
									statelessSession_tss.update(jwScoreObj);
									transaction_tss.commit();
								}
							}
							
							String sTeacherName="";
							if(teacherDetail.getFirstName()!=null && !teacherDetail.getFirstName().equals(""))
								sTeacherName=teacherDetail.getFirstName();
							if(teacherDetail.getLastName()!=null && !teacherDetail.getLastName().equals(""))
								sTeacherName=sTeacherName+" "+teacherDetail.getLastName();
							
							String sEmailToUserName="";
							if(userMaster!=null)
							{
								if(userMaster.getFirstName()!=null && !userMaster.getFirstName().equalsIgnoreCase(""))
									sEmailToUserName=userMaster.getFirstName();
								if(userMaster.getLastName()!=null && !userMaster.getLastName().equalsIgnoreCase(""))
									sEmailToUserName=sEmailToUserName+" "+userMaster.getLastName();
							}
							
							String sMailBody="";
							String sMailSubject="";
							String sToEmailAddress="";
							boolean bEmailSendToCandidateByUserConfig=false;
							if(bEmailSendToCandidateByUserConfig && bSentEmailToCandidate) 
							{
								
								if(teacherDetail!=null)
								{
									sMailBody=MailText.statusNoteSendToCandidate(userMaster,sGridURL,sUserFullName,sStatusShortCode,sDisplayStatusName,sTeacherName,sJobTitle,arrHrDetail);
									sMailSubject="Status Changed for "+jobOrder.getJobTitle()+" Position You Had Applied For";
									sToEmailAddress=teacherDetail.getEmailAddress();
									DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
									dsmt.setEmailerService(emailerService);
									dsmt.setMailfrom("cockroach@netsutra.com");
									dsmt.setMailto(sToEmailAddress);
									dsmt.setMailsubject(sMailSubject);
									dsmt.setMailcontent(sMailBody);
									
									PrintOnConsole.debugPrintlnCGMass("Send email for Status change for Candidate");
									PrintOnConsole.debugPrintlnCGMass("Try to send email for Email "+sToEmailAddress +" Subject :: "+sMailSubject);
									PrintOnConsole.debugPrintlnCGMass("mailContent "+sMailBody);
									try {
										dsmt.start();
									} catch (Exception e) {}
									
									
									MessageToTeacher  messageToTeacher= new MessageToTeacher();
									messageToTeacher.setTeacherId(teacherDetail);
									messageToTeacher.setJobId(jobOrder);
									messageToTeacher.setTeacherEmailAddress(teacherDetail.getEmailAddress());
									messageToTeacher.setMessageSubject(sMailSubject);
									messageToTeacher.setMessageSend(sMailBody);
									messageToTeacher.setMessageSource("cgstatus");
									messageToTeacher.setSenderId(userMaster);
									messageToTeacher.setSenderEmailAddress(userMaster.getEmailAddress());
									messageToTeacher.setEntityType(userMaster.getEntityType());
									messageToTeacher.setIpAddress(IPAddressUtility.getIpAddress(request));
									
									transaction_tss=statelessSession_tss.beginTransaction();
									statelessSession_tss.insert(messageToTeacher);
									transaction_tss.commit();
									
								}
							}

							List<JobCategoryWiseStatusPrivilege> lstJCP=jobCategoryWiseStatusPrivilegeDAO.getStatusForFinalizeEvent(jobOrder.getDistrictMaster(),jobOrder.getJobCategoryMaster(), secondaryStatus,statusMaster);
							try{
								if(lstJCP!=null && lstJCP.size()>0 && lstJCP.get(0).getFinalizeForEvent()!=null && lstJCP.get(0).getFinalizeForEvent()==1){
									finalizeForEvent=1;	
									/*TeacherStatusHistoryForJob statusHistoryEvent=teacherStatusHistoryForJobDAO.findByTeacherStatusAndUserMaster(teacherDetail,jobOrder,statusMaster,secondaryStatus,userMaster);
									if(statusHistoryEvent!=null){
										finalizeForEvent=0;
									}*/
								}
							}catch(Exception e){
								e.printStackTrace();
							}
							
							
							if(bSentEmailToDASA && bSentEmailToCandidate) 
							{
								PrintOnConsole.debugPrintlnCGMass("Send email for Status change to DA/SA");
								int iEmailCount=0;
								boolean overrideAll = false;
								
								List<UserMaster> userMasters=new ArrayList<UserMaster>();
								List<UserMaster> AllDAuserMasters=new ArrayList<UserMaster>();
							//	lstUserMasters=getUserMasterListForEmail_CGMass(jobOrder, userMaster, sStatusShortCode, districtMaster);
								
								
								///////////////////////////////// For Selected users /////////////////////////////
								try
								{
									//Get Selected District----------------------------------------------------
									List<JobCategoryWiseStatusPrivilege> jobCateWSPList = new ArrayList<JobCategoryWiseStatusPrivilege>(); 
									//List<UserMaster> userAllList = new ArrayList<UserMaster>();

									try
									{
								//		System.out.println(" >>>> jobOrder "+jobOrder);
								//		System.out.println(" >>>> secondaryStatusPanel "+secondaryStatusPanel);
								//		System.out.println(" >>>> statusMasterPanel "+statusMasterPanel);
									}
									catch(Exception e)
									{
										e.printStackTrace();
									}
												
									//For Selected DAs/SAa
									jobCateWSPList = jobCategoryWiseStatusPrivilegeDAO.getFilterStatusWithDPoint(jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster(), secondaryStatus,statusMaster);

									if(jobCateWSPList!=null && jobCateWSPList.size()>0)
									{
										System.out.println(" jobCateWSPList ::::::::: >>>>>>>>> "+jobCateWSPList.get(0).getJobCategoryStatusPrivilegeId()+" userMasters "+userMasters);
									}
									String userDaIdsStr = ""; 
									boolean allSchoolFlag = false;

									if(jobCateWSPList.size()>0)
									{
										userDaIdsStr = jobCateWSPList.get(0).getEmailNotificationToSelectedDistrictAdmins();
										allSchoolFlag = jobCateWSPList.get(0).isEmailNotificationToAllSchoolAdmins();
										overrideAll = jobCateWSPList.get(0).isOverrideUserSettings();
										
										System.out.println(" userDaIdsStr "+userDaIdsStr+" allSchoolFlag :: "+allSchoolFlag+" overrideAll "+overrideAll);

										if(userDaIdsStr==null || userDaIdsStr.equals(""))
										{
											AllDAuserMasters = userMasterDAO.getActiveDAUserByDistrict(districtMaster);
										}


										if((userDaIdsStr!=null && !userDaIdsStr.equals("")) || allSchoolFlag)
										{
											System.out.println(" Condiation 1");
											List<Integer> daIdList = new ArrayList<Integer>();
											System.out.println(" uuuuuuuuuuuuuuu ");
											if(userDaIdsStr!=null && !userDaIdsStr.equals(""))
											{
												String[] userDaIds = userDaIdsStr.split("#");

												if((userDaIdsStr!=null && !userDaIdsStr.equals("")))
												{
													for(int i=0;i<userDaIds.length;i++)
													{
														daIdList.add(Integer.parseInt(userDaIds[i]));
													}
												}
											}

											//System.out.println("allSchoolFlag "+allSchoolFlag+" daIdList size :: "+daIdList.size());

											//get Schools
											if(allSchoolFlag)
											{
												if(userDaIdsStr==null || userDaIdsStr.equals(""))
												{
													AllDAuserMasters = userMasterDAO.getActiveDAUserByDistrict(districtMaster);
												}

												System.out.println(" jobOrder.getCreatedForEntity() "+jobOrder.getCreatedForEntity()+" userMaster.getEntityType() "+userMaster.getEntityType());


												if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2){
													System.out.println("00001");
													List<SchoolMaster>  schoolMasters= new ArrayList<SchoolMaster>();
													if(jobOrder.getSchool()!=null){
														System.out.println("00002");
														if(jobOrder.getSchool().size()!=0){
															System.out.println("00003");

															System.out.println("00004 :: "+jobOrder.getSchool());

															for(SchoolMaster sMaster : jobOrder.getSchool()){
																schoolMasters.add(sMaster);
															}
														}
													}
													if(schoolMasters!=null && schoolMasters.size()>0)
													{
														if(daIdList!=null && daIdList.size()>0)
															userMasters	= userMasterDAO.getUserIDSBySchoolsAndDistrict(schoolMasters,daIdList);
														else
															userMasters	= userMasterDAO.getUserIDSBySchools(schoolMasters);

														if(AllDAuserMasters.size()>0)
															userMasters.addAll(AllDAuserMasters);
													}
													else
													{
														if(userDaIdsStr!=null && !userDaIdsStr.equals(""))
															userMasters = userMasterDAO.getUsersByUserIds(daIdList);
														else
														{
															if(AllDAuserMasters.size()>0)
																userMasters.addAll(AllDAuserMasters);
														}
													}
												}
											}else{
												userMasters = userMasterDAO.getUsersByUserIds(daIdList);
											}

											if(userMasters!=null && userMasters.size()>0)
												System.out.println(" >>>>>>>>>>> final user list :: "+userMasters.size());
											else
												System.out.println(" >>>>>>>>>>> final user list is 0 ");

										}
										else
										{
											System.out.println(" Condidation 1.1 ");
											System.out.println("AllDAuserMasters size :: "+AllDAuserMasters.size());
											if(AllDAuserMasters.size()>0)
												userMasters.addAll(AllDAuserMasters);
											
											System.out.println(" userMasters size :::::: "+userMasters.size());
										}
										
										//For All selected users which have opted notification on
										if(overrideAll==false)
										{
											if(userMasters.size()>0)
												userMasters=getUserMasterListForEmail_CGMass(jobOrder, userMasters, sStatusShortCode, districtMaster);
										}
											
										
										System.out.println(" userMasters final>>>>>>>>>>>>>>>>>>>>>>>size :::::"+userMasters.size());
										
									}
									
								}catch(Exception e)
								{
									e.printStackTrace();
								}
								/////////////////////////////////////////////////////////////////////////////////
								
								
								
								if(userMasters!=null)
								{
									for(UserMaster userMasterObj : userMasters)
									{
										System.out.println(">>>>>>>>>>>>>>>>>>>>>>> Send Mail");
										try {
											sToEmailAddress=userMasterObj.getEmailAddress();
											if(isEmailTemplateChanged==0 && lstStatusSpecificEmailTemplates.size()>0)
											{
												System.out.println("01");
												sMailSubject=lstStatusSpecificEmailTemplates.get(0).getSubjectLine();
												sMailBody	=	lstStatusSpecificEmailTemplates.get(0).getTemplateBody();
												if(sMailBody.contains("&lt; "+lblTeacherName1+" &gt;") || sMailBody.contains("&lt;"+lblUserName+" &gt;") || sMailBody.contains("&lt;"+lblJoTil+" &gt;")|| sMailBody.contains("&lt; "+lblStatusName+" &gt;"))
												{
													System.out.println("02");
													String[] arrHrDetailHr = arrHrDetail;
													String hrContactFirstName = arrHrDetailHr[0];
													String hrContactLastName = arrHrDetailHr[1];
													String phoneNumber			 = arrHrDetailHr[3];
													String title		 = arrHrDetailHr[4];
													int isHrContactExist = Integer.parseInt(arrHrDetailHr[5]);
													String schoolDistrictName =	arrHrDetailHr[6];
													String dmName 			 = arrHrDetailHr[7];
													String dmPhoneNumber	 = arrHrDetailHr[9];

													StringBuffer hrSb 					= 	new StringBuffer();
													hrSb.append("<table>");
													hrSb.append("<tr>");
													hrSb.append("<td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>");
													if(isHrContactExist==0)
														hrSb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
													else 
														if(isHrContactExist==1)
														{
															hrSb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
															if(title!="")
																hrSb.append(title+"<br/>");
															if(phoneNumber!="")
																sb.append(phoneNumber+"<br/>");
															hrSb.append(schoolDistrictName+"<br/><br/>");
														}
														else
														{
															if(isHrContactExist==2)
															{
																hrSb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+dmName+"<br/>");
																if(dmPhoneNumber!="")
																	sb.append(dmPhoneNumber+"<br/>");
																hrSb.append(schoolDistrictName+"<br/><br/>");
															}
														}
													hrSb.append("</td>");
													hrSb.append("</tr>");
													hrSb.append("</table>");

													sMailBody	=	sMailBody.replaceAll("&lt; "+lblTeacherName1+" &gt;",sTeacherName);
													sMailBody	=	sMailBody.replaceAll("&lt;"+lblUserName+" &gt;",sEmailToUserName);
													sMailBody	=	sMailBody.replaceAll("&lt; "+lblJoTil+"&gt;",sJobTitle);
													sMailBody	=	sMailBody.replaceAll("&lt; "+lblStatusName+"&gt;",sDisplayStatusName);
													sMailBody	=	sMailBody.replaceAll("&lt; "+lblURL1+" &gt;",sGridURL);
													sMailBody	=	sMailBody.replaceAll("&lt; "+lblHrContact1+" &gt;",hrSb.toString());
													sMailBody	=	sMailBody.replaceAll("&lt; "+lblUserEmailAddress1+" &gt;",userMaster.getEmailAddress());
												}
											}
											else
											{
												System.out.println("03");
												if(isEmailTemplateChanged==1)
												{
													System.out.println("04");
													sMailSubject=aDASAMsgSubject;
													sMailBody=sAdminMailBody;
													sMailBody	=	sMailBody.replaceAll("&lt;"+lblTeacherName1+" &gt;",sTeacherName);
												}
											}
											
											if(sToEmailAddress!=null && !sToEmailAddress.equals("") && sMailSubject!=null && !sMailSubject.equals("") && sMailBody!=null && !sMailBody.equals(""))
											{
												DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
												dsmt.setEmailerService(emailerService);
												dsmt.setMailfrom("noreply@teachermatch.net");
												dsmt.setMailto(sToEmailAddress);
												dsmt.setMailsubject(sMailSubject);
												dsmt.setMailcontent(sMailBody);
												
												iEmailCount++;
												PrintOnConsole.debugPrintlnCGMass("<HR>"+iEmailCount+" Try to send email for Email "+sToEmailAddress +" Subject :: "+sMailSubject+"<BR>");
												PrintOnConsole.debugPrintlnCGMass("mailContent "+sMailBody+" <HR> ");
												try {
													dsmt.start();
												} catch (Exception e) {
													// TODO: handle exception
												}
											}
											
										
										} catch (Exception e) {
											// TODO: handle exception
										}
										sToEmailAddress=userMasterObj.getEmailAddress();
										if(isEmailTemplateChanged==0 && lstStatusSpecificEmailTemplates.size()>0)
										{
											sMailSubject=lstStatusSpecificEmailTemplates.get(0).getSubjectLine();
											sMailBody	=	lstStatusSpecificEmailTemplates.get(0).getTemplateBody();
											if(sMailBody.contains("&lt; "+lblTeacherName1+" &gt;") || sMailBody.contains("&lt;"+lblUserName+"&gt;") || sMailBody.contains("&lt; "+lblJoTil+" &gt;")|| sMailBody.contains("&lt; "+lblStatusName+" &gt;"))
											{
												String[] arrHrDetailHr = arrHrDetail;
												String hrContactFirstName = arrHrDetailHr[0];
												String hrContactLastName = arrHrDetailHr[1];
												String phoneNumber			 = arrHrDetailHr[3];
												String title		 = arrHrDetailHr[4];
												int isHrContactExist = Integer.parseInt(arrHrDetailHr[5]);
												String schoolDistrictName =	arrHrDetailHr[6];
												String dmName 			 = arrHrDetailHr[7];
												String dmPhoneNumber	 = arrHrDetailHr[9];

												StringBuffer hrSb 					= 	new StringBuffer();
												hrSb.append("<table>");
												hrSb.append("<tr>");
												hrSb.append("<td style='font-family: Century Gothic,Open Sans, sans-serif;font-size: 16px;'>");
												if(isHrContactExist==0)
													hrSb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+schoolDistrictName+" Recruitment and Selection Team <br/><br/>");
												else 
													if(isHrContactExist==1)
													{
														hrSb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+hrContactFirstName+" "+hrContactLastName+"<br/>");
														if(title!="")
															hrSb.append(title+"<br/>");
														if(phoneNumber!="")
															sb.append(phoneNumber+"<br/>");
														hrSb.append(schoolDistrictName+"<br/><br/>");
													}
													else
													{
														if(isHrContactExist==2)
														{
															hrSb.append(""+mailAcceotOrDeclineMailHtmal11+"<br/>"+dmName+"<br/>");
															if(dmPhoneNumber!="")
																sb.append(dmPhoneNumber+"<br/>");
															hrSb.append(schoolDistrictName+"<br/><br/>");
														}
													}
												hrSb.append("</td>");
												hrSb.append("</tr>");
												hrSb.append("</table>");

												sMailBody	=	sMailBody.replaceAll("&lt; "+lblTeacherName1+"&gt;",sTeacherName);
												sMailBody	=	sMailBody.replaceAll("&lt; "+lblUserName+" &gt;",sEmailToUserName);
												sMailBody	=	sMailBody.replaceAll("&lt; "+lblJoTil+" &gt;",sJobTitle);
												sMailBody	=	sMailBody.replaceAll("&lt; "+lblStatusName+" &gt;",sDisplayStatusName);
												
												sMailBody	=	sMailBody.replaceAll("&lt; "+lblURL1+" &gt;",sGridURL);
												sMailBody	=	sMailBody.replaceAll("&lt;"+lblHrContact1+" &gt;",hrSb.toString());
												sMailBody	=	sMailBody.replaceAll("&lt;"+lblUserEmailAddress1+" &gt;",userMaster.getEmailAddress());
											}
										}
										else
										{
											if(isEmailTemplateChanged==1)
											{
												sMailSubject=aDASAMsgSubject;
												sMailBody=sAdminMailBody;
												sMailBody	=	sMailBody.replaceAll("&lt; "+lblTeacherName1+" &gt;",sTeacherName);
											}
										}
										
										if(sToEmailAddress!=null && !sToEmailAddress.equals("") && sMailSubject!=null && !sMailSubject.equals("") && sMailBody!=null && !sMailBody.equals(""))
										{
											DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
											dsmt.setEmailerService(emailerService);
											dsmt.setMailfrom("cockroach@netsutra.com");
											dsmt.setMailto(sToEmailAddress);
											dsmt.setMailsubject(sMailSubject);
											dsmt.setMailcontent(sMailBody);
											
											iEmailCount++;
											PrintOnConsole.debugPrintlnCGMass("<HR>"+iEmailCount+" Try to send email for Email "+sToEmailAddress +" Subject :: "+sMailSubject+"<BR>");
											PrintOnConsole.debugPrintlnCGMass("mailContent "+sMailBody+" <HR> ");
											try {
												dsmt.start();
											} catch (Exception e) {
												// TODO: handle exception
											}
										}
									}
								}
								
							}
							
							// End process here ....
						}						
						
					}
				}
				System.out.println(" >>>>>>>>>>>>>> teacherDetailLst >>>>>>>>"+schoolId_mass+">>>> :: "+teacherDetailLst.size());
				mailSchoolSelection(statusMaster,secondaryStatus,jobOrder,teacherDetailLst,userMaster,request);//added
				
				{
					String statusMasterSecondaryName=null;
					if(statusMaster!=null)
					{
						List<SecondaryStatus>  lstSecondaryStatus_temp=secondaryStatusDAO.getSecondaryStatusForPanel(jobOrder.getDistrictMaster(), jobOrder.getJobCategoryMaster(), statusMaster);
						if(lstSecondaryStatus_temp.size() ==1)
						{
							statusMasterSecondaryName=lstSecondaryStatus_temp.get(0).getSecondaryStatusName();//added							
						}
					}					
				manageStatusAjax.statusWise(jobOrder,teacherDetailLst,statusMaster,secondaryStatus,statusMasterSecondaryName,userMaster,null,schoolId_mass);//added by ram nath
				}
				statelessSession_tss.close();
				
			}
		}
		catch (Exception e) {
			// TODO: handle exception
		}
		String sReturnArray[]=new String[3];
		sReturnArray[0]=sCGInfoMessage;// message info
		sReturnArray[1]="0"; // confirmation DIV (1 - Need to Conform , 0 - OK)
		
		sReturnArray[2]=finalizeForEvent+"";
		
		return sReturnArray;
	}
	
	
	public Map<Integer, List<StatusSpecificScore>> getSSSListMap(List<StatusSpecificScore> lstStatusSpecificScore)
	{
		Map<Integer, List<StatusSpecificScore>> mapStatusSpecificScoreList=new HashMap<Integer, List<StatusSpecificScore>>();
		for(StatusSpecificScore specificScore :lstStatusSpecificScore)
		{
			List<StatusSpecificScore> tempList=new ArrayList<StatusSpecificScore>();
			if(mapStatusSpecificScoreList.get(specificScore.getTeacherDetail().getTeacherId())==null)
			{
				tempList=new ArrayList<StatusSpecificScore>();
				tempList.add(specificScore);
				mapStatusSpecificScoreList.put(specificScore.getTeacherDetail().getTeacherId(), tempList);
			}
			else
			{
				tempList=new ArrayList<StatusSpecificScore>();
				tempList=mapStatusSpecificScoreList.get(specificScore.getTeacherDetail().getTeacherId());
				tempList.add(specificScore);
				mapStatusSpecificScoreList.put(specificScore.getTeacherDetail().getTeacherId(), tempList);
			}
		}
		
		return mapStatusSpecificScoreList;
	}
	
	
	public Map<Integer, List<TeacherStatusNotes>> getTSNListMap(List<TeacherStatusNotes> lstTeacherStatusNotes)
	{
		Map<Integer, List<TeacherStatusNotes>> mapTeacherStatusNotesList=new HashMap<Integer, List<TeacherStatusNotes>>();
		for(TeacherStatusNotes statusNotes :lstTeacherStatusNotes)
		{
			List<TeacherStatusNotes> tempList=new ArrayList<TeacherStatusNotes>();
			if(mapTeacherStatusNotesList.get(statusNotes.getTeacherDetail().getTeacherId())==null)
			{
				tempList=new ArrayList<TeacherStatusNotes>();
				tempList.add(statusNotes);
				mapTeacherStatusNotesList.put(statusNotes.getTeacherDetail().getTeacherId(), tempList);
			}
			else
			{
				tempList=new ArrayList<TeacherStatusNotes>();
				tempList=mapTeacherStatusNotesList.get(statusNotes.getTeacherDetail().getTeacherId());
				tempList.add(statusNotes);
				mapTeacherStatusNotesList.put(statusNotes.getTeacherDetail().getTeacherId(), tempList);
			}
		}
		
		return mapTeacherStatusNotesList;
	}
	
	public List<TeacherDetail> getTeacherListFromSSS(List<StatusSpecificScore> lstStatusSpecificScore)
	{
		List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
		for(StatusSpecificScore specificScore :lstStatusSpecificScore)
		{
			if(lstTeacherDetails.contains(specificScore.getTeacherDetail())==true)
			{
			}
			else
				lstTeacherDetails.add(specificScore.getTeacherDetail());
		}
		return lstTeacherDetails;
	}
	
	public List<TeacherDetail> getTeacherListFromTSN(List<TeacherStatusNotes> lstTeacherStatusNotes)
	{
		List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
		for(TeacherStatusNotes teacherStatusNotes :lstTeacherStatusNotes)
		{
			if(lstTeacherDetails.contains(teacherStatusNotes.getTeacherDetail())==true)
			{
			}
			else
				lstTeacherDetails.add(teacherStatusNotes.getTeacherDetail());
		}
		return lstTeacherDetails;
	}
	
	public List<TeacherDetail> getTeacherListFromTSHForJob(List <TeacherStatusHistoryForJob> lstTSHForJob)
	{
		List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
		for(TeacherStatusHistoryForJob teacherStatusHistoryForJob :lstTSHForJob)
		{
			if(lstTeacherDetails.contains(teacherStatusHistoryForJob.getTeacherDetail())==true)
			{
				//PrintOnConsole.debugPrintlnCGMass("No Green "+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+" "+teacherStatusHistoryForJob.getTeacherDetail().getEmailAddress());
			}
			else
			{
				lstTeacherDetails.add(teacherStatusHistoryForJob.getTeacherDetail());
				PrintOnConsole.debugPrintlnCGMass("Green "+teacherStatusHistoryForJob.getTeacherDetail().getTeacherId()+" "+teacherStatusHistoryForJob.getTeacherDetail().getEmailAddress());
			}
		}
		return lstTeacherDetails;
	}
	
	public List<TeacherDetail> NotAllSetDPointTeacherList(JobOrder jobOrder,StatusMaster statusMaster,List<TeacherDetail> lstTeacherDetail)
	{
		List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
		if(jobOrder.getDistrictMaster()!=null && statusMaster!=null && (statusMaster.getStatusShortName().equalsIgnoreCase("scomp") || statusMaster.getStatusShortName().equalsIgnoreCase("ecomp") || statusMaster.getStatusShortName().equalsIgnoreCase("vcomp")))
		{
			String secondaryStatusNames="";
			if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()!=null){
				if(jobOrder.getDistrictMaster().getSetAssociatedStatusToSetDPoints()){
					String accessDPoints="";
					if(jobOrder.getDistrictMaster().getAccessDPoints()!=null)
						accessDPoints=jobOrder.getDistrictMaster().getAccessDPoints();

					if(accessDPoints.contains(""+statusMaster.getStatusId())){
						List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs=new ArrayList<TeacherStatusHistoryForJob>();
						if(lstTeacherDetail!=null && lstTeacherDetail.size() > 0 && jobOrder!=null)
						{
							teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.massStatusSelected(lstTeacherDetail, jobOrder);
							
							List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithStatus(jobOrder,statusMaster);

							Map<String,SecondaryStatus> secMap=new HashMap<String, SecondaryStatus>();
							if(teacherStatusHistoryForJobs!=null && teacherStatusHistoryForJobs.size() > 0)
							{
								for(TeacherStatusHistoryForJob tSHObj : teacherStatusHistoryForJobs){
									if(tSHObj.getSecondaryStatus()!=null)
										secMap.put(tSHObj.getTeacherDetail().getTeacherId()+"_"+tSHObj.getSecondaryStatus().getSecondaryStatusId(),tSHObj.getSecondaryStatus());
								}
							}
							
							for(TeacherDetail teacherDetail:lstTeacherDetail)
							{
								if(teacherDetail!=null)
								{
									if(listSecondaryStatus!=null && listSecondaryStatus.size()>0)
									{
										secondaryStatusNames="";
										int counter=0;
										for(SecondaryStatus  obj : listSecondaryStatus.get(0).getChildren())
										{
											if(obj.getStatus().equalsIgnoreCase("A") && obj.getStatusMaster()==null)
											{
												SecondaryStatus sec=secMap.get(teacherDetail.getTeacherId()+"_"+obj.getSecondaryStatusId());
												if(sec==null)
												{
													boolean isJobAssessment=false;
													if(jobOrder.getIsJobAssessment()!=null)
													{
														if(jobOrder.getIsJobAssessment()){
															isJobAssessment=true;
														}
													}
													if((isJobAssessment && obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))|| !obj.getSecondaryStatusName().equalsIgnoreCase("JSI"))
													{
														if(counter==1){
															secondaryStatusNames+=", ";
														}
														counter=0;
														secondaryStatusNames+=obj.getSecondaryStatusName();
														counter++;
														break;
													}
												}
											}
										}
									}
								}
								
								if(secondaryStatusNames!=null && secondaryStatusNames.length() > 0)
								{	
									lstTeacherDetails.add(teacherDetail);
									PrintOnConsole.debugPrintlnCGMass("Not All Set DPoint "+teacherDetail.getTeacherId()+" "+teacherDetail.getEmailAddress());
								}
								else
								{
									PrintOnConsole.debugPrintlnCGMass("All Set DPoint "+teacherDetail.getTeacherId()+" "+teacherDetail.getEmailAddress());
								}
							}
						}
					}
				}
			}
		}
		return lstTeacherDetails;
	}
	
	public Map<Integer, List<TeacherStatusHistoryForJob>> getMapForTSHForJob(List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs)
	{
		Map<Integer, List<TeacherStatusHistoryForJob>> mapTSHForJob=new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
		for (TeacherStatusHistoryForJob teacherStatusHistoryForJob : teacherStatusHistoryForJobs) 
		{
			List<TeacherStatusHistoryForJob> lstTemp=new ArrayList<TeacherStatusHistoryForJob>();
			if(mapTSHForJob.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId())==null)
			{
				lstTemp.add(teacherStatusHistoryForJob);
				mapTSHForJob.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(), lstTemp);
			}
			else
			{
				lstTemp=new ArrayList<TeacherStatusHistoryForJob>();
				lstTemp=mapTSHForJob.get(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId());
				lstTemp.add(teacherStatusHistoryForJob);
				mapTSHForJob.put(teacherStatusHistoryForJob.getTeacherDetail().getTeacherId(), lstTemp);
			}
		}
		return mapTSHForJob;
	}
	
	
	public List<TeacherDetail> NotSetAllBeforeOfferReadyTeacherList(JobOrder jobOrder,List<TeacherDetail> lstTeacherDetail)
	{
		List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
		List<TeacherStatusHistoryForJob> teacherStatusHistoryForJobs=new ArrayList<TeacherStatusHistoryForJob>();
		if(lstTeacherDetail!=null && lstTeacherDetail.size() > 0 && jobOrder!=null)
		{
			teacherStatusHistoryForJobs=teacherStatusHistoryForJobDAO.massStatusSelected(lstTeacherDetail, jobOrder);
			Map<Integer, List<TeacherStatusHistoryForJob>> mapTSHForJob=new HashMap<Integer, List<TeacherStatusHistoryForJob>>();
			mapTSHForJob=getMapForTSHForJob(teacherStatusHistoryForJobs);
			
			List<SecondaryStatus> listSecondaryStatus = secondaryStatusDAO.findSecondaryStatusByJobCategoryWithOutStatus(jobOrder);
			
			for(TeacherDetail teacherDetail:lstTeacherDetail)
			{
				if(teacherDetail!=null)
				{
					Map<String,Integer> allMasterStatusMap = new HashMap<String, Integer>();
					if(listSecondaryStatus!=null && listSecondaryStatus.size()>0)
					{
						allMasterStatusMap = new HashMap<String, Integer>();
						for (SecondaryStatus secondaryStatus : listSecondaryStatus) 
						{
							if(secondaryStatus.getStatusMaster()!=null)
								allMasterStatusMap.put(secondaryStatus.getStatusMaster().getStatusShortName(), 1);
							else
								allMasterStatusMap.put(secondaryStatus.getSecondaryStatusName(), 1);
						}
					}
					
					Map<String,Integer> cmpMap = new HashMap<String, Integer>();
					List<TeacherStatusHistoryForJob> lstTemp=new ArrayList<TeacherStatusHistoryForJob>();

					if(mapTSHForJob.get(teacherDetail.getTeacherId())!=null)
					{
						lstTemp=mapTSHForJob.get(teacherDetail.getTeacherId());
						cmpMap = new HashMap<String, Integer>();
						for (TeacherStatusHistoryForJob tSHFJob : lstTemp) 
						{
							if(tSHFJob.getSecondaryStatus()!=null)
								cmpMap.put(tSHFJob.getSecondaryStatus().getSecondaryStatusName(),1);
							if(tSHFJob.getStatusMaster()!=null && !tSHFJob.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp"))
								cmpMap.put(tSHFJob.getStatusMaster().getStatusShortName(),1);
						}
					}
					
					
					
					
					if(allMasterStatusMap.size()>0)
					{
						allMasterStatusMap.remove("vcomp");
						allMasterStatusMap.remove("apl");
						allMasterStatusMap.remove("Offer Ready");
						allMasterStatusMap.remove("icomp");
						allMasterStatusMap.remove("vlt");
						allMasterStatusMap.remove("hird");
						allMasterStatusMap.remove("dcln");
						allMasterStatusMap.remove("rem");
						allMasterStatusMap.remove("widrw");
						
						cmpMap.remove("Offer Ready");
						cmpMap.remove("hird");
						cmpMap.remove("dcln");
						cmpMap.remove("rem");
						cmpMap.remove("widrw");
						cmpMap.remove("vcomp");
						cmpMap.remove("icomp");
						cmpMap.remove("vlt");
						cmpMap.remove("apl");
					}
					
					if(cmpMap.size()!=allMasterStatusMap.size())
					{
						lstTeacherDetails.add(teacherDetail);
						PrintOnConsole.debugPrintlnCGMass("Not Set All Before OfferReady "+teacherDetail.getTeacherId()+" "+teacherDetail.getEmailAddress());
					}
					else
					{
						PrintOnConsole.debugPrintlnCGMass("Set All Before OfferReady "+teacherDetail.getTeacherId()+" "+teacherDetail.getEmailAddress());
					}
					
				}
			}
			
			
		}	
		return lstTeacherDetails;
	}
	
	
	public List<TeacherDetail> getAlreadyOfferTeacherList(List<JobForTeacher> jftOffer)
	{
		List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
		
		if(jftOffer!=null && jftOffer.size() > 0)
		{
			for(JobForTeacher forTeacher:jftOffer)
			{
				if(forTeacher!=null)
				{
					if(lstTeacherDetails.contains(forTeacher.getTeacherId())==true)
					{
						PrintOnConsole.debugPrintlnCGMass("No Already Offer Ready "+forTeacher.getTeacherId().getTeacherId()+" "+forTeacher.getTeacherId().getEmailAddress());
					}
					else
					{
						lstTeacherDetails.add(forTeacher.getTeacherId());
						PrintOnConsole.debugPrintlnCGMass("Already Offer Ready "+forTeacher.getTeacherId().getTeacherId()+" "+forTeacher.getTeacherId().getEmailAddress());
					}
				}
			}
		}
		return lstTeacherDetails;
	}
	
	public Map<Integer, JobForTeacher> getJFTMapCGMass(List<JobForTeacher> jobForTeachers)
	{
		Map<Integer, JobForTeacher> mapJFT=new HashMap<Integer, JobForTeacher>();
		
		if(jobForTeachers!=null && jobForTeachers.size() > 0)
		{
			for(JobForTeacher jft:jobForTeachers)
			{
					if(jft!=null && mapJFT.get(jft.getTeacherId().getTeacherId())==null)
						mapJFT.put(jft.getTeacherId().getTeacherId(), jft);
			}
		}
		
		return mapJFT;
	}
	
	public List<TeacherDetail> getFinalizeTSSTeacherList(List<TeacherStatusScores> lstTSS_V)
	{
		List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
		
		if(lstTSS_V!=null && lstTSS_V.size() > 0)
		{
			for(TeacherStatusScores tss:lstTSS_V)
			{
				if(tss!=null)
				{
					if(lstTeacherDetails.contains(tss.getTeacherDetail())==true)
					{
						PrintOnConsole.debugPrintlnCGMass("No Finalize TSS "+tss.getTeacherDetail().getTeacherId()+" "+tss.getTeacherDetail().getEmailAddress());
					}
					else
					{
						lstTeacherDetails.add(tss.getTeacherDetail());
						PrintOnConsole.debugPrintlnCGMass("Already Finalize TSS "+tss.getTeacherDetail().getTeacherId()+" "+tss.getTeacherDetail().getEmailAddress());
					}
				}
			}
		}
		return lstTeacherDetails;
	}
	
	public List<UserMaster> getUserMasterListForEmail_CGMass(JobOrder jobOrder,List<UserMaster> userMasters,String sStatusShortCode,DistrictMaster districtMaster)
	{
		
		if(sStatusShortCode!=null && sStatusShortCode.equals("oth"))
			sStatusShortCode="soth";
		
		List<UserMaster> lstUserMasters=new ArrayList<UserMaster>();
		List<RoleMaster> roleMasters=new ArrayList<RoleMaster>();
		RoleMaster roleMaster=new RoleMaster();
		roleMaster.setRoleId(2);
		roleMasters.add(roleMaster);
		roleMaster=new RoleMaster();
		roleMaster.setRoleId(3);
		roleMasters.add(roleMaster);
		try{
			sStatusShortCode = "soth";
			System.out.println(" sStatusShortCode :: "+sStatusShortCode+" roleMasters :: "+roleMasters.get(0).getRoleId()+" userMasters "+userMasters.size());
			lstUserMasters = userEmailNotificationsDAO.getUserByNotificationOn_slc(userMasters, sStatusShortCode, roleMasters);
			
			/*boolean isSchool=false;
			if(jobOrder.getSelectedSchoolsInDistrict()!=null){
				if(jobOrder.getSelectedSchoolsInDistrict()==1){
					isSchool=true;
				}
			}
			if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchool==false){
				lstUserMasters=userEmailNotificationsDAO.getUserByDistrict_slc(jobOrder.getDistrictMaster(),sStatusShortCode,roleMasters);
			}else if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==2 && isSchool){
				List<SchoolMaster>  schoolMasters= new ArrayList<SchoolMaster>();
				if(jobOrder.getSchool()!=null){
					if(jobOrder.getSchool().size()!=0){
						for(SchoolMaster sMaster : jobOrder.getSchool()){
							schoolMasters.add(sMaster);
						}
					}
				}
				lstUserMasters=userEmailNotificationsDAO.getUserByDistrictAndSchoolList_slc(districtMaster, schoolMasters,jobOrder,sStatusShortCode,roleMasters);
			}else if(jobOrder.getCreatedForEntity()==2 && userMaster.getEntityType()==3 && isSchool){
				lstUserMasters=userEmailNotificationsDAO.getUserByDistrictListOnlySchool_slc(districtMaster,userMaster.getSchoolId(),jobOrder,sStatusShortCode,roleMasters);
			}else if(jobOrder.getCreatedForEntity()==3 && userMaster.getEntityType()==3){
				lstUserMasters=userEmailNotificationsDAO.getUserByOnlySchool_slc(userMaster.getSchoolId(),sStatusShortCode,roleMasters);
			}*/
		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally
		{
			return lstUserMasters;
		}
	}
	
	
	public void sendEmailOfficialTranscripts_CGMass(TeacherDetail teacherDetail,String sDistrictName)
	{
		PrintOnConsole.debugPrintln(":::::::::::::::::::::::::::::::::Mass ... Copies of your official transcripts::::::::::::::::::::::");
		if(teacherDetail!=null)
		{
			List<TeacherAcademics> lstTeacherAcademics=teacherAcademicsDAO.findAcadamicDetailByTeacher(teacherDetail);
			if(lstTeacherAcademics!=null && lstTeacherAcademics.size()>0)
			{
				for(TeacherAcademics academics:lstTeacherAcademics)
				{
					if(academics!=null)
					{
						academics.setStatus("V");
						teacherAcademicsDAO.makePersistent(academics);
					}
				}
			}

			String sTo=teacherDetail.getEmailAddress();
			DemoScheduleMailThread dsmt = new DemoScheduleMailThread();
			String mailContent = MailText.OfficialTranscripts(teacherDetail,sDistrictName);
			dsmt.setEmailerService(emailerService);
			dsmt.setMailfrom("admin@netsutra.com");
			dsmt.setMailto(sTo);
			dsmt.setMailsubject(""+lblCopiesOfficialTran+"");
			dsmt.setMailcontent(mailContent);

			PrintOnConsole.debugPrintln("*************** SendEmailOfficialTranscripts ************");
			PrintOnConsole.debugPrintln(""+msgTrySendEmail1+" "+sTo +" "+msgTrySendEmail2+"");
			
			try {
				dsmt.start();
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	}

	public StatusMaster findStatusByShortName(List <StatusMaster> statusMasterList,String statusShortName)
	{
		StatusMaster status= null;
		try 
		{   if(statusMasterList!=null)
			for(StatusMaster statusMaster: statusMasterList){
				if(statusMaster.getStatusShortName().equalsIgnoreCase(statusShortName)){
					status=statusMaster;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		if(status==null)
			return null;
		else
			return status;
	}
	

	public Map<Integer, List<TeacherStatusScores>> getTSSListMap(List<TeacherStatusScores> lstTSS_Master)
	{
		Map<Integer, List<TeacherStatusScores>> maplstTSS_Master=new HashMap<Integer, List<TeacherStatusScores>>();
		for(TeacherStatusScores teacherStatusScores :lstTSS_Master)
		{
			List<TeacherStatusScores> tempList=new ArrayList<TeacherStatusScores>();
			if(maplstTSS_Master.get(teacherStatusScores.getTeacherDetail().getTeacherId())==null)
			{
				tempList=new ArrayList<TeacherStatusScores>();
				tempList.add(teacherStatusScores);
				maplstTSS_Master.put(teacherStatusScores.getTeacherDetail().getTeacherId(), tempList);
			}
			else
			{
				tempList=new ArrayList<TeacherStatusScores>();
				tempList=maplstTSS_Master.get(teacherStatusScores.getTeacherDetail().getTeacherId());
				tempList.add(teacherStatusScores);
				maplstTSS_Master.put(teacherStatusScores.getTeacherDetail().getTeacherId(), tempList);
			}
		}
		
		return maplstTSS_Master;
	}
	
	public Map<Integer, List<JobWiseConsolidatedTeacherScore>> getJWCTSMap(List<JobWiseConsolidatedTeacherScore> lstJWCTSMaster)
	{
		Map<Integer, List<JobWiseConsolidatedTeacherScore>> mapLstJWCTSMaster=new HashMap<Integer, List<JobWiseConsolidatedTeacherScore>>();
		for(JobWiseConsolidatedTeacherScore jWCTS :lstJWCTSMaster)
		{
			List<JobWiseConsolidatedTeacherScore> tempList=new ArrayList<JobWiseConsolidatedTeacherScore>();
			if(mapLstJWCTSMaster.get(jWCTS.getTeacherDetail().getTeacherId())==null)
			{
				tempList=new ArrayList<JobWiseConsolidatedTeacherScore>();
				tempList.add(jWCTS);
				mapLstJWCTSMaster.put(jWCTS.getTeacherDetail().getTeacherId(), tempList);
			}
			else
			{
				tempList=new ArrayList<JobWiseConsolidatedTeacherScore>();
				tempList=mapLstJWCTSMaster.get(jWCTS.getTeacherDetail().getTeacherId());
				tempList.add(jWCTS);
				mapLstJWCTSMaster.put(jWCTS.getTeacherDetail().getTeacherId(), tempList);
			}
		}
		return mapLstJWCTSMaster;
	}
	
	public Map<Integer,TeacherPersonalInfo> getTeacherPersonalInfoCGMass(List<TeacherPersonalInfo> teacherPersonalInfoList)
	{
		Map<Integer, TeacherPersonalInfo> mapTeacherPersonalInfo=new HashMap<Integer, TeacherPersonalInfo>();
		if(teacherPersonalInfoList!=null && teacherPersonalInfoList.size() > 0)
		{
			for(TeacherPersonalInfo teacherPersonalInfo:teacherPersonalInfoList)
			{
					if(teacherPersonalInfo!=null && mapTeacherPersonalInfo.get(teacherPersonalInfo.getTeacherId())==null)
						mapTeacherPersonalInfo.put(teacherPersonalInfo.getTeacherId(), teacherPersonalInfo);
			}
		}
		return mapTeacherPersonalInfo;
	}
	
	
	public List<StatusSpecificScore> getFinalizeSSSList(List<StatusSpecificScore> lstStatusSpecificScore)
	{
		List<StatusSpecificScore> lstStatusSpecificScore_Finalize=new ArrayList<StatusSpecificScore>();
		if(lstStatusSpecificScore!=null && lstStatusSpecificScore.size() > 0)
		{
			for(StatusSpecificScore score :lstStatusSpecificScore)
			{
				if(score!=null && score.getFinalizeStatus()==1)
				{
					lstStatusSpecificScore_Finalize.add(score);
				}
			}
		}
		
		return lstStatusSpecificScore_Finalize;
		
	}

	
	public Map<Integer, Boolean> getMapSSS(List<StatusSpecificScore> scoreslst)
	{
		Map<Integer, Boolean> mapSSS=new HashMap<Integer, Boolean>();
		if(scoreslst!=null && scoreslst.size() > 0)
		{
			for(StatusSpecificScore score:scoreslst)
			{
				if(score!=null && score.getStatusSpecificQuestions()!=null && mapSSS.get(score.getStatusSpecificQuestions().getQuestionId())==null)
				{
					mapSSS.put(score.getStatusSpecificQuestions().getQuestionId(), false);
				}
			}
		}
		return mapSSS;
	}
	
	public Map<Integer, StatusSpecificScore> getMapSSSDetails(List<StatusSpecificScore> scoreslst)
	{
		Map<Integer, StatusSpecificScore> mapSSS=new HashMap<Integer, StatusSpecificScore>();
		if(scoreslst!=null && scoreslst.size() > 0)
		{
			for(StatusSpecificScore score:scoreslst)
			{
				if(score!=null && score.getStatusSpecificQuestions()!=null && mapSSS.get(score.getStatusSpecificQuestions().getQuestionId())==null)
				{
					mapSSS.put(score.getStatusSpecificQuestions().getQuestionId(), score);
				}
			}
		}
		return mapSSS;
	}
	
	
	public List<TeacherDetail> getHWRDTeacherList(List<JobForTeacher> jftobj)
	{
		List<TeacherDetail> lstTeacherDetails=new ArrayList<TeacherDetail>();
		if(jftobj!=null && jftobj.size() > 0)
		{
			for(JobForTeacher forTeacher:jftobj)
			{
				if(forTeacher!=null)
				{
					if(forTeacher.getStatus()!=null)
					{
						String sStatusShortName=forTeacher.getStatus().getStatusShortName();
						if(sStatusShortName!=null && !sStatusShortName.equals("hird") && ( sStatusShortName.equals("widrw") || sStatusShortName.equals("rem") || sStatusShortName.equals("dcln")))
						{
							if(lstTeacherDetails.contains(forTeacher.getTeacherId())==true)
							{
								PrintOnConsole.debugPrintlnCGMass("No Already WithDrawn "+forTeacher.getTeacherId().getTeacherId()+" "+forTeacher.getTeacherId().getEmailAddress());
							}
							else
							{
								lstTeacherDetails.add(forTeacher.getTeacherId());
								PrintOnConsole.debugPrintlnCGMass("Already WithDrawn "+forTeacher.getTeacherId().getTeacherId()+" "+forTeacher.getTeacherId().getEmailAddress());
							}
						}
						
					}
				}
			}
		}
		return lstTeacherDetails;
	}
	
	
	public Map<Long, TeacherStatusNotes> getMapTSN(List<TeacherStatusNotes> lstTempTSN)
	{
		Map<Long, TeacherStatusNotes> mapReturn=new HashMap<Long, TeacherStatusNotes>();
		
		if(lstTempTSN!=null && lstTempTSN.size()>0)
		{
			for(TeacherStatusNotes teacherStatusNotes:lstTempTSN)
			{
				if(mapReturn.get(teacherStatusNotes.getTeacherAssessmentQuestionId())==null)
				{
					mapReturn.put(teacherStatusNotes.getTeacherAssessmentQuestionId(), teacherStatusNotes);
				}
			}
		}
		
		return mapReturn;
	}
	
	//added 
	public void mailSchoolSelection(StatusMaster statusMaster,SecondaryStatus secondaryStatus,JobOrder jobOrder,List<TeacherDetail> teacherDetailLst,UserMaster userMaster,HttpServletRequest request){
			System.out.println("::::::::::::::::::::::::::::::::::::::::::::::;;:::::::::::: Mass mailToTeacher :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
			
			try{
				StatusMaster statusMasterCC = null;
				SecondaryStatus secondaryStatusCC = null;
				boolean ccPrivilege = false;
				JobForTeacher jobForTeacher = null;
				List<JobForTeacher> jobForTeachers = new ArrayList<JobForTeacher>();
				Map<Integer, JobForTeacher> jobForTeachersMap = new LinkedHashMap<Integer, JobForTeacher>();
					try {
			System.out.println(" teacherDetailLst :: "+teacherDetailLst.size()+" jobOrder :: "+jobOrder.getJobId());
						for (JobForTeacher jft : jobForTeacherDAO.getJFTByTIDsAndJID(teacherDetailLst, jobOrder))
							jobForTeachersMap.put(jft.getTeacherId().getTeacherId(), jft);
			
						DistrictMaster districtMaster = jobOrder.getDistrictMaster();
					//	System.out.println("jobOrder.getDistrictMaster().getDistrictId()==="+jobOrder.getDistrictMaster().getDistrictId());
					//	System.out.println("districtMaster.getStatusMasterForCC()=="+jobOrder.getDistrictMaster().getStatusMasterForCC()+" districtMaster.getSecondaryStatusForCC()=="+jobOrder.getDistrictMaster().getSecondaryStatusForCC());
						if (jobOrder.getDistrictMaster().getStatusMasterForCC() != null && jobOrder.getDistrictMaster().getSecondaryStatusForCC() == null) {
							statusMasterCC = districtMaster.getStatusMasterForCC();
							ccPrivilege = true;
						} else if (districtMaster.getStatusMasterForCC() == null && districtMaster.getSecondaryStatusForCC() != null) {
							secondaryStatusCC = districtMaster.getSecondaryStatusForCC();
							ccPrivilege = true;
			
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				System.out.println(":::" + statusMaster + " :" + statusMasterCC + "::" + secondaryStatus + ":::" + secondaryStatusCC + "::::"+ jobOrder.getJobId() + "::::" + userMaster.getUserId()+":::::"+ccPrivilege);
				if (ccPrivilege) {
					SessionFactory factory = jobForTeacherDAO.getSessionFactory();
					StatelessSession statelessSession = factory.openStatelessSession();
					Transaction transaction = statelessSession.beginTransaction();
					boolean mailSend = false;
					SecondaryStatus secondaryStatusObj = secondaryStatusDAO.findSecondaryStatusByJobOrder(jobOrder, "School Selection");
					if (secondaryStatusObj != null) {
						System.out.println("secondaryStatusObj========"	+ secondaryStatusObj.getSecondaryStatusName());
					}
					if (secondaryStatusObj != null) {
						if (statusMasterCC != null) {
							//System.out.println("::::::::::::statusMasterCC:::"	+ statusMasterCC.getStatusId());
							//System.out.println("::::::::::::statusMaster:::"+ statusMaster.getStatusId());
							if (statusMasterCC.getStatusId().equals(statusMaster.getStatusId())){
								mailSend = true;
							}
						} else if (secondaryStatusCC != null) {
							System.out.println("secondaryStatusCC:::::"	+ secondaryStatusCC.getSecondaryStatusName());
							System.out.println("secondaryStatus:::::" 	+ secondaryStatus.getSecondaryStatusName());
							if (secondaryStatusCC.getSecondaryStatusName().equalsIgnoreCase(secondaryStatus.getSecondaryStatusName())) {
								mailSend = true;
							}
						}
					}
					
					System.out.println("Before mailSend:::::::::::::::::::::::::::::::::::"+mailSend);
					if(mailSend  && jobOrder!=null && jobOrder.getJobCategoryMaster()!=null && jobOrder.getJobCategoryMaster().getSchoolSelection()!=null )
						if(!jobOrder.getJobCategoryMaster().getSchoolSelection())
							mailSend=false;
					
					System.out.println("After mailSend:::::::::::::::::::::::::::::::::::"+mailSend);
					if (mailSend) {
						for (TeacherDetail teacherDetail : teacherDetailLst) {
							try {
								jobForTeacher = jobForTeachersMap.get(teacherDetail.getTeacherId());
								jobForTeacher.setCandidateConsideration(false);
								statelessSession.update(jobForTeacher);
							} catch (Exception e) {
								e.printStackTrace();
							}
							SchoolSelectionThread dsmt = new SchoolSelectionThread();
							dsmt.setSchoolSelectionAjax(schoolSelectionAjax);
							dsmt.setTeacherDetail(teacherDetail);
							dsmt.setJobOrder(jobOrder);
							dsmt.setUserMaster(userMaster);
							dsmt.setRequest(request);
							try {
								dsmt.start();
							} catch (Exception e) {
							}
						}
					}
				transaction.commit();
				statelessSession.close();
			}
			}catch(Exception e){
				e.printStackTrace();
			}
	}
	//added by 08-04-2015
	public void sendMultipleOnlineActivityLink(String teacherIds,Integer jobId,Integer questionSetId){
		System.out.println(":::::::::::::=sendOnlineActivityLink::::::"+questionSetId);
		WebContext context;
		context = WebContextFactory.get();
		HttpServletRequest request = context.getHttpServletRequest();
		HttpSession session1 = request.getSession(false);
		if(session1 == null ||(session1.getAttribute("teacherDetail")==null && session1.getAttribute("userMaster")==null)) {
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
		}
		
		String[] allTD=teacherIds.split(",");
		Integer[] allTeacherIds=new Integer[allTD.length];
		for(int index=0;index<allTD.length;index++){
			allTeacherIds[index]=Integer.parseInt(allTD[index]);
		}
		List<TeacherDetail> listTD=teacherDetailDAO.findByCriteria(Restrictions.in("teacherId", allTeacherIds));

		JobOrder jobOrder=jobOrderDAO.findById(jobId, false, false);
		OnlineActivityQuestionSet onlineActivityQuestionSet=onlineActivityQuestionSetDAO.findById(questionSetId, false, false);
		List<OnlineActivityStatus> listOAS=onlineActivityStatusDAO.findByCriteria(Restrictions.in("teacherDetail", listTD),Restrictions.eq("districtMaster", jobOrder.getDistrictMaster()),Restrictions.eq("jobCategoryMaster", onlineActivityQuestionSet.getJobCategoryMaster()),Restrictions.eq("onlineActivityQuestionSet", onlineActivityQuestionSet));
		Map<Integer,OnlineActivityStatus> teacherStatusSend=new LinkedHashMap<Integer, OnlineActivityStatus>();
		for(OnlineActivityStatus oAS:listOAS)
			teacherStatusSend.put(oAS.getTeacherDetail().getTeacherId(), oAS);
		//TeacherDetail teacherDetail=teacherDetailDAO.findById(teacherId, false, false);
		
		
		for(TeacherDetail td:listTD){
			System.out.println("Teacher id=="+td.getTeacherId()+" setId=="+onlineActivityQuestionSet.getQuestionSetId() );
			boolean isSendMail=false;
		if(teacherStatusSend.get(td.getTeacherId())==null){
			OnlineActivityStatus status=new OnlineActivityStatus();
			status.setTeacherDetail(td);
			status.setDistrictMaster(jobOrder.getDistrictMaster());
			status.setJobCategoryMaster(jobOrder.getJobCategoryMaster());
			status.setOnlineActivityQuestionSet(onlineActivityQuestionSet);
			status.setStatus("S");
			status.setActivityStartTime(new Date());
			status.setActivityDate(new Date());
			onlineActivityStatusDAO.makePersistent(status);
			isSendMail=true;
		}
		if(teacherStatusSend.get(td.getTeacherId())!=null && !teacherStatusSend.get(td.getTeacherId()).getStatus().trim().equalsIgnoreCase("C")){
			isSendMail=true;
		}
		if(isSendMail){
		MailSendSchedulerThread dsmt = new MailSendSchedulerThread();
		dsmt.setQuestionSetId(questionSetId);
		dsmt.setOnlineActivityAjax(onlineActivityAjax);
		dsmt.setRequest(request);
		dsmt.setJobId(jobId);
		dsmt.setOnlineActivityFlag(0);
		dsmt.setTeacherId(td.getTeacherId());
		dsmt.setDistrictId(jobOrder.getDistrictMaster().getDistrictId());
		try {
			dsmt.start();	
		} catch (Exception e) {}
		}
		}
	}
	//ended by 08-04-2015
	
	public String getOnlyCredentialReview(String teacherIds, String jobId, String nodeName, Integer districtId){
		System.out.println("<<<<<<<<<<<<<<<<<<<::::::::::::::getOnlyCredentialReview:::::::::::::>>>>>>>>>>>>>>>>>>>");
		StringBuffer sb=new StringBuffer();
		try{
		String[] teacherIdString=teacherIds.split(",");
		List<Integer> teacherIdList=new ArrayList<Integer>();
		for(String str:teacherIdString)
			teacherIdList.add(Integer.parseInt(str));
		DistrictMaster districtMaster= districtMasterDAO.findById(districtId, false, false);
		List<TeacherDetail> lstTeacherDetail=teacherDetailDAO.findByCriteria(Restrictions.in("teacherId", teacherIdList));
		JobOrder jobOrder=jobOrderDAO.findById(Integer.parseInt(jobId), false, false);
		List<SecondaryStatus> lstSS=secondaryStatusDAO.findByCriteria(Restrictions.eq("districtMaster", districtMaster),Restrictions.eq("secondaryStatusName", nodeName));
		System.out.println("seoncaty===");
		List<TeacherStatusHistoryForJob> historyList=teacherStatusHistoryForJobDAO.findByCriteria(Restrictions.in("teacherDetail", lstTeacherDetail),Restrictions.eq("jobOrder", jobOrder),Restrictions.in("secondaryStatus", lstSS));
		for(TeacherStatusHistoryForJob tSHFJ:historyList)
			if(!sb.toString().trim().equalsIgnoreCase(""))
				sb.append(","+tSHFJ.getTeacherDetail().getTeacherId());
				else
				sb.append(tSHFJ.getTeacherDetail().getTeacherId());	
		
		System.out.println("teacherId===================="+sb.toString());
		}catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
}


