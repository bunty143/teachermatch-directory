package tm.controller.district;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.hibernate.SessionFactory;
import org.hibernate.StatelessSession;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tm.bean.StatusWiseAutoEmailSend;
import tm.bean.cgreport.DistrictMaxFitScore;
import tm.bean.hqbranchesmaster.BranchMaster;
import tm.bean.hqbranchesmaster.HeadQuarterMaster;
import tm.bean.i4.I4QuestionSets;
import tm.bean.master.ApplitrackDistricts;
import tm.bean.master.CityMaster;
import tm.bean.master.ContactTypeMaster;
import tm.bean.master.DistrictMaster;
import tm.bean.master.ExclusivePeriodMaster;
import tm.bean.master.JobCategoryMaster;
import tm.bean.master.QqQuestionSets;
import tm.bean.master.SchoolMaster;
import tm.bean.master.SecondaryStatus;
import tm.bean.master.SkillAttributesMaster;
import tm.bean.master.StateMaster;
import tm.bean.master.StatusMaster;
import tm.bean.master.StatusNodeMaster;
import tm.bean.master.StatusPrivilegeForSchools;
import tm.bean.user.UserMaster;
import tm.dao.ApplitrackDistrictsDAO;
import tm.dao.JobOrderDAO;
import tm.dao.StatusWiseAutoEmailSendDAO;
import tm.dao.UserLoginHistoryDAO;
import tm.dao.cgreport.DistrictMaxFitScoreDAO;
import tm.dao.districtassessment.DistrictAssessmentDetailDAO;
import tm.dao.hqbranchesmaster.BranchMasterDAO;
import tm.dao.hqbranchesmaster.HeadQuarterMasterDAO;
import tm.dao.i4.I4QuestionSetsDAO;
import tm.dao.master.CityMasterDAO;
import tm.dao.master.ContactTypeMasterDAO;
import tm.dao.master.DistrictMasterDAO;
import tm.dao.master.DistrictSchoolsDAO;
import tm.dao.master.ExclusivePeriodMasterDAO;
import tm.dao.master.JobCategoryMasterDAO;
import tm.dao.master.SchoolMasterDAO;
import tm.dao.master.SecondaryStatusDAO;
import tm.dao.master.SkillAttributesMasterDAO;
import tm.dao.master.StateMasterDAO;
import tm.dao.master.StatusMasterDAO;
import tm.dao.master.StatusNodeMasterDAO;
import tm.dao.master.StatusPrivilegeForSchoolsDAO;
import tm.dao.master.TimeZoneMasterDAO;
import tm.dao.menu.RoleAccessPermissionDAO;
import tm.services.EmailerService;
import tm.services.MD5Encryption;
import tm.services.MailText;
import tm.services.teacher.DashboardAjax;
import tm.servlet.WorkThreadServlet;
import tm.utility.IPAddressUtility;
import tm.utility.ImageResize;
import tm.utility.Utility;

@Controller
public class DistrictManageController 
{
	
	String locale = Utility.getValueOfPropByKey("locale");

	 
	@Autowired
	private StatusWiseAutoEmailSendDAO statusWiseAutoEmailSendDAO;
	
	@Autowired
	private SecondaryStatusDAO secondaryStatusDAO;
	
	@Autowired
	private StatusNodeMasterDAO statusNodeMasterDAO;
	
	@Autowired
	private JobCategoryMasterDAO jobCategoryMasterDAO;
	
	@Autowired
	private JobOrderDAO jobOrderDAO;
	public void setJobOrderDAO(JobOrderDAO jobOrderDAO) {
		this.jobOrderDAO = jobOrderDAO;
	}
	@Autowired
	private DistrictMasterDAO districtMasterDAO;
	public void setDistrictMasterDAO(DistrictMasterDAO districtMasterDAO) 
	{
		this.districtMasterDAO = districtMasterDAO;
	}
		
	@Autowired
	private StateMasterDAO stateMasterDAO;
	public void setStateMasterDAO(StateMasterDAO stateMasterDAO) {
		this.stateMasterDAO = stateMasterDAO;
	}
	
	@Autowired
	private CityMasterDAO cityMasterDAO;
	public void setCityMasterDAO(CityMasterDAO cityMasterDAO) {
		this.cityMasterDAO = cityMasterDAO;
	}
	
	@Autowired 
	private ContactTypeMasterDAO contactTypeMasterDAO;
	public void setContactTypeMasterDAO(ContactTypeMasterDAO contactTypeMasterDAO) {
		this.contactTypeMasterDAO = contactTypeMasterDAO;
	}
	
	@Autowired
	private SchoolMasterDAO schoolMasterDAO;
	public void setSchoolMasterDAO(SchoolMasterDAO schoolMasterDAO) {
		this.schoolMasterDAO = schoolMasterDAO;
	}
	@Autowired
	private DistrictSchoolsDAO districtSchoolsDAO;
	public void setDistrictSchoolsDAO(DistrictSchoolsDAO districtSchoolsDAO) {
		this.districtSchoolsDAO = districtSchoolsDAO;
	}
	@Autowired
	private RoleAccessPermissionDAO roleAccessPermissionDAO;
	public void setRoleAccessPermissionDAO(RoleAccessPermissionDAO roleAccessPermissionDAO) {
		this.roleAccessPermissionDAO = roleAccessPermissionDAO;
	}
	@Autowired
	private EmailerService emailerService;
	public void setEmailerService(EmailerService emailerService) 
	{
		this.emailerService = emailerService;
	}
	@Autowired
	private UserLoginHistoryDAO userLoginHistoryDAO;
	public void setUserLoginHistoryDAO(UserLoginHistoryDAO userLoginHistoryDAO) {
		this.userLoginHistoryDAO = userLoginHistoryDAO;
	}
	@Autowired
	private ExclusivePeriodMasterDAO exclusivePeriodMasterDAO;
	public void setExclusivePeriodMasterDAO(ExclusivePeriodMasterDAO exclusivePeriodMasterDAO)
	{
		this.exclusivePeriodMasterDAO = exclusivePeriodMasterDAO;
	}
	
	@Autowired
	private StatusMasterDAO statusMasterDAO;
	public void setStatusMasterDAO(StatusMasterDAO statusMasterDAO) {
		this.statusMasterDAO = statusMasterDAO;
	}
	
	@Autowired
	private DistrictMaxFitScoreDAO districtMaxFitScoreDAO;
	public void setDistrictMaxFitScoreDAO(
			DistrictMaxFitScoreDAO districtMaxFitScoreDAO) {
		this.districtMaxFitScoreDAO = districtMaxFitScoreDAO;
	}
	
	@Autowired
	private StatusPrivilegeForSchoolsDAO statusPrivilegeForSchoolsDAO;
	public void setStatusPrivilegeForSchoolsDAO(
			StatusPrivilegeForSchoolsDAO statusPrivilegeForSchoolsDAO) {
		this.statusPrivilegeForSchoolsDAO = statusPrivilegeForSchoolsDAO;
	}
	
	@Autowired
	private SkillAttributesMasterDAO skillAttributesMasterDAO;
	public void setSkillAttributesMasterDAO(
			SkillAttributesMasterDAO skillAttributesMasterDAO) {
		this.skillAttributesMasterDAO = skillAttributesMasterDAO;
	}
	
	@Autowired
	private I4QuestionSetsDAO i4QuestionSetsDAO;
	
	@Autowired
	private DistrictAssessmentDetailDAO districtAssessmentDetailDAO;
	
	@Autowired
	private HeadQuarterMasterDAO headQuarterMasterDAO;
	@Autowired
	private BranchMasterDAO branchMasterDAO;
	
	@Autowired
	private ApplitrackDistrictsDAO applitrackDistrictsDAO;
	
	//timezone field in edit district account information page start
	@Autowired
	private TimeZoneMasterDAO timeZoneMasterDAO;
	public void setTimeZoneMasterDAO(TimeZoneMasterDAO timeZoneMasterDAO) {
		this.timeZoneMasterDAO = timeZoneMasterDAO;
	}
	//timezone field in edit district account information page end
	
	@RequestMapping(value="/managedistrict.do", method=RequestMethod.GET)
	public String districtGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster	userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			session.setAttribute("displayType","0");
			managedistrictMethod(map,request,roleAccessPermissionDAO,userLoginHistoryDAO);
			
/*String authKeyVal = request.getParameter("authKeyVal")==null?"0":request.getParameter("authKeyVal").trim();
			
			String roleAccess=null;
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,5,"managedistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("authKeyVal", authKeyVal);
			try{
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in manage district");
			}catch(Exception e){
				e.printStackTrace();
			}*/
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "managedistrict";
	}
	/* @Author: Gagan 
	 * @Discription: editdistrict.do Controller.
	 */	
	@RequestMapping(value="/editdistrict.do", method=RequestMethod.GET)
	public String doEditDistrictGET(ModelMap map,HttpServletRequest request)
	{
		try 
		{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		
		UserMaster userSession									=	(UserMaster) session.getAttribute("userMaster");
		List<UserMaster> districtsUser							=	null;
		DistrictMaster districtMaster							=	null;
		String districtId = request.getParameter("distId")		==	null?"0":request.getParameter("distId").trim();
		if(districtId.equals("0"))
		{
			districtMaster										=	new DistrictMaster();
		}
		else
		{
			int iDistrictId=Utility.getIntValue(districtId);
			if(iDistrictId >0)
				districtMaster=districtMasterDAO.findById(Utility.decryptNo(Integer.parseInt(districtId)), false, false);
			if(districtMaster==null)
				districtMaster=districtMasterDAO.findById(Integer.parseInt(districtId), false, false);
			
			if(districtMaster!=null)
			{
				ApplitrackDistricts applitrackDistricts = applitrackDistrictsDAO.findByDistrictId(districtMaster);
				if(applitrackDistricts==null)
				{	applitrackDistricts = new ApplitrackDistricts(); 
					applitrackDistricts.setAppDistrictId(0);
				}

				map.put("applitrackDistricts", applitrackDistricts);
			}
		}
		
		
		if(userSession.getEntityType()!=null && userSession.getEntityType()>1)
		{
			if(districtMaster!=null && userSession.getDistrictId()!=null && userSession.getDistrictId().getDistrictId().equals(districtMaster.getDistrictId()))
			{
				//TODO
			}
			else
			{
				return "redirect:editdistrict.do?distId="+Utility.encryptNo(userSession.getDistrictId().getDistrictId());
			}
				
		}
			if(districtMaster.getCanTMApproach()!=null && districtMaster.getCanTMApproach()!=0){
				districtMaster.setCanTMApproach(1);
			}
		
			String roleAccess=null,roleAccessGI=null,roleAccessCI=null,roleAccessU=null,roleAccessAI=null;
			int roleId=0;
			if(userSession.getRoleId()!=null){
				roleId=userSession.getRoleId().getRoleId();
			}
			try{
				roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,6,"editdistrict.do",0);
				roleAccessGI=roleAccessPermissionDAO.getMenuOptionList(roleId,7,"editdistrict.do",0);
				roleAccessCI=roleAccessPermissionDAO.getMenuOptionList(roleId,8,"editdistrict.do",0);
				roleAccessU=roleAccessPermissionDAO.getMenuOptionList(roleId,9,"editdistrict.do",0);
				roleAccessAI=roleAccessPermissionDAO.getMenuOptionList(roleId,10,"editdistrict.do",0);
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("roleAccess", roleAccess);
			map.addAttribute("roleAccessGI", roleAccessGI);
			map.addAttribute("roleAccessCI", roleAccessCI);
			map.addAttribute("roleAccessU", roleAccessU);
			map.addAttribute("roleAccessAI", roleAccessAI);
			System.out.println("dddddddddddddddddddddddddddd: "+timeZoneMasterDAO.getAllTimeZone().size());
			//timezone field in edit district account information page start
			map.addAttribute("timezone",timeZoneMasterDAO.getAllTimeZone());
			//timezone field in edit district account information page end
		
		// =============== for Getting the All database Field Values from database ====================
			List<ContactTypeMaster> contactTypeMaster			=	contactTypeMasterDAO.findByCriteria(Order.asc("contactType"));
			List<StateMaster> listStateMasters  = stateMasterDAO.findAllStateByOrder();
			List<CityMaster> listCityMasters	= cityMasterDAO.findCityByState(districtMaster.getStateId());
			String authKey=null;
			if(districtMaster.getAuthKey()!=null){
				authKey=Utility.decodeBase64(districtMaster.getAuthKey());
			}
			if(districtMaster.getExclusivePeriod()==null || districtMaster.getExclusivePeriod()==0)
			{
				List<ExclusivePeriodMaster> lstExclusivePeriodMaster = exclusivePeriodMasterDAO.findAll();
				ExclusivePeriodMaster exclusivePeriodMaster = lstExclusivePeriodMaster.get(0);
				districtMaster.setExclusivePeriod(exclusivePeriodMaster.getExclPeriod());
			}
			
			/*============ Gagan: Status with slider Functionality =======================  */
			List<StatusMaster> lstStatusMaster	=	null;
			List<StatusMaster> statusMasterList	=new ArrayList<StatusMaster>();
			List<SecondaryStatus> secondaryStatusList	=new ArrayList<SecondaryStatus>();
			
			List<SecondaryStatus> lstSecStatus=	secondaryStatusDAO.findSecondaryStatusOnlyStatus(districtMaster);
			Map<Integer,SecondaryStatus> mapSStatus = new HashMap<Integer, SecondaryStatus>();
			if(lstSecStatus!=null)
			for(SecondaryStatus secStatus : lstSecStatus){
				mapSStatus.put(secStatus.getStatusMaster().getStatusId(),secStatus);
			}
			List<DistrictMaxFitScore> lstDistrictMaxFitScore	=	districtMaxFitScoreDAO.getFitStatusScore(districtMaster);
			List<SecondaryStatus> lstsecondaryStatus=  secondaryStatusDAO.findSecondaryStatusOnly(districtMaster);
			List<StatusMaster> lstStatusMasterDPoints	= new ArrayList<StatusMaster>();//	statusMasterDAO.findStatusByStatusByShortNames(statusForDPoints);
			List<SecondaryStatus> lstSecStatusForDPoints=	secondaryStatusDAO.findSecondaryStatusOnlyStatusForDPoints(districtMaster);
			if(lstSecStatusForDPoints.size()>0)
			for(SecondaryStatus sec:lstSecStatusForDPoints){
				if(sec.getStatusMaster()!=null)
				if(sec.getStatusMaster().getStatusShortName().equalsIgnoreCase("scomp")|| sec.getStatusMaster().getStatusShortName().equalsIgnoreCase("ecomp")|| sec.getStatusMaster().getStatusShortName().equalsIgnoreCase("vcomp")){
					StatusMaster status=new StatusMaster();
					status.setStatusId(sec.getStatusMaster().getStatusId());
					status.setStatus(sec.getSecondaryStatusName());
					lstStatusMasterDPoints.add(status);
				}
			}
			map.addAttribute("lstStatusMasterDPoints", lstStatusMasterDPoints);
			
			String[] statuss = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			String baseUrl=Utility.getBaseURL(request);
			/*if(baseUrl.contains("nccloud") || baseUrl.contains("nc.teachermatch"))
			{
				statuss=new String[1];
				statuss[0] = "rem";
			}*/
				
			lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statuss);
			map.addAttribute("displayRecordNewRecords",0);
			if(lstDistrictMaxFitScore.size()==0){
				SecondaryStatus secondaryStatus=null;
				for(StatusMaster sm: lstStatusMaster){
					if(sm.getBanchmarkStatus()==1){
						secondaryStatus=mapSStatus.get(sm.getStatusId());
						if(secondaryStatus!=null){
							sm.setStatus(secondaryStatus.getSecondaryStatusName());
						}
					}
					statusMasterList.add(sm);
				}
				map.addAttribute("displayRecordFlagFromDistrictMaxFitScoreTable",0);
				map.addAttribute("lstStatusMaster", statusMasterList);
				map.addAttribute("lstsecondaryStatus", lstsecondaryStatus);
			}else{
				List<DistrictMaxFitScore> lstStatusDistrictMaxFitScore	=	districtMaxFitScoreDAO.getStatusMasterList(districtMaster);
				List<DistrictMaxFitScore> lstSecStatusDistrictMaxFitScore	=	districtMaxFitScoreDAO.getSecStatusMasterList(districtMaster);
				SecondaryStatus secStatusObj=new SecondaryStatus();
				List secStatusIds = new ArrayList();
				for(DistrictMaxFitScore dmax: lstSecStatusDistrictMaxFitScore){
					secStatusIds.add(dmax.getSecondaryStatus().getSecondaryStatusId());
					secStatusObj=new SecondaryStatus();
					secStatusObj.setMaxFitScore(dmax.getMaxFitScore());
					if(dmax.getSecondaryStatus()!=null){
						secStatusObj.setSecondaryStatusId(dmax.getSecondaryStatus().getSecondaryStatusId());
						secStatusObj.setSecondaryStatusName(dmax.getSecondaryStatus().getSecondaryStatusName());
					}
					secondaryStatusList.add(secStatusObj);
				}
				List<SecondaryStatus> lstsecondaryStatusNew=  secondaryStatusDAO.findSecondaryStatusOnlyWithSecStatusIds(districtMaster,secStatusIds);
				if(lstsecondaryStatusNew!=null && lstsecondaryStatusNew.size()>0){
					for(SecondaryStatus secObj: lstsecondaryStatusNew){
						secStatusObj=new SecondaryStatus();
						secStatusObj.setMaxFitScore(100.0);
						secStatusObj.setSecondaryStatusId(secObj.getSecondaryStatusId());
						secStatusObj.setSecondaryStatusName(secObj.getSecondaryStatusName());
						secondaryStatusList.add(secStatusObj);
					}
				}
				map.addAttribute("displayRecordFlagFromDistrictMaxFitScoreTable",1);
				SecondaryStatus secondaryStatus=null;
				StatusMaster statusObj=new StatusMaster();
				for(DistrictMaxFitScore dmax: lstStatusDistrictMaxFitScore){
					statusObj=new StatusMaster();
					statusObj.setMaxFitScore(dmax.getMaxFitScore());
					if(dmax.getStatusMaster()!=null){
						statusObj.setStatusId(dmax.getStatusMaster().getStatusId());
						secondaryStatus=mapSStatus.get(dmax.getStatusMaster().getStatusId());
						if(secondaryStatus!=null){
							statusObj.setStatus(secondaryStatus.getSecondaryStatusName());
						}else{
							statusObj.setStatus(dmax.getStatusMaster().getStatus());
						}
					}else{
						statusObj.setStatusId(dmax.getSecondaryStatus().getSecondaryStatusId());
						statusObj.setStatus(dmax.getSecondaryStatus().getSecondaryStatusName());
					}
					/*if(baseUrl.contains("nccloud") || baseUrl.contains("nc.teachermatch") || baseUrl.contains("localhost"))
					{
						
						if(statusObj!=null && statusObj.getStatus()!=null 
								&& statusObj.getStatus().equals("Rejected"))
							statusMasterList.add(statusObj);
					}
					else*/
						statusMasterList.add(statusObj);
				}
				map.addAttribute("lstStatusMaster", statusMasterList);
				map.addAttribute("lstsecondaryStatus", secondaryStatusList);
			}
			
			/*========= If Decision Maker Password is available then showing it Blank*/
			map.addAttribute("authKey", authKey);
			map.addAttribute("districtMaster", districtMaster);
			map.addAttribute("listStateMasters", listStateMasters);
			map.addAttribute("listCityMasters", listCityMasters);	
			map.addAttribute("userSession", userSession);
			map.addAttribute("contactTypeMaster", contactTypeMaster);
			
			if(districtMaster.getInitiatedOnDate()==null){
				map.addAttribute("initiatedOnDate",null);
			}else{
				map.addAttribute("initiatedOnDate", Utility.getCalenderDateFormart(districtMaster.getInitiatedOnDate()+""));
			}
			if(districtMaster.getContractStartDate()==null){
				map.addAttribute("contractStartDate",null);
			}else{
				map.addAttribute("contractStartDate", Utility.getCalenderDateFormart(districtMaster.getContractStartDate()+""));
			}
			
			if(districtMaster.getContractEndDate()==null){
				map.addAttribute("contractEndDate",null);
			}else{
				map.addAttribute("contractEndDate", Utility.getCalenderDateFormart(districtMaster.getContractEndDate()+""));
			}
			int jobOrderSize=0;
			jobOrderSize=jobOrderDAO.checkDefaultDistrictAndSchool(districtMaster,null,1);
			if(jobOrderSize==1){
				map.addAttribute("aJobRelation","disabled=\"disabled\"");
			}else{
				map.addAttribute("aJobRelation",null);
			}
			String windowFunc="if(this.href!='javascript:void(0)')window.open(this.href, 'mywin','left=200,top=50,width=700,height=600,toolbar=1,resizable=0'); return false;";
			map.addAttribute("windowFunc",windowFunc);
			String JobBoardURL=null;
			try{
				if(districtMaster.getDistrictId().equals(1200390) || districtMaster.getDistrictId().equals(7800043)){
					JobBoardURL =Utility.getValueOfPropByKey("basePath")+"jobsboard.do?districtId="+Utility.encryptNo(districtMaster.getDistrictId());
				} else {
					JobBoardURL=Utility.getShortURL(Utility.getBaseURL(request)+"jobsboard.do?districtId="+Utility.encryptNo(districtMaster.getDistrictId()));
				}

			}catch (Exception e) {
				// TODO: handle exception
			}
			map.addAttribute("JobBoardURL",JobBoardURL);
			map.addAttribute("approvalBeforeGoLive1",districtMaster.getApprovalBeforeGoLive());
			
			String InternalTransferURL=null;
			try{
				if(districtMaster.getDistrictId().equals(1200390) || districtMaster.getDistrictId().equals(7800043)){
					InternalTransferURL=Utility.getValueOfPropByKey("basePath")+"internaltransfer.do?districtId="+Utility.encryptNo(districtMaster.getDistrictId());
				} else {
					InternalTransferURL=Utility.getShortURL(Utility.getBaseURL(request)+"internaltransfer.do?districtId="+Utility.encryptNo(districtMaster.getDistrictId()));
				}
			}catch (Exception e) {
				// TODO: handle exception
			}
			map.addAttribute("InternalTransferURL",InternalTransferURL);
			
			try{
				userSession.setDistrictId(districtMaster);
				userLoginHistoryDAO.insertUserLoginHistory(userSession,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in edit district");
			}catch(Exception e){
				e.printStackTrace();
			}
			map.addAttribute("scompFitScore", Utility.getValueOfPropByKey("scompFitScore"));
			map.addAttribute("ecompFitScore", Utility.getValueOfPropByKey("ecompFitScore"));
			map.addAttribute("vcompFitScore", Utility.getValueOfPropByKey("vcompFitScore"));
			map.addAttribute("defaultFitScore", Utility.getValueOfPropByKey("defaultFitScore"));
			
			
			//*************** Privilege For School(S) ********************
			
			// delete Inactive 
			List<SecondaryStatus> lstsecondaryStatusForInactive=  secondaryStatusDAO.findSecondaryStatusForInactive(districtMaster);
			if(lstsecondaryStatusForInactive!=null && lstsecondaryStatusForInactive.size()>0){
				List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchoolForDelete=statusPrivilegeForSchoolsDAO.findSecondaryStatusIdBySecondaryStatusList(districtMaster, lstsecondaryStatusForInactive);
				int deletedCount=statusPrivilegeForSchoolsDAO.deleteData(lstStatusPrivilegeForSchoolForDelete);
			}
			// End delete inactive 
			
			String statusListBoxValue="";
			
			if(districtMaster.getStatusMaster()!=null)
				statusListBoxValue=""+districtMaster.getStatusMaster().getStatusId();
			else if(districtMaster.getSecondaryStatus()!=null)
				statusListBoxValue="SSID_"+districtMaster.getSecondaryStatus().getSecondaryStatusId();
			
			
			// For VVI
			try
			{
				String statusListBoxValueForVVI="";
				String quesSetIDValue= "";
				String quesSetNameValue= "";
				if(districtMaster.getStatusMasterForVVI()!=null)
					statusListBoxValueForVVI=""+districtMaster.getStatusMasterForVVI().getStatusId();
				else if(districtMaster.getSecondaryStatusForVVI()!=null)
				{
					System.out.println("second");
					statusListBoxValueForVVI="SSIDForVVI_"+districtMaster.getSecondaryStatusForVVI().getSecondaryStatusId();
				}
					
				
				if(districtMaster.getI4QuestionSets()!=null)
				{
					quesSetIDValue=""+districtMaster.getI4QuestionSets().getID();
					quesSetNameValue=districtMaster.getI4QuestionSets().getQuestionSetText();
				}
					System.out.println(" LLLLLLLLLLLLLLL statusListBoxValueForVVI :: "+statusListBoxValueForVVI);
				
				map.addAttribute("statusListBoxValueForVVI", statusListBoxValueForVVI);
				map.addAttribute("quesSetIDValue", quesSetIDValue);
				map.addAttribute("quesSetNameValue", quesSetNameValue);
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			//////////////////////////////// District Assessment ////////////////////////////////////
			/*try
			{
				String distASMTId = "";
				String distASMTVlaue = "";
				
				if(districtMaster.getDistrictAssessmentId()!=null && !districtMaster.getDistrictAssessmentId().equals("") && districtMaster.getDistrictAssessmentId()>0)
				{
					DistrictAssessmentDetail districtAssessmentDetail = districtAssessmentDetailDAO.findById(districtMaster.getDistrictAssessmentId(), false, false);
					
					if(districtAssessmentDetail!=null)
					{
						distASMTId = districtAssessmentDetail.getDistrictAssessmentId().toString();
						distASMTVlaue = districtAssessmentDetail.getDistrictAssessmentName();
					}
				
				}
				
				
				map.addAttribute("distASMTId", distASMTId);
				map.addAttribute("distASMTVlaue", distASMTVlaue);
			}catch(Exception e)
			{e.printStackTrace();}*/
			
			
			
			//For CC
			try
			{
				
				String statusListBoxValueForCC="";
				
				if(districtMaster.getStatusMasterForCC()!=null)
				{
					System.out.println(" First ");
					statusListBoxValueForCC=""+districtMaster.getStatusMasterForCC().getStatusId();
				}
				else if(districtMaster.getSecondaryStatusForCC()!=null)
				{
					System.out.println("second");
					statusListBoxValueForCC=""+districtMaster.getSecondaryStatusForCC().getSecondaryStatusId();
				}
				
				System.out.println(" statusListBoxValueForCC >>>>>>>>> "+statusListBoxValueForCC);
				map.addAttribute("statusListBoxValueForCC", statusListBoxValueForCC);
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			//For Reference
			try
			{
				
				String statusListBoxValueForReminder="";
				
				if(districtMaster.getStatusMasterForReference()!=null)
				{
					System.out.println(" First ");
					statusListBoxValueForReminder=""+districtMaster.getStatusMasterForReference().getStatusId();
				}
				else if(districtMaster.getSecondaryStatusForReference()!=null)
				{
					System.out.println("second");
					statusListBoxValueForReminder=""+districtMaster.getSecondaryStatusForReference().getSecondaryStatusId();
				}
				
				System.out.println(" statusListBoxValueForReminder >>>>>>>>> "+statusListBoxValueForReminder);
				map.addAttribute("statusListBoxValueForReminder", statusListBoxValueForReminder);
				
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			// For Reminder
			String statusIdReminderExpireAction = "";
			Integer noOfReminder = null;
			try{
				if(districtMaster.getStatusIdReminderExpireAction() != null){
					statusIdReminderExpireAction	=	""+districtMaster.getStatusIdReminderExpireAction().getStatusId();
				}
				
				if(districtMaster.getNoOfReminder()!=null){
					noOfReminder = districtMaster.getNoOfReminder();
				}
				
				map.addAttribute("statusIdReminderExpireAction", statusIdReminderExpireAction);
				map.addAttribute("noOfReminder", noOfReminder);
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			
			// Portfolio Reminder
			String statusIdReminderExpireActionPortfolio = "";
			Integer noOfReminderPortfolio = null;
			try{
				if(districtMaster.getStatusIdReminderExpireActionPortfolio() != null){
					statusIdReminderExpireActionPortfolio	=	""+districtMaster.getStatusIdReminderExpireActionPortfolio();
				}
				
				if(districtMaster.getNoOfReminderPortfolio()!=null){
					noOfReminderPortfolio = districtMaster.getNoOfReminderPortfolio();
				}
				
				map.addAttribute("statusIdReminderExpireActionPortfolio", statusIdReminderExpireActionPortfolio);
				map.addAttribute("noOfReminderPortfolio", noOfReminderPortfolio);
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			try{
				String statusSendMailPortfolio = districtMaster.getStatusSendMailPortfolio();
				String secondaryStatusSendMailPortfolio = districtMaster.getSecondaryStatusSendMailPortfolio();
				if(statusSendMailPortfolio!=null && statusSendMailPortfolio!=""){
					String[] parts = statusSendMailPortfolio.split(",");
					List<String> statusList = new ArrayList();
					for(String part : parts){
						statusList.add(part);
					}
					map.addAttribute("statusListPortfolio", statusList);
				}
				
				if(secondaryStatusSendMailPortfolio!=null && secondaryStatusSendMailPortfolio!=""){
					String[] parts = secondaryStatusSendMailPortfolio.split(",");
					List<String> secondaryStatusListReminder = new ArrayList();
					for(String part : parts){
						secondaryStatusListReminder.add(part);
					}
					map.addAttribute("secondaryStatusListPortfolio", secondaryStatusListReminder);
				}
				
			}catch(Exception exception){
				exception.printStackTrace();
			}
			
			statusMasterList	=new ArrayList<StatusMaster>();
			ArrayList<StatusMaster> statusMasterListRem	=new ArrayList<StatusMaster>();
			SecondaryStatus secondaryStatus=null;
			for(StatusMaster sm: lstStatusMaster){
				if(sm.getBanchmarkStatus()==1){
					secondaryStatus=mapSStatus.get(sm.getStatusId());
					if(secondaryStatus!=null){
						sm.setStatus(secondaryStatus.getSecondaryStatusName());
					}
				}
				if(baseUrl.contains("nccloud") || baseUrl.contains("nc.teachermatch")  )
				{
					
					if(sm!=null && sm.getStatus()!=null && sm.getStatus().equals("Rejected"))
						statusMasterListRem.add(sm);
				}
				else
					statusMasterListRem.add(sm);
					
				statusMasterList.add(sm);
			}
			
			map.addAttribute("lstStatusMaster_PFC", statusMasterList);
			map.addAttribute("lstStatusMasterRem_PFC", statusMasterListRem);
			map.addAttribute("lstsecondaryStatus_PFC", lstsecondaryStatus);
			map.addAttribute("statusListBoxValue", statusListBoxValue);
			
			List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchool=statusPrivilegeForSchoolsDAO.findSchoolStatusForStatusMasterId(districtMaster);
			String [] chkStatusMaster=new String[lstStatusPrivilegeForSchool.size()];
			int k=0;
			for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchool)
				chkStatusMaster[k++]=pojo.getStatusMaster().getStatusId().toString();
			districtMaster.setChkStatusMaster(chkStatusMaster);
			
			
			List<Integer> secStatusIds=new ArrayList<Integer>();
			List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchool_forSS=statusPrivilegeForSchoolsDAO.findSchoolStatusForSecondaryStatusId(districtMaster);
			String [] chkSecondaryStatusName=new String[lstStatusPrivilegeForSchool_forSS.size()];
			int j=0;
			for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchool_forSS){
				chkSecondaryStatusName[j++]=pojo.getSecondaryStatus().getSecondaryStatusId().toString();
				secStatusIds.add(pojo.getSecondaryStatus().getSecondaryStatusId());
				
			}
			districtMaster.setChkSecondaryStatusName(chkSecondaryStatusName);
			
			if(districtMaster.getDisplayTMDefaultJobCategory()!=null){
				map.addAttribute("displayTMDefaultJobCategory", districtMaster.getDisplayTMDefaultJobCategory());
			}else{
				map.addAttribute("displayTMDefaultJobCategory",true);
			}
			
			// Mosaic Changes
			if(districtMaster.getDisplayCandidateTOMosaic()!=null){
				map.addAttribute("displayCandidateTOMosaic", districtMaster.getDisplayCandidateTOMosaic());
			}else{
				map.addAttribute("displayCandidateTOMosaic",true);
			}
			
			// Start Assessment Notifications To Candidates Setting
			if(districtMaster.getEpiSetting()!=null && districtMaster.getEpiSetting()!=""){
				map.addAttribute("epiSetting", districtMaster.getEpiSetting());
			}else{
				map.addAttribute("epiSetting","null");
			}
			if(districtMaster.getJsiSetting()!=null && districtMaster.getJsiSetting()!=""){
				map.addAttribute("jsiSetting", districtMaster.getJsiSetting());
			}else{
				map.addAttribute("jsiSetting","null");
			}
			// End Assessment Notifications To Candidates Setting
			
			if(districtMaster.getDisplayCommunication()!=null && districtMaster.getDisplayCommunication()==true){
			
				map.addAttribute("displayCommunication",false);
			}else{
				map.addAttribute("displayCommunication",true);
			}
			if(districtMaster.getSetAssociatedStatusToSetDPoints()!=null){
				map.addAttribute("setAssociatedStatusToSetDPoints", districtMaster.getSetAssociatedStatusToSetDPoints());
			}else{
				map.addAttribute("setAssociatedStatusToSetDPoints",false);
			}
			//map.addAllAttribute("districtMaster",districtMaster.getApprovalBeforeGoLive());
			// Strat ... verify new secondary Status and Checked check box
			/*List<SecondaryStatus> secondaryStatus=secondaryStatusDAO.findSecondaryStatusOnlyWithSecStatusIds(districtMaster, secStatusIds);
			if(secondaryStatus!=null && secondaryStatus.size()>0)
			{
				String [] chkSecondaryStatusName_1=new String[secondaryStatus.size()];
				int h=0;
				if(secondaryStatus.size()>0)
					for(SecondaryStatus pojo:secondaryStatus)
						chkSecondaryStatusName_1[h++]=pojo.getSecondaryStatusId().toString();
				
				String [] chkSecondaryStatusName_2=new String[(chkSecondaryStatusName.length+chkSecondaryStatusName_1.length)];
				int d=0;
				for(int i1=0;i1<chkSecondaryStatusName.length;i1++)
					chkSecondaryStatusName_2[d++]=chkSecondaryStatusName[i1];
				
				for(int i2=0;i2<chkSecondaryStatusName_1.length;i2++)
					chkSecondaryStatusName_2[d++]=chkSecondaryStatusName_1[i2];
				
				districtMaster.setChkSecondaryStatusName(chkSecondaryStatusName_2);
			}
			else
			{
				districtMaster.setChkSecondaryStatusName(chkSecondaryStatusName);	
			}*/
			
			
			// End ... verify new secondary Status
			//map.addAttribute("displayTMDefaultJobCategory", districtMaster.getDisplayTMDefaultJobCategory());
			List<StatusWiseAutoEmailSend> listStatusWiseAutoEmailSends =null; 
			listStatusWiseAutoEmailSends = 	statusWiseAutoEmailSendDAO.findAllRecords(districtMaster);	
			
			List<Integer> statusIDs = new ArrayList<Integer>();
			List<Integer> sStatusID = new ArrayList<Integer>();
			
			if(listStatusWiseAutoEmailSends!=null){
				 for(StatusWiseAutoEmailSend swes : listStatusWiseAutoEmailSends){
					 if(swes.getStatusId()!=null)
					 {
					  //System.out.println("1111111122 :: "+swes.getStatusId());
					  statusIDs.add(swes.getStatusId());
					 }
					
					 if(swes.getSecondaryStatusId()!=null)
					 {
					 // System.out.println("222222222 :: "+swes.getSecondaryStatusId());
					  sStatusID.add( swes.getSecondaryStatusId());
					 }
				 }
			}
			
			
			String [] chkStatusMasterEml=new String[statusIDs.size()];
			
			int k1=0;
			for(Integer pojo:statusIDs)
				chkStatusMasterEml[k1++]=pojo.toString();
			districtMaster.setChkStatusMasterEml(chkStatusMasterEml);
			
			
			String [] chkSecondaryStatusNameEml=new String[sStatusID.size()];
			
			int k2=0;
			for(Integer pojo:sStatusID)
				chkSecondaryStatusNameEml[k2++]=pojo.toString();
			districtMaster.setChkSecondaryStatusNameEml(chkSecondaryStatusNameEml);
			
			if(statusIDs.size()>0 || sStatusID.size()>0){
				districtMaster.setAutoEmailRequired(true);
			}
			map.addAttribute("sendNotificationOnNegativeQQValue", districtMaster.getSendNotificationOnNegativeQQ());
			
			if(districtMaster != null && districtMaster.getHeadQuarterMaster() != null && districtMaster.getHeadQuarterMaster().getHeadQuarterId() == DashboardAjax.NC_HEADQUARTER){
			  map.addAttribute("headQuarterId",districtMaster.getHeadQuarterMaster().getHeadQuarterId());
			  map.addAttribute("sendReferenceOnJobCompleteNC",districtMaster.getSendReferenceOnJobComplete());
			  
			}  
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return "editdistrict";
	}
	
	public int createDistrictStatusHome(HttpServletRequest request,DistrictMaster districtMaster)
	{
		HttpSession session = request.getSession();
		if(session == null ||(session.getAttribute("teacherDetail")==null && session.getAttribute("userMaster")==null)) 
		{
			throw new IllegalStateException(Utility.getLocaleValuePropByKey("msgYrSesstionExp", locale));
	    }
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		SecondaryStatus secondaryStatus = new SecondaryStatus();
		List<SecondaryStatus> lstIsStatusHomeCreated=	null;
		try 
		{
			Criterion criterion1				=	Restrictions.eq("districtMaster", districtMaster);
			lstIsStatusHomeCreated				=	secondaryStatusDAO.findByCriteria(criterion1);
			if(lstIsStatusHomeCreated.size()==0){
				
				SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
				List<StatusNodeMaster> statusNodeMasterList= new ArrayList<StatusNodeMaster>();
				statusNodeMasterList=statusNodeMasterDAO.findStatusNodeList();
				
				List<StatusMaster> statusMasterList= new ArrayList<StatusMaster>();
				statusMasterList=statusMasterDAO.findStatusNodeList();
				
				for(StatusNodeMaster statusNodeMaster :statusNodeMasterList){
					secondaryStatus=new SecondaryStatus();
					secondaryStatus.setUsermaster(userSession);
					secondaryStatus.setDistrictMaster(districtMaster);
					secondaryStatus.setSecondaryStatusName(statusNodeMaster.getStatusNodeName());
					secondaryStatus.setOrderNumber(statusNodeMaster.getOrderNumber());
					secondaryStatus.setStatus("A");
					secondaryStatus.setStatusNodeMaster(statusNodeMaster);
					secondaryStatus.setCreatedDateTime(new Date());
					statelesSsession.insert(secondaryStatus);
				}
				txOpen.commit();
		       	statelesSsession.close(); 
		       	
				statelesSsession = sessionFactory.openStatelessSession();
				txOpen =statelesSsession.beginTransaction();
		      
		    	List<SecondaryStatus>	secondaryStatusList=	secondaryStatusDAO.findByCriteria(criterion1);
				SecondaryStatus secondaryStatusObj=new SecondaryStatus();
				for(StatusMaster statusMaster :statusMasterList){
					secondaryStatusObj=getSecondaryStatus(secondaryStatusList,statusMaster.getStatusNodeMaster());
					secondaryStatus=new SecondaryStatus();
					secondaryStatus.setUsermaster(userSession);
					secondaryStatus.setDistrictMaster(districtMaster);
					secondaryStatus.setSecondaryStatusName(statusMaster.getStatus());
					secondaryStatus.setOrderNumber(statusMaster.getOrderNumber());
					secondaryStatus.setStatus("A");
					secondaryStatus.setStatusMaster(statusMaster);
					secondaryStatus.setSecondaryStatus(secondaryStatusObj);
					secondaryStatus.setCreatedDateTime(new Date());
					statelesSsession.insert(secondaryStatus);
				}
				txOpen.commit();
		       	statelesSsession.close(); 
				 return 2;
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return 1;
	}
	
	public SecondaryStatus getSecondaryStatus(List<SecondaryStatus> secondaryStatusList,StatusNodeMaster statusNodeMaster)
	{
		SecondaryStatus secondaryStatusObj= new SecondaryStatus();
		try {
				if(secondaryStatusList.size()>0)
				for(SecondaryStatus secondaryStatus: secondaryStatusList){
					if(secondaryStatus.getStatusNodeMaster().getStatusNodeId().equals(statusNodeMaster.getStatusNodeId())){
						secondaryStatusObj=secondaryStatus;
					}
				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatusObj;
	}
	
	@RequestMapping(value="/editdistrict.do", method=RequestMethod.POST)
	public String doEditDistrictPOST(@ModelAttribute(value="districtMaster") DistrictMaster districtMaster,BindingResult result, ModelMap map,HttpServletRequest request)
	{
		System.out.println(">>>>>>>>>>>>>>>>>>>> For save district :: "+districtMaster.getsACreateDistJOb()+"========"+districtMaster.getsAEditUpdateJob());
		

		
		
		
		HttpSession session = request.getSession(false);
		ServletContext servletContext = request.getSession().getServletContext();
		UserMaster userMaster = null;
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		String  authKeyVal="2";
		try 
		{
			
			userMaster=(UserMaster)session.getAttribute("userMaster");
			/*============= For updating Not Editable Field ===================*/
			
			
			DistrictMaster distMaster 					=	districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
			createDistrictStatusHome(request,distMaster);
			if(districtMaster.getDistrictId()!=0)
			{
				districtMaster.setDistrictId(districtMaster.getDistrictId());
			}
			if(!districtMaster.getApprovalBeforeGoLive() && (distMaster.getNoOfApprovalNeeded()==null || districtMaster.getNoOfApprovalNeeded()==null)){
				districtMaster.setNoOfApprovalNeeded(distMaster.getNoOfApprovalNeeded());
			}
			
			if(districtMaster!=null)
			{
				
				
				
				ApplitrackDistricts applitrackDistricts = applitrackDistrictsDAO.findByDistrictId(districtMaster);
				if(applitrackDistricts!=null)
				{	
					String clientCode = request.getParameter("clientCode");
					String apiUserName = request.getParameter("apiUserName");
					String apiPassword = request.getParameter("apiPassword");
					
					System.out.println("clientCode:: "+clientCode);
					System.out.println("apiUserName:: "+apiUserName);
					System.out.println("apiPassword:: "+apiPassword);
					if(clientCode!=null)
					clientCode = clientCode.trim();
					applitrackDistricts.setClientcode(clientCode);
					if(apiUserName!=null)
					applitrackDistricts.setUserName(apiUserName.trim());
					if(apiPassword!=null)
					applitrackDistricts.setPassword(apiPassword.trim());
					String apiURL = "https://www.applitrack.com/"+clientCode+"/api/applitrackapi.svc/";
					String realm = clientCode+" AppliTrack API";
					applitrackDistricts.setApiURL(apiURL);
					applitrackDistricts.setRealm(realm);
					
					try {
						applitrackDistrictsDAO.makePersistent(applitrackDistricts);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
				}
			}
			
			/*********Logo Upload File And AssessmentUploadURLFile *********/
			try{
				if(districtMaster.getLogoPathFile().getSize()>0){
					
					if(distMaster.getLogoPath()!=null){
						try{
							String filePath="";
							filePath=Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/"+distMaster.getLogoPath();
							File file = new File(filePath);
				    		if(file.delete()){
				    			//System.out.println(file.getName() + " is deleted!");
				    		}else{
				    			//System.out.println("Delete operation is failed.");
				    		}
						}catch(Exception e){
					 		e.printStackTrace();
					  	}
					}
					
					String filePath=Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/";
					
					File f=new File(filePath);
					if(!f.exists())
						 f.mkdirs();
					
					FileItem fileItem =districtMaster.getLogoPathFile().getFileItem();
					String fileName="Logo"+Utility.getUploadFileName(fileItem);
					fileItem.write(new File(filePath, fileName));
					ImageResize.resizeImage(filePath+ fileName);
					districtMaster.setLogoPath(fileName);
					try{
					String target = servletContext.getRealPath("/")+"/"+"/district/"+districtMaster.getDistrictId()+"/";
					f=new File(target);
					if(!f.exists())
						 f.mkdirs();
					fileItem.write(new File(target, fileName));
					ImageResize.resizeImage(target+ fileName);
					}catch(Exception e){e.printStackTrace();}
					
				}else{
					districtMaster.setLogoPath(distMaster.getLogoPath());
				}
				int cbxUploadAssessment=0;
				if(request.getParameter("cbxUploadAssessment")!=null){
					if(request.getParameter("cbxUploadAssessment").equals("on")){
						cbxUploadAssessment=1;
					}else{
						cbxUploadAssessment=0;
					}
				}else if(request.getParameter("aJobRelation")!=null && !request.getParameter("aJobRelation").equals("")){
					cbxUploadAssessment=1;
				}
				if(districtMaster.getAssessmentUploadURLFile().getSize()>0 && cbxUploadAssessment==1){
					if(distMaster.getAssessmentUploadURL()!=null){
						try{
							String filePath="";
							filePath=Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/"+distMaster.getAssessmentUploadURL();
							File file = new File(filePath);
				    		if(file.delete()){
				    			//System.out.println(file.getName() + " is deleted!");
				    		}else{
				    			//System.out.println("Delete operation is failed.");
				    		}
						}catch(Exception e){
					 		e.printStackTrace();
					  	}
					}
					
					String filePath=Utility.getValueOfPropByKey("districtRootPath")+districtMaster.getDistrictId()+"/";
					
					File f=new File(filePath);
					if(!f.exists())
						 f.mkdirs();
					
					FileItem fileItem =districtMaster.getAssessmentUploadURLFile().getFileItem();
					String fileName="Inventory"+Utility.getUploadFileName(fileItem);
					fileItem.write(new File(filePath, fileName));
					districtMaster.setAssessmentUploadURL(fileName);
				}else{
					if(cbxUploadAssessment==1){
						districtMaster.setAssessmentUploadURL(distMaster.getAssessmentUploadURL());
					}else{
						districtMaster.setAssessmentUploadURL(null);
					}
				}
			}catch(FileNotFoundException e){
				districtMaster.setAssessmentUploadURL(distMaster.getAssessmentUploadURL());
				districtMaster.setLogoPath(distMaster.getLogoPath());
				e.printStackTrace();
			}
			
			districtMaster.setLogoPathFile(null);
			districtMaster.setAssessmentUploadURLFile(null);
			
			
			/*mukesh 
			 * saving status/secoundry  status for  Auto Email sending
			 */
			//*******************************************************************************************
			boolean deletedData=false;
			List<StatusWiseAutoEmailSend> allData   =    null;
			                              allData   = statusWiseAutoEmailSendDAO.findAllRecords(districtMaster);
			      if(allData!=null)
			         deletedData = statusWiseAutoEmailSendDAO.deleteRecords(allData);
			
			StatusWiseAutoEmailSend stAutoEmail =new StatusWiseAutoEmailSend();
			
			SessionFactory sessionFactory1=statusPrivilegeForSchoolsDAO.getSessionFactory();
   	 		StatelessSession statelesSsession1 = sessionFactory1.openStatelessSession();
   	 		Transaction txOpen1 =statelesSsession1.beginTransaction();
			try{
				if(districtMaster.getChkStatusMasterEml()!=null)
				{
					String [] chkStatusMasterEml=districtMaster.getChkStatusMasterEml();
					if(chkStatusMasterEml!=null && chkStatusMasterEml.length>0)
					{
						for(String str : chkStatusMasterEml)
						{
							stAutoEmail.setDistrictId(districtMaster);
							stAutoEmail.setStatusId(Integer.parseInt(str));
							stAutoEmail.setAutoEmailRequired(1);
							stAutoEmail.setUsermaster(userMaster);
							stAutoEmail.setCreatedDateTime(new Date());
							statelesSsession1.insert(stAutoEmail);
						}
					}
				}
				
			}catch(Exception e){}
			stAutoEmail.setStatusId(null);
			
			try{
				if(districtMaster.getChkSecondaryStatusNameEml()!=null)
				{
					String [] chkSecondaryStatusNameEml=districtMaster.getChkSecondaryStatusNameEml();
					if(chkSecondaryStatusNameEml!=null && chkSecondaryStatusNameEml.length>0)
					{
						for(String str : chkSecondaryStatusNameEml){
							stAutoEmail.setDistrictId(districtMaster);
							stAutoEmail.setSecondaryStatusId(Integer.parseInt(str));
							stAutoEmail.setAutoEmailRequired(1);
							stAutoEmail.setUsermaster(userMaster);
							stAutoEmail.setCreatedDateTime(new Date());
							statelesSsession1.insert(stAutoEmail);
							
						}
					}
				}
				
			}catch(Exception e){}
			txOpen1.commit();
	       	statelesSsession1.close();
	       //********************************************************************************************		
		
			if(request.getParameter("isZoneRequired")==null)
			districtMaster.setIsZoneRequired((Integer)0);
			else
			districtMaster.setIsZoneRequired(Integer.parseInt(request.getParameter("isZoneRequired")));
			//************* Start ... Privilege For School(s) *******************
			StatusMaster statusMaster=new StatusMaster();
			SecondaryStatus secondaryStatus=new SecondaryStatus();
			Integer statusIdForSchoolPrivilege=null;
			Integer secondaryStatusIdForSchoolPrivilege=null;
			String statusListBox=request.getParameter("statusListBox");
			
			if(statusListBox!=null && !statusListBox.equalsIgnoreCase(""))
			{
				if(statusListBox.contains("SSID_"))
				{
					secondaryStatusIdForSchoolPrivilege=new Integer(statusListBox.substring(5));
					secondaryStatus=secondaryStatusDAO.findById(secondaryStatusIdForSchoolPrivilege, false, false);
					if(secondaryStatus!=null)
						districtMaster.setSecondaryStatus(secondaryStatus);
					districtMaster.setStatusMaster(null);
				}
				else
				{
					statusIdForSchoolPrivilege=new Integer(statusListBox);
					statusMaster=WorkThreadServlet.statusIdMap.get(statusIdForSchoolPrivilege);
					if(statusMaster!=null)
						districtMaster.setStatusMaster(statusMaster);
					districtMaster.setSecondaryStatus(null);
				}
			}
			
			/* 
			 * 	Purpose		:	Set Reminder day and status
			 * 	BY			:	Hanzala Subhani
			 * 	Dated		:	10 Feb 2015
			 */
			
			String sendReminderToIcompCandiates		=	request.getParameter("sendReminderToIcompCandiates");
			String reminderFrequencyInDays			=	request.getParameter("reminderFrequencyInDays");
			String reminderOfFirstFrequencyInDays	=	request.getParameter("reminderOfFirstFrequencyInDays");
			String noOfReminder						=	request.getParameter("noOfReminder");
			
			try{
				if(sendReminderToIcompCandiates == null || sendReminderToIcompCandiates == ""){
					districtMaster.setReminderFrequencyInDays(null);
					districtMaster.setReminderOfFirstFrequencyInDays(null);
					districtMaster.setNoOfReminder(null);
					districtMaster.setStatusIdReminderExpireAction(null);
				} else {
					Integer statusIdReminderExpireActionVal=null;
					districtMaster.setNoOfReminder(Integer.parseInt(noOfReminder));
					String statusIdReminderExpireAction=request.getParameter("statusIdReminderExpireAction");
					if(statusIdReminderExpireAction != null && !statusIdReminderExpireAction.equalsIgnoreCase("")){
						statusIdReminderExpireActionVal = new Integer(statusIdReminderExpireAction);
						statusMaster = WorkThreadServlet.statusIdMap.get(statusIdReminderExpireActionVal);
						if(statusMaster != null){
							districtMaster.setStatusIdReminderExpireAction(statusMaster);
						}
					}
				}
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			/* End Reminder Status */
			
			/*
			 * 		Set Reminder for Portfolio
			 * 
			 * */
			
			String sendReminderForPortfolio					=	request.getParameter("sendReminderForPortfolio");
			String reminderFrequencyInDaysPortfolio			=	request.getParameter("reminderFrequencyInDaysPortfolio");
			String reminderOfFirstFrequencyInDaysPortfolio	=	request.getParameter("reminderOfFirstFrequencyInDaysPortfolio");
			String noOfReminderPortfolio					=	request.getParameter("noOfReminderPortfolio");
			
			try{
				if(sendReminderForPortfolio == null || sendReminderForPortfolio == ""){
					districtMaster.setReminderFrequencyInDaysPortfolio(null);
					districtMaster.setReminderOfFirstFrequencyInDaysPortfolio(null);
				    districtMaster.setNoOfReminderPortfolio(null);
				    districtMaster.setStatusIdReminderExpireActionPortfolio(null);
				    districtMaster.setStatusSendMailPortfolio(null);
				} else {
					Integer statusIdReminderExpireActionPortfolioVal=null;
					districtMaster.setNoOfReminderPortfolio(Integer.parseInt(noOfReminderPortfolio));
					String statusIdReminderExpireActionPortfolio=request.getParameter("statusIdReminderExpireActionPortfolio");
					Integer secondaryStatusSendMailPortfolio	=	null;
					
					String[] statusSendMailPortfolio = request.getParameterValues("statusSendMailPortfolio");
					String statusSendMailPortfolioVal = "";
					String secondaryStatusSendMailPortfolioVal = "";
					for(String item : statusSendMailPortfolio){
						if(item.contains("SSIDForReminder_")){
							secondaryStatusSendMailPortfolio=new Integer(item.substring(16));
							if(!item.equals("")){
								secondaryStatusSendMailPortfolioVal	=	secondaryStatusSendMailPortfolioVal+","+secondaryStatusSendMailPortfolio;
							}
						} else {
							if(!item.equals("")){
								statusSendMailPortfolioVal	=	statusSendMailPortfolioVal+","+item;
							}
						}
				    }
					if(statusSendMailPortfolioVal!=null && !statusSendMailPortfolioVal.equals(""))
						statusSendMailPortfolioVal			=	statusSendMailPortfolioVal.substring(1);
					
					if(secondaryStatusSendMailPortfolioVal!=null && !secondaryStatusSendMailPortfolioVal.equals(""))
						secondaryStatusSendMailPortfolioVal	=	secondaryStatusSendMailPortfolioVal.substring(1);

					if(statusIdReminderExpireActionPortfolio != null && !statusIdReminderExpireActionPortfolio.equalsIgnoreCase("")){
						statusIdReminderExpireActionPortfolioVal = new Integer(statusIdReminderExpireActionPortfolio);
						//statusMaster = WorkThreadServlet.statusIdMap.get(statusIdReminderExpireActionPortfolioVal);
						if(statusMaster != null){
							districtMaster.setStatusIdReminderExpireActionPortfolio(statusIdReminderExpireActionPortfolioVal);
						}
					}
					if((statusSendMailPortfolioVal != null && !statusSendMailPortfolioVal.equalsIgnoreCase("")) || (secondaryStatusSendMailPortfolioVal != null && !secondaryStatusSendMailPortfolioVal.equalsIgnoreCase(""))){
						districtMaster.setStatusSendMailPortfolio(statusSendMailPortfolioVal);
						districtMaster.setSecondaryStatusSendMailPortfolio(secondaryStatusSendMailPortfolioVal);
						//districtMaster.setStatusSendMailPortfolio("Status");
					}
					
				}
			} catch(Exception exception){
				exception.printStackTrace();
			}
			
			/*	End Section */
			
			StatusPrivilegeForSchools privilegeForSchools=new StatusPrivilegeForSchools();
			
			Map<String, Boolean> mapstaus=new HashMap<String, Boolean>();
			Map<String, Boolean> mapSstaus=new HashMap<String, Boolean>();
			
			String [] chkStatusMaster=districtMaster.getChkStatusMaster();
			String [] chkSecondaryStatusName=districtMaster.getChkSecondaryStatusName();
			
			/*Criterion criterion_dm				=	Restrictions.eq("districtMaster", districtMaster);
			Criterion criterion_sm				=	Restrictions.isNull("statusMaster");
			Criterion criterion_snm				=	Restrictions.isNull("statusNodeMaster");
			Criterion criterion_s				=	Restrictions.like("status","A");*/
			
			List<SecondaryStatus> lstsecondaryStatus=secondaryStatusDAO.findSecondaryStatusOnly(districtMaster);
			
			String[] statusShortName = {"hird","scomp","ecomp","vcomp","dcln","rem","widrw"};
			List<StatusMaster> lstStatusMaster	=	statusMasterDAO.findStatusByStatusByShortNames(statusShortName);
			
			List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchoolForSM=statusPrivilegeForSchoolsDAO.findStatusPrivilegeForStatusMaster(districtMaster);
			List<StatusPrivilegeForSchools> lstStatusPrivilegeForSchoolForSS=statusPrivilegeForSchoolsDAO.findStatusPrivilegeForSecondaryStatusId(districtMaster);
			// *************** Status Master *********************
			if(lstStatusMaster.size()>0)
				for(StatusMaster pojo:lstStatusMaster)
					mapstaus.put(""+pojo.getStatusId(), new Boolean(false));
			
			for(int i=0; i<chkStatusMaster.length; i++)
				if(mapstaus.get(chkStatusMaster[i])!=null)
					mapstaus.put(chkStatusMaster[i], new Boolean(true));
			
			// *************** Secondary Status Master *********************
			
			if(lstsecondaryStatus!=null && lstsecondaryStatus.size()>0)
			{
				for(SecondaryStatus pojo:lstsecondaryStatus)
					mapSstaus.put(""+pojo.getSecondaryStatusId(), new Boolean(false));
			}
			if(chkSecondaryStatusName!=null && chkSecondaryStatusName.length>0)
			{
				for(int i=0; i<chkSecondaryStatusName.length; i++)
					if(mapSstaus.get(chkSecondaryStatusName[i])!=null)
						mapSstaus.put(chkSecondaryStatusName[i], new Boolean(true));
			}
			
				/**** Session dynamic ****/
                SessionFactory sessionFactory=statusPrivilegeForSchoolsDAO.getSessionFactory();
       	 		StatelessSession statelesSsession = sessionFactory.openStatelessSession();
       	 		Transaction txOpen =statelesSsession.beginTransaction();
       	 
			if(districtMaster.getStatusPrivilegeForSchools())
			{
				// ************* Status Master ************************
				if(lstStatusPrivilegeForSchoolForSM.size()==0)
				{
					
					Iterator iter = mapstaus.entrySet().iterator();
					while (iter.hasNext()) 
					{
						Map.Entry mEntry = (Map.Entry) iter.next();
						
						Integer icanSchoolSetStatus=0;
						if(mEntry.getValue().equals(new Boolean(true)))
							icanSchoolSetStatus=1;
						
						int statusMasterId=Integer.parseInt(mEntry.getKey().toString());
						StatusMaster master=WorkThreadServlet.statusIdMap.get(new Integer(statusMasterId));
						
						privilegeForSchools=new StatusPrivilegeForSchools();
						privilegeForSchools.setDistrictMaster(districtMaster);
						privilegeForSchools.setStatusMaster(master);
						privilegeForSchools.setSecondaryStatus(null);
						privilegeForSchools.setCanSchoolSetStatus(icanSchoolSetStatus);
						privilegeForSchools.setUserMaster(userMaster);
						privilegeForSchools.setCreatedDateTime(new Date());
						//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
						statelesSsession.insert(privilegeForSchools);
					}
				}
				else
				{
					//Status Master :: Update and Insert
					privilegeForSchools=new StatusPrivilegeForSchools();
					boolean isFound=false;
					Iterator iter = mapstaus.entrySet().iterator();
					while (iter.hasNext()) 
					{
						Map.Entry mEntry = (Map.Entry) iter.next();
						
						Integer icanSchoolSetStatus=0;
						if(mEntry.getValue().equals(new Boolean(true)))
							icanSchoolSetStatus=1;
						int statusMasterId=Integer.parseInt(mEntry.getKey().toString());
						
						isFound=false;
						privilegeForSchools=new StatusPrivilegeForSchools();
						
						for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchoolForSM)
						{
							if(pojo.getStatusMaster().getStatusId()==statusMasterId)
							{
								privilegeForSchools=pojo;
								isFound=true;
							}
							
						}
						if(isFound)
						{
							privilegeForSchools.setCanSchoolSetStatus(icanSchoolSetStatus);
							//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
							statelesSsession.update(privilegeForSchools);
							
						}
						else
						{
							StatusMaster master=WorkThreadServlet.statusIdMap.get(new Integer(statusMasterId));
							privilegeForSchools.setDistrictMaster(districtMaster);
							privilegeForSchools.setStatusMaster(master);
							privilegeForSchools.setSecondaryStatus(null);
							privilegeForSchools.setCanSchoolSetStatus(1);
							privilegeForSchools.setUserMaster(userMaster);
							privilegeForSchools.setCreatedDateTime(new Date());
							//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
							statelesSsession.insert(privilegeForSchools);
							
						}
					}
				}
				
				
				//  **************** Secondary Status **********************
				
				if(lstStatusPrivilegeForSchoolForSS.size()==0)
				{
					Iterator iter_ss = mapSstaus.entrySet().iterator();
					while (iter_ss.hasNext()) 
					{
						Map.Entry mEntry = (Map.Entry) iter_ss.next();
						
						Integer icanSchoolSetStatus=0;
						if(mEntry.getValue().equals(new Boolean(true)))
							icanSchoolSetStatus=1;
						
						int secondaryStatusId=Integer.parseInt(mEntry.getKey().toString());
						SecondaryStatus secondaryStatus2=secondaryStatusDAO.findById(new Integer(secondaryStatusId), false, false);
						
						privilegeForSchools=new StatusPrivilegeForSchools();
						privilegeForSchools.setDistrictMaster(districtMaster);
						privilegeForSchools.setStatusMaster(null);
						privilegeForSchools.setSecondaryStatus(secondaryStatus2);
						privilegeForSchools.setCanSchoolSetStatus(icanSchoolSetStatus);
						privilegeForSchools.setUserMaster(userMaster);
						privilegeForSchools.setCreatedDateTime(new Date());
						//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
						statelesSsession.insert(privilegeForSchools);
					}
				
				}
				else
				{
					//Secondary Status Master :: Update and Insert
					
					System.out.println("********************** "+lstStatusPrivilegeForSchoolForSS.size());
					
					privilegeForSchools=new StatusPrivilegeForSchools();
					boolean isFound_ss=false;
					Iterator iter_ss = mapSstaus.entrySet().iterator();
					
					int v=0;
					
					while (iter_ss.hasNext()) 
					{
						v++;
						Map.Entry mEntry = (Map.Entry) iter_ss.next();
						
						Integer icanSchoolSetStatus=0;
						if(mEntry.getValue().equals(new Boolean(true)))
							icanSchoolSetStatus=1;
						int secondaryStatusMasterId=Integer.parseInt(mEntry.getKey().toString());
						
						isFound_ss=false;
						privilegeForSchools=new StatusPrivilegeForSchools();
						for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchoolForSS)
						{
							if(pojo.getSecondaryStatus().getSecondaryStatusId()==secondaryStatusMasterId)
							{
								privilegeForSchools=pojo;
								isFound_ss=true;
							}
							
						}
						if(isFound_ss)
						{
							privilegeForSchools.setCanSchoolSetStatus(icanSchoolSetStatus);
							statelesSsession.update(privilegeForSchools);
							
						}
						else
						{
							SecondaryStatus secondaryStatus2=secondaryStatusDAO.findById(new Integer(secondaryStatusMasterId), false, false);
							privilegeForSchools.setDistrictMaster(districtMaster);
							privilegeForSchools.setStatusMaster(null);
							privilegeForSchools.setSecondaryStatus(secondaryStatus2);
							privilegeForSchools.setCanSchoolSetStatus(1);
							privilegeForSchools.setUserMaster(userMaster);
							privilegeForSchools.setCreatedDateTime(new Date());
							statelesSsession.insert(privilegeForSchools);
						}
					}
					
				}
				
				// *************** End Secondary status ********************
			}
			else
			{
				//**************** Status Master *************************
				if(lstStatusPrivilegeForSchoolForSM.size()==0)
				{
					Iterator iter = mapstaus.entrySet().iterator();
					while (iter.hasNext()) 
					{
						Map.Entry mEntry = (Map.Entry) iter.next();
						Integer icanSchoolSetStatus=1;
						int statusMasterId=Integer.parseInt(mEntry.getKey().toString());
						StatusMaster master=WorkThreadServlet.statusIdMap.get(new Integer(statusMasterId));
						
						privilegeForSchools=new StatusPrivilegeForSchools();
						privilegeForSchools.setDistrictMaster(districtMaster);
						privilegeForSchools.setStatusMaster(master);
						privilegeForSchools.setSecondaryStatus(null);
						privilegeForSchools.setCanSchoolSetStatus(icanSchoolSetStatus);
						privilegeForSchools.setUserMaster(userMaster);
						privilegeForSchools.setCreatedDateTime(new Date());
						//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
						statelesSsession.insert(privilegeForSchools);
						
					}
				
				}
				else
				{
					//Update and Insert 1
					boolean isFound=false;
					Iterator iter = mapstaus.entrySet().iterator();
					while (iter.hasNext()) 
					{
						Map.Entry mEntry = (Map.Entry) iter.next();
						
						Integer icanSchoolSetStatus=1;
						int statusMasterId=Integer.parseInt(mEntry.getKey().toString());
						
						isFound=false;
						privilegeForSchools=new StatusPrivilegeForSchools();
						
						for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchoolForSM)
						{
							if(pojo.getStatusMaster().getStatusId()==statusMasterId)
							{
								privilegeForSchools=pojo;
								isFound=true;
							}
							
						}
						if(isFound)
						{
							privilegeForSchools.setCanSchoolSetStatus(icanSchoolSetStatus);
							//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
							statelesSsession.update(privilegeForSchools);
							
						}
						else
						{
							StatusMaster master=WorkThreadServlet.statusIdMap.get(new Integer(statusMasterId));
							privilegeForSchools.setDistrictMaster(districtMaster);
							privilegeForSchools.setStatusMaster(master);
							privilegeForSchools.setSecondaryStatus(null);
							privilegeForSchools.setCanSchoolSetStatus(1);
							privilegeForSchools.setUserMaster(userMaster);
							privilegeForSchools.setCreatedDateTime(new Date());
							//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
							statelesSsession.insert(privilegeForSchools);
						}
						
					}
					
				}
				
				//*********** Start :: Secondary Status ******************
				
				if(lstStatusPrivilegeForSchoolForSS.size()==0)
				{
					Iterator iter = mapSstaus.entrySet().iterator();
					while (iter.hasNext()) 
					{
						Map.Entry mEntry = (Map.Entry) iter.next();
						Integer icanSchoolSetStatus=1;
						int secondaryStatusId=Integer.parseInt(mEntry.getKey().toString());
						SecondaryStatus secondaryStatus2=secondaryStatusDAO.findById(new Integer(secondaryStatusId), false, false);
						
						privilegeForSchools=new StatusPrivilegeForSchools();
						privilegeForSchools.setDistrictMaster(districtMaster);
						privilegeForSchools.setStatusMaster(null);
						privilegeForSchools.setSecondaryStatus(secondaryStatus2);
						privilegeForSchools.setCanSchoolSetStatus(icanSchoolSetStatus);
						privilegeForSchools.setUserMaster(userMaster);
						privilegeForSchools.setCreatedDateTime(new Date());
						//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
						statelesSsession.insert(privilegeForSchools);
					}
				
				}
				else
				{
					//Update and Insert 1
					boolean isFound=false;
					Iterator iter = mapSstaus.entrySet().iterator();
					while (iter.hasNext()) 
					{
						Map.Entry mEntry = (Map.Entry) iter.next();
						
						Integer icanSchoolSetStatus=1;
						int secondaryStatusId=Integer.parseInt(mEntry.getKey().toString());
						
						isFound=false;
						privilegeForSchools=new StatusPrivilegeForSchools();
						
						for(StatusPrivilegeForSchools pojo:lstStatusPrivilegeForSchoolForSS)
						{
							if(pojo.getSecondaryStatus().getSecondaryStatusId()==secondaryStatusId)
							{
								privilegeForSchools=pojo;
								isFound=true;
							}
							
						}
						if(isFound)
						{
							privilegeForSchools.setCanSchoolSetStatus(icanSchoolSetStatus);
							//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
							statelesSsession.update(privilegeForSchools);
						}
						else
						{
							SecondaryStatus secondaryStatus2=secondaryStatusDAO.findById(new Integer(secondaryStatusId), false, false);
							privilegeForSchools.setDistrictMaster(districtMaster);
							privilegeForSchools.setStatusMaster(null);
							privilegeForSchools.setSecondaryStatus(secondaryStatus2);
							privilegeForSchools.setCanSchoolSetStatus(1);
							privilegeForSchools.setUserMaster(userMaster);
							privilegeForSchools.setCreatedDateTime(new Date());
							//statusPrivilegeForSchoolsDAO.makePersistent(privilegeForSchools);
							statelesSsession.insert(privilegeForSchools);
						}
						
					}
					
				}
				
				// ************ End Secondary Status ************ 
			}
			
			
			txOpen.commit();
	       	statelesSsession.close();
	       	
			//************* End ... Privilege For School(s) *******************
			
			//.............................................
			//System.out.println("districtMaster.getTimezone():  "+districtMaster.getTimezone().toString());
			//System.out.println("distMaster.getTimezone():  "+distMaster.getTimezone().toString());
			//districtMaster.setTimezone(distMaster.getTimezone());
	       	
	       	//.............................................
	       	
	       	
	       	
			/*********Edit Dates *********/
			String initiatedOnDate =null;
			String contractStartDate =null;
			String contractEndDate =null;
			
			if(request.getParameter("initiatedOnDate")!=null){
				if(!request.getParameter("initiatedOnDate").equals("")){
					initiatedOnDate=request.getParameter("initiatedOnDate").trim();
					districtMaster.setInitiatedOnDate(Utility.getCurrentDateFormart(initiatedOnDate));
				}
			}else{
				if(userMaster.getEntityType()==2){
					districtMaster.setInitiatedOnDate(distMaster.getInitiatedOnDate());
				}
			}
			if(request.getParameter("contractStartDate")!=null){
				if(!request.getParameter("contractStartDate").equals("")){
					contractStartDate=request.getParameter("contractStartDate").trim();
					districtMaster.setContractStartDate(Utility.getCurrentDateFormart(contractStartDate));
				}
			}else{
				if(userMaster.getEntityType()==2){
					districtMaster.setContractStartDate(distMaster.getContractStartDate());
				}
			}
			if(request.getParameter("contractEndDate")!=null){
				if(!request.getParameter("contractEndDate").equals("")){
					contractEndDate=request.getParameter("contractEndDate").trim();
					districtMaster.setContractEndDate(Utility.getCurrentDateFormart(contractEndDate));
				}
			}else{
				if(userMaster.getEntityType()==2){
					districtMaster.setContractEndDate(distMaster.getContractEndDate());
				}
			}
			if(userMaster.getEntityType()==2){
				districtMaster.setNoSchoolUnderContract(distMaster.getNoSchoolUnderContract());
				districtMaster.setTeachersUnderContract(distMaster.getTeachersUnderContract());
				districtMaster.setStudentsUnderContract(distMaster.getStudentsUnderContract());
				districtMaster.setSchoolsUnderContract(distMaster.getSchoolsUnderContract());
				districtMaster.setAnnualSubsciptionAmount(distMaster.getAnnualSubsciptionAmount());
				districtMaster.setAllGradeUnderContract(distMaster.getAllGradeUnderContract());
				districtMaster.setSelectedSchoolsUnderContract(distMaster.getSelectedSchoolsUnderContract());
			}
			if(userMaster.getEntityType()==2){
				districtMaster.setAllSchoolsUnderContract(distMaster.getAllSchoolsUnderContract());
			}else{
				if(districtMaster.getAllSchoolsUnderContract()!=null)
					if(districtMaster.getAllSchoolsUnderContract()==1)
						districtMaster.setAllSchoolsUnderContract(1);
					else
						districtMaster.setAllSchoolsUnderContract(2);
			}
			
			if(districtMaster.getPostingOnDistrictWall()==null){
				districtMaster.setPostingOnDistrictWall(0);
			}
			if(districtMaster.getPostingOnTMWall()==null){
				if(userMaster.getEntityType()!=1){
					districtMaster.setPostingOnTMWall(distMaster.getPostingOnTMWall());
				}else{
					districtMaster.setPostingOnTMWall(0);
				}
			}
			if(districtMaster.getAllowMessageTeacher()==null){
				districtMaster.setAllowMessageTeacher(0);
				districtMaster.setEmailForTeacher(null);
			}
			districtMaster.setAreAllSchoolsInContract(false);
			
			if(userMaster.getEntityType()==1){
				if(districtMaster.getIsWeeklyCgReport()==null){
					districtMaster.setIsWeeklyCgReport(0);
				}
			}else{
				districtMaster.setIsPortfolioNeeded(distMaster.getIsPortfolioNeeded());
				districtMaster.setIsWeeklyCgReport(distMaster.getIsWeeklyCgReport());
			}
			//StringEncrypter se= new StringEncrypter();
			if(distMaster.getAuthKey()==null || distMaster.getAuthKey().trim().equals("")){				
				String authorizationkey=(Utility.randomString(8)+Utility.getDateTime());
				authKeyVal=authorizationkey;
				//districtMaster.setAuthKey(se.encrypt(authorizationkey));
				districtMaster.setAuthKey(Utility.encodeInBase64(authorizationkey));
			}else{
				districtMaster.setAuthKey(distMaster.getAuthKey());
			}
			
			/*======== Setting  Non Editable Field Values ===========*/
			districtMaster.setLocationCode(distMaster.getLocationCode());
			
			districtMaster.setAccessDPoints(distMaster.getAccessDPoints());
			districtMaster.setDistrictName(distMaster.getDistrictName());
			districtMaster.setAddress(distMaster.getAddress());
			districtMaster.setNcesStateId(distMaster.getNcesStateId());
			districtMaster.setZipCode(distMaster.getZipCode());
			districtMaster.setStateId(distMaster.getStateId());
			districtMaster.setCityName(distMaster.getCityName());
			districtMaster.setPhoneNumber(distMaster.getPhoneNumber());
			districtMaster.setTotalNoOfSchools(distMaster.getTotalNoOfSchools());
			districtMaster.setTotalNoOfTeachers(distMaster.getTotalNoOfTeachers());
			districtMaster.setCreatedDateTime(distMaster.getCreatedDateTime());
			districtMaster.setStatus(distMaster.getStatus());
			if(distMaster.getDmPassword()!=	"" && distMaster.getDmPassword()!=	null){
				districtMaster.setDmPassword(distMaster.getDmPassword());
			}else{
				String to=districtMaster.getDmEmailAddress();
				String dmName= districtMaster.getDmName();
				String dmPassword=districtMaster.getDmPassword();
				
				String subject=Utility.getLocaleValuePropByKey("msgTeacherMatchAccountAdministrator1", locale);
				
				emailerService.sendMailAsHTMLText(to,subject,MailText.messageForFinalDecisionMaker(dmName,dmPassword,userMaster));
				districtMaster.setDmPassword(MD5Encryption.toMD5(dmPassword));
			}
			
			if(districtMaster.getFlagForURL()!=null)
			{	
				if(districtMaster.getFlagForURL()==1)
					districtMaster.setFlagForURL(1);
				else
					districtMaster.setFlagForURL(2);
			}
			else
			{
				districtMaster.setFlagForURL(2);
			}
			if(districtMaster.getFlagForMessage()!=null)
			{	
				if(districtMaster.getFlagForMessage()==1)
					districtMaster.setFlagForMessage(1);	
				else
					districtMaster.setFlagForMessage(2);
			}
			else
			{
				districtMaster.setFlagForMessage(2);
			}
			
			/*============  For No School  Under Contract  Radio Functionality ==================== */
			if(userMaster.getEntityType()==2){
				districtMaster.setPkOffered(distMaster.getPkOffered());
				districtMaster.setKgOffered(distMaster.getKgOffered());
				districtMaster.setG01Offered(distMaster.getG01Offered());
				districtMaster.setG02Offered(distMaster.getG02Offered());
				districtMaster.setG03Offered(distMaster.getG03Offered());
				districtMaster.setG04Offered(distMaster.getG04Offered());
				districtMaster.setG05Offered(distMaster.getG05Offered());
				districtMaster.setG06Offered(distMaster.getG06Offered());
				districtMaster.setG07Offered(distMaster.getG07Offered());
				districtMaster.setG08Offered(distMaster.getG08Offered());
				districtMaster.setG09Offered(distMaster.getG09Offered());
				districtMaster.setG10Offered(distMaster.getG10Offered());
				districtMaster.setG11Offered(distMaster.getG11Offered());
				districtMaster.setG12Offered(distMaster.getG12Offered());
			}else if((districtMaster.getNoSchoolUnderContract()!=null) && (districtMaster.getAllSchoolsUnderContract()==null) && (districtMaster.getAllGradeUnderContract()==null) && (districtMaster.getSelectedSchoolsUnderContract()==null))
			{
				districtMaster.setAllGradeUnderContract(2);
				districtMaster.setAllSchoolsUnderContract(2);
				districtMaster.setSelectedSchoolsUnderContract(2);
				districtMaster.setPkOffered(distMaster.getPkOffered());
				districtMaster.setKgOffered(distMaster.getKgOffered());
				districtMaster.setG01Offered(distMaster.getG01Offered());
				districtMaster.setG02Offered(distMaster.getG02Offered());
				districtMaster.setG03Offered(distMaster.getG03Offered());
				districtMaster.setG04Offered(distMaster.getG04Offered());
				districtMaster.setG05Offered(distMaster.getG05Offered());
				districtMaster.setG06Offered(distMaster.getG06Offered());
				districtMaster.setG07Offered(distMaster.getG07Offered());
				districtMaster.setG08Offered(distMaster.getG08Offered());
				districtMaster.setG09Offered(distMaster.getG09Offered());
				districtMaster.setG10Offered(distMaster.getG10Offered());
				districtMaster.setG11Offered(distMaster.getG11Offered());
				districtMaster.setG12Offered(distMaster.getG12Offered());
			}
			else
			{
				//*============  For All SchoolsUnderContract  Radio Functionality ====================
				if((districtMaster.getNoSchoolUnderContract()==null) && (districtMaster.getAllSchoolsUnderContract()!=null) && (districtMaster.getAllGradeUnderContract()==null) && (districtMaster.getSelectedSchoolsUnderContract()==null))
				{
					districtMaster.setAllGradeUnderContract(2);
					districtMaster.setSelectedSchoolsUnderContract(2);
					districtMaster.setNoSchoolUnderContract(2);
					districtMaster.setPkOffered(distMaster.getPkOffered());
					districtMaster.setKgOffered(distMaster.getKgOffered());
					districtMaster.setG01Offered(distMaster.getG01Offered());
					districtMaster.setG02Offered(distMaster.getG02Offered());
					districtMaster.setG03Offered(distMaster.getG03Offered());
					districtMaster.setG04Offered(distMaster.getG04Offered());
					districtMaster.setG05Offered(distMaster.getG05Offered());
					districtMaster.setG06Offered(distMaster.getG06Offered());
					districtMaster.setG07Offered(distMaster.getG07Offered());
					districtMaster.setG08Offered(distMaster.getG08Offered());
					districtMaster.setG09Offered(distMaster.getG09Offered());
					districtMaster.setG10Offered(distMaster.getG10Offered());
					districtMaster.setG11Offered(distMaster.getG11Offered());
					districtMaster.setG12Offered(distMaster.getG12Offered());
				}
				else
				{
					//============  For AllGradeUnderContract Radio Functionality ==================== 
					if((districtMaster.getNoSchoolUnderContract()==null) && (districtMaster.getAllSchoolsUnderContract()==null) && (districtMaster.getAllGradeUnderContract()!=null) && (districtMaster.getSelectedSchoolsUnderContract()==null))
					{
						districtMaster.setAllSchoolsUnderContract(2);
						districtMaster.setSelectedSchoolsUnderContract(2);
						districtMaster.setNoSchoolUnderContract(2);
					}
					else
					{
						//============  For SelectedSchoolsUnderContract Radio Functionality ==================== 
						if((districtMaster.getNoSchoolUnderContract()==null) && (districtMaster.getAllSchoolsUnderContract()==null) && (districtMaster.getAllGradeUnderContract()==null) && (districtMaster.getSelectedSchoolsUnderContract()!=null))
						{
							districtMaster.setAllSchoolsUnderContract(2);
							districtMaster.setAllGradeUnderContract(2);
							districtMaster.setNoSchoolUnderContract(2);
							districtMaster.setPkOffered(distMaster.getPkOffered());
							districtMaster.setKgOffered(distMaster.getKgOffered());
							districtMaster.setG01Offered(distMaster.getG01Offered());
							districtMaster.setG02Offered(distMaster.getG02Offered());
							districtMaster.setG03Offered(distMaster.getG03Offered());
							districtMaster.setG04Offered(distMaster.getG04Offered());
							districtMaster.setG05Offered(distMaster.getG05Offered());
							districtMaster.setG06Offered(distMaster.getG06Offered());
							districtMaster.setG07Offered(distMaster.getG07Offered());
							districtMaster.setG08Offered(distMaster.getG08Offered());
							districtMaster.setG09Offered(distMaster.getG09Offered());
							districtMaster.setG10Offered(distMaster.getG10Offered());
							districtMaster.setG11Offered(distMaster.getG11Offered());
							districtMaster.setG12Offered(distMaster.getG12Offered());
						}
						else
						{
							districtMaster.setSelectedSchoolsUnderContract(2);
						}
						if(districtMaster.getAllGradeUnderContract()!=null){
							if(districtMaster.getAllGradeUnderContract()==1)	
								districtMaster.setAllGradeUnderContract(1);
							else
								districtMaster.setAllGradeUnderContract(2);
						}else
							districtMaster.setAllGradeUnderContract(2);
					}
					if(districtMaster.getAllSchoolsUnderContract()!=null){
						if(districtMaster.getAllSchoolsUnderContract()==1)
							districtMaster.setAllSchoolsUnderContract(1);
						else
							districtMaster.setAllSchoolsUnderContract(2);
					}else
						districtMaster.setAllSchoolsUnderContract(2);
				}
				if(districtMaster.getNoSchoolUnderContract()!=null){
					if(districtMaster.getNoSchoolUnderContract()==1)
						districtMaster.setNoSchoolUnderContract(1);
					else
						districtMaster.setNoSchoolUnderContract(2);
				}else
					districtMaster.setNoSchoolUnderContract(2);
			}
			
			/******Delete District School******/
			if(districtMaster.getNoSchoolUnderContract().equals(1)){
				districtSchoolsDAO.deleteDistrictSchools(districtMaster);
			}
			
			/******Add District School******/
			if(districtMaster.getAllSchoolsUnderContract().equals(1)){
				districtSchoolsDAO.deleteDistrictSchools(districtMaster);
				Criterion criterionSchool = Restrictions.eq("districtId",districtMaster);
				List<SchoolMaster> lstSchoolMaster	=	schoolMasterDAO.findByCriteria(criterionSchool);
				districtSchoolsDAO.allSchoolInsertInDistrictSchools(lstSchoolMaster,districtMaster,userMaster);
			}
			if(districtMaster.getAllGradeUnderContract().equals(1)){
				districtSchoolsDAO.deleteDistrictSchools(districtMaster);
				Criterion criterionDSchool = Restrictions.eq("districtId",districtMaster);
				
				Criterion criterion1=null;
				Criterion criterion2=null;
				Criterion criterion3=null;
				Criterion criterion4=null;
				Criterion criterion5=null;
				Criterion criterion6=null;
				Criterion criterion7=null;
				Criterion criterion8=null;
				Criterion criterion9=null;
				Criterion criterion10=null;
				Criterion criterion11=null;
				Criterion criterion12=null;
				Criterion criterion13=null;
				Criterion criterion14=null;
			
				List<SchoolMaster> lstSchoolMaster=null;
				Criterion completeCondition =null;
				Disjunction disjunction = Restrictions.disjunction();
				
				if(districtMaster.getPkOffered()!=null)
				if(districtMaster.getPkOffered().equals(1)){
					criterion1 = Restrictions.eq("pkOffered",districtMaster.getPkOffered());
					disjunction.add(criterion1);
				}
				
				if(districtMaster.getKgOffered()!=null)
				if(districtMaster.getKgOffered().equals(1)){
					criterion2 = Restrictions.eq("kgOffered",districtMaster.getKgOffered());
					disjunction.add(criterion2);
				}
				
				if(districtMaster.getG01Offered()!=null)
				if(districtMaster.getG01Offered().equals(1) && districtMaster.getG01Offered()!=null){
					criterion3 = Restrictions.eq("g01Offered",districtMaster.getG01Offered());
					disjunction.add(criterion3);
				}
				
				if(districtMaster.getG02Offered()!=null)
				if(districtMaster.getG02Offered().equals(1)){
					criterion4 = Restrictions.eq("g02Offered",districtMaster.getG02Offered());
					disjunction.add(criterion4);
				}
				
				if(districtMaster.getG03Offered()!=null)
				if(districtMaster.getG03Offered().equals(1)){
					criterion5 = Restrictions.eq("g03Offered",districtMaster.getG03Offered());
					disjunction.add(criterion5);
				}
				if(districtMaster.getG04Offered()!=null)
				if(districtMaster.getG04Offered().equals(1)){
					criterion6 = Restrictions.eq("g04Offered",districtMaster.getG04Offered());
					disjunction.add(criterion6);
				}
				
				if(districtMaster.getG05Offered()!=null)
				if(districtMaster.getG05Offered().equals(1)){
					criterion7 = Restrictions.eq("g05Offered",districtMaster.getG05Offered());
					disjunction.add(criterion7);
				}
				
				if(districtMaster.getG06Offered()!=null)
				if(districtMaster.getG06Offered().equals(1)){
					criterion8 = Restrictions.eq("g06Offered",districtMaster.getG06Offered());
					disjunction.add(criterion8);
				}
				
				if(districtMaster.getG07Offered()!=null)
				if(districtMaster.getG07Offered().equals(1)){
					criterion9 = Restrictions.eq("g07Offered",districtMaster.getG07Offered());
					disjunction.add(criterion9);
				}
				
				if(districtMaster.getG08Offered()!=null)
				if(districtMaster.getG08Offered().equals(1)){
					criterion10 = Restrictions.eq("g08Offered",districtMaster.getG08Offered());
					disjunction.add(criterion10);
				}
				
				if(districtMaster.getG09Offered()!=null)
				if(districtMaster.getG09Offered().equals(1)){
					criterion11 = Restrictions.eq("g09Offered",districtMaster.getG09Offered());
					disjunction.add(criterion11);
				}
				
				if(districtMaster.getG10Offered()!=null)
				if(districtMaster.getG10Offered().equals(1)){
					criterion12 = Restrictions.eq("g10Offered",districtMaster.getG10Offered());
					disjunction.add(criterion12);
				}
				
				if(districtMaster.getG11Offered()!=null)
				if(districtMaster.getG11Offered().equals(1)){
					criterion13 = Restrictions.eq("g11Offered",districtMaster.getG11Offered());
					disjunction.add(criterion13);
				}
				
				if(districtMaster.getG12Offered()!=null)
				if(districtMaster.getG12Offered().equals(1)){
					criterion14 = Restrictions.eq("g12Offered",districtMaster.getG12Offered());
					disjunction.add(criterion14);
				}
				lstSchoolMaster	=	schoolMasterDAO.findByCriteria(criterionDSchool,disjunction);
				districtSchoolsDAO.allSchoolInsertInDistrictSchools(lstSchoolMaster,districtMaster,userMaster);
			}

			if(districtMaster.getIsResearchDistrict()==null || districtMaster.getIsResearchDistrict()==false)
				districtMaster.setIsResearchDistrict(false);
			else
				districtMaster.setIsResearchDistrict(true);
				
			/* ======== Gagan : For overridding disatrict settings on school settings ================*/
			//System.out.println("districtMaster.getCanSchoolOverrideJobFeed()::: "+districtMaster.getCanSchoolOverrideJobFeed());
			//System.out.println("districtMaster.getCanSchoolOverrideCandidateFeed()::: "+districtMaster.getCanSchoolOverrideCandidateFeed());
			List<SchoolMaster> lstschoolmaster=null;
			if(districtMaster.getCanSchoolOverrideJobFeed()==false || districtMaster.getCanSchoolOverrideCandidateFeed()==false)
			{
				Criterion criterion1	=	Restrictions.eq("districtId", districtMaster);
				Criterion criterion2	=	Restrictions.eq("lblCriticialJobName", "");
				Criterion criterion3	=	Restrictions.isNull("lblCriticialJobName");
				Criterion criterion4   = Restrictions.or(criterion2, criterion3);
				
				lstschoolmaster = schoolMasterDAO.findByCriteria(criterion1,criterion4);
				//System.out.println("lstschoolmaster.size()::: "+lstschoolmaster.size());
				for(SchoolMaster scm:lstschoolmaster)
				{
					if(districtMaster.getCanSchoolOverrideJobFeed()==false && districtMaster.getCanSchoolOverrideCandidateFeed()==false)
					{
						/* ========= Candidate Field =============== scm stands for school object of mosaic */
						scm.setCandidateFeedNormScore(districtMaster.getCandidateFeedNormScore());
						scm.setCandidateFeedDaysOfNoActivity(districtMaster.getCandidateFeedDaysOfNoActivity());
						
						/* ========= Job Feed Field ===============*/
						scm.setJobFeedCriticalJobActiveDays(districtMaster.getJobFeedCriticalJobActiveDays());
						scm.setJobFeedCriticalCandidateRatio(districtMaster.getJobFeedCriticalCandidateRatio());
						scm.setJobFeedCriticalNormScore(districtMaster.getJobFeedCriticalNormScore());
						scm.setLblCriticialJobName(districtMaster.getLblCriticialJobName());
						
						/* ========= Job Feed Attention Field ===============*/
						scm.setJobFeedAttentionJobActiveDays(districtMaster.getJobFeedAttentionJobActiveDays());
						scm.setJobFeedAttentionJobNotFilled(districtMaster.getJobFeedAttentionJobNotFilled());
						scm.setJobFeedAttentionNormScore(districtMaster.getJobFeedAttentionNormScore());
						scm.setLblAttentionJobName(districtMaster.getLblAttentionJobName());
						
						schoolMasterDAO.makePersistent(scm);
					}
					else
					{
						if(districtMaster.getCanSchoolOverrideCandidateFeed()==false)
						{
							scm.setCandidateFeedNormScore(districtMaster.getCandidateFeedNormScore());
							scm.setCandidateFeedDaysOfNoActivity(districtMaster.getCandidateFeedDaysOfNoActivity());
							schoolMasterDAO.makePersistent(scm);
						}
						else
						{
							/* ========= Job Feed Field ===============*/
							scm.setJobFeedCriticalJobActiveDays(districtMaster.getJobFeedCriticalJobActiveDays());
							scm.setJobFeedCriticalCandidateRatio(districtMaster.getJobFeedCriticalCandidateRatio());
							scm.setJobFeedCriticalNormScore(districtMaster.getJobFeedCriticalNormScore());
							scm.setLblCriticialJobName(districtMaster.getLblCriticialJobName());
							
							/* ========= Job Feed Attention Field ===============*/
							scm.setJobFeedAttentionJobActiveDays(districtMaster.getJobFeedAttentionJobActiveDays());
							scm.setJobFeedAttentionJobNotFilled(districtMaster.getJobFeedAttentionJobNotFilled());
							scm.setJobFeedAttentionNormScore(districtMaster.getJobFeedAttentionNormScore());
							scm.setLblAttentionJobName(districtMaster.getLblAttentionJobName());
							
							schoolMasterDAO.makePersistent(scm);
						}
					}
					
				}
			}
			
			// @Ashish :: insert ResetQualificationIssuesPrivilegeToSchool flag
			if(districtMaster.getResetQualificationIssuesPrivilegeToSchool()!=null)
			{
				districtMaster.setResetQualificationIssuesPrivilegeToSchool(districtMaster.getResetQualificationIssuesPrivilegeToSchool());
			}
			
			boolean offerDistrictSpecificItems=Boolean.valueOf(request.getParameter("offerDistrictSpecificItems"));
			boolean offerQualificationItems=Boolean.valueOf(request.getParameter("offerQualificationItems"));
			boolean offerEPI=Boolean.valueOf(request.getParameter("offerEPI"));
			boolean offerJSI=Boolean.valueOf(request.getParameter("offerJSI"));
			boolean offerPortfolioNeeded=Boolean.valueOf(request.getParameter("offerPortfolioNeeded"));
			
			districtMaster.setOfferDistrictSpecificItems(offerDistrictSpecificItems);
			districtMaster.setOfferQualificationItems(offerQualificationItems);
			districtMaster.setOfferEPI(offerEPI);
			districtMaster.setOfferJSI(offerJSI);
			districtMaster.setOfferPortfolioNeeded(offerPortfolioNeeded);
			//questDistrict
			//System.out.println("districtMaster.getQuestDistrict()::: "+districtMaster.getQuestDistrict());
			if(districtMaster.getQuestDistrict()==null)
				districtMaster.setQuestDistrict(0);
			
			
			/* ========== VVI Fields To Save */
			try
			{
				System.out.println("=======request.getParameter('maxScoreForVVI')) "+request.getParameter("maxScoreForVVI"));

				Integer quesId =0;
				int vviExpiresInDays = 0;
				int timeAllowedPerQuestion = 0;
				boolean offerVVI = false;
				boolean sendVVILink = false;
				
				Integer maxScore = null;
				if(request.getParameter("quesId")!=null && !request.getParameter("quesId").equals(""))
					quesId = Integer.parseInt(request.getParameter("quesId"));
				
				if(request.getParameter("maxScoreForVVI")!=null && !request.getParameter("maxScoreForVVI").equals(""))
					maxScore = Integer.parseInt(request.getParameter("maxScoreForVVI"));
				
				if(request.getParameter("VVIExpiresInDays")!=null && !request.getParameter("VVIExpiresInDays").equals(""))
					vviExpiresInDays = Integer.parseInt(request.getParameter("VVIExpiresInDays"));
				
				if(request.getParameter("timeAllowedPerQuestion")!=null && !request.getParameter("timeAllowedPerQuestion").equals(""))
					timeAllowedPerQuestion = Integer.parseInt(request.getParameter("timeAllowedPerQuestion"));
				
				if(request.getParameter("offerVirtualVideoInterview")!=null && !request.getParameter("offerVirtualVideoInterview").equals(""))
					offerVVI = Boolean.parseBoolean(request.getParameter("offerVirtualVideoInterview"));
				
				if(request.getParameter("sendAutoVVILink")!=null && !request.getParameter("sendAutoVVILink").equals(""))
					sendVVILink = Boolean.parseBoolean(request.getParameter("sendAutoVVILink"));
				
				
				
				I4QuestionSets i4QuestionSets = i4QuestionSetsDAO.findById(quesId, false, false);
				if(offerVVI==true)
				{
					StatusMaster statusMasterForVVI=new StatusMaster();
					SecondaryStatus secondaryStatusForVVI=new SecondaryStatus();
					Integer statusIdForSchoolPrivilegeForVVI=null;
					Integer secondaryStatusIdForSchoolPrivilegeForVVI=null;
					String statusListBoxForVVI=request.getParameter("statusListBoxForVVI");
					
					
					System.out.println("::::::::: maxScore ::::::::::: "+maxScore+"    statusListBoxForVVI   ::::: "+statusListBoxForVVI);
					
					districtMaster.setI4QuestionSets(i4QuestionSets);
					districtMaster.setMaxScoreForVVI(maxScore);
					districtMaster.setVVIExpiresInDays(vviExpiresInDays);
					districtMaster.setTimeAllowedPerQuestion(timeAllowedPerQuestion);
					districtMaster.setOfferVirtualVideoInterview(offerVVI);
					districtMaster.setSendAutoVVILink(sendVVILink);
					districtMaster.setI4QuestionSets(i4QuestionSets);
					
					if(sendVVILink==true)
					{
						if(statusListBoxForVVI!=null && !statusListBoxForVVI.equalsIgnoreCase(""))
						{
							if(statusListBoxForVVI.contains("SSIDForVVI_"))
							{
								secondaryStatusIdForSchoolPrivilegeForVVI=new Integer(statusListBoxForVVI.substring(11));
								secondaryStatusForVVI=secondaryStatusDAO.findById(secondaryStatusIdForSchoolPrivilegeForVVI, false, false);
								if(secondaryStatusForVVI!=null)
								{
									districtMaster.setSecondaryStatusForVVI(secondaryStatusForVVI);
								}
								districtMaster.setStatusMasterForVVI(null);
							}
							else
							{
								statusIdForSchoolPrivilegeForVVI=new Integer(statusListBoxForVVI);
								statusMasterForVVI=WorkThreadServlet.statusIdMap.get(statusIdForSchoolPrivilegeForVVI);
								if(statusMasterForVVI!=null)
								{
									districtMaster.setStatusMasterForVVI(statusMasterForVVI);
								}
								districtMaster.setSecondaryStatusForVVI(null);
							}
						}
					}
				}
				else
				{
					districtMaster.setOfferVirtualVideoInterview(false);
					districtMaster.setSendAutoVVILink(false);
					districtMaster.setMaxScoreForVVI(null);
					districtMaster.setI4QuestionSets(null);
					districtMaster.setTimeAllowedPerQuestion(null);
					districtMaster.setVVIExpiresInDays(null);
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			
			/*======== District Assessment Save===========*/
			try
			{
				boolean offerDistASMT = false;
				String distASMTIDSList = null;
				
				if(request.getParameter("offerAssessmentInviteOnly")!=null && !request.getParameter("offerAssessmentInviteOnly").equals(""))
					offerDistASMT = Boolean.parseBoolean(request.getParameter("offerAssessmentInviteOnly"));
				
				if(offerDistASMT)
				{
					distASMTIDSList = request.getParameter("distASMTIDSList");
					
					System.out.println(" ss >>>>>district assessment Data >>>>>>>>>>>>>>>>>>>>> ss :::: "+distASMTIDSList);
					
					
					
					if(request.getParameter("distASMTIDSList")!=null && !request.getParameter("distASMTIDSList").equals(""))
						distASMTIDSList = request.getParameter("distASMTIDSList");
					
					districtMaster.setOfferAssessmentInviteOnly(offerDistASMT);
					districtMaster.setDistrictAssessmentId(distASMTIDSList);
					
				}
				else
				{
					districtMaster.setOfferAssessmentInviteOnly(false);
					districtMaster.setDistrictAssessmentId(null);
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			/*============================================*/
			try {
				if(request.getParameter("qqAvlDistList")!=null && !request.getParameter("qqAvlDistList").equalsIgnoreCase("0")){
					String qqId =request.getParameter("qqAvlDistList");
					QqQuestionSets qQuestionSets= new QqQuestionSets();
					 qQuestionSets.setID(Integer.parseInt(qqId));
					 districtMaster.setQqQuestionSets(qQuestionSets);
				}else{
					districtMaster.setQqQuestionSets(null);
				}
			} catch (Exception e) {
			}
			try {
				if(request.getParameter("qqAvlDistListForOnboard")!=null && !request.getParameter("qqAvlDistListForOnboard").equalsIgnoreCase("0")){
					String qqId =request.getParameter("qqAvlDistListForOnboard");
					QqQuestionSets qQuestionSets= new QqQuestionSets();
					 qQuestionSets.setID(Integer.parseInt(qqId));
					 districtMaster.setQqQuestionSetsForOnboarding(qQuestionSets);
				}else{
					districtMaster.setQqQuestionSetsForOnboarding(null);
				}
			} catch (Exception e) {
			}
			
			try
			{
				
				StatusMaster statusMasterForCC=new StatusMaster();
				SecondaryStatus secondaryStatusForCC=new SecondaryStatus();
				Integer statusIdForSchoolPrivilegeForCC=null;
				Integer secondaryStatusIdForSchoolPrivilegeForCC=null;
				String statusListBoxForCC=request.getParameter("statusListBoxForCC");
				
				if(statusListBoxForCC!=null && !statusListBoxForCC.equalsIgnoreCase(""))
				{
					if(statusListBoxForCC.contains("SSIDForCC_"))
					{
						secondaryStatusIdForSchoolPrivilegeForCC=new Integer(statusListBoxForCC.substring(10));
						secondaryStatusForCC=secondaryStatusDAO.findById(secondaryStatusIdForSchoolPrivilegeForCC, false, false);
						if(secondaryStatusForCC!=null)
						{
							districtMaster.setSecondaryStatusForCC(secondaryStatusForCC);
						}
						districtMaster.setStatusMasterForCC(null);//------------------------------------------
					}
					else
					{
						statusIdForSchoolPrivilegeForCC=new Integer(statusListBoxForCC);
						statusMasterForCC=WorkThreadServlet.statusIdMap.get(statusIdForSchoolPrivilegeForCC);
						if(statusMasterForCC!=null)
						{
							districtMaster.setStatusMasterForCC(statusMasterForCC);
						}
						districtMaster.setSecondaryStatusForCC(null);//===================================================
					}
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			/* 	e-Reference Set Status 04-05-2015 */
			try
			{
				
				StatusMaster statusMasterForReference=new StatusMaster();
				SecondaryStatus secondaryStatusForReference=new SecondaryStatus();
				Integer statusIdForeReferenceFinalize			=	null;
				Integer secondaryStatusIdForeReferenceFinalize	=	null;
				String statusListBoxForReminder=request.getParameter("statusListBoxForReminder");
				if(statusListBoxForReminder!=null && !statusListBoxForReminder.equalsIgnoreCase(""))
				{
					if(statusListBoxForReminder.contains("SSIDForReminder_"))
					{
						secondaryStatusIdForeReferenceFinalize=new Integer(statusListBoxForReminder.substring(16));
						secondaryStatusForReference=secondaryStatusDAO.findById(secondaryStatusIdForeReferenceFinalize, false, false);
						if(secondaryStatusForReference!=null)
						{
							districtMaster.setSecondaryStatusForReference(secondaryStatusForReference);
						}
						districtMaster.setStatusMasterForReference(null);//------------------------------------------
					}
					else
					{
						statusIdForeReferenceFinalize=new Integer(statusListBoxForReminder);
						statusMasterForReference=WorkThreadServlet.statusIdMap.get(statusIdForeReferenceFinalize);
						if(statusMasterForReference!=null)
						{
							districtMaster.setStatusMasterForReference(statusMasterForReference);
						}
						districtMaster.setSecondaryStatusForReference(null);//===================================================
					}
				}
			}catch(Exception e)
			{
				e.printStackTrace();
			}
			
			/*	END Section */
			
	        
//Start Sandeep update headquarter in districtmaster for NC
			try {	
			String headQuarter = request.getParameter("headQuarterId");
			System.out.println("HeadQuarter Id########  "+headQuarter);
				if(headQuarter != null && !headQuarter.isEmpty() && Integer.parseInt(headQuarter) == DashboardAjax.NC_HEADQUARTER){
					HeadQuarterMaster headQuarterMaster = new HeadQuarterMaster();
					headQuarterMaster.setHeadQuarterId(Integer.parseInt(headQuarter));
					districtMaster.setHeadQuarterMaster(headQuarterMaster);
				}
			} catch (Exception e1) {
				e1.printStackTrace();
			}
			
//end			
			if(districtMaster!=null  && districtMaster.getDistrictId()==806900){
				districtMaster.setSkipApprovalProcess(true);
			}
			districtMasterDAO.makePersistent(districtMaster);
			
			try
			{
				userMaster.setDistrictId(districtMaster);
				userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Edited district");
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		} 
		catch (Exception e) 
		{
			e.printStackTrace();	
		}
		
		return "redirect:managedistrict.do?authKeyVal="+authKeyVal;
	}
	
		@RequestMapping(value="/displaystatus.do", method=RequestMethod.GET)
		public String doDispalyStatusGET(ModelMap map,HttpServletRequest request)
		{
			
			HttpSession session = request.getSession(false);

			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}
			UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
			List<SecondaryStatus> lstTreeStructure=	new ArrayList<SecondaryStatus>();
			
			StringBuffer sb =	new StringBuffer();
			try 
			{
				if(userSession.getEntityType()!=1){
					map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
					map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
				}else{
					map.addAttribute("DistrictName",null);
				}
				if(userSession.getEntityType()==3){
					map.addAttribute("SchoolName",userSession.getSchoolId().getSchoolName());
					map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
				}else{
					map.addAttribute("SchoolName",null);
				}
				try{
					if(userSession.getEntityType()!=1){
						lstTreeStructure =	secondaryStatusDAO.findSecondaryStatus(userSession.getDistrictId());
					}
				}catch(Exception e){
						e.printStackTrace();
				}
				sb.append("<ul>");
				for(SecondaryStatus tree: lstTreeStructure)
				{
					if(tree.getSecondaryStatus()==null)
					{
						
						sb.append("<li id='"+tree.getSecondaryStatusId()+"' class=\"folder\"  >"+tree.getSecondaryStatusName()+" ");
						if(tree.getChildren().size()>0)
						sb.append(printChild(request, tree.getChildren(),tree));
					}
				}
				sb.append("</ul>");
				
				map.addAttribute("entityType",userSession.getEntityType());;
				map.addAttribute("tree",sb.toString());
				
			}catch (Exception e) {
				e.printStackTrace();
			}		
			map.addAttribute("userMaster", userSession);
			return "displaystatus";
		}
	/*================  Gagan : Manage Status Related Function Start Here =============          */
	@RequestMapping(value={"/managestatus.do" , "/managestatusnew.do"}, method=RequestMethod.GET)   // add by Anurag 
	public String doManageStatusGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);
		 
	    if (session == null || session.getAttribute("userMaster") == null) 
	    {
	        return "redirect:index.jsp";
	    }
	    UserMaster userSession                    =    (UserMaster) session.getAttribute("userMaster");
	    session.setAttribute("displayType","0");
	    //List<SecondaryStatus> lstTreeStructure=    new ArrayList<SecondaryStatus>();
	    System.out.println("userSession.getEntityType()::::::::::"+userSession.getEntityType());
	    //StringBuffer sb =    new StringBuffer();
	    try 
	    {
	        if(userSession.getEntityType()!=1 && userSession.getEntityType()!=5){
	            map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
	            map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
	        }else{
	            map.addAttribute("DistrictName",null);
	        }
	        if(userSession.getEntityType()==3){
	            map.addAttribute("SchoolName",userSession.getSchoolId().getSchoolName());
	            map.addAttribute("SchoolId",userSession.getSchoolId().getSchoolId());
	        }else{
	            map.addAttribute("SchoolName",null);
	        }
	        
	        Integer districtId=0;
	        String sdistrictId="";
	        
	        Integer jobCategory=0;
	        String sjobCategory="";
	        
	        Integer jobSubCateId=0;
			String sJobSubCateId ="";
	        
	        sdistrictId=(request.getParameter("districtId")!=null)?request.getParameter("districtId").toString():"0";
	        sjobCategory=(request.getParameter("jobCategory")!=null)?request.getParameter("jobCategory").toString():"0";
	        
	        
	        sJobSubCateId=(request.getParameter("jobSubCategoryId")!=null)?request.getParameter("jobSubCategoryId").toString():"0";
	        
	        
	        int headQuarterId = 0;
	        if(userSession.getHeadQuarterMaster()!=null){
	            headQuarterId = userSession.getHeadQuarterMaster().getHeadQuarterId();
	        }else{
	            headQuarterId = Integer.parseInt((request.getParameter("headQuarterId")!=null)?request.getParameter("headQuarterId").toString():"0");    
	        }
	        System.out.println("headQuarterId::::::::>>>>>>>>>>>>>>>>>====>"+headQuarterId);
	        HeadQuarterMaster headQuarterMaster = null;
	        if(headQuarterId!=0){
	            headQuarterMaster = headQuarterMasterDAO.findById(headQuarterId, false, false);
	        }
	        if(headQuarterMaster!=null)
	        {
	            map.addAttribute("headQuarterName",headQuarterMaster.getHeadQuarterName());
	            map.addAttribute("headQuarterId",headQuarterId);
	        }
	        
	        int branchId = 0;
	        if(userSession.getBranchMaster()!=null){
	            branchId = userSession.getBranchMaster().getBranchId();
	        }else{
	            branchId = Integer.parseInt((request.getParameter("branchId")!=null)?request.getParameter("branchId").toString():"0");    
	        }
	        System.out.println("branchId::::::::>>>>>>>>>>>>>>>>>====>"+branchId);
	        
	        BranchMaster branchMaster = null;
	        if(branchId!=0){
	            branchMaster = branchMasterDAO.findById(branchId, false, false);
	        }
	        if(branchMaster!=null)
	        {
	            map.addAttribute("BranchName",branchMaster.getBranchName());
	            map.addAttribute("branchId",branchId);
	            
	        }
	        
	        if(!sdistrictId.equals("") && !sdistrictId.equals("0"))
	            districtId=Integer.parseInt(sdistrictId);
	        
	        DistrictMaster districtMaster=null;
	        
	        if(districtId!=0){
	            districtMaster=districtMasterDAO.findById(districtId, false, false);
	        }
	        
	        
	        if(districtMaster!=null)
	        {
	            map.addAttribute("DistrictName",districtMaster.getDistrictName());
	            map.addAttribute("DistrictId",districtId);
	            
	        }
	        
	        if(!sjobCategory.equals("") && !sjobCategory.equals("0"))
	        {
	            jobCategory=Integer.parseInt(sjobCategory);
	            map.addAttribute("jobCategory",jobCategory);
	        }
	        else
	        {
	            map.addAttribute("jobCategory","");
	        }
	        
	        if(!sJobSubCateId.equals("") && !sJobSubCateId.equals("0")){
				jobSubCateId=Integer.parseInt(sJobSubCateId);
				map.addAttribute("jobSubCateId",jobSubCateId);
			}else{
				map.addAttribute("jobSubCateId","");
			}
	        
	        
	        map.addAttribute("entityType",userSession.getEntityType());;
	        
	        List<SkillAttributesMaster> skillAttributesMasters=skillAttributesMasterDAO.getActiveList(jobCategory);
	        map.addAttribute("skillAttributesMasters", skillAttributesMasters);
	        
	        
	    }catch (Exception e) {
	        e.printStackTrace();
	    }        
	    map.addAttribute("userMaster", userSession);
	    return "managestatus";
	}
	
	@RequestMapping(value="/managenode.do")
	public String manageNode(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		
		try 
		{
			System.out.println("::::::::::::::::::::::::::::: managenode.do :::::::::::::::::::::::::::::::::::::");
			DistrictMaster districtMaster = userSession.getDistrictId();
			List<JobCategoryMaster> jobCategoryMasterList = jobCategoryMasterDAO.findAllJobCategoryNameByDistrict(districtMaster);
			
			System.out.println("DistrictName:- "+userSession.getDistrictId().getDistrictName());
			System.out.println("DistrictId:- "+userSession.getDistrictId().getDistrictId());
			System.out.println("jobCategoryMasterList:- "+jobCategoryMasterList);
			
			map.addAttribute("DistrictName",userSession.getDistrictId().getDistrictName());
			map.addAttribute("DistrictId",userSession.getDistrictId().getDistrictId());
			map.addAttribute("userMaster", userSession);
			map.addAttribute("jobCategoryMasterList",jobCategoryMasterList);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		
		return "managenode";
	}
	
	public String printChild(HttpServletRequest request,List<SecondaryStatus> lstuserChildren,SecondaryStatus tree)
	{
		StringBuffer sbchild=new StringBuffer();
		sbchild.append("<ul>");
		int banchmarkStatus=0;
		for(SecondaryStatus subfL: lstuserChildren ){
			if(subfL.getStatusMaster()!=null){
				if(subfL.getStatusMaster().getBanchmarkStatus()!=null){
					banchmarkStatus=subfL.getStatusMaster().getBanchmarkStatus();
				}
			}
			if(banchmarkStatus==0){	
				sbchild.append("<li id='"+subfL.getSecondaryStatusId()+"' class=\"folder\" >"+subfL.getSecondaryStatusName()+"");
				/*if(subfL.getChildren().size()>0){
					sbchild.append(printChild(request, subfL.getChildren(),subfL));
					sbchild.append("</li>");
				}*/
			}else if(subfL.getStatusMaster()==null){
				sbchild.append("<li id='"+subfL.getSecondaryStatusId()+"' class=\"folder\" >"+subfL.getSecondaryStatusName()+"");
			}
		}
		for(SecondaryStatus subfL: lstuserChildren){
			if(subfL.getStatusMaster()!=null){
				if(subfL.getStatusMaster().getStatusShortName().equals("hcomp")|| subfL.getStatusMaster().getStatusShortName().equals("vcomp")||subfL.getStatusMaster().getStatusShortName().equals("ecomp")||subfL.getStatusMaster().getStatusShortName().equals("scomp")){
					sbchild.append("<li id='"+subfL.getSecondaryStatusId()+"' class=\"folder\" >"+subfL.getSecondaryStatusName()+"");
				} 
			}
		}
		sbchild.append("</ul>");
		return sbchild.toString();
	}
	
	
	/*================  Sekahr : Inserted All District Default Secondary Status =============          */
	@RequestMapping(value="/secondarystatus.do", method=RequestMethod.GET)
	public String secondaryStatusGET(ModelMap map,HttpServletRequest request)
	{
		HttpSession session = request.getSession(false);

		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		UserMaster userSession					=	(UserMaster) session.getAttribute("userMaster");
		List<DistrictMaster> lstDistrictMaster=	null;
		List<Integer> lstDMaster=null;
		try 
		{
			lstDMaster				=	secondaryStatusDAO.getDistrictId();
			Criterion criterion1				=	Restrictions.eq("status","A");
			Criterion criterion2				=	Restrictions.not(Restrictions.in("districtId",lstDMaster));
			if(lstDMaster.size()!=0){
				lstDistrictMaster				=	districtMasterDAO.findByCriteria(criterion1,criterion2);
			}else{
				lstDistrictMaster				=	districtMasterDAO.findByCriteria(criterion1);
			}
			if(lstDistrictMaster.size()!=0){
				SessionFactory sessionFactory=secondaryStatusDAO.getSessionFactory();
				StatelessSession statelesSsession = sessionFactory.openStatelessSession();
				Transaction txOpen =statelesSsession.beginTransaction();
				
				List<StatusNodeMaster> statusNodeMasterList= new ArrayList<StatusNodeMaster>();
				statusNodeMasterList=statusNodeMasterDAO.findStatusNodeList();
				
				List<StatusMaster> statusMasterList= new ArrayList<StatusMaster>();
				statusMasterList=statusMasterDAO.findStatusNodeList();
				
				for(DistrictMaster districtMaster: lstDistrictMaster){
					for(StatusNodeMaster statusNodeMaster :statusNodeMasterList){
						SecondaryStatus secondaryStatus=new SecondaryStatus();
						secondaryStatus.setUsermaster(userSession);
						secondaryStatus.setDistrictMaster(districtMaster);
						secondaryStatus.setSecondaryStatusName(statusNodeMaster.getStatusNodeName());
						secondaryStatus.setOrderNumber(statusNodeMaster.getOrderNumber());
						secondaryStatus.setStatus("A");
						secondaryStatus.setStatusNodeMaster(statusNodeMaster);
						secondaryStatus.setCreatedDateTime(new Date());
						statelesSsession.insert(secondaryStatus);
					}
				}
				txOpen.commit();
		       	statelesSsession.close(); 
			       	
				statelesSsession = sessionFactory.openStatelessSession();
				txOpen =statelesSsession.beginTransaction();
				
		    	List<SecondaryStatus>	secondaryStatusList=	secondaryStatusDAO.findByCriteria(criterion1);
			    for(DistrictMaster districtMaster: lstDistrictMaster){
					SecondaryStatus secondaryStatusObj=new SecondaryStatus();
					for(StatusMaster statusMaster :statusMasterList){
						secondaryStatusObj=getSecondaryStatus(secondaryStatusList,statusMaster.getStatusNodeMaster(),districtMaster);
						SecondaryStatus secondaryStatus=new SecondaryStatus();
						secondaryStatus.setUsermaster(userSession);
						secondaryStatus.setDistrictMaster(districtMaster);
						secondaryStatus.setSecondaryStatusName(statusMaster.getStatus());
						secondaryStatus.setOrderNumber(statusMaster.getOrderNumber());
						secondaryStatus.setStatus("A");
						secondaryStatus.setStatusMaster(statusMaster);
						secondaryStatus.setSecondaryStatus(secondaryStatusObj);
						secondaryStatus.setCreatedDateTime(new Date());
						statelesSsession.insert(secondaryStatus);
					}
			    }
				txOpen.commit();
		       	statelesSsession.close(); 
				
			}
		}catch (Exception e) {
			e.printStackTrace();
		}		
		return null;
	}
	public SecondaryStatus getSecondaryStatus(List<SecondaryStatus> secondaryStatusList,StatusNodeMaster statusNodeMaster,DistrictMaster districtMaster)
	{
		SecondaryStatus secondaryStatusObj= new SecondaryStatus();
		try {
				if(secondaryStatusList.size()>0)
				for(SecondaryStatus secondaryStatus: secondaryStatusList){
					if(secondaryStatus.getDistrictMaster().equals(districtMaster) && secondaryStatus.getStatusNodeMaster().getStatusNodeId().equals(statusNodeMaster.getStatusNodeId())){
						secondaryStatusObj=secondaryStatus;
					}
				}	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return secondaryStatusObj;
	}
	
	/**
	 * @author Amit Chaudhary
	 * @param map
	 * @param request
	 * @return {@link String}
	 */
	@RequestMapping(value="/jobapprovalgroups.do", method=RequestMethod.GET)
	public String getGroups(ModelMap map,HttpServletRequest request)
	{
		System.out.println(":::::::::::::::::: jobapprovalgroups.do ::::::::::::::::::");
		HttpSession session = request.getSession(false);
		DistrictMaster districtMaster = null;
		
		try{
			//districtMaster = districtMasterDAO.findById(804800, false, false);
			int entityID=0;
			UserMaster userMaster=null;

			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				entityID=userMaster.getEntityType();
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}
				if(districtMaster!=null && entityID==1)
				{
					districtMaster = districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
				}
			}
			if(districtMaster!=null){
				map.addAttribute("districtMaster",districtMaster);
				System.out.println("districtMaster : "+districtMaster.getDistrictId());
			}
			
			String roleAccess=null;
			
			map.addAttribute("roleAccess", roleAccess);
			
			if(userMaster.getEntityType()!=1){
				map.addAttribute("DistrictOrSchoolName", userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId", userMaster.getDistrictId().getDistrictId());
			}
			else{
				map.addAttribute("DistrictOrSchoolName",null);
			}
			map.addAttribute("userMaster",userMaster);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "groups";
	}
	@RequestMapping(value="/jobapprovalprocess.do", method=RequestMethod.GET)
	public String getJobApprovalProcess(ModelMap map,HttpServletRequest request)
	{
		System.out.println(":::::::::::::::::: jobApprovalProcess.do ::::::::::::::::::");
		HttpSession session = request.getSession(false);
		if (session == null || session.getAttribute("userMaster") == null) 
		{
			return "redirect:index.jsp";
		}
		DistrictMaster districtMaster = null;
		
		try{
			//districtMaster = districtMasterDAO.findById(804800, false, false);
			int entityID=0;
			UserMaster userMaster=null;

			if (session == null || session.getAttribute("userMaster") == null) {
				//return "false";
			}else{
				userMaster=(UserMaster)session.getAttribute("userMaster");

				entityID=userMaster.getEntityType();
				if(userMaster.getDistrictId()!=null){
					districtMaster=userMaster.getDistrictId();
				}
				if(districtMaster!=null && entityID==1)
				{
					districtMaster = districtMasterDAO.findById(districtMaster.getDistrictId(), false, false);
				}
			}
			if(districtMaster!=null){
				map.addAttribute("districtMaster",districtMaster);
				System.out.println("districtMaster : "+districtMaster.getDistrictId());
			}
			
			String roleAccess=null;
			
			map.addAttribute("roleAccess", roleAccess);
			
			if(userMaster.getEntityType()!=1){
				map.addAttribute("DistrictOrSchoolName", userMaster.getDistrictId().getDistrictName());
				map.addAttribute("DistrictOrSchoolId", userMaster.getDistrictId().getDistrictId());
			}
			else{
				map.addAttribute("DistrictOrSchoolName",null);
			}
			map.addAttribute("userMaster",userMaster);
			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return "jobapprovalprocess";
	}
	//****************** Adding by Deepak ************************
	
	public synchronized static void managedistrictMethod(ModelMap map,HttpServletRequest request,RoleAccessPermissionDAO roleAccessPermissionDAO,UserLoginHistoryDAO userLoginHistoryDAO)
	{
		
		HttpSession session = request.getSession(false);
		int roleId=0;
		UserMaster	userMaster=null;
		
		userMaster=	(UserMaster) session.getAttribute("userMaster");
		if(userMaster.getRoleId().getRoleId()!=null){
			roleId=userMaster.getRoleId().getRoleId();
		}
			
	String authKeyVal = request.getParameter("authKeyVal")==null?"0":request.getParameter("authKeyVal").trim();
	
	String roleAccess=null;
	try{
		roleAccess=roleAccessPermissionDAO.getMenuOptionList(roleId,5,"managedistrict.do",0);
	}catch(Exception e){
		e.printStackTrace();
	}
	map.addAttribute("roleAccess", roleAccess);
	map.addAttribute("authKeyVal", authKeyVal);
	try{
		userLoginHistoryDAO.insertUserLoginHistory(userMaster,session.getId(),IPAddressUtility.getIpAddress(request),new Date(),"Enter in manage district");
	}catch(Exception e){
		e.printStackTrace();
	}

	}	
	
	@RequestMapping(value="/activenewdistrict.do", method=RequestMethod.GET)
	public String activeNewDistrict(ModelMap map,HttpServletRequest request)
	{
		try 
		{
			HttpSession session = request.getSession(false);
			int roleId=0;
			UserMaster	userMaster=null;
			if (session == null || session.getAttribute("userMaster") == null) 
			{
				return "redirect:index.jsp";
			}else{
				userMaster=	(UserMaster) session.getAttribute("userMaster");
				if(userMaster.getRoleId().getRoleId()!=null){
					roleId=userMaster.getRoleId().getRoleId();
				}
			}
			session.setAttribute("displayType","0");
			managedistrictMethod(map,request,roleAccessPermissionDAO,userLoginHistoryDAO);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return "activeNewDistrict";
	}
	
	//***************************** End *****************************************
}
