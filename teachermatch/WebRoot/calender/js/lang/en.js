Calendar.LANG("en", "English", {

        fdow: 1,                // first day of week for this locale; 0 = Sunday, 1 = Monday, etc.

        goToday: "Go Today",

        //today: "Today",         // appears in bottom bar

        wk: "wk",

        weekend: "0,6",         // 0 = Sunday, 1 = Monday, etc.

        AM: "am",

        PM: "pm",

        mn : [ "January",
               "February",
               "March",
               "April",
               "May",
               "June",
               "July",
               "August",
               "September",
               "October",
               "November",
               "December" ],

        smn : [ "&nbsp;Jan",
                "&nbsp;Feb",
                "&nbsp;Mar",
                "&nbsp;Apr",
                "&nbsp;May",
                "&nbsp;Jun",
                "&nbsp;Jul",
                "&nbsp;Aug",
                "&nbsp;Sep",
                "&nbsp;Oct",
                "&nbsp;Nov",
                "&nbsp;Dec" ],
        dn : [ "Sunday",
               "Monday",
               "Tuesday",
               "Wednesday",
               "Thursday",
               "Friday",
               "Saturday",
               "Sunday" ],

        sdn : [ "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa",
                "Su" ]

});
